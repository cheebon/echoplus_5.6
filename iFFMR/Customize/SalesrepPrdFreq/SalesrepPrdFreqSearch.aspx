<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesrepPrdFreqSearch.aspx.vb"
    Inherits="SalesrepPrdFreqSearch" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Salesrep Prd Freq search</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>

<script language="javascript" type="text/javascript">
function RefreshContentBar()
{
var TEAM_CODE = "";
var SALESREP_CODE = "";
var SALESREP_NAME= "";
var SALESTEAM_NAME= "";
var START_YEAR = "";
var START_MONTH = "";
var END_YEAR = "";
var END_MONTH = "";
var PTL_CODE = "";
var CLASS = ""; 

var lsbSelectedTeam = document.getElementById("lsbSelectedTeam");
for(var i=0;i<lsbSelectedTeam.options.length;i++)
{
    if(lsbSelectedTeam.options[i].selected = true)
    if (i > 0){TEAM_CODE = TEAM_CODE + ",";}
    TEAM_CODE = TEAM_CODE + lsbSelectedTeam.options[i].value ;
    if (i > 0){SALESTEAM_NAME = SALESTEAM_NAME + ","; }
    SALESTEAM_NAME = SALESTEAM_NAME + lsbSelectedTeam.options[i].text.split(" ")[2];
}

var lsbSelectedSalesrep = document.getElementById("lsbSelectedSalesrep");
for(var i=0;i<lsbSelectedSalesrep.options.length;i++)
{
    if(lsbSelectedSalesrep.options[i].selected = true)
    if (i > 0){SALESREP_CODE = SALESREP_CODE + ","; }
    SALESREP_CODE = SALESREP_CODE + lsbSelectedSalesrep.options[i].value ;
    if (i > 0){SALESREP_NAME = SALESREP_NAME + ","; }
    SALESREP_NAME = SALESREP_NAME + lsbSelectedSalesrep.options[i].text.split(" ")[2];}
    
var ddlstartdateyear = document.getElementById ("ddlstartdateyear");
START_YEAR = ddlstartdateyear.value
var ddlstartdatemonth = document.getElementById ("ddlstartdatemonth");
START_MONTH = ddlstartdatemonth.value
var ddlenddateyear = document.getElementById ("ddlenddateyear");
END_YEAR = ddlenddateyear.value
var ddlenddatemonth = document.getElementById ("ddlenddatemonth");
END_MONTH = ddlenddatemonth.value
var ddlptlcode = document.getElementById ("ddlptlcode");
PTL_CODE = ddlptlcode.value
var ddlclass = document.getElementById ("ddlclass");
CLASS = ddlclass.value

parent.document.getElementById('ContentBarIframe').src="../../iFFMR/Customize/SalesrepPrdFreq/SalesrepPrdFreqGrid.aspx?SALESREP_CODE=" + SALESREP_CODE + "&TEAM_CODE=" + TEAM_CODE+ "&START_YEAR=" + START_YEAR+ "&START_MONTH=" + START_MONTH+ "&END_YEAR=" + END_YEAR+ "&END_MONTH=" + END_MONTH+ "&PTL_CODE=" + PTL_CODE+ "&CLASS=" + CLASS;
}
function HideElement(element){var TopDiv =self.parent.document.getElementById(element);  TopDiv.style.display='none';


}
</script>

<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmprdfreqsearch" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <asp:Label ID="lblInfo" runat="server" CssClass="cls_label_err"></asp:Label>
                        <asp:UpdatePanel ID="UpdatePnlMultiAuthen" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="PnlMultiAuthen" runat="server">
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0" class="Bckgroundreport">
                                        <tr>
                                            <td style="width: 45%;">
                                            </td>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 45%;">
                                            </td>
                                        </tr>
                                        <tr id="Team Selection">
                                            <td valign="top" align="center">
                                                <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                                <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                    Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                            </td>
                                            <td valign="middle" style="padding: 0px 0px 0px 10px">
                                                <table>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkTeamAdd" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text=">" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkTeamRemove" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text="<" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkTeamAddAll" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text=">>" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkTeamRemoveAll" runat="server" CssClass="cls_button" Width="50"
                                                                Height="25" Font-Bold="true" Text="<<" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center">
                                                <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                                <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                    Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr id="Salesrep Selection">
                                            <td valign="top" align="center">
                                                <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                                <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                    Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                            </td>
                                            <td valign="middle" style="padding: 0px 0px 0px 10px">
                                                <table>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkSRAdd" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text=">" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkSRRemove" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text="<" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkSRAddAll" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text=">>" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkSRRemoveAll" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                                Font-Bold="true" Text="<<" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center">
                                                <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                                <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                    Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="Left">
                                                <asp:Panel ID="potherselection" runat="server">
                                                    <table style="width: 100%">
                                                        <tr id="StartDateSelection">
                                                            <td style="width: 10%">
                                                                <span id="lblselectedstartdate" class="cls_label_header">Start date</span>
                                                            </td>
                                                            <td>
                                                                <span id="lblstartdateyear" class="cls_label_header">Year:</span>
                                                                <asp:DropDownList ID="ddlstartdateyear" runat="server" CssClass="cls_dropdownlist">
                                                                </asp:DropDownList>
                                                                <span id="lblstartdatemonth" class="cls_label_header">Month:</span>
                                                                <asp:DropDownList ID="ddlstartdatemonth" runat="server" CssClass="cls_dropdownlist">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="EndDateSelection">
                                                            <td>
                                                                <span id="lblenddate" class="cls_label_header">End date</span>
                                                            </td>
                                                            <td>
                                                                <span id="lblenddateyear" class="cls_label_header">Year:</span>
                                                                <asp:DropDownList ID="ddlenddateyear" runat="server" CssClass="cls_dropdownlist">
                                                                </asp:DropDownList>
                                                                <span id="lblenddatemonth" class="cls_label_header">Month:</span>
                                                                <asp:DropDownList ID="ddlenddatemonth" runat="server" CssClass="cls_dropdownlist">
                                                                </asp:DropDownList>
                                                            <td>
                                                        </tr>
                                                        <tr id="Product">
                                                            <td>
                                                                <span id="lblproduct" class="cls_label_header">Product: </span>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlptlcode" runat="server" CssClass="cls_dropdownlist">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr id="class">
                                                            <td>
                                                                <span id="lblclass" class="cls_label_header">Product Class: </span>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlclass" runat="server" CssClass="cls_dropdownlist">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="Left">
                                                <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                                <%-- <asp:Button ID="btnRefresh" CssClass="cls_button" runat="server" Text="Refresh" ValidationGroup="Search">
                                                </asp:Button>--%>
                                                <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                                    value="Search" class="cls_button" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
