﻿Imports System.Data


Partial Class iFFMR_Customize_MerchSfms_MerchSfms
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_MerchSfms")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_MerchSfms") = value
        End Set
    End Property

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property


    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_MerchSfms"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region


#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 20))
        'Call Panel
        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.MERCHSFMS
            .DataBind()
            .Visible = True
        End With

        'Call Paging
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MERCHSFMS) '"Sales List"
                .DataBind()
                .Visible = True
            End With

            wuc_ctrlpanel.strRestoreGroupingTextValue = "Customer,Cust Type,Product,Product Group~CUST_CODE;CUST_TYPE;PRD_CODE;PRD_GRP_CODE"
         
            GroupingValue = String.Empty
            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    Session("TREE_PATH") = .Tree_Path
                    Session("NetValue") = .NetValue
                    GroupingValue = .GroupField
                    SortingExpression = .SortExpression
                    wuc_ctrlpanel.strRestoreGroupingTextValue = .GroupTextValue
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                CriteriaCollector = Nothing ' New clsSharedValue
            End If
            'wuc_ctrlpanel.pnlField_InsertPointer(True, 0, "Team")
            TimerControl1.Enabled = True
            'Try
            'Catch ex As Exception
            '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
            'End Try
        End If

        lblErr.Text = ""
    End Sub
    Public ReadOnly Property PageName() As String
        Get
            Return "MerchSfms"
        End Get
    End Property

#Region "Event Handler"
    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        If IsPostBack Then RenewDataBind()
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        RenewDataBind()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        GroupingValue = String.Empty
        RenewDataBind()
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub


#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            Dim style As String = "<style> .text { mso-number-format:\@; } </style> "
            wuc_ctrlpanel.ExportToFile(dgList, PageName, style)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try

    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub


   
#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)


        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        '    ViewState("dtCurrentView") = dtCurrentTable
        '    SortingExpression = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.PageSize = intPageSize
        dgList.DataBind()

        'Call Paging
        With wuc_dgpaging

            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
        End With
        wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 10 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MerchSfms.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MerchSfms.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MerchSfms.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MerchSfms.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_MerchSfms.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_MerchSfms.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields() As String = Nothing

                    strUrlFormatString = ""
                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing
                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case FieldColumntype.InvisibleColumn

                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MerchSfms.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_MerchSfms.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MerchSfms.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_MerchSfms.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_MerchSfms.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            
        End If
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                e.Row.Cells(aryDataItem.IndexOf("CHAIN")).Attributes.Add("class", "text")

            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_MerchSfms.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        
                    End If
                Next
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Dim strYear, strMonth, strUserID, strPrincipalId, strPrincipalCode As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalId = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")

        Dim strGroupCode, strGroupName, strSalesRepList As String
        strGroupCode = Session("NODE_GROUP_NAME")
        strGroupName = strGroupCode.Replace("_CODE", " (Tree)").Replace("_", " ")
        strSalesRepList = Session("SALESREP_LIST")

   
        Dim strGroupField As String
        If String.IsNullOrEmpty(GroupingValue) Then
            strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue
        Else
            strGroupField = GroupingValue
            GroupingValue = String.Empty
        End If

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .Year = strYear
            .Month = strMonth
            .PrincipalID = strPrincipalId
            .PrincipalCode = strPrincipalCode
            .Tree_Path = Session("TREE_PATH")
            .GroupField = strGroupField '
            .GroupTextValue = wuc_ctrlpanel.pnlField_GetGroupingTextValue
        End With

        Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        strGroupField = strGroupField.Replace(strPointer, "TREE_" & strGroupCode)
        wuc_ctrlpanel.pnlField_EditPointerText(strGroupName)

        Dim clsMerchSfms As New rpt_Customize.clsMerchSfms
        Try
            DT = clsMerchSfms.GetMerchSfmsList(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepList, strGroupField)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "SHELF_FACING" OrElse _
             strColumnName Like "SHELF_STOCK" OrElse _
             strColumnName Like "DISPLAY_STOCK" OrElse strColumnName Like "TOTAL_STOCK") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExportToXLSFromDataTable(ByVal dtExport As DataTable, ByVal filename As String)
        Dim dataToExport As New StringBuilder()

        dataToExport.Append("<table>")
        dataToExport.Append("<tr>")

        For Each dCol As DataColumn In dtExport.Columns
            dataToExport.Append("<td>")
            dataToExport.Append(Server.HtmlEncode(CF_MerchSfms.GetDisplayColumnName(dCol.ColumnName)))
            dataToExport.Append("</td>")
        Next

        dataToExport.Append("</tr>")

        For Each dRow As DataRow In dtExport.Rows
            dataToExport.Append("<tr>")
            For Each obj As Object In dRow.ItemArray
                dataToExport.Append("<td>")
                dataToExport.Append(Server.HtmlEncode(obj.ToString()))
                dataToExport.Append("</td>")
            Next
            dataToExport.Append("</tr>")
        Next

        dataToExport.Append("</table>")

        If Not String.IsNullOrEmpty(dataToExport.ToString()) Then
            Response.Clear()

            HttpContext.Current.Response.ContentType = "application/ms-excel"
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & filename & ".xls")

            HttpContext.Current.Response.Write(dataToExport.ToString())
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.[End]()
        End If
    End Sub

    Protected Sub btnExportAsCsv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportAsCsv.Click
        Dim dt As DataTable = GetRecList()
        ExportToCsv(dt, Response, PageName)

    End Sub

    Public Shared Sub ExportToCsv(ByVal dt As DataTable, ByRef MyResponse As System.Web.HttpResponse, ByVal strFileName As String)
        Dim context As HttpContext = HttpContext.Current
        context.Response.Clear()


        context.Response.Charset = "utf-8"
        context.Response.ContentEncoding = System.Text.Encoding.UTF8



        ' write the column names 
        For Each column As DataColumn In dt.Columns
            context.Response.Write(CF_MerchSfms.GetDisplayColumnName(column.ColumnName) + ",")
        Next
        context.Response.Write(Environment.NewLine)


        ' write the rows 
        For Each row As DataRow In dt.Rows
            For i As Integer = 0 To dt.Columns.Count - 1
                context.Response.Write(row(i).ToString().Replace(";", String.Empty) & ",")
            Next
            context.Response.Write(Environment.NewLine)
        Next
        context.Response.ContentType = "text/csv"
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" & strFileName & ".csv")


        context.Response.Flush()
        context.Response.[End]()
    End Sub

    Protected Sub btnExportAsWebExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportAsWebExcel.Click
        Dim dt As DataTable = GetRecList()
        ExportToXLSFromDataTable(dt, PageName)
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub
End Class


Public Class CF_MerchSfms
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "TXN_DATE"
                strFieldName = "Txn. Date"
            Case "TXN_TIME"
                strFieldName = "Txn. Time"
            Case "CUST_TYPE"
                strFieldName = "Cust. Type"
            Case "SHELF_FACING"
                strFieldName = "Shelf Facing"
            Case "SHELF_STOCK"
                strFieldName = "Shelf Stock"
            Case "DISPLAY_STOCK"
                strFieldName = "Display Stock"
            Case "TOTAL_STOCK"
                strFieldName = "Total Stock"
            Case "CAT_1_CODE"
                strFieldName = "Cat 1 Code"
            Case "CAT_1_NAME"
                strFieldName = "Hotspot"
            Case "CAT_2_CODE"
                strFieldName = "Cat 2 Code"
            Case "CAT_2_NAME"
                strFieldName = "4Ways"
            Case "CAT_3_CODE"
                strFieldName = "Cat 3 Code"
            Case "CAT_3_NAME"
                strFieldName = "TG"
            Case "CAT_4_CODE"
                strFieldName = "Cat 4 Code"
            Case "CAT_4_NAME"
                strFieldName = "Dump-bin"
            Case "CAT_5_CODE"
                strFieldName = "Cat 5 Code"
            Case "CAT_5_NAME"
                strFieldName = "特價標"
            Case "CAT_6_CODE"
                strFieldName = "Cat 6 Code"
            Case "CAT_6_NAME"
                strFieldName = "側網架"
            Case "CAT_7_CODE"
                strFieldName = "Cat 7 Code"
            Case "CAT_7_NAME"
                strFieldName = "吊掛"
            Case "CAT_8_CODE"
                strFieldName = "Cat 8 Code"
            Case "CAT_8_NAME"
                strFieldName = "D型筒"
            Case "CAT_9_CODE"
                strFieldName = "Cat 9 Code"
            Case "CAT_9_NAME"
                strFieldName = "藥局前櫃"
            Case "CAT_10_CODE"
                strFieldName = "Cat 10 Code"
            Case "CAT_10_NAME"
                strFieldName = "Backup 1"
            Case "CAT_11_CODE"
                strFieldName = "Cat 11 Code"
            Case "CAT_11_NAME"
                strFieldName = "Backup 2"
            Case "SFMS_DESC"
                strFieldName = "SFMS Desc"
            Case "CHAIN"
                strFieldName = "Chain Cust Code"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

       
        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "SHELF_FACING", "SHELF_STOCK", "DISPLAY_STOCK", "TOTAL_STOCK"
                strStringFormat = "{0:#,0}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class






