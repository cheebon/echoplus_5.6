﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SFMSConfig.aspx.vb" Inherits="Admin_BCP_SFMSConfig" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="wucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="wuclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustSearch" Src="~/iFFMA/Common/wuc_CustSearch.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SFMS Configuration</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script type="text/javascript" language="Javascript">
        function SelectRow(chkSelected) {
            if (chkSelected.checked == false) {
                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    var e = document.forms[0].elements[i];

                    if (e.type == 'checkbox') {
                        e.checked = chkSelected.checked;
                        break;
                    }
                }
            }
        }

        function SelectAllRows(chkAll) {

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox') {
                    e.checked = chkAll.checked;
                }
            }
        }

        function checkDelete() {
            var isChecked = false;

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox' && e.checked) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                if (!confirm('Are you sure want to delete?')) {
                    return false;
                }
            }
            else {
                alert('Please select at least one of the record to delete!');
                return false;
            }

            return true;
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSFMSConfig" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <!--<div id="MainFrame" class="MainFrame">-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />

                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />

                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                        <asp:Button ID="btnDelete" CssClass="cls_button" runat="server" Text="Delete" OnClientClick="if (!checkDelete()){return false; }" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <wucdgpaging:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                            Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="team_code, salesrep_code, cat_code, cust_code, cust_class, region, distchannel, cgcode, cgcode1, cgcode2, cgcode3, cgcode4, cgcode5">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>

                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <wuclblInfo:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%">
                                                                            <tr>
                                                                                <td style="width: 17%"></td>
                                                                                <td style="width: 3%"></td>
                                                                                <td style="width: 80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Team Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlTeamCode" runat="server" CssClass="cls_dropdownlist" Width="180px" AutoPostBack="true" OnSelectedIndexChanged="ddlTeamCode_SelectedIndexChanged" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvTeamCode" runat="server" ControlToValidate="ddlTeamCode"
                                                                                    ErrorMessage="Team Code is Required." ValidationGroup="Save" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Salesrep Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlSalesrepCode" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Category Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlSFMSCatCode" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvSFMSCatCode" runat="server" ControlToValidate="ddlSFMSCatCode"
                                                                                    ErrorMessage="Cat Code is Required." ValidationGroup="Save" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox" Enabled="True" Width="180px"></asp:TextBox>
                                                                                    <asp:Button ID="btnSearchCust" runat="server" CssClass="cls_button" Text="Search" OnClick="btnSearchCust_Click" />
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Cust Class</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCustClass" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Region</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlRegion" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Distchannel</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlDistchannel" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">CG Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCGCode" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">CG Code 1</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCGCode1" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">CG Code 2</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCGCode2" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">CG Code 3</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCGCode3" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">CG Code 4</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCGCode4" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">CG Code 5</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlCGCode5" runat="server" CssClass="cls_dropdownlist" Width="180px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>

                                            <span style="float: left;">
                                                <customToolkit:wuc_CustSearch ID="wuc_CustSearch" Title="Customer Search" runat="server" />
                                            </span>
                               
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>

                            <tr class="Bckgroundreport">
                                <td style="height: 5px"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
