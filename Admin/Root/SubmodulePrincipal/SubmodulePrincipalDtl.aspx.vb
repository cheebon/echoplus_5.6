﻿Option Explicit On

Imports System.Data

Partial Class Admin_Root_SubmodulePrincipal_SubmodulePrincipalDtl
    Inherits System.Web.UI.Page

#Region "Properties"
    Private Enum MODE
        Normal
        Edit
        Create
    End Enum
    Private Property PrincipalID() As String
        Get
            If ViewState("PRINCIPAL_ID") Is Nothing Then
                Return -1
            Else
                Return ViewState("PRINCIPAL_ID")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("PRINCIPAL_ID") = value
        End Set
    End Property
    Public Property PrincipalCode() As String
        Get
            Return ViewState("PRINCIPAL_CODE")
        End Get
        Set(ByVal value As String)
            ViewState("PRINCIPAL_CODE") = value
        End Set
    End Property

    Private Property ActionMode() As MODE
        Get
            Return IIf(ViewState("ActionMode") Is Nothing, MODE.Normal, ViewState("ActionMode"))
        End Get
        Set(ByVal value As MODE)
            ViewState("ActionMode") = value
        End Set
    End Property

    Public Property SelectedModule() As String
        Get
            Return ViewState("SelectedModule")
        End Get
        Set(ByVal value As String)
            ViewState("SelectedModule") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Submodule Principal " & IIf(PrincipalID <> "", "- " & PrincipalID & " " & PrincipalCode, "")

            If Not IsPostBack Then
                Dim strPrincipalId, strPrincipalCode As String
                strPrincipalId = Trim(Request.QueryString("PRINCIPAL_ID"))
                strPrincipalCode = Trim(Request.QueryString("PRINCIPAL_CODE"))
                If IsNothing(strPrincipalId) Or strPrincipalId = "" Then
                    PrincipalID = 0
                    PrincipalCode = ""
                    'btnBack.Visible = False
                Else
                    PrincipalID = strPrincipalId
                    PrincipalCode = strPrincipalCode
                End If

                'LoadAllPrincipalDDL()
                LoadModule()
                LoadCountryDDL()

                Dim strAction As String = Trim(Request.QueryString("action"))
                Select Case strAction.ToUpper
                    Case "CREATE"
                        ChangeMode(MODE.Create)
                    Case "EDIT"
                        ChangeMode(MODE.Edit)
                    Case Else 'VIEW
                        ChangeMode(MODE.Normal)
                End Select

                wuc_lblHeader.Title = "Submodule Principal " & IIf(PrincipalID <> "", "- " & PrincipalID & " " & PrincipalCode, "")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Databinding"
    Public Sub RenewDataBind(Optional ByVal IsRef As Boolean = False)
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind(IsRef)
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind(Optional ByVal IsRef As Boolean = False)
        RefreshDatabinding(IsRef)

    End Sub

    Private Sub RefreshDatabinding(Optional ByVal IsRef As Boolean = False)
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList(IsRef)

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dgList.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList(Optional ByVal IsRef As Boolean = False) As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsSubmodulePrincipal
        Dim mode As String
        Try
            If SelectedModule Is Nothing Or SelectedModule = "" Then
            Else
                If ActionMode = Admin_Root_SubmodulePrincipal_SubmodulePrincipalDtl.MODE.Create Or ActionMode = Admin_Root_SubmodulePrincipal_SubmodulePrincipalDtl.MODE.Edit Then
                    mode = 1
                Else
                    mode = 0
                End If


                If Not IsRef Then
                    DT = obj.SubModulePrincipalDtl(PrincipalID, SelectedModule, Session("UserID"), ActionMode)
                Else
                    DT = obj.SubModulePrincipalDtl(ddlPrincipal.SelectedValue, SelectedModule, Session("UserID"), ActionMode)
                End If
            End If

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "Function"
    Private Sub ChangeMode(ByVal actMode As MODE)
        Select Case actMode
            Case MODE.Create
                ActionMode = MODE.Create
            Case MODE.Edit
                ActionMode = MODE.Edit
                'btnSave.Visible = True
                pnlRef.Visible = True
            Case Else 'Normal
                ActionMode = MODE.Normal
                btnSave.Visible = False
                pnlRef.Visible = False
        End Select

        'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit) Then
        '    btnEditDtl.Visible = False
        '    btnCancelDtl.Visible = False
        'End If

        RenewDataBind()
        UpdateDatagrid.Update()
    End Sub
    Private Sub LoadModule()
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                dt = objAccessRightQuery.GetModuleList
            End With

            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            With dgModuleList
                .DataSource = dt
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub
    Private Sub LoadCountryDDL()
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            obj = New adm_SuperAdmin.clsSubmodulePrincipal

            dt = obj.LoadCountryList(Session("UserID"))

            With ddlCountry
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "COUNTRY_NAME"
                .DataValueField = "COUNTRY_ID"
                .DataBind()
                .Items.Insert(0, "-- Select --")
                .SelectedIndex = 0
            End With
            With ddlPrincipal
                .Items.Clear()
                .Items.Add(New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    'Private Sub LoadAllPrincipalDDL()
    '    Dim dt As DataTable
    '    Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
    '    Try
    '        obj = New adm_SuperAdmin.clsSubmodulePrincipal

    '        dt = obj.LoadPrincipalListAll(Session("UserID"))

    '        With ddlSelectPrincipal
    '            .Items.Clear()
    '            .DataSource = dt
    '            .DataTextField = "PRINCIPAL_NAME"
    '            .DataValueField = "PRINCIPAL_ID"
    '            .DataBind()
    '            .Items.Insert(0, "-- Select --")
    '            .SelectedIndex = 0
    '        End With
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
    Private Function GetSelectedSubmodule() As String
        Dim row As GridViewRow
        Dim strSubmodulelist As String = ""
        Dim chk As CheckBox
        Try
            For i As Integer = 0 To dgList.Rows.Count - 1
                row = dgList.Rows(i)
                chk = CType(row.FindControl("chkSelect"), CheckBox)

                If chk.Checked Then
                    strSubmodulelist += IIf(strSubmodulelist = "", "", ",") & "'" & dgList.DataKeys(i).Value & "'"
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
        Return strSubmodulelist
    End Function
    Private Sub Saved(ByVal bool As Boolean)
        If bool Then
            dgList.Visible = False
            lblSaveMsg.Visible = True
            btnSave.Visible = False
        Else
            dgList.Visible = True
            lblSaveMsg.Visible = False
            If Not ActionMode = MODE.Normal Then
                btnSave.Visible = True
            Else
                btnSave.Visible = False
            End If

        End If
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            If ddlCountry.SelectedIndex = 0 Then
                ddlPrincipal.Items.Clear()
                ddlPrincipal.Items.Add("-- Select --")
            Else
                obj = New adm_SuperAdmin.clsSubmodulePrincipal
                dt = obj.LoadPrincipalList(ddlCountry.SelectedValue, Session("UserID"))

                With ddlPrincipal
                    .Items.Clear()
                    .DataSource = dt
                    .DataValueField = "PRINCIPAL_ID"
                    .DataTextField = "PRINCIPAL_NAME"
                    .DataBind()
                    .Items.Insert(0, "-- Select --")
                    .SelectedIndex = 0
                End With
               
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRef.Click
        Try
            If ddlPrincipal.SelectedIndex > 0 Then
                RenewDataBind(True)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgModuleList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgModuleList.RowCommand
        Try
            Select Case e.CommandName
                Case "Select"
                    SelectedModule = Trim(dgModuleList.DataKeys(Convert.ToInt32(e.CommandArgument)).Value)

                    RenewDataBind()
                    Saved(False)
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Dim strSubmodulelist As String
        Dim strPrincipalID As String
        Try
            strSubmodulelist = GetSelectedSubmodule()

            obj = New adm_SuperAdmin.clsSubmodulePrincipal
            obj.Save(PrincipalID, SelectedModule, strSubmodulelist, Session("UserID"))

            Saved(True)
            ChangeMode(MODE.Edit)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.Header Then
                'If CType(e.Row.FindControl("SelectAllCheckBox"), CheckBox).Checked Then
                CType(e.Row.FindControl("SelectAllCheckBox"), CheckBox).Attributes("onclick") = "ChangeAllCheckBoxState(this.checked, '" & dgList.ClientID.ToString & "');"
                'Else
                ' CType(e.Row.FindControl("SelectAllCheckBox"), CheckBox).Attributes("onclick") = "ChangeAllCheckBoxState(false, " & dgList.ClientID.ToString & ");"
                'End If

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class
