﻿Option Explicit On

Imports System.Data

Partial Class iFFMA_SAP_New_SAPDtl
    Inherits System.Web.UI.Page

#Region "Properties/Variables"
    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection

    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SalesList")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SalesList") = value
        End Set
    End Property

    Public Property TxnNo() As String
        Get
            If IsNothing(ViewState("txnno")) Then
                Return String.Empty
            Else
                Return ViewState("txnno").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("txnno") = value
        End Set
    End Property

    Public Property DocType() As String
        Get
            If IsNothing(ViewState("doctype")) Then
                Return String.Empty
            Else
                Return ViewState("doctype").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("doctype") = value
        End Set
    End Property

    Private dtAR As DataTable
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "SAP Details" '"SAP Details"
                .DataBind()
                .Visible = True
            End With

            If Not Page.IsPostBack Then
                AcceptIncomingRequest()
                SetHeader()
                SetDetail()
                ToggleActions()
                'ApplyAccessibility()
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#Region "Function"
    Public Sub Refresh()
        SetHeader()
        SetDetail()
        ToggleActions()
    End Sub
    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function
    Private Sub ApplyAccessibility()
        Try
            If Not GetAccessRight(1, 227, 3) Then 'Edit
                btnUnBlock.Visible = False
                btnCancel.Visible = False
            End If
            If Not GetAccessRight(1, 227, 2) Then 'Add
                btnResubmit.Visible = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ToggleActions()
        If lblStatusValue.Text.Trim = "B" Then
            If GetAccessRight(1, 227, 3) Then
                btnUnBlock.Visible = True
                btnCancel.Visible = True
            End If
        Else
            btnUnBlock.Visible = False
            btnCancel.Visible = False
        End If
        If lblStatusValue.Text.Trim = "S" Or lblStatusValue.Text.Trim = "R" Or lblStatusValue.Text.Trim = "E" Or lblStatusValue.Text.Trim = "W" Then
            If GetAccessRight(1, 227, 2) Then
                btnResubmit.Visible = True
            End If
        Else
            btnResubmit.Visible = False
        End If

    End Sub
    Private Sub AcceptIncomingRequest()
        DocType = Trim(Request.QueryString("doc_type"))
        'SearchType = Trim(Request.QueryString("search_type"))
        'SearchValue = Trim(Request.QueryString("search_value"))
        'SearchValue1 = Trim(Request.QueryString("search_value_1"))
        TxnNo = Trim(Request.QueryString("txn_no"))
        'Team = Trim(Request.QueryString("team"))
    End Sub
    Private Function ReadValue(ByVal obj As Object) As String
        If IsDBNull(obj) Then
            Return String.Empty
        ElseIf IsNothing(obj) Then
            Return String.Empty
        Else
            Return obj.ToString.Trim
        End If
    End Function
    Private Sub SetHeader()
        Dim dt As DataTable

        Try
            dt = RetrieveHeader()

            If dt.Rows.Count > 0 Then
                lblStatusValue.Text = ReadValue(dt.Rows(0)("STATUS"))
                lblTxnStatusValue.Text = ReadValue(dt.Rows(0)("txn_status"))
                If DocType = "TRA" Then
                    lblLNStatusValue.Text = ReadValue(dt.Rows(0)("ln_status"))
                    lblLNRemarksValue.Text = ReadValue(dt.Rows(0)("ln_reason"))
                    pnlLN.Visible = True
                Else
                    pnlLN.Visible = False
                End If

                lblTxnNoValue.Text = ReadValue(dt.Rows(0)("txn_no"))
                lblDocNoValue.Text = ReadValue(dt.Rows(0)("DOC_NO"))
                lblSRCodeValue.Text = ReadValue(dt.Rows(0)("salesrep_code"))
                lblLastUpdateValue.Text = ReadValue(dt.Rows(0)("LASTUPDATE"))
                lblTxnDateValue.Text = ReadValue(dt.Rows(0)("txn_timestamp"))
                lblOrderTypeValue.Text = ReadValue(dt.Rows(0)("AUART"))
                lblSalesOrgValue.Text = ReadValue(dt.Rows(0)("VKORG"))
                lblDistChannelValue.Text = ReadValue(dt.Rows(0)("VTWEG"))
                lblDivCodeValue.Text = ReadValue(dt.Rows(0)("SPART"))
                lblSalesOffValue.Text = ReadValue(dt.Rows(0)("VKBUR"))
                lblSalesGrpValue.Text = ReadValue(dt.Rows(0)("VKGRP"))
                lblPlantValue.Text = ReadValue(dt.Rows(0)("WERKS"))
                lblSlocValue.Text = ReadValue(dt.Rows(0)("LGORT"))
                lblShipPointValue.Text = ReadValue(dt.Rows(0)("VSTEL"))
                lblSoldToValue.Text = ReadValue(dt.Rows(0)("KUNNR"))
                lblShipToValue.Text = ReadValue(dt.Rows(0)("KUNDE"))
                lblPayerCodeValue.Text = ReadValue(dt.Rows(0)("PAYER"))
                lblPONoValue.Text = ReadValue(dt.Rows(0)("BSTNK"))
                lblPODateValue.Text = ReadValue(dt.Rows(0)("BSTDK"))
                lblPOTypeValue.Text = ReadValue(dt.Rows(0)("BSTRK"))
                lblPOSuppValue.Text = ReadValue(dt.Rows(0)("BSTZD"))
                lblBNameValue.Text = ReadValue(dt.Rows(0)("BNAME"))
                lblIntRefValue.Text = ReadValue(dt.Rows(0)("IHREZ"))
                lblHdrShipText1Value.Text = ReadValue(dt.Rows(0)("H_SHIP_TEXT_1"))
                lblHdrShipText2Value.Text = ReadValue(dt.Rows(0)("H_SHIP_TEXT_2"))
                lblHdrShipText3Value.Text = ReadValue(dt.Rows(0)("H_SHIP_TEXT_3"))
                lblHdrDiscType1Value.Text = ReadValue(dt.Rows(0)("H_DISC_TYP_1"))
                lblHdrDiscVal1Value.Text = ReadValue(dt.Rows(0)("H_DISC_VAL_1"))
                lblHdrDiscType2Value.Text = ReadValue(dt.Rows(0)("H_DISC_TYP_2"))
                lblHdrDiscVal2Value.Text = ReadValue(dt.Rows(0)("H_DISC_VAL_2"))
                lblDelDateValue.Text = ReadValue(dt.Rows(0)("KETDAT"))
                lblUsageIndValue.Text = ReadValue(dt.Rows(0)("VKAUS"))
                lblResendCntValue.Text = ReadValue(dt.Rows(0)("RESENDCOUNTER"))
                lblOrderByValue.Text = ReadValue(dt.Rows(0)("ORDER_BY"))
                lblRemarksValue.Text = ReadValue(dt.Rows(0)("REMARKS"))
                lblNoteValue.Text = ReadValue(dt.Rows(0)("NOTE"))

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function RetrieveHeader() As DataTable
        Dim obj As New mst_SAP.clsSAPQuery
        Dim dt As DataTable

        Try
            With obj
                .clsProperties.doc_type = DocType
                .clsProperties.txn_no = TxnNo
                dt = .GetSAPDtl()
                'dtDtl = .GetSAPDtlList()
            End With
            Return dt
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Function
    Private Sub SetDetail()
        Try
            RenewDataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region
#Region "Databind"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable '= CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()
            If dtCurrenttable Is Nothing Then
                dtCurrenttable = New DataTable
            Else
                If dtCurrenttable.Rows.Count = 0 Then
                    dtCurrenttable.Rows.Add(dtCurrenttable.NewRow())
                    'dgList.Columns(0).Visible = False
                    'dgList.Columns(1).Visible = False
                End If
            End If

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = IIf(dtCurrenttable.Rows.Count > 0, True, False)
            'End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            'UpdatePanel.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New mst_SAP.clsSAPQuery
        Try
            With obj
                .clsProperties.doc_type = DocType
                .clsProperties.txn_no = TxnNo
                DT = .SAPGetDtlList()
            End With

            'DT = obj.SubmoduleList(txtModuleCode.Text.Trim, txtSubmoduleCode.Text.Trim, txtSubmoduleName.Text.Trim)


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
    End Sub
    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()
        'ClearDGListColumns()


        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_SAPDetails.GetFieldColumnType(ColumnName)
                
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_SAPDetails.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_SalesList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SAPDetails.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SAPDetails.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SAPDetails.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression Like ("* DESC") Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnResubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResubmit.Click
        Dim obj As mst_SAP.clsSAP
        Try
            obj = New mst_SAP.clsSAP

            With obj
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.txn_no = TxnNo
                .clsProperties.doc_type = DocType

                .SAPResubmit()
            End With
            Refresh()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btnUnBlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnBlock.Click
        Dim obj As mst_SAP.clsSAP
        Try
            obj = New mst_SAP.clsSAP

            With obj
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.txn_no = TxnNo
                .clsProperties.doc_type = DocType

                .SAPUnblock()
            End With
            Refresh()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim obj As mst_SAP.clsSAP
        Try
            obj = New mst_SAP.clsSAP

            With obj
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.txn_no = TxnNo
                .clsProperties.doc_type = DocType

                .SAPCancel()
            End With
            Refresh()
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class
Public Class CF_SAPDetails
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'SALESREP_CODE ,SALESREP_NAME ,
        'CHAIN_CODE ,CHAIN_NAME ,CHANNEL_CODE ,CHANNEL_NAME ,
        'CUST_CODE ,CUST_NAME ,CUST_GRP_CODE ,CUST_GRP_NAME ,
        'PRD_CODE ,PRD_NAME ,PRD_GRP_CODE ,PRD_GRP_NAME ,
        'SALES_AREA_CODE ,SALES_AREA_NAME ,
        'SHIPTO_CODE ,SHIPTO_NAME

        'TEAM_CODE ,REGION_CODE ,REGION_NAME 
        'MTD_SALES ,MTD_FOC_QTY ,MTD_SALES_QTY ,MTD_TGT ,MTDVAR, 
        'QTD_SALES ,QTD_FOC_QTY ,QTD_SALES_QTY ,QTD_TGT ,QTDVAR ,
        'YTD_SALES ,YTD_FOC_QTY ,YTD_SALES_QTY ,YTD_TGT ,YTDVAR ,
        'PYTD_SALES,PYTD_FOC_QTY ,PYTD_SALES_QTY ,YTDGROSS

        Select Case ColumnName.ToUpper
            Case "LINE_NO"
                strFieldName = "Line No"
            Case "PRODUCT_CODE"
                strFieldName = "Product Code"
            Case "QTY"
                strFieldName = "Qty"
            Case "UOM_CODE"
                strFieldName = "UOM Code"
            Case "ITEM_CAT"
                strFieldName = "Item Category"
            Case "MAN_PRICE_UOM"
                strFieldName = "Manual Price Per UOM"
            Case "PAY_TERM_NAME"
                strFieldName = "Payment Term"
            Case "L_DISC_TYP_1"
                strFieldName = "Line Disc. Type 1"
            Case "L_DISC_VAL_1"
                strFieldName = "Line Disc. Value 1"
            Case "L_DISC_TYP_2"
                strFieldName = "Line Disc. Type 2"
            Case "L_DISC_VAL_2"
                strFieldName = "Line Disc. Value 2"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        
        FCT = FieldColumntype.ButtonColumn

        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
                 "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strStringFormat = "{0:#,0.0}"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
                 "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strStringFormat = "{0:#,0}"
            Case "SHIPTO_DATE", "TXN_DATE_IN", "TXN_DATE_OUT", "PAY_DATE", "TXN_TIMESTAMP"
                strStringFormat = "{0:yyyy-MM-dd}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class