Imports System.Data

Partial Class iFFMA_Salesteam_SalesteamAssignListV2
    Inherits System.Web.UI.UserControl

    Public Event AddButton_Click As EventHandler
    Public Event DeleteButton_Click As EventHandler
    Private dtAR As DataTable
#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum assignTypeName As Long
        FieldForce = 0
        Supplier = 1
        PrdGrp = 2
        Prd = 3
        PrdV2 = 4
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

        'Call Paging()
        'With wuc_dgAssignPaging
        '    '.PageCount = dgAssignList.PageCount
        '    '.CurrentPageIndex = dgAssignList.PageIndex
        '    .DataBind()
        '    .Visible = True
        'End With

        If AssignType = assignTypeName.FieldForce Then
            lblTitle.Text = "Field Force List"
            lblTotalHdr.Text = "Total"
            pnlCtrlAction.Visible = False
        ElseIf AssignType = assignTypeName.Supplier Then
            lblTitle.Text = "Supplier List"
            lblTotalHdr.Text = "Total"
            If Not IsPostBack Then
                LoadCrtlSearch()
            End If
        ElseIf AssignType = assignTypeName.PrdGrp Then
            lblTitle.Text = "Product Group List"
            lblTotalHdr.Text = "Total"
        ElseIf AssignType = assignTypeName.PrdV2 Then
            lblTitle.Text = "Product Exception List"
            lblTotalHdr.Text = "Total Excluded"
            If Not IsPostBack Then
                LoadCrtlSearch()
            End If
        End If

        'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If
        '----------------------------------------------------------------------------------------------------
    End Sub

    Private Sub LoadCrtlSearch()
        If AssignType = assignTypeName.Supplier Then
            ddlSearchType.Items.Clear()
            ddlSearchType.Items.Insert(0, New ListItem("All", "ALL"))
            ddlSearchType.Items.Insert(1, New ListItem("Supplier Name", "SUPP_NAME"))
            ddlSearchType.Items.Insert(2, New ListItem("Supplier Code", "SUPP_CODE"))
            pnlCtrlSearch.Visible = True
        ElseIf AssignType = assignTypeName.PrdV2 Then
            ddlSearchType.Items.Clear()
            ddlSearchType.Items.Insert(0, New ListItem("All", "ALL"))
            ddlSearchType.Items.Insert(1, New ListItem("Product Name", "PRD_NAME"))
            ddlSearchType.Items.Insert(2, New ListItem("Product Code", "PRD_CODE"))
            pnlCtrlSearch.Visible = True
        End If

        SearchValueRow.Visible = False
        UpdatepnlCtrlSearch.Update()

    End Sub

    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        If ddlSearchType.SelectedValue.ToUpper = "ALL" Then
            SearchValueRow.Visible = False
            RefreshDatabinding()
        Else
            SearchValueRow.Visible = True
        End If
        UpdatepnlCtrlSearch.Update()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        RefreshDatabinding()
        UpdatepnlCtrlSearch.Update()

    End Sub

#Region "PROPERTY"
    Public Property Total() As String
        Get
            Return Trim(lblTotal.Text)
        End Get
        Set(ByVal value As String)
            lblTotal.Text = value
        End Set
    End Property

    Public Property AssignType() As Long
        Get
            Return Trim(hdAssignType.Value)
        End Get
        Set(ByVal value As Long)
            hdAssignType.Value = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdCode.Value)
        End Get
        Set(ByVal value As String)
            hdCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgAssignList.PageIndex = 0
        wuc_dgAssignPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Dim strSearhType As String = ddlSearchType.SelectedValue
        Dim strSearchValue As String = txtSearchValue.Text

        Try
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam

            If AssignType = assignTypeName.FieldForce Then
                DT = clsSalesteam.GetSalesteamSalesrepList(TeamCode)
                dgAssignList.DataKeyNames = New String() {"SALESREP_CODE"}
            ElseIf AssignType = assignTypeName.Supplier Then
                DT = clsSalesteam.GetSalesteamSupplierList(TeamCode, strSearhType, strSearchValue)
                dgAssignList.DataKeyNames = New String() {"SUPPLIER_CODE"}
            ElseIf AssignType = assignTypeName.PrdGrp Then
                DT = clsSalesteam.GetSalesteamPrdGrpList(TeamCode)
                dgAssignList.DataKeyNames = New String() {"SUPPLIER_CODE", "PRD_GRP_CODE"}
            ElseIf AssignType = assignTypeName.PrdV2 And GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM_SUB, "'1'") Then
                DT = clsSalesteam.GetSalesteamPrdExcludeListV2(TeamCode, strSearhType, strSearchValue)
                dgAssignList.DataKeyNames = New String() {"SUPPLIER_CODE", "PRD_GRP_CODE", "PRD_CODE", "REGION_CODE"}
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

#End Region

#Region "dgAssignList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgAssignList.PageIndex = 0
            '    wuc_dgAssignPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgAssignList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            Total = Master_Row_Count 'dtCurrentTable.Rows.Count

            'Call Paging
            With wuc_dgAssignPaging
                .PageCount = dgAssignList.PageCount
                .CurrentPageIndex = dgAssignList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlSalesteamAssign.Update()
        End Try
    End Sub


    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgAssignList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgAssignList.Columns.Clear()

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 Then

                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = "Delete"

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgAssignList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_DELETE")


            End If


            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SalesteamAssignListV2.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SalesteamAssignListV2.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SalesteamAssignListV2.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_SalesteamAssignListV2.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_SalesteamAssignListV2.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgAssignList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgAssignList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgAssignList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgAssignList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing

                        If btnDelete IsNot Nothing Then
                            If AssignType = assignTypeName.Supplier Then
                                strDeleteMsg = "The Product Group(s) associated with it will be deleted as well. \nAre you sure want to delete?"
                            ElseIf AssignType = assignTypeName.PrdGrp Then
                                strDeleteMsg = "The Product(s) associated with it will be deleted as well. \nAre you sure want to delete?"
                            ElseIf AssignType = assignTypeName.PrdV2 Then
                                strDeleteMsg = "The selected product will be deleted from the exception list. \nAre you sure want to delete?"
                            ElseIf AssignType = assignTypeName.FieldForce Then
                                strDeleteMsg = "This is a very serious action. The selected field force will be permanently deleted from the system.\nAre you sure want to delete?"
                            End If
                            'btnDelete.OnClientClick = "if (confirm('" + strDeleteMsg + "') == false) { window.event.returnValue = false; return false; }"
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgAssignList.RowDeleting
        Try
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            Dim strSupplierCode, strPrdGrpCode, strPrdCode, strSalesrepCode, strRegionCode As String

            strSupplierCode = sender.datakeys(e.RowIndex).item("SUPPLIER_CODE")
            strPrdGrpCode = IIf(sender.datakeys(e.RowIndex).item("PRD_GRP_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("PRD_GRP_CODE"))
            strPrdCode = IIf(sender.datakeys(e.RowIndex).item("PRD_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("PRD_CODE"))
            strSalesrepCode = IIf(sender.datakeys(e.RowIndex).item("SALESREP_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("SALESREP_CODE"))
            strRegionCode = IIf(sender.datakeys(e.RowIndex).item("REGION_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("REGION_CODE"))

            If AssignType = assignTypeName.Supplier Then
                clsSalesteam.DeleteSalesteamSupplier(strSupplierCode, TeamCode)
            ElseIf AssignType = assignTypeName.PrdGrp Then
                clsSalesteam.DeleteSalesteamPrdGrp(strSupplierCode, strPrdGrpCode, TeamCode)
            ElseIf AssignType = assignTypeName.PrdV2 Then
                clsSalesteam.DeleteSalesteamPrdExcludeV2(strSupplierCode, strPrdGrpCode, strPrdCode, TeamCode, strRegionCode)
            ElseIf AssignType = assignTypeName.FieldForce Then
                clsSalesteam.DeleteSalesteamSalesrep(TeamCode, strSalesrepCode)
            End If

            RenewDataBind()

            RaiseEvent DeleteButton_Click(sender, e)
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgAssignList() As GridView
        Dim dgAssignListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgAssignList.AllowSorting
            Dim blnAllowPaging As Boolean = dgAssignList.AllowPaging

            dgAssignList.AllowSorting = False
            dgAssignList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgAssignListToExport = dgAssignList

            dgAssignList.AllowPaging = blnAllowPaging
            dgAssignList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgAssignListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Go_Click
        Try
            dgAssignList.PageIndex = CInt(wuc_dgAssignPaging.PageNo - 1)

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Previous_Click
        Try
            If dgAssignList.PageIndex > 0 Then
                dgAssignList.PageIndex = dgAssignList.PageIndex - 1
            End If
            wuc_dgAssignPaging.PageNo = dgAssignList.PageIndex + 1

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Next_Click
        Try
            If dgAssignList.PageCount - 1 > dgAssignList.PageIndex Then
                dgAssignList.PageIndex = dgAssignList.PageIndex + 1
            End If
            wuc_dgAssignPaging.PageNo = dgAssignList.PageIndex + 1

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        RaiseEvent AddButton_Click(sender, e)
    End Sub

    'Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    Try
    '        Dim strCodeList As String
    '        strCodeList = Trim(GetSelectedString())

    '        Dim clsSalesteam As New mst_Salesteam.clsSalesteam

    '        If AssignType = assignTypeName.Principal Then
    '            clsSalesteam.DeleteSalesteamAgency(strCodeList, TeamCode)
    '        ElseIf AssignType = assignTypeName.PrdGrp Then
    '            clsSalesteam.DeleteSalesteamPrdGrp(strCodeList, TeamCode)
    '        ElseIf AssignType = assignTypeName.Prd Then
    '            clsSalesteam.DeleteSalesteamPrd(strCodeList, TeamCode)
    '        End If

    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgAssignList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgAssignList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgAssignList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function



End Class

Public Class CF_SalesteamAssignListV2
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "WHS_DESC"
                strFieldName = "Warehouse"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class