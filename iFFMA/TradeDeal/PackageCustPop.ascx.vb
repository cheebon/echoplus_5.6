﻿Imports System.Data

Partial Class iFFMA_TradeDeal_PackageCustPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property PackageCode() As String
        Get
            Return Trim(hdPackageCode.Value)
        End Get
        Set(ByVal value As String)
            hdPackageCode.Value = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlMustInd As DropDownList = CType(DetailsView1.FindControl("ddlMustInd"), DropDownList)
            If ddlMustInd IsNot Nothing Then
                ddlMustInd.SelectedValue = IIf(IsDBNull(DT.Rows(0)("MUST_IND")), "", DT.Rows(0)("MUST_IND"))
            End If

            Dim lblMustInd As Label = CType(DetailsView1.FindControl("lblMustInd"), Label)
            If lblMustInd IsNot Nothing Then
                lblMustInd.Text = IIf(DT.Rows(0)("MUST_IND") = "Y", "Yes", "No")
            End If

            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            If ddlUOM IsNot Nothing Then
                dtUOM = clsCommon.GetUOMDDL
                With ddlUOM
                    .Items.Clear()
                    .DataSource = dtUOM.DefaultView
                    .DataTextField = "UOM_NAME"
                    .DataValueField = "UOM_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("UOM_CODE")), "", DT.Rows(0)("UOM_CODE"))
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ResetPage()
        'SubCatID = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    'Public Sub LoadDvEditMode()
    '    pnlViewMode.Visible = False
    '    pnlEditMode.Visible = True

    '    DetailsView1.ChangeMode(DetailsViewMode.Edit)
    'End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsPackage As New mst_TradeDeal.clsPackage

            DT = clsPackage.GetPackageCustDetails(PackageCode, CustCode)
            If DT.Rows.Count > 0 Then

            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PACKAGE, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_CustSearch)
                .ResetPage()
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SelectButton_Click
        Try
            Dim txtCustCode As TextBox = CType(DetailsView1.FindControl("txtCustCode"), TextBox)
            txtCustCode.Text = wuc_CustSearch.CustCode

            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtCustCode As TextBox = CType(DetailsView1.FindControl("txtCustCode"), TextBox)
            Dim lblCustCode As Label = CType(DetailsView1.FindControl("lblCustCode"), Label)

            Dim clsPackage As New mst_TradeDeal.clsPackage

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                'clsPackage.UpdatePackageCust(PackageCode, Trim(lblCustCode.Text))
                'lblInfo.Text = "The record is successfully saved."
                'LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim isDuplicate As Integer = 0

                DT = clsPackage.CreatePackageCust(PackageCode, Trim(txtCustCode.Text))
                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Customer Code already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    CustCode = Trim(txtCustCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view

            ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        ResetPage()
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class