Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports rpt_Customize
Partial Class iFFMR_Customize_TerritoryMthly_TerritoryMthlySearch
    Inherits System.Web.UI.Page
    Public ReadOnly Property ClassName() As String
        Get
            Return "TerritoryMthlySearch"
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.MTHTERRITORY)
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            LoadLsbTeam()
            LoadDDLYear()
            LoadDDLMonth()
            Loadddlptlcode()
            'ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
        End If
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        LoadLsbTeam()
        LoadDDLYear()
        LoadDDLMonth()
        Loadddlptlcode()
        LoadLsbSalesrep()
    End Sub

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            Loadddlptlcode()
            LoadLsbSalesrep()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            Loadddlptlcode()
            LoadLsbSalesrep()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            Loadddlptlcode()
            LoadLsbSalesrep()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            Loadddlptlcode()
            LoadLsbSalesrep()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            If lsbSelectedSalesrep.Items.Count > 1 Then
                lsbSelectedSalesrep.Items(0).Selected = True
                AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
    '    Try
    '        Dim liToShow As ListItem
    '        For Each liToShow In lsbSalesrep.Items
    '            liToShow.Selected = True
    '        Next
    '        AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
    '    Try
    '        Dim liToShow As ListItem
    '        For Each liToShow In lsbSelectedSalesrep.Items
    '            liToShow.Selected = True
    '        Next
    '        AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub
#End Region

#Region "YEAR"
    Private Sub LoadDDLYear()
        Dim strcurrentyear As Integer, strpreviousyear As Integer
        strcurrentyear = Now.Date.Year
        strpreviousyear = Now.Date.Year - 1

        With ddlyear
            .Items.Insert(0, New ListItem(strcurrentyear, strcurrentyear))
            .Items.Insert(1, New ListItem(strpreviousyear, strpreviousyear))
            .SelectedIndex = 0
        End With
    End Sub

#End Region

#Region "MONTH"
    Private Sub LoadDDLMonth()
        Dim strcurrentmonth As Integer
        strcurrentmonth = Now.Date.Month - 1
        With ddlmonth
            .SelectedIndex = strcurrentmonth
        End With
    End Sub


#End Region

#Region "PTL_CODE"
    Private Sub Loadddlptlcode()

        Dim clsCatName As New rpt_Customize.clsSFActy

        Dim strUserID As String = Session.Item("UserID")
        Dim strPrincipalID As String = Session("PRINCIPAL_ID")

        Dim strTeamList As String
        strTeamList = GetItemsInString(lsbSelectedTeam)

        With ddlptlcode
            .Items.Clear()
            .DataSource = clsCatName.GetCatName(strUserID, strPrincipalID, strTeamList)
            .DataTextField = "CAT_NAME"
            .DataValueField = "CAT_NAME"
            .DataBind()
            .Items.Insert(0, New ListItem("Unspecified", ""))
            .SelectedIndex = 0
        End With
    End Sub


#End Region

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
