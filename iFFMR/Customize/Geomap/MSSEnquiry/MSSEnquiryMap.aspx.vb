﻿Imports System.Data
Imports rpt_Customize

''' <summary>
''' Name: CHEONG BOO LIM
''' Date: 30 Sept 2010
''' Description: 
''' </summary>
''' <remarks></remarks>
Partial Class iFFMR_Customize_MSSEnquiry_MSSEnquiryMap
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Public ReadOnly Property Root() As String
        Get
            Dim strCompletePath As String = Me.Server.MapPath(Me.Request.ApplicationPath).Replace("/", "\\")
            Dim strApplicationPath As String = Me.Request.ApplicationPath.Replace("/", "\\")
            Dim strRootPath As String = strCompletePath.Replace(strApplicationPath, String.Empty)

            Return strRootPath
        End Get
    End Property

    Public Property CustName() As String
        Get
            Return ViewState("strCustName")
        End Get
        Set(ByVal value As String)
            ViewState("strCustName") = value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return ViewState("strAddress")
        End Get
        Set(ByVal value As String)
            ViewState("strAddress") = value
        End Set
    End Property
    Public Property District() As String
        Get
            Return ViewState("strDistrict")
        End Get
        Set(ByVal value As String)
            ViewState("strDistrict") = value
        End Set
    End Property
    Public Property CustGrpName() As String
        Get
            Return ViewState("strCustGrpName")
        End Get
        Set(ByVal value As String)
            ViewState("strCustGrpName") = value
        End Set
    End Property
    Public Property CustClass() As String
        Get
            Return ViewState("strCustClass")
        End Get
        Set(ByVal value As String)
            ViewState("strCustClass") = value
        End Set
    End Property
    Public Property CustType() As String
        Get
            Return ViewState("strCustType")
        End Get
        Set(ByVal value As String)
            ViewState("strCustType") = value
        End Set
    End Property
    Public Property TitleCode() As String
        Get
            Return ViewState("strTitleCode")
        End Get
        Set(ByVal value As String)
            ViewState("strTitleCode") = value
        End Set
    End Property
    Public Property QuesCode() As String
        Get
            Return ViewState("strQuesCode")
        End Get
        Set(ByVal value As String)
            ViewState("strQuesCode") = value
        End Set
    End Property
    Public Property SubQuesCode() As String
        Get
            Return ViewState("strSubQuesCode")
        End Get
        Set(ByVal value As String)
            ViewState("strSubQuesCode") = value
        End Set
    End Property
    Public Property MssStartDate() As String
        Get
            Return ViewState("strMssStartDate")
        End Get
        Set(ByVal value As String)
            ViewState("strMssStartDate") = value
        End Set
    End Property
    Public Property MssEndDate() As String
        Get
            Return ViewState("strMssEndDate")
        End Get
        Set(ByVal value As String)
            ViewState("strMssEndDate") = value
        End Set
    End Property
    Public Property MtdStart() As String
        Get
            Return ViewState("strMtdStart")
        End Get
        Set(ByVal value As String)
            ViewState("strMtdStart") = value
        End Set
    End Property
    Public Property MtdEnd() As String
        Get
            Return ViewState("strMtdEnd")
        End Get
        Set(ByVal value As String)
            ViewState("strMtdEnd") = value
        End Set
    End Property
    Public Property YtdStart() As String
        Get
            Return ViewState("strYtdStart")
        End Get
        Set(ByVal value As String)
            ViewState("strYtdStart") = value
        End Set
    End Property
    Public Property YtdEnd() As String
        Get
            Return ViewState("strYtdEnd")
        End Get
        Set(ByVal value As String)
            ViewState("strYtdEnd") = value
        End Set
    End Property
    Public Property NoSkuStart() As String
        Get
            Return ViewState("strNoSkuStart")
        End Get
        Set(ByVal value As String)
            ViewState("strNoSkuStart") = value
        End Set
    End Property
    Public Property NoSkuEnd() As String
        Get
            Return ViewState("strNoSkuEnd")
        End Get
        Set(ByVal value As String)
            ViewState("strNoSkuEnd") = value
        End Set
    End Property
    Public Property TeamCode() As String
        Get
            Return ViewState("strTeamCode")
        End Get
        Set(ByVal value As String)
            ViewState("strTeamCode") = value
        End Set
    End Property
    Public Property SalesrepCode() As String
        Get
            Return ViewState("strSalesrepCode")
        End Get
        Set(ByVal value As String)
            ViewState("strSalesrepCode") = value
        End Set
    End Property
    Public Property PrdCode() As String
        Get
            Return ViewState("strPrdCode")
        End Get
        Set(ByVal value As String)
            ViewState("strPrdCode") = value
        End Set
    End Property
    Public Property SubQuesType() As String
        Get
            Return ViewState("strSubQuesType")
        End Get
        Set(ByVal value As String)
            ViewState("strSubQuesType") = value
        End Set
    End Property
    Public Property MssAnswer() As String
        Get
            Return ViewState("strMssAnswer")
        End Get
        Set(ByVal value As String)
            ViewState("strMssAnswer") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "MSSEnquiryMap.aspx"
        End Get
    End Property

    Public Property strGUID() As String
        Get
            Return ViewState("GUID")
        End Get
        Set(ByVal value As String)
            ViewState("GUID") = value
        End Set
    End Property

    'Dim strCustName, strAddress, strDistrict, strCustGrpName, strCustClass, strCustType, _
    'strTitleCode, strQuesCode, strSubQuesCode, strMssStartDate, strMssEndDate, strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
    'strNoSkuStart, strNoSkuEnd, strTeamCode, strSalesrepCode, strPrdCode As String

    Dim strTitleName, strQuesName, strSubQuesName, strPeriod As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            'With wuc_lblHeader
            '    .Title = "Market Survey - Map" '"Call Analysis List By Customer"
            '    .DataBind()
            '    .Visible = True
            'End With           

            If Not Page.IsPostBack Then
                strGUID = Guid.NewGuid.ToString
                CustName = Request.QueryString("CustName").ToString
                Address = Request.QueryString("Address").ToString
                District = Request.QueryString("District").ToString
                CustGrpName = Request.QueryString("CustGrpName").ToString
                CustClass = Request.QueryString("CustClass").ToString
                CustType = Request.QueryString("CustType").ToString
                TitleCode = Request.QueryString("TitleCode").ToString
                QuesCode = Request.QueryString("QuesCode").ToString
                SubQuesCode = Request.QueryString("SubQuesCode").ToString
                MssStartDate = Request.QueryString("StartDate").ToString
                MssEndDate = Request.QueryString("EndDate").ToString
                strTitleName = Request.QueryString("TitleName").ToString
                strQuesName = Request.QueryString("QuesName").ToString
                strSubQuesName = Request.QueryString("SubQuesName").ToString
                MtdStart = Request.QueryString("MtdStart").ToString
                MtdEnd = Request.QueryString("MtdEnd").ToString
                YtdStart = Request.QueryString("YtdStart").ToString
                YtdEnd = Request.QueryString("YtdEnd").ToString
                NoSkuStart = Request.QueryString("NoSkuStart").ToString
                NoSkuEnd = Request.QueryString("NoSkuEnd").ToString
                TeamCode = Request.QueryString("TeamCode").ToString
                SalesrepCode = Request.QueryString("SalesrepCode").ToString
                PrdCode = Request.QueryString("PrdCode").ToString
                SubQuesType = Request.QueryString("SubQuesType").ToString
                MssAnswer = Request.QueryString("Answer").ToString

                lblTitle.Text = strTitleName
                lblPeriod.Text = MssStartDate + " - " + MssEndDate
                lblQuestion.Text = strQuesName
                lblSubQuestion.Text = strSubQuesName

                'RenewDataBind() 'No need to show stats table anymore.

                TimerControl1.Enabled = True

                'AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "NetValue", "NetValue= '" & Session("NetValue") & "';SalesDate='" & DateTime.Now.ToString("yyyy-MM-dd") & "';", True)

                'Set Root path for client side
                Dim strRoot As String = ResolveClientUrl("~")
                strRoot = strRoot.Substring(0, strRoot.Length - 1)
                AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "RootVar", "root= '" & strRoot & "';", True)
            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then CallMapQuery()
        TimerControl1.Enabled = False
    End Sub
    Private Sub CallMapQuery()
        Try
            'Set Guid first
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "SetMapVariable", "Guid = '" & strGUID & "';", True)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "SetMapVariable", String.Format("CustName='{0}';Address='{1}';District='{2}';CustGrpName='{3}';CustClass='{4}';CustType='{5}';", CustName, Address, District, CustGrpName, CustClass, CustType) & _
                                                                                      String.Format("TitleCode='{0}';QuesCode='{1}';SubQuesCode='{2}';MssStartDate='{3}';MssEndDate='{4}';MtdStart='{5}';MtdEnd='{6}';", TitleCode, QuesCode, SubQuesCode, MssStartDate, MssEndDate, MtdStart, MtdEnd) & _
                                                                                      String.Format("YtdStart='{0}';YtdEnd='{1}';NoSkuStart='{2}';NoSkuEnd='{3}';TeamCode='{4}';SalesrepCode='{5}';PrdCode='{6}';NetValue='{7}';SalesDate='{8}';MssAnswer='{9}';SubQuesType='{10}';", _
                                                                                                    YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode, Session("NetValue"), DateTime.Now.ToString("yyyy-MM-dd"), MssAnswer, SubQuesType), True)

            'Call Map Query
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "QueryMap", "SearchCustomerTitle();", True)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#Region "DATA BIND"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0


        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsMssEnqQuery

            DT = clsCustomer.SearchCustTitleStats(strGUID, CustName, Address, District, CustGrpName, CustClass, CustType, TitleCode, QuesCode, SubQuesCode, MssStartDate, MssEndDate, Session("NetValue"), _
                                                  DateTime.Now.ToString("yyyy-MM-dd"), MtdStart, MtdEnd, YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListExport() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsMssEnqQuery

            DT = clsCustomer.SearchCustTitleStats(strGUID, CustName, Address, District, CustGrpName, CustClass, CustType, TitleCode, QuesCode, SubQuesCode, MssStartDate, MssEndDate, Session("NetValue"), _
                                                  DateTime.Now.ToString("yyyy-MM-dd"), MtdStart, MtdEnd, YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try

            dtCurrentTable = GetRecListExport()

            If dtCurrentTable IsNot Nothing Then
                If dtCurrentTable.Rows.Count > 1 Then
                    With dgList
                        .DataSource = dtCurrentTable
                        .DataBind()
                    End With

                    lblStats.Visible = True
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    'Private Sub PreRenderMode(ByRef DT As DataTable)
    '    Try
    '        dgList_Init(DT)
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub dgList_Init(ByRef dtToBind As DataTable)
    '    Dim i As Integer
    '    Dim columnDataType As System.Type = Nothing

    '    Try
    '        aryDataItem.Clear()
    '        dgList.Columns.Clear()


    '        Dim ColumnName As String = ""
    '        For i = 0 To dtToBind.Columns.Count - 1
    '            ColumnName = dtToBind.Columns(i).ColumnName
    '            Select Case CF_CustomerProfile.GetFieldColumnType(ColumnName)
    '                Case FieldColumntype.HyperlinkColumn


    '                Case FieldColumntype.InvisibleColumn

    '                Case FieldColumntype.BoundColumn
    '                    Dim dgColumn As New BoundField 'BoundColumn

    '                    dgColumn.ReadOnly = True

    '                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
    '                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
    '                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

    '                    Dim strFormatString As String = CF_CustomerProfile.GetOutputFormatString(ColumnName)
    '                    If String.IsNullOrEmpty(strFormatString) = False Then
    '                        dgColumn.DataFormatString = strFormatString
    '                        dgColumn.HtmlEncode = False
    '                    End If
    '                    dgColumn.ItemStyle.HorizontalAlign = CF_CustomerProfile.ColumnStyle(ColumnName).HorizontalAlign
    '                    dgColumn.ItemStyle.Wrap = CF_CustomerProfile.ColumnStyle(ColumnName).Wrap

    '                    dgColumn.HeaderText = CF_CustomerProfile.GetDisplayColumnName(ColumnName)
    '                    dgColumn.DataField = ColumnName
    '                    dgColumn.SortExpression = ColumnName
    '                    dgList.Columns.Add(dgColumn)
    '                    dgColumn = Nothing
    '                    aryDataItem.Add(ColumnName)
    '            End Select
    '        Next

    '        aryDataItem = aryDataItem
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    #End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    
End Class
