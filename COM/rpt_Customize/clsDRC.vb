Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsDRC
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDRC"
        End Get
    End Property

    Public Function DLLGetSR(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsCustomizeDB As clsDB
        Try
            clsCustomizeDB = New clsDB
            With clsCustomizeDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_GET_SALESREP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DLLGetSR :" & ex.Message))
        Finally
            clsCustomizeDB = Nothing
        End Try
    End Function
    Public Function DLLGetPRD(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsCustomizeDB As clsDB
        Try
            clsCustomizeDB = New clsDB
            With clsCustomizeDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_GET_PRDLIST" '"SPP_RPT_DRC_GET_PRDLIST"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DLLGetPRD :" & ex.Message))
        Finally
            clsCustomizeDB = Nothing
        End Try
    End Function

    Public Function GetDRCByCust(ByVal strSalesrepCode As String, ByVal strDateStart As String, ByVal strDateEnd As String, ByVal strPrdCode As String, Optional ByVal strNV As String = "1") As DataTable
        Dim clsCustomizeDB As clsDB
        Try
            Dim strUserID As String = CStr(Web.HttpContext.Current.Session("UserID"))
            Dim strPrincipalID As String = CStr(Web.HttpContext.Current.Session("PRINCIPAL_ID"))
            Dim strPrincipalCode As String = CStr(Web.HttpContext.Current.Session("PRINCIPAL_CODE"))
            clsCustomizeDB = New clsDB
            With clsCustomizeDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_BY_CUST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("STARTDATE", strDateStart, clsDB.DataType.DBString)
                .addItem("ENDDATE", strDateEnd, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("NV", strNV, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDRCByCust :" & ex.Message))
        Finally
            clsCustomizeDB = Nothing
        End Try
    End Function
    Public Function GetDRCByCustClass(ByVal strSalesrepCode As String, ByVal strDateStart As String, ByVal strDateEnd As String, ByVal strPrdCode As String, Optional ByVal strNV As String = "1") As DataTable
        Dim clsCustomizeDB As clsDB
        Try
            Dim strUserID As String = CStr(Web.HttpContext.Current.Session("UserID"))
            Dim strPrincipalID As String = CStr(Web.HttpContext.Current.Session("PRINCIPAL_ID"))
            Dim strPrincipalCode As String = CStr(Web.HttpContext.Current.Session("PRINCIPAL_CODE"))
            clsCustomizeDB = New clsDB
            With clsCustomizeDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_BY_CUSTCLASS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("STARTDATE", strDateStart, clsDB.DataType.DBString)
                .addItem("ENDDATE", strDateEnd, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("NV", strNV, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDRCByCustClass :" & ex.Message))
        Finally
            clsCustomizeDB = Nothing
        End Try
    End Function
    Public Function GetDRCByDistrict(ByVal strSalesrepCode As String, ByVal strDateStart As String, ByVal strDateEnd As String, ByVal strPrdCode As String, Optional ByVal strNV As String = "1") As DataTable
        Dim clsCustomizeDB As clsDB
        Try
            Dim strUserID As String = CStr(Web.HttpContext.Current.Session("UserID"))
            Dim strPrincipalID As String = CStr(Web.HttpContext.Current.Session("PRINCIPAL_ID"))
            Dim strPrincipalCode As String = CStr(Web.HttpContext.Current.Session("PRINCIPAL_CODE"))
            clsCustomizeDB = New clsDB
            With clsCustomizeDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_BY_DISTRICT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("STARTDATE", strDateStart, clsDB.DataType.DBString)
                .addItem("ENDDATE", strDateEnd, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("NV", strNV, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDRCByDistrict :" & ex.Message))
        Finally
            clsCustomizeDB = Nothing
        End Try
    End Function
#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class
