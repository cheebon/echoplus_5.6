'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	16/04/2008
'	Purpose	    :	Field Force Customer List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_Mapping_FieldForceCustList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum TypeName As Long
        Customer = 0
        Contact = 1
    End Enum

    Public ReadOnly Property PageName() As String
        Get
            Return "FieldForceCustList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.FIELDFORCECUST)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.FIELDFORCECUST
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                End If

            Else
                If Master_Row_Count > 0 Then
                    For Each dr As GridViewRow In dgList.Rows
                        Dim btnPreview As Button = CType(dr.Cells(aryDataItem.IndexOf("BTN_ROUTE")).Controls(0), Button)
                        If btnPreview IsNot Nothing Then
                            btnPreview.CssClass = "cls_button"
                        End If
                    Next
                End If
            End If

            lblErr.Text = ""

            wuc_FieldForceCustPDSList.Type = TypeName.Customer
            wuc_FieldForceContPDSList.Type = TypeName.Contact

            wuc_FieldForceCustBTCList.Type = TypeName.Customer
            wuc_FieldForceContBTCList.Type = TypeName.Contact

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            wuc_toolbar.InfoString = ""

            ActivateFirstTab()
            RenewDataBind()

            If wuc_toolbar.TeamCode = "" Then
                wuc_toolbar.InfoString = "Please select the team."
            ElseIf wuc_toolbar.SalesrepCode = "" Then
                wuc_toolbar.InfoString = "Please select the salesrep."
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        If wuc_toolbar.TeamCode = "" Then
            wuc_toolbar.InfoString = "Please select the team."
            wuc_toolbar.updatePanel()
            Exit Sub
        ElseIf wuc_toolbar.SalesrepCode = "" Then
            wuc_toolbar.InfoString = "Please select the salesrep."
            wuc_toolbar.updatePanel()
            Exit Sub
        End If

        With (wuc_FieldForceCustPop)
            .TeamCode = wuc_toolbar.TeamCode
            .SalesrepCode = wuc_toolbar.SalesrepCode
            .ResetPage()
            .BindDefault()
            .Show()
        End With
    End Sub

    Protected Sub btnPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_FieldForceCustPop.SelectButton_Click
        RenewDataBind()
    End Sub

    Protected Sub btnPopClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_FieldForceCustPop.CloseButton_Click, wuc_FieldForceCustRoutePop.SaveButton_Click, wuc_FieldForceCustRoutePop.CloseButton_Click
        wuc_toolbar.show() 'HL:20080522
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080526
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust

            With (wuc_toolbar)
                DT = clsFieldForceCust.GetFieldForceCustList(.TeamCode, .SalesrepCode, .SearchType, .SearchValue)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListExport() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust

            With (wuc_toolbar)
                DT = clsFieldForceCust.GetFieldForceCustListExport(.TeamCode, .SalesrepCode, .SearchType, .SearchValue)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            If isExport = True Then
                dtCurrentTable = GetRecListExport()
            Else
                dtCurrentTable = GetRecList()
            End If

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowPaging = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

            'ADD BUTTON EDIT
            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details-------------------------------------------------
            'If Master_Row_Count > 0 AndAlso _
            '    Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, SubModuleAction.Edit) Then 'HL: 20080429 (AR)
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 AndAlso _
                Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, SubModuleAction.Delete) Then 'HL: 20080429 (AR)

                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = "Delete"

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_DELETE")
            End If

            'ADD BUTTON ROUTE
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "80"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = ""

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Button
                    dgBtnColumn.ControlStyle.CssClass = "cls_button"
                    dgBtnColumn.ShowSelectButton = True
                    dgBtnColumn.SelectText = "View Route"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_ROUTE")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldForceCustList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldForceCustList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldForceCustList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldForceCustList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldForceCustList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strCustCode As String = sender.datakeys(e.NewEditIndex).item("CUST_CODE")
            Dim strContCode As String = sender.datakeys(e.NewEditIndex).item("CONT_CODE")

            wuc_FieldForceCustPDSList.TeamCode = wuc_toolbar.TeamCode
            wuc_FieldForceCustPDSList.SalesrepCode = wuc_toolbar.SalesrepCode
            wuc_FieldForceCustPDSList.CustCode = strCustCode

            wuc_FieldForceContPDSList.TeamCode = wuc_toolbar.TeamCode
            wuc_FieldForceContPDSList.SalesrepCode = wuc_toolbar.SalesrepCode
            wuc_FieldForceContPDSList.CustCode = strCustCode
            wuc_FieldForceContPDSList.ContCode = strContCode

            wuc_FieldForceCustBTCList.TeamCode = wuc_toolbar.TeamCode
            wuc_FieldForceCustBTCList.SalesrepCode = wuc_toolbar.SalesrepCode
            wuc_FieldForceCustBTCList.CustCode = strCustCode

            wuc_FieldForceContBTCList.TeamCode = wuc_toolbar.TeamCode
            wuc_FieldForceContBTCList.SalesrepCode = wuc_toolbar.SalesrepCode
            wuc_FieldForceContBTCList.CustCode = strCustCode
            wuc_FieldForceContBTCList.ContCode = strContCode

            lblSalesrepCode.Text = wuc_toolbar.SalesrepCode
            lblCustCode.Text = strCustCode
            lblContCode.Text = strContCode

            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust
            Dim DT As DataTable = clsFieldForceCust.GetFieldForceCustContDtl(wuc_toolbar.SalesrepCode, strCustCode, strContCode)
            If DT.Rows.Count > 0 Then
                lblSalesrepName.Text = IIf(IsDBNull(DT.Rows(0)("SALESREP_NAME")), "", DT.Rows(0)("SALESREP_NAME"))
                lblContName.Text = IIf(IsDBNull(DT.Rows(0)("CONT_NAME")), "", DT.Rows(0)("CONT_NAME"))
                lblSpecialtyName.Text = IIf(IsDBNull(DT.Rows(0)("SPECIALITY_NAME")), "", DT.Rows(0)("SPECIALITY_NAME"))
                lblCustName.Text = IIf(IsDBNull(DT.Rows(0)("CUST_NAME")), "", DT.Rows(0)("CUST_NAME"))
                lblDepartmentName.Text = IIf(IsDBNull(DT.Rows(0)("DEPARTMENT_NAME")), "", DT.Rows(0)("DEPARTMENT_NAME"))
                lblAssistantName.Text = IIf(IsDBNull(DT.Rows(0)("ASST_NAME")), "", DT.Rows(0)("ASST_NAME"))
                lblPriorityName.Text = IIf(IsDBNull(DT.Rows(0)("PRIORITY_NAME")), "", DT.Rows(0)("PRIORITY_NAME"))
                lblPersonalInterest.Text = IIf(IsDBNull(DT.Rows(0)("PERSONAL_INTEREST")), "", DT.Rows(0)("PERSONAL_INTEREST"))
                lblMedicalInterest.Text = IIf(IsDBNull(DT.Rows(0)("MED_INTEREST")), "", DT.Rows(0)("MED_INTEREST"))
                lblMarketingPreference.Text = IIf(IsDBNull(DT.Rows(0)("MKTG_PREF")), "", DT.Rows(0)("MKTG_PREF"))
                lblXField1.Text = IIf(IsDBNull(DT.Rows(0)("XFIELD_1")), "", DT.Rows(0)("XFIELD_1"))
                lblXField2.Text = IIf(IsDBNull(DT.Rows(0)("XFIELD_2")), "", DT.Rows(0)("XFIELD_2"))
                lblnotes.Text = IIf(IsDBNull(DT.Rows(0)("NOTES")), "", DT.Rows(0)("NOTES"))
            End If


            ActivateEditTab()
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 AndAlso Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, SubModuleAction.Delete) Then 'HL: 20080429 (AR)
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing

                        If btnDelete IsNot Nothing Then
                            strDeleteMsg = "Are you sure want to delete?"
                            'btnDelete.OnClientClick = "if (confirm('" + strDeleteMsg + "') == false) { window.event.returnValue = false; return false; }"
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        Try
            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust
            Dim strCustCode, strContCode As String

            strCustCode = sender.datakeys(e.RowIndex).item("CUST_CODE")
            strContCode = sender.datakeys(e.RowIndex).item("CONT_CODE")

            clsFieldForceCust.DeleteFieldForceCust(wuc_toolbar.TeamCode, wuc_toolbar.SalesrepCode, strCustCode, strContCode)

            RenewDataBind()
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgList.SelectedIndexChanged
        Try
            Dim strCustCode As String = sender.SelectedDataKey.Item("CUST_CODE").ToString
            Dim strContCode As String = sender.SelectedDataKey.Item("CONT_CODE").ToString

            With (wuc_FieldForceCustRoutePop)
                .TeamCode = wuc_toolbar.TeamCode
                .SalesrepCode = wuc_toolbar.SalesrepCode
                .CustCode = strCustCode
                .ContCode = strContCode
                .LoadDvEditMode()
                .BindDetailsView()
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Field Force Customer List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""
        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""
        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""
        TabPanel5.Visible = False
        TabPanel5.HeaderText = ""

        wuc_toolbar.show()
        'lblInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "Field Force Customer Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = True
        TabPanel2.HeaderText = "Customer Priority"
        TabPanel3.Visible = True
        TabPanel3.HeaderText = "Best Time to Call by Customer"
        TabPanel4.Visible = True
        TabPanel4.HeaderText = "Contact Priority"
        TabPanel5.Visible = True
        TabPanel5.HeaderText = "Best Time to Call by Contact"

        RenewAllTab()

        wuc_toolbar.hide()
        'lblInfo.Text = ""
    End Sub

    Private Sub RenewAllTab()
        wuc_FieldForceCustPDSList.RenewDataBind()
        wuc_FieldForceContPDSList.RenewDataBind()

        wuc_FieldForceCustBTCList.RenewDataBind()
        wuc_FieldForceContBTCList.RenewDataBind()
    End Sub

    Private Sub RefreshAllTab()
        wuc_FieldForceCustPDSList.RefreshDataBind()
        wuc_FieldForceContPDSList.RefreshDataBind()

        wuc_FieldForceCustBTCList.RefreshDataBind()
        wuc_FieldForceContBTCList.RefreshDataBind()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            Dim arydgList As New ArrayList
            arydgList.Add(dgList)
            If TabPanel2.Visible Then
                arydgList.Add(wuc_FieldForceCustPDSList.GetdgCustPDSList())
                arydgList.Add(wuc_FieldForceCustBTCList.GetdgCustBTCList())
                arydgList.Add(wuc_FieldForceContPDSList.GetdgCustPDSList())
                arydgList.Add(wuc_FieldForceContBTCList.GetdgCustBTCList())
            End If

            wuc_toolbar.ExportToFile(arydgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            If TabPanel2.Visible Then
                RefreshAllTab()
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_FieldForceCustList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        ColumnName = ColumnName.ToUpper

        Select Case ColumnName
            Case "VISIT_FREQ"
                strFieldName = "Visit Freq"
            Case "CONT_PRD_SPECIALTY_NAME"
                strFieldName = "Cont Prd Specialty Name"
            Case "CONT_PRIORITY_CODE"
                strFieldName = "Cont Priority Code"
            Case "CUST_PRD_SPECIALTY_NAME"
                strFieldName = "Cust Prd Specialty Name"
            Case "CUST_PRIORITY_CODE"
                strFieldName = "Cust Priority Code"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        If ColumnName Like "*TGT" Then
            strFieldName = ColumnName.Replace("_TGT", "")
        End If

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName Like "*_TGT" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class



