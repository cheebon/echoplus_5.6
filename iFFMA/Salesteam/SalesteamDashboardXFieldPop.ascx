﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SalesteamDashboardXFieldPop.ascx.vb" Inherits="iFFMA_Salesteam_SalesteamAssignDashboardXFieldPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px; text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 92%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: right; width: 8%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; /*width: 98%*/">
                <fieldset style="padding-left: 10px; width: 100%; box-sizing: border-box;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />

                    <table cellspacing="1" cellpadding="2" rules="all" border="0" style="border-width: 0px; border-style: None; width: 100%;">
                        <tbody>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 40%; white-space: nowrap;">Dashboard XField 1
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlXFieldList1" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 40%; white-space: nowrap;">Dashboard XField 2
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlXFieldList2" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 40%; white-space: nowrap;">Dashboard XField 3
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlXFieldList3" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 40%; white-space: nowrap;">Dashboard XField 4
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlXFieldList4" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 40%; white-space: nowrap;">Dashboard XField 5
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlXFieldList5" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 40%; white-space: nowrap;">Dashboard XField 6
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlXFieldList6" runat="server" Width="150px" CssClass="cls_dropdownlist" />
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                        <center>
                            <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" />
                        </center>
                    </span>
                    <!-- End Customized Content -->

                    <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdXFieldId" runat="server" Value="" />
                    <asp:HiddenField ID="hdXFieldSeq" runat="server" Value="" />
                    <asp:HiddenField ID="hdTotal" runat="server" Value="" />
                    <asp:HiddenField ID="hdIsEdit" runat="server" Value="" />
                    <asp:HiddenField ID="hdPerTeamLimit" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server"
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden"
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop"
            BackgroundCssClass="modalBackground"
            DropShadow="True"
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>
