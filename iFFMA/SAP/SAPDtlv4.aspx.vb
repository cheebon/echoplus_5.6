'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/10/2006
'	Purpose	    :	SAP Details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_SAP_SAPDtl
    Inherits System.Web.UI.Page

    Private intPageSize As Integer
    Private dtmFormat As String = "yyyy-MM-dd hh:m:ss"
    Private dt, dtDtl, dtTxnDtl As DataTable

#Region "Local Variable"
    Private Enum DGListField

        Delete = 0
        Edit = 1
        line_no = 2
        prd_code = 3
        prd_name = 4
        ret_qty = 5
        uom_code = 6
        list_price = 7
        ret_amt = 9
        reason_code = 10
        reason_name = 11
        exp_date = 12
        batch_no = 13
       

    End Enum

#End Region
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim strTitle As String

        Try
            lblErr.Text = ""

            'Prepare Recordset,Createobject as needed
            strTitle = "SAP Details V4"
            'Call Header
            With wuc_lblheader
                .Title = strTitle '"SAP Details"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request
                txtDocType.Text = Trim(Request.QueryString("doc_type"))
                txtSearchType.Text = Trim(Request.QueryString("search_type"))
                txtSearchValue.Text = Trim(Request.QueryString("search_value"))
                txtSearchValue1.Text = Trim(Request.QueryString("search_value_1"))
                txtTxnNo.Text = Trim(Request.QueryString("txn_no"))

                objSAPQuery = New mst_SAP.clsSAPQuery
                With objSAPQuery
                    .clsProperties.doc_type = txtDocType.Text
                    .clsProperties.txn_no = txtTxnNo.Text
                    dt = .GetSAPDtl()
                    dtDtl = .GetSAPDtlList()
                    dtTxnDtl = .GetTxnDtlList()

                End With
                btnResubmit.Attributes.Add("onclick", "javascript:return confirm('Are you want to resubmit?')")
                btnCancelTxn.Attributes.Add("onclick", "javascript:return confirm('Are you want to cancel?')")
                btnEdit.Attributes.Add("onclick", "javascript:return confirm('Are you want to edit?')")
                'btnDelete.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Delete)
                'btnEdit.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)

                If dt.Rows.Count > 0 Then
                    lblStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("STATUS")), "", dt.Rows(0)("STATUS"))
                    lblTxnStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_status")), "", dt.Rows(0)("txn_status"))
                    If txtDocType.Text = "TRA" Then
                        lblLNStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_status")), "", dt.Rows(0)("ln_status"))
                        lblLNRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_reason")), "", dt.Rows(0)("ln_reason"))
                        pnlLN.Visible = True
                    Else
                        pnlLN.Visible = False
                    End If

                    lblTxnNoValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_no")), "", dt.Rows(0)("txn_no"))
                    lblDocNoValue.Text = IIf(IsDBNull(dt.Rows(0)("DOC_NO")), "", dt.Rows(0)("DOC_NO"))
                    lblSRCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("salesrep_code")), "", dt.Rows(0)("salesrep_code"))
                    lblLastUpdateValue.Text = IIf(IsDBNull(dt.Rows(0)("LASTUPDATE")), "", dt.Rows(0)("LASTUPDATE"))
                    lblTxnDateValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_timestamp")), "", dt.Rows(0)("txn_timestamp"))
                    lblOrderTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("AUART")), "", dt.Rows(0)("AUART"))
                    lblSalesOrgValue.Text = IIf(IsDBNull(dt.Rows(0)("VKORG")), "", dt.Rows(0)("VKORG"))
                    lblDistChannelValue.Text = IIf(IsDBNull(dt.Rows(0)("VTWEG")), "", dt.Rows(0)("VTWEG"))
                    lblDivCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("SPART")), "", dt.Rows(0)("SPART"))
                    lblSalesOffValue.Text = IIf(IsDBNull(dt.Rows(0)("VKBUR")), "", dt.Rows(0)("VKBUR"))
                    lblSalesGrpValue.Text = IIf(IsDBNull(dt.Rows(0)("VKGRP")), "", dt.Rows(0)("VKGRP"))
                    lblPlantValue.Text = IIf(IsDBNull(dt.Rows(0)("WERKS")), "", dt.Rows(0)("WERKS"))
                    lblSlocValue.Text = IIf(IsDBNull(dt.Rows(0)("LGORT")), "", dt.Rows(0)("LGORT"))
                    lblShipPointValue.Text = IIf(IsDBNull(dt.Rows(0)("VSTEL")), "", dt.Rows(0)("VSTEL"))
                    lblSoldToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNNR")), "", dt.Rows(0)("KUNNR"))
                    lblShipToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNDE")), "", dt.Rows(0)("KUNDE"))
                    lblPayerCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("PAYER")), "", dt.Rows(0)("PAYER"))
                    lblPONoValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTNK")), "", dt.Rows(0)("BSTNK"))
                    lblPODateValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTDK")), "", dt.Rows(0)("BSTDK"))
                    lblPOTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTRK")), "", dt.Rows(0)("BSTRK"))
                    lblPOSuppValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTZD")), "", dt.Rows(0)("BSTZD"))
                    lblBNameValue.Text = IIf(IsDBNull(dt.Rows(0)("BNAME")), "", dt.Rows(0)("BNAME"))
                    lblIntRefValue.Text = IIf(IsDBNull(dt.Rows(0)("IHREZ")), "", dt.Rows(0)("IHREZ"))
                    lblHdrShipText1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_1")), "", dt.Rows(0)("H_SHIP_TEXT_1"))
                    lblHdrShipText2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_2")), "", dt.Rows(0)("H_SHIP_TEXT_2"))
                    lblHdrShipText3Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_3")), "", dt.Rows(0)("H_SHIP_TEXT_3"))
                    lblHdrDiscType1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_1")), "", dt.Rows(0)("H_DISC_TYP_1"))
                    lblHdrDiscVal1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_1")), "", dt.Rows(0)("H_DISC_VAL_1"))
                    lblHdrDiscType2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_2")), "", dt.Rows(0)("H_DISC_TYP_2"))
                    lblHdrDiscVal2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_2")), "", dt.Rows(0)("H_DISC_VAL_2"))
                    lblDelDateValue.Text = IIf(IsDBNull(dt.Rows(0)("KETDAT")), "", dt.Rows(0)("KETDAT"))
                    lblUsageIndValue.Text = IIf(IsDBNull(dt.Rows(0)("VKAUS")), "", dt.Rows(0)("VKAUS"))
                    lblResendCntValue.Text = IIf(IsDBNull(dt.Rows(0)("RESENDCOUNTER")), "", dt.Rows(0)("RESENDCOUNTER"))
                    lblOrderByValue.Text = IIf(IsDBNull(dt.Rows(0)("ORDER_BY")), "", dt.Rows(0)("ORDER_BY"))
                    lblRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("REMARKS")), "", dt.Rows(0)("REMARKS"))
                    lblNoteValue.Text = IIf(IsDBNull(dt.Rows(0)("NOTE")), "", dt.Rows(0)("NOTE"))

                    txtRefNo.Text = IIf(IsDBNull(dt.Rows(0)("REF_NO")), "", dt.Rows(0)("REF_NO"))
                    txtCtnNo.Text = IIf(IsDBNull(dt.Rows(0)("CTN_NO")), "", dt.Rows(0)("CTN_NO"))
                    ddlCollBy.SelectedValue = IIf(IsDBNull(UCase(dt.Rows(0)("COLL_CODE"))), "", UCase(dt.Rows(0)("COLL_CODE")))
                    lblTtlRetAmtValue.Text = IIf(IsDBNull(dt.Rows(0)("TTL_RET_AMT")), "", dt.Rows(0)("TTL_RET_AMT"))
                    lblLNStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("LN_STATUS")), "", dt.Rows(0)("LN_STATUS"))

                    With dgList
                        .DataSource = dtTxnDtl
                        .DataBind()

                        'dgList.Columns(DGListField.Edit).Visible = False
                        'dgList.Columns(DGListField.Delete).Visible = False

                        If lblLNStatusValue.Text = "R" Then
                            If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Edit) Then

                                dgList.Columns(DGListField.Edit).Visible = False
                            Else
                                dgList.Columns(DGListField.Edit).Visible = True
                            End If

                            If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Delete) Then
                                dgList.Columns(DGListField.Delete).Visible = False
                            Else
                                dgList.Columns(DGListField.Delete).Visible = True
                            End If

                        ElseIf lblTxnStatusValue.Text = "B" Then
                            If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Delete) Then
                                dgList.Columns(DGListField.Delete).Visible = False
                            Else
                                dgList.Columns(DGListField.Delete).Visible = True
                            End If

                            dgList.Columns(DGListField.Edit).Visible = True
                        Else

                            dgList.Columns(DGListField.Delete).Visible = False
                            dgList.Columns(DGListField.Edit).Visible = False
                        End If
                    End With



                End If
            End If

            If lblTxnStatusValue.Text = "P" And lblStatusValue.Text = "" Then
                btnCancelTxn.Visible = True
            Else
                btnCancelTxn.Visible = False
            End If

            If lblTxnStatusValue.Text = "B" And lblStatusValue.Text = "" Then
                btnUnBlock.Visible = True
            Else
                btnUnBlock.Visible = False
            End If

            If lblStatusValue.Text = "E" Then
                btnResubmit.Visible = True
            Else
                btnResubmit.Visible = False
            End If

            If lblLNStatusValue.Text = "R" Then
                If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Edit) Then
                    BtnEdit.Visible = False
                Else
                    BtnEdit.Visible = True
                End If

            ElseIf lblTxnStatusValue.Text = "B" Then
                BtnEdit.Visible = True
            Else
                BtnEdit.Visible = False
            End If

         
        Catch ex As Exception
            ExceptionMsg("SAPDtl.Page_Load : " & ex.ToString)
        Finally
            objSAPQuery = Nothing
        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable

        Try
            'If intPassFlag <> 1 Or IsNothing(ViewState("SAPDtl")) Then
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.doc_type = txtDocType.Text
                .clsProperties.txn_no = txtTxnNo.Text
                dt = .GetSAPDtlList()
            End With
            'ViewState("SAPDtl") = dt
            'Else
            'dt = ViewState("SAPDtl")
            'End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            'dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount.ToString
            '    .CurrentPageIndex = dgList.PageIndex
            'End With

        Catch ex As Exception
            ExceptionMsg("SAPDtl.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            objSAPQuery = Nothing
        End Try
    End Sub


    Private Sub RefreshDatabinding()
        Dim objSAPQuery As New mst_SAP.clsSAPQuery
        objSAPQuery = New mst_SAP.clsSAPQuery

        With objSAPQuery
            .clsProperties.doc_type = txtDocType.Text
            .clsProperties.txn_no = txtTxnNo.Text
            dt = .GetSAPDtl()
            dtTxnDtl = .GetTxnDtlList()

        End With

        If dt.Rows.Count > 0 Then
            lblStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("STATUS")), "", dt.Rows(0)("STATUS"))
            lblTxnStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_status")), "", dt.Rows(0)("txn_status"))

            If txtDocType.Text = "TRA" Then
                lblLNStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_status")), "", dt.Rows(0)("ln_status"))
                lblLNRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_reason")), "", dt.Rows(0)("ln_reason"))
                pnlLN.Visible = True
            Else
                pnlLN.Visible = False
            End If

            lblTxnNoValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_no")), "", dt.Rows(0)("txn_no"))
            lblDocNoValue.Text = IIf(IsDBNull(dt.Rows(0)("DOC_NO")), "", dt.Rows(0)("DOC_NO"))
            lblSRCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("salesrep_code")), "", dt.Rows(0)("salesrep_code"))
            lblLastUpdateValue.Text = IIf(IsDBNull(dt.Rows(0)("LASTUPDATE")), "", dt.Rows(0)("LASTUPDATE"))
            lblTxnDateValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_timestamp")), "", dt.Rows(0)("txn_timestamp"))
            lblOrderTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("AUART")), "", dt.Rows(0)("AUART"))
            lblSalesOrgValue.Text = IIf(IsDBNull(dt.Rows(0)("VKORG")), "", dt.Rows(0)("VKORG"))
            lblDistChannelValue.Text = IIf(IsDBNull(dt.Rows(0)("VTWEG")), "", dt.Rows(0)("VTWEG"))
            lblDivCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("SPART")), "", dt.Rows(0)("SPART"))
            lblSalesOffValue.Text = IIf(IsDBNull(dt.Rows(0)("VKBUR")), "", dt.Rows(0)("VKBUR"))
            lblSalesGrpValue.Text = IIf(IsDBNull(dt.Rows(0)("VKGRP")), "", dt.Rows(0)("VKGRP"))
            lblPlantValue.Text = IIf(IsDBNull(dt.Rows(0)("WERKS")), "", dt.Rows(0)("WERKS"))
            lblSlocValue.Text = IIf(IsDBNull(dt.Rows(0)("LGORT")), "", dt.Rows(0)("LGORT"))
            lblShipPointValue.Text = IIf(IsDBNull(dt.Rows(0)("VSTEL")), "", dt.Rows(0)("VSTEL"))
            lblSoldToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNNR")), "", dt.Rows(0)("KUNNR"))
            lblShipToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNDE")), "", dt.Rows(0)("KUNDE"))
            lblPayerCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("PAYER")), "", dt.Rows(0)("PAYER"))
            lblPONoValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTNK")), "", dt.Rows(0)("BSTNK"))
            lblPODateValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTDK")), "", dt.Rows(0)("BSTDK"))
            lblPOTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTRK")), "", dt.Rows(0)("BSTRK"))
            lblPOSuppValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTZD")), "", dt.Rows(0)("BSTZD"))
            lblBNameValue.Text = IIf(IsDBNull(dt.Rows(0)("BNAME")), "", dt.Rows(0)("BNAME"))
            lblIntRefValue.Text = IIf(IsDBNull(dt.Rows(0)("IHREZ")), "", dt.Rows(0)("IHREZ"))
            lblHdrShipText1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_1")), "", dt.Rows(0)("H_SHIP_TEXT_1"))
            lblHdrShipText2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_2")), "", dt.Rows(0)("H_SHIP_TEXT_2"))
            lblHdrShipText3Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_3")), "", dt.Rows(0)("H_SHIP_TEXT_3"))
            lblHdrDiscType1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_1")), "", dt.Rows(0)("H_DISC_TYP_1"))
            lblHdrDiscVal1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_1")), "", dt.Rows(0)("H_DISC_VAL_1"))
            lblHdrDiscType2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_2")), "", dt.Rows(0)("H_DISC_TYP_2"))
            lblHdrDiscVal2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_2")), "", dt.Rows(0)("H_DISC_VAL_2"))
            lblDelDateValue.Text = IIf(IsDBNull(dt.Rows(0)("KETDAT")), "", dt.Rows(0)("KETDAT"))
            lblUsageIndValue.Text = IIf(IsDBNull(dt.Rows(0)("VKAUS")), "", dt.Rows(0)("VKAUS"))
            lblResendCntValue.Text = IIf(IsDBNull(dt.Rows(0)("RESENDCOUNTER")), "", dt.Rows(0)("RESENDCOUNTER"))
            lblOrderByValue.Text = IIf(IsDBNull(dt.Rows(0)("ORDER_BY")), "", dt.Rows(0)("ORDER_BY"))
            lblRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("REMARKS")), "", dt.Rows(0)("REMARKS"))
            lblNoteValue.Text = IIf(IsDBNull(dt.Rows(0)("NOTE")), "", dt.Rows(0)("NOTE"))

            txtRefNo.Text = IIf(IsDBNull(dt.Rows(0)("REF_NO")), "", dt.Rows(0)("REF_NO"))
            txtCtnNo.Text = IIf(IsDBNull(dt.Rows(0)("CTN_NO")), "", dt.Rows(0)("CTN_NO"))

            ddlCollBy.SelectedValue = IIf(IsDBNull(UCase(dt.Rows(0)("COLL_CODE"))), "", UCase(dt.Rows(0)("COLL_CODE")))
            lblTtlRetAmtValue.Text = IIf(IsDBNull(dt.Rows(0)("TTL_RET_AMT")), "", dt.Rows(0)("TTL_RET_AMT"))
            lblLNStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("LN_STATUS")), "", dt.Rows(0)("LN_STATUS"))

            With dgList
                .DataSource = dtTxnDtl
                .DataBind()

                dgList.Columns(DGListField.Edit).Visible = False
                dgList.Columns(DGListField.Delete).Visible = False
            End With



        End If


    End Sub


    Private Sub RefreshDatabindingDtl()
        Dim objSAPQuery As New mst_SAP.clsSAPQuery
        objSAPQuery = New mst_SAP.clsSAPQuery

        With objSAPQuery
            .clsProperties.doc_type = txtDocType.Text
            .clsProperties.txn_no = txtTxnNo.Text
            dtTxnDtl = .GetTxnDtlList()

        End With

        If dtTxnDtl.Rows.Count > 0 Then
            With dgList
                .DataSource = dtTxnDtl
                .DataBind()

                'dgList.Columns(DGListField.Edit).Visible = True

                'If Trim(lblTxnStatusValue.Text) = "B" Then
                '    BtnEdit.Visible = False
                '    dgList.Columns(DGListField.Delete).Visible = False
                'Else
                '    BtnEdit.Visible = True
                '    dgList.Columns(DGListField.Delete).Visible = True
                'End If

                If lblLNStatusValue.Text = "R" Then
                    If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Edit) Then
                        BtnEdit.Visible = False
                        dgList.Columns(DGListField.Edit).Visible = False
                    Else
                        BtnEdit.Visible = True
                        dgList.Columns(DGListField.Edit).Visible = True

                        If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Delete) Then
                            dgList.Columns(DGListField.Delete).Visible = False
                        Else
                            dgList.Columns(DGListField.Delete).Visible = True
                        End If

                    End If

                ElseIf lblTxnStatusValue.Text = "B" Then
                    BtnEdit.Visible = True
                    If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Delete) Then
                        dgList.Columns(DGListField.Delete).Visible = False
                    Else
                        dgList.Columns(DGListField.Delete).Visible = True
                    End If
                    dgList.Columns(DGListField.Edit).Visible = True
                Else
                    BtnEdit.Visible = False
                    dgList.Columns(DGListField.Delete).Visible = False
                    dgList.Columns(DGListField.Edit).Visible = False
                End If



            End With

        End If

    End Sub

    Private Sub LoadDGListEditDeleteByAccessright()

    End Sub


#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPDtl.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPDtl.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region


    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

   


    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("SAPDtl.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

        Dim strBatch As String
        Dim dtv As DataView
        Dim strID As String
        Dim strReasonCode As String
        Dim strExpiryDate As String

        strID = dgList.DataKeys(e.NewEditIndex).Item("prd_code")

        dtv = New DataView(dtTxnDtl)
        dtv.RowFilter = "ID = '" & strID & "'"


        strExpiryDate = IIf(CType(dgList.Rows(e.NewEditIndex).Cells(DGListField.exp_date).FindControl("lblExpDate_Vw"), Label).Text = "", DateTime.Now.ToString("yyyy-MM-dd"), CType(dgList.Rows(e.NewEditIndex).Cells(DGListField.exp_date).FindControl("lblExpDate_Vw"), Label).Text)
        strReasonCode = dgList.Rows(e.NewEditIndex).Cells(DGListField.reason_code).Text


        dgList.EditIndex = e.NewEditIndex
        RefreshDatabindingDtl()

        CType(dgList.Rows(e.NewEditIndex).Cells(DGListField.exp_date).FindControl("txtExpDate"), wuc_DateCalendar).DateStart = strExpiryDate


        'strBatch = CType(dgList.Rows(e.NewEditIndex).Cells(11).FindControl("lblBatch_Vw"), Label).Text


        LoadReasonCode(e.NewEditIndex, strReasonCode)


    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objSAPQuery As New mst_SAP.clsSAP



        Try
            With objSAPQuery

                .clsRetDtlProperties.user_id = Session("UserID")
                .clsRetDtlProperties.user_name = Session("UserName")
                .clsRetDtlProperties.txn_no = txtTxnNo.Text
                .clsRetDtlProperties.line_no = dgList.Rows(e.RowIndex).Cells(DGListField.line_no).Text
                .clsRetDtlProperties.prd_code = dgList.Rows(e.RowIndex).Cells(DGListField.prd_code).Text

                .DeleteRetDtl()

            End With

            RefreshDatabindingDtl()

        Catch ex As Exception
            'ExceptionMsg(PageName & ".dgCondTypeDtl_RowUpdating : " + ex.ToString)
        End Try


    End Sub

    Protected Sub dgList_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgList.RowCancelingEdit
        dgList.EditIndex = -1
        RefreshDatabindingDtl()
    End Sub

    Protected Sub dgList_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgList.RowUpdating

        Dim objSAPQuery As New mst_SAP.clsSAP
        Dim dbActualPrice As Double, dbUnitPrice As Double

        dbActualPrice = dgList.DataKeys(e.RowIndex).Item("actual_price")

        Try
            With objSAPQuery

                .clsRetDtlProperties.user_id = Session("UserID")
                .clsRetDtlProperties.user_name = Session("UserName")
                .clsRetDtlProperties.txn_no = txtTxnNo.Text
                .clsRetDtlProperties.line_no = dgList.Rows(e.RowIndex).Cells(DGListField.line_no).Text
                .clsRetDtlProperties.prd_code = dgList.Rows(e.RowIndex).Cells(DGListField.prd_code).Text
                .clsRetDtlProperties.batch_no = CType(dgList.Rows(e.RowIndex).FindControl("txtBatch_Edit"), TextBox).Text
                .clsRetDtlProperties.reason_code = CType(dgList.Rows(e.RowIndex).FindControl("ddlReasonCode"), DropDownList).SelectedValue

                If (CType(dgList.Rows(e.RowIndex).FindControl("txtListPrice_Edit"), TextBox).Text = "") Then
                    .clsRetDtlProperties.unit_price = "0.00"
                    dbUnitPrice = "0.00"
                Else
                    .clsRetDtlProperties.unit_price = CType(dgList.Rows(e.RowIndex).FindControl("txtListPrice_Edit"), TextBox).Text
                    dbUnitPrice = CType(dgList.Rows(e.RowIndex).FindControl("txtListPrice_Edit"), TextBox).Text
                End If

                .clsRetDtlProperties.expired_date = CType(dgList.Rows(e.RowIndex).FindControl("txtExpDate"), wuc_DateCalendar).DateStart

                If dbUnitPrice > dbActualPrice Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Msg", "alert('The Unit price cannot be higher than the actual price');", True)
                Else
                    .UpdateRetDtl()
                    dgList.EditIndex = -1
                    RefreshDatabindingDtl()
                End If

            End With

            

        Catch ex As Exception
            'ExceptionMsg(PageName & ".dgCondTypeDtl_RowUpdating : " + ex.ToString)
        End Try


    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnViewTray_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnViewTray_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTray.Click
        Try
            Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnViewTray_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnRefresh_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            Response.Redirect("SAPList_v4.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnRefresh_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnResubmit_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnResubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResubmit.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            If Trim(lblTxnNoValue.Text) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(lblStatusValue.Text) <> "E" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to resubmit.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(txtDocType.Text)
                    .clsProperties.txn_no = Trim(txtTxnNo.Text)
                    .Resubmit()
                End With

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has resubmitted.');", True)

                Response.Redirect("SAPList_v4.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnResubmit_Click : " & ex.ToString)
        End Try
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCancelTxn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTxn.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            If Trim(lblTxnNoValue.Text) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(lblTxnStatusValue.Text) <> "P" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to cancel.');", True)
                Exit Sub
            ElseIf Trim(lblStatusValue.Text) <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to SAP and cannot be cancelled.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(txtDocType.Text)
                    .clsProperties.txn_no = Trim(txtTxnNo.Text)
                    .Cancel()
                End With

                Response.Redirect("SAPList_v4.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has cancelled.');", True)
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnCancelTxn_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnUnBlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnBlock.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            If Trim(lblTxnNoValue.Text) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(lblTxnStatusValue.Text) <> "B" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to unblock.');", True)
                Exit Sub
            ElseIf Trim(lblStatusValue.Text) <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to SAP and cannot be unblock.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(txtDocType.Text)
                    .clsProperties.txn_no = Trim(txtTxnNo.Text)
                    .clsProperties.ctn_no = Trim(txtCtnNo.Text)
                    .UnBlock()
                End With

                Response.Redirect("SAPList_v4.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has unblocked.');", True)
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnUnBlock_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEdit.Click
        Dim objSAP As mst_SAP.clsSAP

        If (BtnEdit.Text = "Edit") Then
            Try
                If Trim(lblTxnNoValue.Text) = "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                    Exit Sub
                ElseIf Trim(lblTxnStatusValue.Text) <> "P" And Trim(lblTxnStatusValue.Text) <> "B" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to edit.');", True)
                    Exit Sub
                ElseIf Trim(lblStatusValue.Text) <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to SAP and cannot be edit.');", True)
                    Exit Sub
                ElseIf Trim(lblLNStatusValue.Text) = "W" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to LN and cannot be edit.');", True)
                    Exit Sub
                ElseIf Trim(lblTxnStatusValue.Text) = "B" Then
                    ddlCollBy.Enabled = False
                    txtRefNo.Enabled = False
                    txtCtnNo.Enabled = True

                    dgList.Columns(DGListField.Edit).Visible = True
                    dgList.Columns(DGListField.Delete).Visible = False

                    BtnCancel.Enabled = True
                    BtnEdit.Visible = False

                Else
                    ddlCollBy.Enabled = True
                    txtRefNo.Enabled = True
                    txtCtnNo.Enabled = True

                    dgList.Columns(DGListField.Edit).Visible = True

                    If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.SAPLISTV4, SubModuleAction.Delete) Then
                        dgList.Columns(DGListField.Delete).Visible = False
                    Else
                        dgList.Columns(DGListField.Delete).Visible = True
                    End If

                    BtnCancel.Enabled = True
                    BtnEdit.Text = "Resubmit LN"
                End If

            Catch ex As Exception
                ExceptionMsg("SAPDtl.btnEdit_Click : " & ex.ToString)
            End Try
        ElseIf (BtnEdit.Text = "Resubmit LN") Then

            objSAP = New mst_SAP.clsSAP
            With objSAP

                .clsRetHdrProperties.user_id = Session("UserID")
                .clsRetHdrProperties.user_name = Session("UserName")
                .clsRetHdrProperties.txn_no = Trim(txtTxnNo.Text)
                .clsRetHdrProperties.coll_code = Trim(ddlCollBy.SelectedValue)
                .clsRetHdrProperties.ref_no = Trim(txtRefNo.Text)
                .clsRetHdrProperties.ctn_no = Trim(txtCtnNo.Text)
                .UpdateRetHdr()
            End With

            BtnEdit.Text = "Edit"
            BtnCancel.Enabled = False
            ddlCollBy.Enabled = False
            txtRefNo.Enabled = False
            txtCtnNo.Enabled = False

            dgList.Columns(DGListField.Edit).Visible = False
            dgList.Columns(DGListField.Delete).Visible = False

            RefreshDatabinding()
        ElseIf (BtnEdit.Text = "Save") Then

            objSAP = New mst_SAP.clsSAP
            With objSAP

                .clsRetHdrProperties.user_id = Session("UserID")
                .clsRetHdrProperties.user_name = Session("UserName")
                .clsRetHdrProperties.txn_no = Trim(txtTxnNo.Text)
                .clsRetHdrProperties.coll_code = Trim(ddlCollBy.SelectedValue)
                .clsRetHdrProperties.ref_no = Trim(txtRefNo.Text)
                .clsRetHdrProperties.ctn_no = Trim(txtCtnNo.Text)
                .UpdateRetHdr()
            End With

            BtnEdit.Text = "Edit"
            BtnCancel.Enabled = False
            ddlCollBy.Enabled = False
            txtRefNo.Enabled = False
            txtCtnNo.Enabled = False

            dgList.Columns(DGListField.Edit).Visible = False
            dgList.Columns(DGListField.Delete).Visible = False

            RefreshDatabinding()
        End If

       
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            ddlCollBy.Enabled = False
            txtRefNo.Enabled = False
            txtCtnNo.Enabled = False

            dgList.Columns(DGListField.Edit).Visible = False
            dgList.Columns(DGListField.Delete).Visible = False

            BtnCancel.Enabled = False
            BtnEdit.Text = "Edit"
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnCancel_Click : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadReasonCode(ByVal rowIndex As String, ByVal strReasonCode As String)
        Dim dtReasonCode As DataTable
        Dim objSAPQuery As New mst_SAP.clsSAPQuery

        Try
            Dim ddlReasonCode As DropDownList = CType(dgList.Rows(rowIndex).FindControl("ddlReasonCode"), DropDownList)
            If ddlReasonCode IsNot Nothing Then
                dtReasonCode = objSAPQuery.GetRetReasonCode
                With ddlReasonCode
                    .Items.Clear()
                    .DataSource = dtReasonCode.DefaultView
                    .DataTextField = "REASON_NAME"
                    .DataValueField = "REASON_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedValue = strReasonCode
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub



End Class
