﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OnlineGallery.aspx.vb" Inherits="iFFMR_OnlineGallery" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging_v7.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../styles/jquery.iviewer.css" rel="stylesheet" type="text/css" media="all" />

    <link href="../styles/camera.css" rel="stylesheet" type="text/css" />
    <link href="../styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" />

    <script src="../scripts/iViewer/jquery.js"></script>
    <script src="../scripts/iViewer/jqueryui.js"></script>
    <script src="../scripts/iViewer/jquery.iviewer.min.js"></script>
    <script src="../scripts/iViewer/jquery.mousewheel.min.js"></script>
    <script src="../scripts/camera/jquery.easing.1.3.js"></script>
    <script src="../scripts/camera/camera.min.js"></script>

    <%--<script src="../scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>--%>

    <script src="../scripts/jquery-ui-1.8.14.custom.min.js"></script>
    <link href="~/include/DKSH.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        [id^="RImages"] {
            word-wrap: break-word!important;
        }

        .viewer {
            width: 650px;
            height: 450px;
            border: 1px solid black;
            position: relative;
        }

        .smallerviewer {
            width: 350px;
            height: 380px;
            border: 1px solid black;
            position: relative;
        }

        .wrapper {
            overflow: hidden;
        }

        .auto-style1 {
            width: 404px;
        }

        .auto-style2 {
            width: 431px;
        }

        hr {
            display: block;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
            border-color: darkgray;
        }

        .rotated360 {

            transform: rotate(360deg);
        }
        .rotated90 {

            transform: rotate(90deg);
        }
        .rotated180 {

            transform: rotate(180deg);
        }
        .rotated270 {

            transform: rotate(270deg);
        }

    </style>
    <script type="text/javascript">
        var items = [];
        $(document).ready(function () {
            //LoadHeaderHtml();
            //LoadHeaderStyle();


            ShowDetail();
            positionDetail();
            ShowSlideShow();
            positionSlideShow();
            //LoadDateField();

            //initSession();
            LoadCollectorDdlAutoComplete();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //LoadHeaderHtml();
                //LoadHeaderStyle();

                ShowDetail();
                positionDetail();
                ShowSlideShow();
                positionSlideShow();
                //LoadDateField();
                LoadCollectorDdlAutoComplete();
            }
        });

        function Select(obj) {
            var itemId = obj.id;
            var id = itemId.replace('img', '');
            //var id = itemId.replace('RImages_img_', '');

            //if ($('#hfSelectedImages').val() == '' || $('#hfSelectedImages').val() == ',') { items = []; } else {
            //    items = $.parseJSON($('#hfSelectedImages').val());
            //}

            //var imageNameId = '#' + 'RImages_hfImageName_' + id;
            var imageNameId = '#' + id + 'hfImageName'

            //RImages_ctl01_hfImageName'RImages_hfCreatedDate_'

            var item_id = $(imageNameId).val();
            //var imageURLId = '#' + 'RImages_hfImageUrl_' + id;
            var imageURLId = '#' + id + 'hfImageUrl'
            var item_value = $(imageURLId).val();
            //var imageNameId = '#' + 'RImages_hfName_' + id;
            var imageNameId = '#' + id + 'hfName';
            var item_name = $(imageNameId).val();
            // var imageCreatedDateId = '#' + 'RImages_hfCreatedDate_' + id;
            var imageCreatedDateId = '#' + id + 'hfCreatedDate';
            var item_created_date = $(imageCreatedDateId).val();

            selected_id = obj.id;
            process_item(item_id, item_value, item_name, item_created_date, selected_id);

            $('#hfSelectedImages').val(JSON.stringify(items));
            LoadiViewChoosed(item_value.replace(/'/g, "\\'"));
        }

        function RotateSelected() {
            //var id = 'RImages_ctl01_img';
            //$("#"+id).addClass("rotated");

            var arr_len = items.length;
            for (var x = 0; x < arr_len; x++) {
                var selected_id = items[x]['selected_id'];
                
                if ($("#" + selected_id).hasClass("rotated90")) {
                    $("#" + selected_id).removeClass("rotated90");
                    $("#" + selected_id).addClass("rotated180");
                }
                else if ($("#" + selected_id).hasClass("rotated180")) {
                    $("#" + selected_id).removeClass("rotated180");
                    $("#" + selected_id).addClass("rotated270");
                }
                else if ($("#" + selected_id).hasClass("rotated270")) {
                    $("#" + selected_id).removeClass("rotated270");
                    $("#" + selected_id).addClass("rotated360");
                }
                else {
                    $("#" + selected_id).addClass("rotated90");
                }
            }
            
        }

        function HighLightSelected() {

            //if ($('#hfSelectedImages').val() == '') { items = []; } else { items = $.parseJSON($('#hfSelectedImages').val()); }
            var arr_len = items.length;
            for (var x = 0; x < arr_len; x++) {
                var item_id = items[x]['item_id'];
                var item_value = items[x]['item_value'];
                var ItemNameId = "div[name='" + item_id.replace(/'/g, "\\'") + "']";
                $(ItemNameId).css("border", "5px solid rgb(116, 118, 120)");
            }

        }

        function LoadSelectedImage() {
            $("#dSelectedImages").empty();

            //if ($('#hfSelectedImages').val() == '') { items = []; } else { items = $.parseJSON($('#hfSelectedImages').val()); }
            var arr_len = items.length;
            var strHTML = '';
            for (var x = 0; x < arr_len; x++) {
                var item_id = items[x]['item_id'];
                var item_value = items[x]['item_value'];
                var item_name = items[x]['item_name'];
                var item_created_date = items[x]['item_created_date'];

                strHTML = strHTML + '<div class="wrapper" style="float:left;">';
                strHTML = strHTML + '<div id="name_' + item_name.replace(/ /g, "_").replace(/'/g, "\\'") + '">' + item_name + '<button type="button" class="cls_button" onclick="RemoveItem(\'' + item_id.replace(/'/g, "\\'") + '\');">Remove</button></div>';
                strHTML = strHTML + '<div id="viewer_' + item_name.replace(/ /g, "_").replace(/'/g, "\\'") + '" class="viewer" style="width:550px;"></div>';
                strHTML = strHTML + '<div id="created_date_' + item_name.replace(/ /g, "_").replace(/'/g, "\\'") + '">' + item_created_date + '</div>';
                strHTML = strHTML + '<br />';
                strHTML = strHTML + '</div>';

            }

            $("#dSelectedImages").append(strHTML);

            for (var x = 0; x < arr_len; x++) {
                var item_id = items[x]['item_id'];
                var item_value = items[x]['item_value'];
                var item_name = items[x]['item_name'];
                var item_created_date = items[x]['item_created_date'];


                var iv1 = $("#viewer_" + item_name.replace(/ /g, "_").replace(/'/g, "\\'")).iviewer({
                    src: item_value,
                    update_on_resize: true,
                    zoom_animation: true,
                    mousewheel: true,
                    onMouseMove: function (ev, coords) { },
                    onStartDrag: function (ev, coords) { }, //this image will not be dragged
                    onDrag: function (ev, coords) { }
                });


            }
        }

        function RemoveItem(strItemId) {
            delete_item(strItemId);
            $('#hfSelectedImages').val(JSON.stringify(items));
            LoadSelectedImage();
        }

        function process_item(item_id, item_value, item_name, item_created_date, selected_id) {
            if (item_value == "") {
                delete_item(item_id);
            } else if (checkIfExists(item_id)) {
                //edit(item_id, item_value);
                delete_item(item_id);
            } else if (!checkIfExists(item_id)) {
                add(item_id, item_value, item_name, item_created_date, selected_id);
            }
        }
        function add(item_id, item_value, item_name, item_created_date, selected_id) {
            items.push({
                "item_id": item_id,
                "item_value": item_value,
                "item_name": item_name,
                "item_created_date": item_created_date,
                "selected_id": selected_id
            });
        }
        function edit(item_id, item_value, item_name, item_created_date) {
            items.remove("item_id", item_id);
            items.push({
                "item_id": item_id,
                "item_value": item_value,
                "item_name": item_name,
                "item_created_date": item_created_date
            });
        }
        function delete_item(item_id) {
            var ItemNameId = "div[name='" + item_id.replace(/'/g, "\\'") + "']";
            $(ItemNameId).css("border", "1px solid rgb(116, 118, 120)");
            items.remove("item_id", item_id);
        }
        Array.prototype.remove = function (name, value) {
            array = this;
            var rest = $.grep(this, function (item) {
                return (item[name] != value);
            });

            array.length = rest.length;
            $.each(rest, function (n, obj) {
                array[n] = obj;
            });
        };
        function checkIfExists(check_item_id) {
            var arr_len = items.length;
            for (var x = 0; x < arr_len; x++) {
                var item_id = items[x]['item_id'];
                var item_value = items[x]['item_value'];
                if (check_item_id == item_id.replace(/'/g, "\\'")) {
                    return true;
                }
            }
            return false;
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            var TopHdrHeight;
            TopHdrHeight = $(window.parent.parent.parent.document).contents().find('#fraTopID').height();
            if (TopHdrHeight == null || TopHdrHeight == 0) { TopHdrHeight = 20; }
            if (navigator.appName == 'Microsoft Internet Explorer') {
                this.css("top", "35px");
                this.css("left", "30px");
            } else {
                this.css("top", "35px");
                this.css("left", "35px");
            }
            this.css("height", $(window).height());
            return this;
        }
        function ShowDetail() {
            var value = $('#hfDetailStatus').val()
            if (value == "") { $('#Detail').hide(); }
            else {
                if (value == "Show") { $('#Detail').show(); }
                else { $('#Detail').hide(); };
            };
        }
        function SetDetail(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfDetailStatus').val('Hide'); }
            else {
                var value = $('#hfDetailStatus').val();
                if (value == 'Hide') { $('#hfDetailStatus').val('Show'); }
                else { $('#hfDetailStatus').val('Hide'); }
            }
        }
        function positionDetail() {
            $('#Detail').center();
        }

        function LoadiViewChoosed(strUrl) {
            //strUrl = strUrl;
            strUrl = strUrl.replace('\\',"");
            $("dWrapperChosenViewer").empty();
            var iv1 = $("#dChosenViewer").iviewer({
                src: strUrl,
                update_on_resize: true,
                zoom_animation: true,
                mousewheel: true,
                onMouseMove: function (ev, coords) { },
                onStartDrag: function (ev, coords) { }, //this image will not be dragged
                onDrag: function (ev, coords) { }
            });
            $("#dChosenViewer").iviewer('loadImage', strUrl);
        }

        function ShowSlideShow() {
            var value = $('#hfSlideShowStatus').val()
            if (value == "") { $('#SlideShow').hide(); }
            else {
                if (value == "Show") { $('#SlideShow').show(); }
                else { $('#SlideShow').hide(); };
            };
        }
        function SetSlideShow(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfSlideShowStatus').val('Hide'); }
            else {
                var value = $('#hfSlideShowStatus').val();
                if (value == 'Hide') { $('#hfSlideShowStatus').val('Show'); }
                else { $('#hfSlideShowStatus').val('Hide'); }
            }
        }
        function positionSlideShow() {
            $('#SlideShow').center();
        }

        function LoadSlideShow() {
            $('#camera_wrap_1').unbind();
            jQuery('#camera_wrap_1').camera({
                loader: 'bar',
                pagination: true,
                thumbnails: false,
                alignment: 'center',
                autoAdvance: true,
                playPause: true
            });
            //jQuery('#camera_wrap_1').cameraStop();
        }

        //function LoadDateField() {
        //    $("#txtStartDate").datepicker({
        //        showOn: "button",
        //        buttonImage: "../images/icoCalendar.gif",
        //        buttonImageOnly: true,
        //        buttonText: "Select date",
        //        dateFormat: "yy-mm-dd",
        //        onSelect: function (selected) {
        //            var dt = new Date(selected);
        //            dt.setDate(dt.getDate() + 1);
        //            $("#txtEndDate").datepicker("option", "minDate", dt);
        //        }
        //    });
        //    $("#txtEndDate").datepicker({
        //        showOn: "button",
        //        buttonImage: "../images/icoCalendar.gif",
        //        buttonImageOnly: true,
        //        buttonText: "Select date",
        //        dateFormat: "yy-mm-dd",
        //        onSelect: function (selected) {
        //            var dt = new Date(selected);
        //            dt.setDate(dt.getDate() - 1);
        //            $("#txtStartDate").datepicker("option", "maxDate", dt);
        //        }
        //    });
        //}
         
        function ValidateDateRange() {
            var start_date = $('#txtDate_txtDateStart').val(),
                end_date = $('#txtDate_txtDateEnd').val();

            if (start_date == '' && end_date == '') {
                var todayDate = new Date();
                $('#txtDate_txtDateStart').val(todayDate);
                $('#txtDate_txtDateEnd').val(todayDate);
                start_date = new Date(todayDate);
                end_date = new Date(todayDate);
            }
            else if (start_date == '') {
                $('#txtDate_txtDateStart').val(end_date);
                start_date = $('#txtDate_txtDateStart').val();
            }
            else if (end_date == '') {
                $('#txtDate_txtDateEnd').val(start_date);
                end_date = $('#txtDate_txtDateEnd').val();
            }
            return true;
        }


        function LoadCollectorDdlAutoComplete() {
            var strCollectorCode = $("#ddlCollectorCode").val();
            $("#txtCollectorCustomerCode").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../DataServices/ws_OnlineGallery.asmx/GetOnlineGalleryCollectorCustList",
                        data: "{'strCollectorCode': '" + strCollectorCode + "','strSearch':'" + request.term + "' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    text: item.CustName,
                                    value: item.CustCode,
                                    label: item.CustName
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $("#txtCollectorCustomerCode").autocomplete("close");
                        }
                    });
                },
                minLength: 1,
                focus: function (event, ui) {
                    $('#txtCollectorCustomerCode').val(ui.item.value);
                    return false;
                },
                select: function (event, ui) {
                    $('#txtCollectorCustomerCode').val(ui.item.value);
                    return false;
                }
            });

 
        }
    </script>

</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmOnlineGallery" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />

        <div id="Header" class="MenuHeader">
        </div>

        <%--<div id="Content" style="width: 98%; padding-left: 25px; padding-top: 20px;">--%>
        <div id="Content">
            <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
            <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" title="" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="updPnlHdr" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <customToolkit:wuc_lblheader ID="lblHeader" runat="server" Title="Online Gallery" />

                    <%--<table style="width: 100%">--%>
                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td style="width: 98%; vertical-align: top">
                                <div id="dheader" class="cls_HeaderDiV">
                                    <asp:Label ID="lblSearch" runat="server" Text="Search" CssClass="cls_label_header"></asp:Label>
                                </div>
                                <asp:Panel ID="pHdr" runat="server">

                                    <table style="width: 100%; background-color: lightgrey;">

                                        <tr>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text="Filter Type" CssClass="cls_label"></asp:Label></td>
                                            <td class="auto-style2">
                                                <asp:DropDownList ID="ddlFilterType" runat="server" AutoPostBack="true" CssClass="cls_dropdownlist" Style="height: 15px; width: 150px;">
                                                    <asp:ListItem Value="0" Text="Market Survey"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Field Activity"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Sales Order"></asp:ListItem>
                                                     <asp:ListItem Value="3" Text="Collection"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="TRA Order"></asp:ListItem>
                                                </asp:DropDownList></td>
                                            <%--<td> <asp:Label ID="Label2" runat="server" Text="Customer" CssClass="cls_label"></asp:Label></td>--%>
                                            <td><%--<asp:TextBox ID="TextBox2" runat="server" CssClass="cls_textbox"   Style="height: 19px; max-width: 100%;"></asp:TextBox>--%></td>

                                        </tr>
                                        <tr id="PrdCust" runat="server"  visible="false">
                                            <td>
                                                <asp:Label ID="lblProduct" runat="server" Text="Product" CssClass="cls_label"></asp:Label></td>
                                            <td id="MSS_Prd" runat="server" class="auto-style2">
                                                <asp:TextBox ID="txtProduct" runat="server" CssClass="cls_textbox" Style="height: 12px; width: 150px;"></asp:TextBox></td>
                                            <td id="SFMS_Cat" runat="server" visible="false">
                                                <asp:DropDownList ID="ddlSFMSCat" AutoPostBack="true" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;">
                                                    <asp:ListItem Value="MSS" Text="- Select -"></asp:ListItem>
                                                </asp:DropDownList></td>
                                             <td id="SO_PO_NO" runat="server" class="auto-style2"  visible="false">
                                                <asp:TextBox ID="txtPONO" runat="server" CssClass="cls_textbox" Style="height: 12px; width: 150px;"></asp:TextBox></td>
                                            <td>
                                                <asp:Label ID="lblCustomer" runat="server" Text="Customer" CssClass="cls_label"></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtCustomer" runat="server" CssClass="cls_textbox" Style="height: 12px; width: 150px;"></asp:TextBox></td>
                                        </tr>
                                        <tr id="ActSr" runat="server" visible="false" >
                                            <td>
                                                <asp:Label ID="lblActivity" runat="server" Text="Activity" CssClass="cls_label"></asp:Label></td>
                                            <td id="MSS_SFMS" runat="server" class="auto-style2">
                                                <asp:DropDownList ID="ddlActivity" AutoPostBack="true" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;"></asp:DropDownList></td>
                                           <td id="SO_SO_NO" runat="server" class="auto-style2"  visible="false">
                                                <asp:TextBox ID="txtSONO" runat="server" CssClass="cls_textbox" Style="height: 12px; width: 150px;"></asp:TextBox></td>
                                             <td>
                                                <asp:Label ID="lblSalesrep" runat="server" Text="Salesrep" CssClass="cls_label"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlSalesrep" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;"></asp:DropDownList></td>
                                        </tr>

                                        <tr id="MSS_Ques" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="MSS Ques" CssClass="cls_label"></asp:Label></td>
                                            <td class="auto-style2">
                                                <asp:DropDownList ID="ddlMSSQues" AutoPostBack="true" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;">
                                                    <asp:ListItem Value="selected" Text="- Select -"></asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="MSS Sub Ques" CssClass="cls_label"></asp:Label></td>
                                            <td>
                                                <asp:DropDownList ID="ddlMSSSubQues" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;">
                                                    <asp:ListItem Value="selected" Text="- Select -"></asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr id="SO" runat="server" visible="false">
                                            <td>
                                                <asp:Label ID="lblChannel" runat="server" Text="Channel" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td>
                                                 <asp:DropDownList ID="ddlChannel" AutoPostBack="false" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;">
                                                    <asp:ListItem Value="selected" Text="- Select -"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr id="Collection" runat="server" visible="false">
                                            <td><asp:Label ID="lblCollectionNo" runat="server" Text="Collection No" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td><asp:TextBox ID="txtCollectionNo" runat="server" CssClass="cls_textbox" Style="height: 12px; width: 150px;"></asp:TextBox>
                                                
                                            </td>
                                           <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr id="Collector" runat="server" visible="false">
                                             <td><asp:Label ID="lblCollector" runat="server" Text="Collector" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCollectorCode" AutoPostBack="true" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;" OnSelectedIndexChanged="ddlCollectorCode_SelectedIndexChanged">
                                                    <asp:ListItem Value="selected" Text="- Select -"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                             <td><asp:Label ID="lblCollectionCustomer" runat="server" Text="Customer" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td>
                                                <div style="display:none;">
                                                <asp:DropDownList ID="ddlCollectorCustomerCode" AutoPostBack="false" runat="server" CssClass="cls_dropdownlist" Style="height: 19px; width: 150px;"  >
                                                    <asp:ListItem Value="selected" Text="- Select -"></asp:ListItem>
                                                </asp:DropDownList>
                                                </div>
                                                <asp:TextBox ID="txtCollectorCustomerCode" runat="server"  CssClass="cls_textbox" Style="height: 12px; width: 150px;"></asp:TextBox>
                                            </td>
                                             
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDateRange" runat="server" Text="Date Range" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td class="auto-style2">
                                                <customToolkit:wuc_txtCalendarRange
                                                    ID="txtDate"
                                                    runat="server"
                                                    RequiredValidation="true"
                                                    RequiredValidationGroup="Save"
                                                    DateFormatString="yyyy-MM-dd"
                                                    CompareDateRangeValidation="true" />
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" OnClientClick="return ValidateDateRange();" /></td>
                                            <td class="auto-style2"></td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>
                                <hr>

                                <asp:Panel ID="pDtl" runat="server">
                                    <div id="divData">

                                        <asp:UpdatePanel ID="UpdPnlImages" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                <div id="search">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <div id="ddtl" class="cls_HeaderDiV">
                                                                <asp:Label ID="lblResult" runat="server" Text="Results" CssClass="cls_label_header"></asp:Label>

                                                            </div>
                                                            <td>
                                                                <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="DownloadFiles" CssClass="cls_button" />
                                                                <asp:Button ID="btnExport" Text="Export Excel" OnClick="ExportExcel" runat="server" CssClass="cls_button" Style="display: none;" />
                                                                <asp:Button ID="btnExportWord" Text="Export Word" OnClick="ExportWord" runat="server" CssClass="cls_button" Style="display: none;" />
                                                                <asp:Button ID="btnExportExcel" Text="Export Excel" OnClick="btnExportExcel_Click" runat="server" CssClass="cls_button" />
                                                                <asp:Button ID="btnViewSelected" Text="View Selected" OnClientClick="SetDetail('Show');ShowDetail(); return false;" runat="server" CssClass="cls_button" />
                                                                <asp:Button ID="btnViewSlideShow" Text="View SlideShow" OnClientClick="SetSlideShow('Show');ShowSlideShow(); return false;" runat="server" CssClass="cls_button" />

                                                                <%--<asp:Button ID="btnRotate" runat="server" Text="Rotate" CssClass="cls_button" />--%>
                                                                <asp:HiddenField ID="hfSelectedImages" runat="server" />
                                                                <input id="btnRotate1" type="button" onclick="RotateSelected();" class="cls_button" value="Rotate"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="container" style="margin: 0px; padding: 0px; width: 100%; max-width: none;">
                                                                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="False"></customToolkit:wuc_dgpaging>
                                                                    <br />
                                                                    <br />
                                                                    <asp:Repeater ID="RImages" runat="server">
                                                                        <HeaderTemplate>
                                                                            <div id="dWrapperChosenViewer" class="wrapper" style="width: 380px; height: 420px; float: left; vertical-align: middle; margin: 10px;">
                                                                                <div id="dChosenViewer" class="smallerviewer">
                                                                                </div>
                                                                            </div>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <div style="width: 180px; height: 280px; float: left; text-align: center; vertical-align: middle; margin: 10px;">
                                                                                <asp:Panel ID="pnlName" runat="server" Style="border: solid 1px #747678;">
                                                                                    <asp:HiddenField ID="hfName" runat="server" Value='<%# Eval("NAME")%>' />
                                                                                    <asp:HiddenField ID="hfCreatedDate" runat="server" Value='<%# Eval("CREATED_DATE")%>' />
                                                                                    <asp:HiddenField ID="hfImageName" runat="server" Value='<%# Eval("IMAGE_NAME")%>' />
                                                                                    <asp:HiddenField ID="hfImageUrl" runat="server" Value='<%# Eval("IMAGE_URL")%>' />
                                                                                    <div style="width: 100%;">
                                                                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("NAME")%>' CssClass="cls_label"></asp:Label>
                                                                                    </div>
                                                                                    <%--<div id="dvImg" style="width: 100%; height: 150px;" >--%>
                                                                                    <div style="width:100%;">
                                                                                        <asp:ImageButton ID="img" runat="server" Height="150px" Width="150px"/>
                                                                                    </div>
                                                                                    <div style="width: 100%;">
                                                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("CREATED_DATE")%>' CssClass="cls_label"></asp:Label>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnDownload" />
                                                <asp:PostBackTrigger ControlID="btnExport" />
                                                <asp:PostBackTrigger ControlID="btnExportWord" />
                                                <asp:PostBackTrigger ControlID="btnExportExcel" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>


                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UpdateDetail" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="hfDetailStatus" runat="server" Value="Hide" />
                    <div id="Detail" style="width: 97%; float: right; position: absolute; display: none; z-index: 9999;">
                        <div class="shadow">
                            <div id="DetailContentTop" class="content" style="width: 100%;">
                                <div id="DetailPnl" style="width: 100%; padding: 0px 0px 0px 0px;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="text-align: left;">
                                                <customToolkit:wuc_lblheader ID="Wuc_lblHeaderPopUp" runat="server" Title="Online gallery - Compare" />
                                            </td>
                                            <td style="text-align: right;">
                                                <img alt="Close" src="../images/ico_close.gif" onclick="SetDetail('ForceHide');ShowDetail();"
                                                    style="height: 25px; width: 25px; cursor: pointer;" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="ContainerDivBlue">
                                    <div id="DetailContent" style="width: 100%;">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <div style="width: 100%" id="dSelectedImages">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                    <asp:HiddenField ID="hfDetail" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdateSlideShow" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="hfSlideShowStatus" runat="server" Value="Hide" />
                    <div id="SlideShow" style="width: 800px; float: right; position: absolute; display: none; z-index: 9999;">
                        <div class="shadow">
                            <div id="SlideShowContentTop" class="content" style="width: 100%;">
                                <div id="SlideShowPnl" style="width: 100%; padding: 0px 0px 0px 0px;">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="text-align: left;">
                                                <customToolkit:wuc_lblheader ID="Wuc_lblHeader1" runat="server" Title="Online gallery - Slide Show" />
                                            </td>
                                            <td style="text-align: right;">
                                                <img alt="Close" src="../images/ico_close.gif" onclick="SetSlideShow('ForceHide');ShowSlideShow(); jQuery('#camera_wrap_1').cameraPause();"
                                                    style="height: 25px; width: 25px; cursor: pointer;" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="ContainerDivBlue">
                                    <div id="SlideShowContent" style="width: 100%;">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
                                                        <asp:Repeater ID="rSlideShow" runat="server">
                                                            <HeaderTemplate>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <div style="width: 100%" data-thumb='<%# Eval("IMAGE_URL")%>' data-src='<%# Eval("IMAGE_URL")%>'>
                                                                    <div class="camera_caption fadeFromBottom">
                                                                        <%# Eval("NAME")%>
                                                                    </div>
                                                                </div>

                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>

                                    </div>
                                    <asp:HiddenField ID="hfSlideShow" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>



