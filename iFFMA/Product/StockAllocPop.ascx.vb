Imports System.Data

Partial Class iFFMA_Product_StockAllocPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty
    Dim strPrdCode As String
    Dim strUomCode As String
    Dim strstardate As String


#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property UomCode() As String
        Get
            Return Trim(hdUOMCode.Value)
        End Get
        Set(ByVal value As String)
            hdUOMCode.Value = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return Trim(hdStartDate.Value)
        End Get
        Set(ByVal value As String)
            hdStartDate.Value = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            If ddlUOM IsNot Nothing Then
                dtUOM = clsCommon.GetUOMDDL
                With ddlUOM
                    .Items.Clear()
                    .DataSource = dtUOM.DefaultView
                    .DataTextField = "UOM_NAME"
                    .DataValueField = "UOM_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("UOM_CODE")), "", DT.Rows(0)("UOM_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ResetPage()
        'StockAllocID = ""
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsStockAlloc As New mst_Product.clsStockAlloc

            DT = clsStockAlloc.GetStockAllocDetails(PrdCode, SalesrepCode, UomCode, StartDate)
            If DT.Rows.Count > 0 Then
                txtDate.DateStart = DT.Rows(0)("START_DATE")
                txtDate.DateEnd = DT.Rows(0)("END_DATE")

                lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("START_DATE")), "", DT.Rows(0)("START_DATE")))
                lblEndDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("END_DATE")), "", DT.Rows(0)("END_DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
            RaiseEvent CloseButton_Click(Me, Nothing)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKALLOC, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdSearch)
                .ResetPage()
                .TeamCode = TeamCode
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click
        Try
            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            txtPrdCode.Text = wuc_PrdSearch.PrdCode

            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Page.Validate()
            If Page.IsValid = False Then
                Show()
                Exit Sub
            End If

            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            Dim txtAllocQty As TextBox = CType(DetailsView1.FindControl("txtAllocQty"), TextBox)
            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            Dim isDuplicate As Integer = 0

            txtAllocQty.Text = IIf(Trim(txtAllocQty.Text) = "", 0, Trim(txtAllocQty.Text))

            Dim clsStockAlloc As New mst_Product.clsStockAlloc
            Dim DT As DataTable

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then

                DT = clsStockAlloc.UpdateStockAlloc(PrdCode, SalesrepCode, UomCode, StartDate, Trim(ddlUOM.SelectedValue), Trim(txtAllocQty.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd))

                'If DT.Rows.Count > 0 Then
                '    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                'End If
                If DT Is Nothing Then
                    isDuplicate = 1
                Else
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Stock allocation for this product with the same date range already exists!"
                    LoadDvEditMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully saved."
                    LoadDvViewMode()
                End If
            Else
                DT = clsStockAlloc.CreateStockAlloc(Trim(txtPrdCode.Text), SalesrepCode, Trim(ddlUOM.SelectedValue), Trim(txtAllocQty.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd))

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Stock allocation for this product with the same date range already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully created."
                    PrdCode = txtPrdCode.Text.ToString
                    LoadDvViewMode()
                End If
            End If

            UomCode = ddlUOM.SelectedValue.ToString
            StartDate = txtDate.DateStart
            BindDetailsView()                       'Rebind the details view

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        ResetPage()
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub txtDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.Load
    '    If StockAllocID <> "" Then
    '        Show()
    '    End If
    'End Sub

    Protected Sub imgCal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.imgCal_Click
        Show()
    End Sub
End Class
