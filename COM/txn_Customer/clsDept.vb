'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	27/10/2006
'	Purpose	    :	Class to build datatables for department and contact listings.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsDept
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDept"
        End Get
    End Property

    'get the departments 
    Function getDeptDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERDEPTLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("shiptoacct", strCustCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDeptDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    'get Contact name based on departments
    Function getContactDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strContactCode As String) As DataTable
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERCONTACTLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)
                Return .spRetrieve
            End With


        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Function

    'get customer name based on cust code
    Function getContactName(ByVal strCont As String) As String
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Dim strCustName As String = ""

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERNAME"
                .addItem("custcode", strCont, 2)
                dt = .spRetrieve
            End With

            If dt.Rows.Count > 0 Then
                strCustName = Trim(dt.Rows(0)("cust_name"))
            End If

            Return strCustName

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactName :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class

