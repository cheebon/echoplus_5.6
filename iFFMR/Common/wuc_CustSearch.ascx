<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_CustSearch.ascx.vb" Inherits="iFFMR_Common_wuc_CustSearch" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkitPaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Assembly="cor_CustomCtrl" Namespace="cor_CustomCtrl" TagPrefix="ccGV" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel DefaultButton="btnSearch" ID="pnlMsgPop" runat="server" Style="display: none; width: 600px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:95%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:left; width:5%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%; padding-bottom: 5px;">
                    <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Customer Code</span>
                    <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                    <asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox" />
                    <br />
                    <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Customer Name</span>
                    <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                    <asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" />
                     <br />
                    <span style="float:left; padding-top: 2px; padding-bottom: 2px;"><asp:Button ID="btnSearch" runat="server" CssClass="cls_button" Text="Search" /></span>
                </fieldset>
                                
                <fieldset style="padding-left: 10px; width: 100%;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                    <customToolkitPaging:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="300px" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="CUST_CODE,CUST_NAME"
                        >
                    </ccGV:clsGridView>
                    
                    <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
                     <asp:HiddenField ID="hdCustName" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            PopupControlID="pnlMsgPop" 
            CancelControlID="imgClose"
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>