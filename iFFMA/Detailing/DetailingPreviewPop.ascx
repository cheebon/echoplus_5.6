<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DetailingPreviewPop.ascx.vb" Inherits="iFFMA_Detailing_DetailingPreviewPop" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 300px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:90%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:right; width:10%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                
                <fieldset style="padding-left: 10px; width: 100%; padding-top: 5px; padding-bottom: 5px;">
                    <span class="cls_label_header">Category :</span>
                    <asp:DropDownList ID="ddlCat" runat="server" CssClass="cls_dropdownlist" />
                </fieldset>
                
                <fieldset style="padding-left:10px; width:100%; height:180px; padding-bottom:5px;">
                    <span style="float:left; width:90%; padding-top: 2px; padding-bottom: 2px;">
                        <span class="cls_label_header">Activities :</span><br />
                        <asp:ListBox ID="lsbActy" CssClass="cls_listbox" runat="server" Height="150px" ForeColor="Black" Width="80%"></asp:ListBox>
                    </span>
                </fieldset>
                              
                <asp:HiddenField ID="hdDetCode" runat="server" Value="" />
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>