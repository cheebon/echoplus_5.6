﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SFMSEnquiry.aspx.vb" Inherits="iFFMR_Customize_SFMSEnquiry" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SFMS Enquiry</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="margin: 0; border: 0; padding: 0;" class="BckgroundInsideContentLayout">
    <span id="ContentBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="ContentBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../iFFMR/Customize/Geomap/SFMSEnquiry/SFMSEnquirySearch.aspx"
            width="100%" height="0" scrolling="auto" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span><span id="DetailBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="DetailBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" scrolling="auto" height="0" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span><span id="MapBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="MapBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" scrolling="auto" height="0" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span>
</body>
</html>
