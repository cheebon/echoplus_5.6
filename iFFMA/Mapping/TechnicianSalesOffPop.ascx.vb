Imports System.Data

Partial Class iFFMA_Mapping_TechnicianSalesOffPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "PROPERTY"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TechnicianCode() As String
        Get
            Return Trim(hdTechnicianCode.Value)
        End Get
        Set(ByVal value As String)
            hdTechnicianCode.Value = value
        End Set
    End Property

    Public Property SalesOff() As String
        Get
            Return Trim(hdSalesOff.Value)
        End Get
        Set(ByVal value As String)
            hdSalesOff.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtTechnician, dtSalesOff As DataTable
        Dim clsDDL As New mst_Common.clsDDL
        Dim strTechnicianCode As String

        strTechnicianCode = TechnicianCode

        Try
            Dim ddlTechnician As DropDownList = CType(DetailsView1.FindControl("ddlTechnician"), DropDownList)
            If ddlTechnician IsNot Nothing Then
                dtTechnician = clsDDL.GetTechnicianDDL
                With ddlTechnician
                    .Items.Clear()
                    .DataSource = dtTechnician.DefaultView
                    .DataTextField = "TECHNICIAN_NAME"
                    .DataValueField = "TECHNICIAN_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("TECHNICIAN_CODE")), "", DT.Rows(0)("TECHNICIAN_CODE"))
                End With

                strTechnicianCode = ddlTechnician.SelectedValue
            End If

            Dim ddlSalesOff As DropDownList = CType(DetailsView1.FindControl("ddlSalesOff"), DropDownList)
            If ddlSalesOff IsNot Nothing Then
                dtSalesOff = clsDDL.GetSalesOffDDL
                With ddlSalesOff
                    .Items.Clear()
                    .DataSource = dtSalesOff.DefaultView
                    .DataTextField = "SALESOFF_NAME"
                    .DataValueField = "SALESOFF_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("SALES_OFF")), "", DT.Rows(0)("SALES_OFF"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsTechnicianSalesOff As New mst_Mapping.clsTechnicianSalesOff

            DT = clsTechnicianSalesOff.GetTechnicianSalesOffDetails(TechnicianCode, SalesOff)
            If DT.Rows.Count > 0 Then

            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
            RaiseEvent CloseButton_Click(Page, Nothing)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ddlTechnician As DropDownList = CType(DetailsView1.FindControl("ddlTechnician"), DropDownList)
            Dim ddlSalesOff As DropDownList = CType(DetailsView1.FindControl("ddlSalesOff"), DropDownList)

            Dim clsTechnicianSalesOff As New mst_Mapping.clsTechnicianSalesOff
            Dim DT As DataTable
            Dim isDuplicate As Integer = 0

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                DT = clsTechnicianSalesOff.UpdateTechnicianSalesOff(TechnicianCode, SalesOff, Trim(ddlSalesOff.SelectedValue))

                If DT Is Nothing Then
                    isDuplicate = 1
                Else
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This mapping already exists!"
                    LoadDvEditMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully saved."
                    LoadDvViewMode()
                End If
            Else
                DT = clsTechnicianSalesOff.CreateTechnicianSalesOff(Trim(ddlTechnician.SelectedValue), Trim(ddlSalesOff.SelectedValue))

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This mapping already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully created."
                    TechnicianCode = ddlTechnician.SelectedValue
                    LoadDvViewMode()
                End If
            End If

            SalesOff = ddlSalesOff.SelectedValue
            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
