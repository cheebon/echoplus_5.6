'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	14/04/2007
'	Purpose	    :	ConnectionString Details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Public Class Admin_ConnectionString_ConnectionStringDtl
    Inherits System.Web.UI.Page

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_Comm_Svr_Setting.clsCommSvrQuery
        Dim strUserCode, strUserID As String
        Dim dt As DataTable

        Try
            lblErr.Text = ""
            strUserID = ""

            If Not IsPostBack Then
                'constant

                'request
                strUserCode = Trim(Request.QueryString("comm_svr_setting_code"))
                strUserID = Trim(Request.QueryString("comm_svr_setting_id"))

                'Call Header
                With wuc_lblheader
                    .Title = "Connection Details" 'strTitle
                    .DataBind()
                    .Visible = True
                End With

                'strUserID will be empty if redirected from create user
                If strUserID = "" Then
                    objUserQuery = New adm_Comm_Svr_Setting.clsCommSvrQuery
                    With objUserQuery
                        .clsProperties.strCommCode = strUserCode
                        dt = .GetPwd
                    End With

                    If dt.Rows.Count > 0 Then
                        strUserID = dt.Rows(0).Item("comm_svr_setting_id")
                    Else
                        Response.Redirect("ConnectionStringList.aspx", False)
                    End If
                End If

                objUserQuery = New adm_Comm_Svr_Setting.clsCommSvrQuery
                With objUserQuery
                    .clsProperties.strCommID = strUserID
                    dt = .GetCommSvrDetail
                End With

                If dt.Rows.Count > 0 Then
                    lblStatusValue.Text = IIf(Trim(dt.Rows(0)("active_flag")) = "1", "Active", "In-Active")
                    lblCommIDValue.Text = Trim(dt.Rows(0)("comm_svr_setting_id"))
                    lblCommCodeValue.Text = Trim(dt.Rows(0)("comm_svr_setting_code"))
                    lblCommNameValue.Text = Trim(dt.Rows(0)("comm_svr_setting_name"))
                    lblServerNameValue.Text = Trim(dt.Rows(0)("server_name"))
                    lblServerIPValue.Text = Trim(dt.Rows(0)("server_ip"))
                    lblDBIPValue.Text = Trim(dt.Rows(0)("db_ip"))
                    If IsDBNull(dt.Rows(0)("db_port")) Then
                        lblDBPortValue.Text = ""
                    Else
                        lblDBPortValue.Text = Trim(dt.Rows(0)("db_port"))
                    End If
                    lblDBNameValue.Text = Trim(dt.Rows(0)("db_name"))
                    If IsDBNull(dt.Rows(0)("link_server_name")) Then
                        lblLinkServerNameValue.Text = ""
                    Else
                        lblLinkServerNameValue.Text = Trim(dt.Rows(0)("link_server_name"))
                    End If
                    lblDBUserIDValue.Text = Trim(dt.Rows(0)("db_user_id"))
                    lblURLValue.Text = Trim(dt.Rows(0)("url"))
                    If IsDBNull(dt.Rows(0)("proxy")) Then
                        lblProxyValue.Text = ""
                    Else
                        lblProxyValue.Text = Trim(dt.Rows(0)("proxy"))
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionMsg("ConnectionStringDtl.Page_Load : " & ex.ToString)
        Finally
            objUserQuery = Nothing
        End Try
    End Sub

#Region "Button operation"
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnEdit_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Response.Redirect("ConnectionStringEdit.aspx?comm_svr_setting_id=" & Trim(lblCommIDValue.Text), False)
        Catch ex As Exception
            ExceptionMsg("ConnectionStringDtl.btnEdit_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnClose_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("ConnectionStringList.aspx", False)
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnDelete_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objUser As adm_Comm_Svr_Setting.clsCommSvr
        Try
            'If Trim(lblStatusValue.Text) = "Active" Then
            '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Please set status to In-Active before proceed.');</script>")
            '    Exit Sub
            'End If
            objUser = New adm_Comm_Svr_Setting.clsCommSvr
            With objUser
                .clsProperties.strCommID = Trim(lblCommIDValue.Text)
                .clsProperties.strSyncOperation = "D"
                .clsProperties.strActiveFlag = "0"
                .Suspend()
            End With

            Response.Redirect("ConnectionStringList.aspx", False)

        Catch ex As Exception
            ExceptionMsg("ConnectionStringDtl.btnDelete_Click : " & ex.ToString)
        Finally
            objUser = Nothing
        End Try
    End Sub
#End Region

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
