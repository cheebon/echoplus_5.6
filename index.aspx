<%@ Page Language="vb" AutoEventWireup="false" Inherits="index" CodeFile="index.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="include/wuc_lblheader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>index</title>
    <link rel="stylesheet" href='include/DKSH.css' />
    <script type="text/javascript">
        function Integration(strDestination) {
            if (strDestination == 'FFMR') {
                popup('Admin/Integration/ffmr.aspx')
            }
            else {
                if (strDestination == 'FFMA') {
                    popup('Admin/Integration/ffma.aspx')
                }
                else {
                    popup('Admin/Integration/ffms.aspx')
                }

            }
        }

        function Integration6(strDestination) {
            if (strDestination == 'FFMR') {
                popup('Admin/Integration/ffmr6.aspx')
            }
            else {
                if (strDestination == 'FFMA') {
                    popup('Admin/Integration/ffma6.aspx')
                }
                else {
                    popup('Admin/Integration/ffms6.aspx')
                }

            }
        }

        function popup(url) {
            params = 'location=0,menubar=0,scrollbars=yes,resizable=yes,status=1,left=0,top=0,maximize=1,width=' + screen.width + ',height=' + screen.height;
            newwin = window.open(url, 'Echoplus', params);
            if (window.focus) { newwin.focus() }
            return false;
        }
       
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmIndex" method="post" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr align="center">
                            <td valign="top" style="width: 100%" class="GridNormal" align="left">
                                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                <uc1:wuc_lblheader ID="wuc_lblHeader" runat="server"></uc1:wuc_lblheader>
                                <div id="wrapper">
                                    <div id="header" class="divheader">
                                    </div>
                                    <div id="content">
                                        <div id="wrapper1" class="divwrapper1">
                                            <%  If GetAccessRight(4) Then%>
                                            <div id="col1" class="divcol">
                                                <div class="GridHeader" style="vertical-align: middle; width: 100%">
                                                    REPORTS</div>
                                                <%  End If%>

                                                <%  If GetAccessRight(4) Then%>
                                                <div class="divContent">
                                                    <%  If GetAccessRight(4, 5, "'1','8'") Or GetAccessRight(4, 63, "'1','8'") Or GetAccessRight(4, 184, "'1','8'") Then%>
                                                    <asp:Label ID="lblDRC" runat="server" CssClass="cls_label_header">INVENTORY FORECASTING</asp:Label><br />
                                                    <%  If GetAccessRight(4, 5, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDRC" runat="server" Text="Inventory Forecasting Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 63, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDRCEnq" runat="server" Text="Inventory Forecasting Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 184, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDRCExtract" runat="server" Text="Inventory Forecasting Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Or GetAccessRight(4, 73, "'1','8'") Or GetAccessRight(4, 120, "'1','8'") Or GetAccessRight(4, 72, "'1','8'") Then%>
                                                    <br />
                                                    <asp:Label ID="lblSales" runat="server" CssClass="cls_label_header">SALES</asp:Label><br />
                                                    <%  If GetAccessRight(4, 7, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesList" runat="server" Text="Sales Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 73, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesByKeyPrd" runat="server" Text="Key Product Sales"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 72, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesTop80CustByRep" runat="server" Text="Top 80% Customer Sales"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 120, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesByCustCount" runat="server" Text="Sales By Customer Count"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 8, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesEnq" runat="server" Text="Sales Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 58, "'1','8'") Or _
                                                          GetAccessRight(4, 59, "'1','8'") Or GetAccessRight(4, 61, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
                                                          GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Or GetAccessRight(4, 12, "'1','8'") Or _
                                                          GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 62, "'1','8'") Or GetAccessRight(4, 14, "'1','8'") Or _
                                                          GetAccessRight(4, 71, "'1','8'") Or GetAccessRight(4, 311, "'1','8'") Or GetAccessRight(4, 319, "'1','8'") Or _
                                                          GetAccessRight(4, 328, "'1','8'") Then%>
                                                    <br />
                                                    <asp:Label ID="lblSFE" runat="server" CssClass="cls_label_header">SALES FORCE EFFECTIVENESS</asp:Label><br />
                                                    <%  If GetAccessRight(4, 10, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCAByMonth" runat="server" Text="Monthly Call Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 9, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCAByDay" runat="server" Text="Daily Call Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 61, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCAByStrike" runat="server" Text="Strike Call Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 58, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCAByCont" runat="server" Text="Actual Call Coverage"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 35, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallCoverage1" runat="server" Text="Effective Call Coverage"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 313, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallCoverage1Adv" runat="server" Text="Effective Call Coverage (Adv.)"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 36, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallCoverage2" runat="server" Text="Hit Call Coverage"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 59, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCABySpec" runat="server" Text="Call Analysis By Specialty"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 12, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallProductivity" runat="server" Text="Monthly Field Force Activity"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 62, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSPRFM" runat="server" Text="Field Force Activity by Class"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 14, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSActy" runat="server" Text="Activity List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 13, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkPreplan" runat="server" Text="Preplan Call"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 25, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallEnq" runat="server" Text="Call Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 311, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDetCoverageByProd" runat="server" Text="Coverage by Specialty by Product"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 319, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDetCallByArea" runat="server" Text="Call by Area by Product"></asp:LinkButton><br />
                                                    <%  End If%>
                                                     <%  If GetAccessRight(4, 328, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallByClassByProduct" runat="server" Text="Coverage and Call By Class By Product"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Or GetAccessRight(4, 40, "'1','8'") Or GetAccessRight(4, 315, "'1','8'") Then%>
                                                    <br />
                                                    <asp:Label ID="lblKPI" runat="server" CssClass="cls_label_header">KEY PERFORMANCE INDEX</asp:Label><br />
                                                    <%  If GetAccessRight(4, 38, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMonthlyOverview" runat="server" Text="Monthly KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 71, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustStatus" runat="server" Text="Customer KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 40, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkActSales" runat="server" Text="Field Force KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 37, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkRelativePerformance" runat="server" Text="Relative KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 37, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkContStatus" runat="server" Text="Contact KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 312, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFEKPI" runat="server" Text="SFE KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 4, "'1','8'") Or GetAccessRight(4, 60, "'1','8'") Or GetAccessRight(4, 118, "'1','8'") Then%>
                                                    <br />
                                                    <asp:Label ID="lblComm" runat="server" CssClass="cls_label_header">COMMUNICATION</asp:Label><br />
                                                    <%  If GetAccessRight(4, 4, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkPDAComm" runat="server" Text="Refresh & Upload"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 60, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkUplInfo" runat="server" Text="Transaction Upload"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 118, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkAuditLog" runat="server" Text="Audit Log"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or _
                                                        GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Or GetAccessRight(4, 123, "'1','8'") Or _
                                                        GetAccessRight(4, 247, "'1','8'") Or GetAccessRight(4, 258, "'1','8'") Or GetAccessRight(4, 220, "'1','8'") Or _
                                                        GetAccessRight(4, 288, "'1','8'") Or GetAccessRight(4, 300, "'1','8'") Or GetAccessRight(4, 301, "'1','8'") Or _
                                                        GetAccessRight(4, 302, "'1','8'") Or GetAccessRight(4, 303, "'1','8'") Then%>
                                                    <br />
                                                    <asp:Label ID="lblExtract" runat="server" CssClass="cls_label_header">EXTRACTION</asp:Label><br />
                                                    <%  If GetAccessRight(4, 31, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustomerExt" runat="server" Text="Customer"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 32, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDRCExt" runat="server" Text="Inventory Forecasting"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 33, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMSSExt" runat="server" Text="MSS"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 34, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSOExt" runat="server" Text="Sales Order"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 6, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSExt" runat="server" Text="SFMS"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 123, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSExtraction" runat="server" Text="Field Activities Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 220, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSExtraction2" runat="server" Text="Field Activities Extraction 2"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 247, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkPrePlanExtract" runat="server" Text=" Preplan Activities Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 258, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMSSExtraction" runat="server" Text="MSS Sampling Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 288, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSExtractionExtraCat" runat="server" Text="Field Activities Extraction With Extra Cat"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 300, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSExtract" runat="server" Text="Field Activities Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 301, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkVisitExtract" runat="server" Text="Visit Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 302, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustContExtract" runat="server" Text="Customer Contact Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 303, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustContPrdExtract" runat="server" Text="Customer Contact Product Extraction"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 64, "'1','8'") Or GetAccessRight(4, 68, "'1','8'") Or _
                                                  GetAccessRight(4, 69, "'1','8'") Or GetAccessRight(4, 70, "'1','8'") Or _
                                                  GetAccessRight(4, 103, "'1','8'") Or GetAccessRight(4, 104, "'1','8'") Or _
                                                  GetAccessRight(4, 105, "'1','8'") Or GetAccessRight(4, 124, "'1','8'") Or _
                                                  GetAccessRight(4, 125, "'1','8'") Or GetAccessRight(4, 165, "'1','8'") Or _
                                                  GetAccessRight(4, 166, "'1','8'") Or GetAccessRight(4, 168, "'1','8'") Or _
                                                  GetAccessRight(4, 169, "'1','8'") Or GetAccessRight(4, 170, "'1','8'") Or _
                                                  GetAccessRight(4, 175, "'1','8'") Or GetAccessRight(4, 182, "'1','8'") Or _
                                                  GetAccessRight(4, 186, "'1','8'") Or GetAccessRight(4, 193, "'1','8'") Or _
                                                  GetAccessRight(4, 196, "'1','8'") Or GetAccessRight(4, 199, "'1','8'") Or _
                                                  GetAccessRight(4, 200, "'1','8'") Or GetAccessRight(4, 201, "'1','8'") Or _
                                                  GetAccessRight(4, 202, "'1','8'") Or GetAccessRight(4, 214, "'1','8'") Or _
                                                  GetAccessRight(4, 215, "'1','8'") Or GetAccessRight(4, 216, "'1','8'") Or _
                                                  GetAccessRight(4, 217, "'1','8'") Or GetAccessRight(4, 223, "'1','8'") Or _
                                                  GetAccessRight(4, 224, "'1','8'") Or GetAccessRight(4, 229, "'1','8'") Or _
                                                   GetAccessRight(4, 230, "'1','8'") Or GetAccessRight(4, 234, "'1','8'") Or _
                                                   GetAccessRight(4, 235, "'1','8'") Or GetAccessRight(4, 236, "'1','8'") Or _
                                                   GetAccessRight(4, 237, "'1','8'") Or GetAccessRight(4, 238, "'1','8'") Or _
                                                    GetAccessRight(4, 241, "'1','8'") Or GetAccessRight(4, 243, "'1','8'") Or _
                                                    GetAccessRight(4, 244, "'1','8'") Or GetAccessRight(4, 245, "'1','8'") Or _
                                                    GetAccessRight(4, 246, "'1','8'") Or GetAccessRight(4, 249, "'1','8'") Or _
                                                     GetAccessRight(4, 250, "'1','8'") Or GetAccessRight(4, 251, "'1','8'") Or _
                                                     GetAccessRight(4, 289, "'1','8'") Or _
                                                     GetAccessRight(4, SubModuleType.CUZMSSENQUIRY, "'1','8'") Or _
                                                     GetAccessRight(4, SubModuleType.CUZSFMSENQUIRY, "'1','8'") Or _
                                                     GetAccessRight(4, SubModuleType.CUSTPROFILEENQ, "'1','8'") Or _
                                                     GetAccessRight(4, SubModuleType.SALES_BY_CHAIN, "'1','8'") Or _
                                                      GetAccessRight(4, SubModuleType.MISSCALLANALYSIS, "'1','8'") Or _
                                                      GetAccessRight(4, SubModuleType.REMAIN_CALL, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_BIMONTH, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUARTER, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUADMONTH, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.FIELDFORCEMTHACTYPrd, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.TRAREPORT_EDI, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.MUSTSELL, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.ONLINEGALLERY, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.MERCHANDISING, "'1','8'") Or _
                                                        GetAccessRight(4, SubModuleType.COMPETITOR, "'1','8'") Then%>
                                                    <br />
                                                    <asp:Label ID="lblCustom" runat="server" CssClass="cls_label_header">CUSTOMIZATION</asp:Label><br />
                                                    <%  If GetAccessRight(4, 289, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkActCallKPI" runat="server" Text="Actual Call KPI"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 64, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCollCheqEnq" runat="server" Text="Payment Collection Cheque Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 199, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCollEnq" runat="server" Text="Collection Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 68, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkPAFEnq" runat="server" Text="Promotion Acceptance Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 69, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkStkRetByMth" runat="server" Text="Goods Return By Month End Closing"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 70, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesrepStkAlloc" runat="server" Text="Field Force Stock Allocation"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 103, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallRate" runat="server" Text="Call Rate"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 104, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesTgtVsAct" runat="server" Text="Sales Target Vs Actual"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 105, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesEstVsAct" runat="server" Text="Sales Estimation Vs Actual"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 124, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallInfoByMonth" runat="server" Text="Call Info By Month"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 125, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFCAP" runat="server" Text="Salesforce Capacity By Month"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 165, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallByDKSHClass" runat="server" Text="Call Analysis by DKSH Class"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 166, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallBySupplierClass" runat="server" Text="Call Analysis by Supplier Class"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 168, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallActyEnq" runat="server" Text="Call Activity Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 169, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesrepMthActy" runat="server" Text="Field Force Monthly Activity"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 170, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesrepPrdFreq" runat="server" Text="Field Force Productivity Freq"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 175, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallAnalyEnq" runat="server" Text="Call Analysis Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 182, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCProcActivSup" runat="server" Text="Monthly Field Force Activity by Supplier"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 186, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkTechnicianItinerary" runat="server" Text="Technician Itinerary"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 192, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesOrderPrdMatrix" runat="server" Text="Sales Order With Product Matrix"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 193, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesrepCallRate" runat="server" Text="Field Force Call Rate"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 196, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkServiceStatistic" runat="server" Text="Service Statistic"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 200, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkInvForecastCust" runat="server" Text="Inventory Forecasting by Customer"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 201, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkInvForecastCustClass" runat="server" Text="Inventory Forecasting by Customer Class"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 202, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkInvForecastDistrict" runat="server" Text="Inventory Forecasting by District"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 214, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMTHPRDFREQ" runat="server" Text="Monthly Productive Frequency By Field Force"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 215, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMTHTERRITORY" runat="server" Text="Monthly Territory "></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 216, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMTHSPECIALITY" runat="server" Text="Monthly Specialty"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 217, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSRSCORECARD" runat="server" Text="Field Force Score Card"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 223, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkPACKAGE" runat="server" Text="Package Analysis Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 224, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallDtlByGroup" runat="server" Text="Speciality Call Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 229, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesCustPerProduct" runat="server" Text="Sales Customer Per Product"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 230, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesCuz" runat="server" Text="Sales by Customer Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 234, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustSalesAnalysis" runat="server" Text="Customer Sales Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 235, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustPrdSalesAnalysis" runat="server" Text="Customer Product Sales Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 236, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSOVariance" runat="server" Text="Sales Order Variance"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 237, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkPlanVsActual" runat="server" Text="Preplan vs Actual By Customer"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 238, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallRateByBrand" runat="server" Text="Call Rate Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 241, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkTRAReport" runat="server" Text="Goods Return Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 243, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMERCHSFMS" runat="server" Text="Merchandiser Stock and Display Check Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 244, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSRKPI" runat="server" Text="Salesrep KPI Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 245, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDailyCallDistribution" runat="server" Text="Daily Call Distribution Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 246, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDRCCuZList" runat="server" Text="Inventory Forecasting Analysis Cuz. Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 249, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallAchievement" runat="server" Text="Call Achievement Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 250, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallByDay" runat="server" Text="Daily Call Analysis Adv Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 251, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCallByMonth" runat="server" Text="Monthly Call Analysis Adv Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.CUZMSSENQUIRY, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMSSEnquiry" runat="server" Text="Customer Market Survey Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.CUZSFMSENQUIRY, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSEnquiry" runat="server" Text="Customer Field Activity Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.CUSTPROFILEENQ, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCustProfile" runat="server" Text="Customer Profile Enquiry"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.MISSCALLANALYSIS, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMissCallAnalysis" runat="server" Text="Missing Call Analysis"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.SALES_BY_CHAIN, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSalesByChain" runat="server" Text="Sales By Chain"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.REMAIN_CALL, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkRemainCall" runat="server" Text="Call Remaining Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_BIMONTH, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkStkConsumpByBiMonth" runat="server" Text="Stock Consumption By Bi Month"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUARTER, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkStkConsumpByQtr" runat="server" Text="Stock Consumption By Quarter"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUADMONTH, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkStkConsumpByQuadMonth" runat="server" Text="Stock Consumption By 4 Month"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, 283, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkSrMthActByPrd" runat="server" Text="Field Force Monthly Activity By Product"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.TRAREPORT_EDI, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkTraReportEDI" runat="server" Text="TRA Report"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.MUSTSELL, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMustSell" runat="server" Text="Product Must Sell"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.ONLINEGALLERY, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkOnlineGallery" runat="server" Text="Online Gallery"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.MERCHANDISING, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkMerchandising" runat="server" Text="Merchandising"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.COMPETITOR, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkCompetitor" runat="server" Text="Competitor Activity and Pricing by Product"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.DAILYTRACKING, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkDailyTracking" runat="server" Text="Daily tracking"></asp:LinkButton> <br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.CUSTOMERCOVERAGE, "'1','8'") Then%>
                                                        <asp:LinkButton ID="lnkCustomerCoverage" runat="server" Text="Customer Coverage"></asp:LinkButton> <br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(4, SubModuleType.HISTORICALTRACKING, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkHistoricalTracking" runat="server" Text="Historical tracking"></asp:LinkButton> <br />
                                                    <%  End If%>
                                                    <% If GetAccessRight(4, SubModuleType.VIEW_SALESMAN_ACTIVITY, "'1','8'") Then%>
                                                    <asp:LinkButton ID="lnkViewSalesmanActivity" runat="server" Text="View Salesman Activity"></asp:LinkButton> <br />
                                                    <%  End If%>

                                                    <%  If GetAccessRight(4, 30, "'1','8'") Then%>
                                                    <br />
                                                    <asp:LinkButton ID="lnkDataFiles" runat="server" Text="DATA FILE"></asp:LinkButton>
                                                    <%  End If%>


                                                    <%  If GetAccessRight(4, 228, "'1','8'") Then%>
                                                    <br />
                                                    <asp:LinkButton ID="lnkDataFiles2" runat="server" Text="DATA FILE LIST"></asp:LinkButton>
                                                    <%  End If%>
                                                </div>
                                            </div>
                                            <%  End If%>


                                            <%  If GetAccessRight(3) Then%>
                                            <div id="col2" class="divcol">
                                                <div class="GridHeader" style="vertical-align: middle; width: 100%">
                                                    OnDemand</div>
                                                <%  End If%>

                                                <%  If GetAccessRight(3) Then%>
                                                <div class="divContent">
                                                    <%  If GetAccessRight(3, 15, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkCustomer" runat="server" Text="CUSTOMER"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 24, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkProduct" runat="server" Text="PRODUCT"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkTransaction" runat="server" Text="TRANSACTION"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 43, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkMessaging" runat="server" Text="MESSAGING"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 45, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkPerformance" runat="server" Text="PERFORMANCE"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 44, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkReporting" runat="server" Text="REPORTING"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 46, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkSummary" runat="server" Text="SUMMARY"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 47, "'1'") Then%>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="PRE PLAN"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 248, "'1'") Then%>
                                                    <asp:LinkButton ID="LnkPrePlanCuz" runat="server" Text="PRE PLAN Advance"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 102, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMSDataFiles" runat="server" Text="DATA FILE"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 221, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkLeave" runat="server" Text="LEAVE"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 187, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkTRAOrderList" runat="server" Text="TRA List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(3, 292, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkTRAOrderListV2" runat="server" Text="TRA List V2"></asp:LinkButton><br />
                                                    <%  End If%>
                                                </div>
                                            </div>
                                            <%  End If%>


                                            <%--</div>
<div id="wrapper2">--%>
                                            <%  If GetAccessRight(ModuleID.FFMA) Then%>
                                            <div id="col3" class="divcol">
                                                <div class="GridHeader" style="vertical-align: middle; width: 100%">
                                                    MASTER</div>
                                                <%  End If%>


                                                <%  If GetAccessRight(ModuleID.FFMA) Then%>
                                                <div class="divContent">
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANY, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.SALESAREA, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.WAREHOUSE, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.REGION, "'1'") Then%>
                                                    <span class="cls_label_header">GENERAL</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANY, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACompany" runat="server" Text="Company"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESAREA, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASalesArea" runat="server" Text="Sales Area"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.WAREHOUSE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAWarehouse" runat="server" Text="Warehouse"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.REGION, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMARegion" runat="server" Text="Region"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, "'1'") Then%>
                                                    <span class="cls_label_header">SALESTEAM</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASalesTeam" runat="server" Text="SalesTeam Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.SHIPTO, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTINFO, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTCLASS, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTSALESTGT, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGENERATELOC, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTTGTPRD, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUST_TEAM_CLASS, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTPROFILEMAINTAIN, "'1'") Then%>
                                                    <span class="cls_label_header">SALES ACCOUNT</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustomer" runat="server" Text="Customer Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                      <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGENERATELOC, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustGenerateLoc" runat="server" Text="Customer Generate Location"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SHIPTO, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAShipto" runat="server" Text="Shipto Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTINFO, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustInfo" runat="server" Text="Other Customer Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTCLASS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustClass" runat="server" Text="Class Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTSALESTGT, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustSalesTgt" runat="server" Text="Sales Target By Customer"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTPROFILEMAINTAIN, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustProfileMaintain" runat="server" Text="Customer Profile Maintenance"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTTGTPRD, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMATargetProductList" runat="server" Text="Target Product Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUST_TEAM_CLASS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMATeamClassList" runat="server" Text="Team Class Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CONTACT, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT, "'1'") Then%>
                                                    <span class="cls_label_header">CONTACT</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CONTACT, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAContact" runat="server" Text="Contact Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAMssContact" runat="server" Text="MSS Contact Update"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT_DTL, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAMssContactDtl" runat="server" Text="MSS Contact Details"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTGROUP, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.SUPPLIER, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKALLOC, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT_MATRIX, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTMUSTSELL, "'1'") Or _
                                                                GetAccessRight(ModuleID.FFMA, SubModuleType.PRDCATALOG, "'1'") Then%>
                                                    <span class="cls_label_header">PRODUCT</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAProduct" runat="server" Text="Product Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTGROUP, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAProductGroup" runat="server" Text="Product Group Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SUPPLIER, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASupplier" runat="server" Text="Supplier Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKALLOC, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAStockAlloc" runat="server" Text="Stock Allocation"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT_MATRIX, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAProductMatrix" runat="server" Text="Product Matrix"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPrdSeqList" runat="server" Text="Product Sequence"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTMUSTSELL, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPrdMustSell" runat="server" Text="Product Must Sell"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRDCATALOG, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPrdCatalog" runat="server" Text="Product Catalog"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <% If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTMUSTSELLADV, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPrdMustSellAdv" runat="server" Text="Product Must Sell (Adv.)"></asp:LinkButton><br />
                                                    <% End If %>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDPRICE, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALPRICE, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPPRICE, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPPRICE, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDBONUS, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALBONUS, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPBONUS, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPBONUS, "'1'") Or _
                                                          GetAccessRight(ModuleID.FFMA, SubModuleType.PACKAGE, "'1'") Then%>
                                                    <span class="cls_label_header">TRADE DEAL</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDPRICE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAStandardPrice" runat="server" Text="Price - Standard"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALPRICE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASpecialPrice" runat="server" Text="Price - Customer Special"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPPRICE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustGrpPrice" runat="server" Text="Price - Customer Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPPRICE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPriceGrpPrice" runat="server" Text="Price - Price Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDBONUS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAStandardBonus" runat="server" Text="Bonus - Standard"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALBONUS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASpecialBonus" runat="server" Text="Bonus - Customer Special"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPBONUS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACustGrpBonus" runat="server" Text="Bonus - Customer Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPBONUS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPriceGrpBonus" runat="server" Text="Bonus - Price Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PACKAGE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPackage" runat="server" Text="Assortment (Package)"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, "'1'") Or _
                                                    GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV, "'1'") Then%>
                                                    <span class="cls_label_header">FIELD FORCE ACTIVITY</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldActy" runat="server" Text="Field Activity"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAMSSAdv" runat="server" Text="Market Survey"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAMSSAdv2" runat="server" Text="Market Survey 2"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_TEAM_MAPPING, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMSTeamMap" runat="server" Text="Market Survey Team Mapping"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMADetailingBrand" runat="server" Text="Detailing"></asp:LinkButton>
                                                    <%  End If%>
                                                    <br />
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCE, "'1'") Or _
  GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTGT, "'1'") Then%>
                                                    <span class="cls_label_header">FIELD FORCE</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForce" runat="server" Text="Field Force Info"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTGT, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASalesTgt" runat="server" Text="Sales Target"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.REASON, "'1'") Or _
                                                    GetAccessRight(ModuleID.FFMA, SubModuleType.ROUTE, "'1'") Or _
                                                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGROUP, "'1'") Or _
                                                    GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, "'1'") Or _
                                                    GetAccessRight(ModuleID.FFMA, SubModuleType.LEAD, "'1'") Or _
                                                    GetAccessRight(ModuleID.FFMA, SubModuletype.SYNCIMAGE, "'1'") Then%>  
                                                    <span class="cls_label_header">OTHERS</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.REASON, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAReason" runat="server" Text="Reason Code"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.ROUTE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMARoute" runat="server" Text="Route"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGROUP, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPriceGroup" runat="server" Text="Price Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPromoSO" runat="server" Text="Promotion Sales Order"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.LEAD, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMALead" runat="server" Text="Lead"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.SYNCIMAGE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMASyncImg" runat="server" Text="Sync Image"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.INBOX, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.OUTBOX, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.UPLOADITINERARY, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADER, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADERV2, "'1'") Then%>
                                                    <span class="cls_label_header">UTILITY</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.INBOX, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAInbox" runat="server" Text="Messaging - Inbox"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.OUTBOX, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAOutbox" runat="server" Text="Messaging - Outbox"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.UPLOADITINERARY, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAUPLOADITINERARY" runat="server" Text="Upload Itinerary"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADER, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAEasyLoader" runat="server" Text="EasyLoader"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADERV2, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAEasyLoaderV2" runat="server" Text="EasyLoaderV2"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.PUSHNOTIFICATION, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAPushNotification" runat="server" Text="Push Notification"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>


                                                    <br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEROUTE, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPRDGRP, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEACTY, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEMSS, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPACKAGE, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEITINERARYTRANSFER, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESGRP, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANFUNCLOC, "'1'") Or _
                                                        GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESOFF, "'1'") Then%>
                                                    <span class="cls_label_header">MAPPING</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForceCust" runat="server" Text="Field Force Customer"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEROUTE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForceRoute" runat="server" Text="Field Force Route"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPRDGRP, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForcePrdGrp" runat="server" Text="Field Force Product Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEACTY, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForceActy" runat="server" Text="Field Force Activity"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEMSS, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForceMSS" runat="server" Text="Field Force Market Survey"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPACKAGE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForcePackage" runat="server" Text="Field Force Package"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEITINERARYTRANSFER, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAFieldForceItineraryTransfer" runat="server" Text="Field Force Itinerary Transfer"
                                                        PostBackUrl="~/iFFMA/Mapping/FieldForceItineraryTransfer.aspx"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESGRP, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMATechnicianSalesGrp" runat="server" Text="Technician Sales Group"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANFUNCLOC, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMATechnicianFuncLoc" runat="server" Text="Technician Functional Location"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESOFF, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMATechnicianSalesOff" runat="server" Text="Technician Sales Office"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />

                                                    <%  End If%>


                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKTRANSFERLIST, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.NEWEQPLIST, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.APPLYSTOCKLIST, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.MANUFACTURER, "'1'") Or _
                                                            GetAccessRight(ModuleID.FFMA, SubModuleType.EQPHISTLIST, "'1'") Then%>
                                                    <span class="cls_label_header">SERVICES</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKTRANSFERLIST, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAStockTransferList" runat="server" Text="Confirm Stock List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.NEWEQPLIST, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMANewEqpList" runat="server" Text="New Equipment List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.APPLYSTOCKLIST, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAApplyStockList" runat="server" Text="Apply Stock List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.MANUFACTURER, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAManufacturerList" runat="server" Text="Manufacturer List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.EQPHISTLIST, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMAEqpHistList" runat="server" Text="Equipment History List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <br />
                                                    <%  End If%>

                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BUPROFILE, "'1'") Or _
                                                           GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BANKPROFILE, "'1'") Or _
                                                           GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORPROFILE, "'1'") Or _
                                                           GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORCUSTOMERMAPPING, "'1'") Then%>  
                                                    <span class="cls_label_header">COLLECTION</span><br />
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BUPROFILE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACompBU" runat="server" Text="Company Code - BU Profile"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BANKPROFILE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACompBank" runat="server" Text="Company Code - Bank Profile"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORPROFILE, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACollectorProfile" runat="server" Text="Collector Profile"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORCUSTOMERMAPPING, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkFFMACollectorCustMapping" runat="server" Text="Collector - Customer Mapping"></asp:LinkButton><br />
                                                    <%  End If%>
                                                   
                                                    <br />
                                                    <%  End If%>



                                                    <%  If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKADJUST, "'1'") Or _
GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKHIST, "'1'") Or _
GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKSTATUS, "'1'") Then%>
                                                    <br />
                                                    <%  End If%>
                                                </div>
                                            </div>
                                            <%  End If%>


                                            <%  If GetAccessRight(1) Then%>
                                            <div id="col4" class="divcol">
                                                <div class="GridHeader" style="vertical-align: middle; width: 100%">
                                                    ADMIN</div>
                                                <% End If%>
                                                <%  If GetAccessRight(1) Then%>
                                                <div class="divContent">
                                                    <% If GetAccessRight(1, 2, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkAR" runat="server" Text="ACCESS RIGHT"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 92, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkConn" runat="server" Text="CONNECTION"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 122, "'1'") Then%>
                                                    <asp:LinkButton ID="lnlSAPList" runat="server" Text="SAP"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 225, "'1'") Then%>
                                                    <asp:LinkButton ID="lnlSAPListV2" runat="server" Text="SAP"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 227, "'1'") Then%>
                                                    <asp:LinkButton ID="lnlSAPListV3" runat="server" Text="SAP"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 242, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkSapListV4" runat="server" Text="SAP"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 226, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkSRGrp" runat="server" Text="SALESREP GROUP"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 1, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkUser" runat="server" Text="USER"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 91, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkUserMobile" runat="server" Text="USER MOBILE"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 240, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkTRAlist" runat="server" Text="Goods Return List"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, 259, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkffms" runat="server" Text="EP OnDemand v5.5" OnClientClick="Integration('FFMS');"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <% If GetAccessRight(1, 264, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkCfgDevice" runat="server" Text="Config Device"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <% If GetAccessRight(1, 264, "'1'") Then%>
                                                    <%--<a href="Admin/Device Config/ConfigDtl.aspx">Config Device Detail</a>--%>
                                                    <asp:LinkButton ID="lnkCfgDeviceDtl" runat="server" Text="Config Device Detail"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <% If GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkELAccessRight" runat="server" Text="Easy Loader V2 Accessright"></asp:LinkButton><br />
                                                    <%  End If%>
                                                    <%  If GetAccessRight(1, SubModuleType.LINKEPONDEMAND6, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkffms6" runat="server" Text="EP OnDemand v6" OnClientClick="Integration6('FFMS');"></asp:LinkButton><br />
                                                    <%  End If%>

                                                    <%  If GetAccessRight(1, SubModuleType.BCP, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkBCP" runat="server" Text="BCP"></asp:LinkButton><br />
                                                    <%  End If%>

                                                    <%  If GetAccessRight(1, SubModuleType.SFMS_CFG, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkSFMSCfg" runat="server" Text="SFMS Configuration"></asp:LinkButton><br />
                                                    <%  End If%>

                                                    <%  If GetAccessRight(1, SubModuleType.MSS_CFG, "'1'") Then%>
                                                    <asp:LinkButton ID="lnkMSSCfg" runat="server" Text="MSS Configuration"></asp:LinkButton><br />
                                                    <%  End If%>
                                                </div>
                                            </div>
                                            <%  End If%>
                                        </div>
                                        <div id="footer" class="divfooter">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
