Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Imports txn_WebActy
Partial Class iFFMS_Customer_CustTxn_TxnEnd
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        'Call Header
        With wuc_lblHeader
            .Title = "End Visit" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
            LoadTxnDescName()
        End With


        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl2.Enabled = True

        End If
    End Sub

    Protected Sub LoadDdlReasonCode()
  
        Dim ValidTxn As String
        ValidTxn = Session("TxnValid")
        If Not IsNothing(ValidTxn) And ValidTxn = "1" Then
            With ddlReasonCode
                .Items.Clear()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .Enabled = False
            End With
        Else
            Dim DT As DataTable
            Dim clsDTS As New txn_WebActy.clsDTS
            DT = clsDTS.GetDTSReasonCode(Trim(Request.QueryString("salesrepcode")), Trim(Session("UserID")))
            With ddlReasonCode
                .Items.Clear()
                .DataSource = DT.DefaultView
                .DataTextField = "REASON_NAME"
                .DataValueField = "REASON_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        End If
    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        If TimerControl2.Enabled Then
            LoadDdlReasonCode()
            UpdatePage.Update()
        End If
        TimerControl2.Enabled = False
    End Sub

    Private Sub UpdateDailyTimeSumm(ByVal StrTxnStatus As String)
        Dim StrSalesrepCode As String, StrCustCode As String, StrContCode As String, StrCallDate As String, StrTimeOut As String, _
        StrVisitId As String, strReasonCode As String, strRemarks As String, StrUserId As String

        StrSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
        StrCustCode = Trim(Request.QueryString("CustCode"))
        StrContCode = Trim(Request.QueryString("ContCode"))
        StrCallDate = Trim(Request.QueryString("DateIn"))
        StrTimeOut = Now().ToString("HH:mm")
        StrVisitId = Trim(Request.QueryString("VisitID"))
        StrUserId = Trim(Session("UserID"))
        strReasonCode = ddlReasonCode.SelectedValue.ToString
        strRemarks = Trim(txtremarks.Text)

        Dim clsDTS As New txn_WebActy.clsDTS
        clsDTS.UpdateDailyTimeSumm(StrSalesrepCode, StrCustCode, StrContCode, StrCallDate, StrTimeOut, StrVisitId, StrTxnStatus, strReasonCode, strRemarks, StrUserId)

    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        UpdateDailyTimeSumm("P")
    End Sub

    Private Sub LoadTxnDescName()
        Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strvisitid As String, strMapPath As String

        strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
        strCustCode = Trim(Request.QueryString("custcode"))
        strContCode = Trim(Request.QueryString("contcode"))
        strvisitid = Trim(Request.QueryString("visitid"))
        strMapPath = "Ondemand > New Transaction > Exit Visit"

        Dim dt As DataTable
        Dim clstxncommon As New txn_common.clstxncommon
        dt = clstxncommon.GetTxnDescName(strSalesrepCode, strCustCode, strContCode)

        If dt.Rows.Count > 0 Then
            lblFieldForceName.Text = Trim(dt.Rows(0)("SALESREP_NAME"))
            lblCustName.Text = Trim(dt.Rows(0)("CUST_NAME"))
            lblContName.Text = Trim(dt.Rows(0)("CONT_NAME"))
            lblVisitId.Text = strvisitid

            lblMapPath.Text = strMapPath
        End If


    End Sub
End Class
