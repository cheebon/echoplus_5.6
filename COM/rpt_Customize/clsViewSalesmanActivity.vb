﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsViewSalesmanActivity
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsViewSalesmanActivity"
        End Get
    End Property



    Public Function GetViewSalesmanActivityList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strStartDate As String, ByVal strEndDate As String, _
                                        ByVal strSalesrepCode As String) As DataTable
        Dim clsVisitExtractDB As clsDB
        Try
            clsVisitExtractDB = New clsDB
            With clsVisitExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_VIEW_SALESMAN_ACTIVITY"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepCode, clsDB.DataType.DBString, True)


                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetViewSalesmanActivityList :" & ex.Message))
        Finally
            clsVisitExtractDB = Nothing
        End Try
    End Function

    Public Function GetViewSalesmanActivityDetailList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strDate As String, _
                                    ByVal strSalesrepCode As String) As DataTable
        Dim clsVisitExtractDB As clsDB
        Try
            clsVisitExtractDB = New clsDB
            With clsVisitExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_VIEW_SALESMAN_ACTIVITY_DETAIL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("DATE", strDate, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)


                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetViewSalesmanActivityDetailList :" & ex.Message))
        Finally
            clsVisitExtractDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
