﻿Imports System.Data

Partial Class iFFMA_SAP_New_SAPList
    Inherits System.Web.UI.Page
    Private intPageSize As Integer
    Dim aryDataItem As New ArrayList
    Private _licCustomHeaderCollector As ListItemCollection

#Region "Properties/Variables"
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SalesList")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SalesList") = value
        End Set
    End Property
    Public ReadOnly Property PageName() As String
        Get
            Return "SAP List"
        End Get
    End Property
    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
    Public Property SelectedDocType() As String
        Get
            Return ViewState("SelectedDocType")
        End Get
        Set(ByVal value As String)
            ViewState("SelectedDocType") = value
        End Set
    End Property
    Private dtAR As DataTable
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "SAP List"
                .DataBind()
                .Visible = True
            End With


            If Not Page.IsPostBack Then
                'Call Paging
                With wuc_dgpaging
                    .PageCount = dgList.PageCount
                    .CurrentPageIndex = dgList.PageIndex
                    .DataBind()
                    .Visible = False
                End With

                LoadStatus()
                LoadTxnStatus()
                GetTeam()
                GetSAPType()
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Function"
    'Private Sub GetTeam()
    '    Dim objSAPQuery As mst_SAP.clsSAPQuery
    '    Dim dt As DataTable
    '    Dim drRow As DataRow

    '    Try
    '        ddlTeam.Items.Clear()
    '        objSAPQuery = New mst_SAP.clsSAPQuery
    '        With objSAPQuery
    '            .clsProperties.user_code = Trim(Session("UserCode"))
    '            dt = .GetSAPTeamList
    '        End With
    '        If dt.Rows.Count > 0 Then
    '            ddlTeam.Items.Add(New ListItem("All", "ALL"))
    '            For Each drRow In dt.Rows
    '                ddlTeam.Items.Add(New ListItem(drRow("team"), drRow("team")))
    '            Next
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub
    Private Sub LoadStatus()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable
        Try
            objSAPQuery = New mst_SAP.clsSAPQuery
            dt = objSAPQuery.GetSAPStatusList

            With ddlStatus
                .DataSource = dt
                .DataTextField = "STATUS"
                .DataValueField = "STATUS"
                .DataBind()

                .Items.Insert(0, New ListItem("All", ""))
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadTxnStatus()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable

        Try
            objSAPQuery = New mst_SAP.clsSAPQuery
            dt = objSAPQuery.GetSAPTxnStatusList

            With ddlTxnStatus
                .DataSource = dt
                .DataTextField = "STATUS"
                .DataValueField = "STATUS"
                .DataBind()

                .Items.Insert(0, New ListItem("All", ""))
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub GetSAPType()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            ddlDocType.Items.Clear()
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.user_code = Trim(Session("UserCode"))
                dt = .GetSAPType
            End With
            If dt.Rows.Count > 0 Then
                For Each drRow In dt.Rows
                    ddlDocType.Items.Add(New ListItem(drRow("DOC_VALUE").ToString.Trim, drRow("DOC_CODE").ToString.Trim))
                Next
            End If
            If ddlDocType.Items.Count > 0 Then
                ddlDocType.SelectedIndex = 0
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub GetTeam()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            ddlTeam.Items.Clear()
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.user_code = Trim(Session("UserCode"))
                dt = .GetSAPTeamList
            End With
            If dt.Rows.Count > 0 Then
                ddlTeam.Items.Add(New ListItem("All", "ALL"))
                For Each drRow In dt.Rows
                    ddlTeam.Items.Add(New ListItem(drRow("team_code").ToString.Trim, drRow("team_code").ToString.Trim))
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ResubmitSAP(ByVal strTxnNo As String)
        Dim obj As mst_SAP.clsSAP
        Try
            obj = New mst_SAP.clsSAP

            With obj
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.txn_no = strTxnNo
                .clsProperties.doc_type = SelectedDocType

                .SAPResubmit()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UnblockSAP(ByVal strTxnNo As String)
        Dim obj As mst_SAP.clsSAP
        Try
            obj = New mst_SAP.clsSAP

            With obj
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.txn_no = strTxnNo
                .clsProperties.doc_type = SelectedDocType

                .SAPUnblock()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CancelSAP(ByVal strTxnNo As String)
        Dim obj As mst_SAP.clsSAP
        Try
            obj = New mst_SAP.clsSAP

            With obj
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.txn_no = strTxnNo
                .clsProperties.doc_type = SelectedDocType

                .SAPCancel()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable '= CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()
            If dtCurrenttable Is Nothing Then
                dtCurrenttable = New DataTable
            Else
                If dtCurrenttable.Rows.Count = 0 Then
                    dtCurrenttable.Rows.Add(dtCurrenttable.NewRow())
                    'dgList.Columns(0).Visible = False
                    'dgList.Columns(1).Visible = False
                End If
            End If

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.PageSize = intPageSize
            dgList.DataBind()
            dgList.GridHeight = 410

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = IIf(dtCurrenttable.Rows.Count > intPageSize, True, False)
            End With

            ApplyAccessibility()
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdatePanel.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New mst_SAP.clsSAPQuery
        Try
            With obj
                .clsProperties.doc_type = ddlDocType.SelectedValue
                .clsProperties.team_code = ddlTeam.SelectedValue
                .clsProperties.error_reason = txtErrReason.Text.Trim
                .clsProperties.last_update_start = Wuc_txtDateLastUpdate.DateStart.Trim
                .clsProperties.last_update_end = Wuc_txtDateLastUpdate.DateEnd.Trim
                .clsProperties.txn_no = txtTxnNo.Text.Trim
                .clsProperties.txn_date_start = Wuc_txtDateTxnDate.DateStart.Trim
                .clsProperties.txn_date_end = Wuc_txtDateTxnDate.DateEnd.Trim
                .clsProperties.txn_status = ddlTxnStatus.SelectedValue
                .clsProperties.sales_office = txtSalesOffice.Text.Trim
                .clsProperties.salesrep_code = txtSalesrepCode.Text.Trim
                .clsProperties.status = ddlStatus.SelectedValue
                .clsProperties.saving_no = txtSavingNo.Text.Trim
                .clsProperties.note = "" 'txtNote.Text.Trim

                DT = .ListSAP()
            End With

            'DT = obj.SubmoduleList(txtModuleCode.Text.Trim, txtSubmoduleCode.Text.Trim, txtSubmoduleName.Text.Trim)


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
    End Sub
    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()
        'ClearDGListColumns()

        AddUnblockButton()
        AddResubmitButton()
        AddCancelButton()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_SAPLists.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_SAPLists.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_SAPLists.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SAPLists.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SAPLists.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SAPLists.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields(1) As String
                    Dim strUrlField(0) As String
                    strUrlField(0) = ColumnName

                    strUrlFormatString = " parent.document.getElementById('DetailBarIframe').src='../../iFFMA/SAP/SAPv3/SAPDtl.aspx?txn_no={0}&doc_type=" & SelectedDocType & "' "

                    dgColumn.DataNavigateUrlFields = strUrlField
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case FieldColumntype.ButtonColumn
                    Dim dgColumn As New ButtonField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left

                    If String.IsNullOrEmpty(CF_SAPLists.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_SAPLists.GetOutputFormatString(ColumnName)
                    End If
                    'dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SAPLists.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SAPLists.GetDisplayColumnName(ColumnName)
                    'dgColumn.Text = "Unblock"
                    dgColumn.SortExpression = ColumnName
                    dgColumn.DataTextField = ColumnName
                    dgColumn.CommandName = "Details"
                    dgColumn.ButtonType = ButtonType.Link
                    'dgColumn.ImageUrl = "../../images/ico_Unblock.gif"

                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_SAPLists.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_SalesList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SAPLists.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SAPLists.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SAPLists.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub
    Private Sub ClearDGListColumns()
        For i As Integer = dgList.Columns.Count - 1 To 0 Step -1
            dgList.Columns.RemoveAt(i)
        Next
    End Sub

    Private Sub AddUnblockButton()
        Try
            Dim dgColumn As New ButtonField ' HyperLinkColumn

            dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

            'If String.IsNullOrEmpty(CF_SAPLists.GetOutputFormatString(ColumnName)) = False Then
            '    dgColumn.DataTextFormatString = CF_SAPLists.GetOutputFormatString(ColumnName)
            'End If
            'dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
            dgColumn.ItemStyle.Wrap = True

            dgColumn.HeaderText = ""
            dgColumn.Text = "Unblock"
            dgColumn.SortExpression = "Unblock"
            dgColumn.CommandName = "Unblock"
            dgColumn.ButtonType = ButtonType.Button
            'dgColumn.ImageUrl = "../../../images/ico_Unblock.gif"

            dgList.Columns.Add(dgColumn)
            dgColumn = Nothing

            'Add the field name
            aryDataItem.Add("Unblock")
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub AddResubmitButton()
        Try
            Dim dgColumn As New ButtonField ' HyperLinkColumn

            dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

            'If String.IsNullOrEmpty(CF_SAPLists.GetOutputFormatString(ColumnName)) = False Then
            '    dgColumn.DataTextFormatString = CF_SAPLists.GetOutputFormatString(ColumnName)
            'End If
            'dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
            dgColumn.ItemStyle.Wrap = True

            dgColumn.HeaderText = ""
            dgColumn.Text = "Resubmit"
            dgColumn.SortExpression = "Resubmit"
            dgColumn.CommandName = "Resubmit"
            dgColumn.ButtonType = ButtonType.Button
            'dgColumn.ImageUrl = "../../../images/icoCopy.gif"
            
            dgList.Columns.Add(dgColumn)
            dgColumn = Nothing

            'Add the field name
            aryDataItem.Add("Resubmit")
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub AddCancelButton()
        Try
            Dim dgColumn As New ButtonField ' HyperLinkColumn

            dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

            'If String.IsNullOrEmpty(CF_SAPLists.GetOutputFormatString(ColumnName)) = False Then
            '    dgColumn.DataTextFormatString = CF_SAPLists.GetOutputFormatString(ColumnName)
            'End If
            'dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
            dgColumn.ItemStyle.Wrap = True

            dgColumn.HeaderText = ""
            dgColumn.Text = "Cancel"
            dgColumn.SortExpression = "Cancel"
            dgColumn.CommandName = "Cancel"
            dgColumn.ButtonType = ButtonType.Button
            'dgColumn.ImageUrl = "../../../images/icoCancel.gif"

            dgList.Columns.Add(dgColumn)
            dgColumn = Nothing

            'Add the field name
            aryDataItem.Add("Cancel")
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region
#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region
#Region "Function"
    Private Sub DissectIncomingRequest()
        'Dim strDocType, strSearchType, strSearchValue, strSearchValue1, strTeam As String

        'Try
        '    strDocType = IIf(Trim(Request.QueryString("doc_type")) <> "", Trim(Request.QueryString("doc_type")), "SO")
        '    strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "LASTUPDATE")
        '    strSearchValue = Trim(Request.QueryString("search_value"))
        '    strSearchValue1 = Trim(Request.QueryString("search_value_1"))

        '    'Prepare Recordset,Createobject as needed
        '    ddlDocType.SelectedValue = strDocType
        '    ddlSearchType.SelectedValue = strSearchType
        '    ddlSearchType_onChanged()

        '    If Trim(strSearchType) = "TXN_TIMESTAMP" Then
        '        Wuc_txtDate1.Text = IIf(strSearchValue <> "", strSearchValue, Now.ToString("yyyy-MM-dd"))
        '        Wuc_txtDate2.Text = IIf(strSearchValue1 <> "", strSearchValue1, Now.ToString("yyyy-MM-dd"))
        '    ElseIf Trim(strSearchType) = "TXN_STATUS" Then
        '        ddlList.SelectedValue = IIf(strSearchValue <> "", strSearchValue, "")
        '    Else
        '        txtSearchValue.Text = IIf(strSearchValue <> "", strSearchValue, "")
        '        txtSearchValue1.Text = IIf(strSearchValue1 <> "", strSearchValue1, "")
        '    End If



        'Catch ex As Exception
        '    ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        'End Try
    End Sub

    Private Sub FirstTimeLoader()
        'Dim strTeam As String
        'Try
        '    GetTeam()
        '    strTeam = Trim(Request.QueryString("team"))
        '    ddlTeam.SelectedValue = strTeam
        '    BindGrid(ViewState("SortCol"))

        'Catch ex As Exception
        '    ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        'End Try
    End Sub

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function
    Private Sub ApplyAccessibility()
        Try
            If Not GetAccessRight(1, 227, 3) Then 'Edit
                dgList.Columns(0).Visible = False 'unblock
                dgList.Columns(2).Visible = False 'cancel
            End If
            If Not GetAccessRight(1, 227, 2) Then 'Add
                dgList.Columns(1).Visible = False 'resubmit
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Try
            SelectedDocType = ddlDocType.SelectedValue
            CPE_PnlSearch.ClientState = "true"
            CPE_PnlSearch.Collapsed = True
            RenewDataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgList.RowCancelingEdit

    End Sub

    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Try

            Select Case e.CommandName.ToUpper
                Case "DETAILS"
                    'Response.Redirect("SAPDtl.aspx?txn_no=" & txn_no & "&doc_type=" & SelectedDocType)
                Case "RESUBMIT"
                    Dim txn_no As String = dgList.DataKeys.Item(Convert.ToInt32(e.CommandArgument)).Value
                    ResubmitSAP(txn_no)
                Case "CANCEL"
                    Dim txn_no As String = dgList.DataKeys.Item(Convert.ToInt32(e.CommandArgument)).Value
                    CancelSAP(txn_no)
                Case "UNBLOCK"
                    Dim txn_no As String = dgList.DataKeys.Item(Convert.ToInt32(e.CommandArgument)).Value
                    UnblockSAP(txn_no)
            End Select
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim imgButtonResubmit, imgButtonUnblock, imgButtonCancel As Button
        Dim lnkDetail As HyperLink
        Try
            Select e.Row.RowType
                Case DataControlRowType.DataRow
                    imgButtonUnblock = CType(e.Row.Cells(0).Controls(0), Button)
                    imgButtonResubmit = CType(e.Row.Cells(1).Controls(0), Button)
                    imgButtonCancel = CType(e.Row.Cells(2).Controls(0), Button)
                    lnkDetail = CType(e.Row.Cells(licHeaderCollector.IndexOf(licHeaderCollector.FindByText("TXN_NO"))).Controls(0), HyperLink)

                    
                    drv = CType(e.Row.DataItem, DataRowView)

                    lnkDetail.Target = String.Empty
                    lnkDetail.Attributes.Add("onclick", lnkDetail.NavigateUrl & ";resizeLayout3();")
                    lnkDetail.NavigateUrl = "#"

                    If drv("STATUS").ToString = "B" Then
                        imgButtonUnblock.Visible = True
                        imgButtonUnblock.Attributes.Add("onclick", "javascript:if (confirm('Are you sure want to unblock?') == false) { window.event.returnValue = false; return false; }")

                        imgButtonCancel.Visible = True
                        imgButtonCancel.Attributes.Add("onclick", "javascript:if (confirm('Are you sure want to cancel?') == false) { window.event.returnValue = false; return false; }")
                    Else
                        imgButtonUnblock.Visible = False
                        imgButtonCancel.Visible = False
                    End If

                    If drv("STATUS").ToString = "S" Or drv("STATUS").ToString = "R" Or drv("STATUS").ToString.Trim = "E" Or drv("STATUS").ToString.Trim = "W" Then
                        imgButtonResubmit.Visible = True
                        imgButtonResubmit.Attributes.Add("onclick", "javascript:if (confirm('Are you sure want to resubmit?') == false) { window.event.returnValue = false; return false; }")
                    Else
                        imgButtonResubmit.Visible = False
                    End If

                    imgButtonUnblock.CssClass = "cls_button"
                    imgButtonResubmit.CssClass = "cls_button"
                    imgButtonCancel.CssClass = "cls_button"
            End Select


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression Like ("* DESC") Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
End Class
Public Class CF_SAPLists
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'SALESREP_CODE ,SALESREP_NAME ,
        'CHAIN_CODE ,CHAIN_NAME ,CHANNEL_CODE ,CHANNEL_NAME ,
        'CUST_CODE ,CUST_NAME ,CUST_GRP_CODE ,CUST_GRP_NAME ,
        'PRD_CODE ,PRD_NAME ,PRD_GRP_CODE ,PRD_GRP_NAME ,
        'SALES_AREA_CODE ,SALES_AREA_NAME ,
        'SHIPTO_CODE ,SHIPTO_NAME

        'TEAM_CODE ,REGION_CODE ,REGION_NAME 
        'MTD_SALES ,MTD_FOC_QTY ,MTD_SALES_QTY ,MTD_TGT ,MTDVAR, 
        'QTD_SALES ,QTD_FOC_QTY ,QTD_SALES_QTY ,QTD_TGT ,QTDVAR ,
        'YTD_SALES ,YTD_FOC_QTY ,YTD_SALES_QTY ,YTD_TGT ,YTDVAR ,
        'PYTD_SALES,PYTD_FOC_QTY ,PYTD_SALES_QTY ,YTDGROSS

        Select Case ColumnName.ToUpper
            Case "AUART"
                strFieldName = "Order Type"
            Case "VKORG"
                strFieldName = "Sales Org"
            Case "VTWEG"
                strFieldName = "Distribution Channel"
            Case "SPART"
                strFieldName = "Division"
            Case "VKBUR"
                strFieldName = "Sales Office"
            Case "VKGRP"
                strFieldName = "Sales Group"
            Case "WERKS"
                strFieldName = ""
            Case "LGORT"
                strFieldName = ""
            Case "VSTEL"
                strFieldName = ""
            Case "KUNNR"
                strFieldName = "Soldto Party"
            Case "KUNDE"
                strFieldName = "Shipto Party"
            Case "PAYER"
                strFieldName = "Payer Code"
            Case "PERNR"
                strFieldName = ""
            Case "BSTNK"
                strFieldName = "PO No"
            Case "BSTDK"
                strFieldName = "PO Date"
            Case "BSTRK"
                strFieldName = ""
            Case "BSTZD"
                strFieldName = ""
            Case "BNAME"
                strFieldName = ""
            Case "IHREZ"
                strFieldName = ""
            Case "H_SHIP_TEXT_1"
                strFieldName = ""
            Case "H_SHIP_TEXT_2"
                strFieldName = ""
            Case "H_SHIP_TEXT_3"
                strFieldName = ""
            Case "H_DISC_TYP_1"
                strFieldName = ""
            Case "H_DISC_VAL_1"
                strFieldName = ""
            Case "H_DISC_TYP_2"
                strFieldName = ""
            Case "H_DISC_VAL_2"
                strFieldName = ""
            Case "KETDAT"
                strFieldName = "Request Delivery Date"
            Case "STATUS"
                strFieldName = "Status"
            Case "DOC_NO"
                strFieldName = "Saving No"
            Case "REMARKS"
                strFieldName = "Err Reason"
            Case "LASTUPDATE"
                strFieldName = "Last Update"
            Case "VKAUS"
                strFieldName = ""
            Case "RESENDCOUNTER"
                strFieldName = "Resubmit Counter"
            Case "PDA_ORD_NO"
                strFieldName = ""
            Case "ORDER_BY"
                strFieldName = ""
            Case "TXN_NO"
                strFieldName = "Txn No"
            Case "TXN_STATUS"
                strFieldName = "Txn Status"
            Case "TXN_TIMESTAMP"
                strFieldName = "Txn Date"
            Case "SALESREP_CODE"
                strFieldName = "Salesrep Code"
            Case "LN_STATUS"
                strFieldName = "LN Status"
            Case "LN_REASON"
                strFieldName = "LN Remarks"
            Case "NOTE"
                strFieldName = "Note"
            Case "TEAM_CODE"
                strFieldName = ""
            Case "EMAIL_DATE"
                strFieldName = "Email Date"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            Case "SAP_STATUS"
                FCT = FieldColumntype.ButtonColumn
            Case "TXN_NO"
                FCT = FieldColumntype.HyperlinkColumn
        End Select
        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
                 "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strStringFormat = "{0:#,0.0}"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
                 "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strStringFormat = "{0:#,0}"
            Case "SHIPTO_DATE", "TXN_DATE_IN", "TXN_DATE_OUT", "PAY_DATE", "TXN_TIMESTAMP", "PO_DATE", "SO_DATE", "BSTDK", "EMAIL_DATE", "KETDAT"
                strStringFormat = "{0:yyyy-MM-dd}"
            Case "LASTUPDATE"
                strStringFormat = "{0:yyyy-MM-dd hh:mm:ss:tt}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class