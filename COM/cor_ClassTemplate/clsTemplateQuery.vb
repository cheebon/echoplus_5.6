'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/06/2005
'	Purpose	    :	Class Template for Query
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On 
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel

Interface Icor_TemplateQuery
    Function GetTemplateList(ByVal UserID As Long) As DataTable
End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsTemplateQuery
    'Inherits ServicedComponent
    Implements Icor_TemplateQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Public clsProperties As New clsProperties.clsTemplateQuery

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetTemplateList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: UserID
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetTemplateList(ByVal UserID As Long) As DataTable Implements Icor_TemplateQuery.GetTemplateList
        Try
            'Call Trail class based on system setting

            'Dim obj as Object.clsObjectClass
            'set obj = new Object.clsObjectClass
            'with obj
            '
            'end with
            'set obj = Nothing

            'ContextUtil.SetComplete()
            Return Nothing

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg(ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
