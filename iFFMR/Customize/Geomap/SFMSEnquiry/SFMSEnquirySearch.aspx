﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SFMSEnquirySearch.aspx.vb" Inherits="iFFMR_Customize_SFMSEnquiry_SFMSEnquirySearch" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SFMS Enquiry</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../../include/layout.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MapIconMaker/mapiconmaker.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
    <script type='text/javascript'>
        function NavigateAnswer(TxnNo, CustCode, Source) {
            $("#DetailBarIframe", parent.document).attr('src', "../../../iFFMR/Customize/Geomap/SFMSEnquiry/SFMSActivityList.aspx?txnno=" + TxnNo + "&custcode=" + CustCode + "&source=" + Source);
            // alert(item.count());
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('MapBar');HideElement('DetailBar');ShowElement('ContentBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">        
        
    </ajaxToolkit:ToolkitScriptManager>
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                        <div style="display: none;">
                                            <asp:Button ID="btnClientRefresh" runat="server" Text="Button" /> 
                                            </div>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">                                                                
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                            Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" 
                                                                            GridWidth="" AllowPaging="True" DataKeyNames="TXN_NO,CUST_CODE" bordercolor="Black" 
                                                                            borderwidth="1" GridBorderColor="Black" GridBorderWidth="1px" 
                                                                            RowHighlightColor="AntiqueWhite">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="Customer Code" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CUST_CODE" SortExpression="CUST_CODE" Visible="false" />
                                                                                    <asp:BoundField HeaderText="Customer" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="CUST_NAME" SortExpression="CUST_NAME" />
                                                                                     <asp:BoundField HeaderText="Address" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="ADDRESS" SortExpression="ADDRESS" />
                                                                                     <asp:BoundField HeaderText="District" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="DISTRICT" SortExpression="DISTRICT" />
                                                                                     <asp:BoundField HeaderText="Customer Group" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="CUST_GRP_NAME" SortExpression="CUST_GRP_NAME" />
                                                                                     <asp:BoundField HeaderText="Cust Type" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CUST_TYPE" SortExpression="CUST_TYPE" />
                                                                                     <asp:BoundField HeaderText="Class" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CLASS" SortExpression="CLASS" />
                                                                                     <asp:BoundField HeaderText="SFMS Date" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="TXN_DATE" SortExpression="TXN_DATE" DataFormatString="{0:yyyy-MM-dd}" />
                                                                                     
                                                                                     <asp:TemplateField>
                                                                                        <HeaderTemplate>Action</HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkView" runat="server">View</asp:LinkButton>                                                                                            
                                                                                        </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                    
                                                                                </Columns>
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                                <EmptyDataTemplate>
                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                </EmptyDataTemplate>
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                      
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>   
    </form>
</body>
</html>
