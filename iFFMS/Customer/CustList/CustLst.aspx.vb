Imports System.Data
Partial Class iFFMS_Customer_CustomerList_CustLst
    Inherits System.Web.UI.Page

    Dim strCustCode As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerListing"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Try
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

        'Call Header
        With wuc_lblheader
            .Title = "Customer Listing"
            .DataBind()
            .Visible = True
        End With

        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPosition();',100);", True)

        If Not IsPostBack Then

            TimerControl1.Enabled = True
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
        End If


 
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            BindRouteDLL()
            UpdateSearch.Update()
            UpdateDatagrid.Update()

        End If

    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Sub BindRouteDLL()
        Dim obj As New txn_Customer.clsCustomerListing
        Dim dt As DataTable = Nothing
        Dim strSalesmanCOde As String
        strSalesmanCOde = Trim(Session("SALESREP_CODE"))
        dt = obj.getRouteDT(strSalesmanCOde)
        If dt.Rows.Count > 0 Then
            ddlRoute.DataSource = dt
            ddlRoute.DataValueField = "route_code"
            ddlRoute.DataTextField = "route_desc"
            ddlRoute.DataBind()
            ddlRoute.Items.Insert(0, "--SELECT--")
            ddlRoute.Items.Insert(1, "All")
        Else
            ddlRoute.Items.Add("None")
        End If
        RefreshDatabinding()
        UpdateRoute.Update()
        'Try


        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".BindRouteDLL : " & ex.ToString)
        'Finally
        '    obj = Nothing
        'End Try
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        dtCurrenttable = GetRecList()

        'PreRenderMode(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        If dtCurrenttable.Rows.Count > 0 Then
            gridview1.DataSource = dvCurrentView
            gridview1.DataBind()
        Else
            ' dtnew = dtCurrenttable.Copy
            ' dtnew.Rows.Add(dtnew.NewRow)
            gridview1.DataSource = Nothing
            gridview1.DataBind()
        End If


        UpdateDatagrid.Update()

        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gridview1.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim obj As New txn_Customer.clsCustomerListing
        Dim DT As DataTable = Nothing
        Dim strSalesmanCode As String, strRoutecode As String, strSearchValue As String, strSearchBy As String

        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strRoutecode = Trim(ddlRoute.SelectedValue)
            strSearchValue = Trim(ddlsearchby.SelectedValue)
            strSearchBy = Trim(txtsearch.Text)

            DT = obj.getVisit_CustListDT(strSalesmanCode, strRoutecode, strSearchBy, strSearchValue)
            
            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub ddlRoute_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoute.SelectedIndexChanged
        gridview1.SelectedIndex = -1
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "SetScrollPosition", "SetScrollPosition();", True)
        RefreshDatabinding()
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPosition();',100);", True)
    End Sub

    Protected Sub gridview1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridview1.RowCreated

        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim strCustCode = gridview1.DataKeys(e.Row.RowIndex).Item("CUST_CODE")

                Dim str As String
                str = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Customer/CustList/CustomerDtl.aspx?custcode=" + strCustCode + "';"
                e.Row.Cells(1).Attributes.Add("onclick", str)
                Dim strPosition As String = "SetScrollPosition();"
                Dim str3 As String '5 is the column to add attributes the link
                str3 = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Customer/CustList/TransactionCustSelection.aspx?custcode=" + strCustCode + "';"
                e.Row.Cells(5).Attributes.Add("onclick", str3 + strPosition)
                e.Row.Cells(5).Attributes.Add("style", "cursor:pointer;")
                e.Row.Attributes.Add("onclick", "javascript:__doPostBack('gridview1','Select$" & e.Row.RowIndex & "');")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Sub gridview_Command(ByVal s As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand

        'Try
        'Dim strCustCode As String = gridview1.DataKeys(e.CommandArgument).Item("CUST_CODE")

        'Select Case e.CommandName
        '    Case "Visit"
        '        Dim StrScript As String
        '        StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Customer/CustList/TransactionCustSelection.aspx?custcode=" + strCustCode + "';"
        '        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)

        '    Case "Details"
        '        Dim StrScript As String
        '        StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Customer/CustList/CustomerDtl.aspx?custcode=" + strCustCode + "';"
        '        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)

        'End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".gridview_Command : " & ex.ToString)
        'End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
   
    Protected Sub btnCust_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCust_Refresh.Click
        ddlRoute.SelectedIndex = 1
        UpdateRoute.Update()
        gridview1.SelectedIndex = -1
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "SetScrollPosition", "SetScrollPosition();", True)
        RefreshDatabinding()
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPosition();',100);", True)
    End Sub

    Protected Sub btnrefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnrefresh.Click
        RefreshDatabinding()

    End Sub

End Class








