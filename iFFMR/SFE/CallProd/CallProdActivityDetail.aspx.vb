Imports System.Data

Partial Class CallProdActivityDetail
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryAllColumnName As ArrayList
    Protected Property aryAllColumnName() As ArrayList
        Get
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = ViewState("AllDataItem")
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = New ArrayList
            Return _aryAllColumnName
        End Get
        Set(ByVal value As ArrayList)
            ViewState("AllDataItem") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

#Region "Inner Class"
    Class clsSharedValue_CallProdDetail
        Inherits clsSharedValue
        'Param pass from Previous: MONTH, CAT_CODE, SUB_CAT_CODE, CUST_CODE, CUST_GRP_CODE, TEAM_CODE, SALESREP_CODE
        Private strCatCode, strSubCatCode As String
        Private strCustCode, strCustGrp As String

        Sub New()
            MyBase.New()
        End Sub

        Public Property CatCode() As String
            Get
                Return strCatCode
            End Get
            Set(ByVal value As String)
                strCatCode = value
                RaiseValueChangedEvent()
            End Set
        End Property

        Public Property SubCatCode() As String
            Get
                Return strSubCatCode
            End Get
            Set(ByVal value As String)
                strSubCatCode = value
                RaiseValueChangedEvent()
            End Set
        End Property

        Public Property CustCode() As String
            Get
                Return strCustCode
            End Get
            Set(ByVal value As String)
                strCustCode = value
                RaiseValueChangedEvent()
            End Set
        End Property

        Public Property CustGroup() As String
            Get
                Return strCustGrp
            End Get
            Set(ByVal value As String)
                strCustGrp = value
                RaiseValueChangedEvent()
            End Set
        End Property

    End Class
#End Region
    Dim WithEvents clsCriteriaCollector As clsSharedValue_CallProdDetail
#Region "    Criteria Collector"
    Dim strCollectorName As String = "Collector_CallProdActivityDetail"
    Private Property CriteriaCollector() As clsSharedValue_CallProdDetail
        Get
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue_CallProdDetail()
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue_CallProdDetail)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region
#End Region



#Region " Web Form Designer Generated Code "


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CallProdActivityDetail"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.CPRODDAILYACTY) '"SFMS Activities Statistic Information "
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.CPRODDAILYACTY
                .DataBind()
                .Visible = True
            End With
            'lblErr.Text = ""

            'Dim intYear As Integer = Session("Year")
            Dim intYear As Integer = CInt(ViewState("YearInThisPage"))
            If intYear = 0 Then intYear = Session("Year")
            Dim intMonth As Integer = CInt(ViewState("MonthInThisPage"))
            lblErr.Text = ""

            'lblErr.Text = Request.RawUrl.ToString

            If Not IsPostBack Then
                Session("CallProdActivityDetail_QueryString") = Request.QueryString.ToString.Replace("+", "")

                'Param pass from Previous: MONTH, CAT_CODE, SUB_CAT_CODE, CUST_CODE, CUST_GRP_CODE, TEAM_CODE, SALESREP_CODE
                ViewState("dtCurrentView") = Nothing
                ViewState("SessionMonth") = Session("MONTH")
                ViewState("SessionYear") = Session("YEAR")

                Dim strKeyName, strKeyValue As String
                strKeyName = String.Empty
                strKeyValue = String.Empty
                For Each KEY As String In Request.QueryString
                    If KEY Like "TREE*CODE" Then
                        strKeyName = KEY
                        strKeyValue = Trim(Request.QueryString(strKeyName))
                        Exit For
                    End If
                Next
                If strKeyName <> String.Empty Then
                    Session("SALESREP_LIST") = Report.GetSalesrepList(strKeyName.Replace("TREE_", ""), strKeyValue)
                End If
                Report.UpdateTreePath("SALESREP_CODE", Session("SALESREP_LIST"))

                If PreviousPage IsNot Nothing Then
                    With CriteriaCollector
                        Session.Item("Year") = .Year
                        Session.Item("Month") = .Month
                        intMonth = .Month
                        ViewState("SessionMonth") = Session("MONTH")
                        ViewState("SessionYear") = Session("YEAR")
                        session("PRINCIPAL_ID") = .PrincipalID
                        Session("PRINCIPAL_CODE") = .PrincipalCode
                        Session("SALESREP_CODE") = .SalesrepCode
                        Session("TREE_PATH") = .Tree_Path
                        ViewState("strSortExpression") = .SortExpression
                    End With
                Else
                    CriteriaCollector = Nothing
                    intMonth = CInt(Request.QueryString("MONTH"))
                    intYear = CInt(Request.QueryString("YEAR"))
                    If intYear = 0 Then intYear = Session("YEAR")

                    'Dim strTeamCode, strSalesrepCode, strRegionCode As String
                    ''ViewstaeValue : MONTH, CAT_CODE, SUB_CAT_CODE, CUST_CODE, CUST_GRP_CODE, TEAM_CODE, SALESREP_CODE
                    'strTeamCode = Trim(Request.QueryString("TEAM_CODE"))
                    'strRegionCode = Trim(Request.QueryString("REGION_CODE"))
                    'strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
                    'If Not String.IsNullOrEmpty(strTeamCode) Then session("TEAM_CODE") = strTeamCode
                    'If Not String.IsNullOrEmpty(strRegionCode) Then session("REGION_CODE") = strRegionCode
                    'If Not String.IsNullOrEmpty(strSalesrepCode) Then session("SALESREP_CODE") = strSalesrepCode

                End If

                ViewState("CAT_CODE") = Trim(Request.QueryString("CAT_CODE"))
                ViewState("SUB_CAT_CODE") = Trim(Request.QueryString("SUB_CAT_CODE"))
                ViewState("CUST_CODE") = Trim(Request.QueryString("CUST_CODE"))
                ViewState("CUST_GRP_CODE") = Trim(Request.QueryString("CUST_GRP_CODE"))

                'wuc_Menu.RestoreSessionState = True
                ViewState("MonthInThisPage") = intMonth
                ViewState("YearInThisPage") = intYear

                TimerControl1.Enabled = True
            End If

            ViewState("MonthInThisPage") = intMonth
            ViewState("YearInThisPage") = intYear
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    Try
    '        'wuc_ctrlPanel.RefreshDetails()
    '        'wuc_ctrlPanel.UpdateControlPanel()
    '        If IsPostBack Then RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ChangeReportType()
        Try
            ViewState("MonthInThisPage") = Session("MONTH")
        Catch ex As Exception
            ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
    '    Try
    '        UpdateQueryDate()
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub
    'Private Sub UpdateQueryDate()
    '    Try
    '        'activated when user change the selected month
    '        ViewState("MonthInThisPage") = Session("MONTH")
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".UpdateQueryDate : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlPanel.ExportToFile(dgList, "CallActivityInDetail")
            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
            End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Private Function GenerateAllColumnName(ByRef dtToBind As DataTable) As ArrayList
        Dim aryList As New ArrayList
        Try
            For Each DC As DataColumn In dtToBind.Columns
                aryList.Add(DC.ColumnName.ToUpper)
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".GenerateAllColumnName : " & ex.ToString)
        End Try
        Return aryList
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Dim aryAllColumnName As ArrayList = GenerateAllColumnName(dtToBind)

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            aryDataItem.Clear()
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_CallProdActDtl.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        'Select Case dtToBind.Columns(i).DataType().ToString
                        '    Case "System.Int32", "System.Double"
                        '        dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                        '        dgColumn.DataTextFormatString = "{0:0}"
                        '    Case Else
                        '        dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left 'HorizontalAlign.Center
                        'End Select
                        If String.IsNullOrEmpty(CF_CallProdActDtl.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CallProdActDtl.GetOutputFormatString(ColumnName)
                        End If

                        dgColumn.ItemStyle.HorizontalAlign = CF_CallProdActDtl.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallProdActDtl.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFields(3) As String
                        'Pass in 'CUST_CODE, VISIT_ID, SALESREP_CODE
                        'AS PARAM NAME: CUST_CODE , VISIT_ID, SALESREP_CODE , TXN_DATE
                        strUrlFields(0) = IIf(aryAllColumnName.Contains("CUST_CODE"), "CUST_CODE", "")
                        strUrlFields(1) = IIf(aryAllColumnName.Contains("VISIT_ID"), "VISIT_ID", "")
                        strUrlFields(2) = IIf(aryAllColumnName.Contains("SALESREP_CODE"), "SALESREP_CODE", "")
                        strUrlFields(3) = IIf(aryAllColumnName.Contains("DATE"), "DATE", "")

                        Dim strUrlFormatString As String = "CallProdDailyActivity.aspx?CUST_CODE={0}&VISIT_ID={1}&SALESREP_CODE={2}&TXN_DATE={3:yyyy-MM-dd}"

                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString

                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        'Select Case dtToBind.Columns(i).DataType().ToString
                        '    Case "System.Int32", "System.Double"
                        '        dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                        '        dgColumn.DataFormatString = "{0:0}"
                        '        dgColumn.HtmlEncode = False
                        '    Case Else
                        '        dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                        'End Select


                        '"DTLREMARK", "GNREMARK"
                        'If ColumnName Like "*REMARK*" Then
                        '    dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                        '    dgColumn.ItemStyle.Font.Size = FontUnit.XXSmall
                        '    dgColumn.ItemStyle.Width = Unit.Percentage(30)
                        'ElseIf ColumnName Like "*DATE*" Then
                        '    dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                        '    dgColumn.DataFormatString = "{0:yyyy-MM-dd}"
                        '    dgColumn.HtmlEncode = False
                        '    dgColumn.ItemStyle.Wrap = False
                        'ElseIf ColumnName Like "*CUST*CODE" Then
                        '    dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                        'End If

                        Dim strFormatString As String = CF_CallProdActDtl.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                            dgColumn.ItemStyle.Wrap = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallProdActDtl.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallProdActDtl.GetDisplayColumnName(ColumnName)
                        dgColumn.HeaderStyle.Wrap = False
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        ''Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
            aryDataItem = _aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try

            ''Month value accept 0, means YTD, show ALL month in the year
            'ViewstaeValue : MONTH, CAT_CODE, SUB_CAT_CODE, CUST_CODE, CUST_GRP_CODE, TEAM_CODE, SALESREP_CODE
            Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strCustCode, strCgCocde, strCatCode, strSubCat As String

            strUserID = Session("UserID")
            strPrincipalID = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            'strYear = Session("Year")
            strYear = CInt(ViewState("YearInThisPage"))
            If strYear = 0 Then strYear = Session("Year")
            strMonth = CInt(ViewState("MonthInThisPage"))
            strCustCode = ViewState("CUST_CODE")
            strCgCocde = ViewState("CUST_GRP_CODE")
            strCatCode = ViewState("CAT_CODE")
            strSubCat = ViewState("SUB_CAT_CODE")

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .PrincipalID = strPrincipalID
                .PrincipalCode = strPrincipalCode
                .Year = strYear
                .Month = strMonth
                .CatCode = strCatCode
                .SubCatCode = strSubCat
                .CustCode = strCustCode
                .CustGroup = strCgCocde
                .Tree_Path = Session("TREE_PATH")
            End With


            Dim clsCallDB As New rpt_CALL.clsCallQuery
            'Accepted param: strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strTeamCode, strRegionCode, strSalesrepCode, strCustCode, strCgCode, strCatCode, strSubCat
            DT = clsCallDB.GetCallProd_ACTDTLList(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, strCustCode, strCgCocde, strCatCode, strSubCat)

            aryAllColumnName = GenerateAllColumnName(DT)
            PreRenderMode(DT)
            'dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function


#Region "Footer Calculation"
    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
            'Cal_ItemFigureCollector()
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub
    'Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
    '    Try
    '        Select Case e.Row.RowType
    '            Case DataControlRowType.Header
    '            Case DataControlRowType.DataRow
    '                Dim iIndex As Integer

    '                For Each li As ListItem In licItemFigureCollector
    '                    iIndex = aryDataItem.IndexOf(li.Text)
    '                    If iIndex > 0 Then
    '                        li.Value = SUM(li.Value, DataBinder.Eval(e.Row.DataItem, li.Text))
    '                    End If
    '                Next
    '            Case DataControlRowType.Footer
    '                Dim iIndex As Integer

    '                For Each li As ListItem In licItemFigureCollector
    '                    iIndex = aryDataItem.IndexOf(li.Text)
    '                    If iIndex > 0 Then
    '                        e.Row.Cells(iIndex).Text = String.Format("{0:0}", li.Value)
    '                    End If
    '                Next
    '        End Select
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub
    'Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
    '    Try
    '        Dim iValue1 As Integer = ConvertToDouble(Value1)
    '        Dim iValue2 As Integer = ConvertToDouble(Value2)
    '        Return iValue1 + iValue2
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".SUM : " & ex.ToString)
    '    Finally
    '    End Try
    'End Function

    'Private Function ConvertToDouble(ByVal objValue As Object) As Double
    '    Try
    '        Dim dblValue As Double = 0
    '        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

    '        Return dblValue
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
    '    Finally
    '    End Try
    'End Function
    'Private Sub Cal_ItemFigureCollector()
    '    'used to get the collection list of column that need to sum the figure
    '    Try
    '        licItemFigureCollector = New ListItemCollection
    '        Dim strColumnName As String
    '        Dim liColumnField As ListItem
    '        For Each strColumnName In aryDataItem
    '            strColumnName = strColumnName.ToUpper
    '            Select Case strColumnName
    '                Case "Q1", "Q2", "Q3", "Q4", _
    '                     "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", _
    '                     "SEPT", "OCT", "NOV", "DEC", "YTD", "PYTD"
    '                    liColumnField = New ListItem(strColumnName, 0)
    '                    licItemFigureCollector.Add(liColumnField)
    '            End Select
    '        Next

    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub
    'Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated

    '    If e.Row.RowType = DataControlRowType.Header Then
    '        Dim oGridView As GridView = dgList 'CType(sender, GridView)
    '        Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
    '        Dim TC As TableHeaderCell
    '        Dim CF As ListItem

    '        For Each CF In licCustomHeaderCollector
    '            TC = New TableHeaderCell
    '            TC.Text = CF.Text
    '            TC.ColumnSpan = CF.Value
    '            GVR.Cells.Add(TC)
    '        Next
    '        oGridView.Controls(0).Controls.AddAt(0, GVR)
    '    End If
    'End Sub
#End Region

    'Private Sub Cal_CustomerHeader()
    '    Try
    '        'aryDataItem will refresh each time rebind
    '        'aryColumnFieldCollector will refresh only reget the data from the database
    '        licCustomHeaderCollector = New ListItemCollection

    '        Dim blnisNew As Boolean
    '        Dim strColumnName As String
    '        Dim liColumnField As ListItem
    '        Dim aryCuttedName As New ArrayList

    '        'collect the column name that ready to show on the screen
    '        'Cut the string with char '_CALLS' and '_WD'
    '        For Each strColumnName In aryDataItem
    '            strColumnName = strColumnName.ToUpper.Replace("CALLS", "").Replace("WD", "")
    '            strColumnName = strColumnName.Replace("_", "")
    '            strColumnName = strColumnName.Replace("SALESREP_NAME", "&nbsp;")
    '            strColumnName = strColumnName.Replace("Q", "Quarter ") '.Replace("Q1", "Quarter 1").Replace("Q1", "Quarter 1").Replace("Q1", "Quarter 1")
    '            aryCuttedName.Add(strColumnName)
    '        Next

    '        'witht the cutted columnName, fill in the Occorance count
    '        For Each strColumnName In aryCuttedName
    '            blnisNew = True
    '            liColumnField = licCustomHeaderCollector.FindByText(strColumnName)

    '            If Not liColumnField Is Nothing Then
    '                liColumnField.Value = CInt(liColumnField.Value) + 1
    '            Else
    '                liColumnField = New ListItem(strColumnName, 1)
    '                'liColumnField.Text = strColumnName
    '                'liColumnField.Value = 1
    '                licCustomHeaderCollector.Add(liColumnField)
    '            End If

    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RenewDataBind()
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
End Class

Public Class CF_CallProdActDtl
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strTblName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "AGENCY_CODE"
                strTblName = "Supplier Code"
            Case "AGENCY_NAME"
                strTblName = "Supplier"
            Case "DATE"
                strTblName = "Date"
            Case "QTY"
                strTblName = "Qty"
            Case "DEPARTMENT"
                strTblName = "Department"
            Case "GNREMARKS"
                strTblName = "Visit Remark"
            Case "DTLREMARKS"
                strTblName = "Remark"
            Case "ADDRESS"
                strTblName = "Address"
            Case "EXTRA_CAT_CODE"
                strTblName = "Extra Category Code"
            Case "EXTRA_CAT_NAME"
                strTblName = "Extra Category"
            Case Else
                strTblName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strTblName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If (strColumnName = "VISIT_ID") OrElse (strColumnName = "TEAM_CODE") OrElse (strColumnName = "REGION_CODE") OrElse (strColumnName = "REGION_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
                'ElseIf (strColumnName = "DATE") Then
                '    FCT = FieldColumntype.HyperlinkColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        Try
            Select Case strNewName
                Case "QTY"
                    strStringFormat = "{0:#,0}"
                Case "DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case Else
                    strStringFormat = ""
            End Select

        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" _
                 OrElse strColumnName Like "CLASS" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    BoundColumn_V2 = 6
'    InvisibleColumn = 99
'End Enum