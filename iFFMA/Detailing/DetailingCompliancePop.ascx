﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DetailingCompliancePop.ascx.vb" Inherits="iFFMA_Detailing_DetailingCompliancePop" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<asp:UpdatePanel ID="updPnlMaintenance1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 800px; padding: 15px"
            CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 92%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: right; width: 8%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button"
                                    CausesValidation="false" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="400px"
                    ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Detail">
                        <ContentTemplate>
                            <fieldset style="padding-left: 10px; width: 98%">
                                <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" />
                                <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                <!-- Begin Customized Content -->
                                <asp:DetailsView ID="DetailsView2" runat="server" AutoGenerateRows="False" Width="100%"
                                    BorderStyle="None" BorderWidth="0px" CellPadding="2" CellSpacing="1" DataKeyNames="COMPLIANCE_CODE"
                                    OnModeChanging="DetailsView2_ModeChanging">
                                    <FieldHeaderStyle VerticalAlign="Middle" HorizontalAlign="Left" CssClass="cls_label_header"
                                        Width="35%" Wrap="False" />
                                    <RowStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    <Fields>
                                        <asp:TemplateField HeaderText="Compliance Code">
                                            <EditItemTemplate>
                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                <asp:Label ID="lblComplianceCode" runat="server" Text='<%# Bind("COMPLIANCE_CODE")%>' CssClass="cls_label" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                <asp:Label ID="lblComplianceCode" runat="server" Text='<%# Bind("COMPLIANCE_CODE")%>' CssClass="cls_label" />
                                            </ItemTemplate>
                                            <InsertItemTemplate>
                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                <asp:TextBox ID="txtComplianceCode" runat="server" Text='<%# Bind("COMPLIANCE_CODE")%>'
                                                    CssClass="cls_textbox" MaxLength="50" />
                                                <span class="cls_label_err">*</span>
                                                <asp:RequiredFieldValidator ID="rfvComplianceCode" runat="server" ControlToValidate="txtComplianceCode"
                                                    ErrorMessage="Compliance Code is Required." ValidationGroup="SaveCompliance" Display="Dynamic"
                                                    CssClass="cls_validator" />
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Compliance Name">
                                            <EditItemTemplate>
                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                <asp:TextBox ID="txtComplianceName" runat="server" Text='<%# Bind("COMPLIANCE_NAME")%>'
                                                    CssClass="cls_textbox" MaxLength="100" Width="200px" />
                                                <span class="cls_label_err">*</span>
                                                <asp:RequiredFieldValidator ID="rfvSubCatName" runat="server" ControlToValidate="txtComplianceName"
                                                    ErrorMessage="Compliance Name is Required." ValidationGroup="SaveCompliance" Display="Dynamic"
                                                    CssClass="cls_validator" />
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                <asp:Label ID="lblComplianceName" runat="server" Text='<%# Bind("COMPLIANCE_NAME")%>' CssClass="cls_label" />
                                            </ItemTemplate>
                                            <InsertItemTemplate>
                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                <asp:TextBox ID="txtComplianceName" runat="server" Text='<%# Bind("COMPLIANCE_NAME")%>'
                                                    CssClass="cls_textbox" MaxLength="100" Width="200px" />
                                                <span class="cls_label_err">*</span>
                                                <asp:RequiredFieldValidator ID="rfvComplianceName" runat="server" ControlToValidate="txtComplianceName"
                                                    ErrorMessage="Compliance Name is Required." ValidationGroup="SaveCompliance" Display="Dynamic"
                                                    CssClass="cls_validator" />
                                            </InsertItemTemplate>
                                        </asp:TemplateField>
                                       
                                    </Fields>
                                </asp:DetailsView>
                                <asp:Panel ID="pnlEditMode" runat="server">
                                    <span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                                            <center>
                                                <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="SaveCompliance">
                                                </asp:Button>
                                            </center>
                                    </span>        
                                </asp:Panel>
                                <asp:Panel ID="pnlViewMode" runat="server" Visible="False">
                                     <span style="float: left; padding-top: 15px; padding-bottom: 30px;"><span style="float: left;
                                        width: 15%; padding-left: 2;" class="cls_label_header">Start Date : </span><span
                                            style="float: left; width: 30%; padding-left: 2;">
                                            <asp:Label ID="lblStartDate" runat="server" CssClass="cls_label" /></span> <span
                                                style="float: left; width: 15%; padding-left: 2;" class="cls_label_header">End Date
                                                : </span><span style="float: left; width: 30%; padding-left: 2;">
                                                    <asp:Label ID="lblEndDate" runat="server" CssClass="cls_label" /></span>
                                    </span><span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                                        <center>
                                        </center>
                                    </span>
                                </asp:Panel>
                                <asp:HiddenField ID="hdDetCode" runat="server" />
                                <asp:HiddenField ID="hdSubDetCode" runat="server" />
                                <asp:HiddenField ID="hdComplianceCode" runat="server" />
                            </fieldset>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance2" runat="server" BehaviorID="ModalPopupMaintenance2Behavior"
            TargetControlID="btnHidden" CancelControlID="imgClose" PopupControlID="pnlMsgPop"
            BackgroundCssClass="modalBackground" DropShadow="True" RepositionMode="RepositionOnWindowResizeAndScroll" />
         
    </ContentTemplate>
</asp:UpdatePanel>