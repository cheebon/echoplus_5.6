﻿Imports System.Data

Partial Class iFFMR_Customize_CustSalesAnalysis_CustSalesSummList
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustSalesSummList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            'Call Header
            With wuc_lblHeader
                .Title = "Customer Sales Infomation Summary" 'Report.GetName(SubModuleType.SALESSUMM) '"Summarized Sales Report"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.SALESSUMM
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""

            If Not IsPostBack Then
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")


                ViewState("dtCurrentView") = Nothing
                Dim strQueryString As String = String.Empty

                'Dim strTeamCode As String = Trim(Request.QueryString("TEAM_CODE"))
                'Dim strRegion As String = Trim(Request.QueryString("REGION_CODE"))
                'Dim strSalesrepCode As String = Trim(Request.QueryString("SALESREP_CODE"))

                ViewState("SelectedDate") = Trim(Request.QueryString("SELECTED_DATE"))
                Dim strPageIndicator As String = Trim(Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(strPageIndicator) AndAlso strPageIndicator = "ACTYSALESBYREP" Then
                    ViewState("POSTBACK_URL") = "~/iFFMR/KPI/ActSalesBySalesrep.aspx"
                Else 'SALESINFOBYDATE
                    Dim strPreviousPageQueryString As String = Session("SalesInfoByDate_QueryString")
                    ViewState("POSTBACK_URL") = "~/iFFMR/Sales/SalesInfoByDate.aspx?" & strPreviousPageQueryString
                End If

                'If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then
                '    btnBack.PostBackUrl = ViewState("POSTBACK_URL")
                '    btnBack.OnClientClick = String.Empty
                'End If

                'wuc_Menu.RestoreSessionState = True
                TimerControl1.Enabled = True
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "Event Handler"
    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    Try
    '        'wuc_ctrlpanel.RefreshDetails()
    '        'wuc_ctrlpanel.UpdateControlPanel()
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 20 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "SalesSummList")
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    'dgList.ShowFooter = False
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

#End Region

#Region "Custom dgList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CustSalesSummList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CustSalesSummList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CustSalesSummList.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CustSalesSummList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

                Case DataControlRowType.Footer
                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_CustSalesSummList.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim intNetValue As Integer
            Dim strYear, strMonth, strUserID, strTeamCode, strRegionCode, strSalesRepCode, strPrincipalId, strPrincipalCode As String
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            'strTeamCode = Session("TEAM_CODE")
            'strRegionCode = Session("REGION_CODE")
            'strSalesRepCode = Session("SALESREP_CODE")
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))


            Dim strSelectedDate, strChainCode, strChannelCode, strCustGrpCode, strCustCode, strPrdGrpCode, strPrdCode, _
            strSalesAreaCode, strShiptoCode As String

            strTeamCode = Trim(Request.QueryString("TEAM_CODE"))
            strRegionCode = Trim(Request.QueryString("REGION_CODE"))
            strSalesRepCode = Trim(Request.QueryString("SALESREP_CODE"))
            strSelectedDate = Trim(Request.QueryString("SELECTED_DATE"))
            strChainCode = Trim(Request.QueryString("CHAIN_CODE"))
            strChannelCode = Trim(Request.QueryString("CHANNEL_CODE"))
            strCustGrpCode = Trim(Request.QueryString("CUST_GRP_CODE"))
            strCustCode = Trim(Request.QueryString("CUST_CODE"))
            strPrdGrpCode = Trim(Request.QueryString("PRD_GRP_CODE"))
            strPrdCode = Trim(Request.QueryString("PRD_CODE"))
            strSalesAreaCode = Trim(Request.QueryString("SALES_AREA_CODE"))
            strShiptoCode = Trim(Request.QueryString("SHIPTO_CODE"))
            wuc_ctrlpanel.SelectedDateTimeString = strSelectedDate

            Dim strTreeSalesrep As String
            strTreeSalesrep = Session("SALESREP_LIST")

            Dim clsSalesDB As New rpt_Customize.clsCustSalesAnalysis
            DT = clsSalesDB.GetCustSalesAnalysisSummList( _
                strUserID, strPrincipalId, strPrincipalCode, strSelectedDate, _
                strTeamCode, strRegionCode, strSalesRepCode, _
                strChainCode, strChannelCode, strCustGrpCode, strCustCode, strPrdGrpCode, strPrdCode, _
                 strSalesAreaCode, strShiptoCode, _
                 intNetValue, strTreeSalesrep)

            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper

                If (strColumnName Like "SALES*" OrElse strColumnName Like "*QTY") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        TimerControl1.Enabled = False
        RenewDataBind()
    End Sub

End Class

Public Class CF_CustSalesSummList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "INV_NO"
                strFieldName = "Inv No."
            Case "SALES_QTY"
                strFieldName = "QTY"
            Case "FOC_QTY"
                strFieldName = "FOC"
            Case "SALES"
                strFieldName = "Sales Value"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case "SALES"
                    strStringFormat = "{0:#,0.00}"
                Case "FOC_QTY", "SALES_QTY"
                    strStringFormat = "{0:#,0}"
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class
