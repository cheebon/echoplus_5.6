<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MssContDtl.aspx.vb" Inherits="iFFMA_HCD_MssCont_MssContDtl" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%--<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>--%>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtDate" Src="~/include/wuc_txtdate_Grid.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mss Cont Detail</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
   function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('tcResult_TabPanel1_dglist');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState; }
   }
   function ConfirmSave() {if (confirm('Contact name already exists in the database! Are u sure you want to continue?')) {document.getElementById('Button2').click();}}
   function DistinctSelection(chkElement) {
       var isCheck = chkElement.checked;

       var dglist = document.getElementById('tcResult_TabPanel2_dgContGenList');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = false; }
       chkElement.checked = isCheck;
   }
</script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout"  >
    <form id="frmMssContDtl" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <input type="button" runat="server" id="btnback" value="Back" style="width: 80px"
                                        onclick="ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');ReloadIframe('ContentBarIframe');"
                                        class="cls_button" />
                                    <asp:Button ID="Button2" runat="server" Text="Button" style="display:none" OnClick="Button2_Click" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                                ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Mss Contact Detail List">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <div style="width: 98%; padding: 5px 5px 5px 5px;">
                                                                <input type="button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                                    style="width: 80px" validationgroup="ApproveSum" runat="server" visible="false" />
                                                                <input type="button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                                    style="width: 80px" validationgroup="ApproveSum" runat="server" visible="false" />
                                                                <asp:Button ID="btnvalidate" runat="server" Text="Validate Transaction" CssClass="cls_button"
                                                                    Visible="false" Width="120px" CausesValidation="false" ValidationGroup="ApproveSum" />
                                                                <asp:Button ID="btnapprove" runat="server" Text="Approve Selected" CssClass="cls_button"
                                                                    Visible="false" Width="120px" CausesValidation="false" ValidationGroup="ApproveSum"
                                                                    OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;" />
                                                            </div>
                                                            <asp:Label runat="server" ID="lblmsg" Text="" CssClass="cls_label_header" />
                                                            <div style="width: 98%; padding: 10px 10px 10px 10px;">
                                                               <%-- <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />--%>
                                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="QUES_CODE,SUB_QUES_CODE,CRITERIA_CODE_GRID,TYPE,MANDATORY_FLAG">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Select">
                                                                            <itemstyle horizontalalign="Center" />
                                                                            <itemtemplate>
                                                                            <asp:CheckBox id="chkselect" runat="server"></asp:CheckBox>
                                                                            </itemtemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <itemstyle horizontalalign="Center" />
                                                                            <itemtemplate>
                                                                             <asp:Button runat="server" ID="btnedit"  CommandName="Edit" Text="Edit" cssclass="cls_button" visible="True" width="50px"></asp:Button>
                                                                            <asp:Button runat="server" ID="btnupdate"  CommandName="Update" Text="Update" cssclass="cls_button" visible="True" width="50px"></asp:Button> 
                                                                            <asp:Button runat="server"  ID="btncancel"  CommandName="Cancel" Text="Cancel" cssclass="cls_button" visible="True" width="50px"></asp:Button>
                                                                             </itemtemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:BoundField DataField="QUES_NAME" HeaderText="Ques. Name" ReadOnly="True" SortExpression="QUES_NAME">
                                                                            <itemstyle horizontalalign="Left" />
                                                                        </asp:BoundField>
                                                                        
                                                                        <asp:BoundField DataField="SUB_QUES_NAME" HeaderText="Sub Ques. Name" ReadOnly="True"
                                                                            SortExpression="SUB_QUES_NAME">
                                                                            <itemstyle horizontalalign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CURRENT_VALUE" HeaderText="Current Value" ReadOnly="True"
                                                                            SortExpression="CURRENT_VALUE">
                                                                            <itemstyle horizontalalign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="New Value">
                                                                            <itemtemplate>
                                                                            <asp:Label ID="lblS1" runat="server" Text='<%#Bind("CRITERIA_NAME") %>' cssclass="cls_label"></asp:Label>
                                                                            <itemstyle horizontalalign="Left" />                                                             
                                                                            </itemtemplate>
                                                                            <edititemtemplate>          
                                                                                   <asp:TextBox ID="txtS1" runat="server" CssClass="cls_textbox" Visible="true" Text='<%#Bind("CRITERIA_NAME") %>' />
                                                                                    <asp:DropDownList ID="ddlS1" runat="server" CssClass="cls_dropdownlist" Visible="true" />
                                                                                   <customToolkit:wuc_txtDate ID="txtDateS1" runat="server" RequiredValidation="true"
                                                                                        RequiredValidationGroup="Save" DateTimeFormatString="yyyy-MM-dd" Visible="true" />
                                                                            </edititemtemplate>
                                                                            <itemstyle horizontalalign="Left" />
                                                                        </asp:TemplateField>
                                                                         <asp:BoundField DataField="UPDATED_FLAG" HeaderText="Status" ReadOnly="True" SortExpression="UPDATED_FLAG">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    </Columns>
                                                                </ccGV:clsGridView>
                                                            </div>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                            <asp:Panel ID="pnlContGenList" runat="server"> 
                                                            <div style="width: 98%; padding: 10px 10px 10px 10px;" >
                                                                <asp:Button ID="btnapproveDuplicate" runat="server" Text="Approve Selected" CssClass="cls_button"
                                                                    Visible="false" Width="120px" CausesValidation="false" ValidationGroup="ApproveDuplicate"
                                                                    OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;" />
                                                                <asp:Button ID="btnCancelDuplicate" runat="server" Text="Cancel" CssClass="cls_button"
                                                                    Visible="True" Width="120px" CausesValidation="false" ValidationGroup="ApproveDuplicate" />
                                                            </div>
                                                            <div style="width: 98%; padding: 10px 10px 10px 10px;">
                                                                <ccGV:clsGridView ID="dgContGenList" runat="server" AllowSorting="false" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="CONT_CODE">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Select">
                                                                            <itemstyle horizontalalign="Center" />
                                                                            <itemtemplate>
                                                                            <asp:CheckBox id="chkselect" runat="server" onclick="javascript:DistinctSelection(this);"></asp:CheckBox>
                                                                            </itemtemplate>
                                                                        </asp:TemplateField>
                                                               <%--          <asp:HyperLinkField DataNavigateUrlFields="CONT_CODE" DataNavigateUrlFormatString="#"
                                                                          DataTextField="CONT_CODE" HeaderText="Contact Code" SortExpression="CONT_CODE">
                                                                        </asp:HyperLinkField>--%>
                                                                        <asp:BoundField DataField="CONT_CODE" HeaderText="Contact Code" ReadOnly="True" SortExpression="CONT_CODE">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TITLE" HeaderText="Title" ReadOnly="True" SortExpression="TITLE">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FIRST_NAME" HeaderText="First name" ReadOnly="True" SortExpression="FIRST_NAME">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="LAST_NAME" HeaderText="Surname" ReadOnly="True" SortExpression="LAST_NAME">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="GENDER" HeaderText="Gender" ReadOnly="True" SortExpression="GENDER">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
<%--                                                                        <asp:BoundField DataField="POSITION_NAME" HeaderText="Position" ReadOnly="True" SortExpression="POSITION_NAME">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundField>
--%>                                                                    </Columns>
                                                                </ccGV:clsGridView>
                                                            </div>
                                                            </asp:Panel>
                                                     </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td style="height: 5px">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
