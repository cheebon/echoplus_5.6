﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FieldActyExtraSubCatList.ascx.vb" Inherits="iFFMA_FieldForceActy_FieldActyExtraSubCatList" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldActyExtraSubCatPop" Src="FieldActyExtraSubCatPop.ascx" %>

<asp:UpdatePanel ID="UpdatedgList" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="pnlCtrlAction" runat="server">
            <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
            <asp:Button ID="btnDelete" CssClass="cls_button" runat="server" Text="Delete" OnClientClick="if (!checkDelete()){return false; }" />
        </asp:Panel>
        <customToolkit:wuc_dgpaging ID="wuc_dgExtrSubCatPaging" runat="server" />
        <ccGV:clsGridView ID="dgExtrSubCatList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%--<asp:checkbox ID="chkSelect" runat="server" class="cls_checkbox"/>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </ccGV:clsGridView>

        <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
        <asp:HiddenField ID="hdCatCode" runat="server" Value="" />
        <asp:HiddenField ID="hdSubCatCode" runat="server" Value="" />

        <customToolkit:wuc_FieldActyExtraSubCatPop ID="wuc_FieldActyExtraSubCatPop" Title="Extra Sub Category Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
