﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Partial Class iFFMA_Product_ProductImage
    Inherits System.Web.UI.Page

    Public ReadOnly Property PrdCode() As String
        Get
            Return Request.QueryString("PrdCode")
        End Get

    End Property

    Public Property NewImagePath() As String
        Get
            Return ViewState("NewImagePath")
        End Get
        Set(ByVal value As String)
            ViewState("NewImagePath") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then 
        End If
    End Sub

    Protected Sub btnLoadImg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadImg.Click

        Dim strPath As String = ""
        Try
            If UploadPhoto(strPath) Then
                imgNewPrd.ImageUrl = strPath 
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function UploadPhoto(ByRef strPath As String) As Boolean
        'Modified- Soo Fong -VN-100289: FFMA - Product Info is not displayed
        'Dim strCustProfilePath As String = "/iFFMA/Product/StoreIMG/"
        Dim strCustProfilePath As String = "/Documents/Root/" + CStr(Web.HttpContext.Current.Session("FFMA_DB_NAME")) + "/ProductImage/"
        'Dim strGUID As String = System.Guid.NewGuid().ToString()
        Dim UploadFile As HttpPostedFile
        Try
            If fuPhoto.HasFile Then
                UploadFile = fuPhoto.PostedFile

                Dim strPostedFileName As String = UploadFile.FileName

                If (strPostedFileName <> String.Empty) Then
                    Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower
                End If

                If Not Directory.Exists(Server.MapPath("~" & strCustProfilePath)) Then
                    Directory.CreateDirectory(Server.MapPath("~" & strCustProfilePath))
                End If

                fuPhoto.SaveAs(Server.MapPath("~" & strCustProfilePath) & strPostedFileName)
                NewImagePath = strCustProfilePath & strPostedFileName

                strPath = "/" & System.Configuration.ConfigurationManager.AppSettings("ServerName") & strCustProfilePath & strPostedFileName

                UpdPnlImg.Update()
                Return (True)
                'lblUploadInfo.Text = ""
                'Dim tmpExt As String = fuPhoto.FileName.Split(".", 3, StringSplitOptions.RemoveEmptyEntries)(1)

                'If Not IsValidFileExtension(tmpExt) Then

                '    lblUploadInfo.Text = "Selected file's extension is not allowed."
                '    Return False


                'End If

                'If Not Directory.Exists(Server.MapPath("~" & strCustProfilePath)) Then
                '    Directory.CreateDirectory(Server.MapPath("~" & strCustProfilePath))
                'End If

                'Dim namePattern As String = System.Web.HttpContext.Current.Server.MapPath("~/") + "iFFMA\Product\StoreIMG\"
                'Dim strSearchPatternjpg As String = PrdCode.Trim + "-" + Portal.UserSession.UserID + "*_new.jpg"
                'For Each f As String In Directory.GetFiles(namePattern, strSearchPatternjpg)
                '    File.Delete(f)
                'Next
                'Dim strSearchPatternjpeg As String = PrdCode.Trim + "-" + Portal.UserSession.UserID + "*_new.jpeg"
                'For Each f As String In Directory.GetFiles(namePattern, strSearchPatternjpeg)
                '    File.Delete(f)
                'Next
                'Dim strSearchPatterngif As String = PrdCode.Trim + "-" + Portal.UserSession.UserID + "*_new.gif"
                'For Each f As String In Directory.GetFiles(namePattern, strSearchPatterngif)
                '    File.Delete(f)
                'Next
                'Dim strSearchPatternpng As String = PrdCode.Trim + "-" + Portal.UserSession.UserID + "*_new.png"
                'For Each f As String In Directory.GetFiles(namePattern, strSearchPatternpng)
                '    File.Delete(f)
                'Next

                'fuPhoto.SaveAs(Server.MapPath("~" & strCustProfilePath & PrdCode.Trim & "-" & Portal.UserSession.UserID & "-" & strGUID + "_new" & "." & tmpExt))
                'NewImagePath = Server.MapPath("~" & strCustProfilePath & PrdCode.Trim & "-" & Portal.UserSession.UserID & "-" & strGUID + "_new" & "." & tmpExt)
                'UpdPnlImg.Update()
                'Return save path
                'strPath = "/" & System.Configuration.ConfigurationManager.AppSettings("ServerName") & strCustProfilePath & PrdCode.Trim & "-" & Portal.UserSession.UserID & "-" & strGUID + "_new" & "." & tmpExt
                'Return (True)
            Else
                lblUploadInfo.Text = "Please select a file to upload."
            End If

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function IsValidFileExtension(ByVal strExtension As String) As Boolean
        Try
            If strExtension.ToUpper = "JPG" Or strExtension.ToUpper = "JPEG" Or strExtension.ToUpper = "PNG" Or strExtension.ToUpper = "GIF" Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function

    Protected Sub btnSaveImg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImg.Click 
        Try
            If Not IsNothing(NewImagePath()) Then
                SaveImg()
                NewImagePath() = Nothing
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "<script>alert('New image is saved!');</script>", False)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Refresh", "var ifra = self.parent.document.getElementById('frmProductList'); ifra.document.getElementById('tcResult_TabPanel4_btnRefresh').click();", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "<script>alert('Load an image to continue!');</script>", False)
            End If
            
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub SaveImg()
        Try
            'Modified- Soo Fong -VN-100289: FFMA - Product Info is not displayed
            'Dim FS As FileStream = New FileStream((NewImagePath), FileMode.Open, FileAccess.Read)
            'Dim img() As Byte = New Byte((FS.Length) - 1) {}
            'FS.Read(img, 0, Convert.ToInt32(FS.Length))

            'Dim clsProduct As New mst_Product.clsProduct
            'clsProduct.SavePrdImg(PrdCode, _fileBytes())

            Dim strConnString As String = CStr(Web.HttpContext.Current.Session("ffma_conn")) ' System.Configuration.ConfigurationManager.ConnectionStrings("Computer_Klubben_CommunitySiteConnectionString").ConnectionString
            Dim con As SqlConnection = New SqlConnection(strConnString)
            Dim strQuery As String = ("SPP_MST_PRD_IMG_SAVE")
            Dim cmd As SqlCommand = New SqlCommand(strQuery)
            cmd.Parameters.Add("@PRD_CODE", SqlDbType.Text).Value = PrdCode
            'Modified- Soo Fong -VN-100289: FFMA - Product Info is not displayed
            'cmd.Parameters.Add("@PRD_IMG", SqlDbType.Image).Value = img
            cmd.Parameters.Add("@PRD_IMG", SqlDbType.Text).Value = NewImagePath
            cmd.Parameters.Add("@USER_ID", SqlDbType.Text).Value = Portal.UserSession.UserID
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = con

           

            Try
                con.Open()
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Response.Write(ex.Message)
            Finally
                con.Close()
                con.Dispose()

                'Modified- Soo Fong -VN-100289: FFMA - Product Info is not displayed
                'FS.Close()
                'FS.Dispose()
                'FS = Nothing
            End Try

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

   
End Class
