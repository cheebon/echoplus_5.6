﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsSync
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function Sync(ByVal strPrincipalCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim crypto As New cor_Crypto.clsEncryption

        Try

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_SYNC_BY_SUPERADMIN"
                .addItem("PRINCIPAL_CODE", strPrincipalCode, cor_DB.clsDB.DataType.DBString)
                Sync = .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.Sync" & ex.ToString))
        Finally
            objDB = Nothing
            crypto = Nothing
        End Try
    End Function

    Public Sub UpdSync(ByVal strPrincipalCode As String)
        Dim objDB As cor_DB.clsDB
        Dim crypto As New cor_Crypto.clsEncryption

        Try

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_SYNC_UPD_BY_PRINCIPAL"
                .addItem("PRINCIPAL_CODE", strPrincipalCode, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.UpdSync" & ex.ToString))
        Finally
            objDB = Nothing
            crypto = Nothing
        End Try
    End Sub

    Public Sub UpdLevelDtl(ByVal strPrincipalID As String)
        Dim objDB As cor_DB.clsDB
        Dim crypto As New cor_Crypto.clsEncryption

        Try

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_SUMM_ADM_LEVEL_DTL_BY_SUPERADMIN"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.UpdLevelDtl" & ex.ToString))
        Finally
            objDB = Nothing
            crypto = Nothing
        End Try
    End Sub
    Public Sub UpdUserSalesrep(ByVal strPrincipalID As String)
        Dim objDB As cor_DB.clsDB
        Dim crypto As New cor_Crypto.clsEncryption

        Try

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_UPD_ADM_USER_SALESREP_BY_SUPERADMIN"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.UpdUserSalesrep" & ex.ToString))
        Finally
            objDB = Nothing
            crypto = Nothing
        End Try
    End Sub
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
