
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsFieldForceItineraryTransfer
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsFieldForceCust"
        End Get
    End Property

#Region "ITINERARY TRANSFER"

    Public Sub EditFieldForceItineraryTransfer(ByVal strTeamCode As String, ByVal strSourceFieldForceCode As String, ByVal strDestinationFieldForceCode As String, ByVal strActionCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALESREP_ITENARY_TRANSFER_EDIT"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SOURCE_SALESREP_CODE", strSourceFieldForceCode, cor_DB.clsDB.DataType.DBString)
                .addItem("DESTINATION_SALESREP_CODE", strDestinationFieldForceCode, clsDB.DataType.DBString)
                .addItem("ACTION_CODE", strActionCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
