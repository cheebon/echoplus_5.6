Imports System.Data

Partial Class iFFMA_FieldForce_SalesTgtPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property

    Public Property PrdGrpCode() As String
        Get
            Return Trim(hdPrdGrpCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdGrpCode.Value = value
        End Set
    End Property

    Public Property Year() As String
        Get
            Return Trim(hdYear.Value)
        End Get
        Set(ByVal value As String)
            hdYear.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsSalesTgt As New mst_FieldForce.clsSalesTgt

            DT = clsSalesTgt.GetSalesTgtDetails(TeamCode, SalesrepCode, PrdGrpCode, Year)
            If DT.Rows.Count > 0 Then
                
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Show()
            RaiseEvent CloseButton_Click(Page, Nothing)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrdGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdGrpSearch)
                .ResetPage()
                .TeamCode = TeamCode
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdGrpSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdGrpSearch.SelectButton_Click
        Try
            Dim txtPrdGrpCode As TextBox = CType(DetailsView1.FindControl("txtPrdGrpCode"), TextBox)
            txtPrdGrpCode.Text = wuc_PrdGrpSearch.PrdGrpCode

            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub SearchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdGrpSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdGrpSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdGrpSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtPrdGrpCode As TextBox = CType(DetailsView1.FindControl("txtPrdGrpCode"), TextBox)
            Dim lblPrdGrpCode As Label = CType(DetailsView1.FindControl("lblPrdGrpCode"), Label)
            Dim txtJanTgt As TextBox = CType(DetailsView1.FindControl("txtJanTgt"), TextBox)
            Dim txtFebTgt As TextBox = CType(DetailsView1.FindControl("txtFebTgt"), TextBox)
            Dim txtMarTgt As TextBox = CType(DetailsView1.FindControl("txtMarTgt"), TextBox)
            Dim txtAprTgt As TextBox = CType(DetailsView1.FindControl("txtAprTgt"), TextBox)
            Dim txtMayTgt As TextBox = CType(DetailsView1.FindControl("txtMayTgt"), TextBox)
            Dim txtJuneTgt As TextBox = CType(DetailsView1.FindControl("txtJuneTgt"), TextBox)
            Dim txtJulyTgt As TextBox = CType(DetailsView1.FindControl("txtJulyTgt"), TextBox)
            Dim txtAugTgt As TextBox = CType(DetailsView1.FindControl("txtAugTgt"), TextBox)
            Dim txtSeptTgt As TextBox = CType(DetailsView1.FindControl("txtSeptTgt"), TextBox)
            Dim txtOctTgt As TextBox = CType(DetailsView1.FindControl("txtOctTgt"), TextBox)
            Dim txtNovTgt As TextBox = CType(DetailsView1.FindControl("txtNovTgt"), TextBox)
            Dim txtDecTgt As TextBox = CType(DetailsView1.FindControl("txtDecTgt"), TextBox)

            txtJanTgt.Text = IIf(Trim(txtJanTgt.Text) = "", 0, Trim(txtJanTgt.Text))
            txtFebTgt.Text = IIf(Trim(txtFebTgt.Text) = "", 0, Trim(txtFebTgt.Text))
            txtMarTgt.Text = IIf(Trim(txtMarTgt.Text) = "", 0, Trim(txtMarTgt.Text))
            txtAprTgt.Text = IIf(Trim(txtAprTgt.Text) = "", 0, Trim(txtAprTgt.Text))
            txtMayTgt.Text = IIf(Trim(txtMayTgt.Text) = "", 0, Trim(txtMayTgt.Text))
            txtJuneTgt.Text = IIf(Trim(txtJuneTgt.Text) = "", 0, Trim(txtJuneTgt.Text))
            txtJulyTgt.Text = IIf(Trim(txtJulyTgt.Text) = "", 0, Trim(txtJulyTgt.Text))
            txtAugTgt.Text = IIf(Trim(txtAugTgt.Text) = "", 0, Trim(txtAugTgt.Text))
            txtSeptTgt.Text = IIf(Trim(txtSeptTgt.Text) = "", 0, Trim(txtSeptTgt.Text))
            txtOctTgt.Text = IIf(Trim(txtOctTgt.Text) = "", 0, Trim(txtOctTgt.Text))
            txtNovTgt.Text = IIf(Trim(txtNovTgt.Text) = "", 0, Trim(txtNovTgt.Text))
            txtDecTgt.Text = IIf(Trim(txtDecTgt.Text) = "", 0, Trim(txtDecTgt.Text))

            Dim clsSalesTgt As New mst_FieldForce.clsSalesTgt

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsSalesTgt.UpdateSalesTgt(lblPrdGrpCode.Text, Year, TeamCode, SalesrepCode, Trim(txtJanTgt.Text), Trim(txtFebTgt.Text), Trim(txtMarTgt.Text), Trim(txtAprTgt.Text), Trim(txtMayTgt.Text), Trim(txtJuneTgt.Text), Trim(txtJulyTgt.Text), Trim(txtAugTgt.Text), Trim(txtSeptTgt.Text), Trim(txtOctTgt.Text), Trim(txtNovTgt.Text), Trim(txtDecTgt.Text))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim isPrdGrpExists As Integer

                DT = clsSalesTgt.CreateSalesTgt(Trim(txtPrdGrpCode.Text), Year, TeamCode, SalesrepCode, Trim(txtJanTgt.Text), Trim(txtFebTgt.Text), Trim(txtMarTgt.Text), Trim(txtAprTgt.Text), Trim(txtMayTgt.Text), Trim(txtJuneTgt.Text), Trim(txtJulyTgt.Text), Trim(txtAugTgt.Text), Trim(txtSeptTgt.Text), Trim(txtOctTgt.Text), Trim(txtNovTgt.Text), Trim(txtDecTgt.Text))
                If DT.Rows.Count > 0 Then
                    isPrdGrpExists = DT.Rows(0)("IS_PRD_GRP_EXISTS")
                End If

                If isPrdGrpExists = 1 Then
                    lblInfo.Text = "This product group already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully created."
                    PrdGrpCode = Trim(txtPrdGrpCode.Text)
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view

            'ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

