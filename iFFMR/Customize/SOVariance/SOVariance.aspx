﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SOVariance.aspx.vb" Inherits="iFFMR_Customize_SOVariance_SOVariance" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <title>SO Variance</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
      <script type="text/javascript" language="javascript">
        function resizeLayout2() { var dgList = $get('div_dgList'); if (dgList) { dgList.scrollTop = 0; dgList.style.height = Math.max((getFrameHeight('TopBarIframe') - 70), 0) + 'px'; } }
        window.onresize = function() { resizeLayout2(); }

        function ScrollTop() {
            var dgList = document.getElementById('div_dgList');
            dgList.scrollTop = 0;
        }
        
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('DetailBar');HideElement('ContentBar');MaximiseFrameHeight('TopBarIframe');resetSize('div_dgList','TopBarIframe');">
    <form id="frmSOVariance" runat="server">
     <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" align="left">
                    <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional"  RenderMode="Inline" >
                        <ContentTemplate>
                            <asp:Panel ID="pnlTreeMenuControl" runat="server" >
                                <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Bckgroundreport" colspan="3">
                                             <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                <div style=" padding:5px 5px 5px 5px;">
                                                    <table   >
                                                        <tr>
                                                            <td width="150px"><asp:label id="lblSearchDateBy" cssclass=" cls_label" runat="server" Text="Search By:"/></td>
                                                            <td width="150px" >
                                                                <asp:DropDownList ID="ddlSearchDateBy" runat="server" CssClass=" cls_dropdownlist">
                                                                <asp:ListItem Selected="True" Text="Inv Date" Value="INV_DATE" ></asp:ListItem>
                                                                <asp:ListItem Text="SO Date" Value="SO_DATE" ></asp:ListItem>
                                                                </asp:DropDownList> 
                                                             <td width="150px"> <asp:label id="lblfrom" cssclass=" cls_label" runat="server" Text=" From: "/></td>
                                                             <td width="350px">  <customToolkit:wuc_txtCalendarRange ID="txtSearchDateValue" runat="server" RequiredValidation="true"
                                                                    RequiredValidationGroup="SOVarianceSearch" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true"  /></td>
                                                            </td>
                                                       </tr>
                                                        <tr>
                                                            <td><asp:label id="lblsalesrepcode" cssclass=" cls_label" runat="server" Text="Salesrep Code:"/></td>
                                                            <td >
                                                                <asp:TextBox ID="txtSalesrepCode" CssClass="cls_textbox" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td><asp:label id="lblCustCode" cssclass=" cls_label" runat="server" Text="Customer Code:"/></td>
                                                            <td>
                                                                <asp:TextBox ID="txtCustCode" CssClass="cls_textbox" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:label id="lblSONO" cssclass=" cls_label" runat="server" Text="SO No. :"/></td>
                                                            <td >
                                                                <asp:TextBox ID="txtSONO" CssClass="cls_textbox" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                            <td><asp:label id="lblPdaOrdDNo" cssclass=" cls_label" runat="server" Text="PDA. ORD. Code:"/></td>
                                                            <td >
                                                                <asp:TextBox ID="txtPdaOrdNo" CssClass="cls_textbox" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><asp:label id="lblStatus" cssclass=" cls_label" runat="server" Text="Status:"/></td>
                                                            <td><asp:DropDownList ID="ddlstatus" runat="server" CssClass=" cls_dropdownlist">
                                                                <asp:ListItem Selected="True" Text="All" Value="" ></asp:ListItem>
                                                                <asp:ListItem Text="Completed" Value="1" ></asp:ListItem>
                                                                <asp:ListItem Text="Outstanding" Value="0" ></asp:ListItem>
                                                                </asp:DropDownList> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                             <td colspan="4" ><asp:Button ID="btnsearch" runat="server" CssClass=" cls_button" Text="Search"  ValidationGroup="SOVarianceSearch"/>
                                                              <asp:Button ID="btnUpdate" runat="server" CssClass=" cls_button" Text="Update Selected"  ValidationGroup="SOVarianceUpdate"  />
                                                             </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                </ContentTemplate> 
                                               </asp:UpdatePanel> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <div style="padding:5px 5px 5px 5px">
                                                            <asp:Label ID="lblinfo" runat="server" Text="" CssClass=" cls_label_err"></asp:Label>
                                                        </div>
                                                        <asp:Timer  id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowPaging="false"
                                                                        AllowSorting="true" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                        GridHeight="455" RowSelectionEnabled="true" DataKeyNames="PDA_ORD_NO,SO_NO,INV_NO,USER_STATUS">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Select">
                                                                                <ItemTemplate>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
