'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	16/04/2007
'	Purpose	    :	Class CommSvrQuery
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.ComponentModel
Imports System.Configuration


Public Class clsCommSvrQuery

    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Private strConn As String = Web.HttpContext.Current.Session("echoplus_conn") '"server=" & ConfigurationSettings.AppSettings("server") & ";database=" & ConfigurationSettings.AppSettings("database") & ";User ID=" & ConfigurationSettings.AppSettings("userid") & ";password=" & ConfigurationSettings.AppSettings("password")

    Public clsProperties As New clsProperties.clsComm

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub GetCommSvrList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetCommSvrList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Dim strSQL As String

        Try
            strSQL = "SELECT comm_svr_setting_id, comm_svr_setting_code, comm_svr_setting_name, " & _
                            "changed_date, changed_user_id, active_flag " & _
                     "FROM TBL_ADM_COMM_SVR_SETTING " & _
                     "WHERE sync_operation <> 'D'"

            With clsProperties

                If Trim(.strCommCode) <> "" Then
                    strSQL += " AND comm_svr_setting_code like '" & .strCommCode & "%'"
                ElseIf Trim(.strCommName) <> "" Then
                    strSQL += " AND comm_svr_setting_name like '" & .strCommName & "%'"
                ElseIf Trim(.strActiveFlag) <> "" Then
                    strSQL += " AND active_flag = " & .strActiveFlag
                End If

            End With

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                dt = .Retrieve(strSQL)
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvrQuery.GetUserList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub GetCommSvrDetail
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetCommSvrDetail() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Dim strSQL As String

        Try
            With clsProperties
                strSQL = "SELECT *" & _
                         "FROM TBL_ADM_COMM_SVR_SETTING " & _
                         "WHERE comm_svr_setting_id = " & .strCommID
            End With

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                dt = .Retrieve(strSQL)
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvrQuery.GetCommSvrDetail" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetPwd
    ' Purpose	    :	Return Ecrypted password for matching
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetPwd() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Dim strSQL As String

        Try
            With clsProperties
                strSQL = "SELECT db_user_pwd, comm_svr_setting_id " & _
                         "FROM TBL_ADM_COMM_SVR_SETTING " & _
                         "WHERE comm_svr_setting_code = '" & .strCommCode & "' and sync_operation <> 'D'"
            End With

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                dt = .Retrieve(strSQL)
            End With

            GetPwd = dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvrQuery.GetPwd" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function UserExist
    ' Purpose	    :	Check against duplicate record
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function UserExist() As Boolean
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Dim strSQL As String
        UserExist = True

        Try
            With clsProperties
                strSQL = "SELECT COUNT(*) " & _
                         "FROM TBL_ADM_COMM_SVR_SETTING " & _
                         "WHERE comm_svr_setting_code = '" & .strCommCode & "' and sync_operation <> 'D'"
            End With

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                dt = .Retrieve(strSQL)
            End With

            Return (dt.Rows(0).Item(0) > 0)

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvrQuery.GetUserExist" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class

