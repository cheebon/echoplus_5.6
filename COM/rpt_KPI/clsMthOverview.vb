'clsRelPerform
'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	18/12/2005
'	Purpose	    :	Class to build Relative Performance Report from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsMthOverview
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMthOverview"
        End Get
    End Property

#Region "Monthly Overview"
    'SPP_RPT_MONTHLY_OVERVIEW_BY_TEAM     @USER_ID, @YEAR, @SELECTION, @PRINCIPAL_ID
    'SPP_RPT_MONTHLY_OVERVIEW_BY_SALESREP @USER_ID, @YEAR, @SELECTION, @PRINCIPAL_ID, @TEAM_CODE, @SALESREP_CODE
    'SPP_RPT_MONTHLY_OVERVIEW   @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @MONTH, @TEAM_CODE, @REGION_CODE @SALESREP_CODE, @SELECTION, @NET_VALUE
    Public Function GetMonthlyOverview(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, _
                                    ByVal strGroupField As String, ByVal intSelection As Integer, ByVal intNetValue As Integer) As DataTable
        Dim clsKpiDB As clsDB
        Try
            clsKpiDB = New clsDB
            With clsKpiDB
                .ConnectionString = strConn
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("SELECTION", CStr(intSelection), clsDB.DataType.DBInt)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                .CmdText = "SPP_RPT_MONTHLY_OVERVIEW"

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMonthOverview :" & ex.Message))
        Finally
            clsKpiDB = Nothing
        End Try
    End Function
#End Region

#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class

