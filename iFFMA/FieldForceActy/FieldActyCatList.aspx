<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FieldActyCatList.aspx.vb" Inherits="iFFMA_FieldForceActy_FieldActyCatList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldActySubCatList" Src="FieldActySubCatList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldActyCatPreviewPop" Src="FieldActyCatPreviewPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>SFMS Category List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script type="text/javascript" language="Javascript">
    	function SelectRow(chkSelected)
    	{
            //var chkSelected = window.event.srcElement;
            if (chkSelected.checked == false)
            {
                /*
                if (window.document.getElementById('tcResult$TabPanel2$wuc_FieldActySubCatList$dgSubCatList$ctl01$chkSelectAll') != null)
                {
                    window.document.getElementById('tcResult$TabPanel2$wuc_FieldActySubCatList$dgSubCatList$ctl01$chkSelectAll').checked = false
                }
                */
                for (var i=0; i < document.forms[0].elements.length; i++) 
                {
                    var e = document.forms[0].elements[i];
                                            
                    if (e.type == 'checkbox')
                    {
                        e.checked = chkSelected.checked;
                        break;
                    }
                }
            }
   	    }
    	  	
    	function SelectAllRows(chkAll)
    	{
    	    //var chkAll = window.event.srcElement; 
    	    
    	    for (var i=0; i < document.forms[0].elements.length; i++) 
            {
                var e = document.forms[0].elements[i];
                
                if (e.type == 'checkbox')
                {
                    e.checked = chkAll.checked;
                }
            }
    	}

    	function SelectAllRows_ExtraSubCat(chkAll, grid_id) {
    	  
    	    var grid = document.getElementById(grid_id);    	     	    
    	    for (var i = 1; i < grid.rows.length; i++) {
    	        var e = grid.rows[i].cells[0].getElementsByTagName("input")[0];

    	        if (e.type == 'checkbox') {
    	            e.checked = chkAll.checked;
    	        }
    	    }
    	}
        
        function checkDelete()
        {
            var isChecked = false;
            
            for (var i=0; i < document.forms[0].elements.length; i++) 
            {
                var e = document.forms[0].elements[i];
                
                if (e.type == 'checkbox' && e.checked)
                {
                    isChecked = true;
                    break;
                }
            }
        
            if (isChecked)
            {
                if (!confirm('Are you sure want to delete?'))
                {
                    //window.event.returnValue = false; 
                    return false;
                }
            }
            else
            {
                alert('Please select at least one of the record to delete!');
                //window.event.returnValue = false; 
                return false;
            }
            
            return true;
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSFMSCatList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                  
                                            <%--<span style="float:left; width:100%;">--%>                
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" 
                                                                                GridWidth="" AllowPaging="True" DataKeyNames="TEAM_CODE,CAT_CODE" 
                                                                                bordercolor="Black" borderwidth="1" GridBorderColor="Black" 
                                                                                GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Category Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtCatCode" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvCatCode" runat="server" ControlToValidate="txtCatCode"
                                                                                    ErrorMessage="Category Code is Required." ValidationGroup="Save" Display="Dynamic" CssClass="cls_validator" />
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Category Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtCatName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel> 
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                                          
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_FieldActySubCatList ID="wuc_FieldActySubCatList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>

                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>

        <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="This category cannot be deleted." runat="server" />
        <customToolkit:wuc_FieldActyCatPreviewPop ID="wuc_FieldActyCatPreviewPop" Title="Field Activity Preview" runat="server" />
    </form>
</body>
</html>
