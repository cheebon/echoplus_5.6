Partial Class wuc_Menu
    Inherits System.Web.UI.UserControl
    Public Event SelectedNodeChanged As EventHandler
    Public Event CloseThisPanel As EventHandler

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    'Public Shared RestoreSessionState As Boolean = False
    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_Menu"
        End Get
    End Property

#Region "PROPERTY"
    Public Property RestoreSessionState() As Boolean
        Get
            If ViewState("RestoreSessionState") Is Nothing Then ViewState("RestoreSessionState") = True
            Return CBool(ViewState("RestoreSessionState"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("RestoreSessionState") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                ScriptManager.RegisterClientScriptInclude(Page, Me.GetType, "JsWuc_Menu", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/Menu/wuc_Menu.js")

                Dim strUserID As String = Trim(HttpContext.Current.Session.Item("UserID"))
                Dim strCountryName As String = Trim(HttpContext.Current.Session.Item("currentCountryName"))
                Dim strPrincipleName As String = Trim(HttpContext.Current.Session.Item("currentPrincipalName"))

                If String.IsNullOrEmpty(strCountryName) Then strCountryName = HttpContext.Current.Session.Item("dflCountryName")
                If String.IsNullOrEmpty(strPrincipleName) Then strPrincipleName = HttpContext.Current.Session.Item("dflPrincipalName")

                LoadCountryList(strUserID)

                If ddlCountryID.Items.Count > 0 Then
                    If Not String.IsNullOrEmpty(strCountryName) AndAlso Not (ddlCountryID.Items.FindByText(strCountryName) Is Nothing) Then
                        ddlCountryID.Items.FindByText(strCountryName).Selected = True

                    End If
                    LoadPrinciple(strUserID, getComboValue(ddlCountryID.SelectedValue)(0))
                    If ddlPrincipalID.Items.Count > 0 AndAlso Not String.IsNullOrEmpty(strPrincipleName) AndAlso Not (ddlPrincipalID.Items.FindByText(strPrincipleName) Is Nothing) Then
                        ddlPrincipalID.Items.FindByText(strPrincipleName).Selected = True
                    End If
                End If

                RefreshSalesrepTreeList()

                'Else
                '    Dim IntOffsetHeight As Integer = IIf(CBool(CPE_Search.ClientState), 80, 170)
                '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OnLoad", "var pnlTreeMenu = $get(""wuc_Menu_pnlTreeMenu"");if (pnlTreeMenu)pnlTreeMenu.style.height=(document.documentElement.clientHeight-" & IntOffsetHeight & ");", True)
            End If

            'lblCountry.Text = HttpContext.Current.session("COUNTRY_NAME").ToString
            'lblPrinciple.Text = HttpContext.Current.session("PRINCIPAL_NAME").ToString
            'lblTeam.Text = HttpContext.Current.session("TEAM_NAME").ToString
            'lblSalesRep.Text = HttpContext.Current.session("SALESREP_NAME").ToString

        Catch ex As Exception
            ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        Finally
        End Try

    End Sub

#Region "pnlSearch"
    Private Sub LoadCountryList(ByVal strUserID As String)
        Dim clsUserData As adm_User.clsUserQuery = Nothing
        Try
            clsUserData = New adm_User.clsUserQuery
            Dim dtCountryList As Data.DataTable = clsUserData.GetCountryList(strUserID)
            With ddlCountryID
                .DataSource = dtCountryList
                .DataTextField = "country_name"
                '.DataValueField = "country_id"
                .DataValueField = "value"
                .DataBind()
            End With
        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadPrinciple : " & ex.ToString)
        Finally
            If clsUserData IsNot Nothing Then clsUserData = Nothing
        End Try

    End Sub

    Private Sub LoadPrinciple(ByVal strUserID As String, ByVal strCountryID As String)
        Dim clsUserData As adm_User.clsUserQuery = Nothing
        Try
            clsUserData = New adm_User.clsUserQuery
            Dim dtPrincipleList As Data.DataTable = clsUserData.GetPrincipleList(strUserID, strCountryID)
            If dtPrincipleList IsNot Nothing Then
                With ddlPrincipalID
                    .DataSource = dtPrincipleList
                    .DataTextField = "principal_name"
                    '.DataValueField = "principal_id"
                    .DataValueField = "value"
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadPrinciple : " & ex.ToString)
        Finally
            If clsUserData IsNot Nothing Then clsUserData = Nothing
        End Try
    End Sub

    Protected Sub ddlCountryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountryID.SelectedIndexChanged
        Try
            HttpContext.Current.Session.Item("currentCountryName") = Trim(ddlCountryID.SelectedItem.Text)

            RefreshPrinciple()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ddlCountryID_SelectedIndexChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ddlPrincipalID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrincipalID.SelectedIndexChanged
        Try
            HttpContext.Current.Session.Item("currentPrincipalName") = Trim(ddlPrincipalID.SelectedItem.Text)
            RefreshSalesrepTreeList()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ddlPrincipalID_SelectedIndexChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function IsSearchMatch(ByVal strToMatch As String, ByVal strPattern As String, Optional ByVal IsCaseSensitive As Boolean = False) As Boolean
        Try
            If Not IsCaseSensitive Then
                strPattern = strPattern.ToUpper
                strToMatch = strToMatch.ToUpper
            End If

            IsSearchMatch = strToMatch Like strPattern
        Catch ex As Exception
            ExceptionMsg(ClassName & ".IsSearchMatch : " & ex.ToString)
        Finally
        End Try
    End Function

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            tvUserViewList.CollapseAll()
            tvUserViewList.Nodes(0).Expand()
            ExpendWithQuery(Trim(txtSearch.Text).ToUpper, tvUserViewList.Nodes(0))
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnGo_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region " Tree View"

    Private Sub RefreshPrinciple()
        Try
            Dim strUserID As String = HttpContext.Current.Session.Item("UserID")
            Dim strCountryValues() As String = getComboValue(ddlCountryID.SelectedItem.Value)

            LoadPrinciple(strUserID, strCountryValues(0))
            RefreshSalesrepTreeList()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".RefreshPrinciple : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub RefreshSalesrepTreeList()
        Try
            Dim strUserID As String = HttpContext.Current.Session.Item("UserID")

            If ddlCountryID.Items.Count = 0 OrElse ddlPrincipalID.Items.Count = 0 Then Exit Sub

            Dim strCountryValues() As String = getComboValue(ddlCountryID.SelectedItem.Value)
            Dim strPricipleValues() As String = getComboValue(ddlPrincipalID.SelectedItem.Value)
            updateSessionCurrentCountry(ddlCountryID.SelectedItem.Text, strCountryValues(0), strCountryValues(1))
            updateSessionCurrentPrinciple(ddlPrincipalID.SelectedItem.Text, strPricipleValues(0), strPricipleValues(1))

            Dim strCountryID As String = strCountryValues(0)
            Dim strPrincipalID As String = strPricipleValues(0)
            Dim strPrincipalCode As String = strPricipleValues(1)

            UpdateDatabaseSetting(strUserID, strPrincipalID)
            'LoadSalesrepTreeList(strUserID, strCountryID, strPrincipalID)
            LoadSalesrepLevelList(strUserID, strCountryID, strPrincipalID, strPrincipalCode)

            'RefreshFrameScript(tvUserViewList.Nodes(0).Target)
            ActivateSelectedNodeChangeEvent(Me, Nothing)
            'tvUserViewList.Nodes(0).Select()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".RefreshSalesrepTreeList : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub LoadSalesrepLevelList(ByVal strUserID As String, ByVal strCountryID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String)
        Try
            tvUserViewList.Nodes.Clear()

            Dim clsUserInfo As New adm_User.clsUserQuery

            Dim dtTreeList As Data.DataTable = clsUserInfo.GetLevelMenuList(strUserID, strPrincipalCode)
            AddNode(tvUserViewList, dtTreeList, Nothing, String.Empty)

            Session("TREE_VIEW") = tvUserViewList

            If RestoreSessionState = True Then
                Dim tnNode As TreeNode = tvUserViewList.FindNode(Session("TREE_PATH"))
                If tnNode IsNot Nothing Then
                    ExpandThisNode(tnNode)
                    UpdateSessionValue(tnNode)
                ElseIf tvUserViewList.Nodes.Count > 0 Then
                    ExpandThisNode(tvUserViewList.Nodes(0))
                    UpdateSessionValue(tvUserViewList.Nodes(0))
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadSalesrepTreeList : " & ex.ToString)
        Finally
        End Try
    End Sub


    ''' <remarks>
    ''' NODE_ID     - Unique ID to determine row
    ''' NODE_CODE 	- Actual value Code (SR001)
    ''' NODE_NAME	- Actual value Name (Salesrep1)
    ''' PARENT_CODE - Parent's NODE_CODE (TM001)
    ''' PARENT_ID 	- Parent's Node_Id ()
    ''' LEVEL_IND 	- Org Level (1,2,3)
    ''' NODE_LEVEL 	- Actual Level Column Name (TEAM_CODE)
    ''' CHILD_LEVEL	- Next level Column Name (SALESREP_CODE)
    ''' </remarks>
    Private Function AddNode(ByRef tvTreeList As TreeView, ByRef dtData As Data.DataTable, ByRef tnParent As TreeNode, ByVal strParentID As String) As Integer
        'NODE_ID, NODE_CODE, NODE_NAME, 
        'PARENT_CODE, PARENT_ID, 
        'LEVEL_IND, NODE_LEVEL, CHILD_LEVEL

        Dim strNodeName, strNodeID, strNodeCode, strNodeColumnName, strChildColumnName, strLevelInd As String

        Dim strFilter As String = "PARENT_ID "
        strFilter += IIf(tnParent Is Nothing, "IS NULL OR PARENT_ID=0", " = " & strParentID)

        For Each ROW As Data.DataRow In dtData.Select(strFilter)
            strNodeName = Trim(ROW("NODE_NAME"))
            strNodeID = Trim(ROW("NODE_ID"))
            strNodeCode = Trim(ROW("NODE_CODE"))
            strNodeColumnName = Trim(ROW("NODE_LEVEL"))
            strChildColumnName = Trim(ROW("CHILD_LEVEL"))
            strLevelInd = Trim(ROW("LEVEL_IND"))

            Dim tnChild As New TreeNode
            With tnChild
                .Text = strNodeName
                .Value = strNodeColumnName & "@" & strNodeCode
                .Target = strNodeID & "@" & strChildColumnName
                .SelectAction = TreeNodeSelectAction.SelectExpand
            End With
            AddNode(tvTreeList, dtData, tnChild, strNodeID)
            If tnParent Is Nothing Then
                tvTreeList.Nodes.Add(tnChild)
                'UpdateSessionValue(tnChild)
            Else
                tnParent.ChildNodes.Add(tnChild)
            End If
        Next

    End Function

    Private Sub ExpandThisNode(ByVal tnSelectedNode As TreeNode)
        Try
            If tnSelectedNode IsNot Nothing Then
                tnSelectedNode.Expand()
                ExpandThisNode(tnSelectedNode.Parent)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ExpandThiNode : " & ex.ToString)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Split the Value into ID and Code. With the spliter "@"
    ''' </summary>
    ''' <param name="strIDandCode">ID@CODE</param>
    ''' <returns>[ID],[CODE]</returns>
    ''' <remarks></remarks>
    Private Function getComboValue(ByVal strIDandCode As String) As String()
        Dim strsCombo(5) As String

        Try
            Dim strValues() As String = strIDandCode.Split("@")

            For idx As Integer = 0 To strValues.Length - 1
                strsCombo(idx) = strValues(idx)
            Next
        Catch ex As Exception
            ExceptionMsg(ClassName & ".getComboValue : " & ex.ToString)
        Finally
        End Try

        Return strsCombo
    End Function

    Private Function ExpendWithQuery(ByVal strQuery As String, ByVal tnThisNode As TreeNode) As Boolean
        Try
            Dim blnFound As Boolean = False

            If tnThisNode.ChildNodes.Count > 0 Then
                For Each tnChildNode As TreeNode In tnThisNode.ChildNodes
                    blnFound = ExpendWithQuery(strQuery, tnChildNode)
                    If blnFound Then
                        'tnThisNode.Expand()
                        ExpandThisNode(tnThisNode)
                    End If
                Next
            Else
                blnFound = IsSearchMatch(Trim(tnThisNode.Text.ToUpper), strQuery)
            End If
            Return blnFound
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ExpandThisNodes : " & ex.ToString)
        Finally
        End Try
    End Function

    Protected Sub tvUserViewList_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvUserViewList.SelectedNodeChanged
        Try
            Dim tnSelectedNode As TreeNode = tvUserViewList.SelectedNode
            tvUserViewList.CollapseAll()
            ExpandThisNode(tnSelectedNode)
            If tnSelectedNode IsNot Nothing Then UpdateSessionValue(tnSelectedNode)

            'RefreshFrameScript(tnSelectedNode.Target)
            ActivateSelectedNodeChangeEvent(Me, e)

        Catch ex As Exception
            ExceptionMsg(ClassName & ".tvUserViewList_SelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ActivateSelectedNodeChangeEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RaiseEvent SelectedNodeChanged(Me, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ActivateSelectedNodeChangeEvent : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "Update Session Value"
    Private Enum SessionAction
        InsertValue = 0
        DeleteValue = 1
    End Enum
    Private Sub UpdateDatabaseSetting(ByVal strUserID As String, ByVal strPrincipalID As String)
        Dim dt As Data.DataTable
        Try
            Dim clsSetting As New adm_User.clsUserQuery
            dt = clsSetting.GetDbSetting(strUserID, strPrincipalID)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                Session("echoplus_conn") = _
              "server=" & Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_server_ip")), "", dt.Rows(0)("echoplus_server_ip"))) & _
              ";database=" & Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_db_name")), "", dt.Rows(0)("echoplus_db_name"))) & _
              ";User ID=" & Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_user_id")), "", dt.Rows(0)("echoplus_user_id"))) & _
              ";password=" & Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_user_pwd")), "", dt.Rows(0)("echoplus_user_pwd")))

                ''ffma
                Session("ffma_conn") = _
                "server=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffma_server_ip")), "", dt.Rows(0)("ffma_server_ip"))) & _
                ";database=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffma_db_name")), "", dt.Rows(0)("ffma_db_name"))) & _
                ";User ID=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffma_user_id")), "", dt.Rows(0)("ffma_user_id"))) & _
                ";password=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffma_user_pwd")), "", dt.Rows(0)("ffma_user_pwd")))

                ''ffmr
                Session("ffmr_conn") = _
                "server=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_server_ip")), "", dt.Rows(0)("ffmr_server_ip"))) & _
                ";database=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_db_name")), "", dt.Rows(0)("ffmr_db_name"))) & _
                ";User ID=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_user_id")), "", dt.Rows(0)("ffmr_user_id"))) & _
                ";password=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_user_pwd")), "", dt.Rows(0)("ffmr_user_pwd")))

                ''ffms
                Session("ffms_conn") = _
                "server=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffms_server_ip")), "", dt.Rows(0)("ffms_server_ip"))) & _
                ";database=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffms_db_name")), "", dt.Rows(0)("ffms_db_name"))) & _
                ";User ID=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffms_user_id")), "", dt.Rows(0)("ffms_user_id"))) & _
                ";password=" & Trim(IIf(IsDBNull(dt.Rows(0)("ffms_user_pwd")), "", dt.Rows(0)("ffms_user_pwd")))
                ''echoplus
                'Session("echoplus_server_ip") = Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_server_ip")), "", dt.Rows(0)("echoplus_server_ip")))
                'Session("echoplus_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_server_name")), "", dt.Rows(0)("echoplus_server_name")))
                'Session("echoplus_db_name") = Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_db_name")), "", dt.Rows(0)("echoplus_db_name")))
                'Session("echoplus_link_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_link_server_name")), "", dt.Rows(0)("echoplus_link_server_name")))
                'Session("echoplus_user_id") = Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_user_id")), "", dt.Rows(0)("echoplus_user_id")))
                'Session("echoplus_user_pwd") = Trim(IIf(IsDBNull(dt.Rows(0)("echoplus_user_pwd")), "", dt.Rows(0)("echoplus_user_pwd")))
                'Session("echoplus_conn") = "server=" & Session("echoplus_server_ip") & ";database=" & Session("echoplus_db_name") & ";User ID=" & Session("echoplus_user_id") & ";password=" & Session("echoplus_user_pwd")

                ''ffma
                'Session("ffma_server_ip") = Trim(IIf(IsDBNull(dt.Rows(0)("ffma_server_ip")), "", dt.Rows(0)("ffma_server_ip")))
                'Session("ffma_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffma_server_name")), "", dt.Rows(0)("ffma_server_name")))
                'Session("ffma_db_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffma_db_name")), "", dt.Rows(0)("ffma_db_name")))
                'Session("ffma_link_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffma_link_server_name")), "", dt.Rows(0)("ffma_link_server_name")))
                'Session("ffma_user_id") = Trim(IIf(IsDBNull(dt.Rows(0)("ffma_user_id")), "", dt.Rows(0)("ffma_user_id")))
                'Session("ffma_user_pwd") = Trim(IIf(IsDBNull(dt.Rows(0)("ffma_user_pwd")), "", dt.Rows(0)("ffma_user_pwd")))
                'Session("ffma_conn") = "server=" & Session("ffma_server_ip") & ";database=" & Session("ffma_db_name") & ";User ID=" & Session("ffma_user_id") & ";password=" & Session("ffma_user_pwd")

                ''ffmr
                'Session("ffmr_server_ip") = Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_server_ip")), "", dt.Rows(0)("ffmr_server_ip")))
                'Session("ffmr_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_server_name")), "", dt.Rows(0)("ffmr_server_name")))
                'Session("ffmr_db_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_db_name")), "", dt.Rows(0)("ffmr_db_name")))
                'Session("ffmr_link_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_link_server_name")), "", dt.Rows(0)("ffmr_link_server_name")))
                'Session("ffmr_user_id") = Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_user_id")), "", dt.Rows(0)("ffmr_user_id")))
                'Session("ffmr_user_pwd") = Trim(IIf(IsDBNull(dt.Rows(0)("ffmr_user_pwd")), "", dt.Rows(0)("ffmr_user_pwd")))
                'Session("ffmr_conn") = "server=" & Session("ffmr_server_ip") & ";database=" & Session("ffmr_db_name") & ";User ID=" & Session("ffmr_user_id") & ";password=" & Session("ffmr_user_pwd")

                ''ffms
                'Session("ffms_server_ip") = Trim(IIf(IsDBNull(dt.Rows(0)("ffms_server_ip")), "", dt.Rows(0)("ffms_server_ip")))
                'Session("ffms_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffms_server_name")), "", dt.Rows(0)("ffms_server_name")))
                'Session("ffms_db_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffms_db_name")), "", dt.Rows(0)("ffms_db_name")))
                'Session("ffms_link_server_name") = Trim(IIf(IsDBNull(dt.Rows(0)("ffms_link_server_name")), "", dt.Rows(0)("ffms_link_server_name")))
                'Session("ffms_user_id") = Trim(IIf(IsDBNull(dt.Rows(0)("ffms_user_id")), "", dt.Rows(0)("ffms_user_id")))
                'Session("ffms_user_pwd") = Trim(IIf(IsDBNull(dt.Rows(0)("ffms_user_pwd")), "", dt.Rows(0)("ffms_user_pwd")))
                'Session("ffms_conn") = "server=" & Session("ffms_server_ip") & ";database=" & Session("ffms_db_name") & ";User ID=" & Session("ffms_user_id") & ";password=" & Session("ffms_user_pwd")

            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateDatabaseSetting : " & ex.ToString)
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Sub UpdateSessionValue(ByRef tnSelectedNode As TreeNode)
        Try
            If tnSelectedNode IsNot Nothing Then
                Dim strTargetValue() As String = getComboValue(tnSelectedNode.Target)
                Dim strGrpName As String
                strGrpName = getComboValue(tnSelectedNode.Value)(0)
                strGrpName = IIf(strTargetValue.Length > 1 AndAlso strTargetValue(1) <> String.Empty, strTargetValue(1), strGrpName)

                Session("TREE_PATH") = tnSelectedNode.ValuePath
                Session("NODE_GROUP_NAME") = strGrpName
                Session("SALESREP_LIST") = GetSubTreeItemList(tnSelectedNode)


                UpdateSessionValues(tnSelectedNode, SessionAction.InsertValue)
                If tnSelectedNode.ChildNodes.Count > 0 Then UpdateSessionValues(tnSelectedNode.ChildNodes(0), SessionAction.DeleteValue)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateSessionValue : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Function GetSubTreeItemList(ByRef tnNode As TreeNode) As String
        Dim strList As New StringBuilder
        Try
            If tnNode.ChildNodes.Count > 0 Then
                For Each NODE As TreeNode In tnNode.ChildNodes
                    strList.Append(GetSubTreeItemList(NODE) & ",")
                Next
                strList.Remove(strList.Length - 1, 1)
            Else
                strList.Append("'" & getComboValue(tnNode.Value)(1) & "'")
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateSessionValue : " & ex.ToString)
        End Try
        Return strList.ToString
    End Function

    Private Function UpdateSessionValues(ByRef tnNode As TreeNode, ByVal Action As SessionAction) As Boolean
        Try
            '.Text		-	West Malaysia
            '.Value	    -	REGION_CODE@MY-WM
            '.Target	-	18@SALESREP_CODE	(NODE_ID, CHILD_COLUMN_NAME)

            Dim strSessionValue() As String = getComboValue(tnNode.Value)
            Dim strText, strCode, strName As String
            'strName    =   REGION_CODE
            'strCode    =   MY-WM
            'strText    =   West Malaysia
            strName = strSessionValue(0)

            If Action = SessionAction.InsertValue Then
                strCode = strSessionValue(1)
                strText = tnNode.Text
                If tnNode.Parent IsNot Nothing Then UpdateSessionValues(tnNode.Parent, Action)
            Else
                strCode = String.Empty
                strText = String.Empty
                If tnNode.ChildNodes.Count > 0 Then UpdateSessionValues(tnNode.ChildNodes(0), Action)
            End If
            Session(strName) = strCode
            Session(strName.Replace("_CODE", "_NAME")) = strText

        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateSessionValue : " & ex.ToString)
        End Try
    End Function

    'Private Sub UpdateSessionValues(ByVal tnSelectedNode As TreeNode)
    '    Try
    '        If tnSelectedNode IsNot Nothing Then
    '            Dim strName As String = Trim(tnSelectedNode.Text)
    '            Dim strValues() As String = getComboValue(tnSelectedNode.Value)
    '            Dim strID As String = strValues(0)
    '            Dim strCode As String = strValues(1)

    '            Select Case tnSelectedNode.Depth
    '                Case 0 'is Country-Principle node.
    '                    Dim strCountryValues() As String = getComboValue(ddlCountryID.SelectedItem.Value)
    '                    Dim strPricipleValues() As String = getComboValue(ddlPrincipalID.SelectedItem.Value)
    '                    updateSessionCurrentCountry(ddlCountryID.SelectedItem.Text, strCountryValues(0), strCountryValues(1))
    '                    updateSessionCurrentPrinciple(ddlPrincipalID.SelectedItem.Text, strPricipleValues(0), strPricipleValues(1))
    '                Case 1
    '                    If tnSelectedNode.ChildNodes.Count > 0 AndAlso tnSelectedNode.ChildNodes(0).ChildNodes.Count > 0 Then
    '                        'is SalesTeam
    '                        updateSessionCurrentSalesTeam(strName, strID, strCode)
    '                    Else
    '                        'is Region
    '                        UpdateSessionValues(tnSelectedNode.Parent)
    '                        updateSessionCurrentRegion(strName, strID, strCode)
    '                    End If
    '                Case 2
    '                    If tnSelectedNode.ChildNodes.Count > 0 Then
    '                        'is Region
    '                        UpdateSessionValues(tnSelectedNode.Parent)
    '                        updateSessionCurrentRegion(strName, strID, strCode)
    '                    Else
    '                        'is SalesRep
    '                        UpdateSessionValues(tnSelectedNode.Parent)
    '                        updateSessionCurrentSalesRep(strName, strID, strCode)
    '                    End If
    '                Case 3
    '                    'is SalesRep
    '                    UpdateSessionValues(tnSelectedNode.Parent)
    '                    updateSessionCurrentSalesRep(strName, strID, strCode)
    '            End Select
    '        End If
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".UpdateSessionValue : " & ex.ToString)
    '    Finally
    '    End Try

    'End Sub

    Private Sub updateSessionCurrentCountry(ByVal strName As String, ByVal strID As String, ByVal strCode As String)
        updateSessionCurrentPrinciple("", "", "")
        Session("COUNTRY_ID") = strID
        Session("COUNTRY_CODE") = strCode
        Session("COUNTRY_NAME") = strName
    End Sub
    Private Sub updateSessionCurrentPrinciple(ByVal strName As String, ByVal strID As String, ByVal strCode As String)
        Session("PRINCIPAL_ID") = strID
        Session("PRINCIPAL_CODE") = strCode
        Session("PRINCIPAL_NAME") = strName
    End Sub

    'Private Sub updateSessionCurrentSalesTeam(ByVal strName As String, ByVal strID As String, ByVal strCode As String)
    '    updateSessionCurrentRegion("", "", "")
    '    updateSessionCurrentSalesRep("", "", "")
    '    session("TEAM_CODE") = strCode
    '    session("TEAM_NAME") = strName
    'End Sub
    'Private Sub updateSessionCurrentRegion(ByVal strName As String, ByVal strID As String, ByVal strCode As String)
    '    session("REGION_CODE") = strCode
    '    session("REGION_NAME")= strName
    'End Sub
    'Private Sub updateSessionCurrentSalesRep(ByVal strName As String, ByVal strID As String, ByVal strCode As String)
    '    session("SALESREP_CODE") = strCode
    '    session("SALESREP_NAME") = strName
    'End Sub

#End Region

    'Private Sub lnkSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles lnkSearch.Click
    '    Dim MyHeight As UI.WebControls.Unit
    '    If pnlSearch.Visible = True Then
    '        pnlSearch.Visible = False
    '        MyHeight = New UI.WebControls.Unit(370) '(415) '(408) '(448)

    '    Else
    '        pnlSearch.Visible = True
    '        MyHeight = New UI.WebControls.Unit(285) '(315) '(296) '(376)
    '    End If
    '    pnlTreeMenu.Height = MyHeight
    'End Sub

    'Public Sub HideOrShow(ByVal blnShow As Boolean)
    '    'Try
    '    '    If blnShow Then
    '    '        pnlTreeMain.ScrollBars = ScrollBars.None
    '    '        fce.GetTargetProperties(pnlTreeMain).HorizontalOffset = 0
    '    '        fce.GetTargetProperties(pnlTreeMain).VerticalOffset = 0
    '    '        pnlTreeMenu.Height = New UI.WebControls.Unit(370)
    '    '    Else
    '    '        pnlTreeMain.ScrollBars = ScrollBars.Both
    '    '        fce.GetTargetProperties(pnlTreeMain).HorizontalOffset = 800
    '    '        fce.GetTargetProperties(pnlTreeMain).VerticalOffset = 0
    '    '    End If
    '    'Catch ex As Exception
    '    '    ExceptionMsg(ClassName & ".HideOrShow : " & ex.ToString)
    '    'Finally
    '    'End Try
    'End Sub

    'Protected Sub imgCloseMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCloseMenu.Click
    '    Try
    '        RaiseEvent CloseThisPanel(Me, e)
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".imgCloseMenu_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Property TreeMenu_Visibility() As Boolean
        Get
            Return Not (CPE_Menu.Collapsed)
        End Get
        Set(ByVal boolVisible As Boolean)
            CPE_Menu.ClientState = (Not boolVisible).ToString.ToLower
            CPE_Menu.Collapsed = Not boolVisible
        End Set
    End Property
End Class
