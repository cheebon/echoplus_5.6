'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	18/07/2006
'	Purpose	    :	Office Components Class
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On 
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports Excel = Microsoft.Office.Interop.Excel

'Interface IcorOB_Template
'Function Create() As Long
'Sub Update()
'Sub Delete()
'End Interface

'<Guid("368F4EC0-D9A0-4399-B793-494F9D165904"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsOffice
    'Inherits ServicedComponent
    'Implements IcorOB_Template
    Implements IDisposable

    Private mSQLString As String
    Private mConnString As String
    Private mVisible As String
    Private mFileName As String
    Private mSheetName As String
    Private mPivotFields As String

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    'Public clsProperties As New clsProperties.clsTemplate
    Public Property SQLString() As String
        Get
            Return mSQLString
        End Get
        Set(ByVal Value As String)
            mSQLString = Value
        End Set
    End Property

    Public Property ConnString() As String
        Get
            Return mConnString
        End Get
        Set(ByVal Value As String)
            mConnString = Value
        End Set
    End Property

    Public Property Visible() As Boolean
        Get
            Return mVisible
        End Get
        Set(ByVal Value As Boolean)
            mVisible = Value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal Value As String)
            mFileName = Value
        End Set
    End Property

    Private Function GetFieldOrientationIndex(ByVal intFieldOrientation As Integer) As Excel.XlPivotFieldOrientation
        Select Case intFieldOrientation
            Case 1
                GetFieldOrientationIndex = Excel.XlPivotFieldOrientation.xlColumnField
            Case 2
                GetFieldOrientationIndex = Excel.XlPivotFieldOrientation.xlDataField
            Case 3
                GetFieldOrientationIndex = Excel.XlPivotFieldOrientation.xlHidden
            Case 4
                GetFieldOrientationIndex = Excel.XlPivotFieldOrientation.xlPageField
            Case 5
                GetFieldOrientationIndex = Excel.XlPivotFieldOrientation.xlRowField
            Case Else
                GetFieldOrientationIndex = Excel.XlPivotFieldOrientation.xlDataField
        End Select
        Exit Function
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub addPivotFields
    ' Purpose	    :	Add Pivot Fields
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: 
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub addPivotFields(ByVal strFieldName As String, ByVal intFieldOrientation As Integer) 'Implements IcorOB_Template.Create 
        Try
            If strFieldName <> "" And intFieldOrientation <> 0 Then
                mPivotFields = mPivotFields & strFieldName & "," & intFieldOrientation & "|*"
            Else
                mPivotFields = mPivotFields & "" & "," & intFieldOrientation & "|*"
            End If

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_Office.clsOffice.addPivotFields : " & ex.ToString))
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub addSheetName
    ' Purpose	    :	Add Pivot Fields
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: 
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub addSheetName(ByVal strSheetName As String) 'Implements IcorOB_Template.Create 
        Try
            If strSheetName <> "" Then
                mSheetName = mSheetName & strSheetName & ","
            Else
                mSheetName = mSheetName & "" & ","
            End If

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_Office.clsOffice.addSheetName : " & ex.ToString))
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GeneratePivot
    ' Purpose	    :	Generate Pivot Table
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: SQLString,ConnString,Visible,FileName,SheetName
    '		            [out]      : Excel Output File
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GeneratePivot(Optional ByVal intPivotType As Integer = 0) As Boolean 'Implements IcorOB_Template.Create
        Dim xlApp As New Excel.Application
        Dim xlBook As Excel.Workbook
        Dim xlSheet, xlSheet1 As Excel.Worksheet
        'Dim xlSheets As Excel.Worksheets
        Dim ptCache As Excel.PivotCache
        Dim ptTable As Excel.PivotTable
        Dim aryPivotFields, aryPivotFieldValue, arySheet As Array
        Dim objQryTable As Object
        Dim xllastcell As String
        Dim i As Integer

        Try
            'xlApp = New Excel.Application
            xlBook = xlApp.Workbooks.Add
            xlSheet = xlBook.Sheets.Add
            arySheet = Split(mSheetName, ",")
            xlSheet.Name = arySheet(0)
            xlApp.Visible = False

            Select Case intPivotType
                Case "1" 'Offline
                    'Populate Data
                    xlSheet1 = xlBook.Sheets.Add
                    xlSheet1.Name = arySheet(1)
                    objQryTable = xlSheet1.QueryTables.Add(mConnString, xlSheet1.Range("A1"), mSQLString)
                    objQryTable.RefreshStyle = 2 ' xlInsertEntireRows = 2
                    objQryTable.Refresh(False)
                    xllastcell = xlSheet1.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Address
                    ptCache = xlBook.PivotCaches.Add(SourceType:=Excel.XlPivotTableSourceType.xlDatabase, SourceData:=xlSheet1.Range("A1:" & xllastcell))
                    xlSheet1.Cells.EntireColumn.AutoFit()

                Case Else
                    'Create the Pivotcache.
                    ptCache = xlBook.PivotCaches.Add(SourceType:=Excel.XlPivotTableSourceType.xlExternal)
                    With ptCache
                        .Connection = mConnString
                        .CommandText = mSQLString
                        .CommandType = Excel.XlCmdType.xlCmdSql
                    End With
            End Select

            'Create the Pivot table.
            ptTable = xlSheet.PivotTables.Add(PivotCache:=ptCache, TableDestination:=xlSheet.Range("A1"), TableName:="PivotTable1")
            With ptTable
                .Format(Excel.XlPivotFormatType.xlTable2)
                .ManualUpdate = True
                aryPivotFields = Split(mPivotFields, "|*")
                For i = 0 To UBound(aryPivotFields) - 1
                    aryPivotFieldValue = Split(aryPivotFields(i), ",")
                    .PivotFields(aryPivotFieldValue(0)).Orientation = GetFieldOrientationIndex(aryPivotFieldValue(1))
                Next
                .ManualUpdate = False
            End With

            'xlBook.ActiveSheet.Cells.EntireColumn.AutoFit()
            xlSheet.Activate()
            xlSheet.Cells.EntireColumn.AutoFit()
            xlApp.Visible = mVisible

            If mFileName <> "" Then
                xlBook.SaveAs(mFileName)
            End If
            Return True

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_Office.clsOffice.GeneratePivot : " & ex.ToString))
        Finally
            objQryTable = Nothing
            xlSheet1 = Nothing
            xlSheet = Nothing
            xlBook = Nothing
            xlApp.Quit()
            xlApp = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = msg
                .Log()
            End With
            objLog = Nothing
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
