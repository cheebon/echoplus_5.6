<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerList.aspx.vb" Inherits="CustomerList" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_pnlRecordNotFound.ascx" TagName="wuc_pnlRecordNotFound" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer List</title>
    <link rel="stylesheet" href="~/include/DKSH.css" />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmCustomerList" method="post" runat="server" defaultbutton="btnSearch" defaultfocus="txtSearchCriteria">
      <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
       <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="left">
                    <table cellpadding="1" cellspacing="3" width="100%" border="0" class="cls_panel">
                        <tr valign="top">
                            <td style="width: 20%;">
                                <asp:Label ID="lblSearchBy" runat="server" CssClass="cls_label_header">Search By</asp:Label></td>
                            <td style="width: 2%">
                                <asp:Label ID="lblDot" runat="server">:</asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlSearchCriteria" runat="server" CssClass="cls_dropdownlist">
                                    <asp:ListItem Value="0">Customer Code</asp:ListItem>
                                    <asp:ListItem Value="1">Customer Name</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr valign="top">
                            <td style="width: 20%;">
                                <asp:Label ID="lblCriteria" runat="server" CssClass="cls_label_header">Criteria</asp:Label></td>
                            <td style="width: 2%">
                                <asp:Label ID="lblDot2" runat="server">:</asp:Label>
                            </td>
                            <td style="vertical-align: baseline" align="left">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="vertical-align: baseline" align="left">
                                            <asp:TextBox ID="txtSearchCriteria" runat="server" CssClass="cls_textbox"></asp:TextBox>&nbsp;
                                            <asp:Button ID="btnSearch" runat="server" CssClass="cls_button" Text="Search" />&nbsp;<br />
                                        </td>
                                        <td style="vertical-align: baseline; width: 100%" align="left">
                                            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                            <asp:Label ID="Label1" runat="server" CssClass="cls_label">Eg: "Ec*" to search "Echoplus"</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please provide search criteria!"
                                                ControlToValidate="txtSearchCriteria" Display="Dynamic" CssClass="cls_label_err"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <%--<tr>
                <td align="center" style="width: 100%;" class="Bckgroundreport">
                </td>
            </tr>--%>
            <tr>
                <td align="center" class="Bckgroundreport">
                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server" ></uc1:wuc_dgpaging>
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True"
                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="430px"
                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                FreezeRows="0" GridWidth="" AllowPaging="True" PagerSettings-Visible="false" 
                                RowHighlightColor="AntiqueWhite" BorderColor="Black" BorderWidth="1" GridBorderColor="Black" 
                                GridBorderWidth="1px" DataKeyNames="CUST_CODE">
                                <EmptyDataTemplate>
                                    <customToolkit:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound1" runat="server" ShowPanel="true" />
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField HeaderText="Customer Code" DataField="CUST_CODE" SortExpression="CUST_CODE"
                                        ReadOnly="True" >
                                        <itemstyle horizontalalign="CENTER" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Name" DataField="CUST_NAME" SortExpression="CUST_NAME"
                                        ReadOnly="True"  >
                                        <itemstyle horizontalalign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Address" DataField="ADDRESS" SortExpression="ADDRESS"
                                        ReadOnly="True"  >
                                        <itemstyle horizontalalign="Left" />
                                    </asp:BoundField>
<%--                                    <asp:TemplateField HeaderText="Address">
                                        <itemstyle horizontalalign="Left" />
                                        <itemtemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# EVAL("Add1") & " " &  EVAL("ADD2") & " " &  EVAL("POSTCODE") & " " & EVAL("ADD3") %>'></asp:Label>
                                
</itemtemplate>
                                    </asp:TemplateField>
--%>                                <asp:TemplateField HeaderText="Select">
                                        <itemtemplate>
                                            <%--<input id="btnSelect" class="cls_button" onclick="PickCodeUom('<%# trim(Container.DataItem("CUST_CODE")) %>','<%#  trim(Strings.Replace(Container.DataItem("CUST_NAME"), "'", "^"))  %>')"
                                            type="button" value="Select" />
                                            --%>
                                            <asp:Button id="btnSelect"  runat="server" CssClass="cls_button" Text="Select" CommandArgument='<%# trim(Container.DataItem("CUST_CODE")) %> ' CommandName="select" CausesValidation="false"></asp:Button>
                                        </itemtemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </ccGV:clsGridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr class="Bckgroundreport">
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </form>
    <%'List function called by in-line scripts%>
    
    <asp:Literal ID="ltr_ScriptPlaceholder" runat="server"></asp:Literal>
    <%--<script type="text/javascript" language="JavaScript">
	    //select a appropriate product and return back to parent form
	    function PickCodeUom(code, name) {
		    //update product code & name into the parent window by calling parent function
		    origcode = /\^/gi;  //replace from ^ to ' with global and ignored 
		    newname = name.replace(origcode, "'"); 
    		
        if (window.opener && !window.opener.closed)
        {
		    window.opener.updateShiptoInfo(code, newname );
		}
		    //close the window
		    window.close();
	    } 
    </script>--%>

</body>
</html>

