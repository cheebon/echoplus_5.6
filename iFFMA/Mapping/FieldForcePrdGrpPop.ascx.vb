Imports System.Data

Partial Class iFFMA_Mapping_FieldForcePrdGrpPop
    Inherits System.Web.UI.UserControl

    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

    Public Event SelectButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

            'Call Paging
            'With wuc_dgPrdGrpPopPaging
            '    '.PageCount = dgPrdGrpPopList.PageCount
            '    '.CurrentPageIndex = dgPrdGrpPopList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not Page.IsPostBack Then
                DataBind()
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "PROPERTY"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()

        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgPrdGrpPopList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgPrdGrpPopList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgPrdGrpPopList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function

    Public Sub ResetPage()
        txtPrdGrpCode.Text = ""
        txtPrdGrpName.Text = ""

        updPnlMaintenance.Update()
    End Sub
#End Region

#Region "EVENTS"
    Public Overrides Sub DataBind()

        lblContent.Text = strMessage
        lblTitle.Text = strTitle

        updPnlMaintenance.Update()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RenewDataBind()
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    RaiseEvent CloseButton_Click(sender, e)
    'End Sub

    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            lblInfo.Text = ""

            Dim strDataKeyList As String
            strDataKeyList = Trim(GetSelectedString())
            If strDataKeyList.Length = 0 Then lblInfo.Text = "Please select at least 1 item to proceed!" : Show() : Exit Sub 'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "<script>alert('Please select at least 1 item to proceed!');</script>", False) : Exit Sub

            Dim clsFieldForcePrdGrp As New mst_Mapping.clsFieldForcePrdGrp

            clsFieldForcePrdGrp.CreateFieldForcePrdGrp(strDataKeyList, TeamCode, SalesrepCode)

            lblInfo.Text = "The selected item(s) was successfully added! Please click on Close button when finish assigning."
            RenewDataBind()

            RaiseEvent SelectButton_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgPrdGrpPopList.PageIndex = 0
        wuc_dgPrdGrpPopPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldForcePrdGrp As New mst_Mapping.clsFieldForcePrdGrp
            Dim strAgencyCode As String
            strAgencyCode = ddlAgencyCode.SelectedValue

            DT = clsFieldForcePrdGrp.GetFieldForcePrdGrpExcludeList(txtPrdGrpCode.Text, txtPrdGrpName.Text, TeamCode, SalesrepCode, strAgencyCode)
            dgPrdGrpPopList.DataKeyNames = New String() {"PRD_GRP_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgPrdGrpPopList"
    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgPrdGrpPopList.PageIndex = 0
            '    wuc_dgPrdGrpPopPaging.PageNo = 1 'HL:20080327
            'End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgPrdGrpPopList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .DataBind()
            End With


            'Call Paging
            With wuc_dgPrdGrpPopPaging
                .PageCount = dgPrdGrpPopList.PageCount
                .CurrentPageIndex = dgPrdGrpPopList.PageIndex
                .RowCount = dvCurrentView.Count
                .DataBind()
                .Visible = IIf(dgPrdGrpPopList.Rows.Count > 0, True, False)
            End With

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            updPnlMaintenance.Update()
            RaiseEvent CloseButton_Click(Page, Nothing)
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgPrdGrpPopList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgPrdGrpPopList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()

            'CHECKBOX
            aryDataItem.Add("chkSelect")
            While dgPrdGrpPopList.Columns.Count > 1
                dgPrdGrpPopList.Columns.RemoveAt(1)
            End While
            dgPrdGrpPopList.Columns(0).HeaderStyle.Width = "25"
            dgPrdGrpPopList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldForcePrdGrpPop.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldForcePrdGrpPop.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldForcePrdGrpPop.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldForcePrdGrpPop.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldForcePrdGrpPop.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgPrdGrpPopList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgPrdGrpPopList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgPrdGrpPopList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgPrdGrpPopList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgPrdGrpPopList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If chk Is Nothing Then
                    chk = New CheckBox
                    chk.ID = "chkSelect"
                    e.Row.Cells(0).Controls.Add(chk)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgPrdGrpPopPaging.Go_Click
        Try
            dgPrdGrpPopList.PageIndex = CInt(wuc_dgPrdGrpPopPaging.PageNo - 1)

            dgPrdGrpPopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgPrdGrpPopPaging.Previous_Click
        Try
            If dgPrdGrpPopList.PageIndex > 0 Then
                dgPrdGrpPopList.PageIndex = dgPrdGrpPopList.PageIndex - 1
            End If
            wuc_dgPrdGrpPopPaging.PageNo = dgPrdGrpPopList.PageIndex + 1

            dgPrdGrpPopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgPrdGrpPopPaging.Next_Click
        Try
            If dgPrdGrpPopList.PageCount - 1 > dgPrdGrpPopList.PageIndex Then
                dgPrdGrpPopList.PageIndex = dgPrdGrpPopList.PageIndex + 1
            End If
            wuc_dgPrdGrpPopPaging.PageNo = dgPrdGrpPopList.PageIndex + 1

            dgPrdGrpPopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Public Sub LoadSupplierDDL()

        Dim dt As DataTable = Nothing
        Dim clsDDL As New mst_Common.clsDDL
        dt = clsDDL.GetSupplierDDL(TeamCode)
        If dt.Rows.Count > 0 Then
            With ddlAgencyCode
                .Items.Clear()
                .DataSource = dt.DefaultView
                .DataTextField = "SUPPLIER_NAME"
                .DataValueField = "SUPPLIER_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(IsDBNull(dt.Rows(0)("SUPPLIER_CODE")), "", dt.Rows(0)("SUPPLIER_CODE"))
            End With
        End If
        
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_FieldForcePrdGrpPop
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "PRD_GRP_CODE"
                strFieldName = "Product Group Code"
            Case "PRD_GRP_NAME"
                strFieldName = "Product Group Name"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            FCT = FieldColumntype.BoundColumn

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*QTY" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" Then
                strFormatString = "{0:#,0.00}"
            ElseIf strColumnName Like "*DBL" Then
                strFormatString = "{0:#,0.0000}"
            ElseIf strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName = "BLANK" Then
                strFormatString = "{0:#,0}"
            Else
                strFormatString = ""
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

