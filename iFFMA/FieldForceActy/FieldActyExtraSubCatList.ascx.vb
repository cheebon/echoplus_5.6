﻿
Imports System.Data


Partial Class iFFMA_FieldForceActy_FieldActyExtraSubCatList
    Inherits System.Web.UI.UserControl

#Region "Variable"

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property CatCode() As String
        Get
            Return Trim(hdCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdCatCode.Value = value
        End Set
    End Property

    Public Property SubCatCode() As String
        Get
            Return Trim(hdSubCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubCatCode.Value = value
        End Set
    End Property


#End Region


#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgExtrSubCatList.PageIndex = 0
        wuc_dgExtrSubCatPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try 
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
            DT = clsFieldActy.GetExtraSubCatList(TeamCode, CatCode, SubCatCode) 
            dgExtrSubCatList.DataKeyNames = New String() {"TEAM_CODE", "CAT_CODE", "SUB_CAT_CODE", "EXTRA_CAT_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgSubCatList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try

            dtCurrentTable = GetRecList()
             

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgExtrSubCatList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgExtrSubCatPaging
                .PageCount = dgExtrSubCatList.PageCount
                .CurrentPageIndex = dgExtrSubCatList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatedgList.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgSubCatList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubCatList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            'dgExtrSubCatList.Columns.Clear()

            'CHECKBOX
            aryDataItem.Add("chkSelect")
            While dgExtrSubCatList.Columns.Count > 1
                dgExtrSubCatList.Columns.RemoveAt(1)
            End While
            dgExtrSubCatList.Columns(0).HeaderStyle.Width = "25"
            dgExtrSubCatList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            'ADD BUTTON EDIT
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgExtrSubCatList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If


            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldActyExtraSubCatList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldActyExtraSubCatList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldActyExtraSubCatList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldActyExtraSubCatList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldActyExtraSubCatList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgExtrSubCatList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubCatList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgExtrSubCatList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubCatList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgExtrSubCatList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header AndAlso Master_Row_Count > 0 Then
                Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                If chkAll Is Nothing Then
                    chkAll = New CheckBox
                    chkAll.ID = "chkSelectAll"
                    e.Row.Cells(0).Controls.Add(chkAll)
                End If
                chkAll.Attributes.Add("onClick", "SelectAllRows_ExtraSubCat(this,'" + dgExtrSubCatList.ClientID + "')")
                chkAll.ToolTip = "Click to toggle the selection of ALL rows"
            ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If chk Is Nothing Then
                    chk = New CheckBox
                    chk.ID = "chkSelect"
                    e.Row.Cells(0).Controls.Add(chk)
                End If
                chk.Attributes.Add("onClick", "SelectRow(this)")
                chk.Attributes.Add("win-name", "ExtraSubCat")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgSubCatList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgExtrSubCatList.RowEditing
        Try
            Dim strSelectedCode As String = sender.datakeys(e.NewEditIndex).item("EXTRA_CAT_CODE")

            With (wuc_FieldActyExtraSubCatPop)
                .ResetPage()
                .TeamCode = TeamCode
                .CatCode = CatCode
                .SubCatCode = SubCatCode
                .ExtraCatCode = strSelectedCode
                .LoadDvEditMode()
                .BindDetailsView()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgSubCatList() As GridView
        Dim dgSubCatListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgExtrSubCatList.AllowSorting
            Dim blnAllowPaging As Boolean = dgExtrSubCatList.AllowPaging

            dgExtrSubCatList.AllowSorting = False
            dgExtrSubCatList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgSubCatListToExport = dgExtrSubCatList

            dgExtrSubCatList.AllowPaging = blnAllowPaging
            dgExtrSubCatList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgSubCatListToExport
    End Function
#End Region

    Public Sub BindExtrCatCodeList()
        Try

            RefreshDataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10
        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If

        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Delete) Then
            btnDelete.Visible = False
        End If
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        With (wuc_FieldActyExtraSubCatPop)
            .ResetPage()
            .TeamCode = TeamCode
            .CatCode = CatCode
            .SubCatCode = SubCatCode
            .ExtraCatCode = ""
            .LoadDvInsertMode()
            .BindDetailsView()
        End With
    End Sub

    Protected Sub btnFieldActyExtraSubCatPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_FieldActyExtraSubCatPop.SaveButton_Click
        RenewDataBind()
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            'Dim strCodeList As String
            'strCodeList = Trim(GetSelectedString())

            Dim sbSelectedStr As New Text.StringBuilder

            If dgExtrSubCatList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgExtrSubCatList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgExtrSubCatList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            'sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
                            clsFieldActy.DeleteExtraSubCat(DK(0), DK(1), DK(2), DK(3))
                        End If
                    End If
                    i += 1
                Next
            End If

            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

End Class

Public Class CF_FieldActyExtraSubCatList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "START_DATE"
                strFieldName = "Start Date"
            Case "END_DATE"
                strFieldName = "End Date"
            Case "EXTRA_CAT_CODE"
                strFieldName = "Extra Cat Code"
            Case "EXTRA_CAT_NAME"
                strFieldName = "Extra Cat Name"
            Case "IMPLEMENTED"
                strFieldName = "Implemented"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "TEAM_CODE" Or strColumnName = "CAT_CODE" Or strColumnName = "SUB_CAT_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class