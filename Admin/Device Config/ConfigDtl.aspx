﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConfigDtl.aspx.vb" Inherits="Admin_Device_Config_ConfigDtl" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Device Configuration Detail</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <style type="text/css">
        .border {border: solid 1px #b9b8b8; background-color:#eeeeee;margin-bottom:10px;padding-bottom:10px}
    </style>
    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
     <script type="text/javascript">
        function ValidateAllChecked(HdrChkBox, DtlChkBox) {
            if ($('input[name$="' + DtlChkBox + '"]').length == $('input[name$="' + DtlChkBox + '"]:checked').length) {
                $('input[name$="'+ HdrChkBox + '"]').attr('checked', true);
            }
            else {
                $('input[name$="' + HdrChkBox + '"]').attr('checked', false);
            }
        }
        function ToggleAllCheckbox(TargetChkBox, domElement) {
            $('input[name$="' + TargetChkBox + '"]').each(function () {
                $(this).attr('checked', domElement.checked);
            })
        }
    </script>
   
</head>
<!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout" style="margin:10px 5px">
    <form id="frmConfigDtl" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300">
        </asp:ScriptManager>
        <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="updPnlConfigDtl" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err" 
                ></asp:Label>
            <div class="border">
                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                
                <table width="90%" cellpadding="0" cellspacing="0" border="0" style="margin:10px 10px">
                    <tr><td>
                        <asp:Label ID="lblTeam" runat="server" Text="Team" CssClass="cls_label_header"></asp:Label></td><td>
                            <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvDateStart" runat="server" Display="Dynamic" ControlToValidate="ddlTeam" ErrorMessage="Select team !" CssClass="cls_validator" />
                        </td></tr>
                    <tr><td>
                        <asp:Label ID="lblSalesrep" runat="server" Text="Salesrep" CssClass="cls_label_header"></asp:Label></td><td>
                        <asp:DropDownList ID="ddlSalesrep" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true">
                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                            </asp:DropDownList></td></tr>
                    <tr><td>
                        <asp:Label ID="lblCategoryType" runat="server" Text="Category" CssClass="cls_label_header"></asp:Label></td><td>
                        <asp:DropDownList ID="ddlCategoryType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true">
                            </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvCatType" runat="server" Display="Dynamic" ControlToValidate="ddlCategoryType" ErrorMessage="Select category !" CssClass="cls_validator" />    
                            </td></tr>
                </table>
                <div style="padding-left:10px">
                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" />
                </div>
            </div>      
            
                
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td vAlign="top" width="50%">
                    
                    <asp:Button ID="btnSave" runat="server" Text="Save" Visible="False" cssclass="cls_button" />&nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" CssClass="cls_button" Visible="false" />&nbsp;&nbsp;<asp:Label ID="lblMesg" runat="server" CssClass="cls_label"></asp:Label><br /><br />
                         <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowSorting="False"
                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                            FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="SETTING_CODE" Visible="False">
                            <Columns>                   
                                <asp:TemplateField headertext = "Apply" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70">
                                    <headertemplate>
                                      <asp:checkbox id="chkbApplyAll"
                                        CssClass="cls_checkbox"
                                        runat="server"/>Apply
                                    </headertemplate>
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>                                               
                                         <asp:CheckBox runat="server"  CssClass="cls_checkbox" ID="chkbApply" checked='<%# Container.DataItem("CHECKBOX") %>'/>                                      
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>                           
                                <asp:BoundField DataField="SETTING_CODE" HeaderText="Setting Code" ReadOnly="True" SortExpression="SETTING_CODE" HeaderStyle-HorizontalAlign="Center">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>                       
                                <asp:BoundField DataField="SETTING_NAME" HeaderText="Setting Name" ReadOnly="True" SortExpression="SETTING_NAME" HeaderStyle-HorizontalAlign="Center">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>                                            
	                           <asp:BoundField DataField="SETTING_DESC" HeaderText="Setting Description" ReadOnly="True" SortExpression="SETTING_DESC" HeaderStyle-HorizontalAlign="Center">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField headertext = "Value" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70">
                                    <headertemplate>
                                      <asp:checkbox id="chkbValueAll"
                                        CssClass="cls_checkbox"
                                        runat="server"/> Value
                                    </headertemplate>
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>                                               
                                         <asp:CheckBox runat="server"  CssClass="cls_checkbox" ID="chkbValue" checked='<%# Container.DataItem("VALUE") %>'/>                                      
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField> 
	                            <asp:TemplateField headertext = "Value 2" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">					                            
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValue2" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("VALUE2") %>'></asp:TextBox>                                
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField headertext = "Value 3" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">                                                
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtValue3" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("VALUE3") %>'></asp:TextBox>                                               
                                    </ItemTemplate>
                                </asp:TemplateField>    
                                <asp:TemplateField headertext = "Value 4" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">                                                
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtValue4" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("VALUE4") %>'></asp:TextBox>                                               
                                    </ItemTemplate>
                                </asp:TemplateField>                                           
                            </Columns>                                                
                             <FooterStyle CssClass="GridFooter" />
                             <HeaderStyle CssClass="GridHeader" />
                             <AlternatingRowStyle CssClass="GridAlternate" />
                             <RowStyle CssClass="GridNormal" />
                             <PagerSettings Visible="False" />
                        </ccGV:clsGridView>	</td>		
                            
                    </tr>
            </table>
        
        
        
        
        </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
