<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FieldForceItineraryTransfer.aspx.vb"
    Inherits="iFFMA_Mapping_FieldForceItineraryTransfer" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Field Force Itinerary Transfer</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesrepItineraryTransfer" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                                ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <HeaderTemplate>
                                                        Itinerary Transfer</HeaderTemplate>
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="true">
                                                            <span style="float: left; width: 98%; padding-top: 15px; padding-left: 10px;">
                                                                <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                            </span>
                                                            <table border="0" cellpadding="2" cellspacing="0" width="95%">
                                                                <tr>
                                                                    <td style="width: 15%;">
                                                                    </td>
                                                                    <td style="width: 3%;">
                                                                    </td>
                                                                    <td style="width: 80%;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <table cellspacing="0" cellpadding="2" width="98%" border="0" align="center">
                                                                    <!-- BEGIN ADD HERE -->
                                                                    <tr valign="bottom">
                                                                        <td style="width: 20%">
                                                                        </td>
                                                                        <td style="width: 2%">
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="cls_label_header">Team</span> <span class="cls_label_err">*</span></td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span></td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                                            <asp:RequiredFieldValidator ID="rfvddlTeam" runat="server" ErrorMessage="Select a team"
                                                                                ControlToValidate="ddlTeam" CssClass="cls_validator">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="cls_label_header">Field Force(Source)</span> <span class="cls_label_err">
                                                                                *</span></td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span></td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlFieldForcesource" runat="server" CssClass="cls_dropdownlist"
                                                                                AutoPostBack="True" />
                                                                            <asp:RequiredFieldValidator ID="rfvddlFieldForcesource" runat="server" ErrorMessage="Select a source field force"
                                                                                ControlToValidate="ddlFieldForcesource" CssClass="cls_validator">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="cls_label_header">Field Force(Destination)</span> <span class="cls_label_err">
                                                                                *</span></td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span></td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlFieldForcedestination" runat="server" CssClass="cls_dropdownlist"
                                                                                AutoPostBack="True" />
                                                                            <asp:RequiredFieldValidator ID="rfvddlFieldForcedestination" runat="server" ErrorMessage="Select a destination field force"
                                                                                ControlToValidate="ddlFieldForcedestination" CssClass="cls_validator">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CompareValidator ID="cvddlFieldForcedestination" runat="server" ErrorMessage="Select a different destination Field Force"
                                                                                ControlToCompare="ddlFieldForcesource" ControlToValidate="ddlFieldForcedestination"
                                                                                Operator="NotEqual" CssClass="cls_validator">
                                                                            </asp:CompareValidator></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="cls_label_header">Action</span> <span class="cls_label_err">*</span></td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span></td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlFieldForceAction" runat="server" CssClass="cls_dropdownlist"
                                                                                AutoPostBack="True" />
                                                                            <asp:RequiredFieldValidator ID="rfvddlFieldForceAction" runat="server" ErrorMessage="Select an action"
                                                                                ControlToValidate="ddlFieldForceAction" CssClass="cls_validator">
                                                                            </asp:RequiredFieldValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- END ADD HERE -->
                                                                    <tr>
                                                                        <td style="height: 10px;" colspan="3">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:Button ID="btnproceed" runat="server" Text="Proceed" CssClass="cls_button" OnClick="btnproceed_Click">
                                                                            </asp:Button><%--</asp:Button>--%>
                                                                            <asp:Label ID="lblvalidator" runat="server" CssClass="cls_validator"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td style="height: 5px">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        
        <customToolkit:wuc_lblMsgPop ID="wuc_lblMsgPop" Title="Field Force Itinerary Transfer" runat="server" />
    </form>
</body>
</html>
