<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UserMobileDtl.aspx.vb" Inherits="Admin_UserMobile_UserMobileDtl" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Mobile User Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmUserDtl" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td style="height: 95px">
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">					                            
					                            <tr>
							                        <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr><td>&nbsp;</td></tr>						                        
						                        <tr>
							                        <td width="20%" style="height: 19px"><asp:label id="lblUserID" runat="server" CssClass="cls_label_header">Mobile User ID</asp:label></td>
							                        <td width="2%" style="height: 19px"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 19px"><asp:label id="lblUserIDValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblUserCode" runat="server" CssClass="cls_label_header">Sales Rep Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblUserCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblUserName" runat="server" CssClass="cls_label_header">Sales Rep Name</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblUserNameValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblMobileCode" runat="server" CssClass="cls_label_header">Mobile Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot7" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblMobileCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblContactNo" runat="server" CssClass="cls_label_header">Contact No</asp:label></td>
							                        <td width="2%"><asp:label id="Label2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblContactNoValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblEmail" runat="server" CssClass="cls_label_header">Email</asp:label></td>
							                        <td width="2%"><asp:label id="Label5" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblEmailValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>						                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblAddress" runat="server" CssClass="cls_label_header">Address</asp:label></td>
							                        <td width="2%"><asp:label id="Label8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblAddressValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>						                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblSettingCode" runat="server" CssClass="cls_label_header">Setting Code</asp:label></td>
							                        <td width="2%"><asp:label id="Label3" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblSettingCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>		
					                        </table>
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
                                            &nbsp; &nbsp; &nbsp;&nbsp;
					                        <asp:button id="btnDelete" runat="server" Text="Delete" CssClass="cls_button"></asp:button>
					                        <asp:button id="btnEdit" runat="server" CssClass="cls_button" Text="Edit"></asp:button>
                                            <asp:button id="btnClose" runat="server" CssClass="cls_button" Text="Close"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
