<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPDtlv4.aspx.vb" Inherits="Admin_SAP_SAPDtl" %>

<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_DateCalendar" Src="~/include/wuc_DateCalendar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SAP Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSAPDtl" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" />

        <table id="tbl1" cellspacing="0" cellpadding="0" width="98%" border="0">
            <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr>
                            <td class="BckgroundBenealthTitle" colspan="3" height="5"></td>
                        </tr>
                        <tr class="Bckgroundreport">
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td align="left">
                                            <asp:TextBox ID="txtDocType" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
                                            <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
                                            <asp:TextBox ID="txtSearchValue1" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
                                            <asp:TextBox ID="txtTxnNo" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
                                            <asp:Button ID="btnViewTray" runat="server" Text="View Tray" CssClass="cls_button"></asp:Button>
                                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="cls_button"></asp:Button>
                                            <asp:Button ID="btnCancelTxn" runat="server" Text="Cancel Txn" CssClass="cls_button"></asp:Button>
                                            <asp:Button ID="btnResubmit" runat="server" Text="Resubmit" CssClass="cls_button"></asp:Button>
                                            <asp:Button ID="btnUnBlock" runat="server" Text="UnBlock" CssClass="cls_button"></asp:Button>
                                            <asp:Button ID="BtnEdit" runat="server" Text="Edit" CssClass="cls_button"></asp:Button>
                                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CssClass="cls_button" Enabled="false"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot35" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStatusValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblTxnStatus" runat="server" CssClass="cls_label_header">Txn Status</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="Label2" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTxnStatusValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlLN" runat="server">
                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="lblLNStatus" runat="server" CssClass="cls_label_header">LN Status</asp:Label>
                                                        </td>
                                                        <td width="2%">
                                                            <asp:Label ID="lblDot40" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblLNStatusValue" runat="server" CssClass="cls_label"></asp:Label>
                                                        </td>
                                                        <td width="20%">
                                                            <asp:Label ID="lblLNRemarks" runat="server" CssClass="cls_label_header">LN Remarks</asp:Label>
                                                        </td>
                                                        <td width="2%">
                                                            <asp:Label ID="lblDot41" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblLNRemarksValue" runat="server" CssClass="cls_label"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblTxnNo" runat="server" CssClass="cls_label_header">Txn No</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot1" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTxnNoValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblDocNo" runat="server" CssClass="cls_label_header">Saving No</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot2" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDocNoValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>--%>
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblSRCode" runat="server" CssClass="cls_label_header">Salesrep Code</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot3" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td colspan="4">
                                                        <asp:Label ID="lblSRCodeValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblLastUpdate" runat="server" CssClass="cls_label_header">Last Update</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot5" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblLastUpdateValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblTxnDate" runat="server" CssClass="cls_label_header">Txn Date</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="Label3" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTxnDateValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblOrderType" runat="server" CssClass="cls_label_header">Order Type</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot6" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOrderTypeValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblSalesOrg" runat="server" CssClass="cls_label_header">Sales Org</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot7" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSalesOrgValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblDistChannel" runat="server" CssClass="cls_label_header">Distribution Channel</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot8" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDistChannelValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblDivCode" runat="server" CssClass="cls_label_header">Division Code</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot4" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDivCodeValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="lblSalesOff" runat="server" CssClass="cls_label_header">Sales Office</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot9" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSalesOffValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblSalesGrp" runat="server" CssClass="cls_label_header">Sales Group</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot10" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSalesGrpValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblPlant" runat="server" CssClass="cls_label_header">Plant</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot18" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblPlantValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblSloc" runat="server" CssClass="cls_label_header">Storage Location</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot19" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblSlocValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblShipPoint" runat="server" CssClass="cls_label_header">Shipping Point</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot20" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblShipPointValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblSoldTo" runat="server" CssClass="cls_label_header">Soldto Party</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot11" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSoldToValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblShipto" runat="server" CssClass="cls_label_header">Shipto Party</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot12" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblShipToValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td width="20%">
                                                        <asp:Label ID="lblPayerCode" runat="server" CssClass="cls_label_header">Payer Code</asp:Label>
                                                    </td>
                                                    <td width="2%">
                                                        <asp:Label ID="lblDot13" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPayerCodeValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblPONo" runat="server" CssClass="cls_label_header">PO No</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot14" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblPONoValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblPODate" runat="server" CssClass="cls_label_header">PO Date</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot15" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblPODateValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblPOType" runat="server" CssClass="cls_label_header">PO Type</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot16" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblPOTypeValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblPOSupp" runat="server" CssClass="cls_label_header">PO Supplement</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot17" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblPOSuppValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblBName" runat="server" CssClass="cls_label_header">Name of Orderer</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot21" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblBNameValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblIntRef" runat="server" CssClass="cls_label_header">Customer / Vendor Internal Ref</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot22" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblIntRefValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrShipText1" runat="server" CssClass="cls_label_header">Header shipping text 1</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot23" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrShipText1Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>

                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrShipText2" runat="server" CssClass="cls_label_header">Header shipping text 2</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot24" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrShipText2Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblHdrShipText3" runat="server" CssClass="cls_label_header">Header shipping text 3</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot25" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top" colspan="4">
                                                        <asp:Label ID="lblHdrShipText3Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblHdrDiscType1" runat="server" CssClass="cls_label_header">Header disc. type 1</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot26" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrDiscType1Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblHdrDiscVal1" runat="server" CssClass="cls_label_header">Header disc. value 1</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot27" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrDiscVal1Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblHdrDiscType2" runat="server" CssClass="cls_label_header">Header disc. type 2</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot28" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrDiscType2Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top" width="20%">
                                                        <asp:Label ID="lblHdrDiscVal2" runat="server" CssClass="cls_label_header">Header disc. value 2</asp:Label>
                                                    </td>
                                                    <td valign="top" width="2%">
                                                        <asp:Label ID="lblDot29" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblHdrDiscVal2Value" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDelDate" runat="server" CssClass="cls_label_header">Delivery Date</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot30" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDelDateValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblUsageInd" runat="server" CssClass="cls_label_header">Usage Indicator</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="Labe31" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblUsageIndValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lblResendCnt" runat="server" CssClass="cls_label_header">Resubmit Counter</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot32" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblResendCntValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblOrderBy" runat="server" CssClass="cls_label_header">Order By</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot33" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblOrderByValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lblRemarks" runat="server" CssClass="cls_label_header">Remarks</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot34" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblRemarksValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblTtlRetAmt" runat="server" CssClass="cls_label_header">Total Ret Amt</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot42" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblTtlRetAmtValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>



                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lblRefNo" runat="server" CssClass="cls_label_header">Reference No</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot39" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <%--<asp:Label ID="lblRefNoValue" runat="server" CssClass="cls_label"></asp:Label>--%>
                                                        <asp:TextBox ID="txtRefNo" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" Enabled="false" />
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblNote" runat="server" CssClass="cls_label_header">Note</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblDot38" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblNoteValue" runat="server" CssClass="cls_label"></asp:Label>
                                                    </td>

                                                    <tr>

                                                        <td valign="top">
                                                            <asp:Label ID="lblCollectBy" runat="server" CssClass="cls_label_header">Collect By</asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Label ID="lblDot45" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:DropDownList ID="ddlCollBy" runat="server" CssClass="cls_dropdownlist" Enabled="false">
                                                                <asp:ListItem Value="COLL001">Salesrep</asp:ListItem>
                                                                <asp:ListItem Value="COLL002">Transporter</asp:ListItem>
                                                                <asp:ListItem Value="COLL003">Transporter (Cold Truck)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>

                                                        <td valign="top">
                                                            <asp:Label ID="Label1" runat="server" CssClass="cls_label_header">Ctn No</asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Label ID="Label4" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                        </td>
                                                        <td valign="top">
                                                            <%--<asp:Label ID="lblRefNoValue" runat="server" CssClass="cls_label"></asp:Label>--%>
                                                            <asp:TextBox ID="txtCtnNo" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" Enabled="false" />
                                                        </td>

                                                    </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Bckgroundreport" colspan="3">
                                            <!--<uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>-->
                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                Width="98%" FreezeHeader="True" GridHeight="300px" AddEmptyHeaders="0" CellPadding="2"
                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="prd_code,actual_price" BorderColor="Black"
                                                BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite" OnRowDeleting="dgList_RowDeleting">
                                                <Columns>
                                                    <%--<asp:CommandField 
                                                    DeleteText="<img src='../../images/ico_Delete.gif'  alt='Delete' border='0'/>"
                                                    ShowDeleteButton="True" />--%>
                                                    <asp:CommandField CancelText="<img src='../images/ico_close.gif' alt='Cancel' border='0'/>"
                                                        DeleteText="<img src='../../images/ico_Delete.gif'  alt='Delete' border='0'/>"
                                                        ShowDeleteButton="True" UpdateText="<img src='../../images/ico_editProfile.gif' alt='Update' border='0'/>" />
                                                    <asp:CommandField CancelText="<img src='../../images/ico_close.gif' alt='Cancel' border='0'/>;"
                                                        EditText="<img src='../../images/ico_Edit.gif' alt='Edit' border='0'>" ShowEditButton="True"
                                                        UpdateText="<img src='../../images/ico_editProfile.gif' alt='Update' border='0'/>" />
                                                    <asp:BoundField DataField="LINE_NO" HeaderText="Line Number" ReadOnly="True" SortExpression="LINE_NO">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" ReadOnly="True" SortExpression="PRD_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="PRD_NAME" HeaderText="Product Name" ReadOnly="True" SortExpression="PRD_NAME">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RET_QTY" HeaderText="Qty" ReadOnly="True" SortExpression="RET_QTY">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UOM_CODE" HeaderText="UOM" ReadOnly="True" SortExpression="UOM_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <%--<asp:BoundField DataField="LIST_PRICE" HeaderText="UNIT PRICE" ReadOnly="True" SortExpression="LIST_PRICE">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>--%>
                                                    <asp:TemplateField HeaderText="List Price">
                                                        <EditItemTemplate>
                                                            <asp:TextBox runat="server" Text='<%# Bind("LIST_PRICE") %>' ID="txtListPrice_Edit"
                                                                CssClass="cls_textbox" Width="150px" MaxLength="300" HtmlEncode="false" DataFormatString="{0:#,0.00}"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="revListPrice" runat="server" ControlToValidate="txtListPrice_Edit"
                                                                CssClass="cls_label_err" ErrorMessage="Invalid, only number with decimal value allowed!"
                                                                ValidationGroup="ListingSave" Display="Dynamic" ValidationExpression="^\d*(.\d{1,2})?$" />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" Text='<%# Eval("LIST_PRICE","{0:#,0.00}")%>' ID="lblUnitPrice_Vw"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ACTUAL_PRICE" HeaderText="Actual Price" ReadOnly="True" SortExpression="ACTUAL_PRICE">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RET_AMT" HeaderText="NET VALUE" ReadOnly="True" SortExpression="RET_AMT">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="REASON_CODE" HeaderText="Reason Code" ReadOnly="True" SortExpression="REASON_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>

                                                    <asp:TemplateField HeaderText="Reason Name">
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlReasonCode" runat="server" CssClass="cls_dropdownlist" />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" Text='<%# Bind("REASON_NAME") %>' ID="lblReasonName_Vw"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <%--<asp:BoundField DataField="REASON_NAME" HeaderText="Reason Name" ReadOnly="True"
                                                    SortExpression="REASON_NAME">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>--%>
                                                    <%-- <asp:BoundField DataField="EXP_DATE" HeaderText="Expired Date" ReadOnly="True" SortExpression="EXP_DATE">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>--%>
                                                    <asp:TemplateField HeaderText="EXP Date">
                                                        <EditItemTemplate>
                                                            <customToolkit:wuc_DateCalendar ID="txtExpDate" runat="server" RequiredValidation="True" DateFormatString="yyyy-MM-dd" />
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" Text='<%# Bind("EXP_DATE") %>' ID="lblExpDate_Vw"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="BATCH_NO">
                                                        <EditItemTemplate>
                                                            <asp:TextBox runat="server" Text='<%# Bind("BATCH_NO") %>' ID="txtBatch_Edit" CssClass="cls_textbox"
                                                                Width="150px" MaxLength="300"></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" Text='<%# Bind("BATCH_NO") %>' ID="lblBatch_Vw"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle CssClass="GridFooter" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                <RowStyle CssClass="GridNormal" />
                                                <PagerSettings Visible="False" />
                                            </ccGV:clsGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td align="right">&nbsp;
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="Bckgroundreport">
                            <td colspan="3">&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
<%'List function called by in-line scripts%>