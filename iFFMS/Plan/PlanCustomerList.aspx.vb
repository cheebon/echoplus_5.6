Imports System.Data

Partial Class PlanCustomerList
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "PlanCustomerList"
        End Get
    End Property
    Dim strDate As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strSalesmanCode As String
        'Put user code to initialize the page here
        Try
            strDate = Session("plandateselected")
            With wuc_lblheader
                .Title = "Customer Plan List"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.CUSTPREPLAN
                .DataBind()
                .Visible = True
            End With

            strSalesmanCode = Trim(Session("dflSalesRepName"))

            If Not IsPostBack Then
                'strDate = Request.QueryString("date")
                'Session("plandateselected") = strDate
                bindGridview()
                lblSelectedDate.Text = strDate
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub bindGridview()
        Dim dt, dtnew As DataTable
        Dim dr As DataRow
        Dim drcurr() As DataRow
        Try
            dt = Session("Customerselected")
            dtnew = dt.Copy
            drcurr = dtnew.Select("field3<>'" & strDate & "'")
            For Each dr In drcurr
                dtnew.Rows.Remove(dr)
            Next
            If dtnew.Rows.Count > 0 Then
                dgList.DataSource = dtnew
                dgList.DataKeyNames = New String() {"field1"}
                dgList.DataBind()
            Else
                Session("PlanPostBack") = "Yes"
                Response.Redirect("../plan/plancustomer.aspx", False)
                'dtnew.Rows.Add(dtnew.NewRow)
                'dgList.DataSource = dtnew
                'dgList.DataBind()
            End If
           
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Sub gridview_Command(ByVal s As Object, ByVal e As GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim strCustCode As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim drcurr() As DataRow
        Try
            strDate = Session("plandateselected")
            strCustCode = dgList.DataKeys(e.CommandArgument).Value
            Select Case e.CommandName
                Case "AddOn"
                    Session("customercode") = strCustCode
                    Response.Redirect("../Plan/PlanActivity.aspx?custcode=" & strCustCode & "&date=" & strDate)
                Case "DeleteSession"
                    'dt = Session("CustomerPlanList")
                    dt = Session("customerselected")
                    drcurr = dt.Select("field1='" & strCustCode & "'")
                    For Each dr In drcurr
                        dt.Rows.Remove(dr)
                    Next
                    Session("CustomerPlanList") = dt
                    activityDeletion(strCustCode)
                    drcurr = dt.Select("field2='" & strDate & "'")
                    If drcurr.Length > 0 Then
                        bindGridview()
                    Else
                        Session("PlanPostBack") = "Yes"
                        Response.Redirect("../plan/plancustomer.aspx", False)
                    End If

            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".gridview_Command : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub activityDeletion(ByVal strCustCode As String)
        strDate = Session("plandateselected")
        Try
            removeActivity(strCustCode, strDate, "PlanContDept", "PlanContDeptSelected")
            removeActivity(strCustCode, strDate, "PlanCont", "PlanContSelected")
            removeActivity(strCustCode, strDate, "PlanPrdGrp", "PlanPrdGrpSelected")
            removeActivity(strCustCode, strDate, "PlanSFMS", "PlanSFMSSelected")
            removeActivity(strCustCode, strDate, "PlanMSS", "PlanMSSSelected")
            removeActivity(strCustCode, strDate, "PlanPromoCamp", "PlanPromoCampSelected")
            removeActivity(strCustCode, strDate, "PlanOther", "PlanOtherSelected")
            removeActivity(strCustCode, strDate, "PlanFreeText")
        Catch ex As Exception

        End Try
    End Sub

    Sub removeActivity(ByVal strCustCode As String, ByVal strDate As String, ByVal strSession1 As String, Optional ByVal strSession2 As String = Nothing)
        Dim dt1, dt2 As DataTable
        Dim drcurr1(), drcurr2() As DataRow
        Try
            dt1 = Session(strSession1)
            If Not IsNothing(dt1) AndAlso dt1.Rows.Count > 0 Then
                drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                removeDataTable(drcurr1, dt1)
                If Not IsNothing(strSession2) Then
                    dt2 = Session(strSession2)
                    If Not IsNothing(dt2) AndAlso dt2.Rows.Count > 0 Then
                        drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                        removeDataTable(drcurr2, dt2)
                    End If

                End If
            End If


        Catch ex As Exception

        End Try
    End Sub

    Sub removeDataTable(ByVal drcurr() As DataRow, ByVal dt As DataTable)
        Try
            For Each dr As DataRow In drcurr
                dt.Rows.Remove(dr)
            Next
        Catch ex As Exception

        End Try
    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../plan/planmenu.aspx", False)
        'Server.Transfer("../plan/planmenu.aspx")
    End Sub
End Class



