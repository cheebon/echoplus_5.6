﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_PlanCuz_PlanRmks
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Assign Remarks Advance" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControlRmks.Enabled = True

        End If
    End Sub

    Protected Sub TimerControlRmks_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlRmks.Tick
        If TimerControlRmks.Enabled Then RefreshDatabindingRmks()
        TimerControlRmks.Enabled = False
    End Sub

    Private Sub RefreshDatabindingRmks()
        Dim DT As DataTable = Nothing
        DT = GetRecListRmks()
        If DT.Rows.Count > 0 Then
            txtRmks.Text = DT.Rows(0)("REMARKS")
            UpdateRmks.Update()
            UpdatePage.Update()
        End If

    End Sub

    Private Function GetRecListRmks() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strContCode As String, strUserId As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strContCode = Trim(Request.QueryString("ContCode"))


            Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
            DT = clsPrePlanCuz.GetPlanAddonRmksCont(strSalesrepCode, strRouteDate, strCustCode, strContCode, strUserId)

        End If

        Return DT
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If Page.IsValid Then
            SaveRmks()
            lblmsg.Text = "Remarks Saved !"
        End If
    End Sub

    Private Sub SaveRmks()
        Try
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strContCode As String, strRemarks As String, StrUserId As String
            Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz

            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strContCode = Trim(Request.QueryString("ContCode"))
            strRemarks = Trim(txtRmks.Text)
            StrUserId = Trim(Session("UserID"))

            clsPrePlanCuz.InstPlanAddonRmksCont(strSalesrepCode, strRouteDate, strCustCode, strContCode, strRemarks, StrUserId)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub
End Class
