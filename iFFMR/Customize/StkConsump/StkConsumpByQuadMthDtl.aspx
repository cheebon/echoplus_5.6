﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StkConsumpByQuadMthDtl.aspx.vb" Inherits="iFFMR_Customize_StkConsump_StkConsumpByQuadDtl" %>


<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc2" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc3" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="uc4" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Stock Consumption By Quad Month Detail</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script src="../../../include/layout.js" type="text/javascript"></script>

    <script type="text/javascript">
        function resetScrollTop() { var frm = self.parent.document.frames[1]; if (frm) { var dgList = frm.document.getElementById('div_dgList'); if (dgList) { dgList.scrollTop = 0; } } }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe')">
    <form id="frmStkConspByQuadMonthDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
        <uc3:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uc3:wuc_lblHeader>
        <uc2:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true"></uc2:wuc_ctrlpanel>
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                    <tr>
                        <td>
                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="BckgroundBenealthTitle" colspan="3">
                            <input type="button" runat="server" id="btnback" value="Back" onclick="ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','DetailBarIframe');"
                                class="cls_button" />
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport"
                    style="padding-top: 10px;">
                    <tr>
                        <td>
                            <asp:Button ID="btnCollapse" Text="Click to Expand or Collapse" runat="server" CssClass="cls_button" OnClientClick="resetScrollTop();"
                                Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="PnlSearch" runat="server" Visible="true" class="cls_panel_header">
                                <table width="100%">
                                    <tr valign="top">
                                        <td>
                                            <asp:Label ID="lblSelectedTeam" runat="server" Text="Selected Team:" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lsbSelectedTeam" runat="server" CssClass="cls_listbox"></asp:ListBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSelectedSalesrep" runat="server" Text="Selected Salesrep:" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lsbSelectedSalesrep" runat="server" CssClass="cls_listbox"></asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td>
                                            <asp:Label ID="lblSelectedPrd" runat="server" Text="Selected Product:" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lsbSelectedProduct" runat="server" CssClass="cls_listbox"></asp:ListBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblYearDesc" runat="server" Text="Year:" CssClass="cls_label_header"></asp:Label><br />
                                             <asp:Label ID="lblBiMonthDesc" runat="server" Text="Bi Month:" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblYear" runat="server" Text=" " CssClass="cls_label"></asp:Label><br />
                                            <asp:Label ID="lblBiMonth" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlSearch" runat="server" CollapseControlID="btnCollapse"
                                ExpandControlID="btnCollapse" TargetControlID="PnlSearch" CollapsedSize="0" Collapsed="True"
                                ExpandDirection="Vertical" SuppressPostBack="true">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Timer ID="TimerControl1" runat="server" Interval="100" OnTick="TimerControl1_Tick" />
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                Width="98%" FreezeHeader="True" GridHeight="480" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="True" AllowPaging="False" PagerSettings-Visible="False">
                                <EmptyDataTemplate>
                                    <uc1:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                </EmptyDataTemplate>
                            </ccGV:clsGridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </fieldset>
    </form>
</body>
</html>
