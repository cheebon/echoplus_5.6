﻿Imports System.Data
Partial Class iFFMR_Customize_SOVariance_SOVariance
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "HeaderCollector_SOVariance"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
    
#End Region

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "SOVariance"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.SOVARIANCE)
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.SOVARIANCE
                .DataBind()
                .Visible = True
            End With


            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
                TimerControl1.Enabled = True
            End If
            lblinfo.Text = ""
            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            RefreshDatabinding()

        End If

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        RefreshDatabinding()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed, wuc_ctrlpanel.NetValue_Changed
        RefreshDataBind()
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If dgList.Rows.Count > 0 Then

                'RefreshDatabinding()

                Dim i As Integer = 0
                Dim isInvalidExists As Boolean = False

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)

                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        Dim chkSelect As CheckBox = CType(dgList.Rows(i).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then

                            Dim strUserID As String, strPrincipalID As String, strPrincipalCode As String
                            Dim strPdaOrdNo As String, strSoNo As String, strInvNo As String, strUserStatus As String

                            strUserID = Session.Item("UserID")
                            strPrincipalID = Session("PRINCIPAL_ID")
                            strPrincipalCode = Session("PRINCIPAL_CODE")

                            strPdaOrdNo = dgList.DataKeys(i).Item("PDA_ORD_NO")
                            strSoNo = dgList.DataKeys(i).Item("SO_NO")
                            strInvNo = dgList.DataKeys(i).Item("INV_NO")
                            strUserStatus = dgList.DataKeys(i).Item("USER_STATUS")

                            'Dim TxtRemarks As TextBox = CType(dgList.Rows(i).Cells(aryDataItem.IndexOf("REMARKS")).Controls(0), TextBox)
                            'Dim ChkStatus As CheckBox = CType(dgList.Rows(i).Cells(aryDataItem.IndexOf("USER_STATUS")).Controls(0), CheckBox)
                            Dim TxtRemarks As TextBox = CType(dgList.Rows(i).FindControl("TxtRemarks"), TextBox)
                            Dim ChkStatus As CheckBox = CType(dgList.Rows(i).FindControl("ChkStatus"), CheckBox)

                            Dim strChkStatus As String
                            If strUserStatus = 1 Then
                                strChkStatus = ""
                            Else
                                strChkStatus = IIf(ChkStatus.Checked = True, 1, 0)

                            End If
                            

                            Dim clsSOVariance As New rpt_Customize.clsSOVariance
                            Dim dt As DataTable
                            dt = clsSOVariance.SOVarianceUpdate(strUserID, strPrincipalID, strPrincipalCode, strPdaOrdNo, strSoNo, strInvNo, Trim(TxtRemarks.Text), strChkStatus)

                            lblinfo.Text = "Records successfully saved!"
                        End If
                    End If

                    i += 1
                Next


                RefreshDatabinding()
                UpdateDatagrid.Update()

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    '#Region "Paging Control"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnGo_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
    '        Try
    '            dgList.PageIndex = CInt(Wuc_dgpaging.PageNo - 1)

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkPrevious_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
    '        Try
    '            If dgList.PageIndex > 0 Then
    '                dgList.PageIndex = dgList.PageIndex - 1
    '            End If
    '            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkNext_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
    '        Try
    '            If dgList.PageCount - 1 > dgList.PageIndex Then
    '                dgList.PageIndex = dgList.PageIndex + 1
    '            End If
    '            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing ' CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()
            ViewState("strSortExpression") = Nothing
            ViewState("dtCurrentView") = dtCurrentTable
            dgList.PageIndex = 0
            'End If

      
            If dtCurrentTable.Rows.Count = 0 Then
                ' dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New Data.DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.PageSize = 20 'intPageSize
            dgList.DataBind()

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            'End With

            'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            'aryDataItem.Clear()
            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            'dgList.Columns.Clear()

            aryDataItem.Clear()

            'CHECKBOX
            aryDataItem.Add("chkSelect")
            While dgList.Columns.Count > 1
                dgList.Columns.RemoveAt(1)
            End While
            dgList.Columns(0).HeaderStyle.Width = "25"
            dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center


            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SOVariance.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_SOVariance.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_SOVariance.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SOVariance.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_SOVariance.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_SOVariance.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields(1) As String
                        strUrlFields(0) = "SO_NO"
                        strUrlFields(1) = "INV_NO"

                        strUrlFormatString = ("parent.document.getElementById('ContentBarIframe').src='../../iFFMR/Customize/SOVariance/SOVarianceDtl.aspx?SO_NO={0}&INV_NO={1}'")

                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SOVariance.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SOVariance.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SOVariance.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                End Select


            Next
            aryDataItem = aryDataItem

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        LoadChkStatus(e)
                        LoadTxtRemarks(e)
                        RedirectToAnotherFrameForSONO(e)
                        RedirectToAnotherFrameForINVNO(e)

                    End If


                Case DataControlRowType.Footer
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        LoadChkStatus(e)
                        LoadTxtRemarks(e)

                        Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                        If chk Is Nothing Then
                            chk = New CheckBox
                            chk.ID = "chkSelect"
                            e.Row.Cells(0).Controls.Add(chk)
                        End If
                    End If

                Case DataControlRowType.Footer
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub RedirectToAnotherFrameForSONO(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        Dim tc As TableCell
        tc = e.Row.Cells(aryDataItem.IndexOf("SO_NO"))
        Dim HYL As HyperLink = CType(tc.Controls(0), HyperLink)
        If HYL IsNot Nothing Then
            Dim strlink As String = HYL.NavigateUrl
            HYL.NavigateUrl = "#"
            HYL.Target = String.Empty
            HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();ScrollTop();")

        End If

    End Sub

    Private Sub RedirectToAnotherFrameForINVNO(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)

        Dim tc As TableCell
        tc = e.Row.Cells(aryDataItem.IndexOf("INV_NO"))
        Dim HYL As HyperLink = CType(tc.Controls(0), HyperLink)
        If HYL IsNot Nothing Then
            Dim strlink As String = HYL.NavigateUrl
            HYL.NavigateUrl = "#"
            HYL.Target = String.Empty
            HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();ScrollTop();")

        End If

    End Sub

    Private Sub LoadTxtRemarks(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim tc As TableCell
            Dim TxtRemarks As New TextBox
            Dim strRemarks As String

            tc = e.Row.Cells(aryDataItem.IndexOf("REMARKS"))
            strRemarks = Trim(e.Row.Cells(aryDataItem.IndexOf("REMARKS")).Text)

            With TxtRemarks
                .ID = "TxtRemarks"
                .CssClass = "cls_textbox"
                .Width = "200"
                .MaxLength = "250"
                If Not String.IsNullOrEmpty(strRemarks) AndAlso strRemarks <> "&nbsp;" Then
                    .Text = Trim(e.Row.Cells(aryDataItem.IndexOf("REMARKS")).Text)
                End If
                .TextMode = TextBoxMode.MultiLine
                .Enabled = True
            End With
            tc.Controls.Add(TxtRemarks)


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadChkStatus(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim tc As TableCell
            Dim strStatus As String
            tc = e.Row.Cells(aryDataItem.IndexOf("USER_STATUS"))
            strStatus = e.Row.Cells(aryDataItem.IndexOf("USER_STATUS")).Text

            Dim ChkStatus As New CheckBox
            With ChkStatus
                .ID = "ChkStatus"
                .CssClass = "cls_checkbox"
                If Not String.IsNullOrEmpty(strStatus) AndAlso strStatus <> "&nbsp;" Then
                    If strStatus = "0" Then
                        .Enabled = True
                        .Visible = True
                    ElseIf strStatus = "1" Then
                        .Visible = False
                        .Enabled = False
                    End If
                End If
            End With
            tc.Controls.Add(ChkStatus)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsSOVariance As New rpt_Customize.clsSOVariance

            Dim strUserID As String, strPrincipalID As String, strPrincipalCode As String, _
             strDateBy As String, strDateStart As String, strDateEnd As String, strSalesrepCode As String, _
             strCustCode As String, strPdaOrdNo As String, strStatus As String, strSoNo As String

            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            strDateBy = ddlSearchDateBy.SelectedValue
            strDateStart = txtSearchDateValue.DateStart
            strDateEnd = txtSearchDateValue.DateEnd
            strSalesrepCode = txtSalesrepCode.Text
            strCustCode = txtCustCode.Text
            strSoNo = txtSONO.Text
            strPdaOrdNo = txtPdaOrdNo.Text
            strStatus = ddlstatus.SelectedValue

            DT = clsSOVariance.GetSOVariance(strUserID, strPrincipalID, strPrincipalCode, strDateBy, strDateStart, strDateEnd, strSalesrepCode, _
             strCustCode, strSoNo, strPdaOrdNo, strStatus)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, "SOVariance")

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub


End Class

Public Class CF_SOVariance
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "PDA_ORD_NO"
                strFieldName = "PDA Ord. No."
            Case "TXN_DATE"
                strFieldName = "Txn. Date"
            Case "SO_NO"
                strFieldName = "So. No."
            Case "SO_AMT"
                strFieldName = "So. Amount"
            Case "SO_DATE"
                strFieldName = "So. Date"
            Case "INV_DATE"
                strFieldName = "Inv. Date"
            Case "USER_STATUS"
                strFieldName = "Status"
           
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)

                If strFieldName = "&NBSP;" Then
                    strFieldName = ""
                End If
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Select Case strColumnName
                Case "SO_NO"
                    FCT = FieldColumntype.HyperlinkColumn
                Case "INV_NO"
                    FCT = FieldColumntype.HyperlinkColumn
            End Select

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "SO_DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "INV_DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "QTY"
                    strFormatString = "{0:#,0}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "SO_AMT" Then
                    .HorizontalAlign = HorizontalAlign.Right
                ElseIf strColumnName Like "REMARKS" Or strColumnName Like "*NAME" Then
                    .HorizontalAlign = HorizontalAlign.Left
                Else
                    .HorizontalAlign = HorizontalAlign.Center
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class