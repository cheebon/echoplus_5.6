'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	10/11/2006
'	Purpose	    :	User Group Create
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_UserGrp_UserGrpCreate
    Inherits System.Web.UI.Page

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Creating User Group"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request

                'Prepare Recordset,Createobject as needed
                

            End If

        Catch ex As Exception
            ExceptionMsg("UserGrpCreate.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
