Imports System.Data

Partial Class SalesOrder
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Protected WithEvents ddlTxnNo_Temp As DropDownList
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "SalesOrder"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.SALESORD) '"Sales Order"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.SALESORD
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""

            If Not IsPostBack Then
                Dim strQueryString As String = String.Empty ' Session("CallAnalysisListByCustomer_QueryString")
                'If Not String.IsNullOrEmpty(strQueryString) Then btnBack.PostBackUrl += "?" & strQueryString

                'Param pass from Previous: SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE
                ViewState("dtCurrentView") = Nothing
                'ViewstaeValue : SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE
                ViewState("SALESREP_CODE") = Trim(Request.QueryString("SALESREP_CODE"))
                ViewState("VISIT_ID") = Trim(Request.QueryString("VISIT_ID"))
                ViewState("CUST_CODE") = Trim(Request.QueryString("CUST_CODE"))
                ViewState("TXN_DATE") = Trim(Request.QueryString("TXN_DATE"))
                ViewState("CONT_CODE") = Trim(Request.QueryString("CONT_CODE"))
                Dim strPageIndicator As String = Trim(Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(strPageIndicator) AndAlso strPageIndicator = "CALLENQUIRY" Then
                    ViewState("POSTBACK_URL") = "~/iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx"
                    strQueryString = Session("CallEnquiry_QueryString")
                    If Not String.IsNullOrEmpty(strQueryString) Then ViewState("POSTBACK_URL") += "?" & strQueryString
                ElseIf strPageIndicator = "SRPRDFREQDTL" Then 'SRPRDFREQDTL
                    ViewState("POSTBACK_URL") = "~/iFFMR/Customize/SalesrepPrdFreq/SalesrepPrdFreqDtl.aspx"
                    strQueryString = Session("SRPRDFREQDTL_QueryString")
                ElseIf strPageIndicator = "MISSCALLANALYSISDTL" Then 'MISSCALLANALYSISDTL
                    ViewState("POSTBACK_URL") = "~/iFFMR/Customize/MissCallAnalysis/CallDtlMC.aspx"
                    strQueryString = Session("MISSCALLANALYSISDTL_QueryString")
                Else 'CALLBYCUSTOMER
                    Dim strPreviousPageURL As String = Session("CallAnalysisListByCustomer_QueryString")
                    If Not String.IsNullOrEmpty(strPreviousPageURL) Then ViewState("POSTBACK_URL") = btnBack.PostBackUrl & "?" & strPreviousPageURL
                End If

                If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then btnBack.PostBackUrl = ViewState("POSTBACK_URL")

                TimerControl_1.Enabled = True
                'RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "Collection")
            Dim aryDgList As New ArrayList
            dvList_Left.BorderWidth = 1
            dvList_Right.BorderWidth = 1

            tblcontrol.Visible = False

            aryDgList.Add(tblReport)
            wuc_ctrlpanel.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting

            RefreshDatabinding()
            'Dim blnAllowSorting As Boolean = dgList.AllowSorting
            'Dim blnAllowPaging As Boolean = dgList.AllowPaging

            'dgList.AllowSorting = False
            'dgList.AllowPaging = False
            'RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesOrder")

            'dgList.AllowPaging = blnAllowPaging
            'dgList.AllowSorting = blnAllowSorting
            'RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim dtDataList As DataTable = CType(ViewState("dtDataList"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
                dtDataList = GetDataList()
                ViewState("dtDataList") = dtDataList
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dvList_Left.DataSource = dtDataList
            dvList_Left.DataBind()
            dvList_Right.DataSource = dtDataList
            dvList_Right.DataBind()

            Dim dtTxnNo As DataTable = ViewState("TXN_NO_List")
            If dtTxnNo Is Nothing Then dtTxnNo = Get_TXN_NO_List()

            Dim ddl As DropDownList = Nothing
            If dvList_Left.Rows.Count > 0 AndAlso _
                dvList_Left.Rows(0).Cells.Count > 0 AndAlso _
                dvList_Left.Rows(0).Cells(1).FindControl("ddlTXNNO2") IsNot Nothing Then _
                    ddl = dvList_Left.Rows(0).Cells(1).FindControl("ddlTXNNO2")

            If ddl IsNot Nothing Then
                With ddl
                    .DataSource = dtTxnNo
                    .DataTextField = "TXN_NO"
                    .DataValueField = "TXN_NO"
                    .DataBind()
                    If ddl.Items.Count > 0 Then
                        Dim strValue As String = ViewState("TXN_NO")
                        If Not String.IsNullOrEmpty(strValue) AndAlso ddl.Items.FindByValue(strValue) IsNot Nothing Then
                            ddl.SelectedValue = strValue
                        End If
                    End If
                End With
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "BIND TXN_NO"
    'Protected Sub ddlTXNNO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTXNNO.SelectedIndexChanged
    '    Try
    '        ViewState("TXN_NO") = ddlTXNNO.SelectedValue
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ddlTXNNO_SelectedIndexChanged : " & ex.ToString)
    '    Finally
    '        UpdateDatagrid_Update()
    '    End Try
    'End Sub

    Protected Sub ddlTXNNO2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddl As DropDownList = Nothing
            If dvList_Left.Rows(0).Cells(1).FindControl("ddlTXNNO2") IsNot Nothing Then ddl = dvList_Left.Rows(0).Cells(1).FindControl("ddlTXNNO2")
            If ddl IsNot Nothing Then
                ViewState("TXN_NO") = ddl.SelectedValue
                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlTXNNO2_SelectedIndexChanged : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub ddl_TXNNO_DataBind()
        Try
            Dim dt As DataTable = Get_TXN_NO_List()
            ViewState("TXN_NO_List") = dt

            ddlTxnNo_Temp = New DropDownList
            With (ddlTxnNo_Temp)
                .DataSource = dt
                .DataTextField = "TXN_NO"
                .DataValueField = "TXN_NO"
                .DataBind()

                If dt.Rows.Count > 0 Then
                    .SelectedIndex = 0
                    ViewState("TXN_NO") = ddlTxnNo_Temp.SelectedValue
                    RenewDataBind()
                End If
            End With


            Dim ddl As DropDownList = Nothing
            If dvList_Left.Rows.Count > 0 AndAlso _
                dvList_Left.Rows(0).Cells.Count > 0 AndAlso _
                dvList_Left.Rows(0).Cells(1).FindControl("ddlTXNNO2") IsNot Nothing Then _
                    ddl = dvList_Left.Rows(0).Cells(1).FindControl("ddlTXNNO2")

            If ddl IsNot Nothing Then
                With ddl
                    .DataSource = dt
                    .DataTextField = "TXN_NO"
                    .DataValueField = "TXN_NO"
                    .DataBind()

                    If dt.Rows.Count > 0 Then
                        .SelectedIndex = 0
                        ddlTXNNO2_SelectedIndexChanged(ddl, Nothing)
                    End If
                End With
            Else
                RefreshDatabinding()
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".ddl_TXNNO_DataBind : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Function Get_TXN_NO_List() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE
            Dim strUserID, strPrincipalID, strPrincipalCode, strCustCode, strVisitID, strSalesrepCode, strTxnDate As String
            strUserID = Session.Item("UserID")
            strPrincipalID = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            strVisitID = ViewState("VISIT_ID")
            strCustCode = ViewState("CUST_CODE")
            strTxnDate = ViewState("TXN_DATE")

            Dim clsSalesDB As New rpt_Sales.clsSalesOrder
            'Accepted param: strUserID,strVisitID,strSalesrepCode,strCustCode
            DT = clsSalesDB.GET_ORD_HDR_TXNNO(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strVisitID, strCustCode, strTxnDate)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Get_TXN_NO_List : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "CUSTOM DGLIST"
    'Private Function GenerateAllColumnName(ByRef dtToBind As DataTable) As ArrayList
    '    Dim aryList As New ArrayList
    '    Try
    '        For Each DC As DataColumn In dtToBind.Columns
    '            aryList.Add(DC.ColumnName.ToUpper)
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".GenerateAllColumnName : " & ex.ToString)
    '    End Try
    '    Return aryList
    'End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            aryDataItem.Clear()
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_SalesOrder.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn

                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn
                        Dim csColumnStyle As ColumnStyle = CF_SalesOrder.ColumnStyle(ColumnName)
                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        dgColumn.ItemStyle.HorizontalAlign = IIf(csColumnStyle.HorizontalAlign = HorizontalAlign.NotSet, HorizontalAlign.Left, csColumnStyle.HorizontalAlign)
                        If Not String.IsNullOrEmpty(csColumnStyle.FormatString) Then
                            dgColumn.DataFormatString = csColumnStyle.FormatString
                            dgColumn.HtmlEncode = False
                        End If


                        dgColumn.HeaderText = CF_SalesOrder.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        ''Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
            aryDataItem = _aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer
                    Dim iIndex As Integer
                    Dim intRowCount As Integer = dgList.Rows.Count

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_SalesOrder.GetOutputFormatString(li.Text), CDbl(li.Value))
                        End If
                    Next
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetDataList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE,TXN_NO
            Dim strUserID, strPrincipalID, strPrincipalCode, strCustCode, strVisitID, strSalesrepCode, strTXNDate, strTXNNO As String
            strUserID = Session.Item("UserID")
            strPrincipalID = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            strVisitID = ViewState("VISIT_ID")
            strCustCode = ViewState("CUST_CODE")
            strTXNDate = ViewState("TXN_DATE")
            strTXNNO = ViewState("TXN_NO")

            If String.IsNullOrEmpty(strTXNNO) Then Exit Try
            Dim clsSalesDB As New rpt_Sales.clsSalesOrder
            DT = clsSalesDB.GET_ORD_HDR(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strVisitID, strCustCode, strTXNDate, strTXNNO)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetDataList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strTXNNO, strTXNDate As String
            ''ViewstaeValue : SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE,TXN_NO
            'Dim strUserID, strPrincipalID, strPrincipalCode, strCustCode, strVisitID, strSalesrepCodeAs String
            'strUserID = Session.Item("UserID")
            'strPrincipalID = session("PRINCIPAL_ID")
            'strPrincipalCode = session("PRINCIPAL_CODE")
            'strSalesrepCode = ViewState("SALESREP_CODE")
            'strVisitID = ViewState("VISIT_ID")
            'strCustCode = ViewState("CUST_CODE")
            strTXNDate = ViewState("TXN_DATE")
            strTXNNO = ViewState("TXN_NO")

            If Not String.IsNullOrEmpty(strTXNDate) Then wuc_ctrlpanel.SelectedDateTimeValue = Date.ParseExact(strTXNDate, "yyyy-MM-dd", Nothing)
            ViewState("SelectedDate") = wuc_ctrlpanel.SelectedDateTimeString


            If String.IsNullOrEmpty(strTXNNO) Then Exit Try
            Dim clsSalesDB As New rpt_Sales.clsSalesOrder
            'Accepted param: strUserID,strVisitID,strSalesrepCode,strCustCode
            DT = clsSalesDB.GET_ORD_DET(strTXNNO)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                '"FOC", "QTY", "UNIT_PRICE", "AMOUNT"
                strColumnName = DC.ColumnName.ToUpper
                If strColumnName Like "FOC*" OrElse _
                 strColumnName Like "QTY" OrElse _
                 strColumnName Like "NET*PRICE" OrElse _
                 strColumnName Like "*AMT" Then
                    liColumnField = New ListItem(strColumnName.ToUpper, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl_1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl_1.Tick
        ddl_TXNNO_DataBind()
        TimerControl_1.Enabled = False
    End Sub


End Class

Public Class CF_SalesOrder

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "FOC_QTY"
                strFieldName = "FOC"
            Case "QTY"
                strFieldName = "QTY"
            Case "UOM_CODE"
                strFieldName = "UOM"
            Case "UNIT_PRICE"
                strFieldName = "Unit Price"
            Case "DISC_AMT"
                strFieldName = "Discount"
            Case "AMT"
                strFieldName = "Total Value"
            Case "NET_PRICE"
                strFieldName = "Net Value"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If (strColumnName = "UOM_CODE") Then
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal ColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case ColumnName
                Case "FOC_QTY", "QTY"
                    strStringFormat = "{0:#,0}"
                Case "UNIT_PRICE", "AMT", "DISC_AMT", "NET_PRICE"
                    strStringFormat = "{0:#,0.00}"
                Case Else
                    strStringFormat = ""
            End Select

        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function


    'Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
    '    Dim CS As New ColumnStyle
    '    Try
    '        With CS
    '            Dim strColumnName As String = ColumnName.ToUpper
    '            Select Case strColumnName
    '                Case "PRD_CODE", "UOM"
    '                    .HorizontalAlign = HorizontalAlign.Center
    '                Case "PRD_NAME"
    '                    .HorizontalAlign = HorizontalAlign.Left
    '                Case "FOC_QTY", "QTY", "UNIT_PRICE", "AMT"
    '                    .HorizontalAlign = HorizontalAlign.Right
    '            End Select
    '            .FormatString = GetOutputFormatString(ColumnName)
    '        End With

    '    Catch ex As Exception

    '    End Try
    '    Return CS
    'End Function
End Class


