﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Config.aspx.vb" Inherits="Admin_Device_Config_Config" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Device Configuration</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ValidateAllChecked() {
            if ($('input[name$="chkbValue"]').length == $('input[name$="chkbValue"]:checked').length) {
                $('input[name$="chkbValueAll"]').attr('checked', true);
            }
            else {
                $('input[name$="chkbValueAll"]').attr('checked', false);
            }
        }
        function ToggleAllCheckbox(domElement) {
            $('input[name$="chkbValue"]').each(function () {
                $(this).attr('checked', domElement.checked);
            })
        }

        function OpenPopup(key, type) {
            $("#divLoading").css("display", "block");
            $find('ModalPopupCall').show();

            ws_Config.ReturnSettingTeamSR(key, type, onSuccessLoad, onFailLoad);
            //ws_Config.ReturnSettingTeamSR('sfms_lock', 'SR', onSuccessLoad, onFailLoad);
        }

        function onSuccessLoad(result) {
            $("#divLoading").css("display", "none");
            var gridView = $find('<%= dgAjaxList.ClientID %>'); gridView.set_dataSource(result.Rows); gridView.dataBind();
            //alert((gridView.get_dataKeys())(0));
        }

        function onFailLoad(ex) {
            $("#divLoading").css("display", "none");
            alert(ex.get_message());
        }
        
        function onSelectedIndexChanged(sender, e) {

            var code = document.getElementById('<%=dgAjaxList.ClientID%>').childNodes[1].childNodes[e.get_row().get_rowIndex()].childNodes[1].innerHTML;
            var dtltype = $find('<%= dgAjaxList.ClientID %>').get_dataKeys()[e.get_row().get_rowIndex()];

            $find('ModalPopupCall').hide();

            //window.location = './ConfigDtl.aspx?type=' + dtltype + '&code=' + code + '&cat=' + $('#hfCurrentCat').val();
            redirect(dtltype, code, $('#hfCurrentCat').val());
            return false;
        }
        function redirect(type, code, cat) {
            window.location = './ConfigDtl.aspx?type=' + type + '&code=' + code + '&cat=' + cat;
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout">
    <form id="frmConfig" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300">
        <Services>
                <asp:ServiceReference Path="~/DataServices/ws_Config.asmx" />
         </Services>
        </asp:ScriptManager>
        <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="updPnlConfig" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err" 
                ></asp:Label>
                
            <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0" style="margin:10px 10px">
                <tr>
                    <td vAlign="top" width="15%">
                        <ccGV:clsGridView ID="dgCategoryList" runat="server" ShowFooter="false" AllowSorting="True"
                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" 
                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                            FreezeRows="0" GridWidth="" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TYPE">
                            <Columns>                                                     
                                <asp:ButtonField DataTextField="TYPE" HeaderText="Category" CommandName="Select" AccessibleHeaderText="Select" ButtonType="Link" />                                                                                                                                                      
                            </Columns>                                                
                        </ccGV:clsGridView>
                    </td>
                    <td vAlign="top" width="50%">
                    <asp:Label ID="lblMesg" runat="server" CssClass="cls_label">Please select a category from left to edit.</asp:Label>
                    <asp:Button ID="btnSave" runat="server" Text="Save" Visible="False" cssclass="cls_button" /><br /><br />
                         <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowSorting="False"
                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                            FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="SETTING_CODE" Visible="False">
                            <Columns>                                             
                                <asp:BoundField DataField="SETTING_CODE" HeaderText="Setting Code" ReadOnly="True" SortExpression="SETTING_CODE" HeaderStyle-HorizontalAlign="Center">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>     
                                <asp:BoundField DataField="SETTING_NAME" HeaderText="Setting Name" ReadOnly="True" SortExpression="SETTING_NAME" HeaderStyle-HorizontalAlign="Center">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>                                                        
	                           <asp:BoundField DataField="SETTING_DESC" HeaderText="Setting Description" ReadOnly="True" SortExpression="SETTING_DESC" HeaderStyle-HorizontalAlign="Center">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField headertext = "Value" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70">
                                    <headertemplate>
                                      <asp:checkbox id="chkbValueAll"
                                        CssClass="cls_checkbox"
                                        runat="server"/> Value
                                    </headertemplate>
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>                                               
                                         <asp:CheckBox runat="server"  CssClass="cls_checkbox" ID="chkbValue" checked='<%# Container.DataItem("VALUE") %>'/>                                      
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField> 
	                            <asp:TemplateField headertext = "Value 2" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">					                            
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtValue2" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("VALUE2") %>'></asp:TextBox>                                
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField headertext = "Value 3" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">                                                
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtValue3" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("VALUE3") %>'></asp:TextBox>                                               
                                    </ItemTemplate>
                                </asp:TemplateField>    
                                <asp:TemplateField headertext = "Value 4" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">                                                
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                         <asp:TextBox ID="txtValue4" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("VALUE4") %>'></asp:TextBox>                                               
                                    </ItemTemplate>
                                </asp:TemplateField>     
                                <asp:TemplateField headertext = "Team Count" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">                                                
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                         <%--<asp:Label ID="lblTeamCount" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("TEAM_COUNT") %>'></asp:Label>--%>      
                                        <asp:HyperLink ID="hlTeamCount" runat="server" Text='<%# Container.DataItem("TEAM_COUNT") %>' style="cursor:pointer"></asp:HyperLink>                                         
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField headertext = "SR Count" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30">                                                
                                    <headerstyle horizontalalign="Center" />
                                    <ItemTemplate>
                                         <%--<asp:Label ID="lblSRCount" runat="server" Width="70" MaxLength="10" Text='<%# Container.DataItem("SR_COUNT") %>'></asp:Label>   --%>
                                         <asp:HyperLink ID="hlSRCount" runat="server" Text='<%# Container.DataItem("SR_COUNT") %>' style="cursor:pointer"></asp:HyperLink>                                             
                                    </ItemTemplate>
                                </asp:TemplateField>                                         
                            </Columns>                                                
                             <FooterStyle CssClass="GridFooter" />
                             <HeaderStyle CssClass="GridHeader" />
                             <AlternatingRowStyle CssClass="GridAlternate" />
                             <RowStyle CssClass="GridNormal" />
                             <PagerSettings Visible="False" />
                        </ccGV:clsGridView>	</td>		
                            
                    </tr>
            </table>
            
            
        <asp:HiddenField ID="hfCurrentCat" runat="server" />
        </ContentTemplate>
        </asp:UpdatePanel>
        
        <!-- PopUp -->
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px"
            CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <span style="float:left;width:95%"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="Details" /></span>
                <%--<input id="Button2" type="button" class="cls_button" value="Close" onclick="javascript:$find('ModalPopupCall').hide();" />--%>
                <span style="float:left;width:5%"><img src="../../images/Close.png" onclick="javascript:$find('ModalPopupCall').hide();" style="cursor:pointer; float:right" alt="Close" /></span>
            </asp:Panel>
            <div id="divLoading" style="display:none"><img src="../../images/indicator.gif" alt="Loading" />&nbsp;<span class="cls_label">Loading...</span></div>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'btnClose')">
                
                <fieldset style="padding-left: 10px; width: 98%">
                    <asp:Panel ID="pnldgPrdList" runat="server" ScrollBars="Auto" >
                        <AjaxData:GridView ID="dgAjaxList" runat="server" CssClass="Grid" DataKeyName="TYPE"
                            HeaderStyle-CssClass="GridHeader" RowStyle-CssClass="GridNormal" AlternatingRowStyle-CssClass="GridAlternate" 
                            Width="100%" CellSpacing="0" EditCommandEvent="onSelectedIndexChanged">
                            <Columns>      
                                <AjaxData:GridViewCommandColumn ButtonType="Image" EditImageUrl="~/images/ico_tick.gif" EditText="Select" ShowEditButton="true" HeaderStyle-Width="5" HeaderText="" />              
                                <AjaxData:GridViewBoundColumn HeaderText="Code" DataField="CODE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" />
                                <AjaxData:GridViewBoundColumn HeaderText="Name" DataField="NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" />                                
                            </Columns>
                            <EmptyDataTemplate>There is no records available.</EmptyDataTemplate>
                            <EmptyDataRowStyle CssClass="cls_label_header" HorizontalAlign="Center" />
                        </AjaxData:GridView>
                    </asp:Panel>
                    <center>
                        <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" style="display:none" />
                    </center>
                </fieldset>
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender 
            ID="ModalPopup" runat="server" 
            BehaviorID="ModalPopupCall"
            TargetControlID="btnHidden" 
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="False" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
        <!-- PopUp -->
    
    </form>
    
    
</body>
</html>
