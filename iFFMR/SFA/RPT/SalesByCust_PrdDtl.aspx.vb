Imports System.Data

Partial Class iFFMR_SFA_RPT_SalesByCust_PrdDtl
    Inherits System.Web.UI.Page
    Dim _cust_code As String
    Dim _agency_code As String
    Dim _year As String
    Dim _month As String
    Dim prdDS As DataSet
    Dim agencyDT As DataTable
    Dim prdDT As DataTable

    Public ReadOnly Property PageName() As String
        Get
            Return "RPT_SalesByCust_PrdDtl"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        

        If Not Page.IsPostBack Then
            TimerControl1.Enabled = True
        End If

    End Sub

    Private Function GetRecList() As dataset
        Dim objSFA As rpt_SFA.SFA_RPT = New rpt_SFA.SFA_RPT
        Dim DS As DataSet = Nothing
        Try
            DS = objSFA.GET_SalesByCust_GETPrdDtl(_year, _month, Session("SALESREP_LIST"), _cust_code, _agency_code)

            'If DS Is Nothing OrElse DS.Columns.Count = 0 Then Return DS
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        Return DS
    End Function

    Private Sub BindRpt()
        Try
            _cust_code = Request.QueryString("cust_code")
            _agency_code = Request.QueryString("agency")
            _year = Request.QueryString("year")
            _month = Request.QueryString("month")

            If _cust_code <> "" And _year <> "" And _month <> "" Then
                prdDS = GetRecList()
                agencyDT = prdDS.Tables(0)
                prdDT = prdDS.Tables(1)

                RptOuter.DataSource = agencyDT
                RptOuter.DataBind()
                UpdateDatagrid.Update()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindRpt : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    Protected Sub RptOuter_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RptOuter.ItemDataBound
        Dim _inner_rpt As Repeater
        Dim _inner_dv As DataView
        Dim drv As DataRowView
        Dim _agency_code As String

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            _inner_rpt = CType(e.Item.FindControl("RptInner"), Repeater)
            drv = CType(e.Item.DataItem, DataRowView)
            _agency_code = drv("AGENCY_CODE")
            _inner_dv = New DataView(prdDT, "AGENCY_CODE='" & _agency_code & "'", "PRD_NAME", DataViewRowState.CurrentRows)

            _inner_rpt.DataSource = _inner_dv
            _inner_rpt.DataBind()
        End If
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        BindRpt()
        TimerControl1.Enabled = False
    End Sub
End Class
