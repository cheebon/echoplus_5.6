﻿Option Explicit On

Imports System.Data
Imports adm_SuperAdmin
Partial Class Admin_Root_SubmodulePrincipal_SubmodulePrincipalList
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "Submodule Principal List"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Submodule Principal"
            If Not IsPostBack Then
                LoadCountryDDL()
                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging


        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsSubmodulePrincipal
        Try

            DT = obj.SubModulePrincipalList(ddlCountry.SelectedValue, ddlPrincipal.SelectedValue, Session("UserID"))


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region
#Region "Function"
    Private Sub LoadCountryDDL()
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            obj = New adm_SuperAdmin.clsSubmodulePrincipal

            dt = obj.LoadCountryDDL(Session("UserID"))

            With ddlCountry
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "COUNTRY_NAME"
                .DataValueField = "COUNTRY_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With

            With ddlPrincipal
                .Items.Clear()
                .Items.Add(New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
    '    Try
    '        Response.Redirect("SubmodulePrincipalDtl.aspx?action=CREATE", False)
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub


    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim obj As New adm_SuperAdmin.clsSubmodulePrincipal

        Try
            Dim index As Integer = 0
            Dim row As GridViewRow
            Dim strPrincipalID As String
            Dim strPrincipalCode As String
            Dim lnkPrincipalCode As LinkButton

            If Integer.TryParse(e.CommandArgument, index) = True Then
                row = IIf(index >= 0 AndAlso index <= dgList.Rows.Count, dgList.Rows(index), Nothing)
                strPrincipalID = Trim(dgList.DataKeys(row.RowIndex).Value)
                lnkPrincipalCode = DirectCast(dgList.Rows(row.RowIndex).Cells(4).Controls(0), LinkButton) 'Trim(DirectCast(DirectCast(DirectCast(dgList.Rows(row.RowIndex).Cells(1).Controls(0),System.Web.UI.Control),System.Web.UI.WebControls.DataControlLinkButton).Text.ToString)
                If lnkPrincipalCode IsNot Nothing Then
                    strPrincipalCode = lnkPrincipalCode.Text
                Else
                    strPrincipalCode = ""
                End If

                Select Case e.CommandName
                    'Case "Delete"
                    '    With obj
                    '        '.Delete(strSubmodulePrincipalID, Session("UserID"))
                    '    End With
                    '    Response.Redirect("SubmodulePrincipalList.aspx", False)

                    Case "Details"
                        Response.Redirect("SubmodulePrincipalDtl.aspx?PRINCIPAL_ID=" & strPrincipalID & "&PRINCIPAL_CODE=" & strPrincipalCode & "&action=VIEW", False)

                    Case "Modify"
                        Response.Redirect("SubmodulePrincipalDtl.aspx?PRINCIPAL_ID=" & strPrincipalID & "&PRINCIPAL_CODE=" & strPrincipalCode & "&action=EDIT", False)

                End Select
            End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Dim imgEdit, imgDelete As ImageButton

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Dim btnDelete As ImageButton = CType(e.Row.Cells(5).Controls(0), ImageButton)
                'If btnDelete IsNot Nothing Then
                '    btnDelete.OnClientClick = "if (confirm('Are you sure want to delete?') == false) { window.event.returnValue = false; return false; }"
                'End If

                'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.PRINCIPAL, SubModuleAction.Edit) Then
                '    imgEdit = CType(e.Row.Cells(10).Controls(0), ImageButton)
                '    imgEdit.Visible = False
                'End If

                'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.PRINCIPAL, SubModuleAction.Delete) Then
                '    imgDelete = CType(e.Row.Cells(9).Controls(0), ImageButton)
                '    imgDelete.Visible = False
                'End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RenewDataBind()
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            If ddlCountry.SelectedIndex = 0 Then
                ddlPrincipal.Items.Clear()
                ddlPrincipal.Items.Add(New ListItem("-- Select --", ""))
            Else
                obj = New adm_SuperAdmin.clsSubmodulePrincipal
                dt = obj.LoadPrincipalDDL(ddlCountry.SelectedValue, Session("UserID"))

                With ddlPrincipal
                    .Items.Clear()
                    .DataSource = dt
                    .DataValueField = "PRINCIPAL_ID"
                    .DataTextField = "PRINCIPAL_NAME"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- Select --", ""))
                    .SelectedIndex = 0
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression Like ("* DESC") Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
End Class
