﻿Imports System.Data
Imports adm_SalesrepGrp

Partial Class Admin_SalesrepGrp_SalesrepGrpDtl
    Inherits System.Web.UI.Page

#Region "Enumeration"
    Enum Mode
        Edit = 1
        Create = 2
        View = 3
        Copy = 4
    End Enum
#End Region

#Region "Properties"

    Public Property OriEditSalesrepGrpName() As String
        Get
            Return Viewstate("OriEditSalesrepGrpName")
        End Get
        Set(ByVal value As String)
            Viewstate("OriEditSalesrepGrpName") = value
        End Set
    End Property

    Public Property CurrentMode() As Mode
        Get
            Return ViewState("CurrentMode")
        End Get
        Set(ByVal value As Mode)
            ViewState("CurrentMode") = value
        End Set
    End Property

    Public Property SalesrepGrpID() As String
        Get
            Return ViewState("SalesrepGrpID")
        End Get
        Set(ByVal value As String)
            ViewState("SalesrepGrpID") = value
        End Set
    End Property

    Public ReadOnly Property SessionUserCode() As String
        Get
            Return Trim(Session("UserCode"))
        End Get
    End Property

    Public ReadOnly Property UserID() As String
        Get
            Return Session("UserID").ToString
        End Get
    End Property

    Public Property SearchType() As String
        Get
            Return ViewState("SearchType")
        End Get
        Set(ByVal value As String)
            ViewState("SearchType") = value
        End Set
    End Property
    Public Property SearchValue() As String
        Get
            Return ViewState("SearchValue")
        End Get
        Set(ByVal value As String)
            ViewState("SearchValue") = value
        End Set
    End Property
#End Region

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            ShowExistsMessage(False)
            IsBsAdmin()

            With wuc_lblheader
                .Title = "Salesrep Group"
                .DataBind()
                .Visible = True
            End With

            If Not Page.IsPostBack Then
                AcceptIncomingRequest()
                LoadCountryPrincipalDDL()
                ChangeMode(CurrentMode)
            End If
        Catch ex As Exception
            ExceptionMsg("UserDtl.Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "Function/Sub"
    Private Sub IsBsAdmin()
        'If SessionUserCode = "" Or IsNothing(SessionUserCode) Then
        '    Response.Redirect("../../index.aspx")
        'ElseIf Not SessionUserCode.ToUpper.Contains("BSADMIN") Then
        '    Response.Redirect("../../index.aspx")
        'End If
        If SessionUserCode = "" Or IsNothing(SessionUserCode) Then
            Response.Redirect("../../index.aspx")
        ElseIf Not SessionUserCode.ToUpper.Contains("BSADMIN") Then
           
        End If
    End Sub
    Private Sub ShowExistsMessage(Optional ByVal flag As Boolean = True)
        lblExists.Visible = flag
    End Sub
    Private Sub ChangeMode(ByVal mode As Mode)
        CurrentMode = mode
        ToggleControlEditability(mode)
        Select Case mode
            Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.Create

            Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.Edit
                Bind()
            Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.View
                Bind()
            Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.Copy
                Bind()
        End Select
    End Sub
    Private Sub ToggleControlEditability(ByVal mode As Mode)
        If Not SessionUserCode.ToUpper.Contains("BSADMIN") Then
            Wuc_ddlCountry.SetEnable = False
            Wuc_ddlPrincipal.SetEnable = False
            txtSalesrepGrpName.Enabled = False
            txtSalesrepGrpDesc.Enabled = False
            txtCriteria.Enabled = False
            cmdCancel.Visible = False
            cmdSave.Visible = False
            cmdReturn.Visible = True
            cmdEdit.Visible = False
        Else
            Select Case mode
                Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.Create, Admin_SalesrepGrp_SalesrepGrpDtl.Mode.Copy
                    Wuc_ddlCountry.SetEnable = True
                    Wuc_ddlPrincipal.SetEnable = True
                    txtSalesrepGrpName.Enabled = True
                    txtSalesrepGrpDesc.Enabled = True
                    txtCriteria.Enabled = True
                    cmdCancel.Visible = True
                    cmdSave.Visible = True
                    cmdReturn.Visible = False
                    cmdEdit.Visible = False
                Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.Edit
                    Wuc_ddlCountry.SetEnable = True
                    Wuc_ddlPrincipal.SetEnable = True
                    txtSalesrepGrpName.Enabled = True
                    txtSalesrepGrpDesc.Enabled = True
                    txtCriteria.Enabled = True
                    cmdCancel.Visible = True
                    cmdSave.Visible = True
                    cmdReturn.Visible = False
                    cmdEdit.Visible = False
                Case Admin_SalesrepGrp_SalesrepGrpDtl.Mode.View
                    Wuc_ddlCountry.SetEnable = False
                    Wuc_ddlPrincipal.SetEnable = False
                    txtSalesrepGrpName.Enabled = False
                    txtSalesrepGrpDesc.Enabled = False
                    txtCriteria.Enabled = False
                    cmdCancel.Visible = False
                    cmdSave.Visible = False
                    cmdReturn.Visible = True
                    cmdEdit.Visible = True
            End Select
        End If
       
    End Sub

    Private Sub AcceptIncomingRequest()
        Dim action, salesrepgrp_id, search_type, search_value As String

        action = Trim(Request.QueryString("action"))
        salesrepgrp_id = Trim(Request.QueryString("salesrepgrp_id"))
        search_type = Trim(Request.QueryString("search_type"))
        search_value = Trim(Request.QueryString("search_value"))

        SearchType = search_type
        SearchValue = search_value

        Select Case action.ToUpper
            Case "CREATE"
                CurrentMode = Mode.Create
            Case "EDIT"
                CurrentMode = Mode.Edit
                SalesrepGrpID = salesrepgrp_id
            Case "VIEW"
                CurrentMode = Mode.View
                SalesrepGrpID = salesrepgrp_id
            Case "COPY"
                CurrentMode = Mode.Copy
                SalesrepGrpID = salesrepgrp_id
        End Select
    End Sub
    Private Sub LoadCountryPrincipalDDL()
        'Call Country
        With Wuc_ddlCountry
            .UserCode = SessionUserCode
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True

        End With

        'Call Principal
        With Wuc_ddlPrincipal
            .CountryCode = Wuc_ddlCountry.SelectedValue
            .UserCode = SessionUserCode
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        Wuc_ddlCountry_SelectedIndexChanged(Nothing, Nothing)
    End Sub
    Private Sub CreateSalesrepGrp()
        Dim SalesrepGrp As New clsSalesrepGrp
        Dim SalesrepGrpMani As clsSalesrepGrpManipulator
        Dim aryPrincipal As Array
        Dim salesrepgrp_id As Long
        Try
            aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            With SalesrepGrp
                .Principal = Trim(aryPrincipal(0))
                .SalesrepGroupName = txtSalesrepGrpName.Text.Trim
                .SalesrepGroupDesc = txtSalesrepGrpDesc.Text.Trim
                .CriteriaValue = txtCriteria.Text.Trim
            End With

            SalesrepGrpMani = New clsSalesrepGrpManipulator(SalesrepGrp)

            If Not SalesrepGrpMani.CheckNameExists Then
                salesrepgrp_id = SalesrepGrpMani.Create(UserID)
                SalesrepGrpID = salesrepgrp_id
                ChangeMode(Mode.View)
                OriEditSalesrepGrpName = ""
            Else
                ShowExistsMessage()
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub EditSalesrepGrp()
        Dim SalesrepGrp As New clsSalesrepGrp
        Dim SalesrepGrpMani As clsSalesrepGrpManipulator
        Dim aryPrincipal As Array
        Try
            aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            With SalesrepGrp
                .Principal = Trim(aryPrincipal(0))
                .SalesrepGroupName = txtSalesrepGrpName.Text.Trim
                .SalesrepGroupDesc = txtSalesrepGrpDesc.Text.Trim
                .CriteriaValue = txtCriteria.Text.Trim
                .SalesrepGrpID = SalesrepGrpID
            End With

            SalesrepGrpMani = New clsSalesrepGrpManipulator(SalesrepGrp)

            If OriEditSalesrepGrpName <> txtSalesrepGrpName.Text.Trim Then
                If Not SalesrepGrpMani.CheckNameExists Then
                    SalesrepGrpMani.Update(UserID)
                    ChangeMode(Mode.View)
                    OriEditSalesrepGrpName = ""
                Else
                    ShowExistsMessage()
                End If
            Else
                SalesrepGrpMani.Update(UserID)
                ChangeMode(Mode.View)
                OriEditSalesrepGrpName = ""
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function ReadValue(ByVal obj As Object) As String
        If IsDBNull(obj) Then
            Return String.Empty
        ElseIf IsNothing(obj) Then
            Return String.Empty
        Else
            Return obj.ToString.Trim
        End If
    End Function
#End Region

#Region "Bind data"
    Private Sub Bind()
        Dim dt As DataTable
        Dim objMani As clsSalesrepGrpManipulator
        Dim objSalesrepGrp As clsSalesrepGrp

        Try
            objSalesrepGrp = New clsSalesrepGrp
            objSalesrepGrp.SalesrepGrpID = SalesrepGrpID

            objMani = New clsSalesrepGrpManipulator(objSalesrepGrp)
            dt = objMani.RetrieveBySalesrepGrpID

            AlignData(dt)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objMani = Nothing
            objSalesrepGrp = Nothing
        End Try
    End Sub
    Private Sub AlignData(ByVal dt As DataTable)
        Try
            Wuc_ddlCountry.SelectedValue = ReadValue(dt.Rows(0)("country_id")) & "@" & ReadValue(dt.Rows(0)("country_code"))
            Wuc_ddlCountry_SelectedIndexChanged(Nothing, Nothing)

            Wuc_ddlPrincipal.SelectedValue = ReadValue(dt.Rows(0)("principal_id")) & "@" & ReadValue(dt.Rows(0)("principal_code"))
            txtSalesrepGrpName.Text = ReadValue(dt.Rows(0)("SALESREP_GRP_NAME"))
            txtSalesrepGrpDesc.Text = ReadValue(dt.Rows(0)("SALESREP_GRP_DESC"))
            txtCriteria.Text = ReadValue(dt.Rows(0)("CRITERIA_VALUE"))
            OriEditSalesrepGrpName = txtSalesrepGrpName.Text.Trim
        Catch ex As Exception
            Throw
        End Try

    End Sub
#End Region

#Region "Events"
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlCountry_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlCountry.SelectedIndexChanged
        Dim ary As Array

        Try
            'If CurrentMode = Mode.Create Then
            ary = Split(Wuc_ddlCountry.SelectedValue, "@")

            'Call Principal
            With Wuc_ddlPrincipal
                If Wuc_ddlCountry.SelectedValue <> "0" Then
                    .CountryCode = ary(1)
                Else
                    .CountryCode = ary(0) 'Wuc_ddlCountry.SelectedValue
                End If
                .UserCode = SessionUserCode
                .RequiredValidation = True
                .AutoPostBack = True
                .DataBind()
                .Visible = True
            End With
            'End If

        Catch ex As Exception
            ExceptionMsg("SalesrepGrpDtl.Wuc_ddlCountry_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub
#End Region

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Try
            Select Case CurrentMode
                Case Mode.Create, Mode.Copy
                    CreateSalesrepGrp()
                Case Mode.Edit
                    EditSalesrepGrp()
                    'ChangeMode(Mode.View)
            End Select

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReturn.Click
        Response.Redirect("SalesrepGrpList.aspx?search_type=" & SearchType.Trim & "&search_value=" & SearchValue.Trim, False)
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Select Case CurrentMode
            Case Mode.Create, Mode.Copy
                Response.Redirect("SalesrepGrpList.aspx?search_type=" & SearchType.Trim & "&search_value=" & SearchValue.Trim, False)
            Case Mode.Edit
                ChangeMode(Mode.View)
        End Select

    End Sub

    Protected Sub cmdEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        ChangeMode(Mode.Edit)
        OriEditSalesrepGrpName = txtSalesrepGrpName.Text.Trim
    End Sub
End Class
