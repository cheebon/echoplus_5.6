Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext


''' <summary>
''' Keep all Sub Module ID that uniquely identify every page.
''' </summary>
''' <remarks>
''' Total record : 29
''' updated by   : Alex Chia
''' Date         : 2006-11-16
''' </remarks>
''' 
Public Enum SubModuleType As Long
    UNDEFINE = 0
    USR = 1
    AR = 2
    USRGRP = 3
    PDACOMM = 4
    DRC = 5
    SFMSEXTRACT = 6
    SALES = 7
    SALESENQ = 8
    CABYDAY = 9
    CABYMTH = 10
    CABYCUST = 11
    CPROD = 12
    PREPLAN = 13
    SFMSACTY = 14
    CUSTLIST = 15
    CUSTGENINFO = 16
    CUSTCONTACT = 17
    CUSTDELADD = 18
    TXNDRC = 19
    TXNSFMS = 20
    TXNMSS = 21
    TXNSO = 22
    TXNTRA = 23
    PROD = 24
    CALLENQ = 25
    CPRODACTYDTL = 26
    CPRODDAILYACTY = 27
    PREPLANCUST = 28
    SALESINFOBYDATE = 29
    DATA = 30
    CUSTEXTRACT = 31
    DRCEXTRACT = 32
    MSSEXTRACT = 33
    SOEXTRACT = 34
    CALLCOV1 = 35
    CALLCOV2 = 36
    RELPERFORM = 37
    MTHOVER = 38
    SALESBYCUST = 39
    ACTYSALESBYINDSR = 40
    ACTSALESBYDAILYDTL = 41
    SALESORD = 42
    MESSAGING = 43
    REPORTING = 44
    PERFORMANCE = 45
    SUMMARY = 46
    CUSTPREPLAN = 47
    CUSTOPENITEM = 48
    CUSTARAGING = 49
    CUSTMTHSALESHIST = 50
    CUSTINVOICEHIST = 51
    CUSTSFMSHIST = 52
    CUSTDEALERREC = 53
    CUSTORDERSTATUS = 54
    CUSTDISC = 55
    CUSTPROMOCAMP = 56
    CUSTPLANHIST = 57
    CABYCONT = 58
    CABYSPEC = 59
    COMMUPDINFO = 60
    CABYSTRIKE = 61
    SFMSPRFM = 62
    DRCENQ = 63
    CHEQENQ = 64
    CUSTCONTINFO = 65
    TRAINFO = 66
    COLLINFO = 67
    PAFENQ = 68
    STKRETBYMTH = 69
    STKALLOC = 70
    CUSTSTATUS = 71
    TOP80CUSTBYREP = 72
    KEYPRD = 73
    SALESSUMM = 74
    CALLRATE = 103
    SALESTGTVSACT = 104
    SALESESTVSACT = 105
    DRCINFO = 115
    MSSINFO = 116
    AUDITLOG = 118
    AUDITLOGDTL = 119
    SALESBYCUSTCOUNT = 120
    DNINFO = 121
    SFMSEXTRACTION = 123
    CALLINFOBYMONTH = 124
    SALESFORCECAPACITY = 125
    DAILYCALLANALYSIS = 126
    DAILYCALLACTY = 127
    BCP = 344
    SFMS_CFG = 345
    MSS_CFG = 346

    'FFMA
    COMPANY = 128
    SALESAREA = 129
    WAREHOUSE = 130
    REGION = 131
    SALESTEAM = 132
    CUSTOMER = 133
    SHIPTO = 134
    CONTACT = 135
    PRODUCT = 136
    PRODUCTGROUP = 137
    SUPPLIER = 138
    STOCKALLOC = 139
    STANDARDPRICE = 140
    SPECIALPRICE = 141
    CUSTGRPPRICE = 142
    PRICEGRPPRICE = 143
    STANDARDBONUS = 144
    SPECIALBONUS = 145
    CUSTGRPBONUS = 146
    PRICEGRPBONUS = 147
    PACKAGE = 148
    FIELDACTY = 149
    MSSADV = 150
    FIELDFORCE = 151
    SALESTGT = 152
    REASON = 153
    ROUTE = 154
    PRICEGROUP = 155
    INBOX = 156
    OUTBOX = 157
    FIELDFORCECUST = 158
    FIELDFORCEROUTE = 159
    FIELDFORCEPRDGRP = 160
    FIELDFORCEACTY = 161
    FIELDFORCEMSS = 162
    FIELDFORCEPACKAGE = 163
    FIELDFORCEITINERARYTRANSFER = 164
    CALLBYDKSHCLASS = 165
    CALLBYSUPPLIERCLASS = 166
    CALLACTYHISTBYCONT = 167
    CALLACTYENQ = 168
    SAPLISTV2 = 225
    SAPLISTV4 = 242
    CUSTGPS = 298
    COMPANYCODE_BUPROFILE = 320
    COMPANYCODE_BANKPROFILE = 321
    COLLECTORPROFILE = 322
    COLLECTORCUSTOMERMAPPING = 323
    PRODUCTMUSTSELL = 324
    LEAD = 326
    PRODUCTMUSTSELLADV = 343

    'FFMR
    FIELDFORCEMTHACTY = 169
    FIELDFORCEPRDFREQ = 170
    FIELDFORCESFMSDETACTY = 172
    STOCKADJUST = 173
    STOCKMOVE = 174
    CALLANALYENQ = 175
    CALLANALYBYCONTCLASS = 176
    CPRODACTYSUP = 182
    STOCKTRANSFERLIST = 177
    NEWEQPLIST = 178
    APPLYSTOCKLIST = 179
    STOCKHIST = 180
    STOCKSTATUS = 181
    CPRODDAILYACTYCUST = 183
    DRCEXTRACTION = 184
    TECHNICIANSALESGRP = 185
    TECHNICIANITINERARY = 186
    TRAORDER = 187
    TECHNICIANITINERARYDTL = 188
    TECHNICIANFUNCLOC = 189
    TRAORDERLIST = 190
    TECHNICIANSALESOFF = 191
    ORDPRDMATRIX = 192
    SRCALLRATE = 193
    SRCALLRATEDTL = 194
    MANUFACTURER = 195
    SVCSTAT = 196
    SVCDTL = 197
    EQPHISTLIST = 198
    COLLENQ = 199
    CUZDRCCUST = 200
    CUZDRCCUSTCLASS = 201
    CUZDRCDISTRICT = 202
    CUZPACKAGE = 223

    SFA_ATCHART = 203
    SFA_PERF_M = 204
    SFA_PERF_Q = 205

    SFA_PERF2_Q = 206
    SFA_SALESBYAGENCY = 207
    SFA_SALESBYCUST = 208
    SFA_SALESBYPRODUCT = 209
    SFA_SALESBYCHANNEL = 210
    SFA_SALESBYREGION = 211


    UPLOADITINERARY = 213
    MTHPRDFREQ = 214
    MTHTERRITORY = 215
    MTHSPECIALITY = 216
    SRSCORECARD = 217
    MSS_CONT = 218
    MSS_CONT_DTL = 219

    SFMSEXTRACTION_2 = 220
    LEAVE = 221
    EASYLOADER = 222

    CALLDTLBYGROUP = 224
    SRGROUP = 226
    SAPV3 = 227
    DATA_V2 = 228
    SALES_CUST_PER_PRD = 229
    SALES_CUZ = 230

    CUSTINFO = 231
    CUSTCLASS = 232
    CUSTSALESTGT = 233
    CUSTSALESANALYSIS = 234
    CUSTPRDSALESANALYSIS = 235
    SOVARIANCE = 236
    PLAN_ACTUAL = 237
    CALLRATE_BYBRAND = 238

    TXNSFMSCUZ = 239
    TRAList = 240
    TRAReport = 241
    SAPLIST_V4 = 242
    MERCHSFMS = 243
    SALESREP_KPI = 244
    DAYCALLDISTRIBUTION = 245
    DRCCuzList = 246
    PREPLANEXTRACT = 247
    PREPLANCUZ = 248
    CALLACHIEVEMENT = 249
    CALLBYDAYADV = 250
    CALLBYMONTHADV = 251
    CUSTPROFILEENQ = 252


    CUSTPROFILEMAINTAIN = 253
    CUZMSSENQUIRY = 254
    CUZSFMSENQUIRY = 255

    MSSEXTRACTION = 258

    MSS_TEAM_MAPPING = 261
    MISSCALLANALYSIS = 262
    MISSCALLANALYSISDTL = 263
    CFGDEVICE = 264
    CFGDEVICEDTL = 265 'same as CFGDEVICE, but require a new id for link redirection

    SALES_BY_CHAIN = 270
    SALES_BY_CUST = 271

    REMAIN_CALL = 272
    PRODUCT_MATRIX = 273

    STK_CONSUMP_BY_BIMONTH = 274
    STK_CONSUMP_BY_BIMONTH_DTL = 275
    STK_CONSUMP_BY_QUARTER = 276
    STK_CONSUMP_BY_QUARTER_DTL = 277

    EASYLOADERV2 = 278
    ELACCESSRIGHT = 279

    MSSADV2 = 280
    SALESTEAM_SUB = 281

    FIELDFORCEMTHACTYPrd = 283

    LINKEPONDEMAND6 = 285

    STK_CONSUMP_BY_QUADMONTH = 286
    STK_CONSUMP_BY_QUADMONTH_DTL = 287
    SFMSEXTRACTION_EXTRACAT = 288
    ActCallKPI = 289
    ActCallKPIbySR = 290

    TRAREPORT_EDI = 291
    TRAORDERLISTV2 = 293
    CUSTGENERATELOC = 297

    SFMS_EXTRACT = 300
    VISIT_EXTRACT = 301
    CUST_CONT_EXTRACT = 302
    CUST_CONT_PRD_EXTRACT = 303

    SFE_KPI = 304
    DASHBOARDXFIELD = 305
    CUST_TEAM_CLASS = 306
    CUSTTGTPRD = 307
    PRDSEQ = 308
    DETAIL_BRAND = 310
    DET_COVERAGE_BY_SPECIALTY = 311
    SFEKPIREPORT = 312
    PROMO_SO = 314

    CONTKPI = 315
    DET_CALL_BY_AREA = 319

    CALLCOV1ADV = 313
    CUSTCONTCALLINFO = 309
    CUSTCONTCOVERAGEINFO = 325
    CUSTCONTNONCOVERAGEINFO = 316
    CUSTCONTEFFECTIVECALLINFO = 317
    CUSTCONTNONEFFECTIVEINFO = 318
    DET_COVERAGE_CALL_BY_CLASS_BY_PRODUCT = 328
    MUSTSELL = 327
    ONLINEGALLERY = 329
    DETINFO = 330
    SYNCIMAGE = 331
    PRDCATALOG = 332
    MERCHANDISING = 333
    MERCHANDISINGDETAIL = 335
    COMPETITOR = 336
    COMPETITORDETAILS = 337

    PUSHNOTIFICATION = 338
    CUSTOMERCOVERAGE = 498
    DAILYTRACKING = 494
    HISTORICALTRACKING = 495

    VIEW_SALESMAN_ACTIVITY = 347
    VIEW_SALESMAN_ACTIVITY_DETAIL = 348
End Enum

Public Enum SubModuleAction As Long
    View = 1
    Create = 2
    Edit = 3
    Delete = 4
    GPS = 5 'kee wai 10 /10/ 2014  button to confirm gps at customerlist
End Enum

Public Enum ModuleID As Long
    ADM = 1
    FFMA = 2
    FFMS = 3
    FFMR = 4
End Enum

Public Enum FieldColumntype As Integer
    BoundColumn = 0
    ButtonColumn = 1
    EditCommandColumn = 2
    HyperlinkColumn = 3
    TemplateColumn = 4
    TemplateColumn_Percentage = 5
    BoundColumn_V2 = 6
    BoundColumn_Total = 7
    InvisibleColumn = 99
End Enum

Public Enum NetValue
    NV1 = 1
    NV2 = 2
End Enum

Public Enum DisplayMode
    NameOnly = 1
    CodeOnly = 2
    NameAndCode = 3
End Enum

Public Enum ListBoxType
    lsbAll = 0
    lsbHide = 1
    lsbShow = 2
End Enum
Public Enum GridPageSize As Integer
    PageSize = 20
End Enum
Public Class clsSharedValue
    Public Event OnValueChanged As EventHandler
    Private strYear, strMonth, strDay, strStartYear As String
    Private strYear2, strMonth2, strDay2 As String
    Private strGroupValue As String
    Private strGroupTextValue As String
    Private strPrincipalID, strPrincipalCode As String
    Private strRegionID, strRegionCode As String
    Private strTeamCode, strSalesrepCode As String
    Private strSortExpression As String
    Private intDdlReportType As Integer
    Private intDdlMonthMode As Integer
    Private blnIsActivate As Boolean
    Private intNetValue As Integer = 1
    Private enDisplayMode As DisplayMode
    Private intPageIndex As Integer
    Private strTreePath, strNodeGrpName, strSalesrepList As String
    Private strPTLCode, strClass As String
    Private strAgencyCode, strCustCode As String, strPrdGrpCode As String
    Private strType As String
    Private strProductCode As String
    Private strStartDate As String
    Private strEndDate As String
    Private strDate As String

    Sub New()
        blnIsActivate = True
        RaiseValueChangedEvent()
    End Sub

    Public Sub RaiseValueChangedEvent()
        RaiseEvent OnValueChanged(Me, Nothing)
    End Sub

    Public ReadOnly Property IsActivate() As Boolean
        Get
            Return blnIsActivate
        End Get
    End Property

    Public Property Tree_Path() As String
        Get
            Return strTreePath
        End Get
        Set(ByVal value As String)
            strTreePath = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Node_Grp_Name() As String
        Get
            Return strNodeGrpName
        End Get
        Set(ByVal value As String)
            strNodeGrpName = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Selected_Salesrep_List() As String
        Get
            Return strSalesrepList
        End Get
        Set(ByVal value As String)
            strSalesrepList = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Year() As String
        Get
            If String.IsNullOrEmpty(strYear) Then strYear = HttpContext.Current.Session("YEAR")
            Return strYear
        End Get
        Set(ByVal value As String)
            strYear = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Year2() As String
        Get
            'If String.IsNullOrEmpty(strYear2) Then strYear2 = HttpContext.Current.Session("YEAR2")
            Return strYear2
        End Get
        Set(ByVal value As String)
            strYear2 = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property StartYear() As String
        Get
            If String.IsNullOrEmpty(strStartYear) Then strStartYear = HttpContext.Current.Session("STARTYEAR")
            Return strStartYear
        End Get
        Set(ByVal value As String)
            strStartYear = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Month() As String
        Get
            If String.IsNullOrEmpty(strMonth) Then strMonth = HttpContext.Current.Session("MONTH")
            Return strMonth
        End Get
        Set(ByVal value As String)
            strMonth = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Month2() As String
        Get
            'If String.IsNullOrEmpty(strMonth2) Then strMonth2 = HttpContext.Current.Session("MONTH2")
            Return strMonth2
        End Get
        Set(ByVal value As String)
            strMonth2 = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Day() As String
        Get
            If String.IsNullOrEmpty(strDay) Then strDay = 1
            Return strMonth
        End Get
        Set(ByVal value As String)
            strMonth = IIf(IsNumeric(value), value, 1)
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Day2() As String
        Get
            If String.IsNullOrEmpty(strDay2) Then strDay2 = 1
            Return strMonth2
        End Get
        Set(ByVal value As String)
            strMonth2 = IIf(IsNumeric(value), value, 1)
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property PrincipalID() As String
        Get
            'If String.IsNullOrEmpty(strPrincipal) Then strPrincipal = HttpContext.Current.session("PRINCIPAL_CODE")
            Return strPrincipalID
        End Get
        Set(ByVal value As String)
            strPrincipalID = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property PrincipalCode() As String
        Get
            'If String.IsNullOrEmpty(strPrincipal) Then strPrincipal = HttpContext.Current.session("PRINCIPAL_CODE")
            Return strPrincipalCode
        End Get
        Set(ByVal value As String)
            strPrincipalCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property RegionCode() As String
        Get
            Return strRegionCode
        End Get
        Set(ByVal value As String)
            strRegionCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            'If String.IsNullOrEmpty(strTeamCode) Then strTeamCode = HttpContext.Current.session("TEAM_CODE")
            Return strTeamCode
        End Get
        Set(ByVal value As String)
            strTeamCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            'If String.IsNullOrEmpty(strSalesman) Then strSalesman = HttpContext.Current.session("SALESREP_CODE")
            Return strSalesrepCode
        End Get
        Set(ByVal value As String)
            strSalesrepCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property GroupField() As String
        Get
            'If String.IsNullOrEmpty(strGroupValue) Then strGroupValue = HttpContext.Current.Session("GroupingValue")
            Return strGroupValue
        End Get
        Set(ByVal value As String)
            strGroupValue = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property GroupTextValue() As String
        Get
            'If String.IsNullOrEmpty(strGroupValue) Then strGroupValue = HttpContext.Current.Session("GroupingValue")
            Return strGroupTextValue
        End Get
        Set(ByVal value As String)
            strGroupTextValue = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property SortExpression() As String
        Get
            Return strSortExpression
        End Get
        Set(ByVal value As String)
            strSortExpression = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property ReportType() As Integer
        Get
            Return intDdlReportType
        End Get
        Set(ByVal value As Integer)
            intDdlReportType = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property MonthType() As Integer
        Get
            Return intDdlMonthMode
        End Get
        Set(ByVal value As Integer)
            intDdlMonthMode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property NetValue() As Integer
        Get
            Return intNetValue
        End Get
        Set(ByVal value As Integer)
            intNetValue = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property DisplayMode() As DisplayMode
        Get
            Return enDisplayMode
        End Get
        Set(ByVal value As DisplayMode)
            enDisplayMode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property PageIndex() As Integer
        Get
            Return intPageIndex
        End Get
        Set(ByVal value As Integer)
            intPageIndex = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property PTLCode() As String
        Get
            Return strPTLCode
        End Get
        Set(ByVal value As String)
            strPTLCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Classification() As String
        Get
            Return strClass
        End Get
        Set(ByVal value As String)
            strClass = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property AgencyCode() As String
        Get
            Return strAgencyCode
        End Get
        Set(ByVal value As String)
            strAgencyCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return strCustCode
        End Get
        Set(ByVal value As String)
            strCustCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property PrdGrpCode() As String
        Get
            Return strPrdGrpCode
        End Get
        Set(ByVal value As String)
            strPrdGrpCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property Type() As String
        Get
            Return strType
        End Get
        Set(ByVal value As String)
            strType = value
            RaiseValueChangedEvent()
        End Set
    End Property


    Public Property ProductCode() As String
        Get
            'If String.IsNullOrEmpty(strTeamCode) Then strTeamCode = HttpContext.Current.session("TEAM_CODE")
            Return strProductCode
        End Get
        Set(ByVal value As String)
            strProductCode = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property StartDate() As String
        Get
            'If String.IsNullOrEmpty(strTeamCode) Then strTeamCode = HttpContext.Current.session("TEAM_CODE")
            Return strStartDate
        End Get
        Set(ByVal value As String)
            strStartDate = value
            RaiseValueChangedEvent()
        End Set
    End Property


    Public Property EndDate() As String
        Get
            'If String.IsNullOrEmpty(strTeamCode) Then strTeamCode = HttpContext.Current.session("TEAM_CODE")
            Return strEndDate
        End Get
        Set(ByVal value As String)
            strEndDate = value
            RaiseValueChangedEvent()
        End Set
    End Property

    Public Property TxnDate() As String
        Get
            'If String.IsNullOrEmpty(strTeamCode) Then strTeamCode = HttpContext.Current.session("TEAM_CODE")
            Return strDate
        End Get
        Set(ByVal value As String)
            strDate = value
            RaiseValueChangedEvent()
        End Set
    End Property
End Class

Public Class ColumnStyle
    Private _HorizontalAlign As HorizontalAlign = WebControls.HorizontalAlign.NotSet
    Private _FormatString As String = ""
    Private _VerticalAlign As VerticalAlign = VerticalAlign.NotSet
    Private _Wrap As Boolean = True
    Private _ColumnType As FieldColumntype
    Private _ColumnName As String = ""

    Public Property ColumnName() As String
        Get
            Return _ColumnName
        End Get
        Set(ByVal value As String)
            _ColumnName = value
        End Set
    End Property

    Public Property ColumnType() As FieldColumntype
        Get
            Return _ColumnType
        End Get
        Set(ByVal value As FieldColumntype)
            _ColumnType = value
        End Set
    End Property

    Public Property HorizontalAlign() As HorizontalAlign
        Get
            Return _HorizontalAlign
        End Get
        Set(ByVal value As HorizontalAlign)
            _HorizontalAlign = value
        End Set
    End Property

    Public Property VerticalAlign() As VerticalAlign
        Get
            Return _VerticalAlign
        End Get
        Set(ByVal value As VerticalAlign)
            _VerticalAlign = value
        End Set
    End Property

    Public Property Wrap() As Boolean
        Get
            Return _Wrap
        End Get
        Set(ByVal value As Boolean)
            _Wrap = value
        End Set
    End Property

    Public Property FormatString() As String
        Get
            Return _FormatString
        End Get
        Set(ByVal value As String)
            _FormatString = value
        End Set
    End Property

End Class

Public Class GoogleMap
    Public Shared Function Key() As String
        Return ConfigurationManager.AppSettings("GmapKey").ToString
    End Function
End Class

Public Class Report

    Public Shared Function GetName(ByVal subModuleID As SubModuleType) As String
        Dim strName As String = String.Empty
        Try
            Select subModuleID
                Case SubModuleType.DRC
                    strName = "Inventory Forecasting Analysis"
                Case SubModuleType.DRCENQ
                    strName = "Inventory Forecasting Enquiry"
                Case SubModuleType.SALES
                    strName = "Sales Analysis"
                Case SubModuleType.SALESINFOBYDATE
                    strName = "Sales Information by Date"
                Case SubModuleType.SALESSUMM
                    strName = "Sales Information Summary"
                Case SubModuleType.KEYPRD
                    strName = "Key Product Sales"
                Case SubModuleType.TOP80CUSTBYREP
                    'strName = "Top 80% Customer Sales by Reps for 3 Months"
                    strName = "Top 20% Customers Contributing 80% of Sales for 3 Months"
                Case SubModuleType.SALESENQ
                    strName = "Sales Enquiry"
                Case SubModuleType.CABYMTH
                    strName = "Monthly Call Analysis"
                Case SubModuleType.CABYDAY, SubModuleType.DAILYCALLANALYSIS
                    strName = "Daily Call Analysis"
                Case SubModuleType.CABYCUST
                    strName = "Customer Call Analysis"
                Case SubModuleType.CPRODACTYDTL
                    strName = "Field Activity Information"
                Case SubModuleType.SALESORD
                    strName = "Order Management Information"
                Case SubModuleType.TRAINFO
                    strName = "Goods Return Information"
                Case SubModuleType.COLLINFO
                    strName = "Payment Collection Information"
                Case SubModuleType.CABYSTRIKE
                    strName = "Strike Call Analysis"
                Case SubModuleType.CABYCONT
                    strName = "Actual Call Coverage"
                Case SubModuleType.CALLCOV1
                    strName = "Effective Call Coverage"
                Case SubModuleType.CUSTCONTINFO
                    strName = "Customer Contact Information"
                Case SubModuleType.CALLCOV2
                    strName = "Hit Call Coverage"
                Case SubModuleType.CPROD
                    strName = "Monthly Field Force Activity"
                Case SubModuleType.CPRODDAILYACTY, SubModuleType.DAILYCALLACTY
                    strName = "Daily Field Force Activity"
                Case SubModuleType.SFMSPRFM
                    strName = "Field Force Activity by Class"
                Case SubModuleType.SFMSACTY
                    strName = "Field Force Activity List"
                Case SubModuleType.PREPLAN
                    strName = "Preplan Call Information"
                Case SubModuleType.PREPLANCUST
                    strName = "Preplan Call Information by Customer"
                Case SubModuleType.CALLENQ
                    strName = "Call Enquiry"
                Case SubModuleType.MTHOVER
                    strName = "Monthly Key Performance Index"
                Case SubModuleType.CUSTSTATUS
                    strName = "Customer Key Performance Index"
                Case SubModuleType.ACTYSALESBYINDSR
                    strName = "Salesrep Key Performance Index"
                Case SubModuleType.RELPERFORM
                    strName = "Relative Key Performance Index"
                Case SubModuleType.PDACOMM
                    strName = "Refresh & Upload Information"
                Case SubModuleType.COMMUPDINFO
                    strName = "Transaction Upload Information"
                Case SubModuleType.CHEQENQ
                    strName = "Payment Collection Cheque Enquiry"
                Case SubModuleType.PAFENQ
                    strName = "Promotion Acceptance Enquiry"
                Case SubModuleType.STKRETBYMTH
                    strName = "Goods Return By Month End Closing"
                Case SubModuleType.STKALLOC
                    strName = "Salesrep Stock Allocation"
                Case SubModuleType.ActCallKPI
                    strName = "Actual Call KPI"
                Case SubModuleType.ActCallKPIbySR
                    strName = "Actual Call KPI by SR"
                Case SubModuleType.CALLRATE
                    strName = "Call Rate"
                Case SubModuleType.SALESTGTVSACT
                    strName = "Sales Target Vs Actual"
                Case SubModuleType.SALESESTVSACT
                    strName = "Sales Estimation Vs Actual"
                Case SubModuleType.DATA
                    strName = "Data File List"
                Case SubModuleType.DRCINFO
                    strName = "Inventory Forecasting Information"
                Case SubModuleType.MSSINFO
                    strName = "Market Survey ?Data Collection Information"
                Case SubModuleType.AUDITLOG
                    strName = "Audit Log"
                Case SubModuleType.AUDITLOGDTL
                    strName = "Audit Log Detail"
                Case SubModuleType.SALESBYCUSTCOUNT
                    strName = "Sales By Customer Count"
                Case SubModuleType.DNINFO
                    strName = "Debit Note Information"
                Case SubModuleType.SFMSEXTRACTION
                    strName = "Field Activities Extraction"
                Case SubModuleType.CALLINFOBYMONTH
                    strName = "Call Information By Month"
                Case SubModuleType.SALESFORCECAPACITY
                    strName = "Sales Force Capacity"

                Case SubModuleType.COMPANY
                    strName = "Company"
                Case SubModuleType.SALESAREA
                    strName = "Sales Area"
                Case SubModuleType.WAREHOUSE
                    strName = "Warehouse"
                Case SubModuleType.REGION
                    strName = "Region"
                Case SubModuleType.SALESTEAM
                    strName = "Salesteam"
                Case SubModuleType.CUSTOMER
                    strName = "Customer"
                Case SubModuleType.SHIPTO
                    strName = "Shipto"
                Case SubModuleType.CONTACT
                    strName = "Contact"
                Case SubModuleType.PRODUCT
                    strName = "Product"
                Case SubModuleType.PRODUCTGROUP
                    strName = "Product Group"
                Case SubModuleType.SUPPLIER
                    strName = "Supplier"
                Case SubModuleType.STOCKALLOC
                    strName = "Stock Allocation"
                Case SubModuleType.PRODUCT_MATRIX
                    strName = "Product Matrix"
                Case SubModuleType.STANDARDPRICE
                    strName = "Standard Price"
                Case SubModuleType.SPECIALPRICE
                    strName = "Customer Special Price"
                Case SubModuleType.CUSTGRPPRICE
                    strName = "Customer Group Price"
                Case SubModuleType.PRICEGRPPRICE
                    strName = "Price Group Price"
                Case SubModuleType.STANDARDBONUS
                    strName = "Standard Bonus"
                Case SubModuleType.SPECIALBONUS
                    strName = "Customer Special Bonus"
                Case SubModuleType.CUSTGRPBONUS
                    strName = "Customer Group Bonus"
                Case SubModuleType.PRICEGRPBONUS
                    strName = "Price Group Bonus"
                Case SubModuleType.PACKAGE
                    strName = "Assortment (Package)"
                Case SubModuleType.FIELDACTY
                    strName = "Field Activity"
                Case SubModuleType.MSSADV
                    strName = "Market Survey"
                Case SubModuleType.FIELDFORCE
                    strName = "Field Force"
                Case SubModuleType.SALESTGT
                    strName = "Sales Target"
                Case SubModuleType.REASON
                    strName = "Reason"
                Case SubModuleType.ROUTE
                    strName = "Route"
                Case SubModuleType.LEAD
                    strName = "Lead"
                Case SubModuleType.PRICEGROUP
                    strName = "Price Group"
                Case SubModuleType.INBOX
                    strName = "Inbox"
                Case SubModuleType.OUTBOX
                    strName = "Outbox"
                Case SubModuleType.FIELDFORCECUST
                    strName = "Field Force Customer"
                Case SubModuleType.FIELDFORCEROUTE
                    strName = "Field Force Route"
                Case SubModuleType.FIELDFORCEPRDGRP
                    strName = "Field Force Product Group"
                Case SubModuleType.FIELDFORCEACTY
                    strName = "Field Force Activity"
                Case SubModuleType.FIELDFORCEMSS
                    strName = "Field Force Market Survey"
                Case SubModuleType.FIELDFORCEPACKAGE
                    strName = "Field Force Package"
                Case SubModuleType.FIELDFORCEITINERARYTRANSFER
                    strName = "Field Force Itinerary Transfer"
                Case SubModuleType.CUSTPROFILEMAINTAIN
                    strName = "Customer Profile Maintenances"

                    'FFMA - AR - Shen Yee 22/08/2016
                Case SubModuleType.COMPANYCODE_BUPROFILE
                    strName = "Company Code - BU Profile"
                Case SubModuleType.COMPANYCODE_BANKPROFILE
                    strName = "Company Code - Bank Profile"
                Case SubModuleType.COLLECTORPROFILE
                    strName = "Collector Profile"
                Case SubModuleType.COLLECTORCUSTOMERMAPPING
                    strName = "Collector - Customer Mapping"
                Case SubModuleType.PRODUCTMUSTSELL
                    strName = "Product Must Sell"
                Case SubModuleType.PRODUCTMUSTSELLADV
                    strName = "Product Must Sell Adv."

                Case SubModuleType.CALLBYDKSHCLASS
                    strName = "Call Analysis by DKSH Class"
                Case SubModuleType.CALLBYSUPPLIERCLASS
                    strName = "Call Analysis by Supplier Class"
                Case SubModuleType.CALLACTYHISTBYCONT
                    strName = "Last 6 Month Call Activity Information by Contact"
                Case SubModuleType.CALLACTYENQ
                    strName = "Call Activity Enquiry"
                Case SubModuleType.FIELDFORCEMTHACTY
                    strName = "Field Force Monthly Activity"
                Case SubModuleType.FIELDFORCEPRDFREQ
                    strName = "Field Force Productivity Freq"
                Case SubModuleType.FIELDFORCESFMSDETACTY
                    strName = "Field Force Detailing Activity"
                Case SubModuleType.STOCKADJUST
                    strName = "Stock Adjustment List"
                Case SubModuleType.STOCKMOVE
                    strName = "Stock Movement List"
                Case SubModuleType.CALLANALYENQ
                    strName = "Call Analysis Enquiry"
                Case SubModuleType.CALLANALYBYCONTCLASS
                    strName = "Call Analysis By Contact Class"
                Case SubModuleType.CPRODACTYSUP
                    strName = "Monthly Field Force Activity by Supplier"
                Case SubModuleType.STOCKTRANSFERLIST
                    strName = "Confirm Stock List"
                Case SubModuleType.NEWEQPLIST
                    strName = "New Equipment List"
                Case SubModuleType.APPLYSTOCKLIST
                    strName = "Apply Stock List"
                Case SubModuleType.STOCKHIST
                    strName = "Stock History List"
                Case SubModuleType.STOCKSTATUS
                    strName = "Stock Status List"
                Case SubModuleType.CPRODDAILYACTYCUST
                    strName = "Call Prod Activity Detail Customize"
                Case SubModuleType.DRCEXTRACTION
                    strName = "Inventory Forecasting Extraction"
                Case SubModuleType.TECHNICIANSALESGRP
                    strName = "Technician Sales Group"
                Case SubModuleType.TECHNICIANITINERARY
                    strName = "Technician Itinerary"
                Case SubModuleType.TRAORDER
                    strName = "TRA "
                Case SubModuleType.TECHNICIANITINERARYDTL
                    strName = "Technician Itinerary Detail"
                Case SubModuleType.TECHNICIANFUNCLOC
                    strName = "Technician Functional Location"
                Case SubModuleType.TECHNICIANSALESOFF
                    strName = "Technician Sales Office"
                Case SubModuleType.ORDPRDMATRIX
                    strName = "Sales Order With Product Matrix"
                Case SubModuleType.SRCALLRATE
                    strName = "Field Force Call Rate"
                Case SubModuleType.SRCALLRATEDTL
                    strName = "Field Force Call Rate Detail"
                Case SubModuleType.MANUFACTURER
                    strName = "Manufacturer List"
                Case SubModuleType.SVCSTAT
                    strName = "Service Statistic"
                Case SubModuleType.SVCDTL
                    strName = "Service Detail"
                Case SubModuleType.EQPHISTLIST
                    strName = "Equipment History List"
                Case SubModuleType.COLLENQ
                    strName = "Collection Enquiry"
                Case SubModuleType.UPLOADITINERARY
                    strName = "Upload Itinerary"
                Case SubModuleType.MTHPRDFREQ
                    strName = "Monthly Productive Frequency By Field Force"
                Case SubModuleType.MTHTERRITORY
                    strName = "Monthly Territory"
                Case SubModuleType.MTHSPECIALITY
                    strName = "Monthly Specialty"
                Case SubModuleType.SRSCORECARD
                    strName = "Field Force Score Card"
                Case SubModuleType.MSS_CONT
                    strName = "MSS Contact Update"
                Case SubModuleType.MSS_CONT_DTL
                    strName = "MSS Contact Details"
                Case SubModuleType.SFMSEXTRACTION_2
                    strName = "Field Activities Extraction 2"
                Case SubModuleType.LEAVE
                    strName = "Leave"
                Case SubModuleType.EASYLOADER
                    strName = "EasyLoader"
                Case SubModuleType.CALLDTLBYGROUP
                    strName = "Speciality Call Analysis" '"Call Detail By Grouping"
                Case SubModuleType.SAPV3
                    strName = "SAP List V3"
                Case SubModuleType.SAPLISTV4
                    strName = "SAP List V4"
                Case SubModuleType.DATA_V2
                    strName = "Data File List"
                Case SubModuleType.SALES_CUST_PER_PRD
                    strName = "Sales of Customer Per Product"
                Case SubModuleType.SALES_CUZ
                    strName = "Sales by Customer Group"
                Case SubModuleType.CUSTINFO
                    strName = "Other Customer Info"
                Case SubModuleType.CUSTCLASS
                    strName = "Class Info"
                Case SubModuleType.CUSTSALESTGT
                    strName = "Sales Target by Customer"
                Case SubModuleType.CUSTSALESANALYSIS
                    strName = "Customer Sales Analysis"
                Case SubModuleType.CUSTPRDSALESANALYSIS
                    strName = "Customer Product Sales Analysis"
                Case SubModuleType.SOVARIANCE
                    strName = "Sales Order Variance Report"
                Case SubModuleType.PLAN_ACTUAL
                    strName = "Preplan Vs Actual By Customer Report"
                Case SubModuleType.CALLRATE_BYBRAND
                    strName = "Call Rate Report "
                Case SubModuleType.TRAList
                    strName = "Goods Return List"
                Case SubModuleType.TRAReport
                    strName = "Goods Return Report"
                Case SubModuleType.MERCHSFMS
                    strName = "Merchandiser Stock and Display Check Report"
                Case SubModuleType.SALESREP_KPI
                    strName = "Salesrep KPI Report"
                Case SubModuleType.SAPLIST_V4
                    strName = "SAP List v4"
                Case SubModuleType.DAYCALLDISTRIBUTION
                    strName = "Daily Call Distribution Report"
                Case SubModuleType.DRCCuzList
                    strName = "Inventory Forecasting Analysis Cuz. Report"
                Case SubModuleType.PREPLANEXTRACT
                    strName = "Preplan Activities Extraction"
                Case SubModuleType.PREPLANCUZ
                    strName = "Preplan Activities"
                Case SubModuleType.CALLACHIEVEMENT
                    strName = "Call Achievement Report"
                Case SubModuleType.CALLBYDAYADV
                    strName = "Daily Call Analysis Adv"
                Case SubModuleType.CALLBYMONTHADV
                    strName = "Monthly Call Analysis Adv"
                Case SubModuleType.CUSTPROFILEENQ
                    strName = "Customer Profile Enquiry"
                Case SubModuleType.CUZMSSENQUIRY
                    strName = "Customer Market Survey Enquiry"
                Case SubModuleType.CUZSFMSENQUIRY
                    strName = "Customer Field Activity Enquiry"
                Case SubModuleType.MSSEXTRACTION
                    strName = "MSS Sampling Report"
                Case SubModuleType.MSS_TEAM_MAPPING
                    strName = "MSS Team Mapping"
                Case SubModuleType.MISSCALLANALYSIS
                    strName = "Missing Call Analysis"
                Case SubModuleType.MISSCALLANALYSISDTL
                    strName = "Missing Call Analysis Detail"
                Case SubModuleType.SALES_BY_CHAIN
                    strName = "Sales Report by Chain"
                Case SubModuleType.SALES_BY_CUST
                    strName = "Sales Report by Customer"
                Case SubModuleType.REMAIN_CALL
                    strName = "Call Remaining Report"
                Case SubModuleType.STK_CONSUMP_BY_BIMONTH
                    strName = "Stock Consumption By Bi Month"
                Case SubModuleType.STK_CONSUMP_BY_QUARTER
                    strName = "Stock Consumption By Quarter"
                Case SubModuleType.STK_CONSUMP_BY_BIMONTH_DTL
                    strName = "Stock Consumption By Bi Month Detail"
                Case SubModuleType.STK_CONSUMP_BY_QUARTER_DTL
                    strName = "Stock Consumption By Quarter Detail"
                Case SubModuleType.MSSADV2
                    strName = "Market Survey 2"
                Case SubModuleType.FIELDFORCEMTHACTYPrd
                    strName = "Field Force Monthly Activity Product"
                Case SubModuleType.STK_CONSUMP_BY_QUADMONTH
                    strName = "Stock Consumption By 4 Month "
                Case SubModuleType.STK_CONSUMP_BY_QUADMONTH_DTL
                    strName = "Stock Consumption By 4 Month Detail"
                Case SubModuleType.SFMSEXTRACTION_EXTRACAT
                    strName = "Field Activities Extraction Extra Cat"
                Case SubModuleType.TRAREPORT_EDI
                    strName = "TRA Report"
                Case SubModuleType.SFMS_EXTRACT
                    strName = "Field Activities Extraction"
                Case SubModuleType.VISIT_EXTRACT
                    strName = "Visit Extraction"
                Case SubModuleType.CUST_CONT_EXTRACT
                    strName = "Customer Contact Extraction"
                Case SubModuleType.CUST_CONT_PRD_EXTRACT
                    strName = "Customer Contact Product Extraction"
                Case SubModuleType.DASHBOARDXFIELD
                    strName = "Dashboard X Field"
                Case SubModuleType.SFE_KPI
                    strName = "SFE KPI"
                Case SubModuleType.CUSTTGTPRD
                    strName = "Customer Target Product"
                Case SubModuleType.PRDSEQ
                    strName = "Product Sequence"
                Case SubModuleType.CUST_TEAM_CLASS
                    strName = "Customer Team Class"
                Case SubModuleType.PROMO_SO
                    strName = "Promotion"
                Case SubModuleType.DETAIL_BRAND
                    strName = "Detailing"
                Case SubModuleType.CONTKPI
                    strName = "Contact Key Performance Index"
                Case SubModuleType.SFEKPIREPORT
                    strName = "SFE KPI Report"
                Case SubModuleType.DET_COVERAGE_BY_SPECIALTY
                    strName = "Coverage by Specialty by Product"
                Case SubModuleType.DET_CALL_BY_AREA
                    strName = "Call by Area by Product"
                Case SubModuleType.CALLCOV1ADV
                    strName = "Effective Call Coverage (Adv)"
                Case SubModuleType.CUSTCONTCALLINFO
                    strName = "Customer Contact Call Information"
                Case SubModuleType.CUSTCONTCOVERAGEINFO
                    strName = "Customer Contact Coverage Call Information"
                Case SubModuleType.CUSTCONTNONCOVERAGEINFO
                    strName = "Customer Contact Non Coverage Call Information"
                Case SubModuleType.CUSTCONTEFFECTIVECALLINFO
                    strName = "Customer Contact Effective Call Information"
                Case SubModuleType.CUSTCONTNONEFFECTIVEINFO
                    strName = "Customer Contact Non Effective Call Information"
                Case SubModuleType.MUSTSELL
                    strName = "Product Must Sell"
                Case SubModuleType.DET_COVERAGE_CALL_BY_CLASS_BY_PRODUCT
                    strName = "Coverage and Call By Class By Product"
                Case SubModuleType.ONLINEGALLERY
                    strName = "Online Gallery"
                Case SubModuleType.SYNCIMAGE
                    strName = "Sync Image"
                Case SubModuleType.DETINFO
                    strName = "Detailing Information"
                Case SubModuleType.PRDCATALOG
                    strName = "Product Catalog"
                Case SubModuleType.MERCHANDISING
                    strName = "Merchandising"
                Case SubModuleType.MERCHANDISINGDETAIL
                    strName = "Merchandising Details"
                Case SubModuleType.COMPETITOR
                    strName = "Competitor Activity and Pricing by Product"
                Case SubModuleType.COMPETITORDETAILS
                    strName = "Competitor Details"
                Case SubModuleType.BCP
                    strName = "BCP"
                Case SubModuleType.SFMS_CFG
                    strName = "SFMS Configuration"
                Case SubModuleType.MSS_CFG
                    strName = "MSS Configuration"
                Case SubModuleType.VIEW_SALESMAN_ACTIVITY
                    strName = "View Salesman Activity"
                Case SubModuleType.VIEW_SALESMAN_ACTIVITY_DETAIL
                    strName = "View Salesman Activity Detail"
                Case Else
                    strName = "REPORT"
            End Select
        Catch ex As Exception
            'Catch Error
        End Try
        Return strName
    End Function

    Public Shared Function GetAccessRight(ByVal dblModuleID As ModuleID, ByVal dblSubModuleID As SubModuleType, ByVal dblActionID As SubModuleAction) As Boolean
        Try
            Return GetAccessRight(dblModuleID, dblSubModuleID, Str(dblActionID))
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetAccessRight(ByVal dblModuleID As ModuleID, ByVal dblSubModuleID As SubModuleType, ByVal strActionID As String) As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False
        Dim dtAR As DataTable

        Try
            dtAR = HttpContext.Current.Session("UserAccessRight")
            If Not IsNothing(dtAR) Then
                If dblSubModuleID = 0 Then
                    drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
                Else
                    drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
                End If

                If drCurrRow.Length > 0 Then
                    blnValue = True
                End If

                Return blnValue
            End If
        Catch ex As Exception

        End Try
    End Function

#Region "Tree Control"
    Public Shared Sub UpdateTreePath(ByVal strNodeName As String, ByVal strNodeValue As String)
        Try
            Dim NODE As TreeNode
            NODE = FindTreeNode(strNodeName, strNodeValue)
            If NODE IsNot Nothing Then HttpContext.Current.Session("TREE_PATH") = NODE.ValuePath

        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function GetSalesrepList(ByVal strNodeName As String, ByVal strNodeValue As String) As String
        Dim strSalesrepList As String = String.Empty
        Try
            Dim NODE As TreeNode
            NODE = FindTreeNode(strNodeName, strNodeValue)
            If NODE IsNot Nothing Then strSalesrepList = Tree.GetSubTreeItemList(NODE)
        Catch ex As Exception

        End Try
        Return strSalesrepList
    End Function

    Public Shared Function FindTreeNode(ByVal strNodeName As String, ByVal strNodeValue As String) As TreeNode
        Dim resultNode As TreeNode = Nothing
        Dim tvTreeMenu As TreeView = CType(HttpContext.Current.Session("TREE_VIEW"), TreeView)
        Dim strTreePath As String = CStr(HttpContext.Current.Session("TREE_PATH"))

        Dim tnNode As TreeNode
        If tvTreeMenu IsNot Nothing Then
            tnNode = tvTreeMenu.FindNode(strTreePath)
            If tnNode IsNot Nothing Then
                If tnNode.ChildNodes.Count = 0 Then
                    resultNode = FindTreeNode(tvTreeMenu, tnNode, strNodeName, strNodeValue)
                Else
                    For Each childNode As TreeNode In tnNode.ChildNodes
                        resultNode = FindTreeNode(tvTreeMenu, childNode, strNodeName, strNodeValue)
                        If resultNode IsNot Nothing Then Exit For
                    Next
                End If
            Else
                'GengChao
            End If
        End If
        Return resultNode
    End Function

    Private Shared Function FindTreeNode(ByRef tvTreeMenu As TreeView, ByRef NODE As TreeNode, ByVal strNodeName As String, ByVal strNodeValue As String, Optional ByVal blnSearchSubNode As Boolean = True) As TreeNode
        Dim resultNode As TreeNode = Nothing
        Dim strValues() As String
        Try
            strValues = Tree.SplitComboValues(NODE.Value)
            If strValues(0) = strNodeName AndAlso strValues(1) = strNodeValue Then
                resultNode = NODE
            ElseIf blnSearchSubNode = True Then
                For Each childNode As TreeNode In NODE.ChildNodes
                    resultNode = FindTreeNode(tvTreeMenu, childNode, strNodeName, strNodeValue)
                    If resultNode IsNot Nothing Then Exit For
                Next
            End If
        Catch ex As Exception

        End Try
        Return resultNode

    End Function


#End Region

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "SUPPLIER_CODE"
                strFieldName = "Supplier Code"
            Case "SUPPLIER_NAME"
                strFieldName = "Supplier"
            Case "TREE_TEAM_CODE"
                strFieldName = "Team Code(T)"
            Case "TREE_TEAM_NAME"
                strFieldName = "Team(T)"
            Case "TREE_REGION_CODE"
                strFieldName = "Region Code(T)"
            Case "TREE_REGION_NAME"
                strFieldName = "Region(Tree)"
            Case "TREE_SALESREP_CODE"
                strFieldName = "Field Force Code(T)"
            Case "TREE_SALESREP_NAME"
                strFieldName = "Field Force(T)"
            Case "TREE_SALES_AREA_CODE"
                strFieldName = "Sales Area Code(T)"
            Case "TREE_SALES_AREA_NAME"
                strFieldName = "Sales Area(T)"
            Case "TEAM_CODE"
                strFieldName = "Team Code"
            Case "TEAM_NAME"
                strFieldName = "Team"
            Case "REGION_CODE"
                strFieldName = "Region Code"
            Case "REGION_NAME"
                strFieldName = "Region"
            Case "SALESREP_CODE"
                strFieldName = "Field Force Code"
            Case "SALESREP_NAME"
                strFieldName = "Field Force"
            Case "SALES_AREA_CODE"
                strFieldName = "Sales Area Code"
            Case "SALES_AREA_NAME"
                strFieldName = "Sales Area"
            Case "CHANNEL_CODE"
                strFieldName = "Channel Code"
            Case "CHANNEL_NAME"
                strFieldName = "Channel"
            Case "CHAIN_CODE"
                strFieldName = "Chain Code"
            Case "CHAIN_NAME"
                strFieldName = "Chain"
            Case "CUST_GRP_CODE"
                strFieldName = "Customer Group Code"
            Case "CUST_GRP_NAME"
                strFieldName = "Customer Group"
            Case "CUST_CODE"
                strFieldName = "Customer Code"
            Case "CUST_NAME"
                strFieldName = "Customer"
            Case "SHIPTO_CODE"
                strFieldName = "Shipto Code"
            Case "SHIPTO_NAME"
                strFieldName = "Shipto"
            Case "AGENCY_CODE"
                strFieldName = "Agency Code"
            Case "AGENCY_NAME"
                strFieldName = "Agency"
            Case "PRD_GRP_CODE"
                strFieldName = "Product Group Code"
            Case "PRD_GRP_NAME"
                strFieldName = "Product Group"
            Case "PRD_CODE"
                strFieldName = "Product Code"
            Case "PRD_NAME"
                strFieldName = "Product"
            Case "CONT_CODE"
                strFieldName = "Contact Code"
            Case "CONT_NAME"
                strFieldName = "Contact"
            Case "DATE"
                strFieldName = "Date"
            Case "YEAR"
                strFieldName = "Year"
            Case "MONTH"
                strFieldName = "Month"
            Case "CLASS"
                strFieldName = "Class"
            Case "CAT_CODE"
                strFieldName = "Category Code"
            Case "CAT_NAME"
                strFieldName = "Category"
            Case "SUB_CAT_CODE"
                strFieldName = "Sub Category Code"
            Case "SUB_CAT_NAME"
                strFieldName = "Sub Category"
            Case "ROUTE_CODE"
                strFieldName = "Route Code"
            Case "ROUTE_NAME"
                strFieldName = "Route"
            Case "SFMS_IND"
                strFieldName = "A"
            Case "SALES_IND"
                strFieldName = "$"
            Case "COLL_IND"
                strFieldName = "C"
            Case "TRA_IND"
                strFieldName = "T"
            Case "DRC_IND"
                strFieldName = "D"
            Case "MSS_IND"
                strFieldName = "M"
            Case "DN_IND"
                strFieldName = "N"
            Case "HIST_IND"
                strFieldName = "H"
            Case "ADDRESS"
                strFieldName = "Address"
            Case "REMARKS"
                strFieldName = "Remarks"
            Case "REASON_NAME"
                strFieldName = "Reason"
            Case "STATUS"
                strFieldName = "Status"
            Case "TREE_NSM_CODE"
                strFieldName = "NSM Code(T)"
            Case "TREE_DSM_CODE"
                strFieldName = "DSM Code(T)"
            Case "TREE_SUP_CODE"
                strFieldName = "SUP Code(T)"
            Case "TREE_NSM_NAME"
                strFieldName = "NSM Name(T)"
            Case "TREE_DSM_NAME"
                strFieldName = "DSM Name(T)"
            Case "TREE_SUP_NAME"
                strFieldName = "SUP Name(T)"
            Case "SO_NO"
                strFieldName = "SO. No."
            Case "INV_NO"
                strFieldName = "Inv. No."
            Case "DASHBOARD_XFIELD_ID"
                strFieldName = "Field No"
            Case "DASHBOARD_XFIELD_DESC"
                strFieldName = "Field Description"
            Case "SEQ"
                strFieldName = "Sequence"
            Case "CUSTOMER"
                strFieldName = "Customer"
            Case "CONTACT"
                strFieldName = "Contact"
            Case "PRODUCT"
                strFieldName = "Product"
            Case "1ST TARGET PRODUCT"
                strFieldName = "1st Target Product"
            Case "2ND TARGET PRODUCT"
                strFieldName = "2nd Target Product"
            Case "3RD TARGET PRODUCT"
                strFieldName = "3rd Target Product"
            Case "TYPE"
                strFieldName = "Type"
            Case "TEAM_CLASS"
                strFieldName = "Team Class"
            Case "DET_CODE"
                strFieldName = "Detailing Code"
            Case "DET_NAME"
                strFieldName = "Detailing Name"
            Case "SUB_DET_CODE"
                strFieldName = "Sub Detailing Code"
            Case "SUB_DET_NAME"
                strFieldName = "Sub Detailing Name"
            Case "EXTRA_DET_CODE"
                strFieldName = "Extra Detailing Code"
            Case "EXTRA_DET_NAME"
                strFieldName = "Extra Detailing Name"
            Case "SFE_KPI_ID"
                strFieldName = "SFE KPI ID"
            Case "SFE_KPI_DESC"
                strFieldName = "SFE KPI Description"
            Case "START_DATE"
                strFieldName = "Start Date"
            Case "END_DATE"
                strFieldName = "End Date"
            Case "CGCODE"
                strFieldName = "Customer Group Code"
            Case "CONT_CLASS"
                strFieldName = "Class"
            Case "CALL_DATE"
                strFieldName = "Call Date"
            Case "DET_IND"
                strFieldName = "DET"
            Case "RATE_CODE"
                strFieldName = "Rate Code"
            Case "RATE_VALUE"
                strFieldName = "Rate Value"
            Case "FILE_PATH"
                strFieldName = "File Path"
            Case "SEQ_ID"
                strFieldName = "Sequence"
            Case "TEAM"
                strFieldName = "Team"
            Case "CATEGORY"
                strFieldName = "Category"
            Case "OLD_PRD_CODE"
                strFieldName = "Old Material Code"
            Case Else
                strFieldName = ColumnName.ToUpper '"UNKNOWN"
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetRowData(ByVal item As Object) As String
        Return Trim(IIf(IsDBNull(item), "", item))
    End Function
End Class

Public Class Tree

    Public Shared Function GetSubTreeItemList(ByRef tnNode As TreeNode) As String
        Dim strList As New StringBuilder
        Try
            If tnNode.ChildNodes.Count > 0 Then
                For Each NODE As TreeNode In tnNode.ChildNodes
                    strList.Append(GetSubTreeItemList(NODE) & ",")
                Next
                strList.Remove(strList.Length - 1, 1)
            Else
                strList.Append("'" & SplitComboValues(tnNode.Value)(1) & "'")
            End If

        Catch ex As Exception
        End Try
        Return strList.ToString
    End Function

    Public Shared Function SplitComboValues(ByVal strIDandCode As String) As String()
        Dim strsCombo(5) As String

        Try
            Dim strValues() As String = strIDandCode.Split("@")

            For idx As Integer = 0 To strValues.Length - 1
                strsCombo(idx) = strValues(idx)
            Next
        Catch ex As Exception
        Finally
        End Try

        Return strsCombo
    End Function

End Class

Public Class Portal
    Public Shared Property UserSession() As clsUser
        Get
            Current.Session("clsUserSession") = IIf(Current.Session("clsUserSession") Is Nothing, New clsUser, Current.Session("clsUserSession"))
            Return Current.Session("clsUserSession")
        End Get
        Set(ByVal value As clsUser)
            Current.Session("clsUserSession") = value
            If value Is Nothing Then
                Current.Session.Remove("clsUserSession")
            End If
        End Set
    End Property
    Public Class Util
        Public Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object)

            If IsDBNull(dbOri) = False Then
                dbObj = Trim(dbOri)
            ElseIf GetType(T) Is GetType(String) Then
                dbObj = String.Empty
            ElseIf GetType(T) Is GetType(Integer) Then
                dbObj = 0
            Else
                dbObj = Nothing
            End If
        End Sub
        Public Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
            Dim rtValue As T
            If IsDBNull(dbOri) = False Then
                rtValue = dbOri
            Else
                rtValue = dfValue
            End If
            Return rtValue
        End Function
        Public Shared ReadOnly Property GetNewSessionKey() As String
            Get
                Return Guid.NewGuid.ToString.ToUpper.Substring(24, 6) & DateTime.Now.ToString("HHmmss")
            End Get
        End Property
    End Class

    Public Shared Property YearCycle() As DataTable
        Get
            Return Current.Session("dtYearCycle")
        End Get
        Set(ByVal value As DataTable)
            Current.Session("dtYearCycle") = value
        End Set
    End Property

    Public Class Policy
        Public Shared Function WriteClientPolicy() As String
            Dim strRet As New StringBuilder

            strRet.AppendLine("function PolicyCheck(sender, e) { ")
            strRet.Append("var pwd = e.Value;")
            strRet.Append("var passed = validatePassword(pwd,")
            strRet.Append("{")

            '// Min char
            Dim intMinLength As Integer = ConfigurationManager.AppSettings("MinLength")
            strRet.Append("length: [" + intMinLength.ToString + ", Infinity]")

            '// Lowercase
            strRet.Append(IIf(ConfigurationManager.AppSettings("LowerCase").ToString = 1, ", lower: [1, Infinity]", ""))

            '// Uppercase
            strRet.Append(IIf(ConfigurationManager.AppSettings("UpperCase").ToString = 1, ", upper: [1, Infinity]", ""))

            '// Numeric
            strRet.Append(IIf(ConfigurationManager.AppSettings("Numeric").ToString = 1, ", numeric: [1, Infinity]", ""))

            '// Special
            strRet.Append(IIf(ConfigurationManager.AppSettings("Special").ToString = 1, ", special: [1, Infinity]", ""))

            strRet.Append("});")
            strRet.Append("e.IsValid = passed;")
            strRet.Append("}")

            Return strRet.ToString
        End Function

        Public Shared Function CheckPwd(ByVal strPwd As String) As Boolean
            Dim blnRet As Boolean = True

            Dim lowerRegEx As Regex = New Regex("[a-z]")
            Dim upperRegEx As Regex = New Regex("[A-Z]")
            Dim numericRegEx As Regex = New Regex("[0-9]")
            Dim specialRegEx As Regex = New Regex("[\W_]")

            Dim intMinLength As Integer = ConfigurationManager.AppSettings("MinLength")
            If strPwd.Length < intMinLength Then
                Return False
            End If

            If ConfigurationManager.AppSettings("LowerCase").ToString = 1 AndAlso lowerRegEx.Matches(strPwd).Count < 1 Then
                Return False
            End If
            If ConfigurationManager.AppSettings("UpperCase").ToString = 1 AndAlso upperRegEx.Matches(strPwd).Count < 1 Then
                Return False
            End If
            If ConfigurationManager.AppSettings("Numeric").ToString = 1 AndAlso numericRegEx.Matches(strPwd).Count < 1 Then
                Return False
            End If
            If ConfigurationManager.AppSettings("Special").ToString = 1 AndAlso specialRegEx.Matches(strPwd).Count < 1 Then
                Return False
            End If

            Return blnRet
        End Function

        Public Shared Function GetPwdErrorMsg() As String
            Dim strCustomizedError As String = String.Empty

            Try
                If ConfigurationManager.AppSettings("LowerCase").ToString = 1 Then
                    strCustomizedError += " lower case"
                End If
                If ConfigurationManager.AppSettings("UpperCase").ToString = 1 Then
                    strCustomizedError += ", upper case"
                End If
                If ConfigurationManager.AppSettings("Numeric").ToString = 1 Then
                    strCustomizedError += ", numerics"
                End If
                If ConfigurationManager.AppSettings("Special").ToString = 1 Then
                    strCustomizedError += ", special character"
                End If

                If (strCustomizedError.Length > 0) Then
                    strCustomizedError = "Password must contain a mixture of " & strCustomizedError & " and minimum "
                Else
                    strCustomizedError = "Password must contain minimum "
                End If

                Dim intMinLength As Integer = ConfigurationManager.AppSettings("MinLength")
                strCustomizedError += ConfigurationManager.AppSettings("MinLength") & " characters."

                Return strCustomizedError
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Class

Public Class clsUser
    Private strUserID, strUserCode, strUserName, strUserLogin, strUserType As String

    Public Property strSyncImageLoc As String

    Public Property UserID() As String
        Get
            Return strUserID
        End Get
        Set(ByVal value As String)
            strUserID = Trim(value)
        End Set
    End Property
    Public Property UserCode() As String
        Get
            Return strUserCode
        End Get
        Set(ByVal value As String)
            strUserCode = Trim(value)
        End Set
    End Property
    Private _Login As String
    Public Property Login() As String
        Get
            Return _Login
        End Get
        Set(ByVal value As String)
            _Login = value
        End Set
    End Property

    Private _sessionKey As String
    Public Property SessionKey() As String
        Get
            Return _sessionKey
        End Get
        Set(ByVal value As String)
            _sessionKey = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return strUserName
        End Get
        Set(ByVal value As String)
            strUserName = Trim(value)
        End Set
    End Property
    Public Property UserLogin() As String
        Get
            Return strUserLogin
        End Get
        Set(ByVal value As String)
            strUserLogin = Trim(value)
        End Set
    End Property
    Public Property UserType() As String
        Get
            Return strUserType
        End Get
        Set(ByVal value As String)
            strUserType = Trim(value)
        End Set
    End Property

    Private _year As Integer
    Public Property YEAR() As Integer
        Get
            Return _year
        End Get
        Set(ByVal value As Integer)
            _year = value
        End Set
    End Property
    Private _month As Integer
    Public Property MONTH() As Integer
        Get
            Return _month
        End Get
        Set(ByVal value As Integer)
            _month = value
        End Set
    End Property
    Private _quarter As Integer
    Public Property QUARTER() As Integer
        Get
            Return _quarter
        End Get
        Set(ByVal value As Integer)
            _quarter = value
        End Set
    End Property
    Private _pageSize As Integer
    Public Property PageSize() As Integer
        Get
            Return IIf(_pageSize = 0, 15, _pageSize)
        End Get
        Set(ByVal value As Integer)
            _pageSize = value
        End Set
    End Property

    Private _EPVersion As String
    Public Property EPVersion() As String
        Get
            Return IIf(_EPVersion = "0", "15", _EPVersion)
        End Get
        Set(ByVal value As String)
            _EPVersion = value
        End Set
    End Property

    '--------------------------Customised Report Usage '--------------------------Customised Report Usage
    Private _CuzRptTeamList As String
    Public Property CuzRptTeamList() As String
        Get
            Return _CuzRptTeamList
        End Get
        Set(ByVal value As String)
            _CuzRptTeamList = value
        End Set
    End Property

    Private _CuzRptSrList As String
    Public Property CuzRptSrList() As String
        Get
            Return _CuzRptSrList
        End Get
        Set(ByVal value As String)
            _CuzRptSrList = value
        End Set
    End Property

    Private _CuzRptPrdList As String
    Public Property CuzRptPrdList() As String
        Get
            Return _CuzRptPrdList
        End Get
        Set(ByVal value As String)
            _CuzRptPrdList = value
        End Set
    End Property
    '--------------------------Customised Report Usage '--------------------------Customised Report Usage
End Class


Public Class Excel
    Public Shared Sub PrepareControlForExport(ByRef ctrlHTML As Control)
        Dim ltrField As Literal = New Literal()
        Dim intIdx As Integer
        Dim ctrl As Control = Nothing
        For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
            ctrl = ctrlHTML.Controls(intIdx)
            If TypeOf ctrl Is LinkButton Then
                ltrField.Text = (CType(ctrl, LinkButton)).Text
                ctrlHTML.Controls.Remove(ctrl)
                ctrlHTML.Controls.AddAt(intIdx, ltrField)
            ElseIf TypeOf ctrl Is HyperLink Then
                ltrField.Text = (CType(ctrl, HyperLink)).Text
                ctrlHTML.Controls.Remove(ctrl)
                ctrlHTML.Controls.AddAt(intIdx, ltrField)
            ElseIf TypeOf ctrl Is DropDownList Then
                ltrField.Text = (CType(ctrl, DropDownList)).SelectedItem.Text
                ctrlHTML.Controls.Remove(ctrl)
                ctrlHTML.Controls.AddAt(intIdx, ltrField)
            ElseIf TypeOf ctrl Is CheckBox Then
                'ltrField.Text = IIf(CType(ctrl, CheckBox).Checked, "True", "False")
                ctrlHTML.Controls.Remove(ctrl)
                'ctrlHTML.Controls.AddAt(intIdx, ltrField)
            ElseIf TypeOf ctrl Is ImageButton OrElse TypeOf ctrl Is Image Then
                ctrlHTML.Controls.Remove(ctrl)
                ctrlHTML.Controls.AddAt(intIdx, ltrField)
            ElseIf TypeOf ctrl Is Button Then   'HL:20080508
                ltrField.Text = ""
                ctrlHTML.Controls.Remove(ctrl)
                ctrlHTML.Controls.AddAt(intIdx, ltrField)
            End If
            If ctrl IsNot Nothing AndAlso ctrl.HasControls Then PrepareControlForExport(ctrl)
        Next
        'If ctrlHTML.GetType.BaseType Is GetType(GridView) Then
        '    Dim dgList As GridView = ctrlHTML
        '    Dim aryList As New ArrayList
        '    Dim idx As Integer
        '    Try
        '        For idx = 0 To dgList.Rows(0).Cells.Count - 1
        '            If IsDate(dgList.Rows(0).Cells(idx).Text) Then aryList.Add(idx)
        '        Next
        '        If aryList.Count > 0 Then
        '            Dim DR As GridViewRow
        '            For Each DR In dgList.Rows
        '                For Each idx In aryList
        '                    DR.Cells(idx).Attributes.CssStyle.Add("mso-number-format", "'yyyy\\-mm\\-dd'")
        '                Next
        '            Next
        '        End If
        '    Catch ex As Exception
        '    End Try
        'End If
    End Sub

    Public Shared Sub ExportToFile(ByRef dgList As Control, ByRef MyRespose As System.Web.HttpResponse, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
        Dim strFileName As String = customFileName & ".xls"
        MyRespose.Clear()
        MyRespose.AddHeader("content-disposition", "attachment;filename=" & strFileName)
        MyRespose.Charset = "utf-8"
        MyRespose.ContentType = "application/vnd.xls"
        'MyRespose.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        'create a emptry row table, act as empty row for insert
        Dim newRow As New HtmlTable()
        newRow.Rows.Add(New HtmlTableRow())
        newRow.RenderControl(htmlWrite) 'insert a empty row
        MyRespose.Write("<meta http-equiv=Content-Type content=""text/html; charset=utf-8"">")
        PrepareControlForExport(dgList)
        dgList.RenderControl(htmlWrite)
        If Not String.IsNullOrEmpty(SheetStyle) Then MyRespose.Write(SheetStyle)
        MyRespose.Write(stringWrite.ToString) 
        MyRespose.Flush()
        MyRespose.End()
    End Sub

    Public Shared Sub ExportToFile(ByRef aryDgList As ArrayList, ByRef MyRespose As System.Web.HttpResponse, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
        Dim strFileName As String = customFileName & ".xls"
        MyRespose.Clear()
        MyRespose.AddHeader("content-disposition", "attachment;filename=" & strFileName)
        MyRespose.Charset = ""
        MyRespose.ContentType = "application/vnd.xls"
        'MyRespose.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        'create a emptry row table, act as empty row for insert
        Dim newRow As New HtmlTable()
        newRow.Rows.Add(New HtmlTableRow())

        For Each dgList As Control In aryDgList
            newRow.RenderControl(htmlWrite) 'insert a empty row
            PrepareControlForExport(dgList)
            dgList.RenderControl(htmlWrite)
        Next
        If Not String.IsNullOrEmpty(SheetStyle) Then MyRespose.Write(SheetStyle)
        MyRespose.Write(stringWrite.ToString) 
        MyRespose.Flush()
        MyRespose.End()
    End Sub
End Class


Public Class Cuzrpt
    Public Shared Property CallRateByBrandSession() As pptCriteria
        Get
            Current.Session("CallRateByBrandSession") = IIf(Current.Session("CallRateByBrandSession") Is Nothing, New pptCriteria, Current.Session("CallRateByBrandSession"))
            Return Current.Session("CallRateByBrandSession")
        End Get
        Set(ByVal value As pptCriteria)
            Current.Session("CallRateByBrandSession") = value
            If value Is Nothing Then
                Current.Session.Remove("CallRateByBrandSession")
            End If
        End Set
    End Property

End Class

Public Class pptCriteria
    Public strTeamCode, strSalesrepCode, strDistrictCode, strChannelCode, strCustClass, strSpecialty, strPrdClass, strCriteria, strYear, strMonth As String

    Public Property Year() As String
        Get
            Return strYear
        End Get
        Set(ByVal value As String)
            strYear = Trim(value)
        End Set
    End Property


    Public Property Month() As String
        Get
            Return strMonth
        End Get
        Set(ByVal value As String)
            strMonth = Trim(value)
        End Set
    End Property


    Public Property TeamCode() As String
        Get
            Return strTeamCode
        End Get
        Set(ByVal value As String)
            strTeamCode = Trim(value)
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return strSalesrepCode
        End Get
        Set(ByVal value As String)
            strSalesrepCode = Trim(value)
        End Set
    End Property

    Public Property DistrictCode() As String
        Get
            Return strDistrictCode
        End Get
        Set(ByVal value As String)
            strDistrictCode = Trim(value)
        End Set
    End Property

    Public Property ChannelCode() As String
        Get
            Return strChannelCode
        End Get
        Set(ByVal value As String)
            strChannelCode = Trim(value)
        End Set
    End Property

    Public Property CustClass() As String
        Get
            Return strCustClass
        End Get
        Set(ByVal value As String)
            strCustClass = Trim(value)
        End Set
    End Property

    Public Property Specialty() As String
        Get
            Return strSpecialty
        End Get
        Set(ByVal value As String)
            strSpecialty = Trim(value)
        End Set
    End Property

    Public Property PrdClass() As String
        Get
            Return strPrdClass
        End Get
        Set(ByVal value As String)
            strPrdClass = Trim(value)
        End Set
    End Property

    Public Property Criteria() As String
        Get
            Return strCriteria
        End Get
        Set(ByVal value As String)
            strCriteria = Trim(value)
        End Set
    End Property
End Class

Public Class YearCycleController
    Public Shared Sub LoadYearCycle()
        Dim dt As DataTable
        Dim objCommon As New mst_Common.clsCommon
        Try
            dt = objCommon.GetYearCycleList

            Portal.YearCycle = dt

        Catch ex As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' For HK EISAI
    ''' For use in call analysis by month. Use start year + month to determine the year of month
    ''' eg, January for 2010 start year, result: 2011
    ''' 
    ''' For other principals, they don't have special year cycle data, and it will return start year as return result
    ''' </summary>
    ''' <param name="strMonth"></param>
    ''' <param name="strYear"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetYearByMonthAndYear(ByVal strMonth As String, ByVal strYear As String) As String
        Dim results() As DataRow
        Try
            If Not HasYearCycle() Then
                Return strYear
            Else
                results = Portal.YearCycle.Select("month = '" & strMonth & "'")

                If results.Length > 0 Then
                    Select Case results(0)("YEAR_IND")
                        Case "1"
                            Return strYear
                        Case "-1"
                            Return Convert.ToInt32(strYear) + 1
                        Case Else
                            Return strYear
                    End Select
                Else
                    Return strYear
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

#Region "Private"
    Private Shared Function HasYearCycle() As Boolean
        If IsNothing(Portal.YearCycle) Then
            Return False
        ElseIf Portal.YearCycle.Rows.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region
End Class