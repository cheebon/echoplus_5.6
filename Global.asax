<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        Dim exx As Exception = Server.GetLastError.GetBaseException
        Dim objLog As New cor_Log.clsLog
        
        Try
            With objLog
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = "Message:" & exx.Message & "\nSource: " & exx.Source & "\nForm: " & Request.Form.ToString & "\nQuesryString: " & Request.QueryString.ToString() & "\nTARGETSITE: " & exx.TargetSite.ToString & "\nSTACKTRACE: " & exx.StackTrace
                .Log()
            End With
        Catch ex As Exception
        Finally
            objLog = Nothing
        End Try
    End Sub
    Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
        If Not TypeOf (Context.Handler) Is IRequiresSessionState Then Exit Sub
        If Portal.UserSession IsNot Nothing AndAlso Not String.IsNullOrEmpty(Portal.UserSession.UserID) Then
            Dim objUser As New adm_User.clsUser
            Dim strKeyUpdateCount As String = 0
            With Portal.UserSession
                strKeyUpdateCount = objUser.UserLoginKeyCheck(.UserID, .UserLogin, .SessionKey)
            End With
            If (IsNumeric(strKeyUpdateCount) AndAlso strKeyUpdateCount > 0) _
                Or Portal.UserSession.Login Like "*BSADMIN" Then
                'Means session updated successful
                'BSADMIN will be bypass the checking
            Else
                'session lost or has been kicked out
                Portal.UserSession.UserID = String.Empty
            End If
        End If
    End Sub


    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
       
</script>