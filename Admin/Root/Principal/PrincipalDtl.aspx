﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrincipalDtl.aspx.vb" Inherits="Admin_Root_Principal_PrincipalDtl" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Principal Details</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmPrincipalDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /><br />
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <!--Begin Modify Here -->
                <div id="MainFrame" class="MainFrame">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 0px 15px 0px;">
                                            <tr>
                                                <td>
                                                    <div class="border_std">
                                                    <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="False"
                                                        UseSubmitBehavior="false" Width="60px" />
                                                    </div>    
                                                </td>
                                            </tr>
                                           
                                            <tr class="ajax__tab_portal">
                                                <td class="ajax__tab_PNL">
                                                    <div class="border_std">
                                                    <customToolkit:wuc_lblInfo ID="lblErrDtl" runat="server" />
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                            <asp:Button ID="btnEditDtl" runat="server" Text="Edit" CausesValidation="False"
                                                                Width="60px" />&nbsp;
                                                            <asp:Button ID="btnSaveDtl" runat="server" Text="Save" CausesValidation="true"
                                                                ValidationGroup="ValidateDtl" Width="60px" />&nbsp;
                                                            <asp:Button ID="btnCancelDtl" runat="server" Text="Cancel"
                                                                CausesValidation="false" Width="60px" />&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="width:50%">
                                                                <asp:DetailsView ID="dvHdr_Left" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="PRINCIPAL_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="Principal Code">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalCode" runat="server" Enabled="false" Text='<%# Bind("PRINCIPAL_CODE") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalCode" runat="server" ControlToValidate="txtPrincipalCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalCode" runat="server" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalCode" runat="server" ControlToValidate="txtPrincipalCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPrincipalCode" runat="server" Text='<%# Bind("PRINCIPAL_CODE") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Country">
                                                                            <EditItemTemplate>
                                                                                <asp:DropDownList ID="ddlCountry" runat="server" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:DropDownList ID="ddlCountry" runat="server" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Bind("COUNTRY_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Setting ID">
                                                                            <EditItemTemplate>
                                                                                <asp:DropDownList ID="ddlSetting" runat="server" />
                                                                                <asp:RequiredFieldValidator ID="rfvSetting" runat="server" ControlToValidate="ddlSetting"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:DropDownList ID="ddlSetting" runat="server" />
                                                                                <asp:RequiredFieldValidator ID="rfvSetting" runat="server" ControlToValidate="ddlSetting"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSettingID" runat="server" Text='<%# Bind("SETTING_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>
                                                            <td style="width:50%">
                                                                <asp:DetailsView ID="dvHdr_Right" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="PRINCIPAL_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="Principal Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalName" runat="server" Text='<%# Bind("PRINCIPAL_NAME") %>' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalName" runat="server" ControlToValidate="txtPrincipalName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalName" runat="server" Text='' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalName" runat="server" ControlToValidate="txtPrincipalName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPrincipalName" runat="server" Text='<%# Bind("PRINCIPAL_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Principal Name 1">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalName1" runat="server" Text='<%# Bind("PRINCIPAL_NAME_1") %>' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalName1" runat="server" ControlToValidate="txtPrincipalName1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalName1" runat="server" Text='' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalName1" runat="server" ControlToValidate="txtPrincipalName1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPrincipalName1" runat="server" Text='<%# Bind("PRINCIPAL_NAME_1") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Principal Description">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalDesc" runat="server" Text='<%# Bind("PRINCIPAL_DESC") %>' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalDesc" runat="server" ControlToValidate="txtPrincipalDesc"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalDesc" runat="server" Text='' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalDesc" runat="server" ControlToValidate="txtPrincipalDesc"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPrincipalDesc" runat="server" Text='<%# Bind("PRINCIPAL_DESC") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Principal Description 1">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalDesc1" runat="server" Text='<%# Bind("PRINCIPAL_DESC_1") %>' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalDesc1" runat="server" ControlToValidate="txtPrincipalDesc1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtPrincipalDesc1" runat="server" Text='' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvPrincipalDesc1" runat="server" ControlToValidate="txtPrincipalDesc1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPrincipalDesc1" runat="server" Text='<%# Bind("PRINCIPAL_DESC_1") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>                                                         
                                                        </tr>                                                        
                                                    </table>
                                                    </div>
                                                </td>
                                            </tr>
                                           
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--End Modify Here -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
