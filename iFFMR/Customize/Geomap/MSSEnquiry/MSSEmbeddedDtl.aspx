﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MSSEmbeddedDtl.aspx.vb" Inherits="iFFMR_Customize_Geomap_MSSEnquiry_MSSEmbeddedDtl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <title>MSS Details</title>
    <style type="text/css">
        .answerPane
        {
            FONT-WEIGHT: normal;
            FONT-SIZE:8pt;
            COLOR: Black;   
            FONT-FAMILY:  "Frutiger LT 45 Light", Tahoma,Verdana, Arial;
        }
        .Header
        {
            background-color: #5d7b9d;
            COLOR: White;
             border-bottom-color:Black; 
            border-top-color:Black; 
            border-left-color:black; 
            border-right-color:black;
            text-align: left; 
            font-weight: bold; 
            
            }    
        
        .Row
        {
            border-bottom-color:Black; 
            border-top-color:Black; 
            border-left-color:black; 
            border-right-color:black;
            vertical-align: top;
            background-color: #eff8fc;
            }
       
        .ctrlPanel {border: solid 1px #0099ff; width:99%; padding: 5px 0px 5px 5px}
    </style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="margin: 0; border: 0; padding: 0;" class="BckgroundInsideContentLayout">
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
    <div>
    
    <div style="width:333px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898">
        <span class="cls_label_header">Customer: </span><asp:Label CssClass="cls_label" ID="lblCustomer" runat="server" Text=""></asp:Label>
    </div>
    <asp:Repeater ID="rptrAnswer" runat="server">
            <HeaderTemplate>
                <table border="1" cellpadding="5" cellspacing="0" width="340px" rules="all" style="border: solid 1px black; border-collapse:collapse">
            </HeaderTemplate>
            <ItemTemplate>
                <%#GetGroup(Container.DataItem("QUES_CODE").ToString, Container.DataItem("QUES_NAME").ToString, Container.DataItem("GROUP_COUNT").ToString)%>
                    
                    <tr>           
                    <td class="Row">
                        <div class="cls_label">
                            <%#Container.DataItem("SUB_QUES_CODE").ToString%>. <%#Container.DataItem("SUB_QUES_NAME").ToString%>
                        </div>
                        <br />
                        <span class="cls_label"  style="font-weight:bold">Answer: </span>
                        <div class="cls_label" style="margin-left:5px;margin-right:5px;background-color:#fdfdfd;padding:2px; border:solid 1px #d9d9d9">
                            <%#Server.HtmlDecode(Container.DataItem("ANSWER").ToString)%>
                        </div>
                    </td>                    
                   </tr>
            </ItemTemplate>  
            <FooterTemplate>
                </table>
            </FooterTemplate>          
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
