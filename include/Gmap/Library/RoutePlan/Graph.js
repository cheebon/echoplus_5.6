﻿var _graph; //store reference to Graph object
var CallBackFunction = function() { };
var FailCallBackFunction = function() { };
var gdir = []; //Store gDirection obj
var path; //multidimensional array to store paths. as loadfromwaypoints only accep max of 25 points. so need to break full path into several paths.

function Graph(_map)
{
    this.RawGraph = new Array; //Customer Type; The original graph
    this.ShortestPath = new Array; //Node Type; Processed shortest path
    this.Neighbours = new Array; //Node Type; Place to place current node's neighbour
    this.Map = _map;
    
    _graph = this;
}

//Functions prototype
Graph.prototype.BulkAddWaypoint = bulkAddWaypoint;
Graph.prototype.PopulateNeighbours = populateNeighbours;
Graph.prototype.setMin=setMin;
Graph.prototype.ReturnError=ReturnError;
Graph.prototype.GetNextDestination = getNextDestination;
Graph.prototype.PrepPath = prepPath;
/////////////////////////////////////////////////////////////////////


//
//Function Definition
function bulkAddWaypoint(_rgArray)
{
    _graph.RawGraph = _rgArray;
}

/******************************************************************
//Find all unvisited neighbours
******************************************************************/
function populateNeighbours(_CurrentCust)
{
    _graph.Neighbours = [];
    var neigh
    
    for(var i=0;i<_graph.RawGraph.length;++i)
    {
        if (_graph.RawGraph[i].CustCode != _CurrentCust.CustCode && _graph.RawGraph[i].Visited == false && _graph.RawGraph[i].IsDestination == false)
        {
            neigh = new Node(_graph.RawGraph[i].CustCode, _graph.RawGraph[i].Latitude, _graph.RawGraph[i].Longitude);
            _graph.Neighbours.push(neigh);
        }
    }

}

function getNextDestination(_CallBackFunc, _FailCallBackFunc, _CurrentCust)
{
    if (typeof _CallBackFunc == 'function')
    {
        CallBackFunction = _CallBackFunc;
    }
    if (typeof _FailCallBackFunc == 'function') {
        FailCallBackFunction = _FailCallBackFunc;
    }
    
    
    MinDistance=0;
    MinPoint = 0;
    path = _graph.PrepPath(_CurrentCust);


    //Power GEvent loop.
    //by using length of path, we loop to add GEvent for loadfromwaypoints' load event.
    //after each load, we call the next gdirection.loadfromwaypoints to continue load next member of path
    //for(var pl=0;pl<_graph.Neighbours.length;++pl)
    for(var pl=0;pl<path.length;++pl)
    {
          gdir[pl] = new GDirections();


          GEvent.addListener(gdir[pl], "load", (function(pl) {
              return function() {

                  if (pl == 0) { //First round, set initial first. Important as it will cause a bug if overwritten by 2nd or 3rd round
                      MinDistance = gdir[pl].getRoute(0).getDistance().meters;
                      MinPoint = 0;
                  }

                  for (var i = 0; i < gdir[pl].getNumRoutes(); i += 2) {
                      if (gdir[pl].getRoute(i).getDistance().meters < MinDistance) {
                          MinDistance = gdir[pl].getRoute(i).getDistance().meters;
                          MinPoint = (pl * 10) + (i / 2); //Need calculate back the current node index, as the neighbours are split into arrays of 10 to make the gDirections call. pl is the index of current array, while i is the index for distance array returned from gDirections.
                      }
                  }

                  if (path.length - 1 > pl) {
                      gdir[pl + 1].loadFromWaypoints(path[pl + 1]);
                  }
                  else {
                      _graph.setMin();
                  }

              };
          }
        )(pl));


          GEvent.addListener(gdir[pl], "error", (function(pl) {
              return function() {
                _graph.ReturnError(gdir[pl].getStatus().code);

              };
          }
        )(pl));
    }
    //Start the loop
    gdir[0].loadFromWaypoints(path[0]);
}
function setMin()
{
    for(var i=0; i<_graph.RawGraph.length; ++i)
    {
        if (_graph.RawGraph[i].CustCode.toString() == _graph.Neighbours[MinPoint].CustCode.toString())
        {
            var node = new Node(_graph.Neighbours[MinPoint].CustCode, _graph.Neighbours[MinPoint].Latitude, _graph.Neighbours[MinPoint].Longitude);
            //_graph.SPaths.push(_graph.Waypoints[i].LatLng.toString());
            _graph.ShortestPath.push(node);
            _graph.RawGraph[i].Visited = true;
            break;          
        }       
    }
    CallBackFunction(_graph.Neighbours[MinPoint].CustCode);
}
function ReturnError(ErrorCode) {
    FailCallBackFunction(ErrorCode);
} 

function prepPath(_CurrentCust)
{
    var paths = [];
    var partialpath;

    
    for(var i = 0;i<_graph.Neighbours.length;++i)
    {
        if (i % 10 == 0)
        {
            if (i==0)
            { partialpath = []; }
            else
            {   paths.push(partialpath);
                partialpath = [];   }                
        }
            
        partialpath.push(_CurrentCust.Latitude + ", " + _CurrentCust.Longitude);
        partialpath.push(_graph.Neighbours[i].Latitude + ", " + _graph.Neighbours[i].Longitude);
       
    }
    paths.push(partialpath);
    return paths;
}