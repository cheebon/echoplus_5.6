'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	15/09/2005
'	Purpose	    :	Class to build SQL Query string
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports cor_DB

'Interface Icor_TemplateQuery
'    Function GetTemplateList(ByVal UserID As Long) As DataTable
'End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsSqlQueryBuilder
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    ''---------------------------------------------------------------------
    '' Procedure 	: 	Function BuildSqlQuery
    '' Purpose	    :	To build the sql squery string
    '' Parameters    :	[in] strTableName As String, ByVal strAttributes As String, Optional ByVal strCriteria As String = "", Optional ByVal strSortingOrders As String = "", Optional ByVal strGroupOrders As String = "", Optional ByVal strHavingOrders As String = ""
    ''   		        [in]      e: -
    ''		            [out]      : SqlQueryString
    ''----------------------------------------------------------------------
    Public Shared Function BuildSqlQuery(ByVal strTableName As String, ByVal strAttributes As String, Optional ByVal strCriteria As String = "", Optional ByVal strSortingOrders As String = "", Optional ByVal strGroupOrders As String = "", Optional ByVal strHavingOrders As String = "") As String
        If strTableName Is String.Empty Then Throw New NullReferenceException("Database table name cannot be empty!")
        Dim strSqlQuery As New Text.StringBuilder

        Try
            With strSqlQuery
                .Append("SELECT ")
                If strAttributes = Nothing OrElse strAttributes.Length = 0 Then
                    .Append("* ")
                Else
                    .Append(strAttributes & " ")
                End If
                .Append(" FROM " & strTableName & " ")
                If strCriteria.Length <> 0 Then .Append(" WHERE " & strCriteria & " ")
                If strSortingOrders.Length <> 0 Then .Append(" ORDER BY " & strSortingOrders)
                If strGroupOrders.Length <> 0 Then .Append(" GROUP BY " & strGroupOrders)
                If strHavingOrders.Length <> 0 Then .Append(" HAVING " & strHavingOrders)
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("clsSqlQueryBuilder.BuildSqlQuery : " & ex.Message))
        Finally
        End Try

        Return strSqlQuery.ToString
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
