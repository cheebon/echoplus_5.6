Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data


<WebService([Namespace]:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ScriptService()> _
Public Class ws_CustTxn
    Inherits System.Web.Services.WebService

    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class


    Public Class Customer
        Inherits BaseEntity
#Region "LocalVariable"
        Private _CustName As String = String.Empty
        Private _Address As String = String.Empty
        Private _PrdName As String = String.Empty
        Private _PayerName As String = String.Empty
#End Region

#Region "Public Porperty"
        Public Property CUST_NAME() As String
            Get
                Return _CustName
            End Get
            Set(ByVal value As String)
                _CustName = value
            End Set
        End Property
        Public Property ADDRESS() As String
            Get
                Return _Address
            End Get
            Set(ByVal value As String)
                _Address = value
            End Set
        End Property
        Public Property PRD_NAME() As String
            Get
                Return _PrdName
            End Get
            Set(ByVal value As String)
                _PrdName = value
            End Set
        End Property
        Public Property PAYER_NAME() As String
            Get
                Return _PayerName
            End Get
            Set(ByVal value As String)
                _PayerName = value
            End Set
        End Property
#End Region

    End Class

    Public Class Product
        Inherits BaseEntity
#Region "LocalVariable"
        Private _PrdName As String = String.Empty
        Private _UOMPrice As String = String.Empty
        Private _UOMCode As String = String.Empty
#End Region

#Region "Public Porperty"

        Public Property PRD_NAME() As String
            Get
                Return _PrdName
            End Get
            Set(ByVal value As String)
                _PrdName = value
            End Set
        End Property
        Public Property UOM_PRICE() As String
            Get
                Return _UOMPrice
            End Get
            Set(ByVal value As String)
                _UOMPrice = value
            End Set
        End Property
        Public Property UOM_CODE() As String
            Get
                Return _UOMCode
            End Get
            Set(ByVal value As String)
                _UOMCode = value
            End Set
        End Property
#End Region

    End Class

#Region "ClsCONN"
    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
            sqlCmd.CommandTimeout = 120
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
    'Private Shared ReadOnly ConnectionString As String = HttpContext.Current.Session("ffmr_conn")
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffms_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Private Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class

#End Region

#Region "Product Name"

    <WebMethod(EnableSession:=True)> _
 Public Function ValidateProduct(ByVal strPrdCode As String, ByVal strSalesrepCode As String) As PagedResult(Of Product)

        Dim rptRows As New List(Of Product)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Product)
            rptRows.Add(NewRow_ValidatePrd)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_WEB_GET_TRA_PRD_VALIDATE"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_ValidatePrd(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Product)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_ValidatePrd(ByRef reader As IDataReader) As Product
        Dim rptRow As New Product
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "PRD_NAME"
                        SetValue(Of String)(.PRD_NAME, reader("PRD_NAME"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_ValidatePrd() As Product
        Dim rptRow As New Product
        With rptRow
            .PRD_NAME = String.Empty
        End With
        Return rptRow
    End Function
#End Region

#Region "Product UOM"
    <WebMethod(EnableSession:=True)> _
     Public Function GetUOMCode(ByVal strPrdCode As String) As PagedResult(Of Product)

        Dim rptRows As New List(Of Product)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Product)
            rptRows.Add(NewRow_GetUOMCode)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_WEB_GET_TRA_UOMCODE"
                sqlCmd.CommandType = CommandType.StoredProcedure
                'clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                'clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_GetUOMCode(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Product)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_GetUOMCode(ByRef reader As IDataReader) As Product
        Dim rptRow As New Product
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "UOM_PRICE"
                        SetValue(Of String)(.UOM_PRICE, reader("UOM_PRICE"), String.Empty)
                    Case "UOM_CODE"
                        SetValue(Of String)(.UOM_CODE, reader("UOM_CODE"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_GetUOMCode() As Product
        Dim rptRow As New Product
        With rptRow
            .UOM_PRICE = String.Empty
            .UOM_CODE = String.Empty
        End With
        Return rptRow
    End Function
#End Region


#Region "ShipToAddress"
    <WebMethod(EnableSession:=True)> _
         Public Function GetShipToAddress(ByVal strCustCode As String, ByVal strShipToCode As String) As PagedResult(Of Customer)

        Dim rptRows As New List(Of Customer)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Customer)
            rptRows.Add(NewRow_GetShipToAddress)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_WEB_GET_SO_SHIPTO_ADDRESS_WS"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "SHIPTO_CODE", strShipToCode, clsCONN.dtType.dbString)
                'clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)

                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_GetShipToAddress(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Customer)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_GetShipToAddress(ByRef reader As IDataReader) As Customer
        Dim rptRow As New Customer
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "ADDRESS"
                        SetValue(Of String)(.ADDRESS, reader("ADDRESS"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_GetShipToAddress() As Customer
        Dim rptRow As New Customer
        With rptRow
            .ADDRESS = String.Empty
        End With
        Return rptRow
    End Function

#End Region


#Region "SO UOM PRD Price"
    <WebMethod(EnableSession:=True)> _
     Public Function GetPrdUOMPrice(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String _
                                    , ByVal strPrdCode As String, ByVal strUOMCode As String, ByVal strQty As String) As PagedResult(Of Product)

        Dim rptRows As New List(Of Product)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Product)
            rptRows.Add(NewRow_GetPrdUOMPrice)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_WEB_GET_SO_PRD_UOM_PRICE_WS"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "CONT_CODE", strContCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "UOM_CODE", strUOMCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "QTY", strQty, clsCONN.dtType.dbString)
                'clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)

                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_GetPrdUOMPrice(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Product)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_GetPrdUOMPrice(ByRef reader As IDataReader) As Product
        Dim rptRow As New Product
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "UOM_PRICE"
                        SetValue(Of String)(.UOM_PRICE, reader("UOM_PRICE"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_GetPrdUOMPrice() As Product
        Dim rptRow As New Product
        With rptRow
            .UOM_PRICE = String.Empty
        End With
        Return rptRow
    End Function

#End Region













End Class
