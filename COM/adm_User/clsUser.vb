'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	27/10/2006
'	Purpose	    :	Class User 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |21/06/2007    	|Puah Hong Ling     |Add Sub ProfileUpdate() |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data
Public Class clsUser
    Implements IDisposable
#Region "Intenal Properties"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New clsProperties.clsUser

#End Region
#Region "User"
    Public Sub Create()
        Dim objDB As cor_DB.clsDB
        Dim objUserAccessRight As clsUserAccessRight
        Dim objUserSalesRep As clsUserSalesRep
        Dim i As Integer = 0
        Dim y As Integer = 0
        Dim lngUserID As Long

        Try
            'Insert into User
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERCREATE"
                .addItem("user_type_id", clsProperties.user_type_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_id", clsProperties.salesteam_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_code", clsProperties.salesteam_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_id", clsProperties.salesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesrep_code", clsProperties.salesrep_code, cor_DB.clsDB.DataType.DBString)
                .addItem("region_id", clsProperties.region_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("region_code", clsProperties.region_code, cor_DB.clsDB.DataType.DBString)
                .addItem("area_id", clsProperties.area_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("area_code", clsProperties.area_code, cor_DB.clsDB.DataType.DBString)
                .addItem("language_id", clsProperties.language_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("branch_id", clsProperties.branch_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("branch_code", clsProperties.branch_code, cor_DB.clsDB.DataType.DBString)
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd", clsProperties.pwd, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name_1", clsProperties.user_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("email", clsProperties.email, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_expiry", clsProperties.pwd_expiry, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd_expiry_rmd", clsProperties.pwd_expiry_rmd, cor_DB.clsDB.DataType.DBInt)
                .addItem("login_expiry_flag", clsProperties.login_expiry_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("max_retry", clsProperties.max_retry, cor_DB.clsDB.DataType.DBInt)
                .addItem("page_size", clsProperties.page_size, cor_DB.clsDB.DataType.DBInt)
                .addItem("nv_flag", clsProperties.nv_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("default_nv", clsProperties.default_nv, cor_DB.clsDB.DataType.DBInt)
                .addItem("operation_type_id", clsProperties.operation_type_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("operation_type_code", clsProperties.operation_type_code, cor_DB.clsDB.DataType.DBString)
                .addItem("new_cust_no", clsProperties.new_cust_no, cor_DB.clsDB.DataType.DBString)
                .addItem("dummy_cust_no", clsProperties.dummy_cust_no, cor_DB.clsDB.DataType.DBString)
                .addItem("id_prefix", clsProperties.id_prefix, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_changed_date", clsProperties.pwd_changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("active_flag", clsProperties.active_flag, cor_DB.clsDB.DataType.DBInt)
                lngUserID = .spInsert()
                '.spInsert()
            End With

            'Insert into UserAccessRight
            If Not IsNothing(clsProperties.liAR) Then
                objUserAccessRight = New clsUserAccessRight
                With objUserAccessRight
                    For i = 0 To clsProperties.liAR.Count - 1
                        .clsProperties.user_id = lngUserID
                        .clsProperties.user_code = clsProperties.user_code
                        .clsProperties.accessright_id = Trim(clsProperties.liAR.Item(i).Value) '("accessright_id")
                        .clsProperties.country_id = 0 'Trim(drRow("country_id"))
                        .clsProperties.principal_id = 0 'Trim(drRow("principal_id"))
                        .clsProperties.created_date = clsProperties.created_date
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .clsProperties.changed_date = clsProperties.changed_date
                        .clsProperties.changed_user_id = clsProperties.changed_user_id
                        .clsProperties.delete_flag = clsProperties.delete_flag
                        .Create()
                    Next
                End With
            End If

            'Insert into UserSalesRep
            If Not IsNothing(clsProperties.liSR) Then
                objUserSalesRep = New clsUserSalesRep
                With objUserSalesRep
                    For i = 0 To clsProperties.liSR.Count - 1
                        .clsProperties.user_id = lngUserID
                        .clsProperties.user_code = clsProperties.user_code
                        .clsProperties.salesrep_code = "" 'Trim(clsProperties.liSR.Item(i).Value)
                        .clsProperties.salesrep_id = Trim(clsProperties.liSR.Item(i).Value) 'Trim(drRow("salesrep_id"))
                        .clsProperties.salesteam_id = 0 'Trim(drRow("salesteam_id"))
                        .clsProperties.created_date = clsProperties.created_date
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .clsProperties.changed_date = clsProperties.changed_date
                        .clsProperties.changed_user_id = clsProperties.changed_user_id
                        .clsProperties.delete_flag = clsProperties.delete_flag
                        .Create()
                    Next
                End With
            End If

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.Create" & ex.ToString))
        Finally
            objUserSalesRep = Nothing
            objUserAccessRight = Nothing
            objDB = Nothing
        End Try
    End Sub
    Public Sub Update()
        Dim objDB As cor_DB.clsDB
        Dim objUserAccessRight As clsUserAccessRight
        Dim objUserSalesRep As clsUserSalesRep
        Dim i As Integer = 0
        Dim y As Integer = 0

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERUPDATE"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_type_id", clsProperties.user_type_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_id", clsProperties.salesteam_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_code", clsProperties.salesteam_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_id", clsProperties.salesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesrep_code", clsProperties.salesrep_code, cor_DB.clsDB.DataType.DBString)
                .addItem("region_id", clsProperties.region_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("region_code", clsProperties.region_code, cor_DB.clsDB.DataType.DBString)
                .addItem("area_id", clsProperties.area_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("area_code", clsProperties.area_code, cor_DB.clsDB.DataType.DBString)
                .addItem("language_id", clsProperties.language_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("branch_id", clsProperties.branch_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("branch_code", clsProperties.branch_code, cor_DB.clsDB.DataType.DBString)
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_flag", clsProperties.pwd_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd", clsProperties.pwd, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name_1", clsProperties.user_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("email", clsProperties.email, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_expiry", clsProperties.pwd_expiry, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd_expiry_rmd", clsProperties.pwd_expiry_rmd, cor_DB.clsDB.DataType.DBInt)
                .addItem("login_expiry_flag", clsProperties.login_expiry_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("max_retry", clsProperties.max_retry, cor_DB.clsDB.DataType.DBInt)
                .addItem("page_size", clsProperties.page_size, cor_DB.clsDB.DataType.DBInt)
                .addItem("nv_flag", clsProperties.nv_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("default_nv", clsProperties.default_nv, cor_DB.clsDB.DataType.DBInt)
                .addItem("operation_type_id", clsProperties.operation_type_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("operation_type_code", clsProperties.operation_type_code, cor_DB.clsDB.DataType.DBString)
                .addItem("new_cust_no", clsProperties.new_cust_no, cor_DB.clsDB.DataType.DBString)
                .addItem("dummy_cust_no", clsProperties.dummy_cust_no, cor_DB.clsDB.DataType.DBString)
                .addItem("id_prefix", clsProperties.id_prefix, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_changed_date", clsProperties.pwd_changed_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("active_flag", clsProperties.active_flag, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

            'Update UserAccessRight
            If Not IsNothing(clsProperties.liAR) Then
                objUserAccessRight = New clsUserAccessRight
                With objUserAccessRight
                    .clsProperties.user_code = clsProperties.user_code
                    .Delete()

                    For i = 0 To clsProperties.liAR.Count - 1
                        .clsProperties.user_id = clsProperties.user_id
                        .clsProperties.user_code = clsProperties.user_code
                        .clsProperties.accessright_id = Trim(clsProperties.liAR.Item(i).Value) '("accessright_id")
                        .clsProperties.country_id = 0 'Trim(drRow("country_id"))
                        .clsProperties.principal_id = 0 'Trim(drRow("principal_id"))
                        .clsProperties.created_date = clsProperties.created_date
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .clsProperties.changed_date = clsProperties.changed_date
                        .clsProperties.changed_user_id = clsProperties.changed_user_id
                        .clsProperties.delete_flag = clsProperties.delete_flag
                        .Create()
                    Next
                End With
            End If

            'Update UserSalesRep
            If Not IsNothing(clsProperties.liSR) Then
                objUserSalesRep = New clsUserSalesRep
                With objUserSalesRep
                    .clsProperties.user_code = clsProperties.user_code
                    .Delete()

                    For i = 0 To clsProperties.liSR.Count - 1
                        .clsProperties.user_id = clsProperties.user_id
                        .clsProperties.user_code = clsProperties.user_code
                        .clsProperties.salesrep_code = "" 'Trim(clsProperties.liSR.Item(i).Value)
                        .clsProperties.salesrep_id = Trim(clsProperties.liSR.Item(i).Value) 'Trim(drRow("salesrep_id"))
                        .clsProperties.salesteam_id = 0 'Trim(drRow("salesteam_id"))
                        .clsProperties.created_date = clsProperties.created_date
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .clsProperties.changed_date = clsProperties.changed_date
                        .clsProperties.changed_user_id = clsProperties.changed_user_id
                        .clsProperties.delete_flag = clsProperties.delete_flag
                        .Create()
                    Next
                End With
            End If

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.Update" & ex.ToString))
        Finally
            objUserSalesRep = Nothing
            objUserAccessRight = Nothing
            objDB = Nothing
        End Try
    End Sub
    Public Sub Delete()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERDELETE"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub UpdateUserDtl()
        Dim objDB As cor_DB.clsDB
        'Dim objUserAccessRight As clsUserAccessRight
        'Dim objUserSalesRep As clsUserSalesRep
        Dim i As Integer = 0
        Dim y As Integer = 0

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERUPDATE"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_type_id", clsProperties.user_type_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_id", clsProperties.salesteam_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_code", clsProperties.salesteam_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_id", clsProperties.salesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesrep_code", clsProperties.salesrep_code, cor_DB.clsDB.DataType.DBString)
                .addItem("region_id", clsProperties.region_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("region_code", clsProperties.region_code, cor_DB.clsDB.DataType.DBString)
                .addItem("area_id", clsProperties.area_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("area_code", clsProperties.area_code, cor_DB.clsDB.DataType.DBString)
                .addItem("language_id", clsProperties.language_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("branch_id", clsProperties.branch_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("branch_code", clsProperties.branch_code, cor_DB.clsDB.DataType.DBString)
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_flag", clsProperties.pwd_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd", clsProperties.pwd, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name_1", clsProperties.user_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("email", clsProperties.email, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_expiry", clsProperties.pwd_expiry, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd_expiry_rmd", clsProperties.pwd_expiry_rmd, cor_DB.clsDB.DataType.DBInt)
                .addItem("login_expiry_flag", clsProperties.login_expiry_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("max_retry", clsProperties.max_retry, cor_DB.clsDB.DataType.DBInt)
                .addItem("page_size", clsProperties.page_size, cor_DB.clsDB.DataType.DBInt)
                .addItem("nv_flag", clsProperties.nv_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("default_nv", clsProperties.default_nv, cor_DB.clsDB.DataType.DBInt)
                .addItem("operation_type_id", clsProperties.operation_type_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("operation_type_code", clsProperties.operation_type_code, cor_DB.clsDB.DataType.DBString)
                .addItem("new_cust_no", clsProperties.new_cust_no, cor_DB.clsDB.DataType.DBString)
                .addItem("dummy_cust_no", clsProperties.dummy_cust_no, cor_DB.clsDB.DataType.DBString)
                .addItem("id_prefix", clsProperties.id_prefix, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_changed_date", clsProperties.pwd_changed_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("active_flag", clsProperties.active_flag, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.UpdateUserDtl" & ex.ToString))
        Finally
            'objUserSalesRep = Nothing
            'objUserAccessRight = Nothing
            objDB = Nothing
        End Try
    End Sub
    Public Sub UpdateUserAccessRight()
        'Dim objDB As cor_DB.clsDB
        Dim objUserAccessRight As clsUserAccessRight
        'Dim objUserSalesRep As clsUserSalesRep
        Dim i As Integer = 0
        Dim y As Integer = 0

        Try

            'Update UserAccessRight
            If Not IsNothing(clsProperties.liAR) Then
                objUserAccessRight = New clsUserAccessRight
                With objUserAccessRight
                    .clsProperties.user_code = clsProperties.user_code
                    .Delete()

                    For i = 0 To clsProperties.liAR.Count - 1
                        .clsProperties.user_id = clsProperties.user_id
                        .clsProperties.user_code = clsProperties.user_code
                        .clsProperties.accessright_id = Trim(clsProperties.liAR.Item(i).Value) '("accessright_id")
                        .clsProperties.country_id = 0 'Trim(drRow("country_id"))
                        .clsProperties.principal_id = 0 'Trim(drRow("principal_id"))
                        .clsProperties.created_date = clsProperties.created_date
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .clsProperties.changed_date = clsProperties.changed_date
                        .clsProperties.changed_user_id = clsProperties.changed_user_id
                        .clsProperties.delete_flag = clsProperties.delete_flag
                        .Create()
                    Next
                End With
            End If



        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.UpdateUserAccessRight" & ex.ToString))
        Finally
            'objUserSalesRep = Nothing
            objUserAccessRight = Nothing
            'objDB = Nothing
        End Try
    End Sub
    Public Sub UpdateUserSalesRep()
        'Dim objDB As cor_DB.clsDB
        'Dim objUserAccessRight As clsUserAccessRight
        Dim objUserSalesRep As clsUserSalesRep
        Dim i As Integer = 0
        Dim y As Integer = 0

        Try
            'Update UserSalesRep
            If Not IsNothing(clsProperties.liSR) Then
                objUserSalesRep = New clsUserSalesRep
                With objUserSalesRep
                    .clsProperties.user_code = clsProperties.user_code
                    .Delete()

                    For i = 0 To clsProperties.liSR.Count - 1
                        .clsProperties.user_id = clsProperties.user_id
                        .clsProperties.user_code = clsProperties.user_code
                        .clsProperties.salesrep_code = "" 'Trim(clsProperties.liSR.Item(i).Value)
                        .clsProperties.salesrep_id = Trim(clsProperties.liSR.Item(i).Value) 'Trim(drRow("salesrep_id"))
                        .clsProperties.salesteam_id = 0 'Trim(drRow("salesteam_id"))
                        .clsProperties.created_date = clsProperties.created_date
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .clsProperties.changed_date = clsProperties.changed_date
                        .clsProperties.changed_user_id = clsProperties.changed_user_id
                        .clsProperties.delete_flag = clsProperties.delete_flag
                        .Create()
                    Next
                End With
            End If
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.UpdateUserSalesRep" & ex.ToString))
        Finally
            objUserSalesRep = Nothing
            'objUserAccessRight = Nothing
            'objDB = Nothing
        End Try
    End Sub
    Public Sub UpdateUserSalesRepGrp()
        'Dim objDB As cor_DB.clsDB
        'Dim objUserAccessRight As clsUserAccessRight
        Dim objUserSalesRepGrp As clsUserSalesRepGrp
        Dim i As Integer = 0
        Dim y As Integer = 0

        Try
            'Update UserSalesRep
            If Not IsNothing(clsProperties.liSRG) Then
                objUserSalesRepGrp = New clsUserSalesRepGrp
                With objUserSalesRepGrp
                    .clsProperties.user_id = clsProperties.user_id
                    '.clsProperties.salesrepgrp_id = Trim(clsProperties.liSRG.Item(i).Value)
                    .clsProperties.changed_user_id = clsProperties.changed_user_id
                    .Delete()

                    For i = 0 To clsProperties.liSRG.Count - 1
                        .clsProperties.user_id = clsProperties.user_id
                        .clsProperties.salesrepgrp_id = Trim(clsProperties.liSRG.Item(i).Value)
                        .clsProperties.creator_user_id = clsProperties.creator_user_id
                        .Create()
                    Next
                End With
            End If
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.UpdateUserSalesRepGrp" & ex.ToString))
        Finally
            objUserSalesRepGrp = Nothing
            'objUserAccessRight = Nothing
            'objDB = Nothing
        End Try
    End Sub
    Public Sub LoginUpdate()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERLOGINUPDATE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("login_cnt", clsProperties.login_cnt, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.LoginUpdate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub PwdUpdate()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERPWDUPDATE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd", clsProperties.pwd, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.PwdUpdate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub ProfileUpdate()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERPROFILEUPDATE"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("pwd_flag", clsProperties.pwd_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd", clsProperties.pwd, cor_DB.clsDB.DataType.DBString)
                .addItem("user_name", clsProperties.user_name, cor_DB.clsDB.DataType.DBString)
                .addItem("email", clsProperties.email, cor_DB.clsDB.DataType.DBString)
                .addItem("page_size", clsProperties.page_size, cor_DB.clsDB.DataType.DBInt)
                .addItem("pwd_changed_date", clsProperties.pwd_changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.ProfileUpdate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#End Region
#Region "Audit"
    'HL:20070904: AUDIT LOG IN DATE **********************
    Public Function AuditLogin() As Integer
        Try
            Dim conn As New SqlConnection(strEchoplusConn)
            conn.Open()

            Dim myCommand As New SqlCommand("SPP_ADM_AUDITLOGIN", conn)
            myCommand.CommandType = CommandType.StoredProcedure

            myCommand.Parameters.AddWithValue("@user_id", clsProperties.user_id)
            Dim audit_id As New SqlParameter("@audit_id", SqlDbType.Int)
            audit_id.Direction = ParameterDirection.Output
            myCommand.Parameters.Add(audit_id)
            myCommand.ExecuteNonQuery()

            Return audit_id.Value

            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.AuditLogin" & ex.ToString))
        Finally

        End Try
    End Function
    'HL:20070905: AUDIT LOG OUT DATE **********************
    Public Sub AuditLogout(ByVal audit_id As Integer)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_AUDITLOGOUT"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBInt)
                .addItem("audit_id", audit_id, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

            Exit Sub
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.AuditLogout" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    'HL:20070905: AUDIT DTL **********************
    Public Sub AuditDtl(ByVal audit_id As Integer, ByVal submodule_id As Integer)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_AUDITDTL"
                .addItem("audit_id", audit_id, cor_DB.clsDB.DataType.DBInt)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBInt)
                .addItem("submodule_id", submodule_id, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

            Exit Sub
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.AuditDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    'HL:20090714: AUDIT LOG **********************
    Public Sub AuditLog(ByVal strUserID As Integer, ByVal strTitleName As String, ByVal strFilePath As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_AUDIT_LOG"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBInt)
                .addItem("TITLE_NAME", strTitleName, cor_DB.clsDB.DataType.DBString)
                .addItem("FILE_PATH", strFilePath, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

            Exit Sub
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUser.AuditDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#End Region

#Region "User Login"
    Public Sub UserLoginKeyAdd(ByVal strUserID As String, ByVal strLoginID As String, ByVal strUserKey As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_LOGIN_KEY_ADD"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("LOGIN_ID", strLoginID, cor_DB.clsDB.DataType.DBString)
                .addItem("SESSION_KEY", strUserKey, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
            Exit Sub
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub UserLoginKeyUpdate(ByVal strUserID As String, ByVal strLoginID As String, ByVal strUserKey As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_LOGIN_KEY_UPD"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("LOGIN_ID", strLoginID, cor_DB.clsDB.DataType.DBString)
                .addItem("SESSION_KEY", strUserKey, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
            Exit Sub
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function UserLoginKeyCheck(ByVal strUserID As String, ByVal strLoginID As String, ByVal strUserKey As String) As String
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_LOGIN_KEY_CHECK"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("LOGIN_ID", strLoginID, cor_DB.clsDB.DataType.DBString)
                .addItem("SESSION_KEY", strUserKey, cor_DB.clsDB.DataType.DBString)
                UserLoginKeyCheck = .spRetrieveValue
            End With
            Exit Function
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region
#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
