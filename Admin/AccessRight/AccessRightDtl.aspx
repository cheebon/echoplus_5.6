<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccessRightDtl.aspx.vb" Inherits="Admin_AccessRight_AccessRightDtl" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Access Right Dtails</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->  
<body class="BckgroundInsideContentLayout">
    <form id="frmAccessRightDtl" method="post" runat="server">
    <div>  
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>
							                        <td width="20%"><asp:label id="lblName" runat="server" CssClass="cls_label_header">Name</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblNameValue" runat="server" CssClass="cls_label" /></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblName1" runat="server" CssClass="cls_label_header">Name 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot2" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblName1Value" runat="server" CssClass="cls_label" /></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblDesc" runat="server" CssClass="cls_label_header">Description</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblDescValue" runat="server" CssClass="cls_label" /></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblDesc1" runat="server" CssClass="cls_label_header">Description 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblDesc1Value" runat="server" CssClass="cls_label" /></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCountry" runat="server" CssClass="cls_label_header">Country</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblCountryValue" runat="server" CssClass="cls_label" /></td>
							                    </tr>
							                    <tr>
						                            <td width="20%"><asp:label id="lblPrincipal" runat="server" CssClass="cls_label_header">Principal</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblPrincipalValue" runat="server" CssClass="cls_label" /></td>
						                        </tr>	
						                    </table>
				                        </td>
			                        </tr>
			                        <tr><td>&nbsp;</td></tr>
			                        <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>					                                
					                                <td vAlign="top" width="50%">
					                                     <ccGV:clsGridView ID="dgARList" runat="server" ShowFooter="false" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                            FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="submodule_id">
                                                            <Columns>
                                                                <asp:BoundField DataField="module_id" HeaderText="Module ID" ReadOnly="True" SortExpression="module_id" visible="false">
                                                                    <headerstyle />
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>   
                                                                <asp:BoundField DataField="submodule_id" HeaderText="Sub Module ID" ReadOnly="True" SortExpression="submodule_id" visible="false">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>                                                                   
											                    <asp:BoundField DataField="module_name" HeaderText="Module">
												                     <itemstyle horizontalalign="Left" />
											                    </asp:BoundField>
											                    <asp:BoundField DataField="submodule_name" HeaderText="Sub Module">
												                      <itemstyle horizontalalign="Left" />
											                    </asp:BoundField>
											                    <asp:BoundField DataField="action_string" HeaderText="Action">
												                      <itemstyle horizontalalign="Left" />
											                    </asp:BoundField>
											                    <asp:BoundField Visible="False" DataField="action_id" HeaderText="action_id"></asp:BoundField>
											                    <asp:BoundField Visible="False" HeaderText="accessrightdtl_id"></asp:BoundField>
											                    <asp:BoundField Visible="False" DataField="country_id" HeaderText="country_id"></asp:BoundField>
											                    <asp:BoundField Visible="False" DataField="principal_id" HeaderText="principal_id"></asp:BoundField>
                                                            </Columns>                                                
                                                        </ccGV:clsGridView>			
					                                </td>					                                
					                            </tr> 
					                        </table> 
					                    </td> 
					                </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
				                             <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
				                            <asp:TextBox ID="txtAccessRightID" CssClass="cls_textbox" runat="server" Visible=false></asp:TextBox>
				                            <asp:button id="btnSetEL" runat="server" Text="Set Easy Loader" CssClass="cls_button" Visible="false" CausesValidation="False"></asp:button>
				                            <asp:button id="btnSetDF" runat="server" Text="Set Data File" CssClass="cls_button" Visible="false" CausesValidation="False"></asp:button>
					                        <asp:button id="btnViewTray" runat="server" Text="View Tray" CssClass="cls_button"></asp:button>
					                        <asp:button id="btnDelete" runat="server" Text="Delete" CssClass="cls_button"></asp:button>
					                        <asp:button id="btnEdit" runat="server" CssClass="cls_button" Text="Edit"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
    </div>
    </form>
</body>
</html>
