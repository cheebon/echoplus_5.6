Namespace Echoplus

    Partial Class wuc_lblHeader
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private strTitle As String

        Public Property Title() As String
            Get
                Return strTitle
            End Get
            Set(ByVal Value As String)
                strTitle = Value
            End Set
        End Property

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblTitle.Text = strTitle

                If Not IsPostBack Then
                    If strTitle <> "Quick Link" Then
                        Dim clsUser As adm_User.clsUser
                        clsUser = New adm_User.clsUser
                        clsUser.AuditLog(Session("UserID"), strTitle, Page.AppRelativeVirtualPath)
                    End If
                End If

            Catch ex As Exception

            End Try
        End Sub
    End Class

End Namespace
