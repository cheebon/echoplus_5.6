﻿/***************************************************************
/ This two functions have the following pre-requisite on the 
/ caller page.
/ 1. A gmap by the id 'map_canvas'
/ 2. A div by the id 'Message'
/ 3. The 'Message' div z-index must be set, so that it can be seen. Current value used is 30.
/ 
/ Based on Messager.js. The only difference is this one are for message div that's inside a popup div.
***************************************************************/
var timer;
var mapOffset;// = $("#map_canvas").offset();

function ShowMessage(msg)
{   
    var mapOffset = $("#map_canvas").position();
    if (mapOffset) {
        $("#Message").css({ top: mapOffset.top, left: mapOffset.left });
    } 
//    

    if(timer)
    {
        $("#Message").text(msg);
        clearTimeout(timer);
        timer = null;
    }
    else
    {
        $("#Message").text(msg).slideDown();  
    }
    
    timer = setTimeout("HideMessage()", 10000);
}

function HideMessage()
{
    $("#Message").slideUp();
    
    if(timer)
    {
        clearTimeout(timer);
        timer = null;
    }
}