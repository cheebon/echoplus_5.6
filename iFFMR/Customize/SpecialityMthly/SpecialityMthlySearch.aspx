<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SpecialityMthlySearch.aspx.vb" Inherits="iFFMR_Customize_SpecialityMthly_SpecialityMthlySearch" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Speciality Mthly Search</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
function RefreshContentBar()
{var TEAM_CODE = "";
var listbox = document.getElementById("lsbSelectedTeam");
for(var i=0;i<listbox.options.length;i++){if(listbox.options[i].selected = true) if (i > 0){TEAM_CODE = TEAM_CODE + ",";} TEAM_CODE = TEAM_CODE + listbox.options[i].value ;}
var SALESREP_CODE = "";
var listboxSR = document.getElementById("lsbSelectedSalesrep");
for(var i=0;i<listboxSR.options.length;i++){if(listboxSR.options[i].selected = true) if (i > 0){SALESREP_CODE = SALESREP_CODE + ",";} SALESREP_CODE = SALESREP_CODE + listboxSR.options[i].value ;}
var ddlyear = document.getElementById("ddlyear");var YEAR = ddlyear.value;
var ddlymonth = document.getElementById("ddlmonth");var MONTH = ddlymonth.value;
var ddlptlcode = document.getElementById("ddlptlcode");var PTL_CODE = ddlptlcode.value;
parent.document.getElementById('ContentBarIframe').src="../../iFFMR/Customize/SpecialityMthly/SpecialityMthlyGrid.aspx?TEAM_CODE=" + TEAM_CODE + "&YEAR=" + YEAR+ "&MONTH=" + MONTH + "&PTL_CODE=" + PTL_CODE + "&SALESREP_CODE=" + SALESREP_CODE;
}
    </script>
</head>
<body class="BckgroundInsideContentLayout">
   <form id="frmSpecialityMthlySearch" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport"
                            align="left">
                            <tr align="left">
                                <td align="left">
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <table align="left">
                                        <tr>
                                            <td valign="top" align="center" style="width: 45%;">
                                                <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                                <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                            </td>
                                            <td valign="middle" align="center" style="width: 10%;">
                                                <table>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35"
                                                                Text="<<" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center" style="width: 35%">
                                                <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                                <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                            </td>
                                        </tr>
                                           <tr>
                            <td valign="top" align="center">
                                <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Single"
                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center">
                                <table>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                Text="<" />
                                        </td>
                                    </tr>
                                   <%-- <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                Text=">>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                Text="<<" />
                                        </td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td align="center">
                                <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                            </td>
                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td>
                                                <span class="cls_label_header">Year</span></td>
                                            <td>
                                                <span class="cls_label_header">:</span></td>
                                            <td>
                                                <asp:DropDownList ID="ddlyear" runat="server" CssClass="cls_dropdownlist" /></td>
                                      
                                            <td>
                                                <span class="cls_label_header">Month</span></td>
                                            <td>
                                                <span class="cls_label_header">:</span></td>
                                            <td>
                                                <asp:DropDownList ID="ddlmonth" runat="server" CssClass="cls_dropdownlist">
                                                    <asp:ListItem Text="Jan" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Feb" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Mar" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Apr" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="May" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="Jun" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="Jul" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="Aug" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="Sep" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="Oct" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="Nov" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="Dec" Value="12"></asp:ListItem>
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label_header">Product</span></td>
                                            <td>
                                                <span class="cls_label_header">:</span></td>
                                            <td>
                                                <asp:DropDownList ID="ddlptlCode" runat="server" CssClass="cls_dropdownlist" /></td>
                                             <td colspan="3"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                           
                            <tr>
                                <td colspan="3" align="left">
                                    <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                    <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                        value="Search" class="cls_button" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
