<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_pnlRecordNotFound.ascx.vb" Inherits="include_wuc_pnlRecordNotFound" %>
<asp:Panel ID="Panel_RecordNotFound" runat="server" Visible="False" Width="100%">
    <br />
    <table width="100%">
        <tr>
            <td >
                <div style="font-weight: bold; font-size: large; width: 100%; color: white; background-color: #000066;
                    text-align: center">
                    Record(s) Not Found</div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="There is no information based on parameter entered."
                    CssClass="cls_label_header"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>
