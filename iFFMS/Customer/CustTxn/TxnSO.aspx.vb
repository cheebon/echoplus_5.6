Imports System.Data
Imports txn_WebActy

Partial Class iFFMS_Customer_CustTxn_TxnSO
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property BONUS_MODIFCATION() As Boolean
        Get
            Return CInt(ViewState("BONUS_MODIFCATION"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("BONUS_MODIFCATION") = value
        End Set
    End Property

    Private Property SO_SUBMIT_AMT() As Boolean
        Get
            Return CInt(ViewState("SO_SUBMIT_AMT"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("SO_SUBMIT_AMT") = value
        End Set
    End Property

    Private Property SO_POISON_IND() As Boolean
        Get
            Return CInt(ViewState("SO_POISON_IND"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("SO_POISON_IND") = value
        End Set
    End Property

    Public Enum dgcol As Integer
        LINE_NO = 2
        TXN_NO = 3
        PRD_CODE = 4
        PRD_NAME = 5
        LINE_TYPE = 6
        LINE_TYPE_NAME = 7
        UOM_CODE = 8
        QTY = 9
        LIST_PRICE = 10
        AMT = 11
    End Enum
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustTxnSO"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
     
        LoadTxnDescName()

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
            txtdeldate.Attributes.Add("onblur", "formatDate('" & txtdeldate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            txtpaydate.Attributes.Add("onblur", "formatDate('" & txtpaydate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            rfvCheckDataType.ErrorMessage = " Invalid Date!(" & "yyyy-MM-dd" & ")"
            'ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "CommonJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/JSCommon.js")

            BONUS_MODIFCATION = GetConfigValue("BONUS_MODIFCATION")
            SO_SUBMIT_AMT = GetConfigValue("SO_SUBMIT_AMT")
            SO_POISON_IND = GetConfigValue("SO_POISON_IND")
            TimerControl2.Enabled = True

        End If
        lblErr.Text = ""
        CVtxtdeldatedate.DataBind()
        cvtxtpaydatedate.DataBind()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        InsertSOHdr()
    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        If TimerControl2.Enabled Then

            If Request.QueryString("salesrepcode") IsNot Nothing Then
                LoadTxnNo()
                LoadddlShipTo(Trim(Request.QueryString("SalesrepCode")), Trim(Request.QueryString("CustCode")))
                LoadSOSummHdr()
            End If
        End If
        TimerControl2.Enabled = False
    End Sub
 
#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        dtCurrentTable = GetRecList()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'btncheckall.Visible = False
                ' btnuncheckall.Visible = False
                btndeletesum.Visible = False
                Master_Row_Count = 0
            Else
                ' btncheckall.Visible = True
                ' btnuncheckall.Visible = True
                btndeletesum.Visible = True
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgList
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With


        UpdateDatagrid_Update()

    End Sub

    Private Function GetRecList() As DataTable
        Try
            Dim DT As DataTable = Nothing
            If Not String.IsNullOrEmpty(Session("UserID")) Then
                Dim strTXNNo As String, strUserid As String, strsessionid As String
                strTXNNo = lbltxnno.Text
                strUserid = Web.HttpContext.Current.Session("UserID")
                strsessionid = Request.QueryString("sessionid")

                Dim clsSO As New txn_WebActy.clsSO
                DT = clsSO.GetSOSumDtl(strsessionid, strTXNNo, strUserid)

            End If

            Return DT
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Public Sub UpdateDatagrid_Update()

        UpPnlDtlSum.Update()
        UpdatePage.Update()
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        If BONUS_MODIFCATION = False And sender.datakeys(e.Row.RowIndex).item("FOC_IND") = 1 Then
                            Dim chkdelete As CheckBox = CType(e.Row.FindControl("chkdelete"), CheckBox)
                            chkdelete.Enabled = False
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strTxnCode As String = sender.datakeys(e.NewEditIndex).item("TXN_NO")
            Dim strLineNo As String = sender.datakeys(e.NewEditIndex).item("LINE_NO")
            Dim strUserId As String = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            Dim strsessionid As String = Trim(Request.QueryString("sessionid"))
            Dim strFOCInd As String = sender.datakeys(e.NewEditIndex).item("FOC_IND")

            If BONUS_MODIFCATION = False And strFOCInd = 1 Then

                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Bonus item are not editable!')", True)

            Else
                Dim dtEdit As DataTable
                Dim clsSO As New txn_WebActy.clsSO
                dtEdit = clsSO.GetTMPSODtlEdit(strTxnCode, strLineNo, strUserId, strsessionid)

                lbllineno.Text = dtEdit.Rows(0)("LINE_NO")
                txtPrdCode.Text = dtEdit.Rows(0)("PRD_CODE")
                txtPrdName.Text = dtEdit.Rows(0)("PRD_NAME")
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LoadddlUOM", "bindDDLUOM();", True)
                LoadddlUOM(dtEdit.Rows(0)("PRD_CODE"))
                txtselecteduom.Text = dtEdit.Rows(0)("UOM_CODE")
                Dim lstItem As ListItem = ddluom.Items.FindByText(Trim(dtEdit.Rows(0)("UOM_CODE")))
                If lstItem IsNot Nothing Then
                    ddluom.SelectedIndex = -1
                    lstItem.Selected = True
                End If
                lbluomprice.Text = dtEdit.Rows(0)("UOM_PRICE")
                LoadddlLineType(Trim(Request.QueryString("salesrepcode")))

                Dim lstItemlinetype As ListItem = ddllinetype.Items.FindByValue(Trim(dtEdit.Rows(0)("LINE_TYPE")))
                If lstItemlinetype IsNot Nothing Then
                    ddllinetype.SelectedIndex = -1
                    lstItemlinetype.Selected = True
                End If

                txtqty.Text = dtEdit.Rows(0)("QTY")
                lblamount.Text = dtEdit.Rows(0)("AMT")
                cpesoDtl.Collapsed = False
                pSoDtl.Visible = True

                UpdPnlDtl.Update()
                UpdatePage.Update()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "EVENT HANDLER"

    Protected Sub btnsavedtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavedtl.Click
        If Page.IsValid Then
            If CheckPoisonInd() Then
                If CInt(lbllineno.Text) = 0 Then
                    'InsertSOHdr()
                    InsertSODtl()
                Else
                    UpdateSODtl()
                    CreateDtlNewRecord()
                End If

                RefreshDatabinding()
                ResetPrdGrpSearch()
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('Cannot bundle poisonous item and non-poisonous item together. Kindly select other items.')", True)
            End If

        End If
        UpdatePage.Update()
    End Sub

    Protected Sub rdopayby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdopayby.SelectedIndexChanged
        SelectionPayby()
        UpdatePage.Update()
    End Sub

    Protected Sub btndeletesum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeletesum.Click
        DelSOSumDtl()
        RefreshDatabinding()
        ResetPrdGrpSearch()
        UpdatePage.Update()
    End Sub

    Protected Sub btnresetdtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnresetdtl.Click
        ' CreateHdrNewRecord()
        CreateDtlNewRecord()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            If CheckAllPoisonInd() Then
                SubmitTransaction()
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('There are a mix of poisonous items and non-poisonous items in order. Kindly select either one type of items only.')", True)
            End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnProceeed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceeed.Click
        Try
            
            SubmitTransaction()

        Catch ex As Exception

            ExceptionMsg("btnsubmit_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

#End Region

#Region "Product Function"
    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        With (wuc_Prd_PrgGrpSearch)
            .ResetPage()
            .BindDefault()
            .Show()
        End With
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Prd_PrgGrpSearch.SelectButton_Click
        Try

            Dim txtProductCode As TextBox = txtPrdCode
            txtProductCode.Text = wuc_Prd_PrgGrpSearch.PrdCode
            Dim txtProductName As TextBox = txtPrdName
            txtProductName.Text = wuc_Prd_PrgGrpSearch.PrdName

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LoadddlUOM", "setTimeout('bindDDLUOM();', 50);", True)
            'LoadddlUOM(wuc_Prd_PrgGrpSearch.PrdCode)
            'LoadddlUOMPrice()
            UPHdr.Update()
            UpdPnlDtl.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Prd_PrgGrpSearch.CloseButton_Click
        ResetPrdGrpSearch()
    End Sub

    Private Sub ResetPrdGrpSearch()
        wuc_Prd_PrgGrpSearch.ResetPage()
        wuc_Prd_PrgGrpSearch.BindDefault()
        wuc_Prd_PrgGrpSearch.Hide()
    End Sub

#End Region

#Region "Header Function"
    Protected Sub SelectionPayby()
        Try

            If rdopayby.SelectedValue.ToUpper = "DEFAULT" Then
                txtpaydate.Text = ""
                txtpaydate.Visible = False
                imgpaydate.Visible = False
                txtpaydate.CausesValidation = False
                lblpaydate.Visible = False
                cvtxtpaydate.Enabled = False
                cvtxtpaydate2.Enabled = False


                ddltermcode.Visible = False
                ddltermcode.SelectedIndex = 0
                ddltermcode.CausesValidation = False
                lblpayterm.Visible = False
                rfvddltermcode.Enabled = False


            ElseIf rdopayby.SelectedValue.ToUpper = "DATE" Then
                txtpaydate.Visible = True
                imgpaydate.Visible = True
                txtpaydate.CausesValidation = True
                lblpaydate.Visible = True

                ddltermcode.Visible = False
                ddltermcode.CausesValidation = False
                lblpayterm.Visible = False
                rfvddltermcode.Enabled = False

            ElseIf rdopayby.SelectedValue.ToUpper = "TERM" Then
                ddltermcode.Visible = True
                ddltermcode.CausesValidation = True
                lblpayterm.Visible = True
                LoadddlPymtTerm(Trim(Request.QueryString("SalesrepCode")), Trim(Request.QueryString("custcode")))

                txtpaydate.Visible = False
                imgpaydate.Visible = False
                txtpaydate.CausesValidation = False
                lblpaydate.Visible = False
                cvtxtpaydate.Enabled = False
                cvtxtpaydate2.Enabled = False

            End If
            UpdatePage.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadTxnDescName()
        Try
            Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strvisitid As String, strMapPath As String

            strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))
            strvisitid = Trim(Request.QueryString("visitid"))
            strMapPath = "Ondemand > New Transaction > Sales Order"

            Dim dt As DataTable
            Dim clstxncommon As New txn_common.clstxncommon
            dt = clstxncommon.GetTxnDescName(strSalesrepCode, strCustCode, strContCode)

            If dt.Rows.Count > 0 Then
                lblFieldForceName.Text = Trim(dt.Rows(0)("SALESREP_NAME"))
                lblCustName.Text = Trim(dt.Rows(0)("CUST_NAME"))
                lblContName.Text = Trim(dt.Rows(0)("CONT_NAME"))
                lblVisitId.Text = strvisitid

                lblMapPath.Text = strMapPath
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub CreateHdrNewRecord()
        Try
            txtpaydate.Visible = False
            imgpaydate.Visible = False
            lblpaydate.Visible = False
            ddltermcode.Visible = False
            lblpayterm.Visible = False

            ddlshiptocode.SelectedIndex = 0
            txtshiptoadd.Text = ""
            rdopayby.SelectedIndex = 0
            SelectionPayby()

            txtdeldate.Text = GetDlvyDate()


            txtpono.Text = ""
            txtrmks.Text = ""

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "GetShipToAddress", "GetShipToAddress();", True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetDlvyDate() As String
        Try
            Dim strDlvyDAte As String = Nothing
            Dim strTxnNo As String = Trim(Request.QueryString("TxnNo"))
            If IsNothing(strTxnNo) Or strTxnNo = "" Then
                Dim DT As DataTable
                Dim clsSO As New txn_WebActy.clsSO

                DT = clsSO.GetDlvyDate(Trim(Request.QueryString("salesrepcode")), Trim(Web.HttpContext.Current.Session("UserID")))
                strDlvyDAte = IIf(IsDBNull(DT.Rows(0)("DLVY_DATE")), "", DT.Rows(0)("DLVY_DATE"))
            Else
                strDlvyDAte = Nothing
            End If
            Return strDlvyDAte
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Sub LoadTxnNo()
        Try
            Dim strTxnNo As String = Trim(Request.QueryString("TxnNo"))
            If IsNothing(strTxnNo) Or strTxnNo = "" Then
                Dim DTTXNno As DataTable
                Dim clsSO As New txn_WebActy.clsSO

                DTTXNno = clsSO.GetTRANo(Trim(Request.QueryString("salesrepcode")))
                lbltxnno.Text = DTTXNno.Rows(0)("TXN_NO")

            Else
                lbltxnno.Text = strTxnNo
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlShipTo(ByVal strSalesrepCode As String, ByVal strCustCode As String)
        Try
            Dim dtshiptocode As DataTable
            Dim clsSO As New txn_WebActy.clsSO

            dtshiptocode = clsSO.GetShipToCode(strSalesrepCode, strCustCode)
            With ddlshiptocode
                .Items.Clear()
                .DataSource = dtshiptocode.DefaultView
                .DataTextField = "SHIPTO_NAME"
                .DataValueField = "SHIPTO_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlPymtTerm(ByVal strSalesrepCode As String, ByVal strCustCode As String)
        Try
            Dim dtPymtTerm As DataTable
            Dim clsSO As New txn_WebActy.clsSO

            dtPymtTerm = clsSO.GetPymtTerm(strSalesrepCode, strCustCode)
            With ddltermcode
                .Items.Clear()
                .DataSource = dtPymtTerm.DefaultView
                .DataTextField = "PAY_TERM_NAME"
                .DataValueField = "PAY_TERM_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub InsertSOHdr()
        Try
            Dim strSessionId As String, strTxnNo As String, _
            strVisitId As String, strSalesrepCode As String, strCustCode As String, strContCode As String, _
            strShipToCode As String, strShipToDate As String, strPONO As String, strPayTermCode As String, _
            strPayDate As String, strPartialDlvy As String, strRemarks As String, strUserId As String, strPayBy As String, strTxnDate As String

            strSessionId = Request.QueryString("sessionid")
            strTxnNo = lbltxnno.Text
            strVisitId = Request.QueryString("visitid")
            strSalesrepCode = Request.QueryString("salesrepcode")
            strCustCode = Request.QueryString("custcode")
            strContCode = Request.QueryString("contcode")
            strUserId = Web.HttpContext.Current.Session("UserID").ToString
            strTxnDate = Request.QueryString("datein") + " " + Request.QueryString("timein")

            strShipToCode = ddlshiptocode.SelectedValue.ToString
            strShipToDate = txtdeldate.Text
            strPONO = txtpono.Text
            strPayTermCode = ddltermcode.SelectedValue.ToString
            strPayDate = txtpaydate.Text
            strPartialDlvy = IIf(chkpartdel.Checked = True, 1, 0)
            strRemarks = txtrmks.Text
            strPayBy = rdopayby.SelectedValue.ToString

            Dim clsSO As New txn_WebActy.clsSO
            clsSO.InstTMPSOHeader(strSessionId, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContCode, _
            strShipToCode, strShipToDate, strPONO, strPayTermCode, strPayDate, strPartialDlvy, _
            strRemarks, strPayBy, strUserId, strTxnDate)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadSOSummHdr()
        Try
            Dim DT As DataTable
            Dim clsSO As New txn_WebActy.clsSO
            Dim strTXNNo As String, strUserid As String, strsessionid As String, strCustCode As String, strContCode As String
            strTXNNo = lbltxnno.Text
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strsessionid = Trim(Request.QueryString("sessionid"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))

            DT = clsSO.GetSOSumHdrs(strsessionid, strTXNNo, strUserid, strCustCode, strContCode)
            If DT.Rows.Count > 0 Then
                lbltxnno.Text = IIf(IsDBNull((DT.Rows(0)("TXN_NO"))), " ", DT.Rows(0)("TXN_NO"))
                txtpono.Text = DT.Rows(0)("PO_NO")
                ddlshiptocode.SelectedValue = DT.Rows(0)("SHIPTO_CODE")


                txtdeldate.Text = DT.Rows(0)("SHIPTO_DATE")
                txtshiptoadd.Text = DT.Rows(0)("SHIPTO_ADDRESS")
                If DT.Rows(0)("PARTIAL_DLVY") = 1 Then
                    chkpartdel.Checked = True
                End If

                'ddlpayby.SelectedValue = IIf(IsDBNull((DT.Rows(0)("PAYBY"))), " ", DT.Rows(0)("PAYBY"))
                Dim lstItem As ListItem = rdopayby.Items.FindByValue(Trim(IIf(IsDBNull((DT.Rows(0)("PAYBY"))), " ", DT.Rows(0)("PAYBY"))))
                If lstItem IsNot Nothing Then
                    rdopayby.SelectedIndex = -1
                    lstItem.Selected = True
                End If

                SelectionPayby()
                txtpaydate.Text = IIf(IsDBNull((DT.Rows(0)("PAY_DATE"))), " ", DT.Rows(0)("PAY_DATE"))
                ddltermcode.SelectedValue = IIf(IsDBNull((DT.Rows(0)("PAY_TERM_CODE"))), " ", DT.Rows(0)("PAY_TERM_CODE"))
                txtrmks.Text = DT.Rows(0)("REMARKS")
                RefreshDatabinding()
                ResetPrdGrpSearch()
                CreateDtlNewRecord()
            Else
                CreateHdrNewRecord()
                CreateDtlNewRecord()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub SubmitOrd()
        Try
            Dim SumOfOrder As Double = 0
            SumOfOrder = GetSumOfOrder()


            If SO_SUBMIT_AMT = False And SumOfOrder = 0 Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Order with zero amount are not allowed!')", True)
            Else
                rfvprdcode.IsValid = True
                rfvddllinetype.IsValid = True
                cvtxtqty.IsValid = True
                cvtxtqty2.IsValid = True
                rfvtxtqty.IsValid = True
                rfvddluom.IsValid = True

                If Page.IsValid Then
                    If dgList.Rows.Count > 0 Then
                        InsertSOHdr()
                        Dim strSessionId As String, strVisitID As String, strSalesrepCode As String, strTxnNo As String, strCustCode As String, strContCode As String
                        strSessionId = Trim(Request.QueryString("sessionid"))
                        strVisitID = Trim(Request.QueryString("visitid"))
                        strSalesrepCode = Trim(Session("SALESREP_CODE"))
                        strCustCode = Trim(Request.QueryString("CustCode"))
                        strContCode = Trim(Request.QueryString("ContCode"))
                        strTxnNo = Trim(lbltxnno.Text)

                        Dim strurl = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnSOSub.aspx?sessionid=" + strSessionId + "&visitid=" + strVisitID + "&salesrepcode=" + strSalesrepCode + "&txnno=" + strTxnNo + "&custcode=" + strCustCode + "&contcode=" + strContCode + "';"
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "HideElement('SubDetailBar');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Kindly add at least one detail record!')", True)

                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetConfigValue(ByVal strSettingCode As String) As Boolean
        Try
            Dim Value As Boolean
            Dim dt As DataTable
            Dim clstxncommon As New txn_WebActy.clsCommon
            Dim strUserId As String = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            dt = clstxncommon.GetAdmConfig(strSettingCode, strUserId)
            Value = IIf(dt.Rows(0)(0) = 1, True, False)
            Return Value
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Function GetSumOfOrder() As Double
        Try
            Dim SumOfOrder As Double = 0

            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        SumOfOrder = SumOfOrder + Trim(dgList.Rows(i).Cells(dgcol.AMT).Text)
                    End If

                    i += 1
                Next
            End If

            Return SumOfOrder
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Sub SubmitTransaction()
        Dim clstxncommon As New txn_WebActy.clsCommon
        Dim dt As DataTable
        If GetConfigValue("DENY_DEL_DATE_AFTER_11") Then 'Session("PRINCIPAL_ID") = "48"
            Dim deldate As Date
            deldate = txtdeldate.Text
            Dim todaydate As Date
            todaydate = Now.ToShortDateString
            Dim nowtime As DateTime
            nowtime = TimeOfDay.ToShortTimeString
            Dim limittime As DateTime
            dt = clstxncommon.GetAdmConfig("DENY_DEL_DATE_AFTER_11", Trim(Web.HttpContext.Current.Session("UserID").ToString))
            limittime = "#" + dt.Rows(0)(2) + "#" '"#11:00:00 PM#"
            If deldate = todaydate And nowtime >= limittime Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "Msgbox", "alert('Same day deliveries are not allowed for orders submitted after " + dt.Rows(0)(2) + "!')", True)
            Else
                SubmitOrd()
            End If

        Else
            SubmitOrd()
        End If


    End Sub
#End Region

#Region "Detail  Function"

    Private Sub CreateDtlNewRecord()
        Try
            cpesoDtl.Collapsed = False
            pSoDtl.Visible = True
            LoadddlLineType(Trim(Request.QueryString("salesrepcode")))
            txtPrdCode.Text = ""
            txtPrdName.Text = ""
            ddluom.Items.Clear()
            txtselecteduom.Text = ""
            lbluomprice.Text = 0
            txtqty.Text = ""
            lblamount.Text = 0
            lbllineno.Text = 0
            UpdPnlDtl.Update()
            UpdatePage.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlUOM(ByVal strPrdCode As String)
        Try
            Dim dtuom As DataTable
            Dim clsSO As New txn_WebActy.clsSO

            dtuom = clsSO.GetUOMCode(strPrdCode)
            With ddluom
                .Items.Clear()
                .DataSource = dtuom.DefaultView
                .DataTextField = "UOM_CODE"
                .DataValueField = "UOM_PRICE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlUOMPrice()
        Try


            Dim struomprice As Double
            Dim strSalesrepCode = Request.QueryString("SalesrepCode")
            Dim strCustCode = Request.QueryString("CustCode")
            Dim strContCode = Request.QueryString("ContCode")
            Dim clsSO As New txn_WebActy.clsSO
            struomprice = clsSO.GetUOMPrice(strSalesrepCode, strCustCode, strContCode, txtPrdCode.Text.ToString, ddluom.SelectedItem.Text, 1)
            If Not IsNothing(struomprice) Then
                lbluomprice.Text = struomprice
                txtselecteduom.Text = ddluom.SelectedItem.Text
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlLineType(ByVal strSalesrepCode As String)
        Try
            Dim clsSO As New txn_WebActy.clsSO
            Dim dtlinetype As DataTable = CType(ViewState("dtlinetype"), DataTable)
            If dtlinetype Is Nothing Then
                dtlinetype = clsSO.GetLineType(strSalesrepCode)
            Else
                ViewState("dtlinetype") = dtlinetype
            End If

            With ddllinetype
                .Items.Clear()
                .DataSource = dtlinetype.DefaultView
                .DataTextField = "LINE_TYPE_NAME"
                .DataValueField = "LINE_TYPE_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function CheckPoisonInd() As Boolean
        Try
            Dim strSessionId As String, _
            strTxnNo As String, strPrdCode As String, _
            strUserId As String

            Dim clsSO As New txn_WebActy.clsSO

            Dim dt As DataTable

            If SO_POISON_IND Then
                strSessionId = Trim(Request.QueryString("sessionid"))
                strTxnNo = lbltxnno.Text
                strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)
                strPrdCode = txtPrdCode.Text

                dt = clsSO.GetOrdPoisonCheck(strTxnNo, strPrdCode, strUserId, strSessionId)

                If IsNothing(dt) Then
                    Return False
                Else
                    If dt.Rows.Count = 0 Then
                        Return False
                    Else
                        If dt.Rows(0)("RETURN").ToString = "0" Then
                            Return False
                        Else
                            Return True
                        End If
                    End If
                End If
            Else
                Return True
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Function CheckAllPoisonInd() As Boolean
        Try
            Dim strSessionId As String, _
            strTxnNo As String, _
            strUserId As String

            Dim clsSO As New txn_WebActy.clsSO

            Dim dt As DataTable

            If SO_POISON_IND Then
                strSessionId = Trim(Request.QueryString("sessionid"))
                strTxnNo = lbltxnno.Text
                strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)


                dt = clsSO.GetOrdPoisonCheckAll(strTxnNo, strUserId, strSessionId)

                If IsNothing(dt) Then
                    Return False
                Else
                    If dt.Rows.Count = 0 Then
                        Return False
                    Else
                        If dt.Rows(0)("RETURN").ToString = "0" Then
                            Return False
                        Else
                            Return True
                        End If
                    End If
                End If
            Else
                Return True
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Sub InsertSODtl()
        Try
            Dim strSessionId As String, strSalesrepCode As String, strCustCode As String, strContCode As String, _
            strTxnNo As String, strPrdCode As String, strUomCode As String, strLineType As String, _
            strQty As Double, strUserId As String

            strSessionId = Trim(Request.QueryString("sessionid"))
            strTxnNo = lbltxnno.Text
            strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))
            strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)

            strPrdCode = txtPrdCode.Text
            strUomCode = txtselecteduom.Text 'ddluom.SelectedValue.ToString
            strLineType = ddllinetype.SelectedValue.ToString
            strQty = CDbl(txtqty.Text)

            If txtselecteduom.Text = "0" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('Kindly reselect UOM to continue!')", True)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "bindDDLUOM", "bindDDLUOM()", True)
            Else

                If ValidateSODtlExists(strPrdCode, strLineType) = True Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('The selected product and line type already exists. Kindly edit the transaction or select another product or line type!')", True)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "bindDDLUOM", "bindDDLUOM()", True)
                Else
                    Dim clsSO As New txn_WebActy.clsSO
                    clsSO.InstTMPSODetails(strSessionId, strSalesrepCode, strCustCode, strContCode, strTxnNo, strPrdCode, _
                    strUomCode, strLineType, strQty, strUserId)

                    CreateDtlNewRecord()
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successsfully added the transaction(s)!')", True)
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Function ValidateSODtlExists(ByVal strPrdCode As String, ByVal strLinetype As String) As Boolean
        Try
            If dgList.Rows.Count > 0 Then
                Dim strExistsPrdCode As String, strExistsLinetype As String, intExists As Boolean
                intExists = False
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows
                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        strExistsPrdCode = dgList.DataKeys(i).Item("PRD_CODE")
                        strExistsLinetype = dgList.DataKeys(i).Item("LINE_TYPE")

                        If strExistsPrdCode = strPrdCode And strExistsLinetype = strLinetype Then
                            intExists = True
                        End If
                    End If
                    i += 1
                Next

                Return intExists
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Sub UpdateSODtl()
        Try
            Dim strSessionId As String, strSalesrepCode As String, strCustCode As String, strContCode As String, _
            strTxnNo As String, strPrdCode As String, strUomCode As String, strLineType As String, _
            strQty As Double, strUserId As String, strLineNo As Integer

            strSessionId = Trim(Request.QueryString("sessionid"))
            strTxnNo = lbltxnno.Text
            strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))
            strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)

            strPrdCode = txtPrdCode.Text
            strUomCode = txtselecteduom.Text 'ddluom.SelectedValue.ToString
            strLineType = ddllinetype.SelectedValue.ToString
            strQty = CDbl(txtqty.Text)
            strLineNo = CInt(lbllineno.Text)

            Dim clsSO As New txn_WebActy.clsSO
            clsSO.UpdateTMPSODtl(strSessionId, strSalesrepCode, strCustCode, strContCode, strTxnNo, strPrdCode, _
            strUomCode, strLineType, strQty, strUserId, strLineNo)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successsfully updated the transaction(s)!')", True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelSOSumDtl()

        Try
            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strTxnNo As String, strUserId As String, strsessionid As String, strLineNo As String


                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dgList.Rows(i).FindControl("chkdelete"), CheckBox)

                        If chkdelete.Checked = True Then
                            strTxnNo = Trim(lbltxnno.Text)
                            strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
                            strsessionid = Trim(Request.QueryString("sessionid"))
                            strLineNo = Trim(dgList.DataKeys(i).Item("LINE_NO"))

                            Dim clsSO As New txn_WebActy.clsSO
                            clsSO.DelTMPSODetails(strsessionid, strTxnNo, strUserId, strLineNo)
                        End If

                    End If

                    i += 1
                Next

            End If

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "bindDDLUOM", "bindDDLUOM()", True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class

