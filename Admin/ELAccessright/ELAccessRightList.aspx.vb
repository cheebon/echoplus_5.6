﻿Imports System.Data
Imports Portal
Imports System.Threading
Imports System.Globalization


''' <summary>
'''  ************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-08-15
'''	Purpose	    :	Easy Loader Accessright List
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>

Partial Class Admin_ELAccessright_ELAccessRightList
    Inherits System.Web.UI.Page 
#Region "Local Variable "
    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property vsSortCol() As String
        Get
            If ViewState("SortCol") Is Nothing Then
                ViewState("SortCol") = ""
            End If
            Return ViewState("SortCol")
        End Get
        Set(ByVal value As String)
            ViewState("SortCol") = value
        End Set
    End Property

    Private Property vsSortDir() As String
        Get
            If ViewState("SortDir") Is Nothing Then
                ViewState("SortDir") = "ASC"
            End If
            Return ViewState("SortDir")
        End Get
        Set(ByVal value As String)
            ViewState("SortDir") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            UpdateErrorPanel()

            lblHeader.Title = "Easy Loader V2 Accessright"


            If Not Page.IsPostBack Then
                'lblHeader.Title = "Accessright List"


                Dim strSearchType As String = Trim(Request.QueryString("search_type"))
                Dim strSearchValue As String = Trim(Request.QueryString("search_value"))
                If strSearchValue <> "" Then
                    txtSearchValue.Text = strSearchValue
                End If
                ddlSearchType.SelectedValue = strSearchType

                hfDllVisibility.Value = IIf(ddlSearchType.SelectedValue = "all", "none", "inline")

                RefreshDataBind()

                btnCreate.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.Create)
                wuc_dgpaging.CurrentPageIndex = 0
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub RefreshDataBind()
        RefreshDataBinding()
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DGLIST - DATA BIND"
    Private Sub RefreshDataBinding(Optional ByVal blnExport As Boolean = False)
        Try
            dgList.SelectedRowStyle.Reset()

            Dim dtResult As DataTable
            dtResult = GetRecList(IIf(blnExport, 1, 0))

            If dtResult Is Nothing Then
                dtResult = New DataTable
            End If

            If dtResult.Rows.Count = 0 Then
                dtResult.Rows.Add(dtResult.NewRow)
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtResult.Rows(0)("TTL_ROWS")
                If String.IsNullOrEmpty(vsSortCol) Then
                    'vsSortCol = "user_name"
                    vsSortDir = "ASC"
                End If
            End If

            With dgList
                .AllowSorting = IIf(Master_Row_Count > 1, True, False)
                .DataSource = dtResult.DefaultView
                '.PageSize = Portal.UserSession.UserID 'Modify by Soo Fong 2017/02/16 - Too large hit error
                .DataBind()
            End With

            If Master_Row_Count = 0 Then
                dgList.Rows(0).Height = "15"
            End If

            If Not blnExport Then
                With wuc_dgpaging
                    ' .RecordCount = Master_Row_Count
                    '.PageSize = dgList.PageSize
                    .Visible = IIf(Master_Row_Count > 0, True, False)
                    .DataBind()
                End With
            End If

            For Each COL As DataControlField In dgList.Columns
                Select Case COL.HeaderText.Trim.ToUpper
                    Case "DELETE"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.Delete)
                    Case "EDIT"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.Edit)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateResultPanel()
        End Try
    End Sub

    Private Function GetRecList(ByVal intIsExport As Integer) As DataTable
        Dim clsARList As adm_ELAccessright.clsELAccessRightQuery
        Dim dt As DataTable = Nothing
        Try
            clsARList = New adm_ELAccessright.clsELAccessRightQuery

            With clsARList
                .clsQuery.search_type = ddlSearchType.SelectedValue.Trim
                .clsQuery.usr_id = Portal.UserSession.UserID
                Select Case ddlSearchType.SelectedIndex
                    Case 0
                    Case Else
                        .clsQuery.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                End Select

                dt = .GetAccessrightList(vsSortCol, vsSortDir, wuc_dgpaging.CurrentPageIndex + 1, Portal.UserSession.PageSize, intIsExport)

            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            clsARList = Nothing
        End Try

        Return dt
    End Function
#End Region

#Region "DGLIST - RENDER"
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim objAccessRight As adm_ELAccessright.clsELAccessRight
        Dim intIndex As Integer = 0
        Dim row As GridViewRow

        Try


            If Integer.TryParse(e.CommandArgument, intIndex) = True Then
                row = IIf(intIndex >= 0 AndAlso intIndex <= dgList.Rows.Count, dgList.Rows(intIndex), Nothing)

                Select Case e.CommandName
                    Case "Copy"
                        Response.Redirect("ELAccessRightCreate.aspx?accessright_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Delete"
                        objAccessRight = New adm_ELAccessright.clsELAccessRight
                        With objAccessRight
                            .clsPpt.accessright_id = Trim(dgList.DataKeys(row.RowIndex).Value)
                            .DeleteAR()
                        End With

                        Response.Redirect("ELAccessRightList.aspx?search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Details"
                        Response.Redirect("ELAccessRightDtl.aspx?accessright_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Edit"
                        Response.Redirect("ELAccessRightEdit.aspx?accessright_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&ori=edit&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim imgEdit, imgDelete, imgCopy As ImageButton
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btnDelete As ImageButton = CType(e.Row.Cells(3).Controls(0), ImageButton)
                If btnDelete IsNot Nothing Then
                    btnDelete.OnClientClick = "if (confirm('Are you sure you want to delete?') == false) { window.event.returnValue = false; return false; }"
                End If

                drv = CType(e.Row.DataItem, DataRowView)

                If IsDBNull(drv("ACCESSRIGHT_ID")) Then
                    'imgCopy = CType(e.Row.Cells(5).Controls(0), ImageButton)
                    imgDelete = CType(e.Row.Cells(3).Controls(0), ImageButton)
                    imgEdit = CType(e.Row.Cells(4).Controls(0), ImageButton)
                    'imgCopy.Visible = False
                    imgDelete.Visible = False
                    imgEdit.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting

    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        If vsSortCol IsNot Nothing AndAlso vsSortCol.Length > 0 Then
            If vsSortCol Like (e.SortExpression & "*") Then
                vsSortCol = e.SortExpression

                If vsSortDir = "DESC" Then
                    vsSortDir = "ASC"
                Else
                    vsSortDir = "DESC"
                End If
            Else
                vsSortCol = e.SortExpression
                vsSortDir = "ASC"
            End If
        Else
            vsSortCol = e.SortExpression
            vsSortDir = "ASC"
        End If

        RefreshDataBinding()
    End Sub
#End Region

#Region "Paging Control"
    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            wuc_dgpaging.CurrentPageIndex = CInt(wuc_dgpaging.PageNo - 1)
            RefreshDataBinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If wuc_dgpaging.CurrentPageIndex > 0 Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.CurrentPageIndex - 1
                wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1

                RefreshDataBinding()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If wuc_dgpaging.PageCount - 1 > wuc_dgpaging.CurrentPageIndex Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.CurrentPageIndex + 1
                wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1

                RefreshDataBinding()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Sub TOPFilter_Click(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.TOPFilter_Click
    '    Try
    '        RefreshDataBinding()
    '        Exit Sub

    '    Catch ex As Exception
    '        ExceptionMsg(ex)
    '    End Try
    'End Sub

    'Sub ddlPageSize_Click(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.ddlPageSize_Click
    '    Try
    '        RefreshDataBinding()
    '        Exit Sub

    '    Catch ex As Exception
    '        ExceptionMsg(ex)
    '    End Try
    'End Sub
#End Region

#Region "Update panels"
    Private Sub UpdateErrorPanel()
        updPnlErr.Update()
    End Sub
    Private Sub UpdateSearchPanel()
        updPnlSearch.Update()
    End Sub
    Private Sub UpdateResultPanel()
        updPnlResult.Update()
    End Sub
#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            Response.Redirect("ELAccessRightCreate.aspx?action=ADDNEW&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

End Class
