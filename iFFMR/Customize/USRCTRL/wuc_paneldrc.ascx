<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_paneldrc.ascx.vb" Inherits="iFFMR_Customize_USRCTRL_wuc_paneldrc" %>
<%@ Register TagPrefix="uc1" TagName="wuc_txtCalendarRange" Src="../../../include/wuc_txtCalendarRange.ascx"  %>


<asp:UpdatePanel ID="DRCUpdatePanel" runat="server">
<ContentTemplate>
<table border="0" cellpadding="0" cellspacing="2">
    <tr>
        <td><div class="cls_label_header">Sales Representative</div></td>
        <td><div class="cls_label_header">:</div></td>
        <td>
            <asp:DropDownList ID="ddlSalesRep" runat="server" Width="350px" CssClass="cls_dropdownlist" AutoPostBack="true">
            </asp:DropDownList></td>
    </tr>
    <tr valign="top">
        <td><div class="cls_label_header">Date</div></td>
        <td><div class="cls_label_header">:</div></td>
        <td><uc1:wuc_txtCalendarRange ID="wuc_txtCalendarRange" runat="server" EnableViewState="true" /></td>
    </tr>
    <tr valign="top">
        <td><div class="cls_label_header">Product</div></td>
        <td><div class="cls_label_header">:</div></td>
        <td>
            <%--<asp:ListBox ID="ddlPrd" runat="server" Height="150px" Width="350px" CssClass="cls_listbox"></asp:ListBox>--%>
            <asp:DropDownList ID="ddlPrd" runat="server" Width="350px" CssClass="cls_dropdownlist" AutoPostBack="false"></asp:DropDownList>
        </td>
    </tr>
    
</table>
</ContentTemplate>
</asp:UpdatePanel>