'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	03/04/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsBonus
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsBonus"
        End Get
    End Property

#Region "STANDARD BONUS"
    Public Function GetStandardBonusList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STANDARD_BONUS_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetStandardBonusDetails(ByVal strPrdCode As String, ByVal strQty As String, _
    ByVal strUomCode As String, ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STANDARD_BONUS_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateStandardBonus(ByVal strPrdCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STANDARD_BONUS_CREATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateStandardBonus(ByVal strPrdCode As String, ByVal strPKQty As String, ByVal strPKUOMCode As String, _
                                    ByVal strPKStartDate As String, ByVal strPKEndDate As String, ByVal strPKRegionCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Dim DT As DataTable = Nothing

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STANDARD_BONUS_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_QTY", strPKQty, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_UOM_CODE", strPKUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_START_DATE", strPKStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_END_DATE", strPKEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("PK_REGION_CODE", strPKRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                DT = .spRetrieve()
            End With
        Catch ex As Exception
            'Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

        Return DT
    End Function

    Public Sub DeleteStandardBonus(ByVal strPrdCode As String, ByVal strQty As String, _
    ByVal strUomCode As String, ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STANDARD_BONUS_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "SPECIAL BONUS"
    Public Function GetSpecialBonusList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SPECIAL_BONUS_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSpecialBonusDetails(ByVal strPrdCode As String, ByVal strCustCode As String, ByVal strQty As String, _
    ByVal strUomCode As String, ByVal strStartDate As String, ByVal strEndDate As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SPECIAL_BONUS_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateSpecialBonus(ByVal strPrdCode As String, ByVal strCustCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SPECIAL_BONUS_CREATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateSpecialBonus(ByVal strPrdCode As String, ByVal strCustCode As String, ByVal strPKQty As String, ByVal strPKUOMCode As String, _
                                    ByVal strPKStartDate As String, ByVal strPKEndDate As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Dim DT As DataTable = Nothing

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SPECIAL_BONUS_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBInt)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBInt)
                .addItem("PK_QTY", strPKQty, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_UOM_CODE", strPKUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_START_DATE", strPKStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_END_DATE", strPKEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                DT = .spRetrieve()
            End With
        Catch ex As Exception
            'Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

        Return DT
    End Function

    Public Sub DeleteSpecialBonus(ByVal strPrdCode As String, ByVal strCustCode As String, ByVal strQty As String, _
    ByVal strUomCode As String, ByVal strStartDate As String, ByVal strEndDate As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SPECIAL_BONUS_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "CUSTOMER GROUP BONUS"
    Public Function GetCustGrpBonusList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GRP_BONUS_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustGrpBonusDetails(ByVal strPrdCode As String, ByVal strCustGrpCode As String, ByVal strQTY As String, ByVal strUomCode As String, _
    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GRP_BONUS_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, clsDB.DataType.DBString)
                .addItem("QTY", strQTY, clsDB.DataType.DBDouble)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateCustGrpBonus(ByVal strPrdCode As String, ByVal strCustGrpCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GRP_BONUS_CREATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateCustGrpBonus(ByVal strPrdCode As String, ByVal strCustGrpCode As String, ByVal strPKQty As String, ByVal strPKUOMCode As String, _
                                    ByVal strPKStartDate As String, ByVal strPKEndDate As String, ByVal strPKRegionCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Dim DT As DataTable = Nothing

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GRP_BONUS_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_QTY", strPKQty, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_UOM_CODE", strPKUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_START_DATE", strPKStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_END_DATE", strPKEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("PK_REGION_CODE", strPKRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                DT = .spRetrieve()
            End With
        Catch ex As Exception
            'Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

        Return DT
    End Function

    Public Sub DeleteCustGrpBonus(ByVal strPrdCode As String, ByVal strCustGrpCode As String, ByVal strQTY As String, ByVal strUomCode As String, _
    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GRP_BONUS_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, clsDB.DataType.DBString)
                .addItem("QTY", strQTY, clsDB.DataType.DBDouble)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "PRICE GROUP BONUS"
    Public Function GetPriceGrpBonusList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRICE_GRP_BONUS_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPriceGrpBonusDetails(ByVal strPrdCode As String, ByVal strPriceGrpCode As String, ByVal strQty As String, ByVal strUomCode As String, _
    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRICE_GRP_BONUS_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("PRICE_GRP_CODE", strPriceGrpCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePriceGrpBonus(ByVal strPrdCode As String, ByVal strPriceGrpCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRICE_GRP_BONUS_CREATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRICE_GRP_CODE", strPriceGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdatePriceGrpBonus(ByVal strPrdCode As String, ByVal strPriceGrpCode As String, ByVal strPKQty As String, ByVal strPKUOMCode As String, _
                                    ByVal strPKStartDate As String, ByVal strPKEndDate As String, ByVal strPKRegionCode As String, ByVal strQty As String, ByVal strUOMCode As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String, _
                                    ByVal strFOCPrdCode As String, ByVal strFOCQty As String, ByVal strFOCUOMCode As String, _
                                    ByVal strFOCType As String, ByVal strMaxFOCQty As String) As DataTable
        Dim objDB As clsDB
        Dim DT As DataTable = Nothing

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRICE_GRP_BONUS_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRICE_GRP_CODE", strPriceGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_QTY", strPKQty, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_UOM_CODE", strPKUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_START_DATE", strPKStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_END_DATE", strPKEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("PK_REGION_CODE", strPKRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("FOC_PRD_CODE", strFOCPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_QTY", strFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_UOM_CODE", strFOCUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FOC_TYPE", strFOCType, cor_DB.clsDB.DataType.DBString)
                .addItem("MAX_FOC_QTY", strMaxFOCQty, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                DT = .spRetrieve()
            End With
        Catch ex As Exception
            'Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

        Return DT
    End Function

    Public Sub DeletePriceGrpBonus(ByVal strPrdCode As String, ByVal strPriceGrpCode As String, ByVal strQty As String, ByVal strUomCode As String, _
    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strRegionCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRICE_GRP_BONUS_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("PRICE_GRP_CODE", strPriceGrpCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class


