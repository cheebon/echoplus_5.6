﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustProfileMaintenanceGPS.aspx.vb" Inherits="iFFMA_SalesAccount_CustProfileMaintenance_CustProfileMaintenanceGPS" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Profile Maintenance - GPS</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>  
    <script src="../../../include/Gmap/Library/GenericMarker/GenericMarker.js" type="text/javascript"></script>
    <script src="../../../include/layout.js" type="text/javascript"></script>
    <script type="text/jscript">
        var map;
        var marker;
        
        var Default_Latitude, Default_Longitude, Default_Zoomlevel;

        function Initialize() {

            if (GBrowserIsCompatible()) {
                map = new GMap2(document.getElementById('map_canvas'));

                QueryLocation();

                AddControl();

            }
        }
        /**********************************************************************
        / Get default location based on principal
        /
        **********************************************************************/
        function QueryLocation() {
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {
            if (result) {
                if (result.Total > 0) {


                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;

                    
                    SetMapDefaultPoint();
                    DropMarker(Default_Latitude, Default_Longitude, Default_Zoomlevel);
                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {

                var point = new GLatLng(Default_Latitude, Default_Longitude);
                map.setCenter(point, parseInt(Default_Zoomlevel));
            }
        }
        function DropMarker(Lat, Lng, Zoom) {
            var point = new GLatLng(Lat, Lng);

            map.clearOverlays();
            marker = new GMarker(point, { draggable: true });
	        map.addOverlay(marker);
	
	        // add a drag listener to the map
	        GEvent.addListener(marker, "dragend", function() {
		        var point = marker.getPoint();
		        map.panTo(point);
		        $("#txtLatitude").val(point.lat());
		        $("#txtLongitude").val(point.lng());
		        
		        $("#hfLatitude").val(point.lat());
		        $("#hfLongitude").val(point.lng());
		    });

		    map.setCenter(point, parseInt(Zoom));
        }
        function AddControl() {
            map.enableScrollWheelZoom();
            
            var mapTypeControl = new GMapTypeControl();
            var LargeMapControl3D = new GLargeMapControl3D();
            var bottomLeft = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 10));
            var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 10));

            map.addControl(mapTypeControl, bottomRight);
            map.addControl(LargeMapControl3D, bottomLeft);
        }
        function CheckResize() {
            if (map) {
                alert(map);
                map.checkResize();
            }
        }
        function PreviewLocation() {
            var Lat = $("#txtLatitude").val();
            var Lng = $("#txtLongitude").val();

            if (Lat != "" && Lng != "") {
                if (!isNaN(Lat) && !isNaN(Lng)) {
                    DropMarker(Lat, Lng, 10);
                }
                else { alert("Latitude and Longitude must be numeric."); }
            }
            else { alert("Latitude and Longitude must be filled."); }
        }
        function InvokeRefresh() {
            var frame = window.parent.document.getElementById('ContentBarIframe');

            if (frame) {
                frame.contentWindow.ClientRefresh();
            }
        }
        function SearchByAddress() {
            var address, Geo;

            Geo = new GClientGeocoder();

            address = $("#txtAddress").val();

            if (address) {
                Geo.getLocations(address, ShowCoordinate);
            }
        }
        function ShowCoordinate(response) {
            //address = document.getElementById("txtAddress").value;

            if (!response || response.Status.code != 200) {
                alert("Address not found");
                //ShowMessage("Address not found");
            }
            else {
                place = response.Placemark[0];
               

                DropMarker(place.Point.coordinates[1], place.Point.coordinates[0], 10);

                $("#txtLatitude").val(place.Point.coordinates[1]);
                $("#txtLongitude").val(place.Point.coordinates[0]);
            }
        }
</script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="padding: 10px 0px 0px 10px; margin-right: 30px" class="BckgroundInsideContentLayout" onload="HideElement('ContentBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe');Initialize();" onunload="GUnload()">
    <form id="form1" runat="server">
     <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">
            <Services>
            <asp:ServiceReference
           path="~/DataServices/ws_PrincipalLoc.asmx" />
      </Services>
            </ajaxToolkit:ToolkitScriptManager>
            
    <asp:HiddenField ID="hfLatitude" runat="server" />
    <asp:HiddenField ID="hfLongitude" runat="server" />
    <asp:HiddenField ID="hfUpdateFlag" runat="server" Value="false" />    
            
        
        <div id="lblPopNotify_pnlMsgPopContentZone" class="Bckgroundreport" style="padding: 5px; width: 100%">   
            <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" />
            <div style="padding-left:10px">
                     <div style="width: 870px; padding: 5px" class="cls_label">
                         <asp:UpdatePanel ID="updPnlMaintenanceCustomerGPS" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="1000" OnTick="TimerControl1_Tick" />
                                <input type="button"  runat="server"  id="btnback" value="Back"  style="width:80px"
                                 onclick="ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');" class="cls_button" /><br /><br />
                                
                                <div style="width:430px; float:left">
                                    <div class="cls_label_header" style="width: 420px; float:left; background-color: #ffffdd; border: solid 1px #000000; padding: 5px">
                                        <customToolkit:wuc_lblInfo ID="lblErr" runat="server" /><br />
                                        <asp:Label ID="lblCustMsg" runat="server" Text="Label">Update Customer GPS Coordinate:</asp:Label> <br /><br />

                                        <div style="width:400px">
                                            <div class="cls_label" style="width:150px; float:left;">
                                                Latitude: 
                                            </div>
                                            <div style="width:100px; float:left;padding: 0px 0px 0px 10px">
                                                <asp:TextBox ID="txtLatitude" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox>
                                            </div> 
                                            <div class="cls_label" style="width:150px; float:left;">
                                                Longitude: 
                                            </div>
                                            <div style="width:100px; float:left;padding: 0px 0px 0px 10px">
                                                <asp:TextBox ID="txtLongitude" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox>
                                            </div>   
                                            <div style="width:400px">
                                                <asp:Label ID="lblValidInfo" runat="server" Text="" CssClass="cls_validator"></asp:Label>
                                            </div>
                                            <div style="width:400px">
                                                <input id="btnPreviewLoc" class="cls_button" type="button" value="Preview Location" onclick="PreviewLocation()" />
                                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="cls_button" />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="cls_label_header" style="margin-top: 10px;width: 420px; float:left; background-color: #ffffdd; border: solid 1px #000000; padding: 5px">
                                        Search by Address: <br /><br />
                                        
                                        <div class="cls_label">Address: </div> 
                                        <input id="txtAddress" type="text" class="cls_textbox" style="width: 200px" onkeypress="if(event.keyCode == 13){ event.cancelBubble = true; event.returnValue = false; SearchByAddress();}" />
                                        
                                        <input id="btnSearchAddress" type="button" value="Search" class="cls_button" onclick="SearchByAddress();"/>
                                    </div>
                                </div>
                            
                            </ContentTemplate>
                         </asp:UpdatePanel>
                           
                         <div style="width:400px; float:right;background-color: #ffffdd; border: solid 1px #000000; padding: 10px">
                              <div id="map_canvas" style="width: 100%; height: 350px; position: relative; padding: 0px 0px 0px 0px;z-index:20">
                              <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAviZX_5ANQLbn5c6dZmkdkRSSF4RoMyN7orcJSVRUD19sX6pe3hQRouH06-TjWCldY1M8qE45jNoHDA" type="text/javascript"></script>--%>
                              <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>" type="text/javascript"></script>
                              </div>                           
                         </div>
                       
                     </div>
                       
                    
            </div>  
            
        </div>                    

    
    
   
    </form>
</body>
</html>
