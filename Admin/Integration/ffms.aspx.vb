﻿Imports System.Data

Partial Class Admin_Integration_ffms
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                InsertIdentity()
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub InsertIdentity()
        Try
            Dim dt As DataTable
            lblMsg.Text = "Redirecting"

            Dim strUserId As String = Portal.UserSession.UserID
            Dim strGUID As String = System.Guid.NewGuid().ToString()
            Dim strSessionKey As String = Portal.UserSession.SessionKey


            Dim clsIntegration As New adm_integration.clsIntegration

            With clsIntegration.clsProperties
                .dbuserid = strUserId
                .strguid = strGUID
            End With

            dt = clsIntegration.InsertIntegration()

            Dim strServerName As String = Nothing
            If ConfigurationManager.AppSettings("ServerName").ToUpper = "ECHOPLUSPRE" Then
                strServerName = "epondemandpre"
            ElseIf ConfigurationManager.AppSettings("ServerName").ToUpper = "ECHOPLUS" Then
                strServerName = "epondemand"
            Else
                strServerName = ""
            End If


            If (Request.IsSecureConnection) Then
                Response.Redirect("https://portal.dksh.com/" + strServerName + "/Admin/Integration/Incoming.aspx?userid=" + strUserId + "&guid=" + strGUID + "&SessionKey=" + strSessionKey)
            Else
                Response.Redirect("http://portal.dksh.com/" + strServerName + "/Admin/Integration/Incoming.aspx?userid=" + strUserId + "&guid=" + strGUID + "&SessionKey=" + strSessionKey)
            End If

            'Response.Redirect("http://localhost:59382/Echoplus_Ondemand_V2/Admin/Integration/Incoming.aspx?userid=" + strUserId + "&guid=" + strGUID)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
