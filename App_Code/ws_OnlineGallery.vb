﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data

<WebService([Namespace]:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ScriptService()> _
Public Class ws_OnlineGallery
    Inherits System.Web.Services.WebService

    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
            sqlCmd.CommandTimeout = 120

        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
    'Private Shared ReadOnly ConnectionString As String = HttpContext.Current.Session("ffmr_conn")
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffms_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Private Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class



    Public Class clsOGCustomer
        Inherits BaseEntity

        Private _CustCode As String = String.Empty
        Public Property CustCode() As String
            Get
                Return _CustCode
            End Get
            Set(ByVal value As String)
                _CustCode = value
            End Set
        End Property

        Private _CustName As String = String.Empty
        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal value As String)
                _CustName = value
            End Set
        End Property

    End Class

    <WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetOnlineGalleryCollectorCustList(ByVal strCollectorCode As String, ByVal strSearch As String) As List(Of clsOGCustomer)
        Dim sbclsOGCustomer As New List(Of clsOGCustomer)()
        Try

            Dim strUserID As String = Session.Item("UserID")

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_ONLINE_GALLERY_COLLECTOR_CUST_LIST"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "Search", strSearch, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "user_id", strUserID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "collector_code", strCollectorCode, clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            Dim st As New clsOGCustomer()
                            st.CustCode = sqlReader.GetValue(0)
                            st.CustName = sqlReader.GetValue(1)
                            sbclsOGCustomer.Add(st)
                        End While
                    End Using
                End Using
            End Using

        Catch ex As Exception

        End Try
        Return sbclsOGCustomer
    End Function
End Class