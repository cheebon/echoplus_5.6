<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRARetByMth.aspx.vb" Inherits="TRARetByMth" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblHeader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ucctrlpanel5" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="ucMenu" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="ucUpdateProgress" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Stock Return By Month End Closing</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="Form1" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" align="left">
                <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTreeMenuControl" runat="server">
                            <ucMenu:wuc_Menu ID="wuc_Menu" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <ucctrlpanel5:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true" />
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3" style="width: 100%">
                                            <uclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <ucUpdateProgress:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                    AllowSorting="True" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                    GridHeight="440" RowSelectionEnabled="true">
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3" style="height: 5px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
