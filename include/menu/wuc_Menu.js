﻿//CollapseSearchPanel
    //Control the Searching panel's visibility
    function pnlSearch_ChangeCollapsibleState()
    {
        // get the pnlSearch object
        var ctrl =   $get("wuc_Menu_pnlSearch");
        if (ctrl) 
        {
            // Loop through its behaviors
            var behaviors = Sys.UI.Behavior.getBehaviors($get("wuc_Menu_pnlSearch"));
            for (var j = 0; j < behaviors.length; j++)
            {
                // Check if we have a CollapsiblePanel using its type name
                var behavior = behaviors[j];
                if (Object.getTypeName(behavior) == "AjaxControlToolkit.CollapsiblePanelBehavior")
                {
                    var pnlTreeMenu = $get("wuc_Menu_pnlTreeMenu");
                   
                   //Get the currentState
                    var isClose = behavior.get_ClientState();
                    // Open or close the panel
                    if (isClose=='true')
                    {
                        behavior._doOpen();
//                        if (pnlTreeMenu)pnlTreeMenu.style.height=(document.documentElement.clientHeight-170);
                    }
                    else
                    {
                        behavior._doClose();
//                        if (pnlTreeMenu)pnlTreeMenu.style.height=(document.documentElement.clientHeight-78);
                 }
                }
            }
        }
        resetTreePnlPosition();
    }   
    
    

    function BreakHere()
    {
        ////document.getElementById("wuc_Menu_pnlTreeMain").style.overflow="auto";
        //alert("ClientHeight: " + document.documentElement.clientHeight);
        //alert("TreeMain " + document.getElementById("wuc_Menu_pnlTreeMain").style.height);
        //alert("TreePanel" + document.getElementById("wuc_Menu_pnlTreeMenu").style.height);
    }
//End CollapseSearchPanel

//Collapse Main Panel
   //Reset the dgList scrool position, to avoid the freezed header's position not accurate.
    function reset_dgListScrollPosition(dgList)
    {
        var div_dgList = document.getElementById(dgList);
        var currentPosition =0;
        if (div_dgList)
        {
          currentPosition = div_dgList.scrollTop;
          div_dgList.scrollTop = 0;
        }      
    }
   
   //Control the Tree Menu's visibility
    function pnl_TreeMenu_ChangeCollapsibleState()
    {
        reset_dgListScrollPosition('div_dgList');
        // For each element, find the control created for it in the page
        var ctrl =   $get("wuc_ctrlpanel_imgTreeMenu");
        if (ctrl) 
        {
            // Loop through its behaviors
            var behaviors = Sys.UI.Behavior.getBehaviors($get("wuc_Menu_pnlTreeMain"));
            for (var j = 0; j < behaviors.length; j++)
            {
                // Check if we have a CollapsiblePanel using its type name
                var behavior = behaviors[j];
                if (Object.getTypeName(behavior) == "AjaxControlToolkit.CollapsiblePanelBehavior")
                {
                    var isClose = behavior.get_ClientState();
                    // Open or close the panel
                    if (isClose=='true')
                    {
                        behavior._doOpen();
                    }
                    else
                    {
                        behavior._doClose();
                    }
                }
            }
        }
        resetTreePnlPosition();
    }   
    
    
    
//End Collapse Main Panel

//Reset_Position
window.onresize = resetTreePnlPosition;
   function resetTreePnlPosition()
    {
      var pnlTreeMenu = $get("wuc_Menu_pnlTreeMenu");
      if (pnlTreeMenu)pnlTreeMenu.style.height=Math.max((getViewportSize()[1] - pnlTreeMenu.offsetTop -80),0)+'px';
   }
setTimeout('resetTreePnlPosition()',3000);
function getViewportSize() { var size = [0, 0]; 
if (typeof window.innerWidth != 'undefined') { size = [ window.innerWidth, window.innerHeight ]; } 
else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) 
{ size = [ document.documentElement.clientWidth, document.documentElement.clientHeight ]; } 
else { size = [ document.getElementsByTagName('body')[0].clientWidth, document.getElementsByTagName('body')[0].clientHeight ]; } 
return size; }
//End Reset_Position
