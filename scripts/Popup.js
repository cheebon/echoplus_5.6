﻿/*!
****************************************************
* author: Tan Kee Wai
* purpose: To handle every popup function
* Date: 24 april 2013
****************************************************
* Pop up is a div in html set to "visibiliy:hidden", binding with specific web user control
* and when user call this function, will set div in html set to "visibiliy:visible" and bind text to web user control
****************************************************
*/


function openpopup(DivId) {
    var divobj = document.getElementById(DivId);
    divobj.style.visibility = "visible";
}

function closepopup(DivId) {
    var divobj = document.getElementById(DivId);
    divobj.style.visibility = "hidden";
}

function centrePopup() {
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;

    var popupHeight = $("#divpopup").height();
    var popupWidth = $("#divpopup").width();

    $("#divpopup").css(
		{
		    "position": "absolute",
		    "top": "5px",
		    "left": windowWidth / 2 - popupWidth / 2 + 100
		});

    $("#div").css({
        "height": windowHeight
    });

  
}

//function centrePopupShipto() {
//    var windowWidth = document.documentElement.clientWidth;
//    var windowHeight = document.documentElement.clientHeight;

//    var popupHeight = $("#divShipto").height();
//    var popupWidth = $("#divShipto").width();

//    $("#divShipto").css(
//	    {
//		    "position": "absolute",
//		    "top": "5px",
//		    "left": windowWidth / 2 - popupWidth / 2 + 20
//	    });

//    $("#popupcontent").css({
//        "height": windowHeight
//    });
 
//}

function centrePopupWebRefNo() {
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;

    var popupHeight = $("#divWebRefNo").height();
    var popupWidth = $("#divWebRefNo").width();

    $("#divWebRefNo").css(
		{
		    "position": "absolute",
		    "top": "5px",
		    "left": windowWidth / 2 - popupWidth / 2 + 20
		});

    $("#popupWebRefNo").css({
        "height": windowHeight
    });

}


