<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlUserType.ascx.vb" Inherits="include_wuc_ddlUserType" %>
<asp:DropDownList id="ddlUserTypeID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvUserTypeID" ControlToValidate="ddlUserTypeID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select User Type."></asp:CompareValidator>
