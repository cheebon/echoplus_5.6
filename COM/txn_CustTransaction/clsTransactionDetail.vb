'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	30/10/2006
'	Purpose	    :	Class to build datatables for getting common transaction details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsTransactionDetail
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTransactionDetail"
        End Get
    End Property

    Function getTransactionNo(ByVal strSalesmanCode As String, ByVal strType As String) As String
        Dim strRunningNo As String = Nothing
        Dim strBranchCode As String = Nothing
        Dim strTranNo As String = Nothing

        Try
            strBranchCode = getBranchCode(strSalesmanCode)
            strRunningNo = getRunningNo(strSalesmanCode, strType)

            Select Case strRunningNo.Length
                Case CInt("1")
                    strTranNo = String.Format("WB{0}{1}{2}00000{3}", strBranchCode, strSalesmanCode, strType, strRunningNo)

                Case CInt("2")
                    strTranNo = String.Format("WB{0}{1}{2}0000{3}", strBranchCode, strSalesmanCode, strType, strRunningNo)

                Case CInt("3")
                    strTranNo = String.Format("WB{0}{1}{2}000{3}", strBranchCode, strSalesmanCode, strType, strRunningNo)

                Case CInt("4")
                    strTranNo = String.Format("WB{0}{1}{2}00{3}", strBranchCode, strSalesmanCode, strType, strRunningNo)

                Case CInt("5")
                    strTranNo = String.Format("WB{0}{1}{2}0{3}", strBranchCode, strSalesmanCode, strType, strRunningNo)

                Case CInt("6")
                    strTranNo = String.Format("WB{0}{1}{2}{3}", strBranchCode, strSalesmanCode, strType, strRunningNo)

            End Select
            Return strTranNo
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getTransactionNo :" & ex.Message))
        End Try


    End Function

    Function formatVisitNo(ByVal strSalesmanCode As String) As String
        Dim strVisitNo As String = Nothing

        Try
            strVisitNo = getVisitNo(strSalesmanCode)

            Select Case strVisitNo.Length
                Case CInt("1")
                    strVisitNo = "WB" & String.Format("00000{0}", strVisitNo)
                Case CInt("2")
                    strVisitNo = "WB" & String.Format("0000{0}", strVisitNo)
                Case CInt("3")
                    strVisitNo = "WB" & String.Format("000{0}", strVisitNo)
                Case CInt("4")
                    strVisitNo = "WB" & String.Format("00{0}", strVisitNo)
                Case CInt("5")
                    strVisitNo = "WB" & String.Format("0{0}", strVisitNo)
                Case CInt("6")
                    strVisitNo = "WB" & String.Format("{0}", strVisitNo)

            End Select

            Return strVisitNo
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".formatVisitNo :" & ex.Message))
        End Try
    End Function

    Sub updateDailyTimeSum(ByVal strVisitID As String, ByVal strContactCode As String, ByVal strTime_Spend As String, ByVal strTime_Out As String)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_UPDATEDAILYTIMESUM"

                .addItem("visit_id", strVisitID, 2)
                .addItem("cont_acc_code", strContactCode, 2)
                .addItem("time_spend", strTime_Spend, 2)
                .addItem("time_out", strTime_Out, 2)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateDailyTimeSum :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Sub

    Function getVisitNo(ByVal strSalesmanCode As String) As String
        Dim objdb As New cor_DB.clsDB
        Dim dt As Data.DataTable = Nothing
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETVISITNO"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("type", "VISIT", 2)
                dt = .spRetrieve
            End With


        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getVisitNo :" & ex.Message))
        End Try
        If dt.Rows.Count = 0 Then
            Return ""
        Else
            Return dt.Rows(0)("runno").ToString
        End If
    End Function

    Function getBranchCode(ByVal strSalesmanCode As String) As String
        Dim objDB As New cor_DB.clsDB
        Dim dt As Data.DataTable = Nothing
        Try
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETBRANCHCODE"
                .addItem("salesmancode", strSalesmanCode, 2)
                dt = .spRetrieve

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getBranchCode :" & ex.Message))
        End Try
        If dt.rows.count > 0 Then
            Return dt.Rows(0)("branch_code")
        Else
            Return ""
        End If

    End Function

    Function getRunningNo(ByVal strSalesmanCode As String, ByVal strType As String) As String
        Dim objDB As New cor_DB.clsDB
        Dim dt As Data.DataTable = Nothing


        Try
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETRUNNINGNO"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("type", strType, 2)
                dt = .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRunningNo :" & ex.Message))
        End Try
        Return dt.Rows(0)("runno").ToString
    End Function

    Function getSalesmanStatus(ByVal strSalesmancode As String) As Boolean
        Dim objdb As New cor_DB.clsDB
        Dim dt As Data.DataTable = Nothing
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_CHECKSALESMAN"
                .addItem("salesmancode", strSalesmancode, 2)
                dt = .spRetrieve
            End With

            If dt.Rows.Count = 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSalesmanStatus :" & ex.Message))
        End Try
    End Function

    Sub createNewSalesman(ByVal strSalesmanCode As String)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_NEWSALESMAN"
                .addItem("salesmancode", strSalesmanCode, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".createNewSalesman :" & ex.Message))
        End Try
    End Sub

    Sub increaseRunningNo(ByVal strSalesmanCode As String, ByVal strType As String)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_UPDATERUNNO"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("type", strType, 2)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".increaseRunningNo :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Sub

    Function getEditDetailDT(ByVal strSalesmanCode As String, ByVal strType As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRANSACTIONDETAIL"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("type", strType, 2)
                Return .spRetrieve

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getEditDetailDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Function

    Sub changeSavedStatus(ByVal strTxnNo As String, ByVal strInd As String)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRANSACTIONCHANGESAVEDSTATUS"
                .addItem("txnno", strTxnNo, 2)
                .addItem("type", strInd, 2)
                .spUpdate()

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getEditDetailDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Function truncateString(ByVal strString As String, ByVal intLength As Integer) As String
        Try
            Return Strings.Left(strString, intLength) & ".."
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".truncateString :" & ex.Message))
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
