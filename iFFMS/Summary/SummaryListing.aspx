<%@ Page Language="vb" AutoEventWireup="false" Inherits="SummaryListing" CodeFile="SummaryListing.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmsummarylist" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <asp:UpdatePanel runat="server" ID="UpdatePage" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;" class="BckgroundInsideContentLayout">
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                    </div>
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;" class="BckgroundInsideContentLayout">
                        <table width="100%">
                            <tr>
                                <td width="10%">
                                    <asp:Label ID="lblDate" runat="server" CssClass="cls_label_header">Date :</asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlDate" runat="server" AutoPostBack="True" CssClass="cls_dropdownlist"
                                        Font-Names="Verdana" Width="144px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <asp:UpdatePanel runat="server" ID="updateGV" UpdateMode="Conditional">
                            <ContentTemplate>
                                <uc1:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    Width="50%" FreezeHeader="True" GridHeight="250px" AddEmptyHeaders="0" CellPadding="2"
                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="200px"
                                    RowHighlightColor="AntiqueWhite">
                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                    <EmptyDataTemplate>
                                        There is no data to display.</EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="TXN" HeaderText="Transaction"  ItemStyle-HorizontalAlign="Left"  />
                                        <asp:BoundField DataField="QTY" HeaderText="Count"  ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="AMT" HeaderText="Total Amt."  ItemStyle-HorizontalAlign="Right" />
                                    </Columns>
                                    <FooterStyle CssClass="GridFooter" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                    <RowStyle CssClass="GridNormal" />
                                </ccGV:clsGridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
                </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
