'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSO
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSO"
        End Get
    End Property

#Region "DDL"
    Public Function GetUOMCode(ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_UOMCODE"
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetUOMPrice(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strPrdCode As String, _
                                ByVal strUOMCode As String, ByVal strQty As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_UOMPRICE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUOMCode, clsDB.DataType.DBString)
            .addItem("QTY", strQty, clsDB.DataType.DBString)
            .addOutput("UOM_PRICE", 2)
            Return .spRetrieveValue("UOM_PRICE")
        End With
        objDB = Nothing
    End Function

    Public Function GetShipToAddress(ByVal strCustCode As String, ByVal strShipToCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_SHIPTO_ADDRESS"
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("SHIPTO_CODE", strShipToCode, clsDB.DataType.DBString)
            .addOutput("ADDRESS", 2)
            Return .spRetrieveValue("ADDRESS")
        End With
        objDB = Nothing
    End Function

    Public Function GetShipToCode(ByVal strSalesrepCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_SHIPTO"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetPymtTerm(ByVal strSalesrepCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_PYMT_TERM"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetLineType(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_LINETYPE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetPrdGrpCode(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_PRD_GRP_CODE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetPrdCode(ByVal strSalesrepCode As String, ByVal strPrdGrpCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_PRD_CODE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetDlvyDate(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_DELIVERY_DATE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "TXN_NO"

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

#Region "SO HDR"
    Public Sub InstTMPSOHeader(ByVal strSessionId As String, ByVal strTxnNo As String, _
    ByVal strVisitId As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
    ByVal strShipToCode As String, ByVal strShipToDate As String, ByVal strPONO As String, ByVal strPayTermCode As String, _
    ByVal strPayDate As String, ByVal strPartialDlvy As String, ByVal strRemarks As String, ByVal strPayBy As String, ByVal strUserId As String, ByVal strTxnDate As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_HDR_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBDouble)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("SHIPTO_CODE", strShipToCode, clsDB.DataType.DBString)
            .addItem("SHIPTO_DATE", strShipToDate, clsDB.DataType.DBString)
            .addItem("PO_NO", strPONO, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("PAY_TERM_CODE", strPayTermCode, clsDB.DataType.DBString)
            .addItem("PAY_DATE", strPayDate, clsDB.DataType.DBString)
            .addItem("PARTIAL_DLVY", strPartialDlvy, clsDB.DataType.DBString)
            .addItem("PAY_BY", strPayBy, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing

    End Sub
#End Region

#Region "SO DTL"

    Public Sub InstTMPSODetails(ByVal strSessionId As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
    ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal strUomCode As String, ByVal strLineType As String, ByVal strQty As Double, _
    ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_DTL_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("LINE_TYPE", strLineType, clsDB.DataType.DBString)
            .addItem("QTY", strQty, clsDB.DataType.DBDouble)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelTMPSODetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, ByVal strLineNo As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_DTL_DELETE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub

    Public Function GetSODtl(ByVal strPrdGrpCode As String, ByVal strPrdCode As String, ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SO_PRDCODE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTMPSODtlEdit(ByVal strTXNNo As String, ByVal strLineNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_DTL_EDIT"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Sub UpdateTMPSODtl(ByVal strSessionId As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
 ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal strUomCode As String, ByVal strLineType As String, ByVal strQty As Double, _
 ByVal strUserId As String, ByVal strLineNo As Integer)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_DTL_UPDATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("LINE_TYPE", strLineType, clsDB.DataType.DBString)
            .addItem("QTY", strQty, clsDB.DataType.DBDouble)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBInt)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Function GetOrdPoisonCheck(ByVal strTXNNo As String, ByVal strPrdCode As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_ORD_POISON_CHECK"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetOrdPoisonCheckAll(ByVal strTXNNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_ORD_POISON_CHECK_ALL"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "SO SUMM"

    Public Function GetSOSumHdrs(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, _
     ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_HDR"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSOSumDtl(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_ORD_DTL"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region


#Region "so SUBMIT"

    Public Function InstsoDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strStatus As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SO_DTL_SUBMIT"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

End Class
