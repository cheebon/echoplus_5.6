'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	14/03/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSalesArea
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSalesArea"
        End Get
    End Property

    Public Function GetSalesAreaList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_AREA_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSalesAreaDetails(ByVal strSalesAreaCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_AREA_DTL"
                .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateSalesArea(ByVal strSalesAreaCode As String, ByVal strSalesAreaName As String, ByVal strSalesAreaDesc As String, ByVal strAddr1 As String, ByVal strAddr2 As String, ByVal strAddr3 As String, ByVal strAddr4 As String, ByVal strTelNo As String, ByVal strFaxNo As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_AREA_CREATE"
                .addItem("SALES_AREA_CODE", strSalesAreaCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALES_AREA_NAME", strSalesAreaName, cor_DB.clsDB.DataType.DBString)
                .addItem("SALES_AREA_DESC", strSalesAreaDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_1", strAddr1, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_2", strAddr2, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_3", strAddr3, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_4", strAddr4, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO", strTelNo, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO", strFaxNo, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateSalesArea(ByVal strSalesAreaCode As String, ByVal strSalesAreaName As String, ByVal strSalesAreaDesc As String, ByVal strAddr1 As String, ByVal strAddr2 As String, ByVal strAddr3 As String, ByVal strAddr4 As String, ByVal strTelNo As String, ByVal strFaxNo As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_AREA_UPDATE"
                .addItem("SALES_AREA_CODE", strSalesAreaCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALES_AREA_NAME", strSalesAreaName, cor_DB.clsDB.DataType.DBString)
                .addItem("SALES_AREA_DESC", strSalesAreaDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_1", strAddr1, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_2", strAddr2, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_3", strAddr3, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_4", strAddr4, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO", strTelNo, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO", strFaxNo, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteSalesArea(ByVal strCodeList As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_AREA_DELETE"
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class


