<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_txtDateDynamic.ascx.vb" Inherits="include_wuc_txtDateDynamic" %>
<link href="DKSH.css" rel="stylesheet" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <asp:TextBox ID="txtDate" Width="150" runat="server" CssClass="cls_textbox" ReadOnly="True"></asp:TextBox>
            <asp:ImageButton ID="imgDate" ImageUrl="~/images/icoCalendar.gif" runat="server" CssClass="cls_button" CausesValidation="false" /><br />
            <asp:RequiredFieldValidator ID="rfvDate" runat="server" Display="Dynamic" ControlToValidate="txtDate" ErrorMessage="Date cannot be blank !"></asp:RequiredFieldValidator>
          </td>
     </tr>
     <tr>
        <td>
            <asp:Panel ID="pnlDate" runat="server" Visible="false">
                <table cellpadding="0" cellspacing="0" border="0" class="cls_panel_calendar" >
                    <tr>
                        <td width="15%"><asp:label id="lblMonth" runat="server" CssClass="cls_label_header">Month</asp:label></td>
					    <td width="2%"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
					    <td>
						    <asp:dropdownlist id="ddlMonth" CssClass="cls_dropdownlist" Runat="server" AutoPostBack="True" CausesValidation="false">
							    <asp:ListItem Value="1">January</asp:ListItem>
							    <asp:ListItem Value="2">February</asp:ListItem>
							    <asp:ListItem Value="3">March</asp:ListItem>
							    <asp:ListItem Value="4">April</asp:ListItem>
							    <asp:ListItem Value="5">May</asp:ListItem>
							    <asp:ListItem Value="6">June</asp:ListItem>
							    <asp:ListItem Value="7">July</asp:ListItem>
							    <asp:ListItem Value="8">August</asp:ListItem>
							    <asp:ListItem Value="9">September</asp:ListItem>
							    <asp:ListItem Value="10">October</asp:ListItem>
							    <asp:ListItem Value="11">November</asp:ListItem>
							    <asp:ListItem Value="12">December</asp:ListItem>
						    </asp:dropdownlist>
					    </td>
					    <td width="15%" align="right"><asp:label id="lblYear" runat="server" CssClass="cls_label_header">Year</asp:label></td>
					    <td width="2%"><asp:label id="lblDot3" runat="server" CssClass="cls_label_header">:</asp:label></td>
					    <td style="width: 78px"><asp:dropdownlist id="ddlYear" cssClass="cls_dropdownlist" runat="server" AutoPostBack="True" CausesValidation="false"></asp:dropdownlist></td>
					    <%--<td width="60%">&nbsp;</td>--%>
					  </tr>
                    <asp:Panel ID="pnlTime" runat="server">
                        <tr>
                            <td nowrap>
                                <!--<asp:label id="lblTime" CssClass="cls_label_header" Runat="server">Time : </asp:label>-->
                                <asp:Label ID="lblHour" CssClass="cls_label_header" runat="server">Hour</asp:Label>
                            </td>
                            <td width="2%">
                                <asp:Label ID="lblDot4" runat="server" CssClass="cls_label_header">:</asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlHour" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td>
                                <asp:Label ID="lblMin" CssClass="cls_label_header" runat="server">Minute</asp:Label></td>
                            <td width="2%">
                                <asp:Label ID="lblDot5" runat="server" CssClass="cls_label_header">:</asp:Label></td>
                            <td style="width: 78px">
                                <asp:DropDownList ID="ddlMin" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True"
                                    CausesValidation="false">
                                </asp:DropDownList></td>
                        </tr>
                    </asp:Panel>
                    <tr>
					        <td colspan="7">
					            <%--<asp:Calendar ID="calDate" runat="server"></asp:Calendar> --%>
					            <asp:Calendar id="calDate" runat="Server" CellPadding="0" CellSpacing="0" TodayDayStyle-Font-Bold="True"
							        DayHeaderStyle-Font-Bold="True" OtherMonthDayStyle-ForeColor="gray" TitleStyle-BackColor="#dae4ee"
							        TitleStyle-ForeColor="white" TitleStyle-Font-Bold="True" SelectedDayStyle-BackColor="#ffcc66"
							        SelectedDayStyle-Font-Bold="True" CausesValidation="false">
							        <TodayDayStyle Font-Bold="True"></TodayDayStyle>
							        <DayStyle HorizontalAlign="Center" BorderStyle="Solid" VerticalAlign="Top"></DayStyle>
							        <DayHeaderStyle Font-Bold="True"></DayHeaderStyle>
							        <SelectedDayStyle Font-Bold="True" BackColor="#FFCC66"></SelectedDayStyle>
							        <TitleStyle Font-Bold="True" ForeColor="#4B4B4B" BackColor="#C2D7EE"></TitleStyle>
							        <OtherMonthDayStyle ForeColor="Gray"></OtherMonthDayStyle>
						        </asp:Calendar>
					        </td>
					   </tr>            
					   <tr><td colspan="7" align="center"><asp:Button ID="btnCalendar" CssClass="cls_button" runat="server" CausesValidation="false" /></td></tr> 
					   <tr><td colspan="7"></td></tr>
                </table>
            </asp:Panel>
        </td>
     </tr>
</table>

</ContentTemplate>
</asp:UpdatePanel>



