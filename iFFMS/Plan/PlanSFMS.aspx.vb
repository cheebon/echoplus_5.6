Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_Plan_PlanSFMS
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection


    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountSelectedSFMS() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSelectedSFMS"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSelectedSFMS") = value
        End Set
    End Property

    Private Property Master_Row_CountNewSFMS() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountNewSFMS"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountNewSFMS") = value
        End Set
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Assign Field Activity" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControlSelectedSFMS.Enabled = True
            TimerControlNewSFMS.Enabled = True
            LoadCatDDL()
        End If
    End Sub


#Region "DGLISTSelectedCont"
    Protected Sub TimerControlSelectedSFMS_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlSelectedSFMS.Tick
        If TimerControlSelectedSFMS.Enabled Then RefreshDatabindingSelectedSFMS()
        TimerControlSelectedSFMS.Enabled = False
    End Sub

    Public Sub RenewDataBindSelectedSFMS()
        RefreshDataBindSelectedSFMS()
    End Sub

    Public Sub RefreshDataBindSelectedSFMS()
        RefreshDatabindingSelectedSFMS()
    End Sub

    Public Sub RefreshDatabindingSelectedSFMS(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableSelectedSFMS As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionSelectedSFMS As String = CType(ViewState("strSortExpressionSelectedSFMS"), String)


        dtCurrentTableSelectedSFMS = GetRecListSelectedSFMS()

        If dtCurrentTableSelectedSFMS Is Nothing Then
            dtCurrentTableSelectedSFMS = New DataTable
        Else
            If dtCurrentTableSelectedSFMS.Rows.Count = 0 Then
                Master_Row_CountSelectedSFMS = 0
                btncheckalldelete.Visible = False
                btnuncheckalldelete.Visible = False
                btndelete.Visible = False
                'dtCurrentTableSelectedCont.Rows.Add(dtCurrentTableSelectedCont.NewRow())
            Else

                Master_Row_CountSelectedSFMS = dtCurrentTableSelectedSFMS.Rows.Count
                btncheckalldelete.Visible = True
                btnuncheckalldelete.Visible = True
                btndelete.Visible = True
            End If
        End If

        Dim dvCurrentViewSelectedSFMS As New DataView(dtCurrentTableSelectedSFMS)
        If Not String.IsNullOrEmpty(strSortExpressionSelectedSFMS) Then
            Dim strSortExpressionNameSelectedSFMS As String = strSortExpressionSelectedSFMS.Replace(" DESC", "")
            dvCurrentViewSelectedSFMS.Sort = IIf(dtCurrentTableSelectedSFMS.Columns.Contains(strSortExpressionSelectedSFMS), strSortExpressionSelectedSFMS, "")
        End If

        With dglistSelectedSFMS
            .DataSource = dvCurrentViewSelectedSFMS
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountSelectedSFMS > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateSelectedSFMS()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListSelectedSFMS() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strUserId As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonSFMS(strSalesrepCode, strRouteDate, strCustCode, strUserId)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateSelectedSFMS()

        UpdateSelectedSFMS.Update()

    End Sub

    Protected Sub dglistSelectedSFMS_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedSFMS.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedSFMS > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListSelectedSFMS_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistSelectedSFMS.Sorting
        Dim strSortExpressionSelectedSFMS As String = ViewState("strSortExpressionSelectedSFMS")

        If strSortExpressionSelectedSFMS IsNot Nothing AndAlso strSortExpressionSelectedSFMS.Length > 0 Then
            If strSortExpressionSelectedSFMS Like (e.SortExpression & "*") Then
                If strSortExpressionSelectedSFMS.IndexOf(" DESC") > 0 Then
                    strSortExpressionSelectedSFMS = e.SortExpression
                Else
                    strSortExpressionSelectedSFMS = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionSelectedSFMS = e.SortExpression
            End If
        Else
            strSortExpressionSelectedSFMS = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionSelectedSFMS") = strSortExpressionSelectedSFMS

        RefreshDatabindingSelectedSFMS()

    End Sub

#End Region

#Region "DGLISTNewCust"
    Protected Sub TimerControlNewSFMS_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlNewSFMS.Tick
        If TimerControlNewSFMS.Enabled Then RefreshDatabindingNewSFMS()
        TimerControlNewSFMS.Enabled = False
    End Sub

    Public Sub RenewDataBindNewSFMS()
        RefreshDataBindNewSFMS()
    End Sub

    Public Sub RefreshDataBindNewSFMS()
        RefreshDatabindingNewSFMS()
    End Sub

    Public Sub RefreshDatabindingNewSFMS(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableNewSFMS As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionNewSFMS As String = CType(ViewState("strSortExpressionNewSFMS"), String)


        dtCurrentTableNewSFMS = GetRecListNewSFMS()

        If dtCurrentTableNewSFMS Is Nothing Then
            dtCurrentTableNewSFMS = New DataTable
        Else
            If dtCurrentTableNewSFMS.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountSelectedSFMS = 0
                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btnadd.Visible = False
            Else
                Master_Row_CountSelectedSFMS = dtCurrentTableNewSFMS.Rows.Count
                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btnadd.Visible = True
            End If
        End If

        Dim dvCurrentViewNewCont As New DataView(dtCurrentTableNewSFMS)
        If Not String.IsNullOrEmpty(strSortExpressionNewSFMS) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionNewSFMS.Replace(" DESC", "")
            dvCurrentViewNewCont.Sort = IIf(dtCurrentTableNewSFMS.Columns.Contains(strSortExpressionNewSFMS), strSortExpressionNewSFMS, "")
        End If

        With dglistNewSFMS
            .DataSource = dvCurrentViewNewCont
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountNewSFMS > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateNewSFMS()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListNewSFMS() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strCustCode As String, strUserId As String, strRouteDate As String, strCatCode As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCatCode = Trim(ddlcatcode.SelectedValue)


            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonContBySFMS(strSalesrepCode, strCustCode, strUserId, strRouteDate, strCatCode)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateNewSFMS()

        UpdateNewSFMS.Update()

    End Sub

    Protected Sub dglistNewSFMS_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistNewSFMS.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountNewSFMS > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListNewSFMS_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistNewSFMS.Sorting
        Dim strSortExpressionNewSFMS As String = ViewState("strSortExpressionNewSFMS")

        If strSortExpressionNewSFMS IsNot Nothing AndAlso strSortExpressionNewSFMS.Length > 0 Then
            If strSortExpressionNewSFMS Like (e.SortExpression & "*") Then
                If strSortExpressionNewSFMS.IndexOf(" DESC") > 0 Then
                    strSortExpressionNewSFMS = e.SortExpression
                Else
                    strSortExpressionNewSFMS = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionNewSFMS = e.SortExpression
            End If
        Else
            strSortExpressionNewSFMS = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionNewSFMS") = strSortExpressionNewSFMS

        RefreshDatabindingNewSFMS()

    End Sub

#End Region


#Region "Event Handler"

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        AddNewSFMS()
        TimerControlSelectedSFMS.Enabled = True
        TimerControlNewSFMS.Enabled = True
    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        DelNewSFMS()
        TimerControlSelectedSFMS.Enabled = True
        TimerControlNewSFMS.Enabled = True
    End Sub

#End Region

    Private Sub AddNewSFMS()
        Try
            If dglistNewSFMS.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strCatCode As String, strSubCatCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistNewSFMS.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistNewSFMS.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strCatCode = dglistNewSFMS.DataKeys(i).Item("CAT_CODE")
                            strSubCatCode = dglistNewSFMS.DataKeys(i).Item("SUB_CAT_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.InstPlanAddonSFMS(strSalesrepCode, strRouteDate, strCustCode, strCatCode, strSubCatCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblNmsg.Text = "Record(s) added !"
            lblsmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelNewSFMS()
        Try
            If dglistSelectedSFMS.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strCatCode As String, strSubCatCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistSelectedSFMS.Rows
                    chkSelected = CType(DR.FindControl("chkDelete"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistSelectedSFMS.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strCatCode = dglistNewSFMS.DataKeys(i).Item("CAT_CODE")
                            strSubCatCode = dglistSelectedSFMS.DataKeys(i).Item("SUB_CAT_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.DelPlanAddonSFMS(strSalesrepCode, strRouteDate, strCustCode, strCatCode, strSubCatCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblSmsg.Text = "Record(s) deleted !"
            lblNmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCatDDL()
        Dim DT As DataTable
        Dim clsSFMS As New txn_WebActy.clsSFMS

        DT = clsSFMS.GetCatcode(Request.QueryString("salesrepcode"))
        With ddlcatcode
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "CAT_NAME"
            .DataValueField = "CAT_CODE"
            .DataBind()
            .Items.Insert(0, New ListItem("-- SELECT --", ""))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlcatcode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcatcode.SelectedIndexChanged
        RefreshDatabindingNewSFMS()
    End Sub
End Class
