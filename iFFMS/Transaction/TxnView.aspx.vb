Imports System.Data
Imports txn_WebActy

Partial Class iFFMS_Transaction_TxnView
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemDRCSubList")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemDRCOrderList") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMS_DRCSubLsit"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

#End Region

    Private intPageSize As Integer
    Public ReadOnly Property PageName() As String
        Get
            Return "Transaction View"
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        lblErr.Text = ""

        'Call Header
        With wuc_lblHeader
            .Title = "Summary View"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True
            btnback.Visible = True
            If Trim(Request.QueryString("TxnStatus")).ToUpper = "K" Then
                btncancel.Visible = True
            End If
        End If
    End Sub

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        dtCurrenttable = GetRecList()

        PreRenderMode(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_TxnView.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_TxnView.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_TxnView.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TxnView.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_TxnView.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName

                        dgColumn.DataNavigateUrlFields = Nothing 'strUrlFields
                        dgColumn.DataNavigateUrlFormatString = Nothing 'strUrlFormatString
                        dgColumn.Target = "_blank" '"_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_TxnView.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TxnView.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_TxnView.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTxnType As String, strTXNNo As String, strVisitId As String, strSalesrepCode As String, strCustCode As String, _
            StrContCode As String, strStatusCode As String, strUserid As String
            strTxnType = Trim(Request.QueryString("TxnType"))
            strTXNNo = Trim(Request.QueryString("TXNNO"))
            strVisitId = Trim(Request.QueryString("visitid"))
            strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            StrContCode = Trim(Request.QueryString("ContCode"))
            strStatusCode = Trim(Request.QueryString("TxnStatus"))
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))

    
            Dim clsCommon As New txn_WebActy.clsCommon
            DT = clsCommon.GetDtlSRTxnList(strTxnType, strTXNNo, strVisitId, strSalesrepCode, strCustCode, StrContCode, strStatusCode, strUserid)

        End If

        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then
                dgList_Init(DT)
                Cal_ItemFigureCollector(DT)

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*QTY" Or strColumnName = "ICO" Or strColumnName = "AMS" Or strColumnName Like "*AMT" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        'Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0
        'Try
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
        Return dblValue
    End Function

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound

        Select Case e.Row.RowType
            Case DataControlRowType.DataRow

            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_TxnView.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next


        End Select

    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()

            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "DETAILVIEW"

    Private Function GetRecList2() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTxnType As String, strTXNNo As String, strVisitId As String, strSalesrepCode As String, strCustCode As String, _
             StrContCode As String, strStatusCode As String, strUserid As String
            strTxnType = Trim(Request.QueryString("TxnType"))
            strTXNNo = Trim(Request.QueryString("TXNNO"))
            strVisitId = Trim(Request.QueryString("visitid"))
            strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            StrContCode = Trim(Request.QueryString("ContCode"))
            strStatusCode = Trim(Request.QueryString("TxnStatus"))
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))

            Dim clsCommon As New txn_WebActy.clsCommon
            DT = clsCommon.GetHdrSRTxnList(strTxnType, strTXNNo, strVisitId, strSalesrepCode, strCustCode, StrContCode, strStatusCode, strUserid)
            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
        End If



        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub RefreshDataBinding2()
        Dim dtCurrenttable2 As Data.DataTable = Nothing
        dtCurrenttable2 = GetRecList2()

        Dim dvCurrentView2 As New Data.DataView(dtCurrenttable2)
        If dtCurrenttable2 IsNot Nothing Then dv_Init(dtCurrenttable2)
        dtviewleft.DataSource = dvCurrentView2
        dtviewleft.DataBind()
        dtviewright.DataSource = dvCurrentView2
        dtviewright.DataBind()


    End Sub

    Protected Sub dv_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim j As Integer = 0
        Dim ColumnNamedv As String = ""

        Try
            dtviewleft.Fields.Clear()
            dtviewright.Fields.Clear()

            For i = 0 To dtToBind.Columns.Count - 1
                ColumnNamedv = dtToBind.Columns(i).ColumnName

                Select Case CF_TxnView.GetFieldColumnType(ColumnNamedv, True)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        j = j + 1
                        Dim dvcolumn As New BoundField

                        dvcolumn = New BoundField

                        Dim strFormatStringdv As String = CF_TxnView.GetOutputFormatString(ColumnNamedv)
                        If String.IsNullOrEmpty(strFormatStringdv) = False Then
                            dvcolumn.DataFormatString = strFormatStringdv
                            dvcolumn.HtmlEncode = False
                        End If

                        dvcolumn.DataField = ColumnNamedv
                        dvcolumn.HeaderText = CF_TxnView.GetDisplayColumnName(ColumnNamedv)
                        dvcolumn.ReadOnly = True
                        dvcolumn.SortExpression = ColumnNamedv

                        If j Mod 2 = 0 Then
                            dtviewright.Fields.Add(dvcolumn)
                        Else
                            dtviewleft.Fields.Add(dvcolumn)
                        End If

                        dvcolumn = Nothing
                End Select
            Next

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "EVENT HANDLER"
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        Dim strTxnType As String, strTxnNo As String, strVisitId As String, strSalesrepCode As String, strCustCode As String, _
        strContCode As String, strSessionID As String, strStatus As String

        strTxnType = Trim(Request.QueryString("TxnType"))
        strTxnNo = Trim(Request.QueryString("TXNNO"))
        strVisitId = Trim(Request.QueryString("visitid"))
        strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
        strCustCode = Trim(Request.QueryString("CustCode"))
        strContCode = Trim(Request.QueryString("ContCode"))
        strSessionID = Trim(Request.QueryString("SessionID"))
        strStatus = "C"

        CancelTxn()
        Response.Redirect("TxnView.aspx?TxnType=" + strTxnType + "&TXNNO=" + strTxnNo + "&visitid=" + strVisitId + "&SALESREPCODE=" + strSalesrepCode + "&CustCode=" + strCustCode + "&ContCode=" + strContCode + "&TxnStatus=" + strStatus)


    End Sub

    Private Sub CancelTxn()
        Dim dt As DataTable
        Dim strTxnType As String, strTxnNo As String, strVisitId As String, strSalesrepCode As String, strCustCode As String, _
        strContCode As String, strSessionID As String, strUserID As String

        strTxnType = Trim(Request.QueryString("TxnType"))
        strTxnNo = Trim(Request.QueryString("TXNNO"))
        strVisitId = Trim(Request.QueryString("visitid"))
        strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
        strCustCode = Trim(Request.QueryString("CustCode"))
        strContCode = Trim(Request.QueryString("ContCode"))
        strSessionID = Trim(Request.QueryString("SessionID"))
        strUserID = Trim(Web.HttpContext.Current.Session("UserID"))


        Dim clscommon As New txn_WebActy.clsCommon
        dt = clscommon.CancelHdrDtlSRTxnList(strTxnType, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContCode, strSessionID, strUserID)

        Dim strerror As Integer = dt.Rows(0)("ERROR")

        If strerror = 0 Then
            btncancel.Visible = False
            btnback.Visible = False
            lblMsgPop.Message = "You have successfully cancelled the transaction!!"
            lblMsgPop.Show()
        Else
            lblMsgPop.Message = "You request is not successfully!!"
            lblMsgPop.Show()
        End If
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDatabinding()
            RefreshDataBinding2()
        End If
        TimerControl1.Enabled = False
        UpdatePage.update()
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

   
End Class

Public Class CF_TxnView
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""


        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn No."
            Case "TXN_STATUS"
                strFieldName = "Status"
            Case "LINE_NO"
                strFieldName = "Line No."
            Case "UOM_CODE"
                strFieldName = "UOM Code"
            Case "COLL_CODE"
                strFieldName = "Collector Code"
            Case "BATCH_NO"
                strFieldName = "Batch No."
            Case "REASON_CODE"
                strFieldName = "Reason Code"
            Case "RET_QTY"
                strFieldName = "Trade Rtn."
            Case "LIST_PRICE"
                strFieldName = "Price"
            Case "RET_AMT"
                strFieldName = "Amount"
            Case "REF_NO"
                strFieldName = "Ref No."
            Case "EXP_DATE"
                strFieldName = "Exp. Date"
            Case "TXN_DATE"
                strFieldName = "Txn. Date"
            Case "VOUCHER_NO"
                strFieldName = "Voucher No."
            Case "COLL_NAME"
                strFieldName = "Collector"
            Case "SLOC_CODE"
                strFieldName = "Storage Loca. Code"
            Case "SLOC_NAME"
                strFieldName = "Storage Loca."
            Case "CTN_NO"
                strFieldName = "Carton"
            Case "OPEN_BAL_QTY"
                strFieldName = "Open Bal. Qty."
            Case "SHELF_QTY"
                strFieldName = "Shelf Qty."
            Case "STORE_QTY"
                strFieldName = "Store Qty."
            Case "CUR_STK_QTY"
                strFieldName = "Current Stk. Qty."
            Case "OFF_TAKE_QTY"
                strFieldName = "Off Take Qty."
            Case "STK_TRF_QTY"
                strFieldName = "Stk Trf Qty."
            Case "PROPOSED_ORD_QTY"
                strFieldName = "PO Qty."
            Case "POAMT"
                strFieldName = "PO Amt."
            Case "UOM_PRICE"
                strFieldName = "UOM Price"
            Case "VISIT_ID"
                strFieldName = "Visit ID"
            Case "TXN_DATE_OUT"
                strFieldName = "Txn. Date Out"
            Case "TXN_DATE_IN"
                strFieldName = "Txn. Date In"
            Case "TXN_TIMESTAMP"
                strFieldName = "Txn. Timestamp"
            Case "TITLE_CODE"
                strFieldName = "Title Code"
            Case "GEN_COMMENT"
                strFieldName = "Gen. Comment"
            Case "QUES_CODE"
                strFieldName = "Ques. Code"
            Case "SUB_QUES_CODE"
                strFieldName = "Sub Ques. Code"
            Case "CRITERIA_CODE"
                strFieldName = "Criteria Code"
            Case "EXPEC"
                strFieldName = "Txt. Ans"
            Case "DATE_VAL"
                strFieldName = "Date Ans"
            Case "OPT_VAL"
                strFieldName = "Opt. Ans"
            Case "SHIPTO_DATE"
                strFieldName = "Shipto Date"
            Case "PAY_TERM_CODE"
                strFieldName = "Pay Term Code"
            Case "PAY_TERM_NAME"
                strFieldName = "Pay Term"
            Case "PAY_DATE"
                strFieldName = "Pay Date"
            Case "PARTIAL_DLVY"
                strFieldName = "Part. Del."
            Case "TTL_ORD_AMT"
                strFieldName = "Ord. Amt."
            Case "DISC_AMT"
                strFieldName = "Disc. Amt."
            Case "PAY_AMT"
                strFieldName = "Pay Amt."
            Case "LINE_TYPE"
                strFieldName = "Line Type"
            Case "ORD_TYPE"
                strFieldName = "Ord. Type"
            Case "QTY"
                strFieldName = "Qty"
            Case "FOC_QTY"
                strFieldName = "Foc. Qty"
            Case "NET_PRICE"
                strFieldName = "Net. Price"
            Case "AMT"
                strFieldName = "Amt."
            Case "RET_TYPE"
                strFieldName = "Ret. Type"
            Case "LN_STATUS"
                strFieldName = "LN Status"
            Case "LN_REASON"
                strFieldName = "LN Reason"
            Case "TTL_RET_AMT"
                strFieldName = "Ret. Amt."
            Case "COLL_BY_CODE"
                strFieldName = "Coll. By Code"
            Case "UOM_NAME"
                strFieldName = "UOM Name"
            Case "TITLE_NAME"
                strFieldName = "Title Name"
            Case "QUES_NAME"
                strFieldName = "Ques. Name"
            Case "SUB_QUES_NAME"
                strFieldName = "Sub Ques. Name"
            Case "CRITERIA_NAME"
                strFieldName = "Criteria Name"
            Case "&NBSP;"
                strFieldName = " "
            Case "ACT_TXN_DATE"
                strFieldName = "Input Date"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "" Then

                FCT = FieldColumntype.HyperlinkColumn

            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try

            If strColumnName.ToUpper Like "*QTY" Then
                strFormatString = "{0:#,0.0}"
            ElseIf strColumnName.ToUpper Like "*AMT" Or strColumnName.ToUpper Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            ElseIf strColumnName.ToUpper Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName.ToUpper = "ICO" Or strColumnName.ToUpper = "AMS" Then
                strFormatString = "{0:#,0}"
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_NAME" Or strColumnName Like "REMARKS" Then
                    .HorizontalAlign = HorizontalAlign.Left
                ElseIf strColumnName Like "*_CODE" Or strColumnName Like "*_STATUS" Or strColumnName Like "*_NO" Then
                    .HorizontalAlign = HorizontalAlign.Center
                Else
                    .HorizontalAlign = HorizontalAlign.Right
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class