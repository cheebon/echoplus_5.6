'clsCallQuery
'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	30/10/2005
'	Purpose	    :	Class to build DRC Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCallQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    'Sub New()
    '    TableName = ""
    'End Sub

    'Sub New(ByVal strTableName As String)
    '    TableName = strTableName
    'End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallQuery"
        End Get
    End Property

#Region "CALL Analysis"
    '--execute SPP_RPT_CALLBYMONTH @YEAR, @USER_ID, @TEAM_CODE, @SALESREP_CODE, @SELECTION
    Public Function GetCallByMonth(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strSalesrepList As String, ByVal intSelectionValue As Integer) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_MONTH"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("SELECTION", intSelectionValue.ToString, 1)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByMonth :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    '--execute SPP_RPT_CALLBYDAY @YEAR,@MONTH,@USER_ID,@TEAM_CODE,@SALESREP_CODE
    Public Function GetCallByDay(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_DAY"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByDay :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    '--execute SPP_RPT_CALLBYCUSTOMER @YEAR, @MONTH, @DAY, @USER_ID, @SALESREP_CODE, [@TEAM_CODE]
    Public Function GetCallByCustomer(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strDay As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_CUST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("DAY", strDay, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByCustomer :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    Public Function GetCallByCustomerADV(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strDay As String, _
     ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strAgencyCode As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_CUST_ADV"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("DAY", strDay, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByCustomer :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    '--execute SPP_RPT_CALLENQUIRYLIST @DATE_START, @DATE_END, @USER_ID, [@SALESREP_CODE], [@CUST_CODE]
    Public Function GetCallEnquiryList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strDATE_START As String, ByVal strDATE_END As String, ByVal strSalesrepCode As String, ByVal strCUSTCODE As String, ByVal strContCode As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_ENQUIRY_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("DATE_START", strDATE_START, clsDB.DataType.DBString)
                .addItem("DATE_END", strDATE_END, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCUSTCODE, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallEnquiryList :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    Public Function GetMonthlyCallRptType(ByVal strYear As String, ByVal strMonth As String, ByVal strUserID As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_MONTHLY_CALL_RPT_TYPE_DDL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMonthlyCallRptType :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function


    Public Function GetQuarterlyCallRptType(ByVal strYear As String, ByVal strMonth As String, ByVal strUserID As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_GET_QUARTER_CALL_RPT_TYPE_DDL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMonthlyCallRptType :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function
#End Region

#Region "CALL Productivity"
    '--execute SPP_RPT_SFMSCALLPRODLIST @YEAR, @USER_ID, @TEAM_CODE, @SALESREP_CODE, @GRP_FIELD , @SELECTION
    '--execute SPP_RPT_CALL_PRDY_LIST @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @GRP_FIELD , @SELECTION
    Public Function GetCallProdList(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, ByVal strYear As String, ByVal strSalesrepList As String, ByVal strGroupField As String, ByVal intSelectionValue As Integer) As DataTable
        Dim clCallProdDB As clsDB
        Try
            clCallProdDB = New clsDB
            With clCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("SELECTION", intSelectionValue.ToString, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProdList :" & ex.Message))
        Finally
            clCallProdDB = Nothing
        End Try
    End Function

    '--execute SPP_RPT_CALL_PRDY_ACTY_DTL @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @MONTH, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @CUSTCODE , @CGCODE , @CATCODE, @SUBCAT
    Public Function GetCallProd_ACTDTLList(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String, ByVal strCustCode As String, ByVal strCgCode As String, ByVal strCatCode As String, ByVal strSubCat As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_ACTY_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCgCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCat, clsDB.DataType.DBString)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_ACTDTLList :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    '--execute SPP_RPT_SFMSCALLPRODDAILYACT @USER_ID , @VISIT_ID , @SALESREP_CODE , @CUST_CODE 
    Public Function GetCallProd_DailyActDtlList(ByVal strUserID As String, ByVal strVisitID As String, ByVal strSalesrepCode As String, ByVal strCustCode As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMSCALLPRODDAILYACT"
                .addItem("USER_ID", strUserID, 2)
                .addItem("VISIT_ID", strVisitID, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CUST_CODE", strCustCode, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_DailyActDtlList :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    '--EXEC SPP_RPT_SFMS_HDR_SFMSNO @USER_ID, @PRINCIPAL_ID @SALESREP_CODE, @VISIT, @CUSTCODE, @TXNDATE 
    'EXEC SPP_RPT_CALL_PRDY_DAILY_ACTY_DTL_TXNNO @USER_ID, @PRINCIPAL_ID,@PRINCIPAL_CODE, @SALESREP_CODE, @VISIT, @CUSTCODE, @TXNDATE 
    Public Function GetCallProd_DailyActDtl_HDR_SFMSNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_DAILY_ACTY_DTL_TXNNO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_DailyActDtl_HDR_SFMSNO :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_SFMS_HDR @USER_ID, @PRINCIPALID @SALESREP, @VISIT, @CUSTCODE, @TXNDATE, @SFMS_NO
    'execute SPP_RPT_CALL_PRDY_DAILY_ACTY_HDR @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @SALESREP_CODE, @VISIT_ID, @CUST_CODE, @TXN_DATE, @TXN_NO
    Public Function GetCallProd_DailyActDtl_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTXN_NO As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_DAILY_ACTY_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_DailyActDtl_HDR :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_SFMS_DET @SFMS_NO
    'execute SPP_RPT_CALL_PRDY_DAILY_ACTY_DTL @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @TEAM_CODE, @TXN_NO
    Public Function GetCallProd_DailyActDtl_DET(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strTxnDate As String, ByVal strTXN_NO As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_DAILY_ACTY_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_DailyActDtl_DET :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    Public Function GetCallProd_DailyActDtl_Img(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
       ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strTxnDate As String, ByVal strTXN_NO As String, _
       ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_DAILY_ACTY_DTL_IMAGE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_DailyActDtl_Img :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

#End Region

#Region "CALL COVERAGE"
    Public Function GetCallCoverage1(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String) As DataTable
        Dim clCallProdDB As clsDB
        Try
            clCallProdDB = New clsDB
            With clCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_COVERAGE_1"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallCoverage1 :" & ex.Message))
        Finally
            clCallProdDB = Nothing
        End Try
    End Function

    Public Function GetCallCoverage2(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String, ByVal intSelection As Integer) As DataTable
        Dim clCallProdDB As clsDB
        Try
            clCallProdDB = New clsDB
            With clCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_COVERAGE_2"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                .addItem("SELECTION", CStr(intSelection), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallCoverage2 :" & ex.Message))
        Finally
            clCallProdDB = Nothing
        End Try
    End Function
    Public Function GetCallCoverage1Adv(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String) As DataTable
        Dim clCallProdDB As clsDB
        Try
            clCallProdDB = New clsDB
            With clCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_COVERAGE_1_ADV"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallCoverage1ADV:" & ex.Message))
        Finally
            clCallProdDB = Nothing
        End Try
    End Function
#End Region

#Region "CALL BY CONTACT"
    'EXEC SPP_RPT_CALL_BY_CONT @USER_ID,@PRINCIPAL_ID,@PRINCIPAL_CODE, @YEAR, @MONTH, @TEAM_CODE, @SALESREP_CODE, @GRP_FIELD
    Public Function GetCallByContactClass(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strGroupFields As String) As DataTable
        Dim clCallProdDB As clsDB
        Try
            clCallProdDB = New clsDB
            With clCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_CONT"
                .addItem("USER_ID", strUserID, 2)
                .addItem("Principal_ID", strPrincipalID, 2)
                .addItem("Principal_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupFields, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByContactClass :" & ex.Message))
        Finally
            clCallProdDB = Nothing
        End Try
    End Function
#End Region

#Region "CALL BY SPECIALITY"
    'EXEC SPP_RPT_CALLBY_SPECIALTY @YEAR,@MONTH,@USER_ID,@AGENCY_CODE,@TEAM_CODE,@SALESMAN_CODE,@GRP_FIELD
    Public Function GetCallBySpeciality(ByVal strYear As String, ByVal strMonth As String, ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strTeamCode As String, ByVal strRegionCode As String, ByVal strSalesrepCode As String, ByVal strGroupFields As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_SPECIALITY"
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("USER_ID", strUserID, 2)
                .addItem("Principal_ID", strPrincipalID, 2)
                .addItem("Principal_CODE", strPrincipalCode, 2)
                .addItem("TEAM_CODE", strTeamCode, 2)
                .addItem("REGION_CODE", strRegionCode, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("GRP_FIELD", strGroupFields, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallBySpecialty :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function
#End Region

#Region "STRIKE CALL BY MONTH"
    'EXEC SPP_RPT_CALLBY_STRIKE @USER_ID,@PRINCIPAL_ID,@YEAR,@MONTH ,@SELECTION,@TEAM_CODE,@SALESREP_CODE
    Public Function GetCallByStrike(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalcode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSelection As String, ByVal strSalesRepList As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_STRIKE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalcode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("SELECTION", strSelection, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByStrike :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

#End Region

#Region "CUSTOMER STATUS"
    'EXEC SPP_RPT_CUST_STATUS @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @SELECTION, @NET_VALUE
    Public Function GetCustStatus(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strSalesrepCode As String, _
        ByVal intSelectionValue As Integer, ByVal intNetValue As Integer) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_STATUS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SELECTION", intSelectionValue.ToString, clsDB.DataType.DBInt)
                .addItem("NET_VALUE", intNetValue.ToString, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustStatus :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function
#End Region
#Region "CONTACT STATUS"
    'EXEC SPP_RPT_CUST_STATUS @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @SELECTION, @NET_VALUE
    Public Function GetContStatus(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strSalesrepCode As String, _
        ByVal intSelectionValue As Integer, ByVal intNetValue As Integer) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CONT_STATUS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SELECTION", intSelectionValue.ToString, clsDB.DataType.DBInt)
                .addItem("NET_VALUE", intNetValue.ToString, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustStatus :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function
#End Region
#Region "SFE KPI REPORT"
    Public Function GetSFEKPIList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strSalesrepList As String, _
        ByVal strMonth As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFEKPI"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("MONTH", strMonth.ToString, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFEKPIList :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function
#End Region
#Region "DRC Info"
    Public Function GET_DRC_DAILY_ACTY_HDR_TXNNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_TXNNO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_DRC_DAILY_ACTY_HDR_TXNNO :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    Public Function GET_DRC_DAILY_ACTY_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTxnNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_DRC_DAILY_ACTY_HDR :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    Public Function GET_DRC_DAILY_ACTY_DTL(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strTxnDate As String, ByVal strTxnNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_DRC_DAILY_ACTY_DTL :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

#End Region

#Region "MSS Info"
    Public Function GET_MSS_DAILY_ACTY_HDR_TXNNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_TXNNO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_MSS_DAILY_ACTY_HDR_TXNNO :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    Public Function GET_MSS_DAILY_ACTY_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTxnNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_MSS_DAILY_ACTY_HDR :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    Public Function GET_MSS_DAILY_ACTY_DTL(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strTxnDate As String, ByVal strTxnNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_MSS_DAILY_ACTY_DTL :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function
#End Region

#Region "DN Info"
    Public Function GET_DN_DAILY_ACTY_HDR_TXNNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DN_TXNNO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_DN_DAILY_ACTY_HDR_TXNNO :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    Public Function GET_DN_DAILY_ACTY_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTxnNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DN_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_DN_DAILY_ACTY_HDR :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    Public Function GET_DN_DAILY_ACTY_DTL(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strTxnDate As String, ByVal strTxnNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DN_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_DN_DAILY_ACTY_DTL :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function
#End Region

#Region "DET Info"
    Public Function GetDetInfo_HDR_DetNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DET_INFO_TXNNO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDetInfo_HDR_DetNO :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    Public Function GetDetInfo_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTXN_NO As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DET_INFO_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDetInfo_HDR :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    Public Function GetDetInfo_DET(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strTxnDate As String, ByVal strTXN_NO As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DET_INFO_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDetInfo_DET :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function
#End Region


#Region "Active/Non Active Customer"
    Public Function GetActiveCustomer(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_COVERAGE_1_ADV_ACTIVE_NON_ACTIVE_CUSTOMER"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("CLASS", strClass, clsDB.DataType.DBString)
                .addItem("IND", "ACTIVE", clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActiveCustomer :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

    Public Function GetNonActiveCustomer(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_COVERAGE_1_ADV_ACTIVE_NON_ACTIVE_CUSTOMER"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("CLASS", strClass, clsDB.DataType.DBString)
                .addItem("IND", "NONACTIVE", clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActiveCustomer :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function
#End Region



#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

    Public Enum MonthReportSelection
        JAN_To_JUNE = 0
        JUL_To_DEC = 1
        QUARTERLY = 2
    End Enum
End Class

