<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FieldForceCustBTCList.ascx.vb" Inherits="iFFMA_Mapping_FieldForceCustBTCList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldForceCustBTCPop" Src="FieldForceCustBTCPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlFieldForceCustBTC" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            
            <tr style="height:15px" align="left" valign="middle">
                <td>
                    <asp:HiddenField ID="hdType" runat="server" Value="0" />
                    <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdSalesrepCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdContCode" runat="server" Value="" />
                </td>
            </tr>
            
            <tr>
                <td align="center" style="width:95%;">
                    <ccGV:clsGridView ID="dgCustBTCList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
            </tr>
        </table>
        
        <customToolkit:wuc_FieldForceCustBTCPop ID="wuc_FieldForceCustBTCPop" Title="BTC Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
