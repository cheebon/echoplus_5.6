<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MSSDailyActy.aspx.vb" Inherits="MSSDailyActy" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>MSSDailyActy</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmMSSDailyActy" method="post" runat="server">
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    &nbsp;</td>
                                                                <td align="left">
                                                                    <table id="tblControl" runat="server" border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="btnBack" runat="server" CssClass="cls_button" PostBackUrl="~/iFFMR/SFE/CallAnalysis/MSSDailyActy.aspx"
                                                                                    ToolTip="Back To Previous Page." Text="Back"></asp:Button></td>
                                                                            <td align="left" style="width: 100%">
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Timer id="TimerControl_1" runat="server" enabled="False" interval="100">
                                                                                </asp:Timer>
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top" style="width: 50px" align="left">
                                                                                <asp:DetailsView ID="dvList_Left" runat="server" AutoGenerateRows="False" CssClass="Grid"
                                                                                    Width="100px" BorderWidth="0px" RowStyle-HorizontalAlign="Left" AlternatingRowStyle-HorizontalAlign="Left">
                                                                                    <Fields>
                                                                                        <asp:TemplateField HeaderText="Txn. No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:DropDownList ID="ddlTXNNO2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTXNNO2_SelectedIndexChanged"
                                                                                                    CssClass="cls_dropdownlist">
                                                                                                </asp:DropDownList>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="CUST_CODE" HeaderText="Customer A/C" ReadOnly="True" SortExpression="CUST_CODE" />
                                                                                        <asp:BoundField DataField="CUST_NAME" HeaderText="Name" ReadOnly="True" SortExpression="CUST_NAME" />
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                        <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS" />
                                                                                    </Fields>
                                                                                    <RowStyle CssClass="cls_label" />
                                                                                    <FieldHeaderStyle CssClass="cls_label_header" HorizontalAlign="Left" Width="15%" Wrap="False" />
                                                                                </asp:DetailsView>
                                                                            </td>
                                                                            <td valign="top" style="width: 50%" align="left">
                                                                                <asp:DetailsView ID="dvList_Right" runat="server" AutoGenerateRows="False" CssClass="Grid"
                                                                                    Width="100%" BorderWidth="0px" RowStyle-HorizontalAlign="Left" AlternatingRowStyle-HorizontalAlign="Left">
                                                                                    <Fields>
                                                                                        <asp:BoundField DataField="TITLE_CODE" HeaderText="Title Code" ReadOnly="True" SortExpression="TITLE_CODE" />
                                                                                        <asp:BoundField DataField="TITLE_NAME" HeaderText="Title Name" ReadOnly="True" SortExpression="TITLE_NAME" />
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                    </Fields>
                                                                                    <RowStyle CssClass="cls_label" />
                                                                                    <FieldHeaderStyle CssClass="cls_label_header" HorizontalAlign="Left" Width="15%" Wrap="False" />
                                                                                </asp:DetailsView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="height: 10px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;</td>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="455" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        RowHighlightColor="AntiqueWhite">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.</EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
