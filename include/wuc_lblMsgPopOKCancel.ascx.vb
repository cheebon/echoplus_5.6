
Partial Class include_wuc_lblMsgPopOKCancel
    Inherits System.Web.UI.UserControl
    Public Event OKButton_Click As EventHandler
    Public Event CancelButton_Click As EventHandler
    Private strTitle As String = String.Empty
    Private strMessage As String = String.Empty
    Private blnAlert As Boolean
    Private clrContentColor As Drawing.Color = Drawing.Color.Black
    Private strreturnvalue As Boolean

#Region "   Property Control"

    Public Property TitleOKCancel() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    Public Property MessageOKCancel() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property isAlertOKCancel() As Boolean
        Get
            Return blnAlert
        End Get
        Set(ByVal value As Boolean)
            blnAlert = value
        End Set
    End Property

    Public Property MessageColorOKCancel() As Drawing.Color
        Get
            Return clrContentColor
        End Get
        Set(ByVal value As Drawing.Color)
            clrContentColor = value
        End Set
    End Property

    Public Property ReturnValue() As Boolean
        Get
            Return strreturnvalue
        End Get
        Set(ByVal value As Boolean)
            strreturnvalue = value
        End Set
    End Property

#End Region


#Region "   Function Control"

    Public Sub Show()
        editBox_OK.Focus()
        DataBind()
        ModalPopupMessageOKCancel.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMessageOKCancel.Hide()
    End Sub

#End Region

#Region "Event Control"
    Public Overrides Sub DataBind()
        lblContent.ForeColor = IIf(blnAlert, Drawing.Color.Red, Drawing.Color.Black)
        lblTitle.Text = strTitle
        lblContent.Text = strMessage
        updPnlMsgPopOKCancel.Update()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DataBind()
    End Sub

    Protected Sub editBox_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles editBox_OK.Click
        Hide()
        ReturnValue = True
        RaiseEvent OKButton_Click(sender, e)
    End Sub

    Protected Sub editBox_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles editBox_Cancel.Click
        Hide()
        ReturnValue = False
        RaiseEvent CancelButton_Click(sender, e)
    End Sub

#End Region

End Class
