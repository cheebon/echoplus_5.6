<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerPlanListDetail"
    CodeFile="CustomerPlanListDetail.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                        </asp:Timer>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 100px" valign="top">
                                                                                <asp:Label ID="lblRouteDate" runat="server" Text="Route Date:" CssClass="cls_label_header"
                                                                                    Width="100px"></asp:Label></td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlDate" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True"
                                                                                    OnSelectedIndexChanged="ddlDate_SelectedIndexChanged">
                                                                                </asp:DropDownList></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl1" runat="server" Visible="False">
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl2" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl3" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl4" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl5" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl6" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl7" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    <asp:Panel ID="pnl8" runat="server" Visible="False">
                                                                        &nbsp;</asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="width: 164px">
                                                                    &nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
