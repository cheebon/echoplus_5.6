﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MSSEnquiry.aspx.vb" Inherits="iFFMR_Customize_MSSEnquiry" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSS Enquiry</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body style="margin: 0; border: 0; padding: 0;" class="BckgroundInsideContentLayout">
    <%--<span id="TopBar" style="display: inline; overflow: hidden; margin: 0; border: 0;
        padding: 0;">
        <iframe id="TopBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMA/SalesAccount/CustProfileMaintenance/CustProfileMaintenanceSearch.aspx"
            width="100%" height="180" scrolling="no" style="border: 0; position: relative;
            top: 0px;"></iframe>
    </span>--%>
    <span id="ContentBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="ContentBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../iFFMR/Customize/Geomap/MSSEnquiry/MSSEnquirySeach.aspx"
            width="100%" height="0" scrolling="auto" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span><span id="DetailBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="DetailBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" scrolling="auto" height="0" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span><span id="MapBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="MapBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" scrolling="auto" height="0" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span>
    
</body>
</html>
