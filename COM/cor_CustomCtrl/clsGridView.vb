Imports System
Imports System.Web.UI
Imports System.ComponentModel
Imports System.Web.UI.WebControls
Imports System.Text.RegularExpressions
Imports System.Collections.Specialized
Imports System.Reflection
Imports System.Drawing

<Assembly: TagPrefix("cor_CustomCtrl.clsGridView", "clsGridView")> 

'Namespace Tittle.Controls
'/ <summary>
'/ A Custom DataGrid which Allow freezing of Headers/Rwos/Columns
'/ http://www.codeproject.com/script/articles/list_articles.asp?userid=1654009
'/ </summary>
'<Editor("System.Web.UI.Design.WebControls.DataGridComponentEditor, System.Design, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", Type.GetType(ComponentEditor)), Designer("System.Web.UI.Design.WebControls.DataGridDesigner, System.Design, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")> _
Public Class clsGridView
    Inherits GridView

#Region "Private Variables"
    Private mFreezeHeader As Boolean = False
    Private mAddEmptyHeaders As Integer = 0
    Private mFreezeRows As Integer = 0
    Private mEmptyHeaderClass As String = ""
    Private mFreezeColumns As Integer = 0
    Private mGridHeight As Unit = Unit.Empty
    Private mGridWidth As Unit = Unit.Empty
    Private mBorderColor As Color = Color.Black
    Private mBorderWidth As Unit = 1
    'Private mRowHighlightColor As Color
#End Region

#Region "Constructors"
    '/ <summary>
    '/ default initialization with datagrid properties
    '/ </summary>
    Public Sub New()
        'set default appearence style
        Me.HeaderStyle.CssClass = "GridHeader"
        Me.FooterStyle.CssClass = "GridFooter"
        Me.RowStyle.CssClass = "GridNormal"
        Me.AlternatingRowStyle.CssClass = "GridAlternate"
        Me.CssClass = "Grid"
        'Me.BorderColor = System.Drawing.Color.FromArgb(47, 33, 98) '"#2f2162"
        'Me.BorderWidth = Unit.Parse("1")
        Me.GridLines = WebControls.GridLines.Both
        Me.CellPadding = 2
        Me.CellSpacing = 0
        Me.RowHighlightColor = Color.AntiqueWhite 'Color.FromName("#e6e6fa")
        Me.Attributes.Add("bordercolor", mBorderColor.Name)
        Me.Attributes.Add("borderwidth", mBorderWidth.Value)

        'paging
        'Me.PagerStyle.Mode = PagerMode.NumericPages
        'Me.PagerStyle.Position = PagerPosition.Bottom
        'Me.PagerStyle.HorizontalAlign = WebControls.HorizontalAlign.Center
        'Me.PagerStyle.ForeColor = System.Drawing.Color.FromArgb(0, 0, 255) '"#2f2162"
        'Me.PageSize = 10
    End Sub 'New

#End Region

#Region "Exposed Attributes"
    '/ <summary>
    '/ Freeze Header : True/False
    '/ 
    '/ Default: False
    '/ </summary>

    <Browsable(True), Category("Freeze"), DefaultValue("false"), Description("Freeze Header.")> _
    Public Property FreezeHeader() As Boolean
        Get
            Return mFreezeHeader
        End Get
        Set(ByVal Value As Boolean)
            mFreezeHeader = Value
            If FreezeHeader = True Then
                'this attribute requires .Net Framework 1.1 service pack 1 installed
                'on system, it generates headers in <th> tags instead of <td> tags.
                Me.UseAccessibleHeader = True
                'If user hasnt set height property set a default here.
                If Me.GridHeight = Unit.Empty Then
                    Me.GridHeight = 800
                End If
            End If
        End Set
    End Property

    '/ <summary>
    '/ Freeze Data Rows : Specify No. of Rows to Freeze from top excluding the header.
    '/ 
    '/ Default: 0
    '/ </summary>
    <Browsable(True), Category("Freeze"), DefaultValue("0"), Description("Freeze Data Rows.")> _
    Public Property FreezeRows() As Integer
        Get
            Return mFreezeRows
        End Get
        Set(ByVal Value As Integer)
            mFreezeRows = Value
            If FreezeRows >= 1 Then
                Me.FreezeHeader = True
            End If
        End Set
    End Property

    '/ <summary>
    '/ Specify here how many columns you want to freeze in the grid, 
    '/ freeze columns from the left.
    '/ 
    '/ Default: 0
    '/ </summary>
    <Browsable(True), Category("Freeze"), DefaultValue("0"), Description("Specify no. of columns to freeze in the grid, it freeze columns from the left.")> _
    Public Property FreezeColumns() As Integer
        Get
            Return mFreezeColumns
        End Get
        Set(ByVal Value As Integer)
            mFreezeColumns = Value
        End Set
    End Property

    '/ <summary>
    '/ this will add empty <tr><th></th>..</tr> rows
    '/ the benefit of it is that later we can through client side add/modify any text to individual cell 
    '/ it is done since through client side we can not add more than one row with <th> tags to table.
    '/ 
    '/ It is used when you set FreezeHeader to "true" and wants to have more than one such headers 
    '/ to be freezed but data of all such headers is decided on client side.
    '/ 
    '/ This is useful you want to keep a static row in all of the pages of a datagrid. 
    '/ </summary>
    <Browsable(True), Category("Freeze"), DefaultValue("0"), Description("It will add empty rows.")> _
    Public Property AddEmptyHeaders() As Integer
        Get
            Return mAddEmptyHeaders
        End Get
        Set(ByVal Value As Integer)
            mAddEmptyHeaders = Value
        End Set
    End Property

    '/ <summary>
    '/ Class of Empty rows <tr><th></th>.. </tr>
    '/ </summary>
    <Browsable(True), Category("Freeze"), Description("Class of empty rows.")> _
    Public Property EmptyHeaderClass() As String
        Get
            Return mEmptyHeaderClass
        End Get
        Set(ByVal Value As String)
            mEmptyHeaderClass = Value
        End Set
    End Property

    '/ <summary>
    '/ Grid height, this is actually height of <DIV> tag not of <table> which can be set through "Height"
    '/ </summary>
    <Browsable(True), Category("Freeze"), Description("Grid height, this is actually height of <DIV> tag not of <table> which can be set through Height.")> _
    Public Property GridHeight() As Unit
        Get
            Return mGridHeight
        End Get
        Set(ByVal Value As Unit)
            mGridHeight = Value
        End Set
    End Property

    '/ <summary>
    '/ Grid width, this is actually width of <DIV> tag not of <table> which can be set through "Width"
    '/ </summary>
    <Browsable(True), Category("Freeze"), Description("Grid width, this is actually width of <DIV> tag not of <table> which can be set through Width.")> _
    Public Property GridWidth() As Unit
        Get
            Return mGridWidth
        End Get
        Set(ByVal Value As Unit)
            mGridWidth = Value
        End Set
    End Property

    <Bindable(True), Category("Appearance"), TypeConverter(GetType(WebColorConverter)), Description("Specifies the color a row is highlighted when the mouse is over it.")> _
   Public Property RowHighlightColor() As Color
        Get
            Dim o As Object = ViewState("RowHighlightColor")
            If o Is Nothing Then
                'mRowHighlightColor = Color.Empty
                Return Color.Empty
            Else
                'mRowHighlightColor = o
                Return CType(o, Color)
            End If
        End Get
        Set(ByVal Value As Color)
            'mRowHighlightColor = Value
            ViewState("RowHighlightColor") = Value
        End Set
    End Property

    <DefaultValue(True), Description("Indicates whether or not rows are highlighted/clickable.")> _
    Public Property RowSelectionEnabled() As Boolean
        Get
            Dim o As Object = ViewState("RowSelectionEnabled")
            If o Is Nothing Then
                Return True
            Else
                Return CBool(o)
            End If
        End Get
        Set(ByVal Value As Boolean)
            ViewState("RowSelectionEnabled") = Value
        End Set
    End Property

    <Browsable(True), Category("Freeze"), DefaultValue("black"), Description("Reset the grid view border color.")> _
    Public Property GridBorderColor() As Color
        Get
            Return mBorderColor
        End Get
        Set(ByVal Value As Color)
            mBorderColor = Value
        End Set
    End Property


    <Browsable(True), Category("Freeze"), DefaultValue("black"), Description("Reset the grid view border color.")> _
    Public Property GridBorderWidth() As Unit
        Get
            Return mBorderWidth
        End Get
        Set(ByVal Value As Unit)
            mBorderWidth = Value
        End Set
    End Property

#End Region

#Region "Overriden events like OnPreRender / Render"
    Protected Overrides Function CreateRow(ByVal rowIndex As Integer, ByVal dataItemIndex As Integer, ByVal rowType As DataControlRowType, ByVal rowState As DataControlRowState) As GridViewRow

        ' Create the clsGridViewRow
        Dim row As New clsGridViewRow(rowIndex, dataItemIndex, rowType, rowState)

        '' Set the client-side onmouseover and onmouseout if RowSelectionEnabled == true
        'If RowSelectionEnabled And rowType <> DataControlRowType.Header And rowType <> DataControlRowType.Footer And rowType <> DataControlRowType.Pager Then
        '    row.Attributes("onmouseover") = "javascript:prettyDG_changeBackColor(this, true);"
        '    row.Attributes("onmouseout") = "javascript:prettyDG_changeBackColor(this, false);"
        'End If

        ' return the clsGridViewRow
        Return row
    End Function

    Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
        MyBase.OnPreRender(e)
        '#Region "DataGrid freezing script"
        'DataGrid functions/styles etc.        
        If Not Page.ClientScript.IsClientScriptBlockRegistered("FreezeGridScript") Then
            Dim freezeGridScript As String = " function FreezeGridColumns(dgTbl, colNo)" & _
                                            " {" & _
                                            " var tbl = document.getElementById(dgTbl);" & _
                                            " for ( var i=0; i<tbl.rows.length; i++)" & _
                                            " {" & _
                                            " for ( var j=0; j<colNo; j++)" & _
                                            " {" & _
                                            " tbl.rows[i].cells[j].className = 'locked';" & _
                                            " }" & _
                                            " }" & _
                                            " }"
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "FreezeGridScript", "<script language=javascript>" + freezeGridScript + "</script>")
        End If

        'Trim space from the div and datagrid table.
        Dim gridOnLoad As String = " var divTbl = document.getElementById('div_" + Me.ID + "');" & _
                                    " var resultsTable = divTbl.children[0];" & _
                                    " if (typeof(resultsTable) != 'undefined')" & _
                                    " {" & _
                                    " if (divTbl.offsetHeight + 3 > resultsTable.offsetHeight) {" & _
                                    " divTbl.style.height = resultsTable.offsetHeight + 50;" & _
                                    " }" & _
                                    " }"

        'If <div> generated then only
        If mFreezeColumns >= 1 Or mFreezeHeader = True Then
            Page.ClientScript.RegisterStartupScript(Me.GetType, "GridOnLoad", gridOnLoad, True)
        End If

        If Not RowSelectionEnabled Then
            Return ' exit if not RowSelectionEnabled == true
        End If

    End Sub

    '/ <summary>
    '/ Render datagridservercontrol
    '/ </summary>
    '/ <param name="output"></param>
    Protected Overrides Sub Render(ByVal output As HtmlTextWriter)
        If mFreezeRows >= 1 Or mAddEmptyHeaders >= 1 Or mFreezeColumns >= 1 Or mFreezeHeader = True Then

            '#Region "Only execute when need to any of this: Freeze Header, Freeze Row, Add Empty Header, Freeze Columns"  
            'Get the output string rendered.
            Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder()
            Dim writer As HtmlTextWriter = New HtmlTextWriter(New System.IO.StringWriter(sb))
            MyBase.Render(writer)

            'the output has been retrieved in the stringbuilder AND do the modification here      
            Dim datgridString As String = sb.ToString()

            'FREEZE MORE ROWS OTHER THAN JUST HEADER
            If mFreezeRows >= 1 Then
                Dim cnt As Integer = 0
                Dim i As Integer
                For i = 0 To mFreezeRows Step i + 1
                    cnt = datgridString.IndexOf("</tr>", cnt)
                    If i < mFreezeRows Then
                        cnt = cnt + 1
                    End If
                Next
                If cnt <> -1 Then
                    Dim firstpart As String, secondpart As String
                    firstpart = datgridString.Substring(0, cnt)
                    secondpart = datgridString.Substring(cnt)

                    firstpart = Regex.Replace(firstpart, "<td", "<th class='innerBorder' ")
                    firstpart = Regex.Replace(firstpart, "</td>", "</th>")

                    datgridString = firstpart + secondpart
                End If
            End If

            'ADD EMPTY HEADERS. <TH>
            'This all done as through client side JAVASCRIPT you can not add more than one row TH 
            'tag to existing TABLE.
            If mAddEmptyHeaders >= 1 Then
                Dim rowData As String = ""
                Dim i As Integer
                For i = 1 To mAddEmptyHeaders Step i + 1
                    rowData += "<TR class='" + mEmptyHeaderClass + "' >"

                    Dim j As Integer
                    For j = 0 To Me.Columns.Count - 1 Step j + 1
                        Dim clsname As String = ""
                        If j = 0 Then
                            clsname = "leftBorder"
                        End If
                        If j = Me.Columns.Count - 1 Then
                            clsname = "rightBorder"
                        End If
                        If j > 0 And j < Me.Columns.Count Then
                            clsname = "innerBorder"
                        End If

                        rowData += "<TH class='" + clsname + "' >&nbsp;</TH>"
                    Next
                    rowData += "</TR>"
                Next
                If mAddEmptyHeaders >= 1 Then
                    Dim headerEnd As Integer = datgridString.IndexOf("</tr>", 0)
                    Dim prePart As String = datgridString.Substring(0, headerEnd + 5)
                    Dim postPart As String = datgridString.Substring(headerEnd + 5)
                    datgridString = prePart + rowData + postPart
                End If
            End If

            If mFreezeColumns >= 1 Then
                If Me.GridWidth = Unit.Empty Then
                    Me.GridWidth = 800
                End If

                Dim freezeGridColumn As String = "FreezeGridColumns('" + Me.ID + "'," + mFreezeColumns.ToString() + ");"
                Page.ClientScript.RegisterStartupScript(Me.GetType, "freezeGridColumn", "<script language=javascript>\n" + freezeGridColumn + "\n</script>")
            End If

            If mFreezeColumns >= 1 Or mFreezeHeader = True Then
                Dim divstring As String
                divstring = "<div id='div_" + Me.ID + "' style='"
                If mFreezeHeader = True Then
                    divstring += " HEIGHT:" + Me.GridHeight.ToString + "; "
                End If
                If mFreezeColumns >= 1 Then
                    divstring += " WIDTH:" + Me.GridWidth.ToString + "; "
                End If

                datgridString = divstring + " OVERFLOW:auto; ' >" + datgridString + "</div>"
            End If

            output.Write(datgridString.ToString())

            '#End Region

        Else
            MyBase.Render(output)
        End If
    End Sub
#End Region

    Public Class clsGridViewRow
        Inherits GridViewRow
        'Implements IPostBackEventHandler 'ToDo: Add Implements Clauses for implementation methods of these interface(s)

        Public Sub New(ByVal rowIndex As Integer, ByVal dataItemIndex As Integer, ByVal rowType As DataControlRowType, ByVal rowState As DataControlRowState)
            MyBase.New(rowIndex, dataItemIndex, rowType, rowState)
        End Sub
    End Class
End Class
'End Namespace
'-------------------------------------------------