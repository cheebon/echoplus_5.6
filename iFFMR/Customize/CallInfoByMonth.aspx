<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallInfoByMonth.aspx.vb" Inherits="iFFMR_Customize_CallInfoByMonth" %>
<%@ Register TagPrefix="ucUpdateProgress" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ucpnlRecordNotFound" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="ucctrlpanel" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx"  %>
<%@ Register TagPrefix="uclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="ucMultiAuthen" TagName="wuc_MultiAuthen" Src="~/include/wuc_MultiAuthen.ascx"  %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Call Info By Month</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout"> 
    <form id="frmSalesForceCapacity" method="post" runat="server">
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <ucctrlpanel:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true" /> 
                                </td>
                            </tr>
                            <tr><td style="height: 2px"></td></tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td style="width: 100%">
                                                <uclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" /> 
                                            </td>
                                        </tr>
                                        <tr><td class="BckgroundBenealthTitle"></td></tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <ucUpdateProgress:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr><td><ucMultiAuthen:wuc_MultiAuthen ID="wuc_MultiAuthen" runat="server" Visible="true" /></td></tr>
                                                            <tr align="right">
                                                                <td >
                                                                    <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                                   <asp:Label ID="lblSelectedPrdSpecialty" runat="server" CssClass="cls_label_header" Text="Product Specialty : " />
                                                                    <asp:DropDownList ID="ddlSelectedPrdSpecialty" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true">
                                                                    </asp:DropDownList> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="width:95%;">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="false" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <ucpnlRecordNotFound:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>        
    </form>
</body>
</html>
