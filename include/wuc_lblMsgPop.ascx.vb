
Partial Class include_wuc_lblMsgPop
    Inherits System.Web.UI.UserControl

    Public Event CloseButton_Click As EventHandler
    Private strTitle As String = "Message"
    Private strMessage As String = String.Empty
    Private blnAlert As Boolean
    Private clrContentColor As Drawing.Color = Drawing.Color.Black

#Region "   Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Indicate the message is Alert or normal message
    ''' </summary>
    ''' <value>Set to true then Red color, else Black</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property isAlert() As Boolean
        Get
            Return blnAlert
        End Get
        Set(ByVal value As Boolean)
            blnAlert = value
        End Set
    End Property

    ''' <summary>
    ''' Used to customize the message color, default is black
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property MessageColor() As Drawing.Color
        Get
            Return clrContentColor
        End Get
        Set(ByVal value As Drawing.Color)
            clrContentColor = value
        End Set
    End Property

#End Region

#Region "   Function Control"
    ''' <summary>
    ''' Activate to popup display message box
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Show()
        btnClose.Focus()
        DataBind()
        ModalPopupMessage.Show()
    End Sub

    ''' <summary>
    ''' Disactivate to hide / close the message box
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Hide()
        ModalPopupMessage.Hide()
    End Sub
#End Region

#Region "Event Control"
    Public Overrides Sub DataBind()
        lblContent.ForeColor = IIf(blnAlert, Drawing.Color.Red, Drawing.Color.Black)
        lblTitle.Text = strTitle
        lblContent.Text = strMessage
        updPnlMsgPop.Update()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DataBind()
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Hide()
        RaiseEvent CloseButton_Click(sender, e)
    End Sub
#End Region

End Class
