'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	11/03/2008
'	Purpose	    :	Toolbar 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |        	   	|       	        |                        |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data
Imports rpt_Customize
Imports mst_Common

Partial Class include_wuc_toolbar
    Inherits System.Web.UI.UserControl
    Private lngSubModuleID As SubModuleType

    Public Event EnqSearchBtn_Click As EventHandler
    Public Event GPSBtn_Click As EventHandler
    Public Event BackBtn_Click As EventHandler
    Public Event ExportBtn_Click As EventHandler
    Public Event KMLBtn_Click As EventHandler
#Region "PROPERTY"
    Public Property EnqCollapsed() As Boolean
        Get
            Return CPE_PnlEnquiry.Collapsed
        End Get
        Set(ByVal value As Boolean)
            CPE_PnlEnquiry.ClientState = (value).ToString.ToLower
            CPE_PnlEnquiry.Collapsed = value
            UpdateCtrlPanel.Update()
        End Set
    End Property

    Public Property SubModuleID() As SubModuleType
        Get
            Return lngSubModuleID
        End Get
        Set(ByVal Value As SubModuleType)
            lngSubModuleID = Value
        End Set
    End Property

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_Toolbar"
        End Get
    End Property

    Public Property InfoString() As String
        Get
            Return Trim(lblInfo.Text)
        End Get
        Set(ByVal value As String)
            lblInfo.Text = value
        End Set
    End Property

    '----BEGIN ADD HERE-----'
    Public Property TeamCode() As String
        Get
            Return Trim(ddlTeam.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlTeam.SelectedValue = value
        End Set
    End Property

    Public Property SalesrepName() As String
        Get
            Return ddlSalesrep.SelectedItem.Text.Trim()
        End Get
        Set(ByVal value As String)
            ddlSalesrep.SelectedItem.Text = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(ddlSalesrep.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlSalesrep.SelectedValue = value
        End Set
    End Property

    Public Property TechnicianCode() As String
        Get
            Return Trim(ddlTechnician.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlTechnician.SelectedValue = value
        End Set
    End Property

    Public Property RouteCode() As String
        Get
            Return Trim(ddlRoute.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlRoute.SelectedValue = value
        End Set
    End Property

    Public Property RouteName() As String
        Get
            Return ddlRoute.SelectedItem.Text.Trim
        End Get
        Set(ByVal value As String)
            ddlRoute.SelectedItem.Text = value
        End Set
    End Property

    Public Property Year() As String
        Get
            Return Trim(ddlYear.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlYear.SelectedValue = value
        End Set
    End Property

    Public Property Month() As String
        Get
            Return Trim(ddlMonth.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlMonth.SelectedValue = value
        End Set
    End Property

    Public Property SearchType() As String
        Get
            Return Trim(ddlSearchType.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlSearchType.SelectedValue = value
        End Set
    End Property

    Public Property SearchValue() As String
        Get
            Return Trim(txtSearchValue.Text)
        End Get
        Set(ByVal value As String)
            txtSearchValue.Text = value
        End Set
    End Property

    Public Property Status() As String
        Get
            Return Trim(ddlStatus.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlStatus.SelectedValue = value
        End Set
    End Property

    Public Property GPSStatus() As String
        Get
            Return Trim(ddlGPSStatus.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlGPSStatus.SelectedValue = value
        End Set
    End Property
    Public Property ReasonType() As String
        Get
            Return Trim(ddlReasonType.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlReasonType.SelectedValue = value
        End Set
    End Property

    Public Property ViewType() As String
        Get
            Return Trim(ddlViewType.selectedvalue)
        End Get
        Set(ByVal value As String)
            ddlViewType.selectedvalue = value
        End Set
    End Property

    Public Property TxnDateFrom() As String
        Get
            Return Trim(txtTxnDate.DateStart)
        End Get
        Set(ByVal value As String)
            txtTxnDate.DateStart = value
        End Set
    End Property

    Public Property TxnDateTo() As String
        Get
            Return Trim(txtTxnDate.DateEnd)
        End Get
        Set(ByVal value As String)
            txtTxnDate.DateEnd = value
        End Set
    End Property

   
    Public Property GPSButtonVisibility() As Boolean
        Get
            Return btnGPS.Visible
        End Get
        Set(ByVal value As Boolean)
            btnGPS.Visible = value
        End Set
    End Property
    Public Property KMLButtonVisibility() As Boolean
        Get
            Return btnKML.Visible
        End Get
        Set(ByVal value As Boolean)
            btnKML.Visible = value
        End Set
    End Property
    Public Property CustName() As String
        Get
            Return Trim(txtCustName.Text)
        End Get
        Set(ByVal value As String)
            txtCustName.Text = value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return txtAddress.Text.Trim
        End Get
        Set(ByVal value As String)
            txtAddress.Text = value
        End Set
    End Property

    Public Property District() As String
        Get
            Return txtDistrict.Text.Trim
        End Get
        Set(ByVal value As String)
            txtDistrict.Text = value
        End Set
    End Property

    Public Property CustGrp() As String
        Get
            'Return txtCustGrp.Text.Trim
            If ddlCustGrp.SelectedIndex = 0 Then
                Return ""
            End If
            Return ddlCustGrp.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            'txtCustGrp.Text = value
            ddlCustGrp.SelectedItem.Text = value
        End Set
    End Property

    Public Property CustClass() As String
        Get
            Return ddlCustClass.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlCustClass.SelectedValue = value
        End Set
    End Property

    Public Property CustType() As String
        Get
            Return ddlCustType.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlCustType.SelectedValue = value
        End Set
    End Property
    Public Property CustTypeName() As String
        Get
            Return ddlCustType.SelectedItem.Text.Trim
        End Get
        Set(ByVal value As String)
            ddlCustType.SelectedItem.Text = value
        End Set
    End Property
    Public Property MSSTitle() As String
        Get
            Return ddlMssTitle.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlMssTitle.SelectedValue = value
        End Set
    End Property

    Public Property MSSTitleName() As String
        Get
            If ddlMssTitle.SelectedIndex <> 0 Then
                Return ddlMssTitle.SelectedItem.Text
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ddlMssTitle.SelectedValue = value
        End Set
    End Property

    Public Property MSSQues() As String
        Get
            Return ddlMSSQuestion.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlMSSQuestion.SelectedValue = value
        End Set
    End Property
    Public Property MSSQuesName() As String
        Get
            If ddlMSSQuestion.SelectedIndex <> 0 Then
                Return ddlMSSQuestion.SelectedItem.Text
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ddlMSSQuestion.SelectedValue = value
        End Set
    End Property
    Public Property MSSSubQues() As String
        Get
            Return ReturnSubQuesCode(ddlMSSSubQuestion.SelectedValue).Trim
        End Get
        Set(ByVal value As String)
            ddlMSSSubQuestion.SelectedValue = value
        End Set
    End Property
    Public Property MSSSubQuesName() As String
        Get
            If ddlMSSSubQuestion.SelectedIndex <> 0 Then
                Return ddlMSSSubQuestion.SelectedItem.Text
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ddlMSSSubQuestion.SelectedValue = value
        End Set
    End Property
    Public Property MssStartDate() As String
        Get
            Return txnMssTitleDate.DateStart
        End Get
        Set(ByVal value As String)
            txnMssTitleDate.DateStart = value
        End Set
    End Property

    Public Property MssEndDate() As String
        Get
            Return txnMssTitleDate.DateEnd.Trim
        End Get
        Set(ByVal value As String)
            txnMssTitleDate.DateEnd = value
        End Set
    End Property

    Public Property SFMSCategory() As String
        Get
            Return ddlSFMSCategory.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlSFMSCategory.SelectedValue = value
        End Set
    End Property

    Public Property SFMSCategoryName() As String
        Get
            If ddlSFMSCategory.SelectedIndex <> 0 Then
                Return ddlSFMSCategory.SelectedItem.Text.Trim
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ddlSFMSCategory.SelectedValue = value
        End Set
    End Property

    Public Property SFMSSubCategory() As String
        Get
            Return ddlSFMSSubCategory.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlSFMSSubCategory.SelectedValue = value
        End Set
    End Property

    Public Property SFMSSubCategoryName() As String
        Get
            If ddlSFMSSubCategory.SelectedIndex <> 0 Then
                Return ddlSFMSSubCategory.SelectedItem.Text.Trim
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ddlSFMSSubCategory.SelectedValue = value
        End Set
    End Property

    Public Property SFMSStartDate() As String
        Get
            Return txnSFMSTitleDate.DateStart
        End Get
        Set(ByVal value As String)
            txnMssTitleDate.DateStart = value
        End Set
    End Property

    Public Property SFMSEndDate() As String
        Get
            Return txnSFMSTitleDate.DateEnd.Trim
        End Get
        Set(ByVal value As String)
            txnMssTitleDate.DateEnd = value
        End Set
    End Property

    Public Property MtdSalesStartRange() As String
        Get
            Return MtdSalesRange.ValueStart.Trim
        End Get
        Set(ByVal value As String)
            MtdSalesRange.ValueStart = value
        End Set
    End Property

    Public Property MtdSalesEndRange() As String
        Get
            Return MtdSalesRange.ValueEnd.Trim
        End Get
        Set(ByVal value As String)
            MtdSalesRange.ValueEnd = value
        End Set
    End Property

    Public Property YtdSalesStartRange() As String
        Get
            Return YtdSalesRange.ValueStart.Trim
        End Get
        Set(ByVal value As String)
            YtdSalesRange.ValueStart = value
        End Set
    End Property

    Public Property YtdSalesEndRange() As String
        Get
            Return YtdSalesRange.ValueEnd.Trim
        End Get
        Set(ByVal value As String)
            YtdSalesRange.ValueEnd = value
        End Set
    End Property

    Public Property NoSkuStartRange() As String
        Get
            Return NoSkuRange.ValueStart.Trim
        End Get
        Set(ByVal value As String)
            NoSkuRange.ValueStart = value
        End Set
    End Property

    Public Property NoSkuEndRange() As String
        Get
            Return NoSkuRange.ValueEnd.Trim
        End Get
        Set(ByVal value As String)
            NoSkuRange.ValueEnd = value
        End Set
    End Property

    Public Property MTeam() As String
        Get
            Return ddlMapTeam.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlMapTeam.SelectedValue = value
        End Set
    End Property
    Public Property MTeamName() As String
        Get
            If ddlMapTeam.SelectedIndex = 0 Then
                Return ""
            Else
                Return ddlMapTeam.SelectedItem.Text.Trim
            End If

        End Get
        Set(ByVal value As String)
            ddlMapTeam.SelectedItem.Text = value
        End Set
    End Property
    Public Property MSalesrep() As String
        Get
            Return ddlMapSalesrep.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlMapSalesrep.SelectedValue = value
        End Set
    End Property
    Public Property MSalesrepName() As String
        Get
            If ddlMapSalesrep.SelectedIndex = 0 Then
                Return ""
            Else
                Return ddlMapSalesrep.SelectedItem.Text.Trim
            End If

        End Get
        Set(ByVal value As String)
            ddlMapSalesrep.SelectedItem.Text = value
        End Set
    End Property
    Public Property SKU() As String
        Get
            Return ddlSKU.SelectedValue.Trim
        End Get
        Set(ByVal value As String)
            ddlSKU.SelectedValue = value
        End Set
    End Property
    Public Property SKUName() As String
        Get
            If ddlSKU.SelectedIndex = 0 Then
                Return ""
            Else
                Return ddlSKU.SelectedItem.Text.Trim
            End If

        End Get
        Set(ByVal value As String)
            ddlSKU.SelectedItem.Text = value
        End Set
    End Property
    Public ReadOnly Property SubQuesType() As String
        Get
            Return ReturnAnswerType(ddlMSSSubQuestion.SelectedValue)
        End Get

    End Property


    Public ReadOnly Property MssAnswer() As String
        Get
            Select Case ReturnAnswerType(ddlMSSSubQuestion.SelectedValue)
                Case SubQuestionType.Freetext
                    Return txtMssTextAnswer.Text.Trim
                Case SubQuestionType.Radio
                    Return radMssRadioAnswer.SelectedValue
                Case SubQuestionType.Selection
                    Return ddlMssDDLAnswer.SelectedValue
                Case Else
                    Return ""
            End Select
        End Get
    End Property
    Public ReadOnly Property MssAnswerName() As String
        Get
            Select Case ReturnAnswerType(ddlMSSSubQuestion.SelectedValue)
                Case SubQuestionType.Freetext
                    Return txtMssTextAnswer.Text.Trim
                Case SubQuestionType.Radio
                    Return radMssRadioAnswer.SelectedItem.Text
                Case SubQuestionType.Selection
                    Return ddlMssDDLAnswer.SelectedItem.Text
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    '----END ADD HERE-----'
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Apply for all reports, check for the session
            If Session("UserID") Is Nothing OrElse Session("UserID") = "" Then
                Dim strScript As String = ""
                strScript = "self.parent.parent.location='" & ResolveClientUrl("~/login.aspx") & "?ErrMsg=Session Time Out, Please login again !!!';"
                ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Timeout", strScript, True)
            End If
            'End Session Check
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Public Overloads Sub DataBind()
        Try
            Dim lstItem As New ListItem("All", "ALL")

            Select Case lngSubModuleID
                '----BEGIN ADD HERE-----'
                Case SubModuleType.COMPANY
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Company Code", "COMPANY_CODE"))
                        .Items.Insert(2, New ListItem("Company Name", "COMPANY_NAME"))
                        .Items.Insert(3, New ListItem("Prefix", "PREFIX"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.SALESAREA
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Sales Area Code", "SALES_AREA_CODE"))
                        .Items.Insert(2, New ListItem("Sales Area Name", "SALES_AREA_NAME"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.WAREHOUSE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Warehouse Code", "WHS_CODE"))
                        .Items.Insert(2, New ListItem("Warehouse Name", "WHS_DESC"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.REGION
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Region Code", "REGION_CODE"))
                        .Items.Insert(2, New ListItem("Region Name", "REGION_NAME"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.SALESTEAM
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Team Code", "TEAM_CODE"))
                        .Items.Insert(2, New ListItem("Team Name", "TEAM_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlStatus.Visible = True

                Case SubModuleType.CUSTOMER
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(2, New ListItem("Customer Name", "CUST_NAME"))

                        .SelectedIndex = 1
                    End With
                    pnlStatus.Visible = True
                    pnlGPSStatus.Visible = True
                    btnGPS.Visible = False
                Case SubModuleType.CUSTGENERATELOC
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(2, New ListItem("Customer Name", "CUST_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlStatus.Visible = True
                    btnGPS.Visible = False

                Case SubModuleType.SHIPTO
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(2, New ListItem("Shipto Code", "SHIPTO_CODE"))
                        .Items.Insert(3, New ListItem("Shipto Name", "SHIPTO_NAME"))
                        .SelectedIndex = 2
                    End With
                    pnlStatus.Visible = True

                Case SubModuleType.CONTACT
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Contact Code", "CONT_CODE"))
                        .Items.Insert(2, New ListItem("Contact Name", "CONT_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlStatus.Visible = True

                Case SubModuleType.COMPANYCODE_BUPROFILE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Company Code", "COMP_CODE"))
                        .Items.Insert(2, New ListItem("Business Unit", "BU_CODE"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.COMPANYCODE_BANKPROFILE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Company Code", "COMP_CODE"))
                        .Items.Insert(2, New ListItem("Business Unit", "BU_CODE"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.COLLECTORPROFILE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Country Code", "COUNTRY_CODE"))
                        .Items.Insert(2, New ListItem("Collector SAP Code", "COLLECTOR_SAP_CODE"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.COLLECTORCUSTOMERMAPPING
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Collector SAP Code", "COLLECTOR_SAP_CODE"))
                        .Items.Insert(2, New ListItem("Customer Code", "CUST_CODE"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.PRODUCTMUSTSELL
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Team Code", "TEAM"))
                        .Items.Insert(2, New ListItem("Param Type", "PARAM_NAME"))
                        .Items.Insert(3, New ListItem("Product Code", "PRD"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.PRODUCTMUSTSELLADV
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Team Code", "TEAM"))
                        .Items.Insert(2, New ListItem("Product Code", "PRD"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.SFMS_CFG
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Team Code", "TEAM"))
                        .Items.Insert(2, New ListItem("Category Code", "CAT"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.MSS_CFG
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Team Code", "TEAM"))
                        .Items.Insert(2, New ListItem("Title Code", "TITLE"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.LEAD
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Team Code", "TEAM"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.PRODUCT
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Supplier Code", "SUPPLIER_CODE"))
                        .Items.Insert(2, New ListItem("Product Group Code", "PRD_GRP_CODE"))
                        .Items.Insert(3, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(4, New ListItem("Product Name", "PRD_NAME"))
                        .SelectedIndex = 3
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlStatus.Visible = True

                Case SubModuleType.PRODUCTGROUP
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Supplier Code", "SUPPLIER_CODE"))
                        .Items.Insert(2, New ListItem("Product Group Code", "PRD_GRP_CODE"))
                        .Items.Insert(3, New ListItem("Product Group Name", "PRD_GRP_NAME"))
                        .SelectedIndex = 2
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlStatus.Visible = True

                Case SubModuleType.SUPPLIER
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Supplier Code", "SUPPLIER_CODE"))
                        .Items.Insert(2, New ListItem("Supplier Name", "SUPPLIER_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlStatus.Visible = True

                Case SubModuleType.STOCKALLOC
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()

                Case SubModuleType.PRODUCT_MATRIX
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Customer Group Code", "CUST_GRP_CODE"))
                        .Items.Insert(4, New ListItem("Customer Group Name", "CUST_GRP_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.STANDARDPRICE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Region Code", "REGION_CODE"))
                        .Items.Insert(4, New ListItem("Region Name", "REGION_NAME"))
                        .Items.Insert(5, New ListItem("UOM Code", "UOM_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.SPECIALPRICE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(4, New ListItem("Customer Name", "CUST_NAME"))
                        .Items.Insert(5, New ListItem("UOM Code", "UOM_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.CUSTGRPPRICE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Region Code", "REGION_CODE"))
                        .Items.Insert(4, New ListItem("Region Name", "REGION_NAME"))
                        .Items.Insert(5, New ListItem("Customer Group Code", "CUST_GRP_CODE"))
                        .Items.Insert(6, New ListItem("Customer Group Name", "CUST_GRP_NAME"))
                        .Items.Insert(7, New ListItem("UOM Code", "UOM_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.PRICEGRPPRICE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Region Code", "REGION_CODE"))
                        .Items.Insert(4, New ListItem("Region Name", "REGION_NAME"))
                        .Items.Insert(5, New ListItem("Price Group Code", "PRICE_GRP_CODE"))
                        .Items.Insert(6, New ListItem("UOM Code", "UOM_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.STANDARDBONUS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.SPECIALBONUS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(4, New ListItem("Customer Name", "CUST_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.CUSTGRPBONUS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Customer Group Code", "CUST_GRP_CODE"))
                        .Items.Insert(4, New ListItem("Customer Group Name", "CUST_GRP_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.PRICEGRPBONUS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Product Name", "PRD_NAME"))
                        .Items.Insert(3, New ListItem("Price Group Code", "PRICE_GRP_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.PACKAGE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Package Code", "PACKAGE_CODE"))
                        .Items.Insert(2, New ListItem("Package Name", "PACKAGE_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.FIELDACTY
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Category Code", "CAT_CODE"))
                        .Items.Insert(2, New ListItem("Category Name", "CAT_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.MSSADV
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Title Code", "TITLE_CODE"))
                        .Items.Insert(2, New ListItem("Title Name", "TITLE_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlStatus.Visible = True

                    '----------------MSSADV2
                Case SubModuleType.MSSADV2
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Title Code", "TITLE_CODE"))
                        .Items.Insert(2, New ListItem("Title Name", "TITLE_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlStatus.Visible = True

                Case SubModuleType.FIELDFORCE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Field Force Code", "SALESREP_CODE"))
                        .Items.Insert(2, New ListItem("Field Force Name", "SALESREP_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlStatus.Visible = True
                    lblTeamastr.Visible = False




                Case SubModuleType.SALESTGT
                    pnlSearch.Visible = False
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()
                    pnlYear.Visible = True
                    LoadDDLYear()

                Case SubModuleType.REASON
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Reason Code", "REASON_CODE"))
                        .Items.Insert(2, New ListItem("Reason Name", "REASON_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlReasonType.Visible = True
                    LoadDDLReasonType()

                Case SubModuleType.ROUTE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Route Code", "ROUTE_CODE"))
                        .Items.Insert(2, New ListItem("Route Name", "ROUTE_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.PRICEGROUP
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Price Group Code", "PRICE_GRP_CODE"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.INBOX, SubModuleType.OUTBOX, SubModuleType.STOCKMOVE, SubModuleType.EASYLOADER
                    hide()
                    pnlBack.Visible = False

                Case SubModuleType.FIELDFORCECUST
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(2, New ListItem("Customer Name", "CUST_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()

                Case SubModuleType.FIELDFORCEROUTE
                    pnlSearch.Visible = False
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()
                    pnlRoute.Visible = True
                    LoadDDLRoute()

                Case SubModuleType.FIELDFORCEPRDGRP
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Group Code", "PRD_GRP_CODE"))
                        .Items.Insert(2, New ListItem("Product Group Name", "PRD_GRP_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()

                Case SubModuleType.FIELDFORCEACTY
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Category Code", "CAT_CODE"))
                        .Items.Insert(2, New ListItem("Category Name", "CAT_NAME"))
                        .Items.Insert(3, New ListItem("Sub Category Code", "SUB_CAT_CODE"))
                        .Items.Insert(4, New ListItem("Sub Category Name", "SUB_CAT_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()

                Case SubModuleType.FIELDFORCEMSS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Title Code", "TITLE_CODE"))
                        .Items.Insert(2, New ListItem("Title Name", "TITLE_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()

                Case SubModuleType.FIELDFORCEPACKAGE
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Package Code", "PACKAGE_CODE"))
                        .Items.Insert(2, New ListItem("Package Name", "PACKAGE_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlSalesrep.Visible = True
                    LoadDDLSalesrep()

                Case SubModuleType.STOCKADJUST
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Transaction No", "TXN_NO"))
                        .Items.Insert(2, New ListItem("Field Force Code", "SALESREP_CODE"))
                        .Items.Insert(3, New ListItem("Serial No", "SERIAL_NO"))
                        .Items.Insert(4, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(5, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(6, New ListItem("Status Code", "STATUS_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlTxnDate.Visible = True

                Case SubModuleType.STOCKHIST, SubModuleType.STOCKSTATUS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Transaction No", "TXN_NO"))
                        .Items.Insert(2, New ListItem("Field Force Code", "SALESREP_CODE"))
                        .Items.Insert(3, New ListItem("Serial No", "SERIAL_NO"))
                        .SelectedIndex = 1
                    End With
                    pnlTxnDate.Visible = True

                Case SubModuleType.NEWEQPLIST
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Transaction No", "TXN_NO"))
                        .Items.Insert(2, New ListItem("Technician Code", "TECHNICIAN_CODE"))
                        .Items.Insert(3, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(4, New ListItem("Contact Code", "CONT_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlViewType.Visible = True

                Case SubModuleType.STOCKTRANSFERLIST, SubModuleType.APPLYSTOCKLIST
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Transaction No", "TXN_NO"))
                        .Items.Insert(2, New ListItem("Technician Code", "TECHNICIAN_CODE"))
                        .SelectedIndex = 1
                    End With
                    pnlViewType.Visible = True

                Case SubModuleType.TECHNICIANSALESGRP
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Sales Group Code", "SALES_GRP_CODE"))
                        .Items.Insert(2, New ListItem("Sales Group Name", "SALES_GRP_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTechnician.Visible = True
                    LoadDDLTechnician()

                Case SubModuleType.TECHNICIANFUNCLOC
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Technician Code", "TECHNICIAN_CODE"))
                        .Items.Insert(2, New ListItem("Technician Name", "TECHNICIAN_NAME"))
                        .Items.Insert(3, New ListItem("Functional Location Code", "FUNC_LOC_CODE"))
                        .Items.Insert(4, New ListItem("Functional Location Name", "FUNC_LOC_NAME"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.TECHNICIANSALESOFF
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Technician Code", "TECHNICIAN_CODE"))
                        .Items.Insert(2, New ListItem("Technician Name", "TECHNICIAN_NAME"))
                        .Items.Insert(3, New ListItem("Sales Office Code", "SALES_OFF"))
                        .Items.Insert(4, New ListItem("Sales Office Name", "SALES_OFF_NAME"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.MANUFACTURER
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Manufacturer Code", "MANUFACTURER_CODE"))
                        .Items.Insert(2, New ListItem("Manufacturer Name", "MANUFACTURER_NAME"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.EQPHISTLIST
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Equipment Code", "EQUIPMENT_CODE"))
                        .Items.Insert(2, New ListItem("Equipment Name", "EQUIPMENT_NAME"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.CUSTINFO
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Salesrep Code", "SALESREP_CODE"))
                        .Items.Insert(2, New ListItem("Salesrep Name", "SALESREP_NAME"))
                        .Items.Insert(3, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(4, New ListItem("Customer Name", "CUST_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.CUSTCLASS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Salesrep Code", "SALESREP_CODE"))
                        .Items.Insert(2, New ListItem("Salesrep Name", "SALESREP_NAME"))
                        .Items.Insert(3, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(4, New ListItem("Customer Name", "CUST_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.CUSTSALESTGT
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Salesrep Code", "SALESREP_CODE"))
                        .Items.Insert(2, New ListItem("Salesrep Name", "SALESREP_NAME"))
                        .Items.Insert(3, New ListItem("Customer Code", "CUST_CODE"))
                        .Items.Insert(4, New ListItem("Customer Name", "CUST_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                    pnlYear.Visible = True
                    LoadDDLYear()
                    pnlMonth.Visible = True
                    LoadDDLMonth()

                Case SubModuleType.CUSTPROFILEENQ, SubModuleType.CUSTPROFILEMAINTAIN
                    LoadMapDDLTeam()
                    pnlSku.Visible = False
                    'LoadMapDDLSKU()
                    LoadMapDDLCustGrp()

                    pnlCustProfile.Visible = True
                    pnlSearch.Visible = False

                Case SubModuleType.CUZMSSENQUIRY
                    LoadMapDDLTeam()
                    LoadMSSTitle()
                    pnlSku.Visible = False
                    'LoadMapDDLSKU()
                    LoadMapDDLCustGrp()

                    pnlCustProfile.Visible = True
                    pnlMSSEnquiry.Visible = True
                    pnlSearch.Visible = False

                Case SubModuleType.CUZSFMSENQUIRY
                    LoadMapDDLTeam()
                    LoadSFMSCategory()
                    pnlSku.Visible = False
                    'LoadMapDDLSKU()
                    LoadMapDDLCustGrp()

                    pnlCustProfile.Visible = True
                    pnlSFMSEnquiry.Visible = True
                    pnlSearch.Visible = False

                Case SubModuleType.DASHBOARDXFIELD
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Field No", "DASHBOARD_FIELD_NO"))
                        .Items.Insert(2, New ListItem("Field Description", "DASHBOARD_FIELD_DESC"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.CUSTTGTPRD
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Customer", "CUSTOMER"))
                        .Items.Insert(2, New ListItem("Contact", "CONTACT"))
                        .Items.Insert(3, New ListItem("Product", "PRODUCT"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.CUST_TEAM_CLASS
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Customer", "CUSTOMER"))
                        .Items.Insert(2, New ListItem("Contact", "CONTACT"))
                        .Items.Insert(3, New ListItem("Type", "TYPE"))
                        .Items.Insert(4, New ListItem("Team Class", "TEAMCLASS"))
                        .SelectedIndex = 1
                    End With

                Case SubModuleType.PRDSEQ
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product", "PRODUCT"))
                        .SelectedIndex = 1
                    End With


                Case SubModuleType.PROMO_SO 'Add new module - Promotion Master Maintenance
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Product Code", "PRD_CODE"))
                        .Items.Insert(2, New ListItem("Customer group code", "CGCODE"))
                        .SelectedIndex = 0
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()

                Case SubModuleType.DETAIL_BRAND 'Add new module - Detailing Brand
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Detailing Code", "DET_CODE"))
                        .Items.Insert(2, New ListItem("Detailing Name", "DET_NAME"))
                        .SelectedIndex = 0
                    End With
                Case SubModuleType.PRDCATALOG
                    With ddlSearchType
                        .Items.Clear()
                        .Items.Insert(0, lstItem)
                        .Items.Insert(1, New ListItem("Category Code", "CAT_CODE"))
                        .Items.Insert(2, New ListItem("Category Name", "CAT_NAME"))
                        .SelectedIndex = 1
                    End With
                    pnlTeam.Visible = True
                    LoadDDLTeam()
                Case Else
                    '----END ADD HERE-----'

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub hide()
        'pnlToolbar.Visible = False
        'pnlBack.Visible = True
        'UpdPnlToolbar.Update()

        imgEnquiry.Visible = False
        imgEnquirySeparator.Visible = False
        pnlEnquiry.Visible = False
        pnlBack.Visible = True
        UpdateCtrlPanel.Update()
    End Sub

    Public Sub show()
        'pnlToolbar.Visible = True
        'pnlBack.Visible = False
        'UpdPnlToolbar.Update()

        imgEnquiry.Visible = True
        imgEnquirySeparator.Visible = True
        pnlEnquiry.Visible = True
        pnlBack.Visible = False
        UpdateCtrlPanel.Update()
    End Sub

    Public Sub updatePanel()
        UpdateCtrlPanel.Update()
    End Sub

    Public Sub LoadPnlCtrl()
        pnlCtrl.Visible = ddlSearchType.SelectedIndex
        UpdateCtrlPanel.Update()
    End Sub

    'Public Sub ShowEnquiry(ByVal isShow As Boolean)
    '    imgEnquiry.Visible = isShow
    '    imgEnquirySeparator.Visible = isShow
    '    pnlEnquiry.Visible = isShow
    'End Sub

#Region "DDL"
    Private Sub LoadDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL
            With ddlTeam
                .Items.Clear()
                .DataSource = dtTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(Session("SELECTED_TEAM") = "", "", Session("SELECTED_TEAM"))
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' CHEONG BOO LIM
    ''' For VNPM Gmap modules like Cust Profile, MSS, SFSM enquiries
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadMapDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL
            With ddlMapTeam
                .Items.Clear()
                .DataSource = dtTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadDDLSalesrep()
        Dim dtSalesrep As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSalesrep = clsCommon.GetSalesrepDDL(ddlTeam.SelectedValue)
            With ddlSalesrep
                .Items.Clear()
                .DataSource = dtSalesrep.DefaultView
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(Session("SELECTED_SR") = "", "", Session("SELECTED_SR"))
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' CHEONG BOO LIM
    ''' For VNPM Gmap modules like Cust Profile, MSS, SFSM enquiries
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadMapDDLSalesrep()
        Dim dtSalesrep As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSalesrep = clsCommon.GetSalesrepDDL(ddlMapTeam.SelectedValue)
            With ddlMapSalesrep
                .Items.Clear()
                .DataSource = dtSalesrep.DefaultView
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' CHEONG BOO LIM
    ''' For VNPM Gmap modules like Cust Profile, MSS, SFSM enquiries
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadMapDDLSKU()
        Dim dtSKU As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSKU = clsCommon.GetSKUDDL()
            With ddlSKU
                .Items.Clear()
                .DataSource = dtSKU.DefaultView
                .DataTextField = "PRD_NAME"
                .DataValueField = "PRD_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' CHEONG BOO LIM
    ''' For VNPM Gmap modules like Cust Profile, MSS, SFSM enquiries
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub LoadMapDDLCustGrp()
        Dim dtCustGrp As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtCustGrp = clsCommon.GetCustGrpListDDL()
            With ddlCustGrp
                .Items.Clear()
                .DataSource = dtCustGrp.DefaultView
                .DataTextField = "CUST_GRP_NAME"
                .DataValueField = "CUST_GRP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLTechnician()
        Dim dtTechnician As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTechnician = clsCommon.GetTechnicianDDL
            With ddlTechnician
                .Items.Clear()
                .DataSource = dtTechnician.DefaultView
                .DataTextField = "TECHNICIAN_NAME"
                .DataValueField = "TECHNICIAN_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLRoute()
        Dim dtRoute As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtRoute = clsCommon.GetRouteDDL(ddlTeam.SelectedValue)
            With ddlRoute
                .Items.Clear()
                .DataSource = dtRoute.DefaultView
                .DataTextField = "ROUTE_NAME"
                .DataValueField = "ROUTE_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLYear()
        Try
            Dim strYear1, strYear2, strYear3 As String

            strYear1 = Now.Year - 1
            strYear2 = Now.Year.ToString
            strYear3 = Now.Year + 1

            With ddlYear
                .Items.Clear()
                .Items.Insert(0, New ListItem(strYear1, strYear1))
                .Items.Insert(1, New ListItem(strYear2, strYear2))
                .Items.Insert(2, New ListItem(strYear3, strYear3))
                .SelectedIndex = 1
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLMonth()
        Dim strmonth As Integer
        strmonth = Now.Month
        With ddlMonth
            .SelectedValue = strmonth
        End With
    End Sub

    Private Sub LoadDDLReasonType()
        Dim dtReasonType As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtReasonType = clsCommon.GetReasonTypeDDL
            With ddlReasonType
                .Items.Clear()
                .DataSource = dtReasonType.DefaultView
                .DataTextField = "REASON_TYPE_NAME"
                .DataValueField = "REASON_TYPE_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadMSSTitle()
        Dim dtTitle As DataTable
        Dim objMSSTitle As New clsMssEnqQuery

        Try
            dtTitle = objMSSTitle.MSSTitleList
            With ddlMssTitle
                .Items.Clear()
                .DataTextField = "TITLE_NAME"
                .DataValueField = "TITLE_CODE"
                .DataSource = dtTitle.DefaultView
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadMSSQuestion(ByVal strTitleCode As String)
        Dim dtTitle As DataTable
        Dim objMSSTitle As New clsMssEnqQuery

        Try
            dtTitle = objMSSTitle.MSSQuesList(strTitleCode)
            With ddlMSSQuestion
                .Items.Clear()
                .DataTextField = "QUES_NAME"
                .DataValueField = "QUES_CODE"
                .DataSource = dtTitle.DefaultView
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadMSSSubQuestion(ByVal strTitleCode As String, ByVal strQuesCode As String)
        Dim dtTitle As DataTable
        Dim objMSSTitle As New clsMssEnqQuery

        Try
            dtTitle = objMSSTitle.MSSSubQuesList(strTitleCode, strQuesCode)
            With ddlMSSSubQuestion
                .Items.Clear()
                .DataTextField = "SUB_QUES_NAME"
                .DataValueField = "SUB_QUES_CODE"
                .DataSource = dtTitle.DefaultView
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            ddlMSSSubQuestion_SelectedIndexChanged(Nothing, Nothing)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadSFMSCategory()
        Dim dtTitle As DataTable
        Dim objMSSTitle As New clsddl

        Try
            dtTitle = objMSSTitle.SFMSCategoryList
            With ddlSFMSCategory
                .Items.Clear()
                .DataTextField = "CAT_NAME"
                .DataValueField = "CAT_CODE"
                .DataSource = dtTitle.DefaultView
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objMSSTitle = Nothing
            dtTitle = Nothing
        End Try
    End Sub

    Private Sub LoadSFMSSubCategory()
        Dim dtTitle As DataTable
        Dim objMSSTitle As New clsDDL
        Dim strCatCode As String
        Try
            strCatCode = ddlSFMSCategory.SelectedValue
            dtTitle = objMSSTitle.SFMSSubCategoryList(strCatCode)

            With ddlSFMSSubCategory
                .Items.Clear()
                .DataTextField = "SUB_CAT_NAME"
                .DataValueField = "SUB_CAT_CODE"
                .DataSource = New DataView(dtTitle, "", "SUB_CAT_NAME", DataViewRowState.CurrentRows)
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            objMSSTitle = Nothing
            dtTitle = Nothing
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        show()
        RaiseEvent BackBtn_Click(sender, e)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Page.Validate()
            RaiseEvent EnqSearchBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnGPS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGPS.Click
        Try
            'Page.Validate()
            RaiseEvent GPSBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnKML_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKML.Click
        Try
            'Page.Validate()
            RaiseEvent KMLBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        Try
            RaiseEvent ExportBtn_Click(Me, e)
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            pnlCtrl.Visible = ddlSearchType.SelectedIndex
            SearchValue = ""
            UpdateCtrlPanel.Update()
            RaiseEvent EnqSearchBtn_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub
    Protected Sub ddlGPSStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGPSStatus.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged

        Session("SELECTED_TEAM") = ddlTeam.SelectedValue


        If lngSubModuleID = SubModuleType.SALESTGT OrElse lngSubModuleID = SubModuleType.STOCKALLOC OrElse lngSubModuleID = SubModuleType.FIELDFORCECUST _
            OrElse lngSubModuleID = SubModuleType.FIELDFORCEROUTE OrElse lngSubModuleID = SubModuleType.FIELDFORCEPRDGRP _
            OrElse lngSubModuleID = SubModuleType.FIELDFORCEACTY OrElse lngSubModuleID = SubModuleType.FIELDFORCEMSS OrElse lngSubModuleID = SubModuleType.FIELDFORCEPACKAGE Then
            LoadDDLSalesrep()
        End If

        If lngSubModuleID = SubModuleType.FIELDFORCEROUTE Then
            LoadDDLRoute()
        End If

        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub
    Protected Sub ddlMapTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMapTeam.SelectedIndexChanged
        LoadMapDDLSalesrep()

        UpdateCtrlPanel.Update()
    End Sub
    Protected Sub ddlSalesrep_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesrep.SelectedIndexChanged
        Session("SELECTED_SR") = ddlSalesrep.SelectedValue

        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlTechnician_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTechnician.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlRoute_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoute.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlReasonType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReasonType.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlViewType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlViewType.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub


    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        UpdateCtrlPanel.Update()
        RaiseEvent EnqSearchBtn_Click(sender, e)
    End Sub

    Protected Sub ddlSFMSCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSFMSCategory.SelectedIndexChanged

        LoadSFMSSubCategory()

        UpdateCtrlPanel.Update()
    End Sub
    Protected Sub ddlMssTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMssTitle.SelectedIndexChanged
        LoadMSSQuestion(ddlMssTitle.SelectedValue)
        LoadMSSSubQuestion(ddlMssTitle.SelectedValue, ddlMSSQuestion.SelectedValue)

        UpdateCtrlPanel.Update()
    End Sub

    Protected Sub ddlMSSQuestion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMSSQuestion.SelectedIndexChanged
        LoadMSSSubQuestion(ddlMssTitle.SelectedValue, ddlMSSQuestion.SelectedValue)
        UpdateCtrlPanel.Update()
    End Sub

    Protected Sub ddlMSSSubQuestion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMSSSubQuestion.SelectedIndexChanged
        Try
            pnlMssDropdownlist.Visible = False
            pnlMssFreetext.Visible = False
            pnlMssRadio.Visible = False
            pnlAnswerOff.Visible = True
            pnlAnswerOn.Visible = False

            If ddlMSSSubQuestion.SelectedIndex <> 0 Then
                Select Case ReturnAnswerType(ddlMSSSubQuestion.SelectedValue)
                    Case SubQuestionType.Freetext
                        ActivateFreetext()
                    Case SubQuestionType.Radio
                        ActivateRadio()
                    Case SubQuestionType.Selection
                        ActivateSelection()
                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "MSS Answer"
    Private Enum SubQuestionType
        Freetext = 1
        Radio = 4
        Selection = 2
    End Enum
    Private Function ReturnSubQuesCode(ByVal strSubQues As String) As String
        Dim strSplit() As String
        Try
            strSplit = strSubQues.Split("@")

            If strSplit.Length > 1 Then
                Return strSplit(1).Trim
            End If

            Return ""
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function ReturnAnswerType(ByVal strSubQues As String) As String
        Dim strSplit() As String
        Try
            strSplit = strSubQues.Split("@")

            If strSplit.Length > 0 Then
                Return strSplit(0).Trim
            End If

            Return ""
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub ActivateFreetext()
        Try
            pnlMssFreetext.Visible = True
            txtMssTextAnswer.Text = ""
            ShowAnswerPanel()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivateRadio()
        Try
            pnlMssRadio.Visible = True
            radMssRadioAnswer.SelectedIndex = 0
            ShowAnswerPanel()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivateSelection()
        Try
            pnlMssDropdownlist.Visible = True

            BindCriteria()
            ShowAnswerPanel()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindCriteria()
        Dim dt As DataTable
        Dim objCriteria As clsMssEnqQuery
        Try
            objCriteria = New clsMssEnqQuery

            With ddlMssDDLAnswer
                dt = objCriteria.GetMSSCriteria(ddlMssTitle.SelectedValue, ddlMSSQuestion.SelectedValue, ReturnSubQuesCode(ddlMSSSubQuestion.SelectedValue))
                .Items.Clear()
                .DataTextField = "CRITERIA_NAME"
                .DataValueField = "CRITERIA_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ShowAnswerPanel()
        pnlAnswerOff.Visible = False
        pnlAnswerOn.Visible = True
    End Sub
#End Region
#Region "Export"
    Public Enum ExportType
        EXCEL = 0
        TEXT = 1
    End Enum

    Public ReadOnly Property ExportFileType() As ExportType
        Get
            Return ddlExport.SelectedIndex
        End Get
    End Property

    Public ReadOnly Property ExportContentType() As String
        Get
            Dim iValue As Integer = ddlExport.SelectedIndex
            Dim strContentType As String = "application/vnd.xls"
            Select Case iValue
                Case 0
                    'Excel Format
                    strContentType = "application/vnd.xls"
                Case 1
                    'Text Format
                    strContentType = "application/vnd.text"
                Case Else
                    'strContentType = ""
            End Select
            Return strContentType
        End Get
    End Property

    Public ReadOnly Property ExportFileExtenstion() As String
        Get
            Dim iValue As Integer = ddlExport.SelectedIndex
            Dim strExt As String = ".xls"
            Select Case iValue
                Case 0
                    'Excel Format
                    strExt = ".xls"
                Case 1
                    'Text Format
                    strExt = ".txt"
                Case Else
                    'strExt = ""
            End Select
            Return strExt
        End Get
    End Property

    Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
        Dim ltrField As Literal = New Literal()
        Dim intIdx As Integer
        Dim ctrl As Control = Nothing
        Try
            For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
                ctrl = ctrlHTML.Controls(intIdx)
                If TypeOf ctrl Is LinkButton Then
                    'ltrField.Text = (CType(ctrl, LinkButton)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is HyperLink Then
                    ltrField.Text = (CType(ctrl, HyperLink)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is DropDownList Then
                    ltrField.Text = (CType(ctrl, DropDownList)).SelectedItem.Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is TextBox Then
                    ltrField.Text = (CType(ctrl, TextBox)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is CheckBox Then
                    'ltrField.Text = IIf(CType(ctrl, CheckBox).Checked, "True", "False")
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is ImageButton OrElse TypeOf ctrl Is Image OrElse TypeOf ctrl Is Button Then
                    ctrlHTML.Controls.Remove(ctrl)
                End If
                If ctrl IsNot Nothing AndAlso ctrl.HasControls Then PrepareControlForExport(ctrl)
            Next
        Catch ex As Exception
            ExceptionMsg(ClassName & ".PrepareGridViewForExport : " & ex.ToString)
        End Try

    End Sub


    'Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
    '    Dim ltrField As Literal = New Literal()
    '    Dim intIdx As Integer

    '    Try
    '        For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
    '            If ctrlHTML.Controls(intIdx).GetType().Equals((New LinkButton).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), LinkButton)).Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New HyperLink).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), HyperLink)).Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New DropDownList).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), DropDownList)).SelectedItem.Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New CheckBox).GetType()) Then
    '                ltrField.Text = IIf(CType(ctrlHTML.Controls(intIdx), CheckBox).Checked, "True", "False")
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New ImageButton).GetType()) OrElse _
    '            ctrlHTML.Controls(intIdx).GetType().Equals((New Image).GetType()) Then
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '            End If
    '            If ctrlHTML.Controls(intIdx).HasControls Then PrepareControlForExport(ctrlHTML.Controls(intIdx))
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".PrepareGridViewForExport : " & ex.ToString)
    '    End Try

    'End Sub

    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        Try
            'customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            'Dim strFileName As String = customFileName & ExportFileExtenstion
            'Response.Clear()
            'Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            'Response.Charset = ""
            'Response.ContentType = ExportContentType '"application/vnd.xls"
            'Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            'Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

            ''create a emptry row table, act as empty row for insert
            'Dim newRow As New HtmlTable()
            'newRow.Rows.Add(New HtmlTableRow())

            ''Dim pnlInfo As Control
            ''pnlInfo = pnlGeneralInfo
            ''PrepareControlForExport(pnlInfo)
            ''pnlInfo.RenderControl(htmlWrite)

            'newRow.RenderControl(htmlWrite) 'insert a empty row

            'PrepareControlForExport(dgList)
            'dgList.RenderControl(htmlWrite)
            'Response.Write(stringWrite.ToString)
            'Response.End()

            Excel.ExportToFile(dgList, Response, customFileName, SheetStyle)
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "")
        Try
            customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            Dim strFileName As String = customFileName & ExportFileExtenstion
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            Response.Charset = "utf-8"
            Response.ContentType = ExportContentType '"application/vnd.xls"
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)


            'create a emptry row table, act as empty row for insert
            Dim newRow As New HtmlTable()
            newRow.Rows.Add(New HtmlTableRow())

            'newRow.RenderControl(htmlWrite) 'insert a empty row
            'Dim pnlInfo As Control
            'pnlInfo = pnlGeneralInfo
            'PrepareControlForExport(pnlInfo)
            'pnlInfo.RenderControl(htmlWrite)

            For Each dgList As Control In aryDgList
                newRow.RenderControl(htmlWrite) 'insert a empty row
                Response.Write("<meta http-equiv=Content-Type content=""text/html; charset=utf-8"">")
                PrepareControlForExport(dgList)
                dgList.RenderControl(htmlWrite)
            Next
            Response.Write(stringWrite.ToString) 
            Response.Flush()
            Response.End()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

 
    
    
    
    
    
End Class

