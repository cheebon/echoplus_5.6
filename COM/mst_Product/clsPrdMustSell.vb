﻿'************************************************************************
'	Author	    :	Shen Yee
'	Date	    :	19/09/2016
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPrdMustSell
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPrdMustSell"
        End Get
    End Property

    Public Function GetPrdMustSellList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdMustSellDetails(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strParamType As String, ByVal strParamValue As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PARAM_TYPE", strParamType, clsDB.DataType.DBString)
                .addItem("PARAM_VALUE", strParamValue, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                '.addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePrdMustSell(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strParamType As String, ByVal strParamValue As String, ByVal strPrdCode As String, ByVal strStartDate As String, ByVal strEndDate As String, ByVal target As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PARAM_TYPE", strParamType, cor_DB.clsDB.DataType.DBString)
                .addItem("PARAM_VALUE", strParamValue, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("TARGET", target, cor_DB.clsDB.DataType.DBInt)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    'Public Function UpdatePrdMustSell(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strParamType As String, ByVal strParamValue As String, ByVal strPrdCode As String) As DataTable
    '    Dim objDB As clsDB
    '    Try
    '        objDB = New clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_MST_PRD_MUST_SELL_UPDATE"
    '            .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
    '            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
    '            .addItem("PARAM_TYPE", strParamType, cor_DB.clsDB.DataType.DBString)
    '            .addItem("PARAM_VALUE", strParamValue, cor_DB.clsDB.DataType.DBString)
    '            .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
    '            .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
    '            Return .spRetrieve()
    '            '.spUpdate()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    Public Sub DeletePrdMustSell(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strParamType As String, ByVal strParamValue As String, ByVal strPrdCode As String, ByVal strStartDate As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PARAM_TYPE", strParamType, cor_DB.clsDB.DataType.DBString, True)
                .addItem("PARAM_VALUE", strParamValue, cor_DB.clsDB.DataType.DBString, True)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetTeamDDL() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DDL_PRD_MUST_SELL_TEAM"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSalesrepDDL(ByVal strTeamCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DDL_PRD_MUST_SELL_SALESREP"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetParamTypeDDL() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DDL_PARAM_TYPE"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetParamValueDDL(ByVal strParamType As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DDL_PARAM_VALUE"
                .addItem("PARAM_TYPE", strParamType, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdMustSellListAdv(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_LIST_ADV"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdMustSellDetailsDtl(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strStartDate As String, ByVal strEndDate As String, _
                                             ByVal strCustCode As String, ByVal strCustClass As String, ByVal strRegion As String, ByVal strDistchannel As String, ByVal strCGCode As String, _
                                             ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String, ByVal strCGCode4 As String, ByVal strCGCode5 As String, ByVal strPriceGroup As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_DTL_ADV"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("REGION", strRegion, clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, clsDB.DataType.DBString)
                .addItem("PRICE_GROUP", strPriceGroup, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePrdMustSellAdv(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strStartDate As String, ByVal strEndDate As String, _
                                         ByVal strCustCode As String, ByVal strCustClass As String, ByVal strRegion As String, ByVal strDistchannel As String, _
                                         ByVal strCGCode As String, ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String _
                                         , ByVal strCGCode4 As String, ByVal strCGCode5 As String, ByVal strPriceGroup As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_CREATE_ADV"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION", strRegion, cor_DB.clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, cor_DB.clsDB.DataType.DBString)
                .addItem("PRICE_GROUP", strPriceGroup, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeletePrdMustSellAdv(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strStartDate As String, ByVal strEndDate As String, ByVal strCustCode As String, ByVal strCustClass As String, _
                                    ByVal strRegion As String, ByVal strDistchannel As String, ByVal strCGCode As String, ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String, _
                                    ByVal strCGCode4 As String, ByVal strCGCode5 As String, ByVal strPriceGroup As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MUST_SELL_DELETE_ADV"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString, True)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION", strRegion, cor_DB.clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, cor_DB.clsDB.DataType.DBString)
                .addItem("PRICE_GROUP", strPriceGroup, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



