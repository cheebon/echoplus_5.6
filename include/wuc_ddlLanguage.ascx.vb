'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Language User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlLanguage
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlLanguageID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlLanguageID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlLanguageID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlLanguageID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlLanguageID.SelectedValue = "0"
    '        Else
    '            ddlLanguageID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlLanguageID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return ddlLanguageID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlLanguageID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlLanguageID.SelectedValue = "0"
            Else
                ddlLanguageID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlLanguageID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlLanguageID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlLanguageID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlLanguageID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlLanguageID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlLanguageID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlLanguageID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlLanguageID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlLanguageID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvLanguageID.Visible 'cfvLanguageID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvLanguageID.Enabled = Value
            cfvLanguageID.Visible = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow
        Dim it As Integer

        Try
            If ddlLanguageID.Items.Count <= 0 Then
                'Get Language List
                objCommonQuery = New cor_Common.clsCommonQuery
                With objCommonQuery
                    '    .clsProperties.LanguageID = lngLanguageID
                    '    .clsProperties.DocTypeID = lngDocTypeID
                    dt = .GetLanguageList
                End With
                objCommonQuery = Nothing

                ddlLanguageID.Items.Clear()
                ddlLanguageID.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlLanguageID.Items.Add(New ListItem(Trim(drRow("language_name")), Trim(drRow("language_id")) & "@" & Trim(drRow("language_code"))))
                    Next

                    For it = 0 To ddlLanguageID.Items.Count
                        If CType(ddlLanguageID.Items(it), ListItem).Text.Trim.ToUpper = "ENG" Then
                            Exit For
                        End If
                    Next

                    ddlLanguageID.SelectedIndex = IIf(it > ddlLanguageID.Items.Count, 0, it)
                End If
            End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlLanguage.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


