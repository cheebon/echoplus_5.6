﻿Imports System.Data

Partial Class Admin_Root_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Clear()

            lblErr.Text = Request.QueryString("ErrMsg")

            Dim strScript As New StringBuilder
            strScript.Append("<script type=""text/javascript"" language=""javascript"">")
            strScript.Append("if (document.all && (window.screenTop!=0 && window.screenLeft!=0) || window.screenTop!=0){")
            strScript.Append("if (window.moveTo){ window.moveTo(-5,-5);}")
            strScript.Append("if (window.outerWidth) {window.outerWidth = screen.availWidth+10;window.outerHeight = screen.availHeight+10;}")
            strScript.Append("else if (window.resizeTo) {window.resizeTo(screen.availWidth+10,screen.availHeight+10);}")
            strScript.Append("}")
            strScript.Append("</" + "script>")
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Login", strScript.ToString)
        Else
            Page.Validate()
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            Page.Validate()
            If Page.IsValid Then
                If IsBSAdmin() Then
                    LoginUser()
                Else
                    lblErr.Text = "Access denied for non administrator."
                End If
            End If
        Catch ex As Exception
            ExceptionMsg("Login.btnLogin_Click : " + ex.ToString)
        End Try
    End Sub
    Public Function IsBSAdmin() As Boolean
        Dim usertype As String

        If txtLogin.Text.Trim.Length <= 7 Then
            Return False
        End If

        usertype = txtLogin.Text.Trim.Substring((txtLogin.Text.Trim.Length - 7), 7)

        If usertype.ToUpper = "BSADMIN" Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Sub Clear()
        Session.Clear()
        Session.Abandon()
    End Sub
    Public Sub LoginUser()
        Dim objUser As adm_User.clsUser
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCart As cor_Cart.clsCart
        Dim objCartDT As cor_Cart.clsCartDtl
        Dim dt, dtAR As DataTable
        Dim boolReturnValue As Boolean
        Dim strUserCode As String
        Dim strScript As New StringBuilder
        Dim dtmLoginDate, dtmPwdDate As DateTime
        Dim intPwdExpiry As Integer = 0
        Dim intLoginExpiryFlag As Integer = 0

        Session("echoplus_conn") = IIf(Session("echoplus_conn") Is Nothing, ConfigurationManager.ConnectionStrings("echoplus_conn").ConnectionString, Session("echoplus_conn"))
        Try

            boolReturnValue = False

            objUserQuery = New adm_User.clsUserQuery
            With objUserQuery
                'Authenticate
                .clsProperties.login = txtLogin.Text
                .clsProperties.pwd = txtPassword.Text
                strUserCode = .Authenticate
                'Get User Login
                .clsProperties.login = txtLogin.Text
                dt = .GetUserLogin
            End With

            'Max Retry Reach, Deactivate account
            If dt.Rows.Count > 0 Then
                If IIf(IsDBNull(dt.Rows(0)("login_cnt")), 0, dt.Rows(0)("login_cnt")) >= dt.Rows(0)("max_retry") And dt.Rows(0)("max_retry") <> 0 Then
                    lblErr.Text = "Max Retry Reach, Please contact Administrator !"
                    boolReturnValue = True
                End If
            End If

            objUser = New adm_User.clsUser

            If boolReturnValue = False Then
                'Valid Password and User
                If strUserCode <> "" Then
                    'Reset Count to 0
                    'objUser = New adm_User.clsUser
                    With objUser
                        .clsProperties.user_code = strUserCode
                        .clsProperties.login_cnt = 0
                        .LoginUpdate()
                    End With

                    If dt.Rows.Count <> 0 Then
                        'Get Access Right
                        With objUserQuery
                            .clsProperties.user_code = strUserCode
                            dtAR = .GetUserAccessRightDtlListByUserCode()
                        End With

                        intLoginExpiryFlag = dt.Rows(0)("login_expiry_flag")
                        If IsDBNull(dt.Rows(0)("login_date")) Then
                            dtmLoginDate = Nothing
                        Else
                            dtmLoginDate = dt.Rows(0)("login_date")
                        End If
                        If IsDBNull(dt.Rows(0)("pwd_changed_date")) Then
                            dtmPwdDate = Nothing
                        Else
                            dtmPwdDate = dt.Rows(0)("pwd_changed_date")
                        End If
                        intPwdExpiry = dt.Rows(0)("pwd_expiry")

                        'Session("UserID") = Trim(IIf(IsDBNull(dt.Rows(0)("user_id")), "", dt.Rows(0)("user_id")))
                        'Session("UserCode") = Trim(IIf(IsDBNull(dt.Rows(0)("user_code")), "", dt.Rows(0)("user_code")))
                        'Session("UserName") = Trim(IIf(IsDBNull(dt.Rows(0)("user_name")), "", dt.Rows(0)("user_name")))
                        SetValue(Of String)(Session("UserID"), dt.Rows(0)("user_id"))
                        SetValue(Of String)(Session("UserCode"), dt.Rows(0)("user_code"))
                        SetValue(Of String)(Session("UserName"), dt.Rows(0)("user_name"))

                        'Session("dflCountryID") = Trim(IIf(IsDBNull(dt.Rows(0)("country_id")), "", dt.Rows(0)("country_id")))
                        Session("dflCountryCode") = Trim(IIf(IsDBNull(dt.Rows(0)("country_code")), "", dt.Rows(0)("country_code")))
                        'Session("dflCountryName") = Trim(IIf(IsDBNull(dt.Rows(0)("country_name")), "", dt.Rows(0)("country_name")))
                        'Session("dflPrincipalID") = Trim(IIf(IsDBNull(dt.Rows(0)("principal_id")), "", dt.Rows(0)("principal_id")))
                        'Session("dflPrincipalCode") = Trim(IIf(IsDBNull(dt.Rows(0)("principal_code")), "", dt.Rows(0)("principal_code")))
                        'Session("dflPrincipalName") = Trim(IIf(IsDBNull(dt.Rows(0)("principal_name")), "", dt.Rows(0)("principal_name")))
                        'Session("dflSalesRepID") = Trim(IIf(IsDBNull(dt.Rows(0)("salesrep_id")), "", dt.Rows(0)("salesrep_id")))
                        Session("dflSalesRepCode") = Trim(IIf(IsDBNull(dt.Rows(0)("salesrep_code")), "", dt.Rows(0)("salesrep_code")))
                        'Session("dflSalesRepName") = Trim(IIf(IsDBNull(dt.Rows(0)("salesrep_name")), "", dt.Rows(0)("salesrep_name")))

                        Session("COUNTRY_ID") = Trim(GetValue(Of String)(dt.Rows(0)("country_id")))
                        'Session("COUNTRY_CODE") = Trim(GetValue(Of String)(dt.Rows(0)("country_code")))
                        Session("COUNTRY_NAME") = Trim(GetValue(Of String)(dt.Rows(0)("country_name")))
                        Session("PRINCIPAL_ID") = Trim(GetValue(Of String)(dt.Rows(0)("principal_id")))
                        'Session("PRINCIPAL_CODE") = Trim(GetValue(Of String)(dt.Rows(0)("principal_code")))
                        Session("PRINCIPAL_NAME") = Trim(GetValue(Of String)(dt.Rows(0)("principal_name")))

                        'Session("currentSalesTeamID") = Trim(IIf(IsDBNull(dt.Rows(0)("salesteam_id")), "", dt.Rows(0)("salesteam_id")))
                        'Session("TEAM_CODE") = Trim(IIf(IsDBNull(dt.Rows(0)("salesteam_code")), "", dt.Rows(0)("salesteam_code")))
                        'Session("TEAM_NAME") = Trim(IIf(IsDBNull(dt.Rows(0)("salesteam_name")), "", dt.Rows(0)("salesteam_name")))
                        'Session("currentRegionID") = Trim(IIf(IsDBNull(dt.Rows(0)("region_id")), "", dt.Rows(0)("region_id")))
                        'Session("REGION_CODE") = Trim(IIf(IsDBNull(dt.Rows(0)("region_code")), "", dt.Rows(0)("region_code")))

                        'Session("currentSalesRepID") = Trim(IIf(IsDBNull(dt.Rows(0)("salesrep_id")), "", dt.Rows(0)("salesrep_id")))
                        'Session("SALESREP_CODE") = Trim(GetValue(Of String)(dt.Rows(0)("salesrep_code")))
                        'Session("SALESREP_NAME") = Trim(IIf(IsDBNull(dt.Rows(0)("salesrep_name")), "", dt.Rows(0)("salesrep_name")))


                        'Session("echoplus_conn") = _
                        '"server=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_server_ip"))) & _
                        '";database=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_db_name"))) & _
                        '";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_user_id"))) & _
                        '";password=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_user_pwd")))

                        ''ffma
                        Session("ffma_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_user_pwd")))

                        ''ffmr
                        Session("ffmr_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_user_pwd")))

                        ''ffms
                        Session("ffms_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_user_pwd")))

                        Session("Year") = Year(Now)
                        Session("Month") = Month(Now)
                        Session("PageSize") = Trim(GetValue(Of String)(dt.Rows(0)("page_size"), 30))
                        Session("UserAccessRight") = dtAR
                        Session("NV_Enable") = Trim(GetValue(Of String)(dt.Rows(0)("nv_flag"), 0))  '1 '0-Disabled, 1-Enabled
                        Session("NetValue") = Trim(GetValue(Of String)(dt.Rows(0)("default_nv"), 1))  '1 '1-NV1, 2-NV2
                        Session("FC_Enable") = Trim(GetValue(Of String)(dt.Rows(0)("field_config_flag"), 1))  '1 '0-Disabled, 1-Enabled
                        Session("FieldConfig") = Trim(GetValue(Of String)(dt.Rows(0)("default_field_config"), 1))  '1 '1-Name Only, 2-Code Only, 3-Code and Name

                        With Portal.UserSession
                            Portal.Util.SetValue(Of String)(.UserID, dt.Rows(0)("user_id"))
                            Portal.Util.SetValue(Of String)(.UserCode, dt.Rows(0)("user_code"))
                            Portal.Util.SetValue(Of String)(.UserName, dt.Rows(0)("user_name"))
                            Portal.Util.SetValue(Of String)(.Login, dt.Rows(0)("login"))

                            'Portal.Util.SetValue(Of String)(.CountryID, dt.Rows(0)("country_id"))
                            'Portal.Util.SetValue(Of String)(.CountryName, dt.Rows(0)("country_name"))
                            'Portal.Util.SetValue(Of String)(.PrincipalID, dt.Rows(0)("principal_id"))
                            'Portal.Util.SetValue(Of String)(.PrincipalName, dt.Rows(0)("principal_name"))

                            Portal.Util.SetValue(Of Integer)(.PageSize, dt.Rows(0)("page_size"))

                            .SessionKey = Portal.Util.GetNewSessionKey
                            Dim objUser2 As New adm_User.clsUser
                            objUser2.UserLoginKeyAdd(.UserID, .Login, .SessionKey)
                        End With
                    Else
                        'No User Profile found
                        lblErr.Text = "User Profile Not Found !"
                        boolReturnValue = True
                    End If
                Else
                    'Not Valid User/Password
                    lblErr.Text = "Invalid Login/Password, Please Retry !"
                    boolReturnValue = True
                End If
            End If

            If boolReturnValue = False Then
                ''Populate Cart
                'objCart = New cor_Cart.clsCart
                'objCartDT = New cor_Cart.clsCartDtl

                'objCart.Populate()
                'objCartDT.Populate()

                'HL:20070904: AUDIT LOG IN DATE **********************
                With objUser
                    .clsProperties.user_id = Session("UserID")
                    Session("AuditID") = .AuditLogin()
                End With

                With objUser
                    .clsProperties.principal_id = Session("PRINCIPAL_ID")
                    .AuditDtl(Session("AuditID"), 0)
                End With
                '****************************************************

                'First login /Password expired
                If (dtmLoginDate.Date <= Today And IsNothing(dtmPwdDate) And intLoginExpiryFlag = 1) Or (DateAdd(DateInterval.Day, intPwdExpiry, dtmPwdDate).Date <= Today And intPwdExpiry <> 0) Then
                    strScript.Append("window.open('Admin/User/UserPwdEdit.aspx?user_code=" & Trim(strUserCode) & "','Echoplus','WIDTH=500,HEIGHT=250,LEFT=10,TOP=10,menubar=no,scrollbars=yes');window.focus();")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "anything", "<script language=javascript>" & strScript.ToString & "</script>")
                Else
                    strScript.Append("<script>")
                    strScript.Append("window.location='SuperAdmin.aspx';window.blur();")
                    strScript.Append("</" + "script>")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "anything", strScript.ToString)
                End If
            Else
                'strScript = "<script>"
                'strScript.Append( "if (!self.parent.parent){"
                'strScript.Append( "self.parent.parent.location='login.aspx?ErrMsg=" & Trim(lblErr.Text) & "';"
                'strScript.Append( "}"
                'strScript.Append( "else"
                'strScript.Append( "{"
                'strScript.Append( "window.location='login.aspx?ErrMsg=" & Trim(lblErr.Text) & "';"
                'strScript.Append( "}"
                'strScript.Append( "</" + "script>"
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Login", strScript)
                ''Response.Redirect("login.aspx?ErrMsg=" & Trim(lblErr.Text) & "", False)               
            End If

        Catch ex As Exception
            lblErr.Text = "Login.LoginUser : " + ex.ToString()
        Finally
            objCart = Nothing
            objCartDT = Nothing
            objUser = Nothing
            objUserQuery = Nothing
        End Try
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg
    End Sub
    Private Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Private Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
End Class
