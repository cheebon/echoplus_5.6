﻿Imports System.Data

Partial Class Admin_Root_User_BsadminAccDtl
    Inherits System.Web.UI.Page
#Region "Properties"
    Private Enum MODE
        Normal
        Edit
        Create
    End Enum
    Private Property UserID() As String
        Get
            Return ViewState("UserID")
        End Get
        Set(ByVal value As String)
            ViewState("UserID") = value
        End Set
    End Property
    Private Property ActionMode() As MODE
        Get
            Return IIf(ViewState("ActionMode") Is Nothing, MODE.Normal, ViewState("ActionMode"))
        End Get
        Set(ByVal value As MODE)
            ViewState("ActionMode") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "BS Admin Details"

            If Not IsPostBack Then
                Dim strUserId As String
                strUserId = Trim(Request.QueryString("USER_ID"))
                If IsNothing(strUserId) Or strUserId = "" Then
                    UserID = 0
                    'btnBack.Visible = False
                Else
                    UserID = strUserId
                End If

                Dim strAction As String = Trim(Request.QueryString("action"))
                Select Case strAction.ToUpper
                    ' Case "CREATE"
                    'ChangeMode(MODE.Create)
                    'Case "EDIT"
                    'ChangeMode(MODE.Edit)
                    Case Else 'VIEW
                        ChangeMode(MODE.Normal)
                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#Region "DTL Button Function Handling"
    'Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditDtl.Click
    '    ChangeMode(MODE.Edit)
    'End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("BsadminAccList.aspx", False)
    End Sub

    'Protected Sub btnSaveDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDtl.Click
    '    If ActionMode = MODE.Create Then
    '        dvHdr_Left.InsertItem(True)
    '    Else
    '        dvHdr_Left.UpdateItem(True)
    '    End If
    'End Sub

    'Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDtl.Click
    '    If ActionMode = MODE.Create Then
    '        ActionMode = MODE.Normal
    '        btnBack_Click(sender, Nothing)
    '    Else
    '        ChangeMode(MODE.Normal)
    '    End If
    'End Sub
#End Region
#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dvHdr_Left.DataSource = dtCurrenttable.DefaultView
            dvHdr_Left.DataBind()

            'Dim strModuleID As String = ""
            'If dtCurrenttable.Rows.Count > 0 Then
            '    strModuleID = dtCurrenttable.Rows(0)("MODULE_CODE").ToString
            'End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsBsadmin
        Try

            DT = obj.BsadminList(UserID, Session("UserID"))


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

    Private Sub ChangeMode(ByVal actMode As MODE)
        Select Case actMode
            'Case MODE.Create
            '    dvHdr_Left.ChangeMode(DetailsViewMode.Insert)
            '    ActionMode = MODE.Create
            '    btnEditDtl.Visible = False
            '    btnSaveDtl.Visible = True
            '    btnCancelDtl.Visible = True
            'Case MODE.Edit
            '    dvHdr_Left.ChangeMode(DetailsViewMode.Edit)
            '    ActionMode = MODE.Edit
            '    btnEditDtl.Visible = False
            '    btnSaveDtl.Visible = True
            '    btnCancelDtl.Visible = True
            Case Else 'Normal
                dvHdr_Left.ChangeMode(DetailsViewMode.ReadOnly)
                ActionMode = MODE.Normal
                'btnEditDtl.Visible = True
                'btnSaveDtl.Visible = False
                'btnCancelDtl.Visible = False
        End Select


        'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit) Then
        '    btnEditDtl.Visible = False
        '    btnCancelDtl.Visible = False
        'End If

        RenewDataBind()
        UpdateDatagrid.Update()
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
