﻿Imports System.Data
Imports rpt_Customer
Imports System.IO

''' <summary>
''' Name: CHEONG BOO LIM
''' Date: 20 Aug 2010
''' Description: 
''' </summary>
''' <remarks></remarks>
Partial Class iFFMR_Geomap_CustProfileEnqV2
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Public ReadOnly Property Root() As String
        Get
            Dim strCompletePath As String = Me.Server.MapPath(Me.Request.ApplicationPath).Replace("/", "\\")
            Dim strApplicationPath As String = Me.Request.ApplicationPath.Replace("/", "\\")
            Dim strRootPath As String = strCompletePath.Replace(strApplicationPath, String.Empty)


            Return strRootPath
        End Get

    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustProfileEnq.aspx"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CUSTPROFILEENQ) '"Call Analysis List By Customer"
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.CUSTPROFILEENQ
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            If Not Page.IsPostBack Then
                'Call Paging
                With wuc_dgpaging
                    .PageCount = dgList.PageCount
                    .CurrentPageIndex = dgList.PageIndex
                    .DataBind()
                    .Visible = False
                End With

                'Set Root path for client side
                Dim strRoot As String = ResolveClientUrl("~")
                strRoot = strRoot.Substring(0, strRoot.Length - 1)
                AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "RootVar", "root= '" & strRoot & "';", True)

            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            RenewDataBind()

            If Master_Row_Count > 0 Then
                wuc_toolbar.GPSButtonVisibility = True
                wuc_toolbar.KMLButtonVisibility = True
            Else
                wuc_toolbar.GPSButtonVisibility = False
                wuc_toolbar.KMLButtonVisibility = False
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub wuc_toolbar_ExportBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_toolbar.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".wuc_toolbar_ExportBtn_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub wuc_toolbar_GPSBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.GPSBtn_Click
        Dim sb As StringBuilder
        'CustName, Address, District, CustGrpName, CustClass, CustType,
        '    UserID, PrincipalID, PrincipalCode, NetValue, tDate;
        Try
            sb = New StringBuilder

            With wuc_toolbar
                sb.AppendLine("CustName = '" & .CustName & "';")
                sb.AppendLine("Address = '" & .Address & "';")
                sb.AppendLine("District = '" & .District & "';")
                sb.AppendLine("CustGrpName = '" & .CustGrp & "';")
                sb.AppendLine("CustClass = '" & .CustClass & "';")
                sb.AppendLine("CustType = '" & .CustType & "';")
                sb.AppendLine("UserID = '" & Session("UserID") & "';")
                sb.AppendLine("PrincipalID = '" & Session("PRINCIPAL_ID") & "';")
                sb.AppendLine("PrincipalCode = '" & Session("Principal_Code") & "';")
                sb.AppendLine("NetValue = '" & Session("NetValue") & "';")
                sb.AppendLine("tDate = '" & DateTime.Now.ToString("yyyy-MM-dd") & "';")
                sb.AppendLine(String.Format("MtdStart = '{0}';", .MtdSalesStartRange))
                sb.AppendLine(String.Format("MtdEnd = '{0}';", .MtdSalesEndRange))
                sb.AppendLine(String.Format("YtdStart = '{0}';", .YtdSalesStartRange))
                sb.AppendLine(String.Format("YtdEnd = '{0}';", .YtdSalesEndRange))
                sb.AppendLine(String.Format("NoSkuStart = '{0}';", .NoSkuStartRange))
                sb.AppendLine(String.Format("NoSkuEnd = '{0}';", .NoSkuEndRange))
                sb.AppendLine(String.Format("TeamCode = '{0}';", .MTeam))
                sb.AppendLine(String.Format("SalesrepCode = '{0}';", .MSalesrep))
                sb.AppendLine(String.Format("PrdCode = '{0}';", .SKU))
            End With

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "SetMapVariable", sb.ToString, True)

            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShowMap", "mc_toggleMap(map, '#pnlMapPopUp', 'on', SearchCustomerLocation);", True)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShowMap", "WrapModalPopup('pnlMapPopUp', 'divModalMask', 'on', SearchCustomerLocation);", True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub wuc_toolbar_KMLBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.KMLBtn_Click
        Dim strFilePath As String
        Try
            Dim clsXML As New rpt_Customer.clsCustomerProfileKML

            With wuc_toolbar
                strFilePath = clsXML.generateKMLFile(.CustName, .Address, .District, .CustGrp, .CustClass, .CustType, _
                                                      Session("UserID"), Session("NetValue"), Session("PRINCIPAL_ID"), "", DateTime.Now.ToString("yyyy-MM-dd"), .MtdSalesStartRange, _
                                                      .MtdSalesEndRange, .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU, .MTeamName, .MSalesrepName, .SKUName, .CustTypeName)
            End With

            If IO.File.Exists(strFilePath) Then
                downloadKML(strFilePath)
            Else
                lblErr.Text = "Failed to initial download KML file, file does not exists or not readable."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub downloadKML(ByVal strFilePath As String)
        Dim file As System.IO.FileInfo = New System.IO.FileInfo(strFilePath)
        If file.Exists Then
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End()
        End If
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsCustomerProfile
            With (wuc_toolbar)
                DT = clsCustomer.GetCustomerProfile(.CustName, .Address, .District, .CustGrp, .CustClass, .CustType, Session("UserID"), Session("NetValue"), _
                                                    Session("PRINCIPAL_ID"), "", DateTime.Now.ToString("yyyy-MM-dd"), .MtdSalesStartRange, .MtdSalesEndRange, _
                                                    .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListExport() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsCustomerProfile
            With (wuc_toolbar)
                DT = clsCustomer.GetCustomerProfile(.CustName, .Address, .District, .CustGrp, .CustClass, .CustType, Session("UserID"), Session("NetValue"), _
                                                   Session("PRINCIPAL_ID"), "", DateTime.Now.ToString("yyyy-MM-dd"), .MtdSalesStartRange, .MtdSalesEndRange, _
                                                   .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If isExport = True Then
                dtCurrentTable = GetRecListExport()
            Else
                dtCurrentTable = GetRecList()
            End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()


            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CustomerProfileV2.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn


                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CustomerProfileV2.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CustomerProfileV2.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_CustomerProfileV2.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_CustomerProfileV2.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text.Trim = String.Empty Or e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text.Trim = "&nbsp;" Then
                        e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text = "No image"
                    Else
                        'e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text = "<a href='#' onclick='PopImage(""" & ResolveClientUrl("~").Substring(0, ResolveClientUrl("~").Length - 1) & e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text & """, """ & Server.HtmlEncode(e.Row.Cells(aryDataItem.IndexOf("CUST_NAME")).Text.Replace("'", "\'")) & """, """ & Server.HtmlEncode(e.Row.Cells(aryDataItem.IndexOf("ADDRESS")).Text.Replace("'", "\'")) & """)'>Outlet Photo</a>"
                        e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text = "<a href='#' onclick=""PopImage('" & ResolveClientUrl("~").Substring(0, ResolveClientUrl("~").Length - 1) & e.Row.Cells(aryDataItem.IndexOf("IMG_LOC")).Text & "', '" & Server.HtmlEncode(e.Row.Cells(aryDataItem.IndexOf("CUST_NAME")).Text.Replace("'", "\'")) & "', '" & Server.HtmlEncode(e.Row.Cells(aryDataItem.IndexOf("ADDRESS")).Text.Replace("'", "\'")) & "', '" & Server.HtmlEncode(e.Row.Cells(aryDataItem.IndexOf("CUST_CODE")).Text.Replace("'", "\'")) & "')"">Outlet Photo</a>"
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region
#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "COMMON FUNCTION"

#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub RefreshDatabindingrpCust()

        Dim dt As DataTable
        dt = GetRecListrpCust()
        If dt.Rows.Count > 0 Then
            rpCust.DataSource = dt
            rpCust.DataBind()
        Else
            rpCust.DataSource = Nothing
            rpCust.DataBind()
        End If

        lblCustCode.Text = hfPicCustCode.Value
        lblCustName.Text = hfPicCustName.Value
        lblAddress.Text = hfPicCustAdd.Value
    End Sub

    Private Function GetRecListrpCust() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsCustomerProfile
            Dim strCustCode As String = hfPicCustCode.Value
            DT = clsCustomer.GetCustomerProfilePicture(strCustCode, Portal.UserSession.UserID)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function


    Protected Sub btnRefreshPic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshPic.Click
        RefreshDatabindingrpCust()
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "Show", "SetVisit('Show'); ShowVisit(); $('#Visit').center();", True)
    End Sub


    Protected Sub rpCust_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCust.ItemDataBound
        Dim imgCust As Image = DirectCast(e.Item.FindControl("imgCust"), Image)
        Dim hfCustCode As HiddenField = DirectCast(e.Item.FindControl("hfCustCode"), HiddenField)
        Dim hfCustName As HiddenField = DirectCast(e.Item.FindControl("hfCustName"), HiddenField)
        Dim hfImgLoc As HiddenField = DirectCast(e.Item.FindControl("hfImgLoc"), HiddenField)


        Dim strImgLoc As String = hfImgLoc.Value
        Dim strCustName As String = hfCustName.Value

        If strImgLoc.ToString.Trim <> "" Then
            If File.Exists(Server.MapPath("~" + strImgLoc.ToString)) Then
                imgCust.ImageUrl = "~" & strImgLoc.ToString & "?" & DateTime.Now.ToString("yyyyMMddHHmmss")

            Else
                imgCust.ImageUrl = "~" + "\images\anonymous.jpg"

            End If

            ' EmbedConfirmation(UploadedFlag)
        Else
            imgCust.ImageUrl = "~" + "\images\anonymous.jpg"

        End If
    End Sub
End Class

Public Class CF_CustomerProfileV2
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "ABBV"
                strFieldName = "Abbreviation"
            Case "DISTRICT"
                strFieldName = "District"
            Case "CUST_TYPE"
                strFieldName = "Cust Type"
            Case "MTD_SALES"
                strFieldName = "MTD Sales"
            Case "YTD_SALES"
                strFieldName = "YTD Sales"
            Case "NO_SKU"
                strFieldName = "No. SKU"
            Case "IMG_LOC"
                strFieldName = "Image"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "" Then
                FCT = FieldColumntype.HyperlinkColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*SALES" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "*_TYPE" OrElse strColumnName Like "*_SKU" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class