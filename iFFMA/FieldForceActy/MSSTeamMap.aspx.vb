﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext



Partial Class iFFMA_FieldForceActy_MSSTeamMap
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "MSSTeamMapping"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MSS_TEAM_MAPPING)
                .DataBind()
                .Visible = True
            End With


            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                LoadDDLTeam()
                LoadLsbDestTeam()

            Else
                If Master_Row_Count > 0 Then
                    'For Each dr As GridViewRow In dgList.Rows
                    '    Dim btnStatus As Button = CType(dr.Cells(aryDataItem.IndexOf("BTN_STATUS")).Controls(0), Button)
                    '    If btnStatus IsNot Nothing Then
                    '        btnStatus.CssClass = "cls_button"
                    '    End If
                    'Next
                End If
            End If

            lblErr.Text = ""


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub ddlSalesTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesTeam.SelectedIndexChanged
        LoadDDLTitle()
    End Sub
 
    Protected Sub ddlTitle_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTitle.SelectedIndexChanged
        LoadLsbQues()
    End Sub

    Protected Sub btnPopulate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPopulate.Click
        RefreshDatabinding()
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i As Integer = SaveTeamMSSMapping()
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('" + CStr(i) + " records updated!')", True)
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0


        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSTeamMap As New mst_FieldForceActy.clsMSSTeamMap

            Dim strTeamCode As String = ddlSalesTeam.SelectedValue
            Dim strTitleCode As String = ddlTitle.SelectedValue
            Dim strQuesList As String = GetItemsInString(lsbSelectedQues)
            Dim strDestTeam As String = GetItemsInString(lsbSelectedDestTeam)

            DT = clsMSSTeamMap.GetMSSTeamList(strTeamCode, strTitleCode, strQuesList, strDestTeam)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                    btndgListCheckAll.Visible = False
                    btndgListUnCheckAll.Visible = False
                    btnSave.Visible = False
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                    btndgListCheckAll.Visible = True
                    btndgListUnCheckAll.Visible = True
                    btnSave.Visible = True
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .RowCount = Master_Row_Count
            '    '.Visible = IIf(Master_Row_Count > 0, True, False)
            'End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

       
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_MSSTeamMap.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case FieldColumntype.EditCommandColumn
                      
                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MSSTeamMap.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MSSTeamMap.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_MSSTeamMap.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_MSSTeamMap.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        LoadChkBoxSelected(e, "SELECTED")
                    End If

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        LoadChkBoxSelected(e, "SELECTED")
                    End If

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub LoadChkBoxSelected(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs, ByVal colName As String)
        Try
            Dim tc As TableCell
            Dim ChkBox As New CheckBox
            Dim strValue As String

            tc = e.Row.Cells(aryDataItem.IndexOf(colName))
            strValue = Trim(e.Row.Cells(aryDataItem.IndexOf(colName)).Text)

           
            With ChkBox
                .ID = "chk" + colName
                .CssClass = "cls_checkbox"
                If strValue = "1" Then
                    .Checked = True
                    .Enabled = False
                Else
                    .Checked = False
                    .Enabled = True
                End If
            End With

            tc.Controls.Add(ChkBox)



        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadChkBoxSelected : " & ex.ToString)
        End Try
    End Sub
#End Region

    '#Region "Paging Control"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnGo_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
    '        Try
    '            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkPrevious_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
    '        Try
    '            If dgList.PageIndex > 0 Then
    '                dgList.PageIndex = dgList.PageIndex - 1
    '            End If
    '            wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkNext_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
    '        Try
    '            If dgList.PageCount - 1 > dgList.PageIndex Then
    '                dgList.PageIndex = dgList.PageIndex + 1
    '            End If
    '            wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

#Region "QUES"
    Private Sub LoadLsbQues()
        Try
            lsbQues.Items.Clear()
            lsbSelectedQues.Items.Clear()

            Dim clsMSSTeamMap As New mst_FieldForceActy.clsMSSTeamMap
            Dim strTeamCode = ddlSalesTeam.SelectedValue
            Dim strTitleCode = ddlTitle.SelectedValue

            With lsbQues
                .DataSource = clsMSSTeamMap.GetLstMSSQues(strTeamCode, strTitleCode)
                .DataTextField = "QUES_NAME"
                .DataValueField = "QUES_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbQues : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddQues.Click
        Try
            AddToListBox(lsbQues, lsbSelectedQues)
            'LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveQues.Click
        Try
            AddToListBox(lsbSelectedQues, lsbQues)
            'LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllQues.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbQues.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbQues, lsbSelectedQues)
            'LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllQues.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedQues.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedQues, lsbQues)
            'LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "TEAM"
    Private Sub LoadLsbDestTeam()
        Try
            'Clear list box before fill
            lsbDestTeam.Items.Clear()
            lsbSelectedDestTeam.Items.Clear()

            Dim clsCommon As New mst_Common.clsDDL

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbDestTeam
                .DataSource = clsCommon.GetTeamDDL
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddDestTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddDestTeam.Click
        Try
            AddToListBox(lsbDestTeam, lsbSelectedDestTeam)


        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveDestTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveDestTeam.Click
        Try
            AddToListBox(lsbSelectedDestTeam, lsbDestTeam)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllDestTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllDestTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbDestTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbDestTeam, lsbSelectedDestTeam)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllDestTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllDestTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedDestTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedDestTeam, lsbDestTeam)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub LoadDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL
            With ddlSalesTeam
                .Items.Clear()
                .DataSource = dtTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(Session("SELECTED_TEAM") = "", "", Session("SELECTED_TEAM"))
            End With

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLTitle()
        Dim dtTitle As DataTable
        Dim clsMSSTeamMap As New mst_FieldForceActy.clsMSSTeamMap

        Try
            Dim strTeamCode As String = ddlSalesTeam.SelectedValue

            dtTitle = clsMSSTeamMap.GetDDLMSSTitle(strTeamCode)
            With ddlTitle
                .Items.Clear()
                .DataSource = dtTitle.DefaultView
                .DataTextField = "TITLE_NAME"
                .DataValueField = "TITLE_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With


            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function SaveTeamMSSMapping() As Integer
        Try
            Dim dt As DataTable
            If dgList.Rows.Count > 0 Then

                Dim i As Integer = 0
                Dim j As Integer = 0
                Dim DK As DataKey

                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)

                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim strTeamCode As String, strTitleCode As String, strQuesCode As String, strSubQuesCode As String, strCriteriaCode As String
                        Dim chkSelect As CheckBox = CType(dgList.Rows(i).FindControl("chkSELECTED"), CheckBox)

                        If chkSelect.Checked = True Then
                            strTeamCode = dgList.DataKeys(i).Item("TEAM_CODE")
                            strTitleCode = dgList.DataKeys(i).Item("TITLE_CODE")
                            strQuesCode = dgList.DataKeys(i).Item("QUES_CODE")
                            strSubQuesCode = dgList.DataKeys(i).Item("SUB_QUES_CODE")
                            strCriteriaCode = IIf(IsDBNull(dgList.DataKeys(i).Item("CRITERIA_CODE")), "", dgList.DataKeys(i).Item("CRITERIA_CODE"))

                            Dim clsMSSTeamMap As New mst_FieldForceActy.clsMSSTeamMap
                            dt = clsMSSTeamMap.SaveMSSTeamList(strTeamCode, strTitleCode, strQuesCode, strSubQuesCode, strCriteriaCode)
                            j = j + 1
                        End If

                    End If

                    i += 1

                Next
                RefreshDatabinding()
                UpdateDatagrid.Update()
                Return j
            End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function
#End Region

#Region "Export Extender"

    Protected Sub imgExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList)


            wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()


        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

   


End Class


Public Class CF_MSSTeamMap
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "SELECTED"
                strFieldName = "Select"
            Case "TEAM_CODE"
                strFieldName = "Team Code"
            Case "TEAM_NAME"
                strFieldName = "Team Name"
            Case "TITLE_CODE"
                strFieldName = "Title Code"
            Case "TITLE_NAME"
                strFieldName = "Title Name"
            Case "QUES_CODE"
                strFieldName = "Ques. Code"
            Case "QUES_NAME"
                strFieldName = "Ques. Name"
            Case "SUB_QUES_CODE"
                strFieldName = "Sub. Ques. Name"
            Case "SUB_QUES_NAME"
                strFieldName = "Sub. Ques. Name"
            Case "CRITERIA_CODE"
                strFieldName = "Criteria Code"
            Case "CRITEIRA_NAME"
                strFieldName = "Criteria Name"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "BTN_STATUS" Then
                FCT = FieldColumntype.EditCommandColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
