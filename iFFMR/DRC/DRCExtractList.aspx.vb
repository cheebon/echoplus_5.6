Imports System.Data
Partial Class iFFMR_DRC_DRCExtractList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_DRC_Extract"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "DRCExtractionList"
        End Get
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.DRCEXTRACTION) '"Dealer Record Card List"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.DRCEXTRACTION
                .DataBind()
                .Visible = True
            End With

            wuc_pnlDRCExtraction.GenerateFiledList(SubModuleType.DRCEXTRACTION)

            BindDefault()
        End If
        lblErr.Text = ""
    End Sub

#Region "Event Handle"
    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        RenewDataBind()
    End Sub
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
    End Sub
    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        ElseIf wuc_ctrlpanel.GetClientHeight > 0 Then
            dgList.GridHeight = wuc_ctrlpanel.GetClientHeight - 100
        End If
        UpdateDatagrid.Update()
    End Sub
    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_pnlDRCExtraction.ResetBtn_Click
        BindDefault()
    End Sub
    Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_pnlDRCExtraction.RefreshBtn_Click
        RefreshDatabinding()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        ' Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'wuc_ctrlpanel.ExportToFile(dgList, "DRC")
        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"

    Private Sub BindDefault()
        Dim dt As Data.DataTable = GetRecList(Session("YEAR"), Session("MONTH"), "", "", "", "PRD_CODE")
        dt.Rows.Add(dt.NewRow)
        RefreshDatabinding(dt)
    End Sub
    Public Sub RenewDataBind()
        RefreshDataBind()
    End Sub
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding(Optional ByRef dtCurrenttable As DataTable = Nothing)
        Dim strSortExpression As String = CType(SortingExpression, String)
        If dtCurrenttable Is Nothing Then
            dtCurrenttable = GetRecList()
        End If
        If dtCurrenttable Is Nothing Then
            dtCurrenttable = New DataTable
        Else
            PreRenderMode(dtCurrenttable)
        End If

        Dim dvCurrentView As New DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        UpdateDatagrid_Update()
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_DRC_Extract.GetFieldColumnType(ColumnName)
                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_DRC_Extract.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_DRC_Extract.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_DRC_Extract.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        ViewState("DataItem") = aryDataItem
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer
                If licItemFigureCollector IsNot Nothing Then
                    Dim iIndex As Integer
                    Dim intRowCount As Integer = dgList.Rows.Count

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_DRC_Extract.GetOutputFormatString(li.Text), CDbl(li.Value))
                        End If
                    Next
                End If

        End Select
        ViewState("DataItem") = aryDataItem
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function


    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .Year = Session("YEAR")
                .Month = Session("MONTH")
                .PrincipalID = Session("PRINCIPAL_ID")
                .PrincipalCode = Session("PRINCIPAL_CODE")
                If Page.IsPostBack Then
                    .Classification = wuc_pnlDRCExtraction.PrdGrpCode
                    .TeamCode = wuc_pnlDRCExtraction.TeamCode
                    .SalesrepCode = wuc_pnlDRCExtraction.SalesrepCode
                    .GroupField = wuc_pnlDRCExtraction.GroupingField
                End If
            End With


            With CriteriaCollector
                DT = GetRecList(Session("YEAR"), Session("MONTH"), .TeamCode, .SalesrepCode, .Classification, .GroupField)
            End With

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecList(ByVal strYear As String, ByVal strMonth As String, ByVal strTeamList As String, _
                                            ByVal strSalesRepList As String, ByVal strPrdGrpList As String, ByVal strGroupField As String) As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            If strGroupField Is Nothing Or strGroupField = "" Then
                strGroupField = "PRD_CODE"
                wuc_pnlDRCExtraction.adddefaultfield()
            End If

            Dim clsDRCDB As New rpt_DRC.clsDRCQuery
            DT = clsDRCDB.GetDRCExtractionList(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepList, strGroupField, _
          strTeamList, strPrdGrpList)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function


    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper

            If (strColumnName Like "*QTY" OrElse _
             strColumnName Like "*AMT") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression

        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        RefreshDatabinding()
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
End Class
Public Class CF_DRC_Extract
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        'TEAM_CODE ,PRD_CODE ,PRD_NAME ,CUST_NAME ,
        'OPEN_BAL_QTY ,OFF_TAKE_QTY ,STK_TRF_QTY ,
        'INV_QTY ,POD_QTY ,RET_QTY ,TTL_AMT

        Dim strColumnName As String = ""

        Select Case ColumnName.ToUpper
            Case "DRC_DATE"
                strColumnName = "Last DRC"
            Case "OPEN_BAL_QTY"
                strColumnName = "Opening Balance"
            Case "CUR_STK_QTY"
                strColumnName = "Current Stock"
            Case "OFF_TAKE_QTY"
                strColumnName = "Off Take"
            Case "STK_TRF_QTY"
                strColumnName = "Stock Transfer"
            Case "INV_QTY"
                strColumnName = "Invoice"
            Case "POD_QTY"
                strColumnName = "POD"
            Case "RET_QTY"
                strColumnName = "Return"
            Case "TTL_AMT"
                strColumnName = "Total Value"
            Case Else
                strColumnName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strColumnName

    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        ' Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If
        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        'Try
        If strNewName Like "*QTY" Then
            strStringFormat = "{0:#,0}"
        ElseIf strNewName Like "*_AMT" Then
            strStringFormat = "{0:#,0.00}"
        ElseIf strNewName Like "*DATE*" Then
            strStringFormat = "{0:yyyy-MM-dd}"
        Else
            strStringFormat = ""
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        'Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

End Class
