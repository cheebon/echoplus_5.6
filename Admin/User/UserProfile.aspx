<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserProfile.aspx.vb" Inherits="Admin_User_UserProfile" %>

<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Profile</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script type="text/javascript" src="../../include/dkshpolicy.min.js"></script>
     <script type="text/javascript">
    <% = Portal.Policy.WriteClientPolicy() %>
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmUserProfile" method="post" runat="server">
    <table id="tbl1" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="BckgroundInsideContentLayout">
                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                    <tr>
                        <td colspan="3">
                            <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                        </td>
                    </tr>
                    <tr>
                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                        </td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td colspan="3">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="btnCancel" runat="server" CssClass="cls_button" Text="Cancel" CausesValidation="False">
                            </asp:Button>&nbsp;<asp:Button ID="btnSave" runat="server" CssClass="cls_button"
                                Text="Save"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td valign="top" class="Bckgroundreport">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr class="Bckgroundreport">
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="Bckgroundreport">
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblLogin" runat="server" CssClass="cls_label_header">Login</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot2" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txtLogin" MaxLength="20" runat="server" CssClass="cls_textbox" Enabled="false"
                                                        ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblUserCode" runat="server" CssClass="cls_label_header">User Code</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot5" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txtUserCode" MaxLength="20" runat="server" CssClass="cls_textbox"
                                                        Enabled="false" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <asp:CheckBox ID="chkPassword" runat="server" Text="Change Password" AutoPostBack="True"
                                                        CssClass="cls_checkbox"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblPassword" runat="server" CssClass="cls_label_header">Password</asp:Label><asp:Label
                                                        ID="lblMark3" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot3" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" MaxLength="50" runat="server" Width="110px" TextMode="Password"
                                                        CssClass="cls_textbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblConfirmedPassword" runat="server" CssClass="cls_label_header">Confirmed Password</asp:Label><asp:Label
                                                        ID="lblMark4" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot4" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtConfirmedPassword" MaxLength="50" runat="server" Width="110px"
                                                        TextMode="Password" CssClass="cls_textbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvConfirmedPassword" runat="server" ControlToValidate="txtConfirmedPassword"
                                                        ErrorMessage="Password cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvPoorPass" runat="server" ErrorMessage="Minimum 8 alphanumeric character" Display="Dynamic"
                                                        CssClass="cls_validator" ControlToValidate="txtConfirmedPassword" OnServerValidate="PolicyCheck"
                                                        ClientValidationFunction="PolicyCheck"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <asp:Label ID="lblUserName" runat="server" CssClass="cls_label_header">User Name</asp:Label><asp:Label
                                                        ID="lblMark6" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td width="2%">
                                                    <asp:Label ID="lblDot6" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txtUserName" MaxLength="50" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName"
                                                        ErrorMessage=" User Name cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Label ID="lblEmail" runat="server" CssClass="cls_label_header">Email</asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:Label ID="lblDot15" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td valign="top" colspan="4">
                                                    <asp:TextBox ID="txtEmail" MaxLength="100" runat="server" CssClass="cls_textbox"></asp:TextBox><asp:RegularExpressionValidator
                                                        ID="revEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        ErrorMessage="Email must be a valid Email Address format" ControlToValidate="txtEmail"
                                                        Display="Dynamic" CssClass="cls_validator"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Label ID="lblPageSize" runat="server" CssClass="cls_label_header">Page Size</asp:Label><asp:Label
                                                        ID="lblMark21" runat="server" CssClass="cls_mandatory">*</asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:Label ID="lblDot21" runat="server" CssClass="cls_label_header">:</asp:Label>
                                                </td>
                                                <td valign="top" colspan="4">
                                                    <asp:TextBox ID="txtPageSize" MaxLength="4" runat="server" CssClass="cls_textbox">20</asp:TextBox><asp:CompareValidator
                                                        ID="cfvPageSize" runat="server" ErrorMessage="Page Size must be numeric" ControlToValidate="txtPageSize"
                                                        Type="Double" Operator="DataTypeCheck" Display="Dynamic" CssClass="cls_validator"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvPageSize" runat="server" ControlToValidate="txtPageSize"
                                                        ErrorMessage="Page Size cannot be Blank !" Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cfvPageSize1" runat="server" Display="Dynamic" ErrorMessage="Page Size must be greater or equal to 1."
                                                        ControlToValidate="txtPageSize" ValueToCompare="0" Operator="GreaterThan" CssClass="cls_validator"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="Bckgroundreport">
                                    <td align="right">
                                        <asp:TextBox ID="txtType" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox ID="txtUserID" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr class="Bckgroundreport">
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
