﻿Imports System.Data
Partial Class iFFMA_TRA_TRADtl
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "TRaListDtl"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "TRaListDtl"
        End Get
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        With wuc_lblHeader
            .Title = "Goods Return List Details" 'Report.GetName(SubModuleType.TRAList)
            .DataBind()
            .Visible = True
        End With

            'Call Paging()
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not Page.IsPostBack Then
                TimerControl1.Enabled = True
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

            End If
    End Sub

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        '  Try
        dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

        dgList.EditIndex = -1
        RefreshDatabinding()
        Exit Sub

        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        '  End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        'Try
        If dgList.PageIndex > 0 Then
            dgList.PageIndex = dgList.PageIndex - 1
        End If
        wuc_dgpaging.PageNo = dgList.PageIndex + 1

        dgList.EditIndex = -1
        RefreshDatabinding()
        Exit Sub

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        ' End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click

        If dgList.PageCount - 1 > dgList.PageIndex Then
            dgList.PageIndex = dgList.PageIndex + 1
        End If
        wuc_dgpaging.PageNo = dgList.PageIndex + 1

        dgList.EditIndex = -1
        RefreshDatabinding()
        Exit Sub


    End Sub
#End Region
#Region "EVENT HANDLER"

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            RefreshDatabinding()

        End If

    End Sub

#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        '   Try
        'If dtCurrentTable Is Nothing Then
        'ViewState("strSortExpression") = nothing
        'ViewState("dtCurrentView") = dtCurrentTable
        'End If  
        dtCurrentTable = GetRecList()
        PreRenderMode(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        End If

        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        dgList.PageSize = 150 'intPageSize
        dgList.DataBind()

        'Call Paging
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
        End With

        If dgList.Rows.Count = 0 Then
            wuc_dgpaging.Visible = False
        ElseIf dgList.PageCount = 1 Or dgList.PageCount = 0 Then
            wuc_dgpaging.Visible = False
        Else
            wuc_dgpaging.Visible = True
        End If
        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)
        'wuc_dgpaging.Visible = IIf(dgList.PageCount = 1, False, True)

        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_Update()

        '  End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        ' Try
        aryDataItem.Clear()
        dgList_Init(DT)

        'Catch ex As Exception
        ' ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_TRAList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_TRAList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_TRAList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_TRAList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_TRAList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "DESC_CODE"

                    Dim strSalesrepCode As String, strTeamCode As String
                    strTeamCode = Request.QueryString("Team_Code")
                    strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String

                    strUrlFormatString = ""
                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "DtlContentBar"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_TRAList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_TRAList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_TRAList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 15 Then dgList.GridHeight = Nothing
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','DetailBarIframe');", True)
        UpdateDatagrid.Update()
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        ' Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer
        End Select
        ' Catch ex As Exception
        ' ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub


#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        '  Try
        Dim dt As Data.DataTable = GetRecList()
        dt.Rows.Add(dt.NewRow)
        'ViewState("dtCurrentView") = dt
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        ' End Try
    End Sub


    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsTRA As New adm_TRA.clsTRA

            With clsTRA
                .clsProperties.txn_no = Request.QueryString("TXN_NO")
                .clsProperties.user_code = Session.Item("UserCode")
                DT = .GetTRAListDtl
            End With


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

    '#Region "Export Extender"
    '    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
    '        Dim objStringWriter As New System.IO.StringWriter
    '        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
    '        Try
    '            Dim blnAllowSorting As Boolean = dgList.AllowSorting
    '            Dim blnAllowPaging As Boolean = dgList.AllowPaging

    '            dgList.AllowSorting = False
    '            dgList.AllowPaging = False
    '            RefreshDatabinding()

    '            wuc_ctrlpanel.ExportToFile(dgList, "SALES_ORD_PRD_MATRIX")

    '            dgList.AllowPaging = blnAllowPaging
    '            dgList.AllowSorting = blnAllowSorting
    '            RefreshDatabinding()
    '        Catch ex As Threading.ThreadAbortException
    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
    '        End Try
    '    End Sub

    '    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
    '        ''Hide this function to prevent internal asp.net error
    '        ''To enable export function
    '        'MyBase.VerifyRenderingInServerForm(control)
    '    End Sub

    '#End Region

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = CStr(ViewState("strSortExpression"))

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()

    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_TRAList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "ITEM_CODE"
                strFieldName = "Product code (Prd ID) "
            Case "prd_name"
                strFieldName = "Product name"
            Case "OLDMATERIALCODE"
                strFieldName = "Old Material Code"
            Case "RET_QTY"
                strFieldName = "Quantity"
            Case "UOM_CD"
                strFieldName = "UOM"
            Case "reason_name"
                strFieldName = "Return reason by item (Bus.transaction type) "
            Case Else

                strFieldName = Report.GetDisplayColumnName(ColumnName)

        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '    Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        'Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        'strColumnName = strColumnName.ToUpper
        'Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        'If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
        '   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
        '    FCT = FieldColumntype.InvisibleColumn
        'Else
        '    FCT = FieldColumntype.BoundColumn
        'End If

        If (strColumnName Like "DAY_*_DEF") OrElse (strColumnName Like "ORD") OrElse _
            (strColumnName Like "REGION*") OrElse _
            (strColumnName = "YEAR") OrElse (strColumnName = "MONTH") Then
            FCT = FieldColumntype.InvisibleColumn
        ElseIf strColumnName = "" Then
            FCT = FieldColumntype.HyperlinkColumn

        End If

        Return FCT
        '  Catch ex As Exception

        '  End Try
    End Function


    Public Shared Function GetDayValue(ByVal ColumnName As String) As Integer
        '  Try
        Dim strColumnName As String = ColumnName.ToUpper
        Dim intValue As Integer = 0

        strColumnName = strColumnName.Replace("_CALL", "").Replace("_DEF", "").Replace("DAY_", "")
        'strColumnName = strColumnName.Replace("S", "")
        If Not String.IsNullOrEmpty(strColumnName) AndAlso IsNumeric(strColumnName) Then intValue = CInt(strColumnName)
        Return intValue
        ' Catch ex As Exception
        ' End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        '   Try

        Select Case strNewName
            Case "CALL_ACH", "ACT_FIELD_DAY_HALF"
                strStringFormat = "{0:0.0}"
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
                strStringFormat = "{0:0.00}"
            Case Else
                strStringFormat = ""
        End Select

        'If strColumnName.ToUpper Like "DAY_*_DEF" And Not strColumnName.ToUpper Like "DAY_*_CALL" Then
        If strColumnName.ToUpper Like "DAY_*_CALL" And Not strColumnName.ToUpper Like "DAY_*_DEF" Then
            strStringFormat = "{0:0.##}"
        End If
        ' Catch ex As Exception
        ' End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf strColumnName Like "*_NAME" Then
                .HorizontalAlign = HorizontalAlign.Left
                'ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                '    .HorizontalAlign = HorizontalAlign.Left
            ElseIf strColumnName = "MULTIPLIER" Then
                .HorizontalAlign = HorizontalAlign.Right
            End If

        End With

        ' Catch ex As Exception

        ' End Try
        Return CS
    End Function

End Class