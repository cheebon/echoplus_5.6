'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	04/03/2008
'	Purpose	    :	Salesteam List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports PushSharp.Google
Imports PushSharp.Core
Imports Newtonsoft.Json.Linq

Partial Class iFFMA_Utility_PushNotificationList
    Inherits System.Web.UI.Page
    Private dtAR As DataTable

    Private intPageSize As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSearchType, strSearchValue As String

        Try
            'lblErr.Text = ""
            ''intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = "Push Notification - Salesrep List"
                .DataBind()
                .Visible = True
            End With

            ''Call Paging
            ''With wuc_dgpaging
            ''    .PageCount = dgList.PageCount
            ''    .CurrentPageIndex = dgList.PageIndex
            ''    .DataBind()
            ''    .Visible = True
            ''End With

            If Not IsPostBack Then
                BindGrid(ViewState("SortCol"))

                lblSearchValue.Visible = False
                lblDot.Visible = False
                txtSearchValue.Visible = False
                btnSearch.Visible = False
            End If
            '    'constant

            '    'request
            '    strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "user_name")
            '    strSearchValue = Trim(Request.QueryString("search_value"))
            '    If strSearchValue <> "" Then
            '        txtSearchValue.Text = strSearchValue
            '    End If

            '    'Prepare Recordset,Createobject as needed
            '    ddlSearchType.SelectedValue = strSearchType
            '    ddlSearchType_onChanged()

            'End If
        Catch ex As Exception
            ExceptionMsg("PushNotificationList.Page_Load : " & ex.ToString)
        End Try
    End Sub



    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim pushQuery = New mst_Utility.clsPushNotification
        Dim dt As DataTable

        Try
            If intPassFlag <> 1 Or IsNothing(ViewState("deviceTokenList")) Then

                Dim searchType As String = ddlSearchType.SelectedValue
                Dim searchValue As String = String.Empty

                Select Case searchType
                    Case "message"
                        searchValue = txtSearchValue.Text().Trim()
                    Case "all"
                        searchValue = "%"
                End Select
                dt = pushQuery.GetSalesrepList(Portal.UserSession.UserCode, ddlSearchType.SelectedValue) 'GetMessageList(searchType, searchValue, Portal.UserSession.UserCode)
                ViewState("deviceTokenList") = dt
            Else
                dt = ViewState("deviceTokenList")
            End If
            dt.DefaultView.Sort = SortExpression

            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()
            ChangeActive()

            If ddlSearchType.Items.Count = 0 Then
                With ddlSearchType
                    .DataSource = dt.DefaultView.ToTable(True, "team_code")
                    .DataTextField = "team_code"
                    .DataValueField = "team_code"
                    .DataBind()
                    .Items.Insert(0, New ListItem("All", ""))
                End With
            End If

            ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "$('#div_dgList').css('height','auto').css('padding-bottom','20px');", True)

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount.ToString
            '    .CurrentPageIndex = dgList.PageIndex
            'End With

        Catch ex As Exception
            ExceptionMsg("PushNotificationList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            pushQuery = Nothing
        End Try
    End Sub

    '#Region "dgList"
    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	how 
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim pushQuery = New mst_Utility.clsPushNotification
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgList.Rows(index)

        Try
            Select Case e.CommandName
                Case "Delete"
                    pushQuery.DeleteMessage(dgList.DataKeys(row.RowIndex).Value)
                    Response.Redirect(Request.RawUrl)
                Case "ViewHistory"
                    Dim dt = pushQuery.GetHistoryList(dgList.DataKeys(row.RowIndex).Value)

                    If dt.Rows.Count > 0 Then
                        With dgHistoryList
                            .DataSource = dt
                            .DataBind()
                        End With
                        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "showHistoryList();", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "alert('No history found!');", True)
                    End If
                Case "SendMsg"
                    Dim dt = pushQuery.GetSalesrepList(Portal.UserSession.UserCode, ddlSearchType.SelectedValue)
                    With dgSalesrepList
                        .DataSource = dt
                        .DataBind()
                    End With
                    hfSelectedMsgId.Value = dgList.DataKeys(row.RowIndex).Value
                    ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "showSalesrepList();", True)
            End Select

        Catch ex As Exception
            ExceptionMsg("PushNotificationList.dgList_RowCommand : " + ex.ToString)
        End Try
    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub dgList_RowDataBound
    '    ' Purpose	        :	
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------
    '    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
    '        Dim drv As DataRowView
    '        'Dim lblStatus As Label
    '        Dim imgEdit, imgDelete As ImageButton

    '        Try
    '            If dgList.Rows.Count > 0 Then
    '                If e.Row.RowType = DataControlRowType.DataRow Then
    '                    drv = CType(e.Row.DataItem, DataRowView)

    '                    'lblStatus = New Label
    '                    'lblStatus = CType(e.Row.FindControl("lblStatus"), Label)

    '                    If IsDBNull(drv("status")) Then
    '                        'lblStatus.Text = ""
    '                        imgDelete = CType(e.Row.Cells(5).Controls(0), ImageButton)
    '                        imgEdit = CType(e.Row.Cells(6).Controls(0), ImageButton)
    '                        imgDelete.Visible = False
    '                        imgEdit.Visible = False
    '                    Else
    '                        'lblStatus.Text = IIf(drv("status") = "1", "Active", "In-Active")
    '                    End If
    '                End If
    '                'dgList.Rows(e.Row.RowIndex).Cells(1).Text = IIf(e.Row.Cells(1).Text = "1", "Active", "In-Active")
    '            End If
    '        Catch ex As Exception
    '            ExceptionMsg("PushNotificationList.dgList_RowDataBound : " + ex.ToString)
    '        End Try
    '    End Sub

    Private Sub ChangeActive()
        Dim intloop As Integer = 0
        Do While intloop < dgList.Rows.Count
            With dgList.Rows(intloop).Cells(0)
                .Style.Add("width", "30px")
                .Style.Add("text-align", "center")
            End With
            With dgList.Rows(intloop).Cells(1)
                .Style.Add("text-align", "left")
            End With

            With dgList.Rows(intloop).Cells(2)
                .Style.Add("text-align", "left")
            End With

            'With dgList.Rows(intloop).Cells(3)
            '    .Style.Add("width", "80px")
            '    .Style.Add("text-align", "center")
            'End With

            'With dgList.Rows(intloop).Cells(4)
            '    .Style.Add("width", "20px")
            '    .Style.Add("text-align", "center")
            'End With
            intloop = intloop + 1
        Loop
    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub dgList_SortCommand
    '    ' Purpose	        :	This Sub manipulate the record sorting event
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------
    '    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
    '        ' Dim SortCol As 

    '        Try
    '            'For Each SortCol In dgList.Columns
    '            '    If SortCol.SortExpression = e.SortExpression Then
    '            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
    '            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
    '            '        Else
    '            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
    '            '        End If
    '            '    End If
    '            'Next SortCol

    '            Dim strSortExpression As String = ViewState("SortCol")

    '            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
    '                If strSortExpression Like (e.SortExpression & "*") Then
    '                    If strSortExpression.IndexOf(" DESC") > 0 Then
    '                        strSortExpression = e.SortExpression
    '                    Else
    '                        strSortExpression = e.SortExpression & " DESC"
    '                    End If
    '                Else
    '                    strSortExpression = e.SortExpression
    '                End If
    '            Else
    '                strSortExpression = e.SortExpression
    '            End If

    '            dgList.EditIndex = -1
    '            ViewState("SortCol") = strSortExpression
    '            BindGrid(ViewState("SortCol"))

    '        Catch ex As Exception
    '            ExceptionMsg("PushNotificationList.dgList_SortCommand : " + ex.ToString)
    '        End Try
    '    End Sub

    '    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
    '    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Dim pushQuery = New mst_Utility.clsPushNotification
        Dim index As Integer = Convert.ToInt32(e.NewEditIndex)
        Dim row As GridViewRow = dgList.Rows(index)
        Dim a = dgList.DataKeys(row.RowIndex).Value
        'strSessionId = Trim(Request.QueryString("sessionid"))
        'strVisitID = dgList.Rows(index).Cells(dgcol.VISIT_ID).Text
        'strTxnStatus = dgList.Rows(index).Cells(dgcol.TXN_STATUS).Text
        'strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
        'strCustCode = sender.datakeys(e.NewEditIndex).item("CUST_CODE")
        'strContCode = sender.datakeys(e.NewEditIndex).item("CONT_CODE")
        'strTxnNo = sender.datakeys(e.NewEditIndex).item("TXN_NO")
    End Sub
    '#End Region

    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            'ddlSearchType_onChanged()
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "PushNotificationList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub ddlSearchType_onChanged
    '    ' Purpose	        :	
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------
    Private Sub ddlSearchType_onChanged()
        Try
            Select Case ddlSearchType.SelectedValue
                Case "all"
                    lblSearchValue.Visible = False
                    lblDot.Visible = False
                    txtSearchValue.Visible = False
                    btnSearch.Visible = False
                Case Else
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    txtSearchValue.Visible = True
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "PushNotificationList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub btnCreateMsg_Click() Handles btnCreateMsg.Click
        Dim clsPushNotification = New mst_Utility.clsPushNotification
        'clsPushNotification.CreateMessage(txtMsg.Value)

        Dim title = txtTitle.Text,
            msg = txtMsg.Value,
            action = ddlAction.SelectedValue,
            url = txtURL.Text,
            expiryDays = ddlExpiryDays.SelectedValue,
            strID = hfSelectedTokenId.Value

        If action = "L" And url.Trim() = "" Then
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "alert('URL cannot be empty!');", True)
        Else
            'get token list
            Dim dt = clsPushNotification.GetToken(strID)

            If dt.Rows.Count > 0 Then
                Dim lstToken = dt.AsEnumerable().Select(Function(r) r.Field(Of String)("DEVICE_TOKEN")).ToList()

                clsPushNotification.CreateMessage(title, msg, action, url, expiryDays, strID)

                Dim config = New GcmConfiguration("61188310720", "AAAADj8bhsA:APA91bE79Phww_264eBr885wTHK4jLeP3gwmQK56lN6tET6rClEY9hhvhZ_MwEhrerz-FY_0FYWbb1rYHZaPECwKxg5nc04Gt33wI3CL3qbJmiMnfKCUyv2tQzO4j3qrxv-DT9TjVCHx", Nothing)
                config.GcmUrl = "https://fcm.googleapis.com/fcm/send"
                Dim GCM = New GcmServiceBroker(config)

                AddHandler GCM.OnNotificationSucceeded, AddressOf GCM_OnNotificationSucceeded
                AddHandler GCM.OnNotificationFailed, AddressOf GCM_OnNotificationFailed

                Dim key As String = ""
                If action = "M" Then
                    key = "message"
                    url = "Message"
                ElseIf action = "A" Then
                    key = "upload"
                    url = "Upload"
                ElseIf action = "L" Then
                    key = "link"
                End If

                GCM.Start()
                For Each token In lstToken
                    GCM.QueueNotification(New GcmNotification() With { _
                         .TimeToLive = expiryDays * 86400, _
                         .RegistrationIds = New List(Of String)() From {token}, _
                         .Notification = JObject.Parse("{ ""body"" : """ + msg + """, ""title"": """ + title + """}"), _
                         .Data = JObject.Parse("{ """ + key + """ : """ + url + """}"), _
                         .Priority = GcmNotificationPriority.High, _
                         .CollapseKey = Date.Now.ToString
                    })
                Next
            End If


            Response.Redirect(Request.RawUrl) 'Refresh page
        End If
    End Sub

    Private Sub btnSendMsg_Click() Handles btnSendMsg.Click
        Dim clsPushNotification = New mst_Utility.clsPushNotification

        Dim salesrepList = New List(Of String)
        For Each row As GridViewRow In dgSalesrepList.Rows
            Dim chkbox As CheckBox = row.FindControl("chk_box")
            If chkbox.Checked Then
                salesrepList.Add(dgSalesrepList.DataKeys(row.DataItemIndex).Value)
            End If
        Next
        If salesrepList.Count > 0 And Not String.IsNullOrEmpty(hfSelectedMsgId.Value) Then
            clsPushNotification.SendMessage(hfSelectedMsgId.Value, String.Join(",", salesrepList.ToArray()))
            hfSelectedMsgId.Value = String.Empty
            Response.Redirect(Request.RawUrl) 'Refresh page
        Else
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "alert('Please select salesrep first!');showSalesrepList();", True)
        End If
    End Sub

    Sub GCM_OnNotificationSucceeded(notification)

        Dim bbb = ":"
    End Sub
    Sub GCM_OnNotificationFailed(notification, aggregateEx)
        Dim errorEx = CType(aggregateEx, AggregateException)
        errorEx.Handle(Function(ex)
                           Select Case ex.GetType()
                               Case GetType(GcmNotificationException)
                               Case GetType(GcmMulticastResultException)
                               Case GetType(DeviceSubscriptionExpiredException)
                               Case GetType(RetryAfterException)
                               Case Else
                           End Select


                           'Dim watch1 As Stopwatch = Stopwatch.StartNew()

                           '' See what kind of exception it was to further diagnose
                           'If TypeOf ex Is GcmNotificationException Then
                           '    Dim notificationException = DirectCast(ex, GcmNotificationException)

                           '    ' Deal with the failed notification
                           '    Dim gcmNotification = notificationException.Notification
                           '    Dim description = notificationException.Description
                           '    errorMsgList.Add("GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}")
                           'ElseIf TypeOf ex Is GcmMulticastResultException Then
                           '    Dim multicastException = DirectCast(ex, GcmMulticastResultException)

                           '    For Each succeededNotification In multicastException.Succeeded
                           '        errorMsgList.Add("GCM Notification Failed: ID={succeededNotification.MessageId}")

                           '    Next

                           '    For Each failedKvp In multicastException.Failed
                           '        Dim n = failedKvp.Key
                           '        Dim e = failedKvp.Value

                           '        errorMsgList.Add("GCM Notification Failed: ID={n.MessageId}, Desc={e.Description}")
                           '    Next
                           'ElseIf TypeOf ex Is DeviceSubscriptionExpiredException Then
                           '    Dim expiredException = DirectCast(ex, DeviceSubscriptionExpiredException)

                           '    Dim oldId = expiredException.OldSubscriptionId
                           '    Dim newId = expiredException.NewSubscriptionId
                           '    errorMsgList.Add("Device RegistrationId Expired: {oldId}")

                           '    If Not String.IsNullOrWhiteSpace(newId) Then
                           '        ' If this value isn't null, our subscription changed and we should update our database
                           '        errorMsgList.Add("Device RegistrationId Changed To: {newId}")
                           '    End If
                           'ElseIf TypeOf ex Is RetryAfterException Then
                           '    Dim retryException = DirectCast(ex, RetryAfterException)
                           '    ' If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                           '    errorMsgList.Add("GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}")
                           'Else
                           '    errorMsgList.Add("GCM Notification Failed for some unknown reason")
                           'End If

                           'watch1.Stop()
                           '' Mark it as handled
                           Return True

                       End Function)
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    '#Region "Button operation"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnSearch_Click
    '    ' Purpose	        :	
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
        Catch ex As Exception
            lblErr.Text = "PushNotificationList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    Private Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Dim chkValue As CheckBox
        Dim strSettingCode As String
        Dim lstID = New List(Of String)

        For Each row As GridViewRow In dgList.Rows
            chkValue = CType(row.FindControl("chk_box"), CheckBox)

            If chkValue.Checked = True Then
                lstID.Add(dgList.DataKeys(row.RowIndex).Value)
                'strSettingCode = dgList.DataKeys(row.RowIndex).Value
            End If
        Next

        If lstID.Count > 0 Then
            hfSelectedTokenId.Value = String.Join(",", lstID)
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "showCreateMessage();", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), Guid.NewGuid().ToString("N"), "alert('Please select at least one salesrep!');", True)
        End If
    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnCreate_Click
    '    ' Purpose	        :	
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------
    '    'Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
    '    '    Try
    '    '        Response.Redirect("ConnectionStringCreate.aspx?search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
    '    '    Catch ex As Exception
    '    '        ExceptionMsg("deviceTokenList.btnCreate_Click : " & ex.ToString)
    '    '    End Try
    '    'End Sub

    '    'Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    '    Response.Redirect("../Menu.aspx", False)
    '    'End Sub
    '#End Region
End Class


