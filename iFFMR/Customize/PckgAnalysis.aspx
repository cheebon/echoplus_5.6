<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PckgAnalysis.aspx.vb" Inherits="iFFMR_Customize_PckgAnalysis" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Package Analysis Report</title>
    <link rel="stylesheet" href='~/include/DKSH.css' /> 
</head>
 <!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout">
    <form id="frmPckgAnalysis" runat="server">
    <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
    <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
    <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Always" RenderMode="Inline" >
    <ContentTemplate>
        <div style="padding:5px 0px 0px 5px;">
                            <fieldset class="" style="width: 98%;">
            <table id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
                <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
                <tr class="BckgroundInsideContentLayout">
                    <td><uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader></td>
                </tr>
                <tr style="height:5px">
                    <td></td>
                </tr>
                <tr class="BckgroundBenealthTitle">
                    <td>
                    
                    <table cellpadding="1" cellspacing="1" width="98%" class="cls_table">
                        <tr>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date:" CssClass="cls_label"></asp:Label> <customToolkit:wuc_txtDate ID="Wuc_txtDateStart" runat="server" ControlType="DateOnly" /></td>
                            <td><asp:Label ID="lblEndDate" runat="server" Text="End Date:" CssClass="cls_label"></asp:Label> <customToolkit:wuc_txtDate ID="Wuc_txtDateEnd" runat="server" ControlType="DateOnly" /></td>
                            <td><asp:Label ID="lblSalesRep" runat="server" Text="Sales Representative:" CssClass="cls_label"></asp:Label> 
                                <asp:DropDownList ID="ddlSR" runat="server" CssClass="cls_dropdownlist">
                                </asp:DropDownList></td>
                            <td>
                                <asp:Button ID="cmdSearch" runat="server" Text="Search" cssclass="cls_button" OnClick="cmdSearch_Click" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    
                    <%--<uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>--%>
                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="true"
                    AllowSorting="true" AutoGenerateColumns="false" GridWidth="" FreezeHeader="true" Width="98%" PagerSettings-Visible="false"
                    GridHeight="400px" RowSelectionEnabled="true" EnableViewState="true" OnSorting="dgList_Sorting">                    
                    <FooterStyle CssClass="GridFooter" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternate" />
                    <RowStyle CssClass="GridNormal" />
                    </ccGV:clsGridView>
                    </td>
                </tr>
            </table>
                          </fieldset>
            
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
