'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	10/04/2007
'	Purpose	    :	Mobile User Details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Public Class Admin_UserMobile_UserMobileDtl
    Inherits System.Web.UI.Page

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_PDAUser.clsPDAUserQuery
        Dim strUserID, strUserCode As String
        Dim dt As DataTable

        Try
            lblErr.Text = ""

            If Not IsPostBack Then
                'constant

                strUserID = Trim(Request.QueryString("user_id"))
                strUserCode = Trim(Request.QueryString("user_code"))

                'Call Header
                With wuc_lblheader
                    .Title = "Mobile User Details" 'strTitle
                    .DataBind()
                    .Visible = True
                End With

                'strUserID will be empty if redirected from create user
                If strUserID = "" Then
                    objUserQuery = New adm_PDAUser.clsPDAUserQuery
                    With objUserQuery
                        .clsProperties.strSalesRepCode = strUserCode
                        dt = .GetData
                    End With

                    If dt.Rows.Count > 0 Then
                        strUserID = dt.Rows(0).Item("user_mobile_id")
                    Else
                        Response.Redirect("UserMobileList.aspx", False)
                    End If
                End If

                objUserQuery = New adm_PDAUser.clsPDAUserQuery
                With objUserQuery
                    .clsProperties.strUserID = strUserID
                    dt = .GetUserDetail
                End With

                If dt.Rows.Count > 0 Then
                    lblUserIDValue.Text = Trim(dt.Rows(0)("user_mobile_id"))
                    lblStatusValue.Text = IIf(Trim(dt.Rows(0)("active_flag")) = "1", "Active", "In-Active")
                    lblUserCodeValue.Text = Trim(dt.Rows(0)("user_mobile_code"))
                    lblUserNameValue.Text = Trim(dt.Rows(0)("user_mobile_name"))
                    lblMobileCodeValue.Text = Trim(dt.Rows(0)("mobile_code"))
                    If IsDBNull(dt.Rows(0)("user_mobile_contactno")) Then
                        lblContactNoValue.Text = ""
                    Else
                        lblContactNoValue.Text = Trim(dt.Rows(0)("user_mobile_contactno"))
                    End If
                    If IsDBNull(dt.Rows(0)("user_mobile_email")) Then
                        lblEmailValue.Text = ""
                    Else
                        lblEmailValue.Text = Trim(dt.Rows(0)("user_mobile_email"))
                    End If
                    If IsDBNull(dt.Rows(0)("user_mobile_address")) Then
                        lblAddressValue.Text = ""
                    Else
                        lblAddressValue.Text = Trim(dt.Rows(0)("user_mobile_address"))
                    End If
                    If IsDBNull(dt.Rows(0)("comm_svr_setting_code")) Then
                        lblSettingCodeValue.Text = ""
                    Else
                        lblSettingCodeValue.Text = Trim(dt.Rows(0)("comm_svr_setting_code"))
                    End If
                End If
                End If

        Catch ex As Exception
            ExceptionMsg("UserMobileDtl.Page_Load : " & ex.ToString)
        Finally
            objUserQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnEdit_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Response.Redirect("UserMobileEdit.aspx?user_id=" & Trim(lblUserIDValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("UserMobileDtl.btnEdit_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnClose_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("UserMobileList.aspx", False)
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnDelete_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objUser As adm_PDAUser.clsPDAUser
        Try
            'If Trim(lblStatusValue.Text) = "Active" Then
            '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Please set status to In-Active before proceed.');</script>")
            '    Exit Sub
            'End If
            objUser = New adm_PDAUser.clsPDAUser
            With objUser
                .clsProperties.strUserID = Trim(lblUserIDValue.Text)
                .clsProperties.strSyncOperation = "D"
                .clsProperties.strActiveFlag = "0"
                .Suspend()
            End With

            Response.Redirect("UserMobileList.aspx", False)

        Catch ex As Exception
            ExceptionMsg("UserMobileDtl.btnDelete_Click : " & ex.ToString)
        Finally
            objUser = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
