﻿Imports System.Data
'Imports Ionic.Zip
Imports ICSharpCode.SharpZipLib.Zip
Imports System.IO
Imports NPOI.XSSF.UserModel
Imports NPOI.SS.UserModel
Imports System.Web.Script.Serialization

Partial Class iFFMR_OnlineGallery

    Inherits System.Web.UI.Page

    Private intPageSize As Integer
    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property vsSortCol() As String
        Get
            If ViewState("SortCol") Is Nothing Then
                ViewState("SortCol") = ""
            End If
            Return ViewState("SortCol")
        End Get
        Set(ByVal value As String)
            ViewState("SortCol") = value
        End Set
    End Property

    Private Property vsSortDir() As String
        Get
            If ViewState("SortDir") Is Nothing Then
                ViewState("SortDir") = "ASC"
            End If
            Return ViewState("SortDir")
        End Get
        Set(ByVal value As String)
            ViewState("SortDir") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'hfUserId.Value = Portal.PortalSession.UserProfile.User.UserID
        'hfPrincipalId.Value = Portal.PortalSession.UserProfile.User.PrincipalID
        'hfAppName.Value = ConfigurationManager.AppSettings("ServerName").ToString()
        'lblUserName.Text = Portal.PortalSession.UserProfile.User.UserName
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        If Not IsPostBack Then
            wuc_dgpaging.CurrentPageIndex = 0

            TimerControl1.Enabled = True
        End If

    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If TimerControl1.Enabled Then
                LoadTypeView()
                LoadSalerepDdl()
                LoadActivityDdl()
                LoadChannelDdl()

                txtDate.DateStart = Today.AddDays(-2).ToString("yyyy-MM-dd")
                txtDate.DateEnd = Today.AddDays(-1).ToString("yyyy-MM-dd")

                RefreshDatabindingImages()


            End If
            TimerControl1.Enabled = False
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadSalerepDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGallerySalesrepList(strUserID)

            With ddlSalesrep
                .Items.Clear()

                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadActivityDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGalleryActivityList(strUserID)

            If dt.Rows.Count > 0 Then
                With ddlActivity
                    .Items.Clear()
                    .DataTextField = "ACTIVITY_NAME"
                    .DataValueField = "ACTIVITY_CODE"
                    .DataSource = dt
                    .DataBind()
                    .Items.Insert(0, New ListItem("- Select -", ""))
                    '.SelectedIndex = 0
                End With
            End If
            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadSFMSCatDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGallerySFMSCategoryList(strUserID)

            With ddlSFMSCat
                .Items.Clear()
                .DataTextField = "CAT_NAME"
                .DataValueField = "CAT_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadSFMSSubCatDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGallerySFMSSubCategoryList(ddlSFMSCat.SelectedValue, strUserID)

            With ddlActivity
                .Items.Clear()
                .DataTextField = "SUB_CAT_NAME"
                .DataValueField = "SUB_CAT_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadMSSQuesDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGalleryMSSQuesList(ddlActivity.SelectedValue, strUserID)

            With ddlMSSQues
                .Items.Clear()
                .DataTextField = "QUES_NAME"
                .DataValueField = "QUES_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadMSSSubQuesDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGalleryMSSSubQuesList(ddlMSSQues.SelectedValue, strUserID)

            With ddlMSSSubQues
                .Items.Clear()
                .DataTextField = "SUB_QUES_NAME"
                .DataValueField = "SUB_QUES_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

#Region "Images"

    Private Sub RefreshDatabindingImages(Optional ByVal blnExport As Boolean = False)
        Try
            Dim dtResult As System.Data.DataTable = GetImages(IIf(blnExport, 1, 0)),
                totalRows As Integer = 0,
                dt2 As New DataTable

            dt2 = dtResult.Clone()

            If dtResult.Rows.Count = 0 Then
                dtResult.Rows.Add(dtResult.NewRow)
                Master_Row_Count = 0
            Else
                'Master_Row_Count = dtResult.Rows(0)("TTL_ROWS")
                If String.IsNullOrEmpty(vsSortCol) Then
                    vsSortDir = "ASC"
                End If

                For Each row As DataRow In dtResult.Rows
                    If File.Exists(Server.MapPath(row.Item("IMAGE_URL"))) Then
                        dt2.Rows.Add(row.ItemArray)
                        'totalRows += 1
                    End If
                Next
                Master_Row_Count = dtResult.Rows(0).Item("TTL_ROWS").ToString
            End If

            If Not IsNothing(dtResult) Then
                RImages.DataSource = dt2
                RImages.DataBind()

                rSlideShow.DataSource = dt2
                rSlideShow.DataBind()
            End If

            If Not blnExport Then
                With wuc_dgpaging
                    .RowCount = Master_Row_Count
                    .Visible = IIf(Master_Row_Count > 0, True, False)
                    .DataBind()
                End With
            End If

            UpdPnlImages.Update()
            UpdateSlideShow.Update()

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ReSelectedImages", "HighLightSelected(); setTimeout(function(){ LoadSlideShow(); }, 500); ", True)
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Private Function GetImages(ByVal intIsExport As Integer) As System.Data.DataTable
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")

            Dim strProduct As String = txtProduct.Text
            Dim strCustomer As String = txtCustomer.Text
            Dim strActivityCode As String = ddlActivity.SelectedValue
            Dim strSalesrepCode As String = ddlSalesrep.SelectedValue
            Dim strStartDate As String = txtDate.DateStart 'txtStartDate.Text
            Dim strEndDate As String = txtDate.DateEnd 'txtEndDate.Text

            Dim ddl01 As New DropDownList,
                ddl02 As New DropDownList,
                strCode01 As String = String.Empty,
                strCode02 As String = String.Empty,
                strCode03 As String = String.Empty

            If ddlFilterType.SelectedValue = 0 Then
                ddl01 = ddlMSSQues
                ddl02 = ddlMSSSubQues

                strCode01 = If(ddl01.SelectedValue = "selected", String.Empty, ddl01.SelectedValue)
                strCode02 = If(ddl02.SelectedValue = "selected", String.Empty, ddl02.SelectedValue)
            ElseIf ddlFilterType.SelectedValue = 1 Then
                ddl01 = ddlSFMSCat
                ddl02 = ddlActivity

                strCode01 = If(ddl01.SelectedValue = "selected", String.Empty, ddl01.SelectedValue)
                strCode02 = If(ddl02.SelectedValue = "selected", String.Empty, ddl02.SelectedValue)

            ElseIf ddlFilterType.SelectedValue = 2 Then
                strCode01 = txtPONO.Text
                strCode02 = txtSONO.Text
                strCode03 = ddlChannel.SelectedValue

            ElseIf ddlFilterType.SelectedValue = 3 Then

                strProduct = ""
                strActivityCode = txtCollectionNo.Text
                strCustomer = txtCollectorCustomerCode.Text 'ddlCollectorCustomerCode.SelectedValue
                strSalesrepCode = ddlCollectorCode.SelectedValue

            ElseIf ddlFilterType.SelectedValue = 4 Then
                strCode01 = txtPONO.Text
                strCode02 = txtSONO.Text
            End If



            dt = cls_OnlineGallery.GetOnlineGalleryList(vsSortCol, vsSortDir, wuc_dgpaging.CurrentPageIndex + 1, _
                                                        intPageSize, intIsExport, strUserID, strProduct, _
                                                        strCustomer, strActivityCode, strSalesrepCode, strStartDate, strEndDate,
                                                        ddlFilterType.SelectedValue.ToString, strCode01, strCode02, strCode03)

            'dt = cls_OnlineGallery.GetOnlineGalleryList(vsSortCol, vsSortDir, wuc_dgpaging.CurrentPageIndex + 1, _
            '                                            intPageSize, intIsExport, strUserID, strProduct, _
            '                                            strCustomer, strActivityCode, strSalesrepCode, strStartDate, strEndDate,
            '                                            ddlFilterType.SelectedValue.ToString, strCode01, strCode02)
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
        Return dt
    End Function

    Protected Sub RSearch_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles RImages.ItemDataBound
        Try

            Dim hfImageUrl As HiddenField = DirectCast(e.Item.FindControl("hfImageUrl"), HiddenField)
            Dim hfImageName As HiddenField = DirectCast(e.Item.FindControl("hfImageName"), HiddenField)
            Dim img As ImageButton = DirectCast(e.Item.FindControl("img"), ImageButton)
            Dim pnlName As Panel = DirectCast(e.Item.FindControl("pnlName"), Panel)

            If Not IsNothing(hfImageUrl) And Not IsNothing(img) Then
                img.ImageUrl = hfImageUrl.Value
                img.OnClientClick = "Select(this);HighLightSelected();LoadSelectedImage(); return false;"
            End If

            If Not IsNothing(hfImageName) And Not IsNothing(pnlName) Then
                pnlName.Attributes.Add("name", hfImageName.Value)
            End If

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

#Region "Paging Control"
    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            wuc_dgpaging.CurrentPageIndex = CInt(wuc_dgpaging.PageNo - 1)
            RefreshDatabindingImages()
            Exit Sub
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If wuc_dgpaging.CurrentPageIndex > 0 Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.CurrentPageIndex - 1
                wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1

                RefreshDatabindingImages()
            End If
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If wuc_dgpaging.PageCount - 1 > wuc_dgpaging.CurrentPageIndex Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.CurrentPageIndex + 1
                wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1

                RefreshDatabindingImages()
            End If
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    'Sub TOPFilter_Click(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.TOPFilter_Click
    '    Try
    '        RefreshDatabindingImages()
    '        Exit Sub

    '    Catch ex As Exception
    '        ExceptionMsg(ex)
    '    End Try
    'End Sub

    'Sub ddlPageSize_Click(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.ddlPageSize_Click
    '    Try
    '        RefreshDatabindingImages()
    '        Exit Sub

    '    Catch ex As Exception
    '        ExceptionMsg(ex)
    '    End Try
    'End Sub
#End Region
#End Region

#Region "DownloadAsZip"

    Protected Sub DownloadFiles(sender As Object, e As EventArgs)
        Dim dtResult As System.Data.DataTable = GetImages(1)
        If Not IsNothing(dtResult) Then
            Dim zipName As String = [String].Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"))

            Using zip = New FileStream(AppDomain.CurrentDomain.BaseDirectory + "\Documents\" + zipName, FileMode.OpenOrCreate, FileAccess.ReadWrite)
                Using zipStream = New ZipOutputStream(zip)
                    For Each row As DataRow In dtResult.Rows
                        Dim filePathName As String = row.Item("IMAGE_URL"),
                            fileName As String = row.Item("IMAGE_NAME")
                        If File.Exists(Server.MapPath(filePathName)) Then
                            Dim entry = New ZipEntry(fileName)
                            entry.IsUnicodeText = True
                            zipStream.PutNextEntry(entry)

                            Using reader = New FileStream(Server.MapPath(filePathName), FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                                Dim buffer__1 As Byte() = New Byte(4095) {}
                                Dim bytesRead As Integer
                                While (InlineAssignHelper(bytesRead, reader.Read(buffer__1, 0, buffer__1.Length))) > 0
                                    Dim actual As Byte() = New Byte(bytesRead - 1) {}
                                    System.Buffer.BlockCopy(buffer__1, 0, actual, 0, bytesRead)
                                    zipStream.Write(actual, 0, actual.Length)
                                End While
                            End Using
                        End If
                    Next
                End Using
            End Using

            Response.Clear()
            Response.BufferOutput = False
            Response.ContentType = "application/octet-stream"
            Response.AddHeader("content-disposition", "attachment; filename=" + zipName)
            Response.WriteFile(AppDomain.CurrentDomain.BaseDirectory + "\Documents\" + zipName)

            'Delete the file after generated and saved into Response
            File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\Documents\" + zipName)

            'zip.Save(Response.OutputStream)
            Response.[End]()

        End If


        'Using zip As New ZipFile()
        '    zip.AlternateEncodingUsage = ZipOption.AsNecessary
        '    zip.AddDirectoryByName("Files")
        'For Each row As GridViewRow In GridView1.Rows
        '    If TryCast(row.FindControl("chkSelect"), CheckBox).Checked Then
        '        Dim filePath As String = TryCast(row.FindControl("lblFilePath"), Label).Text
        '        zip.AddFile(filePath, "Files")
        '    End If
        'Next
        'Dim dtResult As System.Data.DataTable = GetImages(1)
        'If Not IsNothing(dtResult) Then
        '    For Each row As DataRow In dtResult.Rows
        '        Dim filePath As String = row.Item("IMAGE_URL")

        '        If File.Exists(Server.MapPath(filePath)) Then
        '            zip.AddFile(Server.MapPath(filePath), "Files")
        '        End If
        '    Next row
        'End If

        'Response.Clear()
        'Response.BufferOutput = False
        'Dim zipName As String = [String].Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"))
        'Response.ContentType = "application/zip"
        'Response.AddHeader("content-disposition", "attachment; filename=" + zipName)
        'zip.Save(Response.OutputStream)
        'Response.[End]()
        'End Using
    End Sub
#End Region

    Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
        target = value
        Return value
    End Function

#Region "ExportExcelImage"
    Protected Sub ExportExcel(sender As Object, e As EventArgs)
        'Convert the Relative Url to Absolute Url and set it to Image control.


        Using sw As New StringWriter()
            Using hw As New HtmlTextWriter(sw)
                'Create a Table.
                Dim table As New Table()

                Dim dtResult As System.Data.DataTable = GetImages(1)
                If Not IsNothing(dtResult) Then
                    Dim i As Integer = 0
                    For Each row As DataRow In dtResult.Rows
                        Dim filePath As String = row.Item("IMAGE_URL")

                        Dim image As New Image
                        image.ImageUrl = Server.MapPath(filePath)

                        'Dim label As New Label
                        'label.Text = Server.MapPath(filePath)

                        'Add Image control to the Table Cell.
                        Dim rowExcel As New TableRow()
                        rowExcel.Cells.Add(New TableCell())
                        rowExcel.Cells(0).Controls.Add(image)
                        'rowExcel.Cells.Add(New TableCell())
                        'rowExcel.Cells(1).Controls.Add(label)
                        table.Rows.Add(rowExcel)

                        i = i + 1
                    Next row
                End If



                'Render the Table as HTML.
                table.RenderControl(hw)

                'Export the Table to Excel.
                Response.Clear()
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=Images.xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.ms-excel"

                'Write the HTML string to Response.
                Response.Write(sw.ToString())
                Response.Flush()
                Response.End()
            End Using
        End Using
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(control As Control)
        ' Verifies that the control is rendered 
    End Sub

    Private Function GetAbsoluteUrl(relativeUrl As String) As String
        relativeUrl = relativeUrl.Replace("~/", String.Empty)
        Dim splits As String() = Request.Url.AbsoluteUri.Split("/")
        If splits.Length >= 2 Then
            Dim url As String = splits(0) + "//"
            For i As Integer = 2 To splits.Length - 2
                url += splits(i)
                url += "/"
            Next

            Return url & relativeUrl
        End If
        Return relativeUrl
    End Function

    Protected Sub ExportWord(sender As Object, e As EventArgs)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", _
         "attachment;filename=Images.doc")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-word "
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        RImages.RenderControl(hw)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()


    End Sub


    Private Sub ExportExcelFull()
        Dim workbook As IWorkbook = New XSSFWorkbook()
        Dim sheet1 As ISheet = workbook.CreateSheet("PictureSheet")


        Dim patriarch As IDrawing = sheet1.CreateDrawingPatriarch()


        Dim dtResult As System.Data.DataTable = GetImages(1)
        If Not IsNothing(dtResult) Then
            Dim i As Integer = 0

            Dim col1 As Integer = 0
            Dim row1 As Integer = 1
            Dim col2 As Integer = 4
            Dim row2 As Integer = 7

            Dim intColumnCntHdr As Integer = dtResult.Columns.Count - 1
            Dim intCoulmnIndexHdr As Integer = 0
            Dim SheetRowHdr As IRow = sheet1.CreateRow(0)

            While intCoulmnIndexHdr <= intColumnCntHdr
                Dim intCell As Integer = 5 + intCoulmnIndexHdr
                If dtResult.Columns(intCoulmnIndexHdr).ColumnName <> "ttl_rows" And dtResult.Columns(intCoulmnIndexHdr).ColumnName <> "IMAGE_URL" Then
                    SheetRowHdr.CreateCell(intCell).SetCellValue(dtResult.Columns(intCoulmnIndexHdr).ColumnName)
                End If
                intCoulmnIndexHdr = intCoulmnIndexHdr + 1
            End While

            For Each row As DataRow In dtResult.Rows
                Dim filePath As String = row.Item("IMAGE_URL")

                If Not File.Exists(Server.MapPath(filePath)) Then
                    Continue For
                End If
                Dim anchor As New XSSFClientAnchor(500, 200, 0, 0, col1, row1, col2, row2)
                anchor.AnchorType = AnchorType.MoveDontResize
                Dim imageId As Integer = LoadImage(Server.MapPath(filePath), workbook)
                Dim picture As XSSFPicture = DirectCast(patriarch.CreatePicture(anchor, imageId), XSSFPicture)
                picture.LineStyle = LineStyle.DashDotGel

                'Dim strFileName As String = row.Item("IMAGE_NAME")
                'sheet1.CreateRow(row1).CreateCell(5).SetCellValue(strFileName)

                Dim intColumnCnt As Integer = dtResult.Columns.Count - 1
                Dim intCoulmnIndex As Integer = 0
                Dim SheetRow As IRow = sheet1.CreateRow(row1)

                While intCoulmnIndex <= intColumnCnt
                    Dim intCell As Integer = 5 + intCoulmnIndex
                    If dtResult.Columns(intCoulmnIndex).ColumnName <> "ttl_rows" And dtResult.Columns(intCoulmnIndex).ColumnName <> "IMAGE_URL" Then
                        SheetRow.CreateCell(intCell).SetCellValue(row.Item(intCoulmnIndex).ToString)
                    End If
                    intCoulmnIndex = intCoulmnIndex + 1
                End While

                col1 = 0
                row1 = row1 + 7
                col2 = 4
                row2 = row2 + 7
                i = i + 1
            Next row
        End If


        Dim sw As FileStream = File.Create(Server.MapPath("../Documents/images.xlsx"))
        workbook.Write(sw)
        sw.Close()

        Response.Clear()
        Response.ClearHeaders()
        Response.ClearContent()
        Response.AddHeader("Content-Disposition", "attachment; filename=images.xlsx")
        Response.ContentType = "application/vnd.ms-excel"
        Response.Flush()
        Response.TransmitFile(Server.MapPath("../Documents/images.xlsx"))
        Response.[End]()

    End Sub

    Public Shared Function LoadImage(path As String, wb As IWorkbook) As Integer
        Dim file As New FileStream(path, FileMode.Open, FileAccess.Read)
        Dim buffer As Byte() = New Byte(file.Length - 1) {}
        file.Read(buffer, 0, CInt(file.Length))
        Return wb.AddPicture(buffer, PictureType.JPEG)

    End Function


#End Region

    Private Sub ExceptionMsg(ByVal objEx As Object)
        lblErr.Text = ""
        lblErr.Text = objEx.ToString 'Log.ExceptionMsg(objEx)
        updPnlErr.Update()
    End Sub

    Protected Sub btnExportExcel_Click(sender As Object, e As EventArgs)
        ExportExcelFull()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        RenewDataBind()
        'RefreshDatabindingImages()
    End Sub

    Public Sub RenewDataBind()
        wuc_dgpaging.CurrentPageIndex = 0
        'wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
        RefreshDatabindingImages()
    End Sub

    Protected Sub ddlFilterType_OnSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilterType.SelectedIndexChanged
        Try

            LoadTypeView()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub



    Protected Sub ddlSFMSCat_OnSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSFMSCat.SelectedIndexChanged
        Dim ddl As DropDownList = sender
        If Not ddl.SelectedValue.ToString = "selected" Then
            LoadSFMSSubCatDdl()
        Else
            ddlActivity.Items.Clear()
            ddlActivity.Items.Insert(0, New ListItem("- Select -", "selected"))
        End If
    End Sub

    Protected Sub ddlActivity_OnSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActivity.SelectedIndexChanged
        Dim ddl As DropDownList = sender
        If Not ddl.SelectedValue.ToString = "selected" Then
            LoadMSSQuesDdl()
        Else
            ddlActivity.Items.Clear()
            ddlActivity.Items.Insert(0, New ListItem("- Select -", "selected"))
        End If
    End Sub
    Protected Sub ddlMSSQues_OnSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMSSQues.SelectedIndexChanged
        Dim ddl As DropDownList = sender
        If Not ddl.SelectedValue.ToString = "selected" Then
            LoadMSSSubQuesDdl()
        Else
            ddlActivity.Items.Clear()
            ddlActivity.Items.Insert(0, New ListItem("- Select -", "selected"))
        End If
    End Sub

    Protected Sub LoadChannelDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGalleryChannelList(strUserID)

            With ddlChannel
                .Items.Clear()
                .DataTextField = "CHANNEL_NAME"
                .DataValueField = "CHANNEL_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Protected Sub LoadCollectorDdl()
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            dt = cls_OnlineGallery.GetOnlineGalleryCollectorList("%", strUserID)

            With ddlCollectorCode
                .Items.Clear()
                .DataTextField = "COLLECTOR_NAME"
                .DataValueField = "COLLECTOR_CODE"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function LoadCollectorCustDdlAutoComplete(ByVal strSearch As String, ByVal strCollectorCode As String) As String
        Dim dtResults As DataTable = Nothing
        Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
        Try
            Dim strUserId As String = Portal.UserSession.UserID

            With cls_OnlineGallery
                dtResults = .GetOnlineGalleryCollectorCustList(strUserId, strCollectorCode, strSearch)
            End With

            Dim ds As DataSet = New DataSet
            ds.Tables.Add(dtResults)

            'Dim dict As Dictionary(Of String, Object) = New Dictionary(Of String, Object)
            'For Each dt As DataTable In ds.Tables
            '    Dim arr() As Object = New Object(((dt.Rows.Count + 1)) - 1) {}
            '    Dim i As Integer = 0
            '    Do While (i _
            '                <= (dt.Rows.Count - 1))
            '        arr(i) = dt.Rows(i).ItemArray
            '        i = (i + 1)
            '    Loop
            '    dict.Add(dt.TableName, arr)
            'Next


            'Dim json As JavaScriptSerializer = New JavaScriptSerializer
            'Return json.Serialize(dict)

            Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            Dim row As Dictionary(Of String, Object) = Nothing

            For Each dr As DataRow In dtResults.Rows
                row = New Dictionary(Of String, Object)()

                For Each col As DataColumn In dtResults.Columns
                    row.Add(col.ColumnName.Trim(), dr(col))
                Next

                rows.Add(row)
            Next

            Return serializer.Serialize(rows)

        Catch ex As Exception
            'ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    

    Protected Sub LoadCollectorCustDdl(ByVal strSearch As String)
        Dim dt As New System.Data.DataTable
        Try
            Dim cls_OnlineGallery As New rpt_Customize.cls_OnlineGallery
            Dim strUserID As String = Session.Item("UserID")
            Dim strCollectorCode As String = ddlCollectorCode.SelectedValue
            dt = cls_OnlineGallery.GetOnlineGalleryCollectorCustList(strUserID, strCollectorCode, strSearch)

            With ddlCollectorCustomerCode
                .Items.Clear()
                .DataTextField = "cust_name"
                .DataValueField = "cust_code"
                .DataSource = dt
                .DataBind()
                .Items.Insert(0, New ListItem("- Select -", ""))
                .SelectedIndex = 0
            End With

            updPnlHdr.Update()

        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub

    Private Sub LoadTypeView()
        Try
            Dim ddl As DropDownList = ddlFilterType
            If ddl.SelectedItem.ToString = "Field Activity" Then

                PrdCust.Visible = True
                ActSr.Visible = True

                lblProduct.Text = "Category"
                lblActivity.Text = "Sub Category"
                MSS_Prd.Visible = False
                SFMS_Cat.Visible = True
                MSS_Ques.Visible = False
                SO.Visible = False
                LoadSFMSCatDdl()
                ddlActivity.Items.Clear()
                ddlActivity.Items.Insert(0, New ListItem("- Select -", "selected"))

                SO_PO_NO.Visible = False
                SO_SO_NO.Visible = False
                MSS_SFMS.Visible = True

                Collection.Visible = False
                Collector.Visible = False

            ElseIf ddl.SelectedItem.ToString = "Market Survey" Then

                PrdCust.Visible = True
                ActSr.Visible = True

                lblProduct.Text = "Product"
                lblActivity.Text = "Activity"
                MSS_Prd.Visible = True
                SFMS_Cat.Visible = False
                MSS_Ques.Visible = True
                SO.Visible = False
                LoadActivityDdl()

                ddlMSSQues.Items.Clear()
                ddlMSSQues.Items.Insert(0, New ListItem("- Select -", "selected"))
                ddlMSSSubQues.Items.Clear()
                ddlMSSSubQues.Items.Insert(0, New ListItem("- Select -", "selected"))

                SO_PO_NO.Visible = False
                SO_SO_NO.Visible = False
                MSS_SFMS.Visible = True

                Collection.Visible = False
                Collector.Visible = False

            ElseIf ddl.SelectedItem.ToString = "Sales Order" Then

                PrdCust.Visible = True
                ActSr.Visible = True

                lblProduct.Text = "PO No"
                lblActivity.Text = "SO No"
                MSS_Prd.Visible = False
                SFMS_Cat.Visible = False
                MSS_Ques.Visible = False
                SO.Visible = True

                SO_PO_NO.Visible = True
                SO_SO_NO.Visible = True
                MSS_SFMS.Visible = False

                Collection.Visible = False
                Collector.Visible = False

            ElseIf ddl.SelectedItem.ToString = "TRA Order" Then
                PrdCust.Visible = True
                lblProduct.Text = "Ref No"
                SO_PO_NO.Visible = True
                MSS_Prd.Visible = False
                txtProduct.Visible = False
                SFMS_Cat.Visible = False
                ddlSFMSCat.Visible = False

                ActSr.Visible = True
                lblActivity.Text = "Tra No."
                MSS_SFMS.Visible = False
                ddlActivity.Visible = False
                SO_SO_NO.Visible = True
                txtSONO.Visible = True

                MSS_Ques.Visible = False
                SO.Visible = False
                Collection.Visible = False
                Collector.Visible = False
            ElseIf ddl.SelectedItem.ToString = "Collection" Then

                PrdCust.Visible = False
                ActSr.Visible = False
                MSS_Ques.Visible = False
                SO.Visible = False

                Collection.Visible = True
                Collector.Visible = True

                LoadCollectorDdl()
                LoadCollectorCustDdl("%")
            End If
        Catch ex As Exception
            ExceptionMsg(ex)
        End Try
    End Sub


    Protected Sub ddlCollectorCode_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadCollectorCustDdl("%")
    End Sub

 

End Class


