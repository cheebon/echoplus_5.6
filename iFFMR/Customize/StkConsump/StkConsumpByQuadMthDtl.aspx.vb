﻿Imports System.Data

Partial Class iFFMR_Customize_StkConsump_StkConsumpByQuadDtl
    Inherits System.Web.UI.Page
#Region "Local Variable"

    Private intPageSize As Integer
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim licItemFigureCollector As ListItemCollection

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_StkConsump_StkConsumpByBiMthDtl"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "iFFMR_Customize_StkConsump_StkConsumpByBiMthDtl"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.STK_CONSUMP_BY_QUADMONTH_DTL)
            .DataBind()
            .Visible = True
        End With

        'Call Panel
        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.STK_CONSUMP_BY_QUADMONTH_DTL
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            TimerControl1.Enabled = True
        End If



    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDatabinding()
        End If

        TimerControl1.Enabled = False
    End Sub


#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = Nothing 'CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrenttable = GetRecList()
            ViewState("strSortExpression") = Nothing


            If dtCurrenttable Is Nothing Then
                dtCurrenttable = New DataTable
            Else
                PreRenderMode(dtCurrenttable)
                If dtCurrenttable.Rows.Count = 0 Then
                    dtCurrenttable.Rows.Add(dtCurrenttable.NewRow())
                End If
            End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
            dgList.PageIndex = 0

            LoadParamDisplay()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

#Region "ParamDisplay"


    Private Sub LoadParamDisplay()
        lblYear.Text = Request.QueryString("year")
        lblBiMonth.Text = Request.QueryString("bimonth")

        LoadLsbProduct()
        LoadLsbSalesrep()
        LoadLsbTeam()

    End Sub

    Private Sub LoadLsbProduct()
        Try
            lsbSelectedProduct.Items.Clear()

            Dim clsStkConsump As New rpt_Customize.clsStkConsump

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strPrdList As String

            strPrdList = Portal.UserSession.CuzRptPrdList

            With lsbSelectedProduct
                .DataSource = clsStkConsump.GetStkConsumpSelectedPrdList(strPrdList, strPrincipalID, strUserID)
                .DataTextField = "PRD_NAME"
                .DataValueField = "PRD_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbProduct : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub LoadLsbSalesrep()
        Try
            lsbSelectedSalesrep.Items.Clear()

            Dim clsStkConsump As New rpt_Customize.clsStkConsump

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strSalesrepList As String

            strSalesrepList = Portal.UserSession.CuzRptSrList

            With lsbSelectedSalesrep
                .DataSource = clsStkConsump.GetStkConsumpSelectedSalesrepList(strSalesrepList, strUserID, strPrincipalID)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill 
            lsbSelectedTeam.Items.Clear()

            Dim clsStkConsump As New rpt_Customize.clsStkConsump

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = Portal.UserSession.CuzRptTeamList

            With lsbSelectedTeam
                .DataSource = clsStkConsump.GetStkConsumpSelectedTeamList(strTeamList, strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region


    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then dgList_Init(DT)
            Cal_ItemFigureCollector(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_SFStkConsumpByQuadMthDtl.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_SFStkConsumpByQuadMthDtl.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_SFStkConsumpByQuadMthDtl.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFStkConsumpByQuadMthDtl.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFStkConsumpByQuadMthDtl.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName


                        Dim strUrlFields(0) As String
                        strUrlFields(0) = ""
                        'strUrlFields(0) = "ACTUAL_CALL"
                        'strUrlFields(1) = "SALESREP_CODE"
                        'strUrlFields(2) = "CUST_CODE"
                        'strUrlFields(3) = "CONT_CODE" 

                        'Dim strYear As String = ddlYear.SelectedValue
                        'Dim strBiMonth As String = ColumnName

                        Dim strUrlFormatString As String = ""
                        'strUrlFormatString = (" parent.document.getElementById('DetailBarIframe').src='StkConsumpByBiMthDtl.aspx?" & _
                        '                    "year=" & strYear & "&BiMonth=" & strBiMonth & " '")

                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SFStkConsumpByQuadMthDtl.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFStkConsumpByQuadMthDtl.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFStkConsumpByQuadMthDtl.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    For Each TC As TableCell In e.Row.Cells
                        If TC.Controls.Count > 0 Then
                            Dim HYL As HyperLink = CType(TC.Controls(0), HyperLink)
                            If HYL IsNot Nothing Then
                                Dim strlink As String = HYL.NavigateUrl
                                HYL.NavigateUrl = "#"
                                HYL.Target = String.Empty
                                HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();")
                            End If
                        End If
                    Next
                Case DataControlRowType.Footer

                    Dim iIndex As Integer
                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_SFStkConsumpByQuadMthDtl.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
            End Select

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try

    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "STOCK_QTY" OrElse strColumnName Like "STOCK_QTY_LAST" OrElse _
             strColumnName Like "TOTAL_SALES_QTY" OrElse strColumnName Like "CONSUMPTION" OrElse _
             strColumnName Like "CONSUMPTION_PER_MONTH" OrElse strColumnName Like "AVG_MONTHLY_CONSUMPTION" OrElse strColumnName Like "INVENTORY_MONTH") _
             AndAlso Not (strColumnName Like "*VAR" OrElse strColumnName Like "*GROSS") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        Dim dblStkQty As Double = 0
        Dim dblAvgMthCons As Double = 0

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                If li.Text = "INVENTORY_MONTH" Then
                    li.Value = IIf(dblAvgMthCons = 0, 0, dblStkQty / dblAvgMthCons)
                ElseIf li.Text = "STOCK_QTY" Then
                    dblStkQty = SUM(li.Value, DR(li.Text))
                    li.Value = SUM(li.Value, DR(li.Text))
                ElseIf li.Text = "AVG_MONTHLY_CONSUMPTION" Then
                    dblAvgMthCons = SUM(li.Value, DR(li.Text))
                    li.Value = SUM(li.Value, DR(li.Text))
                Else
                    li.Value = SUM(li.Value, DR(li.Text))
                End If

            Next
        Next

    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            Dim clsStkConsump As New rpt_Customize.clsStkConsump
            Dim strSalesrepCode As String = Portal.UserSession.CuzRptSrList
            Dim strYear As String = Request.QueryString("year")
            Dim strPrdList As String = Portal.UserSession.CuzRptPrdList
            Dim strQuadMonth As String = Request.QueryString("Quadmonth")
            Dim strUserId As String = Portal.UserSession.UserID

            DT = clsStkConsump.GetStkConsumpByQuadMonthlyDtl(strYear, strSalesrepCode, strPrdList, strQuadMonth, strUserId)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub


#End Region

#Region "EVENT HANDLER"
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub



#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()
            dt.Rows.Add(dt.NewRow)
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try

            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()


        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub


#End Region

End Class
Public Class CF_SFStkConsumpByQuadMthDtl

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "DISTRICT_CODE"
                strFieldName = "District Code"
            Case "DISTRICT_NAME"
                strFieldName = "District Name"
            Case "STOCK_TAKE_DATE"
                strFieldName = "Stock Take Date"
            Case "STOCK_QTY"
                strFieldName = "Stock Qty. (Current)"
            Case "STOCK_QTY_LAST"
                strFieldName = "Stock Qty. (Last Bi Month)"
            Case "TOTAL_SALES_QTY"
                strFieldName = "Total Sales Qty."
            Case "CONSUMPTION"
                strFieldName = "Consumption"
            Case "CONSUMPTION_PER_MONTH"
                strFieldName = "Consumption per Month"
            Case "AVG_MONTHLY_CONSUMPTION"
                strFieldName = "Avg. Monthly Consumption"
            Case "INVENTORY_MONTH"
                strFieldName = "Inventory Month"
            Case "CUST_CLASS"
                strFieldName = "Class"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn

            Else
                If strColumnName Like "YEAR" OrElse strColumnName Like "QUAD_WEEK_NO" OrElse strColumnName Like "QUAD_WEEK_NAME" OrElse strColumnName Like "STOCK_TAKE_DATE_LAST" Then
                    FCT = FieldColumntype.InvisibleColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If

            End If


            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "STOCK_TAKE_DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "STOCK_QTY", "STOCK_QTY_LAST", "TOTAL_SALES_QTY", "CONSUMPTION"
                    strFormatString = "{0:#,0}"
                Case "CONSUMPTION_PER_MONTH", "AVG_MONTHLY_CONSUMPTION", "INVENTORY_MONTH"
                    strFormatString = "{0:#,0.########}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName = "STOCK_QTY" Or strColumnName = "STOCK_QTY_LAST" Or strColumnName = "TOTAL_SALES_QTY" Or strColumnName = "CONSUMPTION" _
                Or strColumnName = "CONSUMPTION_PER_MONTH" Or strColumnName = "AVG_MONTHLY_CONSUMPTION" Or strColumnName = "INVENTORY_MONTH" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class