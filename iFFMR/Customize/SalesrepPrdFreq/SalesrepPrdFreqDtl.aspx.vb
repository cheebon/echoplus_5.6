Imports System.Data
Partial Class SalesrepPrdFreqDtl
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property
#End Region


#Region "Criteria Collector"
    Dim WithEvents clsCriteriaCollector As clsSharedValue
    Dim strCollectorName As String = "Collector_CallAnalysisListByCustomer"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CallAnalysisListByCustomer.aspx"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CABYCUST) '"Call Analysis List By Customer"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CABYCUST
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""


            Dim intYear As Integer = Session("Year")
            Dim intMonth As Integer
            Dim intDay As Integer = 0

            If Not IsPostBack Then
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

                Session("SRPRDFREQDTL_QueryString") = Request.QueryString.ToString.Replace("+", "")

                intMonth = CInt(Request.QueryString("MONTH"))
                intDay = CInt(Request.QueryString("DAY"))

                If intMonth = 0 Then intMonth = Session("MONTH")
                'Dim strSalesman As String = Trim(Request.QueryString("SALESREP_CODE"))
                'If Not String.IsNullOrEmpty(strSalesman) Then session("SALESREP_CODE") = strSalesman

                ViewState("dtCurrentView") = Nothing
                ViewState("SessionMonth") = Session("MONTH")

                Dim strPageIndicator As String = Trim(Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(strPageIndicator) Then
                    Dim strQueryString As String = String.Empty
                    If strPageIndicator = "CALLBYDAY" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx"
                        strQueryString = Session("CallAnalysisListByDay_QueryString")
                    ElseIf strPageIndicator = "SALESACTY" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/KPI/ActSalesBySalesrep.aspx"
                        strQueryString = Session("CallEnquiry_QueryString")
                    ElseIf strPageIndicator = "DAILYCALLANALYSIS" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/DailyCallAnalysis.aspx"
                        strQueryString = Session("DailyCallAnalysis_QueryString")
                    ElseIf strPageIndicator = "FIELDFORCEPRDFREQ" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/SalesrepPrdFreq/SalesrepPrdFreqGrid.aspx"
                        strQueryString = "PAGE_INDICATOR=CALLANABYCUST"
                    ElseIf strPageIndicator = "CALLANALYBYCONTCLASS" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/CallAnalysisByContClass.aspx"
                        strQueryString = "PAGE_INDICATOR=CABYCUST"
                    End If
                    If Not String.IsNullOrEmpty(strQueryString) Then ViewState("POSTBACK_URL") += "?" & strQueryString
                End If

                TimerControl1.Enabled = True
            Else
                If ViewState("SessionMonth") <> Session("MONTH") Then
                    ViewState("SessionMonth") = Session("MONTH")
                    ViewState("MonthInThisPage") = Session("MONTH")
                End If

                intMonth = ViewState("MonthInThisPage")
                If intMonth = 0 Then intMonth = Session("MONTH")

                intDay = ViewState("DayInThisPage")
            End If

            If intDay = 0 Then intDay = 1

            ViewState("DayInThisPage") = intDay
            ViewState("MonthInThisPage") = intMonth

            'If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then btnBack.PostBackUrl = ViewState("POSTBACK_URL")

            'Report.UpdateTreePath("SALESREP_CODE", Trim(Request.QueryString("SALESREP_CODE")))



        Catch ex As Exception

        End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    Try
    ''        wuc_ctrlPanel.RefreshDetails()
    ''        wuc_ctrlPanel.UpdateControlPanel()
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "CallAnalysisByCustomer")
            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Else
                PreRenderMode(dtCurrentTable)
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallByCustomer.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CallByCustomer.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CallByCustomer.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallByCustomer.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallByCustomer.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing

                        strUrlFormatString = FormUrlFormatString(ColumnName, dtToBind, strUrlFields)
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CallByCustomer.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            'dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallByCustomer.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallByCustomer.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function FormUrlFormatString(ByVal strColumnName As String, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Try
            ReDim strUrlFields(4)
            strUrlFields(0) = "SALESREP_CODE"
            strUrlFields(1) = "VISIT_ID"
            strUrlFields(2) = "CUST_CODE"
            strUrlFields(3) = "DATE"
            strUrlFields(4) = "CONT_CODE"

            Select Case strColumnName
                Case "SFMS_IND" 'Generate SFMS_IND
                    strUrlFormatString = "~/iFFMR/SFE/CallProd/CallProdDailyActivity.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "SALES_IND"
                    strUrlFormatString = "~/iFFMR/Sales/SalesOrder.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "TRA_IND"
                    strUrlFormatString = "~/iFFMR/TRA/TraRetDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "COLL_IND"
                    strUrlFormatString = "~/iFFMR/COLL/COLLDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "DRC_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/DRCDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "MSS_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/MSSDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "DN_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/DNDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case "HIST_IND"
                    strUrlFormatString = "~/iFFMR/Customize/CallActyHistByCont.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=SRPRDFREQDTL"
                Case Else
                    strUrlFields = Nothing
                    strUrlFormatString = ""
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        End Try
        Return strUrlFormatString
    End Function

    'Protected Sub dgList_Init(ByVal DT As DataTable)
    '    Dim strColumnName, strUrlFormatString As String

    '    Try
    '        aryDataItem.Clear()
    '        dgList.Columns.Clear()
    '        'For Each Column As DataControlField In dgList.Columns
    '        '    aryDataItem.Add(Column.SortExpression)
    '        'Next

    '        For int As Integer = 0 To DT.Columns.Count - 1
    '            strColumnName = DT.Columns(int).ColumnName.ToUpper
    '            Select Case CF_CallByCustomer.getFieldColumnType(strColumnName)
    '                Case FieldColumntype.BoundColumn
    '                    Dim dgcolumn As New BoundField
    '                    dgcolumn.HeaderText = CF_CallByCustomer.GetDisplayColumnName(strColumnName)
    '                    dgcolumn.ItemStyle.HorizontalAlign = CF_CallByCustomer.GetColumnStyle(strColumnName).HorizontalAlign
    '                    dgcolumn.DataField = strColumnName
    '                    dgcolumn.SortExpression = strColumnName
    '                    dgList.Columns.Add(dgcolumn)
    '                    aryDataItem.Add(strColumnName)
    '                    dgcolumn = Nothing
    '                Case FieldColumntype.HyperlinkColumn
    '                    Dim dgcolumn As New HyperLinkField
    '                    dgcolumn.HeaderText = CF_CallByCustomer.GetDisplayColumnName(strColumnName)
    '                    dgcolumn.ItemStyle.HorizontalAlign = CF_CallByCustomer.GetColumnStyle(strColumnName).HorizontalAlign
    '                    dgcolumn.DataTextField = strColumnName
    '                    dgcolumn.SortExpression = strColumnName
    '                    Dim strUrlFields(3) As String
    '                    strUrlFields(0) = "SALESREP_CODE"
    '                    strUrlFields(1) = "VISIT_ID"
    '                    strUrlFields(2) = "CUST_CODE"
    '                    strUrlFields(3) = "CALL_DATE"
    '                    Select Case strColumnName
    '                        Case "SFMS_IND"
    '                            strUrlFormatString = "~/iFFMR/SFE/CallProd/CallProdDailyActivity.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
    '                        Case "SALES_IND"
    '                            strUrlFormatString = "~/iFFMR/Sales/SalesOrder.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
    '                    End Select
    '                    dgcolumn.DataNavigateUrlFields = strUrlFields
    '                    dgcolumn.DataNavigateUrlFormatString = strUrlFormatString
    '                    dgcolumn.Target = "_self"
    '                    dgList.Columns.Add(dgcolumn)
    '                    aryDataItem.Add(strColumnName)
    '                    dgcolumn = Nothing
    '                Case FieldColumntype.InvisibleColumn

    '            End Select
    '        Next
    '        'Dim strUrlFields(3) As String
    '        'strUrlFields(0) = "SALESREP_CODE"
    '        'strUrlFields(1) = "VISIT_ID"
    '        'strUrlFields(2) = "CUST_CODE"
    '        'strUrlFields(3) = "CALL_DATE"

    '        'Dim dgColumn_SALES_INDICATOR As HyperLinkField = dgList.Columns(aryDataItem.IndexOf("SALES_IND"))
    '        'If dgColumn_SALES_INDICATOR IsNot Nothing Then
    '        '    Dim strUrlFormatString As String = "~/iFFMR/Sales/SalesOrder.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
    '        '    dgColumn_SALES_INDICATOR.DataNavigateUrlFields = strUrlFields
    '        '    dgColumn_SALES_INDICATOR.DataNavigateUrlFormatString = strUrlFormatString
    '        '    dgColumn_SALES_INDICATOR.Target = "_self"
    '        'End If

    '        'Dim dgColumn_SFMS_INDICATOR As HyperLinkField = dgList.Columns(aryDataItem.IndexOf("SFMS_IND"))
    '        'If dgColumn_SFMS_INDICATOR IsNot Nothing Then
    '        '    Dim strUrlFormatString As String = "~/iFFMR/SFE/CallProd/CallProdDailyActivity.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
    '        '    dgColumn_SFMS_INDICATOR.DataNavigateUrlFields = strUrlFields
    '        '    dgColumn_SFMS_INDICATOR.DataNavigateUrlFormatString = strUrlFormatString
    '        '    dgColumn_SFMS_INDICATOR.Target = "_self"
    '        'End If


    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
    '    End Try
    'End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strYear, strMonth, strDay, strUserID, strPrincipalCode, strPrincipalID, strSalesrepCode As String

            If ViewState("SessionMonth") <> Session("MONTH") Then
                ViewState("SessionMonth") = Session("MONTH")
                ViewState("MonthInThisPage") = Session("MONTH")
            End If

            strYear = CInt(Session.Item("Year"))
            strMonth = CInt(ViewState("MonthInThisPage"))
            strDay = CInt(ViewState("DayInThisPage"))
            If strMonth = 0 Then strMonth = Session("MONTH")

            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE")) 'session("SALESREP_CODE")
            'Wuc_lblDate1.ControlType = include_wuc_lblDate.DateTimeControlType.DateOnly
            'Wuc_lblDate1.SelectedDateTime = New Date(CInt(strYear), CInt(strMonth), CInt(strDay))

            wuc_ctrlpanel.SelectedDateTimeValue = New Date(CInt(strYear), CInt(strMonth), CInt(strDay))
            'ViewState("SelectedDate") = wuc_ctrlpanel.SelectedDateTimeString

            Dim PAGE_INDICATOR As String
            PAGE_INDICATOR = Request.QueryString("PAGE_INDICATOR")

            If PAGE_INDICATOR = "FIELDFORCEPRDFREQ" Then
                Dim clsSFPrdFreq As New rpt_Customize.clsSFPrdFreq
                With clsSFPrdFreq.properties
                    .UserID = Session.Item("UserID")
                    .PrinID = Session("PRINCIPAL_ID")
                    .PrinCode = Session("PRINCIPAL_CODE")
                    .SalesrepCode = Request.QueryString("SALESREP_CODE")
                    .CustCode = Request.QueryString("CUST_CODE")
                    .ContCode = Request.QueryString("CONT_CODE")
                    .StartYear = Request.QueryString("STARTYEAR")
                    .StartMonth = Request.QueryString("STARTMONTH")
                    .EndYear = Request.QueryString("ENDYEAR")
                    .EndMonth = Request.QueryString("ENDMONTH")
                    .PTLCode = Request.QueryString("CAT_CODE")
                End With

                DT = clsSFPrdFreq.GetSFPrdFreqSFMS

            ElseIf PAGE_INDICATOR = "CALLANALYBYCONTCLASS" Then

                Dim strSalesrepCode2 As String, strCustCode As String, strContCode As String, stryear2 As String, strmonth2 As String _
                , strday2 As String, strAgencyCode As String
                strSalesrepCode2 = Request.QueryString("SALESREP_CODE")
                strCustCode = Request.QueryString("CUST_CODE")
                strContCode = Request.QueryString("CONT_CODE")
                stryear2 = Request.QueryString("YEAR")
                strmonth2 = Request.QueryString("MONTH")
                strday2 = Request.QueryString("DAY")
                strAgencyCode = Request.QueryString("AGENCY_CODE")

                Dim clsCallDB As New rpt_CALL.clsCallQuery
                DT = clsCallDB.GetCallByCustomerADV(strUserID, strPrincipalID, strPrincipalCode, stryear2, strmonth2, strday2, strSalesrepCode2, _
                strCustCode, strContCode, strAgencyCode)

            Else
                Dim clsCallDB As New rpt_CALL.clsCallQuery
                DT = clsCallDB.GetCallByCustomer(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strDay, strSalesrepCode)
            End If
            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub


End Class

Public Class CF_CallByCustomer
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "DEPARTMENT"
                strFieldName = "Department"
            Case "POSITION"
                strFieldName = "Position"
            Case "TIME_IN"
                strFieldName = "Time In"
            Case "TIME_OUT"
                strFieldName = "Time Out"
            Case "TIME_SPEND"
                strFieldName = "Time Spend"
            Case "VISIT_IND"
                strFieldName = "Type"
            Case "COOR_LOC"
                strFieldName = "Location"
            Case "CALL_DATE"
                strFieldName = "Date"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Select Case strColumnName.ToUpper
                Case "SFMS_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CPRODACTYDTL, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "SALES_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESORD, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "TRA_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.TRAINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "COLL_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.COLLINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DRC_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DRCINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "MSS_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.MSSINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DN_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DNINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "HIST_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CALLACTYHISTBYCONT, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "SALESREP_CODE", "REASON_CODE", "VISIT_ID", "DATE"
                    FCT = FieldColumntype.InvisibleColumn
                Case "REASON_NAME"
                    FCT = FieldColumntype.BoundColumn
            End Select

            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "COOR_LOC"
                    strFormatString = "{0}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function


    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
