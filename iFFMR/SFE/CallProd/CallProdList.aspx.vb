Imports System.Data

Partial Class CallProdList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _aryAllColumnName As ArrayList
    Protected Property aryAllColumnName() As ArrayList
        Get
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = ViewState("AllDataItem")
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = New ArrayList
            Return _aryAllColumnName
        End Get
        Set(ByVal value As ArrayList)
            ViewState("AllDataItem") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "    Criteria Collector"
    Dim strCollectorName As String = "Collector_CallProdList"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region


    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property


    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CallProductivity"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Try
        If Not IsPostBack Then
            'List Year Cycle
            YearCycleController.LoadYearCycle()

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CPROD) '"SFMS Activities Statistic Information "
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CPROD
                .DataBind()
                .Visible = True
                .blnMakeGroupingFiledDistinct = True
                .intListBoxIndexToFilter = 0
            End With
            GroupingValue = ""
            BindDDLYear()

            Dim strNodeName As String
            Dim strNodeValue As String

            strNodeName = Session("NODE_GROUP_NAME")
            strNodeValue = Trim(Request.QueryString("TREE_" + strNodeName))

            Report.UpdateTreePath(strNodeName, strNodeValue)

            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    Session("TREE_PATH") = .Tree_Path
                    If .ReportType < ddlReportType.Items.Count Then ddlReportType.SelectedIndex = .ReportType
                    If ddlYear.Items.Contains(New ListItem(.StartYear, .StartYear)) Then ddlYear.SelectedValue = .StartYear
                    GroupingValue = .GroupField
                    SortingExpression = .SortExpression
                    wuc_ctrlpanel.strRestoreGroupingTextValue = .GroupTextValue
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                ChangeReportType()
                'Dim intMonth As Integer = CInt(Session.Item("Month"))
                'If intMonth > 6 Then ddlMonthType.SelectedIndex = 1

                If Not String.IsNullOrEmpty(Trim(Request.QueryString("TREE_TEAM_CODE"))) Then
                    ddlReportType.SelectedIndex = CriteriaCollector.ReportType
                    wuc_Menu.RestoreSessionState = True
                Else
                    CriteriaCollector = Nothing
                End If
                '***********************************
                'Edited by Alex Chia 2007-08-02
                'Additional AddIn, to make the report more meaning full
                wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, "")
                wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow), "SFMS Category", "CAT_CODE")
                '***********************************
            End If
            TimerControl1.Enabled = True
        End If
        lblErr.Text = ""
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try
    End Sub


#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        '  Try
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        '   Try
        ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Sub BindDDLYear()
        Dim liLast2Year As New ListItem
        liLast2Year.Text = Date.Now.AddYears(-2).Year
        liLast2Year.Value = Date.Now.AddYears(-2).Year
        ddlYear.Items.Add(liLast2Year)

        Dim liLastYear As New ListItem
        liLastYear.Text = Date.Now.AddYears(-1).Year
        liLastYear.Value = Date.Now.AddYears(-1).Year
        ddlYear.Items.Add(liLastYear)

        Dim liThisYear As New ListItem
        liThisYear.Text = Date.Now.Year
        liThisYear.Value = Date.Now.Year
        ddlYear.Items.Add(liThisYear)
    End Sub
    Private Sub ChangeReportType()
        '  Try

        Dim dt As DataTable
        Dim clsCallQuery As New rpt_CALL.clsCallQuery

        dt = clsCallQuery.GetMonthlyCallRptType(Session.Item("Year"), Session.Item("Month"), Session.Item("UserID"))
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0) = "2" Then 'ddlReportType.SelectedIndex = dt.Rows(0)(0) = "2" Then
                ddlReportType.SelectedIndex = 1
            Else
                ddlReportType.SelectedIndex = dt.Rows(0)(0)
            End If

            If ddlYear.Items.Contains(New ListItem(dt.Rows(0)("START_YEAR").ToString, dt.Rows(0)("START_YEAR").ToString)) Then ddlYear.SelectedValue = dt.Rows(0)("START_YEAR").ToString 'ddlYear.Items.FindByValue(dt.Rows(0)("START_YEAR").ToString).Selected = True
        End If

        '--------------------------------------------------------
        'Dim intMonth As Integer = CInt(Session.Item("Month"))
        'If intMonth <= 6 Then
        '    ddlReportType.SelectedIndex = 0
        'ElseIf intMonth > 6 Then
        '    ddlReportType.SelectedIndex = 1
        'Else
        '    ddlReportType.SelectedIndex = 2
        'End If
        '--------------------------------------------------------

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        '  Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        '  Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.GroupingFieldChanged
        ' Try
        GroupingValue = String.Empty
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        'Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        'wuc_ctrlpanel.ExportToFile(dgList, "CallProductivity" & ddlReportType.SelectedItem.Text)
        'wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ' ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        'Try
        ' If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
        End If

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        ElseIf dtCurrentTable.Rows.Count = 0 Then
            dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        'End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        '  Try
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Private Function GenerateAllColumnName(ByRef dtToBind As DataTable) As ArrayList
        Dim aryList As New ArrayList
        ' Try
        For Each DC As DataColumn In dtToBind.Columns
            aryList.Add(DC.ColumnName.ToUpper)
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GenerateAllColumnName : " & ex.ToString)
        'End Try
        Return aryList
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Dim aryAllColumnName As ArrayList = GenerateAllColumnName(dtToBind)

        '  Try
        'Add Data Grid Columns
        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
            Select Case CF_CallProdList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CallProdList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_CallProdList.GetOutputFormatString(ColumnName)
                    End If

                    dgColumn.ItemStyle.HorizontalAlign = CF_CallProdList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallProdList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName


                    'Dim intmonth As Integer = CF_CallProdList.GetMonthValue(ColumnName)
                    'Dim strUrlFields(6) As String

                    ''Pass in 'intMONTH , AGENCY_CODE (TEAM_CODE) , CAT_CODE , SUB_CAT , CUST_CODE , CUSTGROUP
                    'strUrlFields(0) = IIf(aryAllColumnName.Contains("TEAM_CODE"), "TEAM_CODE", "")
                    'strUrlFields(1) = IIf(aryAllColumnName.Contains("CAT_CODE"), "CAT_CODE", "")
                    'strUrlFields(2) = IIf(aryAllColumnName.Contains("SUB_CAT_CODE"), "SUB_CAT_CODE", "")
                    'strUrlFields(3) = IIf(aryAllColumnName.Contains("CUST_CODE"), "CUST_CODE", "")
                    'strUrlFields(4) = IIf(aryAllColumnName.Contains("CUST_GRP_CODE"), "CUST_GRP_CODE", "")
                    'strUrlFields(5) = IIf(aryAllColumnName.Contains("SALESREP_CODE"), "SALESREP_CODE", "")
                    'strUrlFields(6) = IIf(aryAllColumnName.Contains("REGION_CODE"), "REGION_CODE", "")

                    'Dim strSalesrepCode As String = Session("SALESREP_CODE")
                    'Dim strRegionCode As String = Session("REGION_CODE")
                    'Dim strTeamCode As String = Session("TEAM_CODE")
                    'Dim strGroupingValue As String = wuc_ctrlpanel.pnlField_GetGroupingValue

                    'Dim strUrlFormatString As String = ""
                    ''To Make sure Team and Salesman was specified before proceed to next page.
                    ''Logic talk: 
                    ''Is Team is not provided, or Salesman is not provided, redirect to same page.
                    ''If String.IsNullOrEmpty(strTeamCode) OrElse String.IsNullOrEmpty(strRegionCode) OrElse _
                    ''(String.IsNullOrEmpty(strGroupingValue) AndAlso String.IsNullOrEmpty(strSalesrepCode)) Then _
                    '' 'OrElse (Not String.IsNullOrEmpty(strGroupingValue) AndAlso strGroupingValue Like "*SALESREP_NAME*" = False) _
                    ''    strUrlFormatString = "CallProdList.aspx?" & "&CAT_CODE={1}&SUB_CAT_CODE={2}&CUST_CODE={3}&CUST_GRP_CODE={4}&TEAM_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strTeamCode, "") & "&SALESREP_CODE={5}" & IIf(String.IsNullOrEmpty(strUrlFields(5)), strSalesrepCode, "")
                    ''Else
                    ''    strUrlFormatString = "CallProdActivityDetail.aspx?MONTH=" & intmonth & "&CAT_CODE={1}&SUB_CAT_CODE={2}&CUST_CODE={3}&CUST_GRP_CODE={4}&TEAM_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strTeamCode, "") & "&SALESREP_CODE={5}" & IIf(String.IsNullOrEmpty(strUrlFields(5)), strSalesrepCode, "")
                    ''End If

                    'If (aryAllColumnName.Contains("TEAM_CODE") OrElse aryAllColumnName.Contains("REGION_CODE")) _
                    '    AndAlso (Not (aryAllColumnName.Contains("SALESREP_CODE") OrElse _
                    '            aryAllColumnName.Contains("CAT_CODE") OrElse _
                    '            aryAllColumnName.Contains("SUB_CAT_CODE") OrElse _
                    '            aryAllColumnName.Contains("CUST_CODE") OrElse _
                    '            aryAllColumnName.Contains("CUST_GRP_CODE"))) Then
                    '    strUrlFormatString = "CallProdList.aspx?" & "&CAT_CODE={1}&SUB_CAT_CODE={2}&CUST_CODE={3}&CUST_GRP_CODE={4}&TEAM_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strTeamCode, "") & "&REGION_CODE={6}" & IIf(String.IsNullOrEmpty(strUrlFields(6)), strRegionCode, "") & "&SALESREP_CODE={5}" & IIf(String.IsNullOrEmpty(strUrlFields(5)), strSalesrepCode, "")
                    'Else
                    '    strUrlFormatString = "CallProdActivityDetail.aspx?MONTH=" & intmonth & "&CAT_CODE={1}&SUB_CAT_CODE={2}&CUST_CODE={3}&CUST_GRP_CODE={4}&TEAM_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strTeamCode, "") & "&REGION_CODE={6}" & IIf(String.IsNullOrEmpty(strUrlFields(6)), strRegionCode, "") & "&SALESREP_CODE={5}" & IIf(String.IsNullOrEmpty(strUrlFields(5)), strSalesrepCode, "")
                    'End If


                    Dim strUrlFormatString As String
                    Dim strUrlFields() As String = Nothing

                    strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields, ColumnName)

                    If ColumnName = "YTD" AndAlso Not aryAllColumnName.Contains("CAT_CODE") AndAlso Not aryAllColumnName.Contains("SUB_CAT_CODE") AndAlso Not _
                    aryAllColumnName.Contains("CUST_CODE") AndAlso Not aryAllColumnName.Contains("CUST_GRP_CODE") Then
                        strUrlFields = Nothing
                        strUrlFormatString = ""
                    End If

                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_CallProdList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallProdList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallProdList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        aryDataItem = _aryDataItem
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'End Try
    End Sub

    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String, ByVal ColumnName As String) As String
        Dim strUrlFormatString As String = String.Empty
        'Try
        Select Case intIndex
            Case 0 'Generate sales info by date links
                Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
                Dim arrFields As New ArrayList 'store field name used in grouping
                Dim intmonth As Integer = CF_CallProdList.GetMonthValue(ColumnName)
                Dim intyear As Integer = YearCycleController.GetYearByMonthAndYear(intmonth, ddlYear.SelectedValue)

                Dim licGroupFieldList As New ListItemCollection
                licGroupFieldList.Add(New ListItem("CAT_CODE", -1))
                licGroupFieldList.Add(New ListItem("SUB_CAT_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_GRP_CODE", -1))
                licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

                For Each liFieldToFind As ListItem In licGroupFieldList
                    For Each DC As DataColumn In dtToBind.Columns
                        If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
                            liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        End If
                    Next
                Next

                Dim blnBackToOwnPage As Boolean
                blnBackToOwnPage = True

                ReDim strUrlFields(arrFields.Count - 1)
                For intIndx As Integer = 0 To arrFields.Count - 1
                    strUrlFields(intIndx) = arrFields.Item(intIndx)

                    If arrFields.Item(intIndx) = "CAT_CODE" OrElse _
                        arrFields.Item(intIndx) = "SUB_CAT_CODE" OrElse _
                        arrFields.Item(intIndx) = "CUST_CODE" OrElse _
                        arrFields.Item(intIndx) = "CUST_GRP_CODE" OrElse _
                        arrFields.Item(intIndx) = "TREE_SALESREP_CODE" Then
                        blnBackToOwnPage = False
                    End If
                Next

                If blnBackToOwnPage Then
                    strUrlFormatString = "CallProdList.aspx?" & strbFilterGroup.ToString
                Else
                    strUrlFormatString = "CallProdActivityDetail.aspx?" & strbFilterGroup.ToString & "&MONTH=" & intmonth & "&YEAR=" & intyear
                End If


                Session("CallProdList_GroupingField") = arrFields
            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        'End Try
        Return strUrlFormatString
    End Function

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '     Try
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        li.Value = SUM(li.Value, DataBinder.Eval(e.Row.DataItem, li.Text))
                    End If
                Next
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        e.Row.Cells(iIndex).Text = String.Format("{0:0}", li.Value)
                    End If
                Next
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        ' Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        '  Try
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        '  Try
        Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strStartYear As String
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strYear = Session.Item("Year")
        strStartYear = ddlYear.SelectedValue

        Dim strGroupCode, strGroupName, strSalesRepList As String
        strGroupCode = Session("NODE_GROUP_NAME")
        strGroupName = strGroupCode.Replace("_CODE", " (Tree)").Replace("_", " ")
        strSalesRepList = Session("SALESREP_LIST")

        Dim strPointerText As String
        strPointerText = strGroupName 'IIf(String.IsNullOrEmpty(strTeamCode), "Team", IIf(String.IsNullOrEmpty(strRegionCode), "Region", "Sales Rep."))
        'If wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False Then wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
        'Add in salesrep_code when first visit, or when grouping is nothing
        If (IsPostBack = False AndAlso wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False) OrElse _
            (wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow) = 0) Then
            wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
        End If

        Dim strGroupField As String
        If String.IsNullOrEmpty(GroupingValue) Then
            strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue
        Else
            strGroupField = GroupingValue
            GroupingValue = String.Empty
        End If

        'select group field to show
        'Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        'If Not String.IsNullOrEmpty(strTeamCode) Then
        '    If Not String.IsNullOrEmpty(strRegionCode) Then
        '        'group by salesrep
        '        strGroupField = strGroupField.Replace(strPointer, "SALESREP_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Sales Rep.")
        '    Else
        '        'Group by Region
        '        strGroupField = strGroupField.Replace(strPointer, "REGION_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Region")
        '    End If
        'Else
        '    'group by Team
        '    strGroupField = strGroupField.Replace(strPointer, "TEAM_CODE")
        '    wuc_ctrlpanel.pnlField_EditPointerText("Team")
        'End If

        Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        strGroupField = strGroupField.Replace(strPointer, "TREE_" & strGroupCode)
        wuc_ctrlpanel.pnlField_EditPointerText(strGroupName)

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .Year = strYear
            .StartYear = strStartYear
            .PrincipalID = strPrincipalID
            .PrincipalCode = strPrincipalCode
            .Tree_Path = Session("TREE_PATH")
            .GroupField = strGroupField
            .GroupTextValue = wuc_ctrlpanel.pnlField_GetGroupingTextValue
            .ReportType = ddlReportType.SelectedIndex
        End With

        Dim clsCallDB As New rpt_CALL.clsCallQuery
        'DT = clsCallDB.GetCallProdList(strUserID, strPrincipalID, strPrincipalCode, strYear, strSalesRepList, strGroupField, ddlReportType.SelectedValue)
        'changed to use start year for HK ES different year cycle requirement. start year is a new ddl
        DT = clsCallDB.GetCallProdList(strUserID, strPrincipalID, strPrincipalCode, strStartYear, strSalesRepList, strGroupField, ddlReportType.SelectedValue)

        aryAllColumnName = GenerateAllColumnName(DT)
        PreRenderMode(DT)
        'dgList_Init(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        ' Try
        'aryDataItem.Clear()
        dgList_Init(DT)
        Cal_ItemFigureCollector()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_ItemFigureCollector()
        'used to get the collection list of column that need to sum the figure
        '  Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            Select Case strColumnName
                Case "Q1", "Q2", "Q3", "Q4", _
                     "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", _
                     "SEPT", "OCT", "NOV", "DEC", "YTD", "PYTD"
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
            End Select
        Next

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        CriteriaCollector.SortExpression = strSortExpression
        SortingExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        ' Try
        SortingExpression = Nothing
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        'Try
        SortingExpression = Nothing
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '    Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub

End Class

Public Class CF_CallProdList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "Q1", "Q2", "Q3", "Q4"
                strFieldName = Replace(strColumnName, "Q", "Quarter ")
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", _
                  "SEPT", "OCT", "NOV", "DEC", "YTD", "PYTD"
                strFieldName = strColumnName
            Case "AGENCY_CODE"
                strFieldName = "Supplier Code"
            Case "AGENCY_NAME"
                strFieldName = "Supplier"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '    Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            Case "PYTD"
                FCT = FieldColumntype.InvisibleColumn
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"
                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CPRODDAILYACTY, "'1','8'") Then
                    FCT = FieldColumntype.HyperlinkColumn
                End If
        End Select

        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        '     Try
        Select Case strColumnName.ToUpper
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC", "YTD", _
                 "Q1", "Q2", "Q3", "Q4"
                strStringFormat = "{0:0}"
            Case Else
                strStringFormat = ""
        End Select
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        '  Try
        Dim strColumnName As String = ColumnName.ToUpper
        strColumnName = strColumnName.Replace("_CALLS", "")
        strColumnName = strColumnName.Replace("_WD", "")
        Select Case strColumnName
            Case "JAN"
                Return 1
            Case "FEB"
                Return 2
            Case "MAR"
                Return 3
            Case "APR"
                Return 4
            Case "MAY"
                Return 5
            Case "JUN"
                Return 6
            Case "JULY"
                Return 7
            Case "AUG"
                Return 8
            Case "SEPT"
                Return 9
            Case "OCT"
                Return 10
            Case "NOV"
                Return 11
            Case "DEC"
                Return 12
            Case "YTD"
                Return 0
            Case Else
                Return 1
        End Select
        'Catch ex As Exception
        'End Try
    End Function
End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    BoundColumn_V2 = 6
'    InvisibleColumn = 99
'End Enum