﻿Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdvQuesPreview2Pop
    Inherits System.Web.UI.UserControl

    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub BindQuesPreview()
        Dim dtPreviewTable As DataTable = Nothing
        Dim dtCriteria As DataTable
        Dim dvCriteria As DataView
        Dim strSubQuesCode As String
        Dim strSubQuesType As String
        Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv

        Try
            dtPreviewTable = GetRecList()

            Dim dvCurrentView As New DataView(dtPreviewTable)
            With dListPreview
                .DataSource = dvCurrentView
                .DataBind()
            End With

            If dtPreviewTable.Rows.Count > 0 Then
                Message = Portal.Util.GetValue(Of String)(dtPreviewTable.Rows(0)("QUES_NAME"))
            End If

            For i As Integer = 0 To dListPreview.Items.Count - 1
                strSubQuesType = Portal.Util.GetValue(Of String)(dtPreviewTable.Rows(i)("SUB_QUES_TYPE"))
                strSubQuesCode = Portal.Util.GetValue(Of String)(dListPreview.DataKeys(i))

                If strSubQuesType = "0" Then
                    'S1.Visible = False
                ElseIf strSubQuesType = "1" Then
                    Dim txtSubQues As TextBox = CType(dListPreview.Controls(i).FindControl("txtSubQues"), TextBox)
                    txtSubQues.Visible = True
                ElseIf strSubQuesType = "2" Then
                    Dim ddlSubQues As DropDownList = CType(dListPreview.Controls(i).FindControl("ddlSubQues"), DropDownList)
                    ddlSubQues.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, strSubQuesCode)
                    With ddlSubQues
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strSubQuesType = "3" Then
                    dListPreview.Controls(i).FindControl("txtDateSubQues").Visible = True
                ElseIf strSubQuesType = "4" Then
                    Dim rdbListSubQues As RadioButtonList = CType(dListPreview.Controls(i).FindControl("rdbListSubQues"), RadioButtonList)
                    rdbListSubQues.Visible = True
                ElseIf strSubQuesType = "5" Then
                    Dim pnlSubQues As Object = CType(dListPreview.Controls(i).FindControl("pnlSubQues"), Object)
                    Dim dgTextSubQues As GridView = CType(dListPreview.Controls(i).FindControl("dgTextSubQues"), GridView)

                    pnlSubQues.Style.Item("height") = "100px"
                    dgTextSubQues.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, strSubQuesCode)
                    dvCriteria = dtCriteria.DefaultView
                    dgTextSubQues.DataSource = dvCriteria
                    dgTextSubQues.DataBind()
                    dtCriteria = Nothing
                ElseIf strSubQuesType = "6" Then
                    Dim pnlSubQues As Object = CType(dListPreview.Controls(i).FindControl("pnlSubQues"), Object)
                    Dim dgSelectSubQues As GridView = CType(dListPreview.Controls(i).FindControl("dgSelectSubQues"), GridView)

                    pnlSubQues.Style.Item("height") = "100px"
                    dgSelectSubQues.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, strSubQuesCode)
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectSubQues.DataSource = dvCriteria
                    dgSelectSubQues.DataBind()
                    dtCriteria = Nothing
                End If
            Next

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv

            DT = clsMSSAdv.GetMSSQuesAdvPreview2(TeamCode, TitleCode, QuesCode)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                'lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
