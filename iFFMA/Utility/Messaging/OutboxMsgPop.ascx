<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OutboxMsgPop.ascx.vb" Inherits="iFFMA_Utility_Messaging_OutboxMsgPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 600px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:92%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:right; width:8%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                    
                    <!-- Begin Customized Content -->
                    <asp:Panel ID="pnlViewMode" runat="server">
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Date</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:Label ID="lblDate" runat="server" CssClass="cls_label" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">From</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:Label ID="lblFrom" runat="server" CssClass="cls_label" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Recipient(s)</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:Label ID="lblRecipients" runat="server" CssClass="cls_label" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Subject</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:Label ID="lblSubject" runat="server" CssClass="cls_label" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Content</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:Label ID="lblMsgContent" runat="server" CssClass="cls_label" />
                        </span>
                            
                        <span style="float:left; width:100%; padding-top:10px; padding-bottom:10px">
                            <center>
                                <asp:Button ID="btnForward" runat="server" CssClass="cls_button" Text="Forward" />
                                <%--<asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" />--%>
                            </center>
                        </span>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlEditMode" runat="server" Visible="false">
                    
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Sales Team</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Recipient(s)</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <span style="float:left; width:45%; padding-top: 2px; padding-bottom: 2px;">
                                <span class="cls_label_header">Salesrep</span><br />
                                <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="100px" ForeColor="Black" Width="95%"></asp:ListBox>
                            </span>
                            
                            <span style="float:left; width:8%; padding-top: 2px; padding-bottom:2px;">
                                <span style="float:left; padding-top:15px;"><asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="20px" Text=">" /></span>
                                <span style="float:left; padding-top:3px;"><asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="20px" Text="<" /></span>
                                <span style="float:left; padding-top:3px;"><asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="20px" Text=">>" /></span>
                                <span style="float:left; padding-top:3px;"><asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="20px" Text="<<" /></span>
                            </span>
                            
                            <span style="float:left; width:45%; padding-top: 2px; padding-bottom: 2px;">
                                <span class="cls_label_header">Selected </span> <span class="cls_label_err" >*</span><br />
                                <asp:ListBox ID="lsbSelected" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="100px" ForeColor="Black" Width="95%"></asp:ListBox>
                                <%--<asp:RequiredFieldValidator ID="rfvRecipient" runat="server" ControlToValidate="lsbSelected"
                                    ErrorMessage="Recipient(s) is Required." ValidationGroup="Sent"
                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>--%>
                            </span>
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Subject</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:TextBox ID="txtSubject" runat="server" CssClass="cls_textbox"></asp:TextBox>
                            <span class="cls_label_err" >*</span>
                            <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ControlToValidate="txtSubject"
                                ErrorMessage="Subject is Required." ValidationGroup="Sent"
                                Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                        </span>
                        <br />
                        
                        <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Content</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:70%;">
                            <asp:TextBox ID="txtMsgContent" runat="server" CssClass="cls_textbox" TextMode="MultiLine" Width="80%" Rows="6"></asp:TextBox>
                        </span>
                        
                        <span style="float:left; width:100%; padding-top:10px; padding-bottom:10px">
                            <center>
                                <asp:Button ID="btnSend" runat="server" CssClass="cls_button" Text="Send" ValidationGroup="Sent" />
                                <%--<asp:Button ID="btnCancel" runat="server" CssClass="cls_button" Text="Cancel" />--%>
                            </center>
                        </span>
                    </asp:Panel>
                    <!-- End Customized Content -->
                    
                    <asp:HiddenField ID="hdID" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>

<customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="The message is successfully sent." runat="server" />