<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserGrpCreate.aspx.vb" Inherits="Admin_UserGrp_UserGrpCreate" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Creating User Group</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmUserGrpCreate" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">					                           						                       
						                        <tr>
							                        <td width="20%"><asp:label id="lblName" runat="server" CssClass="cls_label_header">Name</asp:label><asp:label id="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage=" Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
									            </tr>
									            <tr>
						                            <td width="20%"><asp:label id="lblName1" runat="server" CssClass="cls_label_header">Name 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtName1" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblDesc" runat="server" CssClass="cls_label_header">Description</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:TextBox ID="txtDesc" runat="server" CssClass="cls_textbox"></asp:TextBox></td>
						                        </tr>
						                         <tr>
							                        <td width="20%"><asp:label id="lblDesc1" runat="server" CssClass="cls_label_header">Description 1</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:TextBox ID="txtDesc1" runat="server" CssClass="cls_textbox"></asp:TextBox></td>
						                        </tr>						                        
						                        <TR>
                                                    <TD vAlign=top><asp:label id="lblUser" runat="server" CssClass="cls_label_header">Users</asp:label><asp:label id="lblMark6" runat="server" CssClass="cls_mandatory">*</asp:label></TD>
                                                    <td vAlign=top><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></TD>
                                                    <TD style="WIDTH: 166px"><asp:listbox id=lstUsersFrom runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                                    <td>
                                                        <table width="100%" border="0">
                                                            <tr><td align=center><asp:button id=btnSel runat="server" CssClass="cls_button" Text=">>" CausesValidation="False"></asp:button></TD></TR>
                                                            <tr><td align=center><asp:button id=btnRemove runat="server" CssClass="cls_button" Text="<<" CausesValidation="False"></asp:button></TD></TR>
                                                        </TABLE>
                                                    </TD>
                                                    <td><asp:listbox id="lstUsersTo" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                               </TR>                                               
                                                <tr>
							                        <td width="20%" valign="top"><asp:label id="lblAccessRight" runat="server" CssClass="cls_label_header">Access Rights</asp:label><asp:label id="lblMark5" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%" valign="top"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <TD style="WIDTH: 166px"><asp:listbox id="lstAccessRightFrom" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                                    <td>
                                                        <table width="100%" border="0">
                                                            <tr><td align=center><asp:button id=btnSel1 runat="server" CssClass="cls_button" Text=">>" CausesValidation="False"></asp:button></TD></TR>
                                                            <tr><td align=center><asp:button id=btnRemove1 runat="server" CssClass="cls_button" Text="<<" CausesValidation="False"></asp:button></TD></TR>
                                                        </TABLE>
                                                    </TD>
                                                    <td><asp:listbox id="lstAccessRightTo" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
						                        </tr>
						                        <TR>
                                                    <TD vAlign=top><asp:label id="lblSalesRep" runat="server" CssClass="cls_label_header">Sales Representatives</asp:label></TD>
                                                    <td vAlign=top><asp:label id="lblDot7" runat="server" CssClass="cls_label_header">:</asp:label></TD>
                                                   <TD style="WIDTH: 166px"><asp:listbox id="lstSRFrom" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                                    <td>
                                                        <table width="100%" border="0">
                                                            <tr><td align=center><asp:button id=btnSel2 runat="server" CssClass="cls_button" Text=">>" CausesValidation="False"></asp:button></TD></TR>
                                                            <tr><td align=center><asp:button id=btnRemove2 runat="server" CssClass="cls_button" Text="<<" CausesValidation="False"></asp:button></TD></TR>
                                                        </TABLE>
                                                    </TD>
                                                    <td><asp:listbox id="lstSRTo" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></TD>
                                               </TR>
					                        </table>
				                        </td>
			                        </tr>			                        
			                        <tr class="Bckgroundreport">
				                        <td align="right">
					                        <asp:button id="btnReset" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="False"></asp:button>
					                        <asp:button id="btnSave" runat="server" CssClass="cls_button" Text="Save"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
