'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsTRAOrderList
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTRAOrderList"
        End Get
    End Property
    Public Function GetTRAHdrDetails(ByVal strTxnNo As String, ByVal strTxnDate As String, _
    ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_HDR_DTL_LIST"
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTRAHdrDetailsKIV(ByVal strTxnNo As String, ByVal strTxnDate As String, _
ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_HDR_DTL_LIST_KIV"
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTRADetails(ByVal strTxnNo As String, ByVal strTxnDate As String, _
    ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_DTL_LIST"
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTRADetailsKIV(ByVal strTxnNo As String, ByVal strTxnDate As String, _
ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_DTL_LIST_KIV"
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTRAHeader(ByVal strSalesrepCode As String, ByVal strUserCode As String, ByVal strTeamCode As String, ByVal strSearchType As String, _
    ByVal strSearchValue As String, ByVal strSearchValue1 As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_HDR_LIST"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
            .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
            .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
            .addItem("SEARCH_VALUE1", strSearchValue1, clsDB.DataType.DBString)
            .addItem("USERID", strUserCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTRAHeaderCriteria(ByVal strUserCode As String, ByVal strSalesrepCode As String, ByVal strSalesrepName As String, ByVal strTxnNo As String, _
                                         ByVal strTxnDateStart As String, ByVal strTxnDateEnd As String, ByVal strTxnStatus As String, ByVal strRefNo As String, _
                                         ByVal strPayerCode As String, ByVal strPayerName As String, ByVal strSearchUserId As String, ByVal strSalesAreaCode As String, ByVal strSalesTeamCode As String, ByVal strSAPNo As String, ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_HDR_LIST_2"
            .addItem("USER_CODE", strUserCode, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("SALESREP_NAME", strSalesrepName, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("DATE_START", strTxnDateStart, clsDB.DataType.DBString)
            .addItem("DATE_END", strTxnDateEnd, clsDB.DataType.DBString)
            .addItem("STATUS", strTxnStatus, clsDB.DataType.DBString)
            .addItem("REF_NO", strRefNo, clsDB.DataType.DBString)
            .addItem("PAYER_CODE", strPayerCode, clsDB.DataType.DBString)
            .addItem("PAYER_NAME", strPayerName, clsDB.DataType.DBString)
            .addItem("SEARCH_USER_ID", strSearchUserId, clsDB.DataType.DBString)
            .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
            .addItem("SALES_TEAM_CODE", strSalesTeamCode, clsDB.DataType.DBString)
            .addItem("SAP_NO", strSAPNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserID, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
    Public Function GetTRAHeaderCriteriaV2(ByVal strUserCode As String, ByVal strSalesrepCode As String, ByVal strSalesrepName As String, ByVal strTxnNo As String, _
                                         ByVal strTxnDateStart As String, ByVal strTxnDateEnd As String, ByVal strTxnStatus As String, ByVal strRefNo As String, _
                                         ByVal strPayerCode As String, ByVal strPayerName As String, ByVal strSearchUserId As String, ByVal strSalesAreaCode As String, ByVal strSalesTeamCode As String, ByVal strSAPNo As String, ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TBL_TXN_TRADE_RET_HDR_LIST_V2"
            .addItem("USER_CODE", strUserCode, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("SALESREP_NAME", strSalesrepName, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("DATE_START", strTxnDateStart, clsDB.DataType.DBString)
            .addItem("DATE_END", strTxnDateEnd, clsDB.DataType.DBString)
            .addItem("STATUS", strTxnStatus, clsDB.DataType.DBString)
            .addItem("REF_NO", strRefNo, clsDB.DataType.DBString)
            .addItem("PAYER_CODE", strPayerCode, clsDB.DataType.DBString)
            .addItem("PAYER_NAME", strPayerName, clsDB.DataType.DBString)
            .addItem("SEARCH_USER_ID", strSearchUserId, clsDB.DataType.DBString)
            .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
            .addItem("SALES_TEAM_CODE", strSalesTeamCode, clsDB.DataType.DBString)
            .addItem("SAP_NO", strSAPNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserID, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function


    Public Function GetTraTxnStatus(ByVal strUserId As String, ByVal strTxnNo As String, ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_TXN_STATUS"
            .addItem("SALESREP_CODE ", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing

    End Function

#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
