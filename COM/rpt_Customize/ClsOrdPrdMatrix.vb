Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class ClsOrdPrdMatrix
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsOrdPrdMatrix"
        End Get
    End Property

    Public Function GetDDLTeamList(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsDDLTeamListDB As clsDB
        Try
            clsDDLTeamListDB = New clsDB
            With clsDDLTeamListDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DDL_TEAM_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallActyEnquiryList :" & ex.Message))
        Finally
            clsDDLTeamListDB = Nothing
        End Try
    End Function


    Public Function GetOrdPrdMatrixList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, _
    ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strPrdCode As String, ByVal strTxnNo As String, _
    ByVal strTxnDateFrom As String, ByVal strTxnDateTo As String, ByVal strSoNo As String) As DataTable
        Dim clsOrdPrdMatrixListDB As clsDB
        Try
            clsOrdPrdMatrixListDB = New clsDB
            With clsOrdPrdMatrixListDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ORD_PRD_MATRIX"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("TXN_DATE_FROM", strTxnDateFrom, clsDB.DataType.DBString)
                .addItem("TXN_DATE_TO", strTxnDateTo, clsDB.DataType.DBString)
                .addItem("SO_NO", strSoNo, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetOrdPrdMatrixList :" & ex.Message))
        Finally
            clsOrdPrdMatrixListDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class
