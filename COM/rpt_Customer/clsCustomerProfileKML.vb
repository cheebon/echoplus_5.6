﻿Imports System.Xml
Imports System.IO
Imports System.ComponentModel
Imports cor_DB
Imports rpt_Customize
Imports System.Data
Imports rpt_Customer

Public Class clsCustomerProfileKML
    Private _defaultStyleA As String = "styleA"
    Public Property StyleA() As String
        Get
            Return _defaultStyleA
        End Get
        Set(ByVal value As String)
            _defaultStyleA = value
        End Set
    End Property
    Private _defaultStyleB As String = "styleB"
    Public Property StyleB() As String
        Get
            Return _defaultStyleB
        End Get
        Set(ByVal value As String)
            _defaultStyleB = value
        End Set
    End Property
    Private _defaultStyleC As String = "styleC"
    Public Property StyleC() As String
        Get
            Return _defaultStyleC
        End Get
        Set(ByVal value As String)
            _defaultStyleC = value
        End Set
    End Property
    Private _defaultStyleOth As String = "styleOth"
    Public Property StyleOth() As String
        Get
            Return _defaultStyleOth
        End Get
        Set(ByVal value As String)
            _defaultStyleOth = value
        End Set
    End Property

    Dim objKML As clsKMLExporter
#Region "Style"
    Private Sub addStyleA()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleA)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
    Private Sub addStyleB()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleB)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
    Private Sub addStyleC()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleC)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
    Private Sub addStyleOth()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleOth)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
#End Region
#Region "Main Information Tag"
    Public Sub addMainInforTag(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strTeamName As String, ByVal strSalesrepName As String, ByVal strPrdName As String, ByVal strCustTypeName As String)
        Try
            addSearchInfo(strCustName, strAddress, strDistrict, _
                                        strCustGrp, strCustClass, _
                                        strCustType, _
                                        strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
                                        strNoSkuStart, strNoSkuEnd, _
                                        strTeamCode, strSalesrepCode, strPrdCode, strTeamName, strSalesrepName, strPrdName, strCustTypeName)
            addStyleA()
            addStyleB()
            addStyleC()
            addStyleOth()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub AddStyle()
        Try
            addStyleA()
            addStyleB()
            addStyleC()
            addStyleOth()
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
        'Define style

    End Sub
    Private Sub addSearchInfo(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strTeamName As String, ByVal strSalesrepName As String, ByVal strPrdName As String, ByVal strCustTypeName As String)
        objKML.addNewXmlElementWithValue("name", "Customer Profile Searching", objKML.xmlElement)
        objKML.addNewXmlElementWithValue("open", "1", objKML.xmlElement)
        objKML.addNewXmlElementWithValue("description", String.Format("Customer: {0}", strCustName) & vbCrLf & _
                                  String.Format("Address: {0}", strAddress) & vbCrLf & _
                                  String.Format("District: {0}", strDistrict) & vbCrLf & _
                                  String.Format("Cust Grp: {0}", strCustClass) & vbCrLf & _
                                  String.Format("Cust Class: {0}", strCustName) & vbCrLf & _
                                  String.Format("Cust Type: {0}", strCustTypeName) & vbCrLf & _
                                  String.Format("Mtd Sales Range: {0} - {1}", strMtdStart, strMtdEnd) & vbCrLf & _
                                  String.Format("Ytd Sales Range: {0} - {1}", strYtdStart, strYtdEnd) & vbCrLf & _
                                  String.Format("No SKU Range: {0} - {1}", strNoSkuStart, strNoSkuEnd) & vbCrLf & _
                                  String.Format("Team: {0}", strTeamName) & vbCrLf & _
                                  String.Format("Salesrep: {0}", strSalesrepName) & vbCrLf & _
                                  String.Format("SKU: {0}", strPrdName) & vbCrLf, objKML.xmlElement)
    End Sub

#End Region
#Region "TrackPoints"
    Public Sub addTrackPoints(ByRef drRows As DataRowCollection)
        Dim _xmlGroup, _xmlPlaceMark As XmlElement
        _xmlGroup = objKML.addNewXmlElement("Folder", objKML.xmlElement)
        objKML.addNewXmlElementWithValue("name", "TrackPoint", _xmlGroup)
        objKML.addNewXmlElementWithValue("open", "1", _xmlGroup)
        For Each drRow As DataRow In drRows
            If isValidPoint(drRow) Then
                _xmlPlaceMark = objKML.addNewXmlElement("Placemark", _xmlGroup)
                objKML.addPlaceMark(GetValue(Of String)(drRow("CUST_NAME"), String.Empty), TrackPointDescriptions(drRow), TrackPointStyle(drRow), TrackPointCoordinates(drRow), _
                         _xmlPlaceMark, drRow)
            End If
        Next
    End Sub
    Private Function TrackPointDescriptions(ByVal dr As DataRow) As String
        Dim sbDescription As String

        'sbDescription = "<![CDATA["
        sbDescription = ""
        sbDescription += String.Format("<b>Contact</b>: {0}<br />", GetValue(Of String)(dr("CONT_NAME"), String.Empty))
        sbDescription += String.Format("<b>Address</b>: {0}<br />", GetValue(Of String)(dr("ADDRESS"), String.Empty))
        sbDescription += String.Format("<b>District</b>: {0}<br />", GetValue(Of String)(dr("DISTRICT"), String.Empty))
        sbDescription += String.Format("<b>Cust Grp</b>: {0}<br />", GetValue(Of String)(dr("CUST_GRP_NAME"), String.Empty))
        sbDescription += String.Format("<b>Cust Class</b>: {0}<br />", GetValue(Of String)(dr("CUST_CLASS"), String.Empty))
        sbDescription += String.Format("<b>Cust Type</b>: {0}<br />", GetValue(Of String)(dr("CUST_TYPE"), String.Empty))
        sbDescription += String.Format("<b>Mtd Sales</b>: {0}<br />", Convert.ToDecimal(dr("MTD_SALES")).ToString("N2"))
        sbDescription += String.Format("<b>Ytd Sales</b>: {0}<br />", Convert.ToDecimal(dr("YTD_SALES")).ToString("N2"))


        sbDescription += String.Format("<b>No of SKU</b>: {0}<br />", Convert.ToDecimal(dr("NO_SKU")).ToString("N2"))
        sbDescription += String.Format("<b>Credit Limit</b>: {0}<br />", Convert.ToDecimal(dr("CREDITLIMIT")).ToString("N2"))
        sbDescription += String.Format("<b>Outstanding Balance</b>: {0}<br />", Convert.ToDecimal(dr("OUTBAL")).ToString("N2"))

        'sbDescription += "]]>"

        Return sbDescription
    End Function
    Private Function TrackPointCoordinates(ByVal dr As DataRow) As String
        Dim sbDescription As String

        sbDescription = ""
        sbDescription += String.Format("{0}, {1}", dr("LONGITUDE").ToString, dr("LATITUDE").ToString)
        sbDescription += ""

        Return sbDescription
    End Function
    Private Function TrackPointStyle(ByVal dr As DataRow) As String
        Select Case GetValue(Of String)(dr("CUST_CLASS"), String.Empty).ToUpper
            Case "A"
                Return StyleA
            Case "B"
                Return StyleB
            Case "C"
                Return StyleC
            Case Else
                Return StyleOth
        End Select
    End Function
    Private Function isValidPoint(ByVal dr As DataRow) As Boolean
        If IsNumeric(dr("LATITUDE").ToString) And IsNumeric(dr("LONGITUDE").ToString) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

    Public Function generateKMLFile(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strUserID As String, ByVal intNetValue As Integer, _
                                       ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strTeamName As String, ByVal strSalesrepName As String, ByVal strPrdName As String, ByVal strCustTypeName As String) As String

        Dim dtRpt As DataTable '= getCallByCustExport(strSalesrepCode, strCallDate)
        Dim objCust As New clsCustomerProfile
        Dim strFilePath As String = String.Format("{0}{1}{2}_{3}_{4}.kml", System.AppDomain.CurrentDomain.BaseDirectory.ToString, "Documents\iFFMR\KML\", "CustProfEnq", DateTime.Now.ToString("yyyy-MM-dd"), Guid.NewGuid.ToString.Substring(1, 5))
        Try
            objKML = New clsKMLExporter

            dtRpt = objCust.GetCustomerProfileExport(strCustName, strAddress, strDistrict, _
                                        strCustGrp, strCustClass, _
                                        strCustType, strUserID, intNetValue, _
                                        strPrincipalID, strPrincipalCode, strDate, _
                                        strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
                                        strNoSkuStart, strNoSkuEnd, _
                                        strTeamCode, strSalesrepCode, strPrdCode)
            addMainInforTag(strCustName, strAddress, strDistrict, _
                                        strCustGrp, strCustClass, _
                                        strCustType, _
                                        strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
                                        strNoSkuStart, strNoSkuEnd, _
                                        strTeamCode, strSalesrepCode, strPrdCode, strTeamName, strSalesrepName, strPrdName, strCustTypeName)
            addTrackPoints(dtRpt.Rows)
            SaveKML(strFilePath)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
        Return strFilePath
    End Function

    'Private Function getCallByCustExport(ByVal strSalesrepCode As String, ByVal strCallDate As String) As DataTable
    '    Dim clsObj As clsDB
    '    Dim dsObj As DataSet = Nothing
    '    Dim dtRpt As DataTable = Nothing
    '    Try
    '        clsObj = New clsDB
    '        With clsObj
    '            .ConnectionString = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    '            .CmdText = "SPP_RPT_CALL_BY_CUST_EXPORT"
    '            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
    '            .addItem("CALL_DATE", strCallDate, clsDB.DataType.DBString)
    '            dsObj = .spRetrieveDS()
    '            If dsObj.Tables.Count > 0 Then
    '                dtRpt = dsObj.Tables(0)
    '                If dsObj.Tables.Count > 1 AndAlso dsObj.Tables(1).Rows.Count > 0 Then getSalerepInfo(dsObj.Tables(1).Rows(0))
    '            End If

    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        clsObj = Nothing
    '    End Try
    '    Return dtRpt
    'End Function
    'Private Sub getSalerepInfo(ByRef drInfo As DataRow)
    '    If drInfo IsNot Nothing Then
    '        SalesrepCode = GetValue(Of String)(drInfo("SALESREP_CODE"), String.Empty)
    '        SalesrepName = GetValue(Of String)(drInfo("SALESREP_NAME"), String.Empty)
    '        CallDate = GetValue(Of String)(drInfo("CALL_DATE"), String.Empty)
    '    End If
    'End Sub

#Region "Core Functions"
    'Sub New()
    '    InitialiseClass()
    'End Sub
    'Sub New(ByVal strSalesrepCode As String, ByVal strSalesrepName As String, ByVal strCallDate As String)
    '    InitialiseClass()
    '    SalesrepCode = strSalesrepCode
    '    SalesrepName = strSalesrepName
    '    CallDate = strCallDate
    'End Sub
    'Private Sub InitialiseClass()
    '    Dim xmlRoot, xmlSubRoot As XmlElement
    '    Try
    '        xmlDoc = New XmlDocument
    '        xmlDoc.InsertBefore(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", ""), xmlDoc.DocumentElement)

    '        xmlRoot = xmlDoc.CreateElement("kml")
    '        xmlRoot.SetAttribute("xmlns", "http://earth.google.com/kml/2.2")

    '        xmlSubRoot = xmlDoc.CreateElement("Document")
    '        With xmlSubRoot
    '            .SetAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2")
    '            .SetAttribute("xmlns:kml", "http://www.opengis.net/kml/2.2")
    '            .SetAttribute("xmlns:atom", "http://www.w3.org/2005/Atom")
    '        End With
    '        xmlRoot.AppendChild(xmlSubRoot)
    '        xmlDoc.AppendChild(xmlRoot)
    '        XmlElement = xmlSubRoot
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    End Try

    'End Sub
    'Public Function addNewXmlElement(ByVal strElementName As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
    '    addNewXmlElement = xmlDoc.CreateElement(strElementName)
    '    If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(addNewXmlElement)
    '    Return addNewXmlElement
    'End Function
    'Public Function addNewXmlElementWithValue(ByVal strElementName As String, ByVal strElementValue As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
    '    Dim _xmlElement As XmlElement = xmlDoc.CreateElement(strElementName)
    '    If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(_xmlElement)
    '    If Not String.IsNullOrEmpty(strElementValue) Then _xmlElement.InnerText = strElementValue
    '    Return _xmlElement
    'End Function
    'Public Function addNewXmlElementWithXmlText(ByVal strElementName As String, ByVal strXmlText As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
    '    Dim _xmlElement As XmlElement = xmlDoc.CreateElement(strElementName)
    '    If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(_xmlElement)
    '    If Not String.IsNullOrEmpty(strXmlText) Then _xmlElement.InnerXml = strXmlText
    '    Return _xmlElement
    'End Function
    'Private Sub SaveKML(ByVal strDest As String)
    '    If IO.Directory.Exists(IO.Directory.GetParent(strDest).FullName) = False Then IO.Directory.CreateDirectory(IO.Directory.GetParent(strDest).FullName)
    '    Try
    '        xmlDoc.Save(strDest)
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    End Try
    'End Sub
    'Private Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
    '    Dim rtValue As T
    '    If IsDBNull(dbOri) = False Then
    '        rtValue = dbOri
    '    Else
    '        rtValue = dfValue
    '    End If
    '    Return rtValue
    'End Function
    Public Sub SaveKML(ByVal strDest As String)
        If IO.Directory.Exists(IO.Directory.GetParent(strDest).FullName) = False Then IO.Directory.CreateDirectory(IO.Directory.GetParent(strDest).FullName)
        Try
            objKML.xmlDoc.Save(strDest)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
    End Sub
    Public Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
#End Region
#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            Exit Sub
        End Sub
    End Class

    'Public Overloads Sub Dispose()
    '    Dispose(True)
    '    GC.SuppressFinalize(Me)
    'End Sub

    'Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
    '    If Not (Me.disposed) Then
    '        If (disposing) Then
    '            Components.Dispose()
    '        End If
    '        CloseHandle(handle)
    '        handle = IntPtr.Zero
    '    End If
    '    Me.disposed = True
    'End Sub

    '<System.Runtime.InteropServices.DllImport("Kernel32")> _
    ' Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    'End Function

    'Protected Overrides Sub Finalize()
    '    Dispose(False)
    'End Sub

    'Public Sub Close()
    '    Dispose()
    'End Sub

#End Region
End Class
