﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="login" CodeFile="login.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="~/include/DKSHMenu.css" /> 
    <script src="include/jquery-1.4.2.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {

            ShowLastPwdChange();
            //positionLastPwdChange();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {

                ShowLastPwdChange();
                //positionLastPwdChange();
            }
        });

        jQuery.fn.center = function() {
            this.css("position", "absolute");
            var TopHdrHeight;
            this.css("top", (($(window).height() - this.outerHeight()) / 2) + "px");
            this.css("left", (($(window).width() - this.outerWidth()) / 2) + "px");
            return this;
        }

        function ShowLastPwdChange() {
            var value = $('#hfLastPwdChangeStatus').val()
            if (value == "") { $('#LastPwdChange').hide(); }
            else {
                if (value == "Show") { $('#LastPwdChange').show(); }
                else { $('#LastPwdChange').hide(); };
            };
        }

        function SetLastPwdChange(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfLastPwdChangeStatus').val('Hide'); }
            else {
                var value = $('#hfLastPwdChangeStatus').val();
                if (value == 'Hide') { $('#hfLastPwdChangeStatus').val('Show'); }
                else { $('#hfLastPwdChangeStatus').val('Hide'); }
            }
        }

        function positionLastPwdChange() {
            $('#LastPwdChange').center();
        }

        function openWindow(url) {
            var w = window.open(url, 'Echoplus', 'WIDTH=500,HEIGHT=350,LEFT=10,TOP=10,menubar=no,scrollbars=yes'); w.focus();
        }
        
    </script>

</head>
<body class="BckgroundFront">
    <form id="frmLogin" name="frmLogin" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="Release"
        CombineScripts="True" />
    <asp:UpdatePanel ID="UpdateLastPwdChange" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hfLastPwdChangeStatus" runat="server" Value="Hide" />
            <div id="LastPwdChange" style=" position: absolute; display: none; top: 0px ; left: 0px;
                  z-index: 100;">
                <div class="ChangePwdHeader">
                    <div class="ContentLogin" style="width: 100%; height: 100%;">
                        <div id="LastPwdChangePnl" style="width: 100%; padding: 0px 0px 0px 0px;">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblLastPwdChangeDesc" runat="server" Text="Change password" Font-Bold="True" Font-Size="Small" ForeColor="DimGray"
                                            Style="color: Black"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="ContainerDivBlue">
                            <div id="LastPwdChangeContent" style="width: 100%;">
                                <div style="width: 98%; text-align: center;">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblChangePassConfirmDesc" runat="server" Text="" CssClass="frmfield">
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnOk" runat="server" Text="Yes" CssClass="frmbutton" />
                                                <asp:Button ID="btnCancel" runat="server" Text="No" CssClass="frmbutton" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <asp:HiddenField ID="hfLastPwdChange" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="LoginAreaHeader">
        <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 24px">
                    <img alt="" src="images/login_logo.gif" />
                </td>
                <td style="width: 5px">
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" ForeColor="DimGray"
                        Text="User Login"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div class="LoginAreaStyle" style="height: 3px">
        <asp:UpdatePanel ID="upnlLogin" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="right">
                            <span class="ContentLogin">USER NAME :</span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="txtLogin" runat="server" MaxLength="50" CssClass="frmfield" Width="145px" ValidationGroup="UsrLogin"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <span class="ContentLogin">PASSWORD :</span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="50" TextMode="Password" CssClass="frmfield" ValidationGroup="UsrLogin"
                                Width="145px">***</asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">
                            <table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="3" align="center" style="height: 12px">
                                        <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin" ValidationGroup="UsrLogin"
                                            ErrorMessage="User Login cannot be Blank !" CssClass="cls_label" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ValidationGroup="UsrLogin"
                                            ErrorMessage="Password cannot be Blank !" CssClass="cls_label" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center">
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="width: 174px">
                                        <asp:Button ID="btnLogin" runat="server" Text="SIGN IN" CssClass="frmbutton" Width="73px" ValidationGroup="UsrLogin">
                                        </asp:Button>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnReset" runat="server" Text="RESET" CausesValidation="False" CssClass="frmbutton" ValidationGroup="UsrLogin">
                                        </asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--<img src="images/DKSH_Vertical.gif" class="Diethlem_pos" />--%>
    <table align="left" border="0" cellspacing="0" cellpadding="0" style="width: 1006px;
        top: 0px;">
        <tr>
            <td class="BckgroundLoginFFMR1-1">
            </td>
            <td class="BckgroundLoginFFMR1-2">
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="BckgroundLoginFFMR2-1">
            </td>
            <td class="BckgroundLoginFFMR2-2">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="BckgroundLoginFFMR3-1">
            </td>
            <td class="BckgroundLoginFFMR3-2">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Label ID="Label2" runat="server" Text="Best viewed with IE 5.5 and above. Screen resolution 1024 x 768.  
                  Copyright DKSH CSSC SDN BHD " CssClass="lbl_Resolution"></asp:Label>
            </td>
        </tr>
    </table>
    </form>

    <script><asp:literal id="ltlMsg" runat="server"></asp:literal></script>

    <script type="text/javascript" language="javascript">
        document.frmLogin.txtLogin.focus();
        function checkKeyPress() {
            //return document.frmLogin.txtLogin.click();
            if (window.event.keyCode == 13)
                frmLogin.submit();
        }
		
	
    </script>

</body>
</html>
