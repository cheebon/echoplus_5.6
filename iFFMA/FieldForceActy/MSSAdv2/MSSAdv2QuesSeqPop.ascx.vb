Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdv2QuesSeqPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub BindQuesList()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            DT = clsMSSAdv.GetMSSQuesAdvSeq(TeamCode, TitleCode)

            If DT.Rows.Count > 0 Then
                dvDetailView = DT.DefaultView
                lsbQues.DataSource = dvDetailView
                lsbQues.DataTextField = "QUES_NAME"
                lsbQues.DataValueField = "QUES_CODE"
                lsbQues.DataBind()

                lsbOrder.DataSource = dvDetailView
                lsbOrder.DataTextField = "SEQ"
                lsbOrder.DataValueField = "SEQ"
                lsbOrder.DataBind()
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ItemMoveUp(ByVal liItemToMove As ListItem)
        Try
            liItemToMove = lsbQues.Items.FindByText(liItemToMove.Text)
            If liItemToMove IsNot Nothing Then
                Dim intIndex As Integer = lsbQues.Items.IndexOf(liItemToMove)
                If intIndex > 0 Then
                    intIndex -= 1
                    If intIndex >= 0 AndAlso Not lsbQues.Items(intIndex).Selected Then
                        lsbQues.Items.Remove(liItemToMove)
                        lsbQues.Items.Insert(intIndex, liItemToMove)
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ItemMoveDown(ByVal liItemToMove As ListItem)
        Try
            liItemToMove = lsbQues.Items.FindByText(liItemToMove.Text)
            If liItemToMove IsNot Nothing Then
                Dim intIndex As Integer = lsbQues.Items.IndexOf(liItemToMove)
                If intIndex < lsbQues.Items.Count - 1 AndAlso lsbQues.Items(intIndex + 1).Selected = False Then
                    intIndex += 1
                    lsbQues.Items.Remove(liItemToMove)
                    lsbQues.Items.Insert(intIndex, liItemToMove)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim i As Integer
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            For i = 0 To lsbQues.Items.Count - 1
                clsMSSAdv.UpdateMSSQuesAdvSeq(TeamCode, TitleCode, lsbQues.Items(i).Value, i + 1)
            Next
            lblInfo.Text = "The sequence is successfully saved."
            BindQuesList()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub lnkUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUp.Click
        Try
            If lsbQues.GetSelectedIndices().Length > 0 Then
                Dim licItemCollector As New ListItemCollection
                Dim intIndex As Integer

                For Each intIndex In lsbQues.GetSelectedIndices
                    licItemCollector.Add(lsbQues.Items(intIndex))
                Next

                Dim liItemToMove As ListItem
                For Each liItemToMove In licItemCollector
                    ItemMoveUp(liItemToMove)
                Next
            End If
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDown.Click
        Try
            If lsbQues.GetSelectedIndices().Length > 0 Then
                Dim licItemCollector As New ListItemCollection
                Dim intIndex As Integer

                For Each intIndex In lsbQues.GetSelectedIndices
                    licItemCollector.Add(lsbQues.Items(intIndex))
                Next

                Dim liItemToMove As ListItem
                intIndex = licItemCollector.Count - 1
                While intIndex >= 0
                    liItemToMove = licItemCollector.Item(intIndex)
                    If liItemToMove IsNot Nothing Then ItemMoveDown(liItemToMove)
                    intIndex -= 1
                End While
            End If
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class



