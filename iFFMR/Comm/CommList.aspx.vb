Imports System.Data

Partial Class CommList
    Inherits System.Web.UI.Page
#Region "Local Variable"

    Dim aryDataItem As New ArrayList
    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Event Handler"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            'wuc_ctrlPanel.RefreshDetails()
            'wuc_ctrlPanel.UpdateControlPanel()
            If IsPostBack Then RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CommList.aspx"
        End Get
    End Property
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim dt As DataTable
        '
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.PDACOMM) '"Communication List"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.PDACOMM
                .DataBind()
                .Visible = True
            End With
            lblErr.Text = ""
            If Not IsPostBack Then
                TimerControl1.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        'Dim blnNewRowFlag As Boolean = False
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'blnNewRowFlag = True
            Else
                PreRenderMode(dtCurrentTable)
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

    '#Region "DGLIST control"
    '    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    '        If e.Row.RowType = DataControlRowType.Header Then
    '            'e.Row.Cells(0).RowSpan = 2
    '            Dim oGridView As GridView = dgList 'CType(sender, GridView)
    '            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
    '            Dim TC As TableHeaderCell

    '            'TC = New TableHeaderCell
    '            'TC.Text = "&nbsp;"
    '            'TC.ColumnSpan = 1
    '            'GVR.Cells.Add(TC)

    '            'TC = e.Row.Cells(0)
    '            'TC.RowSpan = 2
    '            e.Row.Cells(0).RowSpan = 2
    '            GVR.Cells.Add(e.Row.Cells(0))

    '            For i As Integer = 1 To 31
    '                TC = New TableHeaderCell
    '                TC.Text = i
    '                TC.HorizontalAlign = HorizontalAlign.Center
    '                'TC.VerticalAlign = VerticalAlign.Top
    '                TC.ColumnSpan = 2
    '                GVR.Cells.Add(TC)
    '            Next
    '            TC = New TableHeaderCell
    '            TC.Text = "Total"
    '            TC.HorizontalAlign = HorizontalAlign.Center
    '            'TC.VerticalAlign = VerticalAlign.Top
    '            TC.ColumnSpan = 2
    '            GVR.Cells.Add(TC)

    '            oGridView.Controls(0).Controls.AddAt(0, GVR)
    '            'ElseIf e.Row.RowType = DataControlRowType.DataRow Then

    '            '    Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
    '            '    If dtCurrentTable IsNot Nothing AndAlso dtCurrentTable.Rows.Count = 0 Then
    '            '        Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Insert)
    '            '        Dim TC As New TableCell
    '            '        TC.Text = "Empty"
    '            '        TC.HorizontalAlign = HorizontalAlign.Center
    '            '        TC.ColumnSpan = dgList.Columns.Count
    '            '        GVR.Cells.Add(TC)
    '            '        dgList.Controls(0).Controls.Add(GVR)

    '            '    End If
    '        End If
    '    End Sub

    '    Private Function GetRecList() As DataTable
    '        Dim DT As DataTable = Nothing
    '        Try
    '            Dim strYear, strMonth, strUserID, strSalesrepCode, strTeamCode As String
    '            strYear = Session.Item("Year")
    '            strMonth = Session.Item("Month")
    '            strUserID = Session.Item("UserID")
    '            strSalesrepCode = session("SALESREP_CODE")
    '            strTeamCode = session("TEAM_CODE")

    '            Dim clsComm As New rpt_PdaCom.clsCommLogQuery
    '            DT = clsComm.GetPDAComList(strYear, strMonth, strUserID, strTeamCode, strSalesrepCode)
    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
    '        Finally
    '        End Try
    '        Return DT
    '    End Function

    '    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
    '        Dim strSortExpression As String = ViewState("strSortExpression")
    '        Try
    '            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
    '                If strSortExpression Like (e.SortExpression & "*") Then
    '                    If strSortExpression.IndexOf(" DESC") > 0 Then
    '                        strSortExpression = e.SortExpression
    '                    Else
    '                        strSortExpression = e.SortExpression & " DESC"
    '                    End If
    '                Else
    '                    strSortExpression = e.SortExpression
    '                End If
    '            Else
    '                strSortExpression = e.SortExpression
    '            End If
    '            ViewState("strSortExpression") = strSortExpression
    '            RefreshDatabinding()
    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
    '        Finally
    '        End Try
    '    End Sub
    '#End Region


#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_COMMLOG.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_COMMLOG.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        Else
                            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                        End If

                        dgColumn.HeaderText = CF_COMMLOG.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        If iIndex >= 0 Then
                            e.Row.Cells(0).RowSpan = 2
                            e.Row.Cells(0).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(0))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strYear, strMonth, strUserID, strPrincipalId, strPrincipalCode As String
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")

            Dim clsCallDB As New rpt_PdaCom.clsCommLogQuery
            DT = clsCallDB.GetCommLogList(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepList)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_CustomerHeader()
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnIsNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            'collect the column name that ready to show on the screen
            'DAY_01_UPD DAY_01_RFH
            'TTL_UPD    TTL_RFH
            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "DAY_*" Then
                    strColumnName = strColumnName.ToUpper.Replace("DAY_0", "").Replace("DAY_", "").Replace("_UPD", "").Replace("_RFH", "")
                ElseIf strColumnName Like "TTL_*" Then
                    strColumnName = "Total"
                Else
                    strColumnName = CF_COMMLOG.GetDisplayColumnName(strColumnName)
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnIsNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class CF_COMMLOG
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        ColumnName = ColumnName.ToUpper()

        If ColumnName Like "*_UPD" Then
            strFieldName = "U"
        ElseIf ColumnName Like "*_RFH" Then
            strFieldName = "R"
        Else
            strFieldName = Report.GetDisplayColumnName(ColumnName)
        End If

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function
End Class
