﻿//CollapseSearchPanel
//Control the Searching panel's visibility
function pnlSearch_ChangeCollapsibleState(){
var ctrl =   $get("wuc_Menu_pnlSearch");if (ctrl) {
var behaviors = Sys.UI.Behavior.getBehaviors($get("wuc_Menu_pnlSearch"));for (var j = 0; j < behaviors.length; j++){
var behavior = behaviors[j];if (Object.getTypeName(behavior) == "AjaxControlToolkit.CollapsiblePanelBehavior"){var pnlTreeMenu = $get("wuc_Menu_pnlTreeMenu");
var isClose = behavior.get_ClientState();
if (isClose=='true'){behavior._doOpen();}else{behavior._doClose();}}}}resetTreePnlPosition();}   

function BreakHere(){}
function reset_dgListScrollPosition(dgList){var div_dgList = document.getElementById(dgList);var currentPosition =0;if (div_dgList){currentPosition = div_dgList.scrollTop;div_dgList.scrollTop = 0;}      }
   
function pnl_TreeMenu_ChangeCollapsibleState()
{reset_dgListScrollPosition('div_dgList');var ctrl =   $get("wuc_ctrlpanel_imgTreeMenu");if (ctrl) {var behaviors = Sys.UI.Behavior.getBehaviors($get("wuc_Menu_pnlTreeMain"));
for (var j = 0; j < behaviors.length; j++){var behavior = behaviors[j];if (Object.getTypeName(behavior) == "AjaxControlToolkit.CollapsiblePanelBehavior"){var isClose = behavior.get_ClientState();if (isClose=='true'){behavior._doOpen();}else{behavior._doClose();}}}}resetTreePnlPosition();}   
    
window.onresize = resetTreePnlPosition;
function resetTreePnlPosition()
{var pnlTreeMenu = $get("wuc_Menu_pnlTreeMenu");if (pnlTreeMenu)pnlTreeMenu.style.height=Math.max((getViewportSize()[1] - pnlTreeMenu.offsetTop -80),0)+'px';}
function getViewportSize() { var size = [0, 0]; 
if (typeof window.innerWidth != 'undefined') { size = [ window.innerWidth, window.innerHeight ]; } 
else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) 
{ size = [ document.documentElement.clientWidth, document.documentElement.clientHeight ]; } 
else { size = [ document.getElementsByTagName('body')[0].clientWidth, document.getElementsByTagName('body')[0].clientHeight ]; } 
return size; }
