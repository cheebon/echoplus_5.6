﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanVsActual.aspx.vb" Inherits="iFFMR_Customize_PlanVsActual" %>
<%@ Register TagPrefix="uctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="uclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="uclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="ucUpdateProgress" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="ucctrlpanel1" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="ucpnlRecordNotFound" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Preplan Vs Actual By Customer </title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
function resizeLayout2(){var dgList = $get('div_dgList');if (dgList){dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight('ContentBarIframe') - 70),0)+'px';}}
window.onresize=function(){resizeLayout2();}
function getSelectedCriteria(){var frm =self.parent.document.frames[0];if(frm){var hdf=document.getElementById('hdfSalesrepName');var spn_ori = frm.document.getElementById('hdnSalesrepName');if(hdf && spn_ori){hdf.value=spn_ori.innerHTML;}hdf=document.getElementById('hdfSalesTeamName');spn_ori=frm.document.getElementById('hdnSalesTeamName');if(hdf && spn_ori){hdf.value=spn_ori.innerHTML;}}}
setTimeout("getSelectedCriteria()",1000);
    </script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmPlanVsActual" runat="server">
      <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <ucctrlpanel1:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true" /> 
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                    <uclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  style="width: 98%;" class="Bckgroundreport" valign="top">
                                    <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlSearch" runat="server" >
                                                <table >
                                                    <tr>
                                                        <td valign="top" align="center" style="width: 45%;">
                                                            <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                                            <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                        </td>
                                                        <td valign="middle" align="center" style="width: 10%;">
                                                            <table>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35"
                                                                            Text="<<" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="center" style="width: 35%">
                                                            <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                                            <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" align="center">
                                                            <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                                            <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                        </td>
                                                        <td valign="middle" align="center">
                                                            <table>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                            Text="<" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                            Text=">>" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center">
                                                                        <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                            Text="<<" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="center">
                                                            <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                                            <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding-left: 8px; "> 
                                                            <asp:Label ID="lblClass" runat="server" Text="Class :" CssClass="cls_label" Width="80px"></asp:Label>
                                                            <asp:DropDownList  ID="ddllClass" runat="server" CssClass="cls_dropdownlist"  Width="80px">
                                                                <asp:ListItem Text="All" Value="ALL" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="A" Value="A"  ></asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B"  ></asp:ListItem>
                                                                <asp:ListItem Text="C" Value="C"  ></asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D"  ></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="padding-left: 8px; ">
                                                             <asp:Label ID="lblYear" runat="server" Text="Year :" CssClass="cls_label" Width="80px"></asp:Label>
                                                             <asp:DropDownList ID="ddlyear" runat="server" CssClass="cls_dropdownlist"  Width="80px">
                                                             </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="left" style="padding-left: 8px; padding-bottom: 6px">
                                                            <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                                            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="cls_button" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeSearch" runat="server" CollapseControlID="lnkhideshow"
                                             ExpandControlID="lnkhideshow" Collapsed ="false" TargetControlID="pnlSearch" >
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <input id="lnkhideshow" type="button"  value="Click to Expand or Collapse" class="cls_button" style="width:100%;" />
                                 </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td class="BckgroundBenealthTitle">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                                <ucUpdateProgress:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" style="width: 95%;">
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="445" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <ucpnlRecordNotFound:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td style="height: 5px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
