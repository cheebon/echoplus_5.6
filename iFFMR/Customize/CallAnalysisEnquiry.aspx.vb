'************************************************************************
'	Author	    :	
'	Date	    :	06/08/2008
'	Purpose	    :	
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data
Partial Class iFFMR_Customize_CallAnalysisEnquiry
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue


#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CallAnalysisEnquiry"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property


    Dim licItemFigureCollector As ListItemCollection
#End Region

#End Region
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Public ReadOnly Property PageName() As String
        Get
            Return "CallAnalysisEnquiry"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        wuc_pnlCallAnalysisEnquiry.SubmoduleID = SubModuleType.CALLANALYENQ
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.CALLANALYENQ)
            .DataBind()
            .Visible = True
        End With       '
        Try
            'intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
            If Not IsPostBack Then

                'Call Panel
                With wuc_ctrlpanel
                    .SubModuleID = SubModuleType.CALLANALYENQ
                    .DataBind()
                    .Visible = True
                End With
                wuc_pnlCallAnalysisEnquiry.GenerateFiledList(SubModuleType.CALLANALYENQ)

                Dim gotpreviouspage As String
                gotpreviouspage = IIf(Request.QueryString("PAGE_INDICATOR") = "", Request.QueryString("PAGECALL"), Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(gotpreviouspage) Then
                    RefreshDatabinding()
                    wuc_pnlCallAnalysisEnquiry.PanelCollapese = True
                Else
                    CriteriaCollector = Nothing ' New clsSharedValue    
                    BindDefault()
                End If
            End If
            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    wuc_pnlCallAnalysisEnquiry.RestoreSupplierCode = .Classification
                    wuc_pnlCallAnalysisEnquiry.RestoreTeamCode = .TeamCode
                    wuc_pnlCallAnalysisEnquiry.RestoreSalesrepCode = .SalesrepCode
                    wuc_pnlCallAnalysisEnquiry.RestoreGroupingField = .GroupField
                    'wuc_pnlCallAnalysisEnquiry.RestoreCriteria()

                End With
            ElseIf Request.QueryString("PAGECALL") <> "" Then
                With CriteriaCollector
                    wuc_pnlCallAnalysisEnquiry.RestoreSupplierCode = .Classification
                    wuc_pnlCallAnalysisEnquiry.RestoreTeamCode = .TeamCode
                    wuc_pnlCallAnalysisEnquiry.RestoreSalesrepCode = .SalesrepCode
                    wuc_pnlCallAnalysisEnquiry.RestoreGroupingField = .GroupField
                    'wuc_pnlCallAnalysisEnquiry.RestoreCriteria()

                End With
            Else
                CriteriaCollector = Nothing ' New clsSharedValue
            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub


#Region "EVENT HANDLER"
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_pnlCallAnalysisEnquiry.ResetBtn_Click
        BindDefault()
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        'Finally

        'End Try
    End Sub

    Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_pnlCallAnalysisEnquiry.RefreshBtn_Click
        ViewState("dtCurrentView") = Nothing
        Try
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRefresh_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DGLIST"
    Private Sub RefreshDatabinding(Optional ByRef dtCurrenttable As DataTable = Nothing)
        'Dim dtCurrenttable As Data.DataTable = Nothing 'CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrenttable Is Nothing Then
                dtCurrenttable = GetRecList()
                'ViewState("strSortExpression") = Nothing
                'ViewState("dtCurrentView") = dtCurrenttable
                dgList.PageIndex = 0
            End If


            PreRenderMode(dtCurrenttable)

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dgList.DataBind()

            ''Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            'End With

            'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_CustomerHeader()
            Cal_ItemFigureCollector(DT)


        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallAnalysisEnquiry.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CallAnalysisEnquiry.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CallAnalysisEnquiry.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallAnalysisEnquiry.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_CallAnalysisEnquiry.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_CallAnalysisEnquiry.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing

                        strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields, ColumnName)
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CallAnalysisEnquiry.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_CallActyEnquiryList.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallAnalysisEnquiry.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallAnalysisEnquiry.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String, ByRef strColumnName As String) As String
        Dim strUrlFormatString As String = String.Empty
        Select Case intIndex
            Case 0 'Generate CallAnalysisenquiry links
                Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
                Dim arrFields As New ArrayList 'store field name used in grouping
                Dim licGroupFieldList As New ListItemCollection
                licGroupFieldList.Add(New ListItem("AGENCY_CODE", -1))
                licGroupFieldList.Add(New ListItem("SALESREP_CODE", -1))
                licGroupFieldList.Add(New ListItem("NSM_CODE", -1))
                licGroupFieldList.Add(New ListItem("DSM_CODE", -1))
                licGroupFieldList.Add(New ListItem("CUST_CODE", -1))

                For Each liFieldToFind As ListItem In licGroupFieldList
                    For Each DC As DataColumn In dtToBind.Columns
                        If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
                            liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
                            strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
                            Exit For
                        End If
                    Next
                Next

                ReDim strUrlFields(arrFields.Count - 1)
                For intIndx As Integer = 0 To arrFields.Count - 1
                    strUrlFields(intIndx) = arrFields.Item(intIndx)
                Next

                'Dim intTeamInd As Integer = licGroupFieldList.FindByText("TEAM_CODE").Value
                'Dim intRegionInd As Integer = licGroupFieldList.FindByText("REGION_CODE").Value

                If strColumnName = "CALL_TTL" Then
                    Dim intSalesrepInd As Integer = licGroupFieldList.FindByText("SALESREP_CODE").Value
                    strUrlFormatString = "DailyCallAnalysis.aspx?&BACK=CallAnalysisEnquiry.aspx&" + _
                    strbFilterGroup.ToString + _
                     IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & CriteriaCollector.SalesrepCode, "") + _
                    "&MONTH=" + CriteriaCollector.Month
                ElseIf strColumnName = "CONT_TTL" Then
                    strUrlFormatString = "CallAnalysisByContClass.aspx?" + _
                                   strbFilterGroup.ToString
                End If
              
                'IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
                'IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _


                'Session("SalesInfoByDate_GroupingField") = arrFields
            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select
        Return strUrlFormatString
    End Function

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = CStr(ViewState("strSortExpression"))

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()

    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "*CONT_*" Then
                    strColumnName = "Total Contact"
                ElseIf strColumnName Like "*COVERAGE_*" Then
                    strColumnName = "Coverage"
                ElseIf strColumnName Like "*CALL_TGT_*" Then
                    strColumnName = "Target Call"
                ElseIf strColumnName Like "*CALL_*" Then
                    strColumnName = "Actual Call"
                ElseIf strColumnName Like "*HIT_*" Then
                    strColumnName = "Hit Call"
                ElseIf strColumnName Like "*EFF_*" Then
                    strColumnName = "Effective Call"
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except ACH_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If (strColumnName Like "*_WD" OrElse _
                 strColumnName Like "*_CD*" OrElse _
                 strColumnName Like "*_A" OrElse strColumnName Like "*_B" OrElse _
                 strColumnName Like "*_C" OrElse strColumnName Like "*_OTH" OrElse _
                 strColumnName Like "*_TTL") AndAlso Not strColumnName Like "ACH_*" Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer
                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next

                    'For All "ACH_"
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_A")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_A"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_A"), licItemFigureCollector.FindByText("CONT_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_B")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_B"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_B"), licItemFigureCollector.FindByText("CONT_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_C")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_C"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_C"), licItemFigureCollector.FindByText("CONT_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_OTH")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_OTH"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_OTH"), licItemFigureCollector.FindByText("CONT_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_TTL")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_TTL"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_TTL"), licItemFigureCollector.FindByText("CONT_TTL")) * 100))

                    If aryDataItem.Contains("ACH_HIT_A") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_HIT_A")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_HIT_A"), (DIVISION(licItemFigureCollector.FindByText("HIT_A"), licItemFigureCollector.FindByText("CONT_A")) * 100))
                    End If
                    If aryDataItem.Contains("ACH_HIT_B") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_HIT_B")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_HIT_B"), (DIVISION(licItemFigureCollector.FindByText("HIT_B"), licItemFigureCollector.FindByText("CONT_B")) * 100))
                    End If
                    If aryDataItem.Contains("ACH_HIT_C") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_HIT_C")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_HIT_C"), (DIVISION(licItemFigureCollector.FindByText("HIT_C"), licItemFigureCollector.FindByText("CONT_C")) * 100))
                    End If

                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_A")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_CALL_A"), (DIVISION(licItemFigureCollector.FindByText("CALL_A"), licItemFigureCollector.FindByText("CALL_TGT_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_B")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_CALL_B"), (DIVISION(licItemFigureCollector.FindByText("CALL_B"), licItemFigureCollector.FindByText("CALL_TGT_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_C")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_CALL_C"), (DIVISION(licItemFigureCollector.FindByText("CALL_C"), licItemFigureCollector.FindByText("CALL_TGT_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_OTH")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_CALL_OTH"), (DIVISION(licItemFigureCollector.FindByText("CALL_OTH"), licItemFigureCollector.FindByText("CALL_TGT_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_TTL")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_CALL_TTL"), (DIVISION(licItemFigureCollector.FindByText("CALL_TTL"), licItemFigureCollector.FindByText("CALL_TGT_TTL")) * 100))


                    If aryDataItem.Contains("ACH_EFF_A") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_EFF_A")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_EFF_A"), (DIVISION(licItemFigureCollector.FindByText("EFF_A"), licItemFigureCollector.FindByText("CALL_TGT_A")) * 100))
                    End If
                    If aryDataItem.Contains("ACH_EFF_B") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_EFF_B")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_EFF_B"), (DIVISION(licItemFigureCollector.FindByText("EFF_B"), licItemFigureCollector.FindByText("CALL_TGT_B")) * 100))
                    End If
                    If aryDataItem.Contains("ACH_EFF_C") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_EFF_C")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_EFF_C"), (DIVISION(licItemFigureCollector.FindByText("EFF_C"), licItemFigureCollector.FindByText("CALL_TGT_C")) * 100))
                    End If
                    If aryDataItem.Contains("ACH_EFF_OTH") Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_EFF_OTH")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_EFF_OTH"), (DIVISION(licItemFigureCollector.FindByText("EFF_OTH"), licItemFigureCollector.FindByText("CALL_TGT_OTH")) * 100))
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList(Session("YEAR"), Session("MONTH"), "Agency_code", "Team_code", "Salesrep_code", "SALESREP_CODE,TEAM_CODE")
            dt.Rows.Add(dt.NewRow)
            RefreshDatabinding(dt)
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .Year = Session("YEAR")
                .Month = Session("MONTH")
                .PrincipalID = Session("PRINCIPAL_ID")
                .PrincipalCode = Session("PRINCIPAL_CODE")
                If Page.IsPostBack Then
                    .Classification = wuc_pnlCallAnalysisEnquiry.SupplierCode
                    .TeamCode = wuc_pnlCallAnalysisEnquiry.TeamCode
                    .SalesrepCode = wuc_pnlCallAnalysisEnquiry.SalesrepCode
                    .GroupField = wuc_pnlCallAnalysisEnquiry.GroupingField
                End If
            End With

            With CriteriaCollector
                DT = GetRecList(Session("YEAR"), Session("MONTH"), .Classification, .TeamCode, .SalesrepCode, .GroupField)
            End With

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecList(ByVal strYear As String, ByVal strMonth As String, ByVal strAgencyCode As String, ByVal strTeamCode As String, _
                                            ByVal strSalesrepCode As String, ByVal strGroupingCode As String) As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            Dim clsCallAnalyEnquiryList As New rpt_Customize.clsCallAnalyEnquiryList
            DT = clsCallAnalyEnquiryList.GetCallAnalyEnquiryList(strUserID, strPrincipalId, strPrincipalCode, _
                                            strYear, strMonth, strAgencyCode, strTeamCode, strSalesrepCode, strGroupingCode)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function
#End Region


#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Class CF_CallAnalysisEnquiry
        Public Shared Function FilterPrefixName(ByVal columnName As String) As String
            Dim strName As String = columnName
            Try
                If Not String.IsNullOrEmpty(columnName) AndAlso Not columnName Like "SALESREP_CODE" Then
                    strName = columnName.ToUpper.Replace("CONT_", "").Replace("COVERAGE_", "").Replace("CALL_TGT_", "").Replace("CALL_", "").Replace("HIT_", "")
                    If strName.StartsWith("ACH_") Then
                        strName = "ACH"
                    End If
                End If
            Catch ex As Exception

            End Try
            Return strName
        End Function

        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""
            Dim strNewName As String = FilterPrefixName(ColumnName.ToUpper)

            Select Case strNewName
                Case "AGENCY_CODE"
                    strFieldName = "Supplier Code"
                Case "AGENCY_NAME"
                    strFieldName = "Supplier Name"
                Case "NSM_CODE"
                    strFieldName = "NSM Code"
                Case "NSM_NAME"
                    strFieldName = "NSM"
                Case "DSM_CODE"
                    strFieldName = "DSM Code"
                Case "DSM_NAME"
                    strFieldName = "DSM"
                Case "CUST_COUNT"
                    strFieldName = "Total Customer"
                Case "ACH"
                    strFieldName = "%"
                Case "A"
                    strFieldName = "A"
                Case "B"
                    strFieldName = "B"
                Case "C"
                    strFieldName = "C"
                Case "OTH"
                    strFieldName = "OTH"
                Case "TTL"
                    strFieldName = "Total"
                Case "EFF_A"
                    strFieldName = "A"
                Case "EFF_B"
                    strFieldName = "B"
                Case "EFF_C"
                    strFieldName = "C"
                Case "EFF_OTH"
                    strFieldName = "OTH"
                Case "EFF_TTL"
                    strFieldName = "Total"
                Case Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
            End Select

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
            Try
                Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
                Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
                strColumnName = strColumnName.ToUpper

                Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
                If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
                   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                    FCT = FieldColumntype.InvisibleColumn
                ElseIf (strColumnName = "CONT_TTL") Then
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CALLANALYBYCONTCLASS, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If
                ElseIf (strColumnName = "CALL_TTL") Then
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DAILYCALLANALYSIS, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If
                Else
                    FCT = FieldColumntype.BoundColumn
                End If

                Return FCT
            Catch ex As Exception

            End Try
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strStringFormat As String = ""
            Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)
            Try

                Select Case strNewName
                    Case "ACH", "CD_HALF"
                        strStringFormat = "{0:0.0}"
                    Case "A", "B", "C", "OTH", "TTL", "FTE_WD", "FTE_CD", "CUST_COUNT", "EFF_A", "EFF_B", "EFF_C", "EFF_OTH", "EFF_TTL"
                        strStringFormat = "{0:0}"
                    Case Else
                        strStringFormat = ""
                End Select

                If strColumnName Like "ACH_*" Then strStringFormat = "{0:0.0}"
            Catch ex As Exception
            End Try

            Return strStringFormat
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            Try
                With CS
                    Dim strColumnName As String = ColumnName.ToUpper
                    .FormatString = GetOutputFormatString(ColumnName)

                    If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                        .HorizontalAlign = HorizontalAlign.Center
                    ElseIf Not String.IsNullOrEmpty(.FormatString) OrElse strColumnName Like "WD" OrElse strColumnName Like "CD" Then
                        .HorizontalAlign = HorizontalAlign.Right
                    Else
                        .HorizontalAlign = HorizontalAlign.Left
                    End If

                End With
            Catch ex As Exception

            End Try
            Return CS
        End Function
    End Class
End Class
