Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data


<WebService([Namespace]:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ScriptService()> _
Public Class ws_TRAOrder
    Inherits System.Web.Services.WebService
    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class

    Public Class Customer
        Inherits BaseEntity
#Region "LocalVariable"
        Private _CustName As String = String.Empty
        Private _SoldToName As String = String.Empty
        Private _ShipToName As String = String.Empty
        Private _PrdName As String = String.Empty
        Private _PayerName As String = String.Empty
        Private _Division As String = String.Empty
#End Region

#Region "Public Porperty"
        Public Property CUST_NAME() As String
            Get
                Return _CustName
            End Get
            Set(ByVal value As String)
                _CustName = value
            End Set
        End Property
        Public Property SOLDTO_NAME() As String
            Get
                Return _SoldToName
            End Get
            Set(ByVal value As String)
                _SoldToName = value
            End Set
        End Property
        Public Property SHIPTO_NAME() As String
            Get
                Return _ShipToName
            End Get
            Set(ByVal value As String)
                _ShipToName = value
            End Set
        End Property
        Public Property PRD_NAME() As String
            Get
                Return _PrdName
            End Get
            Set(ByVal value As String)
                _PrdName = value
            End Set
        End Property
        Public Property PAYER_NAME() As String
            Get
                Return _PayerName
            End Get
            Set(ByVal value As String)
                _PayerName = value
            End Set
        End Property
        Public Property DIVISION() As String
            Get
                Return _Division
            End Get
            Set(ByVal value As String)
                _Division = value
            End Set
        End Property
#End Region


    End Class

    Public Class Product
        Inherits BaseEntity
#Region "LocalVariable"
        Private _PrdName As String = String.Empty
        Private _UOMPrice As String = String.Empty
        Private _UOMCode As String = String.Empty
#End Region

#Region "Public Porperty"

        Public Property PRD_NAME() As String
            Get
                Return _PrdName
            End Get
            Set(ByVal value As String)
                _PrdName = value
            End Set
        End Property
        Public Property UOM_PRICE() As String
            Get
                Return _UOMPrice
            End Get
            Set(ByVal value As String)
                _UOMPrice = value
            End Set
        End Property
        Public Property UOM_CODE() As String
            Get
                Return _UOMCode
            End Get
            Set(ByVal value As String)
                _UOMCode = value
            End Set
        End Property
#End Region

    End Class

    Public Class FieldConfig
        Inherits BaseEntity
        Private _setup_value As String = String.Empty
        Public Property setup_value() As String
            Get
                Return _setup_value
            End Get
            Set(ByVal value As String)
                _setup_value = value
            End Set
        End Property
    End Class

    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
            sqlCmd.CommandTimeout = 120

        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
    'Private Shared ReadOnly ConnectionString As String = HttpContext.Current.Session("ffmr_conn")
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffms_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Private Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class

#Region "Customer PayerName"
    <WebMethod(EnableSession:=True)> _
   Public Function ValidateCustPayer(ByVal strCustCode As String, ByVal strPayerCode As String, ByVal strSalesrepCode As String) As PagedResult(Of Customer)

        Dim rptRows As New List(Of Customer)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Customer)
            rptRows.Add(NewRow_ValidateCustPayer)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_SFA_GET_TRA_CUSTPAYER_VALIDATE"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PAYER_CODE", strPayerCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_ValidateCustPayer(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Customer)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_ValidateCustPayer(ByRef reader As IDataReader) As Customer
        Dim rptRow As New Customer
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "PAYER_NAME"
                        SetValue(Of String)(.PAYER_NAME, reader("PAYER_NAME"), String.Empty)
                    Case "SOLDTO_NAME"
                        SetValue(Of String)(.SOLDTO_NAME, reader("SOLDTO_NAME"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_ValidateCustPayer() As Customer
        Dim rptRow As New Customer
        With rptRow
            .PAYER_NAME = String.Empty
            .CUST_NAME = String.Empty
        End With
        Return rptRow
    End Function
#End Region


#Region "Customer Name"

    <WebMethod(EnableSession:=True)> _
 Public Function ValidateCust(ByVal strCustCode As String, ByVal strPayerCode As String, ByVal strSalesrepCode As String) As PagedResult(Of Customer)

        Dim rptRows As New List(Of Customer)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Customer)
            rptRows.Add(NewRow_ValidateCust)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_SFA_GET_TRA_CUST_VALIDATE"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PAYER_CODE", strPayerCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_ValidateCust(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Customer)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_ValidateCust(ByRef reader As IDataReader) As Customer
        Dim rptRow As New Customer
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "SHIPTO_NAME"
                        SetValue(Of String)(.SHIPTO_NAME, reader("SHIPTO_NAME"), String.Empty)
                    Case "SOLDTO_NAME"
                        SetValue(Of String)(.SOLDTO_NAME, reader("SOLDTO_NAME"), String.Empty)
                    Case "DIVISION"
                        SetValue(Of String)(.DIVISION, reader("DIVISION"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_ValidateCust() As Customer
        Dim rptRow As New Customer
        With rptRow
            .CUST_NAME = String.Empty
        End With
        Return rptRow
    End Function
#End Region

#Region "Product Name"

    <WebMethod(EnableSession:=True)> _
 Public Function ValidateProduct(ByVal strPrdCode As String, ByVal strSalesrepCode As String) As PagedResult(Of Product)

        Dim rptRows As New List(Of Product)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Product)
            rptRows.Add(NewRow_ValidatePrd)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_SFA_GET_TRA_PRD_VALIDATE"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_ValidatePrd(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Product)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_ValidatePrd(ByRef reader As IDataReader) As Product
        Dim rptRow As New Product
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "PRD_NAME"
                        SetValue(Of String)(.PRD_NAME, reader("PRD_NAME"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_ValidatePrd() As Product
        Dim rptRow As New Product
        With rptRow
            .PRD_NAME = String.Empty
        End With
        Return rptRow
    End Function
#End Region

#Region "Product UOM"
    <WebMethod(EnableSession:=True)> _
     Public Function GetUOMCode(ByVal strPrdCode As String, ByVal strSalesrepCode As String) As PagedResult(Of Product)

        Dim rptRows As New List(Of Product)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Product)
            rptRows.Add(NewRow_GetUOMCode)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_SFA_GET_TRA_UOMCODE"
                sqlCmd.CommandType = CommandType.StoredProcedure
                'clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_GetUOMCode(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Product)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_GetUOMCode(ByRef reader As IDataReader) As Product
        Dim rptRow As New Product
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "UOM_PRICE"
                        SetValue(Of String)(.UOM_PRICE, reader("UOM_PRICE"), String.Empty)
                    Case "UOM_CODE"
                        SetValue(Of String)(.UOM_CODE, reader("UOM_CODE"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_GetUOMCode() As Product
        Dim rptRow As New Product
        With rptRow
            .UOM_PRICE = String.Empty
            .UOM_CODE = String.Empty
        End With
        Return rptRow
    End Function
#End Region

#Region "Customer Division"
    <WebMethod(EnableSession:=True)> _
    Public Function GetDivisionByCust(ByVal strCustCode As String) As PagedResult(Of Customer)

        Dim rptRows As New List(Of Customer)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of Customer)
            rptRows.Add(NewRow_GetDivisionByCust)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_SFA_GET_CUST_DIVISION"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)


                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_GetDivisionByCust(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of Customer)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_GetDivisionByCust(ByRef reader As IDataReader) As Customer
        Dim rptRow As New Customer
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "DIVISION"
                        SetValue(Of String)(.DIVISION, reader("DIVISION"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_GetDivisionByCust() As Customer
        Dim rptRow As New Customer
        With rptRow
            .DIVISION = String.Empty
        End With
        Return rptRow
    End Function
#End Region

#Region "Config Value"
    <WebMethod(EnableSession:=True)> _
    Public Function GetConfigValue(strFieldControlType As String) As PagedResult(Of FieldConfig)

        Dim rptRows As New List(Of FieldConfig)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of FieldConfig)
            rptRows.Add(NewRow_GetConfigValue)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("echoplus_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_MST_FIELD_CONFIG"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "FIELD_CONTROL_TYPE", strFieldControlType, clsCONN.dtType.dbString)

                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_GetConfigValue(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of FieldConfig)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_GetConfigValue(ByRef reader As IDataReader) As FieldConfig
        Dim rptRow As New FieldConfig
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "setup_value"
                        SetValue(Of String)(.setup_value, reader("setup_value"), String.Empty)

                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_GetConfigValue() As FieldConfig
        Dim rptRow As New FieldConfig
        With rptRow
            .setup_value = String.Empty
        End With
        Return rptRow
    End Function
#End Region

    Public Class clsSAPBillingNo
        Inherits BaseEntity

        Private _id As String = String.Empty
        Public Property id() As String
            Get
                Return _id
            End Get
            Set(ByVal value As String)
                _id = value
            End Set
        End Property

        Private _SAPBillingNo As String = String.Empty
        Public Property SAPBillingNo() As String
            Get
                Return _SAPBillingNo
            End Get
            Set(ByVal value As String)
                _SAPBillingNo = value
            End Set
        End Property

    End Class

    <WebMethod(EnableSession:=True)> _
<ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetAutoCompleteSAPBillingNo(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strInvNo As String) As List(Of clsSAPBillingNo)
        Dim sbclsSAPBillingNo As New List(Of clsSAPBillingNo)()
        Try

            Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_SFA_GET_TRA_INV_WS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "INV_NO", strInvNo, clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            Dim st As New clsSAPBillingNo()
                            st.id = sqlReader.GetValue(0) 'id
                            st.SAPBillingNo = sqlReader.GetValue(1) '"PRD_CODE" 
                            sbclsSAPBillingNo.Add(st)
                        End While
                    End Using
                End Using
            End Using

        Catch ex As Exception

        End Try
        Return sbclsSAPBillingNo
    End Function
End Class
