'************************************************************************
'	Author	    :	Chee Kin Fatt
'	Date	    :	10/04/2007
'	Purpose	    :	Class UserMobile
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.ComponentModel
Imports System.Configuration

Public Class clsPDAUser
    
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = Web.HttpContext.Current.Session("echoplus_conn") '"server=" & ConfigurationSettings.AppSettings("server") & ";database=" & ConfigurationSettings.AppSettings("database") & ";User ID=" & ConfigurationSettings.AppSettings("userid") & ";password=" & ConfigurationSettings.AppSettings("password")

    Public clsProperties As New clsProperties.clsPDAUser

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Create
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Create()

        Dim objDB As cor_DB.clsDB

        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_CREATE"
                .addItem("USER_MOBILE_CODE", clsProperties.strSalesRepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_NAME", clsProperties.strSalesRepName, cor_DB.clsDB.DataType.DBString)
                .addItem("PWD", clsProperties.strPassword, cor_DB.clsDB.DataType.DBString)
                .addItem("MOBILE_CODE", clsProperties.strMobileCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CREATED_DATE", clsProperties.strCreatedDate, cor_DB.clsDB.DataType.DBString)
                .addItem("CREATOR_USER_ID", clsProperties.strCreatorUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("CHANGED_DATE", clsProperties.strCreatedDate, cor_DB.clsDB.DataType.DBString)
                .addItem("CHANGED_USER_ID", clsProperties.strCreatorUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("SYNC_OPERATION", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
                .addItem("ACTIVE_FLAG", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_ADDRESS", clsProperties.strAddress, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_CONTACTNO", clsProperties.strContactNo, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_EMAIL", clsProperties.strEmail, cor_DB.clsDB.DataType.DBString)
                .addItem("COMM_SVR_SETTING_CODE", clsProperties.strConnCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.strUserID, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("Create(): " & ex.Message))
        Finally
            objDB = Nothing
        End Try 
    End Sub
    'Public Sub Create()

    '    Dim objDB As cor_DB.clsDB

    '    Try
    '        'Insert into User
    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .TableName = "TBL_ADM_USER_MOBILE"
    '            .addItem("user_mobile_code", clsProperties.strSalesRepCode, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_name", clsProperties.strSalesRepName, cor_DB.clsDB.DataType.DBString)
    '            .addItem("pwd", clsProperties.strPassword, cor_DB.clsDB.DataType.DBString)
    '            .addItem("mobile_code", clsProperties.strMobileCode, cor_DB.clsDB.DataType.DBString)
    '            .addItem("created_date", clsProperties.strCreatedDate, cor_DB.clsDB.DataType.DBString)
    '            .addItem("creator_user_id", clsProperties.strCreatorUserID, cor_DB.clsDB.DataType.DBInt)
    '            .addItem("changed_date", clsProperties.strCreatedDate, cor_DB.clsDB.DataType.DBString)
    '            .addItem("changed_user_id", clsProperties.strCreatorUserID, cor_DB.clsDB.DataType.DBInt)
    '            .addItem("sync_operation", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
    '            .addItem("active_flag", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_address", clsProperties.strAddress, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_contactno", clsProperties.strContactNo, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_email", clsProperties.strEmail, cor_DB.clsDB.DataType.DBString)
    '            .addItem("comm_svr_setting_code", clsProperties.strConnCode, cor_DB.clsDB.DataType.DBString)
    '            .Insert()
    '        End With

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.Create : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _


    Public Sub Update()

        Dim objDB As cor_DB.clsDB

        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_UPDATE"
                .addItem("USER_MOBILE_ID", clsProperties.strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_CODE", clsProperties.strSalesRepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_NAME", clsProperties.strSalesRepName, cor_DB.clsDB.DataType.DBString)
                If clsProperties.bChangePwd Then
                    .addItem("PWD", clsProperties.strPassword, cor_DB.clsDB.DataType.DBString)
                Else
                    .addItem("PWD", "", cor_DB.clsDB.DataType.DBString)
                End If 
                .addItem("MOBILE_CODE", clsProperties.strMobileCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CHANGED_DATE", clsProperties.strChangedDate, cor_DB.clsDB.DataType.DBString)
                .addItem("CHANGED_USER_ID", clsProperties.strChangedUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("SYNC_OPERATION", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
                .addItem("ACTIVE_FLAG", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_ADDRESS", clsProperties.strAddress, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_CONTACTNO", clsProperties.strContactNo, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_MOBILE_EMAIL", clsProperties.strEmail, cor_DB.clsDB.DataType.DBString)
                .addItem("COMM_SVR_SETTING_CODE", clsProperties.strConnCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.strChangedUserID, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("Update(): " & ex.Message))
        Finally
            objDB = Nothing
        End Try
    End Sub
    'Public Sub Update()

    '    Dim objDB As cor_DB.clsDB

    '    Try
    '        'Insert into User
    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .TableName = "TBL_ADM_USER_MOBILE"
    '            .addItem("user_mobile_code", clsProperties.strSalesRepCode, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_name", clsProperties.strSalesRepName, cor_DB.clsDB.DataType.DBString)
    '            If clsProperties.bChangePwd Then
    '                .addItem("pwd", clsProperties.strPassword, cor_DB.clsDB.DataType.DBString)
    '            End If
    '            .addItem("mobile_code", clsProperties.strMobileCode, cor_DB.clsDB.DataType.DBString)
    '            .addItem("changed_date", clsProperties.strChangedDate, cor_DB.clsDB.DataType.DBString)
    '            .addItem("changed_user_id", clsProperties.strChangedUserID, cor_DB.clsDB.DataType.DBInt)
    '            .addItem("sync_operation", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
    '            .addItem("active_flag", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_address", clsProperties.strAddress, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_contactno", clsProperties.strContactNo, cor_DB.clsDB.DataType.DBString)
    '            .addItem("user_mobile_email", clsProperties.strEmail, cor_DB.clsDB.DataType.DBString)
    '            .addItem("comm_svr_setting_code", clsProperties.strConnCode, cor_DB.clsDB.DataType.DBString)
    '            .addFilter("user_mobile_id", clsProperties.strUserID, "=", cor_DB.clsDB.DataType.DBInt)
    '            .Update()

    '        End With

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.Update : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Suspend
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _


    Public Sub Suspend()

        Dim objDB As cor_DB.clsDB

        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_SUSPEND" 'User unable delete the mobile user info
                '.addItem("USER_MOBILE_ID", clsProperties.strSalesRepCode, cor_DB.clsDB.DataType.DBString) 
                .addItem("USER_MOBILE_ID", clsProperties.strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("SYNC_OPERATION", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
                .addItem("ACTIVE_FLAG", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString) 
                '.addItem("USER_ID", clsProperties.strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.strSalesRepCode, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("Suspend(): " & ex.Message))
        Finally
            objDB = Nothing
        End Try
    End Sub
    'Public Sub Suspend()

    '    Dim objDB As cor_DB.clsDB

    '    Try
    '        'Suspend User
    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .TableName = "TBL_ADM_USER_MOBILE"
    '            .addItem("sync_operation", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
    '            .addItem("active_flag", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
    '            .addFilter("user_mobile_id", clsProperties.strUserID, "=", cor_DB.clsDB.DataType.DBInt)
    '            .Update()
    '        End With

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.Suspend : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
