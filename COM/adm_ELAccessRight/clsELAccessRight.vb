﻿Option Explicit On
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data

''' <summary>
'''************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-11-15
'''	Purpose	    :	Class EAsy Loader Accessright 
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''*************************************************************************
'''  
''' </summary>
''' <remarks></remarks>
Public Class clsELAccessRight
    Implements IDisposable
#Region "Internal Properties"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn")) 'cor_DB.clsConStr.FFMAConnectionString
    Public clsPpt As New clsProperties.clsARPpt
#End Region

    Public Function CheckARDuplicate() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_CHECKDUPLICATE"
                .addItem("ACCESSRIGHT_ID", clsPpt.accessright_id, cor_DB.clsDB.DataType.DBString)
                .addItem("ACCESSRIGHT_NAME", clsPpt.accessright_name, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateAR(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_CREATE"
                .addItem("ACCESSRIGHT_NAME", clsPpt.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("ACCESSRIGHT_NAME_1", clsPpt.accessright_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("ACCESSRIGHT_DESC", clsPpt.accessright_desc, cor_DB.clsDB.DataType.DBString)
                .addItem("ACCESSRIGHT_DESC_1", clsPpt.accessright_desc_1, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)

                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub UpdateAR(ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Dim objAccessRightDtl As clsELAccessRightDtl
        Dim drRow As DataRow = Nothing

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_UPDATE"
                .addItem("accessright_id", clsPpt.accessright_id, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name", clsPpt.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name_1", clsPpt.accessright_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc", clsPpt.accessright_desc, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc_1", clsPpt.accessright_desc_1, cor_DB.clsDB.DataType.DBString)
                '.addItem("principal_id", clsPpt.principal_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("country_id", clsPpt.country_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("user_id", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objAccessRightDtl = Nothing
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteAR()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_DELETE"
                .addItem("accessright_id", clsPpt.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "User Easy Loader"

    Public Function GetUserEasyLoaderAccessRight(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_GETELUSERACCESSRIGHTLISTBYUSERID"
                .addItem("user_id", strUserID, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetEasyLoaderAccessrightList(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_EL_USER_LISTAR"
                .addItem("user_id", strUserID, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


    Public Sub DeleteEasyLoaderUserAccessright(ByVal strUserID As String, ByVal strChangedUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELUSERACCESSRIGHT_DELETE"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("CHANGED_USER_ID", strChangedUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub CreateEasyLoaderUserAccessright(ByVal strUserID As String, ByVal strAccessRightID As String, ByVal strChangedUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELUSERACCESSRIGHT_CREATE"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("ACCESSRIGHT_ID", strAccessRightID, cor_DB.clsDB.DataType.DBString)
                .addItem("CHANGED_USER_ID", strChangedUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#End Region
#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.SeverityID = 4
                '.clsProperties.Exception = msg
                .Log()
            End With
            objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
