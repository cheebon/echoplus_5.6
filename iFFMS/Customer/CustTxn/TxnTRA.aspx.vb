Imports System.Data
Imports txn_WebActy

Partial Class iFFMS_Customer_CustTxn_TxnTRA
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustTxnTRA"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoadTxnDescName()

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
            txtexpdate.Attributes.Add("onblur", "formatDate('" & txtexpdate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            rfvCheckDataType.ErrorMessage = " Invalid Date!(" & "yyyy-MM-dd" & ")"

            TimerControl2.Enabled = True
        End If


        lblErr.Text = ""
        'CVtxtexpdate.DataBind()

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        SaveHeader()
    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick

        If TimerControl2.Enabled Then

            If Request.QueryString("salesrepcode") IsNot Nothing Then
                LoadTxnNo()
                LoadddlCollBy()
                LoadddlMReason()
                LoadddlStoreLocal()
                LoadHdrRecords()
            End If
        End If
        TimerControl2.Enabled = False

    End Sub


#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        dtCurrentTable = GetRecList()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'btncheckall.Visible = False
                ' btnuncheckall.Visible = False
                btndeletesum.Visible = False
                Master_Row_Count = 0
            Else
                'btncheckall.Visible = True
                'btnuncheckall.Visible = True
                btndeletesum.Visible = True
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgList
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpPnlDtlSum.Update()
        UpdatePage.Update()

    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTXNNo As String, strUserid As String, strsessionid As String
            strTXNNo = lbltxnno.Text
            strUserid = Web.HttpContext.Current.Session("UserID")
            strsessionid = Request.QueryString("sessionid")

            Dim clsTRA As New txn_WebActy.clsTRA
            DT = clsTRA.GetTMPTRADetails(strTXNNo, strUserid, strsessionid)

        End If

        Return DT
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strTxnCode As String = sender.datakeys(e.NewEditIndex).item("TXN_NO")
            Dim strLineNo As String = sender.datakeys(e.NewEditIndex).item("LINE_NO")
            Dim strUserId As String = Web.HttpContext.Current.Session("UserID").ToString
            Dim strsessionid As String = Request.QueryString("sessionid")

            Dim dtTraOrderEdit As DataTable
            Dim clsTRA As New txn_WebActy.clsTRA
            dtTraOrderEdit = clsTRA.GetTMPTRADetailsEdit(strTxnCode, strLineNo, strUserId, strsessionid)

            Loadddlreason()
            LoadddlUOM(dtTraOrderEdit.Rows(0)("PRD_CODE"))


            lbllineno.Text = dtTraOrderEdit.Rows(0)("LINE_NO")
            txtPrdCode.Text = dtTraOrderEdit.Rows(0)("PRD_CODE")
            txtPrdName.Text = dtTraOrderEdit.Rows(0)("PRD_NAME")
            txtselecteduom.Text = dtTraOrderEdit.Rows(0)("UOM_CODE")
            Dim lstItem As ListItem = ddluom.Items.FindByText(Trim(dtTraOrderEdit.Rows(0)("UOM_CODE")))
            If lstItem IsNot Nothing Then
                ddluom.SelectedIndex = -1
                lstItem.Selected = True
            End If
            'ddluom.SelectedItem.Text = dtTraOrderEdit.Rows(0)("UOM_CODE")
            lbluomprice.Text = dtTraOrderEdit.Rows(0)("UOM_PRICE")
            txtbno.Text = dtTraOrderEdit.Rows(0)("BATCH_NO")
            txtqty.Text = dtTraOrderEdit.Rows(0)("RET_QTY")
            txtprice.Text = dtTraOrderEdit.Rows(0)("LIST_PRICE")
            txtexpdate.Text = dtTraOrderEdit.Rows(0)("EXP_DATE")
            lblamount.Text = dtTraOrderEdit.Rows(0)("RET_AMT")
            ddlreason.SelectedValue = dtTraOrderEdit.Rows(0)("REASON_CODE")
            txtdetailremarks.Text = dtTraOrderEdit.Rows(0)("REMARKS")

            UpdatePage.Update()
            UpdPnlDtl.Update()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


#End Region

#Region "EVENT HANDLER"

    Protected Sub btnsavedtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavedtl.Click
        If Page.IsValid Then
            'SaveHeader()
            SaveDetail()
            CreateDtlNewRecord()
            UpdatePage.Update()

            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successsfully added the transaction(s)!')", True)
        End If

    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        SubmitTransaction()
    End Sub

    Protected Sub btnProceeed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceeed.Click
        SubmitTransaction()
    End Sub

    Protected Sub btndeletesum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeletesum.Click
        DelTRASumDtl()
        RefreshDatabinding()
        ResetPrdGrpSearch()
        UpdatePage.Update()
    End Sub

    Protected Sub btnresetdtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnresetdtl.Click
        CreateHdrNewRecord()
        CreateDtlNewRecord()
    End Sub


#End Region

#Region "Product Function"
    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        With (wuc_Prd_PrgGrpSearch)
            .ResetPage()
            .BindDefault()
            .Show()
        End With

    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Prd_PrgGrpSearch.SelectButton_Click

        Dim txtProductCode As TextBox = txtPrdCode
        txtProductCode.Text = wuc_Prd_PrgGrpSearch.PrdCode
        Dim txtProductName As TextBox = txtPrdName
        txtProductName.Text = wuc_Prd_PrgGrpSearch.PrdName

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LoadddlUOM", "setTimeout('bindDDLUOM();', 50);", True)
        'LoadddlUOM(wuc_Prd_PrgGrpSearch.PrdCode)
        'LoadddlUOMPrice()
        UpdatePage.Update()


    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Prd_PrgGrpSearch.CloseButton_Click
        ResetPrdGrpSearch()
    End Sub

    Private Sub ResetPrdGrpSearch()
        wuc_Prd_PrgGrpSearch.ResetPage()
        wuc_Prd_PrgGrpSearch.BindDefault()
        wuc_Prd_PrgGrpSearch.Hide()
    End Sub
#End Region

#Region "Header Function"
    Private Sub LoadTxnDescName()
        Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strvisitid As String, strMapPath As String

        strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
        strCustCode = Trim(Request.QueryString("custcode"))
        strContCode = Trim(Request.QueryString("contcode"))
        strvisitid = Trim(Request.QueryString("visitid"))
        strMapPath = "Ondemand > New Transaction > Goods Return"

        Dim dt As DataTable
        Dim clstxncommon As New txn_common.clstxncommon
        dt = clstxncommon.GetTxnDescName(strSalesrepCode, strCustCode, strContCode)

        If dt.Rows.Count > 0 Then
            lblFieldForceName.Text = Trim(dt.Rows(0)("SALESREP_NAME"))
            lblCustName.Text = Trim(dt.Rows(0)("CUST_NAME"))
            lblContName.Text = Trim(dt.Rows(0)("CONT_NAME"))
            lblVisitId.Text = strvisitid

            lblMapPath.Text = strMapPath
        End If


    End Sub

    Private Sub CreateHdrNewRecord()
        txtcarton.Text = 0
        txtrefNo.Text = ""
        txtremarks.Text = ""
    End Sub

    Private Function GetHdrRecord() As DataTable
        Try
            Dim DT As DataTable = Nothing

            If Not String.IsNullOrEmpty(Session("UserID")) Then
                Dim strTXNNo As String, strUserid As String, strsessionid As String, strCustCode As String, strContCode As String
                strTXNNo = lbltxnno.Text
                strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
                strsessionid = Trim(Request.QueryString("sessionid"))
                strCustCode = Trim(Request.QueryString("CustCode"))
                strContCode = Trim(Request.QueryString("ContCode"))

                Dim clsTRA As New txn_WebActy.clsTRA
                DT = clsTRA.GetTMPTRAHeader(strTXNNo, strUserid, strsessionid, strCustCode, strContCode)

            End If

            Return DT
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Sub SaveHeader()
        Try
            Dim strSessionId As String, strTxnNo As String, strvisitid As String, strvoucherno As String, _
                   strSalesrepCode As String, strcustcode As String, strcontcode As String, strcollcode As String, strReasonCode As String, strsloccode As String, _
                   strctnno As String, strrefno As String, strRemarks As String, strUserId As String, strTxnDate As String

            strSessionId = Trim(Request.QueryString("sessionid"))
            strTxnNo = Trim(lbltxnno.Text)
            strvoucherno = Trim(lblvouchno.Text)
            strvisitid = Trim(Request.QueryString("visitid"))
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strcustcode = Trim(Request.QueryString("custcode"))
            strcontcode = Trim(Request.QueryString("contcode"))
            strcollcode = Trim(ddlCollBy.SelectedValue.ToString)
            strReasonCode = Trim(ddlmreason.SelectedValue.ToString)
            strsloccode = Trim(ddlstorage.SelectedValue.ToString)
            strctnno = Trim(txtcarton.Text)
            strrefno = Trim(txtrefNo.Text)
            strRemarks = Trim(txtremarks.Text)
            strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            strTxnDate = Request.QueryString("datein") + " " + Request.QueryString("timein")

            Dim clsTRA As New txn_WebActy.clsTRA
            clsTRA.InstTMPTRAHeader(strSessionId, strTxnNo, strvisitid, strvoucherno, strSalesrepCode, strcustcode, strcontcode, _
            strcollcode, strReasonCode, strsloccode, strctnno, strrefno, strRemarks, strUserId, strTxnDate)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadTxnNo()
        Try
            Dim strTxnNo As String = Trim(Request.QueryString("TxnNo"))
            If IsNothing(strTxnNo) Or strTxnNo = "" Then
                Dim DTTXNno As DataTable
                Dim clsTRA As New txn_WebActy.clsTRA

                DTTXNno = clsTRA.GetTRANo(Request.QueryString("salesrepcode"))
                lbltxnno.Text = DTTXNno.Rows(0)("TXN_NO")
                lblvouchno.Text = DTTXNno.Rows(0)("VOUCH_NO")

            Else
                lbltxnno.Text = strTxnNo
                lblvouchno.Text = Trim(Request.QueryString("VOUCHNO"))
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlCollBy()

        Try
            With ddlCollBy
                .Items.Clear()
                .Items.Insert(0, New ListItem("COLL001 - Salesrep", "COLL001"))
                .Items.Insert(1, New ListItem("COLL002 - Transporter", "COLL002"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlMReason()
        Try
            Dim dtreason As DataTable
            Dim clsTRA As New txn_WebActy.clsTRA

            dtreason = clsTRA.GetSRMReasonList(Request.QueryString("salesrepcode"))
            With ddlmreason
                .Items.Clear()
                .DataSource = dtreason.DefaultView
                .DataTextField = "REASON_DESC"
                .DataValueField = "REASON_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlStoreLocal()
        Try
            Dim dtstorelocal As DataTable
            Dim clsTRA As New txn_WebActy.clsTRA
            dtstorelocal = clsTRA.GetStorLocalList(Request.QueryString("salesrepcode"))
            With ddlstorage
                .Items.Clear()
                .DataSource = dtstorelocal.DefaultView
                .DataTextField = "REASON_DESC"
                .DataValueField = "REASON_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadHdrRecords()
        Try
            Dim HdrDT As DataTable
            HdrDT = GetHdrRecord()

            If HdrDT.Rows.Count > 0 Then
                ddlCollBy.SelectedValue = HdrDT.Rows(0)("COLL_CODE")
                ddlmreason.SelectedValue = HdrDT.Rows(0)("REASON_CODE")
                ddlstorage.SelectedValue = HdrDT.Rows(0)("SLOC_CODE")
                txtcarton.Text = HdrDT.Rows(0)("CTN_NO")
                txtrefNo.Text = HdrDT.Rows(0)("REF_NO")
                txtremarks.Text = HdrDT.Rows(0)("REMARKS")
                CreateDtlNewRecord()
                RefreshDatabinding()
                ResetPrdGrpSearch()
            Else
                CreateHdrNewRecord()
                CreateDtlNewRecord()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetSalesrepCode() As String
        Try
            Dim SalesrepCode As String = Nothing

            If Not String.IsNullOrEmpty(Session("UserID")) Then
                Dim strUserid As String
                strUserid = Web.HttpContext.Current.Session("UserID")

                Dim clsTRA As New txn_WebActy.clsTRA
                SalesrepCode = clsTRA.GetSRCode(strUserid)

            End If

            Return SalesrepCode
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Private Sub SubmitTransaction()
        Try
            rfvprdcode.IsValid = True
            rfvFormatDate.IsValid = True
            rfvCheckDataType.IsValid = True
            'CVtxtexpdate.IsValid = True
            rfvddluom.IsValid = True
            cvtxtqty.IsValid = True
            rfvtxtqty.IsValid = True
            cvtxtqty2.IsValid = True
            cvtxtprice.IsValid = True
            cvlblamount.IsValid = True
            rfvddlreason.IsValid = True


            If Page.IsValid Then
                If dgList.Rows.Count > 0 Then
                    SaveHeader()
                    Dim strSessionId As String, strVisitID As String, strSalesrepCode As String, strTxnNo As String, strVoucherNo As String, _
                        StrCustCode As String, StrContCode As String

                    strSessionId = Trim(Request.QueryString("sessionid"))
                    strVisitID = Trim(Request.QueryString("visitid"))
                    strSalesrepCode = Trim(Session("SALESREP_CODE"))
                    StrCustCode = Trim(Request.QueryString("CustCode"))
                    StrContCode = Trim(Request.QueryString("ContCode"))
                    strTxnNo = Trim(lbltxnno.Text)
                    strVoucherNo = Trim(lblvouchno.Text)

                    Dim strurl = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnTRASub.aspx?sessionid=" + strSessionId + "&visitid=" + strVisitID + "&SALESREP_CODE=" + strSalesrepCode + "&TXNNO=" + strTxnNo + "&VOUCHNO=" + strVoucherNo + "&CustCode=" + StrCustCode + "&ContCode=" + StrContCode + "';"
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "HideElement('SubDetailBar');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Kindly add at least one detail record!')", True)
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Details Function"

    Private Sub CreateDtlNewRecord()
        Try
            Loadddlreason()
            lbllineno.Text = 0
            txtPrdCode.Text = ""
            txtPrdName.Text = ""
            ddluom.Items.Clear()
            txtselecteduom.Text = ""
            lbluomprice.Text = ""
            txtbno.Text = ""
            txtqty.Text = ""
            txtprice.Text = ""
            lblamount.Text = 0
            txtexpdate.Text = ""
            txtdetailremarks.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlUOM(ByVal strPrdCode As String)
        Try
            Dim dtuom As DataTable
            Dim clsTRA As New txn_WebActy.clsTRA

            dtuom = clsTRA.GetUOMCode(strPrdCode)
            With ddluom
                .Items.Clear()
                .DataSource = dtuom.DefaultView
                .DataTextField = "UOM_CODE"
                .DataValueField = "UOM_PRICE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadddlUOMPrice()
        Try

            Dim struomprice As Double

            Dim clsTRA As New txn_WebActy.clsTRA
            struomprice = clsTRA.GetUOMPrice(txtPrdCode.Text.ToString, ddluom.SelectedItem.Text)
            If Not IsNothing(struomprice) Then
                lbluomprice.Text = struomprice
                txtselecteduom.Text = ddluom.SelectedItem.Text
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub Loadddlreason()
        Try
            Dim clsTRA As New txn_WebActy.clsTRA

            Dim dtreason As DataTable = CType(ViewState("dtreason"), DataTable)
            If dtreason Is Nothing Then
                dtreason = clsTRA.GetReasonList
            Else
                ViewState("dtreason") = dtreason
            End If


            With ddlreason
                .Items.Clear()
                .DataSource = dtreason.DefaultView
                .DataTextField = "REASON_DESC"
                .DataValueField = "REASON_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelTRASumDtl()

        Try
            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strTxnNo As String, strUserId As String, strsessionid As String, strLineNo As String


                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dgList.Rows(i).FindControl("chkdelete"), CheckBox)

                        If chkdelete.Checked = True Then
                            strTxnNo = Trim(lbltxnno.Text)
                            strTxnNo = dgList.DataKeys(i).Item("TXN_NO")
                            strLineNo = dgList.DataKeys(i).Item("LINE_NO")
                            strUserId = Web.HttpContext.Current.Session("UserID").ToString
                            strsessionid = Request.QueryString("sessionid")

                            Dim clsTRA As New txn_WebActy.clsTRA
                            clsTRA.DelTMPTRADetails(strTxnNo, strLineNo, strUserId, strsessionid)

                        End If

                    End If

                    i += 1
                Next

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub SaveDetail()
        Try
            If lbltxnno.Text <> "" Then
                Dim strlineno As Integer
                strlineno = lbllineno.Text

                If strlineno = 0 Then

                    Dim strSessionId As String, strTxnNo As String, strPrdCode As String, strUomCode As String, _
                    strBNo As String, strRetQty As String, strListPrice As String, strExpDate As String, strRetAmt As String, _
                    strReasonCode As String, strRemarks As String, strUserId As String

                    strSessionId = Trim(Request.QueryString("sessionid"))
                    strTxnNo = Trim(lbltxnno.Text)
                    strPrdCode = Trim(txtPrdCode.Text)
                    strUomCode = txtselecteduom.Text 'Trim(ddluom.SelectedValue.ToString)
                    strBNo = Trim(txtbno.Text)
                    strRetQty = Trim(txtqty.Text)
                    strListPrice = Trim(txtprice.Text)
                    strExpDate = Trim(txtexpdate.Text)
                    strRetAmt = Trim(lblamount.Text)
                    strReasonCode = Trim(ddlreason.SelectedValue.ToString)
                    strRemarks = Trim(txtdetailremarks.Text)
                    strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)

                    Dim i As Integer = 0
                    Dim exists As Boolean
                    If dgList.Rows.Count > 0 Then
                        For Each DR As GridViewRow In dgList.Rows
                            If dgList.Rows(i).Cells(4).Text = strPrdCode And dgList.Rows(i).Cells(11).Text = strUomCode Then
                                exists = True
                            End If
                            i += 1
                        Next
                    End If

                    'If exists = True Then
                    '    lblMsgPop.Message = "Product code '" + strPrdCode + "' and UOM Code '" + strUomCode + "' already exists!!"
                    '    lblMsgPop.Show()
                    'Else
                    Dim clsTRA As New txn_WebActy.clsTRA
                    clsTRA.InstTMPTRADetails(strSessionId, strTxnNo, strPrdCode, strUomCode, strBNo, strRetQty, strListPrice, strExpDate, _
                    strRetAmt, strReasonCode, strRemarks, strUserId)

                    'End If

                Else
                    Dim strSessionId As String, strTxnNo As String, strPrdCode As String, strUomCode As String, _
                              strBNo As String, strRetQty As String, strListPrice As String, strExpDate As String, strRetAmt As String, _
                              strReasonCode As String, strRemarks As String, strUserId As String

                    strSessionId = Trim(Request.QueryString("sessionid"))
                    strTxnNo = Trim(lbltxnno.Text)
                    strPrdCode = Trim(txtPrdCode.Text)
                    strUomCode = Trim(txtselecteduom.Text) 'Trim(ddluom.SelectedValue.ToString)
                    strBNo = Trim(txtbno.Text)
                    strRetQty = Trim(txtqty.Text)
                    strListPrice = Trim(txtprice.Text)
                    strExpDate = Trim(txtexpdate.Text)
                    strRetAmt = Trim(lblamount.Text)
                    strReasonCode = Trim(ddlreason.SelectedValue.ToString)
                    strRemarks = Trim(txtdetailremarks.Text)
                    strUserId = Trim(Web.HttpContext.Current.Session("UserID").ToString)

                    Dim clsTRA As New txn_WebActy.clsTRA
                    clsTRA.UpdTMPTRADetails(strSessionId, strTxnNo, strPrdCode, strUomCode, strBNo, strRetQty, strListPrice, strExpDate, _
                    strRetAmt, strReasonCode, strRemarks, strUserId, strlineno)
                End If

                RefreshDatabinding()
                ResetPrdGrpSearch()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

  
End Class

