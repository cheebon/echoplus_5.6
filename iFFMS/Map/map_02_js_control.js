var mapviewer,  markers, panzoomWidget, loc;

function onLoad() {
    mapviewer = new MultimapViewer( document.getElementById( 'mapviewer' ) );
    mapviewer.goToPosition( new MMLocation( new MMLatLon( 3.118, 101.6332 ) ,15 ) );
    panzoomWidget = new MMPanZoomWidget();
	mapviewer.addWidget( panzoomWidget );
	mapviewer.addWidget ( new MMToolsWidget () );
}

function addposicon(lat, lon, name) {
    var pos = new MMLatLon( lat, lon );
    var locicon = new MMIcon( 'titik.gif' );
    locicon.iconSize = new MMDimensions( 23, 25 );
    locicon.iconAnchor = new MMPoint( 23, 25 );
    locicon.infoBoxAnchor = new MMPoint( 17, 0 );
    loc = mapviewer.createMarker( pos, {'label': 'Location marker', 'icon' : locicon, 'text': name} );
    
    var html =  name ; 
    loc.setInfoBoxContent(html);
}
MMAttachEvent( window, 'load', onLoad );

