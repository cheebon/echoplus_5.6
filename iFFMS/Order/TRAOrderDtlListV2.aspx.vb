
Option Explicit On

Imports System.Data
Partial Class iFFMS_Order_TRAOrderDtlList
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemTRAOrderList")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemTRAOrderList") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMS_TRAOrderDtlLsit"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

#End Region
    Private intPageSize As Integer
    Public ReadOnly Property PageName() As String
        Get
            Return "TRA Order Detail List"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
        If Session("UserID") = "" Then
            Dim strScript As String = ""
            strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        End If
        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

        lblErr.Text = ""
        'Call Header
        With wuc_lblHeader
            .Title = "TRA Detail List"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            lblviewdatetime.Text = " Printed date: " + Format(Now(), "yyyy/MM/dd hh:mm:ss")
            lblviewusername.Text = " Printed by: " + Session("UserName")
            EditButtonView()
            RefreshDatabinding()
            RefreshDataBinding2()

          
        End If
    End Sub
    Private Sub EditButtonView()

        Dim strTxnStatus As String
        'strstatus = Trim(Request.QueryString("TXN_STATUS"))

        Dim objTRAHdrDtlQuery As txn_order.clsTRAOrderList
        objTRAHdrDtlQuery = New txn_order.clsTRAOrderList
        Dim DT As DataTable = Nothing

        Dim strTxnNo As String, strSalesrepCode As String, strUserId As String
        strTxnNo = Trim(Request.QueryString("TXN_NO"))
        strUserId = Trim(Session("UserCode"))
        strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
        DT = objTRAHdrDtlQuery.GetTraTxnStatus(strUserId, strTxnNo, strSalesrepCode)
        If DT.Rows.Count > 0 Then
            strTxnStatus = DT.Rows(0)(0)
        Else
            strTxnStatus = String.Empty
        End If

        If strTxnStatus.ToUpper = "K" Or strTxnStatus.ToUpper = "V" Then
            'btncancel.Visible = True
            btnedit.Visible = True
            GetGuid() = Trim(Request.QueryString("SESSION_ID"))
        Else
            ' btncancel.Visible = False
            btnedit.Visible = False
        End If

        'show/hide attachment button
        Dim cls_common As New txn_common.clstxncommon
        Dim dtAttach = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

        If dtAttach.Rows.Count > 0 Then
            For i = 0 To dtAttach.Rows.Count - 1
                If dtAttach.Rows(i)(0) = "Attachment" Then
                    btnAttachment.Visible = True
                End If
            Next
        End If

        'show/hide wf remarks
        If strTxnStatus.ToUpper = "V" Then
            dtviewright.Fields(1).Visible = True
        End If
    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            SalesrepCode = clsTRAOrder.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Protected Sub btnAttachment_Click(sender As Object, e As System.EventArgs) Handles btnAttachment.Click
        Dim strScript As String = ""
        strScript = ""
        strScript = strScript + "LoadAttachment();"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "popupGPS", strScript, True)

    End Sub
#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        dtCurrenttable = GetRecList()

        PreRenderMode(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        UpdateDatagrid_Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_TRAOrderDtlList_2.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_TRAOrderDtlList_2.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_TRAOrderDtlList_2.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TRAOrderDtlList_2.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_TRAOrderDtlList_2.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName

                        dgColumn.DataNavigateUrlFields = Nothing 'strUrlFields
                        dgColumn.DataNavigateUrlFormatString = Nothing 'strUrlFormatString
                        dgColumn.Target = "_blank" '"_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_TRAOrderDtlList_2.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TRAOrderDtlList_2.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = True
                        dgColumn.HeaderText = CF_TRAOrderDtlList_2.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim objTRAHdrDtlQuery As txn_order.clsTRAOrderList
        Dim DT As DataTable = Nothing
        Try

            objTRAHdrDtlQuery = New txn_order.clsTRAOrderList
            Dim strTxnNo As String, strTxnDate As String, strSalesrepCode As String, strUserId As String, strTxnStatus As String

            strTxnNo = Trim(Request.QueryString("TXN_NO"))
            strTxnDate = Trim(Request.QueryString("TXN_DATE"))
            strUserId = Trim(Session("UserCode"))
            strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
            strTxnStatus = Trim(Request.QueryString("TXN_STATUS"))

            If strTxnStatus.ToUpper = "K" Or strTxnStatus.ToUpper = "V" Then
                With objTRAHdrDtlQuery
                    DT = .GetTRADetailsKIV(strTxnNo, strTxnDate, strSalesrepCode, strUserId)
                End With
            Else
                With objTRAHdrDtlQuery
                    DT = .GetTRADetails(strTxnNo, strTxnDate, strSalesrepCode, strUserId)
                End With
            End If
         

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then
                dgList_Init(DT)
                Cal_ItemFigureCollector(DT)

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper

            If strColumnName = "RET_AMT" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        'Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0
        'Try
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
        Return dblValue
    End Function

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        

        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                'If aryDataItem.IndexOf("BATCH_NO") >= 0 Then
                '    Dim strCapCode As String = Trim(e.Row.Cells(aryDataItem.IndexOf("BATCH_NO")).Text)
                '    If Len(strCapCode) > 10 And Len(strCapCode) < 20 Then
                '        Dim half As Integer = Len(strCapCode) / 2
                '        e.Row.Cells(aryDataItem.IndexOf("BATCH_NO")).Text = strCapCode.Substring(0, half) + " " + strCapCode.Substring(half)
                '    ElseIf Len(strCapCode) > 20 Then
                '        Dim q1 As Integer = Len(strCapCode) / 4
                '        Dim q2 As Integer = q1 + q1
                '        Dim q3 As Integer = q1 + q1 + q1
                '        e.Row.Cells(aryDataItem.IndexOf("BATCH_NO")).Text = strCapCode.Substring(0, q1) + " " + strCapCode.Substring(q1, q1) + " " + strCapCode.Substring(q2, q1) + " " + strCapCode.Substring(q3)
                '    End If
                'End If
       
            Case DataControlRowType.Footer
                Dim iIndex As Integer
                Dim iIndex2 As Integer
                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_TRAOrderDtlList_2.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                    iIndex2 = aryDataItem.IndexOf("LIST_PRICE")
                    If iIndex2 >= 0 Then
                        e.Row.Cells(iIndex2).Text = "Total Net Value:"
                    End If
                Next
        End Select
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()

            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "DETAILVIEW"

    Private Function GetRecList2() As Data.DataTable
        Dim objTRAHdrDtlQuery As txn_order.clsTRAOrderList
        Dim DT As DataTable = Nothing
        Try

            objTRAHdrDtlQuery = New txn_order.clsTRAOrderList
            Dim strTxnNo As String, strTxnDate As String, strSalesrepCode As String, strUserId As String, strTxnStatus As String

            strTxnNo = Trim(Request.QueryString("TXN_NO"))
            strTxnDate = Trim(Request.QueryString("TXN_DATE"))
            strUserId = Trim(Session("UserCode"))
            strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
            strTxnStatus = Trim(Request.QueryString("TXN_STATUS"))

            If strTxnStatus.ToUpper = "K" Or strTxnStatus.ToUpper = "V" Then
                With objTRAHdrDtlQuery
                    DT = .GetTRAHdrDetailsKIV(strTxnNo, strTxnDate, strSalesrepCode, strUserId)
                End With
            Else
                With objTRAHdrDtlQuery
                    DT = .GetTRAHdrDetails(strTxnNo, strTxnDate, strSalesrepCode, strUserId)
                End With
            End If


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub RefreshDataBinding2()
        Dim dtCurrenttable2 As Data.DataTable = Nothing
        dtCurrenttable2 = GetRecList2()

        Dim dvCurrentView2 As New Data.DataView(dtCurrenttable2)
        ' If dtCurrenttable2 IsNot Nothing Then dv_Init(dtCurrenttable2)
        dtviewleft.DataSource = dvCurrentView2
        dtviewleft.DataBind()
        dtviewright.DataSource = dvCurrentView2
        dtviewright.DataBind()


    End Sub

    'Protected Sub dv_Init(ByRef dtToBind As DataTable)
    '    Dim i As Integer
    '    Dim j As Integer = 0
    '    Dim ColumnNamedv As String = ""

    '    Try
    '        dtviewleft.Fields.Clear()
    '        dtviewright.Fields.Clear()

    '        For i = 0 To dtToBind.Columns.Count - 1
    '            ColumnNamedv = dtToBind.Columns(i).ColumnName

    '            Select Case CF_TRAOrderDtlList_2.GetFieldColumnType(ColumnNamedv, True)
    '                Case FieldColumntype.InvisibleColumn

    '                Case Else
    '                    j = j + 1
    '                    Dim dvcolumn As New BoundField

    '                    dvcolumn = New BoundField

    '                    Dim strFormatStringdv As String = CF_TRAOrderDtlList_2.GetOutputFormatString(ColumnNamedv)
    '                    If String.IsNullOrEmpty(strFormatStringdv) = False Then
    '                        dvcolumn.DataFormatString = strFormatStringdv
    '                    End If

    '                    dvcolumn.HtmlEncode = False
    '                    dvcolumn.DataField = ColumnNamedv
    '                    dvcolumn.HeaderText = CF_TRAOrderDtlList_2.GetDisplayColumnName(ColumnNamedv)
    '                    dvcolumn.ReadOnly = True
    '                    dvcolumn.SortExpression = ColumnNamedv

    '                    If j Mod 2 = 0 Then
    '                        dtviewright.Fields.Add(dvcolumn)
    '                    Else
    '                        dtviewleft.Fields.Add(dvcolumn)
    '                    End If

    '                    dvcolumn = Nothing
    '            End Select
    '        Next

    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub dtviewleft_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtviewleft.DataBound
    '    Dim rowView As DataRowView = CType(dtviewleft.DataItem, DataRowView)

    '    'If rowView.Row(0).ToString() = "Remarks" Then
    '    Dim strCapCode As String = rowView.Row(0).ToString()
    '    If Len(strCapCode) > 10 And Len(strCapCode) < 20 Then
    '        Dim half As Integer = Len(strCapCode) / 2
    '        rowView.Row(0) = strCapCode.Substring(0, half) + " " + strCapCode.Substring(half)
    '    ElseIf Len(strCapCode) > 20 Then
    '        Dim q1 As Integer = Len(strCapCode) / 4
    '        Dim q2 As Integer = q1 + q1
    '        Dim q3 As Integer = q1 + q1 + q1
    '        rowView.Row(0) = strCapCode.Substring(0, q1) + " " + strCapCode.Substring(q1, q1) + " " + strCapCode.Substring(q2, q1) + " " + strCapCode.Substring(q3)
    '    End If
    '    'End If

    'End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Property GetGuid()
        Get
            If String.IsNullOrEmpty(Session("GUID")) Then Session("GUID") = Session("USER_ID") & Guid.NewGuid.ToString.ToUpper.Substring(24, 8)
            Return Session("GUID")
        End Get
        Set(ByVal value)
            Session("GUID") = value
        End Set
    End Property

    Protected Sub btnedit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnedit.Click
        ReloadTxn()
        Dim strSessionId As String, strTxnNo As String, strVoucherNo As String, strVisitId As String, strSalesrepCode As String, strTxnStatus As String
        strSessionId = Trim(Request.QueryString("SESSION_ID"))
        strTxnNo = Trim(Request.QueryString("TXN_NO"))
        strVoucherNo = Trim(Request.QueryString("VOUCHER_NO"))
        strVisitId = Trim(Request.QueryString("VISIT_ID"))
        strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
        strTxnStatus = Trim(Request.QueryString("TXN_STATUS"))

        Dim strUrl As String
        strUrl = "TRAOrderV2.aspx?TXN_NO=" + strTxnNo + "&TXN_STATUS=" + strTxnStatus + "&VOUCHER_NO=" + strVoucherNo + "&VISIT_ID=" + strVisitId + "&SALESREP_CODE=" + strSalesrepCode + "&SESSION_ID=" + strSessionId
        Response.Redirect(strUrl)
    End Sub

    Private Sub ReloadTxn()
        Dim strSessionId As String, strTxnNo As String, strUserid As String, strUserName As String
        strSessionId = Trim(Request.QueryString("SESSION_ID"))
        strTxnNo = Trim(Request.QueryString("TXN_NO"))
        strUserid = Web.HttpContext.Current.Session("UserID")
        strUserName = Web.HttpContext.Current.Session("UserName")

        Dim clsTRAOrder As New txn_order.clsTRAOrder
        clsTRAOrder.ReloadTRAHdrDtl(strSessionId, strTxnNo, strUserid, strUserName)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        ReloadTxn()
        CancelHdrDtl("C")
        btncancel.Visible = False
        btnedit.Visible = False
    End Sub

    Private Sub CancelHdrDtl(ByVal strStatus As String)
        Try

            Dim strSessionId As String, strTxnNo As String, strVoucherNo As String, strVisitID As String, _
                   strSalesrepCode As String, strUserName As String, strUserId As String

            strSessionId = Trim(Request.QueryString("SESSION_ID"))
            strTxnNo = Trim(Request.QueryString("TXN_NO"))
            strVoucherNo = Trim(Request.QueryString("VOUCHER_NO"))
            strVisitID = Trim(Request.QueryString("VISIT_ID"))
            strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
            strUserId = Web.HttpContext.Current.Session("UserID").ToString
            strUserName = Web.HttpContext.Current.Session("UserName").ToString

            Dim clsTRAOrder As New txn_order.clsTRAOrder

            Dim DT As DataTable
            DT = clsTRAOrder.InstTRADetails(strSessionId, strTxnNo, strVoucherNo, strVisitID, strSalesrepCode, strStatus, strUserName, strUserId)

            Dim strerror As Integer = DT.Rows(0)("ERROR")

            If strerror = 0 Then
                lblMsgPop.Message = "You have successfully cancelled the transaction!!"
                lblMsgPop.Show()
            Else
                lblMsgPop.Message = "You request is not successfully!!"
                lblMsgPop.Show()
            End If

        Catch ex As Exception

        End Try

    End Sub

   
End Class

Public Class CF_TRAOrderDtlList_2
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn No."
            Case "TXN_STATUS"
                strFieldName = "Status"
            Case "LINE_NO"
                strFieldName = "Line No."
            Case "UOM_CODE"
                strFieldName = "UOM"
            Case "COLL_CODE"
                strFieldName = "Collector Code"
            Case "BATCH_NO"
                strFieldName = "Batch No."
            Case "REASON_CODE"
                strFieldName = "Reason Code"
            Case "RET_QTY"
                strFieldName = "Quantity"
            Case "LIST_PRICE"
                strFieldName = "Unit Price<br />(RM)"
            Case "RET_AMT"
                strFieldName = "Net Value<br />(RM)"
            Case "REF_NO"
                strFieldName = "Ref No."
            Case "EXP_DATE"
                strFieldName = "Exp. Date"
            Case "TXN_DATE"
                strFieldName = "Txn. Date"
            Case "VOUCHER_NO"
                strFieldName = "Voucher No."
            Case "COLL_NAME"
                strFieldName = "Collector"
            Case "SLOC_CODE"
                strFieldName = "Storage Loca. Code"
            Case "SLOC_NAME"
                strFieldName = "Storage Loca."
            Case "CTN_NO"
                strFieldName = "Carton"
            Case "PAYER_CODE"
                strFieldName = "Payer Code"
            Case "PAYER_NAME"
                strFieldName = "Payer"
            Case "SOLDTO_NAME"
                strFieldName = "Sold to"
            Case "CREATOR_USER_ID"
                strFieldName = "User Id"
            Case "USER_NAME"
                strFieldName = "User Name"
            Case "CREATED_DATE"
                strFieldName = "Created Date"
            Case "SALESREP"
                strFieldName = "Salesman"
            Case "SALES_OFFICE"
                strFieldName = "Sales Office"
            Case "SALES_TEAM"
                strFieldName = "Sales Team"
            Case "STORAGE_LOCATION"
                strFieldName = "Storage Location"
            Case "SHIPTO"
                strFieldName = "Ship To"
            Case "BLANK_1"
                strFieldName = " "
            Case "BLANK_2"
                strFieldName = " "
            Case "REFER_NO"
                strFieldName = "Refer No."
            Case "TTL_NO_CARTON"
                strFieldName = "Total No. Of Carton"
            Case "HEADER_REASON"
                strFieldName = "Header Reason"
            Case "CREATED_BY"
                strFieldName = "Created By"
            Case "COLLECTED_BY"
                strFieldName = "Collected By"
            Case "GRAND_TOTAL"
                strFieldName = "Grand Total"
            Case "PAYER"
                strFieldName = "Payer"
            Case "REASON"
                strFieldName = "Return Reason"
            Case "LIST_PRICE_SG"
                strFieldName = "Unit Price (SGD)"
            Case "RET_AMT_SG"
                strFieldName = "Net Value (SGD)"
            Case "&NBSP;"
                strFieldName = " "
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "" Then

                FCT = FieldColumntype.HyperlinkColumn

            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "*QTY"
                    strFormatString = "{0:#,0}"
                Case "RET_AMT"
                    strFormatString = "{0:#,0.00}"
                Case "RET_QTY"
                    strFormatString = "{0:#,0}"
                Case "LIST_PRICE"
                    strFormatString = "{0:#,0.00}"
                Case Else
                    strFormatString = ""
            End Select

            If strColumnName.ToUpper Like "*DATE" Then
                strFormatString = "{0:dd/MM/yyyy}"
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" Or strColumnName Like "*_DATE" Or strColumnName Like "*STATUS" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf strColumnName = "RET_QTY" Or strColumnName = "LIST_PRICE" Or strColumnName = "RET_AMT" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

                If strColumnName = "REASON_CODE" Then
                    .HorizontalAlign = HorizontalAlign.Left
                End If
            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class