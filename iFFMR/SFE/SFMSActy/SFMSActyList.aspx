<%@ Page Language="vb" AutoEventWireup="false" Inherits="SFMSActyList" CodeFile="SFMSActyList.aspx.vb" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SFMS Activity List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top">
                </td>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr colspan="3">
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td align="left">
                                                                            </td>
                                                                            <td>
                                                                                <asp:Timer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick" />
                                                                                <%--<cc1:StoppableTimer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick" />--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td align="center">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr valign="top" align="left">
                                                                            <td>
                                                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                    Width="97%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                    RowHighlightColor="AntiqueWhite">
                                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                                    <EmptyDataTemplate>
                                                                                        There is no data to display.</EmptyDataTemplate>
                                                                                    <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />
                                                                                </ccGV:clsGridView>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                            <td>
                                                                                <ccGV:clsGridView ID="dgSubList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                    Width="97%" FreezeHeader="True" GridHeight="435" AddEmptyHeaders="0" CellPadding="2"
                                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                    RowHighlightColor="AntiqueWhite">
                                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                                    <EmptyDataTemplate>
                                                                                    </EmptyDataTemplate>
                                                                                </ccGV:clsGridView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
<%--<table border="0" cellpadding="0" cellspacing="0">
<tr align="left" valign="baseline">
<td style="width: 25%">
    <asp:Label ID="lblHeader1" runat="server" Text="Team" CssClass="cls_label_header"></asp:Label></td>
<td colspan="3">
    <asp:Label ID="lblTeam" runat="server" Text="" CssClass="cls_label"></asp:Label>
</td>
</tr>
<tr align="left" valign="baseline">
<td>
    <asp:Label ID="lblHeader2" runat="server" Text="Category Code" CssClass="cls_label_header"></asp:Label>
</td>
<td style="width: 25%">
    <asp:Label ID="lblCatCode" runat="server" Text="" CssClass="cls_label"></asp:Label>
</td>
<td style="width:35%">
    <asp:Label ID="lblHeader3" runat="server" Text="Category Description" CssClass="cls_label_header"></asp:Label></td>
</td>
<td>
    <asp:Label ID="lblCatDesc" runat="server" CssClass="cls_label"></asp:Label>
</td></tr> </table>--%>
<%--<table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="TinyTableText" align="right" colspan="15">
                                <a href="">Jan-June</a> | <a href="">July-Dec</a> | <a href="">Quarterly</a>
                            </td>
                        </tr>
                        <tr bgcolor="indigo">
                            <td class="TableLabel" align="center" width="15%" rowspan="2">
                                <font color="white"><b>Sales Reps</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>JAN</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>FEB</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>MAR</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>APR</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>MAY</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>JUNE</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>YTD</b></font></td>
                        </tr>
                        <tr bgcolor="indigo">
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                        </tr>
                        <tr bgcolor="#ffffcc">
                            <td class="TinyTableText" align="left" width="10%">
                                SHARON CHRISTINE
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">17</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">53</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">18</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">66</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">22</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">84</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">18</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">80</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">20</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">71</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">21</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">158</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                157</td>
                            <td class="TinyTableText" align="center" width="5%">
                                780</td>
                        </tr>
                        <tr bgcolor="lightblue">
                            <td class="TinyTableText" align="right" width="10%">
                                <b>Total</b></td>
                            <td class="TinyTableText" align="center" width="5%">
                                17
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                53
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                18
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                66
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                22
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                84
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                18
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                80
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                20
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                71
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                21
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                158
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                <b>157</b></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <b>780</b></td>
                        </tr>
                    </table>--%>
