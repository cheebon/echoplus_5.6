Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports rpt_Customize

Partial Class iFFMR_Customize_SalesOrderPrdMatrix
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.ORDPRDMATRIX)
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            LoadTeamDDLList()
        End If

    End Sub


    Sub LoadTeamDDLList()

        Dim strUserID As String = Session.Item("UserID")
        Dim strPrincipalID As String = Session("PRINCIPAL_ID")
        Dim ClsOrdPrdMatrix As New rpt_Customize.ClsOrdPrdMatrix
        With ddlteam
            .DataSource = ClsOrdPrdMatrix.GetDDLTeamList(strUserID, strPrincipalID)
            .DataTextField = "TEAM_NAME"
            .DataValueField = "TEAM_CODE"
            .DataBind()
        End With
    End Sub
End Class
