﻿Imports System.Data
Imports adm_Config

Partial Class Admin_Device_Config_ConfigDtl
    Inherits System.Web.UI.Page
    Private dtAR As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            With wuc_lblheader
                .Title = "Configuration Details"
                .DataBind()
                .Visible = True
            End With

            If Not Page.IsPostBack Then
                LoadDDLTeam()
                GetCategoryList()
                LoadDDLSalesrep()

                CheckRedirect()
            End If

            lblMesg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DDL"
    Private Sub GetCategoryList()
        Dim dt As DataTable
        Dim objCat As clsConfig
        Try
            objCat = New clsConfig
            dt = objCat.GetCategoryList(Portal.UserSession.UserID)

            With ddlCategoryType
                .Items.Clear()
                .DataTextField = "TYPE"
                .DataValueField = "TYPE"
                .DataSource = dt
                .DataBind()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            'UpdateSearchGrid_Update()
        End Try
    End Sub
    Private Sub LoadDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL

            With ddlTeam
                .Items.Clear()

                For Each dr As DataRow In dtTeam.Rows
                    ddlTeam.Items.Add(New ListItem(dr("TEAM_NAME").ToString.Trim, dr("TEAM_CODE").ToString.Trim))
                Next
                .SelectedIndex = 0
            End With


            'With ddlTeam
            '    .Items.Clear()
            '    .DataSource = dtTeam.DefaultView
            '    .DataTextField = "TEAM_NAME"
            '    .DataValueField = "TEAM_CODE"
            '    .DataBind()
            '    '.Items.Insert(0, New ListItem("-- SELECT --", ""))
            '    .SelectedIndex = 0
            'End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLSalesrep()
        Dim dtSalesrep As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSalesrep = clsCommon.GetSalesrepDDL(ddlTeam.SelectedValue)

            With ddlSalesrep
                .Items.Clear()

                For Each dr As DataRow In dtSalesrep.Rows
                    .Items.Add(New ListItem(dr("SALESREP_NAME").ToString.Trim, dr("SALESREP_CODE").ToString.Trim))
                Next

                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            'With ddlSalesrep
            '    .Items.Clear()
            '    .DataSource = dtSalesrep.DefaultView
            '    .DataTextField = "SALESREP_NAME"
            '    .DataValueField = "SALESREP_CODE"
            '    .DataBind()
            '    .Items.Insert(0, New ListItem("-- SELECT --", ""))
            '    .SelectedIndex = 0
            '    '.SelectedValue = IIf(Session("SELECTED_SR") = "", "", Session("SELECTED_SR"))
            'End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#Region "Get Data"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            RefreshDataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try

            dtCurrentTable = GetRecList()


            Dim dvCurrentView As New DataView(dtCurrentTable)


            With dgList
                .DataSource = dvCurrentView
                .DataBind()
                .Visible = True
            End With

            If dtCurrentTable.Rows.Count > 0 Then
                btnSave.Visible = True
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "ValidateCheckbox", "ValidateAllChecked('chkbValueAll','chkbValue');ValidateAllChecked('chkbApplyAll','chkbApply');", True)
            Else
                btnSave.Visible = False
            End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateConfigDtl_Update()
        End Try
    End Sub
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header
                    Dim chkbox As CheckBox = CType(e.Row.FindControl("chkbValueAll"), CheckBox)
                    chkbox.Attributes("onclick") = "ToggleAllCheckbox('chkbValue', this);"
                    chkbox.Enabled = CanEdit()

                    Dim chkbox2 As CheckBox = CType(e.Row.FindControl("chkbApplyAll"), CheckBox)
                    chkbox2.Attributes("onclick") = "ToggleAllCheckbox('chkbApply', this);"
                    chkbox2.Enabled = CanEdit()
                Case DataControlRowType.DataRow
                    Dim chkbox As CheckBox = CType(e.Row.FindControl("chkbValue"), CheckBox)
                    chkbox.Attributes("onclick") = "ValidateAllChecked('chkbValueAll','chkbValue');"
                    chkbox.Enabled = CanEdit()

                    Dim chkbox2 As CheckBox = CType(e.Row.FindControl("chkbApply"), CheckBox)
                    chkbox2.Attributes("onclick") = "ValidateAllChecked('chkbApplyAll','chkbApply');"
                    chkbox2.Enabled = CanEdit()

                    Dim txtValue2 As TextBox = CType(e.Row.FindControl("txtValue2"), TextBox)
                    Dim txtValue3 As TextBox = CType(e.Row.FindControl("txtValue3"), TextBox)

                    txtValue2.Enabled = CanEdit()
                    txtValue3.Enabled = CanEdit()
            End Select
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        RefreshDataBind()
    End Sub
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub
    Private Function GetRecList() As DataTable
        Dim dt As DataTable
        Dim objConfig As clsConfig
        Try
            objConfig = New clsConfig
            dt = objConfig.GetConfigurationDtl(ddlTeam.SelectedValue.Trim, ddlSalesrep.SelectedValue.Trim, ddlCategoryType.SelectedValue.Trim, Portal.UserSession.UserID)

            Return dt
            'With dgList
            '    .DataSource = dt
            '    .DataBind()
            'End With

            'OnOffGrid(True)
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

#Region "Function"
    Private Sub UpdateConfigDtl_Update()
        updPnlConfigDtl.Update()
    End Sub
    Private Function CanEdit() As Boolean
        Return GetAccessRight(1, SubModuleType.CFGDEVICE, "'3'")
    End Function
    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function
    Private Sub CheckRedirect()
        Try
            If Not IsNothing(Request.QueryString("type")) AndAlso Not IsNothing(Request.QueryString("code")) AndAlso Not IsNothing(Request.QueryString("cat")) Then
                Dim strType, strCode, strCat As String

                strType = Request.QueryString("type").ToString.Trim
                strCode = Request.QueryString("code").ToString.Trim
                strCat = Request.QueryString("cat").ToString.Trim

                Select Case strType
                    Case clsCommon.DtlTypeTeam
                        SelectTeam(strCode)

                    Case clsCommon.DtlTypeSR
                        Dim obj As New mst_FieldForce.clsFieldForce
                        Dim dt As DataTable = obj.GetSalesrepDetails(strCode)

                        Dim strTeam As String = dt.Rows(0)("TEAM_CODE").ToString().Trim

                        SelectTeam(strTeam)
                        SelectSalesrep(strCode)



                End Select

                SelectCategory(strCat)
                btnBack.Visible = True
                RefreshDataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SelectTeam(ByVal strTeam As String)
        If strTeam <> "" Then
            ddlTeam.SelectedValue = strTeam

            LoadDDLSalesrep() 'update salesrep ddl
        End If
    End Sub

    Private Sub SelectSalesrep(ByVal strSalesrep As String)
        If strSalesrep <> "" Then
            ddlSalesrep.SelectedValue = strSalesrep

            'ddlSalesrep_SelectedIndexChanged(Me, Nothing)
        End If
    End Sub

    Private Sub SelectCategory(ByVal strCategory As String)
        If strCategory <> "" Then
            ddlCategoryType.SelectedValue = strCategory

            'ddlCategoryType_SelectedIndexChanged(Me, Nothing)
        End If
    End Sub
#End Region

#Region "Save"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            DeleteSetting()
            SaveSetting()

            RunJob()
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateConfigDtl_Update()
        End Try
    End Sub

    Private Sub DeleteSetting()
        Dim obj As clsConfig

        Try
            obj = New clsConfig

            obj.DeleteSettingDtl(ddlTeam.SelectedValue.Trim, ddlSalesrep.SelectedValue.Trim, Portal.UserSession.UserID)

        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Sub
    Private Sub RunJob()
        Dim objConfig As New clsConfig
        Try
            objConfig.RunJob(Portal.UserSession.UserID)
        Catch ex As Exception
            lblMesg.Text = "Records saved successfully but invoke job failed."
        End Try
    End Sub
    Private Sub SaveSetting()
        Dim obj As clsConfig
        Dim chkApply As CheckBox
        Dim chkValue As CheckBox
        Dim txtValue2 As TextBox
        Dim txtValue3 As TextBox
        Dim txtValue4 As TextBox
        Dim strSettingCode As String

        Try
            obj = New clsConfig

            For Each row As GridViewRow In dgList.Rows
                chkApply = CType(row.FindControl("chkbApply"), CheckBox)

                If chkApply.Checked Then
                    chkValue = CType(row.FindControl("chkbValue"), CheckBox)
                    txtValue2 = CType(row.FindControl("txtValue2"), TextBox)
                    txtValue3 = CType(row.FindControl("txtValue3"), TextBox)
                    txtValue4 = CType(row.FindControl("txtValue4"), TextBox)
                    strSettingCode = dgList.DataKeys(row.RowIndex).Value

                    If txtValue2.Text.Trim = "" Then txtValue2.Text = "0"
                    If txtValue3.Text.Trim = "" Then txtValue3.Text = "0"
                    If txtValue4.Text.Trim = "" Then txtValue4.Text = "0"

                    obj.SaveSettingDtl(ddlTeam.SelectedValue.Trim, ddlSalesrep.SelectedValue.Trim, strSettingCode, Convert.ToInt32(chkValue.Checked), txtValue2.Text.Trim, txtValue3.Text.Trim, txtValue4.Text.Trim, Portal.UserSession.UserID)
                End If

            Next

            lblMesg.Text = "Records saved!"

        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Sub

#End Region

    Protected Sub ddlCategoryType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategoryType.SelectedIndexChanged
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged
        Try
            LoadDDLSalesrep()
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlSalesrep_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesrep.SelectedIndexChanged
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("Config.aspx")
    End Sub
End Class
