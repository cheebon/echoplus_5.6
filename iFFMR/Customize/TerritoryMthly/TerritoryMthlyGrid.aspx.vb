Imports System.Data
Partial Class iFFMR_Customize_TerritoryMthly_TerritoryMthlyGrid
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim licItemFigureCollector As ListItemCollection
    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_TerritoryMthly")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_TerritoryMthly") = value
        End Set
    End Property
    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_TerritoryMthly"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "TerritoryMthly"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.MTHTERRITORY)
            .DataBind()
            .Visible = True
        End With



        If Not IsPostBack Then
            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.MTHTERRITORY
                .DataBind()
                .Visible = True
            End With


            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True

        End If

        lblErr.Text = ""

    End Sub

#Region "EVENT HANDLER"

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            RefreshDatabinding()

        End If

    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        ' Try
        RefreshDataBind()
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        'Try

        RenewDataBind()
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        ' Finally
        'End Try
    End Sub
#End Region

    '#Region "Paging Control"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnGo_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
    '        Try
    '            dgList.PageIndex = CInt(Wuc_dgpaging.PageNo - 1)

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkPrevious_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
    '        Try
    '            If dgList.PageIndex > 0 Then
    '                dgList.PageIndex = dgList.PageIndex - 1
    '            End If
    '            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkNext_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
    '        Try
    '            If dgList.PageCount - 1 > dgList.PageIndex Then
    '                dgList.PageIndex = dgList.PageIndex + 1
    '            End If
    '            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        ViewState("strSortExpression") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        dgList.PageIndex = 0
        'End If
        PreRenderMode(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_Count = 0
        Else
            Master_Row_Count = dtCurrentTable.Rows.Count
        End If

        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        dgList.PageSize = 20 'intPageSize
        dgList.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_Update()

        '  End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '  Try
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_TerritoryMthly.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_TerritoryMthly.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_TerritoryMthly.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_TerritoryMthly.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_TerritoryMthly.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_TerritoryMthly.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_TerritoryMthly.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_TerritoryMthly.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_TerritoryMthly.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 15 Then dgList.GridHeight = Nothing
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagrid.Update()
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        If li.Text Like "MTD_COVERAGE_*_PLANNED" OrElse li.Text Like "MTD_COVERAGE_*_ACHIEVED" Then
                            Dim strDivisorName As String = IIf(li.Text.ToUpper Like "*_ACHIEVED", li.Text.Replace("COVERAGE", "CVG").Replace("ACHIEVED", "PLANNED"), li.Text.Replace("_ACHIEVED", "_CUST_CONT").Replace("_PLANNED", "_CUST_CONT"))
                            e.Row.Cells(iIndex).Text = String.Format(CF_TerritoryMthly.GetOutputFormatString(li.Text), (DIVISION(licItemFigureCollector.FindByText(li.Text.Replace("COVERAGE", "CVG")), licItemFigureCollector.FindByText(strDivisorName)) * 100))
                        Else
                            e.Row.Cells(iIndex).Text = String.Format(CF_TerritoryMthly.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    End If
                Next

        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "MTD_CALL*" Then
                strColumnName = "MTD Actual Call"
            ElseIf strColumnName Like "YTD_CALL*" Then
                strColumnName = "YTD Actual Call"
            ElseIf strColumnName Like "MTD_COVERAGE_A*" Then
                strColumnName = "MTD Coverage A"
            ElseIf strColumnName Like "MTD_COVERAGE_B*" Then
                strColumnName = "MTD Coverage B"
            ElseIf strColumnName Like "MTD_COVERAGE_C*" Then
                strColumnName = "MTD Coverage C"
            ElseIf strColumnName Like "MTD_COVERAGE_OTH*" Then
                strColumnName = "MTD Coverage OTH"
            Else
                strColumnName = CF_TerritoryMthly.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "MTD*" OrElse _
             strColumnName Like "QTD*" OrElse _
             strColumnName Like "YTD*" OrElse strColumnName Like "PYTD*") _
             AndAlso Not (strColumnName Like "*VAR" OrElse strColumnName Like "*GROSS") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        Return dblValue
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        '  Try
        Dim dt As Data.DataTable = GetRecList()
        dt.Rows.Add(dt.NewRow)
        ViewState("dtCurrentView") = dt
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsTerritoryMthly As New rpt_Customize.clsTerritoryMthly

        Dim strUserID As String, strPrincipalID As String, strSalesrepCode As String, _
         strMonth As String, strYear As String, strPTLCode As String

        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strYear = Request.QueryString("YEAR")
        strMonth = Request.QueryString("MONTH")
        strSalesrepCode = Request.QueryString("SALESREP_CODE")
        strPTLCode = Request.QueryString("PTL_CODE")


        DT = clsTerritoryMthly.GetTerritoryMthly(strUserID, strPrincipalID, strYear, strMonth, strSalesrepCode, strPTLCode)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        ' Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'strb.Append("<table>")
        'strb.Append("<tr><td>")
        'strb.Append(hdfSalesrepName.Value)
        'strb.Append("</td></tr>")
        'strb.Append("</table>")

        Dim pnl As New Panel
        Dim strb As New StringBuilder

        Dim lblInfo As New Label
        strb.Append("<B>Year: </B>" & Request.QueryString("YEAR") & "<BR/>")
        strb.Append("<B>Month: </B>" & Request.QueryString("MONTH") & "<BR/>")
        strb.Append("<B>Field Force: </B>" & Request.QueryString("SALESREP_CODE") & "<BR/>")
        strb.Append("<B>Product: </B>" & Request.QueryString("PTL_CODE") & "<BR/>")
        strb.Append("<BR/>")

        lblInfo.Text = strb.ToString
        pnl.Controls.Add(lblInfo)
        pnl.Controls.Add(dgList)

        wuc_ctrlpanel.ExportToFile(pnl, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        ' Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        ' ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region
End Class


Public Class CF_TerritoryMthly
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "MTD_CALL_A"
                strFieldName = "A"
            Case "MTD_CALL_B"
                strFieldName = "B"
            Case "MTD_CALL_C"
                strFieldName = "C"
            Case "MTD_CALL_OTH"
                strFieldName = "OTH"
            Case "MTD_CALL_TTL"
                strFieldName = "Total"
            Case "MTD_CALL_PERCENT"
                strFieldName = "(%) of Call"
            Case "YTD_CALL_A"
                strFieldName = "A"
            Case "YTD_CALL_B"
                strFieldName = "B"
            Case "YTD_CALL_C"
                strFieldName = "C"
            Case "YTD_CALL_OTH"
                strFieldName = "OTH"
            Case "YTD_CALL_TTL"
                strFieldName = "Total"
            Case "YTD_CALL_PERCENT"
                strFieldName = "(%) of Call"
            Case "MTD_COVERAGE_A_CUST_CONT"
                strFieldName = "No Of Cust/Cont"
            Case "MTD_COVERAGE_A_PLANNED"
                strFieldName = "Planned (%)"
            Case "MTD_COVERAGE_A_ACHIEVED"
                strFieldName = "Achieved (%)"
            Case "MTD_COVERAGE_B_CUST_CONT"
                strFieldName = "No Of Cust/Cont"
            Case "MTD_COVERAGE_B_PLANNED"
                strFieldName = "Planned (%)"
            Case "MTD_COVERAGE_B_ACHIEVED"
                strFieldName = "Achieved (%)"
            Case "MTD_COVERAGE_C_CUST_CONT"
                strFieldName = "No Of Cust/Cont"
            Case "MTD_COVERAGE_C_PLANNED"
                strFieldName = "Planned(%)"
            Case "MTD_COVERAGE_C_ACHIEVED"
                strFieldName = "Achieved (%)"
            Case "MTD_COVERAGE_OTH_CUST_CONT"
                strFieldName = "No Of Cust/Cont"
            Case "MTD_COVERAGE_OTH_PLANNED"
                strFieldName = "Planned (%)"
            Case "MTD_COVERAGE_OTH_ACHIEVED"
                strFieldName = "Achieved"
            Case "MTD_COVERAGE_TTL"
                strFieldName = "Total Coverage"
            Case "GROUP_NAME"
                strFieldName = "Territory"
            Case "GROUP_CODE"
                strFieldName = "Territory Code"
            Case "MTD_SALES"
                strFieldName = "MTD Sales"
            Case "YTD_SALES"
                strFieldName = "YTD Sales"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)

        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        ' Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If
        '
        If (strColumnName Like "DESC_CODE") OrElse (strColumnName Like "TOTAL_CLOSING") OrElse (strColumnName Like "MTD_CVG_*") Then
            FCT = FieldColumntype.InvisibleColumn
        ElseIf strColumnName = "JAN" OrElse strColumnName = "FEB" OrElse strColumnName = "MAR" _
                     OrElse strColumnName = "APR" OrElse strColumnName = "MAY" OrElse strColumnName = "JUN" _
                     OrElse strColumnName = "JUL" OrElse strColumnName = "AUG" OrElse strColumnName = "SEP" _
                     OrElse strColumnName = "OCT" OrElse strColumnName = "NOV" OrElse strColumnName = "DEC" Then

            If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SRCALLRATEDTL, SubModuleAction.View) Then
                FCT = FieldColumntype.HyperlinkColumn
            End If

        End If

        Return FCT
        'Catch ex As Exception

        ' End Try
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        strColumnName = strColumnName.Replace("_CALLS", "")
        strColumnName = strColumnName.Replace("_WD", "")
        Select Case strColumnName
            Case "JAN"
                Return 1
            Case "FEB"
                Return 2
            Case "MAR"
                Return 3
            Case "APR"
                Return 4
            Case "MAY"
                Return 5
            Case "JUN"
                Return 6
            Case "JUL"
                Return 7
            Case "AUG"
                Return 8
            Case "SEP"
                Return 9
            Case "OCT"
                Return 10
            Case "NOV"
                Return 11
            Case "DEC"
                Return 12
            Case Else
                Return 1
        End Select
        'Catch ex As Exception
        'End Try
    End Function

    Public Shared Function GetDayValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        Dim intValue As Integer = 0

        strColumnName = strColumnName.Replace("_CALL", "").Replace("_DEF", "").Replace("DAY_", "")
        'strColumnName = strColumnName.Replace("S", "")
        If Not String.IsNullOrEmpty(strColumnName) AndAlso IsNumeric(strColumnName) Then intValue = CInt(strColumnName)
        Return intValue
        ' Catch ex As Exception
        ' End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        ' Try

        Select Case strNewName
            Case "CALL_PER_DAY", "CLOSING_RATE"
                strStringFormat = "{0:0.0}"
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "MTD_SALES", "YTD_SALES"
                strStringFormat = "{0:0.00}" 
            Case Else
                strStringFormat = ""
        End Select


        If strColumnName.ToUpper Like "MTD_CALL_*" Or strColumnName.ToUpper Like "YTD_CALL*" Or strColumnName.ToUpper Like "MTD_COVERAGE_*" Then
            If strColumnName Like "*PLANNED" Or strColumnName Like "*ACHIEVED" Or strColumnName Like "*PERCENT" Then
                strStringFormat = "{0:0.#}"
            Else
                strStringFormat = "{0:0}"
            End If
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf strColumnName Like "*_NAME" Then
                .HorizontalAlign = HorizontalAlign.Left
            ElseIf strColumnName Like "MTD_CALL_*" Or strColumnName Like "YTD_CALL*" Or strColumnName Like "MTD_COVERAGE_*" Or strColumnName = "MTD_SALES" Or strColumnName = "YTD_SALES" Then
                .HorizontalAlign = HorizontalAlign.Right
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

    Public Shared Function GetMonthName(ByVal iMonth As Integer) As String
        Dim strMonthName As String = ""

        ' Try
        Select Case iMonth
            Case 1
                strMonthName = "JAN"
            Case 2
                strMonthName = "FEB"
            Case 3
                strMonthName = "MAR"
            Case 4
                strMonthName = "APR"
            Case 5
                strMonthName = "MAY"
            Case 6
                strMonthName = "JUN"
            Case 7
                strMonthName = "JUL"
            Case 8
                strMonthName = "AUG"
            Case 9
                strMonthName = "SEP"
            Case 10
                strMonthName = "OCT"
            Case 11
                strMonthName = "NOV"
            Case 12
                strMonthName = "DEC"
            Case Else
                strMonthName = ""
        End Select
        'Catch ex As Exception
        'End Try

        Return strMonthName

    End Function
End Class
