<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InboxMsgPop.ascx.vb" Inherits="iFFMA_Utility_Messaging_InboxMsgPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:92%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:right; width:8%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                    
                    <!-- Begin Customized Content -->
                    <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Subject</span>
                    <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                    <span style="float:left; width:70%;">
                        <asp:Label ID="lblSubject" runat="server" CssClass="cls_label" />
                    </span>
                    <br />
                    
                    <span style="float:left; width:25%; padding-top:2px;" class="cls_label_header">Content</span>
                    <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                    <span style="float:left; width:70%;">
                        <asp:Label ID="lblMsgContent" runat="server" CssClass="cls_label" />
                    </span>
                        
                    <span style="float:left; width:100%; padding-top:10px; padding-bottom:10px">
                        <center>
                            <%--<asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" />--%>
                        </center>
                    </span>
                    <!-- End Customized Content -->
                    
                    <asp:HiddenField ID="hdID" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>