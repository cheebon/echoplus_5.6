﻿
Partial Class include_wuc_pnlMssExtraction
    Inherits System.Web.UI.UserControl

    Public Event ResetBtn_Click As EventHandler
    Public Event RefreshBtn_Click As EventHandler

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_pnlExtraction"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                LoadLsbTeam()
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub


#Region "PROPERTY"
    Public Property DateFrom() As String
        Get
            Return Wuc_MSSExtract_Date.DateStart
        End Get
        Set(ByVal value As String)
            Wuc_MSSExtract_Date.DateStart = value
        End Set
    End Property

    Public Property Dateto() As String
        Get
            Return Wuc_MSSExtract_Date.DateEnd
        End Get
        Set(ByVal value As String)
            Wuc_MSSExtract_Date.DateEnd = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedTeam))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedSalesrep))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedTitle))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedQues))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SubQuesCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedSubQues))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(txtCustCode.Text)
        End Get
        Set(ByVal value As String)

        End Set
    End Property

#End Region

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            LoadLsbTitle()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            LoadLsbTitle()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            LoadLsbTitle()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            LoadLsbTitle()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "TITLE"
    Private Sub LoadLsbTitle()
        Try
            lsbTitle.Items.Clear()
            lsbSelectedTitle.Items.Clear()

            Dim clsMSSExtraction As New rpt_Customize.clsMSSExtraction

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)
            Dim strSalesrepList As String = GetItemsInString(lsbSelectedSalesrep)

            With lsbTitle
                .DataSource = clsMSSExtraction.GetTitle(strUserID, strPrincipalID, strTeamList, strSalesrepList)
                .DataTextField = "TITLE_NAME"
                .DataValueField = "TITLE_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTitle : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTitle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTitle.Click
        Try
            AddToListBox(lsbTitle, lsbSelectedTitle)
            LoadLsbQues()
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddTitle_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTitle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTitle.Click
        Try
            AddToListBox(lsbSelectedTitle, lsbTitle)
            LoadLsbQues()
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveTitle_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTitle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTitle.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTitle.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTitle, lsbSelectedTitle)
            LoadLsbQues()
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllTitle_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTitle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTitle.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTitle.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTitle, lsbTitle)
            LoadLsbQues()
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllTitle_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "QUES"
    Private Sub LoadLsbQues()
        Try
            lsbQues.Items.Clear()
            lsbSelectedQues.Items.Clear()

            Dim clsMSSExtraction As New rpt_Customize.clsMSSExtraction

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)
            Dim strSalesrepList As String = GetItemsInString(lsbSelectedSalesrep)
            Dim strTitleList As String = GetItemsInString(lsbSelectedTitle)

            With lsbQues
                .DataSource = clsMSSExtraction.GetQues(strUserID, strPrincipalID, strTeamList, strSalesrepList, strTitleList)
                .DataTextField = "QUES_NAME"
                .DataValueField = "QUES_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbQues : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddQues.Click
        Try
            AddToListBox(lsbQues, lsbSelectedQues)
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveQues.Click
        Try
            AddToListBox(lsbSelectedQues, lsbQues)
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllQues.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbQues.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbQues, lsbSelectedQues)
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllQues.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedQues.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedQues, lsbQues)
            LoadLsbSubQues()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SUB_QUES"
    Private Sub LoadLsbSubQues()
        Try
            lsbSubQues.Items.Clear()
            lsbSelectedSubQues.Items.Clear()

            Dim clsMSSExtraction As New rpt_Customize.clsMSSExtraction

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)
            Dim strSalesrepList As String = GetItemsInString(lsbSelectedSalesrep)
            Dim strTitleList As String = GetItemsInString(lsbSelectedTitle)
            Dim strQuesList As String = GetItemsInString(lsbSelectedQues)

            With lsbSubQues
                .DataSource = clsMSSExtraction.GetSubQues(strUserID, strPrincipalID, strTeamList, strSalesrepList, strTitleList, strQuesList)
                .DataTextField = "SUB_QUES_NAME"
                .DataValueField = "SUB_QUES_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSubQues : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSubQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSubQues.Click
        Try
            AddToListBox(lsbSubQues, lsbSelectedSubQues)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSubQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSubQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSubQues.Click
        Try
            AddToListBox(lsbSelectedSubQues, lsbSubQues)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSubQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSubQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSubQues.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSubQues.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSubQues, lsbSelectedSubQues)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllSubQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSubQues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSubQues.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSubQues.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSubQues, lsbSubQues)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllSubQues_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region


#Region "EVENT HANDLER"
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            LoadLsbTeam()

            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()
            lsbTitle.Items.Clear()
            lsbSelectedTitle.Items.Clear()
            lsbQues.Items.Clear()
            lsbSelectedQues.Items.Clear()
            lsbSubQues.Items.Clear()
            lsbSelectedSubQues.Items.Clear()
            txtCustCode.Text = ""
            txtCustName.Text = ""
            Wuc_MSSExtract_Date.DateStart = ""
            Wuc_MSSExtract_Date.DateEnd = ""

            RaiseEvent ResetBtn_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnReset_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            RaiseEvent RefreshBtn_Click(sender, e)

            CPE_pnlMSSExtraction.ClientState = "true"
            CPE_pnlMSSExtraction.Collapsed = True

            UpdatePnlExtraction.Update()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            With (wuc_CustSearch)
                .ResetPage()
                .BindDefault()
                .Show()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SelectButton_Click
        Try
            txtCustCode.Text = wuc_CustSearch.CustCode
            txtCustName.Text = wuc_CustSearch.CustName
            UpdatePnlExtraction.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.CloseButton_Click
        Try
            wuc_CustSearch.Hide()
            UpdatePnlExtraction.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
