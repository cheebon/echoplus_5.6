Imports System.Data
Imports txn_WebActy

Partial Class iFFMS_Customer_CustTxn_TxnDRC
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Public Enum dgcoldtl As Integer
        PRD_CODE = 1
        PRD_NAME = 2
        OPEN_BAL_QTY = 3
        AMS = 4
        ICO = 5
        UOM_CODE = 6
        UOM_PRICE = 7
    End Enum

    Public Enum dgcol As Integer
        LINE_NO = 1
        PRD_CODE = 2
        OPEN_BAL_QTY = 5
        AMS = 6
        ICO = 7
        UOM_CODE = 8

    End Enum

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountDtl() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountDtl"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountDtl") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
 
        LoadTxnDescName()

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            If Request.QueryString("salesrepcode") IsNot Nothing Then
                LoadTxnNo()
                LoadDRCSummHdr()
            End If

        End If
    End Sub

#Region "EVENT HANDLER"

    Protected Sub ddlprdgrpcode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlprdgrpcode.SelectedIndexChanged
        RefreshDatabindingDtl()
    End Sub

    Protected Sub btnsavedtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavedtl.Click
        If Page.IsValid Then
            InsertDRCHdr()
            InsertDRCDtl()
       
        End If
        UpdateDtl.Update()
        UpdatePage.Update()

    End Sub

    Protected Sub btndeletesum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeletesum.Click
        DelDRCSumDtl()
        RefreshDatabinding()
        UpdatePage.Update()
    End Sub

    Protected Sub btnsavesum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavesum.Click
        If Page.IsValid Then
            UpdateDRCSumDtl()
            RefreshDatabinding()
        End If

    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try

            If dgList.Rows.Count > 0 Then

                InsertDRCHdr()
                Dim strSessionId As String, strVisitID As String, strSalesrepCode As String, strTxnNo As String, strCustCode As String, strContCode As String

                strSessionId = Trim(Request.QueryString("sessionid"))
                strVisitID = Trim(Request.QueryString("visitid"))
                strSalesrepCode = Trim(Session("SALESREP_CODE"))
                strCustCode = Trim(Request.QueryString("CustCode"))
                strContCode = Trim(Request.QueryString("ContCode"))
                strTxnNo = Trim(lbltxnno.Text)

                Dim strurl = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnDRCSub.aspx?sessionid=" + strSessionId + "&visitid=" + strVisitID + "&SALESREP_CODE=" + strSalesrepCode + "&TXNNO=" + strTxnNo + "&CustCode=" + strCustCode + "&ContCode=" + strContCode + "';"
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "HideElement('SubDetailBar');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Kindly add at least one detail record!')", True)
            End If



        Catch ex As Exception
            ExceptionMsg("btnsubmit_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnProceeed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceeed.Click
        btnsubmit_Click(sender, e)
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustTxnDRC"
        End Get
    End Property


#Region "DGLIST Dtl"
    Public Sub RefreshDatabindingDtl(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableDtl As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionDtl As String = CType(ViewState("strSortExpressionDtl"), String)


        dtCurrentTableDtl = GetRecListDtl()

        If dtCurrentTableDtl Is Nothing Then
            dtCurrentTableDtl = New DataTable
        Else
            If dtCurrentTableDtl.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountDtl = 0
            Else

                Master_Row_CountDtl = dtCurrentTableDtl.Rows.Count
            End If
        End If

        Dim dvCurrentViewDtl As New DataView(dtCurrentTableDtl)
        If Not String.IsNullOrEmpty(strSortExpressionDtl) Then
            Dim strSortExpressionNameDtl As String = strSortExpressionDtl.Replace(" DESC", "")
            dvCurrentViewDtl.Sort = IIf(dtCurrentTableDtl.Columns.Contains(strSortExpressionNameDtl), strSortExpressionDtl, "")
        End If

        With dglistdtl
            .DataSource = dvCurrentViewDtl
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountDtl > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateDtl()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListDtl() As DataTable
        Try
            Dim DT As DataTable = Nothing
            If Not String.IsNullOrEmpty(Session("UserID")) Then
                Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strPrdGrpCode As String
                strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
                strCustCode = Trim(Request.QueryString("CustCode"))
                strContCode = Trim(Request.QueryString("ContCode"))
                strPrdGrpCode = ddlprdgrpcode.SelectedValue.ToString

                Dim clsDRC As New txn_WebActy.clsDRC
                DT = clsDRC.GetPrdcode(strSalesrepCode, strCustCode, strContCode, strPrdGrpCode)

            End If

            Return DT
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function

    Public Sub UpdateDatagrid_UpdateDtl()

        UpdateDtl.Update()

    End Sub

    Protected Sub dglistdtl_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistdtl.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountDtl > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListDtl_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistdtl.Sorting
        Dim strSortExpressionDtl As String = ViewState("strSortExpressionDtl")

        If strSortExpressionDtl IsNot Nothing AndAlso strSortExpressionDtl.Length > 0 Then
            If strSortExpressionDtl Like (e.SortExpression & "*") Then
                If strSortExpressionDtl.IndexOf(" DESC") > 0 Then
                    strSortExpressionDtl = e.SortExpression
                Else
                    strSortExpressionDtl = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionDtl = e.SortExpression
            End If
        Else
            strSortExpressionDtl = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionDtl") = strSortExpressionDtl

        RefreshDatabindingDtl()

    End Sub

#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        dtCurrentTable = GetRecList()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'btncheckall.Visible = False
                'btnuncheckall.Visible = False
                btndeletesum.Visible = False
                btnsavesum.Visible = False
                Master_Row_Count = 0
            Else
                ' btncheckall.Visible = True
                'btnuncheckall.Visible = True
                btndeletesum.Visible = True
                btnsavesum.Visible = True
                Master_Row_Count = dtCurrentTable.Rows.Count

            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgList
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With


        UpdateDatagrid_Update()
        UpdatePage.Update()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTXNNo As String, strUserid As String, strsessionid As String, strSalesrepCode As String
            strTXNNo = Trim(lbltxnno.Text)
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strsessionid = Trim(Request.QueryString("sessionid"))
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))

            Dim clsDRC As New txn_WebActy.clsDRC
            DT = clsDRC.GetDRCSumDtl(strsessionid, strTXNNo, strUserid, strSalesrepCode)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_Update()

        If dglistdtl.Rows.Count > 0 Then
            If dglistdtl.Rows.Count < 15 Then
                Dim GridHeight As Integer = dglistdtl.Rows.Count * 25 + 25
                dglistdtl.GridHeight = GridHeight
            Else
                dglistdtl.GridHeight = 200
            End If

        Else
            dglistdtl.GridHeight = 50
        End If

        UpdateDatagrid.Update()

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub
#End Region
    
#Region "Header Function"
    Private Sub LoadTxnDescName()
        Try
            Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strvisitid As String, strMapPath As String

            strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))
            strvisitid = Trim(Request.QueryString("visitid"))
            strMapPath = "Ondemand > New Transaction > Inventory Forecasting"

            Dim dt As DataTable
            Dim clstxncommon As New txn_common.clstxncommon
            dt = clstxncommon.GetTxnDescName(strSalesrepCode, strCustCode, strContCode)

            If dt.Rows.Count > 0 Then
                lblFieldForceName.Text = Trim(dt.Rows(0)("SALESREP_NAME"))
                lblCustName.Text = Trim(dt.Rows(0)("CUST_NAME"))
                lblContName.Text = Trim(dt.Rows(0)("CONT_NAME"))
                lblVisitId.Text = strvisitid

                lblMapPath.Text = strMapPath
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub CreateHdrNewRecord()

        UpdateHdr.Update()
        UpdatePage.Update()
    End Sub

    Private Sub LoadTxnNo()
        Try
            Dim strTxnNo As String = Trim(Request.QueryString("TxnNo"))
            If IsNothing(strTxnNo) Or strTxnNo = "" Then
                Dim DTTXNno As DataTable
                Dim clsDRC As New txn_WebActy.clsDRC
                DTTXNno = clsDRC.GetTRANo(Request.QueryString("salesrepcode"))
                lbltxnno.Text = DTTXNno.Rows(0)("TXN_NO")
            Else
                lbltxnno.Text = strTxnNo
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub InsertDRCHdr()
        Try
            Dim strSessionId As String, strTxnNo As String, _
         strVisitId As String, strSalesrepCode As String, strCustCode As String, strContCode As String, _
         strUserId As String, strTxnDate As String

            strSessionId = Request.QueryString("sessionid")
            strTxnNo = lbltxnno.Text
            strVisitId = Request.QueryString("visitid")
            strSalesrepCode = Request.QueryString("salesrepcode")
            strCustCode = Request.QueryString("custcode")
            strContCode = Request.QueryString("contcode")
            strTxnDate = Request.QueryString("datein") + " " + Request.QueryString("timein")
            strUserId = Web.HttpContext.Current.Session("UserID").ToString

            Dim clsDRC As New txn_WebActy.clsDRC
            clsDRC.InstTMPDRCHeader(strSessionId, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContCode, strUserId, strTxnDate)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDRCSummHdr()
        Try
            Dim DT As DataTable
            Dim clsDRC As New txn_WebActy.clsDRC

            Dim strTXNNo As String, strUserid As String, strsessionid As String, strCustCode As String, strContCode As String
            strTXNNo = lbltxnno.Text
            strUserid = Web.HttpContext.Current.Session("UserID")
            strsessionid = Request.QueryString("sessionid")
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))

            DT = clsDRC.GetDRCSumHdrs(strsessionid, strTXNNo, strUserid, strCustCode, strContCode)
            If DT.Rows.Count > 0 Then
                lbltxnno.Text = DT.Rows(0)("TXN_NO")
                RefreshDatabinding()
                LoadPrdGrpDdl()
                RefreshDatabindingDtl()
            Else
                CreateHdrNewRecord()
                CreateDtlNewRecord()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Detail Function"

    Private Sub CreateDtlNewRecord()
        LoadPrdGrpDdl()

        UpdateDtl.Update()
        UpdatePage.Update()

    End Sub

    Protected Sub LoadPrdGrpDdl()
        Try
            Dim DT As DataTable
            Dim clsDRC As New txn_WebActy.clsDRC

            DT = clsDRC.GetPrdGrpcode(Request.QueryString("salesrepcode"))
            With ddlprdgrpcode
                .Items.Clear()
                .DataSource = DT.DefaultView
                .DataTextField = "PRD_GRP_NAME"
                .DataValueField = "PRD_GRP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub InsertDRCDtl()
        Try
            If dglistdtl.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim prompt As Integer = 0
                Dim DK As DataKey
                For Each DR As GridViewRow In dglistdtl.Rows

                    DK = dglistdtl.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim dbShelfQty As Double, dbStoreQty As Double, dbPOqty As Double

                        Dim txtshelf As TextBox = CType(dglistdtl.Rows(i).FindControl("txtShelf"), TextBox)
                        dbShelfQty = CDbl(IIf(txtshelf.Text = "", 0, txtshelf.Text))
                        Dim txtstore As TextBox = CType(dglistdtl.Rows(i).FindControl("txtStore"), TextBox)
                        dbStoreQty = CDbl(IIf(txtstore.Text = "", 0, txtstore.Text))
                        Dim txtpo As TextBox = CType(dglistdtl.Rows(i).FindControl("txtPO"), TextBox)
                        dbPOqty = CDbl(IIf(txtpo.Text = "", 0, txtpo.Text))
                        Dim chkSelection As CheckBox = CType(dglistdtl.Rows(i).FindControl("chkSelection"), CheckBox)

                        'If dbShelfQty > 0 OrElse dbStoreQty > 0 OrElse dbPOqty > 0 Then
                        If chkSelection.Checked = True Then
                            prompt = prompt + 1
                            Dim strTxnNo As String, strUserId As String, strsessionid As String, strPrdCode As String, strUomCode As String
                            Dim dblOpenBalQty As Double, dblShelfQty As Double, dblStoreQty As Double, dblPOQty As Double, dblICOQty As Double, dblAMSQty As Double

                            strTxnNo = Trim(lbltxnno.Text)
                            strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
                            strsessionid = Trim(Request.QueryString("sessionid"))
                            strPrdCode = Trim(dglistdtl.Rows(i).Cells(dgcoldtl.PRD_CODE).Text)
                            dblOpenBalQty = Trim(dglistdtl.Rows(i).Cells(dgcoldtl.OPEN_BAL_QTY).Text)
                            dblShelfQty = dbShelfQty
                            dblStoreQty = dbStoreQty
                            dblPOQty = dbPOqty
                            dblICOQty = Trim(dglistdtl.Rows(i).Cells(dgcoldtl.ICO).Text)
                            dblAMSQty = Trim(dglistdtl.Rows(i).Cells(dgcoldtl.AMS).Text)
                            strUomCode = Trim(dglistdtl.Rows(i).Cells(dgcoldtl.UOM_CODE).Text)

                            Dim clsDRC As New txn_WebActy.clsDRC
                            clsDRC.InstTMPDRCDetails(strsessionid, strTxnNo, strPrdCode, dblOpenBalQty, dblShelfQty, dblStoreQty, dblPOQty, dblICOQty, dblAMSQty, strUomCode, strUserId)
                        End If

                    End If

                    i += 1
                Next
                If prompt > 0 Then
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successsfully added " + prompt.ToString + " the transaction(s)!')", True)
                    RefreshDatabinding()
                    ddlprdgrpcode.SelectedIndex = 0
                    dglistdtl.DataSource = Nothing
                    dglistdtl.DataBind()
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('Kindly select at least 1 transaction(s)!')", True)
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub DelDRCSumDtl()
        Try
            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strTxnNo As String, strUserId As String, strsessionid As String, strPrdCode As String, strLineNo As String


                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dgList.Rows(i).FindControl("chkdelete"), CheckBox)

                        If chkdelete.Checked = True Then
                            strTxnNo = Trim(lbltxnno.Text)
                            strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
                            strsessionid = Trim(Request.QueryString("sessionid"))
                            strPrdCode = Trim(dgList.DataKeys(i).Item("PRD_CODE"))
                            strLineNo = Trim(dgList.DataKeys(i).Item("LINE_NO"))

                            Dim clsDRC As New txn_WebActy.clsDRC
                            clsDRC.DelTMPDRCDetails(strsessionid, strTxnNo, strUserId, strPrdCode, strLineNo)
                        End If

                    End If

                    i += 1
                Next

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub UpdateDRCSumDtl()
        Try
            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim dbShelfQty As Double, dbStoreQty As Double, dbPOqty As Double

                        Dim txtshelf As TextBox = CType(dgList.Rows(i).FindControl("txtSumShelf"), TextBox)
                        dbShelfQty = CDbl(txtshelf.Text)
                        Dim txtstore As TextBox = CType(dgList.Rows(i).FindControl("txtSumStore"), TextBox)
                        dbStoreQty = txtstore.Text
                        Dim txtpo As TextBox = CType(dgList.Rows(i).FindControl("txtSumPO"), TextBox)
                        dbPOqty = txtpo.Text

                        Dim strTxnNo As String, strUserId As String, strsessionid As String, strPrdCode As String, strUomCode As String, strLineNo As String
                        Dim dblOpenBalQty As Double, dblShelfQty As Double, dblStoreQty As Double, dblPOQty As Double, dblICOQty As Double, dblAMSQty As Double

                        strTxnNo = Trim(lbltxnno.Text)
                        strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
                        strsessionid = Trim(Request.QueryString("sessionid"))
                        strPrdCode = Trim(dgList.Rows(i).Cells(dgcol.PRD_CODE).Text)
                        dblOpenBalQty = Trim(dgList.Rows(i).Cells(dgcol.OPEN_BAL_QTY).Text)
                        dblShelfQty = dbShelfQty
                        dblStoreQty = dbStoreQty
                        dblPOQty = dbPOqty
                        dblICOQty = Trim(dgList.Rows(i).Cells(dgcol.ICO).Text)
                        dblAMSQty = Trim(dgList.Rows(i).Cells(dgcol.AMS).Text)
                        strUomCode = Trim(dgList.Rows(i).Cells(dgcol.UOM_CODE).Text)
                        strLineNo = Trim(dgList.Rows(i).Cells(dgcol.LINE_NO).Text)

                        Dim clsDRC As New txn_WebActy.clsDRC
                        clsDRC.UpdTMPDRCDetails(strsessionid, strTxnNo, strPrdCode, dblOpenBalQty, dblShelfQty, dblStoreQty, dblPOQty, dblICOQty, dblAMSQty, strUomCode, strLineNo, strUserId)

                    End If

                    i += 1
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

#End Region

  

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
  
End Class
