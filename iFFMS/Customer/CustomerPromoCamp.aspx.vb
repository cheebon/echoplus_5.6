Imports System.Data

Partial Class CustomerPromoCamp
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerPromoCamp"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        
        'Call Header
        With wuc_lblheader
            .Title = "Promo. Camp."
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If
      
    End Sub
    Sub onpageload()
        Dim strCustCode, strSalesmanCode As String
        'Put user code to initialize the page here
        Try
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))

            bindGridView(strCustCode)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub
    Sub bindGridView(ByVal strCustCode As String)
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getPromoCampDT(strCustCode, "1")
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
            Else
                dt.Rows.Add(dt.NewRow)
                gridview1.DataSource = dt
                gridview1.DataBind()
                gridview1.Columns(5).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub addClickEvent(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gridview1.RowCreated
        Dim strCustCode, strSalesmanCode As String
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            dt = obj.getPromoCampDT(strCustCode, "1")
            If dt.Rows.Count > 0 Then
                If e.Row.RowIndex <> -1 Then
                    Dim PostBack As String = Page.ClientScript.GetPostBackEventReference(gridview1, "Select$" & e.Row.RowIndex)
                    e.Row.Attributes.Add("OnClick", PostBack)
                    e.Row.Attributes.Add("Style", "cursor:hand")
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".addClickEvent : " & ex.ToString)
        End Try


    End Sub

    Protected Sub dg_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridview1.SelectedIndexChanged
        Dim strCampNo As String
        Try
            strCampNo = gridview1.SelectedRow.Cells(0).Text
            redirect(strCampNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".dg_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Sub redirect(ByVal strCampNo As String)
        Dim dt As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Dim strCustCode As String
        Try
            strCustCode = Trim(Session("customercode"))
            dt = obj.getPromoCampDT(strCustCode, "1")
            If dt.Rows.Count > 0 Then
                Response.Redirect("CustomerPromoCampDetail.aspx?campno=" + strCampNo)
            Else
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub gridview_Click(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand
        Dim strCampNo As String

        Try
            strCampNo = gridview1.Rows(e.CommandArgument).Cells(0).Text
            redirect(strCampNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".gridview_Click : " & ex.ToString)
        Finally

        End Try

    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



