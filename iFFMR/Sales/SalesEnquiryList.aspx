<%@ Page Language="vb" AutoEventWireup="false" Inherits="SalesEnquiryList" CodeFile="SalesEnquiryList.aspx.vb" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sales Enquiry List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesEnquiryList" method="post" runat="server">
       
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td colspan="2" align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="btnBack" runat="server" CssClass="cls_button"  OnClientClick="javascript:history.back();" ToolTip="Back To Previous Page." Text="Back" Visible="false"></asp:Button>
                                                                            </td>
                                                                            <td align="left" style="width: 100%">
                                                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                                                <asp:Timer  id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr align="left">
                                                                <td>
                                                                    &nbsp;</td>
                                                                <td style="width: 98%">
                                                                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>
                                                                </td>
                                                            </tr>
                                                            <tr align="left">
                                                                <td>
                                                                    &nbsp;</td>
                                                                <td style="width: 100%">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="455" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="true" AllowPaging="True" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <customToolkit:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>        
    </form>
</body>
</html>
                                                                        <%--<Columns>
                                                                            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                                                                                <itemstyle horizontalalign="Center" />
                                                                                <itemtemplate>
                                                                    <asp:Label ID="Label1" runat="server" Text='<%# string.format("{0:yyyy-MM-dd}",Container.DataItem("DATE")) %>'></asp:Label>
                                                                
</itemtemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="SALES_AREA_NAME" HeaderText="Sales Area" ReadOnly="True" SortExpression="SALES_AREA_NAME">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SHIPTO_CODE" HeaderText="Shipto Code" ReadOnly="True" SortExpression="SHIPTO_CODE">
                                                                                <itemstyle horizontalalign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SHIPTO_NAME" HeaderText="Shipto Name" ReadOnly="True" SortExpression="SHIPTO_NAME">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SALESREP_CODE" HeaderText="Sales Rep. Code" ReadOnly="True"
                                                                                SortExpression="SALESREP_CODE">
                                                                                <itemstyle horizontalalign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="INV_NO" HeaderText="Inv No." ReadOnly="True" SortExpression="INV_NO">
                                                                                <itemstyle horizontalalign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" ReadOnly="True" SortExpression="PRD_CODE">
                                                                                <itemstyle horizontalalign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="PRD_NAME" HeaderText="Product Name" ReadOnly="True" SortExpression="PRD_NAME">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code" ReadOnly="True" SortExpression="CUST_CODE">
                                                                                <itemstyle horizontalalign="Center" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True" SortExpression="CUST_NAME">
                                                                                <itemstyle horizontalalign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SALES_QTY" HeaderText="QTY" ReadOnly="True" SortExpression="SALES_QTY"
                                                                                DataFormatString="{0:0}" HtmlEncode="False">
                                                                                <itemstyle horizontalalign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="FOC_QTY" HeaderText="FOC" ReadOnly="True" SortExpression="FOC_QTY"
                                                                                DataFormatString="{0:0}" HtmlEncode="False">
                                                                                <itemstyle horizontalalign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="SALES" HeaderText="Sales Value" ReadOnly="True" SortExpression="SALES"
                                                                                DataFormatString="{0:#,0.00}" HtmlEncode="False">
                                                                                <itemstyle horizontalalign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>--%>
<%--<asp:DataGrid ID="dgSalesEnquiryList" CssClass="cls_table" runat="server" Width="100%"
                                                                    DataKeyField="Prdcode" AutoGenerateColumns="False" BorderColor="SlateGray">
                                                                    <AlternatingItemStyle CssClass="cls_table_odd"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="cls_table_item"></ItemStyle>
                                                                    <HeaderStyle CssClass="cls_table_header"></HeaderStyle>
                                                                    <FooterStyle CssClass="cls_tableheader"></FooterStyle>
                                                                    <PagerStyle Position="Top" CssClass="cls_table_header"></PagerStyle>
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="Date" SortExpression="Date">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text='<%# date.Parse( Container.DataItem("Date")).ToString("dd-MM-yyyy") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="description" HeaderText="Branch" ReadOnly="True" SortExpression="branch">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="shipToCode" HeaderText="Ship To Code" ReadOnly="True"
                                                                            SortExpression="shipToCode">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="shiptoname1" HeaderText="Ship To Name" ReadOnly="True"
                                                                            SortExpression="shiptoname1">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="salesman" HeaderText="Salesman Code" ReadOnly="True"
                                                                            SortExpression="salesman">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="invno" HeaderText="Inv No." ReadOnly="True" SortExpression="invno">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="prdcode" HeaderText="Product Code" ReadOnly="True" SortExpression="prdcode">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="prddesc" HeaderText="Product Name" ReadOnly="True" SortExpression="prddesc">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="custcode" HeaderText="Customer Code" ReadOnly="True"
                                                                            SortExpression="custcode">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="company" HeaderText="Customer Name" ReadOnly="True" SortExpression="company">
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="trade_qty" HeaderText="Quantity" ReadOnly="True" SortExpression="trade_qty"
                                                                            DataFormatString="{0:0}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="foc_qty" HeaderText="FOC" ReadOnly="True" SortExpression="foc_qty"
                                                                            DataFormatString="{0:0}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="sales" HeaderText="Sales Value" ReadOnly="True" SortExpression="sales"
                                                                            DataFormatString="{0:0.00}">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>--%>
