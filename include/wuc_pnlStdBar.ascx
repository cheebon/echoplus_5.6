<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_pnlStdBar.ascx.vb"
    Inherits="include_wuc_pnlStdBar" %>
<asp:Panel ID="pnlToolbar" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr align="left">
            <td align="left">
                <asp:UpdatePanel ID="UpdateStdCar" runat="server" UpdateMode="Conditional">
                <Triggers>
                <asp:PostBackTrigger ControlID="imgExport"  />
                </Triggers>
                <ContentTemplate >
                  <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                    width="100%" border="0" style="height: 30px; padding: 2px 0px 2px 2px">
                    <tr valign="bottom">
                        <td>
                            <asp:Image ID="imgExpandCollapse" ImageUrl="~/images/ico_Field.gif" runat="server"
                                CssClass="cls_button" ToolTip="Criteria Selection" EnableViewState="false" onclick="ShowHideElement('TopBar')" />
                        </td>
                        <td style="padding-left: 5px; padding-right: 5px">
                            <asp:Image ID="imgEnquirySeparator" runat="server" ImageUrl="~/images/toolbarseparator.gif" />
                        </td>
                        <td>
                            <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server"
                                CssClass="cls_button" ToolTip="Export"></asp:ImageButton>
                        </td>
                        <td style="width: 95%;">
                        </td>
                    </tr>
                </table>
                
                </ContentTemplate>
                </asp:UpdatePanel>
              
            </td>
        </tr>
        <tr align="left">
            <td align="left">
            </td>
        </tr>
    </table>
</asp:Panel>
<table cellspacing="0" cellpadding="0" width="100%" border="0" class="cls_panel_header">
    <asp:Panel ID="pnlExport" runat="server" Visible="false">
        <tr>
            <td>
                <table class="cls_panel" id="Table1" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlExport" CssClass="cls_dropdownlist" runat="server">
                                            <asp:ListItem Value="0" Selected="True">Excel</asp:ListItem>
                                            <asp:ListItem Value="1">Text</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td>
                                        <asp:Button ID="btnExport" CssClass="cls_button" runat="server" Text="Export"></asp:Button></td>
                                    <td align="right" width="90%">
                                        <asp:ImageButton ID="imgClose_Export" runat="server" ImageUrl="~/images/ico_close.gif">
                                        </asp:ImageButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </asp:Panel>
</table>
