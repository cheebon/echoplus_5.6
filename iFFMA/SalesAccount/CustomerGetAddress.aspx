﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerGetAddress.aspx.vb" Inherits="CustomerGetAddress" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%@ Register TagPrefix="uc1" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Get Address</title>
<link href="../../include/DKSH.css" rel="stylesheet" />
<link href="../../include/jquery-ui.css" rel="stylesheet" />
<link href="../../include/msgBoxcss.css" rel="stylesheet" />
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="-1"/>
</head>
<!--#include File="~/include/commonutil.js"-->

<script src="../../include/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../../include/jquery-ui.js" type="text/javascript"></script>    
    <script src="../../include/jquery.msgBox.js"  type="text/javascript"></script>
<%--<script src="../../../Scripts/Layout.js" type="text/javascript"></script>--%>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo&sensor=true" type="text/javascript"> </script>

      <script type="text/javascript">
          var geocoder;
          var map;
          var marker;
          var Default_Latitude = 3.1390;    // KL Latitude
          var Default_Longitude = 101.6868; // KL Longitude
          var Default_Zoomlevel = 5;
          var IsDrag = false;
          var latlng = new google.maps.LatLng(Default_Latitude, Default_Longitude);
          function initialize() {
             
           
              var options = {
                  zoom: 7,
                  center: latlng,
                  scaleControl: true,
                  mapTypeControl: true,
                  streetViewControl: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP
              };

              map = new google.maps.Map(document.getElementById("map_canvas"), options);
              //GEOCODER
              geocoder = new google.maps.Geocoder();
              marker = new google.maps.Marker({
                  map: map,
                  draggable: true
              });
              marker.setPosition(latlng);
              map.setCenter(latlng);
              Submit();
              PlotCust();
          }
          function Submit() {
            
              var address = $("#txtaddress").val();
              geocoder = new google.maps.Geocoder();
              //if (marker != undefined)
              //{
              //    marker.setMap(null);
              //}
              //$('#txtlatitude').val('');
              //$('#txtlongitude').val('');
             
              if ($('#txtlatitude').val() == '' && $('#txtlongitude').val() == '' && $("#txtaddress").val() != '') {
                  if (geocoder) {
                      geocoder.geocode({ 'address': address }, function (results, status) {

                          if (status == google.maps.GeocoderStatus.OK) {
                              var lat = results[0].geometry.location.lat();
                              var lon = results[0].geometry.location.lng();
                              var location = new google.maps.LatLng(lat, lon);
                              marker.setMap(map);
                              marker.setPosition(location);
                              map.setCenter(location);
                              $('#txtaddress').val(results[0].formatted_address);
                              $('#txtlatitude').val(marker.getPosition().lat());
                              $('#txtlongitude').val(marker.getPosition().lng());
                              $('#hfAccuracy').val(results[0].geometry.location_type);


                              // }
                          } else if (status == 'OVER_QUERY_LIMIT') {

                          }
                          else {

                          }
                      });
                  }
              // $("#btnUpdate").attr('value', 'Confirm GPS');
              } else {
                  $("#btnUpdate").attr('value', 'Update GPS');
                
              }
              if (IsDrag == true) {
                  $('#txtlatitude').val('');
                  $('#txtlongitude').val('');
                  if (geocoder) {
                      geocoder.geocode({ 'address': address }, function (results, status) {

                          if (status == google.maps.GeocoderStatus.OK) {
                              var lat = results[0].geometry.location.lat();
                              var lon = results[0].geometry.location.lng();
                              var location = new google.maps.LatLng(lat, lon);
                              marker.setMap(map);
                              marker.setPosition(location);
                              map.setCenter(location);
                              $('#txtaddress').val(results[0].formatted_address);
                              $('#txtlatitude').val(marker.getPosition().lat());
                              $('#txtlongitude').val(marker.getPosition().lng());
                              $('#hfAccuracy').val(results[0].geometry.location_type);


                              // }
                          } else if (status == 'OVER_QUERY_LIMIT') {

                          }
                          else {

                          }
                      });
                  }
              }
          }
          function SubmitAddress() {

              var address = $("#txtaddress").val();
              geocoder = new google.maps.Geocoder();
              //if (marker != undefined) {
              //    marker.setMap(null);
              //}
              $('#txtlatitude').val('');
              $('#txtlongitude').val('');
             
                  if (geocoder) {
                      geocoder.geocode({ 'address': address }, function (results, status) {

                          if (status == google.maps.GeocoderStatus.OK) {
                              var lat = results[0].geometry.location.lat();
                              var lon = results[0].geometry.location.lng();
                              var location = new google.maps.LatLng(lat, lon);
                              marker.setMap(map);
                              marker.setPosition(location);
                              map.setCenter(location);
                              $('#txtaddress').val(results[0].formatted_address);
                              $('#txtlatitude').val(marker.getPosition().lat());
                              $('#txtlongitude').val(marker.getPosition().lng());
                              $('#hfAccuracy').val(results[0].geometry.location_type);


                              // }
                          } else if (status == 'OVER_QUERY_LIMIT') {

                          }
                          else {

                          }
                      });
                  }
              
          }
         
          $(document).ready(function () {
              $("#imgOpenCustomer").hide();
              initialize();
              $("#txtaddress").autocomplete({
                  //This bit uses the geocoder to fetch address values
                  source: function (request, response) {
                      geocoder.geocode({ 'address': request.term }, function (results, status) {
                          response($.map(results, function (item) {
                              return {
                                  label: item.formatted_address,
                                  value: item.formatted_address,
                                  latitude: item.geometry.location.lat(),
                                  longitude: item.geometry.location.lng()

                              }
                          }));
                      })
                  },
                  ////This bit is executed upon selection of an address
                  select: function (event, ui) {
                      $('#txtaddress').val(ui.item.label.split('-')[0]);
                  },
                  focus: function (event, ui) {
                      $('#txtaddress').val(ui.item.label);
                      return false;
                  },
              });
              $("#txtaddress").keypress(function (e) {
                  if (e.keyCode == 13) {
                      e.preventDefault();

                      return false;
                  }
              });
              //Add listener to marker for reverse geocoding
              google.maps.event.addListener(marker, 'drag', function () {
                  geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                          if (results[0]) {
                              $('#txtaddress').val(results[0].formatted_address);
                              $('#txtlatitude').val(marker.getPosition().lat());
                              $('#txtlongitude').val(marker.getPosition().lng());
                              $('#hfAccuracy').val(results[0].geometry.location_type);
                          }
                      }
                  });

                  IsDrag = true;
              });

          });
         
          function PlotCust() {
              if ($('#txtlatitude').val() != '' && $('#txtlongitude').val() != '') {
                  if ($('#txtlatitude').val() != null && $('#txtlongitude').val() != null) {
                      var myLatLng = new google.maps.LatLng($('#txtlatitude').val(), $('#txtlongitude').val());
                      // setTimeout(delay(myLatLng, imageIconWhite), 500);
                      marker = new google.maps.Marker({
                          position: myLatLng,
                          map: map,
                          draggable: true
                      });
                      marker.setMap(map);
                      marker.setPosition(myLatLng);
                      map.setCenter(myLatLng);
                  }
              } 
          }
           </script>

<body class="BckgroundInsideContentLayout"  >
    <form id="frmCustomerGetAddress" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" CombineScripts="True" />
    <fieldset style="width: 97%; border: 0">
        <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" /> 
        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err" meta:resourcekey="lblErrResource1"></asp:Label>
       
      
       <table border="0" cellpadding="2" cellspacing="2" width="100%" align="center" class="Bckgroundreport">
           
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
             <tr class="Bckgroundreport" >
                <td colspan="2" >
                   <table  style="border: 1px solid #EBEBEB" cellpadding="0" cellspacing="2"  width="100%">
                       <tr>
                                 <td style="width:20%">   <asp:Label ID="lblCustCodeTitle" runat="server" Text="Cust Code" class="cls_label_header"></asp:Label>
              
                                     </td>
                           <td style="width:1%">:
                                     </td>
                                  <td>
                                        <asp:Label ID="lblCustCode" runat="server"  class="cls_label"></asp:Label>
              
                                     </td>
                                 </tr> 
                       <tr>
                <td>
                     <asp:Label ID="lblCustNameTitle" runat="server" Text="Cust Name" class="cls_label_header"></asp:Label>
              
                 </td>
                           <td>:
                                     </td>
                  <td>
                    <asp:Label ID="lblCustName" runat="server"  class="cls_label"></asp:Label>
                 </td>
               </tr>
                       <tr>
                           <td> <asp:Label ID="lblCustAddressTitle" runat="server" Text="Cust Address" class="cls_label_header"></asp:Label></td>
                            <td>:</td>
                            <td><asp:Label ID="lblCustAddress" runat="server"  class="cls_label"></asp:Label></td>
                       </tr>
                       <tr >
                        <td>
                          <asp:Label ID="lblAddress" runat="server" Text="Address" class="cls_label_header"></asp:Label>
                         </td>
                           <td>:
                                     </td>
                         <td>  <input id="txtaddress"  type="text" style="width:400px;" runat="server"  class="cls_textbox"/>
                         </td>
                      </tr>
                       <tr>
                           <td><asp:Button ID="btnSubmit" runat="server" Text="Submit"  CssClass="cls_button" OnClientClick="SubmitAddress();return false;" /></td>
                            <td></td>
                            <td></td>
                       </tr>
                   </table>
              </td>
               </tr>
            
           
           <tr class="Bckgroundreport"  valign="top">
            <td  style="width:605px;"><div id="map_canvas" style="width:600px; height:350px"></div></td>
            <td align="left">
                 
                  
                <table  style="border: 1px solid #EBEBEB" cellpadding="5" cellspacing="0" width="50%">
                             <tr>
                                 <td>
                           <asp:Label ID="lblLatitude" runat="server" Text="Latitude" class="cls_label_header"></asp:Label>
                    </td>
                                   <td><input id="txtlatitude" type="text" runat="server"  class="cls_textbox"/></td>
                             </tr>
                              <tr>
                                 <td>   <asp:Label ID="lblLongitude" runat="server" Text="Longitude" class="cls_label_header"></asp:Label>
   </td>
                                   <td><input id="txtlongitude" type="text" runat="server" class="cls_textbox"/>
                                       <asp:HiddenField ID="hfAccuracy" runat="server" />
                                   </td>
                             </tr>
                     <tr>
                        <td> </td>
                          <td> </td>
                    </tr>
                       <tr>
                             <td colspan="2">
                              <asp:Button ID="btnReset" runat="server" Text="Reset"  CssClass="cls_button" />
                                 <asp:Button ID="btnUpdate" runat="server" Text="Update GPS"  CssClass="cls_button" />
                               
                             </td>
                            
                           </tr>
                    </table>
                  <panel id="pnlConfirmGPS" runat="server"> <table  style="border: 1px solid #EBEBEB" cellpadding="5" cellspacing="0" width="50%">
                    <tr>
                          <td colspan="2">  <asp:Label ID="Label1" runat="server" Text="Existing Confirmed location" class="cls_label_header"></asp:Label></td>
                        
                    </tr>
                    <tr>
                        <td>    <asp:Label ID="Label2" runat="server" Text="Latitude" class="cls_label_header"></asp:Label></td>
                         <td>    <asp:Label ID="lblCLatitude" runat="server" Text="" class="cls_label_header"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>    <asp:Label ID="Label3" runat="server" Text="Longitude" class="cls_label_header"></asp:Label></td>
                         <td>    <asp:Label ID="lblCLongitude" runat="server" Text="" class="cls_label_header"></asp:Label></td>
                    </tr>
                    
                    <tr>
                        <td> </td>
                          <td> </td>
                    </tr>
                        </table>
                    </panel>
                     <panel id="pnlUnConfirmGPS" runat="server">
                         <table  style="border: 1px solid #EBEBEB" cellpadding="5" cellspacing="0" width="50%">
                    <tr>
                          <td colspan="2">  <asp:Label ID="Label4" runat="server" Text="Unconfirmed location" class="cls_label_header"></asp:Label></td>
                        
                    </tr>
                    <tr>
                        <td>    <asp:Label ID="Label5" runat="server" Text="Latitude" class="cls_label_header"></asp:Label></td>
                         <td>    <asp:Label ID="lblNLatitude" runat="server" Text="" class="cls_label_header"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>    <asp:Label ID="Label7" runat="server" Text="Longitude" class="cls_label_header"></asp:Label></td>
                         <td>    <asp:Label ID="lblNLongitude" runat="server" Text="" class="cls_label_header"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="2">   <asp:Button ID="btnComfirm" runat="server" Text="Confirm GPS"  CssClass="cls_button" /><asp:Button ID="btnDeny" runat="server" Text="Deny GPS"  CssClass="cls_button" /> </td>
                       
                    </tr> </table>
</panel>
                    
                 </td>
       </tr> 

        </ContentTemplate>
  </asp:UpdatePanel>
           </table> 


  

 
              
   
 </fieldset>


    </form>
</body>
</html>
