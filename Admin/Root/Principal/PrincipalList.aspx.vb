﻿Option Explicit On

Imports System.Data
Imports adm_SuperAdmin

Partial Class Admin_Root_Principal_PrincipalList
    Inherits System.Web.UI.Page
    Private intPageSize As Integer

    Public ReadOnly Property PageName() As String
        Get
            Return "Principal List"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = 30
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Principal"

            If Not IsPostBack Then
                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable '= CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()
            'If dtCurrenttable Is Nothing Then
            '    dtCurrenttable = New DataTable
            'Else
            '    If dtCurrenttable.Rows.Count = 0 Then
            '        dtCurrenttable.Rows.Add(dtCurrenttable.NewRow())
            '    End If
            'End If

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = IIf(dtCurrenttable.Rows.Count > intPageSize, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsPrincipal
        Try

            'DT = obj.PrincipalList(txtPrincipalCode.Text.Trim, txtPrincipalName.Text.Trim)
            DT = obj.PrincipalList(ddlSearchType.SelectedValue, txtSearchValue.Text)


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region
#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            Response.Redirect("PrincipalDtl.aspx?action=CREATE", False)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim obj As New adm_SuperAdmin.clsPrincipal

        Try
            Dim index As Integer = 0
            Dim row As GridViewRow
            Dim strPrincipalID As String

            If Integer.TryParse(e.CommandArgument, index) = True Then
                row = IIf(index >= 0 AndAlso index <= dgList.Rows.Count, dgList.Rows(index), Nothing)
                strPrincipalID = Trim(dgList.DataKeys(row.RowIndex).Value)

                Select Case e.CommandName
                    Case "Delete"
                        With obj
                            .Delete(strPrincipalID, Session("UserID"))
                        End With
                        Response.Redirect("PrincipalList.aspx", False)

                    Case "Details"
                        Response.Redirect("PrincipalDtl.aspx?PRINCIPAL_ID=" & strPrincipalID & "&action=VIEW", False)

                    Case "Modify"
                        Response.Redirect("PrincipalDtl.aspx?PRINCIPAL_ID=" & strPrincipalID & "&action=EDIT", False)

                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Dim imgEdit, imgDelete As ImageButton

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim btnDelete As ImageButton = CType(e.Row.Cells(0).Controls(0), ImageButton)
                If btnDelete IsNot Nothing Then
                    btnDelete.OnClientClick = "if (confirm('Are you sure want to delete?') == false) { window.event.returnValue = false; return false; }"
                End If

                'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.PRINCIPAL, SubModuleAction.Edit) Then
                '    imgEdit = CType(e.Row.Cells(10).Controls(0), ImageButton)
                '    imgEdit.Visible = False
                'End If

                'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.PRINCIPAL, SubModuleAction.Delete) Then
                '    imgDelete = CType(e.Row.Cells(9).Controls(0), ImageButton)
                '    imgDelete.Visible = False
                'End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RenewDataBind()
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression Like ("* DESC") Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            If ddlSearchType.SelectedIndex = 0 Then
                pnlSeachValue.Visible = False
                txtSearchValue.Text = ""
            Else
                pnlSeachValue.Visible = True
                txtSearchValue.Text = ""
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class
