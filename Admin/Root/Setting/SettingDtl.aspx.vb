﻿Option Explicit On

Imports System.Data

Partial Class Admin_Root_Setting_SettingDtl
    Inherits System.Web.UI.Page
#Region "Properties"
    Private Enum MODE
        Normal
        Edit
        Create
    End Enum
    Private Property SettingID() As String
        Get
            Return ViewState("SETTING_ID")
        End Get
        Set(ByVal value As String)
            ViewState("SETTING_ID") = value
        End Set
    End Property
    Private Property ActionMode() As MODE
        Get
            Return IIf(ViewState("ActionMode") Is Nothing, MODE.Normal, ViewState("ActionMode"))
        End Get
        Set(ByVal value As MODE)
            ViewState("ActionMode") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Setting"
            If Not IsPostBack Then
                Dim strSettingId As String
                strSettingId = Trim(Request.QueryString("SETTING_ID"))
                If IsNothing(strSettingId) Or strSettingId = "" Then
                    SettingID = 0
                    'btnBack.Visible = False
                Else
                    SettingID = strSettingId
                End If

                Dim strAction As String = Trim(Request.QueryString("action"))
                Select Case strAction.ToUpper
                    Case "CREATE"
                        ChangeMode(MODE.Create)
                    Case "EDIT"
                        ChangeMode(MODE.Edit)
                    Case Else 'VIEW
                        ChangeMode(MODE.Normal)
                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub OnAndLoadSettingDDL()
        Dim obj = New adm_SuperAdmin.clsSetting
        Dim DT As DataTable = Nothing
        Dim dt2 As DataTable
        Dim dv As DataView

        Try
            DT = obj.SettingList("SETTING_ID", "*")
            dv = DT.DefaultView
            dv.Sort = "FFMA_DB_NAME asc"

            With ddlSetting
                .Items.Clear()
                .DataSource = dv.ToTable
                .DataTextField = "FFMA_DB_NAME"
                .DataValueField = "SETTING_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- Select --", ""))
                .Visible = True
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub ReferenceID(ByVal dt As DataTable)
        Try
            CType(dvFFMA.Rows(0).FindControl("txtFFMAServerIP"), TextBox).Text = dt.Rows(0)("FFMA_SERVER_IP").ToString.Trim
            CType(dvFFMA.Rows(0).FindControl("txtFFMAServerName"), TextBox).Text = dt.Rows(0)("FFMA_SERVER_NAME").ToString.Trim
            CType(dvFFMA.Rows(0).FindControl("txtFFMADBName"), TextBox).Text = dt.Rows(0)("FFMA_DB_NAME").ToString.Trim
            CType(dvFFMA.Rows(0).FindControl("txtFFMALinkServer"), TextBox).Text = dt.Rows(0)("FFMA_LINK_SERVER_NAME").ToString.Trim
            CType(dvFFMA.Rows(0).FindControl("txtFFMAUserID"), TextBox).Text = dt.Rows(0)("FFMA_USER_ID").ToString.Trim
            CType(dvFFMA.Rows(0).FindControl("txtFFMAPassword"), TextBox).Text = dt.Rows(0)("FFMA_USER_PWD").ToString.Trim

            CType(dvFFMR.Rows(0).FindControl("txtFFMRServerIP"), TextBox).Text = dt.Rows(0)("FFMR_SERVER_IP").ToString.Trim
            CType(dvFFMR.Rows(0).FindControl("txtFFMRServerName"), TextBox).Text = dt.Rows(0)("FFMR_SERVER_NAME").ToString.Trim
            CType(dvFFMR.Rows(0).FindControl("txtFFMRDBName"), TextBox).Text = dt.Rows(0)("FFMR_DB_NAME").ToString.Trim
            CType(dvFFMR.Rows(0).FindControl("txtFFMRLinkServer"), TextBox).Text = dt.Rows(0)("FFMR_LINK_SERVER_NAME").ToString.Trim
            CType(dvFFMR.Rows(0).FindControl("txtFFMRUserID"), TextBox).Text = dt.Rows(0)("FFMR_USER_ID").ToString.Trim
            CType(dvFFMR.Rows(0).FindControl("txtFFMRPassword"), TextBox).Text = dt.Rows(0)("FFMR_USER_PWD").ToString.Trim

            CType(dvFFMS.Rows(0).FindControl("txtFFMSServerIP"), TextBox).Text = dt.Rows(0)("FFMS_SERVER_IP").ToString.Trim
            CType(dvFFMS.Rows(0).FindControl("txtFFMSServerName"), TextBox).Text = dt.Rows(0)("FFMS_SERVER_NAME").ToString.Trim
            CType(dvFFMS.Rows(0).FindControl("txtFFMSDBName"), TextBox).Text = dt.Rows(0)("FFMS_DB_NAME").ToString.Trim
            CType(dvFFMS.Rows(0).FindControl("txtFFMSLinkServer"), TextBox).Text = dt.Rows(0)("FFMS_LINK_SERVER_NAME").ToString.Trim
            CType(dvFFMS.Rows(0).FindControl("txtFFMSUserID"), TextBox).Text = dt.Rows(0)("FFMS_USER_ID").ToString.Trim
            CType(dvFFMS.Rows(0).FindControl("txtFFMSPassword"), TextBox).Text = dt.Rows(0)("FFMS_USER_PWD").ToString.Trim

            CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSServerIP"), TextBox).Text = dt.Rows(0)("ECHOPLUS_SERVER_IP").ToString.Trim
            CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSServerName"), TextBox).Text = dt.Rows(0)("ECHOPLUS_SERVER_NAME").ToString.Trim
            CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSDBName"), TextBox).Text = dt.Rows(0)("ECHOPLUS_DB_NAME").ToString.Trim
            CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSLinkServer"), TextBox).Text = dt.Rows(0)("ECHOPLUS_LINK_SERVER_NAME").ToString.Trim
            CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSUserID"), TextBox).Text = dt.Rows(0)("ECHOPLUS_USER_ID").ToString.Trim
            CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSPassword"), TextBox).Text = dt.Rows(0)("ECHOPLUS_USER_PWD").ToString.Trim
        Catch ex As Exception
            Throw
        End Try
    End Sub
#Region "DTL Button Function Handling"
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditDtl.Click
        ChangeMode(MODE.Edit)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("SettingList.aspx", False)
    End Sub

    Protected Sub btnSaveDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDtl.Click
        If ActionMode = MODE.Create Then
            dvSettingMisc.InsertItem(True)
        Else
            dvSettingMisc.UpdateItem(True)
        End If
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDtl.Click
        If ActionMode = MODE.Create Then
            ActionMode = MODE.Normal
            btnBack_Click(sender, Nothing)
        Else
            ChangeMode(MODE.Normal)
        End If
    End Sub
#End Region
#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dvSettingMisc.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dvSettingMisc.DataBind()

            dvFFMA.DataSource = dvCurrentView
            dvFFMA.DataBind()

            dvFFMR.DataSource = dvCurrentView
            dvFFMR.DataBind()

            dvFFMS.DataSource = dvCurrentView
            dvFFMS.DataBind()

            dvEchoplus.DataSource = dvCurrentView
            dvEchoplus.DataBind()

            'Call Paging


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsSetting
        Try

            DT = obj.SettingDtl(SettingID, Session("UserID"))


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region
    Private Sub ChangeMode(ByVal actMode As MODE)
        Select Case actMode
            Case MODE.Create
                dvSettingMisc.ChangeMode(DetailsViewMode.Insert)
                dvFFMA.ChangeMode(DetailsViewMode.Insert)
                dvFFMR.ChangeMode(DetailsViewMode.Insert)
                dvFFMS.ChangeMode(DetailsViewMode.Insert)
                dvEchoplus.ChangeMode(DetailsViewMode.Insert)
                ActionMode = MODE.Create
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
                OnAndLoadSettingDDL()
            Case MODE.Edit
                dvSettingMisc.ChangeMode(DetailsViewMode.Edit)
                dvFFMA.ChangeMode(DetailsViewMode.Edit)
                dvFFMR.ChangeMode(DetailsViewMode.Edit)
                dvFFMS.ChangeMode(DetailsViewMode.Edit)
                dvEchoplus.ChangeMode(DetailsViewMode.Edit)
                ActionMode = MODE.Edit
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
                ddlSetting.Visible = False
            Case Else 'Normal
                dvSettingMisc.ChangeMode(DetailsViewMode.ReadOnly)
                dvFFMA.ChangeMode(DetailsViewMode.ReadOnly)
                dvFFMR.ChangeMode(DetailsViewMode.ReadOnly)
                dvFFMS.ChangeMode(DetailsViewMode.ReadOnly)
                dvEchoplus.ChangeMode(DetailsViewMode.ReadOnly)
                ActionMode = MODE.Normal
                btnEditDtl.Visible = True
                btnSaveDtl.Visible = False
                btnCancelDtl.Visible = False
                ddlSetting.Visible = False
        End Select

        'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit) Then
        '    btnEditDtl.Visible = False
        '    btnCancelDtl.Visible = False
        'End If

        RenewDataBind()
        UpdateDatagrid.Update()
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dvSettingMisc_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvSettingMisc.ItemInserting
        Dim obj As New adm_SuperAdmin.clsSetting
        Dim dt As DataTable
        Dim FFMAServerIP, FFMAServerName, FFMADBName, FFMALinkServer, FFMAUserID, FFMAPAss As String
        Dim FFMRServerIP, FFMRServerName, FFMRDBName, FFMRLinkServer, FFMRUserID, FFMRPAss As String
        Dim FFMSServerIP, FFMSServerName, FFMSDBName, FFMSLinkServer, FFMSUserID, FFMSPAss As String
        Dim EchoServerIP, EchoServerName, EchoDBName, EchoLinkServer, EchoUserID, EchoPAss As String
        Dim PageSize As String

        Try
            PageSize = "20" 'CType(dvSettingMisc.Rows(0).FindControl("txtPageSize"), TextBox).Text.Trim

            FFMAServerIP = CType(dvFFMA.Rows(0).FindControl("txtFFMAServerIP"), TextBox).Text.Trim
            FFMAServerName = CType(dvFFMA.Rows(0).FindControl("txtFFMAServerName"), TextBox).Text.Trim
            FFMADBName = CType(dvFFMA.Rows(0).FindControl("txtFFMADBName"), TextBox).Text.Trim
            FFMALinkServer = CType(dvFFMA.Rows(0).FindControl("txtFFMALinkServer"), TextBox).Text.Trim
            FFMAUserID = CType(dvFFMA.Rows(0).FindControl("txtFFMAUserID"), TextBox).Text.Trim
            FFMAPAss = CType(dvFFMA.Rows(0).FindControl("txtFFMAPassword"), TextBox).Text.Trim

            FFMRServerIP = CType(dvFFMR.Rows(0).FindControl("txtFFMRServerIP"), TextBox).Text.Trim
            FFMRServerName = CType(dvFFMR.Rows(0).FindControl("txtFFMRServerName"), TextBox).Text.Trim
            FFMRDBName = CType(dvFFMR.Rows(0).FindControl("txtFFMRDBName"), TextBox).Text.Trim
            FFMRLinkServer = CType(dvFFMR.Rows(0).FindControl("txtFFMRLinkServer"), TextBox).Text.Trim
            FFMRUserID = CType(dvFFMR.Rows(0).FindControl("txtFFMRUserID"), TextBox).Text.Trim
            FFMRPAss = CType(dvFFMR.Rows(0).FindControl("txtFFMRPassword"), TextBox).Text.Trim

            FFMSServerIP = CType(dvFFMS.Rows(0).FindControl("txtFFMSServerIP"), TextBox).Text.Trim
            FFMSServerName = CType(dvFFMS.Rows(0).FindControl("txtFFMSServerName"), TextBox).Text.Trim
            FFMSDBName = CType(dvFFMS.Rows(0).FindControl("txtFFMSDBName"), TextBox).Text.Trim
            FFMSLinkServer = CType(dvFFMS.Rows(0).FindControl("txtFFMSLinkServer"), TextBox).Text.Trim
            FFMSUserID = CType(dvFFMS.Rows(0).FindControl("txtFFMSUserID"), TextBox).Text.Trim
            FFMSPAss = CType(dvFFMS.Rows(0).FindControl("txtFFMSPassword"), TextBox).Text.Trim

            EchoServerIP = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSServerIP"), TextBox).Text.Trim
            EchoServerName = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSServerName"), TextBox).Text.Trim
            EchoDBName = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSDBName"), TextBox).Text.Trim
            EchoLinkServer = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSLinkServer"), TextBox).Text.Trim
            EchoUserID = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSUserID"), TextBox).Text.Trim
            EchoPAss = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSPassword"), TextBox).Text.Trim

            dt = obj.Create(PageSize, FFMAServerIP, FFMAServerName, FFMADBName, FFMALinkServer, FFMAUserID, FFMAPAss, _
                            FFMRServerIP, FFMRServerName, FFMRDBName, FFMRLinkServer, FFMRUserID, FFMRPAss, _
                            FFMSServerIP, FFMSServerName, FFMSDBName, FFMSLinkServer, FFMSUserID, FFMSPAss, _
                            EchoServerIP, EchoServerName, EchoDBName, EchoLinkServer, EchoUserID, EchoPAss, _
                            Session("UserID"))

            SettingID = dt.Rows(0)("SETTING_ID").ToString
            ChangeMode(MODE.Normal)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dvSettingMisc_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvSettingMisc.ItemUpdating
        Dim obj As New adm_SuperAdmin.clsSetting
        Dim FFMAServerIP, FFMAServerName, FFMADBName, FFMALinkServer, FFMAUserID, FFMAPAss As String
        Dim FFMRServerIP, FFMRServerName, FFMRDBName, FFMRLinkServer, FFMRUserID, FFMRPAss As String
        Dim FFMSServerIP, FFMSServerName, FFMSDBName, FFMSLinkServer, FFMSUserID, FFMSPAss As String
        Dim EchoServerIP, EchoServerName, EchoDBName, EchoLinkServer, EchoUserID, EchoPAss As String
        Dim PageSize As String

        Try
            If Not SettingID Is Nothing Or SettingID <> "" Then
                PageSize = "20" 'CType(dvSettingMisc.Rows(0).FindControl("txtPageSize"), TextBox).Text.Trim

                FFMAServerIP = CType(dvFFMA.Rows(0).FindControl("txtFFMAServerIP"), TextBox).Text.Trim
                FFMAServerName = CType(dvFFMA.Rows(0).FindControl("txtFFMAServerName"), TextBox).Text.Trim
                FFMADBName = CType(dvFFMA.Rows(0).FindControl("txtFFMADBName"), TextBox).Text.Trim
                FFMALinkServer = CType(dvFFMA.Rows(0).FindControl("txtFFMALinkServer"), TextBox).Text.Trim
                FFMAUserID = CType(dvFFMA.Rows(0).FindControl("txtFFMAUserID"), TextBox).Text.Trim
                FFMAPAss = CType(dvFFMA.Rows(0).FindControl("txtFFMAPassword"), TextBox).Text.Trim

                FFMRServerIP = CType(dvFFMR.Rows(0).FindControl("txtFFMRServerIP"), TextBox).Text.Trim
                FFMRServerName = CType(dvFFMR.Rows(0).FindControl("txtFFMRServerName"), TextBox).Text.Trim
                FFMRDBName = CType(dvFFMR.Rows(0).FindControl("txtFFMRDBName"), TextBox).Text.Trim
                FFMRLinkServer = CType(dvFFMR.Rows(0).FindControl("txtFFMRLinkServer"), TextBox).Text.Trim
                FFMRUserID = CType(dvFFMR.Rows(0).FindControl("txtFFMRUserID"), TextBox).Text.Trim
                FFMRPAss = CType(dvFFMR.Rows(0).FindControl("txtFFMRPassword"), TextBox).Text.Trim

                FFMSServerIP = CType(dvFFMS.Rows(0).FindControl("txtFFMSServerIP"), TextBox).Text.Trim
                FFMSServerName = CType(dvFFMS.Rows(0).FindControl("txtFFMSServerName"), TextBox).Text.Trim
                FFMSDBName = CType(dvFFMS.Rows(0).FindControl("txtFFMSDBName"), TextBox).Text.Trim
                FFMSLinkServer = CType(dvFFMS.Rows(0).FindControl("txtFFMSLinkServer"), TextBox).Text.Trim
                FFMSUserID = CType(dvFFMS.Rows(0).FindControl("txtFFMSUserID"), TextBox).Text.Trim
                FFMSPAss = CType(dvFFMS.Rows(0).FindControl("txtFFMSPassword"), TextBox).Text.Trim

                EchoServerIP = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSServerIP"), TextBox).Text.Trim
                EchoServerName = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSServerName"), TextBox).Text.Trim
                EchoDBName = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSDBName"), TextBox).Text.Trim
                EchoLinkServer = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSLinkServer"), TextBox).Text.Trim
                EchoUserID = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSUserID"), TextBox).Text.Trim
                EchoPAss = CType(dvEchoplus.Rows(0).FindControl("txtECHOPLUSPassword"), TextBox).Text.Trim

                obj.Update(SettingID, PageSize, FFMAServerIP, FFMAServerName, FFMADBName, FFMALinkServer, FFMAUserID, FFMAPAss, _
                            FFMRServerIP, FFMRServerName, FFMRDBName, FFMRLinkServer, FFMRUserID, FFMRPAss, _
                            FFMSServerIP, FFMSServerName, FFMSDBName, FFMSLinkServer, FFMSUserID, FFMSPAss, _
                            EchoServerIP, EchoServerName, EchoDBName, EchoLinkServer, EchoUserID, EchoPAss, _
                            Session("UserID"))

                ChangeMode(MODE.Normal)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlSetting_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSetting.SelectedIndexChanged
        Dim obj = New adm_SuperAdmin.clsSetting
        Dim DT As DataTable = Nothing

        Try
            If ddlSetting.SelectedIndex <> 0 Then
                DT = obj.SettingDtl(ddlSetting.SelectedValue, Session("UserID"))

                ReferenceID(DT)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class
