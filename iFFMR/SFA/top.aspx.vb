Imports System.Data
Partial Class top
    Inherits System.Web.UI.Page

    Private dtAR As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblUserName.Text = Session("UserName")
    End Sub

    Private Sub lnkLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLogout1.Click
        Dim strScript As String
        Dim objUser As adm_User.clsUser
        objUser = New adm_User.clsUser

        Try
            With objUser
                .clsProperties.user_id = Session("UserID")
                .AuditLogout(Session("AuditID"))
            End With
        Catch ex As Exception
            objUser = Nothing
        End Try

        Session.Clear()
        Session.Abandon()
        strScript = "self.parent.parent.location='../../logout.aspx';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Logout", strScript, True)
    End Sub

    Protected Sub lnkProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkProfile.Click

        Dim strScript As String
        Dim objcookie As New HttpCookie("menu")
        strScript = "self.parent.fraMain.location='../../../Admin/User/UserProfile.aspx?type=1&user_code=" & Trim(Session("UserCode")) & "';"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "lnkProfile", strScript, True)
        Response.AppendCookie(objcookie)
    End Sub

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function
End Class
