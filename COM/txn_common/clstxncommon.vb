Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Imports System.Web

Public Class clstxncommon
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = CStr(Web.HttpContext.Current.Session("echoplus_conn"))
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clstxnCommon"
        End Get
    End Property

    Public Function GetCustList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strCustName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_GET_CUSTCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetContList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strContName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_GET_CONTCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdList(ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strPrdName As String, _
    ByVal strPrdGrpCode As String, ByVal strPrdGrpName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_GET_PRDCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("PRD_NAME", strPrdName, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_NAME", strPrdGrpName, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrd_PrdGrpList(ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strPrdName As String, _
ByVal strPrdGrpCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_GET_PRD_PRDGRPCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("PRD_NAME", strPrdName, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdGrpList(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_WEB_GET_PRDGRPCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    'Public Function GetPrdGrpList(ByVal strTeamCode As String, ByVal strPrdGrpCode As String, ByVal strPrdGrpName As String, ByVal isDefault As String) As DataTable
    '    Dim objDB As clsDB
    '    Try
    '        objDB = New clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_MST_COMM_PRD_GRP_LIST"
    '            .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
    '            .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
    '            .addItem("PRD_GRP_NAME", strPrdGrpName, clsDB.DataType.DBString)
    '            .addItem("ENQ_IND", isDefault, clsDB.DataType.DBInt)
    '            .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
    '            Return .spRetrieve()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    'Public Function GetTeamList() As DataTable
    '    Dim objDB As clsDB
    '    Try
    '        objDB = New clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_MST_COMM_TEAM_LIST"
    '            .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
    '            Return .spRetrieve()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function


    Public Function GetSRList(ByVal strSalesrepCode As String, ByVal strSalesrepName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_GET_SRCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SALESREP_NAME", strSalesrepName, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustPayerList(ByVal strSalesrepCode As String, ByVal strCustCode As String, _
    ByVal strCustName As String, ByVal strPayerCode As String, ByVal strPayerName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_SFA_GET_SHIPTO_PAYER_CODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("PAYER_CODE", strPayerCode, clsDB.DataType.DBString)
                .addItem("PAYER_NAME", strPayerName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSalesrepName(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_WEB_GET_SALESREPNAME"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTxnDescName(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_WEB_GET_TXN_DESC_NAME"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetFieldConfig(strPrincipalID As String, strFieldControlType As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_FIELD_CONFIG"
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("FIELD_CONTROL_TYPE", strFieldControlType, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
    End Function


    Public Function GetSalesRepPrdGrp(ByVal strSalesrepList As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_WEB_GET_SALESREP_PRD_GRP_CODE_DDL"
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSalesRepPrdCode(ByVal strSalesrepList As String, ByVal strPrdGrpCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_WEB_GET_SALESREP_PRD_CODE_DDL"
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
