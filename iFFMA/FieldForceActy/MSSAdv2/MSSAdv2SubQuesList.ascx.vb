Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdv2SubQuesList
    Inherits System.Web.UI.UserControl

    Public Event ViewCriteria_Click As EventHandler
    Public Event DeleteSubQues_Click As EventHandler
    Public Event SaveSubQues_Click As EventHandler
    Public Event ViewDisabledQues_Click As EventHandler

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then
            If Master_Row_Count > 0 Then
                For Each dr As GridViewRow In dgSubQuesList.Rows
                    Dim btnCriteria As Button = CType(dr.Cells(aryDataItem.IndexOf("CRITERIA_FLAG")).Controls(0), Button)
                    If btnCriteria IsNot Nothing AndAlso btnCriteria.Text = "0" Then
                        btnCriteria.Visible = False
                    Else
                        'If btnCriteria.Text = 2 Then
                        '    btnCriteria.Text = "Set Disabled Ques"
                        'Else
                        'btnCriteria.Text = "Set Criteria"
                        'End If
                        btnCriteria.CssClass = "cls_button"
                    End If
                Next
            End If
        Else
            ' If Session("PRINCIPAL_ID") = 5 Or Session("PRINCIPAL_ID") = 81 Then
            lblMaxSubQues.Visible = False
            '    Else
            '    lblMaxSubQues.Visible = True
            'End If
        End If

        'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If
        '----------------------------------------------------------------------------------------------------
    End Sub

#Region "PROPERTY"
    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Public Sub UpdateHdrInfo()
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2
            DT = clsMSSAdv.MSSUpdateHdrInfo(TeamCode, TitleCode, QuesCode, "")

            If DT.Rows.Count > 0 Then
                lblTeamCode.Text = DT.Rows(0)("TEAM_NAME").ToString + " (" + TeamCode + ")"
                lblTitleCode.Text = DT.Rows(0)("TITLE_NAME").ToString + " (" + TitleCode + ")"
                lblQuesCode.Text = DT.Rows(0)("QUES_NAME").ToString + " (" + QuesCode + ")"
            Else
                lblTeamCode.Text = TeamCode
                lblTitleCode.Text = TitleCode
                lblQuesCode.Text = QuesCode
            End If
            UpdatePnlSubQues.Update()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            DT = clsMSSAdv.GetMSSSubQuesAdvList(TeamCode, TitleCode, QuesCode)
            dgSubQuesList.DataKeyNames = New String() {"SUB_QUES_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgSubQuesList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            UpdateHdrInfo()

            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgSubQuesList.PageIndex = 0
            '    wuc_dgSubQuesPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgSubQuesList
                .DataSource = dvCurrentView
                '.PageSize = intPageSize
                '.AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            btnPreview.Visible = IIf(Master_Row_Count > 0, True, False)

            'If Session("PRINCIPAL_ID") = 5 Or Session("PRINCIPAL_ID") = 81 Then
            btnAdd.Visible = True
            'Else
            'btnAdd.Visible = IIf(Master_Row_Count < 4, True, False)
            'End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Edit) Then
                btnAdd.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlSubQues.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgSubQuesList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubQuesList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgSubQuesList.Columns.Clear()

            'ADD BUTTON EDIT
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgSubQuesList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = "Delete"

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgSubQuesList.Columns.Add(dgBtnColumn) 
                aryDataItem.Add("BTN_DELETE")
                dgBtnColumn = Nothing
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_MSSAdvSubQuesList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.EditCommandColumn
                        If Master_Row_Count > 0 Then
                            Dim dgColumn As New ButtonField

                            dgColumn.HeaderStyle.Width = "5"
                            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                            dgColumn.CommandName = "SELECT"

                            If dtToBind.Rows.Count > 0 Then
                                dgColumn.ButtonType = ButtonType.Button
                                dgColumn.ControlStyle.CssClass = "cls_button"
                                dgColumn.DataTextField = ColumnName
                                dgColumn.Text = "Set Criteria"
                                dgColumn.Visible = True
                            End If
                            dgSubQuesList.Columns.Add(dgColumn)
                            dgColumn = Nothing
                            aryDataItem.Add(ColumnName)
                        End If

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MSSAdvSubQuesList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MSSAdvSubQuesList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_MSSAdvSubQuesList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_MSSAdvSubQuesList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgSubQuesList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubQuesList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgSubQuesList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubQuesList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgSubQuesList.RowEditing
        Try
            Dim strSelectedSubQuesCode As String = sender.datakeys(e.NewEditIndex).item("SUB_QUES_CODE")

            With (wuc_MSSAdv2SubQuesMand)
                .TeamCode = TeamCode
                .TitleCode = TitleCode
                .QuesCode = QuesCode
                .SubQuesCode = strSelectedSubQuesCode
                .LoadDvEditMode()
                .BindDetailsView()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgSubQuesList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgSubQuesList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing
                        If btnDelete IsNot Nothing Then
                            strDeleteMsg = "Are you sure want to delete?"
                            'btnDelete.OnClientClick = "if (confirm('" + strDeleteMsg + "') == false) { window.event.returnValue = false; return false; }"
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"

                            'HL: 20120702 - Due to BDF, display delete button according to accessright----------------------------
                            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Delete) Then
                                btnDelete.Visible = False
                            End If
                            '----------------------------------------------------------------------------------------------------

                            If e.Row.Cells(aryDataItem.IndexOf("ANS_TYPE_NAME")).Text = "&nbsp;" Then
                                btnDelete.Visible = False
                            End If
                        End If

                        Dim btnEdit As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_EDIT")).Controls(0), LinkButton)
                        If btnEdit IsNot Nothing Then
                            If e.Row.Cells(aryDataItem.IndexOf("ANS_TYPE_NAME")).Text = "&nbsp;" Then
                                btnEdit.Visible = False
                            End If
                        End If

                        Dim btnCriteria As Button = CType(e.Row.Cells(aryDataItem.IndexOf("CRITERIA_FLAG")).Controls(0), Button)
                        If btnCriteria IsNot Nothing AndAlso btnCriteria.Text = "0" Then
                            btnCriteria.Visible = False
                        Else
                            If btnCriteria.Text = "2" Then
                                btnCriteria.Text = "Set Disabled Ques"
                            Else
                                btnCriteria.Text = "Set Criteria"
                            End If
                            btnCriteria.CssClass = "cls_button"
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubQuesList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgSubQuesList.RowDeleting
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            Dim strSubQuesCode As String
            strSubQuesCode = IIf(sender.datakeys(e.RowIndex).item("SUB_QUES_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("SUB_QUES_CODE"))

            clsMSSAdv.DeleteMSSSubQuesAdv(TeamCode, TitleCode, QuesCode, strSubQuesCode)

            RenewDataBind()

            RaiseEvent DeleteSubQues_Click(sender, e)
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgSubQuesList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgSubQuesList.SelectedIndexChanged
        Try
            If dgSubQuesList.SelectedRow.Cells(aryDataItem.IndexOf("ANS_TYPE_NAME")).Text.Substring(0, 1) = "4" Then
                RaiseEvent ViewDisabledQues_Click(sender, e)
            Else
                RaiseEvent ViewCriteria_Click(sender, e)
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Public Function GetdgSubQuesList() As GridView
        Dim dgSubQuesListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgSubQuesList.AllowSorting
            Dim blnAllowPaging As Boolean = dgSubQuesList.AllowPaging

            dgSubQuesList.AllowSorting = False
            dgSubQuesList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgSubQuesListToExport = dgSubQuesList

            dgSubQuesList.AllowPaging = blnAllowPaging
            dgSubQuesList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgSubQuesListToExport
    End Function
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            With (wuc_MSSAdv2SubQuesMand)
                .TeamCode = TeamCode
                .TitleCode = TitleCode
                .QuesCode = QuesCode
                .SubQuesCode = ""
                .LoadDvInsertMode()
                .BindDetailsView()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click, wuc_MSSAdv2SubQuesMand.PreviewButton_Click
        'HL: 20120515 - Due to BDF, enhance MSS to unlimited sub ques---------------------------------
        'If Session("PRINCIPAL_ID") = 5 Or Session("PRINCIPAL_ID") = 81 Then
        With (wuc_MSSAdv2QuesPreview2Pop)
            .TeamCode = TeamCode
            .TitleCode = TitleCode
            .QuesCode = QuesCode
            .BindQuesPreview()
        End With
        'Else
        'With (wuc_MSSAdv2QuesPreviewPop)
        '    .TeamCode = TeamCode
        '    .TitleCode = TitleCode
        '    .QuesCode = QuesCode
        '    .BindQuesPreview()
        'End With
        'End If
    End Sub

    Protected Sub btnMSSAdvSubQuesPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MSSAdv2SubQuesMand.SaveButton_Click
        RenewDataBind()
        RaiseEvent SaveSubQues_Click(sender, e)
    End Sub

#End Region

#Region "COMMON FUNCTION"

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_MSSAdvSubQuesList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "SUB_QUES_CODE"
                strFieldName = "Sub Question Code"
            Case "SUB_QUES_NAME"
                strFieldName = "Sub Question Name"
            Case "ANS_TYPE_NAME"
                strFieldName = "Answer Type"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "CRITERIA_FLAG" Then
                FCT = FieldColumntype.EditCommandColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
