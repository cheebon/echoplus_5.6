Option Explicit On

Imports System.Data

Partial Class DFAccessRight
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtAccessRightID.Text = Trim(Request.QueryString("AccessRightID"))

        If Not Page.IsPostBack Then
            RefreshDatabinding()
        End If

     
    End Sub

    Private Sub RefreshDatabinding()
        Try
            GetDFList(txtAccessRightID.Text)

            If dgDFList.Rows.Count > 0 Then
                btnSave.Visible = True
                lblErr.Text = ""
            Else
                btnSave.Visible = False
                lblErr.Text = "There is no data file for this principal."
            End If

            For Each gvr As GridViewRow In dgDFList.Rows
                Dim cb As CheckBox = CType(gvr.FindControl("chkAccessRight"), CheckBox)

                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            Next
        Catch ex As Exception
            ExceptionMsg("DFAccessRight.RefreshDatabinding : " & ex.ToString)
        End Try
    End Sub

    Private Sub GetDFList(ByVal accessrightID As Integer)
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim i As Integer

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                dt = objAccessRightQuery.GetDFList(accessrightID, txtDFName.Text)
            End With

            With dgDFList
                .DataSource = dt
                .DataBind()
            End With

            For i = 0 To dgDFList.Rows.Count - 1
                Dim row As GridViewRow = dgDFList.Rows(i)
                Dim chkBox As CheckBox = row.FindControl("chkAccessRight")

                If dgDFList.DataKeys(i)(1).ToString = "1" Then
                    chkBox.Checked = True
                Else
                    chkBox.Checked = False
                End If
            Next

        Catch ex As Exception
            ExceptionMsg("DFAccessRight.GetDFList : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>window.close()</script>")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg("DFAccessRight.btnSearch_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i, cnt As Integer
        Dim strDfPrincipalID As String
        Dim objDFAccessRight As adm_AccessRight.clsAccessRightDtl

        strDfPrincipalID = ""
        cnt = 0

        Try
            For i = 0 To dgDFList.Rows.Count - 1
                Dim cb As CheckBox = CType(dgDFList.Rows(i).FindControl("chkAccessRight"), CheckBox)

                If cb.Checked Then
                    If cnt > 0 Then
                        strDfPrincipalID = strDfPrincipalID + ","
                    End If
                    strDfPrincipalID = strDfPrincipalID + dgDFList.DataKeys(i)(0).ToString()
                    cnt = cnt + 1
                End If
            Next

            If strDfPrincipalID = "" Then
                strDfPrincipalID = "0"
            End If

            objDFAccessRight = New adm_AccessRight.clsAccessRightDtl
            With objDFAccessRight
                .clsProperties.accessright_id = txtAccessRightID.Text
                .clsProperties.changed_user_id = Session("UserID")
                .UpdateDFAccessRight(strDfPrincipalID)
            End With

        Catch ex As Exception
            ExceptionMsg("DFAccessRight.btnSave_Click : " & ex.ToString)
        Finally
            objDFAccessRight = Nothing
            Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Data File is successfully updated.');window.close()</script>")
        End Try

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
