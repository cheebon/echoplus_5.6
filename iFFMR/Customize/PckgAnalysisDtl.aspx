<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PckgAnalysisDtl.aspx.vb" Inherits="iFFMR_Customize_PckgAnalysisDtl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Package Analysis</title>
    <link rel="stylesheet" href='~/include/DKSH.css' /> 
</head>
 <!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout">
    <form id="frmPckgAnalDtl" runat="server">
    <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
    <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
    <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Always" RenderMode="Inline" >
    <ContentTemplate>
    <div class="PagePadder">
        <fieldset class="" style="width: 98%;">
        <table id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
                <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
                <tr class="BckgroundInsideContentLayout">
                    <td><uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader></td>
                </tr>
                <tr style="height:5px">
                    <td></td>
                </tr>
                <tr class="BckgroundBenealthTitle">
                    <td>
                        <table cellpadding="1" cellspacing="1" width="60%" class="cls_table">
                        <tr>
                            <td style="width:30%">
                                <asp:Label ID="lblSalesrep" runat="server" Text="Sales Representative" CssClass="cls_label"></asp:Label> 
                            </td>
                            <td>:</td>
                            <td><asp:Label ID="lblSalesrepValue" runat="server" Text="" CssClass="cls_label"></asp:Label> </td>                           
                        </tr>
                        <tr>
                            <td style="width:30%">
                                <asp:Label ID="lblPackage" runat="server" Text="Package" CssClass="cls_label"></asp:Label> 
                            </td>
                            <td>:</td>
                            <td><asp:Label ID="lblPackageValue" runat="server" Text="" CssClass="cls_label"></asp:Label> </td>                           
                        </tr>
                        </table>
                        <asp:Button ID="cmdBack" runat="server" Text="Back" cssclass="cls_button" OnClick="cmdBack_Click" /><br />
                        
                        <%--<uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>--%>
                        <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="true"
                        AllowSorting="true" AutoGenerateColumns="false" GridWidth="" FreezeHeader="true" Width="98%" PagerSettings-Visible="false"
                        GridHeight="300px" RowSelectionEnabled="true" EnableViewState="true">                    
                        <FooterStyle CssClass="GridFooter" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAlternate" />
                        <RowStyle CssClass="GridNormal" />
                        </ccGV:clsGridView>
                    
                    </td>
                </tr>
        </table>
        </fieldset>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
