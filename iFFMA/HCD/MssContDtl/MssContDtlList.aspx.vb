Imports System.Data
Partial Class iFFMA_HCD_MssContDtl_MssContDtlList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private _aryDataItemBTC As ArrayList
    Protected Property aryDataItemBTC() As ArrayList
        Get
            If _aryDataItemBTC Is Nothing Then _aryDataItemBTC = ViewState("DataItemBTC")
            If _aryDataItemBTC Is Nothing Then _aryDataItemBTC = New ArrayList
            Return _aryDataItemBTC
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemBTC") = value
        End Set
    End Property

    Private _aryDataItemPDS As ArrayList
    Protected Property aryDataItemPDS() As ArrayList
        Get
            If _aryDataItemPDS Is Nothing Then _aryDataItemPDS = ViewState("DataItemPDS")
            If _aryDataItemPDS Is Nothing Then _aryDataItemPDS = New ArrayList
            Return _aryDataItemPDS
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemPDS") = value
        End Set
    End Property

    Private _aryDataItemSpecialty As ArrayList
    Protected Property aryDataItemSpecialty() As ArrayList
        Get
            If _aryDataItemSpecialty Is Nothing Then _aryDataItemSpecialty = ViewState("DataItemSpecialty")
            If _aryDataItemSpecialty Is Nothing Then _aryDataItemSpecialty = New ArrayList
            Return _aryDataItemSpecialty
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemSpecialty") = value
        End Set
    End Property

    Private _aryDataItemInvestment As ArrayList
    Protected Property aryDataItemInvestment() As ArrayList
        Get
            If _aryDataItemInvestment Is Nothing Then _aryDataItemInvestment = ViewState("DataItemInvestment")
            If _aryDataItemInvestment Is Nothing Then _aryDataItemInvestment = New ArrayList
            Return _aryDataItemInvestment
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemInvestment") = value
        End Set
    End Property

    Private _aryDataItemPotentialInfo As ArrayList
    Protected Property aryDataItemPotentialInfo() As ArrayList
        Get
            If _aryDataItemPotentialInfo Is Nothing Then _aryDataItemPotentialInfo = ViewState("DataItemPotentialInfo")
            If _aryDataItemPotentialInfo Is Nothing Then _aryDataItemPotentialInfo = New ArrayList
            Return _aryDataItemInvestment
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemPotentialInfo") = value
        End Set
    End Property

    Dim licItemFigureCollector As ListItemCollection
    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SpecialityMthly")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_MssContDtlList") = value
        End Set
    End Property
    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_MssContDtlList"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountBTC() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountBTC"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountBTC") = value
        End Set
    End Property

    Private Property Master_Row_CountPDS() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountPDS"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountPDS") = value
        End Set
    End Property

    Private Property Master_Row_CountSpecialty() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSpecialty"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSpecialty") = value
        End Set
    End Property

    Private Property Master_Row_CountInvestment() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountInvestment"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountInvestment") = value
        End Set
    End Property

    Private Property Master_Row_CountPotentialInfo() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountPotentialInfo"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountPotentialInfo") = value
        End Set
    End Property

    Private Property SalesrepCode() As String
        Get
            Return CStr(ViewState("SALESREP_CODE"))
        End Get
        Set(ByVal Value As String)
            ViewState("SALESREP_CODE") = Value
        End Set
    End Property

    Private Property CustCode() As String
        Get
            Return CStr(ViewState("CUST_CODE"))
        End Get
        Set(ByVal Value As String)
            ViewState("CUST_CODE") = Value
        End Set
    End Property

    Private Property ContCode() As String
        Get
            Return CStr(ViewState("CONT_CODE"))
        End Get
        Set(ByVal Value As String)
            ViewState("CONT_CODE") = Value
        End Set
    End Property

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "MssContDtlList"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.MSS_CONT_DTL)
            .DataBind()
            .Visible = True
        End With


        If Not IsPostBack Then
            'Call Panel
            With wuc_pnlStdBar
                .SubModuleID = SubModuleType.MSS_CONT_DTL
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With


            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True
            TabPanel2.Visible = False
        End If

        lblErr.Text = ""
    End Sub

#Region "EVENT HANDLER"

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            RefreshDatabinding()

        End If

    End Sub

    Protected Sub TimerControldtl_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControldtl.Enabled Then
            TimerControldtl.Enabled = False
            RefreshDatabindingBTC()
            RefreshDatabindingPDS()
            RefreshDatabindingSpecialty()
            RefreshDatabindingInvestment()
            RefreshDatabindingPotentialInfo()

        End If

    End Sub

#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(Wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

#Region "dgListMain"
#Region "DGLIST"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        ViewState("strSortExpression") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        'dgList.PageIndex = 0
        'End If

        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_Count = 0
        Else
            Master_Row_Count = dtCurrentTable.Rows.Count
        End If

        PreRenderMode(dtCurrentTable)

        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        dgList.PageSize = intPageSize
        dgList.DataBind()

        'Call Paging
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
        End With

        wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_Update()

        '  End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '  Try
        aryDataItem.Clear()
        dgList_Init(DT)
        'Cal_CustomerHeader()
        'Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()

        'ADD BUTTON EDIT
        If Master_Row_Count > 0 Then
            Dim dgBtnColumn As New CommandField

            dgBtnColumn.HeaderStyle.Width = "50"
            dgBtnColumn.HeaderText = "Details"
            dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

            If dtToBind.Rows.Count > 0 Then
                dgBtnColumn.ButtonType = ButtonType.Link
                dgBtnColumn.ShowEditButton = True
                dgBtnColumn.EditText = "<img src='../../../images/ico_Edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                dgBtnColumn.Visible = True
            End If

            dgList.Columns.Add(dgBtnColumn)
            dgBtnColumn = Nothing
            aryDataItem.Add("BTN_EDIT")
        End If

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MssContDtlList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MssContDtlList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_MssContDtlList.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 15 Then dgList.GridHeight = Nothing
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagrid.Update()
        updatepage.update()
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer




        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Dim strSalesrepCode As String = IIf(IsDBNull(sender.datakeys(e.NewEditIndex).item("SALESREP_CODE")), "", sender.datakeys(e.NewEditIndex).item("SALESREP_CODE"))
        Dim strCustCode As String = IIf(IsDBNull(sender.datakeys(e.NewEditIndex).item("CUST_CODE")), "", sender.datakeys(e.NewEditIndex).item("CUST_CODE"))
        Dim strContCode As String = IIf(IsDBNull(sender.datakeys(e.NewEditIndex).item("CONT_CODE")), "", sender.datakeys(e.NewEditIndex).item("CONT_CODE"))

        SalesrepCode() = strSalesrepCode
        CustCode() = strCustCode
        ContCode() = strContCode


        TabPanel2.Visible = True
        tcResult.ActiveTabIndex = 1
        UpdatePage.Update()
        TimerControldtl.Enabled = True
        TimerControl1.Enabled = True
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        '  Try
        Dim dt As Data.DataTable = GetRecList()
        dt.Rows.Add(dt.NewRow)
        ViewState("dtCurrentView") = dt
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsMssCont As New mst_Contact.clsMssCont

        Dim strSpecialityName As String, strPositionCode As String, strDepartmentCode As String, _
         strHospRankCode As String, strClassification As String, strCustName As String, strContName As String

        strSpecialityName = Request.QueryString("SPECIALITY")
        strPositionCode = Request.QueryString("POSITION")
        strDepartmentCode = Request.QueryString("DEPARTMENT")
        strHospRankCode = Request.QueryString("HOSP_RANK_CODE")
        strClassification = Request.QueryString("CLASS")
        strCustName = Request.QueryString("CUST_NAME")
        strContName = Request.QueryString("CONT_NAME")


        DT = clsMssCont.GetMssContDtlList(strSpecialityName, strPositionCode, strDepartmentCode, strHospRankCode, strClassification, strCustName, strContName)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region

#End Region

#Region "dgListBTC"
#Region "DGLIST"
    Public Sub RenewDataBindBTC()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabindingBTC()
    End Sub

    Private Sub RefreshDatabindingBTC()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionBTC"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecListBTC()
        ViewState("strSortExpressionBTC") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        'dgList.PageIndex = 0
        'End If
        PreRenderModeBTC(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_CountBTC = 0
        Else
            Master_Row_CountBTC = dtCurrentTable.Rows.Count
        End If
        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgListBTC.DataSource = dvCurrentView
        dgListBTC.PageSize = intPageSize
        dgListBTC.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_UpdateBTC()

        '  End Try
    End Sub

    Private Sub PreRenderModeBTC(ByRef DT As DataTable)
        '  Try
        aryDataItemBTC.Clear()
        dgList_InitBTC(DT)
        'Cal_CustomerHeader()
        'Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_InitBTC(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgListBTC.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MssContDtlList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MssContDtlList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_MssContDtlList.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgListBTC.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemBTC.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgListBTC.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemBTC.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_UpdateBTC()
        If dgListBTC.Rows.Count < 15 Then dgListBTC.GridHeight = Nothing
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagridDtl.Update()

    End Sub

    Protected Sub dgList_RowDataBoundBTC(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListBTC.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer

        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowCreatedBTC(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListBTC.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgListBTC 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgListBTC_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListBTC.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionBTC")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpressionBTC") = strSortExpression
        RefreshDatabindingBTC()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBindBTC()
        RefreshDatabindingBTC()
    End Sub

    Private Sub BindDefaultBTC()
        '  Try
        Dim dt As Data.DataTable = GetRecListBTC()
        dt.Rows.Add(dt.NewRow)
        'ViewState("dtCurrentView") = dt
        RefreshDatabindingBTC()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecListBTC() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsMssCont As New mst_Contact.clsMssCont

        DT = clsMssCont.GetMssContBTCList(SalesrepCode(), CustCode(), ContCode())

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region
#End Region

#Region "dgListPDS"
#Region "DGLIST"
    Public Sub RenewDataBindPDS()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabindingPDS()
    End Sub

    Private Sub RefreshDatabindingPDS()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionPDS"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecListPDS()
        ViewState("strSortExpressionPDS") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        'dgList.PageIndex = 0
        'End If
        PreRenderModePDS(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_CountPDS = 0
        Else
            Master_Row_CountPDS = dtCurrentTable.Rows.Count
        End If
        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgListPDS.DataSource = dvCurrentView
        dgListPDS.PageSize = intPageSize
        dgListPDS.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_UpdatePDS()

        '  End Try
    End Sub

    Private Sub PreRenderModePDS(ByRef DT As DataTable)
        '  Try
        aryDataItemPDS.Clear()
        dgList_InitPDS(DT)
        'Cal_CustomerHeader()
        'Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_InitPDS(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgListPDS.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MssContDtlList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MssContDtlList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_MssContDtlList.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgListPDS.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemPDS.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgListPDS.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemPDS.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_UpdatePDS()
        If dgListPDS.Rows.Count < 15 Then dgListPDS.GridHeight = Nothing
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagridDtl.Update()
    End Sub

    Protected Sub dgList_RowDataBoundPDS(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListPDS.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer




        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowCreatedPDS(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListPDS.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgListPDS_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListPDS.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionPDS")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpressionPDS") = strSortExpression
        RefreshDatabindingPDS()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBindPDS()
        RefreshDatabindingPDS()
    End Sub

    Private Sub BindDefaultPDS()
        '  Try
        Dim dt As Data.DataTable = GetRecListPDS()
        dt.Rows.Add(dt.NewRow)
        'ViewState("dtCurrentView") = dt
        RefreshDatabindingPDS()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecListPDS() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsMssCont As New mst_Contact.clsMssCont


        DT = clsMssCont.GetMssContPDSList(SalesrepCode(), CustCode(), ContCode())

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region
#End Region

#Region "dgListSpecialty"
#Region "DGLIST"
    Public Sub RenewDataBindSpecialty()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabindingSpecialty()
    End Sub

    Private Sub RefreshDatabindingSpecialty()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionSpecialty"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecListSpecialty()
        ViewState("strSortExpressionSpecialty") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        'dgList.PageIndex = 0
        'End If
        PreRenderModeSpecialty(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_CountSpecialty = 0
        Else
            Master_Row_CountSpecialty = dtCurrentTable.Rows.Count
        End If
        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgListSpecialty.DataSource = dvCurrentView
        dgListSpecialty.PageSize = intPageSize
        dgListSpecialty.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_UpdateSpecialty()

        '  End Try
    End Sub

    Private Sub PreRenderModeSpecialty(ByRef DT As DataTable)
        '  Try
        aryDataItemSpecialty.Clear()
        dgList_InitSpecialty(DT)
        'Cal_CustomerHeader()
        'Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_InitSpecialty(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgListSpecialty.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MssContDtlList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MssContDtlList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_MssContDtlList.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgListSpecialty.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemSpecialty.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgListSpecialty.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemSpecialty.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_UpdateSpecialty()
        If dgListSpecialty.Rows.Count < 15 Then dgListSpecialty.GridHeight = Nothing
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagridDtl.Update()
    End Sub

    Protected Sub dgList_RowDataBoundSpecialty(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListSpecialty.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer




        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowCreatedSpecialty(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListSpecialty.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgListSpecialty_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListSpecialty.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionSpecialty")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpressionSpecialty") = strSortExpression
        RefreshDatabindingSpecialty()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBindSpecialty()
        RefreshDatabindingSpecialty()
    End Sub

    Private Sub BindDefaultSpecialty()
        '  Try
        Dim dt As Data.DataTable = GetRecListSpecialty()
        dt.Rows.Add(dt.NewRow)
        'ViewState("dtCurrentView") = dt
        RefreshDatabindingSpecialty()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecListSpecialty() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsMssCont As New mst_Contact.clsMssCont


        DT = clsMssCont.GetMssContSpecialtyList(SalesrepCode(), CustCode(), ContCode())

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region
#End Region

#Region "dgListdgListInvestment"
#Region "DGLIST"
    Public Sub RenewDataBindInvestment()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabindingInvestment()
    End Sub

    Private Sub RefreshDatabindingInvestment()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionInvestment"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecListInvestment()
        ViewState("strSortExpressionInvestment") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        'dgList.PageIndex = 0
        'End If
        PreRenderModeInvestment(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_CountInvestment = 0
        Else
            Master_Row_CountInvestment = dtCurrentTable.Rows.Count
        End If
        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgListInvestment.DataSource = dvCurrentView
        dgListInvestment.PageSize = intPageSize
        dgListInvestment.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_UpdateInvestment()

        '  End Try
    End Sub

    Private Sub PreRenderModeInvestment(ByRef DT As DataTable)
        '  Try
        aryDataItemInvestment.Clear()
        dgList_InitInvestment(DT)
        'Cal_CustomerHeader()
        'Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_InitInvestment(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgListInvestment.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MssContDtlList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MssContDtlList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_MssContDtlList.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgListInvestment.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemInvestment.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgListInvestment.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemInvestment.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_UpdateInvestment()
        If dgListInvestment.Rows.Count < 15 Then dgListInvestment.GridHeight = Nothing
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagridDtl.Update()
    End Sub

    Protected Sub dgList_RowDataBoundInvestment(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListInvestment.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer




        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowCreatedInvestment(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListInvestment.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgListInvestment_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListInvestment.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionInvestment")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpressionInvestment") = strSortExpression
        RefreshDatabindingInvestment()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBindInvestment()
        RefreshDatabindingInvestment()
    End Sub

    Private Sub BindDefaultInvestment()
        '  Try
        Dim dt As Data.DataTable = GetRecListInvestment()
        dt.Rows.Add(dt.NewRow)
        'ViewState("dtCurrentView") = dt
        RefreshDatabindingInvestment()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecListInvestment() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsMssCont As New mst_Contact.clsMssCont

        DT = clsMssCont.GetMssContInvestmentList(SalesrepCode(), CustCode(), ContCode())

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region
#End Region

#Region "dgListPotentialInfo"
#Region "DGLIST"
    Public Sub RenewDataBindPotentialInfo()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabindingPotentialInfo()
    End Sub

    Private Sub RefreshDatabindingPotentialInfo()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionPotentialInfo"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecListPotentialInfo()
        ViewState("strSortExpressionPotentialInfo") = strSortExpression
        'ViewState("dtCurrentView") = dtCurrentTable
        'dgList.PageIndex = 0
        'End If
        PreRenderModePotentialInfo(dtCurrentTable)
        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Master_Row_CountPotentialInfo = 0
        Else
            Master_Row_CountPotentialInfo = dtCurrentTable.Rows.Count
        End If
        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgListPotentialInfo.DataSource = dvCurrentView
        dgListPotentialInfo.PageSize = intPageSize
        dgListPotentialInfo.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_UpdatePotentialInfo()

        '  End Try
    End Sub

    Private Sub PreRenderModePotentialInfo(ByRef DT As DataTable)
        '  Try
        aryDataItemPotentialInfo.Clear()
        dgList_InitPotentialInfo(DT)
        'Cal_CustomerHeader()
        'Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_InitPotentialInfo(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgListPotentialInfo.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MssContDtlList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_MssContDtlList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_MssContDtlList.GetMonthValue(ColumnName)

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "" '"DESC_CODE"

                    'Dim strSalesrepCode As String, strTeamCode As String
                    'strTeamCode = Request.QueryString("Team_Code")
                    'strSalesrepCode = Request.QueryString("Salesrep_Code")

                    Dim strUrlFormatString As String
                    strUrlFormatString = ""
                    'strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateDtl.aspx?MONTH=" & intmonth & "&TEAM_CODE=" & strTeamCode & "&SALESREP_CODE=" & strSalesrepCode & "&DESC_CODE={0}'")


                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgListPotentialInfo.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemPotentialInfo.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MssContDtlList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MssContDtlList.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MssContDtlList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgListPotentialInfo.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItemPotentialInfo.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_UpdatePotentialInfo()
        If dgListPotentialInfo.Rows.Count < 15 Then dgListPotentialInfo.GridHeight = Nothing
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        'ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagridDtl.Update()
    End Sub

    Protected Sub dgList_RowDataBoundPotentialInfo(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListPotentialInfo.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer




        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_RowCreatedPotentialInfoByVal(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgListPotentialInfo.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgListPotentialInfo_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListPotentialInfo.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionPotentialInfo")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpressionPotentialInfo") = strSortExpression
        RefreshDatabindingPotentialInfo()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBindPotentialInfo()
        RefreshDatabindingPotentialInfo()
    End Sub

    Private Sub BindDefaultPotentialInfo()
        '  Try
        Dim dt As Data.DataTable = GetRecListPotentialInfo()
        dt.Rows.Add(dt.NewRow)
        'ViewState("dtCurrentView") = dt
        RefreshDatabindingPotentialInfo()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecListPotentialInfo() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsMssCont As New mst_Contact.clsMssCont

        DT = clsMssCont.GetMssContPotentialInfoList(SalesrepCode(), CustCode(), ContCode())

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_pnlStdBar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            Dim blnAllowSortingBTC As Boolean = dgListBTC.AllowSorting
            Dim blnAllowPagingBTC As Boolean = dgListBTC.AllowPaging
            Dim blnAllowSortingPDS As Boolean = dgListPDS.AllowSorting
            Dim blnAllowPagingPDS As Boolean = dgListPDS.AllowPaging
            Dim blnAllowSortingSpecialty As Boolean = dgListSpecialty.AllowSorting
            Dim blnAllowPagingSpecialty As Boolean = dgListSpecialty.AllowPaging
            Dim blnAllowSortingInvestment As Boolean = dgListInvestment.AllowSorting
            Dim blnAllowPagingInvestment As Boolean = dgListInvestment.AllowPaging
            Dim blnAllowSortingPotentialInfo As Boolean = dgListPotentialInfo.AllowSorting
            Dim blnAllowPagingPotentialInfo As Boolean = dgListPotentialInfo.AllowPaging

            If tcResult.ActiveTabIndex = 0 Then
                dgList.AllowSorting = False
                dgList.AllowPaging = False
                RefreshDatabinding()

                Dim strb As New StringBuilder

                Dim lblInfo As New Label
                strb.Append("<B>Specialty: </B>" & Request.QueryString("SPECIALITY") & "<BR/>")
                strb.Append("<B>Position: </B>" & Request.QueryString("POSITION") & "<BR/>")
                strb.Append("<B>Department: </B>" & Request.QueryString("DEPARTMENT") & "<BR/>")
                strb.Append("<B>Hosp Ranking: </B>" & Request.QueryString("HOSP_RANK_CODE") & "<BR/>")
                strb.Append("<B>Classification: </B>" & Request.QueryString("CLASS") & "<BR/>")
                strb.Append("<B>Cust Name: </B>" & Request.QueryString("CUST_NAME") & "<BR/>")
                strb.Append("<B>Cont Name: </B>" & Request.QueryString("CONT_NAME") & "<BR/>")
                strb.Append("<BR/>")

                lblInfo.Text = strb.ToString
                pnlList.Controls.Add(lblInfo)
                pnlList.Controls.Add(dgList)

                wuc_pnlStdBar.ExportToFile(pnlList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

                dgList.AllowPaging = blnAllowPaging
                dgList.AllowSorting = blnAllowSorting
                RefreshDatabinding()
            Else
                dgListBTC.AllowSorting = False
                dgListBTC.AllowPaging = False
                dgListPDS.AllowSorting = False
                dgListPDS.AllowPaging = False
                dgListSpecialty.AllowSorting = False
                dgListSpecialty.AllowPaging = False
                dgListInvestment.AllowSorting = False
                dgListInvestment.AllowPaging = False
                dgListPotentialInfo.AllowSorting = False
                dgListPotentialInfo.AllowPaging = False

                RefreshDatabindingBTC()
                RefreshDatabindingPDS()
                RefreshDatabindingSpecialty()
                RefreshDatabindingInvestment()
                RefreshDatabindingPotentialInfo()

                Dim strb As New StringBuilder

                Dim lblInfo As New Label
                strb.Append("<B>Specialty: </B>" & Request.QueryString("SPECIALITY") & "<BR/>")
                strb.Append("<B>Position: </B>" & Request.QueryString("POSITION") & "<BR/>")
                strb.Append("<B>Department: </B>" & Request.QueryString("DEPARTMENT") & "<BR/>")
                strb.Append("<B>Hosp Ranking: </B>" & Request.QueryString("HOSP_RANK_CODE") & "<BR/>")
                strb.Append("<B>Classification: </B>" & Request.QueryString("CLASS") & "<BR/>")
                strb.Append("<B>Cust Name: </B>" & Request.QueryString("CUST_NAME") & "<BR/>")
                strb.Append("<B>Cont Name: </B>" & Request.QueryString("CONT_NAME") & "<BR/>")
                strb.Append("<BR/>")

                lblInfo.Text = strb.ToString
                pnlDetails.Controls.Add(lblInfo)
                pnlDetails.Controls.Add(dgListBTC)
                pnlDetails.Controls.Add(dgListPDS)
                pnlDetails.Controls.Add(dgListSpecialty)
                pnlDetails.Controls.Add(dgListInvestment)
                pnlDetails.Controls.Add(dgListPotentialInfo)

                wuc_pnlStdBar.ExportToFile(pnlDetails, wuc_lblHeader.Title.ToString.Replace(" ", "_"))


                dgListBTC.AllowPaging = blnAllowPagingBTC
                dgListBTC.AllowSorting = blnAllowSortingBTC
                dgListPDS.AllowPaging = blnAllowPagingPDS
                dgListPDS.AllowSorting = blnAllowSortingPDS
                dgListSpecialty.AllowPaging = blnAllowPagingSpecialty
                dgListSpecialty.AllowSorting = blnAllowSortingSpecialty
                dgListInvestment.AllowPaging = blnAllowPagingInvestment
                dgListInvestment.AllowSorting = blnAllowSortingInvestment
                dgListPotentialInfo.AllowPaging = blnAllowPagingPotentialInfo
                dgListPotentialInfo.AllowSorting = blnAllowSortingPotentialInfo

                RefreshDatabindingBTC()
                RefreshDatabindingPDS()
                RefreshDatabindingSpecialty()
                RefreshDatabindingInvestment()
                RefreshDatabindingPotentialInfo()
            End If


        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region
End Class


Public Class CF_MssContDtlList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "ASST_NAME"
                strFieldName = "Assistant Name"
            Case "PRIORITY_NAME"
                strFieldName = "Priority"
            Case "VISIT_FREQ"
                strFieldName = "Visit freq"
            Case "PERSONAL_INTEREST"
                strFieldName = "Personal Interest"
            Case "MED_INTEREST"
                strFieldName = "Med Interest"
            Case "MKTG_PREF"
                strFieldName = "Mktg. Perf."
            Case "XFIELD_1"
                strFieldName = "XField 1"
            Case "XFIELD_2"
                strFieldName = "XField 2"
            Case "NOTES"
                strFieldName = "Notes"
            Case "HOSP_ABBV"
                strFieldName = "Hosp. Abbrv"
            Case "DEPARTMENT_CODE_1"
                strFieldName = "Dept."
            Case "LOC"
                strFieldName = "Loc."
            Case "TEL_NO_1"
                strFieldName = "Tel No 1"
            Case "TEL_NO_2"
                strFieldName = "Tel No 2"
            Case "FAX_NO"
                strFieldName = "Fax. No."
            Case "PATIENT_COUNT"
                strFieldName = "Patient Count"
            Case "BED_COUNT"
                strFieldName = "Bed Count"
            Case "PATIENT_CAP"
                strFieldName = "Patient Cap"
            Case "PRACTICE_SIZE"
                strFieldName = "Practise Size"
            Case "TITLE"
                strFieldName = "Title"
            Case "LAST_NAME"
                strFieldName = "Last Name"
            Case "FIRST_NAME"
                strFieldName = "First Name"
            Case "CHRISTIAN_NAME"
                strFieldName = "Christian Name"
            Case "GENDER"
                strFieldName = "Gender"
            Case "BIRTH_DATE"
                strFieldName = "Birth Date"
            Case "SPECIALITY_NAME"
                strFieldName = "Specialty"
            Case "MARITAL_STATUS"
                strFieldName = "Marital Status"
            Case "GRAD_DATE"
                strFieldName = "Grad. Date"
            Case "EMAIL"
                strFieldName = "Email"
            Case "TEL_NO_1_1"
                strFieldName = "Tel No. 1"
            Case "TEL_NO_2_1"
                strFieldName = "Tel No. 2"
            Case "MED_SCHOOL"
                strFieldName = "Med. School"
            Case "POSITION_CODE"
                strFieldName = "Position Code"
            Case "PAC_CODE"
                strFieldName = "Pac Code"
            Case "HOSP_RANK_CODE"
                strFieldName = "Hosp. Rank. Code"
            Case "DEPARTMENT_CODE"
                strFieldName = "Department"
            Case "CALL_DAY"
                strFieldName = "Call Day"
            Case "CALL_TIME"
                strFieldName = "Call Time"
            Case "LOCATION"
                strFieldName = "Location"
            Case "PRD_SPECIALTY_NAME"
                strFieldName = "Prd. Specialty"
            Case "PRIORITY_CODE"
                strFieldName = "Priority Code"
            Case "SPECIALTY"
                strFieldName = "Specialty"
            Case "INVESTMENT_CODE"
                strFieldName = "Investment Code"
            Case "INVESTMENT_AMT"
                strFieldName = "Investment Amt."
            Case "COSTCENTER_CODE"
                strFieldName = "Cost Center Code"
            Case "POTENTIAL_CODE"
                strFieldName = "Potential Code"
            Case "SEGMENT1"
                strFieldName = "Segment 1"
            Case "SEGMENT2"
                strFieldName = "Segment 2"
            Case "SEGMENT3"
                strFieldName = "Segment 3"
            Case "SEGMENT4"
                strFieldName = "Segment 4"
            Case "SEGMENT5"
                strFieldName = "Segment 5"
            Case "LAST_UPDATE"
                strFieldName = "Last Updated"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)

        End Select


        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        ' Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        'Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        'strColumnName = strColumnName.ToUpper
        'Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        'If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
        '   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
        '    FCT = FieldColumntype.InvisibleColumn
        'Else
        '    FCT = FieldColumntype.BoundColumn
        'End If

        If (strColumnName Like "DESC_CODE") OrElse (strColumnName Like "TOTAL_CLOSING") Then
            FCT = FieldColumntype.InvisibleColumn
        ElseIf strColumnName = "JAN" OrElse strColumnName = "FEB" OrElse strColumnName = "MAR" _
                     OrElse strColumnName = "APR" OrElse strColumnName = "MAY" OrElse strColumnName = "JUN" _
                     OrElse strColumnName = "JUL" OrElse strColumnName = "AUG" OrElse strColumnName = "SEP" _
                     OrElse strColumnName = "OCT" OrElse strColumnName = "NOV" OrElse strColumnName = "DEC" Then

            If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SRCALLRATEDTL, SubModuleAction.View) Then
                FCT = FieldColumntype.HyperlinkColumn
            End If

        End If

        Return FCT
        'Catch ex As Exception

        ' End Try
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        strColumnName = strColumnName.Replace("_CALLS", "")
        strColumnName = strColumnName.Replace("_WD", "")
        Select Case strColumnName
            Case "JAN"
                Return 1
            Case "FEB"
                Return 2
            Case "MAR"
                Return 3
            Case "APR"
                Return 4
            Case "MAY"
                Return 5
            Case "JUN"
                Return 6
            Case "JUL"
                Return 7
            Case "AUG"
                Return 8
            Case "SEP"
                Return 9
            Case "OCT"
                Return 10
            Case "NOV"
                Return 11
            Case "DEC"
                Return 12
            Case Else
                Return 1
        End Select
        'Catch ex As Exception
        'End Try
    End Function

    Public Shared Function GetDayValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        Dim intValue As Integer = 0

        strColumnName = strColumnName.Replace("_CALL", "").Replace("_DEF", "").Replace("DAY_", "")
        'strColumnName = strColumnName.Replace("S", "")
        If Not String.IsNullOrEmpty(strColumnName) AndAlso IsNumeric(strColumnName) Then intValue = CInt(strColumnName)
        Return intValue
        ' Catch ex As Exception
        ' End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        ' Try

        Select Case strNewName
            Case "CALL_PER_DAY", "CLOSING_RATE"
                strStringFormat = "{0:0.0}"
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
                strStringFormat = "{0:0.00}"
            Case Else
                strStringFormat = ""
        End Select

        If strColumnName.ToUpper Like "MTD_CALL_*" Or strColumnName.ToUpper Like "YTD_CALL*" Or strColumnName.ToUpper Like "MTD_COVERAGE_*" Then
            strStringFormat = "{0:0.#}"
        ElseIf strColumnName.ToUpper Like "*_PERCENT" Then
            strStringFormat = "{0:0.00}"
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "*TIME" OrElse strColumnName Like "*DAY" OrElse strColumnName Like "LAST_UPDATE" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf strColumnName Like "*_NAME" Then
                .HorizontalAlign = HorizontalAlign.Left
            ElseIf strColumnName Like "MTD_CALL_*" Or strColumnName Like "YTD_CALL*" Or strColumnName Like "MTD_COVERAGE_*" _
            Or strColumnName Like "*AMT" Then
                .HorizontalAlign = HorizontalAlign.Right
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

    Public Shared Function GetMonthName(ByVal iMonth As Integer) As String
        Dim strMonthName As String = ""

        ' Try
        Select Case iMonth
            Case 1
                strMonthName = "JAN"
            Case 2
                strMonthName = "FEB"
            Case 3
                strMonthName = "MAR"
            Case 4
                strMonthName = "APR"
            Case 5
                strMonthName = "MAY"
            Case 6
                strMonthName = "JUN"
            Case 7
                strMonthName = "JUL"
            Case 8
                strMonthName = "AUG"
            Case 9
                strMonthName = "SEP"
            Case 10
                strMonthName = "OCT"
            Case 11
                strMonthName = "NOV"
            Case 12
                strMonthName = "DEC"
            Case Else
                strMonthName = ""
        End Select
        'Catch ex As Exception
        'End Try

        Return strMonthName

    End Function
End Class
