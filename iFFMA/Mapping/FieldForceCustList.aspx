<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FieldForceCustList.aspx.vb" Inherits="iFFMA_Mapping_FieldForceCustList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldForceCustPDSList" Src="FieldForceCustPDSList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldForceCustBTCList" Src="FieldForceCustBTCList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldForceCustPop" Src="FieldForceCustPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldForceCustRoutePop" Src="FieldForceCustRoutePop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Field Force Customer List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesrepCustList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                               
                                            <%--<span style="float:left; width:100%;">--%>   
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="CUST_CODE, CONT_CODE">
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                </tr>
                                                                <tr><td colspan="6">&nbsp;</td></tr>
                                                                
                                                                <tr style="height:15px" align="left">
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Salesrep Code</span></td>
                                                                    <td><span class="cls_label_header">:</span></td>
                                                                    <td><asp:Label ID="lblSalesrepCode" runat="server" CssClass="cls_label" /></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Contact Code</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblContCode" runat="server" CssClass="cls_label"></asp:Label></td> 
                                                                </tr>
                                                                <tr style="height:15px" align="left">
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Salesrep Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td>
                                                                    <td><asp:Label ID="lblSalesrepName" runat="server" CssClass="cls_label" /></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Contact Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblContName" runat="server" CssClass="cls_label"></asp:Label></td> 
                                                                </tr>
                                                                <tr style="height:15px" align="left">
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Customer Code</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblCustCode" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Specialty Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblSpecialtyName" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Customer Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblCustName" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Department Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblDepartmentName" runat="server" CssClass="cls_label"></asp:Label></td>  
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Assistant Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblAssistantName" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Priority Name</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblPriorityName" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                    <td><span style="float:left; padding-left:10px;" class="cls_label_header">Personal Interest</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblPersonalInterest" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                     <td><span style="float:left; padding-left:10px;" class="cls_label_header">Medical Interest</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblMedicalInterest" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                     <td><span style="float:left; padding-left:10px;" class="cls_label_header">Marketing Preference</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblMarketingPreference" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                     <td><span style="float:left; padding-left:10px;" class="cls_label_header">XField1</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblXField1" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                     <td><span style="float:left; padding-left:10px;" class="cls_label_header">XField2</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblXField2" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                 <tr style="height:15px" align="left">
                                                                    <td style="width:15%;"></td>
                                                                    <td style="width:3%;"></td>
                                                                    <td style="width:32%;"></td>
                                                                     <td><span style="float:left; padding-left:10px;" class="cls_label_header">Notes</span></td>
                                                                    <td><span class="cls_label_header">:</span></td> 
                                                                    <td><asp:Label ID="lblnotes" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                 </tr>
                                                                
                                                                <tr><td colspan="3">&nbsp;</td></tr>
                                                            </table>
                                                            
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_FieldForceCustPDSList ID="wuc_FieldForceCustPDSList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_FieldForceCustBTCList ID="wuc_FieldForceCustBTCList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel4" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_FieldForceCustPDSList ID="wuc_FieldForceContPDSList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel5" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_FieldForceCustBTCList ID="wuc_FieldForceContBTCList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        
        <customToolkit:wuc_FieldForceCustPop ID="wuc_FieldForceCustPop" Title="Field Force Customer Assignment" runat="server" />
        <customToolkit:wuc_FieldForceCustRoutePop ID="wuc_FieldForceCustRoutePop" Title="Route Planning" runat="server" />
    </form>
</body>
</html>
