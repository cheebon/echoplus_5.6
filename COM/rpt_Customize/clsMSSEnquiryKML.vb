﻿Imports System.Xml
Imports System.IO
Imports System.ComponentModel
Imports cor_DB
Imports rpt_Customize
Imports System.Data

Public Class clsMSSEnquiryKML
    Private _defaultStyleA As String = "styleA"
    Public Property StyleA() As String
        Get
            Return _defaultStyleA
        End Get
        Set(ByVal value As String)
            _defaultStyleA = value
        End Set
    End Property
    Private _defaultStyleB As String = "styleB"
    Public Property StyleB() As String
        Get
            Return _defaultStyleB
        End Get
        Set(ByVal value As String)
            _defaultStyleB = value
        End Set
    End Property
    Private _defaultStyleC As String = "styleC"
    Public Property StyleC() As String
        Get
            Return _defaultStyleC
        End Get
        Set(ByVal value As String)
            _defaultStyleC = value
        End Set
    End Property
    Private _defaultStyleOth As String = "styleOth"
    Public Property StyleOth() As String
        Get
            Return _defaultStyleOth
        End Get
        Set(ByVal value As String)
            _defaultStyleOth = value
        End Set
    End Property

    Dim objKML As clsKMLExporter

#Region "Style"
    Private Sub addStyleA()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleA)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
    Private Sub addStyleB()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleB)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
    Private Sub addStyleC()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleC)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
    Private Sub addStyleOth()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = objKML.addNewXmlElement("Style", objKML.xmlElement)
        _xmlStyle.SetAttribute("id", StyleOth)
        'IconStyle
        _xmlIconStyle = objKML.addNewXmlElement("IconStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = objKML.addNewXmlElement("Icon", _xmlIconStyle)
        objKML.addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = objKML.addNewXmlElement("LabelStyle", _xmlStyle)
        objKML.addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = objKML.addNewXmlElement("BalloonStyle", _xmlStyle)
        objKML.addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
#End Region
#Region "Main Information Tag"
    Public Sub addMainInforTag(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, ByVal intNetValue As Integer, _
                                       ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strAnswer As String, ByVal strSubQuesType As String, _
                                       ByVal strTeamName As String, ByVal strSalesrepName As String, ByVal strPrdName As String, ByVal strCustTypeName As String, _
                                       ByVal strMSSTitleName As String, ByVal strMSSQuesName As String, ByVal strMSSSubQuesName As String, ByVal strMSSAnswerName As String)
        Try
            addSearchInfo(strCustName, strAddress, strDistrict, _
                                        strCustGrp, strCustClass, _
                                        strCustType, strTitleCode, _
                                        strQuesCode, strSubQuesCode, _
                                        strMssStartDate, strMssEndDate, intNetValue, _
                                        strDate, _
                                        strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
                                        strNoSkuStart, strNoSkuEnd, _
                                        strTeamCode, strSalesrepCode, strPrdCode, strAnswer, strSubQuesType, _
                                        strTeamName, strSalesrepName, strPrdName, strCustTypeName, _
                                        strMSSTitleName, strMSSQuesName, strMSSSubQuesName, strMSSAnswerName)
            addStyleA()
            addStyleB()
            addStyleC()
            addStyleOth()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub AddStyle()
        Try
            addStyleA()
            addStyleB()
            addStyleC()
            addStyleOth()
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
        'Define style

    End Sub
    Private Sub addSearchInfo(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, ByVal intNetValue As Integer, _
                                       ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strAnswer As String, ByVal strSubQuesType As String, _
                                       ByVal strTeamName As String, ByVal strSalesrepName As String, ByVal strPrdName As String, ByVal strCustTypeName As String, _
                                       ByVal strMSSTitleName As String, ByVal strMSSQuesName As String, ByVal strMSSSubQuesName As String, ByVal strMSSAnswerName As String)
        objKML.addNewXmlElementWithValue("name", "Customer Market Survey Searching", objKML.xmlElement)
        objKML.addNewXmlElementWithValue("open", "1", objKML.xmlElement)
        objKML.addNewXmlElementWithValue("description", String.Format("Customer: {0}", strCustName) & vbCrLf & _
                                  String.Format("Address: {0}", strAddress) & vbCrLf & _
                                  String.Format("District: {0}", strDistrict) & vbCrLf & _
                                  String.Format("Cust Grp: {0}", strCustGrp) & vbCrLf & _
                                  String.Format("Cust Class: {0}", strCustClass) & vbCrLf & _
                                  String.Format("Cust Type: {0}", strCustTypeName) & vbCrLf & _
                                  String.Format("Mtd Sales Range: {0} - {1}", strMtdStart, strMtdEnd) & vbCrLf & _
                                  String.Format("Ytd Sales Range: {0} - {1}", strYtdStart, strYtdEnd) & vbCrLf & _
                                  String.Format("No SKU Range: {0} - {1}", strNoSkuStart, strNoSkuEnd) & vbCrLf & _
                                  String.Format("Team: {0}", strTeamName) & vbCrLf & _
                                  String.Format("Salesrep: {0}", strSalesrepName) & vbCrLf & _
                                  String.Format("SKU: {0}", strPrdName) & vbCrLf & _
                                  String.Format("MSS Txn Date Range: {0} - {1}", strMssStartDate, strMssEndDate) & vbCrLf & _
                                  String.Format("MSS Title: {0}", strMSSTitleName) & vbCrLf & _
                                  String.Format("MSS Question: {0}", strMSSQuesName) & vbCrLf & _
                                  String.Format("MSS Sub Question: {0}", strMSSSubQuesName) & vbCrLf & _
                                  String.Format("MSS Answer: {0}", strMSSAnswerName) & vbCrLf, objKML.xmlElement)
    End Sub

#End Region
#Region "TrackPoints"
    Public Sub addTrackPoints(ByRef drRows As DataRowCollection)
        Dim _xmlGroup, _xmlPlaceMark As XmlElement
        _xmlGroup = objKML.addNewXmlElement("Folder", objKML.xmlElement)
        objKML.addNewXmlElementWithValue("name", "TrackPoint", _xmlGroup)
        objKML.addNewXmlElementWithValue("open", "1", _xmlGroup)
        For Each drRow As DataRow In drRows
            If isValidPoint(drRow) Then
                _xmlPlaceMark = objKML.addNewXmlElement("Placemark", _xmlGroup)
                objKML.addPlaceMark(GetValue(Of String)(drRow("CUST_NAME"), String.Empty), TrackPointDescriptions(drRow), TrackPointStyle(drRow), TrackPointCoordinates(drRow), _
                         _xmlPlaceMark, drRow)
            End If
        Next
    End Sub
    Private Function TrackPointDescriptions(ByVal dr As DataRow) As String
        Dim sbDescription As String

        'sbDescription = "<![CDATA["
        sbDescription = ""
        sbDescription += String.Format("<b>Contact</b>: {0}<br />", GetValue(Of String)(dr("CONT_NAME"), String.Empty))
        sbDescription += String.Format("<b>Address</b>: {0}<br />", GetValue(Of String)(dr("ADDRESS"), String.Empty))
        sbDescription += String.Format("<b>District</b>: {0}<br />", GetValue(Of String)(dr("DISTRICT"), String.Empty))
        sbDescription += String.Format("<b>Cust Grp</b>: {0}<br />", GetValue(Of String)(dr("CUST_GRP_NAME"), String.Empty))
        sbDescription += String.Format("<b>Cust Class</b>: {0}<br />", GetValue(Of String)(dr("CLASS"), String.Empty))
        sbDescription += String.Format("<b>Cust Type</b>: {0}<br />", GetValue(Of String)(dr("CUST_TYPE"), String.Empty))
        sbDescription += String.Format("<b>Mtd Sales</b>: {0}<br />", Convert.ToDecimal(dr("MTD_SALES")).ToString("N2"))
        sbDescription += String.Format("<b>Ytd Sales</b>: {0}<br />", Convert.ToDecimal(dr("YTD_SALES")).ToString("N2"))
        sbDescription += String.Format("<b>No of SKU</b>: {0}<br />", Convert.ToDecimal(dr("NO_SKU")).ToString("N0"))
        sbDescription += String.Format("<b>Credit Limit</b>: {0}<br />", Convert.ToDecimal(dr("CREDITLIMIT")).ToString("N2"))
        sbDescription += String.Format("<b>Outstanding Balance</b>: {0}<br />", Convert.ToDecimal(dr("OUTBAL")).ToString("N2"))
        sbDescription += String.Format("<b>Last MSS Date</b>: {0}<br />", GetValue(Of String)(dr("TXN_DATE"), String.Empty))
        sbDescription += String.Format("<b>Last Title</b>: {0}<br />", GetValue(Of String)(dr("TITLE_NAME"), String.Empty))
        'sbDescription += "]]>"

        Return sbDescription
    End Function
    Private Function TrackPointCoordinates(ByVal dr As DataRow) As String
        Dim sbDescription As String

        sbDescription = ""
        sbDescription += String.Format("{0}, {1}", dr("LONGITUDE").ToString, dr("LATITUDE").ToString)
        sbDescription += ""

        Return sbDescription
    End Function
    Private Function TrackPointStyle(ByVal dr As DataRow) As String
        Select Case GetValue(Of String)(dr("CLASS"), String.Empty).ToUpper
            Case "A"
                Return StyleA
            Case "B"
                Return StyleB
            Case "C"
                Return StyleC
            Case Else
                Return StyleOth
        End Select
    End Function
    Private Function isValidPoint(ByVal dr As DataRow) As Boolean
        If IsNumeric(dr("LATITUDE").ToString) And IsNumeric(dr("LONGITUDE").ToString) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

    Public Function generateKMLFile(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, ByVal intNetValue As Integer, _
                                       ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strAnswer As String, ByVal strSubQuesType As String, _
                                       ByVal strTeamName As String, ByVal strSalesrepName As String, ByVal strPrdName As String, ByVal strCustTypeName As String, _
                                       ByVal strMSSTitleName As String, ByVal strMSSQuesName As String, ByVal strMSSSubQuesName As String, ByVal strMSSAnswerName As String) As String

        Dim dtRpt As DataTable '= getCallByCustExport(strSalesrepCode, strCallDate)
        Dim objMSS As New clsMssEnqQuery
        Dim strFilePath As String = String.Format("{0}{1}{2}_{3}_{4}.kml", System.AppDomain.CurrentDomain.BaseDirectory.ToString, "Documents\iFFMR\KML\", "MSSEnquiry", DateTime.Now.ToString("yyyy-MM-dd"), Guid.NewGuid.ToString.Substring(1, 5))
        Try
            objKML = New clsKMLExporter

            dtRpt = objMSS.ExportCustTitle(strCustName, strAddress, strDistrict, _
                                        strCustGrp, strCustClass, _
                                        strCustType, strTitleCode, _
                                        strQuesCode, strSubQuesCode, _
                                        strMssStartDate, strMssEndDate, intNetValue, _
                                        strDate, _
                                        strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
                                        strNoSkuStart, strNoSkuEnd, _
                                        strTeamCode, strSalesrepCode, strPrdCode, strAnswer, strSubQuesType)
            addMainInforTag(strCustName, strAddress, strDistrict, _
                                        strCustGrp, strCustClass, _
                                        strCustType, strTitleCode, _
                                        strQuesCode, strSubQuesCode, _
                                        strMssStartDate, strMssEndDate, intNetValue, _
                                        strDate, _
                                        strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, _
                                        strNoSkuStart, strNoSkuEnd, _
                                        strTeamCode, strSalesrepCode, strPrdCode, strAnswer, strSubQuesType, _
                                        strTeamName, strSalesrepName, strPrdName, strCustTypeName, _
                                        strMSSTitleName, strMSSQuesName, strMSSSubQuesName, strMSSAnswerName)
            addTrackPoints(dtRpt.Rows)
            SaveKML(strFilePath)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
        Return strFilePath
    End Function

#Region "Core Functions"
    Public Sub SaveKML(ByVal strDest As String)
        If IO.Directory.Exists(IO.Directory.GetParent(strDest).FullName) = False Then IO.Directory.CreateDirectory(IO.Directory.GetParent(strDest).FullName)
        Try
            objKML.xmlDoc.Save(strDest)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
    End Sub
    Public Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
#End Region
#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            Exit Sub
        End Sub
    End Class

    
#End Region
End Class
