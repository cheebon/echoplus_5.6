Imports Microsoft.Web.UI.WebControls
Imports Microsoft.Web.UI

    Public Class FFMS_WebOrder_CustomerDtl
        Inherits System.Web.UI.Page

        Dim tabname As String
        Public sIFrameSrc As String

        Public ReadOnly Property PageName() As String
            Get
                Return "CustomerDtl"
            End Get
        End Property

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Session("customercode") = Request.QueryString("custcode")
        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()
            Dim strCustName As String
            Dim obj As New txn_Customer.clsDept
            Dim strCustCode As String
            Try
            strCustCode = Trim(Request.QueryString("custcode"))
                strCustName = obj.getContactName(strCustCode)
            'Put user code to initialize the page here

                'Call Header
                With WucHeader
                .Title = "Customer Details : " & strCustName & " (" & strCustCode & ")"
                    .DataBind()
                    .Visible = True
            End With

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

            'Call Panel
            If Not IsPostBack Then

            End If
   
            Catch ex As Exception
                ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
            Finally
                obj = Nothing
            End Try

        End Sub
    'Private Sub custtab_SelectedIndexChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles custtab.SelectedIndexChange
    '    Try
    '        Dim strSubModule As String = 18
    '        Select Case custtab.SelectedIndex
    '            Case 0
    '                tabname = "General Info"
    '            sIFrameSrc = "../CustomerGeneralInfo.aspx"
    '                strSubModule = 16
    '            Case 1
    '                tabname = "Contact"
    '            sIFrameSrc = "../CustomerContactGeneralInfo.aspx"
    '                strSubModule = 17
    '            Case 2
    '                tabname = "Delivery Address"
    '            sIFrameSrc = "../CustomerShipto.aspx"
    '                strSubModule = 18
    '            Case 3
    '                tabname = "Open Item"
    '            sIFrameSrc = "../CustomerOpenItem.aspx"
    '                strSubModule = 48
    '            Case 4
    '                tabname = "AR Aging"
    '            sIFrameSrc = "../CustomerARAging.aspx"
    '                strSubModule = 49
    '            Case 5
    '                tabname = "Mth Sales Hist"
    '            sIFrameSrc = "../CustomerMthSalesHist.aspx"
    '                strSubModule = 50
    '            Case 6
    '                tabname = "Invoice Hist"
    '            sIFrameSrc = "../CustomerInvoiceHist.aspx"
    '                strSubModule = 51
    '            Case 7
    '                tabname = "SFMS Hist"
    '            sIFrameSrc = "../CustomerSFMSHist.aspx"
    '                strSubModule = 52
    '            Case 8
    '                tabname = "Dealer Rec"
    '            sIFrameSrc = "../CustomerDealerRec.aspx"
    '                strSubModule = 53
    '            Case 9
    '                tabname = "Order Status"
    '            sIFrameSrc = "../CustomerOrderStatus.aspx"
    '                strSubModule = 54
    '            Case 10
    '                tabname = "Cust Disc"
    '            sIFrameSrc = "../CustomerCustDisc.aspx"
    '                strSubModule = 55
    '            Case 11
    '                tabname = "Promo Camp"
    '            sIFrameSrc = "../CustomerPromoCamp.aspx"
    '                strSubModule = 56
    '            Case 12
    '                tabname = "Plan History"
    '            sIFrameSrc = "../CustomerPlanListDetail.aspx"
    '                strSubModule = 57

    '            Case Else
    '                sIFrameSrc = ""
    '        End Select
    '    'With WucctrlPanel
    '    '    .SubModuleID = strSubModule
    '    '    .DataBind()
    '    '    .Visible = True
    '    'End With
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".custtab_SelectedIndexChange : " & ex.ToString)
    '    Finally

    '    End Try

    'End Sub

        Private Sub ExceptionMsg(ByVal strMsg As String)
            Try
                lblErr.Text = ""
                lblErr.Text = strMsg

                'Call error log class
                Dim objLog As cor_Log.clsLog
                objLog = New cor_Log.clsLog
                With objLog
                    .clsProperties.LogTypeID = 1
                    .clsProperties.DateLogIn = Now
                    .clsProperties.DateLogOut = Now
                    .clsProperties.SeverityID = 4
                    .clsProperties.LogMsg = strMsg
                    .Log()
                End With
                objLog = Nothing

            Catch ex As Exception

            End Try
        End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function
    
End Class


