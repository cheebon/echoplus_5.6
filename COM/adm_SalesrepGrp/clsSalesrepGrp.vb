﻿Public Class clsSalesrepGrp
    Private strPrincipal As String
    Private strSalesrepGrpName As String
    Private strSalesrepGrpDesc As String
    Private strCriteriaValue As String
    Private strSalesrepGrpID As Integer

    Public Property SalesrepGrpID() As Integer
        Get
            Return strSalesrepGrpID
        End Get
        Set(ByVal value As Integer)
            strSalesrepGrpID = value
        End Set
    End Property

    Public Property Principal() As String
        Get
            Return strPrincipal
        End Get
        Set(ByVal value As String)
            strPrincipal = value
        End Set
    End Property

    Public Property SalesrepGroupName() As String
        Get
            Return strSalesrepGrpName
        End Get
        Set(ByVal value As String)
            strSalesrepGrpName = value
        End Set
    End Property

    Public Property SalesrepGroupDesc() As String
        Get
            Return strSalesrepGrpDesc
        End Get
        Set(ByVal value As String)
            strSalesrepGrpDesc = value
        End Set
    End Property

    Public Property CriteriaValue() As String
        Get
            Return strCriteriaValue
        End Get
        Set(ByVal value As String)
            strCriteriaValue = value
        End Set
    End Property
End Class
