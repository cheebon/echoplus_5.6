'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	01/10/2007
'	Purpose	    :	Class to query all the data related to master SAP
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSAPQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Public clsProperties As New mst_SAP.clsProperties.clsSAPQuery
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))
    Private strFFMAConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))
    'Private strSAPID As String

    Sub New()
        'Me.strSAPID = strSAPID
    End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSAPQuery"
        End Get
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSAPList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSAPList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETSAPLISTBYTEAM"
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("search_type", clsProperties.search_type, cor_DB.clsDB.DataType.DBString)
                .addItem("team_code", clsProperties.team_code, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value", clsProperties.search_value, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value_1", clsProperties.search_value_1, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function ListSAP() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAPLIST"
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("team_code", clsProperties.team_code, cor_DB.clsDB.DataType.DBString)
                .addItem("error_reason", clsProperties.error_reason, cor_DB.clsDB.DataType.DBString)
                .addItem("last_update_start", clsProperties.last_update_start, cor_DB.clsDB.DataType.DBString)
                .addItem("last_update_end", clsProperties.last_update_end, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_date_start", clsProperties.txn_date_start, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_date_end", clsProperties.txn_date_end, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_status", clsProperties.txn_status, cor_DB.clsDB.DataType.DBString)
                .addItem("sales_office", clsProperties.sales_office, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_code", clsProperties.salesrep_code, cor_DB.clsDB.DataType.DBString)
                .addItem("status", clsProperties.status, cor_DB.clsDB.DataType.DBString)
                .addItem("saving_no", clsProperties.saving_no, cor_DB.clsDB.DataType.DBString)
                .addItem("note", clsProperties.note, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.ListSAP" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSAPStatusList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSAPStatusList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETSAPSTATUSLIST"
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPStatusList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSAPTxnStatusList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSAPTxnStatusList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETSAPTXNSTATUSLIST"
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPTxnStatusList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSAPTeamList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSAPTeamList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETSAPTEAMLIST"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPTeamList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetSAPType() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_SAP_TYPE_LIST"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPType" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSAPDtlList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSAPDtl() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETSAPDTL"
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("so_no", clsProperties.saving_no, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSAPDtlList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSAPDtlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETSAPDTLLIST"
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("so_no", clsProperties.saving_no, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPDtlList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function SAPGetDtlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_SAP_GETDTLLIST"
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.SAPGetDtlList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTxnDtlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_GETTXNDTLLIST"
                .addItem("doc_type", clsProperties.doc_type, cor_DB.clsDB.DataType.DBString)
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.SPP_MST_GETTXNDTLLIST" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetRetReasonCode() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_MST_RET_REASON_CODE"
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetRetReasonCode" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class