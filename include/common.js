﻿function GetDropdownValue(ddlName) { if (ddlName != "" && document.getElementById(ddlName) && document.getElementById(ddlName).selectedIndex >= 0) { return document.getElementById(ddlName).options[document.getElementById(ddlName).selectedIndex].value; } else return ""; }
function GetDropdownText(ddlName) { if (ddlName != "" && document.getElementById(ddlName) && document.getElementById(ddlName).selectedIndex >= 0) { return document.getElementById(ddlName).options[document.getElementById(ddlName).selectedIndex].innerHTML; } else return ""; }
function trim(stringToTrim) { return stringToTrim.replace(/^\s+|\s+$/g, ""); }
function IIf(Statement, TrueStm, FalseStm) { if (Statement) { return TrueStm; } else { return FalseStm; } }
function getTodayDate() { var today = new Date(); var dd = today.getDate(); var mm = today.getMonth() + 1; var yyyy = today.getFullYear(); if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } return (yyyy + '-' + mm + '-' + dd); }
function Page_ClientValidate(validationGroup) { Page_InvalidControlToBeFocused = null; if (typeof (Page_Validators) == "undefined") { return true; } var i; for (i = 0; i < Page_Validators.length; i++) { ValidatorValidate(Page_Validators[i], validationGroup, null); } ValidatorUpdateIsValid(); ValidationSummaryOnSubmit(validationGroup); Page_BlockSubmit = !Page_IsValid; return Page_IsValid; }
function addDropDownValue(ddlCtrl, text, value) { var opt = document.createElement("OPTION"); opt.text = text; opt.value = value; ddlCtrl.options.add(opt); }
function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if ((charCode < 48 || charCode > 57) && charCode != 8) return false; return true;
}
function padleft(val, ch, num) { var re = new RegExp(".{" + num + "}$"); var pad = ""; if (!ch) ch = " "; do { pad += ch; } while (pad.length < num); return re.exec(pad + val); }
function validateNumeric(textbox) { var text = textbox.value; if (!IsNumeric(text) || text == '') { textbox.value = "0"; } }
function IsNumeric(sText) { var ValidChars = "0123456789"; var IsNumber = true; var Char; for (i = 0; i < sText.length && IsNumber == true; i++) { Char = sText.charAt(i); if (ValidChars.indexOf(Char) == -1) { IsNumber = false; } } return IsNumber; }
function validateDecimal(textbox) { var text = textbox.value; if (!IsDecimal(text)) { textbox.value = ""; } }
function IsDecimal(sText) { if (sText.match(/^\d*\.?\d*$/)) { return true; } else { return false; } }
function pressEnter(evt) { var charCode = (evt.which) ? evt.which : event.keyCode; if (charCode == 13) { event.keyCode = 9; return event.keyCode; } }
function checkLength(text) { var str = text.value; var re1 = new RegExp("^[\u4E00-\uFA29]*$"); var re2 = new RegExp("^[\uE7C7-\uE7F3]*$"); var maxlength = new Number(text.getAttribute("MaxChar")); var txtLength = 0; for (i = 0; i < str.length; i++) { var c = text.value.substring(i, i + 1); if (!(re1.test(c) && (!re2.test(c)))) { txtLength = txtLength + 1; } else { txtLength = txtLength + 2; maxlength = maxlength - 1; } } if (txtLength > maxlength) { text.value = text.value.substring(0, maxlength); } }
function Export() { __doPostBack('imgExport', 'Export'); theForm.__EVENTTARGET.value = ""; theForm.__EVENTARGUMENT.value = ""; }
function bypassEnter(evt) { var charCode = (evt.which) ? evt.which : event.keyCode; if (charCode == 13) { return false; } }
function dbinput_Alpha(element) { element.value = element.value.replace(/[^A-Z]/gi, ""); }
function dbinput_Numeric(element) { element.value = element.value.replace(/[^0-9]/gi, ""); }
function dbinput_ALphaNumeric(element, event) { if (!(event.keyCode == 37 || event.keyCode == 39)) { element.value = element.value.replace(/[^0-9A-Z]/gi, ""); } }
function dbinput_FilterValidate(element, event) { if (!(event.keyCode == 37 || event.keyCode == 39)) { element.value = element.value.replace(/[^0-9A-Z'%* ]/gi, ""); } }