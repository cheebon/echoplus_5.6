﻿Option Explicit On

Imports System.Data
Imports adm_SuperAdmin

Partial Class Admin_Root_CountryList
    Inherits System.Web.UI.Page
    Private intPageSize As Integer

    Public ReadOnly Property PageName() As String
        Get
            Return "Country List"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Country"

            If Not IsPostBack Then
                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrenttable Is Nothing Then
                'With wuc_ctrlpanel
                '    .UpdateSalesEnquirySearchDetails()
                '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
                'End With
                dtCurrenttable = GetRecList()

                ViewState("strSortExpression") = Nothing
                ViewState("dtCurrentView") = dtCurrenttable
                'dgList.PageIndex = 0
            End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            'dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
          

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsCountry
        Try
            
            DT = obj.CountryList


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            Response.Redirect("CountryDtl.aspx?action=CREATE", False)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim obj As New adm_SuperAdmin.clsCountry

        Try
            Dim index As Integer = 0
            Dim row As GridViewRow
            Dim strCountryID As String

            If Integer.TryParse(e.CommandArgument, index) = True Then
                row = IIf(index >= 0 AndAlso index <= dgList.Rows.Count, dgList.Rows(index), Nothing)
                strCountryID = Trim(dgList.DataKeys(row.RowIndex).Value)

                Select Case e.CommandName
                    Case "Delete"
                        With obj
                            .Delete(strCountryID, Session("UserID"))
                        End With
                        Response.Redirect("CountryList.aspx", False)

                    Case "Details"
                        Response.Redirect("CountryDtl.aspx?COUNTRY_ID=" & strCountryID & "&action=VIEW", False)

                    Case "Modify"
                        Response.Redirect("CountryDtl.aspx?COUNTRY_ID=" & strCountryID & "&action=EDIT", False)

                End Select
            End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Dim imgEdit, imgDelete As ImageButton

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim btnDelete As ImageButton = CType(e.Row.Cells(0).Controls(0), ImageButton)
                If btnDelete IsNot Nothing Then
                    btnDelete.OnClientClick = "if (confirm('Are you sure want to delete?') == false) { window.event.returnValue = false; return false; }"
                End If

                'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.PRINCIPAL, SubModuleAction.Edit) Then
                '    imgEdit = CType(e.Row.Cells(10).Controls(0), ImageButton)
                '    imgEdit.Visible = False
                'End If

                'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.PRINCIPAL, SubModuleAction.Delete) Then
                '    imgDelete = CType(e.Row.Cells(9).Controls(0), ImageButton)
                '    imgDelete.Visible = False
                'End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression Like ("* DESC") Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
End Class
