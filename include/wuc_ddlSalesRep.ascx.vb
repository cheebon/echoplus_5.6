'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	SalesRep User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlSalesRep
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Private strSalesTeamCode As String
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlSalesRepID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlSalesRepID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlSalesRepID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlSalesRepID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlSalesRepID.SelectedValue = "0"
    '        Else
    '            ddlSalesRepID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlSalesRepID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return ddlSalesRepID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlSalesRepID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlSalesRepID.SelectedValue = "0"
            Else
                ddlSalesRepID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlSalesRepID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlSalesRepID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlSalesRepID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlSalesRepID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlSalesRepID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlSalesRepID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlSalesRepID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlSalesRepID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlSalesRepID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvSalesRepID.Visible 'cfvSalesRepID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvSalesRepID.Enabled = Value
            cfvSalesRepID.Visible = Value
        End Set
    End Property

    Public Property SalesTeamCode() As String
        Get
            Return strSalesTeamCode
        End Get
        Set(ByVal Value As String)
            strSalesTeamCode = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            'If ddlSalesRepID.Items.Count <= 0 Then
            'Get SalesRep List
            objCommonQuery = New cor_Common.clsCommonQuery
            With objCommonQuery
                .clsProperties.SalesTeamCode = strSalesTeamCode
                .clsProperties.PrincipalID = Session("PRINCIPAL_ID")
                dt = .GetSalesRepList
            End With
            objCommonQuery = Nothing

            ddlSalesRepID.Items.Clear()
            ddlSalesRepID.Items.Add(New ListItem("Select", 0))
            If dt.Rows.Count > 0 Then
                For Each drRow In dt.Rows
                    ddlSalesRepID.Items.Add(New ListItem(Trim(drRow("salesrep_name")), Trim(drRow("salesrep_id")) & "@" & Trim(drRow("salesrep_code"))))
                Next
            End If
            'End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlSalesRep.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


