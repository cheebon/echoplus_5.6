﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HistoricalTracking.aspx.vb" Inherits="HistoricalTracking" EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="../../../include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="../../../include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="../../../include/menu/wuc_Menu.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Historical Tracking</title>
    
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="../../../Styles/jquery-ui-1.8.14.custom.css" rel="stylesheet" />

    <style>
        .map_container {
            position: relative;
            width: 100%;
            padding-bottom: 56.25%; /* Ratio 16:9 ( 100%/16*9 = 56.25% ) */
        }

            .map_container .map_canvas {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                margin: 0;
                padding: 0;
            }
    </style>

    <script src="../../../Scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmAREnq" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" CombineScripts="True" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td valign="top" align="left">
                <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTreeMenuControl" runat="server">
                            <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err" ></asp:Label>
                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                <asp:UpdatePanel runat="server" ID="UpdatePage" UpdateMode="Conditional">
                                    <ContentTemplate>
                                     <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                            <%--<customToolkit:wuc_lblHeader ID="lblHeader" runat="server" Title="" />--%>
                                            <div class="ContainerDiv">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <asp:UpdatePanel ID="updPnlSearch" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="col-md-4" style="margin-top: 10px;">
                                                                    <div class="ContainerDiv">
                                                                        <div class="row">
                                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                                <asp:button  ID="btnSearchSalesRep" runat="server" UseSubmitBehavior="False" AutoPostBack="False" Text="Search by SalesRep"
                                                                                    CssClass="cls_button" ValidationGroup="SRCustStatus" OnClientClick="btnSeachbySalesRepClick();return false;">
                                                                                </asp:button>
<%--                                                                                <asp:button  ID="btnSearchPrd" runat="server" UseSubmitBehavior="False" AutoPostBack="False" Text="Search by Product"
                                                                                    CssClass="cls_button" ValidationGroup="SRCustStatus" OnClientClick="btnSeachbyPrdClick();return false;">
                                                                                </asp:button>--%>
                                                                            </div>
                                                                        </div>
                                 
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12" style="margin-top: 10px;">
                                                                            <asp:HiddenField ID="hfSalesrepList" runat="server"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hfLongitude" runat="server"></asp:HiddenField>
                                                                            <asp:HiddenField ID="hfLatitude" runat="server"></asp:HiddenField>
                                                                        </div>
                                                                    </div>
                                                            <%--    <div class="ContainerDiv">
                                                                        <div class="row">
                                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                                <asp:image ID="ASPxImage2" runat="server" ShowLoadingImage="True" ImageUrl="~/images/markerred.png">
                                                                                </asp:image>
                                                                                <asp:label ID="ASPxLabel7" runat="server" Text="Visited outlet" >
                                                                                </asp:label>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>
                                                                </div>
                                                                <div class="col-md-8" style="margin-top: 10px;">
                                                                    <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo" type="text/javascript"></script>
                                                                    <div class="map_container">
                                                                        <div id="map_canvas" class="map_canvas">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td>
       
                </td>
        </tr>
    </table>
    </form>

</body>
    <script src="HistoricalTracking.js?V=1"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {

            PositionViewDetail();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                PositionViewDetail();
            }
            debugger;

            var todayDate = new Date(),
                month = '' + (todayDate.getMonth() + 1),
                day = '' + todayDate.getDate(),
                year = todayDate.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            var todayDate = [year, month, day].join('-');

            //$("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0].value = todayDate;
            //$("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0].value = todayDate;

            //console.log($("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0].value);


            InitializeMap();

            hideTxtEnd();

        });

        //$("#wuc_ctrlpanel_Wuc_txtCalendarRange2_txtDateEnd").on('input', function () {
        //    console.log("its owrking");/* This will be fired every time, when textbox's value changes. */
        //});

        //$("#wuc_ctrlpanel_Wuc_txtCalendarRange2_txtDateStart").on('input', function () {
        //    console.log("its owrking");/* This will be fired every time, when textbox's value changes. */
        //});

        function hideTxtEnd() {
            $("#wuc_ctrlpanel_Wuc_txtCalendarRange2_Label21").hide()
            $("#wuc_ctrlpanel_Wuc_txtCalendarRange2_txtDateEnd").hide()
            $("#wuc_ctrlpanel_Wuc_txtCalendarRange2_imgDateEnd").hide()
        }

        function ShowViewDetail() {
            var value = $('#hfViewDetailStatus').val()
            if (value == "") { $('#ViewDetail').hide(); }
            else {
                if (value == "Show") { $('#ViewDetail').show(); }
                else { $('#ViewDetail').hide(); };
            };
        }
        function SetViewDetail(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfViewDetailStatus').val('Hide'); }
            else {
                if (SetVis == 'ForceShow') { $('#hfViewDetailStatus').val('Show'); }
                else {
                    var value = $('#hfViewDetailStatus').val();
                    if (value == 'Hide') { $('#hfViewDetailStatus').val('Show'); }
                    else { $('#hfViewDetailStatus').val('Hide'); }
                }
            }
        }
        function PositionViewDetail() {
            $('#ViewDetail').center();
        }
        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", "15px");
            this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
            return this;
        }

        var strCustCode = "Cust code";
        var strCustName = "Cust name";
        var strLocation = "Location";

    </script>
</html>
