﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_pnlSFMSExtraction.ascx.vb" Inherits="wuc_pnlSFMSExtraction" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtCalendarRange" TagPrefix="customToolkit" %>
<link href="~/include/DKSH.css" rel="stylesheet" />

<table id="tblCtrlPnlGen" cellspacing="0" cellpadding="0" width="98%" border="0" style="float:left; padding-left:10px;">
    <tr align="center">
        <td>
             <asp:Button ID="btnCollapse" Text="Click to Expand or Collapse" runat="server" CssClass="cls_button" width="100%"/>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdatePnlExtraction" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlSFMSExtraction" runat="server">
                        <table cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tr>
                                <td style="width:45%;"></td>
                                <td style="width:10%;"></td>
                                <td style="width:45%;"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                    <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                    <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                    <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                    <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblCat" class="cls_label_header">SFMS Category</span><br>
                                    <asp:ListBox ID="lsbCat" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddCat" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveCat" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllCat" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllCat" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedCat" class="cls_label_header">Selected SFMS Category</span><br>
                                    <asp:ListBox ID="lsbSelectedCat" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblSubCat" class="cls_label_header">SFMS Sub Category</span><br>
                                    <asp:ListBox ID="lsbSubCat" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddSubCat" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveSubCat" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllSubCat" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllSubCat" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedSubCat" class="cls_label_header">Selected SFMS Sub Category</span><br>
                                    <asp:ListBox ID="lsbSelectedSubCat" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblExCat" class="cls_label_header">SFMS Extra Category</span><br>
                                    <asp:ListBox ID="lsbExCat" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddExCat" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveExCat" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllExCat" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllExCat" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedExCat" class="cls_label_header">Selected SFMS Extra Category</span><br>
                                    <asp:ListBox ID="lsbSelectedExCat" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr valign="top">
                                <td valign="top" align="left" style="white-space: nowrap" colspan="3">
                                    <customToolkit:wuc_txtCalendarRange 
                                        ID="WucSFMSExtract_Date" 
                                        runat="server"  
                                        RequiredValidation="true"
                                        RequiredValidationGroup="Search" 
                                        DateFormatString="yyyy-MM-dd" 
                                        CompareDateRangeValidation="true"
                                   />
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                    <asp:Button ID="btnRefresh" CssClass="cls_button" runat="server" Text="Refresh" ValidationGroup="Search"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlSFMSExtraction" runat="server"
                        CollapseControlID="btnCollapse"
                        ExpandControlID="btnCollapse" 
                        TargetControlID="pnlSFMSExtraction" 
                        CollapsedSize="0"
                        Collapsed="false" 
                        ExpandDirection="Vertical" 
                        SuppressPostBack="true">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
