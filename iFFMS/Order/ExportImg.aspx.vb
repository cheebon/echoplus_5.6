﻿Imports System.IO
Partial Class iFFMS_Order_ExportImg
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Download()
    End Sub



    Sub Download()
        Dim strFileName = Request.QueryString("strFileName")
        Dim strPath As String = System.Web.HttpContext.Current.Server.MapPath("~/") + "Documents\Root\" + Context.Session("CURRENT_PRINCIPAL_CODE") + "\TRA\"
        'Dim strPath As String = ConfigurationManager.AppSettings("UploadPath") + Context.Session("CURRENT_PRINCIPAL_CODE") + "\\TRA\\"

        Dim strUploadPath As String = strPath + strFileName
        Try
            Dim myfile As FileInfo = New FileInfo(strUploadPath)
            If myfile.Exists Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ClearContent()
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=""" + strFileName + "")
                HttpContext.Current.Response.AddHeader("Content-Length", myfile.Length.ToString())
                HttpContext.Current.Response.ContentType = "application/octet-stream"
                HttpContext.Current.Response.WriteFile(strUploadPath)
                HttpContext.Current.Response.End()
            End If
        Catch ex As Exception
        Finally
            'File.Delete(strUploadPath)
        End Try
    End Sub

End Class
