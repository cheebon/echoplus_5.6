<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ContactAssignList.ascx.vb" Inherits="iFFMA_Contact_ContactAssignList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ContactAcctPop" Src="ContactAcctPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ContactRepPop" Src="ContactRepPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlContactAssign" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
          <%--  <tr>
                <td style="width:8%"></td>
                <td style="width:2%"></td>
                <td style="width:90%"></td>
                <td></td>
            </tr>--%>
            <tr>
             <td>
                <table class="cls_panel_header" width="50%" >
                    <tr>
                        <td><asp:Label ID="lblCustCode" runat="server" Text="Cust. Code :" CssClass="cls_label_header" ></asp:Label></td>
                        <td><asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox"  Width="120px" ></asp:TextBox></td>
                        <td> <asp:Label ID="lblCustName" runat="server" Text="Cust Name :" CssClass="cls_label_header" ></asp:Label></td>
                        <td><asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" Width="120px" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblSalesrepCode" runat="server" Text="Field Force Code :" CssClass="cls_label_header" ></asp:Label></td>
                        <td><asp:TextBox ID="txtSalesrepCode" runat="server" CssClass="cls_textbox" Width="120px" ></asp:TextBox></td>
                        <td><asp:Label ID="lblSalesrepName" runat="server" Text="Field Force Name :" CssClass="cls_label_header" ></asp:Label></td>
                        <td><asp:TextBox ID="txtSalesrepName" runat="server" CssClass="cls_textbox" Width="120px" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan ="4">
                          <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button"  />
                            <asp:HiddenField ID="hdAssignType" runat="server" Value="0" />
                            <asp:HiddenField ID="hdCode" runat="server" Value="" />
                        </td>
                    </tr>
                </table>
                </div> 
            </td>
            </tr>
           
           <%-- <tr style="height:15px" align="left" valign="middle">
                <td colspan="3"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Font-Underline="true"></asp:Label></td>
            </tr>
            <tr style="height:15px" align="left">
                <td><span class="cls_label">Total</span></td>
                <td><span class="cls_label">:</span></td> 
                <td><asp:Label ID="lblTotal" runat="server" CssClass="cls_label">0</asp:Label></td> 
               
            </tr>--%>
            
           
            
            <tr><td>&nbsp;</td></tr>
            <asp:Panel ID="pnlCtrlAction" runat="server"> 
                <tr>
                    <td colspan="3" align="left" style="padding-left:15px">
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </td>
                </tr>
            </asp:Panel> 
            <tr><td colspan="3" align="center" style="width:95%;"><customToolkit:wuc_dgpaging ID="wuc_dgAssignPaging" runat="server" /></td></tr>
            <tr>
                <td colspan="3" align="center" style="width:95%;">
                    <ccGV:clsGridView ID="dgAssignList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
            </tr>
        </table>
        
        <customToolkit:wuc_ContactAcctPop ID="wuc_ContactAcctPop" Title="Contact In Sales Account Level Maintenance" runat="server" />
        <customToolkit:wuc_ContactRepPop ID="wuc_ContactRepPop" Title="Contact In Field Force Level Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>

