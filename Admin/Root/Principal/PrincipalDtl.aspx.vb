﻿Option Explicit On

Imports System.Data

Partial Class Admin_Root_Principal_PrincipalDtl
    Inherits System.Web.UI.Page
#Region "Properties"
    Private Enum MODE
        Normal
        Edit
        Create
    End Enum
    Private Property PrincipalID() As String
        Get
            Return ViewState("Principal_ID")
        End Get
        Set(ByVal value As String)
            ViewState("Principal_ID") = value
        End Set
    End Property
    Private Property ActionMode() As MODE
        Get
            Return IIf(ViewState("ActionMode") Is Nothing, MODE.Normal, ViewState("ActionMode"))
        End Get
        Set(ByVal value As MODE)
            ViewState("ActionMode") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            lblErrDtl.Text = ""
            wuc_lblHeader.Title = "Principal"
            If Not IsPostBack Then
                Dim strPrincipalId As String
                strPrincipalId = Trim(Request.QueryString("Principal_ID"))
                If IsNothing(strPrincipalId) Or strPrincipalId = "" Then
                    PrincipalID = 0
                    'btnBack.Visible = False
                Else
                    PrincipalID = strPrincipalId
                End If

                Dim strAction As String = Trim(Request.QueryString("action"))
                Select Case strAction.ToUpper
                    Case "CREATE"
                        ChangeMode(MODE.Create)
                    Case "EDIT"
                        ChangeMode(MODE.Edit)
                    Case Else 'VIEW
                        ChangeMode(MODE.Normal)
                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DTL Button Function Handling"
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditDtl.Click
        ChangeMode(MODE.Edit)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("PrincipalList.aspx", False)
    End Sub

    Protected Sub btnSaveDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDtl.Click
        If ActionMode = MODE.Create Then
            dvHdr_Left.InsertItem(True)
        Else
            dvHdr_Left.UpdateItem(True)
        End If
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDtl.Click
        If ActionMode = MODE.Create Then
            ActionMode = MODE.Normal
            btnBack_Click(sender, Nothing)
        Else
            ChangeMode(MODE.Normal)
        End If
    End Sub
#End Region
#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dvHdr_Left.DataSource = dtCurrenttable.DefaultView
            dvHdr_Left.DataBind()
            dvHdr_Right.DataSource = dtCurrenttable.DefaultView
            dvHdr_Right.DataBind()

            Dim strUserCountryID As String = ""
            Dim strUserSettingID As String = ""
            If dtCurrenttable.Rows.Count > 0 Then
                strUserCountryID = dtCurrenttable.Rows(0)("COUNTRY_NAME")
                strUserSettingID = dtCurrenttable.Rows(0)("SETTING_ID")
            End If
            LoadDDLCountry(strUserCountryID)
            LoadDDLSetting(strUserSettingID)


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsPrincipal
        Try

            DT = obj.PrincipalDtl(PrincipalID, Session("UserID"))


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region
    Private Sub ChangeMode(ByVal actMode As MODE)
        Select Case actMode
            Case MODE.Create
                dvHdr_Left.ChangeMode(DetailsViewMode.Insert)
                dvHdr_Right.ChangeMode(DetailsViewMode.Insert)
                ActionMode = MODE.Create
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
            Case MODE.Edit
                dvHdr_Left.ChangeMode(DetailsViewMode.Edit)
                dvHdr_Right.ChangeMode(DetailsViewMode.Edit)
                ActionMode = MODE.Edit
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
            Case Else 'Normal
                dvHdr_Left.ChangeMode(DetailsViewMode.ReadOnly)
                dvHdr_Right.ChangeMode(DetailsViewMode.ReadOnly)
                ActionMode = MODE.Normal
                btnEditDtl.Visible = True
                btnSaveDtl.Visible = False
                btnCancelDtl.Visible = False
        End Select


        'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit) Then
        '    btnEditDtl.Visible = False
        '    btnCancelDtl.Visible = False
        'End If

        RenewDataBind()
        UpdateDatagrid.Update()
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadDDLCountry(ByVal strUserCountry As String)
        Dim obj As adm_SuperAdmin.clsPrincipal
        Dim dt As DataTable
        Dim lstSelected As ListItem = Nothing

        obj = New adm_SuperAdmin.clsPrincipal
        With obj
            dt = .GetCountry(Session("UserID"))
        End With

        Dim ddlCountry As DropDownList = CType(dvHdr_Left.FindControl("ddlCountry"), DropDownList)

        If dt.Rows.Count >= 0 AndAlso ddlCountry IsNot Nothing Then
            With ddlCountry
                .Items.Clear()
                .DataSource = dt.DefaultView
                .DataTextField = "COUNTRY_NAME"
                .DataValueField = "COUNTRY_ID"
                .DataBind()
                lstSelected = .Items.FindByText(strUserCountry)
                If lstSelected IsNot Nothing Then lstSelected.Selected = True
            End With
        End If
    End Sub

    Private Sub LoadDDLSetting(ByVal strUserSetting As String)
        Dim obj As adm_SuperAdmin.clsPrincipal
        Dim dt As DataTable
        Dim lstSelected As ListItem = Nothing

        obj = New adm_SuperAdmin.clsPrincipal
        With obj
            dt = .GetSetting(Session("UserID"))
        End With

        Dim ddlSetting As DropDownList = CType(dvHdr_Left.FindControl("ddlSetting"), DropDownList)

        If dt.Rows.Count >= 0 AndAlso ddlSetting IsNot Nothing Then
            With ddlSetting
                .Items.Clear()
                .DataSource = dt.DefaultView
                .DataValueField = "SETTING_ID"
                .DataTextField = "SETTING_NAME"
                .DataBind()
                lstSelected = .Items.FindByText(strUserSetting)
                If lstSelected IsNot Nothing Then lstSelected.Selected = True
            End With
        End If
    End Sub
    Protected Sub dvHdr_Left_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvHdr_Left.ItemInserting
        Dim obj As New adm_SuperAdmin.clsPrincipal
        Dim dt As DataTable
        Dim PrincipalCode, CountryID, SettingID As String
        Dim PrincipalName, PrincipalName1, PrincipalDesc, PrincipalDesc1 As String

        Try
            PrincipalCode = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtPrincipalCode"), TextBox).Text)
            CountryID = Trim(CType(dvHdr_Left.Rows(0).FindControl("ddlCountry"), DropDownList).SelectedValue)
            SettingID = Trim(CType(dvHdr_Left.Rows(0).FindControl("ddlSetting"), DropDownList).SelectedValue)
            PrincipalName = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalName"), TextBox).Text)
            PrincipalName1 = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalName1"), TextBox).Text)
            PrincipalDesc = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalDesc"), TextBox).Text)
            PrincipalDesc1 = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalDesc1"), TextBox).Text)

            dt = obj.Create(CountryID, SettingID, PrincipalCode, PrincipalName, PrincipalName1, PrincipalDesc, PrincipalDesc1, Session("UserID"))

            If dt Is Nothing OrElse dt.Rows.Count = 0 OrElse dt.Rows(0)("isduplicate").ToString = "1" Then
                lblErrDtl.Text = "Principal Code already exists."
                Return
            End If

            PrincipalID = dt.Rows(0)("PRINCIPAL_ID").ToString
            ChangeMode(MODE.Normal)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dvHdr_Left_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvHdr_Left.ItemUpdating
        Dim obj As New adm_SuperAdmin.clsPrincipal
        Dim PrincipalCode, CountryID, SettingID As String
        Dim PrincipalName, PrincipalName1, PrincipalDesc, PrincipalDesc1 As String

        Try
            PrincipalCode = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtPrincipalCode"), TextBox).Text)
            CountryID = Trim(CType(dvHdr_Left.Rows(0).FindControl("ddlCountry"), DropDownList).SelectedValue)
            SettingID = Trim(CType(dvHdr_Left.Rows(0).FindControl("ddlSetting"), DropDownList).SelectedValue)
            PrincipalName = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalName"), TextBox).Text)
            PrincipalName1 = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalName1"), TextBox).Text)
            PrincipalDesc = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalDesc"), TextBox).Text)
            PrincipalDesc1 = Trim(CType(dvHdr_Right.Rows(0).FindControl("txtPrincipalDesc1"), TextBox).Text)

            obj.Update(PrincipalID, CountryID, SettingID, PrincipalCode, PrincipalName, PrincipalName1, PrincipalDesc, PrincipalDesc1, Session("UserID"))

            ChangeMode(MODE.Normal)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class
