﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_txtIntFieldRange.ascx.vb" Inherits="include_wuc_txtIntFieldRange" %>
<script type="text/javascript">
<!--
//    function CheckRange(sender, args) {

//      if (ValidatorTrim(document.getElementById(sender.StartControlID).value) == "" && ValidatorTrim(document.getElementById(sender.EndControlID).value) != "") {

//          args.IsValid = false;
//          return;
//      }
//      if (ValidatorTrim(document.getElementById(sender.StartControlID).value) != "" && ValidatorTrim(document.getElementById(sender.EndControlID).value) == "") {

//          args.IsValid = false;
//          return;
//      }
//       
//  }
  
// -->
</script>
<table cellspacing="0" cellpadding="0" border="0">
    <tr valign="top" align="left">
        <td valign="top" align="left" style="white-space: nowrap">
            <span class="cls_label_header">  &nbsp;</span>
        </td>
        <td style="white-space: nowrap;">
        <div style="vertical-align: text-top; float: left;">
    <asp:TextBox ID="txtStart" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox>
    
    <asp:CompareValidator ID="rfvInputStartDataType" runat="server" CssClass="cls_validator" ControlToValidate="txtStart" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ErrorMessage="<BR/>Must be integer!" />
</div>
        
       </td>
        <td style="white-space: nowrap; width:10; padding-left:20px; padding-right:20px">
            <asp:Label ID="Label21" CssClass="cls_label_header" runat="server">To </asp:Label>
        </td>
        <td style="white-space: nowrap;">
<div style="vertical-align: text-top; float: left;">
    <asp:TextBox ID="txtEnd" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox>
    <asp:CompareValidator ID="rfvInputEndDataType" runat="server" CssClass="cls_validator" ControlToValidate="txtEnd" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ErrorMessage="<BR/>Must be integer!" />
    <%--<asp:CustomValidator ID="rfvValidateInputEnd" ClientValidationFunction="CheckRangeEnd" runat="server" Display="Dynamic" ControlToValidate="txtEnd" CssClass="cls_validator" ValidateEmptyText="true" ErrorMessage="<BR/>Incomplete Range!" />--%>
</div>
            <%--<asp:CustomValidator ID="rfvValidateInputStart" ClientValidationFunction="CheckRange" runat="server" Display="Dynamic" CssClass="cls_validator" ValidateEmptyText="true" ErrorMessage="Incomplete Range!" />--%>
            <asp:CompareValidator 
                ID="CV_RangeCompare" 
                runat="server" 
                Display="Dynamic"
                ErrorMessage="Invalid range!"
                ControlToCompare="txtStart"
                ControlToValidate="txtEnd" 
                Operator="GreaterThanEqual" 
                Type="Integer"
                CssClass="cls_validator"
            />
        </td>
    </tr>
</table>


<%--<script type="text/javascript">
    //CustomValidator only validates one control by default. We manually link all the controls to the validator by code below.
//    ValidatorHookupControlID("<%= txtStart.ClientID %>",
//     document.getElementById("<%= rfvValidateInputStart.ClientID %>"));
//    ValidatorHookupControlID("<%= txtEnd.ClientID %>",
//     document.getElementById("<%= rfvValidateInputStart.ClientID %>"));
   

</script>--%>