﻿Imports System.Data

Partial Class Admin_Integration_Incoming
    Inherits System.Web.UI.Page
    Dim strLogin As String
    Dim strPassword As String
    Dim strSessionKey As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                LoadIdentity()

            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadIdentity()

        Try
            Session("echoplus_conn") = IIf(Session("echoplus_conn") Is Nothing, ConfigurationManager.ConnectionStrings("echoplus_conn").ConnectionString, Session("echoplus_conn"))

            Dim clsIntegration As New adm_integration.clsIntegration
            Dim dt As DataTable
            Dim objCrypto As cor_Crypto.clsDecryption
            Dim strDecryptPwd As String

            lblMsg.Text = "Loading"

            With clsIntegration.clsProperties
                .dbuserid = Request.QueryString("userid")
                .strguid = Request.QueryString("guid")
            End With

            dt = clsIntegration.LoadUser
            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("LOGIN") = "" Then
                    Response.Redirect("../../login.aspx")
                Else
                    strLogin = dt.Rows(0)("LOGIN")
                    objCrypto = New cor_Crypto.clsDecryption
                    With objCrypto
                        strDecryptPwd = .TripleDESDecrypt(Trim(dt.Rows(0)("PWD")))
                    End With
                    strPassword = strDecryptPwd
                    strSessionKey = Request.QueryString("SessionKey")
                    LoginUser()
                End If
            Else

                Response.Redirect("../../login.aspx")
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub LoginUser()
        Dim objUser As adm_User.clsUser
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCart As cor_Cart.clsCart
        Dim objCartDT As cor_Cart.clsCartDtl
        Dim dt, dtAR As DataTable
        Dim boolReturnValue As Boolean
        Dim strUserCode As String
        Dim strScript As New StringBuilder
        Dim dtmLoginDate, dtmPwdDate As DateTime
        Dim intPwdExpiry As Integer = 0
        Dim intLoginExpiryFlag As Integer = 0

        Session("echoplus_conn") = IIf(Session("echoplus_conn") Is Nothing, ConfigurationManager.ConnectionStrings("echoplus_conn").ConnectionString, Session("echoplus_conn"))
        Try

            boolReturnValue = False

            objUserQuery = New adm_User.clsUserQuery
            With objUserQuery
                'Authenticate
                .clsProperties.login = strLogin
                .clsProperties.pwd = strPassword
                strUserCode = .Authenticate
                'Get User Login
                .clsProperties.login = strLogin
                dt = .GetUserLogin
            End With

            'Max Retry Reach, Deactivate account
            If dt.Rows.Count > 0 Then
                If IIf(IsDBNull(dt.Rows(0)("login_cnt")), 0, dt.Rows(0)("login_cnt")) >= dt.Rows(0)("max_retry") And dt.Rows(0)("max_retry") <> 0 Then
                    'lblErr.Text = "Max Retry Reach, Please contact Administrator !"
                    'boolReturnValue = True
                    Response.Redirect("../../login.aspx")
                End If

            End If

            objUser = New adm_User.clsUser

            If boolReturnValue = False Then
                'Valid Password and User
                If strUserCode <> "" Then
                    'Reset Count to 0
                    'objUser = New adm_User.clsUser
                    With objUser
                        .clsProperties.user_code = strUserCode
                        .clsProperties.login_cnt = 0
                        .LoginUpdate()
                    End With

                    If dt.Rows.Count <> 0 Then
                        'Get Access Right
                        With objUserQuery
                            .clsProperties.user_code = strUserCode
                            dtAR = .GetUserAccessRightDtlListByUserCode()
                        End With

                        intLoginExpiryFlag = dt.Rows(0)("login_expiry_flag")
                        If IsDBNull(dt.Rows(0)("login_date")) Then
                            dtmLoginDate = Nothing
                        Else
                            dtmLoginDate = dt.Rows(0)("login_date")
                        End If
                        If IsDBNull(dt.Rows(0)("pwd_changed_date")) Then
                            dtmPwdDate = Nothing
                        Else
                            dtmPwdDate = dt.Rows(0)("pwd_changed_date")
                        End If
                        intPwdExpiry = dt.Rows(0)("pwd_expiry")

                        SetValue(Of String)(Session("UserID"), dt.Rows(0)("user_id"))
                        SetValue(Of String)(Session("UserCode"), dt.Rows(0)("user_code"))
                        SetValue(Of String)(Session("UserName"), dt.Rows(0)("user_name"))


                        Session("dflCountryCode") = Trim(IIf(IsDBNull(dt.Rows(0)("country_code")), "", dt.Rows(0)("country_code")))
                        Session("dflSalesRepCode") = Trim(IIf(IsDBNull(dt.Rows(0)("salesrep_code")), "", dt.Rows(0)("salesrep_code")))

                        Session("COUNTRY_ID") = Trim(GetValue(Of String)(dt.Rows(0)("country_id")))
                        Session("COUNTRY_NAME") = Trim(GetValue(Of String)(dt.Rows(0)("country_name")))
                        Session("PRINCIPAL_NAME") = Trim(GetValue(Of String)(dt.Rows(0)("principal_name")))
                        Session("PRINCIPAL_ID") = Trim(GetValue(Of String)(dt.Rows(0)("principal_id")))

                        Session("echoplus_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("echoplus_user_pwd")))

                        ''ffma
                        Session("ffma_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("ffma_user_pwd")))

                        ''ffmr
                        Session("ffmr_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("ffmr_user_pwd")))

                        ''ffms
                        Session("ffms_conn") = _
                        "server=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_server_ip"))) & _
                        ";database=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_db_name"))) & _
                        ";User ID=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_user_id"))) & _
                        ";password=" & Trim(GetValue(Of String)(dt.Rows(0)("ffms_user_pwd")))

                        Session("Year") = Year(Now)
                        Session("Month") = Month(Now)
                        Session("PageSize") = Trim(GetValue(Of String)(dt.Rows(0)("page_size"), 30))
                        Session("UserAccessRight") = dtAR
                        Session("NV_Enable") = Trim(GetValue(Of String)(dt.Rows(0)("nv_flag"), 0))  '1 '0-Disabled, 1-Enabled
                        Session("NetValue") = Trim(GetValue(Of String)(dt.Rows(0)("default_nv"), 1))  '1 '1-NV1, 2-NV2
                        Session("FC_Enable") = Trim(GetValue(Of String)(dt.Rows(0)("field_config_flag"), 1))  '1 '0-Disabled, 1-Enabled
                        Session("FieldConfig") = Trim(GetValue(Of String)(dt.Rows(0)("default_field_config"), 1))  '1 '1-Name Only, 2-Code Only, 3-Code and Name

                        With Portal.UserSession
                            Portal.Util.SetValue(Of String)(.UserID, dt.Rows(0)("user_id"))
                            Portal.Util.SetValue(Of String)(.UserCode, dt.Rows(0)("user_code"))
                            Portal.Util.SetValue(Of String)(.UserName, dt.Rows(0)("user_name"))
                            Portal.Util.SetValue(Of String)(.Login, dt.Rows(0)("login"))

                            'Portal.Util.SetValue(Of String)(.CountryID, dt.Rows(0)("country_id"))
                            'Portal.Util.SetValue(Of String)(.CountryName, dt.Rows(0)("country_name"))
                            'Portal.Util.SetValue(Of String)(.PrincipalID, dt.Rows(0)("principal_id"))
                            'Portal.Util.SetValue(Of String)(.PrincipalName, dt.Rows(0)("principal_name"))

                            Portal.Util.SetValue(Of Integer)(.PageSize, dt.Rows(0)("page_size"))

                            If IsNothing(strSessionKey) Or strSessionKey = "" Then
                                .SessionKey = Portal.Util.GetNewSessionKey
                                Dim objUser2 As New adm_User.clsUser
                                objUser2.UserLoginKeyAdd(.UserID, .Login, .SessionKey)
                            Else
                                .SessionKey = strSessionKey
                            End If
                          
                        End With

                    Else
                        'No User Profile found
                        'lblErr.Text = "User Profile Not Found !"
                        Response.Redirect("../../login.aspx")
                        boolReturnValue = True
                    End If
                Else
                    'Not Valid User/Password
                    'lblErr.Text = "Invalid Login/Password, Please Retry !"
                    Response.Redirect("../../login.aspx")
                    boolReturnValue = True
                End If
            End If

            If boolReturnValue = False Then
                ''Populate Cart
                'objCart = New cor_Cart.clsCart
                'objCartDT = New cor_Cart.clsCartDtl

                'objCart.Populate()
                'objCartDT.Populate()

                'HL:20070904: AUDIT LOG IN DATE **********************
                With objUser
                    .clsProperties.user_id = Session("UserID")
                    Session("AuditID") = .AuditLogin()
                End With

                With objUser
                    .clsProperties.principal_id = Session("PRINCIPAL_ID")
                    .AuditDtl(Session("AuditID"), 0)
                End With
                '****************************************************

                'First login /Password expired
                If (dtmLoginDate.Date <= Today And IsNothing(dtmPwdDate) And intLoginExpiryFlag = 1) Or (DateAdd(DateInterval.Day, intPwdExpiry, dtmPwdDate).Date <= Today And intPwdExpiry <> 0) Then
                    strScript.Append("window.open('Admin/User/UserPwdEdit.aspx?user_code=" & Trim(strUserCode) & "','Echoplus','WIDTH=500,HEIGHT=250,LEFT=10,TOP=10,menubar=no,scrollbars=yes');window.focus();")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "anything", "<script language=javascript>" & strScript.ToString & "</script>")
                Else
                    strScript.Append("<script>")
                    strScript.Append("window.location='../../login2.aspx';window.blur();")
                    strScript.Append("</" + "script>")
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "anything", strScript.ToString)
                End If
            Else
                'strScript = "<script>"
                'strScript.Append( "if (!self.parent.parent){"
                'strScript.Append( "self.parent.parent.location='login.aspx?ErrMsg=" & Trim(lblErr.Text) & "';"
                'strScript.Append( "}"
                'strScript.Append( "else"
                'strScript.Append( "{"
                'strScript.Append( "window.location='login.aspx?ErrMsg=" & Trim(lblErr.Text) & "';"
                'strScript.Append( "}"
                'strScript.Append( "</" + "script>"
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Login", strScript)
                ''Response.Redirect("login.aspx?ErrMsg=" & Trim(lblErr.Text) & "", False)               
            End If

        Catch ex As Exception
            lblErr.Text = "Login.LoginUser : " + ex.ToString()
        Finally
            objCart = Nothing
            objCartDT = Nothing
            objUser = Nothing
            objUserQuery = Nothing
        End Try
    End Sub
    Private Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Private Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
