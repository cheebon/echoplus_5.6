﻿Imports System.Data
Partial Class iFFMR_Customize_PlanVsActual
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_PlanVsActual")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_PlanVsActual") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Public ReadOnly Property PageName() As String
        Get
            Return "PreplanVsActual"
        End Get
    End Property

    Private intPageSize As Integer
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  Try
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.PLAN_ACTUAL)
            .DataBind()
            .Visible = True
        End With

        'Call Panel
        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.PLAN_ACTUAL
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            ' TimerControl1.Enabled = True
            LoadLsbTeam()
            LoadDDLYear()
            UpdateSearch.Update()
        End If

        lblErr.Text = ""

        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        '  End Try

    End Sub

#Region "EVENT HANDLER"

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            RefreshDatabinding()

        End If

    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        ' Try
        RefreshDataBind()
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        'Try

        RenewDataBind()
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        LoadLsbTeam()
        LoadLsbSalesrep()
        LoadDDLYear()
        ddllClass.SelectedIndex = 0
        ddlyear.SelectedIndex = 0
        UpdateSearch.Update()
    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        RefreshDataBind()
        cpeSearch.ClientState = True
        cpeSearch.Collapsed = True
    End Sub
#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        dgList.PageIndex = 0
        'End If
        PreRenderMode(dtCurrentTable)
        'If dtCurrentTable.Rows.Count = 0 Then
        '    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        'End If

        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        'dgList.PageSize = intPageSize
        dgList.DataBind()


        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_Update()

        '  End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '  Try
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_CustomerHeader()
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        ' Finally
        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_PLANVSACTUAL.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_PLANVSACTUAL.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_PLANVSACTUAL.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_PLANVSACTUAL.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_PLANVSACTUAL.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName


                    Dim strUrlFields() As String = Nothing
                    Dim strUrlFormatString As String

                    strUrlFormatString = Nothing

                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = ""
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_PLANVSACTUAL.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_CABM.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_PLANVSACTUAL.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_PLANVSACTUAL.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        ' Finally
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 15 Then dgList.GridHeight = Nothing
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "ResetSize", "resetSize('div_dgList','ContentBarIframe');", True)
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType, "GetScriptedName", "setTimeout('getSelectedCriteria()',1000);", True)
        UpdateDatagrid.Update()
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated

         Select e.Row.RowType
            Case DataControlRowType.Header
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
        End Select
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer
        End Select
        'Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        '  Finally
        ' End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "JAN*" Then
                strColumnName = "Jan"
            ElseIf strColumnName Like "FEB*" Then
                strColumnName = "Feb"
            ElseIf strColumnName Like "MAR*" Then
                strColumnName = "Mar"
            ElseIf strColumnName Like "APR*" Then
                strColumnName = "Apr"
            ElseIf strColumnName Like "MAY*" Then
                strColumnName = "May"
            ElseIf strColumnName Like "JUN*" Then
                strColumnName = "Jun"
            ElseIf strColumnName Like "JUL*" Then
                strColumnName = "Jul"
            ElseIf strColumnName Like "AUG*" Then
                strColumnName = "Aug"
            ElseIf strColumnName Like "SEP*" Then
                strColumnName = "Sep"
            ElseIf strColumnName Like "OCT*" Then
                strColumnName = "Oct"
            ElseIf strColumnName Like "NOV*" Then
                strColumnName = "Nov"
            ElseIf strColumnName Like "DEC*" Then
                strColumnName = "Dec"
            ElseIf strColumnName Like "YTD*" Then
                strColumnName = "YTD"
            Else
                strColumnName = CF_PLANVSACTUAL.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub

    
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        '  Try
        Dim dt As Data.DataTable = GetRecList()
        dt.Rows.Add(dt.NewRow)
        ViewState("dtCurrentView") = dt
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsPlanActual As New rpt_Customize.clsPlanActual

        Dim strUserID As String, strPrincipalID As String, strTeamCode As String, _
         strSalesrepCode As String, strYear As String, strClass As String

        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strYear = ddlyear.SelectedValue
        strClass = ddllClass.SelectedValue
        strTeamCode = Trim(GetItemsInString(lsbSelectedTeam))
        strSalesrepCode = Trim(GetItemsInString(lsbSelectedSalesrep))

        DT = clsPlanActual.GetPlanActual(strUserID, strPrincipalID, strYear, strTeamCode, strSalesrepCode, strClass)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function
#End Region

#Region "POPULATE List Box"

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            UpdateSearch.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_ctrlpanel.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DropdownList"
    Private Sub LoadDDLYear()
        Dim strcurrentyear As Integer, strpreviousyear As Integer
        strcurrentyear = Now.Date.Year
        strpreviousyear = Now.Date.Year - 1

        With ddlyear
            .Items.Insert(0, New ListItem(strcurrentyear, strcurrentyear))
            .Items.Insert(1, New ListItem(strpreviousyear, strpreviousyear))
            .SelectedIndex = 0
        End With
    End Sub


#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class

Public Class CF_PLANVSACTUAL
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "TITLE"
                strFieldName = "Title"
            Case "POSITION"
                strFieldName = "Position"
            Case "DEPARTMENT"
                strFieldName = "Dept."
            Case "SPECIALTY"
                strFieldName = "Specialty"
            Case "ROUTE"
                strFieldName = "Route"
            Case Else
                If ColumnName.ToUpper Like "*PLANNED_CALL" Then
                    strFieldName = "Plan"
                ElseIf ColumnName.ToUpper Like "*ACTUAL_CALL" Then
                    strFieldName = "Act"
                Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
                End If
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        ' Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Return FCT
        'Catch ex As Exception

        ' End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        ' Try

        Select Case strNewName
            Case "CALL_ACH", "ACT_FIELD_DAY_HALF"
                strStringFormat = "{0:0.0}"
            Case "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
                strStringFormat = "{0:0.00}"
            Case Else
                strStringFormat = ""
        End Select

        'If strColumnName.ToUpper Like "DAY_*_DEF" And Not strColumnName.ToUpper Like "DAY_*_CALL" Then
        If strColumnName.ToUpper Like "DAY_*_CALL" And Not strColumnName.ToUpper Like "DAY_*_DEF" Then
            strStringFormat = "{0:0.##}"
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf strColumnName Like "*_NAME" Then
                .HorizontalAlign = HorizontalAlign.Left
                'ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                '    .HorizontalAlign = HorizontalAlign.Left
            ElseIf strColumnName Like "JAN*" OrElse strColumnName Like "FEB*" OrElse strColumnName Like "MAR*" OrElse strColumnName Like "APR*" _
            OrElse strColumnName Like "MAY*" OrElse strColumnName Like "JUN*" OrElse strColumnName Like "JUL*" OrElse strColumnName Like "AUG*" _
            OrElse strColumnName Like "SEP*" OrElse strColumnName Like "OCT*" OrElse strColumnName Like "NOV*" OrElse strColumnName Like "DEC*" OrElse strColumnName Like "YTD*" Then
                .HorizontalAlign = HorizontalAlign.Right
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

End Class

