<%@ Page Language="vb" AutoEventWireup="false" Inherits="TransactionCustSelection"
    CodeFile="TransactionCustSelection.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_txtDate" Src="~/include/wuc_txtDate.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Contact List By Customer</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../../include/layout.js" type="text/javascript"></script>
      <script language="javascript" type="text/javascript">
     function Refresh()
    {
    var ifra = self.parent.document.getElementById('TopBarIframe');
     ifra.contentWindow.ClickBtn();
    }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ResizeFrameWidth('TopBar','50%');ResizeFrameWidth('ContentBar','50%');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe'); MaximiseFrameHeight('TopBarIframe');">
    <form id="frmContactListByCust" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
                <tr>
                    <td>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                            <tr>
                                <td colspan="3">
                                    <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                    <div style="position: absolute; top: 2px; right: 8px;">
                                        <input type="button" runat="server" id="btnback" value="Close" onclick="HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');MaximiseFrameHeight('TopBarIframe');"
                                            style="width: 60px;" class="cls_button" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="Bckgroundreport">
                                
                                    <td>
                                    <asp:UpdatePanel ID="UpdateCallDate" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlCallDate" runat="server" Visible="false">
                                            <table border="0" cellpadding="5" cellspacing="0">
                                                <tr>
                                                    <td><asp:Label ID="lblSelectCallDate" runat="server" Text="Select a txn date" CssClass="cls_label" style="font-weight:bold"></asp:Label></td>
                                                    <td><uc1:wuc_txtDate runat="server" ID="txtCallDate" RequiredValidation="false"  /></td>
                                                    <td>
                                                        <asp:Label ID="lblDateValidation" runat="server" Text="" CssClass="cls_validator"></asp:Label></td>
                                                </tr>
                                            </table>                                            
                                        </asp:Panel>  
                                    </ContentTemplate> 
                                    </asp:UpdatePanel>                                                         
                                    </td>
                            </tr>
                            <tr>
                                <td valign="top" class="Bckgroundreport">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="Bckgroundreport">
                                                <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                        </asp:Timer>
                                                        <ccGV:clsGridView ID="GridView1" runat="server" AddEmptyHeaders="0" AllowSorting="True"
                                                            AutoGenerateColumns="False" CellPadding="2" CssClass="Grid" EmptyHeaderClass=""
                                                            FreezeColumns="0" FreezeHeader="True" FreezeRows="0" GridHeight="505px" GridWidth=""
                                                            RowHighlightColor="AntiqueWhite" ShowFooter="false" Width="100%" DataKeyNames="cont_acc_code">
                                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                                            <EmptyDataTemplate>
                                                                There is no data to display.</EmptyDataTemplate>
                                                            <Columns>
                                                                 <asp:HyperLinkField DataNavigateUrlFields="cont_acc_code" DataNavigateUrlFormatString="#"
                                                                    DataTextField="cont_acc_code" HeaderText="Contact Code" SortExpression="cont_acc_code">
                                                                 </asp:HyperLinkField>
                                                                <%--<asp:BoundField DataField="cont_acc_code" HeaderText="Contact Code" />--%>
                                                                <asp:BoundField DataField="contact_name" HeaderText="Contact Name" />
                                                                <asp:BoundField DataField="rep_speciality" HeaderText="Rep Specialty" />
                                                                <asp:BoundField DataField="position" HeaderText="Position" />
                                                                <asp:BoundField DataField="department" HeaderText="Department" />
                                                                <asp:BoundField DataField="phone" HeaderText="Phone" />
                                                                <asp:BoundField DataField="PRIORITY_NAME" HeaderText="Priority" />
                                                                <asp:TemplateField HeaderText="Visit Contact">
                                                                    <itemtemplate>
                                                                    <asp:Image ID="imgButton" runat="server" ImageUrl="../../../images/ico_edit.gif"  height='18px' width='18px' ></asp:Image>
                                                                    </itemtemplate>
                                                                     <itemstyle horizontalalign="Center" />
                                                                </asp:TemplateField>
                                                                <%--  <asp:ButtonField CommandName="Select"  Text="<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                                HeaderText="Visit Contact"> 
                                                                <itemstyle horizontalalign="Center" />
                                                                </asp:ButtonField>--%>
                                                            </Columns>
                                                            <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />
                                                            <FooterStyle CssClass="GridFooter" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GridAlternate" />
                                                            <RowStyle CssClass="GridNormal" />
                                                        </ccGV:clsGridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td colspan="3">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
