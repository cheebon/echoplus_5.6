<%@ Page Language="VB"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
    .SplitterBar {
        background-color:#BBBBBB;
        background-position:top;
        background-repeat:repeat-y;
        cursor:pointer;
        margin:0px;
        padding:0px;
        vertical-align:middle;
         
        }
    </style>
    <script type="text/javascript">
         function resizeHandlerHoriz() 
         {
            if(top.clientHeight>100)
            {
                  window.document.body.style.height =top.clientHeight-100;
            }
           else if (screen.availHeight>300)
	       {
	          window.document.body.style.height = screen.availHeight-300;
	       }
	       else 
	       {
              window.document.body.style.height=top.screen.height-300;  
	       }
         }    
        window.onresize=resizeHandlerHoriz; 
    </script>
	<link rel="stylesheet" type="text/css" href="~/include/DKSH.css" />
</head>
<body 
    class="SplitterBar" 
    style="background-image:url(../../../images/lib_grippy_Horiz.gif)" 
    onclick="self.parent.CollapseExpandHoriz()" 
     onload="javascript:resizeHandlerHoriz()"
>
</body>

</html>