'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/10/2006
'	Purpose	    :	SAP Details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_SAP_SAPDtl
    Inherits System.Web.UI.Page

    Private intPageSize As Integer
    Private dtmFormat As String = "yyyy-MM-dd hh:m:ss"

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
#Region "Local Variable"
    Private Enum DGListField

        Delete = 0
        POSEX = 1
        MATNR = 2

    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim strTitle As String
        Dim dt, dtDtl As DataTable

        Try
            lblErr.Text = ""

            'Prepare Recordset,Createobject as needed
            strTitle = "SAP Details"
            'Call Header
            With wuc_lblheader
                .Title = strTitle '"SAP Details"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request
                txtDocType.Text = Trim(Request.QueryString("doc_type"))
                txtSearchType.Text = Trim(Request.QueryString("search_type"))
                txtSearchValue.Text = Trim(Request.QueryString("search_value"))
                txtSearchValue1.Text = Trim(Request.QueryString("search_value_1"))
                txtTxnNo.Text = Trim(Request.QueryString("txn_no"))
                Dim strSavingNo As String = Trim(Request.QueryString("so_no")) 'Modified by Soo Fong - Filtered by Saving No

                objSAPQuery = New mst_SAP.clsSAPQuery
                With objSAPQuery
                    .clsProperties.doc_type = txtDocType.Text
                    .clsProperties.txn_no = txtTxnNo.Text
                    .clsProperties.saving_no = strSavingNo 'Modified by Soo Fong - Filtered by Saving No
                    dt = .GetSAPDtl()
                    dtDtl = .GetSAPDtlList()
                End With
                btnResubmit.Attributes.Add("onclick", "javascript:return confirm('Are you want to resubmit?')")
                btnCancel.Attributes.Add("onclick", "javascript:return confirm('Are you want to cancel?')")

                'btnDelete.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Delete)
                'btnEdit.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)

                If dt.Rows.Count > 0 Then
                    lblStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("STATUS")), "", dt.Rows(0)("STATUS"))
                    lblTxnStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_status")), "", dt.Rows(0)("txn_status"))
                    If txtDocType.Text = "TRA" Then
                        lblLNStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_status")), "", dt.Rows(0)("ln_status"))
                        lblLNRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_reason")), "", dt.Rows(0)("ln_reason"))
                        lblBillingNoValue.Text = IIf(IsDBNull(dt.Rows(0)("SAP_billing")), "", dt.Rows(0)("SAP_billing"))
                        pnlLN.Visible = True
                    Else
                        pnlLN.Visible = False
                    End If

                    lblTxnNoValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_no")), "", dt.Rows(0)("txn_no"))
                    lblDocNoValue.Text = IIf(IsDBNull(dt.Rows(0)("DOC_NO")), "", dt.Rows(0)("DOC_NO"))
                    lblSRCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("salesrep_code")), "", dt.Rows(0)("salesrep_code"))
                    lblLastUpdateValue.Text = IIf(IsDBNull(dt.Rows(0)("LASTUPDATE")), "", dt.Rows(0)("LASTUPDATE"))
                    lblTxnDateValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_timestamp")), "", dt.Rows(0)("txn_timestamp"))
                    lblOrderTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("AUART")), "", dt.Rows(0)("AUART"))
                    lblSalesOrgValue.Text = IIf(IsDBNull(dt.Rows(0)("VKORG")), "", dt.Rows(0)("VKORG"))
                    lblDistChannelValue.Text = IIf(IsDBNull(dt.Rows(0)("VTWEG")), "", dt.Rows(0)("VTWEG"))
                    lblDivCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("SPART")), "", dt.Rows(0)("SPART"))
                    lblSalesOffValue.Text = IIf(IsDBNull(dt.Rows(0)("VKBUR")), "", dt.Rows(0)("VKBUR"))
                    lblSalesGrpValue.Text = IIf(IsDBNull(dt.Rows(0)("VKGRP")), "", dt.Rows(0)("VKGRP"))
                    lblPlantValue.Text = IIf(IsDBNull(dt.Rows(0)("WERKS")), "", dt.Rows(0)("WERKS"))
                    lblSlocValue.Text = IIf(IsDBNull(dt.Rows(0)("LGORT")), "", dt.Rows(0)("LGORT"))
                    lblShipPointValue.Text = IIf(IsDBNull(dt.Rows(0)("VSTEL")), "", dt.Rows(0)("VSTEL"))
                    lblSoldToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNNR")), "", dt.Rows(0)("KUNNR"))
                    lblShipToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNDE")), "", dt.Rows(0)("KUNDE"))
                    lblPayerCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("PAYER")), "", dt.Rows(0)("PAYER"))
                    lblPOnovalue.Text = IIf(IsDBNull(dt.Rows(0)("BSTNK")), "", dt.Rows(0)("BSTNK"))             
                    lblPODateValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTDK")), "", dt.Rows(0)("BSTDK"))
                    lblPOTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTRK")), "", dt.Rows(0)("BSTRK"))
                    lblPOSuppValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTZD")), "", dt.Rows(0)("BSTZD"))
                    lblBNameValue.Text = IIf(IsDBNull(dt.Rows(0)("BNAME")), "", dt.Rows(0)("BNAME"))
                    lblIntRefValue.Text = IIf(IsDBNull(dt.Rows(0)("IHREZ")), "", dt.Rows(0)("IHREZ"))
                    lblHdrShipText1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_1")), "", dt.Rows(0)("H_SHIP_TEXT_1"))
                    lblHdrShipText2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_2")), "", dt.Rows(0)("H_SHIP_TEXT_2"))
                    lblHdrShipText3Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_3")), "", dt.Rows(0)("H_SHIP_TEXT_3"))
                    lblHdrDiscType1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_1")), "", dt.Rows(0)("H_DISC_TYP_1"))
                    lblHdrDiscVal1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_1")), "", dt.Rows(0)("H_DISC_VAL_1"))
                    lblHdrDiscType2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_2")), "", dt.Rows(0)("H_DISC_TYP_2"))
                    lblHdrDiscVal2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_2")), "", dt.Rows(0)("H_DISC_VAL_2"))
                    lblDelDateValue.Text = IIf(IsDBNull(dt.Rows(0)("KETDAT")), "", dt.Rows(0)("KETDAT"))
                    lblUsageIndValue.Text = IIf(IsDBNull(dt.Rows(0)("VKAUS")), "", dt.Rows(0)("VKAUS"))
                    lblResendCntValue.Text = IIf(IsDBNull(dt.Rows(0)("RESENDCOUNTER")), "", dt.Rows(0)("RESENDCOUNTER"))
                    lblOrderByValue.Text = IIf(IsDBNull(dt.Rows(0)("ORDER_BY")), "", dt.Rows(0)("ORDER_BY"))
                    lblRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("REMARKS")), "", dt.Rows(0)("REMARKS"))
                    lblNoteValue.Text = IIf(IsDBNull(dt.Rows(0)("NOTE")), "", dt.Rows(0)("NOTE"))

                    With dgList
                        .DataSource = dtDtl
                        .DataBind()
                    End With
                End If
            End If

            If lblTxnStatusValue.Text = "P" And lblStatusValue.Text = "" Then
                btnCancel.Visible = True
            Else
                btnCancel.Visible = False
            End If

            If lblTxnStatusValue.Text = "B" And lblStatusValue.Text = "" Then
                btnUnBlock.Visible = True
            Else
                btnUnBlock.Visible = False
            End If

            If lblStatusValue.Text = "E" Then
                btnResubmit.Visible = True
            Else
                btnResubmit.Visible = False
            End If

            If lblStatusValue.Text = "E" And txtDocType.Text = "TRA" Then
                btnEdit.Visible = True
            Else
                btnEdit.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.Page_Load : " & ex.ToString)
        Finally
            objSAPQuery = Nothing
        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable

        Try
            'If intPassFlag <> 1 Or IsNothing(ViewState("SAPDtl")) Then
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.doc_type = txtDocType.Text
                .clsProperties.txn_no = txtTxnNo.Text
                dt = .GetSAPDtlList()
            End With
            'ViewState("SAPDtl") = dt
            'Else
            'dt = ViewState("SAPDtl")
            'End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            'dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount.ToString
            '    .CurrentPageIndex = dgList.PageIndex
            'End With

        Catch ex As Exception
            ExceptionMsg("SAPDtl.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            objSAPQuery = Nothing
        End Try
    End Sub


#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPDtl.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPDtl.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("SAPDtl.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Private Sub RefreshDatabindingDtl()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt, dtDtl As DataTable

        objSAPQuery = New mst_SAP.clsSAPQuery
        With objSAPQuery
            .clsProperties.doc_type = txtDocType.Text
            .clsProperties.txn_no = txtTxnNo.Text
            dt = .GetSAPDtl()
            dtDtl = .GetSAPDtlList()
        End With

        If dt.Rows.Count > 0 Then
            lblStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("STATUS")), "", dt.Rows(0)("STATUS"))
            lblTxnStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_status")), "", dt.Rows(0)("txn_status"))
            If txtDocType.Text = "TRA" Then
                lblLNStatusValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_status")), "", dt.Rows(0)("ln_status"))
                lblLNRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("ln_reason")), "", dt.Rows(0)("ln_reason"))
                lblBillingNoValue.Text = IIf(IsDBNull(dt.Rows(0)("SAP_billing")), "", dt.Rows(0)("SAP_billing"))
                pnlLN.Visible = True
            Else
                pnlLN.Visible = False
            End If

            lblTxnNoValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_no")), "", dt.Rows(0)("txn_no"))
            lblDocNoValue.Text = IIf(IsDBNull(dt.Rows(0)("DOC_NO")), "", dt.Rows(0)("DOC_NO"))
            lblSRCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("salesrep_code")), "", dt.Rows(0)("salesrep_code"))
            lblLastUpdateValue.Text = IIf(IsDBNull(dt.Rows(0)("LASTUPDATE")), "", dt.Rows(0)("LASTUPDATE"))
            lblTxnDateValue.Text = IIf(IsDBNull(dt.Rows(0)("txn_timestamp")), "", dt.Rows(0)("txn_timestamp"))
            lblOrderTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("AUART")), "", dt.Rows(0)("AUART"))
            lblSalesOrgValue.Text = IIf(IsDBNull(dt.Rows(0)("VKORG")), "", dt.Rows(0)("VKORG"))
            lblDistChannelValue.Text = IIf(IsDBNull(dt.Rows(0)("VTWEG")), "", dt.Rows(0)("VTWEG"))
            lblDivCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("SPART")), "", dt.Rows(0)("SPART"))
            lblSalesOffValue.Text = IIf(IsDBNull(dt.Rows(0)("VKBUR")), "", dt.Rows(0)("VKBUR"))
            lblSalesGrpValue.Text = IIf(IsDBNull(dt.Rows(0)("VKGRP")), "", dt.Rows(0)("VKGRP"))
            lblPlantValue.Text = IIf(IsDBNull(dt.Rows(0)("WERKS")), "", dt.Rows(0)("WERKS"))
            lblSlocValue.Text = IIf(IsDBNull(dt.Rows(0)("LGORT")), "", dt.Rows(0)("LGORT"))
            lblShipPointValue.Text = IIf(IsDBNull(dt.Rows(0)("VSTEL")), "", dt.Rows(0)("VSTEL"))
            lblSoldToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNNR")), "", dt.Rows(0)("KUNNR"))
            lblShipToValue.Text = IIf(IsDBNull(dt.Rows(0)("KUNDE")), "", dt.Rows(0)("KUNDE"))
            lblPayerCodeValue.Text = IIf(IsDBNull(dt.Rows(0)("PAYER")), "", dt.Rows(0)("PAYER"))
            lblPONoValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTNK")), "", dt.Rows(0)("BSTNK"))
            lblPODateValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTDK")), "", dt.Rows(0)("BSTDK"))
            lblPOTypeValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTRK")), "", dt.Rows(0)("BSTRK"))
            lblPOSuppValue.Text = IIf(IsDBNull(dt.Rows(0)("BSTZD")), "", dt.Rows(0)("BSTZD"))
            lblBNameValue.Text = IIf(IsDBNull(dt.Rows(0)("BNAME")), "", dt.Rows(0)("BNAME"))
            lblIntRefValue.Text = IIf(IsDBNull(dt.Rows(0)("IHREZ")), "", dt.Rows(0)("IHREZ"))
            lblHdrShipText1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_1")), "", dt.Rows(0)("H_SHIP_TEXT_1"))
            lblHdrShipText2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_2")), "", dt.Rows(0)("H_SHIP_TEXT_2"))
            lblHdrShipText3Value.Text = IIf(IsDBNull(dt.Rows(0)("H_SHIP_TEXT_3")), "", dt.Rows(0)("H_SHIP_TEXT_3"))
            lblHdrDiscType1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_1")), "", dt.Rows(0)("H_DISC_TYP_1"))
            lblHdrDiscVal1Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_1")), "", dt.Rows(0)("H_DISC_VAL_1"))
            lblHdrDiscType2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_TYP_2")), "", dt.Rows(0)("H_DISC_TYP_2"))
            lblHdrDiscVal2Value.Text = IIf(IsDBNull(dt.Rows(0)("H_DISC_VAL_2")), "", dt.Rows(0)("H_DISC_VAL_2"))
            lblDelDateValue.Text = IIf(IsDBNull(dt.Rows(0)("KETDAT")), "", dt.Rows(0)("KETDAT"))
            lblUsageIndValue.Text = IIf(IsDBNull(dt.Rows(0)("VKAUS")), "", dt.Rows(0)("VKAUS"))
            lblResendCntValue.Text = IIf(IsDBNull(dt.Rows(0)("RESENDCOUNTER")), "", dt.Rows(0)("RESENDCOUNTER"))
            lblOrderByValue.Text = IIf(IsDBNull(dt.Rows(0)("ORDER_BY")), "", dt.Rows(0)("ORDER_BY"))
            lblRemarksValue.Text = IIf(IsDBNull(dt.Rows(0)("REMARKS")), "", dt.Rows(0)("REMARKS"))
            lblNoteValue.Text = IIf(IsDBNull(dt.Rows(0)("NOTE")), "", dt.Rows(0)("NOTE"))

            With dgList
                .DataSource = dtDtl
                .DataBind()
            End With
        End If


    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btnDelete As LinkButton = CType(e.Row.Cells(0).Controls(0), LinkButton)
                If btnDelete IsNot Nothing Then
                    btnDelete.OnClientClick = "if (confirm('Are you sure want to delete?') == false) { window.event.returnValue = false; return false; }"
                End If

            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Dim objSAPQuery As New mst_SAP.clsSAP

        Try
            With objSAPQuery

                .clsRetDtlProperties.user_id = Session("UserID")
                .clsRetDtlProperties.user_name = Session("UserName")
                .clsRetDtlProperties.doc_type = txtDocType.Text
                .clsRetDtlProperties.txn_no = txtTxnNo.Text
                .clsRetDtlProperties.line_no = dgList.Rows(e.RowIndex).Cells(DGListField.POSEX).Text
                .clsRetDtlProperties.prd_code = dgList.Rows(e.RowIndex).Cells(DGListField.MATNR).Text

                .DeleteSAPDTL()

            End With

            RefreshDatabindingDtl()

        Catch ex As Exception
            'ExceptionMsg(PageName & ".dgCondTypeDtl_RowUpdating : " + ex.ToString)
        End Try

    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnViewTray_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnViewTray_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTray.Click
        Try

            If (Trim(Request.QueryString("type")) = 1) Then
                Response.Redirect("SAPList.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=1", False)

            Else
                Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)

            End If

           

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnViewTray_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnRefresh_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            
            If (Trim(Request.QueryString("type")) = 1) Then
                Response.Redirect("SAPList.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=1", False)

            Else
                Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)

            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnRefresh_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnResubmit_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnResubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResubmit.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            If Trim(lblTxnNoValue.Text) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(lblStatusValue.Text) <> "E" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to resubmit.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(txtDocType.Text)
                    .clsProperties.txn_no = Trim(txtTxnNo.Text)
                    .Resubmit()
                End With

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has resubmitted.');", True)

                ' Response.Redirect("SAPDtl.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)

                If (Trim(Request.QueryString("type")) = 1) Then
                    Response.Redirect("SAPList.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=1", False)

                Else
                    Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)

                End If
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnResubmit_Click : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            btnEdit.Visible = False
            btnResubmit.Visible = False
            btnSave.Visible = True
            txtBillingNoValue.Visible = True
            txtBillingNoValue.Enabled = True
            txtBillingNoValue.Text = lblBillingNoValue.Text
            lblBillingNoValue.Visible = False



            'If (Trim(Request.QueryString("type")) = 1) Then
            'Response.Redirect("SAPList.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=1", False)

            'Else
            'Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)
            '
            'End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnRefresh_Click : " & ex.ToString)
        End Try
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
   
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            If Trim(lblTxnNoValue.Text) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(lblTxnStatusValue.Text) <> "P" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to cancel.');", True)
                Exit Sub
            ElseIf Trim(lblStatusValue.Text) <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to SAP and cannot be cancelled.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(txtDocType.Text)
                    .clsProperties.txn_no = Trim(txtTxnNo.Text)
                    .Cancel()
                End With

                ' Response.Redirect("SAPDtl.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)

                If (Trim(Request.QueryString("type")) = 1) Then
                    Response.Redirect("SAPList.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=1", False)

                Else
                    Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)

                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has cancelled.');", True)
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnCancel_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnUnBlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnBlock.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            If Trim(lblTxnNoValue.Text) = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                Exit Sub
            ElseIf Trim(lblTxnStatusValue.Text) <> "B" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Status to unblock.');", True)
                Exit Sub
            ElseIf Trim(lblStatusValue.Text) <> "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn already successfully submittted to SAP and cannot be unblock.');", True)
                Exit Sub
            Else
                objSAP = New mst_SAP.clsSAP
                With objSAP
                    .clsProperties.user_id = Session("UserID")
                    .clsProperties.user_name = Session("UserName")
                    .clsProperties.doc_type = Trim(txtDocType.Text)
                    .clsProperties.txn_no = Trim(txtTxnNo.Text)
                    .UnBlock()
                End With

                'Response.Redirect("SAPDtl.aspx?txn_no=" & txtTxnNo.Text & "&doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
                If (Trim(Request.QueryString("type")) = 1) Then
                    Response.Redirect("SAPList.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=1", False)

                Else
                    Response.Redirect("SAPList_v4.aspx?doc_type=" & Trim(txtDocType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text) & "&type=2", False)

                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has unblocked.');", True)
            End If

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnUnBlock_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim objSAP As mst_SAP.clsSAP

        Try
            objSAP = New mst_SAP.clsSAP
            With objSAP
                .clsProperties.user_id = Session("UserID")
                .clsProperties.user_name = Session("UserName")
                .clsProperties.doc_type = Trim(txtDocType.Text)
                .clsProperties.txn_no = Trim(txtTxnNo.Text)
                .clsProperties.billing_no = Trim(txtBillingNoValue.Text)
                .Update()
            End With

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "", "alert('Txn No." & Trim(lblTxnNoValue.Text) & " has been updated.');", True)

        Catch ex As Exception
            ExceptionMsg("SAPDtl.btnSave_Click : " & ex.ToString)
        End Try
        btnResubmit.Visible = True
        btnEdit.Visible = True
        btnSave.Visible = False
        lblBillingNoValue.Text = txtBillingNoValue.Text
        txtBillingNoValue.Enabled = False
        txtBillingNoValue.Visible = False
        lblBillingNoValue.Visible = True


    End Sub
End Class
