'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	14/04/2007
'	Purpose	    :	ConnectionString Create
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Public Class Admin_ConnectionString_ConnectionStringCreate
    Inherits System.Web.UI.Page
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim objUserQuery As adm_Comm_Svr_Setting.clsCommSvrQuery

        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Creating Connection"
                .DataBind()
                .Visible = True
            End With
        Catch ex As Exception
            ExceptionMsg("ConnectionStringCreate.Page_Load : " & ex.ToString)
        Finally
            'objUserQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnReset_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'ddlStatusID.SelectedValue = 0
            txtDBIP.Text = ""
            txtDBName.Text = ""
            txtDBPort.Text = ""
            txtDBUserID.Text = ""
            txtDBUserPwd.Text = ""
            txtLinkServerName.Text = ""
            txtProxy.Text = ""
            txtServerIP.Text = ""
            txtServerName.Text = ""
            txtCommCode.Text = ""
            txtCommName.Text = ""
            txtURL.Text = ""

        Catch ex As Exception
            ExceptionMsg("ConnectionStringCreate.btnReset_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSave_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objUser As adm_Comm_Svr_Setting.clsCommSvr
        Dim objUserQuery As adm_Comm_Svr_Setting.clsCommSvrQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String
        Dim bUserExist As Boolean

        Try
            'Check login or ConnectionString code exist or not
            objUserQuery = New adm_Comm_Svr_Setting.clsCommSvrQuery
            With objUserQuery
                .clsProperties.strCommCode = Trim(txtCommCode.Text)
                bUserExist = .UserExist()
            End With
            objUserQuery = Nothing

            If bUserExist = True Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('User code already exists!');</script>")
                Exit Sub
            End If

            'Check DB Port is integer
            If Not IsNumeric(txtDBPort.Text) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('DB port number must be in integer');</script>")
                Exit Sub
            End If

            'Check Password match
            If Trim(txtDBUserPwd.Text) <> Trim(txtConfirmPwd.Text) And Trim(txtDBUserPwd.Text <> "") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                Exit Sub
            End If

            'Encrypt Password
            objCrypto = New cor_Crypto.clsEncryption
            With objCrypto
                strEncryptPwd = .TripleDESEncrypt(Trim(txtDBUserPwd.Text))
            End With

            'Insert into ConnectionString Table
            objUser = New adm_Comm_Svr_Setting.clsCommSvr
            With objUser
                .clsProperties.strCommCode = Trim(txtCommCode.Text)
                .clsProperties.strCommName = Trim(txtCommName.Text)
                .clsProperties.strDBIP = Trim(txtDBIP.Text)
                .clsProperties.strDBName = Trim(txtDBName.Text)
                .clsProperties.intDBPort = CInt(Trim(txtDBPort.Text))
                .clsProperties.strDBUserID = Trim(txtDBUserID.Text)
                .clsProperties.strDBPassword = strEncryptPwd
                .clsProperties.strServerIP = Trim(txtServerIP.Text)
                .clsProperties.strServerName = Trim(txtServerName.Text)
                .clsProperties.strLinkServerName = Trim(txtLinkServerName.Text)
                .clsProperties.strProxy = Trim(txtProxy.Text)
                .clsProperties.strURL = Trim(txtURL.Text)
                .clsProperties.strActiveFlag = ddlStatusID.SelectedValue
                .clsProperties.strSyncOperation = "A"
                If Session("UserID") <> "" Then
                    .clsProperties.strCreatorUserID = Session("UserID")
                Else
                    .clsProperties.strCreatorUserID = ""
                End If
                .clsProperties.strCreatedDate = Format(Date.Now, "MM/dd/yyyy hh:mm:ss")
                .Create()
            End With
            'Response.Redirect("ConnectionStringList.aspx", False)
            Response.Redirect("ConnectionStringDtl.aspx?comm_svr_setting_code=" & Trim(txtCommCode.Text), False)


        Catch ex As Exception
            ExceptionMsg("ConnectionStringCreate.btnSave_Click : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("ConnectionStringList.aspx", False)
    End Sub
End Class