'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	21/06/2007
'	Purpose	    :	User Profile
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_User_UserProfile
    Inherits System.Web.UI.Page

    Dim dtPassLib As DataTable

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        cvPoorPass.ErrorMessage = Portal.Policy.GetPwdErrorMsg()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCrypto As cor_Crypto.clsDecryption
        Dim strDecryptPwd, strUserCode As String
        Dim dt As DataTable
        Dim strTitle As String
        Dim strSessionUserCode As String
        
        Try
            lblErr.Text = ""

            If Not IsPostBack Then
                'constant
                strSessionUserCode = Trim(Session("UserCode"))

                'request
                strUserCode = Trim(Request.QueryString("user_code"))
                txtType.Text = Trim(Request.QueryString("type"))

                strTitle = "Editing Profile"

                txtPassword.Enabled = False
                txtConfirmedPassword.Enabled = False
                rfvPassword.Visible = False
                rfvConfirmedPassword.Visible = False
                lblMark3.Visible = False
                lblMark4.Visible = False

                'Prepare Recordset,Createobject as needed
                'Call Header
                With wuc_lblheader
                    .Title = strTitle
                    .DataBind()
                    .Visible = True
                End With

                'Get Details
                objUserQuery = New adm_User.clsUserQuery
                With objUserQuery
                    .clsProperties.user_code = strUserCode
                    dt = .GetUserDtl
                End With

                If dt.Rows.Count > 0 Then
                    'Decrypt Password 
                    objCrypto = New cor_Crypto.clsDecryption
                    If Not IsDBNull(Portal.Util.GetValue(Of String)(dt.Rows(0)("pwd"))) Then
                        With objCrypto
                            strDecryptPwd = .TripleDESDecrypt(Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("pwd"))))
                        End With
                    Else
                        strDecryptPwd = ""
                    End If
                   

                    txtLogin.Text = Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("login")))
                    txtPassword.Text = strDecryptPwd
                    txtConfirmedPassword.Text = strDecryptPwd
                    txtUserID.Text = Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("user_id")))
                    txtUserCode.Text = Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("user_code")))
                    txtUserName.Text = Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("user_name")))
                    txtEmail.Text = Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("email")))
                    txtPageSize.Text = Trim(Portal.Util.GetValue(Of String)(dt.Rows(0)("page_size")))
                End If

            End If

        Catch ex As Exception
            ExceptionMsg("UserProfile.Page_Load : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objUser As adm_User.clsUser
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String = ""
        
        Try
            If chkPassword.Checked Then
                'Check Password match
                If Trim(txtPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtPassword.Text <> "") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                    Exit Sub
                End If

                If (RepeatedPassCheck(txtPassword.Text.Trim) And ConfigurationManager.AppSettings("DiffLast") = "1") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('New password must be different from last " & ConfigurationManager.AppSettings("PwdHistLimit") & " old password.!');</script>")
                    Exit Sub
                End If

                'Encrypt Password
                objCrypto = New cor_Crypto.clsEncryption
                With objCrypto
                    strEncryptPwd = .TripleDESEncrypt(Trim(txtPassword.Text))
                End With
            End If

            'Insert into User Table
            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_id = Trim(txtUserID.Text)
                .clsProperties.login = Trim(txtLogin.Text)
                .clsProperties.user_code = Trim(txtUserCode.Text)
                .clsProperties.pwd_flag = IIf(chkPassword.Checked, 1, 0)
                If chkPassword.Checked Then
                    .clsProperties.pwd = strEncryptPwd
                End If
                .clsProperties.user_name = Trim(txtUserName.Text)
                .clsProperties.email = Trim(txtEmail.Text)
                .clsProperties.page_size = CInt(txtPageSize.Text)
                .clsProperties.pwd_changed_date = Now '.ToString
                .clsProperties.changed_date = Now
                .clsProperties.changed_user_id = Session("UserID")
                .ProfileUpdate()
            End With
            Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Profile is successfully updated.'); self.parent.fraMain.location='../../index.aspx'</script>")
            'Response.Redirect("UserDtl.aspx?user_code=" & Trim(txtUserCode.Text) & "&type=" & Trim(txtType.Text), False)

        Catch ex As Exception
            ExceptionMsg("UserProfile.btnSave_Click : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUser = Nothing
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../../index.aspx", False)
    End Sub

    Protected Sub chkPassword_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPassword.CheckedChanged
        Try
            If chkPassword.Checked Then
                txtPassword.Enabled = True
                txtConfirmedPassword.Enabled = True
                rfvPassword.Visible = True
                rfvConfirmedPassword.Visible = True
                lblMark3.Visible = True
                lblMark4.Visible = True
            Else
                txtPassword.Enabled = False
                txtConfirmedPassword.Enabled = False
                rfvPassword.Visible = False
                rfvConfirmedPassword.Visible = False
                lblMark3.Visible = False
                lblMark4.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg("UserProfile.chkPassword_CheckedChanged : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg
        Catch ex As Exception

        End Try
    End Sub

#Region "Pwd Policy"
    Sub PolicyCheck(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        e.IsValid = Portal.Policy.CheckPwd(e.Value.Trim)
    End Sub

    Private Function RepeatedPassCheck(ByVal strPassword As String) As Boolean
        Dim clsUserQuery As New adm_User.clsUserQuery
        Dim dt As DataTable = Nothing
        Dim rept As Boolean = False
        Try
            If IsNothing(dtPassLib) Then
                With clsUserQuery.clsProperties
                    .user_id = Portal.UserSession.UserID
                End With
                dt = clsUserQuery.GetUsrPassLibList
                dtPassLib = dt
            End If


            Dim objCrypto As cor_Crypto.clsEncryption
            objCrypto = New cor_Crypto.clsEncryption


            Dim dv1 As New DataView(dtPassLib, "pwd='" + objCrypto.TripleDESEncrypt(Trim(strPassword)) + "' ", "pwd", DataViewRowState.CurrentRows)
            If dv1.Count > 0 Then
                rept = True
            End If

        Catch ex As Exception
            ExceptionMsg("RepeatedPassCheck  : " & ex.ToString)
        End Try

        Return rept
    End Function
#End Region

End Class
