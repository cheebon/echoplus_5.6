﻿//
//////////Route Class
var _route;
function SRVisitedRoutes()
{
    this.Salesreps = [];
    _route = this;
}
/////////
//

//
////////Salesrep Class
var _sr;
function Salesrep(srcode, srname)
{
    this.SalesrepCode = srcode;
    this.SalesrepName = srname;
    this.VisitPaths = [];
    _sr = this;
}
////////
//

//
////////VisitPath Class
var _vipath;
function VisitPath(cname, ccode, ctname, cclass, visitid, timein, timeout, timespent, calldate, lat, longi, visitind, imgloc, origpsind)
{
    this.CustName = cname;
    this.CustCode = ccode;
    this.ContName = ctname;
    this.Class = cclass;
    this.VisitID = visitid;
    this.Timein = timein;
    this.Timeout = timeout;
    this.Timespent = timespent;
    this.Calldate = calldate;
    this.Latitude = lat;
    this.Longitude = longi;
    this.VisitInd = visitind;
    this.ImgLoc = imgloc;
    this.OriGPSInd = origpsind;
    _vipath = this;
}