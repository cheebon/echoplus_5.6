﻿'************************************************************************
'	Author	    :	YONG SOO FONG
'	Date	    :	26/02/2016
'	Purpose	    :	Detailing Extra Sub Cat List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data


Partial Class iFFMA_Detailing_DetailingExtraSubCatList
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event DisplayMain_Click As EventHandler

#Region "Variable"


    Public Property DetCode() As String
        Get
            Return ViewState("DetCode")
        End Get
        Set(ByVal value As String)
            ViewState("DetCode") = value
        End Set
    End Property

    Public Property SubDetCode() As String
        Get
            Return ViewState("SubDetCode")
        End Get
        Set(ByVal value As String)
            ViewState("SubDetCode") = value
        End Set
    End Property

    Public Property ExtraDetCode() As String
        Get
            Return ViewState("ExtraDetCode")
        End Get
        Set(ByVal value As String)
            ViewState("ExtraDetCode") = value
        End Set
    End Property

#End Region


#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If

        'If not insert subdetcode, user not allow to insert extradetcode
        'If DetCode <> "" And SubDetCode <> "" Then
        '    btnAdd.Visible = False
        'Else
        '    btnAdd.Visible = True
        'End If

    End Sub
#Region "DATA BIND"
    Public Sub RenewDataBind()

        dgExtraSubCatList.PageIndex = 0
        wuc_dgExtrSubCatPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsDetailing As New mst_Detailing.clsDetailing
            DT = clsDetailing.GetDetailingExtraCatList(DetCode, SubDetCode)
            dgExtraSubCatList.DataKeyNames = New String() {"DET_CODE", "SUB_DET_CODE", "EXTRA_DET_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgExtraSubCatList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try

            dtCurrentTable = GetRecList()


            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgExtraSubCatList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgExtrSubCatPaging
                .PageCount = dgExtraSubCatList.PageCount
                .CurrentPageIndex = dgExtraSubCatList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatedgList.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgExtraSubCatList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgExtraSubCatList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgExtraSubCatList.Columns.Clear()

            ''ADD BUTTON EDIT
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgExtraSubCatList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Delete"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgExtraSubCatList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_DELETE")
            End If


            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_DetailingExtraSubCatList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_DetailingExtraSubCatList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_DetailingExtraSubCatList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_DetailingExtraSubCatList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_DetailingExtraSubCatList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgExtraSubCatList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgExtraSubCatList_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles dgExtraSubCatList.RowDeleting
        Try
            Dim clsDetailing As New mst_Detailing.clsDetailing

            Dim strSelectedDetCode As String = sender.datakeys(e.RowIndex).item("DET_CODE")
            Dim strSelectedSubDetCode As String = sender.datakeys(e.RowIndex).item("SUB_DET_CODE")
            Dim strSelectedExtraDetCode As String = sender.datakeys(e.RowIndex).item("EXTRA_DET_CODE")

            Dim DT As DataTable

            DT = clsDetailing.DeleteDetailingExtraCat(strSelectedDetCode, strSelectedSubDetCode, strSelectedExtraDetCode)

            DetCode = strSelectedDetCode
            SubDetCode = strSelectedSubDetCode
            RefreshDataBind()

            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgExtraSubCatList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgExtraSubCatList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgExtraSubCatList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgExtraSubCatList.RowCreated
    End Sub

    Protected Sub dgExtraSubCatList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgExtraSubCatList.RowEditing
        Try
            Dim strSelectedDetCode As String = sender.datakeys(e.NewEditIndex).item("DET_CODE")
            Dim strSelectedSubDetCode As String = sender.datakeys(e.NewEditIndex).item("SUB_DET_CODE")
            Dim strSelectedExtraDetCode As String = sender.datakeys(e.NewEditIndex).item("EXTRA_DET_CODE")

            With (wuc_DetailingExtraSubCatPop)
                .DetCode = strSelectedDetCode
                .SubDetCode = strSelectedSubDetCode
                .ExtraDetCode = strSelectedExtraDetCode
                .LoadDvEditMode()
                .BindDetailsView()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgExtraSubCatList() As GridView
        Dim dgExtraSubCatListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgExtraSubCatList.AllowSorting
            Dim blnAllowPaging As Boolean = dgExtraSubCatList.AllowPaging

            dgExtraSubCatList.AllowSorting = False
            dgExtraSubCatList.AllowPaging = False
            RefreshDatabinding(True)

            dgExtraSubCatListToExport = dgExtraSubCatList

            dgExtraSubCatList.AllowPaging = blnAllowPaging
            dgExtraSubCatList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgExtraSubCatListToExport
    End Function
#End Region

    Public Sub BindExtrCatCodeList()
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10
            RefreshDataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#Region "EVENT"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        With (wuc_DetailingExtraSubCatPop)
            .DetCode = DetCode
            .SubDetCode = SubDetCode
            .LoadDvInsertMode()
            .BindDetailsView()
        End With
    End Sub

    Protected Sub ReloadTable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_DetailingExtraSubCatPop.ReloadTable_Click
        RenewDataBind()
        RaiseEvent SaveButton_Click(sender, e)
    End Sub

    Protected Sub ShowMainPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_DetailingExtraSubCatPop.ShowMainPop_Click
        RaiseEvent DisplayMain_Click(sender, e)
    End Sub
#End Region
End Class

Public Class CF_DetailingExtraSubCatList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "START_DATE"
                strFieldName = "Start Date"
            Case "END_DATE"
                strFieldName = "End Date"
            Case "EXTRA_DET_CODE"
                strFieldName = "Extra Detailing Code"
            Case "EXTRA_DET_NAME"
                strFieldName = "Extra Detailing Name"
            Case "SUB_DET_CODE"
                strFieldName = "Sub Detailing Code"
            Case "SUB_DET_NAME"
                strFieldName = "Sub Detailing Name"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "TEAM_CODE" Or strColumnName = "DET_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class