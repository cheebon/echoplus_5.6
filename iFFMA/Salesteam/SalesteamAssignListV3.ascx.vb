﻿Imports System.Data

Partial Class iFFMA_Salesteam_SalesteamAssignListV3
    Inherits System.Web.UI.UserControl

    Public Event AddButton_Click As EventHandler
    Public Event DeleteButton_Click As EventHandler
    Public Const DashboardXFieldPerTeamLimit As Integer = 6
    Public Const SfeKpiPerTeamLimit As Integer = 5
    Private dtAR As DataTable
#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum assignTypeName As Long
        'KL 01032016 Set Enum No. according to SalesteamList.aspx.vb assignTypeName
        DashboardXField = 5
        SfeKpi = 6
        Detailing = 7
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        lblTotalHdr.Text = "Total"
        Select Case (AssignType)
            Case assignTypeName.DashboardXField
                lblTitle.Text = "Dashboard XField List"
            Case assignTypeName.SfeKpi
                lblTitle.Text = "SFE KPI List"
            Case assignTypeName.Detailing
                lblTitle.Text = "Detailing List"
        End Select

        If Not IsPostBack Then
            If IsNumeric(Total) Then
                Select Case (AssignType)
                    Case assignTypeName.DashboardXField
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, SubModuleAction.Create) Then
                            SetBtnAddVisibility(DashboardXFieldPerTeamLimit)
                        End If
                    Case assignTypeName.SfeKpi
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, SubModuleAction.Create) Then
                            SetBtnAddVisibility(SfeKpiPerTeamLimit)
                        End If
                    Case assignTypeName.Detailing
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Create) Then
                            LoadCrtlSearch()
                            btnAdd.Visible = True
                        End If
                End Select
            End If
        End If
    End Sub

    Private Sub LoadCrtlSearch()
        Select Case (AssignType)
            Case assignTypeName.Detailing
                With ddlSearchType
                    .Items.Clear()
                    .Items.Insert(0, New ListItem("All", "ALL"))
                    .Items.Insert(1, New ListItem("Detailing Code", "DET_CODE"))
                    .Items.Insert(2, New ListItem("Detailing Name", "DET_NAME"))
                End With

                SearchValueRow.Visible = False
                'Case assignTypeName.DashboardXField
                'Case assignTypeName.SfeKpi
        End Select
        pnlCtrlSearch.Visible = True
        UpdatepnlCtrlSearch.Update()
    End Sub

    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        If ddlSearchType.SelectedValue.ToUpper = "ALL" Then
            SearchValueRow.Visible = False
            RefreshDatabinding()
        Else
            SearchValueRow.Visible = True
        End If
        UpdatepnlCtrlSearch.Update()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RefreshDatabinding()
        UpdatepnlCtrlSearch.Update()
    End Sub

#Region "PROPERTY"
    Public Property Total() As String
        Get
            Return Trim(lblTotal.Text)
        End Get
        Set(ByVal value As String)
            lblTotal.Text = value
        End Set
    End Property

    Public Property AssignType() As Long
        Get
            Return Trim(hdAssignType.Value)
        End Get
        Set(ByVal value As Long)
            hdAssignType.Value = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdCode.Value)
        End Get
        Set(ByVal value As String)
            hdCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgAssignList.PageIndex = 0
        wuc_dgAssignPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strSearhType, strSearchValue As String
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam

            Select Case (AssignType)
                Case assignTypeName.DashboardXField
                    DT = clsSalesteam.GetSalesteamDashboardXFieldListByTeam(TeamCode)
                    dgAssignList.DataKeyNames = New String() {"DASHBOARD_XFIELD_ID"}
                Case assignTypeName.SfeKpi
                    DT = clsSalesteam.GetSalesteamSfeKpiList(TeamCode)
                    dgAssignList.DataKeyNames = New String() {"SFE_KPI_ID"}
                Case assignTypeName.Detailing
                    strSearhType = ddlSearchType.SelectedValue
                    strSearchValue = txtSearchValue.Text
                    DT = clsSalesteam.GetSalesteamDetailingList(TeamCode, strSearhType, strSearchValue)
                    dgAssignList.DataKeyNames = New String() {"DET_CODE"}
            End Select

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
        Return DT
    End Function

#End Region

#Region "dgAssignList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgAssignList.PageIndex = 0
            '    wuc_dgAssignPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgAssignList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            Total = Master_Row_Count 'dtCurrentTable.Rows.Count

            If IsNumeric(Total) Then
                Select Case (AssignType)
                    Case assignTypeName.DashboardXField
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, SubModuleAction.Create) Then
                            SetBtnAddVisibility(DashboardXFieldPerTeamLimit)
                        Else
                            btnAdd.Visible = False
                        End If
                    Case assignTypeName.SfeKpi
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, SubModuleAction.Create) Then
                            SetBtnAddVisibility(SfeKpiPerTeamLimit)
                        Else
                            btnAdd.Visible = False
                        End If
                    Case assignTypeName.Detailing

                End Select
            End If

            'Call Paging
            With wuc_dgAssignPaging
                .PageCount = dgAssignList.PageCount
                .CurrentPageIndex = dgAssignList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlSalesteamAssign.Update()
        End Try
    End Sub

    Private Sub SetBtnAddVisibility(ByVal MaxLimit As Integer)
        If (CInt(Total) >= MaxLimit) Then
            btnAdd.Visible = False
        Else
            btnAdd.Visible = True
        End If
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgAssignList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function CreateBtnColumn(ByVal action As String, ByVal dtToBind As DataTable) As CommandField
        Dim dgBtnColumn As New CommandField

        Select Case action
            Case SubModuleAction.Edit
                With dgBtnColumn
                    .HeaderStyle.Width = "50"
                    .ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    .HeaderText = "Edit"
                End With

                If dtToBind.Rows.Count > 0 Then
                    With dgBtnColumn
                        .ButtonType = ButtonType.Link
                        .ShowEditButton = True
                        .EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                        .Visible = True
                    End With
                End If
            Case SubModuleAction.Delete
                With dgBtnColumn
                    .HeaderStyle.Width = "50"
                    .ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    .HeaderText = "Delete"
                End With

                If dtToBind.Rows.Count > 0 Then
                    With dgBtnColumn
                        .ButtonType = ButtonType.Link
                        .ShowDeleteButton = True
                        .DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                        .Visible = True
                    End With
                End If
        End Select

        Return IIf(dgBtnColumn IsNot Nothing, dgBtnColumn, Nothing)
    End Function

    Protected Sub dgAssignList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgAssignList.Columns.Clear()

            If Master_Row_Count > 0 Then
                Select Case (AssignType)
                    Case assignTypeName.DashboardXField
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, SubModuleAction.Edit) Then
                            dgAssignList.Columns.Add(CreateBtnColumn(SubModuleAction.Edit, dtToBind))
                            aryDataItem.Add("BTN_EDIT")
                        End If
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, SubModuleAction.Delete) Then
                            dgAssignList.Columns.Add(CreateBtnColumn(SubModuleAction.Delete, dtToBind))
                            aryDataItem.Add("BTN_DELETE")
                        End If
                    Case assignTypeName.SfeKpi
                        'By Default No Edit for SFE KPI

                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, SubModuleAction.Delete) Then
                            dgAssignList.Columns.Add(CreateBtnColumn(SubModuleAction.Delete, dtToBind))
                            aryDataItem.Add("BTN_DELETE")
                        End If
                    Case assignTypeName.Detailing
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Edit) Then
                            dgAssignList.Columns.Add(CreateBtnColumn(SubModuleAction.Edit, dtToBind))
                            aryDataItem.Add("BTN_EDIT")
                        End If
                        If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Delete) Then
                            dgAssignList.Columns.Add(CreateBtnColumn(SubModuleAction.Delete, dtToBind))
                            aryDataItem.Add("BTN_DELETE")
                        End If
                End Select
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SalesteamAssignV3List.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        With dgColumn
                            .ReadOnly = True
                            .HeaderStyle.VerticalAlign = VerticalAlign.Middle
                            .ItemStyle.VerticalAlign = VerticalAlign.Middle
                            .HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                            Dim strFormatString As String = CF_SalesteamAssignV3List.GetOutputFormatString(ColumnName)
                            If String.IsNullOrEmpty(strFormatString) = False Then
                                .DataFormatString = strFormatString
                                .HtmlEncode = False
                            End If
                            .ItemStyle.HorizontalAlign = CF_SalesteamAssignV3List.ColumnStyle(ColumnName).HorizontalAlign
                            .ItemStyle.Wrap = CF_SalesteamAssignV3List.ColumnStyle(ColumnName).Wrap
                            .HeaderText = CF_SalesteamAssignV3List.GetDisplayColumnName(ColumnName)
                            .DataField = ColumnName
                            .SortExpression = ColumnName
                        End With

                        dgAssignList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgAssignList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgAssignList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgAssignList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing

                        If btnDelete IsNot Nothing Then
                            Select Case (AssignType)
                                Case assignTypeName.DashboardXField
                                    strDeleteMsg = "The selected XField will be removed from the Dashboard list. \nAre you sure you want to delete?"
                                Case assignTypeName.SfeKpi
                                    strDeleteMsg = "The selected KPI will be removed from the SFE KPI list. \nAre you sure you want to delete?"
                                Case assignTypeName.Detailing
                                    strDeleteMsg = "The selected item will be removed from the Detailing list. \nAre you sure you want to delete?"
                            End Select
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgAssignList.RowDeleting
        Try
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam

            Dim row As Object = sender.datakeys(e.RowIndex)
            Select Case (AssignType)
                Case assignTypeName.DashboardXField
                    Dim strDashboardXField As String = Report.GetRowData(row.item("DASHBOARD_XFIELD_ID"))
                    clsSalesteam.DeleteSalesteamDashboardXField(strDashboardXField, TeamCode)
                Case assignTypeName.SfeKpi
                    Dim strSfeKpi As String = Report.GetRowData(row.item("SFE_KPI_ID"))
                    clsSalesteam.DeleteSalesteamSfeKpi(strSfeKpi, TeamCode)
                Case assignTypeName.Detailing
                    Dim strDetailingCode As String = Report.GetRowData(row.item("DET_CODE"))
                    clsSalesteam.DeleteSalesteamDetailing(strDetailingCode, TeamCode)
            End Select

            RenewDataBind()

            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgAssignList.RowEditing
        Try

            Dim DT As DataTable = Nothing
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam

            Dim row As Object = sender.datakeys(e.NewEditIndex)
            Select Case (AssignType)
                Case assignTypeName.DashboardXField
                    Dim strXFieldId As String = Report.GetRowData(row.item("DASHBOARD_XFIELD_ID"))
                    DT = clsSalesteam.GetSalesteamDashboardXFieldListByTeam(TeamCode)

                    With (wuc_SalesteamDashboardXFieldPop)
                        .TeamCode = TeamCode
                        .Total = CInt(Total)
                        .IsEdit = True
                        .PerTeamLimit = DashboardXFieldPerTeamLimit
                        .LoadDDLXFields()
                        .Show()
                    End With

                Case assignTypeName.SfeKpi
                    'KL - No Edit Feature for SFE KPI
                Case assignTypeName.Detailing
                    Dim strDetailingCode As String = Report.GetRowData(row.item("DET_CODE"))
                    DT = clsSalesteam.GetSalesteamDetailingDetails(strDetailingCode, TeamCode)
                    With (wuc_SalesteamDetailingPop)
                        .IsEdit = True
                        .TeamCode = TeamCode
                        .DetailingCode = DT.Rows(0)("DET_CODE")
                        .LoadDDLDetailing(DT)
                        .Show()
                    End With
            End Select

            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgAssignList() As GridView
        Dim dgAssignListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgAssignList.AllowSorting
            Dim blnAllowPaging As Boolean = dgAssignList.AllowPaging

            dgAssignList.AllowSorting = False
            dgAssignList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgAssignListToExport = dgAssignList

            dgAssignList.AllowPaging = blnAllowPaging
            dgAssignList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgAssignListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Go_Click
        Try
            dgAssignList.PageIndex = CInt(wuc_dgAssignPaging.PageNo - 1)

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Previous_Click
        Try
            If dgAssignList.PageIndex > 0 Then
                dgAssignList.PageIndex = dgAssignList.PageIndex - 1
            End If
            wuc_dgAssignPaging.PageNo = dgAssignList.PageIndex + 1

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Next_Click
        Try
            If dgAssignList.PageCount - 1 > dgAssignList.PageIndex Then
                dgAssignList.PageIndex = dgAssignList.PageIndex + 1
            End If
            wuc_dgAssignPaging.PageNo = dgAssignList.PageIndex + 1

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Select Case (AssignType)
            Case assignTypeName.DashboardXField
                With (wuc_SalesteamDashboardXFieldPop)
                    .TeamCode = Trim(TeamCode)
                    .IsEdit = If(CInt(Total) > 0, True, False)
                    .Total = CInt(Total)
                    .PerTeamLimit = DashboardXFieldPerTeamLimit
                    .LoadDDLXFields()
                    .Show()
                End With
            Case assignTypeName.SfeKpi
                With (wuc_SalesteamSfeKpiPop)
                    .TeamCode = Trim(TeamCode)
                    .Total = CInt(Total)
                    .LoadDvInsertMode()
                    .BindDetailsView()
                    .LoadDDLSfeKpiExclude()
                    .Show()
                End With
            Case assignTypeName.Detailing
                With (wuc_SalesteamDetailingPop)
                    .IsEdit = False
                    .TeamCode = Trim(TeamCode)
                    .LoadDDLDetailingExclude()
                    .Show()
                End With
        End Select
    End Sub

#Region "TAB DASHBOARD XFIELD"
    Protected Sub btnAssignDashboardXFieldPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_SalesteamDashboardXFieldPop.SaveButton_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "TAB SFE KPI"
    Protected Sub btnAssignSfeKpiPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_SalesteamSfeKpiPop.SaveButton_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "TAB DETAILING"
    Protected Sub btnAssignDetailingPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_SalesteamDetailingPop.SaveButton_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#End Region

#Region "COMMON FUNCTION"
    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgAssignList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgAssignList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgAssignList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_SalesteamAssignV3List
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "WHS_DESC"
                strFieldName = "Warehouse"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
