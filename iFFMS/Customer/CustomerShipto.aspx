<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerShipTo" CodeFile="CustomerShipTo.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Ship To Address</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmShiptoAddress" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                        </asp:Timer>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td class="Bckgroundreport">
                                                                    <ccGV:clsGridView ID="gridview1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="420px" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        RowHighlightColor="AntiqueWhite">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.</EmptyDataTemplate>
                                                                        <Columns>
                                                                            <asp:BoundField DataField="delivery_code" HeaderText="Delivery Code" />
                                                                            <asp:BoundField DataField="delivery_name" HeaderText="Delivery Name" />
                                                                            <asp:BoundField DataField="add1" HeaderText="Add 1" />
                                                                            <asp:BoundField DataField="add2" HeaderText="Add 2" />
                                                                            <asp:BoundField DataField="add3" HeaderText="Add 3" />
                                                                            <asp:BoundField DataField="add4" HeaderText="Add 4" />
                                                                            <asp:BoundField DataField="contact_no" HeaderText="Contact No" />
                                                                        </Columns>
                                                                        <FooterStyle CssClass="GridFooter" />
                                                                        <HeaderStyle CssClass="GridHeader" />
                                                                        <AlternatingRowStyle CssClass="GridAlternate" />
                                                                        <RowStyle CssClass="GridNormal" />
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
