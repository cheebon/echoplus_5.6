﻿Imports System.Data

Partial Class iFFMR_Customize_Merchandising
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Dim aryDataItem As New ArrayList
    Dim aryShareItem As New ArrayList

    Dim strProductBrand As Integer

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_Merchandising"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region
    Public Property DateFrom() As String
        Get
            Return txtDate.DateStart
        End Get
        Set(ByVal value As String)
            txtDate.DateStart = value
        End Set
    End Property

    Public Property Dateto() As String
        Get
            Return txtDate.DateEnd
        End Get
        Set(ByVal value As String)
            txtDate.DateEnd = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.MERCHANDISING
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            'RenewDataBind()
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MERCHANDISING)
                .DataBind()
                .Visible = True
            End With
            TimerControl1.Enabled = True
            LoadBrandDDL()
            LoadActivityDDL()
        End If
        lblErr.Text = ""
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "Merchandising"
        End Get
    End Property

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

#Region "Event Handler"
    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        If IsPostBack Then RenewDataBind()
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        RenewDataBind()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        '  Try
        GroupingValue = String.Empty
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
        ddlbrand.Items.Clear()
        ddlActivity.Items.Clear()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Protected Sub btnLoadActivity_Click(sender As Object, e As System.EventArgs) Handles btnLoadActivity.Click
        Dim arrBrand As List(Of String) = New List(Of String)
        For i As Integer = 0 To ddlbrand.Items.Count - 1
            If ddlbrand.Items(i).Selected Then
                arrBrand.Add(ddlbrand.Items(i).Value)
            End If
        Next
        Dim strBrand As String
        strBrand = String.Join(",", arrBrand)

        LoadActivityDDL(strBrand)
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_ctrlpanel.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                strProductBrand = dtCurrentTable.Columns.Count
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    'dgList.ShowFooter = False
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 10 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Custom DGList"
    Private Sub LoadBrandDDL()
        Dim dt As New System.Data.DataTable
        Try
            Dim clsMerchandising As New rpt_Customize.clsMerchandising
            dt = clsMerchandising.GetBrandDDL()

            ddlbrand.DataSource = dt : ddlbrand.DataValueField = "prd_code" : ddlbrand.DataTextField = "prd_name"
            ddlbrand.DataBind()
            ddlbrand.Multiple = True
        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadBrandDDL : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadActivityDDL(Optional ByVal strBrand As String = "")
        Dim dt As DataTable
        Try
            Dim clsMerchandising As New rpt_Customize.clsMerchandising
            If strBrand = "" Then
                dt = clsMerchandising.GetActivityDDL()
            Else
                dt = clsMerchandising.GetActivityDDL(strBrand)
            End If

            ddlActivity.DataSource = dt : ddlActivity.DataValueField = "activity_code" : ddlActivity.DataTextField = "activity_name"
            ddlActivity.DataBind()
            ddlActivity.Multiple = True
        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadActivityDDL : " & ex.ToString)
        End Try
    End Sub

    Private Function FormUrlFormatString(ByVal strColumnName As String, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Try
            ReDim strUrlFields(2)
            strUrlFields(0) = "SALESREP_CODE"
            strUrlFields(1) = "PRD_CODE"
            strUrlFields(2) = "HD_ACTIVITY_CODE"

            Select Case strColumnName
                Case "IMPLEMENTED_STORE"
                    strUrlFormatString = "~/iFFMR/Customize/MerchandisingDetails.aspx?SALESREP_CODE={0}&PRD_CODE={1}&ACTIVITY_CODE={2}&PAGE_INDICATOR=MERCHANDISING"

                Case "TTL_VISITED_STORE"
                    strUrlFormatString = "~/iFFMR/Customize/MerchandisingDetailsTVS.aspx?SALESREP_CODE={0}&PRD_CODE={1}&ACTIVITY_CODE={2}&PAGE_INDICATOR=MERCHANDISING"

                Case Else
                    strUrlFields = Nothing
                    strUrlFormatString = ""
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        End Try
        Return strUrlFormatString
    End Function

    Private Function GetRecList() As DataTable

        Dim strYear, strMonth, strUserID, strPrincipalId, strPrincipalCode, strSalesRepList As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalId = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strSalesRepList = Session("SALESREP_LIST")

        Dim arrBrand As List(Of String) = New List(Of String)
        For i As Integer = 0 To ddlbrand.Items.Count - 1
            If ddlbrand.Items(i).Selected Then
                arrBrand.Add(ddlbrand.Items(i).Value)
            End If
        Next
        Dim strBrand As String
        strBrand = String.Join(",", arrBrand)

        Dim arrActivity As List(Of String) = New List(Of String)
        For i As Integer = 0 To ddlActivity.Items.Count - 1
            If ddlActivity.Items(i).Selected Then
                arrActivity.Add(ddlActivity.Items(i).Value)
            End If
        Next
        Dim strActivity As String
        strActivity = String.Join(",", arrActivity)

        Dim DT As DataTable = Nothing
        Dim clsMerchandising As New rpt_Customize.clsMerchandising
        DT = clsMerchandising.GetMerchandising(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepList, Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strBrand, strActivity)
       
        Return DT
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_MERCHANDISING.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_MERCHANDISING.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_MERCHANDISING.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MERCHANDISING.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.HeaderText = CF_MERCHANDISING.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing

                        strUrlFormatString = FormUrlFormatString(ColumnName, dtToBind, strUrlFields)
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MERCHANDISING.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MERCHANDISING.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_MERCHANDISING.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName.ToUpper)
                        If ColumnName Like "" Then
                            aryShareItem.Add(aryDataItem.Count - 1)
                        End If
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub


    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim iCounter As Integer = 0
                Dim iActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        iActualIndex = IIf(iCounter > 0, iCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(iActualIndex).RowSpan = 2
                            e.Row.Cells(iActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(iActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        iCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

                Case DataControlRowType.Footer
                    Dim iIndex As Integer
                    Dim intRowCount As Integer = dgList.Rows.Count

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = IIf(IsNumeric(li.Value), CDbl(li.Value), 0) 'String.Format(CF_CallByContact.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
                    If (aryDataItem.IndexOf("")) > 0 Then
                        ' e.Row.Cells(aryDataItem.IndexOf("COVERAGE")).Text = String.Format(CF_DETAILING_CALL_BY_AREA.GetOutputFormatString("COVERAGE"), _
                        ' (ConvertToDouble(licItemFigureCollector.FindByText("COVERAGE").Value) / CInt(intRowCount)))
                    End If
                    If (aryShareItem.Count > 0) Then
                        For i As Integer = 0 To aryShareItem.Count - 1
                            Dim aryIndex = aryShareItem(i)
                            e.Row.Cells(aryShareItem(i)).Text = String.Format(CF_MERCHANDISING.GetOutputFormatString(aryDataItem(aryIndex)), (ConvertToDouble(licItemFigureCollector.FindByText(aryDataItem(aryIndex)).Value) / CInt(intRowCount)))
                        Next
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomHeader()
        '      Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "SALESREP_CODE" Then
                strColumnName = "Salesrep Code"
            ElseIf strColumnName = "PRD_CODE" Then
                strColumnName = "Product Category"
            ElseIf strColumnName = "PRD_NAME" Then
                strColumnName = "Product/Brand"
            ElseIf strColumnName Like "ACTIVITY_CODE" Then
                strColumnName = "Activity Code"
            ElseIf strColumnName = "ACTIVITY_NAME" Then
                strColumnName = "Activity Name"
            ElseIf strColumnName = "FROM_DATE" Then
                strColumnName = "Start Date"
            ElseIf strColumnName = "TO_DATE" Then
                strColumnName = "End Date"
            ElseIf strColumnName = "TGT_STORE" Then
                strColumnName = "Target Store"
            ElseIf strColumnName = "TTL_VISITED_STORE" Then
                strColumnName = "Total Visited Store"
            ElseIf strColumnName = "IMPLEMENTED_STORE" Then
                strColumnName = "Implemented Store"
            ElseIf strColumnName = "IMPLEMENTED_ACH" Then
                strColumnName = "% Implemented"
            ElseIf strColumnName = "VISITED_ACH" Then
                strColumnName = "% Visited"
            Else
                strColumnName = "UKNOWN"
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If (strColumnName Like "*_TGT" OrElse _
                 strColumnName Like "*_ACT" OrElse _
                 strColumnName Like "*ACH") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    'li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_CustomHeader()
            Cal_ItemFigureCollector(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class CF_MERCHANDISING
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "SALESREP_NAME"
                strFieldName = "Field Force"
            Case "PRD_CODE"
                strFieldName = "Product Category"
            Case "PRD_NAME"
                strFieldName = "Product/Brand"
            Case "ACTIVITY_CODE"
                strFieldName = "Activity Code"
            Case "ACTIVITY_NAME"
                strFieldName = "Activity Name"
            Case "FROM_DATE"
                strFieldName = "Start Date"
            Case "TO_DATE"
                strFieldName = "End Date"
            Case "TGT_STORE"
                strFieldName = "Target Store"
            Case "TTL_VISITED_STORE"
                strFieldName = "Total Visited Store"
            Case "IMPLEMENTED_STORE"
                strFieldName = "Implemented Store"
            Case "IMPLEMENTED_ACH"
                strFieldName = "% Implemented"
            Case "VISITED_ACH"
                strFieldName = "% Visited"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "SALESREP_CODE") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If strColumnName Like "" Or strColumnName = "" Then
                FCT = FieldColumntype.TemplateColumn_Percentage
            End If

            If (strColumnName = "") Or strColumnName = "HD_ACTIVITY_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "" Or strColumnName = "" Then
                FCT = FieldColumntype.BoundColumn
            End If

            If strColumnName = "IMPLEMENTED_STORE" Or strColumnName = "TTL_VISITED_STORE" Then
                FCT = FieldColumntype.HyperlinkColumn

            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName
                Case "FROM_DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case "TO_DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                    OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                    OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf strColumnName Like "*_NAME" Then
                    .HorizontalAlign = HorizontalAlign.Left
                Else
                    .HorizontalAlign = HorizontalAlign.Right
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class



