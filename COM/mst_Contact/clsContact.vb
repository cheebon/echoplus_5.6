'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	27/03/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsContact
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsContact"
        End Get
    End Property

#Region "CONTACT"
    Public Function GetContList(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetContDetails(ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_DTL"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateCont(ByVal strContCode As String, ByVal strContName As String, ByVal strStatus As String, _
                          ByVal strTitle As String, ByVal strLastName As String, ByVal strFirstName As String, _
                          ByVal strChristianName As String, ByVal strGender As String, ByVal strBirthDate As String, _
                          ByVal strMaritalStatus As String, ByVal strMedSchool As String, ByVal strGradDate As String, _
                          ByVal strEmail As String, ByVal strTelNo1 As String, ByVal strTelNo2 As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_CREATE"
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE", strTitle, cor_DB.clsDB.DataType.DBString)
                .addItem("LAST_NAME", strLastName, cor_DB.clsDB.DataType.DBString)
                .addItem("FIRST_NAME", strFirstName, cor_DB.clsDB.DataType.DBString)
                .addItem("CHRISTIAN_NAME", strChristianName, cor_DB.clsDB.DataType.DBString)
                .addItem("GENDER", strGender, cor_DB.clsDB.DataType.DBString)
                .addItem("BIRTH_DATE", strBirthDate, cor_DB.clsDB.DataType.DBString)
                .addItem("MARITAL_STATUS", strMaritalStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("MED_SCHOOL", strMedSchool, cor_DB.clsDB.DataType.DBString)
                .addItem("GRAD_DATE", strGradDate, cor_DB.clsDB.DataType.DBString)
                .addItem("EMAIL", strEmail, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_1", strTelNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_2", strTelNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateCont(ByVal strContCode As String, ByVal strContName As String, ByVal strStatus As String, _
                          ByVal strTitle As String, ByVal strLastName As String, ByVal strFirstName As String, _
                          ByVal strChristianName As String, ByVal strGender As String, ByVal strBirthDate As String, _
                          ByVal strMaritalStatus As String, ByVal strMedSchool As String, ByVal strGradDate As String, _
                          ByVal strEmail As String, ByVal strTelNo1 As String, ByVal strTelNo2 As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_UPDATE"
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE", strTitle, cor_DB.clsDB.DataType.DBString)
                .addItem("LAST_NAME", strLastName, cor_DB.clsDB.DataType.DBString)
                .addItem("FIRST_NAME", strFirstName, cor_DB.clsDB.DataType.DBString)
                .addItem("CHRISTIAN_NAME", strChristianName, cor_DB.clsDB.DataType.DBString)
                .addItem("GENDER", strGender, cor_DB.clsDB.DataType.DBString)
                .addItem("BIRTH_DATE", strBirthDate, cor_DB.clsDB.DataType.DBString)
                .addItem("MARITAL_STATUS", strMaritalStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("MED_SCHOOL", strMedSchool, cor_DB.clsDB.DataType.DBString)
                .addItem("GRAD_DATE", strGradDate, cor_DB.clsDB.DataType.DBString)
                .addItem("EMAIL", strEmail, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_1", strTelNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_2", strTelNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetNewContCode() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_CODE_GENERATE"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "CONTACT - SALES ACCOUNT"
    Public Function GetContAcctList(ByVal strContCode As String, ByVal strCustCode As String, ByVal strCustName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_ACCT_LIST"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetContAcctDetails(ByVal strContCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_ACCT_DTL"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateContAcct(ByVal strContCode As String, ByVal strCustCode As String, ByVal strStatus As String, _
                            ByVal strPositionCode As String, ByVal strPacCode As String, ByVal strHospAbbv As String, _
                            ByVal strHospRankCode As String, ByVal strDepartmentCode As String, ByVal strLoc As String, ByVal strTelNo1 As String, _
                            ByVal strTelNo2 As String, ByVal strFaxNo As String, ByVal strPatientCount As String, _
                            ByVal strBedCount As String, ByVal strPatientCap As String, ByVal strPracticeSize As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_ACCT_UPDATE"
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("POSITION_CODE", strPositionCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PAC_CODE", strPacCode, cor_DB.clsDB.DataType.DBString)
                .addItem("HOSP_ABBV", strHospAbbv, cor_DB.clsDB.DataType.DBString)
                .addItem("HOSP_RANK_CODE", strHospRankCode, cor_DB.clsDB.DataType.DBString)
                .addItem("DEPARTMENT_CODE", strDepartmentCode, cor_DB.clsDB.DataType.DBString)
                .addItem("LOC", strLoc, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_1", strTelNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_2", strTelNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO", strFaxNo, cor_DB.clsDB.DataType.DBString)
                .addItem("PATIENT_COUNT", strPatientCount, cor_DB.clsDB.DataType.DBString)
                .addItem("BED_COUNT", strBedCount, cor_DB.clsDB.DataType.DBString)
                .addItem("PATIENT_CAP", strPatientCap, cor_DB.clsDB.DataType.DBString)
                .addItem("PRACTICE_SIZE", strPracticeSize, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function CreateContAcct(ByVal strContCode As String, ByVal strCustCode As String, ByVal strStatus As String, _
                            ByVal strPositionCode As String, ByVal strPacCode As String, ByVal strHospAbbv As String, _
                            ByVal strHospRankCode As String, ByVal strDepartmentCode As String, ByVal strLoc As String, ByVal strTelNo1 As String, _
                            ByVal strTelNo2 As String, ByVal strFaxNo As String, ByVal strPatientCount As String, _
                            ByVal strBedCount As String, ByVal strPatientCap As String, ByVal strPracticeSize As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_ACCT_CREATE"
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("POSITION_CODE", strPositionCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PAC_CODE", strPacCode, cor_DB.clsDB.DataType.DBString)
                .addItem("HOSP_ABBV", strHospAbbv, cor_DB.clsDB.DataType.DBString)
                .addItem("HOSP_RANK_CODE", strHospRankCode, cor_DB.clsDB.DataType.DBString)
                .addItem("DEPARTMENT_CODE", strDepartmentCode, cor_DB.clsDB.DataType.DBString)
                .addItem("LOC", strLoc, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_1", strTelNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_2", strTelNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO", strFaxNo, cor_DB.clsDB.DataType.DBString)
                .addItem("PATIENT_COUNT", strPatientCount, cor_DB.clsDB.DataType.DBString)
                .addItem("BED_COUNT", strBedCount, cor_DB.clsDB.DataType.DBString)
                .addItem("PATIENT_CAP", strPatientCap, cor_DB.clsDB.DataType.DBString)
                .addItem("PRACTICE_SIZE", strPracticeSize, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "CONTACT - FIELD FORCE"
    Public Function GetContRepList(ByVal strContCode As String, ByVal strCustCode As String, ByVal strCustName As String, _
        ByVal strSalesrepCode As String, ByVal strSAlesrepName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_REP_LIST"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SALESREP_NAME", strSAlesrepName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetContRepDetails(ByVal strContCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_REP_DTL"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateContRep(ByVal strContCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strStatus As String, _
                            ByVal strSpecialityName As String, ByVal strDepartmentCode As String, ByVal strAsstName As String, ByVal strPriorityName As String, _
                            ByVal strVisitFreq As String, ByVal strPersonalInterest As String, ByVal strMedInterest As String, _
                            ByVal strMktgPref As String, ByVal strXfield1 As String, ByVal strXfield2 As String, ByVal strNotes As String, ByVal strClass As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CONT_REP_UPDATE"
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("SPECIALITY_NAME", strSpecialityName, cor_DB.clsDB.DataType.DBString)
                .addItem("DEPARTMENT_CODE", strDepartmentCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ASST_NAME", strAsstName, cor_DB.clsDB.DataType.DBString)
                .addItem("PRIORITY_NAME", strPriorityName, cor_DB.clsDB.DataType.DBString)
                .addItem("VISIT_FREQ", strVisitFreq, cor_DB.clsDB.DataType.DBInt)
                .addItem("PERSONAL_INTEREST", strPersonalInterest, cor_DB.clsDB.DataType.DBString)
                .addItem("MED_INTEREST", strMedInterest, cor_DB.clsDB.DataType.DBString)
                .addItem("MKTG_PREF", strMktgPref, cor_DB.clsDB.DataType.DBString)
                .addItem("XFIELD_1", strXfield1, cor_DB.clsDB.DataType.DBString)
                .addItem("XFIELD_2", strXfield2, cor_DB.clsDB.DataType.DBString)
                .addItem("NOTES", strNotes, cor_DB.clsDB.DataType.DBString)
                .addItem("CLASS", strClass, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



