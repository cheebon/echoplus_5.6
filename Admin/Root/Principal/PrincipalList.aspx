﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrincipalList.aspx.vb" Inherits="Admin_Root_Principal_PrincipalList" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Principal List</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmPrincipalList" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <div class="border_std">
                    <asp:Button ID="btnCreate" runat="server" Text="Create" CausesValidation="False"
                                                    UseSubmitBehavior="false" Visible="true" />
                </div>
                <!-- Search box -->
                <div class="border_std">
                <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblSearchType" runat="server" Text="Search by "></asp:Label></td><td>
                            <asp:DropDownList ID="ddlSearchType" runat="server" AutoPostBack="true" Width="200px">
                                <asp:ListItem Value="ALL" Text="All"></asp:ListItem>
                                <asp:ListItem Value="PRINCIPAL_ID" Text="Principal ID"></asp:ListItem>
                                <asp:ListItem Value="PRINCIPAL_CODE" Text="Principal Code"></asp:ListItem>
                                <asp:ListItem Value="PRINCIPAL_NAME" Text="Principal Name"></asp:ListItem>
                                <asp:ListItem Value="COUNTRY_NAME" Text="Country Name"></asp:ListItem>
                            </asp:DropDownList></td>
                </tr> 
                <asp:Panel runat="server" ID="pnlSeachValue" Visible="false">
                <tr>
                    <td>
                        <asp:Label ID="lblSearchValue" runat="server" Text="Search value "></asp:Label></td><td>
                            <asp:TextBox ID="txtSearchValue" runat="server" MaxLength="100"></asp:TextBox></td>
                </tr> 
                </asp:Panel>              
                </table>
                <asp:Button ID="btnSearch" runat="server" Text="Search" />
                </div>
                <br />
                <div class="border_std">                     
                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>
                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="330px" AddEmptyHeaders="0" CellPadding="2"
                        EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="PRINCIPAL_ID">
                        <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                        <EmptyDataTemplate>There is no data added.</EmptyDataTemplate>
                        <Columns>
                             <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif" ButtonType="Image" HeaderText="Delete">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="Modify" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="PRINCIPAL_ID" HeaderText="Principal ID" SortExpression="PRINCIPAL_ID" />
                            <asp:BoundField DataField="COUNTRY_NAME" HeaderText="Country" SortExpression="COUNTRY_NAME" />
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="PRINCIPAL_CODE" HeaderText="Principal Code" SortExpression="PRINCIPAL_CODE" />
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="PRINCIPAL_NAME" HeaderText="Principal Name" SortExpression="PRINCIPAL_NAME" />
                            <asp:BoundField DataField="PRINCIPAL_DESC" HeaderText="Principal Description" SortExpression="PRINCIPAL_DESC" />
                            
                            <asp:BoundField DataField="SETTING_ID" HeaderText="Setting ID" SortExpression="SETTING_ID" />
                           
                        </Columns>
                    </ccGV:clsGridView>
                           
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
