﻿<%@ Page Language="vb" AutoEventWireup="false" Inherits="CommonCustContNonCoverageInfo" CodeFile="CommonCustContNonCoverageInfo.aspx.vb" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblDate" Src="~/include/wuc_lblDate.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CustomerContactInformation</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCommonCustContNonCoverageInfo" CodeFile="CommonCustContNonCoverageInfo" method="post" runat="server">
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="left" style="width: 2%">
                                                                            <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" ToolTip="Back To Previous Page."
                                                                                PostBackUrl="~/iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Timer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick">
                                                                            </asp:Timer>
                                                                            <%--<cc1:StoppableTimer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick">
                                                                        </cc1:StoppableTimer>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True"
                                                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="445"
                                                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                                FreezeRows="0" GridWidth="" RowHighlightColor="AntiqueWhite">
                                                                <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                <EmptyDataTemplate>
                                                                    There is no data to display.</EmptyDataTemplate>
                                                               <%-- <Columns>
                                                                   <asp:BoundField DataField="CUST_CODE" HeaderText="Customer A/C" ReadOnly="True" SortExpression="CUST_CODE">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="Customer Name" SortExpression="CUST_NAME">
                                                                        <itemstyle horizontalalign="Left" />
                                                                        <itemtemplate>
                                                                        <asp:Label runat="server" Text='<%# Bind("CUST_NAME") %>' id="lblCompany"></asp:Label>
                                                                        
</itemtemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Address" SortExpression="ADDRESS">
                                                                        <itemstyle horizontalalign="Left" />
                                                                        <itemtemplate>
                                                                        <asp:Label runat="server" Text='<%# EVAL("ADD_1") & "<BR/>" & EVAL("ADD_2") %>' id="lblAddress"></asp:Label>
                                                                        
</itemtemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="CONT_NAME" HeaderText="Contact Name" ReadOnly="True"
                                                                        SortExpression="CONT_NAME">
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TIME_IN" HeaderText="Time In" ReadOnly="True" SortExpression="TIME_IN">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TIME_OUT" HeaderText="Time Out" ReadOnly="True" SortExpression="TIME_OUT">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TIME_SPEND" HeaderText="Time Spend" ReadOnly="True" SortExpression="TIME_SPEND">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REASON_NAME" HeaderText="Reason" ReadOnly="True" SortExpression="REASON_NAME" >
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS" >
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:HyperLinkField DataTextField="SFMS_IND" HeaderText="A" InsertVisible="False"
                                                                        SortExpression="SFMS_IND" />
                                                                    <asp:HyperLinkField DataNavigateUrlFields="SALES_IND" DataTextField="SALES_INDICATOR"
                                                                        HeaderText="$" InsertVisible="False" SortExpression="SALES_IND" />
                                                                </Columns>--%>
                                                            </ccGV:clsGridView>
                                                             </td>
                                                        </tr>
                                                    </table>
                                                            
                                                            
                                                        </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>        

    </form>
</body>
</html>
