Imports System
Imports System.Drawing
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.ComponentModel

'Namespace wcc
Public Class clsDataGrid
    Inherits DataGrid
    'Implements INamingContainer

#Region "Properties"
    <Bindable(True), Category("Appearance"), TypeConverter(GetType(WebColorConverter)), Description("Specifies the color a row is highlighted when the mouse is over it.")> _
    Public Property RowHighlightColor() As Color
        Get
            Dim o As Object = ViewState("RowHighlightColor")
            If o Is Nothing Then
                Return Color.Empty
            Else
                Return CType(o, Color)
            End If
        End Get
        Set(ByVal Value As Color)
            ViewState("RowHighlightColor") = Value
        End Set
    End Property

    <DefaultValue(""), Description("Specifies the CommandName used in the server-side DataGrid event when the row is clicked.")> _
      Public Property RowClickEventCommandName() As String
        Get
            Dim o As Object = ViewState("RowClickEventCommandName")
            If o Is Nothing Then
                Return String.Empty
            Else
                Return CStr(o)
            End If
        End Get
        Set(ByVal Value As String)
            ViewState("RowClickEventCommandName") = Value
        End Set
    End Property

    <DefaultValue(True), Description("Indicates whether or not rows are highlighted/clickable.")> _
    Public Property RowSelectionEnabled() As Boolean
        Get
            Dim o As Object = ViewState("RowSelectionEnabled")
            If o Is Nothing Then
                Return True
            Else
                Return CBool(o)
            End If
        End Get
        Set(ByVal Value As Boolean)
            ViewState("RowSelectionEnabled") = Value
        End Set
    End Property
#End Region

#Region "Overridden DataGrid Methods"
    Protected Overrides Function CreateItem(ByVal itemIndex As Integer, ByVal dataSourceIndex As Integer, ByVal itemType As ListItemType) As DataGridItem

        ' Create the clsDataGridItem
        Dim item As New clsDataGridItem(itemIndex, dataSourceIndex, itemType)

        ' Set the client-side onmouseover and onmouseout if RowSelectionEnabled == true
        If RowSelectionEnabled And itemType <> ListItemType.Header And itemType <> ListItemType.Footer And itemType <> ListItemType.Pager Then '
            'item.Attributes("onmouseover") = "javascript:prettyDG_changeBackColor(this, true);"
            'item.Attributes("onmouseout") = "javascript:prettyDG_changeBackColor(this, false);"
        End If

        ' return the clsDataGridItem
        Return item
    End Function

    Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
        MyBase.OnPreRender(e)

        If Not RowSelectionEnabled Then
            Return ' exit if not RowSelectionEnabled == true
        End If

        ' add the client-side script to change the background color
        Const SCRIPT_KEY As String = "prettyDGscript"
        'Const SCRIPT As String = " <script language='JavaScript'>" & _
        '                                  " <!--" & _
        '                                  " var lastColorUsed;" & _
        '                                  " Function prettyDG_changeBackColor(ByVal row, ByVal highlight)" & _
        '                                  " {" & _
        '                                  " If (highlight) Then" & _
        '                                  " {" & _
        '                                  " lastColorUsed = row.style.backgroundColor;" & _
        '                                  " row.style.backgroundColor = '#{0:X2}{1:X2}{2:X2}';" & _
        '                                  " }" & _
        '                                  " Else" & _
        '                                  " row.style.backgroundColor = lastColorUsed;" & _
        '                                  " }" & _
        '                                  " // -->" & _
        '                                  " </script>"

        If Not RowHighlightColor.IsEmpty And Not Page.ClientScript.IsClientScriptBlockRegistered(SCRIPT_KEY) Then
            'Page.RegisterClientScriptBlock(SCRIPT_KEY, [String].Format(SCRIPT, RowHighlightColor.R, RowHighlightColor.G, RowHighlightColor.B))
        End If
        ' add the click client-side event handler, if needed
        If RowClickEventCommandName <> String.Empty And Me.ChildControlsCreated And Controls.Count > 0 Then
            If TypeOf e Is CommandEventArgs Then
                Dim ce As CommandEventArgs = CType(e, CommandEventArgs)
            End If
            'If ce.CommandName = RowClickEventCommandName Then
            CreateClickEvent()
            'End If
            'End If
        End If
    End Sub

    Protected Overridable Sub CreateClickEvent()
        Dim dgi As clsDataGridItem
        For Each dgi In Me.Items
            If dgi.ItemType <> ListItemType.Header And dgi.ItemType <> ListItemType.Footer And dgi.ItemType <> ListItemType.Pager Then
                If Me.EditItemIndex <> dgi.ItemIndex Then
                    dgi.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(dgi, RowClickEventCommandName)
                    'dgi.Attributes("onkeydown") = Page.GetPostBackClientHyperlink(dgi, RowClickEventCommandName)
                End If
            End If
        Next dgi
    End Sub 'CreateClickEvent
#End Region

    Public Class clsDataGridItem
        Inherits DataGridItem
        Implements IPostBackEventHandler 'ToDo: Add Implements Clauses for implementation methods of these interface(s)

        Public Sub New(ByVal itemIndex As Integer, ByVal dataSetIndex As Integer, ByVal itemType As ListItemType)
            MyBase.New(itemIndex, dataSetIndex, itemType)
        End Sub

        Public Sub RaisePostBackEvent(ByVal eventArgument As String) Implements IPostBackEventHandler.RaisePostBackEvent
            Dim commandArgs As New CommandEventArgs(eventArgument, Nothing)
            Dim args As New DataGridCommandEventArgs(Me, Me, commandArgs)

            MyBase.RaiseBubbleEvent(Me, args)
        End Sub
    End Class
End Class
'End Namespace
