﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesCustPerProductGrid.aspx.vb"
    Inherits="iFFMR_Customize_SalesCustPerProductGrid" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustSearch" Src="~/iFFMR/COMMON/wuc_Custsearch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Customer Per Product</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->

<script type="text/javascript" language="javascript">
    function resizeLayout3() { var dgList = $get('div_dgList'); if (dgList) { dgList.scrollTop = 0; dgList.style.height = Math.max((getFrameHeight('ContentBarIframe') - 70), 0) + 'px'; } }
    window.onresize = function() { resizeLayout2(); }
    function getSelectedCriteria() { var frm = self.parent.document.frames[0]; if (frm) { var hdf = document.getElementById('hdfSalesrepName'); var spn_ori = frm.document.getElementById('hdnSalesrepName'); if (hdf && spn_ori) { hdf.value = spn_ori.innerHTML; } hdf = document.getElementById('hdfSalesTeamName'); spn_ori = frm.document.getElementById('hdnSalesTeamName'); if (hdf && spn_ori) { hdf.value = spn_ori.innerHTML; } } }
    setTimeout("getSelectedCriteria()", 1000);

    function launchCenter(url, name, height, width, addons) {
        var str = "height=" + height + ",innerHeight=" + height;
        str += ",width=" + width + ",innerWidth=" + width + addons;
        if (window.screen) {
            var ah = screen.availHeight - 30;
            var aw = screen.availWidth - 10;
            var xc = (aw - width) / 2;
            var yc = (ah - height) / 2;
            str += ",left=" + xc + ",screenX=" + xc;
            str += ",top=" + yc + ",screenY=" + yc;
        }
        return window.open(url, name, str);
    }
    function SalesCustPreProduct_updateCustomerInfo(custCode, custName) {
        document.getElementById("txtCustCode").value = custCode
    }
    function SalesCustPreProduct_openCustomer() { myProduChild = launchCenter("../../../Core/Catalog/CustomerList.aspx?CATEGORY=SALES_CUST_PER_PRD", "", "600", "800", ",scrollbars=yes, resizable=yes"); }

</script>

<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="frmSalesCustPerProduct" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td valign="top" align="left">
                <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTreeMenuControl" runat="server" >
                            <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%"
                                    align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3" style="width: 100%">
                                            <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3">
                                            <asp:UpdatePanel ID="UpdateDataview" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <span style="padding-left:5px;" class="cls_label_header">Customer Code :</span>
                                                                <asp:TextBox ID="txtCustCode" CssClass="cls_textbox" runat="server"></asp:TextBox>
                                                                <%--&nbsp;<input id="btnShipToNoFile" class="cls_button" onclick="SalesCustPreProduct_openCustomer()"
                                                                size="22" type="button" value="..."  />&nbsp;--%>
                                                                 <asp:Button ID="btnSearchCust" runat="server" CssClass="cls_button" Text="..." OnClick="btnSearchCust_Click" />
                                                                <asp:Button ID="btnsearch" CssClass="cls_button" runat="server" Text="Refresh" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div style="width: 800px; padding-left: 5px; padding-top: 10px; padding-bottom:10px;">
                                                                    <span style="float: left; width: 400px">
                                                                        <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                            Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                            <RowStyle CssClass="cls_DV_Row_MST" />
                                                                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                Width="35%" Wrap="False" />
                                                                        </asp:DetailsView>
                                                                    </span><span style="float: left; width: 400px">
                                                                        <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                            Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                            <RowStyle CssClass="cls_DV_Row_MST" />
                                                                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                Width="35%" Wrap="False" />
                                                                        </asp:DetailsView>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowPaging="false"
                                                                    AllowSorting="false" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                    GridHeight="440" RowSelectionEnabled="true">
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport" style="height: 10px">
                                        <td colspan="3">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
     <div><customToolkit:wuc_CustSearch ID="wuc_CustSearch" Title="Customer Search" runat="server" /></div>
    </form>
</body>
</html>
