'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	15/09/2005
'	Purpose	    :	Class to build Sales Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSalesQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))


    'Sub New()
    '    TableName = ""
    'End Sub

    'Sub New(ByVal strTableName As String)
    '    TableName = strTableName
    'End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSalesQuery"
        End Get
    End Property

    'execute SPP_RPT_SALESENQUIRYLIST 
    '@START_DATE, @END_DATE, @PRD_CODE, @SALESREP_CODE, @SHIPTO_CODE, @INV_NO

    Public Function GetSalesEnquiryList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strStartDate As String, ByVal strEndDate As String, _
     ByVal strPrdCode As String, _
     ByVal strSalesrepCode As String, _
     ByVal strCustCode As String, _
     ByVal strInvNo As String, _
     ByVal intNetValue As Integer) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_ENQUIRY_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, 2)
                .addItem("END_DATE", strEndDate, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("PRD_CODE", strPrdCode, 2)
                .addItem("CUST_CODE", strCustCode, 2)
                .addItem("INV_NO", strInvNo, 2)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesEnquiryList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try

    End Function

    'SPP_RPT_SALES_LIST 
    '@GRP , @YEAR , @MONTH , @USER_ID , @SALESREP_CODE , @TEAM_CODE , @AGENCY_CODE
    Function GetSalesList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strGroupField As String, ByVal intNetValue As Integer) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetSalesInfoByDate(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strMonth As String, _
        ByVal strTeamCode As String, ByVal strRegionCode As String, ByVal strSalesrepCode As String, _
        ByVal strChainCode As String, ByVal strChannelCode As String, _
        ByVal strCustGrpCode As String, ByVal strCustCode As String, _
        ByVal strPrdGrpCode As String, ByVal strPrdCode As String, _
        ByVal strSalesAreaCode As String, ByVal strShiptoCode As String, _
        ByVal intNetValue As Integer, ByVal strTreeSalesrep As String) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_INFO_BY_DATE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CHAIN_CODE", strChainCode, clsDB.DataType.DBString)
                .addItem("CHANNEL_CODE", strChannelCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                .addItem("TREE_SALESREP_LIST", strTreeSalesrep, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesInfoByDate :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetSalesSummList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSelectedDate As String, _
        ByVal strTeamCode As String, ByVal strRegionCode As String, ByVal strSalesrepCode As String, _
        ByVal strChainCode As String, ByVal strChannelCode As String, _
        ByVal strCustGrpCode As String, ByVal strCustCode As String, _
        ByVal strPrdGrpCode As String, ByVal strPrdCode As String, _
        ByVal strSalesAreaCode As String, ByVal strShiptoCode As String, _
        ByVal intNetValue As Integer, ByVal strTreeSalesrep As String) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_SUMM_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SELECTED_DATE", strSelectedDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CHAIN_CODE", strChainCode, clsDB.DataType.DBString)
                .addItem("CHANNEL_CODE", strChannelCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                .addItem("TREE_SALESREP_CODE", strTreeSalesrep, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesInfoByDate :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_SALES_INFO_BY_KEY_PRD @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @MONTH, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @NET_VALUE
    Function GetSalesInfoByKeyPrd(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
            ByVal strYear As String, ByVal strMonth As String,  _
            strSalesrepList As String, ByVal intNetValue As Integer) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_INFO_BY_KEY_PRD"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesInfoByKeyPrd :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_SALES_TOP_80_CUST_BY_REP @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @MONTH, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @GRP_FIELD, @NET_VALUE
    Function GetSalesTop80CustByRep(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
            ByVal strYear As String, ByVal strMonth As String, ByVal strTreeSalesrepList As String, ByVal strGroupField As String, ByVal intNetValue As Integer) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_TOP_80_CUST_BY_REP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strTreeSalesrepList, clsDB.DataType.DBString, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTop80CustByRep :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetSalesByCustCount(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, _
        ByVal strGroupField As String, ByVal intNetValue As Integer) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CUST_COUNT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesByCustCount :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
