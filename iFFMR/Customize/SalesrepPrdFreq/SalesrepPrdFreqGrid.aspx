<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesrepPrdFreqGrid.aspx.vb"
    Inherits="SalesrepPrdFreqGrid" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Salesrep Prd Freq Grid</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>

<script type="text/javascript" language="javascript">
function resizeLayout3(){var dgList = $get('div_dgList');if (dgList){dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight('ContentBarIframe') - 70),0)+'px';}}
window.onresize=function(){resizeLayout2();}
function getSelectedCriteria(){var frm =self.parent.document.frames[0];if(frm){var hdf=document.getElementById('hdfSalesrepName');var spn_ori = frm.document.getElementById('hdnSalesrepName');if(hdf && spn_ori){hdf.value=spn_ori.innerHTML;}hdf=document.getElementById('hdfSalesTeamName');spn_ori=frm.document.getElementById('hdnSalesTeamName');if(hdf && spn_ori){hdf.value=spn_ori.innerHTML;}}}
setTimeout("getSelectedCriteria()",1000);
</script>
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="frmsalesrepPrdfreqGrid" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">

                            <tr>
                                <td>
                                    <customControl:wuc_lblHeader ID="wuc_lblHeader" runat="server"></customControl:wuc_lblHeader>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <customControl:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true">
                                    </customControl:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        
                                                            <tr>
                                                                <td>
                                                                    <span class="cls_label_header">Total(%): </span>
                                                                    <asp:Label ID="lbltotal" runat="server" Text="" CssClass="frmlabel"></asp:Label>
                                                                    <span class="cls_label_header">Over(%): </span>
                                                                    <asp:Label ID="lblover" runat="server" Text="" CssClass="frmlabel"></asp:Label>
                                                                    <span class="cls_label_header">Not Planned: </span>
                                                                    <asp:Label ID="lblnotplanned" runat="server" Text="" CssClass="frmlabel"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="width: 95%;">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="false" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="true" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
  
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
