<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserGrpList.aspx.vb" Inherits="Admin_usergrp_UserGrpList" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
     <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmUserGrpList" runat="server">
    <div>
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
				                    <TR>
					                    <TD width="20%"><asp:label ID="lblSearch" Runat="server" CssClass="cls_label_header">Search By</asp:label></TD>
					                    <TD width="2%"><asp:label ID="lblDot1" Runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td><asp:dropdownlist id="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
							                    <asp:ListItem Value="all">All</asp:ListItem>
							                    <asp:ListItem Value="country_name">Country</asp:ListItem>
							                    <asp:ListItem Value="usergrp_desc">Description</asp:ListItem>
							                    <asp:ListItem Value="usergrp_name">Name</asp:ListItem>
							                    <asp:ListItem Value="principal_name">Principal</asp:ListItem>
							               </asp:dropdownlist></td>
				                    </TR>
				                    <tr>
					                    <TD><asp:label id="lblSearchValue" runat="server" CssClass="cls_label_header"></asp:label></TD>
					                    <TD><asp:label id="lblDot" runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td>
						                    <asp:textbox id="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:textbox>
						                    <%--<asp:linkbutton id="lbDate1" Runat="server">Date</asp:linkbutton>--%>
						                    <asp:label id="lblTo" runat="server" CssClass="cls_label_header">To</asp:label>
						                    <asp:textbox id="txtSearchValue1" runat="server" CssClass="cls_textbox"></asp:textbox>
						                    <%--<asp:linkbutton id="lbDate2" Runat="server">Date</asp:linkbutton>--%>
						                    <asp:dropdownlist id="ddlList" runat="server" CssClass="cls_dropdownlist"></asp:dropdownlist>
						                    <asp:button id="btnSearch" runat="server" Text="Search" CssClass="cls_button"></asp:button>
					                    </td>
				                    </tr>
				                    <tr class="Bckgroundreport">                                        
                                        <td colspan="3">&nbsp;</td>                                                                                                                         
                                    </tr>
                                    <tr>                                      
                                        <td class="Bckgroundreport" colspan="3">
                                            <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server" ></uc1:wuc_dgpaging>
				                            <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowSorting="True"
                                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="300px"
                                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                FreezeRows="0" GridWidth="" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="usergrp_id">
                                                <Columns>
                                                    <asp:BoundField DataField="usergrp_id" HeaderText="Access Right ID" ReadOnly="True" SortExpression="usergrp_id" visible="false">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>                                                                                   
                                                    <asp:HyperLinkField DatatextField="usergrp_name" HeaderText="Name" SortExpression="usergrp_name" DataNavigateUrlFields="usergrp_id" DataNavigateUrlFormatString="UserGrpDtl.aspx?usergrp_id={0}">
                                                       <itemstyle horizontalalign="Center" />
                                                    </asp:HyperLinkField>
                                                    <asp:HyperLinkField DatatextField="usergrp_desc" HeaderText="Description" SortExpression="usergrp_desc" DataNavigateUrlFields="usergrp_id" DataNavigateUrlFormatString="UserGrpDtl.aspx?usergrp_id={0}">
                                                       <itemstyle horizontalalign="Center" />
                                                    </asp:HyperLinkField>
                                                    <asp:BoundField DataField="country_name" HeaderText="Country" ReadOnly="True" SortExpression="country_name">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="principal_name" HeaderText="Principal" ReadOnly="True" SortExpression="principal_name">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>
                                                    <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif" ButtonType="Image"/>                                                                                                                                                      
											        <asp:ButtonField CommandName="Edit" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image"/>                                                                                                                                                      
                                                </Columns>                                                
                                            </ccGV:clsGridView>
                                           <%-- <asp:BoundField DataField="salesrep_code" HeaderText="Sales Rep" ReadOnly="True" SortExpression="salesrep_code">
                                                <itemstyle horizontalalign="Center" />
                                            </asp:BoundField>--%>
					                    </TD>
				                    </TR>      
				                    <TR>
					                    <TD colSpan="3">&nbsp;</TD>
				                    </TR>                   
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                    <tr class="Bckgroundreport">
                                        <td align="right" colspan="3">                                           
                                            <asp:button id="btnCreate" runat="server" Text="Create" CssClass="cls_button" CausesValidation="False"></asp:button>&nbsp;                        					
                                        </td>
                                    </tr>
                                </table>
                            </td>                            
                        </tr>                        
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>                        
                    </table> 
			    </td>
			</tr>			
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 
    </div>
    </form>
</body>
</html>
<%'List function called by in-line scripts%>	
