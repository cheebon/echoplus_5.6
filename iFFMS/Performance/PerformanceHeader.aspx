<%@ Page Language="vb" AutoEventWireup="false" Inherits="ReportDetail" CodeFile="PerformanceHeader.aspx.vb" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="uc1" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300"></asp:ScriptManager>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <uc1:wuc_ctrlpanel id="Wuc_ctrlpanel" runat="server">
                                </uc1:wuc_ctrlpanel></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr">
                                    <ContentTemplate>
                                &nbsp;
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                                        </td>
                                    </tr>
                                    <%--<tr class="Bckgroundreport">
                                        <td colspan="3">
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <%--<tr class="Bckgroundreport">
                                                    <td >
                                                        &nbsp;</td>
                                                        <td>
                                                        
                                                        </td>
                                                </tr>--%>
                                                 <tr class="Bckgroundreport">
                                        <td colspan="3" style="height=10px">
                                            &nbsp;</td>
                                    </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td class="Bckgroundreport">
                                                      
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="left" style="width: 150px; height: 18px;">
                                                                            <asp:Label ID="Label1" runat="server" CssClass="cls_label_header" Text="Sales Rep Code :"></asp:Label></td>
                                                                             <td align="left" style="width:  auto; height: 18px;">
                                                                                 <asp:Label ID="lblSalesRepCode" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        <%--<td align="right">
                                                                            &nbsp;</td>--%>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 150px; height: 18px">
                                                                            <asp:Label ID="Label2" runat="server" CssClass="cls_label_header" Text="Sales Rep Name :"></asp:Label></td>
                                                                        <td align="left" style="width: auto; height: 18px">
                                                                            <asp:Label ID="lblSalesRepName" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 150px; height: 18px">
                                                                            <asp:Label ID="Label3" runat="server" CssClass="cls_label_header" Text="Target Duration : "></asp:Label></td>
                                                                        <td align="left" style="width: auto; height: 18px">
                                                                            <asp:Label ID="lblTarget" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 150px; height: 18px">
                                                                            </td>
                                                                        <td align="left" style="width: auto; height: 18px">
                                                                                 </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" colspan="2" style="height: 18px">
                                                                            <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" Width="60px" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td align="left" style="width: 150px; height: 10px;">
                                                                            </td>
                                                                    </tr>
                                                                </table>
                                                                <ccGV:clsGridView ID="dgPerformance" runat="server" ShowFooter="false"
                                                                    AllowSorting="True" AutoGenerateColumns="False" Width="98%" FreezeHeader="True"
                                                                    GridHeight="" AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="" RowHighlightColor="AntiqueWhite">
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                    <EmptyDataTemplate>
                                                                                                                             <div id='div_dgPerformance' style='OVERFLOW:auto; ' ><div>
	                                                                    <table class="Grid" cellspacing="0" cellpadding="2" rules="all" border="1" id="dgPerformance" style="width:100%;border-collapse:collapse;">
		<tr class="GridHeader">
			<th scope="col">Prd Group</th><th scope="col">S. Target</th><th scope="col">CTD Sales</th><th scope="col">Ration (%)</th><th scope="col">&nbsp;</th>
		</tr><tr class="GridAlternate" onmouseover="javascript:prettyDG_changeBackColor(this, true);" onmouseout="javascript:prettyDG_changeBackColor(this, false);" OnClick="__doPostBack('dgPerformance','Select$7')" style="cursor:hand">
			<td>&nbsp;</td><td></td><td></td><td></td><td></td>
		</tr>
	</table>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="prod_desc" HeaderText="Prd Group"/>
                                                                        <asp:BoundField DataField="sls_tgt" HeaderText="S. Target"/>
                                                                        <asp:BoundField DataField="ctd_sls" HeaderText="CTD Sales"/>
                                                                        <asp:BoundField DataField="ratio" HeaderText="Ration (%)"/>
                                                                         <asp:ButtonField Text="More" CommandName="More" />
                                                                       
                                                                    </Columns>
                                                                    <FooterStyle CssClass="GridFooter" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                                                    <RowStyle CssClass="GridNormal" />
                                                                </ccGV:clsGridView>
                                                         
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
