Imports System.Data

Partial Class iFFMA_Product_ProductUOMPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property UOMCode() As String
        Get
            Return Trim(hdUOMCode.Value)
        End Get
        Set(ByVal value As String)
            hdUOMCode.Value = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtUOM As DataTable
        Dim dtSalesOrg As DataTable
        Dim dtRegion As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            If ddlUOM IsNot Nothing Then
                dtUOM = clsCommon.GetUOMDDL
                With ddlUOM
                    .Items.Clear()
                    .DataSource = dtUOM.DefaultView
                    .DataTextField = "UOM_NAME"
                    .DataValueField = "UOM_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("UOM_CODE")), "", DT.Rows(0)("UOM_CODE"))
                End With
            End If

            If Session("PRINCIPAL_ID") = 5 Then
                Dim ddlSalesOrg As DropDownList = CType(DetailsView1.FindControl("ddlSalesOrg"), DropDownList)
                If ddlSalesOrg IsNot Nothing Then
                    dtSalesOrg = clsCommon.GetSalesOrgDDL
                    With ddlSalesOrg
                        .Items.Clear()
                        .DataSource = dtSalesOrg.DefaultView
                        .DataTextField = "SALES_ORG_NAME"
                        .DataValueField = "SALES_ORG_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                        .SelectedValue = IIf(IsDBNull(DT.Rows(0)("SALES_ORG_CODE")), "", DT.Rows(0)("SALES_ORG_CODE"))
                    End With
                End If

                Dim ddlRegion As DropDownList = CType(DetailsView1.FindControl("ddlRegion"), DropDownList)
                If ddlRegion IsNot Nothing Then
                    dtRegion = clsCommon.GetRegionDDL
                    With ddlRegion
                        .Items.Clear()
                        .DataSource = dtRegion.DefaultView
                        .DataTextField = "REGION_NAME"
                        .DataValueField = "REGION_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                        .SelectedValue = IIf(IsDBNull(DT.Rows(0)("REGION_CODE")), "", DT.Rows(0)("REGION_CODE"))
                    End With
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsProduct As New mst_Product.clsProduct

            DT = clsProduct.GetPrdUOMDetails(PrdCode, UOMCode)
            If DT.Rows.Count > 0 Then
                
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Dim lblPrdCode As Label = CType(DetailsView1.FindControl("lblPrdCode"), Label)
            lblPrdCode.Text = PrdCode

            Dim txtConvFactor As TextBox = CType(DetailsView1.FindControl("txtConvFactor"), TextBox)
            If txtConvFactor IsNot Nothing Then
                txtConvFactor.Text = IIf(txtConvFactor.Text = "", 0, txtConvFactor.Text)
            End If

            Dim txtPrdListPrice As TextBox = CType(DetailsView1.FindControl("txtPrdListPrice"), TextBox)
            If txtPrdListPrice IsNot Nothing Then
                txtPrdListPrice.Text = IIf(txtPrdListPrice.Text = "", 0, txtPrdListPrice.Text)
            End If

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
            If Session("PRINCIPAL_ID") = 5 Then
                Dim hdSalesOrgCode As HiddenField = CType(DetailsView1.FindControl("hdSalesOrgCode"), HiddenField)
                If hdSalesOrgCode IsNot Nothing Then
                    hdSalesOrgCode.Value = DT.Rows(0).Item("SALES_ORG_CODE")
                End If

                Dim lblSalesOrg As Label = CType(DetailsView1.FindControl("lblSalesOrg"), Label)
                If lblSalesOrg IsNot Nothing Then
                    lblSalesOrg.Text = DT.Rows(0).Item("SALES_ORG_NAME")
                End If

                Dim hdRegionCode As HiddenField = CType(DetailsView1.FindControl("hdRegionCode"), HiddenField)
                If hdRegionCode IsNot Nothing Then
                    hdRegionCode.Value = DT.Rows(0).Item("REGION_CODE")
                End If

                Dim lblRegion As Label = CType(DetailsView1.FindControl("lblRegion"), Label)
                If lblRegion IsNot Nothing Then
                    lblRegion.Text = DT.Rows(0).Item("REGION_NAME")
                End If
            Else
                DetailsView1.Rows(1).Visible = False
                DetailsView1.Rows(2).Visible = False
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lblPrdCode As Label = CType(DetailsView1.FindControl("lblPrdCode"), Label)
            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            Dim txtConvFactor As TextBox = CType(DetailsView1.FindControl("txtConvFactor"), TextBox)
            Dim txtPrdListPrice As TextBox = CType(DetailsView1.FindControl("txtPrdListPrice"), TextBox)
          
            txtConvFactor.Text = IIf(Trim(txtConvFactor.Text) = "", 0, Trim(txtConvFactor.Text))
            txtPrdListPrice.Text = IIf(Trim(txtPrdListPrice.Text) = "", 0, Trim(txtPrdListPrice.Text))

            Dim clsProduct As New mst_Product.clsProduct
            Dim strSalesOrgCode As String = ""
            Dim strRegionCode As String = ""

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                
                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Session("PRINCIPAL_ID") = 5 Then
                    Dim hdSalesOrgCode As HiddenField = CType(DetailsView1.FindControl("hdSalesOrgCode"), HiddenField)
                    Dim hdRegionCode As HiddenField = CType(DetailsView1.FindControl("hdRegionCode"), HiddenField)

                    strSalesOrgCode = hdSalesOrgCode.Value
                    strRegionCode = hdRegionCode.Value
                End If

                clsProduct.UpdatePrdUOM(PrdCode, UOMCode, Trim(txtConvFactor.Text), Trim(txtPrdListPrice.Text), strSalesOrgCode, strRegionCode)

                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim isDuplicate As Integer = 1

                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Session("PRINCIPAL_ID") = 5 Then
                    Dim ddlSalesOrg As DropDownList = CType(DetailsView1.FindControl("ddlSalesOrg"), DropDownList)
                    Dim ddlRegion As DropDownList = CType(DetailsView1.FindControl("ddlRegion"), DropDownList)

                    strSalesOrgCode = ddlSalesOrg.SelectedValue
                    strRegionCode = ddlRegion.SelectedValue
                End If

                DT = clsProduct.CreatePrdUOM(PrdCode, Trim(ddlUOM.SelectedValue), Trim(txtConvFactor.Text), Trim(txtPrdListPrice.Text), strSalesOrgCode, strRegionCode)
                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This UOM Code already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    UOMCode = Trim(ddlUOM.SelectedValue)
                    lblInfo.Text = "The record is successfully created."
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

