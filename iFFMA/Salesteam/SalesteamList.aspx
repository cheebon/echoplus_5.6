<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesteamList.aspx.vb" Inherits="iFFMA_Salesteam_SalesteamList" %>

<%@ Register TagPrefix="wuctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="wuclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="wuclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="wucUpdateProgress" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="wucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="wucSalesteamAssignList" TagName="wuc_SalesteamAssignList" Src="SalesteamAssignList.ascx" %>
<%@ Register TagPrefix="wucSalesteamAssignPop" TagName="wuc_SalesteamAssignPop" Src="SalesteamAssignPop.ascx" %>
<%@ Register TagPrefix="wucSalesteamDashboardXFieldPop" TagName="wuc_SalesteamDashboardXFieldPop" Src="SalesteamDashboardXFieldPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="wucAssignPrdV2" TagName="wuc_AssignPrdV2" Src="SalesteamAssignListV2.ascx" %>
<%@ Register TagPrefix="wucSalesteamAssignListV3" TagName="wuc_SalesteamAssignListV3" Src="SalesteamAssignListV3.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Salesteam List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesteamList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <wuclblInfo:wuc_lblInfo ID="lblErr" runat="server" />
                                    <wuclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <wuctoolbar:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <wucUpdateProgress:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />

                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />

                                            <%--<span style="float:left; width:100%;">--%>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <wucdgpaging:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0"
                                                                                GridWidth="" AllowPaging="True" DataKeyNames="TEAM_CODE" BorderColor="Black"
                                                                                BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px"
                                                                                RowHighlightColor="AntiqueWhite">
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>

                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <wuclblInfo:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%">
                                                                            <tr>
                                                                                <td style="width: 17%"></td>
                                                                                <td style="width: 3%"></td>
                                                                                <td style="width: 80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Team Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtTeamCode" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvTeamCode" runat="server" ControlToValidate="txtTeamCode"
                                                                                    ErrorMessage="Team Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Team Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtTeamName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Status</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                                                                        <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                                                    </asp:DropDownList></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignList:wuc_SalesteamAssignList ID="wuc_AssignFieldForce" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignList:wuc_SalesteamAssignList ID="wuc_AssignSupplier" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel4" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignList:wuc_SalesteamAssignList ID="wuc_AssignPrdGrp" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel5" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignList:wuc_SalesteamAssignList ID="wuc_AssignPrd" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel6" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucAssignPrdV2:wuc_AssignPrdV2 ID="wuc_AssignPrdV2" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel7" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignListV3:wuc_SalesteamAssignListV3 ID="wuc_AssignDashboardXField" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel8" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignListV3:wuc_SalesteamAssignListV3 ID="wuc_AssignSfeKpi" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                                <ajaxToolkit:TabPanel ID="TabPanel9" runat="server">
                                                    <ContentTemplate>
                                                        <center>
                                                            <wucSalesteamAssignListV3:wuc_SalesteamAssignListV3 ID="wuc_AssignDetailing" runat="server" />
                                                        </center>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>

                            <tr class="Bckgroundreport">
                                <td style="height: 5px"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>

        <wucSalesteamAssignPop:wuc_SalesteamAssignPop ID="wuc_SalesteamAssignPop" Title="Salesteam Assignment" runat="server" />
    </form>
</body>
</html>
