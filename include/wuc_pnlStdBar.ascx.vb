Imports System.Data
Partial Class include_wuc_pnlStdBar
    Inherits System.Web.UI.UserControl
    Private lngSubModuleID As SubModuleType

    Public Event ExportBtn_Click As EventHandler

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_pnlStdBar"
        End Get
    End Property

    Public Property SubModuleID() As SubModuleType
        Get
            Return lngSubModuleID
        End Get
        Set(ByVal Value As SubModuleType)
            lngSubModuleID = Value
        End Set
    End Property

    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        Try
            RaiseEvent ExportBtn_Click(Me, e)
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


#Region "Export"
    Public Enum ExportType
        EXCEL = 0
        TEXT = 1
    End Enum

    Public ReadOnly Property ExportFileType() As ExportType
        Get
            Return ddlExport.SelectedIndex
        End Get
    End Property

    Public ReadOnly Property ExportContentType() As String
        Get
            Dim iValue As Integer = ddlExport.SelectedIndex
            Dim strContentType As String = "application/vnd.xls"
            Select Case iValue
                Case 0
                    'Excel Format
                    strContentType = "application/vnd.xls"
                Case 1
                    'Text Format
                    strContentType = "application/vnd.text"
                Case Else
                    'strContentType = ""
            End Select
            Return strContentType
        End Get
    End Property

    Public ReadOnly Property ExportFileExtenstion() As String
        Get
            Dim iValue As Integer = ddlExport.SelectedIndex
            Dim strExt As String = ".xls"
            Select Case iValue
                Case 0
                    'Excel Format
                    strExt = ".xls"
                Case 1
                    'Text Format
                    strExt = ".txt"
                Case Else
                    'strExt = ""
            End Select
            Return strExt
        End Get
    End Property

    Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
        Dim ltrField As Literal = New Literal()
        Dim intIdx As Integer
        Dim ctrl As Control = Nothing
        Try
            For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
                ctrl = ctrlHTML.Controls(intIdx)
                If TypeOf ctrl Is LinkButton Then
                    'ltrField.Text = (CType(ctrl, LinkButton)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is HyperLink Then
                    ltrField.Text = (CType(ctrl, HyperLink)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is DropDownList Then
                    ltrField.Text = (CType(ctrl, DropDownList)).SelectedItem.Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is TextBox Then
                    ltrField.Text = (CType(ctrl, TextBox)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is CheckBox Then
                    'ltrField.Text = IIf(CType(ctrl, CheckBox).Checked, "True", "False")
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is ImageButton OrElse TypeOf ctrl Is Image OrElse TypeOf ctrl Is Button Then
                    ctrlHTML.Controls.Remove(ctrl)
                End If
                If ctrl IsNot Nothing AndAlso ctrl.HasControls Then PrepareControlForExport(ctrl)
            Next
        Catch ex As Exception
            ExceptionMsg(ClassName & ".PrepareGridViewForExport : " & ex.ToString)
        End Try

    End Sub


    'Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
    '    Dim ltrField As Literal = New Literal()
    '    Dim intIdx As Integer

    '    Try
    '        For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
    '            If ctrlHTML.Controls(intIdx).GetType().Equals((New LinkButton).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), LinkButton)).Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New HyperLink).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), HyperLink)).Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New DropDownList).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), DropDownList)).SelectedItem.Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New CheckBox).GetType()) Then
    '                ltrField.Text = IIf(CType(ctrlHTML.Controls(intIdx), CheckBox).Checked, "True", "False")
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New ImageButton).GetType()) OrElse _
    '            ctrlHTML.Controls(intIdx).GetType().Equals((New Image).GetType()) Then
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '            End If
    '            If ctrlHTML.Controls(intIdx).HasControls Then PrepareControlForExport(ctrlHTML.Controls(intIdx))
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".PrepareGridViewForExport : " & ex.ToString)
    '    End Try

    'End Sub

    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "")
        Try
            customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            Dim strFileName As String = customFileName & ExportFileExtenstion
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            Response.Charset = ""
            Response.ContentType = ExportContentType '"application/vnd.xls"
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

            'create a emptry row table, act as empty row for insert
            Dim newRow As New HtmlTable()
            newRow.Rows.Add(New HtmlTableRow())

            'Dim pnlInfo As Control
            'pnlInfo = pnlGeneralInfo
            'PrepareControlForExport(pnlInfo)
            'pnlInfo.RenderControl(htmlWrite)

            newRow.RenderControl(htmlWrite) 'insert a empty row

            PrepareControlForExport(dgList)
            dgList.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString)
            Response.End()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "")
        Try
            customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            Dim strFileName As String = customFileName & ExportFileExtenstion
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            Response.Charset = ""
            Response.ContentType = ExportContentType '"application/vnd.xls"
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)


            'create a emptry row table, act as empty row for insert
            Dim newRow As New HtmlTable()
            newRow.Rows.Add(New HtmlTableRow())

            'newRow.RenderControl(htmlWrite) 'insert a empty row
            'Dim pnlInfo As Control
            'pnlInfo = pnlGeneralInfo
            'PrepareControlForExport(pnlInfo)
            'pnlInfo.RenderControl(htmlWrite)

            For Each dgList As Control In aryDgList
                newRow.RenderControl(htmlWrite) 'insert a empty row
                PrepareControlForExport(dgList)
                dgList.RenderControl(htmlWrite)
            Next
            Response.Write(stringWrite.ToString)
            Response.End()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
