﻿Imports System.Xml
Imports System.IO
Imports System.ComponentModel
Imports cor_DB

Public Class clsKMLExporter
    'Public xmlDoc As XmlDocument          ' XML dcoument
    'Private xmlElement As XmlElement  ' the current element

    Private _sbCoordinate As New System.Text.StringBuilder
    Public Property sbCoordinates() As System.Text.StringBuilder
        Get
            Return _sbCoordinate
        End Get
        Set(ByVal value As System.Text.StringBuilder)
            _sbCoordinate = value
        End Set
    End Property


    Private _xmlDoc As XmlDocument
    Public Property xmlDoc() As XmlDocument
        Get
            Return _xmlDoc
        End Get
        Set(ByVal value As XmlDocument)
            _xmlDoc = value
        End Set
    End Property

    Private _docXmlElement As XmlElement
    Public Property xmlElement() As XmlElement
        Get
            Return _docXmlElement
        End Get
        Set(ByVal value As XmlElement)
            _docXmlElement = value
        End Set
    End Property

#Region "Inner Properties"
    'Private _defaultStyleA As String = "styleA"
    'Public Property StyleA() As String
    '    Get
    '        Return _defaultStyleA
    '    End Get
    '    Set(ByVal value As String)
    '        _defaultStyleA = value
    '    End Set
    'End Property
    'Private _defaultStyleB As String = "styleB"
    'Public Property StyleB() As String
    '    Get
    '        Return _defaultStyleB
    '    End Get
    '    Set(ByVal value As String)
    '        _defaultStyleB = value
    '    End Set
    'End Property
    'Private _defaultStyleC As String = "styleC"
    'Public Property StyleC() As String
    '    Get
    '        Return _defaultStyleC
    '    End Get
    '    Set(ByVal value As String)
    '        _defaultStyleC = value
    '    End Set
    'End Property
    'Private _defaultStyleOth As String = "styleOth"
    'Public Property StyleOth() As String
    '    Get
    '        Return _defaultStyleOth
    '    End Get
    '    Set(ByVal value As String)
    '        _defaultStyleOth = value
    '    End Set
    'End Property
#End Region
#Region "TrackPoints"
    'Public Sub addTrackPoints(ByRef drRows As DataRowCollection)
    '    Dim _xmlGroup, _xmlPlaceMark As XmlElement
    '    _xmlGroup = addNewXmlElement("Folder", xmlElement)
    '    addNewXmlElementWithValue("name", "TrackPoint", _xmlGroup)
    '    addNewXmlElementWithValue("open", "1", _xmlGroup)
    '    For Each drRow As DataRow In drRows
    '        _xmlPlaceMark = addNewXmlElement("Placemark", _xmlGroup)
    '        addPlaceMark(_xmlPlaceMark, drRow)
    '    Next

    'End Sub

    Public Sub addPlaceMark(ByVal strName As String, ByVal strDescription As String, ByVal strStyle As String, ByVal strCoordinate As String, ByRef pxmlParent As XmlElement, ByRef drRow As DataRow)
        Dim _xmlPoint As XmlElement
        addNewXmlElementWithValue("name", strName, pxmlParent)
        addNewXmlElementWithValue("description", strDescription, pxmlParent)
        addNewXmlElementWithValue("styleUrl", "#" & strStyle, pxmlParent)
        _xmlPoint = addNewXmlElement("Point", pxmlParent)
        addNewXmlElementWithValue("coordinates", strCoordinate, _xmlPoint)
        sbCoordinates.Append(strCoordinate & " ")
    End Sub
#End Region
#Region "TrackLines"
    'Public Sub addTrackLine(ByVal strCoordinateString As String)
    '    Dim _xmlGroup, _xmlPlaceMark As XmlElement
    '    Dim _xmlLineSytle, _xmlLineString As XmlElement

    '    _xmlGroup = addNewXmlElement("Folder", xmlElement)
    '    addNewXmlElementWithValue("name", "TrackRoute", _xmlGroup)
    '    addNewXmlElementWithValue("open", "1", _xmlGroup)
    '    _xmlPlaceMark = addNewXmlElement("Placemark", _xmlGroup)
    '    addNewXmlElementWithXmlText("Name", String.Format("{0} - {1}_{2}", SalesrepName, SalesrepCode, CallDate), _xmlPlaceMark)
    '    addNewXmlElementWithXmlText("description", String.Format("{0} - {1}_{2}", SalesrepName, SalesrepCode, CallDate), _xmlPlaceMark)
    '    _xmlLineSytle = addNewXmlElement("LineStyle", addNewXmlElement("Style", _xmlPlaceMark))
    '    addNewXmlElementWithValue("color", "ffe600e6", _xmlLineSytle)
    '    addNewXmlElementWithValue("width", "2", _xmlLineSytle)
    '    _xmlLineString = addNewXmlElement("LineString", _xmlPlaceMark)
    '    addNewXmlElementWithValue("tessellate", "1", _xmlLineString)
    '    addNewXmlElementWithValue("coordinates", strCoordinateString, _xmlLineString)
    'End Sub
#End Region

#Region "Core Functions"
    Sub New()
        InitialiseClass()
    End Sub
   
    Private Sub InitialiseClass()
        Dim xmlRoot, xmlSubRoot As XmlElement
        Try
            xmlDoc = New XmlDocument
            xmlDoc.InsertBefore(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", ""), xmlDoc.DocumentElement)

            xmlRoot = xmlDoc.CreateElement("kml")
            xmlRoot.SetAttribute("xmlns", "http://earth.google.com/kml/2.2")

            xmlSubRoot = xmlDoc.CreateElement("Document")
            With xmlSubRoot
                .SetAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2")
                .SetAttribute("xmlns:kml", "http://www.opengis.net/kml/2.2")
                .SetAttribute("xmlns:atom", "http://www.w3.org/2005/Atom")
            End With
            xmlRoot.AppendChild(xmlSubRoot)
            xmlDoc.AppendChild(xmlRoot)
            xmlElement = xmlSubRoot

            
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try

    End Sub
    Public Function addNewXmlElement(ByVal strElementName As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
        addNewXmlElement = xmlDoc.CreateElement(strElementName)
        If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(addNewXmlElement)
        Return addNewXmlElement
    End Function
    Public Function addNewXmlElementWithValue(ByVal strElementName As String, ByVal strElementValue As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
        Dim _xmlElement As XmlElement = xmlDoc.CreateElement(strElementName)
        If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(_xmlElement)
        If Not String.IsNullOrEmpty(strElementValue) Then _xmlElement.InnerText = strElementValue
        Return _xmlElement
    End Function
    Public Function addNewXmlElementWithXmlText(ByVal strElementName As String, ByVal strXmlText As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
        Dim _xmlElement As XmlElement = xmlDoc.CreateElement(strElementName)
        If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(_xmlElement)
        If Not String.IsNullOrEmpty(strXmlText) Then _xmlElement.InnerXml = strXmlText
        Return _xmlElement
    End Function
    Public Sub SaveKML(ByVal strDest As String)
        If IO.Directory.Exists(IO.Directory.GetParent(strDest).FullName) = False Then IO.Directory.CreateDirectory(IO.Directory.GetParent(strDest).FullName)
        Try
            xmlDoc.Save(strDest)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
    End Sub
    Public Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
#End Region
#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            Exit Sub
        End Sub
    End Class

    'Public Overloads Sub Dispose()
    '    Dispose(True)
    '    GC.SuppressFinalize(Me)
    'End Sub

    'Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
    '    If Not (Me.disposed) Then
    '        If (disposing) Then
    '            Components.Dispose()
    '        End If
    '        CloseHandle(handle)
    '        handle = IntPtr.Zero
    '    End If
    '    Me.disposed = True
    'End Sub

    '<System.Runtime.InteropServices.DllImport("Kernel32")> _
    ' Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    'End Function

    'Protected Overrides Sub Finalize()
    '    Dispose(False)
    'End Sub

    'Public Sub Close()
    '    Dispose()
    'End Sub

#End Region
End Class
