<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_MultiAuthen.ascx.vb" Inherits="include_wuc_MultiAuthen" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<link href="~/include/DKSH.css" rel="stylesheet" />

<table id="tblCtrlPnlGen" cellspacing="0" cellpadding="0" width="100%" border="0"  class="cls_panel_header"
    style="float: left; padding-left: 10px;">
    <tr align="center">
        <td style="padding: 3px 0px 5px 0px">
            <asp:Button ID="btnCollapse" Text="Click to Expand or Collapse" runat="server" CssClass="cls_button"
                Width="100%" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdatePnlMultiAuthen" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="PnlMultiAuthen" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td style="width: 45%;">
                                </td>
                                <td style="width: 10%;">
                                </td>
                                <td style="width: 45%;">
                                </td>
                            </tr>
                            <tr id="Team Selection">
                                <td valign="top" align="center">
                                    <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                    <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                        Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                </td>
                                <td valign="middle" style="padding: 0px 0px 0px 10px">
                                    <table>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkTeamAdd" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text=">" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkTeamRemove" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text="<" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkTeamAddAll" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text=">>" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkTeamRemoveAll" runat="server" CssClass="cls_button" Width="50"
                                                    Height="25" Font-Bold="true" Text="<<" /></td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                    <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                        Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr id="Salesrep Selection">
                                <td valign="top" align="center">
                                    <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                    <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                        Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                </td>
                                <td valign="middle" style="padding: 0px 0px 0px 10px">
                                    <table>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkSRAdd" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text=">" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkSRRemove" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text="<" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkSRAddAll" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text=">>" /></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:Button ID="lnkSRRemoveAll" runat="server" CssClass="cls_button" Width="50" Height="25"
                                                    Font-Bold="true" Text="<<" /></td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                    <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                        Height="130px" ForeColor="Black" Width="220px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                 <div id="year" runat="server" style="padding-left:5px; padding-top:10px; padding-bottom: 10px; ">
                                 <span id="lblyear" class="cls_label_header">Year : </span>
                                  <asp:DropDownList ID="ddlyear" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
                                 </div>  
                                 </td>
                            </tr>
                             <tr>
                                <td colspan="3">
                                <asp:Panel ID="pnlProduct" runat="server" Visible="false">
                                 <div id="dPrd" runat="server" style="padding-left:5px; padding-top:10px; padding-bottom: 10px; ">
                                 <span id="sPrd" class="cls_label_header">Product : </span>
                                  <asp:DropDownList ID="ddlPrd" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
                                 </div>   
                                </asp:Panel> 
                                 </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                    <asp:Button ID="btnRefresh" CssClass="cls_button" runat="server" Text="Refresh" ValidationGroup="Search">
                                    </asp:Button>
                                    <asp:Label ID="lblInfo" runat="server" CssClass="cls_label_err"></asp:Label>
                           </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlMultiAuthen" runat="server" CollapseControlID="btnCollapse"
                        ExpandControlID="btnCollapse" TargetControlID="PnlMultiAuthen" CollapsedSize="0"
                        Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
                    </ajaxToolkit:CollapsiblePanelExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>


