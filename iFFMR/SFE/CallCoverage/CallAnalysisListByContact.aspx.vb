Imports System.Data


Partial Class CallAnalysisListByContact
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    '    Dim WithEvents clsCriteriaCollector As clsSharedValue
    '#Region "Criteria Collector"
    '    Dim strCollectorName As String = "Collector_CABYCONT"
    '    Private Property CriteriaCollector() As clsSharedValue
    '        Get
    '            'clsCriteriaCollector = Session(strCollectorName)
    '            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
    '            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
    '            Return clsCriteriaCollector
    '        End Get
    '        Set(ByVal value As clsSharedValue)
    '            clsCriteriaCollector = value
    '            Session(strCollectorName) = clsCriteriaCollector
    '        End Set
    '    End Property

    '    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
    '        Session(strCollectorName) = sender
    '    End Sub

    '#End Region
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CABYCONT) '"Call By Contact"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.CABYCONT
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                ViewState("GroupingValue") = ""
                TimerControl1.Enabled = True
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "Event Handler"
    Public ReadOnly Property PageName() As String
        Get
            Return "CallAnalysisListByContact.aspx"
        End Get
    End Property

    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            'wuc_ctrlPanel.RefreshDetails()
            'wuc_ctrlPanel.UpdateControlPanel()
            If IsPostBack Then RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        Try
            ViewState("GroupingValue") = String.Empty
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    'Private Sub PrepareGridViewForExport(ByRef gvList As GridView)
    '    Dim lb As LinkButton = New LinkButton
    '    Dim l As Literal = New Literal
    '    Dim name As String = String.Empty
    '    Dim i As Integer = 0
    '    While i < gvList.Controls.Count
    '        If gvList.Controls(i).GetType Is GetType(LinkButton) Then
    '            l.Text = (CType(gvList.Controls(i), LinkButton)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(DropDownList) Then
    '            l.Text = (CType(gvList.Controls(i), DropDownList)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(CheckBox) Then
    '            l.Text = (CType(gvList.Controls(i), CheckBox)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        End If

    '        'If gvList.Controls(i).HasControls Then
    '        '    PrepareGridViewForExport(gvList.Controls(i))
    '        'End If
    '        System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
    '    End While
    'End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    'dgList.ShowFooter = False
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 10 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Custom DGList"

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallByContact.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CALLBYCONTACT.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_CallByContact.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CALLBYCONTACT.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CALLBYCONTACT.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim iCounter As Integer = 0
                Dim iActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        iActualIndex = IIf(iCounter > 0, iCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(iActualIndex).RowSpan = 2
                            e.Row.Cells(iActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(iActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        iCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

                Case DataControlRowType.Footer
                    Dim iIndex As Integer
                    Dim intRowCount As Integer = dgList.Rows.Count

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = IIf(IsNumeric(li.Value), CDbl(li.Value), 0) 'String.Format(CF_CallByContact.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next

                    If (aryDataItem.IndexOf("ACH_A")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_A")).Text = String.Format(CF_CALLBYCONTACT.GetOutputFormatString("ACH_A"), (DIVISION(licItemFigureCollector.FindByText("AC_A"), licItemFigureCollector.FindByText("TC_A")) * 100))
                    If (aryDataItem.IndexOf("ACH_B")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_B")).Text = String.Format(CF_CALLBYCONTACT.GetOutputFormatString("ACH_B"), (DIVISION(licItemFigureCollector.FindByText("AC_B"), licItemFigureCollector.FindByText("TC_B")) * 100))
                    If (aryDataItem.IndexOf("ACH_C")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_C")).Text = String.Format(CF_CALLBYCONTACT.GetOutputFormatString("ACH_C"), (DIVISION(licItemFigureCollector.FindByText("AC_C"), licItemFigureCollector.FindByText("TC_C")) * 100))
                    If (aryDataItem.IndexOf("ACH_OTH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_OTH")).Text = String.Format(CF_CALLBYCONTACT.GetOutputFormatString("ACH_OTH"), (DIVISION(licItemFigureCollector.FindByText("AC_OTH"), licItemFigureCollector.FindByText("TC_OTH")) * 100))
                    If (aryDataItem.IndexOf("ACH_TTL")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_TTL")).Text = String.Format(CF_CALLBYCONTACT.GetOutputFormatString("ACH_TTL"), (DIVISION(licItemFigureCollector.FindByText("AC_TTL"), licItemFigureCollector.FindByText("TC_TTL")) * 100))
                    If (aryDataItem.IndexOf("AVG_DAY")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("AVG_DAY")).Text = String.Format(CF_CALLBYCONTACT.GetOutputFormatString("AVG_DAY"), (Convert.ToDouble(licItemFigureCollector.FindByText("AVG_DAY").Value) / CInt(intRowCount)))

            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode As String
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")
            
            Dim strPointerText As String
            'strPointerText = IIf(String.IsNullOrEmpty(strTeamCode), "Team", IIf(String.IsNullOrEmpty(strRegionCode), "Region", "Sales Rep."))
            strPointerText = "Sales Rep."
            'If wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False Then wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
            'Add in salesrep_code when first visit, or when grouping is nothing
            If (IsPostBack = False AndAlso wuc_ctrlpanel.pnlField_IsPointerExist(ListBoxType.lsbShow) = False) OrElse _
                (wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow) = 0) Then
                wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
            End If

            Dim strGroupField As String
            If String.IsNullOrEmpty(ViewState("GroupingValue")) Then
                strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue
            Else
                strGroupField = ViewState("GroupingValue")
                ViewState("GroupingValue") = String.Empty
            End If

            'group by salesrep
            Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
            strGroupField = strGroupField.Replace(strPointer, "SALESREP_CODE")
            wuc_ctrlpanel.pnlField_EditPointerText("Sales Rep.")

            'If Len(strGroupField) = 0 Then
            '    strGroupField = "SALESREP_CODE"
            'Else
            '    strGroupField = strGroupField + ",SALESREP_CODE"
            'End If


            Dim clsSalesDB As New rpt_CALL.clsCallQuery
            DT = clsSalesDB.GetCallByContactClass(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, strGroupField)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)

            Cal_CustomHeader()
            Cal_ItemFigureCollector(DT)


        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            'collect the column name that ready to show on the screen
            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                'If strColumnName Like "TARGET_CALLS*" Then
                If strColumnName Like "*_A" Then
                    strColumnName = "A"
                ElseIf strColumnName Like "*_B" Then
                    strColumnName = "B"
                ElseIf strColumnName Like "*_C" Then
                    strColumnName = "C"
                ElseIf strColumnName Like "*_OTH" Then
                    strColumnName = "OTH"
                ElseIf strColumnName Like "*_TTL" Then
                    strColumnName = "TOTAL CALL"
                Else
                    strColumnName = CF_CALLBYCONTACT.GetDisplayColumnName(strColumnName)
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                '
                '
                If (strColumnName Like "TC*" OrElse _
                    strColumnName Like "AC*" OrElse _
                    strColumnName Like "TTL_DAY*" OrElse _
                    strColumnName Like "AVG_DAY") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class CF_CALLBYCONTACT
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "TC_A", "TC_B", "TC_C", "TC_OTH", "TC_TTL"
                strFieldName = "TC"
            Case "AC_A", "AC_B", "AC_C", "AC_OTH", "AC_TTL"
                strFieldName = "AC"
            Case "ACH_A", "ACH_B", "ACH_C", "ACH_OTH", "ACH_TTL"
                strFieldName = "%"
            Case "DEPT_NAME"
                strFieldName = "Department"
            Case "SPECIALITY_NAME"
                strFieldName = "Specialty"
            Case "TTL_DAY"
                strFieldName = "CD"
            Case "TTL_DAY_HALF"
                strFieldName = "CD"
            Case "AVG_DAY"
                strFieldName = "Avg Call"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If (strColumnName = "DAY_COUNT") Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "DEPT_NAME" Or strColumnName = "SPECIALITY_NAME" Then
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "ACH_A", "ACH_B", "ACH_C", "ACH_OTH", "ACH_TTL", "TTL_DAY_HALF"
                    strStringFormat = "{0:#,0.0}"
                Case "TC_A", "TC_B", "TC_C", "TC_OTH", "TC_TTL", "AVG_DAY"
                    strStringFormat = "{0:#,0}"
                Case "AC_A", "AC_B", "AC_C", "AC_OTH", "AC_TTL", "TTL_DAY"
                    strStringFormat = "{0:#,0}"
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    InvisibleColumn = 99
'End Enum

'Public Class ColumnTemplate_Percentage
'    Implements ITemplate

'    Private _Dividend, _Divisor As String

'    Sub New(ByVal strDividend As String, ByVal strDivisor As String)
'        _Dividend = strDividend
'        _Divisor = strDivisor
'    End Sub

'    Public Sub InstantiateIn(ByVal container As System.Web.UI.Control) Implements System.Web.UI.ITemplate.InstantiateIn
'        Dim l As LiteralControl = New LiteralControl
'        AddHandler l.DataBinding, AddressOf Me.OnDataBinding
'        container.Controls.Add(l)
'    End Sub

'    Public Sub OnDataBinding(ByVal sender As Object, ByVal e As EventArgs)
'        Dim l As LiteralControl = CType(sender, LiteralControl)
'        Dim container As GridViewRow = CType(l.NamingContainer, GridViewRow)
'        Dim dblDividend As Double = IIf(IsNumeric(CType(container.DataItem, DataRowView)(_Dividend)), CType(container.DataItem, DataRowView)(_Dividend), 0)
'        Dim dblDivisor As Double = IIf(IsNumeric(CType(container.DataItem, DataRowView)(_Divisor)), CType(container.DataItem, DataRowView)(_Divisor), 0)

'        If dblDivisor > 0 Then
'            If _Divisor Like "PYTD*" Then

'                l.Text = (((dblDividend / dblDivisor) * 100) - 100).ToString("0.0")
'            Else
'                l.Text = ((dblDividend / dblDivisor) * 100).ToString("0.0")
'            End If

'        Else
'            l.Text = "0.0"
'            If IsNumeric(CType(container.DataItem, DataRowView)(_Divisor)) = False Then l.Text = ""
'        End If
'    End Sub

'End Class




