﻿var map;
//var Default_Latitude = 16.0333;    // VN Latitude
//var Default_Longitude = 106.8500; // VN Longitude
var markers = [];
var infowindow = new google.maps.InfoWindow({
});
var infowindows = [];
var imageIconLightGreen = {
    url: '../../../images/markerlightgreen.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0),
    labelOrigin: new google.maps.Point(13, 13),
};

var imageIconGreen = { // to track the salesrep
    url: '../../../images/markergreen.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0),


};

var imageIconRed = {
    url: '../../../images/markericonred.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconGray = {
    url: '../../../images/markergray.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconYellow = {
    url: '../../../images/markeryellow.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconPurple = {
    url: '../../../images/markerpurple.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconBlue = {
    url: '../../../images/markerblue.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};


var imageIconSalesrep = {
    url: '../../../images/marker_salesman.png',
    size: new google.maps.Size(30, 40),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};

var salesRepCode = "";

function staticIcon(number) {
    var MIcon = imageIconGreen;

    if (number == 1) { MIcon = imageIconRed; }
    else if (number == 2) { MIcon = imageIconLightGreen; }
    else if (number == 3) { MIcon = imageIconGreen; }
    else if (number == 4) { MIcon = imageIconGray; }
    else if (number == 5) { MIcon = imageIconPurple; }
    else if (number == 6) { MIcon = imageIconBlue; }
    else if (number == 7) { MIcon = imageIconYellow; }
    else if (number == 8) { MIcon = imageIconSalesrep; }
    
    return MIcon;
}
function InitializeMap() {
    var Default_Longitude = hfLongitude.value;//'106.695';//hfLongitude.Get("hfLongitude")
    var Default_Latitude = hfLatitude.value;//'10.80444444';//hfLatitude.Get("hfLatitude")
    var mapOptions = {
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        },
        mapTypeControl: true
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    if (map) {
        var point = new google.maps.LatLng(Default_Latitude, Default_Longitude);
        map.setCenter(point, 7);
    }
}

function btnSeachbySalesRepClick() {

    markers = [];
    infowindows = [];
    InitializeMap();

    wuc_ctrlpanel_pnlDailyTrackingSearchDetails.CollapsiblePanelBehavior.set_Collapsed(true);
    wuc_ctrlpanel_pnlDailyTrackingRemark.CollapsiblePanelBehavior.set_Collapsed(false);

    var txtDateStart = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0].value : "";
    var txtDateEnd = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0].value : "";

    $.ajax({
        type: "POST",
        url: "../../../DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking",
        //url: "../../DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking", //http://localhost:1144/iFFMR/DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking
        //data: "{'strSalesrepCode':'" + salesRepCode + "'}",
        //data: "{'strSalesrepCode':'" + $("#ctl00_ctl00_MainPane_Content_MainContent_hfUserSalesrepCode").val() + "','strDistCode':'" + strUserDistCode + "','strRouteCode':'" + $("#ctl00_ctl00_MainPane_Content_MainContent_hfUserRouteCode").val() + "','strCustCode':'" + $("#ctl00_ctl00_MainPane_Content_MainContent_hfUserCustCode").val() + "','strlatitude':'','strlongitude':''}",
        data: "{'strSalesrepCode':'" + salesRepCode + "','strStartDate':'" + txtDateStart + "','strEndDate':'" + txtDateEnd + "'}",
        contentType: "application/json; charset=utf-8",
        success: onSuccessSearchList,
        dataType: "json",
        failure: ajaxCallFailed
    });

}


function refreshMap(strSalesRepCode) {

    salesRepCode = strSalesRepCode;
    console.log("salesRepCode -> " + salesRepCode);
    markers = [];
    infowindows = [];
    InitializeMap();

    var txtDateStart = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0].value : "";
    var txtDateEnd = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0].value : "";

    $.ajax({
        type: "POST",
        url: "../../../DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking",
        //url: "../../DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking", //http://localhost:1144/iFFMR/DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking
        //data: "{'strSalesrepCode':'" + salesRepCode + "'}",
        //data: "{'strSalesrepCode':'" + $("#ctl00_ctl00_MainPane_Content_MainContent_hfUserSalesrepCode").val() + "','strDistCode':'" + strUserDistCode + "','strRouteCode':'" + $("#ctl00_ctl00_MainPane_Content_MainContent_hfUserRouteCode").val() + "','strCustCode':'" + $("#ctl00_ctl00_MainPane_Content_MainContent_hfUserCustCode").val() + "','strlatitude':'','strlongitude':''}",
        data: "{'strSalesrepCode':'" + salesRepCode + "','strStartDate':'" + txtDateStart + "','strEndDate':'" + txtDateEnd + "'}",
        contentType: "application/json; charset=utf-8",
        success: onSuccessSearchList,
        dataType: "json",
        failure: ajaxCallFailed
    });

}


function onSuccessSearchList(response) {

    debugger;
    var result = eval('(' + response.d + ')');
    var lastLoc;

    var strCustCode = 'Customer Code'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strCustName = 'Customer Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var strLocation = 'Location'//'<%= Resources.GEOMAP.lblLocation%>';
    var strSalesrepID = 'Salesman ID'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strSalesrepName = 'Salesman Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var strSalesrepLastDate = 'Salesman last refreshed date'//'<%= Resources.GEOMAP.lblLocation%>';
    var intSalesrepCnt = 1;
    if (result) {
        var froto = [];

        for (var i = 0; i < result.length; i++) {
            var m = result[i];
            var markerColor = parseInt(result[i].COLOR);

            if (i == result.length - 1) { markerColor = 8; }

            var tempLat = parseFloat(result[i].LATITUDE)
            var tempLng = parseFloat(result[i].LONGITUDE)
            var myLatLng = new google.maps.LatLng(parseFloat(result[i].LATITUDE), parseFloat(result[i].LONGITUDE));
            var marker;

            if (markerColor == 7) {
                marker = new google.maps.Marker({
                    position: {
                        lat: tempLat,
                        lng: tempLng,
                    },
                    label: {
                        text: "" + intSalesrepCnt + "",
                        fontWeight: 'bold',
                        fontSize: '16px',
                        fontFamily: '"Courier New", Courier,Monospace',
                        color: 'black'//'#7d7d7d'
                    },
                    icon: staticIcon(markerColor),
                    map: map,
                    infoWindowIndex: i
                });
                intSalesrepCnt++;
            }
            else {
                marker = new google.maps.Marker({
                    position: {
                        lat: tempLat,
                        lng: tempLng,
                    },
                    icon: staticIcon(markerColor),
                    map: map,
                    infoWindowIndex: i
                });
            }
            

            var strcontent = '<div class="map-content">';// + result[i].SALESREP_CODE ;


            //if (i == result.length - 1) {
            if (markerColor == 7 || markerColor == 8) {
                strcontent += '<font color="blue"><b>' + strSalesrepID + ' : ' + '</font></b>' + result[i].SALESREP_CODE + '<br/>';
                strcontent += '<font color="blue"><b>' + strSalesrepName + ' : ' + '</font></b>' + result[i].SALESREP_NAME + '<br/>';
                strcontent += '<font color="blue"><b>' + strSalesrepLastDate + ' : ' + '</font></b>' + result[i].LAST_GPS_DT + '<br/>';
            }
            else {
                strcontent += '<font color="blue"><b>' + strCustCode + ' : ' + '</font></b>' + result[i].CUST_CODE + '<br/>';
                strcontent += '<font color="blue"><b>' + strCustName + ' : ' + '</font></b>' + result[i].CUST_NAME + '<br/>';
            }
            strcontent += '</div>'

            var infowindow = new google.maps.InfoWindow()


            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    closeInfos();
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, strcontent, infowindow));
            

            //var infowindow = new google.maps.InfoWindow()
            //google.maps.event.addListener(marker, 'click', function () {
            //    strContent = "";
            //    closeInfos();
            //    strContent = tempLat + "," + tempLng + " <br/> ";

            //    if (i = result.length - 1) { lastLoc = true; }
            //    else { lastLoc = false; }
            //    setContentcallback(tempLat, tempLng, lastLoc, function (content) {
            //        infowindow.setContent(strContent);
            //    });
                    

            //    infowindow.open(map, marker);

            //});



            if (markerColor == 7 || markerColor == 8) {
                //if (i < result.length - 1) {
                froto.push(new google.maps.LatLng(parseFloat(result[i].LATITUDE), parseFloat(result[i].LONGITUDE)));

                var flightPath = new google.maps.Polyline({
                    path: froto,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });

                flightPath.setMap(map);
            }
        }
        if (result.length > 0) {
            var CenterLatLng = new google.maps.LatLng(result[result.length - 1].LATITUDE, result[result.length - 1].LONGITUDE);
            map.setCenter(CenterLatLng, 10);
            map.setZoom(11);
        } else {
            alert("Records not found!");
        }

    }
}




function onSuccessSearchPrdList(response) {

    debugger;
    var result = eval('(' + response.d + ')');
    var strCustCode = 'Customer Code'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strCustName = 'Customer Name'//'<%= Resources.GEOMAP.lblCustName%> ';

    if (result) {
        var froto = [];

        for (var i = 0; i < result.length; i++) {
            var m = result[i];
            var tempLat = parseFloat(result[i].LATITUDE)
            var tempLng = parseFloat(result[i].LONGITUDE)
            var myLatLng = new google.maps.LatLng(parseFloat(result[i].LATITUDE), parseFloat(result[i].LONGITUDE));
            var marker = new google.maps.Marker({
                position: {
                    lat: tempLat,
                    lng: tempLng,
                },
                icon: staticIcon(parseInt(result[i].COLOR)),
                map: map,
                infoWindowIndex: i
            });


            var strcontent = '<div class="map-content">';// + result[i].SALESREP_CODE ;

            strcontent += '<font color="blue"><b>' + strCustCode + ' : ' + '</font></b>' + result[i].CUST_CODE + '<br/>';
            strcontent += '<font color="blue"><b>' + strCustName + ' : ' + '</font></b>' + result[i].CUST_NAME + '<br/>';
            strcontent += '</div>'

            var infowindow = new google.maps.InfoWindow()


            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    closeInfos();
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, strcontent, infowindow));

            //google.maps.event.addListener(marker, 'click', function () {
            //    strContent = "";
            //    closeInfos();
            //    strContent = tempLat + "," + tempLng + " <br/> ";
            //    setContentcallback(tempLat, tempLng, function (content) {
            //        infowindow.setContent(strContent);
            //    });


            //    infowindow.open(map, this);

            //});

            //froto.push(new google.maps.LatLng(parseFloat(result[i].LATITUDE), parseFloat(result[i].LONGITUDE)));

            //google.maps.event.addListener(marker, 'click', function () {
            //    strContent = "";
            //    closeInfos();
            //    strContent = tempLat + "," + tempLng + " <br/> ";
            //    setContentcallback(tempLat, tempLng, function (content) {
            //        infowindow.setContent(strContent);
            //    });


            //    infowindow.open(map, this);

            //});

        }
        if (result.length > 0) {
            var CenterLatLng = new google.maps.LatLng(result[result.length - 1].LATITUDE, result[result.length - 1].LONGITUDE);
            map.setCenter(CenterLatLng, 10);
            map.setZoom(11);
        } else {
            alert("Records not found!");
        }

    }
}


function setContentcallback(lat, long, lastloc, callback) {
    var response;
    var strCustCode = 'Customer Code'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strCustName = 'Customer Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var strLocation = 'Location'//'<%= Resources.GEOMAP.lblLocation%>';
    var strSalesrepID = 'Salesman ID'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strSalesrepName = 'Salesman Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var strSalesrepLastDate = 'Salesman last refreshed date'//'<%= Resources.GEOMAP.lblLocation%>';

    var txtDateStart = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateStart")[0].value : "";
    var txtDateEnd = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange1_txtDateEnd")[0].value : "";

    //if (result[i].CUST_CODE) {
        //var strUserDistCode = txtUserDistCode.GetValue().substr(0, txtUserDistCode.GetValue().indexOf(' '));

    debugger;

    $.ajax({
        type: "POST",
        url: "../../../DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking",
        //data: "{'strSalesrepCode':'" + salesRepCode + "'}",
        data: "{'strSalesrepCode':'" + salesRepCode + "','strStartDate':'" + txtDateStart + "','strEndDate':'" + txtDateEnd + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var result = eval('(' + response.d + ')');

            if (result) {
                var count = parseFloat(result.length);
                for (var i = 0; i < result.length; i++) {
                    if (i = result.length - 1) {
                        //console.log("show the latest location");
                        strContent = '<font color="blue"><b>' + strSalesrepID + ' : ' + '</font></b>' + result[i].SALESREP_CODE + '<br/>';
                        strContent += '<font color="blue"><b>' + strSalesrepName + ' : ' + '</font></b>' + result[i].SALESREP_NAME + '<br/>';
                        strContent += '<font color="blue"><b>' + strSalesrepLastDate + ' : ' + '</font></b>' + result[i].SALESREP_LAST_LOC_DATE + '<br/>';
                        callback(strContent);
                    }
                    else{
                        strContent = '<font color="blue"><b>' + strCustCode + ' : ' + '</font></b>' + result[i].CUST_CODE + '<br/>';
                        strContent += '<font color="blue"><b>' + strCustName + ' : ' + '</font></b>' + result[i].CUST_NAME + '<br/>';
                        //strContent += '<font color="blue"><b>' + strLocation + ' : ' + '</font></b>' + result[i].LOCATION + '<br/>';
                        callback(strContent);
                    }
                }
            }
        },
        dataType: "json",
        failure: ajaxCallFailed
    });
    //}
}

function closeInfos() {
    strContent = "";
    infowindow.setContent("");
    infowindow.close();
    for (var i = 0; i < infowindows.length; i++) {
        infowindows[i].close();
    }
}

function ajaxCallFailed(error) {
    console.log(error);
}

