Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports txn_order



Partial Class iFFMS_TRA_TRAOrder
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property ShowMessage() As String
        Get
            Return (Session("ShowMessage"))
        End Get
        Set(ByVal value As String)
            Session("ShowMessage") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "TRAOrder.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then
            'TimerControl2.Enabled = True
            If Not String.IsNullOrEmpty(Request.QueryString("SALESREP_CODE")) Then
                txtsalesrepCode.Text = Trim(Request.QueryString("SALESREP_CODE"))
                If ValidateSalesrep() Then
                    TimerControl2.Enabled = True
                End If
            ElseIf Not String.IsNullOrEmpty(Session("SALESREP_CODE")) Then
                txtsalesrepCode.Text = Session("SALESREP_CODE")
                If ValidateSalesrep() Then
                    TimerControl2.Enabled = True
                End If
            End If

            ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
            txtexpdate.Attributes.Add("onblur", "formatDate('" & txtexpdate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            rfvCheckDataType.ErrorMessage = " Invalid Date!(" & "yyyy-MM-dd" & ")"

            'txtPrdCode.Attributes.Add("onkeydown", "EnterToTab();")
            'txtcustomer.Attributes.Add("onkeydown", "EnterToTab();")
            'txtPayerCode.Attributes.Add("onkeydown", "EnterToTab();")
            'txtsalesrepCode.Attributes.Add("onkeydown", "EnterToTab();")

            Load_Submited_KIV_Message()
        End If

        LoadEntertoTab()
        FieldControl()
        lblErr.Text = ""
        lbldtlmsg.Text = ""
        lblhdrmsg.Text = ""

        lblUnitPriceCurrency.Text = Session("country_currency")
        lblPriceCurrency.Text = Session("country_currency")
        lblNetValueCurrency.Text = Session("country_currency")
    End Sub

    Private Sub LoadEntertoTab()
        txtsalesrepCode.Attributes.Add("onkeydown", "EnterToTab();")
        txtSalesrepName.Attributes.Add("onkeydown", "EnterToTab();")
        txtcustomer.Attributes.Add("onkeydown", "EnterToTab();")
        ddlCollBy.Attributes.Add("onkeydown", "EnterToTab();")
        txtCustomerName.Attributes.Add("onkeydown", "EnterToTab();")
        ddlmreason.Attributes.Add("onkeydown", "EnterToTab();")
        txtPayerCode.Attributes.Add("onkeydown", "EnterToTab();")
        ddlstorage.Attributes.Add("onkeydown", "EnterToTab();")
        txtpayername.Attributes.Add("onkeydown", "EnterToTab();")
        txtcarton.Attributes.Add("onkeydown", "EnterToTab();")
        txtsoldto.Attributes.Add("onkeydown", "EnterToTab();")
        txtrefNo.Attributes.Add("onkeydown", "EnterToTab();")
        ddlcontact.Attributes.Add("onkeydown", "EnterToTab();")
        txtremarks.Attributes.Add("onkeydown", "EnterToTab();")
        txtPrdCode.Attributes.Add("onkeydown", "EnterToTab();")
        txtbno.Attributes.Add("onkeydown", "EnterToTab();")
        txtPrdName.Attributes.Add("onkeydown", "EnterToTab();")
        txtexpdate.Attributes.Add("onkeydown", "EnterToTab();")
        ddluom.Attributes.Add("onkeydown", "EnterToTab();")
        txtqty.Attributes.Add("onkeydown", "EnterToTab();")
        txtprice.Attributes.Add("onkeydown", "EnterToTab();")
        ddlreason.Attributes.Add("onkeydown", "EnterToTab();")
    End Sub

#Region "EVENT HANDLER"

    Protected Sub btnAttachment_Click(sender As Object, e As System.EventArgs) Handles btnAttachment.Click
        Dim strScript As String = ""
        strScript = ""
        strScript = strScript + "LoadAttachment();"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "popupImg", strScript, True)
    End Sub

    Protected Sub btnsavedtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavedtl.Click
        If Page.IsValid Then
            SaveHeader()

            If dgList.Rows.Count < 200 Then
                ConfirmedHdrKey()
                If SaveDtl() = True Then
                    ResetDtl()
                    Loadddlreason()
                    UpdateChangedHeader()
                    TimerControl1.Enabled = True
                Else

                End If
            Else
                lbldtlmsg.Text = "Only a maximum of 200 details line can  be added at one time! Currently there are already 200 details lines."
            End If
            UpdateContent.Update()
        End If


    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit2.Click
        If dgList.Rows.Count > 0 Then
            SaveHeader()
        End If

        Dim strurl As String
        strurl = "TRAOrderDtlSubmit.aspx?TXN_NO=" + lbltxnno.Text + "&SESSION_ID=" + GetGuid() + "&SALESREP_CODE=" + lblSalesrepCode.Text _
        + "&VOUCHER_NO=" + lblvouchno.Text + "&VISIT_ID=" + lblvisitid.Text
        Response.Redirect(strurl)

    End Sub

    Protected Sub btnTriggerDivision_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTriggerDivision.Click
        Dim cls_common As New txn_common.clstxncommon
        Dim DT As New DataTable
        DT = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

        If DT.Rows.Count > 0 Then
            For i = 0 To DT.Rows.Count - 1
                If DT.Rows(i)(0) = "Division" Then
                    LoadDDLDivision()
                End If
            Next
        End If
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Reset()
        Response.Redirect("TRAOrder.aspx")

        'ResetHdr()
        'ResetDtl()
        'UnConfirmedHdrKey()
        'TimerControl1.Enabled = True
    End Sub

    Protected Sub btnback_Click(sender As Object, e As System.EventArgs) Handles btnback.Click
        'check whether have rights to upload attachment
        Dim cls_common As New txn_common.clstxncommon
        Dim DT = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

        If DT.Rows.Count > 0 Then
            For i = 0 To DT.Rows.Count - 1
                If DT.Rows(i)(0) = "Attachment" Then
                    If btnAttachment.Visible <> False And dgList.Rows.Count > 0 Then

                    Else
                        Dim strScript As String = ""
                        strScript = ""

                        strScript = strScript + "clearAttachment();"
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "popupImg", strScript, True)
                    End If
                End If
            Next
        End If
        

    End Sub

    Protected Sub txtcustomer_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcustomer.TextChanged
        '  ValidateCust()
        ' UpdateContent.Update()
    End Sub

    Protected Sub txtPrdCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrdCode.TextChanged
        ' ValidatePrd()
        ' UpdateContent.Update()
    End Sub

    Protected Sub txtsalesrepCode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtsalesrepCode.TextChanged
        If ValidateSalesrep() Then
            TimerControl2.Enabled = True
        End If
    End Sub

    Protected Sub imgfix_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgfix.Click
        With (Wuc_CustPayersearch2)
            .BindDefault()
            .Show()
            .Hide()
        End With
    End Sub
#End Region

#Region "Customer"
    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCust.Click
        With (Wuc_CustPayersearch2)
            .ResetPage()
            .BindDefault()
            .SalesrepCode = Trim(txtsalesrepCode.Text)
            .CustCode = Trim(txtcustomer.Text)
            .PayerCode = Trim(txtPayerCode.Text)
            .Show()
        End With

    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_CustPayersearch2.SelectButton_Click

        txtcustomer.Text = Wuc_CustPayersearch2.ShipToCode
        txtCustomerName.Text = Wuc_CustPayersearch2.ShipToName
        txtsoldto.Text = Wuc_CustPayersearch2.CustCode + "-" + Wuc_CustPayersearch.CustName
        txtPayerCode.Text = Wuc_CustPayersearch2.PayerCode
        txtpayername.Text = Wuc_CustPayersearch2.PayerName

        'check whether have rights to upload attachment
        Dim cls_common As New txn_common.clstxncommon
        Dim DT = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

        If DT.Rows.Count > 0 Then
            For i = 0 To DT.Rows.Count - 1
                If DT.Rows(i)(0) = "Attachment" Then
                    lblAttachment.Style("display") = "block"
                    btnAttachment.Style("display") = "block"
                    'lblAttachment.Visible = True
                    'btnAttachment.Visible = True
                End If
                If DT.Rows(i)(0) = "Division" Then
                    LoadDDLDivision()
                End If
            Next
        End If
        UpdateContent.Update()

    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_CustPayersearch2.CloseButton_Click

        Wuc_CustPayersearch2.Hide()

    End Sub
#End Region

#Region "Product"
    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        With (wuc_PrdSearch)
            .ResetPage()
            If lblSalesrepCode.Visible = True Then
                .SalesrepCode = lblSalesrepCode.Text
            Else
                .SalesrepCode = txtsalesrepCode.Text
            End If
            .BindDefault()
            .Show()
        End With

    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click

        Dim txtProductCode As TextBox = txtPrdCode 'CType(DetailsView1.FindControl("txtCustCode"), TextBox)
        txtProductCode.Text = wuc_PrdSearch.PrdCode
        Dim txtProductName As TextBox = txtPrdName
        txtProductName.Text = wuc_PrdSearch.PrdName

        LoadddlUOM(wuc_PrdSearch.PrdCode, Trim(txtsalesrepCode.Text))
        UpdateContent.Update()

    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click

        wuc_PrdSearch.Hide()

    End Sub
#End Region

#Region "CustomerPayer"
    Protected Sub btnSearchCustPayer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCustPayer.Click
        With (Wuc_CustPayersearch)
            .ResetPage()
            .BindDefault()
            .SalesrepCode = Trim(txtsalesrepCode.Text)
            .CustCode = Trim(txtcustomer.Text)
            .PayerCode = Trim(txtPayerCode.Text)
            .Show()
        End With

    End Sub

    Protected Sub btnSelectCustPayerSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_CustPayersearch.SelectButton_Click
        Try

            txtcustomer.Text = Wuc_CustPayersearch.ShipToCode
            txtCustomerName.Text = Wuc_CustPayersearch.ShipToName
            txtsoldto.Text = Wuc_CustPayersearch.CustCode + "-" + Wuc_CustPayersearch.CustName
            txtPayerCode.Text = Wuc_CustPayersearch.PayerCode
            txtpayername.Text = Wuc_CustPayersearch.PayerName

            UpdateContent.Update()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCloseCustPayerSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_CustPayersearch.CloseButton_Click

        Wuc_CustPayersearch.Hide()

    End Sub
#End Region

#Region "Salesrep"
    Protected Sub btnSearchSR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchSR.Click
        With (wuc_SRSearch)
            .ResetPage()
            .BindDefault()
            .Show()
        End With

    End Sub

    Protected Sub btnSelectSRSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_SRSearch.SelectButton_Click
        Try

            ResetHdr()
            txtsalesrepCode.Text = wuc_SRSearch.SalesrepCode
            txtSalesrepName.Text = wuc_SRSearch.SalesrepName
            lblsalesArea.Text = wuc_SRSearch.SalesAreaCode + "-" + wuc_SRSearch.SalesAreaName
            UpdateContent.Update()
            TimerControl2.Enabled = True

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnCloseSRSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_SRSearch.CloseButton_Click

        wuc_SRSearch.Hide()

    End Sub
#End Region

#Region "Header"
    Private Sub CreateHdrNewRecord()
  
        If IsNothing(Request.QueryString("TXN_NO")) Then
            LoadTxnNo()
        Else
            GetGuid() = Trim(Request.QueryString("SESSION_ID"))
            txtsalesrepCode.Text = Trim(Request.QueryString("SALESREP_CODE"))
            lbltxnno.Text = Trim(Request.QueryString("TXN_NO"))
            lblvouchno.Text = Trim(Request.QueryString("VOUCHER_NO"))
            lblvisitid.Text = Trim(Request.QueryString("VISIT_ID"))

        End If


        LoadddlCollBy()
        LoadddlMReason()
        LoadddlStoreLocal()
        Loadddlcontact()
        Loadddlreason()

        'Get session information
        LoadHdr()
        UpdateContent.Update()
        TimerControl1.Enabled = True


    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        If TimerControl2.Enabled Then
            GetExistingSessionID()
            CreateHdrNewRecord()

        End If
        TimerControl2.Enabled = False
    End Sub

    Private Sub ConfirmedHdrKey()
        lblSalesrepCode.Text = txtsalesrepCode.Text
        lblSalesrepCode.Visible = True
        txtsalesrepCode.Visible = False
        btnSearchSR.Visible = False

        lblSalesrepName.Text = txtSalesrepName.Text
        lblSalesrepName.Visible = True
        txtSalesrepName.Visible = False

        lblcustomer.Text = txtcustomer.Text
        lblcustomer.Visible = True
        txtcustomer.Visible = False
        btnSearchCust.Visible = False

        lblcustomername.Text = txtCustomerName.Text
        lblcustomername.Visible = True
        txtCustomerName.Visible = False


        lblcontact.Text = ddlcontact.SelectedItem.Text
        lblcontact.Visible = True
        ddlcontact.Visible = False


        lblPayerCode.Text = txtPayerCode.Text
        lblPayerCode.Visible = True
        txtPayerCode.Visible = False
        btnSearchCustPayer.Visible = False

        lblpayerName.Text = txtpayername.Text
        lblpayerName.Visible = True
        txtpayername.Visible = False

        lblsoldto.Text = txtsoldto.Text
        lblsoldto.Visible = True
        txtsoldto.Visible = False

        btnKIV.Enabled = True
        btnSubmit2.Enabled = True
    End Sub

    Private Sub UnConfirmedHdrKey()
        lblSalesrepCode.Text = ""
        lblSalesrepCode.Visible = False
        txtsalesrepCode.Visible = True
        btnSearchSR.Visible = True

        lblSalesrepName.Text = ""
        lblSalesrepName.Visible = False
        txtSalesrepName.Visible = True

        lblcustomer.Text = ""
        lblcustomer.Visible = False
        txtcustomer.Visible = True
        btnSearchCust.Visible = True

        lblcustomername.Text = ""
        lblcustomername.Visible = False
        txtCustomerName.Visible = True


        lblcontact.Text = ""
        lblcontact.Visible = False
        ddlcontact.Visible = True

        lblPayerCode.Text = ""
        lblPayerCode.Visible = False
        txtPayerCode.Visible = True
        btnSearchCustPayer.Visible = True

        lblpayerName.Text = ""
        lblpayerName.Visible = False
        txtpayername.Visible = True

        txtsoldto.Text = ""
        lblsoldto.Visible = False
        txtsoldto.Visible = True

        btnKIV.Enabled = False
        btnSubmit2.Enabled = False
    End Sub

    Private Sub LoadHdr()
        Dim HdrDT As DataTable
        Try

            HdrDT = GetHdrRecord()

            If HdrDT.Rows.Count > 0 Then
                txtsalesrepCode.Text = HdrDT.Rows(0)("SALESREP_CODE").ToString
                txtSalesrepName.Text = HdrDT.Rows(0)("SALESREP_NAME").ToString
                txtcustomer.Text = HdrDT.Rows(0)("CUST_CODE").ToString
                txtCustomerName.Text = HdrDT.Rows(0)("CUST_NAME").ToString
                ddlcontact.SelectedValue = HdrDT.Rows(0)("CONT_CODE").ToString
                txtPayerCode.Text = HdrDT.Rows(0)("PAYER_CODE").ToString
                txtpayername.Text = HdrDT.Rows(0)("PAYER_NAME").ToString
                txtsoldto.Text = HdrDT.Rows(0)("SOLDTO_NAME").ToString
                ddlCollBy.SelectedValue = HdrDT.Rows(0)("COLL_CODE").ToString
                ddlmreason.SelectedValue = HdrDT.Rows(0)("REASON_CODE").ToString
                ddlstorage.SelectedValue = HdrDT.Rows(0)("SLOC_CODE").ToString
                txtcarton.Text = HdrDT.Rows(0)("CTN_NO").ToString
                txtrefNo.Text = HdrDT.Rows(0)("REF_NO").ToString
                txtremarks.Text = HdrDT.Rows(0)("REMARKS").ToString
                txtSAPBillingNo.Text = Portal.Util.GetValue(Of String)(HdrDT.Rows(0)("SAP_BILLING").ToString)
                ConfirmedHdrKey()

                If txtcustomer.Text IsNot Nothing Then
                    'check whether have rights to upload attachment
                    Dim cls_common As New txn_common.clstxncommon
                    Dim DT = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

                    If DT.Rows.Count > 0 Then
                        For i = 0 To DT.Rows.Count - 1
                            If DT.Rows(i)(0) = "Attachment" Then
                                lblAttachment.Style("display") = "block"
                                btnAttachment.Style("display") = "block"
                                'lblAttachment.Visible = True
                                'btnAttachment.Visible = True
                            End If
                        Next
                    End If
                End If

            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ResetHdr()
        lblvouchno.Text = ""
        lbltxnno.Text = ""
        lblvisitid.Text = ""
        txtsalesrepCode.Text = ""
        txtSalesrepName.Text = ""
        txtcustomer.Text = ""
        txtCustomerName.Text = ""
        txtcarton.Text = 0
        txtrefNo.Text = ""
        txtremarks.Text = ""
        txtPayerCode.Text = ""
        txtpayername.Text = ""
        With ddlcontact
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        With ddlCollBy
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        With ddlmreason
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        With ddlstorage
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        With ddluom
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        lblvouchno.Text = ""
        lbltxnno.Text = ""
        lblvisitid.Text = ""
        
    End Sub

    Private Function GetHdrRecord() As DataTable

        Dim DT As DataTable = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTXNNo As String, strUserid As String, strsessionid As String
            strTXNNo = lbltxnno.Text
            strUserid = Web.HttpContext.Current.Session("UserID")
            strsessionid = GetGuid

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            DT = clsTRAOrder.GetTMPTRAHeader(strTXNNo, strUserid, strsessionid)
        End If

        Return DT
    End Function

    Private Sub SaveHeader()

        Dim strSessionId As String, strTxnNo As String, strvisitid As String, strvoucherno As String, _
               strcustcode As String, strcontcode As String, strPayercode As String, strcollcode As String, strReasonCode As String, strsloccode As String, _
               strctnno As String, strrefno As String, strRemarks As String, strSalesrepCode As String, strUserId As String, strUserName As String, strSAPBilling As String, _
               strCustDivision As String

        strSessionId = GetGuid
        strTxnNo = lbltxnno.Text
        strvisitid = lblvisitid.Text
        strvoucherno = lblvouchno.Text
        strcustcode = txtcustomer.Text
        strcontcode = ddlcontact.SelectedValue.ToString
        strPayercode = txtPayerCode.Text
        strcollcode = ddlCollBy.SelectedValue.ToString
        strReasonCode = ddlmreason.SelectedValue.ToString
        strsloccode = ddlstorage.SelectedValue.ToString
        strctnno = txtcarton.Text
        strrefno = txtrefNo.Text
        strRemarks = txtremarks.Text
        strSalesrepCode = txtsalesrepCode.Text
        strSAPBilling = txtSAPBillingNo.Text
        strUserId = Web.HttpContext.Current.Session("UserID").ToString
        strUserName = Web.HttpContext.Current.Session("UserName").ToString

        If ddlDivision.Visible = True Then
            strCustDivision = ddlDivision.SelectedValue.ToString
        Else
            strCustDivision = ""
        End If
        Dim clsTRAOrder As New txn_order.clsTRAOrder
        clsTRAOrder.InstTMPTRAHeader(strSessionId, strTxnNo, strvisitid, strvoucherno, strcustcode, strcontcode, strPayercode, _
        strcollcode, strReasonCode, strsloccode, strctnno, strrefno, strRemarks, strSalesrepCode, strUserId, strUserName, strSAPBilling, strCustDivision)

    End Sub

    Private Sub LoadTxnNo()
        Dim DTTXNno As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        DTTXNno = clsTRAOrder.GetTRANo(Trim(txtsalesrepCode.Text))
        lbltxnno.Text = DTTXNno.Rows(0)("TXN_NO")
        lblvouchno.Text = DTTXNno.Rows(0)("VOUCH_NO")
        lblvisitid.Text = DTTXNno.Rows(0)("VISIT_ID")
    End Sub

    Private Sub LoadDDLDivision()
        Dim cls_common As New txn_common.clstxncommon
        Dim default_value As New DataTable
        default_value = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "division_default_value")

        Dim dt As DataTable
        Dim clsTraOrder As New txn_order.clsTRAOrder
        dt = clsTraOrder.GetCustDivision(txtcustomer.Text)
        With ddlDivision
            .Items.Clear()
            .DataSource = dt.DefaultView
            .DataTextField = "DIVISION"
            .DataValueField = "DIVISION"
            .DataBind()
            .SelectedIndex = ddlDivision.Items.IndexOf(ddlDivision.Items.FindByValue(default_value.Rows(0)(0))) 'SD'
        End With

    End Sub

    Private Sub LoadddlCustomer()

        Dim dtcustomer As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        dtcustomer = clsTRAOrder.GetSRCustList(txtsalesrepCode.Text)
        'With ddlcustomer
        '    .Items.Clear()
        '    .DataSource = dtcustomer.DefaultView
        '    .DataTextField = "CUST_DESC"
        '    .DataValueField = "CUST_CODE"
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("-- SELECT --", ""))
        '    .SelectedIndex = 0
        'End With
    End Sub

    Private Sub LoadddlCollBy()

        Dim dtcollby As New DataTable
        Dim clsTRA As New txn_WebActy.clsTRA

        dtcollby = clsTRA.GetCollByList
        With ddlCollBy
            .Items.Clear()
            .DataSource = dtcollby.DefaultView
            .DataTextField = "COLL_DESC"
            .DataValueField = "COLL_CODE"
            .DataBind()
             .Items.Insert(0, New ListItem("-- SELECT --", ""))
            '.Items.Insert(1, New ListItem("COLL001-SR", "COLL001"))
            '.Items.Insert(2, New ListItem("COLL002-TRANSPORTER", "COLL002"))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub LoadddlMReason()

        Dim dtreason As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        dtreason = clsTRAOrder.GetSRMReasonList(txtsalesrepCode.Text)
        If dtreason.Rows.Count > 0 Then
            With ddlmreason
                .Items.Clear()
                .DataSource = dtreason.DefaultView
                .DataTextField = "REASON_DESC"
                .DataValueField = "REASON_CODE"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Private Sub LoadddlStoreLocal()

        Dim dtstorelocal As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder


        dtstorelocal = clsTRAOrder.GetStorLocalList(txtsalesrepCode.Text)
        If dtstorelocal.Rows.Count > 0 Then
            With ddlstorage
                .Items.Clear()
                .DataSource = dtstorelocal.DefaultView
                .DataTextField = "REASON_DESC"
                .DataValueField = "REASON_CODE"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Private Sub Loadddlcontact()

        Dim dtcontact As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        dtcontact = clsTRAOrder.GetSRCustContList(Session("SALESREP_CODE"), txtcustomer.Text)
        With ddlcontact
            .Items.Clear()
            .DataSource = dtcontact.DefaultView
            .DataTextField = "CONT_DESC"
            .DataValueField = "CONT_CODE"
            .DataBind()
            '.Items.Insert(0, New ListItem("-- SELECT --", ""))
            .SelectedIndex = 0
        End With
        'ddlcontact.Items.Clear()
        'ddlcontact.Items.Insert(0, New ListItem("SG00000000-DEFAULT", "SG00000000"))
        'ddlcontact.SelectedIndex = 0
    End Sub

    Private Function ValidateSalesrep() As Boolean
        Dim dt As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        Dim strUserId As String, strSalesrepCode As String
        strUserId = Web.HttpContext.Current.Session("UserID")
        strSalesrepCode = Trim(txtsalesrepCode.Text)

        dt = clsTRAOrder.ValidateSalesrep(strUserId, strSalesrepCode)

        If dt.Rows.Count <= 0 Then
            'lblhdrmsg.Text = Trim(txtsalesrepCode.Text) + " is an invalid salesrep code ! Kindly re-enter the salesrep code again."
            txtsalesrepCode.Text = ""
            txtSalesrepName.Text = "invalid salesrep code ! Kindly re-enter the salesrep code again."
            Return False
        Else
            txtSalesrepName.Text = dt.Rows(0)("SALESREP_NAME")
            lblsalesArea.text = dt.Rows(0)("SALES_AREA_NAME")
            Return True
        End If

    End Function

    Private Sub UpdateChangedHeader()

        If Not IsNothing(lblcurrenttxnno.Text) And lblcurrenttxnno.Text <> "" Then

            Dim strTxnNo As String = lbltxnno.Text
            Dim strUserId As String = Web.HttpContext.Current.Session("UserID").ToString
            Dim strsessionid As String = GetGuid()
            Dim strUserName As String = Web.HttpContext.Current.Session("UserName").ToString
            Dim clsTRAOrder As New txn_order.clsTRAOrder
            clsTRAOrder.UpdTMPTRAHeader(strTxnNo, strUserId, strsessionid, strUserName)
        End If
    End Sub

#End Region

#Region " Details"
    Private Function SaveDtl() As Boolean

        Dim strSalesrepCode As String = txtsalesrepCode.Text

        If lbltxnno.Text <> "" Then
            Dim strlineno As Integer
            strlineno = lbllineno.Text
            'NEW save 
            If strlineno = 0 Then

                Dim strSessionId As String, strTxnNo As String, strPrdCode As String, strUomCode As String, _
                strBNo As String, strRetQty As String, strListPrice As String, strExpDate As String, strRetAmt As String, _
                strReasonCode As String, strRemarks As String, strUserId As String, strUserName As String

                strSessionId = GetGuid
                strTxnNo = Trim(lbltxnno.Text)
                strPrdCode = Trim(txtPrdCode.Text)
                strUomCode = Trim(txtselecteduom.Text) 'ddluom.SelectedItem.Text
                strBNo = Trim(txtbno.Text)
                strRetQty = Trim(txtqty.Text)
                strListPrice = Trim(txtprice.Text)
                strExpDate = Trim(txtexpdate.Text)
                strRetAmt = Trim(lblamount.Text)
                strReasonCode = Trim(ddlreason.SelectedValue.ToString)
                strRemarks = "" 'txtdetailremarks.Text
                strUserId = Web.HttpContext.Current.Session("UserID").ToString
                strUserName = Web.HttpContext.Current.Session("UserName").ToString

                If (Len(strBNo) > 0 And Len(strExpDate) = 0) Then
                    lbldtlmsg.Text = "Expiry date required for batch number " + strBNo
                    ReloadddlUOM(strPrdCode, strSalesrepCode, strUomCode)
                    Return False
                ElseIf (Len(strExpDate) > 0 And Len(strBNo) = 0) Then
                    lbldtlmsg.Text = "Batch number required for expiry date " + strExpDate
                    ReloadddlUOM(strPrdCode, strSalesrepCode, strUomCode)
                    Return False
                Else
                    Dim i As Integer = 0
                    Dim exists As Boolean
                    If dgList.Rows.Count > 0 Then
                        For Each DR As GridViewRow In dgList.Rows
                            If Trim(dgList.Rows(i).Cells(3).Text) = Trim(strPrdCode) And Trim(dgList.Rows(i).Cells(8).Text) = Trim(strUomCode) And Trim(dgList.Rows(i).Cells(5).Text) = Trim(strBNo) Then
                                exists = True
                            End If
                            i += 1
                        Next
                    End If
                    'NEW save invalid
                    If exists = True Then
                        lbldtlmsg.Text = "Product code '" + strPrdCode + "', UOM Code '" + strUomCode + "' and Batch '" + strBNo + "' already exists!!"
                        ReloadddlUOM(strPrdCode, strSalesrepCode, strUomCode)
                        Return False
                    Else 'NEW save valid
                        Dim clsTRAOrder As New txn_order.clsTRAOrder
                        clsTRAOrder.InstTMPTRADetails(strSessionId, strTxnNo, strPrdCode, strUomCode, strBNo, strRetQty, strListPrice, strExpDate, _
                        strRetAmt, strReasonCode, strRemarks, strUserId, strUserName)
                        Return True
                    End If
                End If

            Else
                'Edit save 
                Dim strSessionId As String, strTxnNo As String, strPrdCode As String, strUomCode As String, _
                          strBNo As String, strRetQty As String, strListPrice As String, strExpDate As String, strRetAmt As String, _
                          strReasonCode As String, strRemarks As String, strUserId As String, strUserName As String

                strSessionId = GetGuid
                strTxnNo = Trim(lbltxnno.Text)
                strPrdCode = Trim(txtPrdCode.Text)
                strUomCode = Trim(txtselecteduom.Text) 'ddluom.SelectedItem.Text
                strBNo = Trim(txtbno.Text)
                strRetQty = Trim(txtqty.Text)
                strListPrice = Trim(txtprice.Text)
                strExpDate = Trim(txtexpdate.Text)
                strRetAmt = Trim(lblamount.Text)
                strReasonCode = Trim(ddlreason.SelectedValue.ToString)
                strRemarks = "" 'txtdetailremarks.Text
                strUserId = Web.HttpContext.Current.Session("UserID").ToString
                strUserName = Web.HttpContext.Current.Session("UserName").ToString

                If (Len(strBNo) > 0 And Len(strExpDate) = 0) Then
                    lbldtlmsg.Text = "Expiry date required for batch number " + strBNo
                    ReloadddlUOM(strPrdCode, strSalesrepCode, strUomCode)
                    Return False
                ElseIf (Len(strExpDate) > 0 And Len(strBNo) = 0) Then
                    lbldtlmsg.Text = "Batch number required for expiry date " + strExpDate
                    ReloadddlUOM(strPrdCode, strSalesrepCode, strUomCode)
                    Return False
                Else
                    Dim clsTRAOrder As New txn_order.clsTRAOrder
                    clsTRAOrder.UpdTMPTRADetails(strSessionId, strTxnNo, strPrdCode, strUomCode, strBNo, strRetQty, strListPrice, strExpDate, _
                    strRetAmt, strReasonCode, strRemarks, strUserId, strlineno, strUserName)

                End If

                Return True
            End If


        End If
    End Function

    Private Sub ReloadddlUOM(ByVal strPrdCode As String, ByVal strSalesrepCode As String, ByVal strUOMCode As String)
        LoadddlUOM(strPrdCode, strSalesrepCode)
        ddluom.SelectedIndex = ddluom.Items.IndexOf(ddluom.Items.FindByText(strUOMCode))
        txtselecteduom.Text = strUOMCode
        lbluomprice.Text = ddluom.SelectedValue

    End Sub

    Private Sub LoadddlUOM(ByVal strPrdCode As String, ByVal strSalesrepCode As String)
        Dim dtuom As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        dtuom = clsTRAOrder.GetUOMCode(strPrdCode, strSalesrepCode)
        If dtuom.Rows.Count > 0 Then
            With ddluom
                .Items.Clear()
                .DataSource = dtuom.DefaultView
                .DataTextField = "UOM_CODE"
                .DataValueField = "UOM_PRICE"
                .DataBind()
                .SelectedIndex = 0
            End With
            txtselecteduom.Text = dtuom.Rows(0)("UOM_CODE")
            lbluomprice.Text = dtuom.Rows(0)("UOM_PRICE")
        End If

    End Sub

    'Private Sub LoadddlUOMPrice()
    '    Dim struomprice As Double

    '    Dim clsTRAOrder As New txn_order.clsTRAOrder
    '    struomprice = clsTRAOrder.GetUOMPrice(txtPrdCode.Text.ToString, ddluom.SelectedValue.ToString)
    '    lbluomprice.Text = struomprice
    'End Sub

    Private Sub Loadddlreason()
        Dim dtreason As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        dtreason = clsTRAOrder.GetReasonList
        If dtreason.Rows.Count > 0 Then
            With ddlreason
                .Items.Clear()
                .DataSource = dtreason.DefaultView
                .DataTextField = "REASON_DESC"
                .DataValueField = "REASON_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then
            RefreshDatabinding()
        End If
        TimerControl1.Enabled = False
    End Sub

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        'dgList.PageIndex = 0
        'wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub


#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        dtCurrentTable = GetRecList()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            'If dtCurrentTable.Rows.Count = 0 Then
            '    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            '    Master_Row_Count = 0
            'Else
            'show/hide attachment field
            
            Master_Row_Count = dtCurrentTable.Rows.Count
            'End If
        End If

        PreRenderMode(dtCurrentTable)

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgList
            .DataSource = dvCurrentView
            .PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        lblrowcount.Text = " " + CStr(dgList.Rows.Count) + " row(s) added to detail"
        SumTotalAmt()
        UpdateDatagrid_Update()

    End Sub

    Private Sub SumTotalAmt()
        If dgList.Rows.Count > 0 Then
            Dim i As Integer = 0
            Dim Total As Decimal = 0
            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                DK = dgList.DataKeys(i)
                Dim ret_amt As Integer
                ret_amt = aryDataItem.IndexOf("ret_amt") 'ret amt column need to change  
                Total = Total + CDec(dgList.Rows(i).Cells(ret_amt).Text)
                i += 1
            Next

            lbltotal.Text = String.Format("{0:#,0.00}", Total) 'Total
        Else

            lbltotal.Text = 0
        End If
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)

        dgList_Init(DT)

    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTXNNo As String, strUserid As String, strsessionid As String
            strTXNNo = lbltxnno.Text
            strUserid = Web.HttpContext.Current.Session("UserID")
            strsessionid = GetGuid

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            DT = clsTRAOrder.GetTMPTRADetails(strTXNNo, strUserid, strsessionid)

            If DT.Rows.Count > 0 Then
                Dim cls_common As New txn_common.clstxncommon
                Dim DT2 = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

                If DT2.Rows.Count > 0 Then
                    For i = 0 To DT2.Rows.Count - 1
                        If DT2.Rows(i)(0) = "Attachment" Then
                            lblAttachment.Style("display") = "block"
                            btnAttachment.Style("display") = "block"
                            'lblAttachment.Visible = True
                            'btnAttachment.Visible = True
                        End If
                    Next
                End If

                btnsubmit.Visible = True
            End If
        End If

        Return DT
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing


        aryDataItem.Clear()
        'dgList.Columns.Clear()
        While dgList.Columns.Count > 2
            dgList.Columns.RemoveAt(2)
        End While

        'aryDataItem.Add("BTN_BUTTONS")
        aryDataItem.Add("BTN_DELETE")
        dgList.Columns(0).HeaderText = "Delete"
        dgList.Columns(0).HeaderStyle.Width = "25"
        dgList.Columns(0).HeaderStyle.HorizontalAlign = HorizontalAlign.Center
        dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center

        aryDataItem.Add("BTN_EDIT")
        dgList.Columns(1).HeaderText = "Edit"
        dgList.Columns(1).HeaderStyle.Width = "25"
        dgList.Columns(1).HeaderStyle.HorizontalAlign = HorizontalAlign.Center
        dgList.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center


        'ADD BUTTON DELETE
        'If Master_Row_Count > 0 Then 'AndAlso _
        ' Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCE, SubModuleAction.Edit) Then 'HL: 20080429 (AR)

        'Dim dgBtnColumn As New CommandField

        'dgBtnColumn.HeaderStyle.Width = "50"
        'dgBtnColumn.HeaderText = "Delete"
        'dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

        'If dtToBind.Rows.Count > 0 Then
        '    dgBtnColumn.ButtonType = ButtonType.Link
        '    dgBtnColumn.ShowDeleteButton = True
        '    dgBtnColumn.DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
        '    dgBtnColumn.Visible = True
        'End If

        'dgList.Columns.Add(dgBtnColumn)
        'dgBtnColumn = Nothing
        'aryDataItem.Add("BTN_DELETE")
        'End If

        'ADD BUTTON EDIT
        'If Master_Row_Count > 0 Then 'AndAlso _
        ' Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCE, SubModuleAction.Edit) Then 'HL: 20080429 (AR)

        'Dim dgBtnColumn As New CommandField

        'dgBtnColumn.HeaderStyle.Width = "50"
        'dgBtnColumn.HeaderText = "Edit"
        'dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

        'If dtToBind.Rows.Count > 0 Then
        '    dgBtnColumn.ButtonType = ButtonType.Button
        '    dgBtnColumn.ShowEditButton = True
        '    dgBtnColumn.EditText = "Edit" '"<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
        '    dgBtnColumn.Visible = True
        '    dgBtnColumn.CssClass = "cls_button"
        'End If

        'dgList.Columns.Add(dgBtnColumn)
        'dgBtnColumn = Nothing
        'aryDataItem.Add("BTN_EDIT")
        'End If

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_TRAOrder.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_TRAOrder.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_TRAOrder.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_TRAOrder.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_TRAOrder.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing
                    aryDataItem.Add(ColumnName)
            End Select
        Next

        aryDataItem = aryDataItem

    End Sub

    Public Sub UpdateDatagrid_Update()

        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If

        UpdateDatagrid.Update()
        UpdateContent.Update()
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header

                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then

                        Dim btnDelete As ImageButton = CType(e.Row.FindControl("Delete"), ImageButton)
                        If btnDelete Is Nothing Then
                            btnDelete = New ImageButton
                            btnDelete.ID = "btnDelete"
                            btnDelete.CommandName = "Delete"
                            btnDelete.CommandArgument = e.Row.RowIndex
                            btnDelete.CssClass = "cls_button"
                            btnDelete.Width = 20
                            btnDelete.ImageUrl = "..\..\images\ico_Delete.gif"
                            'btnDelete.Text = "Delete"
                            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to Delete the selected record(s)?');")
                            e.Row.Cells(0).Controls.Add(btnDelete)
                            Dim lit As New Literal
                            lit.Text = " "
                            e.Row.Cells(0).Controls.Add(lit)
                        End If

                        Dim btnEdit As ImageButton = CType(e.Row.FindControl("Edit"), ImageButton)
                        If btnEdit Is Nothing Then
                            btnEdit = New ImageButton
                            btnEdit.ID = "btnEdit"
                            btnEdit.CommandName = "Edit"
                            btnEdit.CommandArgument = e.Row.RowIndex
                            btnEdit.CssClass = "cls_button"
                            btnEdit.Width = 20
                            btnEdit.ImageUrl = "..\..\images\ico_Edit.gif"
                            'btnEdit.Text = "Edit"
                            'btnEdit.Attributes.Add("onclick", "return confirm('Are you sure you want to Delete the selected record(s)?');")
                            e.Row.Cells(1).Controls.Add(btnEdit)

                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting

        Dim strTxnNo As String = sender.datakeys(e.RowIndex).item("TXN_NO")
        Dim strLineNo As String = sender.datakeys(e.RowIndex).item("LINE_NO")
        Dim strUserId As String = Web.HttpContext.Current.Session("UserID").ToString
        Dim strsessionid As String = GetGuid()
        Dim strUserName As String = Web.HttpContext.Current.Session("UserName").ToString

        Dim clsTRAOrder As New txn_order.clsTRAOrder
        clsTRAOrder.DelTMPTRADetails(strTxnNo, strLineNo, strUserId, strsessionid, strUserName)

        UpdateChangedHeader()

        If lbllineno.Text = strLineNo Then
            ResetDtl()
            UpdateContent.Update()
        End If

        TimerControl1.Enabled = True

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

        Dim strTxnCode As String = sender.datakeys(e.NewEditIndex).item("TXN_NO")
        Dim strLineNo As String = sender.datakeys(e.NewEditIndex).item("LINE_NO")
        Dim strUserId As String = Web.HttpContext.Current.Session("UserID").ToString
        Dim strsessionid As String = GetGuid()

        Dim dtTraOrderEdit As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder
        dtTraOrderEdit = clsTRAOrder.GetTMPTRADetailsEdit(strTxnCode, strLineNo, strUserId, strsessionid)

        Loadddlreason()
        lbllineno.Text = dtTraOrderEdit.Rows(0)("LINE_NO")
        txtPrdCode.Text = dtTraOrderEdit.Rows(0)("PRD_CODE")
        txtPrdName.Text = dtTraOrderEdit.Rows(0)("PRD_NAME")
        LoadddlUOM(dtTraOrderEdit.Rows(0)("PRD_CODE"), Trim(txtsalesrepCode.Text))



        If ddluom.Items.FindByText(dtTraOrderEdit.Rows(0)("UOM_CODE")) IsNot Nothing Then
            ddluom.SelectedIndex = -1
            ddluom.Items.FindByText(dtTraOrderEdit.Rows(0)("UOM_CODE")).Selected = True
            txtselecteduom.Text = Trim(dtTraOrderEdit.Rows(0)("UOM_CODE"))
        End If



        lbluomprice.Text = dtTraOrderEdit.Rows(0)("UOM_PRICE")
        txtbno.Text = dtTraOrderEdit.Rows(0)("BATCH_NO")
        txtqty.Text = dtTraOrderEdit.Rows(0)("RET_QTY")
        txtprice.Text = dtTraOrderEdit.Rows(0)("LIST_PRICE")
        txtexpdate.Text = dtTraOrderEdit.Rows(0)("EXP_DATE")
        lblamount.Text = dtTraOrderEdit.Rows(0)("RET_AMT")
        ddlreason.SelectedValue = dtTraOrderEdit.Rows(0)("REASON_CODE")
        'txtdetailremarks.Text = dtTraOrderEdit.Rows(0)("REMARKS")

        UpdateContent.Update()
    End Sub


#End Region

    Private Sub ResetDtl()
        lbllineno.Text = 0
        txtPrdCode.Text = ""
        txtbno.Text = ""
        txtPrdName.Text = ""
        txtexpdate.Text = ""
        With ddluom
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        txtqty.Text = 0
        lbluomprice.Text = 0
        txtprice.Text = 0
        With ddlreason
            .DataSource = Nothing
            .DataBind()
            .Items.Clear()
        End With
        lblamount.Text = 0
        'txtdetailremarks.Text = ""


    End Sub

    Private Sub ValidatePrd()
        Dim dt As DataTable
        Dim clsTRAOrder As New txn_order.clsTRAOrder

        Dim strUserId As String, strprdCode As String, strSalesrepCode As String
        strUserId = Web.HttpContext.Current.Session("UserID")
        strprdCode = Trim(txtPrdCode.Text)
        strSalesrepCode = Trim(txtsalesrepCode.Text)

        dt = clsTRAOrder.ValidatePrd(strUserId, strprdCode, strSalesrepCode)

        If dt.Rows.Count <= 0 Then
            lbldtlmsg.Text = Trim(txtPrdCode.Text) + " is an invalid Product code ! Kindly re-enter the product code again."
            txtPrdCode.Text = ""
            txtPrdName.Text = ""
            With ddluom
                .Items.Clear()
            End With
            lbluomprice.Text = ""
        Else
            txtPrdName.Text = dt.Rows(0)("PRD_NAME")
            LoadddlUOM(Trim(txtPrdCode.Text), Trim(txtsalesrepCode.Text))
            lbluomprice.Text = "0"
        End If
    End Sub

#End Region

#Region "Submit/KIV"
    Protected Sub btnsubmit2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit2.Click
        If Page.IsValid Then
            If dgList.Rows.Count > 0 Then
                SaveHeader()
                SubmitHdrDtl("P")
                btnKIV.Enabled = False
                btnSubmit2.Enabled = False
                btnsubmit.Enabled = False

            Else
                lblMsgPop.Message = "Kindly add at least one detail to the transaction!!"
                lblMsgPop.Show()
            End If
        End If



    End Sub

    Private Sub SubmitHdrDtl(ByVal strStatus As String)
        Try

            Dim strSessionId As String, strTxnNo As String, strVoucherNo As String, strVisitID As String, _
                   strSalesrepCode As String, strUserName As String, strUserId As String

            strSessionId = GetGuid() 'Trim(Request.QueryString("SESSION_ID"))
            strTxnNo = lbltxnno.Text 'Trim(Request.QueryString("TXN_NO"))
            strVoucherNo = lblvouchno.Text 'Trim(Request.QueryString("VOUCHER_NO"))
            strVisitID = lblvisitid.Text 'Trim(Request.QueryString("VISIT_ID"))
            strSalesrepCode = lblSalesrepCode.Text 'Trim(Request.QueryString("SALESREP_CODE"))
            strUserId = Web.HttpContext.Current.Session("UserID").ToString
            strUserName = Web.HttpContext.Current.Session("UserName").ToString
            Dim clsTRAOrder As New txn_order.clsTRAOrder

            Dim DT As DataTable
            DT = clsTRAOrder.InstTRADetails(strSessionId, strTxnNo, strVoucherNo, strVisitID, strSalesrepCode, strStatus, strUserName, strUserId)

            If DT.Columns.Contains("output_value") Then
                Dim strOutputValue As String = DT.Rows(0)("output_value")
                If strOutputValue = "V" Then
                    ShowMessage() = "yes"
                    Response.Redirect("TRAOrder.aspx?status=" + strStatus + "&submit_txn_no=" + strTxnNo)
                Else
                    lblMsgPop.Message = "You TRA request (" + strTxnNo + " is not successful!!." + strOutputValue
                    lblMsgPop.Show()
                End If
            Else
                Dim strerror As Integer = DT.Rows(0)("ERROR")
                If strerror = 0 Then
                    ShowMessage() = "yes"
                    Response.Redirect("TRAOrder.aspx?status=" + strStatus + "&submit_txn_no=" + strTxnNo)
                Else
                    lblMsgPop.Message = "You request is not successfully!!"
                    lblMsgPop.Show()
                End If
            End If
            
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub btnkiv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKIV.Click

        If dgList.Rows.Count > 0 Then
            SaveHeader()
            KIVHdrDtl("K")
            btnreset.Enabled = False
            btnKIV.Enabled = False
            btnSubmit2.Enabled = False
            btnsubmit.Enabled = False

        Else
            lblMsgPop.Message = "Kindly add at least one detail to the transaction!!"
            lblMsgPop.Show()
        End If

    End Sub

    Private Sub KIVHdrDtl(ByVal strStatus As String)
        Try

            Dim strSessionId As String, strTxnNo As String, strVoucherNo As String, strVisitID As String, _
                   strSalesrepCode As String, strUserName As String, strUserId As String

            strSessionId = GetGuid() 'Trim(Request.QueryString("SESSION_ID"))
            strTxnNo = lbltxnno.Text 'Trim(Request.QueryString("TXN_NO"))
            strVoucherNo = lblvouchno.Text 'Trim(Request.QueryString("VOUCHER_NO"))
            strVisitID = lblvisitid.Text 'Trim(Request.QueryString("VISIT_ID"))
            strSalesrepCode = lblSalesrepCode.Text 'Trim(Request.QueryString("SALESREP_CODE"))
            strUserId = Web.HttpContext.Current.Session("UserID").ToString
            strUserName = Web.HttpContext.Current.Session("UserName").ToString

            Dim clsTRAOrder As New txn_order.clsTRAOrder

            Dim DT As DataTable
            DT = clsTRAOrder.KIVTRADetails(strSessionId, strTxnNo, strVoucherNo, strVisitID, strSalesrepCode, strStatus, strUserName, strUserId)


            Dim strerror As Integer = DT.Rows(0)("ERROR")

            'If strerror = 0 Then
            '    ShowMessage() = "yes"
            '    Response.Redirect("TRAOrder.aspx?status=" + strStatus + "&submit_txn_no=" + strTxnNo)


            'Else
            '    lblMsgPop.Message = "You request is not successfully!!"
            '    lblMsgPop.Show()
            'End If

            If strerror = 0 Then
                    'Response.Redirect("TRAOrder.aspx?status=" + strStatus + "&submit_txn_no=" + strTxnNo)
                    lblMsgPop.Message = "You have successfully KIV the " + strTxnNo + " transaction !!"
                    lblMsgPop.Show()
            Else
                lblMsgPop.Message = "You request is not successfully!!"
                lblMsgPop.Show()
            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Sub Load_Submited_KIV_Message()
        Dim strstatus As String, strsubmittedtxnno As String, strcurrenttxnno As String
        strstatus = Trim(Request.QueryString("status"))
        strsubmittedtxnno = Trim(Request.QueryString("submit_txn_no"))
        strcurrenttxnno = Trim(Request.QueryString("TXN_NO"))

        If ShowMessage() = "yes" Then

            If strsubmittedtxnno <> "" Then

                If strstatus = "K" Then
                    lblMsgPop.Message = "You have successfully KIV the " + strsubmittedtxnno + " transaction !!"
                    lblMsgPop.Show()
                ElseIf strstatus = "P" Then
                    lblMsgPop.Message = "You have successfully submitted the " + strsubmittedtxnno + " transaction !!"
                    lblMsgPop.Show()
                ElseIf strstatus = "V" Then
                    lblMsgPop.Message = "You have successfully Save (as draft) " + strsubmittedtxnno + " transaction !!"
                    lblMsgPop.Show()
                End If

            End If

            ShowMessage() = "no"
        End If


        If strcurrenttxnno <> "" Then
            lblcurrenttxnno.Text = "(" + strcurrenttxnno + ")"
        End If

    End Sub
#End Region

#Region "Common"
    Protected Function FieldControl() As Boolean
        Dim cls_common As New txn_common.clstxncommon
        Dim DT As New DataTable
        DT = cls_common.GetFieldConfig(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, "field_access")

        If DT.Rows.Count > 0 Then
            For i = 0 To DT.Rows.Count - 1
                If DT.Rows(i)(0) = "No of carton" Then
                    rfvcarton.Enabled = False
                    rfvcarton2.Enabled = False
                    cvtxtcarton.Enabled = False
                    cvtxtcarton2.Enabled = False
                End If
                If DT.Rows(i)(0) = "Storage Location" Then
                    rfvddlstorage.Enabled = False
                End If
                If DT.Rows(i)(0) = "Price" Then
                    lbluomprice.Enabled = True
                End If
                If DT.Rows(i)(0) = "Division" Then
                    lblCustDivision.Visible = True
                    ddlDivision.Visible = True
                    rfvddlDivision.Enabled = True
                End If
            Next
        End If
    End Function

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            SalesrepCode = clsTRAOrder.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Public Property GetGuid()
        Get
            If String.IsNullOrEmpty(Session("GUID")) Then Session("GUID") = Session("USER_ID") & Guid.NewGuid.ToString.ToUpper.Substring(24, 8)
            Return Session("GUID")
        End Get
        Set(ByVal value)
            Session("GUID") = value
        End Set
    End Property

    Private Sub Reset()

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strsessionId As String, strTxnNo As String, strUserid As String

            strsessionId = GetGuid()
            strTxnNo = lbltxnno.Text
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            clsTRAOrder.ResetTRAHdrDtl(strsessionId, strTxnNo, strUserid)
        End If

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub GetExistingSessionID()
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strUserid As String, strsessionid As String
            strSalesrepCode = Trim(txtsalesrepCode.Text) 'Trim(Session("salesrep_code"))
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            DT = clsTRAOrder.GetSessionID(strUserid, strSalesrepCode)
            If DT.Rows.Count > 0 Then
                strsessionid = DT.Rows(0).Item("TRADE_RET_HDR_ID")
                GetGuid() = strsessionid
            End If

        End If
    End Sub


    Protected Sub btnsubmit_Click1(sender As Object, e As EventArgs) Handles btnsubmit.Click
        If dgList.Rows.Count > 0 Then
            SaveHeader()
        End If

        Dim strurl As String
        strurl = "TRAOrderDtlSubmit.aspx?TXN_NO=" + lbltxnno.Text + "&SESSION_ID=" + GetGuid() + "&SALESREP_CODE=" + lblSalesrepCode.Text _
        + "&VOUCHER_NO=" + lblvouchno.Text + "&VISIT_ID=" + lblvisitid.Text
        Response.Redirect(strurl)
    End Sub
End Class

Public Class CF_TRAOrder
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "LINE_NO"
                strFieldName = "Line No"
            Case "TRADE_RET_DTL_ID"
                strFieldName = "Session ID"
            Case "TXN_NO"
                strFieldName = "Txn No"
            Case "BATCH_NO"
                strFieldName = "Batch No"
            Case "RET_QTY"
                strFieldName = "Quantity"
            Case "LIST_PRICE"
                strFieldName = "Unit Price<br />(RM)"
            Case "REASON_CODE"
                strFieldName = "Return Reason"
            Case "RET_AMT"
                strFieldName = "Net Value<br />(RM)"
            Case "EXP_DATE"
                strFieldName = "Exp. Date"
            Case "UOM_CODE"
                strFieldName = "UOM"
            Case "SALESREP_CODE"
                strFieldName = "Salesman Code"
            Case "SALESREP_NAME"
                strFieldName = "Salesman Name"
         
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Or strColumnName = "SESSION_ID" Or strColumnName = "TXN_NO" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" OrElse strColumnName = "LIST_PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                ElseIf strColumnName Like "*_PRICE" OrElse strColumnName Like "*_QTY" OrElse strColumnName Like "*_AMT" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

                If strColumnName = "REASON_CODE" Then
                    .HorizontalAlign = HorizontalAlign.Left
                End If
            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
