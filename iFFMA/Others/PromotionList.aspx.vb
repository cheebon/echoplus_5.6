'************************************************************************
'	Author	    :	YONG SOO FONG
'	Date	    :	26/02/2016
'	Purpose	    :	Promotion List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_Others_Promotion
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "PromotionList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.PROMO_SO)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.PROMO_SO
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With


            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                End If

                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, SubModuleAction.Edit) Then
                    btnSave.Visible = False
                End If

                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, SubModuleAction.Delete) Then
                    btnDelete.Visible = False
                End If

            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("CGCODE") = ""
        ViewState("PRD_CODE") = ""

        BindDDL()

        lblTeamCode.Text = wuc_toolbar.TeamCode

        ddlCustGrpCode.Enabled = True
        ddlProdCode.Enabled = True

        btnSave.Visible = True
        ActivateAddTab()
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try

            Dim sbSelectedStr As New Text.StringBuilder

            If dgList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            Dim clsPromotion As New mst_others.clsPromotion
                            clsPromotion.DeletePromotion(DK(0), DK(1), DK(2))
                        End If
                    End If
                    i += 1
                Next
            End If

            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = ""

            Dim clsPromotion As New mst_Others.clsPromotion

            Dim strSelectedCGCode As String = ViewState("CGCODE").ToString
            Dim strSelectedProdCode As String = ViewState("PRD_CODE").ToString

            If strSelectedCGCode <> "" And strSelectedProdCode <> "" Then
                clsPromotion.UpdatePromotion(lblTeamCode.Text, RTrim(ddlCustGrpCode.SelectedValue), RTrim(ddlProdCode.SelectedValue))
                'RenewDataBind()
                ViewState("CGCODE") = strSelectedCGCode
                ViewState("PRD_CODE") = strSelectedProdCode

                lblInfo.Text = "The record is successfully saved."
            Else
                Dim DT As DataTable

                DT = clsPromotion.CreatePromotion(lblTeamCode.Text, Trim(ddlCustGrpCode.SelectedValue), Trim(ddlProdCode.SelectedValue))

                If DT.Rows.Count > 0 Then
                    strSelectedCGCode = DT.Rows(0)("CGCODE")
                    strSelectedProdCode = DT.Rows(0)("PRD_CODE")
                End If

                If strSelectedCGCode = "" And strSelectedProdCode = "" Then
                    lblInfo.Text = "Promotion List already exists!"
                Else
                    'RenewDataBind()
                    ViewState("CGCODE") = strSelectedCGCode
                    ViewState("PRD_CODE") = strSelectedProdCode

                    ActivateEditTab()
                    If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, SubModuleAction.Edit) Then
                        btnSave.Visible = False
                    End If

                    lblInfo.Text = "The record is successfully created."
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DDL"
    Private Sub BindDDL()
        Dim dtProdCode As DataTable
        Dim dtCustGrpCode As DataTable

        Try
            Dim clsPromotion As New mst_Others.clsPromotion
            dtProdCode = clsPromotion.GetPrdList(wuc_toolbar.TeamCode)
            With ddlProdCode
                .Items.Clear()
                .DataSource = dtProdCode.DefaultView
                .DataTextField = "PRD_NAME"
                .DataValueField = "PRD_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            dtCustGrpCode = clsPromotion.GetCustGrpList()
            With ddlCustGrpCode
                .Items.Clear()
                .DataSource = dtCustGrpCode.DefaultView
                .DataTextField = "CUST_GRP_NAME"
                .DataValueField = "CUST_GRP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "DATA BIND"
    Public Sub RenewDataBind()

        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsPromotion As New mst_Others.clsPromotion
            With (wuc_toolbar)
                DT = clsPromotion.GetPromotionList(.TeamCode, .SearchType, .SearchValue)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count

            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, SubModuleAction.Delete) Then
                aryDataItem.Add("chkSelect")
                While dgList.Columns.Count > 1
                    dgList.Columns.RemoveAt(1)
                End While
                dgList.Columns(0).HeaderStyle.Width = "25"
                dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            Else
                dgList.Columns.Clear()
            End If

            'If Master_Row_Count > 0 Then
            '    Dim dgBtnColumn As New CommandField

            '    dgBtnColumn.HeaderStyle.Width = "50"
            '    dgBtnColumn.HeaderText = "Edit"
            '    dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

            '    If dtToBind.Rows.Count > 0 Then
            '        dgBtnColumn.ButtonType = ButtonType.Link
            '        dgBtnColumn.ShowEditButton = True
            '        dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
            '        dgBtnColumn.Visible = True
            '    End If

            '    dgList.Columns.Add(dgBtnColumn)
            '    dgBtnColumn = Nothing
            '    aryDataItem.Add("BTN_EDIT")
            'End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_Promotion.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_Promotion.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_Promotion.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_Promotion.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_Promotion.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, SubModuleAction.Delete) Then 'HL: 20080429 (AR)
                If e.Row.RowType = DataControlRowType.Header AndAlso Master_Row_Count > 0 Then
                    Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                    If chkAll Is Nothing Then
                        chkAll = New CheckBox
                        chkAll.ID = "chkSelectAll"
                        e.Row.Cells(0).Controls.Add(chkAll)
                    End If
                    chkAll.Attributes.Add("onClick", "SelectAllRows(this)")
                    chkAll.ToolTip = "Click to toggle the selection of ALL rows"
                ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                    Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                    If chk Is Nothing Then
                        chk = New CheckBox
                        chk.ID = "chkSelect"
                        e.Row.Cells(0).Controls.Add(chk)
                    End If
                    chk.Attributes.Add("onClick", "SelectRow(this)")
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strSelectedTeamCode As String = sender.datakeys(e.NewEditIndex).item("TEAM_CODE")
            Dim strSelectedCGCCode As String = sender.datakeys(e.NewEditIndex).item("CGCODE")
            Dim strSelectedPrdCode As String = sender.datakeys(e.NewEditIndex).item("PRD_CODE")
            Dim strSelectedStatus As String = sender.datakeys(e.NewEditIndex).item("STATUS")

            Dim DT As DataTable
            Dim lstSelected As ListItem
            lstSelected = Nothing

            BindDDL()

            Dim clsPromotion As New mst_Others.clsPromotion

            DT = clsPromotion.GetPromotionDetails(strSelectedTeamCode, strSelectedCGCCode, strSelectedPrdCode, strSelectedStatus)

            If DT.Rows.Count > 0 Then
                ViewState("TEAM_CODE") = strSelectedTeamCode
                ViewState("CGCODE") = strSelectedCGCCode
                ViewState("PRD_CODE") = strSelectedPrdCode
                lblTeamCode.Text = IIf(IsDBNull(DT.Rows(0)("TEAM_CODE")), "", DT.Rows(0)("TEAM_CODE"))

                ddlCustGrpCode.SelectedIndex = -1
                lstSelected = ddlCustGrpCode.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("CGCODE")), "", DT.Rows(0)("CGCODE")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                ddlProdCode.SelectedIndex = -1
                lstSelected = ddlProdCode.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("PRD_CODE")), "", DT.Rows(0)("PRD_CODE")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                'ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS")), "", DT.Rows(0)("STATUS"))
                ActivateEditTab()
            End If
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Promotion List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        wuc_toolbar.show()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateAddTab()
        TabPanel1.HeaderText = "Promotion List Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "Promotion List Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        ddlCustGrpCode.Enabled = True
        ddlProdCode.Enabled = True

        wuc_toolbar.hide()
        lblInfo.Text = ""

    End Sub

    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function

#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True)

            wuc_toolbar.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class

Public Class CF_Promotion
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper

            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

