﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallDtlMC.aspx.vb" Inherits="iFFMR_Customize_MissCallAnalysis_CallDtlMC" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Missing Call Analysis Detail</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/layout.js" type="text/javascript"></script>
</head>
<!--#include File="~/include/commonutil.js"-->
<script type="text/javascript" language="javascript">
function resetScrollTop(){var frm =self.parent.document.frames[1];if(frm){var dgList = frm.document.getElementById('div_dgList');if(dgList){dgList.scrollTop=0;}}}
    </script>
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe');resetSize('div_dgList','DetailBarIframe');">
    <form id="frmMissCallAnalysisDtl" runat="server">
      <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td valign="top" align="left">
                <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTreeMenuControl" runat="server" >
                            <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%"
                                    align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3" style="width: 100%">
                                            <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3">
                                        <input type="button"  runat="server"  id="btnback" value="Back" onclick="resetScrollTop();ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','DetailBarIframe');" class="cls_button" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowPaging="false"
                                                                    AllowSorting="false" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                    GridHeight="450" RowSelectionEnabled="true">
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport" style="height: 10px">
                                        <td colspan="3">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
