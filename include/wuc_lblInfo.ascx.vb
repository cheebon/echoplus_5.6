
Partial Class WebUserControl_wuc_lblInfo
    Inherits System.Web.UI.UserControl

    Private _CssClass As String

    Public Property CssClass() As String
        Get
            Return _CssClass
        End Get
        Set(ByVal value As String)
            _CssClass = value
            If lblInfo IsNot Nothing Then lblInfo.CssClass = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return lblInfo.Text
        End Get
        Set(ByVal value As String)
            lblInfo.Text = value
            updPnlInfo.Update()
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(_CssClass) = False Then lblInfo.CssClass = _CssClass
    End Sub
End Class
