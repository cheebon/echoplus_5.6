Option Explicit On

Imports System.Data
Imports System.Collections.Generic

Partial Class Admin_AccessRight_NewAccessRight_AccessRightEdit
    Inherits System.Web.UI.Page

    Private ViewCBChckTotal, CreateCBChckTotal, EditCBChckTotal, DeleteCBChckTotal As Integer 'To keep track how many CheckBoxes checked
    Private ViewJS, CreateJS, EditJS, DeleteJS As New List(Of String)

    Protected Property accessright_id() As String
        Get
            If Not ViewState("accessright_id") = Nothing Then
                Return ViewState("accessright_id").ToString()
            Else
                Return "0"
            End If
        End Get
        Set(ByVal value As String)
            ViewState("accessright_id") = value
        End Set
    End Property 'store them in viewstate to perserve their value
    Protected Property country_id() As String
        Get
            Return ViewState("country_id")
        End Get
        Set(ByVal value As String)
            ViewState("country_id") = value
        End Set
    End Property
    Protected Property accessright_name() As String
        Get
            Return ViewState("accessright_name")
        End Get
        Set(ByVal value As String)
            ViewState("accessright_name") = value
        End Set
    End Property
    Protected Property principal_id() As String
        Get
            Return ViewState("principal_id")
        End Get
        Set(ByVal value As String)
            ViewState("principal_id") = value
        End Set
    End Property
    Protected Property module_id() As Double
        Get
            If Not ViewState("module_id") = Nothing Then
                Return Convert.ToDouble(ViewState("module_id"))
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Double)
            ViewState("module_id") = value
        End Set
    End Property      'across postback
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim _ori As String

        With wuc_lblheader
            .Title = "Access Right Edit"
            .DataBind()
            .Visible = True
        End With
        If Not Page.IsPostBack Then
            accessright_id = Request.QueryString("accessright_id")
            _ori = Request.QueryString("ori")

            If _ori = "edit" Then
                btnBack.Visible = True
            ElseIf _ori = "create" Then
                btnDone.Visible = True
            End If

            FillARDetails()
            GetModuleList()
            LoadReferenceDDL()
        End If
        btnSave.Visible = False
    End Sub

#Region "Reference Functions"
    Private Sub LoadReferenceDDL()
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim dt As DataTable

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                .clsProperties.search_type = "all"
                .clsProperties.search_value = Trim(Replace("*", "*", "%"))

                .clsProperties.delete_flag = 1
                .clsProperties.user_code = Trim(Session("UserCode"))   'HL:20070622
                dt = .GetAccessRightList
            End With

            With ddlAccessright
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "ACCESSRIGHT_NAME"
                .DataValueField = "ACCESSRIGHT_ID"
                .DataBind()
            End With
        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.LoadReferenceDDL : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Private Functions"
    Private Sub GetModuleList()
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                dt = objAccessRightQuery.GetModuleList
            End With

            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            With dgModuleList
                .DataSource = dt
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.GetModuleList : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub 'obtain modules assoc to principal

    Private Sub GetSubModuleList(ByVal lngModuleID As Double, Optional ByVal intPassFlag As Integer = 0, Optional ByVal strAccessrightID As String = "")
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            If intPassFlag = 0 Then
                objAccessRightQuery.clsProperties.accessright_id = accessright_id
            Else
                objAccessRightQuery.clsProperties.accessright_id = strAccessrightID
            End If

            objAccessRightQuery.clsProperties.module_id = lngModuleID
            objAccessRightQuery.clsProperties.principal_id = Session("PRINCIPAL_ID")
            dt = objAccessRightQuery.GetSubModuleListAction

            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            Else
                btnSave.Visible = True
            End If

            With dgSubModuleList
                .DataSource = dt
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.GetSubModuleList : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub 'obtain submodules assoc to selected module

    Private Sub ClientAddCBHeader(ByRef gvr As GridViewRow)
        CType(gvr.FindControl("ViewAllCheckBox"), CheckBox).Attributes("onclick") = "ViewControlAll(this.checked);"
        ViewJS.Add(String.Concat("'", CType(gvr.FindControl("ViewAllCheckBox"), CheckBox).ClientID, "'"))

        CType(gvr.FindControl("CreateAllCheckBox"), CheckBox).Attributes("onclick") = "CreateControlAll(this.checked);"
        CreateJS.Add(String.Concat("'", CType(gvr.FindControl("CreateAllCheckBox"), CheckBox).ClientID, "'"))

        CType(gvr.FindControl("EditAllCheckBox"), CheckBox).Attributes("onclick") = "EditControlAll(this.checked);"
        EditJS.Add(String.Concat("'", CType(gvr.FindControl("EditAllCheckBox"), CheckBox).ClientID, "'"))

        CType(gvr.FindControl("DeleteAllCheckBox"), CheckBox).Attributes("onclick") = "DeleteControlAll(this.checked);"
        DeleteJS.Add(String.Concat("'", CType(gvr.FindControl("DeleteAllCheckBox"), CheckBox).ClientID, "'"))
    End Sub 'setup header checkbox

    Private Sub CheckHeader()
        Dim ViewCBCount, CreateCBCount, EditCBCount, DeleteCBCount As Integer
        Dim vcb, ccb, ecb, dcb, vHeader, cHeader, eHeader, dHeader As CheckBox

        ViewCBCount = 0
        CreateCBCount = 0
        EditCBCount = 0
        DeleteCBCount = 0
        vHeader = CType(dgSubModuleList.HeaderRow.FindControl("ViewAllCheckBox"), CheckBox)
        cHeader = CType(dgSubModuleList.HeaderRow.FindControl("CreateAllCheckBox"), CheckBox)
        eHeader = CType(dgSubModuleList.HeaderRow.FindControl("EditAllCheckBox"), CheckBox)
        dHeader = CType(dgSubModuleList.HeaderRow.FindControl("DeleteAllCheckBox"), CheckBox)

        For Each gvr As GridViewRow In dgSubModuleList.Rows
            vcb = CType(gvr.FindControl("ViewCheckBox"), CheckBox)
            ccb = CType(gvr.FindControl("CreateCheckBox"), CheckBox)
            ecb = CType(gvr.FindControl("EditCheckBox"), CheckBox)
            dcb = CType(gvr.FindControl("DeleteCheckBox"), CheckBox)

            If vcb.Visible Then ViewCBCount = ViewCBCount + 1
            If ccb.Visible Then CreateCBCount = CreateCBCount + 1
            If ecb.Visible Then EditCBCount = EditCBCount + 1
            If dcb.Visible Then DeleteCBCount = DeleteCBCount + 1
        Next

        'if action is n/a for all submodule, then header checkbox shall disappear
        If ViewCBCount = 0 Then
            vHeader.Visible = False
        ElseIf ViewCBCount = ViewCBChckTotal Then
            vHeader.Checked = True
        End If

        If CreateCBCount = 0 Then
            cHeader.Visible = False
        ElseIf CreateCBCount = CreateCBChckTotal Then
            cHeader.Checked = True
        End If

        If EditCBCount = 0 Then
            eHeader.Visible = False
        ElseIf EditCBCount = EditCBChckTotal Then
            eHeader.Checked = True
        End If

        If DeleteCBCount = 0 Then
            dHeader.Visible = False
        ElseIf DeleteCBCount = DeleteCBChckTotal Then
            dHeader.Checked = True
        End If
    End Sub 'setup header checkbox checked state in accordance to cb checked state in rows

    Private Sub InjectClientCBArray()
        Dim js As New StringBuilder
        js.Append("<script type='text/javascript'>")
        js.Append(String.Concat("var ViewCheckBoxIDs = new Array(", String.Join(",", ViewJS.ToArray()), ");"))
        js.Append(String.Concat("var CreateCheckBoxIDs = new Array(", String.Join(",", CreateJS.ToArray()), ");"))
        js.Append(String.Concat("var EditCheckBoxIDs = new Array(", String.Join(",", EditJS.ToArray()), ");"))
        js.Append(String.Concat("var DeleteCheckBoxIDs = new Array(", String.Join(",", DeleteJS.ToArray()), ");"))
        js.Append("</script>")

        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CheckJS", js.ToString(), False)
    End Sub 'pass all checkboxes to client site

    Private Sub FillARDetails()
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                .clsProperties.accessright_id = accessright_id
                dt = .GetAccessRightDtl()
            End With

            txtName.Text = Trim(dt.Rows(0)("accessright_name"))
            accessright_name = Trim(dt.Rows(0)("accessright_name"))
            txtName1.Text = Trim(dt.Rows(0)("accessright_name_1"))
            txtDesc.Text = Trim(dt.Rows(0)("accessright_desc"))
            txtDesc1.Text = Trim(dt.Rows(0)("accessright_desc_1"))
            country_id = Trim(dt.Rows(0)("country_id"))
            principal_id = Trim(dt.Rows(0)("principal_id"))
            lblCounName.Text = Trim(dt.Rows(0)("country_name"))
            lblPrinName.Text = Trim(dt.Rows(0)("principal_name"))

            updPnlARDetail.Update()

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.FillARDetails : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try

    End Sub
#End Region


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


#Region "Events"
    Protected Sub dgModuleList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgModuleList.Rows(index)
        Dim lb As LinkButton

        'reset variables
        ViewCBChckTotal = 0
        CreateCBChckTotal = 0
        EditCBChckTotal = 0
        DeleteCBChckTotal = 0
        ViewJS.Clear()
        CreateJS.Clear()
        EditJS.Clear()
        DeleteJS.Clear()

        Try
            Select Case e.CommandName
                Case "Select"
                    lblMesg.Visible = False
                    dgSubModuleList.Visible = True
                    lb = CType(row.Cells(1).Controls(0), LinkButton)
                    btnSave.Text = "Save (" & lb.Text & " module)"
                    btnSave.Width = btnSave.Text.Length * 6
                    'btnSave.Visible = True
                    module_id = Trim(dgModuleList.DataKeys(row.RowIndex).Value)
                    GetSubModuleList(module_id)
            End Select

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.dgModuleList_RowCommand : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgSubModuleList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim drv As DataRowView
        Dim view As String
        Dim create As String
        Dim edit As String
        Dim delete As String
        'Dim cbh As CheckBox
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                ClientAddCBHeader(e.Row)
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                drv = CType(e.Row.DataItem, DataRowView)
                view = drv("VIEW").ToString()
                create = drv("CREATE").ToString()
                edit = drv("EDIT").ToString()
                delete = drv("DELETE").ToString()

                If view = "2" Then
                    CType(e.Row.FindControl("ViewCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("ViewCheckBox"), CheckBox).Attributes("onclick") = "ValidateViewHeader(this.checked);"
                    ViewJS.Add(String.Concat("'", CType(e.Row.FindControl("ViewCheckBox"), CheckBox).ClientID, "'"))
                    ViewCBChckTotal = ViewCBChckTotal + 1
                ElseIf view = "1" Then
                    CType(e.Row.FindControl("ViewCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("ViewCheckBox"), CheckBox).Attributes("onclick") = "ValidateViewHeader(this.checked);"
                    ViewJS.Add(String.Concat("'", CType(e.Row.FindControl("ViewCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("ViewCheckBox"), CheckBox).Visible = False
                End If

                If create = "2" Then
                    CType(e.Row.FindControl("CreateCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("CreateCheckBox"), CheckBox).Attributes("onclick") = "ValidateCreateHeader(this.checked);"
                    CreateJS.Add(String.Concat("'", CType(e.Row.FindControl("CreateCheckBox"), CheckBox).ClientID, "'"))
                    CreateCBChckTotal = CreateCBChckTotal + 1
                ElseIf create = "1" Then
                    CType(e.Row.FindControl("CreateCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("CreateCheckBox"), CheckBox).Attributes("onclick") = "ValidateCreateHeader(this.checked);"
                    CreateJS.Add(String.Concat("'", CType(e.Row.FindControl("CreateCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("CreateCheckBox"), CheckBox).Visible = False
                End If

                If edit = "2" Then
                    CType(e.Row.FindControl("EditCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("EditCheckBox"), CheckBox).Attributes("onclick") = "ValidateEditHeader(this.checked);"
                    EditJS.Add(String.Concat("'", CType(e.Row.FindControl("EditCheckBox"), CheckBox).ClientID, "'"))
                    EditCBChckTotal = EditCBChckTotal + 1
                ElseIf edit = "1" Then
                    CType(e.Row.FindControl("EditCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("EditCheckBox"), CheckBox).Attributes("onclick") = "ValidateEditHeader(this.checked);"
                    EditJS.Add(String.Concat("'", CType(e.Row.FindControl("EditCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("EditCheckBox"), CheckBox).Visible = False
                End If

                If delete = "2" Then
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Checked = True
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Attributes("onclick") = "ValidateDeleteHeader(this.checked);"
                    DeleteJS.Add(String.Concat("'", CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).ClientID, "'"))
                    DeleteCBChckTotal = DeleteCBChckTotal + 1
                ElseIf delete = "1" Then
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Visible = True
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Attributes("onclick") = "ValidateDeleteHeader(this.checked);"
                    DeleteJS.Add(String.Concat("'", CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).ClientID, "'"))
                Else
                    CType(e.Row.FindControl("DeleteCheckBox"), CheckBox).Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.dgSubModuleList_RowDataBound : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objAccessRightDtl As adm_AccessRight.clsAccessRightDtl
        Dim i As Integer
        Dim row As GridViewRow
        Dim vcb, ccb, ecb, dcb As CheckBox
        Dim submodule_id As String
        Dim vAct, cAct, eAct, dAct As String

        Try
            For i = 0 To dgSubModuleList.Rows.Count - 1
                row = dgSubModuleList.Rows(i)
                vcb = CType(row.FindControl("ViewCheckBox"), CheckBox)
                ccb = CType(row.FindControl("CreateCheckBox"), CheckBox)
                ecb = CType(row.FindControl("EditCheckBox"), CheckBox)
                dcb = CType(row.FindControl("DeleteCheckBox"), CheckBox)

                submodule_id = dgSubModuleList.DataKeys(i).Value
                If vcb.Visible = True Then
                    vAct = IIf(vcb.Checked, "1", "0")
                Else
                    vAct = -1
                End If
                If ccb.Visible = True Then
                    cAct = IIf(ccb.Checked, "1", "0")
                Else
                    cAct = -1
                End If
                If ecb.Visible = True Then
                    eAct = IIf(ecb.Checked, "1", "0")
                Else
                    eAct = -1
                End If
                If dcb.Visible = True Then
                    dAct = IIf(dcb.Checked, "1", "0")
                Else
                    dAct = -1
                End If

                objAccessRightDtl = New adm_AccessRight.clsAccessRightDtl
                With objAccessRightDtl
                    .clsProperties.accessright_id = accessright_id
                    .clsProperties.module_id = Trim(module_id)
                    .clsProperties.submodule_id = Trim(submodule_id)
                    .clsProperties.changed_date = Now
                    .clsProperties.changed_user_id = Session("UserID")
                    .Alter(vAct, cAct, eAct, dAct)
                End With
            Next
            dgSubModuleList.Visible = False
            lblMesg.Text = "Records saved!"
            lblMesg.Visible = True
            btnSave.Visible = False

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.btnSave_Click : " & ex.ToString)
        Finally
            objAccessRightDtl = Nothing
        End Try
    End Sub

    Protected Sub dgSubModuleList_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckHeader()
        InjectClientCBArray()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        mdlARPopup.Show()
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        mdlARPopup.Hide()
    End Sub

    Protected Sub btnSaveARDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objAccessRight As adm_AccessRight.clsAccessRight
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim dt As DataTable
        Dim intCnt As Integer
        

        Try
            'Check AccessRight name exist or not
            If Trim(txtName.Text) <> accessright_name Then
                objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
                With objAccessRightQuery
                    .clsProperties.accessright_id = "0" 'txtAccessRightID.Text
                    .clsProperties.accessright_name = Trim(txtName.Text)
                    dt = .GetAccessRightDuplicate
                    intCnt = IIf(dt.Rows(0)("cnt") = Nothing, 0, dt.Rows(0)("cnt"))
                End With
                objAccessRightQuery = Nothing

                If intCnt > 0 Then
                    lblDupError.Visible = True
                    Exit Sub
                End If
            End If
            'aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            'aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            'Create record
            objAccessRight = New adm_AccessRight.clsAccessRight
            With objAccessRight
                .clsProperties.accessright_id = accessright_id
                .clsProperties.accessright_name = Trim(txtName.Text)
                .clsProperties.accessright_name_1 = Trim(txtName1.Text)
                .clsProperties.accessright_desc = Trim(txtDesc.Text)
                .clsProperties.accessright_desc_1 = Trim(txtDesc1.Text)
                .clsProperties.country_id = Convert.ToDouble(country_id)
                .clsProperties.principal_id = Convert.ToDouble(principal_id)
                '.clsProperties.created_date = Now '.ToString
                '.clsProperties.creator_user_id = Session("UserID")
                .clsProperties.changed_date = Now
                .clsProperties.changed_user_id = Session("UserID")
                .clsProperties.delete_flag = 0
                '.clsProperties.dtDtl = dtDtl
                .UpdateAccessRight()
            End With

            FillARDetails()
            mdlARPopup.Hide()
            lblDupError.Visible = False

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.btnSaveARDetails_Click : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
            objAccessRight = Nothing
        End Try
    End Sub

    Protected Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("AccessRightDtl.aspx?accessright_id=" & accessright_id)
    End Sub
#End Region

    Protected Sub btnRef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRef.Click
        Try
            If ReadyToRef() Then
                LoadRefenceDetails(ddlAccessright.SelectedValue.Trim)
            End If

        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.btnRef_Click : " & ex.ToString)
        End Try
    End Sub

    Private Function ReadyToRef() As Boolean
        If ddlAccessright.SelectedIndex > -1 Then
            If (Not IsNothing(module_id)) Or module_id <> 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Sub LoadRefenceDetails(ByVal strAccessrightID As String)
        Try
            GetSubModuleList(module_id, 1, strAccessrightID)
        Catch ex As Exception
            ExceptionMsg("AccessRightEdit.LoadRefenceDetails : " & ex.ToString)
        End Try
    End Sub
End Class
