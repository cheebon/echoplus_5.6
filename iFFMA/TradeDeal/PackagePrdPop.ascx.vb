Imports System.Data

Partial Class iFFMA_TradeDeal_PackagePrdPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property PackageCode() As String
        Get
            Return Trim(hdPackageCode.Value)
        End Get
        Set(ByVal value As String)
            hdPackageCode.Value = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property PackageType() As String
        Get
            Return Trim(hdPackageType.Value)
        End Get
        Set(ByVal value As String)
            hdPackageType.Value = value
        End Set
    End Property

    Public Property dtUOMList() As DataTable
        Get
            Return ViewState("dtUOMList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtUOMList") = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlMustInd As DropDownList = CType(DetailsView1.FindControl("ddlMustInd"), DropDownList)
            If ddlMustInd IsNot Nothing Then
                'ddlMustInd.SelectedValue = IIf(IsDBNull(DT.Rows(0)("MUST_IND")), "", DT.Rows(0)("MUST_IND"))
                If IsDBNull(Portal.Util.GetValue(Of String)(DT.Rows(0)("MUST_IND"))) Or Portal.Util.GetValue(Of String)(DT.Rows(0)("MUST_IND")) = "" Then
                    If PackageType IsNot Nothing Then
                        If PackageType.ToUpper = "F" Then ' Fixed
                            ddlMustInd.SelectedValue = "Y"
                        ElseIf PackageType.ToUpper = "S" Then ' Selective
                            ddlMustInd.SelectedValue = "N"
                        End If
                    End If
                Else
                    ddlMustInd.SelectedValue = IIf(IsDBNull(DT.Rows(0)("MUST_IND")), "", DT.Rows(0)("MUST_IND"))
                End If

            End If

            Dim lblMustInd As Label = CType(DetailsView1.FindControl("lblMustInd"), Label)
            If lblMustInd IsNot Nothing Then
                lblMustInd.Text = IIf(DT.Rows(0)("MUST_IND") = "Y", "Yes", "No")
            End If

            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            If ddlUOM IsNot Nothing Then
                dtUOM = clsCommon.GetUOMDDLByProduct(PrdCode)
                dtUOMList = dtUOM
                With ddlUOM
                    .Items.Clear()
                    .DataSource = dtUOM.DefaultView
                    .DataTextField = "UOM_NAME"
                    .DataValueField = "UOM_CODE"
                    .DataBind()
                    '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("UOM_CODE")), "", DT.Rows(0)("UOM_CODE"))
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadAddNewUOMDdl(ByVal strPrdCode As String)
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL
        dtUOM = clsCommon.GetUOMDDLByProduct(strPrdCode)
        dtUOMList = dtUOM

        Dim ddlFOCUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
        If ddlFOCUOM IsNot Nothing Then
            With ddlFOCUOM
                .Items.Clear()
                .DataSource = dtUOM.DefaultView
                .DataTextField = "UOM_NAME"
                .DataValueField = "UOM_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                '.SelectedValue = IIf(IsDBNull(DT.Rows(0)("FOC_UOM_CODE")), "", DT.Rows(0)("FOC_UOM_CODE"))
            End With
        End If
    End Sub

    Private Sub ResetPage()
        'SubCatID = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsPackage As New mst_TradeDeal.clsPackage

            DT = clsPackage.GetPackagePrdDetails(PackageCode, PrdCode)
            If DT.Rows.Count > 0 Then
               
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PACKAGE, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdSearch)
                .ResetPage()
                .TeamCode = TeamCode
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click
        Try
            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            txtPrdCode.Text = wuc_PrdSearch.PrdCode
            LoadAddNewUOMDdl((wuc_PrdSearch.PrdCode))
            LoadPrdListPrice()
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            Dim lblPrdCode As Label = CType(DetailsView1.FindControl("lblPrdCode"), Label)
            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            Dim txtQty As TextBox = CType(DetailsView1.FindControl("txtQty"), TextBox)
            Dim ddlMustInd As DropDownList = CType(DetailsView1.FindControl("ddlMustInd"), DropDownList)
            Dim txtPrice As TextBox = CType(DetailsView1.FindControl("txtPrice"), TextBox)

            txtQty.Text = IIf(Trim(txtQty.Text) = "", 0, Trim(txtQty.Text))
            txtPrice.Text = IIf(Trim(txtPrice.Text) = "", 0, Trim(txtPrice.Text))
           
            Dim clsPackage As New mst_TradeDeal.clsPackage

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsPackage.UpdatePackagePrd(PackageCode, Trim(lblPrdCode.Text), Trim(ddlUOM.SelectedValue), Trim(txtQty.Text), Trim(ddlMustInd.SelectedValue), Trim(txtPrice.Text))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim isDuplicate As Integer = 0

                DT = clsPackage.CreatePackagePrd(PackageCode, Trim(txtPrdCode.Text), Trim(ddlUOM.SelectedValue), Trim(txtQty.Text), Trim(ddlMustInd.SelectedValue), Trim(txtPrice.Text))
                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Product Code already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    PrdCode = Trim(txtPrdCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view

            ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlUom_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
         
            LoadPrdListPrice()
            Show()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            updPnlMaintenance.Update()
        End Try
    End Sub

    Private Sub LoadPrdListPrice()
        Dim ddlUOM As DropDownList = DirectCast(DetailsView1.FindControl("ddlUOM"), DropDownList)
        Dim txtPrice As TextBox = DirectCast(DetailsView1.FindControl("txtPrice"), TextBox)

        If ddlUOM IsNot Nothing Then
            Dim strUomCode As String = ddlUOM.SelectedValue
            Dim dt As DataTable = dtUOMList
            Dim drPrdListPrice() As DataRow
            drPrdListPrice = dt.Select("UOM_CODE = '" + strUomCode + "'", "UOM_CODE")
            txtPrice.Text = Portal.Util.GetValue(Of String)(drPrdListPrice(0)("PRD_LISTPRICE"))

        End If
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        ResetPage()
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class