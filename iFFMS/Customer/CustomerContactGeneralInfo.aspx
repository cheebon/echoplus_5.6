<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerContactGeneralInfo" CodeFile="CustomerContactGeneralInfo.aspx.vb" %>

<%@ Register TagPrefix="uc1" Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Contact List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmCustomerContactGeneralInfo" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                        </asp:Timer>
                                                        <ccGV:clsGridView ID="gridview1" runat="server" ShowFooter="false" AllowSorting="False"
                                                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="450px"
                                                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                            FreezeRows="0" GridWidth="" RowHighlightColor="AntiqueWhite" DataKeyNames="cont_acc_code">
                                                            <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                            <EmptyDataTemplate>
                                                                There is no data to display.</EmptyDataTemplate>
                                                            <Columns>
                                                                
                                                               <%-- <asp:BoundField DataField="cont_acc_code" HeaderText="Contact Code" />--%>
                                                                <asp:HyperLinkField DataNavigateUrlFields="cont_acc_code" DataNavigateUrlFormatString="#"
                                                                    DataTextField="cont_acc_code" HeaderText="Contact Code" SortExpression="cont_acc_code">
                                                                     <itemstyle horizontalalign="center" />
                                                                </asp:HyperLinkField>
                                                                <asp:BoundField DataField="contact_name" HeaderText="Contact Name" >
                                                                 <itemstyle horizontalalign="left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="rep_speciality" HeaderText="Rep Specialty" />
                                                                <asp:BoundField DataField="position" HeaderText="Position" />
                                                                <asp:BoundField DataField="department" HeaderText="Department" />
                                                                <asp:BoundField DataField="phone" HeaderText="Phone" />
                                                            </Columns>
                                                            <FooterStyle CssClass="GridFooter" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GridAlternate" />
                                                            <RowStyle CssClass="GridNormal" />
                                                            <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />
                                                        </ccGV:clsGridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
