<%
''Response.Cache.SetExpires(DateTime.Now)
''Response.Cache.SetCacheability(HttpCacheability.NoCache)
''Response.Cache.SetExpires(Now.AddSeconds(-1))
''Response.Cache.SetNoStore()
''Response.AppendHeader("Pragma", "no-cache")

If String.IsNullOrEmpty(Portal.UserSession.UserID) Then 
    Dim strScript As String = ""
     If Portal.UserSession.SessionKey <> "" Then
        strScript = "alert('The session was terminated by another user! RefKey:"+Portal.UserSession.SessionKey+"'); "
    Elseif Session.IsNewSession
        strScript = "alert('Your session has expired due to an extended period of inactivity. Please log in again.'); "
    End If
   strScript = strScript + "self.parent.parent.location='" & ResolveClientUrl("~/login.aspx?ErrMsg=Session Time Out, Please login again !") & "';"       
    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "CommonScript", "<script language=javascript>" & strScript & "</script>")
End If
%>

