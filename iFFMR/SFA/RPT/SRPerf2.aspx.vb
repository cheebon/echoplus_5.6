Imports System.Data
Partial Class iFFMR_SFA_RPT_SRPerf2
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection
    Dim licItemSeqColl As Hashtable

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Public Property ReportType() As String
        Get
            Return ViewState("Type_Ind")
        End Get
        Set(ByVal value As String)
            ViewState("Type_Ind") = value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblErr.Text = ""
        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True
            licHeaderCollector = Nothing

            ReportType = Request.QueryString("IND")
            wuc_ctrlPnl.SubModuleID = IIf(ReportType = "Q", SubModuleType.SFA_PERF_Q, SubModuleType.SFA_PERF_M)
            wuc_ctrlPnl.DataBind()

        End If
    End Sub
    Public ReadOnly Property PageName() As String
        Get
            Return "RPT_SRPerf2"
        End Get
    End Property

#Region "Event Handler"
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click, wuc_ctrlPnl.SearchBtn_Click
        RefreshDataBind()
        wuc_ctrlPnl.UpdateControlPanel()
    End Sub
    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    If IsPostBack Then RenewDataBind()
    'End Sub

    'Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
    '    RenewDataBind()
    'End Sub

    'Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
    '    RefreshDataBind()
    'End Sub

    'Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
    '    UpdateDatagrid_Update()
    'End Sub


#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPnl.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlPnl.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)

        dtCurrentTable = GetRecList()
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 10 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_IN.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    'Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    'dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    'dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    'dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    'If String.IsNullOrEmpty(CF_IN.GetOutputFormatString(ColumnName)) = False Then
                    '    dgColumn.DataTextFormatString = CF_IN.GetOutputFormatString(ColumnName)
                    'End If
                    'dgColumn.ItemStyle.HorizontalAlign = CF_IN.ColumnStyle(ColumnName).HorizontalAlign
                    'dgColumn.ItemStyle.Wrap = CF_IN.ColumnStyle(ColumnName).Wrap

                    'dgColumn.HeaderText = CF_IN.GetDisplayColumnName(ColumnName)
                    'dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName

                    'Dim strUrlFormatString As String
                    'Dim strUrlFields() As String = Nothing

                    'strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields)
                    'dgColumn.DataNavigateUrlFields = strUrlFields
                    'dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    'dgColumn.Target = "_self"
                    'dgList.Columns.Add(dgColumn)
                    'dgColumn = Nothing

                    ''Add the field name
                    'aryDataItem.Add(ColumnName)
                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_IN.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_IN.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    ElseIf ColumnName Like "SP_*" Then
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_IN.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_IN.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_IN.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub

    '''' <summary>
    '''' Pass in the datatable, and generate the url format string based on the
    '''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    '''' to form the hyperlink fields need.
    '''' </summary>
    '''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    '''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    '''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    '''' <returns>URLFormatString, as url to pass with parameters</returns>
    '''' <remarks></remarks>
    'Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
    '    Dim strUrlFormatString As String = String.Empty
    '    Select Case intIndex
    '        Case 0 'Generate sales info by date links
    '            Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
    '            Dim arrFields As New ArrayList 'store field name used in grouping
    '            Dim licGroupFieldList As New ListItemCollection
    '            licGroupFieldList.Add(New ListItem("TEAM_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("REGION_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SALESREP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CHAIN_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CHANNEL_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CUST_GRP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CUST_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("PRD_GRP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("PRD_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SALES_AREA_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SHIPTO_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

    '            For Each liFieldToFind As ListItem In licGroupFieldList
    '                For Each DC As DataColumn In dtToBind.Columns
    '                    If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
    '                        liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
    '                        strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
    '                        Exit For
    '                    ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
    '                        liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
    '                        liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
    '                        strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
    '                        Exit For
    '                    End If
    '                Next
    '            Next

    '            ReDim strUrlFields(arrFields.Count - 1)
    '            For intIndx As Integer = 0 To arrFields.Count - 1
    '                strUrlFields(intIndx) = arrFields.Item(intIndx)
    '            Next

    '            Dim intTeamInd As Integer = licGroupFieldList.FindByText("TEAM_CODE").Value
    '            Dim intRegionInd As Integer = licGroupFieldList.FindByText("REGION_CODE").Value
    '            Dim intSalesrepInd As Integer = licGroupFieldList.FindByText("SALESREP_CODE").Value
    '            strUrlFormatString = "SalesInfoByDate.aspx?" + _
    '            strbFilterGroup.ToString + _
    '           IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
    '           IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _
    '           IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & Session("SALESREP_CODE"), "")

    '            Session("SalesInfoByDate_GroupingField") = arrFields
    '        Case Else
    '            strUrlFields = Nothing
    '            strUrlFormatString = ""
    '    End Select
    '    Return strUrlFormatString
    'End Function

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                If licItemSeqColl IsNot Nothing AndAlso licItemSeqColl.Count > e.Row.RowIndex Then
                    e.Row.CssClass = "GridLvl" & licItemSeqColl(e.Row.RowIndex)

                    Dim idx As Integer = aryDataItem.IndexOf("SALESREP_NAME")
                    If idx >= 0 Then
                        e.Row.Cells(idx).Text = ReplicateString("&nbsp;", licItemSeqColl(e.Row.RowIndex)) & e.Row.Cells(idx).Text
                    End If
                End If
            Case DataControlRowType.Footer
                Dim iIndex As Integer
                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_IN.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next
        End Select
    End Sub

    Private Function ReplicateString(ByVal strSrc As String, ByVal iFreq As Integer) As String
        Dim sb As New Text.StringBuilder(strSrc.Length * iFreq)
        For i As Integer = 1 To iFreq
            sb.Append(strSrc)
        Next
        Return sb.ToString
    End Function

#Region "Math Function"
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function
    Private Function ConvertToString(ByVal objValue As Object) As String
        ConvertToString = String.Empty
        If Not String.IsNullOrEmpty(objValue) Then ConvertToString = Trim(objValue)
        Return ConvertToString
    End Function
#End Region

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsRpt As New rpt_SFA.SFA_RPT
            Dim strSelectedLvlCode As String = String.Empty
            Dim strValuePath As String = Session("TREE_PATH")
            If Not String.IsNullOrEmpty(strValuePath) Then
                Dim strValues() As String
                strValues = strValuePath.Split("/")
                strSelectedLvlCode = strValues(strValues.Length - 1).Split("@")(1)
            End If

            If ReportType = "Q" Then
                DT = clsRpt.GET_SR_PERF2_QTD(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), Session("Year"), Session("Quarter"), strSelectedLvlCode, Session("SALESREP_LIST"), IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue"))))
            Else
                DT = clsRpt.GET_SR_PERF(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), Session("Year"), Session("Month"), strSelectedLvlCode, Session("SALESREP_LIST"), IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue"))))
            End If
            If DT Is Nothing Then Return DT
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
            Return DT
        End Try
        PreRenderMode(DT)
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
        'Cal_ItemSeqColl(DT)
    End Sub

    Private Sub Cal_CustomerHeader()
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "ST_*" Then
                strColumnName = "Sales Team"
            ElseIf strColumnName Like "AT_*" Then
                strColumnName = "Actual vs Target"
            ElseIf strColumnName Like "SP_*" Then
                strColumnName = "Supplier"
            ElseIf strColumnName Like "DP_*" Then
                strColumnName = "Distribution Point"
            Else
                strColumnName = CF_IN.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper

            If (strColumnName Like "SALES_*") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        licItemSeqColl = New Hashtable
        Dim idx As Integer = 0
        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
            licItemSeqColl.Add(idx, DR("LVL_SEQ"))
            idx += 1
        Next
    End Sub
    'Private Sub Cal_ItemSeqColl(ByRef DT As DataTable)
    '    licItemSeqColl = New Hashtable
    '    Dim idx As Integer = 0
    '    For Each DR As DataRow In DT.Rows
    '        licItemSeqColl.Add(idx, DR("LVL_SEQ"))
    '        idx += 1
    '    Next
    'End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    Public Class CF_IN
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""

            If ColumnName Like "*TGT" OrElse ColumnName Like "*_TGT_*" Then
                strFieldName = "Target"
            ElseIf ColumnName Like "*MT" OrElse ColumnName Like "*ACT" Then
                strFieldName = "Actual"
            ElseIf ColumnName Like "*ACH" Then
                strFieldName = "%"
            ElseIf ColumnName Like "SP_*" Then
                strFieldName = ColumnName.Replace("SP_", "")
            Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
            End If

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Select Case strColumnName
                Case "LVL_SEQ", "SALESREP_CODE"
                    FCT = FieldColumntype.InvisibleColumn
                Case Else
                    FCT = FieldColumntype.BoundColumn
            End Select
            Return FCT
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strStringFormat As String = ""
            If strColumnName.ToUpper Like "*MT" OrElse strColumnName Like "*ACT" OrElse strColumnName Like "*TGT" OrElse strColumnName Like "*ACH" Then
                strStringFormat = "{0:#,0}"
            End If

            Return strStringFormat
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" OrElse strColumnName Like "SP_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                    .Wrap = False
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

            Return CS
        End Function
    End Class
End Class
