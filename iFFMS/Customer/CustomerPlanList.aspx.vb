Imports System.Data

Partial Class CustomerPlanList
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerPlanList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strCustCode, strSalesmanCode As String
        'Put user code to initialize the page here
        Try
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            'Call Header
            With wuc_lblheader
                .Title = "Plan History"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                bindListBox(strSalesmanCode, strCustCode)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub


    Sub bindListBox(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getPlanListRouteDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                lstPlanList.DataSource = dt
                lstPlanList.DataTextField = "route_date"
                lstPlanList.DataValueField = "route_date"
                lstPlanList.DataBind()
            Else
                lstPlanList.Items.Add("Not Available")
                btnNextInfo.Visible = False
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Protected Sub btnNextInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNextInfo.Click
        Try
            For intloop As Integer = 0 To lstPlanList.Items.Count - 1
                If lstPlanList.SelectedItem.Selected = True Then
                    Response.Redirect("customerplanlistdetail.aspx?routedate=" + lstPlanList.SelectedItem.Value)
                End If
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnNextInfo_Click : " & ex.ToString)
        End Try

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub



End Class



