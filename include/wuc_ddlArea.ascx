<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlArea.ascx.vb" Inherits="include_wuc_ddlArea" %>
<asp:DropDownList id="ddlAreaID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator id="cfvAreaID" runat="server" ControlToValidate="ddlAreaID" Operator="GreaterThan"
	ValueToCompare="0" Display="Dynamic" CssClass="cls_validator"></asp:CompareValidator>

