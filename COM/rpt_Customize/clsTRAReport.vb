﻿
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsTRAReport
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTRAReport"
        End Get
    End Property


    Public Function GetTRAReport(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strSalesrepCode As String, _
   ByVal strSalesrepName As String, ByVal strSalesOrgCode As String, ByVal strSalesTeamCode As String, ByVal strSalesAreaCode As String, ByVal strFromTxnDate As String, ByVal strToxnDate As String, _
   ByVal strCustCode As String, ByVal strPrdGrpName As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_TRA"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SALESREP_NAME", strSalesrepName, clsDB.DataType.DBString)
                .addItem("SALES_ORG_CODE", strSalesOrgCode, clsDB.DataType.DBString)
                .addItem("SALES_TEAM_CODE", strSalesTeamCode, clsDB.DataType.DBString)
                .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_NAME", strPrdGrpName, clsDB.DataType.DBString)
                .addItem("FROM_TXN_DATE", strFromTxnDate, clsDB.DataType.DBString)
                .addItem("TO_TXN_DATE", strToxnDate, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTRAReport :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
