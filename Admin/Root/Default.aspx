﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Admin_Root_Default" StylesheetTheme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="~/include/DKSHMenu.css" />
</head>
<body>
    <form id="form1" runat="server">
    <br /><br /><br /><br />
    <div>
    <center>
        <asp:Label ID="lblErr" runat="server" skinid="label_err"></asp:Label>
        <div style=" font-size:18pt; color:#444444; font-family:Arial, Verdana;  font-weight:bold">
            <img src="../../images/superuser.jpg" />Super User Login</div>
        <table cellpadding="5" cellspacing="0" style="border-collapse:collapse; border:solid 1px gray; background-color: #f9f9f9; width: 360px">        
        <tr>
        <td style="width:20px"></td>
        <td align="right"><span class="ContentLogin">Login:</span></td><td align="right">
            <asp:TextBox ID="txtLogin" runat="server"></asp:TextBox></td>
        <td style="width:40px"></td>
        </tr>      
        <tr>
        <td style="width:20px"></td>
        <td align="right"><span class="ContentLogin">Password:</span></td><td align="right">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox></td>
        <td style="width:40px"></td>
        </tr>
        <tr>
            <td style="width:20px"></td>
            <td colspan="2" align="right"><asp:Button ID="btnLogin" runat="server" Text="Login" /></td>
            <td style="width:40px"></td>
        </tr>         
        </table>
        <br />
        <span style="font-size:6pt; color:#ababab; font-family:Arial, Verdana;">Restricted area. DKSH CSSC authorized personnel only. Trespassers will be shot.</span>
    </center>
        
        <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="txtLogin"
            ErrorMessage="User Login cannot be Blank !" CssClass="cls_validator" Display="Dynamic"
            ValidationGroup="Login" />
        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
            ErrorMessage="Password cannot be Blank !" CssClass="cls_validator" Display="Dynamic"
            ValidationGroup="Login" />
    </div>
    </form>
</body>
</html>
