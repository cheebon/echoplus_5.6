<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerPlanList" CodeFile="CustomerPlanList.aspx.vb" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="uc1" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
    <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr">
                                    <ContentTemplate>
                                &nbsp;
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                                        </td>
                                    </tr>
                                    <%--<tr class="Bckgroundreport">
                                        <td colspan="3">
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <%--<tr class="Bckgroundreport">
                                                    <td >
                                                        &nbsp;</td>
                                                        <td>
                                                        
                                                        </td>
                                                </tr>--%>
                                                 <tr class="Bckgroundreport">
                                        <td colspan="3" style="height=10px">
                                            &nbsp;</td>
                                    </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td class="Bckgroundreport">
                                                      
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                    <td align="left" style="width: 123px; height: 10px;">
                                                                            </td>
                                                                    </tr>
                                                                </table>
                                                        <asp:ListBox ID="lstPlanList" runat="server"></asp:ListBox>
                                                        <br />
                                                        <br />
                                                        <asp:Button ID="btnNextInfo" runat="server" Text="Next Info" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
<%--<table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td class="TinyTableText" align="right" colspan="15">
                                <a href="">Jan-June</a> | <a href="">July-Dec</a> | <a href="">Quarterly</a>
                            </td>
                        </tr>
                        <tr bgcolor="indigo">
                            <td class="TableLabel" align="center" width="15%" rowspan="2">
                                <font color="white"><b>Sales Reps</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>JAN</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>FEB</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>MAR</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>APR</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>MAY</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>JUNE</b></font></td>
                            <td class="TableLabel" align="center" width="5%" colspan="2">
                                <font color="white"><b>YTD</b></font></td>
                        </tr>
                        <tr bgcolor="indigo">
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>W</b></font></td>
                            <td class="TableLabel" align="center" width="4%">
                                <font color="white"><b>C</b></font></td>
                        </tr>
                        <tr bgcolor="#ffffcc">
                            <td class="TinyTableText" align="left" width="10%">
                                SHARON CHRISTINE
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">17</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">53</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">18</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">66</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="CallAnalysisListByDay.aspx">22</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">84</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">18</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">80</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">20</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">71</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">21</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <a href="">158</a></td>
                            <td class="TinyTableText" align="center" width="5%">
                                157</td>
                            <td class="TinyTableText" align="center" width="5%">
                                780</td>
                        </tr>
                        <tr bgcolor="lightblue">
                            <td class="TinyTableText" align="right" width="10%">
                                <b>Total</b></td>
                            <td class="TinyTableText" align="center" width="5%">
                                17
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                53
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                18
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                66
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                22
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                84
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                18
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                80
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                20
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                71
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                21
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                158
                            </td>
                            <td class="TinyTableText" align="center" width="5%">
                                <b>157</b></td>
                            <td class="TinyTableText" align="center" width="5%">
                                <b>780</b></td>
                        </tr>
                    </table>--%>