﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MerchandisingDetailsTVS.aspx.vb" Inherits="iFFMR_Customize_MerchandisingDetails" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>MerchandisingDetailsTVS</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../include/jquery-1.10.2.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            PositionViewDetailImg();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                PositionViewDetailImg();
            }
        });

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", "15px");
            this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
            return this;
        }

        function ShowViewDetailImg() {
            var value = $('#hfViewDetailImgStatus').val()
            if (value == "") { $('#ViewDetailImg').hide(); }
            else {
                if (value == "Show") { $('#ViewDetailImg').show(); }
                else { $('#ViewDetailImg').hide(); };
            };
        }
        function SetViewDetailImg(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfViewDetailImgStatus').val('Hide'); }
            else {
                if (SetVis == 'ForceShow') { $('#hfViewDetailImgStatus').val('Show'); }
                else {
                    var value = $('#hfViewDetailImgStatus').val();
                    if (value == 'Hide') { $('#hfViewDetailImgStatus').val('Show'); }
                    else { $('#hfViewDetailImgStatus').val('Hide'); }
                }
            }
        }
        function PositionViewDetailImg() {
            $('#ViewDetailImg').center();
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmMerchandisingDetailsTVS" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">

                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px"></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table id="Table1" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress2" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnBack" runat="server" CssClass="cls_button" PostBackUrl="~/iFFMR/Customize/Merchandising.aspx"
                                                                        ToolTip="Back To Previous Page." Text="Back"></asp:Button>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                </td>
                                                            </tr>
                                                            <%--<tr>
                                                                <td>
                                                                    <customToolkit:wuc_dgpaging ID="Wuc_dgpaging" runat="server" />
                                                                </td>
                                                            </tr>--%>
                                                            <tr>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="455" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    RowHighlightColor="AntiqueWhite" DataKeyNames="PRD_CODE, HD_ACTIVITY_CODE, CUST_CODE, TXN_NO">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                                    <%--<ccGV:clsGridView ID="dgList" runat="server" ShowFooter="True" AllowSorting="True"
                                                                        AutoGenerateColumns="True" Width="98%" FreezeHeader="True" GridHeight="455"
                                                                        AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                                        FreezeRows="0" GridWidth="">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <asp:UpdatePanel ID="UpdateViewDetailImg" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:HiddenField ID="hfViewDetailImgStatus" runat="server" Value="Hide" />
                            <div id="ViewDetailImg" style="display: none; width: auto; float: left; position: absolute; z-index: 100; width: 680px;">
                                <div class="shadow">
                                    <div class="content">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnRefreshDetailImg" runat="server" Text="Refresh" CssClass="cls_button" Style="display: none;" />
                                                    <asp:Button ID="btnCloseViewDetailImg" runat="server" Text="Close" OnClientClick="SetViewDetailImg('ForceHide'); ShowViewDetailImg();PositionViewDetailImg();return false;" CssClass="cls_button" />
                                                    <asp:HiddenField ID="hfDetailImgCatCode" runat="server" />
                                                    <asp:HiddenField ID="hfDetailImgSubCatCode" runat="server" />
                                                    <asp:HiddenField ID="hfDetailImgCustCode" runat="server" />
                                                    <asp:HiddenField ID="hfDetailImgTxnNo" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 640px; height: 550px; overflow: auto;">
                                                        <asp:Image ID="imgMssDtl" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <asp:HiddenField ID="hfViewDetailImg" runat="server" />
                                </div>
                            </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </form>
    
</body>
</html>
