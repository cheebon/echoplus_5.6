﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSOVariance
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSOVariance"
        End Get
    End Property

    Function GetSOVariance(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
 ByVal strSearchDateBy As String, ByVal strSearchStartDateValue As String, ByVal strSearchEndDateValue As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strSoNo As String, _
 ByVal strPdaOrdNo As String, ByVal strStatus As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SUMM_ORD_VARIANCE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SEARCH_DATE_BY", strSearchDateBy, clsDB.DataType.DBString)
                .addItem("SEARCH_START_DATE_VALUE", strSearchStartDateValue, clsDB.DataType.DBString)
                .addItem("SEARCH_END_DATE_VALUE", strSearchEndDateValue, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("SO_NO", strSoNo, clsDB.DataType.DBString)
                .addItem("PDA_ORD_NO ", strPdaOrdNo, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetSOVarianceDtl(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
 ByVal strSoNo As String, ByVal strInvNo As String, ByVal strNetValue As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SUMM_ORD_VARIANCE_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SO_NO", strSoNo, clsDB.DataType.DBString)
                .addItem("INV_NO", strInvNo, clsDB.DataType.DBString)
                .addItem("NET_VALUE", strNetValue, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function SOVarianceUpdate(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
 ByVal strPdaOrdNo As String, ByVal strSoNo As String, ByVal strInvNo As String, ByVal strRemarks As String, ByVal strStatus As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_SUMM_ORD_VARIANCE_UPDATE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("PDA_ORD_NO", strPdaOrdNo, clsDB.DataType.DBString)
                .addItem("SO_NO ", strSoNo, clsDB.DataType.DBString)
                .addItem("INV_NO", strInvNo, clsDB.DataType.DBString)
                .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
