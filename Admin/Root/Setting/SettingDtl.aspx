﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SettingDtl.aspx.vb" Inherits="Admin_Root_Setting_SettingDtl" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Setting Detail</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmSettingDtl" runat="server">
    
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /><br />
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <!--Begin Modify Here -->
                <div id="MainFrame" class="MainFrame">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 0px 15px 0px;">
                                            <tr>
                                                <td>
                                                    <div class="border_std">
                                                    <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="False"
                                                        UseSubmitBehavior="false" Width="60px" />
                                                    </div>
                                                </td>
                                            </tr>                                           
                                            <tr class="ajax__tab_portal">
                                                <td class="ajax__tab_PNL">
                                                    <div class="border_std">
                                                    <customToolkit:wuc_lblInfo ID="lblErrDtl" runat="server" />
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                            <asp:Button ID="btnEditDtl" runat="server" Text="Edit" CausesValidation="False"
                                                                Width="60px" />&nbsp;
                                                            <asp:Button ID="btnSaveDtl" runat="server" Text="Save" CausesValidation="true"
                                                                ValidationGroup="ValidateDtl" Width="60px" />&nbsp;
                                                            <asp:Button ID="btnCancelDtl" runat="server" Text="Cancel"
                                                                CausesValidation="false" Width="60px" />&nbsp;
                                                            <asp:Label ID="lblRef" runat="server" Text="Reference a setting:"></asp:Label> &nbsp; 
                                                            <asp:DropDownList ID="ddlSetting" runat="server" AutoPostBack="true" Visible="false">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="width:50%">
                                                                <asp:DetailsView ID="dvSettingMisc" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="SETTING_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="Setting ID">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSettingID" runat="server"  Enabled="false" Text='<%# Bind("SETTING_ID") %>' MaxLength="20" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtSettingID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                                
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSettingID" runat="server" Enabled="false" Text='Auto Assign' MaxLength="20" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtSettingID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                                    
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSettingID" runat="server" Text='<%# Bind("SETTING_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                   <%--     <asp:TemplateField HeaderText="Page Size">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtPageSize" runat="server" Enabled="true" Text='<%# Bind("PAGE_SIZE") %>' MaxLength="8" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="txtPageSize"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                                <asp:RegularExpressionValidator ID="revInteger"
                                                                                    runat="server" ErrorMessage="Must Integer" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" ControlToValidate="txtPageSize" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtPageSize" runat="server" Enabled="true" Text='' MaxLength="8" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="txtPageSize"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                                <asp:RegularExpressionValidator ID="revInteger"
                                                                                    runat="server" ErrorMessage="Must Integer" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" ControlToValidate="txtPageSize" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPageSize" runat="server" Text='<%# Bind("PAGE_SIZE") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                       
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>
                                                            <td></td>
                                                            
                                                        </tr>
                                                        <tr valign="top">
                                                        <td style="width:50%">
                                                                <asp:DetailsView ID="dvFFMA" runat="server" AutoGenerateRows="False" Width="100%"
                                                                   DataKeyNames="SETTING_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="FFMA Server IP">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAServerIP" runat="server" Enabled="TRUE" Text='<%# Bind("FFMA_SERVER_IP") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAServerIP" runat="server" ControlToValidate="txtFFMAServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAServerIP" runat="server" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAServerIP" runat="server" ControlToValidate="txtFFMAServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="txtlblFFMAServerIP" runat="server" Text='<%# Bind("FFMA_SERVER_IP") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMA Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAServerName" runat="server" Enabled="true" Text='<%# Bind("FFMA_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAServerName" runat="server" ControlToValidate="txtFFMAServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAServerName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAServerName" runat="server" ControlToValidate="txtFFMAServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMAServerName" runat="server" Text='<%# Bind("FFMA_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMA DB Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMADBName" runat="server" Enabled="true" Text='<%# Bind("FFMA_DB_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMADBName" runat="server" ControlToValidate="txtFFMADBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMADBName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMADBName" runat="server" ControlToValidate="txtFFMADBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMADBName" runat="server" Text='<%# Bind("FFMA_DB_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMA Link Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMALinkServer" runat="server" Enabled="true" Text='<%# Bind("FFMA_LINK_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMALinkServer" runat="server" ControlToValidate="txtFFMALinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMALinkServer" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMALinkServer" runat="server" ControlToValidate="txtFFMALinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMALinkServer" runat="server" Text='<%# Bind("FFMA_LINK_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMA User ID">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAUserID" runat="server" Enabled="true" Text='<%# Bind("FFMA_USER_ID") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAUserID" runat="server" ControlToValidate="txtFFMAUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAUserID" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAUserID" runat="server" ControlToValidate="txtFFMAUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMAUserID" runat="server" Text='<%# Bind("FFMA_USER_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMA User Password">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAPassword" runat="server" Enabled="true" Text='<%# Bind("FFMA_USER_PWD") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAPassword" runat="server" ControlToValidate="txtFFMAUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMAPassword" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMAPassword" runat="server" ControlToValidate="txtFFMAUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMAPassword" runat="server" Text='<%# Bind("FFMA_USER_PWD") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>
                                                        <td style="width:50%">
                                                                <asp:DetailsView ID="dvFFMR" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="SETTING_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="FFMR Server IP">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRServerIP" runat="server" Enabled="TRUE" Text='<%# Bind("FFMR_SERVER_IP") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRServerIP" runat="server" ControlToValidate="txtFFMRServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRServerIP" runat="server" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRServerIP" runat="server" ControlToValidate="txtFFMRServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="txtlblFFMRServerIP" runat="server" Text='<%# Bind("FFMR_SERVER_IP") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMR Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRServerName" runat="server" Enabled="true" Text='<%# Bind("FFMR_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRServerName" runat="server" ControlToValidate="txtFFMRServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRServerName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRServerName" runat="server" ControlToValidate="txtFFMRServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMRServerName" runat="server" Text='<%# Bind("FFMR_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMR DB Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRDBName" runat="server" Enabled="true" Text='<%# Bind("FFMR_DB_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRDBName" runat="server" ControlToValidate="txtFFMRDBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRDBName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRDBName" runat="server" ControlToValidate="txtFFMRDBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMRDBName" runat="server" Text='<%# Bind("FFMR_DB_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMR Link Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRLinkServer" runat="server" Enabled="true" Text='<%# Bind("FFMR_LINK_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRLinkServer" runat="server" ControlToValidate="txtFFMRLinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRLinkServer" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRLinkServer" runat="server" ControlToValidate="txtFFMRLinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMRLinkServer" runat="server" Text='<%# Bind("FFMR_LINK_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMR User ID">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRUserID" runat="server" Enabled="true" Text='<%# Bind("FFMR_USER_ID") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRUserID" runat="server" ControlToValidate="txtFFMRUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRUserID" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRUserID" runat="server" ControlToValidate="txtFFMRUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMRUserID" runat="server" Text='<%# Bind("FFMR_USER_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMR User Password">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRPassword" runat="server" Enabled="true" Text='<%# Bind("FFMR_USER_PWD") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRPassword" runat="server" ControlToValidate="txtFFMRUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMRPassword" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMRPassword" runat="server" ControlToValidate="txtFFMRUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMRPassword" runat="server" Text='<%# Bind("FFMR_USER_PWD") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>                                                        
                                                        </tr>
                                                        <tr valign="top">
                                                        <td style="width:50%">
                                                                <asp:DetailsView ID="dvFFMS" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="SETTING_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="FFMS Server IP">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSServerIP" runat="server" Enabled="TRUE" Text='<%# Bind("FFMS_SERVER_IP") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSServerIP" runat="server" ControlToValidate="txtFFMSServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSServerIP" runat="server" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSServerIP" runat="server" ControlToValidate="txtFFMSServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="txtlblFFMSServerIP" runat="server" Text='<%# Bind("FFMS_SERVER_IP") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMS Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSServerName" runat="server" Enabled="true" Text='<%# Bind("FFMS_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSServerName" runat="server" ControlToValidate="txtFFMSServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSServerName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSServerName" runat="server" ControlToValidate="txtFFMSServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMSServerName" runat="server" Text='<%# Bind("FFMS_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMS DB Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSDBName" runat="server" Enabled="true" Text='<%# Bind("FFMS_DB_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSDBName" runat="server" ControlToValidate="txtFFMSDBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSDBName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSDBName" runat="server" ControlToValidate="txtFFMSDBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMSDBName" runat="server" Text='<%# Bind("FFMS_DB_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMS Link Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSLinkServer" runat="server" Enabled="true" Text='<%# Bind("FFMS_LINK_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSLinkServer" runat="server" ControlToValidate="txtFFMSLinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSLinkServer" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSLinkServer" runat="server" ControlToValidate="txtFFMSLinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMSLinkServer" runat="server" Text='<%# Bind("FFMS_LINK_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMS User ID">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSUserID" runat="server" Enabled="true" Text='<%# Bind("FFMS_USER_ID") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSUserID" runat="server" ControlToValidate="txtFFMSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSUserID" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSUserID" runat="server" ControlToValidate="txtFFMSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMSUserID" runat="server" Text='<%# Bind("FFMS_USER_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="FFMS User Password">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSPassword" runat="server" Enabled="true" Text='<%# Bind("FFMS_USER_PWD") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSPassword" runat="server" ControlToValidate="txtFFMSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtFFMSPassword" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvFFMSPassword" runat="server" ControlToValidate="txtFFMSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFFMSPassword" runat="server" Text='<%# Bind("FFMS_USER_PWD") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td> 
                                                        <td style="width:50%">
                                                                <asp:DetailsView ID="dvEchoplus" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="SETTING_ID">
                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="ECHOPLUS Server IP">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSServerIP" runat="server" Enabled="TRUE" Text='<%# Bind("ECHOPLUS_SERVER_IP") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSServerIP" runat="server" ControlToValidate="txtECHOPLUSServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSServerIP" runat="server" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSServerIP" runat="server" ControlToValidate="txtECHOPLUSServerIP"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="txtlblECHOPLUSServerIP" runat="server" Text='<%# Bind("ECHOPLUS_SERVER_IP") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ECHOPLUS Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSServerName" runat="server" Enabled="true" Text='<%# Bind("ECHOPLUS_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSServerName" runat="server" ControlToValidate="txtECHOPLUSServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSServerName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSServerName" runat="server" ControlToValidate="txtECHOPLUSServerName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblECHOPLUSServerName" runat="server" Text='<%# Bind("ECHOPLUS_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ECHOPLUS DB Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSDBName" runat="server" Enabled="true" Text='<%# Bind("ECHOPLUS_DB_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSDBName" runat="server" ControlToValidate="txtECHOPLUSDBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSDBName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSDBName" runat="server" ControlToValidate="txtECHOPLUSDBName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblECHOPLUSDBName" runat="server" Text='<%# Bind("ECHOPLUS_DB_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ECHOPLUS Link Server Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSLinkServer" runat="server" Enabled="true" Text='<%# Bind("ECHOPLUS_LINK_SERVER_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSLinkServer" runat="server" ControlToValidate="txtECHOPLUSLinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSLinkServer" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSLinkServer" runat="server" ControlToValidate="txtECHOPLUSLinkServer"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblECHOPLUSLinkServer" runat="server" Text='<%# Bind("ECHOPLUS_LINK_SERVER_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ECHOPLUS User ID">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSUserID" runat="server" Enabled="true" Text='<%# Bind("ECHOPLUS_USER_ID") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSUserID" runat="server" ControlToValidate="txtECHOPLUSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSUserID" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSUserID" runat="server" ControlToValidate="txtECHOPLUSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblECHOPLUSUserID" runat="server" Text='<%# Bind("ECHOPLUS_USER_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ECHOPLUS User Password">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSPassword" runat="server" Enabled="true" Text='<%# Bind("ECHOPLUS_USER_PWD") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSPassword" runat="server" ControlToValidate="txtECHOPLUSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtECHOPLUSPassword" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvECHOPLUSPassword" runat="server" ControlToValidate="txtECHOPLUSUserID"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblECHOPLUSPassword" runat="server" Text='<%# Bind("ECHOPLUS_USER_PWD") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>   
                                                        </tr>
                                                    </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--End Modify Here -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
