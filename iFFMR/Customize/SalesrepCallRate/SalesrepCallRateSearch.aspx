<%@ page language="VB" autoeventwireup="false"  CodeFile="~/iFFMR/Customize/SalesrepCallRate/SalesrepCallRateSearch.aspx.vb"  inherits="iFFMR_Customize_SalesrepCallRate_SalesrepCallRateSearch"%>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Field Force Call Rate</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

<script language="javascript" type="text/javascript">
function RefreshContentBar()
{var TEAM_CODE = "";var SALESREP_CODE = "";var SALESREP_NAME= "";var SALESTEAM_NAME= "";var listbox = document.getElementById("lsbSelectedTeam");
for(var i=0;i<listbox.options.length;i++){if(listbox.options[i].selected = true)if (i > 0){TEAM_CODE = TEAM_CODE + ",";}TEAM_CODE = TEAM_CODE + listbox.options[i].value ;if (i > 0){SALESTEAM_NAME = SALESTEAM_NAME + ","; }SALESTEAM_NAME = SALESTEAM_NAME + listbox.options[i].text.split(" ")[2];}
var listbox2 = document.getElementById("lsbSelectedSalesrep");
for(var i=0;i<listbox2.options.length;i++){if(listbox2.options[i].selected = true)if (i > 0){SALESREP_CODE = SALESREP_CODE + ","; }SALESREP_CODE = SALESREP_CODE + listbox2.options[i].value ;if (i > 0){SALESREP_NAME = SALESREP_NAME + ","; }SALESREP_NAME = SALESREP_NAME + listbox2.options[i].text.split(" ")[2];}
parent.document.getElementById('ContentBarIframe').src="../../iFFMR/Customize/SalesrepCallRate/SalesrepCallRateGrid.aspx?SALESREP_CODE=" + SALESREP_CODE + "&TEAM_CODE=" + TEAM_CODE;
var spnSalesrepName = document.getElementById("hdnSalesrepName");if(spnSalesrepName) spnSalesrepName.innerHTML=SALESREP_NAME;var spnSalesTeamName = document.getElementById("hdnSalesTeamName");if(spnSalesTeamName) spnSalesTeamName.innerHTML=SALESTEAM_NAME;
}
function HideElement(element){var TopDiv =self.parent.document.getElementById(element);  TopDiv.style.display='none';}      

</script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesrepCallRatesearch" runat="server">
    <ajaxtoolkit:toolkitscriptmanager id="ToolkitScriptManager1" runat="server" asyncpostbacktimeout="300"
        scriptmode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                        <tr align="left">
                            <td colspan="4">
                                <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right">
                                <input type="image" id="imgclose" runat="server" src="~/images/ico_close.gif" enableviewstate="false"
                                    onclick="HideElement('TopBar');" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="width: 45%;">
                                <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center" style="width: 10%;">
                                <table>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35"
                                                Text="<<" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" style="width: 35%">
                                <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center">
                                <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                            </td>
                            <td valign="middle" align="center">
                                <table>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                Text="<" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                Text=">>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                Text="<<" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center">
                                <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                    Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" style="padding-left:8px; padding-bottom:6px" >
                                <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                    value="Search" class="cls_button" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
<span id="hdnSalesTeamName" style="display:none"></span>        
<span id="hdnSalesrepName" style="display:none"></span>        

    </form>
</body>
</html>
