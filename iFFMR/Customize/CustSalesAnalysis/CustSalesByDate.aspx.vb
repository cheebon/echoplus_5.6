﻿Imports System.Data
Partial Class iFFMR_Customize_CustSalesAnalysis_CustSalesByDate
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CustSalesInfoByDate"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustSalesByDate"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            'Put user code to initialize the page here
            'Call Header
            With wuc_lblheader
                .Title = "Customer Sales Info By Date" 'Report.GetName(SubModuleType.SALESINFOBYDATE) '"Sales Info By Date"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.SALESINFOBYDATE
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""

            If Not IsPostBack Then
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")


                ViewState("dtCurrentView") = Nothing
                If PreviousPage IsNot Nothing Then
                    'With CriteriaCollector
                    '    Session("Year") = .Year
                    '    Session("Month") = .Month
                    '    Session("PRINCIPAL_ID") = .PrincipalID
                    '    Session("PRINCIPAL_CODE") = .PrincipalCode
                    '    Session("NetValue") = .NetValue
                    '    ViewState("strSortExpression") = .SortExpression
                    'End With
                Else
                    Session("SalesInfoByDate_QueryString") = Trim(Request.QueryString.ToString)
                    CriteriaCollector = Nothing ' New clsSharedValue
                End If


                ViewState("TEAM_CODE") = Trim(Request.QueryString("TEAM_CODE"))
                ViewState("REGION_CODE") = Trim(Request.QueryString("REGION_CODE"))
                ViewState("SALESREP_CODE") = Trim(Request.QueryString("SALESREP_CODE"))

                ViewState("CHAIN_CODE") = Trim(Request.QueryString("CHAIN_CODE"))
                ViewState("CHANNEL_CODE") = Trim(Request.QueryString("CHANNEL_CODE"))
                ViewState("CUST_GRP_CODE") = Trim(Request.QueryString("CUST_GRP_CODE"))
                ViewState("CUST_CODE") = Trim(Request.QueryString("CUST_CODE"))
                ViewState("PRD_GRP_CODE") = Trim(Request.QueryString("PRD_GRP_CODE"))
                ViewState("PRD_CODE") = Trim(Request.QueryString("PRD_CODE"))
                ViewState("SALES_AREA_CODE") = Trim(Request.QueryString("SALES_AREA_CODE"))
                ViewState("SHIPTO_CODE") = Trim(Request.QueryString("SHIPTO_CODE"))

                Dim strKeyName, strKeyValue As String
                strKeyName = String.Empty
                strKeyValue = String.Empty
                For Each KEY As String In Request.QueryString
                    If KEY Like "TREE*CODE" Then
                        strKeyName = KEY
                        strKeyValue = Trim(Request.QueryString(strKeyName))
                        Exit For
                    End If
                Next
                If strKeyName <> String.Empty Then
                    Session("SALESREP_LIST") = Report.GetSalesrepList(strKeyName.Replace("TREE_", ""), strKeyValue)
                End If

                TimerControl1.Enabled = True
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "Event Handler"

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 20 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "SalesInfoByDate")
            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    'dgList.ShowFooter = False
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

#End Region

#Region "Custom dgList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CustSalesInfo.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CustSalesInfo.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CustSalesInfo.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CustSalesInfo.ColumnStyle(ColumnName).HorizontalAlign


                        dgColumn.HeaderText = CF_CustSalesInfo.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing
                        strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields)
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CustSalesInfo.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CustSalesInfo.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CustSalesInfo.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Pass in the datatable, and generate the url format string based on the
    ''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    ''' to form the hyperlink fields need.
    ''' </summary>
    ''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    ''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    ''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    ''' <returns>URLFormatString, as url to pass with parameters</returns>
    ''' <remarks></remarks>
    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Try
            Select Case intIndex
                Case 0 'Generate sales enquiry link
                    ReDim strUrlFields(0)
                    strUrlFields(0) = "DATE"
                    'strUrlFormatString = "SalesSummList.aspx?SELECTED_DATE={0:yyyy-MM-dd}&" + Session("SalesInfoByDate_QueryString") & "&PAGE_INDICATOR=SALESINFOBYDATE"
                    strUrlFormatString = ("parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/CustSalesAnalysis/CustSalesSummList.aspx?" + _
                                          "SELECTED_DATE={0:yyyy-MM-dd}&" + Session("SalesInfoByDate_QueryString") & "&PAGE_INDICATOR=CUSTSALESINFOBYDATE'")
                Case Else
                    strUrlFields = Nothing
                    strUrlFormatString = ""
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        End Try
        Return strUrlFormatString
    End Function


    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    For Each TC As TableCell In e.Row.Cells
                        If TC.Controls.Count > 0 Then
                            Dim HYL As HyperLink = CType(TC.Controls(0), HyperLink)
                            If HYL IsNot Nothing Then
                                Dim strlink As String = HYL.NavigateUrl
                                HYL.NavigateUrl = "#"
                                HYL.Target = String.Empty
                                HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();")
                            End If
                        End If
                    Next
                Case DataControlRowType.Footer
                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_CustSalesInfo.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim intNetValue As Integer
            Dim strYear, strMonth, strUserID, strPrincipalId, strPrincipalCode As String
            strYear = Session("Year")
            strMonth = Session("Month")
            strUserID = Session("UserID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

            Dim strTeamCode, strRegionCode, strSalesRepCode, strSalesAreaCode, _
                strChainCode, strChannelCode, strCustGrpCode, strCustCode, _
                strPrdGrpCode, strPrdCode, strShiptoCode As String
            strTeamCode = ViewState("TEAM_CODE")
            strRegionCode = ViewState("REGION_CODE")
            strSalesRepCode = ViewState("SALESREP_CODE")
            strChainCode = ViewState("CHAIN_CODE")
            strChannelCode = ViewState("CHANNEL_CODE")
            strCustGrpCode = ViewState("CUST_GRP_CODE")
            strCustCode = ViewState("CUST_CODE")
            strPrdGrpCode = ViewState("PRD_GRP_CODE")
            strPrdCode = ViewState("PRD_CODE")
            strSalesAreaCode = ViewState("SALES_AREA_CODE")
            strShiptoCode = ViewState("SHIPTO_CODE")

            Dim strTreeSalesrep As String
            strTreeSalesrep = Session("SALESREP_LIST")
            'Stored Criteria into Static Value Collector
            'With CriteriaCollector
            '    .Year = strYear
            '    .Month = strMonth
            '    .PrincipalID = strPrincipalId
            '    .PrincipalCode = strPrincipalCode
            '    .NetValue = intNetValue
            'End With

            Dim clsSalesDB As New rpt_Customize.clsCustSalesAnalysis
            DT = clsSalesDB.GetCustSalesAnalysisByDate( _
                strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, _
                strTeamCode, strRegionCode, strSalesRepCode, _
                strChainCode, strChannelCode, strCustGrpCode, strCustCode, strPrdGrpCode, strPrdCode, _
                 strSalesAreaCode, strShiptoCode, intNetValue, strTreeSalesrep)

            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper

                If (strColumnName Like "SALES*" OrElse strColumnName Like "*QTY") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

End Class

Public Class CF_CustSalesInfo
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "SALES_QTY"
                strFieldName = "Sales QTY"
            Case "TRADE_RET_QTY"
                strFieldName = "Trade Return QTY"
            Case "FOC_QTY"
                strFieldName = "FOC"
            Case "SALES"
                strFieldName = "Sales Value"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Select Case strColumnName.ToUpper
                Case "DATE"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESSUMM, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                    'Case "DATE"
                    '    FCT = FieldColumntype.InvisibleColumn
                Case Else
                    FCT = FieldColumntype.BoundColumn
            End Select
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case "SALES"
                    strStringFormat = "{0:#,0.00}"
                Case "FOC_QTY", "SALES_QTY", "TRADE_RET_QTY"
                    strStringFormat = "{0:#,0}"
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class