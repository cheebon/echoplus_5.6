<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlSalesRep.ascx.vb" Inherits="include_wuc_ddlSalesRep" %>
<asp:DropDownList id="ddlSalesRepID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvSalesRepID" ControlToValidate="ddlSalesRepID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select Sales Representative."></asp:CompareValidator>
