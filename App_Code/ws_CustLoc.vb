﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports cor_DB
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Runtime.Serialization.Json
Imports System.IO

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class ws_CustLoc
    Inherits System.Web.Services.WebService

    Private strConn As String = Session("ffma_conn")

#Region "Helper class"
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffmr_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class
    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class
    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
#End Region

    '************************************************
    '
    'Define class structure to package returned data
    '
    '************************************************
#Region "Class Entity Defintion"
    Public Class Customer
        Inherits BaseEntity
#Region "LocalVariable"
        Private _CustCode As String = String.Empty
        Private _CustName As String = String.Empty
        Private _Latitude As String = String.Empty
        Private _Longitude As String = String.Empty
        Private _ImgLoc As String = String.Empty
        Private _Add1 As String = String.Empty
        Private _Add2 As String = String.Empty
        Private _Add3 As String = String.Empty
        Private _Add4 As String = String.Empty
        Private _City As String = String.Empty
        Private _Cust_Class As String = String.Empty
#End Region
#Region "Public Property"
        Public Property CUST_CODE() As String
            Get
                Return _CustCode
            End Get
            Set(ByVal value As String)
                _CustCode = value
            End Set
        End Property
        Public Property CUST_NAME() As String
            Get
                Return _CustName
            End Get
            Set(ByVal value As String)
                _CustName = value
            End Set
        End Property
        Public Property LATITUDE() As String
            Get
                Return _Latitude
            End Get
            Set(ByVal value As String)
                _Latitude = value
            End Set
        End Property
        Public Property LONGITUDE() As String
            Get
                Return _Longitude
            End Get
            Set(ByVal value As String)
                _Longitude = value
            End Set
        End Property
        Public Property IMG_LOC() As String
            Get
                Return _ImgLoc
            End Get
            Set(ByVal value As String)
                _ImgLoc = value
            End Set
        End Property
        Public Property ADD_1() As String
            Get
                Return _Add1
            End Get
            Set(ByVal value As String)
                _Add1 = value
            End Set
        End Property
        Public Property ADD_2() As String
            Get
                Return _Add2
            End Get
            Set(ByVal value As String)
                _Add2 = value
            End Set
        End Property
        Public Property ADD_3() As String
            Get
                Return _Add3
            End Get
            Set(ByVal value As String)
                _Add3 = value
            End Set
        End Property
        Public Property ADD_4() As String
            Get
                Return _Add4
            End Get
            Set(ByVal value As String)
                _Add4 = value
            End Set
        End Property
        Public Property CITY() As String
            Get
                Return _City
            End Get
            Set(ByVal value As String)
                _City = value
            End Set
        End Property
        Public Property CUST_CLASS() As String
            Get
                Return _Cust_Class
            End Get
            Set(ByVal value As String)
                _Cust_Class = value
            End Set
        End Property
#End Region
    End Class
    Public Class CustomerProfile
        Inherits BaseEntity
        Private _Latitude As String = String.Empty
        Private _Longitude As String = String.Empty
        Private strCustName As String
        Private strContCode As String
        Private strContName As String
        Private strAddress As String
        Private strDistrict As String
        Private strCustGrp As String
        Private strCustClass As String
        Private strCustType As String
        Private strMTD As String
        Private strYTD As String
        Private strSKU As String
        Private strImgLoc As String
        Private strOutBal As String
        Private strCreditLimit As String
        Private strCustCode As String

        Public Property CUST_CODE() As String
            Get
                Return strCustCode
            End Get
            Set(ByVal value As String)
                strCustCode = value
            End Set
        End Property

        Public Property CUST_NAME() As String
            Get
                Return strCustName
            End Get
            Set(ByVal value As String)
                strCustName = value
            End Set
        End Property
        Public Property ADDRESS() As String
            Get
                Return strAddress
            End Get
            Set(ByVal value As String)
                strAddress = value
            End Set
        End Property
        Public Property DISTRICT() As String
            Get
                Return strDistrict
            End Get
            Set(ByVal value As String)
                strDistrict = value
            End Set
        End Property
        Public Property CONT_CODE() As String
            Get
                Return strContCode
            End Get
            Set(ByVal value As String)
                strContCode = value
            End Set
        End Property
        Public Property CONT_NAME() As String
            Get
                Return strContName
            End Get
            Set(ByVal value As String)
                strContName = value
            End Set
        End Property
        Public Property CUST_GRP_NAME() As String
            Get
                Return strCustGrp
            End Get
            Set(ByVal value As String)
                strCustGrp = value
            End Set
        End Property
        Public Property CUST_CLASS() As String
            Get
                Return strCustClass
            End Get
            Set(ByVal value As String)
                strCustClass = value
            End Set
        End Property
        Public Property CUST_TYPE() As String
            Get
                Return strCustType
            End Get
            Set(ByVal value As String)
                strCustType = value
            End Set
        End Property
        Public Property MTD_SALES() As String
            Get
                Return strMTD
            End Get
            Set(ByVal value As String)
                strMTD = value
            End Set
        End Property
        Public Property YTD_SALES() As String
            Get
                Return strYTD
            End Get
            Set(ByVal value As String)
                strYTD = value
            End Set
        End Property
        Public Property NO_SKU() As String
            Get
                Return strSKU
            End Get
            Set(ByVal value As String)
                strSKU = value
            End Set
        End Property
        Public Property IMG_LOC() As String
            Get
                Return strImgLoc
            End Get
            Set(ByVal value As String)
                strImgLoc = value
            End Set
        End Property
        Public Property LATITUDE() As String
            Get
                Return _Latitude
            End Get
            Set(ByVal value As String)
                _Latitude = value
            End Set
        End Property
        Public Property LONGITUDE() As String
            Get
                Return _Longitude
            End Get
            Set(ByVal value As String)
                _Longitude = value
            End Set
        End Property
        Public Property CREDITLIMIT() As String
            Get
                Return strCreditLimit
            End Get
            Set(ByVal value As String)
                strCreditLimit = value
            End Set
        End Property
        Public Property OUTBAL() As String
            Get
                Return strOutBal
            End Get
            Set(ByVal value As String)
                strOutBal = value
            End Set
        End Property

    End Class
#End Region


    ''' <summary>
    ''' This method is a sample method created during RnD stage of google maps
    ''' </summary>
    ''' <param name="strSearchType"></param>
    ''' <param name="strSearchValue"></param>
    ''' <param name="strStatus"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)> _
    Public Function ReturnCustomerGPS(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As PagedResult(Of Customer)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of Customer)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffma_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_CUST_LIST_MAP"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "SEARCH_TYPE", strSearchType, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SEARCH_VALUE", strSearchValue, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "STATUS", strStatus, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_Customer(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            rptResult = New PagedResult(Of Customer)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_RoutePlan.ReturnRoute :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    ''' <summary>
    ''' The real method used for Customer Profile page to query customer details
    ''' </summary>   
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)> _
    Public Function ReturnCustomerProfile(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal intNetValue As Integer, _
                                       ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strDate As String, _
                                       ByVal strCustNameFlag As String, ByVal strAddressFlag As String, ByVal strDistrictFlag As String, _
                                       ByVal strCustGrpFlag As String, ByVal strCustClassFlag As String, _
                                       ByVal strCustTypeFlag As String, ByVal strMTDFlag As String, _
                                       ByVal strYTDFlag As String, ByVal strSKUFlag As String, ByVal strImgLinkFlag As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String) As PagedResult(Of CustomerProfile)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of CustomerProfile)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_RPT_CUST_PROFILE_ENQ_MAP"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "CUST_NAME", strCustName, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "ADDRESS", strAddress, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DISTRICT", strDistrict, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_GRP_NAME", strCustGrp, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_CLASS", strCustClass, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_TYPE", strCustType, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", strPrincipalID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRINCIPAL_CODE", strPrincipalCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NET_VALUE", intNetValue, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DATE", strDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_NAME_FLAG", strCustNameFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "ADDRESS_FLAG", strAddressFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DISTRICT_FLAG", strDistrictFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_GRP_FLAG", strCustGrpFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_CLASS_FLAG", strCustClassFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_TYPE_FLAG", strCustTypeFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MTD_FLAG", strMTDFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "YTD_FLAG", strYTDFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SKU_FLAG", strSKUFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "IMG_LINK_FLAG", strImgLinkFlag, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)

                    clsCONN.addInputParam(sqlCmd, "MTDSTART", strMtdStart, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MTDEND", strMtdEnd, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "YTDSTART", strYtdStart, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "YTDEND", strYtdEnd, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NOSKUSTART", strNoSkuStart, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NOSKUEND", strNoSkuEnd, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TEAM_CODE", strTeamCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_CustomerProfile(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            rptResult = New PagedResult(Of CustomerProfile)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_CustLoc.ReturnCustomerProfile :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    ''' <summary>
    ''' The real method used for Customer Profile page to query customer details
    ''' </summary>   
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(EnableSession:=True)> _
    Public Function ReturnCustomerProfileByCode(ByVal strCustCode As String) As PagedResult(Of CustomerProfile)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of CustomerProfile)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_RPT_CUST_PROFILE_BY_CODE_MAP"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "CUST_CODE", strCustCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", Session("PRINCIPAL_ID"), clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NET_VALUE", Session("NetValue"), clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_CustomerProfile(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            rptResult = New PagedResult(Of CustomerProfile)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_CustLoc.ReturnCustomerProfile :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    ''' start Daily Tracking
    <WebMethod(EnableSession:=True)> _
    Public Function ReturnSalesrepCustStatusTracking(ByVal strSalesrepCode As String, ByVal strStartDate As String, ByVal strEndDate As String) As String
        '(ByVal strSalesrepCode As String, ByVal strDistCode As String, ByVal strRouteCode As String, ByVal strCustCode As String, ByVal strlatitude As String, ByVal strlongitude As String) As String
        Dim ms As New MemoryStream()
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of ReturnCoordinate)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_RPT_ROUTE_DAILY_TRACKING"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "user_id", Portal.UserSession.UserID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TREE_SALESREP_LIST", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "START_DATE", strStartDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "END_DATE", strEndDate, clsCONN.dtType.dbString)
                    'clsCONN.addInputParam(sqlCmd, "cust_code", strCustCode, clsCONN.dtType.dbString)
                    'If strlatitude.Trim <> "" Then
                    '    clsCONN.addInputParam(sqlCmd, "latitude", strlatitude, clsCONN.dtType.dbString)
                    'End If
                    'If strlongitude.Trim <> "" Then
                    '    clsCONN.addInputParam(sqlCmd, "longitude", strlongitude, clsCONN.dtType.dbString)
                    'End If
                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_Coordinate(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            'your object is your actual object (may be collection) you want to serialize to json
            Dim serializer As New DataContractJsonSerializer(rptRows.[GetType]())

            'serialize the object to memory stream
            serializer.WriteObject(ms, rptRows)

            'convert the serizlized object to string
            Dim jsonString As String = Encoding.[Default].GetString(ms.ToArray())

            'close the memory stream
            ms.Close()
            Return jsonString

        Catch ex As Exception
            Throw (New ExceptionMsg("ws_CustLoc.ReturnCustomerProfile :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function ReturnSalesrepHistoricalTracking(ByVal strSalesrepCode As String, ByVal strStartDate As String, ByVal strEndDate As String) As String
        '(ByVal strSalesrepCode As String, ByVal strDistCode As String, ByVal strRouteCode As String, ByVal strCustCode As String, ByVal strlatitude As String, ByVal strlongitude As String) As String
        Dim ms As New MemoryStream()
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of ReturnCoordinate)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_RPT_ROUTE_HISTORICAL_TRACKING"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "user_id", Portal.UserSession.UserID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TREE_SALESREP_LIST", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "START_DATE", strStartDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "END_DATE", strEndDate, clsCONN.dtType.dbString)

                    'clsCONN.addInputParam(sqlCmd, "dst_code", strDistCode, clsCONN.dtType.dbString)
                    'clsCONN.addInputParam(sqlCmd, "route_code", strRouteCode, clsCONN.dtType.dbString)
                    'clsCONN.addInputParam(sqlCmd, "cust_code", strCustCode, clsCONN.dtType.dbString)
                    'If strlatitude.Trim <> "" Then
                    '    clsCONN.addInputParam(sqlCmd, "latitude", strlatitude, clsCONN.dtType.dbString)
                    'End If
                    'If strlongitude.Trim <> "" Then
                    '    clsCONN.addInputParam(sqlCmd, "longitude", strlongitude, clsCONN.dtType.dbString)
                    'End If
                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_Coordinate(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            'your object is your actual object (may be collection) you want to serialize to json
            Dim serializer As New DataContractJsonSerializer(rptRows.[GetType]())

            'serialize the object to memory stream
            serializer.WriteObject(ms, rptRows)

            'convert the serizlized object to string
            Dim jsonString As String = Encoding.[Default].GetString(ms.ToArray())

            'close the memory stream
            ms.Close()
            Return jsonString

        Catch ex As Exception
            Throw (New ExceptionMsg("ws_CustLoc.ReturnCustomerProfile :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function ReturnPrdTracking(ByVal strRegionCode As String, ByVal strSalesAreaCode As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdGrpCode As String, ByVal strPrdCode As String, ByVal strStartDate As String, ByVal strEndDate As String) As String
        '(ByVal strSalesrepCode As String, ByVal strDistCode As String, ByVal strRouteCode As String, ByVal strCustCode As String, ByVal strlatitude As String, ByVal strlongitude As String) As String
        Dim ms As New MemoryStream()
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of ReturnCoordinate)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_RPT_ROUTE_PRD"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "user_id", Portal.UserSession.UserID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TREE_SALESREP_LIST", Session("SALESREP_LIST"), clsCONN.dtType.dbString, True)
                    clsCONN.addInputParam(sqlCmd, "PRD_GRP_CODE", strPrdGrpCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "START_DATE", strStartDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "END_DATE", strEndDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "REGION_CODE", strRegionCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SALES_AREA_CODE", strSalesAreaCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TEAM_CODE", strTeamCode, clsCONN.dtType.dbString)
                    'clsCONN.addInputParam(sqlCmd, "dst_code", strDistCode, clsCONN.dtType.dbString)
                    'clsCONN.addInputParam(sqlCmd, "route_code", strRouteCode, clsCONN.dtType.dbString)
                    'clsCONN.addInputParam(sqlCmd, "cust_code", strCustCode, clsCONN.dtType.dbString)
                    'If strlatitude.Trim <> "" Then
                    '    clsCONN.addInputParam(sqlCmd, "latitude", strlatitude, clsCONN.dtType.dbString)
                    'End If
                    'If strlongitude.Trim <> "" Then
                    '    clsCONN.addInputParam(sqlCmd, "longitude", strlongitude, clsCONN.dtType.dbString)
                    'End If
                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_Coordinate(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            'your object is your actual object (may be collection) you want to serialize to json
            Dim serializer As New DataContractJsonSerializer(rptRows.[GetType]())

            'serialize the object to memory stream
            serializer.WriteObject(ms, rptRows)

            'convert the serizlized object to string
            Dim jsonString As String = Encoding.[Default].GetString(ms.ToArray())

            'close the memory stream
            ms.Close()
            Return jsonString

        Catch ex As Exception
            Throw (New ExceptionMsg("ws_CustLoc.ReturnCustomerProfile :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    ''' end Daily Tracking

#Region "GENERATE COORDINATATE"

    <WebMethod(EnableSession:=True)> _
    Public Function GenerateCustLoc(strCustCode As String, ByVal strLat As Decimal, ByVal strlong As Decimal, strAccuracy As String) As Boolean
        Dim objDB As clsDB
        Dim SuccessGen As Boolean = False

        Try
            Using sqlConn As IDbConnection = CreateConnection(Session("ffma_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_CUST_GENERATE_LOC_ADD"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "cust_code", strCustCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "latitude", strLat, clsCONN.dtType.dbFloat)
                    clsCONN.addInputParam(sqlCmd, "longitude", strlong, clsCONN.dtType.dbFloat)
                    clsCONN.addInputParam(sqlCmd, "status", "SUCCESS", clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "accuracy", strAccuracy, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "generate_user_id", Portal.UserSession.UserID, clsCONN.dtType.dbString)

                    sqlCmd.ExecuteNonQuery()
                End Using
            End Using


            SuccessGen = True
            Return SuccessGen
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))

        Finally
            objDB = Nothing
        End Try
    End Function

    'update datetime for those lat long cannot be found from address
    <WebMethod(EnableSession:=True)> _
    Public Function GenerateCustLocUndefined(ByVal strCustCode As String) As Boolean
        Dim objDB As clsDB
        Dim SuccessGen As Boolean = False

        Try
            Using sqlConn As IDbConnection = CreateConnection(Session("ffma_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_CUST_GENERATE_LOC_ADD"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "cust_code", strCustCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "status", "FAILED", clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "generate_user_id", Portal.UserSession.UserID, clsCONN.dtType.dbString)

                    sqlCmd.ExecuteNonQuery()
                End Using
            End Using


            SuccessGen = True
            Return SuccessGen
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))

        Finally
            objDB = Nothing
        End Try
    End Function


    <WebMethod(EnableSession:=True)> _
    Public Function ReturnCustAdd() As PagedResult(Of ReturnAddress)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of ReturnAddress)()

            Using sqlConn As IDbConnection = CreateConnection(Session("ffma_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_CUST_GENERATE_LOC_LIST"
                    sqlCmd.CommandType = CommandType.StoredProcedure

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_CustAdd(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            Dim rptResult = New PagedResult(Of ReturnAddress)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_TrackingLoc.ReturnAddress :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

#End Region
    Public Shared Function Build_CustAdd(ByRef reader As IDataReader) As ReturnAddress
        Dim rptRow As New ReturnAddress
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)
                    Case "cust_code"
                        SetValue(Of String)(.CUST_CODE, reader("cust_code"), String.Empty)
                    Case "full_add"
                        SetValue(Of String)(.ADDRESS, reader("full_add"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function


    Public Shared Function Build_Customer(ByRef reader As IDataReader) As Customer
        Dim rptRow As New Customer
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)
                    Case "CUST_CODE"
                        SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
                    Case "CUST_NAME"
                        SetValue(Of String)(.CUST_NAME, reader("CUST_NAME"), String.Empty)
                    Case "LATITUDE"
                        SetValue(Of String)(.LATITUDE, reader("LATITUDE"), 0)
                    Case "LONGITUDE"
                        SetValue(Of String)(.LONGITUDE, reader("LONGITUDE"), 0)
                    Case "IMG_LOC"
                        SetValue(Of String)(.IMG_LOC, reader("IMG_LOC"), String.Empty)
                    Case "ADD_1"
                        SetValue(Of String)(.ADD_1, reader("ADD_1"), String.Empty)
                    Case "ADD_2"
                        SetValue(Of String)(.ADD_2, reader("ADD_2"), String.Empty)
                    Case "ADD_3"
                        SetValue(Of String)(.ADD_3, reader("ADD_3"), String.Empty)
                    Case "ADD_4"
                        SetValue(Of String)(.ADD_4, reader("ADD_4"), String.Empty)
                    Case "CITY"
                        SetValue(Of String)(.CITY, reader("CITY"), String.Empty)
                    Case "CUST_CLASS"
                        SetValue(Of String)(.CUST_CLASS, reader("CUST_CLASS"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function

    Public Shared Function Build_CustomerProfile(ByRef reader As IDataReader) As CustomerProfile
        Dim rptRow As New CustomerProfile
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)
                    Case "CUST_CODE"
                        SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
                    Case "CUST_NAME"
                        SetValue(Of String)(.CUST_NAME, reader("CUST_NAME"), String.Empty)
                    Case "LATITUDE"
                        SetValue(Of String)(.LATITUDE, reader("LATITUDE"), 0)
                    Case "LONGITUDE"
                        SetValue(Of String)(.LONGITUDE, reader("LONGITUDE"), 0)
                    Case "CONT_CODE"
                        SetValue(Of String)(.CONT_CODE, reader("CONT_CODE"), String.Empty)
                    Case "CONT_NAME"
                        SetValue(Of String)(.CONT_NAME, reader("CONT_NAME"), String.Empty)
                    Case "IMG_LOC"
                        SetValue(Of String)(.IMG_LOC, reader("IMG_LOC"), String.Empty)
                    Case "ADDRESS"
                        SetValue(Of String)(.ADDRESS, reader("ADDRESS"), String.Empty)
                    Case "DISTRICT"
                        SetValue(Of String)(.DISTRICT, reader("DISTRICT"), String.Empty)
                    Case "CUST_GRP_NAME"
                        SetValue(Of String)(.CUST_GRP_NAME, reader("CUST_GRP_NAME"), String.Empty)
                    Case "CUST_CLASS"
                        SetValue(Of String)(.CUST_CLASS, reader("CUST_CLASS"), String.Empty)
                    Case "CUST_TYPE"
                        SetValue(Of String)(.CUST_TYPE, reader("CUST_TYPE"), String.Empty)
                    Case "MTD_SALES"
                        SetValue(Of String)(.MTD_SALES, Convert.ToDecimal(reader("MTD_SALES")).ToString("N2"), String.Empty)
                    Case "YTD_SALES"
                        SetValue(Of String)(.YTD_SALES, Convert.ToDecimal(reader("YTD_SALES")).ToString("N2"), String.Empty)
                    Case "NO_SKU"
                        SetValue(Of String)(.NO_SKU, reader("NO_SKU"), String.Empty)
                    Case "CREDITLIMIT"
                        SetValue(Of String)(.CREDITLIMIT, Convert.ToDecimal(reader("CREDITLIMIT")).ToString("N2"), 0)
                    Case "OUTBAL"
                        SetValue(Of String)(.OUTBAL, Convert.ToDecimal(reader("OUTBAL")).ToString("N2"), 0)
                End Select
            Next
        End With
        Return rptRow
    End Function

    Public Shared Function Build_Coordinate(ByRef reader As IDataReader) As ReturnCoordinate
        Dim rptRow As New ReturnCoordinate
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)
                    Case "salesrep_code"
                        SetValue(Of String)(.SALESREP_CODE, reader("salesrep_code"), String.Empty)
                    Case "salesrep_name"
                        SetValue(Of String)(.SALESREP_NAME, reader("salesrep_name"), String.Empty)

                    Case "cust_code"
                        SetValue(Of String)(.CUST_CODE, reader("cust_code"), String.Empty)
                    Case "cust_name"
                        SetValue(Of String)(.CUST_NAME, reader("cust_name"), String.Empty)
                    Case "tel_no"
                        SetValue(Of String)(.TEL_NO, reader("tel_no"), String.Empty)
                        'Case "ttl_revenue"
                        '    SetValue(Of String)(.TTL_REVENUE, reader("ttl_revenue"), String.Empty)
                    Case "last_gps_dt"
                        SetValue(Of String)(.LAST_GPS_DT, reader("last_gps_dt"), String.Empty)
                    Case "location"
                        SetValue(Of String)(.LOCATION, reader("location"), String.Empty)
                    Case "latitude"
                        SetValue(Of String)(.LATITUDE, reader("latitude"), 0)
                    Case "longitude"
                        SetValue(Of String)(.LONGITUDE, reader("longitude"), 0)
                    Case "color"
                        SetValue(Of String)(.COLOR, reader("color"), 0)
                    Case "txn_timestamp"
                        SetValue(Of String)(.TXN_TIMESTAMP, reader("txn_timestamp"), 0)
                End Select
            Next
        End With
        Return rptRow
    End Function

    Public Class ReturnAddress
        Inherits BaseEntity

#Region "LocalVariable"

        Private _CUST_CODE As String = String.Empty
        Private _ADDRESS As String = String.Empty


#End Region

#Region "Public Porperty"
        Public Property CUST_CODE() As String
            Get
                Return _CUST_CODE
            End Get
            Set(ByVal value As String)
                _CUST_CODE = value
            End Set
        End Property

        Public Property ADDRESS() As String
            Get
                Return _ADDRESS
            End Get
            Set(ByVal value As String)
                _ADDRESS = value
            End Set
        End Property


#End Region

    End Class

    <System.Runtime.Serialization.DataContractAttribute()> _
    Public Class ReturnCoordinate

#Region "Constructor"
        Public Sub New()
        End Sub
#End Region
#Region "LocalVariable"
        Private _SALESREP_CODE As String = String.Empty
        Private _LONGITUDE As String = String.Empty
        Private _LATITUDE As String = String.Empty
        Private _CUST_CODE As String = String.Empty
        Private _CUST_NAME As String = String.Empty
        Private _COLOR As String = String.Empty
        Private _INFO As String = String.Empty
        Private _SALESREP_NAME As String = String.Empty
        Private _TEL_NO As String = String.Empty
        'Private _TTL_REVENUE As String = String.Empty
        Private _LAST_GPS_DT As String = String.Empty
        Private _LOCATION As String = String.Empty
        Private _TXN_TIMESTAMP As String = String.Empty

#End Region

#Region "Public Porperty"
        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property CUST_CODE() As String
            Get
                Return _CUST_CODE
            End Get
            Set(ByVal value As String)
                _CUST_CODE = value
            End Set
        End Property


        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property CUST_NAME() As String
            Get
                Return _CUST_NAME
            End Get
            Set(ByVal value As String)
                _CUST_NAME = value
            End Set
        End Property
        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property LOCATION() As String
            Get
                Return _LOCATION
            End Get
            Set(ByVal value As String)
                _LOCATION = value
            End Set
        End Property


        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property LAST_GPS_DT() As String
            Get
                Return _LAST_GPS_DT
            End Get
            Set(ByVal value As String)
                _LAST_GPS_DT = value
            End Set
        End Property

        '<System.Runtime.Serialization.DataMemberAttribute()> _
        'Public Property TTL_REVENUE() As String
        '    Get
        '        Return _TTL_REVENUE
        '    End Get
        '    Set(ByVal value As String)
        '        _TTL_REVENUE = value
        '    End Set
        'End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property TEL_NO() As String
            Get
                Return _TEL_NO
            End Get
            Set(ByVal value As String)
                _TEL_NO = value
            End Set
        End Property
        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property SALESREP_NAME() As String
            Get
                Return _SALESREP_NAME
            End Get
            Set(ByVal value As String)
                _SALESREP_NAME = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property LONGITUDE() As String
            Get
                Return _LONGITUDE
            End Get
            Set(ByVal value As String)
                _LONGITUDE = value
            End Set
        End Property
        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property LATITUDE() As String
            Get
                Return _LATITUDE
            End Get
            Set(ByVal value As String)
                _LATITUDE = value
            End Set
        End Property
        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property SALESREP_CODE() As String
            Get
                Return _SALESREP_CODE
            End Get
            Set(ByVal value As String)
                _SALESREP_CODE = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property INFO() As String
            Get
                Return _INFO
            End Get
            Set(ByVal value As String)
                _INFO = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property COLOR() As String
            Get
                Return _COLOR
            End Get
            Set(ByVal value As String)
                _COLOR = value
            End Set
        End Property

        <System.Runtime.Serialization.DataMemberAttribute()> _
        Public Property TXN_TIMESTAMP() As String
            Get
                Return _TXN_TIMESTAMP
            End Get
            Set(ByVal value As String)
                _TXN_TIMESTAMP = value
            End Set
        End Property

#End Region
    End Class

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class

