﻿Imports System.Data
Imports rpt_Customize

''' <summary>
''' Name: CHEONG BOO LIM
''' Date: 30 Sept 2010
''' Description: 
''' </summary>
''' <remarks></remarks>
Partial Class iFFMR_Customize_SFMSEnquiry_SFMSEnquiryMap
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "SFMSEnquiryMap.aspx"
        End Get
    End Property


    Public Property strCustName() As String
        Get
            Return ViewState("strCustName")
        End Get
        Set(ByVal value As String)
            ViewState("strCustName") = value
        End Set
    End Property
    Public Property strAddress() As String
        Get
            Return ViewState("strAddress")
        End Get
        Set(ByVal value As String)
            ViewState("strAddress") = value
        End Set
    End Property
    Public Property strDistrict() As String
        Get
            Return ViewState("strDistrict")
        End Get
        Set(ByVal value As String)
            ViewState("strDistrict") = value
        End Set
    End Property
    Public Property strCustGrpName() As String
        Get
            Return ViewState("strCustGrpName")
        End Get
        Set(ByVal value As String)
            ViewState("strCustGrpName") = value
        End Set
    End Property
    Public Property strCustClass() As String
        Get
            Return ViewState("strCustClass")
        End Get
        Set(ByVal value As String)
            ViewState("strCustClass") = value
        End Set
    End Property
    Public Property strCustType() As String
        Get
            Return ViewState("strCustType")
        End Get
        Set(ByVal value As String)
            ViewState("strCustType") = value
        End Set
    End Property
    Public Property strCatCode() As String
        Get
            Return ViewState("strCatCode")
        End Get
        Set(ByVal value As String)
            ViewState("strCatCode") = value
        End Set
    End Property
    Public Property strSubCatCode() As String
        Get
            Return ViewState("strSubCatCode")
        End Get
        Set(ByVal value As String)
            ViewState("strSubCatCode") = value
        End Set
    End Property
    Public Property strSFMSStartDate() As String
        Get
            Return ViewState("strSFMSStartDate")
        End Get
        Set(ByVal value As String)
            ViewState("strSFMSStartDate") = value
        End Set
    End Property
    Public Property strSFMSEndDate() As String
        Get
            Return ViewState("strSFMSEndDate")
        End Get
        Set(ByVal value As String)
            ViewState("strSFMSEndDate") = value
        End Set
    End Property
    Public Property strMtdStart() As String
        Get
            Return ViewState("strMtdStart")
        End Get
        Set(ByVal value As String)
            ViewState("strMtdStart") = value
        End Set
    End Property
    Public Property strMtdEnd() As String
        Get
            Return ViewState("strMtdEnd")
        End Get
        Set(ByVal value As String)
            ViewState("strMtdEnd") = value
        End Set
    End Property
    Public Property strYtdStart() As String
        Get
            Return ViewState("strYtdStart")
        End Get
        Set(ByVal value As String)
            ViewState("strYtdStart") = value
        End Set
    End Property
    Public Property strYtdEnd() As String
        Get
            Return ViewState("strYtdEnd")
        End Get
        Set(ByVal value As String)
            ViewState("strYtdEnd") = value
        End Set
    End Property
    Public Property strNoSKUStart() As String
        Get
            Return ViewState("strNoSKUStart")
        End Get
        Set(ByVal value As String)
            ViewState("strNoSKUStart") = value
        End Set
    End Property
    Public Property strNoSKUEnd() As String
        Get
            Return ViewState("strNoSKUEnd")
        End Get
        Set(ByVal value As String)
            ViewState("strNoSKUEnd") = value
        End Set
    End Property
    Public Property strTeamCode() As String
        Get
            Return ViewState("strTeamCode")
        End Get
        Set(ByVal value As String)
            ViewState("strTeamCode") = value
        End Set
    End Property
    Public Property strSalesrepCode() As String
        Get
            Return ViewState("strSalesrepCode")
        End Get
        Set(ByVal value As String)
            ViewState("strSalesrepCode") = value
        End Set
    End Property
    Public Property strPrdCode() As String
        Get
            Return ViewState("strPrdCode")
        End Get
        Set(ByVal value As String)
            ViewState("strPrdCode") = value
        End Set
    End Property
    Public ReadOnly Property Root() As String
        Get
            Dim strCompletePath As String = Me.Server.MapPath(Me.Request.ApplicationPath).Replace("/", "\\")
            Dim strApplicationPath As String = Me.Request.ApplicationPath.Replace("/", "\\")
            Dim strRootPath As String = strCompletePath.Replace(strApplicationPath, String.Empty)

            Return strRootPath
        End Get
    End Property
    Dim strCatName, strSubCatName, strPeriod As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
                  

            If Not Page.IsPostBack Then

                strCustName = Request.QueryString("CustName").ToString
                strAddress = Request.QueryString("Address").ToString
                strDistrict = Request.QueryString("District").ToString
                strCustGrpName = Request.QueryString("CustGrpName").ToString
                strCustClass = Request.QueryString("CustClass").ToString
                strCustType = Request.QueryString("CustType").ToString
                strCatCode = Request.QueryString("CatCode").ToString
                strSubCatCode = Request.QueryString("SubCatCode").ToString
                strSFMSStartDate = Request.QueryString("StartDate").ToString
                strSFMSEndDate = Request.QueryString("EndDate").ToString
                strCatName = Request.QueryString("CatName").ToString
                strSubCatName = Request.QueryString("SubCatName").ToString
                strMtdStart = Request.QueryString("MtdStart").ToString
                strMtdEnd = Request.QueryString("MtdEnd").ToString
                strYtdStart = Request.QueryString("YtdStart").ToString
                strYtdEnd = Request.QueryString("YtdEnd").ToString
                strNoSKUStart = Request.QueryString("NoSKUStart").ToString
                strNoSKUEnd = Request.QueryString("NoSKUEnd").ToString
                strTeamCode = Request.QueryString("TeamCode").ToString
                strSalesrepCode = Request.QueryString("SalesrepCode").ToString
                strPrdCode = Request.QueryString("PrdCode").ToString

                lblPeriod.Text = strSFMSStartDate + " - " + strSFMSEndDate
                lblCat.Text = strCatName
                lblSubCat.Text = strSubCatName

                'AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "NetValue", "NetValue= '" & Session("NetValue") & "';SalesDate='" & DateTime.Now.ToString("yyyy-MM-dd") & "';", True)

                'Set Root path for client side
                Dim strRoot As String = ResolveClientUrl("~")
                strRoot = strRoot.Substring(0, strRoot.Length - 1)
                AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "RootVar", "root= '" & strRoot & "';", True)

                TimerControl1.Enabled = True

            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then CallMapQuery()
        TimerControl1.Enabled = False
    End Sub
    Private Sub CallMapQuery()
        Try
            'Set Guid first


            'Call Map Query
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "QueryMap", String.Format("SearchCustomerSFMS('" & strCustName & "', '" & strAddress & "', '" & strDistrict & "', '" & strCustGrpName & "', '" & strCustClass & "','" & strCustType & "', '" & strCatCode & "','" & strSubCatCode & "','" & strSFMSStartDate & "','" & strSFMSEndDate & _
                                                                                              "','{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}');", _
                                                                                              Session("NetValue"), DateTime.Now.ToString("yyyy-MM-dd"), strMtdStart, strMtdEnd, strYtdStart, strYtdEnd, strNoSKUStart, _
                                                                                              strNoSKUEnd, strTeamCode, strSalesrepCode, strPrdCode), True)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
