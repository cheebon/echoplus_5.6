'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/11/2006
'	Purpose	    :	Class AccessRight 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Data

'Interface Icor_AccessRight
'    Function GetTemplateList(ByVal AccessRightID As Long) As DataTable
'End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
<Serializable()> _
Public Class clsAccessRight
    'Inherits ServicedComponent
    'Implements Icor_AccessRight
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New clsProperties.clsAccessRight

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Create
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function Create() As Double
        Dim objDB As cor_DB.clsDB
        Dim objAccessRightDtl As clsAccessRightDtl
        Dim lngAccessRightID As Double
        Dim i As Integer
        Dim ary As Array
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow = Nothing

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTCREATE"
                '.addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name", clsProperties.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name_1", clsProperties.accessright_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc", clsProperties.accessright_desc, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc_1", clsProperties.accessright_desc_1, cor_DB.clsDB.DataType.DBString)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                lngAccessRightID = .spInsert()
                '.spInsert()
            End With

            If Not IsNothing(clsProperties.dtDtl) Then
                objAccessRightDtl = New clsAccessRightDtl
                With objAccessRightDtl
                    drCurrRow = clsProperties.dtDtl.Select("country_id=" & Trim(clsProperties.country_id) & " AND principal_id=" & Trim(clsProperties.principal_id) & " AND action_id<>''", "module_id ASC")
                    For Each drRow In drCurrRow
                        ary = Split(Trim(drRow("action_id")), ":")
                        If Not IsNothing(ary) Then
                            For i = 0 To UBound(ary)
                                .clsProperties.accessright_id = lngAccessRightID
                                .clsProperties.module_id = Trim(drRow("module_id"))
                                .clsProperties.submodule_id = Trim(drRow("submodule_id"))
                                .clsProperties.action_id = ary(i)
                                .clsProperties.created_date = clsProperties.created_date
                                .clsProperties.creator_user_id = clsProperties.creator_user_id
                                .clsProperties.changed_date = clsProperties.changed_date
                                .clsProperties.changed_user_id = clsProperties.changed_user_id
                                .clsProperties.delete_flag = clsProperties.delete_flag
                                .Create()
                            Next
                        End If
                    Next
                End With
            End If

            Return lngAccessRightID
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRight.Create" & ex.ToString))
        Finally
            objAccessRightDtl = Nothing
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Update()
        Dim objDB As cor_DB.clsDB
        Dim objAccessRightDtl As clsAccessRightDtl
        Dim i As Integer
        Dim ary As Array
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow = Nothing

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTUPDATE"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name", clsProperties.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name_1", clsProperties.accessright_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc", clsProperties.accessright_desc, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc_1", clsProperties.accessright_desc_1, cor_DB.clsDB.DataType.DBString)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

            If Not IsNothing(clsProperties.dtDtl) Then
                objAccessRightDtl = New clsAccessRightDtl
                With objAccessRightDtl
                    .clsProperties.accessright_id = clsProperties.accessright_id
                    .Delete()

                    drCurrRow = clsProperties.dtDtl.Select("country_id=" & Trim(clsProperties.country_id) & " AND principal_id=" & Trim(clsProperties.principal_id) & " AND action_id<>''", "module_id ASC")
                    For Each drRow In drCurrRow
                        ary = Split(Trim(drRow("action_id")), ":")
                        If Not IsNothing(ary) Then
                            For i = 0 To UBound(ary)
                                .clsProperties.accessright_id = clsProperties.accessright_id
                                .clsProperties.module_id = Trim(drRow("module_id"))
                                .clsProperties.submodule_id = Trim(drRow("submodule_id"))
                                .clsProperties.action_id = ary(i)
                                .clsProperties.created_date = clsProperties.created_date
                                .clsProperties.creator_user_id = clsProperties.creator_user_id
                                .clsProperties.changed_date = clsProperties.changed_date
                                .clsProperties.changed_user_id = clsProperties.changed_user_id
                                .clsProperties.delete_flag = clsProperties.delete_flag
                                .Create()
                            Next
                        End If
                    Next
                End With
            End If

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRight.Update" & ex.ToString))
        Finally
            objAccessRightDtl = Nothing
            objDB = Nothing
        End Try
    End Sub

    Public Function InsertAccessRight() As Double
        Dim objDB As cor_DB.clsDB
        Dim lngAccessRightID As Double
        Dim drRow As DataRow = Nothing

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTCREATE"
                '.addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name", clsProperties.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name_1", clsProperties.accessright_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc", clsProperties.accessright_desc, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc_1", clsProperties.accessright_desc_1, cor_DB.clsDB.DataType.DBString)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                lngAccessRightID = .spInsert()
                '.spInsert()
            End With

            Return lngAccessRightID
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRight.Create" & ex.ToString))
        Finally
            'objAccessRightDtl = Nothing
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateAccessRight()
        Dim objDB As cor_DB.clsDB
        Dim objAccessRightDtl As clsAccessRightDtl
        Dim drRow As DataRow = Nothing

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTUPDATE"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name", clsProperties.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_name_1", clsProperties.accessright_name_1, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc", clsProperties.accessright_desc, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_desc_1", clsProperties.accessright_desc_1, cor_DB.clsDB.DataType.DBString)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRight.Update" & ex.ToString))
        Finally
            objAccessRightDtl = Nothing
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Delete()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTDELETE"
                .addItem("AccessRight_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRight.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.AccessRightID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
