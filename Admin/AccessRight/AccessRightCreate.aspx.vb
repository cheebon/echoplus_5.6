'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/11/2006
'	Purpose	    :	Access Right Create
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_AccessRight_AccessRightCreate
    Inherits System.Web.UI.Page

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        'Dim dt, dtDtl As DataTable
        Dim strUserCode As String

        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Creating Access Right"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant
                strUserCode = Trim(Session("UserCode"))
               

                'request
                'txtAccessRightID.Text = Trim(Request.QueryString("accessright_id"))

                'Prepare Recordset,Editobject as needed
                'Call Country
                With Wuc_ddlCountry
                    .UserCode = strUserCode
                    .RequiredValidation = True
                    .AutoPostBack = True
                    .DataBind()
                    .Visible = True
                End With

                
                'Call Principal
                With Wuc_ddlPrincipal
                    .CountryCode = Tree.SplitComboValues(Wuc_ddlCountry.SelectedValue)(1)
                    .UserCode = strUserCode
                    .RequiredValidation = True
                    .AutoPostBack = True
                    .DataBind()
                    .Visible = True
                End With




            End If

        Catch ex As Exception
            ExceptionMsg("AccessRightCreate.Page_Load : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlCountry_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlCountry.SelectedIndexChanged
        Dim ary As Array

        Try
            ary = Split(Wuc_ddlCountry.SelectedValue, "@")

            'Call Principal
            With Wuc_ddlPrincipal
                If Wuc_ddlCountry.SelectedValue <> "0" Then
                    .CountryCode = ary(1)
                Else
                    .CountryCode = ary(0) 'Wuc_ddlCountry.SelectedValue
                End If
                .UserCode = Trim(Session("UserCode"))
                .RequiredValidation = True
                .AutoPostBack = True
                .DataBind()
                .Visible = True
            End With

        Catch ex As Exception
            ExceptionMsg("AccessRightCreate.Wuc_ddlCountry_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Protected Sub Wuc_ddlPrincipal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlPrincipal.SelectedIndexChanged
        'Try
        '    GetModuleList()
        '    If dgSubModuleList.Visible = True Then
        '        'GetSubModuleList(Trim(dgModuleList.SelectedValue))
        '        'ViewState("AccessRightDtl") = Nothing
        '        dgSubModuleList.Visible = False
        '        chkAction.Items.Clear()
        '        Me.btnSet.Visible = False
        '        btnSelAll.Visible = False
        '        btnClearAll.Visible = False
        '    End If

        'Catch ex As Exception
        '    ExceptionMsg("AccessRightCreate.Wuc_ddlPrincipal_SelectedIndexChanged : " & ex.ToString)
        'End Try
    End Sub

    '#Region "dgModuleList"
    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub dgModuleList_RowCommand
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Protected Sub dgModuleList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgModuleList.RowCommand
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim row As GridViewRow = dgModuleList.Rows(index)

    '        Try
    '            Select Case e.CommandName
    '                Case "Select"
    '                    Dim chkBox As CheckBox = row.FindControl("chkModule")

    '                    dgSubModuleList.Visible = True
    '                    chkBox.Checked = True
    '                    btnSet.Visible = False
    '                    btnSelAll.Visible = False
    '                    btnClearAll.Visible = False
    '                    chkAction.Items.Clear()
    '                    GetSubModuleList(Trim(dgModuleList.DataKeys(row.RowIndex).Value))
    '            End Select

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.dgModuleList_RowCommand : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub dgModuleList_RowDataBound
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Protected Sub dgModuleList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgModuleList.RowDataBound
    '        Dim dt As DataTable
    '        Dim drv As DataRowView
    '        Dim drCurrRow As DataRow()
    '        Dim chk As CheckBox

    '        Try
    '            dt = ViewState("AccessRightDtl")
    '            If e.Row.RowType = DataControlRowType.DataRow Then
    '                drv = CType(e.Row.DataItem, DataRowView)

    '                If Not IsDBNull(drv("module_id")) And Not IsNothing(dt) Then
    '                    drCurrRow = dt.Select("module_id=" & Trim(drv("module_id")) & " AND action_id<>''", "")
    '                    chk = CType(e.Row.Cells(1).FindControl("chkModule"), CheckBox)

    '                    If drCurrRow.Length > 0 Then
    '                        chk.Checked = True
    '                    Else
    '                        chk.Checked = False
    '                    End If
    '                End If
    '            End If

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.dgModuleList_RowDataBound : " & ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

    '#Region "dgSubModuleList"
    '    Protected Sub dgSubModuleList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgSubModuleList.RowCommand
    '        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
    '        Dim row As GridViewRow = dgSubModuleList.Rows(index)
    '        Dim intPos As Integer

    '        Try
    '            Select Case e.CommandName
    '                Case "Select"
    '                    'Dim ModuleIDCell As TableCell = row.Cells(0)
    '                    'Dim SubModuleIDCell As TableCell = row.Cells(1)
    '                    'Dim lngModuleID As Long = Long.Parse(moduleIDCell.Text)
    '                    Dim lngSubModuleID As Double = Long.Parse(Trim(dgSubModuleList.DataKeys(row.RowIndex).Value))
    '                    intPos = row.RowIndex
    '                    GetActionList("0", lngSubModuleID, intPos)
    '            End Select

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.dgSubModuleList_RowCommand : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub dgSubModuleList_RowDataBound
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Protected Sub dgSubModuleList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgSubModuleList.RowDataBound
    '        Dim drv As DataRowView
    '        Dim lnkSelect As LinkButton

    '        Try
    '            If e.Row.RowType = DataControlRowType.DataRow Then
    '                drv = CType(e.Row.DataItem, DataRowView)

    '                If IsDBNull(drv("submodule_id")) Then
    '                    lnkSelect = CType(e.Row.Cells(2).Controls(0), LinkButton)
    '                    lnkSelect.Visible = False
    '                End If
    '            End If

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.dgSubModuleList_RowDataBound : " & ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

    '#Region "Get List"
    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub GetModuleList
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Private Sub GetModuleList()
    '        Dim dt As DataTable
    '        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery

    '        Try
    '            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
    '            With objAccessRightQuery
    '                dt = objAccessRightQuery.GetModuleList
    '            End With

    '            If dt.Rows.Count < 1 Then
    '                dt.Rows.Add(dt.NewRow)
    '            End If

    '            With dgModuleList
    '                .DataSource = dt
    '                .DataBind()
    '            End With

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.GetModuleList : " & ex.ToString)
    '        Finally
    '            objAccessRightQuery = Nothing
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub GetSubModuleList
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Private Sub GetSubModuleList(ByVal lngModuleID As Double, Optional ByVal intPassFlag As Integer = 0)
    '        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
    '        Dim aryCountry, aryPrincipal As Array
    '        Dim dt, dtAR, dtNew As DataTable
    '        Dim drCurrRow() As DataRow = Nothing
    '        Dim drCurrRowNew() As DataRow = Nothing
    '        Dim drRow As DataRow
    '        Dim i As Integer

    '        Try
    '            Page.Validate()

    '            If Page.IsValid Then
    '                dt = ViewState("AccessRightDtl")
    '                aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
    '                aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")
    '                If Not IsNothing(dt) Then
    '                    drCurrRow = dt.Select("country_id=" & aryCountry(0) & "AND principal_id=" & aryPrincipal(0) & "AND module_id=" & lngModuleID, "")
    '                    If drCurrRow.Length < 1 Then
    '                        drCurrRow = Nothing
    '                    End If
    '                End If

    '                If IsNothing(drCurrRow) Or intPassFlag = "1" Then
    '                    objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
    '                    With objAccessRightQuery
    '                        .clsProperties.module_id = lngModuleID
    '                        .clsProperties.country_id = aryCountry(0)
    '                        .clsProperties.principal_id = aryPrincipal(0)
    '                        dt = objAccessRightQuery.GetSubModuleList
    '                    End With

    '                    If IsNothing(ViewState("AccessRightDtl")) Then
    '                        ViewState("AccessRightDtl") = dt
    '                    Else
    '                        dtAR = ViewState("AccessRightDtl")
    '                        For i = 0 To dt.Rows.Count - 1
    '                            drRow = dtAR.NewRow
    '                            drRow("country_id") = dt.Rows(i)("country_id")
    '                            drRow("principal_id") = dt.Rows(i)("principal_id")
    '                            drRow("module_id") = lngModuleID
    '                            drRow("submodule_id") = dt.Rows(i)("submodule_id")
    '                            drRow("submodule_name") = dt.Rows(i)("submodule_name")
    '                            drRow("action_id") = dt.Rows(i)("action_id")
    '                            drRow("action_string") = dt.Rows(i)("action_string")

    '                            ' Add the new row to the table.
    '                            dtAR.Rows.Add(drRow)
    '                        Next
    '                        ViewState("AccessRightDtl") = dtAR
    '                    End If
    '                End If

    '                dt = ViewState("AccessRightDtl")
    '                dtNew = dt.Copy
    '                drCurrRowNew = dtNew.Select("country_id<>" & aryCountry(0) & "OR principal_id<>" & aryPrincipal(0) & "OR module_id<>" & lngModuleID, "")
    '                'drCurrRowNew = dtNew.Select("module_id<>" & lngModuleID, "")

    '                For Each drRow In drCurrRowNew
    '                    dtNew.Rows.Remove(drRow)
    '                Next

    '                If dtNew.Rows.Count < 1 Then
    '                    dtNew.Rows.Add(dtNew.NewRow)
    '                End If

    '                With dgSubModuleList
    '                    .DataSource = dtNew 'ViewState("AccessRightDtl")
    '                    .DataBind()
    '                End With
    '            Else
    '                dgSubModuleList.Visible = False
    '            End If

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.GetSubModuleList : " & ex.ToString)
    '        Finally
    '            objAccessRightQuery = Nothing
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub GetActionList
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Private Sub GetActionList(ByVal lngModuleID As Double, ByVal lngSubModuleID As Double, ByVal intPos As Integer)
    '        Dim dt As DataTable
    '        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery

    '        Try
    '            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
    '            With objAccessRightQuery
    '                .clsProperties.module_id = lngModuleID
    '                .clsProperties.submodule_id = lngSubModuleID
    '                dt = .GetActionListBySubModuleID
    '            End With

    '            With chkAction
    '                .DataSource = dt
    '                .DataTextField = "action_name"
    '                .DataValueField = "action_id"
    '                .DataBind()
    '            End With

    '            If dt.Rows.Count > 0 Then
    '                SetAccessRightCheck(intPos)
    '                Me.btnSet.Visible = True
    '                btnSelAll.Visible = True
    '                btnClearAll.Visible = True
    '            Else
    '                Me.btnSet.Visible = False
    '                btnSelAll.Visible = False
    '                btnClearAll.Visible = False
    '            End If

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.GetActionList : " & ex.ToString)
    '        Finally
    '            objAccessRightQuery = Nothing
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub SetAccessRightCheck
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Private Sub SetAccessRightCheck(Optional ByVal intPos As Integer = 0)
    '        Dim i, y As Integer
    '        Dim strActionID As String = ""
    '        Dim ary As Array = Nothing
    '        'Dim drv As DataRowView

    '        Try
    '            'If dgSubModuleList.SelectedIndex <> -1 Then
    '            'drv = CType(dgSubModuleList.Rows(dgSubModuleList.SelectedIndex).DataItem, DataRowView)
    '            strActionID = dgSubModuleList.Rows(intPos).Cells(4).Text
    '            'End If
    '            If strActionID <> "" Then
    '                ary = Split(strActionID, ":")
    '            End If

    '            For i = 0 To Me.chkAction.Items.Count - 1
    '                If Not IsNothing(ary) Then
    '                    For y = 0 To UBound(ary)
    '                        If Trim(ary(y).ToString) = Trim(chkAction.Items(i).Text.ToString) Then
    '                            Me.chkAction.Items(i).Selected = True
    '                        End If
    '                    Next
    '                End If
    '            Next
    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.SetAccessRightCheck : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------
    '    ' Procedure 	: 	Sub SetAccessRight
    '    ' Purpose	    :	
    '    ' Parameters    :	[in] sender: 
    '    '   		        [in]      e: -
    '    '		            [out]      : 
    '    '----------------------------------------------------------------------
    '    Private Sub SetAccessRight()
    '        Dim i As Integer
    '        Dim dt As DataTable
    '        Dim drCurrRow() As DataRow
    '        Dim drRow As DataRow = Nothing
    '        Dim strActionID As String = ""
    '        Dim strActionString As String = ""
    '        Dim strSubModuleName As String = ""
    '        Dim lngModuleID As Double
    '        Dim lngSubModuleID As Double

    '        Try
    '            lngModuleID = dgModuleList.SelectedValue
    '            lngSubModuleID = dgSubModuleList.SelectedValue
    '            strSubModuleName = dgSubModuleList.SelectedRow.Cells(3).Text

    '            For i = 0 To Me.chkAction.Items.Count - 1
    '                If Me.chkAction.Items(i).Selected Then
    '                    strActionID = strActionID & Me.chkAction.Items(i).Value & ":"
    '                    strActionString = strActionString & Me.chkAction.Items(i).Text & ":"
    '                End If
    '            Next
    '            If strActionID <> "" Then
    '                strActionID = Mid(strActionID, 1, Len(strActionID) - 1)
    '                strActionString = Mid(strActionString, 1, Len(strActionString) - 1)
    '            End If

    '            dt = ViewState("AccessRightDtl")
    '            If Not IsNothing(dt) Then
    '                drCurrRow = dt.Select("submodule_id='" & lngSubModuleID & "'", "")
    '                For Each drRow In drCurrRow
    '                    drRow.BeginEdit()
    '                    'drRow("module_id") = lngModuleID
    '                    'drRow("submodule_id") = lngSubModuleID
    '                    'drRow("submodule_name") = strSubModuleName
    '                    drRow("action_id") = strActionID
    '                    drRow("action_string") = strActionString
    '                    drRow.EndEdit()
    '                Next
    '            End If
    '            ViewState("AccessRightDtl") = dt

    '            GetSubModuleList(lngModuleID)

    '        Catch ex As Exception
    '            ExceptionMsg("AccessRightCreate.SetAccessRight : " & ex.ToString)
    '        End Try
    '    End Sub
    '#End Region

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSelAll_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    'Private Sub btnSelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelAll.Click
    '    Try
    '        For i As Integer = 0 To Me.chkAction.Items.Count - 1
    '            Me.chkAction.Items(i).Selected = True
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg("AccessRightCreate.btnSelAll_Click : " & ex.ToString)
    '    End Try
    'End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnClearAll_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    'Private Sub btnClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearAll.Click
    '    Try
    '        For i As Integer = 0 To Me.chkAction.Items.Count - 1
    '            Me.chkAction.Items(i).Selected = False
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg("AccessRightCreate.btnClearAll_Click : " & ex.ToString)
    '    End Try
    'End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSet_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    'Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
    '    Try
    '        SetAccessRight()
    '    Catch ex As Exception
    '        ExceptionMsg("AccessRightCreate.btnSet_Click : " & ex.ToString)
    '    End Try
    'End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSave_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objAccessRight As adm_AccessRight.clsAccessRight
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim aryCountry, aryPrincipal As Array
        Dim dt, dtDtl As DataTable
        Dim intCnt As Integer
        Dim drCurrRow As DataRow()
        Dim lngAccessRightID As Double

        Try
            'Check AccessRight name exist or not
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                .clsProperties.accessright_id = "0" 'txtAccessRightID.Text
                .clsProperties.accessright_name = Trim(txtName.Text)
                dt = .GetAccessRightDuplicate
                intCnt = IIf(dt.Rows(0)("cnt") = Nothing, 0, dt.Rows(0)("cnt"))
            End With
            objAccessRightQuery = Nothing

            If intCnt > 0 Then
                lblDupError.Visible = True
                Exit Sub
            End If

            aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            'Create record
            objAccessRight = New adm_AccessRight.clsAccessRight
            With objAccessRight
                '.clsProperties.accessright_id = txtAccessRightID.Text
                .clsProperties.accessright_name = Trim(txtName.Text)
                .clsProperties.accessright_name_1 = Trim(txtName1.Text)
                .clsProperties.accessright_desc = Trim(txtDesc.Text)
                .clsProperties.accessright_desc_1 = Trim(txtDesc1.Text)
                .clsProperties.country_id = aryCountry(0)
                .clsProperties.principal_id = aryPrincipal(0)
                .clsProperties.created_date = Now '.ToString
                .clsProperties.creator_user_id = Session("UserID")
                '.clsProperties.changed_date = Now
                '.clsProperties.changed_user_id = Session("UserID")
                .clsProperties.delete_flag = 0
                '.clsProperties.dtDtl = dtDtl
                lngAccessRightID = .InsertAccessRight()
            End With

            Response.Redirect("AccessRightEdit.aspx?accessright_id=" & Trim(lngAccessRightID) & "&ori=create", False)

        Catch ex As Exception
            ExceptionMsg("AccessRightCreate.btnSave_Click : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
            objAccessRight = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnReset_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    'Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
    '    Try
    '        txtName.Text = ""
    '        txtName1.Text = ""
    '        Wuc_ddlCountry.SelectedValue = 0
    '        Wuc_ddlPrincipal.SelectedValue = 0
    '        txtDesc.Text = ""
    '        txtDesc1.Text = ""

    '        GetModuleList()
    '        ViewState("AccessRightDtl") = Nothing

    '        If dgSubModuleList.Visible = True Then
    '            'GetSubModuleList(Trim(dgModuleList.SelectedValue))
    '            dgSubModuleList.Visible = False
    '            chkAction.Items.Clear()
    '            Me.btnSet.Visible = False
    '            btnSelAll.Visible = False
    '            btnClearAll.Visible = False
    '        End If

    '    Catch ex As Exception
    '        ExceptionMsg("AccessRightCreate.btnReset_Click : " & ex.ToString)
    '    End Try
    'End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
