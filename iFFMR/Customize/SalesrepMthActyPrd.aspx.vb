Imports System.Data
Partial Class iFFMR_Customize_SalesrepMthActyPrd
    Inherits System.Web.UI.Page

    Public Enum Mode As Integer
        Normal = 0
        Export = 1
    End Enum

#Region "Local Variable"
    Dim _reportMode As Mode
    Public Property ReportMode() As Mode
        Get
            Return _reportMode
        End Get
        Set(ByVal value As Mode)
            _reportMode = value
        End Set
    End Property

    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_SalesrepMthActy"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "SalesrepMthActy.aspx"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.FIELDFORCEMTHACTYPrd)
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd
                .DataBind()
                .Visible = True
            End With

            Dim gotpreviouspage As String
            gotpreviouspage = Request.QueryString("PAGECALL")

            wuc_MultiAuthen.SubModuleID = SubModuleType.FIELDFORCEMTHACTYPrd

            If Not String.IsNullOrEmpty(gotpreviouspage) Then
                RefreshDatabinding()
                wuc_MultiAuthen.PanelCollapese = True
            Else
                CriteriaCollector = Nothing ' New clsSharedValue    
                BindDefault()
            End If

        End If

        lblErr.Text = ""
        'Try

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try

    End Sub

#Region "EVENT HANDLER"
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub


    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MultiAuthen.ResetBtn_Click
        Try

            BindDefault()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MultiAuthen.RefreshBtn_Click
        Try

            ViewState("dtCurrentView") = Nothing
            RefreshDatabinding()

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRefresh_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

#End Region

    Public Property blnHasPreviousPage() As Boolean
        Get
            Return CBool(ViewState("HasPreviousPage"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("HasPreviousPage") = value
        End Set
    End Property

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            ' If dtCurrenttable Is Nothing Then
            dtCurrenttable = GetRecList()
            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            dgList.PageIndex = 0
            'End If
            PreRenderMode(dtCurrenttable)

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SFMTHACTY_Prd.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_SFMTHACTY_Prd.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_SFMTHACTY_Prd.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFMTHACTY_Prd.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFMTHACTY_Prd.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        'dgColumn.SortExpression = ColumnName
                        Dim intmonth As Integer = CF_SFMTHACTY_Prd.GetMonthValue(ColumnName)

                        Dim strUrlFields(0) As String
                        strUrlFields(0) = "DESC_CODE"

                        Dim gotpreviouspage As String
                        gotpreviouspage = Request.QueryString("PAGECALL")
                        Dim strUrlFormatString As String

                        If Not String.IsNullOrEmpty(gotpreviouspage) Then
                            strUrlFormatString = "DailyCallAnalysis.aspx?MONTH=" & intmonth & "&SALESREP_CODE=" & CriteriaCollector.SalesrepCode _
                                                                     & "&BACK=SalesrepMthActyPrd.aspx"
                        Else
                            strUrlFormatString = "DailyCallAnalysis.aspx?MONTH=" & intmonth & "&SALESREP_CODE=" & wuc_MultiAuthen.SalesrepCode _
                                           & "&BACK=SalesrepMthActyPrd.aspx"
                        End If


                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SFMTHACTY_Prd.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFMTHACTY_Prd.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFMTHACTY_Prd.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow

                If e.Row.RowIndex = 13 AndAlso licItemFigureCollector IsNot Nothing Then
                    Dim iIndex, iMonth As Integer
                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        iMonth = CF_SFMTHACTY_Prd.GetMonthValue(li.Text)
                        If iMonth > 0 Then
                            Dim strSFMSData As String = IIf(IsDBNull(DataBinder.Eval(e.Row.DataItem, li.Text)), String.Empty, DataBinder.Eval(e.Row.DataItem, li.Text))
                            If ViewState("EXPORT") = True Then
                                e.Row.Cells(iIndex).Text = strSFMSData
                            Else

                                Dim gotpreviouspage As String
                                gotpreviouspage = Request.QueryString("PAGECALL")

                                If Not String.IsNullOrEmpty(gotpreviouspage) Then
                                    e.Row.Cells(iIndex).Text = "<a href=""SFMSDetActy.aspx?MONTH=" & iMonth & "&SALESREP_CODE=" & CriteriaCollector.SalesrepCode & "&TEAM_CODE=" & CriteriaCollector.TeamCode & "&YEAR=" & Session("YEAR") & _
                                                                     """ target=""_self"">" & _
                                                                     strSFMSData & "</a>"
                                Else
                                    e.Row.Cells(iIndex).Text = "<a href=""SFMSDetActy.aspx?MONTH=" & iMonth & "&SALESREP_CODE=" & wuc_MultiAuthen.SalesrepCode & "&TEAM_CODE=" & wuc_MultiAuthen.TeamCode & "&YEAR=" & Session("YEAR") & _
                                                                                                       """ target=""_self"">" & _
                                                                                                       strSFMSData & "</a>"
                                End If

                            End If
                        End If
                    Next
                End If
        End Select
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If aryDataItem.IndexOf("DESC_CODE") >= 0 Then
                        Dim strCapCode As String = Trim(e.Row.Cells(aryDataItem.IndexOf("DESC_CODE")).Text)
                        If strCapCode = "KD" OrElse strCapCode = "AFD" OrElse strCapCode = "PAFD" OrElse strCapCode = "TC" OrElse strCapCode = "DPC" OrElse strCapCode = "PFC" Then
                            e.Row.CssClass = "GridFooter"
                        End If
                        e.Row.Cells(aryDataItem.IndexOf("DESC_CODE")).Text = CF_SFMTHACTY_Prd.GetDisplayColumnName(strCapCode)
                    End If
                Case DataControlRowType.Footer
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try

    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .PrincipalID = Session("PRINCIPAL_ID")
                .PrincipalCode = Session("PRINCIPAL_CODE")
                If Page.IsPostBack And ReportMode = Mode.Normal Then
                    .TeamCode = wuc_MultiAuthen.TeamCode
                    .SalesrepCode = wuc_MultiAuthen.SalesrepCode
                    .Year = wuc_MultiAuthen.SelectedYear
                    .ProductCode = wuc_MultiAuthen.SelectedProduct
                    Session("Year") = wuc_MultiAuthen.SelectedYear
                End If

            End With

            'CriteriaCollector = CriteriaCollector

            Dim clsSFActy As New rpt_Customize.clsSFActy
            With clsSFActy.properties
                .UserID = Session.Item("UserID")
                .PrinID = Session("PRINCIPAL_ID")
                .PrinCode = Session("PRINCIPAL_CODE")
                .Year = CriteriaCollector.Year
                .TeamCode = CriteriaCollector.TeamCode
                .SalesrepCode = CriteriaCollector.SalesrepCode
                .ProductCode = CriteriaCollector.ProductCode
            End With

            DT = clsSFActy.GetSFActyListPrd

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()

            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub
    'Private Function GetRecList() As Data.DataTable
    '    Dim DT As DataTable = Nothing
    '    Try
    '        If PreviousPage Is Nothing Then
    '            With wuc_MultiAuthen
    '                Session("TEAM_LIST") = .TeamCode
    '                Session("SALESREP_LIST") = .SalesrepCode
    '            End With
    '        End If


    '        DT = GetRecList(Session("TEAM_LIST"), Session("SALESREP_LIST"))


    '        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
    '    Finally
    '    End Try
    '    Return DT
    'End Function

#End Region


#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            ReportMode = Mode.Export
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            ReportMode = Mode.Normal
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region



End Class
Public Class CF_SFMTHACTY_Prd
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'CL(CALENDER_DAY)
        'WD(WEEKEND_DAY)
        'PH(PUBLIC_HOLIDAY)
        'AL(ANNUAL_LEAVE)
        'ML(MEDICAL_LEAVE)
        'KD(WORK_DAY)
        'PFC(PERCENT_FIELD_COACHING)
        'TR(TRAINING)
        'MT(MEETING)
        'CE(COMPANY_EVENT)
        'JC(JOINT_CALL)
        'AFD(ACTUAL_FIELD_DAYS)
        'PAFD(PERCENT_ACTUAL_FIELD_DAYS)
        'BOD(BEFORE_OFFICE_DAYS)
        'OD(OFFICE_DAY)
        'FC(FIELD_COACHING)
        'CAC(CLASS_A_CALL)
        'CBC(CLASS_B_CALL)
        'CCC(CLASS_C_CALL)
        'COC(CLASS_OTH_CALL)
        'TC(TOTAL_CALL)
        'D(DETAILING)
        'DPC(DETAILING_PER_CALL)


        Select Case ColumnName.ToUpper
            Case "CL"
                strFieldName = "CALENDER DAY"
            Case "WD"
                strFieldName = "WEEKEND DAY"
            Case "PH"
                strFieldName = "PUBLIC HOLIDAY"
            Case "AL"
                strFieldName = "ANNUAL LEAVE"
            Case "ML"
                strFieldName = "MEDICAL LEAVE"
            Case "KD"
                strFieldName = "WORK DAY"
            Case "PFC"
                strFieldName = "% FIELD COACHING DAY"
            Case "TR"
                strFieldName = "TRAINING"
            Case "MT"
                strFieldName = "MEETING"
            Case "CE"
                strFieldName = "COMPANY EVENT"
            Case "JC"
                strFieldName = "JOINT CALL"
            Case "OCS"
                strFieldName = "OVERSEAS CONF/SYMPOSIUM"
            Case "AFD"
                strFieldName = "ACTUAL FIELD DAYS"
            Case "PAFD"
                strFieldName = "%  ACTUAL FIELD DAYS"
            Case "BOD"
                strFieldName = "BEFORE OFFICE DAYS"
            Case "OD"
                strFieldName = "OFFICE DAY"
            Case "FC"
                strFieldName = "FIELD COACHING"
            Case "CAC"
                strFieldName = "CLASS A CALL"
            Case "CBC"
                strFieldName = "CLASS B CALL"
            Case "CCC"
                strFieldName = "CLASS C CALL"
            Case "COC"
                strFieldName = "CLASS OTH CALL"
            Case "TC"
                strFieldName = "TOTAL CALL"
            Case "D"
                strFieldName = "DETAILING"
            Case "DPC"
                strFieldName = "DETAILING PER CALL"
            Case "AVGTOT"
                strFieldName = "Total"
            Case "DESC_CODE"
                strFieldName = "Month"
            Case "EL"
                strFieldName = "EMERGENCY LEAVE"
            Case "&NBSP;"
                strFieldName = " "
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "DESC_NAME" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "JAN" OrElse strColumnName = "FEB" OrElse strColumnName = "MAR" _
                    OrElse strColumnName = "APR" OrElse strColumnName = "MAY" OrElse strColumnName = "JUN" _
                    OrElse strColumnName = "JUL" OrElse strColumnName = "AUG" OrElse strColumnName = "SEP" _
                    OrElse strColumnName = "OCT" OrElse strColumnName = "NOV" OrElse strColumnName = "DEC" Then

                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DAILYCALLANALYSIS, SubModuleAction.View) And _
                Report.GetAccessRight(ModuleID.FFMR, SubModuleType.FIELDFORCESFMSDETACTY, SubModuleAction.View) Then
                    FCT = FieldColumntype.HyperlinkColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        Try
            Dim strColumnName As String = ColumnName.ToUpper
            Select Case strColumnName
                Case "JAN"
                    Return 1
                Case "FEB"
                    Return 2
                Case "MAR"
                    Return 3
                Case "APR"
                    Return 4
                Case "MAY"
                    Return 5
                Case "JUN"
                    Return 6
                Case "JUL"
                    Return 7
                Case "AUG"
                    Return 8
                Case "SEP"
                    Return 9
                Case "OCT"
                    Return 10
                Case "NOV"
                    Return 11
                Case "DEC"
                    Return 12
                Case Else
                    Return 0
            End Select
        Catch ex As Exception
        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "QTY"
                    strFormatString = "{0:#,0}"
                Case "VISIT_FREQ"
                    strFormatString = "{0:0.#}"
                Case "AVGTOT"
                    strFormatString = "{0:#,0.00}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "DESC_CODE" Then
                    .HorizontalAlign = HorizontalAlign.Left
                ElseIf strColumnName = "VISIT_FREQ" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Right
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class