﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MSSEnquiryMap.aspx.vb" Inherits="iFFMR_Customize_MSSEnquiry_MSSEnquiryMap" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../../include/layout.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/ColorPicker.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MapIconMaker/mapiconmaker2.js" type="text/javascript"></script>
    <script src="../../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>   
      <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo&sensor=true" type="text/javascript"> </script>
    <script type='text/javascript'>
        /// Parameters to play with
        /// Currently hard code color first. 
        var ClassA = "#bc0000";
        var ClassB = "#345bec";
        var ClassC = "#c88c18";
        var ClassD = "#3bba04";
        var Unclassified = "#999a9d";
        var marker;
        var markers = [];
        //Global variables
        var map;
        var Guid;
        var root; //store app root path
        var CustName, Address, District, CustGrpName, CustClass, CustType, TitleCode,
            QuesCode, SubQuesCode, MssStartDate, MssEndDate, MtdStart, MtdEnd, YtdStart, YtdEnd,
            NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode, NetValue, SalesDate, MssAnswer, SubQuesType;
        var Default_Latitude, Default_Longitude, Default_Zoomlevel;
        
        
        var ValidCoordinates = []; //Store valid coordinate
        
        /***********************************************************
        / Initialiazer
        /
        ************************************************************/
        function Initialize() {

          //  if (GBrowserIsCompatible()) {
              //  map = new GMap2(document.getElementById('map_canvas'));
            var mapOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE

                },
                mapTypeControl: true
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
                QueryLocation();

              //  AddControl();


           // }
        }
        google.maps.event.addDomListener(window, 'load', Initialize);
        // Add a marker to the map and push to the array.
        function addMarker(Latitude, Longitude) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Latitude, Longitude),
                map: map,
                draggable: true
            });
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the overlays from the map, but keeps them in the array.
        function clearOverlays() {
            setAllMap(null);
        }

        // Shows any overlays currently in the array.
        function showOverlays() {
            setAllMap(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteOverlays() {
            clearOverlays();
        }
        function AddControl() {
            var mapTypeControl = new GMapTypeControl();
            var LargeMapControl3D = new GLargeMapControl3D();
            var bottomLeft = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 10));
            var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 10));

            map.addControl(mapTypeControl, bottomRight);
            map.addControl(LargeMapControl3D, bottomLeft);
            map.enableScrollWheelZoom();
        }
        
        function QueryLocation() {
            //map.setCenter(new google.maps.LatLng(25.324167, 109.160156), 3);
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {
            if (result) {
                if (result.Total > 0) {


                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;

                    
                    SetMapDefaultPoint();
                    
                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {

                var point = new google.maps.LatLng(Default_Latitude, Default_Longitude);
                map.setCenter(point, parseInt(Default_Zoomlevel));

            }
        }
        /***********************************************************
        / Method to search customer MSS.
        /
        ************************************************************/
        function SearchCustomerTitle() {

            ws_MSSEnq.ReturnCustomerMSS('', CustName, Address, District, CustGrpName, CustClass, CustType, TitleCode,
                                        QuesCode, SubQuesCode, MssStartDate, MssEndDate, MssAnswer, SubQuesType, NetValue, SalesDate,
                                        MtdStart, MtdEnd, YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode,
                                            onSuccessLocateCustomerMSS, onFailLocateCustomerMSS);

            ShowMessage("Loading map data.");
        }

        function onSuccessLocateCustomerMSS(result) {
            if (result) {
                if (result.Total > 0) {
                    HideMessage();
                    PlotMap(result);
                    SetCenterMap();

                }
                else //Shit happens
                {

                    ShowMessage("No Record Returned.");

                    QueryLocation();
                }
            }
        }

        function onFailLocateCustomerMSS(ex) {
            //If webservice call fail
            ShowMessage("Error occurred when retrieving data." + ex.get_message());
        }

        function PlotMap(result) {
            var count = 0;
            deleteOverlays();
            WriteRecordCount(count);

            for (var i = 0; i < result.Total; ++i) {
                //Check coordinate contains value
                if ((result.Rows[i].LATITUDE != undefined && result.Rows[i].LATITUDE != "") && (result.Rows[i].LONGITUDE != undefined && result.Rows[i].LONGITUDE != "")) {
                    //check is numeric
                    if (!isNaN(result.Rows[i].LATITUDE) && !isNaN(result.Rows[i].LONGITUDE)) {
                        var icon = CreateIcon(result.Rows[i].CUST_CLASS);

                         marker = CreateMarker(result.Rows[i].CUST_CODE, result.Rows[i].CUST_NAME,
                                                  result.Rows[i].CONT_CODE, result.Rows[i].CONT_NAME,
                                                  result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE,
                                                  result.Rows[i].ADDRESS, result.Rows[i].DISTRICT,
                                                  result.Rows[i].CUST_GRP_NAME, result.Rows[i].CUST_TYPE,
                                                  result.Rows[i].MTD_SALES, result.Rows[i].YTD_SALES, result.Rows[i].NO_SKU, 
                                                  result.Rows[i].CUST_CLASS, result.Rows[i].TXN_NO,
                                                  result.Rows[i].TXN_DATE, result.Rows[i].TITLE_CODE,
                                                  result.Rows[i].TITLE_NAME, result.Rows[i].IMG_LOC, result.Rows[i].CREDITLIMIT, result.Rows[i].OUTBAL, icon);

                        marker.setMap(map);
                        WriteRecordCount(++count); //Display records plot on map

                        ValidCoordinates.push(new google.maps.LatLng(result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE));
                    }
                    else
                    { ShowMessage("Certain customers have invalid coordinates."); }
                }
                else
                { ShowMessage("Certain customers does not have coordinates information."); }
            }
        }
        function WriteRecordCount(count) {
            $("#spnCount").text(count);
        }
        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateIcon(Class) {
            var Icon;

            switch (Class) {
                case "A":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassA, label: 'A' });
                    break;
                case "B":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassB, label: 'B' });
                    break;
                case "C":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassC, label: 'C' });
                    break;
                case "D":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassD, label: 'D' });
                    break;
                default:
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: Unclassified, label: 'O' });
                    break;
            }

            return Icon;
        }

        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateMarker(CustCode, CustName, ContCode, ContName, Latitude, Longitude,
                            Address, District, CustGrpName, CustType,
                            MtdSales, YtdSales, NoSKU,
                            Class, TxnNo, TxnDate, TitleCode, TitleName, ImgLoc, CreditLimit, OutBal, Icon) {
            //var marker = new GMarker(new google.maps.LatLng(Latitude, Longitude), Icon);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Latitude, Longitude),
                map: map,
                icon: Icon
            });
            google.maps.event.addListener(marker, "click", function() {
                var str = "<div class=\"cls_label\">";

                str += "<span class='map_header_text'>Customer:</span> " + CustName + "<br />";
                str += "<span class='map_header_text'>Contact:</span> " + ContName + "<br />";
                str += "<span class='map_header_text'>Address:</span> " + Address + "<br />";
                str += "<span class='map_header_text'>District:</span> " + District + "<br />";
                str += "<span class='map_header_text'>Customer Group:</span> " + CustGrpName + "<br />";
                str += "<span class='map_header_text'>Customer Class:</span> " + GetClassName(Class) + "<br />";
                str += "<span class='map_header_text'>Customer Type:</span> " + CustType + "<br />";
                str += "<span class='map_header_text'>Customer MTD Sales:</span> " + MtdSales + "<br />";
                str += "<span class='map_header_text'>Customer YTD Sales:</span> " + YtdSales + "<br />";
                str += "<span class='map_header_text'>Customer No of SKU:</span> " + NoSKU + "<br />";
                str += "<span class='map_header_text'>Credit Limit:</span> " + CreditLimit + "<br />";
                str += "<span class='map_header_text'>Outstanding Balance:</span> " + OutBal + "<br />";
                str += "<span class='map_header_text'>Last MSS Date:</span> " + TxnDate + "<br />";
                str += "<span class='map_header_text'>Last Title Name:</span> " + TitleName + "<br />";
                str += "<a href='#' onclick='PopImage(\"" + root + "" + ImgLoc + "\", \"" + CustName + "\", \"\");'>Outlet Photo</a><br />";
                str += "<a href='javascript:NavigateAnswerEmb(\"" + TxnNo + "\");'>View Details</a>";

                str += "</div>";

                /// Eye Candy Code. Pan marker to center then only show InfoWindow
                /// Just calling panning and InfoWindow sequentially cannot achieve such effects
                var moveEnd = google.maps.event.addListener(map, "moveend", function() {
                    marker.openInfoWindowHtml(str);
                    google.maps.event.removeListener(moveEnd);
                });

                map.panTo(marker.getLatLng());
                //////////////////////////////////////////////////////////////////////////////////
            }
            );

            return marker;
        }
        function GetClassName(Class) {
            switch (Class) {
                case "A":
                    return "A";
                    break;
                case "B":
                    return "B";
                    break;
                case "C":
                    return "C";
                    break;
                case "D":
                    return "D";
                    break;
                default:
                    return "OTH";
                    break;
            }
        }
        function SetCenterMap() {
            if (ValidCoordinates != undefined && ValidCoordinates.length > 0) {
                var iCenter = parseInt(ValidCoordinates.length / 2);

                map.setCenter(ValidCoordinates[iCenter], 10);
                map.setZoom(10);
            }
        }
        function NavigateAnswer(TxnNo) {
            $("#DetailBarIframe", parent.document).attr('src', "../../../iFFMR/Customize/Geomap/MSSEnquiry/MSSQuesList.aspx?txnno=" + TxnNo + "&source=map");
            // alert(item.count());
        }
        function NavigateAnswerEmb(TxnNo) {
            $("#DetailIframe").attr('src', "../../../../iFFMR/Customize/Geomap/MSSEnquiry/MSSEmbeddedDtl.aspx?txnno=" + TxnNo + "");
            // alert(item.count());
        }
        /***********************************************
        / Show cust image
        ***********************************************/
        function PopImage(ImgLoc, CustName, Address) {
            mp_ToggleModalPopup("pnlPictureDiv", "divModalMask2", "on");


            LoadImage(ImgLoc);
            BindCust(CustName, Address);
        }
        function BindCust(CustName, Address) {
            $("#PicDiv_CustName").text(CustName);
            $("#PicDiv_Address").text(Address);
        }
        function LoadImage(ImgLoc) {
            //$("#CustImg").error(Image_OnErrorHide);
            resetCustImg();

            $("#CustImg").attr({
                src: ImgLoc,
                title: "",
                alt: "Customer Image"
            });
        }
        /***********************************************
        / Hide broken image
        ***********************************************/
        function Image_OnErrorHide() {
            $("#CustImg").hide();
            $("#ImgLoadFailMsg").show();
        }
        function resetCustImg() {
            $("#CustImg").show();
            $("#ImgLoadFailMsg").hide();

            $("#CustImg").attr({
                src: "../../../../images/indicator.gif",
                title: "",
                alt: "Loading"
            });

            $("#PicDiv_CustName").text("");
            $("#PicDiv_Address").text("");
        }
        function ToggleHideIcon() {
            if ($('#ico_collapse').attr('alt') == 'Hide') {
                $('#ico_collapse').attr('alt', 'Unhide')

                $('#ico_collapse').attr('src', '../../../../images/ico_down.jpg')
            }
            else {
                $('#ico_collapse').attr('alt', 'Hide')

                $('#ico_collapse').attr('src', '../../../../images/ico_up.jpg')
            }
        }
   </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="padding: 10px 0px 0px 10px; margin-right: 30px" class="BckgroundInsideContentLayout" onload="HideElement('ContentBar');HideElement('DetailBar');ShowElement('MapBar');MaximiseFrameHeight('MapBarIframe');Initialize();">
    <form id="form1" runat="server">
    <ajaxtoolkit:toolkitscriptmanager ID="ToolkitScriptManager1" 
            runat="server" AsyncPostBackTimeout="300" ScriptMode="Release">
            <Services>
           <asp:ServiceReference
           path="~/DataServices/ws_PrincipalLoc.asmx" />
           <asp:ServiceReference
           path="~/DataServices/ws_MSSEnq.asmx" />
      </Services>
    </ajaxtoolkit:toolkitscriptmanager>
    

    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
    <div style="width:90%;border: solid 1px #ffffff; padding: 1px">
        <div class="bckgroundTitleBar" style="width:99%">               
            <span class="cls_label" style="float:left"><b>Customer Market Survey Enquiry Map</b></span>
            <img id="ico_collapse" style="float:left;margin-left:5px" src="../../../../images/ico_up.jpg" onclick="mc_toggleCollapsible('#TxnDetails');ToggleHideIcon();" alt="Hide" />     
            <input id="btnBack" type="button" class="cls_button" style="float:right; margin-right:10px" value="Back" onclick="ShowElement('ContentBar');HideElement('DetailBar');HideElement('MapBar');MaximiseFrameHeight('ContentBarIframe');" />
        </div>    
        <div class="ctrlPanel" id="TxnDetails">
            <table border="0">
                <tr>
                    <td class="cls_label_header" style="width:100px">MSS Title:</td><td class="cls_label" style="width:300px"><asp:Label ID="lblTitle" runat="server" Text="-"></asp:Label></td>
                    <td class="cls_label_header" style="width:100px">Period:</td><td class="cls_label" style="width:300px"><asp:Label ID="lblPeriod" runat="server" Text="-"></asp:Label></td>
                </tr>
                <tr>
                    <td class="cls_label_header">MSS Question:</td><td class="cls_label"><asp:Label ID="lblQuestion" runat="server" Text="-"></asp:Label></td>
                    <td class="cls_label_header"></td><td class="cls_label"></td>
                </tr>
                <tr>
                    <td class="cls_label_header">MSS Sub Question:</td><td class="cls_label" colspan="3"><asp:Label ID="lblSubQuestion" runat="server" Text="-"></asp:Label></td>
                    
                </tr>
            </table>
        </div>
    </div>
    
    <br />
    
    <div style="width: 1100px">
    
    <div style="float:left; position:relative">
        <div style="width:593px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898"><span class="cls_label" id="spnCount" style="font-weight:bold">0</span><span class="cls_label" style="font-weight:bold"> records plot on map.</span></div>
        <div id="map_canvas" style="width: 600px; height: 450px; padding: 0px 0px 0px 0px;z-index:20">
        <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAviZX_5ANQLbn5c6dZmkdkRSSF4RoMyN7orcJSVRUD19sX6pe3hQRouH06-TjWCldY1M8qE45jNoHDA" type="text/javascript"></script>--%>
       <%-- <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>" type="text/javascript"></script>--%>
        </div>
        <div id="Message" class="cls_label" style="width:594px; display:none; z-index:30; position:absolute; padding:3px; background:#3C3C3C; color: #ffffff"></div>
    </div>
    
    <span id="DetailBar" style="float:left; margin-left:10px; position:relative">
        <iframe id="DetailIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="360" scrolling="auto" height="450" style="border: 0; position: absolute; top: 0px;">
        </iframe>
    </span>
    
    
    <div style="float:left; margin-left:10px">
    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
           <ContentTemplate>
           <asp:Timer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick">
                                                                            </asp:Timer>                                                                           
            <asp:Label style="font-weight:bold" CssClass="cls_label" ID="lblStats" runat="server" Text="Survey Statistic:" Visible="false"></asp:Label>
            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="false" AutoGenerateColumns="False"
                FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" 
                GridWidth="" AllowPaging="false" DataKeyNames="CRITERIA_CODE" bordercolor="Black" 
                borderwidth="1" GridBorderColor="Black" GridBorderWidth="1px" 
                RowHighlightColor="AntiqueWhite">
                <Columns>
                    <asp:BoundField HeaderText="Sub Question Options" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                     HtmlEncode="false" ItemStyle-HorizontalAlign="left" DataField="CRITERIA_NAME" SortExpression="CRITERIA_NAME" />
                    <asp:BoundField HeaderText="Number of Answers" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                     HtmlEncode="false" ItemStyle-HorizontalAlign="center" DataField="AMT" SortExpression="AMT" />
                </Columns>
                <AlternatingRowStyle CssClass="GridAlternate" />
                <FooterStyle CssClass="GridFooter" />
                <HeaderStyle CssClass="GridHeader" />
                <PagerSettings Visible="False" />
                <RowStyle CssClass="GridNormal" />
            </ccGV:clsGridView>
            </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    
    </div>
    
    <div id="pnlPictureDiv" class="modalPopup" style="z-index:9001;display: none; padding:10px; width:700px; height:450px; overflow:auto; position:absolute;">
        <%--<input id="btnClosePciture" type="button" value="Back" class="cls_button" OnClick="mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');" />--%>
        <img alt="Close" src="../../../../images/ico_close.gif" OnClick="mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');" style="float:right;margin-right:5px"/>
        <br />
        <span class="cls_label_header">Customer: </span><span id="PicDiv_CustName" class="cls_label"></span><br />
        <br />
        
        <div id="ImgLoadFailMsg" class="cls_label" style="display:none">Unable to load customer picture. This can be due to no image has been uploaded for this customer, or the image file has been removed. </div>
        <img id="CustImg" src="../../../../images/indicator.gif" alt='Loading' onerror='Image_OnErrorHide();' />
    </div>
    <div id="divModalMask2" style="position:absolute; left:0; top:0; z-index: 8000; display:none; background-color:#fff"></div>
    </form>
</body>
</html>
