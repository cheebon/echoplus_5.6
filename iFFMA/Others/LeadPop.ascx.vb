﻿Imports System.Data
Partial Class iFFMA_Others_LeadPop
    Inherits System.Web.UI.UserControl
    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property


    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadTeamCodeDDL(ByVal DT As DataTable)
        Dim dtTeamCode As DataTable
        Dim clsLead As New mst_others.clsLead

        Try
            Dim ddlTeamCode As DropDownList = CType(DetailsView1.FindControl("ddlTeamCode"), DropDownList)
            If ddlTeamCode IsNot Nothing Then
                dtTeamCode = clsLead.GetTeamDDL
                With ddlTeamCode
                    .Items.Clear()
                    .DataSource = dtTeamCode.DefaultView
                    .DataTextField = "TEAM_NAME"
                    .DataValueField = "TEAM_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("TEAM_CODE")), "", DT.Rows(0)("TEAM_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadQuesTypeDDL(ByVal DT As DataTable)
        Dim dtQuesType As DataTable
        Dim clsLead As New mst_others.clsLead

        Try
            Dim ddlQuesType As DropDownList = CType(DetailsView1.FindControl("ddlQuesType"), DropDownList)
            If ddlQuesType IsNot Nothing Then
                dtQuesType = clsLead.GetQuesTypeDDL
                With ddlQuesType
                    .Items.Clear()
                    .DataSource = dtQuesType.DefaultView
                    .DataTextField = "QUES_TYPE_NAME"
                    .DataValueField = "QUES_TYPE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("QUES_TYPE")), "", DT.Rows(0)("QUES_TYPE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadOptionsFieldVisibility()
        Dim ddlQuesType As DropDownList = CType(DetailsView1.FindControl("ddlQuesType"), DropDownList)
        Dim rfvOptions As RequiredFieldValidator = CType(DetailsView1.FindControl("rfvOptions"), RequiredFieldValidator)
        Dim txtOptions As TextBox = CType(DetailsView1.FindControl("txtOptions"), TextBox)
        Dim lblOpt As Label = CType(DetailsView1.FindControl("lblOpt"), Label)

        If ddlQuesType.SelectedValue = "2" Then
            rfvOptions.Visible = True
            txtOptions.Visible = True
            lblOpt.Visible = True

        ElseIf ddlQuesType.SelectedValue = "4" Then
            rfvOptions.Visible = True
            txtOptions.Visible = True
            lblOpt.Visible = True

        ElseIf ddlQuesType.SelectedValue = "1" Then
            rfvOptions.Visible = False
            txtOptions.Visible = False
            lblOpt.Visible = False

        ElseIf ddlQuesType.SelectedValue = "3" Then
            rfvOptions.Visible = False
            txtOptions.Visible = False
            lblOpt.Visible = False

        End If
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsLead As New mst_others.clsLead
            DT = clsLead.GetLeadDetails(TeamCode, QuesCode)
            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()
            If DT IsNot Nothing Then
                LoadTeamCodeDDL(DT)
                LoadQuesTypeDDL(DT)
            End If

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                Dim chk As CheckBox = CType(DetailsView1.FindControl("chkRequired"), CheckBox)
                Dim options As TextBox = CType(DetailsView1.FindControl("txtOptions"), TextBox)
                Dim lblOpt As Label = CType(DetailsView1.FindControl("lblOpt"), Label)
                If DT.Rows.Count > 0 Then
                    If DT.Rows(0)("REQUIRED") = "1" Then
                        chk.Checked = True
                    ElseIf DT.Rows(0)("REQUIRED") = "0" Then
                        chk.Checked = False
                    Else
                        chk.Checked = False
                    End If
                End If

                If DT.Rows(0)("QUES_TYPE") = "2" Then
                    options.Visible = True
                    lblOpt.Visible = True
                ElseIf DT.Rows(0)("QUES_TYPE") = "4" Then
                    options.Visible = True
                    lblOpt.Visible = True
                Else
                    options.Visible = False
                    lblOpt.Visible = False
                End If

                If DT.Rows.Count > 0 Then
                    Dim str As String() = (DT.Rows(0)("OPTIONS")).Split(";"c)
                    For Each value As String In str
                        options.Text += value & vbLf
                    Next
                End If
            End If
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub

    Public Sub RenewDataBind()
        ViewState.Clear()
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID

            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim clsLead As New mst_others.clsLead

            Dim ddlTeamCode As DropDownList = CType(DetailsView1.FindControl("ddlTeamCode"), DropDownList)
            Dim lblQuesCode As Label = CType(DetailsView1.FindControl("lblQuesCode"), Label)
            Dim txtQuesLabel As TextBox = CType(DetailsView1.FindControl("txtQuesLabel"), TextBox)
            Dim txtQuesSeq As TextBox = CType(DetailsView1.FindControl("txtQuesSeq"), TextBox)
            Dim ddlQuesType As DropDownList = CType(DetailsView1.FindControl("ddlQuesType"), DropDownList)
            Dim txtOptions As TextBox = CType(DetailsView1.FindControl("txtOptions"), TextBox)
            Dim chkRequired As CheckBox = CType(DetailsView1.FindControl("chkRequired"), CheckBox)
            Dim txtContInd As TextBox = CType(DetailsView1.FindControl("txtContInd"), TextBox)

            txtOptions.Text = txtOptions.Text.Replace(vbCr, ";").Replace(vbLf, ";")

            'If chkRequired.Checked = True Then
            '    chkRequired.Text = "1"
            'Else
            '    chkRequired.Text = "0"
            'End If

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsLead.UpdateLead(TeamCode, lblQuesCode.Text, Trim(txtQuesLabel.Text), Trim(txtQuesSeq.Text), Trim(ddlQuesType.SelectedValue), Trim(txtOptions.Text), chkRequired.Checked, Trim(txtContInd.Text))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()

            Else
                Dim strTeamCode As String = ddlTeamCode.SelectedValue
                Dim dtQuesCode As DataTable = clsLead.GetNewQuesCode(strTeamCode)
                Dim strQuesCode As String = dtQuesCode.Rows(0)(0)
                Dim DT As DataTable
                DT = clsLead.CreateLead(ddlTeamCode.SelectedValue, strQuesCode, Trim(txtQuesLabel.Text), Trim(txtQuesSeq.Text), Trim(ddlQuesType.SelectedValue), Trim(txtOptions.Text), chkRequired.Checked, Trim(txtContInd.Text))

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Record already exists!"
                    TeamCode = ddlTeamCode.SelectedValue.ToString
                    QuesCode = QuesCode
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else

                    lblInfo.Text = "The record is successfully created."
                    TeamCode = ddlTeamCode.SelectedValue.ToString
                    QuesCode = strQuesCode
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlQuesType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        LoadOptionsFieldVisibility()
        RenewDataBind()
        Show()
    End Sub

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
