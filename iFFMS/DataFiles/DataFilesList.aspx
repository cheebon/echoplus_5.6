<%@ Page Language="vb" AutoEventWireup="false" Inherits="iFFMS_DataFiles_DataFilesList" CodeFile="DataFilesList.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>Data Files List</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" href="../../include/DKSH.css">
    <!--#include File="~/include/commonutil.js"-->     
  </head>
  <body class="BckgroundInsideContentLayout">

    <form id="frmDataFilesList" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                       <tr class="Bckgroundreport">
	                        <td>
		                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
		                            <tr>
		                                <td vAlign="top" width="20%">       
							                <ccGV:clsGridView ID="dgFolderList" runat="server" ShowFooter="false" AllowSorting="True"
                                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                FreezeRows="0" GridWidth="" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="folder_id">
                                                <Columns>
                                                    <asp:BoundField DataField="folder_id" HeaderText="Folder ID" ReadOnly="True" SortExpression="folder_id" visible="false">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>                                                      
								                    <asp:ButtonField DataTextField="folder_name" HeaderText="Folder Name" CommandName="Select" AccessibleHeaderText="Select" ButtonType="Link" />                                                                                                                                                      
                                                </Columns>                                                
                                            </ccGV:clsGridView>									                
		                                </td>
		                                <td vAlign="top" width="50%">
		                                     <ccGV:clsGridView ID="dgFileList" runat="server" ShowFooter="false" AllowSorting="True"
                                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="file_id">
                                                <Columns>
                                                    <asp:BoundField DataField="folder_id" HeaderText="Folder ID" ReadOnly="True" SortExpression="folder_id" visible="false">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>   
                                                    <asp:BoundField DataField="file_id" HeaderText="File ID" ReadOnly="True" SortExpression="file_id" visible="false">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>                                                                   
								                   <asp:ButtonField DataTextField="file_name" HeaderText="File Name" CommandName="Select" AccessibleHeaderText="Select" ButtonType="Link" /> 
								                   <asp:BoundField DataField="file_path" HeaderText="File Path" ReadOnly="True" SortExpression="file_path" visible="false">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>
                                                   <asp:BoundField DataField="date_changed" HeaderText="Last Updated">
									                    <HeaderStyle></HeaderStyle>
								                    </asp:BoundField>
                                                </Columns>                                                
                                            </ccGV:clsGridView>			
		                                </td>
		                            </tr> 
		                        </table> 
		                    </td> 
		                </tr>
                         <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                      </table>
                  </td>
                </tr>               
            </TABLE> 
           
    </form>

  </body>
</html>
