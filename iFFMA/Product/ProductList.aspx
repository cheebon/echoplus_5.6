<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProductList.aspx.vb" Inherits="iFFMA_Product_ProductList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ProductMappingList" Src="ProductMappingList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ProductUOMList" Src="ProductUOMList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdGrpPop" Src="ProductGroupPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Product List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function RefreshPhoto() {
            // __doPostBack('tcResult_TabPanel4_btnRefresh', '');
            __doPostBack('btnRefresh', '');
        }

    </script>

    <style type="text/css">
        .auto-style1 {
            height: 23px;
        }
    </style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmProductList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                            <%--<span style="float:left; width:100%;">--%>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                                ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                AllowPaging="True" DataKeyNames="PRD_CODE" BorderColor="Black" BorderWidth="1"
                                                                                GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="SavePrd" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%">
                                                                            <tr>
                                                                                <td style="width: 17%"></td>
                                                                                <td style="width: 3%"></td>
                                                                                <td style="width: 80%"></td>
                                                                            </tr>
                                                                            <asp:Panel ID="pnlPrdGrp" runat="server">
                                                                                <tr>
                                                                                    <td>
                                                                                        <span class="cls_label_header">Supplier</span> <span class="cls_label_mark">*</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span class="cls_label_header">:</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                                                    </td>
                                                                                    <asp:RequiredFieldValidator ID="rfvSupplier" runat="server" ControlToValidate="ddlSupplier"
                                                                                        ErrorMessage="Supplier is Required." ValidationGroup="SavePrd" Display="Dynamic"
                                                                                        CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <span class="cls_label_header">Product Group</span> <span class="cls_label_mark">*</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <span class="cls_label_header">:</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlPrdGrp" runat="server" CssClass="cls_dropdownlist" />
                                                                                        <asp:Button ID="btnAddPrdGrp" runat="server" Text="Add New" CssClass="cls_button"
                                                                                            Visible="False" />
                                                                                    </td>
                                                                                    <asp:RequiredFieldValidator ID="rfvPrdGrp" runat="server" ControlToValidate="ddlPrdGrp"
                                                                                        ErrorMessage="Product Group is Required." ValidationGroup="SavePrd" Display="Dynamic"
                                                                                        CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                </tr>
                                                                            </asp:Panel>
                                                                            <tr>
                                                                                <td>
                                                                                    <span class="cls_label_header">Product Code</span> <span class="cls_label_mark">*</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="cls_label_header">:</span>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtPrdCode" runat="server" CssClass="cls_textbox" MaxLength="50" />
                                                                                </td>
                                                                                <asp:RequiredFieldValidator ID="rfvPrdCode" runat="server" ControlToValidate="txtPrdCode"
                                                                                    ErrorMessage="Product Code is Required." ValidationGroup="SavePrd" Display="Dynamic"
                                                                                    CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                <div style="display: none;">
                                                                                    <asp:TextBox ID="txtsapind" runat="server" />
                                                                                </div>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <span class="cls_label_header">Product Name</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="cls_label_header">:</span>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtPrdName" runat="server" CssClass="cls_textbox" MaxLength="100"
                                                                                        Width="200px" />
                                                                                    <asp:RequiredFieldValidator ID="rfvtxtPrdName" runat="server" ControlToValidate="txtPrdName"
                                                                                        ErrorMessage="Product Name is Required." ValidationGroup="SavePrd" Display="Dynamic"
                                                                                        CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <span class="cls_label_header">Status</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="cls_label_header">:</span>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                                                                        <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <span class="cls_label_header">Abbreviation</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="cls_label_header">:</span>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtAbbv" runat="server" CssClass="cls_textbox" MaxLength="50" Width="200px" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <span class="cls_label_header">Poison</span>
                                                                                </td>
                                                                                <td>
                                                                                    <span class="cls_label_header">:</span>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlPoison" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="0" Selected="True">Non Poison</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Poison</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                </tr>

                                                                <tr>
                                                                    <td colspan="3">&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
                                                            </tr>
                                                        </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_ProductMappingList ID="wuc_ProductMappingList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_ProductUOMList ID="wuc_ProductUOMList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:UpdatePanel ID="UpdatePanelPhoto" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <div>
                                                                    <table>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <div style="width: 400px; height: 400px;" class="BckgroundInsideContentLayout">
                                                                                    <asp:Label ID="lblCurrentImg" runat="server" Text="Current Image" CssClass="cls_label"></asp:Label>
                                                                                    <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="cls_button" />
                                                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="cls_button" />
                                                                                    <br />
                                                                                    <asp:Image ID="imgPrd" runat="server" AlternateText="Product Image" Width="350px"
                                                                                        Height="350px" />
                                                                                </div>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <iframe runat="server" id="fraImageProduct" src="" height="400px" width="400px" style="border: 0px solid;"></iframe>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel5" runat="server">
                                                    <ContentTemplate>
                                                        <div>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px" colspan="3">
                                                                        <asp:Button ID="btnSavePrdInfo" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" />
                                                                        <customToolkit:wuc_lblInfo ID="lblSavePrdInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%">
                                                                            <tr>
                                                                                <td style="width: 17%"></td>
                                                                                <td style="width: 3%"></td>
                                                                                <td style="width: 80%"></td>
                                                                            </tr>
                                                                            <tr valign="top">
                                                                                <td><span class="cls_label_header">Product Info</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtPrdInfo" runat="server" CssClass="cls_textbox" TextMode="MultiLine" MaxLength="500" Width="200px" /></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>

                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td style="height: 5px"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <customToolkit:wuc_PrdGrpPop ID="wuc_PrdGrpPop" Title="Product Group Maintenance"
            runat="server" />
    </form>
</body>
</html>
