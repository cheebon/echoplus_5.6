<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MSSAdv2QuesSeqPop.ascx.vb" Inherits="iFFMA_FieldForceActy_MSSAdv2QuesSeqPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:92%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:right; width:8%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                        
                    <!-- Begin Customized Content -->
                    <span style="float:left; width:15%; padding-top:10px;">
                        <span class="cls_label_header">No:</span><br>
                        <asp:ListBox ID="lsbOrder" CssClass="cls_listbox" runat="server" Height="150px" ForeColor="Black" Width="50px"></asp:ListBox>
                    </span> 
                    <span style="float:left; width:85%; padding-top:10px;">
                        <span class="cls_label_header">Questions:</span><br>
                        <asp:ListBox ID="lsbQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="150px" ForeColor="Black" Width="100%"></asp:ListBox>
                    </span> 
    <%--                <span style="float:left; width:30%; padding-left:10px; padding-top:60px;">

                    </span> 
                            --%>
                    <span style="float:left; width:100%; padding-top:10px; padding-bottom:10px">
                        <center>
                           <asp:Button ID="lnkUp" runat="server" CssClass="cls_button" Width="35" Text="Up" />
                         
                        <asp:Button ID="lnkDown" runat="server" CssClass="cls_button" Width="35" Text="Down" />
                              <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" />
             <%--           <asp:Panel ID="pnlEditMode" runat="server">

                        </asp:Panel>
                        <asp:Panel ID="pnlViewMode" runat="server" Visible="false">
                        </asp:Panel>--%>
                        </center>
                    </span>
                    <!-- End Customized Content -->
                    <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
        
    </ContentTemplate>
</asp:UpdatePanel>