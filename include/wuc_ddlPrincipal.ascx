<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlPrincipal.ascx.vb" Inherits="include_wuc_ddlPrincipal" %>
<asp:DropDownList id="ddlPrincipalID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvPrincipalID" ControlToValidate="ddlPrincipalID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select Principal."></asp:CompareValidator>
