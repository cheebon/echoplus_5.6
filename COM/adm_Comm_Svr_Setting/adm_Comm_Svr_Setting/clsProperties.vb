'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	16/04/2007
'	Purpose	    :	Properties for Class Comm Server Setting
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
'Imports System.ComponentModel

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
'<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    'Inherits ServicedComponent

    Public Class clsComm
        Public strCommID As String
        Public strCommCode As String
        Public strCommName As String
        Public strServerIP As String
        Public strServerName As String
        Public strDBIP As String
        Public intDBPort As Integer
        Public strDBName As String
        Public strLinkServerName As String
        Public strDBUserID As String
        Public strDBPassword As String
        Public strURL As String
        Public strProxy As String
        Public strCreatorUserID As String
        Public strCreatedDate As String
        Public strChangedUserID As String
        Public strChangedDate As String
        Public strSyncOperation As String
        Public strActiveFlag As String
        Public bChangePwd As Boolean
    End Class

End Class

