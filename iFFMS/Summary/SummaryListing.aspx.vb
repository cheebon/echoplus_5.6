Imports System.Data

Partial Class SummaryListing
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "Summary"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strSalesmanCode As String
        'Put user code to initialize the page here
        Try
            If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            'Call Header
            With wuc_lblheader
                .Title = "Summary"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                TimerControl1.Enabled = True
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub BindDateDLL()
        Dim obj As New txn_Summary.clsSummary
        Dim dt As DataTable = Nothing
        Dim strSalesrepCode As String
        Try
            strSalesrepCode = Trim(Session("SALESREP_CODE"))
            dt = obj.getDatesDT(strSalesrepCode)
            If dt.Rows.Count > 0 Then
                ddlDate.DataSource = dt
                ddlDate.DataValueField = "TXN_DATES"
                ddlDate.DataTextField = "TXN_DATES"
                ddlDate.DataBind()
                BindGrid(ddlDate.Items(0).Value)
            Else
                ddlDate.Items.Add("None")
                BindGrid("1900-01-01")
            End If

            UpdatePage.update()
            updategv.update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDateDLL : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub BindGrid(ByVal strRoutecode As String)
        Dim obj As New txn_Summary.clsSummary
        Dim dt As DataTable = Nothing
        Dim strSalesrepCode As String
        Try
            strSalesrepCode = Trim(Session("SALESREP_CODE"))
            dt = obj.getTransactionDT(strSalesrepCode, strRoutecode)
            dgList.DataSource = dt
            dgList.DataBind()

            UpdatePage.update()
            updategv.update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindGrid : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        Try
            TimerControl2.Enabled = True

        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlRoute_SelectedIndexChanged : " & ex.ToString)
        Finally

        End Try

    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then
            If Trim(Session("SALESREP_CODE")) IsNot Nothing Then
                BindDateDLL()
            End If

            TimerControl1.Enabled = False
        End If
    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        If TimerControl2.Enabled Then
            If Trim(Session("SALESREP_CODE")) IsNot Nothing Then
                BindGrid(ddlDate.SelectedValue)
            End If

            TimerControl2.Enabled = False
        End If
    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class



