
Partial Class include_wuc_txtCalendarRange
    Inherits System.Web.UI.UserControl

    Public Event imgCal_Click As EventHandler

    'RequiredFieldValidator
    Private _strValidationGroup As String
    Private _blnRequiredValidation As Boolean = True
    Private _strValidationErrorMessage As String

    'CompareDateRangeValidator
    Private _blnCompareDateRangeEnabled As Boolean = True
    Private _strCompareDateRangeErrorMessage As String

    'Date Format
    Private _strCurrentFormat As String
    Private _strDefaultDateFormatString As String = "yyyy-MM-dd"

    Private ReadOnly Property ControlName() As String
        Get
            Return "wuc_txtCalendarRange"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(_strCurrentFormat) Then setDefaultFormat()
        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
            txtDateStart.Attributes.Add("onblur", "formatDate('" & txtDateStart.ClientID & "', '" & DateFormatString & "');")
            txtDateStart.Attributes.Add("onkeydown", "EnterToTab();")
            txtDateEnd.Attributes.Add("onblur", "formatDate('" & txtDateEnd.ClientID & "', '" & DateFormatString & "');")
            txtDateEnd.Attributes.Add("onkeydown", "EnterToTab();")
            rfvFormatDateStart.ClientValidationFunction = "formatDate('" & txtDateStart.ClientID & "', '" & DateFormatString & "');"
            rfvFormatDateEnd.ClientValidationFunction = "formatDate('" & txtDateEnd.ClientID & "', '" & DateFormatString & "');"
            rfvCheckDateStartDataType.ErrorMessage = "<BR/>Invalid Date!(" & DateFormatString & ")"
            rfvCheckDateEndDataType.ErrorMessage = "<BR/>Invalid Date!(" & DateFormatString & ")"

            'Set ValidationGoupFiled is exist
            If Not String.IsNullOrEmpty(_strValidationGroup) Then
                rfvDateStart.ValidationGroup = _strValidationGroup
                rfvDateEnd.ValidationGroup = _strValidationGroup
                rfvCheckDateStartDataType.ValidationGroup = _strValidationGroup
                rfvCheckDateEndDataType.ValidationGroup = _strValidationGroup
            End If

            'Set Validation Visibility is Exist
            rfvDateStart.Enabled = _blnRequiredValidation
            rfvDateEnd.Enabled = _blnRequiredValidation
            rfvCheckDateStartDataType.Enabled = _blnRequiredValidation
            rfvCheckDateEndDataType.Enabled = _blnRequiredValidation
        End If
    End Sub

#Region "Text"

    Public Property DateStart() As String
        Get
            Return txtDateStart.Text
        End Get
        Set(ByVal Value As String)
            Dim dtm As DateTime
            If Not String.IsNullOrEmpty(Value) AndAlso IsDate(Value) Then
                dtm = DateTime.Parse(Value)
                txtDateStart.Text = dtm.ToString(DateFormatString)
            Else
                txtDateStart.Text = ""
            End If
        End Set
    End Property

    Public Property DateEnd() As String
        Get
            Return txtDateEnd.Text
        End Get
        Set(ByVal Value As String)
            Dim dtm As DateTime
            If Not String.IsNullOrEmpty(Value) AndAlso IsDate(Value) Then
                dtm = DateTime.Parse(Value)
                txtDateEnd.Text = dtm.ToString(DateFormatString)
            Else
                txtDateEnd.Text = ""
            End If
        End Set
    End Property

#End Region

#Region "Date Format"
    Private Sub setDefaultFormat()
        _strCurrentFormat = _strDefaultDateFormatString
    End Sub

    Public Property DateFormatString() As String
        Get
            If String.IsNullOrEmpty(_strCurrentFormat) Then setDefaultFormat()
            Return _strCurrentFormat
        End Get
        Set(ByVal value As String)
            _strCurrentFormat = value
        End Set
    End Property

#End Region

#Region "Compare Date Range Validator"
    Public Property CompareDateRangeValidation() As Boolean
        Get
            Return _blnCompareDateRangeEnabled
        End Get
        Set(ByVal blnVisible As Boolean)
            _blnCompareDateRangeEnabled = blnVisible
            If Not CV_DateRangeCompare Is Nothing Then CV_DateRangeCompare.Enabled = _blnCompareDateRangeEnabled
        End Set
    End Property

    Public Property CompareDateRangeErrorMessage() As String
        Get
            Return _strCompareDateRangeErrorMessage
        End Get
        Set(ByVal value As String)
            _strCompareDateRangeErrorMessage = value
            CV_DateRangeCompare.ErrorMessage = _strCompareDateRangeErrorMessage
        End Set
    End Property
#End Region

#Region "Require Field Validators"
    Public Property ValidationErrorMessage() As String
        Get
            Return _strValidationErrorMessage
        End Get
        Set(ByVal value As String)
            _strValidationErrorMessage = value
            rfvDateStart.ErrorMessage = _strValidationErrorMessage
            rfvDateEnd.ErrorMessage = _strValidationErrorMessage
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return _blnRequiredValidation
        End Get
        Set(ByVal blnVisible As Boolean)
            _blnRequiredValidation = blnVisible
            If Not rfvDateStart Is Nothing Then rfvDateStart.Enabled = _blnRequiredValidation
            If Not rfvDateEnd Is Nothing Then rfvDateEnd.Enabled = _blnRequiredValidation
            If Not rfvCheckDateStartDataType Is Nothing Then rfvCheckDateStartDataType.Enabled = _blnRequiredValidation 'HL:20080529
            If Not rfvCheckDateEndDataType Is Nothing Then rfvCheckDateEndDataType.Enabled = _blnRequiredValidation 'HL:20080529
        End Set
    End Property

    Public Property RequiredValidationGroup() As String
        Get
            Return _strValidationGroup
        End Get
        Set(ByVal value As String)
            _strValidationGroup = value
            If Not rfvDateStart Is Nothing Then rfvDateStart.ValidationGroup = _strValidationGroup
            If Not rfvDateEnd Is Nothing Then rfvDateEnd.ValidationGroup = _strValidationGroup
            If Not rfvCheckDateStartDataType Is Nothing Then rfvCheckDateStartDataType.ValidationGroup = _strValidationGroup 'HL:20080529
            If Not rfvCheckDateEndDataType Is Nothing Then rfvCheckDateEndDataType.ValidationGroup = _strValidationGroup 'HL:20080529
            If Not CV_DateRangeCompare Is Nothing Then CV_DateRangeCompare.ValidationGroup = _strValidationGroup
        End Set
    End Property

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        'Call error Message Viewer
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg
            'ltlMsg.Text = "alert('" & strMsg & "');"
            'Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>alert('" & strMsg & "');</script>")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "PopupScript", "alert('" & strMsg & "');", True)

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing
        Catch ex As Exception

        Finally
        End Try
    End Sub

End Class
