﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports cor_DB
Imports System.Web.Script.Services
Imports System.Collections.Generic

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class ws_RoutePlanning
    Inherits System.Web.Services.WebService

#Region "Helper class"
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffmr_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class
    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class
    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
#End Region

    '************************************************
    '
    'Define class structure to package returned data
    '
    '************************************************
#Region "Class Entity Defintion"
    Public Class PreviousRoutePlan
        Inherits BaseEntity
#Region "LocalVariable"
        Private _CustCode As String
        Private _CustName As String
        Private _Latitude As String = String.Empty
        Private _Longitude As String = String.Empty
#End Region
#Region "Public Property"
        Public Property CUST_NAME() As String
            Get
                Return _CustName
            End Get
            Set(ByVal value As String)
                _CustName = value
            End Set
        End Property
        Public Property CUST_CODE() As String
            Get
                Return _CustCode
            End Get
            Set(ByVal value As String)
                _CustCode = value
            End Set
        End Property
        Public Property LATITUDE() As String
            Get
                Return _Latitude
            End Get
            Set(ByVal value As String)
                _Latitude = value
            End Set
        End Property
        Public Property LONGITUDE() As String
            Get
                Return _Longitude
            End Get
            Set(ByVal value As String)
                _Longitude = value
            End Set
        End Property
#End Region
    End Class

#End Region
    <WebMethod(EnableSession:=True)> _
    Public Function SaveRouteSeq(ByVal Customers() As Object, ByVal strSalesrepCode As String, ByVal strRouteCode As String) As Integer
        Dim strCustCode As String
        Dim i As Integer = 0
        Dim strGuid As String = System.Guid.NewGuid().ToString()
        Try
            For Each seq As Object In Customers
                i += 1

                Dim cust As New Dictionary(Of String, Object)()
                cust = DirectCast(seq, Dictionary(Of String, Object))

                strCustCode = cust("CustCode").ToString

                InsertSeq(strGuid, strCustCode, strSalesrepCode, strRouteCode, i, Session("ffma_conn"), Session("UserID"))
            Next

            Commit(strSalesrepCode, strRouteCode, strGuid, Session("ffma_conn"), Session("UserID"))

            Return 0
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_RoutePlanning.SaveRouteSeq :" & ex.Message))
        Finally
            Clear(strGuid, Session("ffma_conn"), Session("UserID"))
        End Try
    End Function

    Private Sub InsertSeq(ByVal strGuid As String, ByVal strCustCode As String, ByVal strSalesrepCode As String, ByVal strRouteCode As String, ByVal intSeq As Integer, ByVal strConn As String, ByVal strUserID As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_SEQ_TEMP_INSERT"
                .addItem("GUID", strGuid, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("SEQ", intSeq, clsDB.DataType.DBInt)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Sub Commit(ByVal strSalesrepCode As String, ByVal strRouteCode As String, ByVal strGuid As String, ByVal strConn As String, ByVal strUserID As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_SEQ_COMMIT"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("GUID", strGuid, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Sub Clear(ByVal strGuid As String, ByVal strConn As String, ByVal strUserID As String)

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_SEQ_TEMP_CLEAR"
                .addItem("GUID", strGuid, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw
        Finally
            objDB = Nothing
        End Try
    End Sub

    <WebMethod(EnableSession:=True)> _
    Public Function ReturnPreviousRoutePlan(ByVal strSalesrepCode As String, ByVal strRouteCode As String) As PagedResult(Of PreviousRoutePlan)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of PreviousRoutePlan)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffma_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_ROUTE_PLANNING_SEQ_LIST"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "ROUTE_CODE", strRouteCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_PreviousRoutePlan(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            rptResult = New PagedResult(Of PreviousRoutePlan)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_RoutePlan.ReturnRoute :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    Public Shared Function Build_PreviousRoutePlan(ByRef reader As IDataReader) As PreviousRoutePlan
        Dim rptRow As New PreviousRoutePlan
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)

                    Case "CUST_CODE"
                        SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
                    Case "CUST_NAME"
                        SetValue(Of String)(.CUST_NAME, reader("CUST_CODE"), String.Empty)
                    Case "LATITUDE"
                        SetValue(Of String)(.LATITUDE, reader("LATITUDE"), 0)
                    Case "LONGITUDE"
                        SetValue(Of String)(.LONGITUDE, reader("LONGITUDE"), 0)


                End Select
            Next
        End With
        Return rptRow
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class
