﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dfPrincipalList.aspx.vb" Inherits="Admin_Root_DFPrincipal_dfPrincipalList" StyleSheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data File Principal</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmdfPrincipal" runat="server">
   <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />   
    <div>   
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
        
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <!-- Search box -->
                <div class="border_std">
                <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblCountryCode" runat="server" Text="Country Code "></asp:Label></td><td>
                            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" Width="200px">
                            </asp:DropDownList>
                        </td>
                </tr> 
                <tr>
                    <td>
                        <asp:Label ID="lblPrincipalCode" runat="server" Text="Principal Code "></asp:Label></td><td>
                            <asp:DropDownList ID="ddlPrincipal" runat="server" Width="200px">
                            </asp:DropDownList>
                            </td>
                </tr>               
                </table>
                <asp:Button ID="btnSearch" runat="server" Text="Search" />
                </div>
                <br />
                
                <!--Begin Modify Here -->
                <div class="border_std">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 15px 0px 15px 0px;">
                     <tr class="ajax__tab_portal">
                        <td class="ajax__tab_PNL">
                            <%--<customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>--%>
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                Width="98%" FreezeHeader="True" GridHeight="330px" AddEmptyHeaders="0" CellPadding="2"
                                EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="PRINCIPAL_ID">
                                <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                <EmptyDataTemplate>There is no data added.</EmptyDataTemplate>
                                <Columns>
                                    <asp:ButtonField CommandName="Modify" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit">
                                        <itemstyle horizontalalign="Center" width="5%" />
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="COUNTRY_CODE" HeaderText="Country Code" SortExpression="COUNTRY_CODE" />
                                    <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="COUNTRY_NAME" HeaderText="Country Name" SortExpression="COUNTRY_NAME" />
                                    <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="PRINCIPAL_CODE" HeaderText="Principal Code" SortExpression="PRINCIPAL_CODE" />
                                    <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="PRINCIPAL_NAME" HeaderText="Principal Name" SortExpression="PRINCIPAL_NAME" />
                                    
                                </Columns>
                            </ccGV:clsGridView>
                        </td>
                    </tr>
                    <tr class="Bckgroundreport"><td align="right"></td></tr>
                </table>
                </div>
                <!--End Modify Here -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
