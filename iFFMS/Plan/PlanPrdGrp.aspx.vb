Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_Plan_PlanPrdGrp
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountSelectedPrdGrp() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSelectedPrdGrp"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSelectedPrdGrp") = value
        End Set
    End Property

    Private Property Master_Row_CountNewPrdGrp() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountNewPrdGrp"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountNewPrdGrp") = value
        End Set
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Assign Prduct Group" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControlSelectedPrdGrp.Enabled = True
            TimerControlNewPrdGrp.Enabled = True
        End If
    End Sub


#Region "DGLISTSelectedCont"
    Protected Sub TimerControlSelectedPrdGrp_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlSelectedPrdGrp.Tick
        If TimerControlSelectedPrdGrp.Enabled Then RefreshDatabindingSelectedPrdGrp()
        TimerControlSelectedPrdGrp.Enabled = False
    End Sub

    Public Sub RenewDataBindSelectedPrdGrp()
        RefreshDataBindSelectedPrdGrp()
    End Sub

    Public Sub RefreshDataBindSelectedPrdGrp()
        RefreshDatabindingSelectedPrdGrp()
    End Sub

    Public Sub RefreshDatabindingSelectedPrdGrp(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableSelectedPrdGrp As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionSelectedPrdGrp As String = CType(ViewState("strSortExpressionSelectedPrdGrp"), String)


        dtCurrentTableSelectedPrdGrp = GetRecListSelectedPrdGrp()

        If dtCurrentTableSelectedPrdGrp Is Nothing Then
            dtCurrentTableSelectedPrdGrp = New DataTable
        Else
            If dtCurrentTableSelectedPrdGrp.Rows.Count = 0 Then
                Master_Row_CountSelectedPrdGrp = 0
                btncheckalldelete.Visible = False
                btnuncheckalldelete.Visible = False
                btndelete.Visible = False
                'dtCurrentTableSelectedCont.Rows.Add(dtCurrentTableSelectedCont.NewRow())
            Else

                Master_Row_CountSelectedPrdGrp = dtCurrentTableSelectedPrdGrp.Rows.Count
                btncheckalldelete.Visible = True
                btnuncheckalldelete.Visible = True
                btndelete.Visible = True
            End If
        End If

        Dim dvCurrentViewSelectedPrdGrp As New DataView(dtCurrentTableSelectedPrdGrp)
        If Not String.IsNullOrEmpty(strSortExpressionSelectedPrdGrp) Then
            Dim strSortExpressionNameSelectedPrdGrp As String = strSortExpressionSelectedPrdGrp.Replace(" DESC", "")
            dvCurrentViewSelectedPrdGrp.Sort = IIf(dtCurrentTableSelectedPrdGrp.Columns.Contains(strSortExpressionSelectedPrdGrp), strSortExpressionSelectedPrdGrp, "")
        End If

        With dglistSelectedPrdGrp
            .DataSource = dvCurrentViewSelectedPrdGrp
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountSelectedPrdGrp > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateSelectedPrdGrp()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListSelectedPrdGrp() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strUserId As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonPrdGrp(strSalesrepCode, strRouteDate, strCustCode, strUserId)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateSelectedPrdGrp()

        UpdateSelectedPrdGrp.Update()

    End Sub

    Protected Sub dglistSelectedPrdGrp_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedPrdGrp.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedPrdGrp > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListSelectedPrdGrp_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistSelectedPrdGrp.Sorting
        Dim strSortExpressionSelectedPrdGrp As String = ViewState("strSortExpressionSelectedPrdGrp")

        If strSortExpressionSelectedPrdGrp IsNot Nothing AndAlso strSortExpressionSelectedPrdGrp.Length > 0 Then
            If strSortExpressionSelectedPrdGrp Like (e.SortExpression & "*") Then
                If strSortExpressionSelectedPrdGrp.IndexOf(" DESC") > 0 Then
                    strSortExpressionSelectedPrdGrp = e.SortExpression
                Else
                    strSortExpressionSelectedPrdGrp = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionSelectedPrdGrp = e.SortExpression
            End If
        Else
            strSortExpressionSelectedPrdGrp = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionSelectedPrdGrp") = strSortExpressionSelectedPrdGrp

        RefreshDatabindingSelectedPrdGrp()

    End Sub

#End Region

#Region "DGLISTNewCust"
    Protected Sub TimerControlNewPrdGrp_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlNewPrdGrp.Tick
        If TimerControlNewPrdGrp.Enabled Then RefreshDatabindingNewPrdGrp()
        TimerControlNewPrdGrp.Enabled = False
    End Sub

    Public Sub RenewDataBindNewPrdGrp()
        RefreshDataBindNewPrdGrp()
    End Sub

    Public Sub RefreshDataBindNewPrdGrp()
        RefreshDatabindingNewPrdGrp()
    End Sub

    Public Sub RefreshDatabindingNewPrdGrp(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableNewPrdGrp As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionNewPrdGrp As String = CType(ViewState("strSortExpressionNewPrdGrp"), String)


        dtCurrentTableNewPrdGrp = GetRecListNewPrdGrp()

        If dtCurrentTableNewPrdGrp Is Nothing Then
            dtCurrentTableNewPrdGrp = New DataTable
        Else
            If dtCurrentTableNewPrdGrp.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountSelectedPrdGrp = 0
                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btnadd.Visible = False
            Else
                Master_Row_CountSelectedPrdGrp = dtCurrentTableNewPrdGrp.Rows.Count
                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btnadd.Visible = True
            End If
        End If

        Dim dvCurrentViewNewPrdGrp As New DataView(dtCurrentTableNewPrdGrp)
        If Not String.IsNullOrEmpty(strSortExpressionNewPrdGrp) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionNewPrdGrp.Replace(" DESC", "")
            dvCurrentViewNewPrdGrp.Sort = IIf(dtCurrentTableNewPrdGrp.Columns.Contains(strSortExpressionNewPrdGrp), strSortExpressionNewPrdGrp, "")
        End If

        With dglistNewPrdGrp
            .DataSource = dvCurrentViewNewPrdGrp
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountNewPrdGrp > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateNewPrdGrp()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListNewPrdGrp() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strCustCode As String, strUserId As String, strRouteDate As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonContByPrdGrp(strSalesrepCode, strCustCode, strUserId, strRouteDate)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateNewPrdGrp()

        UpdateNewPrdGrp.Update()

    End Sub

    Protected Sub dglistNewPrdGrp_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistNewPrdGrp.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountNewPrdGrp > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListNewPrdGrp_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistNewPrdGrp.Sorting
        Dim strSortExpressionNewPrdGrp As String = ViewState("strSortExpressionNewPrdGrp")

        If strSortExpressionNewPrdGrp IsNot Nothing AndAlso strSortExpressionNewPrdGrp.Length > 0 Then
            If strSortExpressionNewPrdGrp Like (e.SortExpression & "*") Then
                If strSortExpressionNewPrdGrp.IndexOf(" DESC") > 0 Then
                    strSortExpressionNewPrdGrp = e.SortExpression
                Else
                    strSortExpressionNewPrdGrp = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionNewPrdGrp = e.SortExpression
            End If
        Else
            strSortExpressionNewPrdGrp = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionNewPrdGrp") = strSortExpressionNewPrdGrp

        RefreshDatabindingNewPrdGrp()

    End Sub

#End Region


#Region "Event Handler"

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        AddNewPrdGrp()
        TimerControlSelectedPrdGrp.Enabled = True
        TimerControlNewPrdGrp.Enabled = True
    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        DelNewPrdGrp()
        TimerControlSelectedPrdGrp.Enabled = True
        TimerControlNewPrdGrp.Enabled = True
    End Sub

#End Region

    Private Sub AddNewPrdGrp()
        Try
            If dglistNewPrdGrp.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strPrdGrpCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistNewPrdGrp.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistNewPrdGrp.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strPrdGrpCode = dglistNewPrdGrp.DataKeys(i).Item("PRD_GRP_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.InstPlanAddonPrdGrp(strSalesrepCode, strRouteDate, strCustCode, strPrdGrpCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblNmsg.Text = "Record(s) added !"
            lblsmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelNewPrdGrp()
        Try
            If dglistSelectedPrdGrp.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strPrdGrpCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistSelectedPrdGrp.Rows
                    chkSelected = CType(DR.FindControl("chkDelete"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistSelectedPrdGrp.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strPrdGrpCode = dglistSelectedPrdGrp.DataKeys(i).Item("PRD_GRP_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.DelPlanAddonPrdGrp(strSalesrepCode, strRouteDate, strCustCode, strPrdGrpCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblSmsg.Text = "Record(s) deleted !"
            lblNmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
