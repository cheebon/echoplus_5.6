﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Left.aspx.vb" Inherits="Admin_Root_Left" StylesheetTheme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Left Page</title>
    <%--<script language="javascript" type="text/javascript">
        function CloseOpen()
        {
            var left = parent.document.getElementById("fraLeft");
            var frame = parent.document.getElementById("fraCon");
            if (frame)
            {
                if (left.offsetWidth==20)
                {
                    frame.cols = "150,*";
                }
                else
                {
                    frame.cols = "20,*";
                }
            }
        }
    </script>--%>
</head>
<body style="margin-right:0">
    <form id="form1" runat="server">
    <%--<img src="" onclick="CloseOpen();" style="margin-right:0px;margin-top:0px" />--%>
    
    <div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" >
        <%--<tr>
            <td align="right">
                <asp:ImageButton ID="imgBtnClose" runat="server" onclientclick="CloseOpen();" />
            </td>
        </tr> --%>       
        <%--<tr>
            <td>
                <asp:Label ID="lblLink" runat="server" Text="Link"></asp:Label></td>
        </tr>--%>
        <tr class="link">
            <td><asp:HyperLink ID="lnkCountryList" runat="server" NavigateUrl="Country/CountryList.aspx" Target="fraMain" Text="Country"  /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkSettingList" runat="server" NavigateUrl="Setting/SettingList.aspx" Target="fraMain" Text="Setting" /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkPrincipalList" runat="server" NavigateUrl="Principal/PrincipalList.aspx" Target="fraMain" Text="Principal" /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkSubmodulePrincipalList" runat="server" NavigateUrl="SubmodulePrincipal/SubmodulePrincipalList.aspx" Target="fraMain" Text="Submodule Principal" /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkSubmoduleList" runat="server" NavigateUrl="Submodule/SubmoduleList.aspx" Target="fraMain" Text="Submodule" /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkBSADMIN" runat="server" NavigateUrl="User/BsadminAccList.aspx" Target="fraMain" Text="Bsadmin" /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkSync" runat="server" NavigateUrl="Sync/Sync.aspx" Target="fraMain" Text="Sync" /></td>
        </tr>
        <tr class="link">
            <td><asp:HyperLink ID="lnkDFPrincipalList" runat="server" NavigateUrl="DFPrincipal/dfPrincipalList.aspx" Target="fraMain" Text="Data Files Principal" /></td>
        </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
