<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAOrder.aspx.vb" Inherits="iFFMS_TRA_TRAOrder"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblDate" Src="~/include/wuc_lblDate.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustSearch" Src="~/iFFMS/COMMON/wuc_Custsearch.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdSearch" Src="~/iFFMS/COMMON/wuc_Prdsearch.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="Wuc_CustPayersearch" Src="~/iFFMS/COMMON/wuc_CustPayersearch.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="Wuc_CustPayersearch2" Src="~/iFFMS/COMMON/wuc_CustPayersearch.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_SRSearch" Src="~/iFFMS/COMMON/wuc_SRSearch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<!DOCTYPE html>--%>
<%--<meta http-equiv="X-UA-Compatible" content="IE=10;chrome=1" />--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TRA Order</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="../../styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <%--<script src="../../scripts/jquery-1.10.2.min.js" type="text/javascript"></script>--%>
    <link href="../../styles/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/jquery-2.1.4.min.js"></script>
    <script src="../../scripts/Popup.js" type="text/javascript"></script>
    <script src="../../scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../scripts/bootbox.min.js" type="text/javascript"> </script>
    <link href="../../scripts/jQuery-File-Upload/uploadfile.css" rel="stylesheet" />
    <script src="../../scripts/jQuery-File-Upload/jquery.uploadfile.min.js" type="text/javascript"></script>
    <%--<script src="../../scripts/attachment.js" type="text/javascript"> </script>--%>

    <script src="../../scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

    <style type="text/css">
        .ui-autocomplete {
            max-height: 200px;
            max-width: 400px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
            overflow-x: hidden;
        }
        /* IE 6 doesn't support max-height
        * we use height instead, but this forces the menu to always be this tall
        */
        * html .ui-autocomplete {
            height: 200px;
        }

        td, tr {
            padding: 2px !important;
        }

        .test {
            color: black;
        }
    </style>
    <script language="javascript" type="text/javascript">

        var txn_no = '';

        $(document).ready(function () {
            SAPBillingAutoComplete()

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                SAPBillingAutoComplete()
            }
        });

        function SAPBillingAutoComplete() {
            var strSalesrepCode = $('#txtsalesrepCode').val();
            var strCustCode = $('#txtcustomer').val();
            $("#txtSAPBillingNo").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../../DataServices/ws_TRAOrder.asmx/GetAutoCompleteSAPBillingNo",
                        data: "{'strSalesrepCode': '" + strSalesrepCode + "','strCustCode':'" + strCustCode + "','strInvNo':'" + request.term + "' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    text: item.SAPBillingNo,
                                    value: item.SAPBillingNo
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $("#txtSAPBillingNo").autocomplete("close");
                        }
                    });
                },
                minLength: 1,
                focus: function (event, ui) {
                    $('#txtSAPBillingNo').val(ui.item.value);
                    return false;
                },
                select: function (event, ui) {
                    $('#txtSAPBillingNo').val(ui.item.value);
                    return false;
                }
            });
        }

        function HideNavigation() {
            var height = screen.height;
            if (height < 700) {
                self.parent.parent.document.getElementById('fraDefault').rows = "20,*";
            }
            else { self.parent.parent.parent.document.getElementById('fraDefault').rows = "87,*"; }
        }

        function ShowNavigation() {
            var height = screen.height;
            if (height < 700) {
                var fraDefault = self.parent.parent.parent.document.getElementById('fraDefault').rows = "65,*";
            }
            else { self.parent.parent.parent.document.getElementById('fraDefault').rows = "132,*"; }
        }

        var valueChanged = false;
        function changeValue(value)
        { valueChanged = value; }

        function CalculateAmt() {
            var txtqty = document.getElementById('txtqty'); var txtprice = document.getElementById('txtprice'); var lblamount = document.getElementById('lblamount');
            var amt = (txtqty.value * txtprice.value); lblamount.value = amt.toFixed(2);
        }
        function UOMPrice() {
            var ddluom = document.getElementById('ddluom');
            var lbluomprice = document.getElementById('lbluomprice');
            lbluomprice.value = ddluom.value;
            var txtselecteduom = document.getElementById('txtselecteduom');
            var I = ddluom.selectedIndex;
            txtselecteduom.value = ddluom.options[I].text;
        }

        function ValidateProduct() {
            _message = $get('message');
            _message.style.display = 'none';
            var txtPrdName = document.getElementById('txtPrdName');
            var lbluomload = document.getElementById('lbluomload');
            var txtPrdCode = document.getElementById('txtPrdCode');
            var strPrdCode = txtPrdCode.value;
            if (strPrdCode != '') { txtPrdName.value = 'Loading'; lbluomload.innerHTML = 'Loading'; };
            var strSalesrepCode;
            if (document.getElementById('txtsalesrepCode')) {
                var txtsalesrepCode = document.getElementById('txtsalesrepCode');
                strSalesrepCode = txtsalesrepCode.value;
            }
            else {
                var txtsalesrepCode = document.getElementById('lblSalesrepCode');
                strSalesrepCode = txtsalesrepCode.innerHTML;
            }
            if (strPrdCode) {
                ws_TRAOrder.ValidateProduct(strPrdCode, strSalesrepCode, onValidateProductLoadSuccess,
    function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; });
            }
        }
        function onValidateProductLoadSuccess(result) {
            var txtPrdName = document.getElementById('txtPrdName');
            var txtPrdCode = document.getElementById('txtPrdCode');
            if (result.Total > 0) {
                txtPrdName.value = result.Rows[0]['PRD_NAME']; bindDDLUOM();
            }
            else { txtPrdCode.value = ''; txtPrdName.value = ''; txtPrdName.value = 'Product information not found!' }
        }
        function bindDDLUOM() {
            _message = $get('message');
            _message.style.display = 'none';
            var txtPrdCode = document.getElementById('txtPrdCode');
            var strPrdCode = txtPrdCode.value;
            var strSalesrepCode;
            if (document.getElementById('txtsalesrepCode')) {
                var txtsalesrepCode = document.getElementById('txtsalesrepCode');
                strSalesrepCode = txtsalesrepCode.value;
            }
            else {
                var txtsalesrepCode = document.getElementById('lblSalesrepCode');
                strSalesrepCode = txtsalesrepCode.innerHTML;
            }

            ws_TRAOrder.GetUOMCode(strPrdCode, strSalesrepCode, onUOMLoadSuccess,
    function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; });
        }
        function onUOMLoadSuccess(result) {
            var ddlUOM = document.getElementById("ddluom");
            var lbluomload = document.getElementById('lbluomload');
            ddlUOM.options.length = 0;
            for (var idx = 0; idx < result.Total; idx++) {
                addDropDownValue(ddlUOM, result.Rows[idx]["UOM_CODE"], result.Rows[idx]["UOM_PRICE"]);
            }

            if (result.Total > 0) {
                ddlUOM.selectedIndex = 0; UOMPrice();
            }
            lbluomload.innerHTML = ''
        }
        function addDropDownValue(ddlCtrl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddlCtrl.options.add(opt);
        }
        function EnterToTab() { var keycode = window.event.keyCode; if (keycode == 13) { window.event.keyCode = 9; } }
        function ValidateCustPayer() {
            _message = $get('message');
            _message.style.display = 'none';
            var txtsalesrepCode = document.getElementById('txtsalesrepCode');
            var txtCustCode = document.getElementById('txtcustomer');
            var txtPayerCode = document.getElementById('txtPayerCode');
            var txtpayername = document.getElementById('txtpayername');
            var txtsoldto = document.getElementById('txtsoldto');

            var strCustCode = txtCustCode.value;
            var strPayerCode = txtPayerCode.value;
            var strSalesrepCode = txtsalesrepCode.value;

            if (strPayerCode != '' && strSalesrepCode != '') { txtpayername.value = 'Loading'; txtsoldto.value = 'Loading'; }
            if (strPayerCode == '') { txtPayerCode.value = ''; txtpayername.value = 'Kindly enter payer code first!'; }
            if (strSalesrepCode == '') { txtPayerCode.value = ''; txtpayername.value = 'Kindly enter salesrep code first!'; }

            if (strPayerCode != '' && strSalesrepCode != '') {
                ws_TRAOrder.ValidateCustPayer(strCustCode, strPayerCode, strSalesrepCode, onValidateCustPayerLoadSuccess,
    function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; })
            }
        }
        function onValidateCustPayerLoadSuccess(result) {
            var txtpayername = document.getElementById('txtpayername');
            var txtPayerCode = document.getElementById('txtPayerCode');
            var txtsoldto = document.getElementById('txtsoldto');
            if (result.Total > 0) { txtpayername.value = result.Rows[0]['PAYER_NAME']; txtsoldto.value = result.Rows[0]['SOLDTO_NAME']; }
            else { txtPayerCode.value = ''; txtpayername.value = 'Invalid payer code. Key in payer code again!'; txtsoldto.value = ''; }
        }

        function ValidateCust() {           
            _message = $get('message');
            _message.style.display = 'none';
            var txtsalesrepCode = document.getElementById('txtsalesrepCode');
            var txtCustCode = document.getElementById('txtcustomer');
            var txtPayerCode = document.getElementById('txtPayerCode');
            var txtCustomerName = document.getElementById('txtCustomerName');
            var txtpayername = document.getElementById('txtpayername');
            var txtsoldto = document.getElementById('txtsoldto');

            var strCustCode = txtCustCode.value;
            var strPayerCode = txtPayerCode.value;
            var strSalesrepCode = txtsalesrepCode.value;


            if (strCustCode != '' && strSalesrepCode != '') { txtCustomerName.value = 'Loading'; txtsoldto.value = 'Loading'; }
            if (strCustCode == '') { txtCustCode.value = ''; txtCustomerName.value = 'Kindly enter ship to code first!'; }
            if (strSalesrepCode == '') { txtCustCode.value = ''; txtCustomerName.value = 'Kindly enter salesrep code first!'; }

            if (strCustCode != '' && strSalesrepCode != '') {
                ws_TRAOrder.ValidateCust(strCustCode, strPayerCode, strSalesrepCode, onValidateCustLoadSuccess,
    function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; })
            }
        }
        function onValidateCustLoadSuccess(result) {
            var txtCustomerName = document.getElementById('txtCustomerName');
            var txtCustCode = document.getElementById('txtcustomer');
            var txtsoldto = document.getElementById('txtsoldto');
            var ddlDivision = document.getElementById('ddlDivision');
            if (result.Total > 0) {
                txtCustomerName.value = result.Rows[0]['SHIPTO_NAME'];
                txtsoldto.value = result.Rows[0]['SOLDTO_NAME'];
                __doPostBack("btnTriggerDivision", "");
                //if (result.Rows[0]['DIVISION'] != "") {
                //    var strCustCode = txtCustCode.value;

                //    ws_TRAOrder.GetConfigValue("division_default_value", function (msg) {
                //        ws_TRAOrder.GetDivisionByCust(strCustCode, function (success) {
                //            ddlDivision.options.length = 0;
                //            for (var idx = 0; idx < success.Total; idx++) {
                //                addDropDownValue(ddlDivision, success.Rows[idx]["DIVISION"], success.Rows[idx]["DIVISION"]);
                //                if (success.Rows[idx]["DIVISION"] == msg.Rows[0].setup_value) {
                //                    ddlDivision.value = msg.Rows[0].setup_value;
                //                }
                //            }
                //        },
                //    function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; });
                //    }, function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; });

                //}
                
                ws_TRAOrder.GetConfigValue("field_access", function (success) {
                    if (success.Total > 0) {
                        for (i = 0; i < success.Total; i++) {
                            if (success.Rows[i].setup_value == "Attachment") {
                                document.getElementById('lblAttachment').style.display = "block";
                                document.getElementById('btnAttachment').style.display = "block";
                            }
                        }
                    }
                }, function (exception) { _message.innerHTML = exception.get_message(); _message.style.display = ''; });

            }
            else { txtCustCode.value = ''; txtCustomerName.value = 'Invalid Shipto Code. Key in shipto code again!'; txtsoldto.value = ''; }
        }

        function ValidateDtlQty(source, args) {
            var txtqty = document.getElementById('txtqty');
            var txtPrdCode = document.getElementById('txtPrdCode');
            if (txtPrdCode || txtqty) {
                var strqty = txtqty.value;
                var strprdcode = txtPrdCode.value;
                if (strqty > 0) { args.IsValid = true; }
                else { args.IsValid = false; }
            }
        }

        function ValidateDtlPrice(source, args) {
            var txtprice = document.getElementById('txtprice');
            var txtPrdCode = document.getElementById('txtPrdCode');
            if (txtPrdCode || txtprice) {
                var strprice = txtprice.value;
                var strprdcode = txtPrdCode.value;
                var strprdseries = strprdcode.substring(0, 1);
                if (strprdseries == '2') {
                    if (strprice < 0) {
                        args.IsValid = false;
                    }
                    else { args.IsValid = true; }
                } else {

                    if (strprice > 0) { args.IsValid = true; }
                    else if (strprice == 0) {
                        if (txtPrdCode.value != '') {
                            if (confirm('Do you want to insert the product with zero price?')) {
                                args.IsValid = true;
                            } else {
                                args.IsValid = false;
                            }
                        }
                    }
                    else { args.IsValid = false; }
                }
            } else {
                args.IsValid = false;
            }
        }

        //Attachment
        function clearAttachment() {
            var txn_no = document.getElementById('lbltxnno').textContent;
            $.get('../../DataServices/attachment.ashx?strTxnNo=' + txn_no + '&strFileName=' + txn_no + '&Mode=Remove', function () {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: "../../DataServices/ws_TxnDoAction.asmx/DeleteTempAttachment",
                    data: "{ strTxnNo: '" + txn_no + "', strFileName: '" + txn_no + "'}",
                    error: function (XMLHttpRequest, textStatus, errorThrown) { alert(JSON.parse(XMLHttpRequest.responseText).Message); },
                    success: function (result) {
                        //LoadUploaded();
                    }
                });
            })
        }

        function LoadAttachment() {
            if ($('.ajax-upload-dragdrop').hasClass("ajax-upload-dragdrop")) {
                bootbox.dialog({
                    title: 'Attachment',
                    message: $('#AttachmentForm'),
                    show: false
                })
            .on('shown.bs.modal', function () {
                $('#AttachmentForm').show()
            })
            .on('hide.bs.modal', function (e) {
                $('#AttachmentForm').hide().appendTo('body');
            })
            .modal('show');

            }
            else {
                LoadUpload();
                LoadUploaded();
                bootbox.dialog({
                    title: 'Attachment',
                    message: $('#AttachmentForm'),
                    show: false
                })
                     .on('shown.bs.modal', function () {
                         $('#AttachmentForm').show()
                     })
                     .on('hide.bs.modal', function (e) {
                         $('#AttachmentForm').hide().appendTo('body');
                     })
                     .modal('show');
            }
        }

        //To avoid caching in IE//
        $.ajaxSetup({
            cache: false
        });

        function LoadUpload() {
            txn_no = document.getElementById('lbltxnno').innerHTML;
            $("#fileuploader").uploadFile({
                url: "../../DataServices/attachment.ashx?strTxnNo=" + txn_no + "&Mode=Upload",
                allowedTypes: "jpeg,png,jpg,pdf,gif,bmp",
                fileName: "myfile",
                returnType: "json",
                maxFileSize: 1048576, //limit to 1 MB (in byte)
                multiple: true,
                showProgress: true,
                showStatusAfterSuccess: false,
                //showDelete: true,
                onSubmit: function () {
                },
                onSuccess: function (files, data, xhr) {
                    if (files != '') { var strFileName = files[0].split('/').pop().split('\\').pop(); } else { var strFileName = '' }
                    var attachment = txn_no + '_' + strFileName;
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        url: "../../DataServices/ws_TxnDoAction.asmx/UpdateTempAttachment",
                        data: "{ strTxnNo: '" + txn_no + "', strFileName: '" + attachment + "'}",
                        error: function (XMLHttpRequest, textStatus, errorThrown) { alert(JSON.parse(XMLHttpRequest.responseText).Message); },
                        success: function (result) {
                            LoadUploaded();
                        }
                    });
                },
                onError: function (files, status, errMsg, pd) {
                    alert(errMsg);
                }
            });
        }


        function LoadUploaded(callback) {
            $.get('../../DataServices/attachment.ashx?strTxnNo=' + txn_no + '&Mode=GetAttachment', function (data) {
                var objs = JSON.parse(data).files;
                //console.log(objs);
                if (objs == null) { } else {
                    $("#lstAttachment").empty();
                    if (objs.length > 0) {
                        //var table = document.createElement("table");
                        $.each(objs, function (i, value) {
                            var tr;
                            tr = '<tr style="color:black">'
                                + '<td width=5% ><span>' + parseFloat(i + 1) + '.</span></td>'
                                + '<td width=8% ><a href="#" onclick="doDeleteFile(\'' + value + '\');return false;"><img src="../../images/ico_Delete.gif" style="width: 18px;" title="Delete" /></a></td>'
                                + '<td><a style="color:black" href="#" onclick="doViewFile(\'' + value + '\');return false;">' + value.split('_')[1] + '</a></td>'
                                + '</tr>';
                            $('#lstAttachment').append(tr);
                            //var s = '<tr style="color:black">'
                            //+ '<td width=5% ><span>' + parseFloat(i + 1) + '.</span></td>'
                            //+ '<td width=8% ><a href="#"onclick="doDeleteFile(\'' + value + '\');return false;"><img src="../../images/ico_Delete.gif" style="width: 18px;" title="Delete" /></a></td>'
                            //+ '<td><a style="color:black" href="#" onclick="doViewFile(\'' + value + '\');return false;">' + value.split('_')[1] + '</a></td>'
                            //+ '</tr>';

                            //td.innerHTML = s;
                            //tr.appendChild(td);

                            //tr = $('<tr style="color:black">'
                            //    + '<td width=5% ><span>' + parseFloat(i + 1) + '.</span></td>'
                            //    + '<td width=8% ><a href="#"onclick="doDeleteFile(\'' + value + '\');return false;"><img src="../../images/ico_Delete.gif" style="width: 18px;" title="Delete" /></a></td>'
                            //    + '<td><a style="color:black" href="#" onclick="doViewFile(\'' + value + '\');return false;">' + value.split('_')[1] + '</a></td>'
                            //    + '</tr>');

                            //var tr = document.createElement("tr");
                            //tr.setAttribute("class", "test");
                            //var td = document.createElement("td");
                            //td.innerHTML = '<span>' + value + '.</span>';
                            //tr.appendChild(td);
                            //td = document.createElement("td");
                            //td.setAttribute('onclick', "doDeleteFile(" + value + ");return false;")
                            //var img = document.createElement("img");
                            //img.src = "../../images/ico_Delete.gif";
                            //td.appendChild(img);
                            //tr.appendChild(td);                           
                            //$('#lstAttachment')[0].appendChild(tr);
                        })
                    }
                }
            })
        }

        function doViewFile(file) {
            var encodedInputString = encodeURIComponent(file);
            window.location.href = "ExportImg.aspx?strFileName=" + encodedInputString;
        }

        function doDeleteFile(file) {
            $.get('../../DataServices/attachment.ashx?strTxnNo=' + txn_no + '&strFileName=' + file + '&Mode=Delete', function () {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: "../../DataServices/ws_TxnDoAction.asmx/DeleteTempAttachment",
                    data: "{ strTxnNo: '" + txn_no + "', strFileName: '" + file + "'}",
                    error: function (XMLHttpRequest, textStatus, errorThrown) { alert(JSON.parse(XMLHttpRequest.responseText).Message); },
                    success: function (result) {
                        LoadUploaded();
                    }
                });
            })
        }



    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideNavigation();changeValue(true);">

    <form id="frmTRAOrder" method="get" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">
            <Services>
                <asp:ServiceReference Path="~/DataServices/ws_TRAOrder.asmx" />
            </Services>
        </ajaxToolkit:ToolkitScriptManager>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table class="cls_form_table">
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                            <span id="message" style="display: none; float: left;" class="cls_label_err"></span>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <asp:UpdatePanel runat="server" ID="UpdateContent" RenderMode="Block" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                                <tr>
                                                    <td colspan="3" style="width: 100%">
                                                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" class="Bckgroundreport">
                                                        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                        <asp:Button runat="server" ID="btnHidden" Style="display: none;" Text="" Visible="false" />
                                                        <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                                                        <table width="99.5%">
                                                            <tr>
                                                                <td align="left">
                                                                    <%--<input type="button" runat="server" id="btnback" value="Back" onclick="ShowNavigation(); self.parent.CollapseExpand(); changeValue(false);"
                                                                        class="cls_button" style="width: 80px" />--%>
                                                                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" Width="80px" OnClientClick="ShowNavigation(); self.parent.CollapseExpand(); changeValue(false);" />
                                                                    <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" Width="80px" Enabled="true"
                                                                        OnClientClick="var agree=confirm('System will clear all current record(s)! Are you sure you want to continue?');if(agree){changeValue(false);clearAttachment();return true;}else{return false;}" />
                                                                    <asp:Button ID="btnsubmit" runat="server" Text="Preview" CssClass="cls_button" Width="80px" Visible="false"
                                                                        OnClientClick="changeValue(false);" />
                                                                    <asp:Button ID="btnKIV" runat="server" Text="KIV" CssClass="cls_button" Width="80px"
                                                                        Enabled="false" OnClientClick="var agree=confirm('Are you sure you want to KIV TRA?');if(agree){changeValue(false);return true;}else{return false;}" />
                                                                    <asp:Button ID="btnSubmit2" runat="server" Text="Submit" CssClass="cls_button" Width="80px"
                                                                        Enabled="false" ValidationGroup="TRAHdr2" OnClientClick="var agree=confirm('Are you sure you want to Submit TRA?');if(agree){changeValue(false);return true;}else{return false;}" />
                                                                    <asp:Button ID="btnTriggerDivision" runat="server" CssClass="cls_button" Enabled="true" style="display:none" />
                                                                </td>
                                                                <td align="right">
                                                                    <asp:ImageButton ID="imgfix" ImageUrl="../../images/fix.jpg" runat="server" Height="15px"
                                                                        Width="15px" OnClientClick="changeValue(false);" Visible="false" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div style="width: 99.5%; text-align: left; padding-left: 5px; border: solid 2px black; cursor: pointer"
                                                            class="GridHeader" id="Phdr">
                                                            Header Information
                                                        <asp:Label ID="lblcurrenttxnno" runat="server" Text="" CssClass="GridHeader"></asp:Label>
                                                        </div>
                                                        <asp:Panel ID="pTRAHdr" runat="server" Width="99.5%" CssClass="cls_panel_header">
                                                            <asp:Label ID="lblhdrmsg" runat="server" CssClass="cls_label_err"></asp:Label>
                                                            <table class="cls_form_table">
                                                                <tr>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Salesman Code :</span>
                                                                    </td>
                                                                    <td width="40%">
                                                                        <asp:TextBox ID="txtsalesrepCode" CssClass="cls_textbox" runat="server" MaxLength="50"
                                                                            TabIndex="1" AutoPostBack="true"></asp:TextBox>
                                                                        <asp:Button ID="btnSearchSR" runat="server" CssClass="cls_button" Text="Search" OnClick="btnSearchSR_Click"
                                                                            TabIndex="1" />
                                                                        <asp:Label ID="lblSalesrepCode" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="RFVtxtsalesrepCode" runat="server" ControlToValidate="txtsalesrepCode"
                                                                            CssClass="cls_label_err" ErrorMessage="Select a Field Force" ValidationGroup="TRAHdr"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Voucher No : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblvouchno" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                                        <asp:Label ID="lbltxnno" runat="server" Text="" CssClass="cls_label" sytle="display:none;"> </asp:Label>
                                                                        <asp:Label ID="lblvisitid" runat="server" Text="" CssClass="cls_label" sytle="display:none;"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Salesman Name : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSalesrepName" runat="server" CssClass="cls_textbox" Width="300px"
                                                                            Enabled="false" ValidationGroup="TRAHdr" MaxLength="100">
                                                                        </asp:TextBox>
                                                                        <asp:Label ID="lblSalesrepName" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Sales Office :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblsalesArea" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Ship To Code : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtcustomer" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr"
                                                                            TabIndex="2" MaxLength="50" onBlur="ValidateCust()"></asp:TextBox>
                                                                        <asp:Button ID="btnSearchCust" runat="server" CssClass="cls_button" Text="Search"
                                                                            TabIndex="2" ValidationGroup="TRAHdr" CausesValidation="false" OnClick="btnSearchCust_Click" />
                                                                        <asp:RequiredFieldValidator ID="rfcustomerv" ValidationGroup="TRAHdr" CssClass="cls_label_err"
                                                                            ControlToValidate="txtcustomer" runat="server" ErrorMessage="Select a customer"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:Label ID="lblcustomer" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Collected By : </span>
                                                                    </td>
                                                                    <td width="40%">
                                                                        <asp:DropDownList ID="ddlCollBy" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr"
                                                                            TabIndex="5">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvddlCollBy" runat="server" ControlToValidate="ddlCollBy"
                                                                            CssClass="cls_label_err" ErrorMessage="Select coll by" ValidationGroup="TRAHdr"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvddlCollBy2" runat="server" ControlToValidate="ddlCollBy"
                                                                            CssClass="cls_label_err" ErrorMessage="Select coll by" ValidationGroup="TRAHdr2"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Ship To Name : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="cls_textbox" Width="300px"
                                                                            Enabled="false" ValidationGroup="TRAHdr" MaxLength="100">
                                                                        </asp:TextBox>
                                                                        <asp:Label ID="lblcustomername" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Header Reason : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlmreason" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr"
                                                                            TabIndex="6">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvddlmreason" runat="server" ControlToValidate="ddlmreason"
                                                                            CssClass="cls_label_err" ErrorMessage="Select reason" ValidationGroup="TRAHdr"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Payer Code :</span>
                                                                    </td>
                                                                    <td width="40%">
                                                                        <asp:TextBox ID="txtPayerCode" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr"
                                                                            TabIndex="3" MaxLength="50" onBlur="ValidateCustPayer()" />
                                                                        <asp:Button ID="btnSearchCustPayer" runat="server" CssClass="cls_button" Text="Search"
                                                                            TabIndex="3" OnClick="btnSearchCustPayer_Click" />
                                                                        <asp:Label ID="lblPayerCode" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                        <asp:RequiredFieldValidator ID="rfvtxtPayerCode" ValidationGroup="TRAHdr" CssClass="cls_label_err"
                                                                            ControlToValidate="txtPayerCode" runat="server" ErrorMessage="Select a payer"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Storage Location: </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlstorage" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr"
                                                                            TabIndex="7">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvddlstorage" runat="server" ControlToValidate="ddlstorage"
                                                                            CssClass="cls_label_err" ErrorMessage="Select storage" ValidationGroup="TRAHdr" Enabled="true"
                                                                            Display="Dynamic">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Payer Name : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtpayername" runat="server" CssClass="cls_textbox" Width="300px"
                                                                            Enabled="false" ValidationGroup="TRAHdr" MaxLength="100" />
                                                                        <asp:Label ID="lblpayerName" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Total No. of Carton : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtcarton" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr"
                                                                            TabIndex="8" Text="0" MaxLength="4"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="revcarton" runat="server" ErrorMessage="Enter numeric only"
                                                                            ValidationExpression="^\d+(\.\d\d)?$" CssClass="cls_label_err" ControlToValidate="txtcarton"
                                                                            ValidationGroup="TRAHdr" Display="Dynamic">
                                                                        </asp:RegularExpressionValidator>
                                                                        <asp:RegularExpressionValidator ID="revcarton2" runat="server" ErrorMessage="Enter numeric only"
                                                                            ValidationExpression="^\d+(\.\d\d)?$" CssClass="cls_label_err" ControlToValidate="txtcarton"
                                                                            ValidationGroup="TRAHdr2" Display="Dynamic">
                                                                        </asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvcarton" ControlToValidate="txtcarton" runat="server"
                                                                            ErrorMessage="Enter No. of Cartons to be collected." CssClass="cls_label_err" Enabled="true"
                                                                            ValidationGroup="TRAHdr" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvcarton2" ControlToValidate="txtcarton" runat="server"
                                                                            ErrorMessage="Enter No. of Cartons to be collected." CssClass="cls_label_err" Enabled="true"
                                                                            ValidationGroup="TRAHdr2" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:CompareValidator ID="cvtxtcarton" ControlToValidate="txtcarton" Operator="GreaterThan"
                                                                            ValueToCompare="0" Type="Currency" runat="server" ErrorMessage="Value cannot be 0" Enabled="true"
                                                                            ValidationGroup="TRAHdr" CssClass="cls_label_err" Display="Dynamic"></asp:CompareValidator>
                                                                        <asp:CompareValidator ID="cvtxtcarton2" ControlToValidate="txtcarton" Operator="GreaterThan"
                                                                            ValueToCompare="0" Type="Currency" runat="server" ErrorMessage="Value cannot be 0" Enabled="true"
                                                                            ValidationGroup="TRAHdr2" CssClass="cls_label_err" Display="Dynamic"></asp:CompareValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Sold To Name:</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtsoldto" runat="server" CssClass="cls_textbox" Width="300px" Enabled="false"
                                                                            ValidationGroup="TRAHdr" MaxLength="100" />
                                                                        <asp:Label ID="lblsoldto" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Refer No : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtrefNo" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr"
                                                                            TabIndex="9" MaxLength="10"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvtxtrefNo" ControlToValidate="txtrefNo" runat="server"
                                                                            ErrorMessage="Ref No. is required." CssClass="cls_label_err" ValidationGroup="TRAHdr"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvtxtrefNo2" ControlToValidate="txtrefNo" runat="server"
                                                                            ErrorMessage="Ref No. is required." CssClass="cls_label_err" ValidationGroup="TRAHdr2"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Contact Code : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlcontact" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr"
                                                                            TabIndex="4">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="lblcontact" runat="server" Text="" CssClass="cls_label" Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Remarks : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtremarks" runat="server" CssClass="cls_textbox" Width="300px"
                                                                            TabIndex="10" ValidationGroup="TRAHdr" MaxLength="150"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">SAP Billing Number :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtSAPBillingNo" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr"
                                                                            TabIndex="10" MaxLength="10"></asp:TextBox>

                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Total :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbltotal" runat="server" Text="0" CssClass="cls_label_header" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblCustDivision" runat="server" CssClass="cls_label_header" Visible="false" Text="Division"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlDivision" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TraHdr" Visible="false"></asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvddlDivision" runat="server" ControlToValidate="ddlDivision"
                                                                            CssClass="cls_label_err" ErrorMessage="Select Division" ValidationGroup="TRAHdr" Enabled="false"
                                                                            Display="Dynamic">
                                                                        </asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <span id="lblAttachment" runat="server" class="cls_label_header" style="display:none">Attachment :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Button ID="btnAttachment" runat="server" CssClass="cls_button" Text="Upload File" TabIndex="11" style="display:none" />
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                            <br />
                                                        </asp:Panel>
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="cpehdr" runat="server" CollapseControlID="Phdr"
                                                            ExpandControlID="Phdr" TargetControlID="pTRAHdr">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <div style="width: 99.5%; text-align: left; padding-left: 5px; border: solid 2px black; cursor: pointer"
                                                            class="GridHeader" id="Pdtl">
                                                            Detail Information
                                                        <asp:Label ID="lbllineno" runat="server" Text="0" Visible="true" CssClass="cls_label" />
                                                        </div>
                                                        <asp:Panel ID="pTRADtl" runat="server" Width="99.5%" CssClass="cls_panel_header">
                                                            <asp:Label ID="lbldtlmsg" runat="server" CssClass="cls_label_err"></asp:Label>
                                                            <table class="cls_form_table">
                                                                <tr>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Product Code : </span>
                                                                    </td>
                                                                    <td width="40%">
                                                                        <asp:TextBox ID="txtPrdCode" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr" TabIndex="11"
                                                                            MaxLength="50" onBlur="ValidateProduct()"></asp:TextBox>
                                                                        <asp:Button ID="btnSearchPrd" runat="server" CssClass="cls_button" Text="Search" TabIndex="11"
                                                                            OnClick="btnSearchPrd_Click" />
                                                                        <asp:RequiredFieldValidator ID="rfvprdcode" ControlToValidate="txtPrdCode" runat="server"
                                                                            ErrorMessage="Select a product" CssClass="cls_label_err" ValidationGroup="TRAHdr"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td width="10%">
                                                                        <span class="cls_label_header">Batch No : </span>
                                                                    </td>
                                                                    <td width="40%">
                                                                        <asp:TextBox ID="txtbno" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr" TabIndex="14"
                                                                            MaxLength="20"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Product Name : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPrdName" runat="server" CssClass="cls_textbox" Width="300px"
                                                                            MaxLength="150" Enabled="false" ValidationGroup="TRAHdr"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Exp. Date : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtexpdate" runat="server" CssClass="cls_textbox" ValidationGroup="TRAHdr" TabIndex="15"></asp:TextBox>
                                                                        <img id="img_cal" src="../../images/ico_Calendar.gif" height="18px" width="18px" alt="Select"
                                                                            style="cursor: pointer" />
                                                                        <ajaxToolkit:CalendarExtender ID="ceexpdate" TargetControlID="txtexpdate" runat="server"
                                                                            Animated="false" Format="yyyy-MM-dd" PopupPosition="TopLeft" PopupButtonID="img_cal">
                                                                        </ajaxToolkit:CalendarExtender>
                                                                        <asp:CustomValidator ID="rfvFormatDate" runat="server" Display="Dynamic" ControlToValidate="txtexpdate"
                                                                            CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="TRAHdr" />
                                                                        <asp:CompareValidator ID="rfvCheckDataType" runat="server" CssClass="cls_validator"
                                                                            ControlToValidate="txtexpdate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                                            ErrorMessage=" Invalid Date!" ValidationGroup="TRAHdr" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">UOM : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddluom" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr" TabIndex="12"
                                                                            OnChange="UOMPrice()">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="lbluomload" runat="server" Text="" CssClass="cls_label" />
                                                                        <div style="display: none;">
                                                                            <asp:TextBox ID="txtselecteduom" runat="server"></asp:TextBox>
                                                                        </div>
                                                                        <asp:RequiredFieldValidator ID="rfvddluom" runat="server" ControlToValidate="txtselecteduom"
                                                                            CssClass="cls_label_err" ValidationGroup="TRAHdr" ErrorMessage="Select a UOM"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Quantity : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtqty" runat="server" CssClass="cls_textbox" onChange='CalculateAmt();' TabIndex="16"
                                                                            Text="0" ValidationGroup="TRAHdr" MaxLength="8"></asp:TextBox>
                                                                        <asp:CompareValidator ID="rfvqyt" runat="server" ErrorMessage="Enter numeric only"
                                                                            Operator="DataTypeCheck" Type="Double" CssClass="cls_label_err" ControlToValidate="txtqty"
                                                                            ValidationGroup="TRAHdr" Display="Dynamic"></asp:CompareValidator>
                                                                        <asp:RequiredFieldValidator ID="rfvtxtqty" ControlToValidate="txtqty" runat="server"
                                                                            ErrorMessage="Enter quantity" CssClass="cls_label_err" ValidationGroup="TRAHdr"
                                                                            Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <%--  <asp:CompareValidator ID="cvtxtqty2" ControlToValidate="txtqty" Operator="GreaterThan"
                                                                            Display="Dynamic" ValueToCompare="0" Type="Double" runat="server" ErrorMessage=" Value cannot be 0"
                                                                           ValidationGroup="TRAHdr" CssClass="cls_label_err"></asp:CompareValidator>--%>
                                                                        <asp:CustomValidator ID="cvtxtqty3" runat="server" ErrorMessage=" Value cannot be 0 or less than 0"
                                                                            ClientValidationFunction="ValidateDtlQty" Display="Dynamic" ValidationGroup="TRAHdr"
                                                                            CssClass="cls_label_err">
                                                                        </asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Unit Price (<asp:Label ID="lblUnitPriceCurrency" runat="server"></asp:Label>) :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="lbluomprice" runat="server" Text="0" CssClass="cls_textbox" Enabled="false"
                                                                            ValidationGroup="TRAHdr"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Price (<asp:Label ID="lblPriceCurrency" runat="server"></asp:Label>) :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtprice" runat="server" CssClass="cls_textbox" onChange='CalculateAmt();' TabIndex="17"
                                                                            Text="0" ValidationGroup="TRAHdr" MaxLength="10"></asp:TextBox>
                                                                        <asp:CompareValidator ID="rfvprice" runat="server" ErrorMessage="Enter numeric only"
                                                                            Operator="DataTypeCheck" Type="Double" CssClass="cls_label_err" ControlToValidate="txtprice"
                                                                            ValidationGroup="TRAHdr" Display="Dynamic"></asp:CompareValidator>
                                                                        <asp:CompareValidator ID="cvlblamount" runat="server" ErrorMessage="Amount must be less then UOM Price"
                                                                            ValidationGroup="TRAHdr" CssClass="cls_label_err" ControlToValidate="txtprice"
                                                                            Display="Dynamic" ControlToCompare="lbluomprice" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                                                        <asp:CustomValidator ID="cvtxtprice" runat="server" ErrorMessage=" Value cannot be 0"
                                                                            ClientValidationFunction="ValidateDtlPrice" Display="Dynamic" ValidationGroup="TRAHdr"
                                                                            CssClass="cls_label_err">
                                                                        </asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="cls_label_header">Reason : </span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlreason" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr" TabIndex="13">
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="rfvddlreason" ControlToValidate="ddlreason" runat="server"
                                                                            Display="Dynamic" ErrorMessage="Select a reason" CssClass="cls_label_err" ValidationGroup="TRAHdr"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td>
                                                                        <span class="cls_label_header">Net Value (<asp:Label ID="lblNetValueCurrency" runat="server"></asp:Label>) :</span>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="lblamount" runat="server" Text="0" CssClass="cls_textbox" ValidationGroup="TRAHdr"
                                                                            Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<span class="cls_label_header" >Remarks : </span>--%>
                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:TextBox ID="txtdetailremarks" runat="server" CssClass="cls_textbox" Width="300px"
                                                                        ValidationGroup="TRAHdr" MaxLength="150" Visible="false"></asp:TextBox>--%>
                                                                    </td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" colspan="4">
                                                                        <asp:Button ID="btnsavedtl" runat="server" Text="Insert/Edit" CssClass="cls_button" TabIndex="18"
                                                                            ValidationGroup="TRAHdr" Width="80px" />
                                                                    </td>
                                                                </tr>
                                                                <tr></tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="cpedtl" runat="server" CollapseControlID="Pdtl"
                                                            ExpandControlID="Pdtl" TargetControlID="pTRADtl">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <div style="width: 99.5%; text-align: left; padding-left: 5px; border: solid 2px black; cursor: pointer"
                                                            class="GridHeader" id="Psum">
                                                            Added Detail Transaction Summary
                                                        </div>
                                                        <asp:Panel ID="pgridview" runat="server" CssClass="cls_panel_header" Width="99.5%">
                                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                                <ContentTemplate>
                                                                    <div style="width: 100%">
                                                                        <div style="padding-left: 10px; padding-top: 5px; padding-right: 10px; padding-bottom: 10px; float: left;">
                                                                            <asp:Label ID="lblrowcount" runat="server" CssClass="cls_label_header"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                    <br />
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                    <div style="padding-left: 10px; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; width: 98%">
                                                                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                                            Width="100%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="100%"
                                                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TXN_NO,LINE_NO"
                                                                            onClientClick="changeValue(false);">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </ccGV:clsGridView>
                                                                    </div>
                                                                </ContentTemplate>

                                                            </asp:UpdatePanel>
                                                        </asp:Panel>
                                                        <ajaxToolkit:CollapsiblePanelExtender ID="cpegridview" runat="server" CollapseControlID="Psum"
                                                            ExpandControlID="Psum" TargetControlID="pgridview">
                                                        </ajaxToolkit:CollapsiblePanelExtender>
                                                        <div style="width: 99.5%;">
                                                            <span style="float: left;">
                                                                <customToolkit:wuc_PrdSearch ID="wuc_PrdSearch" Title="Product Search" runat="server" />
                                                            </span><span style="float: left;">
                                                                <customToolkit:wuc_CustSearch ID="wuc_CustSearch" Title="Customer Search" runat="server" />
                                                            </span><span style="float: left;">
                                                                <customToolkit:Wuc_CustPayersearch ID="Wuc_CustPayersearch" Title="Payer Search"
                                                                    runat="server" />
                                                            </span><span style="float: left;">
                                                                <customToolkit:Wuc_CustPayersearch2 ID="Wuc_CustPayersearch2" Title="ShipTo Search"
                                                                    runat="server" />
                                                            </span><span style="float: left;">
                                                                <customToolkit:wuc_SRSearch ID="wuc_SRSearch" Title="Field Force Search" runat="server" />
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    </form>

    <form id="AttachmentForm" method="post" class="form-horizontal" style="display: none;">
        <div id="fileuploader" style="padding: 10px">Upload</div>
        <br />

        <table cellpadding="0" cellspacing="0" style="height: 20px; width: 99.5%; padding-left: 5px; border: solid 2px black; cursor: pointer" class="GridHeader">
            <tr>
                <th>
                    <asp:Label ID="lblUploadedFile" runat="server" Text="Uploaded Files" />
                </th>
            </tr>
            <tr>
                <td>
                    <div>
                        <table id="lstAttachment" width="99%;" style="background-color: white;">
                            <%--<tr></tr>--%>
                        </table>
                    </div>
                </td>
            </tr>
        </table>

    </form>
</body>

</html>
