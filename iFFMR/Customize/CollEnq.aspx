<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CollEnq.aspx.vb" Inherits="iFFMR_Customize_CollEnq" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customControl" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Collection Enquiry</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCollEnqGrid" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="TSM1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customControl:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true">
                                    </customControl:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" style="width: 95%;">
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="false" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="475" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
