'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	18/10/2006
'	Purpose	    :	Date User Control Type 
'                   0 - Date
'                   1 - Datetime
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |  19/10/2006   | Alex Chia	        |add format string, control type|
'To Use this control, need to specific the RequiredValidationGroup is the MUST.
'To let the validation fire the event, kindly add the page.isvalid or page.validate in the control page.

' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On

Imports System.Data

Partial Class include_wuc_txtDateDynamic
    Inherits System.Web.UI.UserControl

    Private _strValidationGroup As String
    Private _blnRequiredValidation As Boolean = True
    Private _strCurrentFormat As String '= "dd/MM/yyyy HH:mm:00"
    Private _strDefaultDateTimeFormatString As String = "yyyy-MM-dd HH:mm:00"
    Private _strDefaultDateFormatString As String = "yyyy-MM-dd"
    Private _ControlType As DateTimeControlType = DateTimeControlType.DateAndTime 'default
    'Private _strRfvDate_ErrMsg As String = "Date cannot be blank !"

    Enum DateTimeControlType
        DateAndTime = 0
        DateOnly = 1
    End Enum

    Public Property Text() As String
        Get
            Return txtDate.Text
        End Get
        Set(ByVal Value As String)
            Try
                Dim dtm As DateTime
                If Not String.IsNullOrEmpty(Value) AndAlso IsDate(Value) Then
                    dtm = DateTime.Parse(Value)
                    txtDate.Text = dtm.ToString(DateTimeFormatString)
                Else
                    txtDate.Text = ""
                End If
            Catch ex As Exception
                ExceptionMsg("wuc_txtDate.Text : " & ex.ToString)
            End Try
        End Set
    End Property

    'Public ReadOnly Property Value() As String
    '    Get

    '        Dim strDate As String
    '        'Dim strYear As String
    '        'Dim strMonth As String
    '        'Dim strDay As String
    '        'Dim strTime As String

    '        If txtDate.Text <> String.Empty Then
    '            '    strDate = Replace(txtDate.Text, " ", "")
    '            '    strDate = Replace(strDate, "/", "")
    '            '    strDate = Replace(strDate, ":", "")

    '            '    strDay = Mid(strDate, 1, 2)
    '            '    strMonth = Mid(strDate, 3, 2)
    '            '    strYear = Mid(strDate, 5, 4)

    '            '    If lblTypeID.Text = "1" Then
    '            '        strTime = Mid(strDate, 9, Len(strDate) - 8)
    '            '        strDate = strYear & strMonth & strDay & strTime
    '            '    Else
    '            '        strDate = strYear & strMonth & strDay
    '            '    End If
    '            strDate = txtDate.Text
    '        Else
    '            strDate = String.Empty
    '        End If
    '        Return strDate
    '    End Get
    'End Property

    Public ReadOnly Property FormatedDateTimeValue() As String
        Get
            Dim strFormatedDatetimeValue As String = ""
            Try
                Dim dtmSelectedDateTime As DateTime = Value
                strFormatedDatetimeValue = dtmSelectedDateTime.ToString(DateTimeFormatString)
            Catch ex As Exception
                ExceptionMsg("wuc_txtDate.Value()as Datetime : " & ex.ToString)
            End Try
            Return strFormatedDatetimeValue
        End Get
    End Property

    Public ReadOnly Property Value() As DateTime
        Get
            Dim dtmSelectedDateTime As DateTime
            Try
                With calDate.SelectedDate
                    dtmSelectedDateTime = New DateTime(.Year, .Month, .Day, ddlHour.SelectedValue, ddlMin.SelectedValue, 0)
                End With

            Catch ex As Exception
                ExceptionMsg("wuc_txtDate.Value()as Datetime : " & ex.ToString)
            End Try
            Return dtmSelectedDateTime
        End Get
    End Property

    Public Property DateTimeFormatString() As String
        Get
            If String.IsNullOrEmpty(_strCurrentFormat) Then setDefaultFormat()
            Return _strCurrentFormat
        End Get
        Set(ByVal value As String)
            _strCurrentFormat = value
        End Set
    End Property

    'Public Property Enabled() As Boolean
    '    Get
    '        Return txtDate.Enabled
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        txtDate.Enabled = Value
    '    End Set
    'End Property

    'Public Property AutoPostBack() As Boolean
    '    Get
    '        Return txtDate.AutoPostBack
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        txtDate.AutoPostBack = Value
    '    End Set
    'End Property

    Public Property ValidationErrorMessage() As String
        Get
            Return rfvDate.ErrorMessage
        End Get
        Set(ByVal value As String)
            rfvDate.ErrorMessage = value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return rfvDate.Visible
        End Get
        Set(ByVal blnVisible As Boolean)
            _blnRequiredValidation = blnVisible
            If Not rfvDate Is Nothing Then rfvDate.Visible = _blnRequiredValidation
        End Set
    End Property

    Public Property RequiredValidationGroup() As String
        Get
            Return rfvDate.ValidationGroup
        End Get
        Set(ByVal value As String)
            _strValidationGroup = value
            If Not rfvDate Is Nothing Then rfvDate.ValidationGroup = _strValidationGroup
        End Set
    End Property

    Protected Sub imgDate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDate.Click
        Try
            pnlDate.Visible = Not pnlDate.Visible

            'If pnlDate.Visible = True Then
            '    pnlDate.Visible = False
            'Else
            '    pnlDate.Visible = True
            'End If
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.imgDate_Click : " & ex.ToString)
        End Try
    End Sub

    'Public Property TypeID() As Long
    '    Get
    '        Return lblTypeID.Text
    '    End Get
    '    Set(ByVal Value As Long)
    '        lblTypeID.Text = Value
    '    End Set
    'End Property

    Public Property ControlType() As DateTimeControlType
        Get
            Return _ControlType
        End Get
        Set(ByVal value As DateTimeControlType)
            _ControlType = value
        End Set
    End Property

    Private Sub setDefaultFormat()
        Try
            'Set Default DateTime String Format
            If ControlType = DateTimeControlType.DateOnly Then
                _strCurrentFormat = _strDefaultDateFormatString
            Else
                _strCurrentFormat = _strDefaultDateTimeFormatString
            End If
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.setDefaultFormat : " & ex.ToString)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim i As Integer
            Dim strHour, strMin As String

            If String.IsNullOrEmpty(_strCurrentFormat) Then setDefaultFormat()

            ddlYear.Items.Clear()
            For i = Date.Now.AddYears(-10).Year To Date.Now.AddYears(1).Year
                ddlYear.Items.Add(i)
            Next

            'Hour
            ddlHour.Items.Clear()
            For i = 0 To 23
                If i >= 0 And i <= 9 Then
                    strHour = "0" & CStr(i)
                Else
                    strHour = CStr(i)
                End If
                ddlHour.Items.Add(strHour)
            Next

            'Min
            ddlMin.Items.Clear()
            For i = 0 To 59
                If i >= 0 And i <= 9 Then
                    strMin = "0" & CStr(i)
                Else
                    strMin = CStr(i)
                End If
                ddlMin.Items.Add(strMin)
            Next

            If ControlType = DateTimeControlType.DateAndTime Then
                pnlTime.Visible = True
                ddlHour.Enabled = True
                ddlMin.Enabled = True
                ddlHour.SelectedValue = Format(TimeOfDay, "HH")
                ddlMin.SelectedValue = Format(TimeOfDay, "mm")
            Else
                ddlHour.Enabled = False
                ddlMin.Enabled = False
                pnlTime.Visible = False
            End If

            'set Defalult Display Value
            If Trim(txtDate.Text) = String.Empty Then
                'Assign the month and year to date
                ddlMonth.SelectedValue = Month(Now)
                ddlYear.SelectedValue = Year(Now)
                calDate.SelectedDates.Clear()
                calDate.SelectedDate = Now.Date 'New Date(ddlYear.SelectedValue, ddlMonth.SelectedValue, Day(Now))
                calDate.VisibleDate = Now.Date
            Else

                Dim dtDefault As DateTime
                If DateTime.TryParse(txtDate.Text, dtDefault) = True Then
                    ddlYear.SelectedValue = dtDefault.Year
                    ddlMonth.SelectedValue = dtDefault.Month
                    calDate.SelectedDates.Clear()
                    calDate.SelectedDate = dtDefault.Date
                    calDate.VisibleDate = dtDefault.Date
                End If

            End If
            If IsPostBack = False Then
                'Set ValidationGoupFiled is exist
                If Not String.IsNullOrEmpty(_strValidationGroup) Then
                    rfvDate.ValidationGroup = _strValidationGroup
                End If

                'Set Validation Visibility is Exist
                rfvDate.Visible = _blnRequiredValidation

                'Year

            End If
            btnCalendar.Text = "Selected : " & FormatedDateTimeValue
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.Page_UnLoad : " & ex.ToString)
        End Try
    End Sub

    Public Sub calDate_DayRender(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles calDate.DayRender
        Dim lblValue As Label = New Label
        Dim calDate As Date
        Dim strDate As String

        Try
            If e.Day.IsOtherMonth Then
                e.Cell.Controls.Clear()
            Else
                calDate = e.Day.Date
                strDate = calDate.ToString(DateTimeFormatString)
            End If

        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.calDate_DayRender : " & ex.ToString)
        End Try
    End Sub

    Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Try
            'calDate.SelectedDates.Clear()
            calDate.VisibleDate = New Date(ddlYear.SelectedValue, ddlMonth.SelectedValue, "01")
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.ddlMonth_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Private Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            'calDate.SelectedDates.Clear()
            calDate.VisibleDate = New Date(ddlYear.SelectedValue, ddlMonth.SelectedValue, "01")
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.ddlYear_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlHour_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlHour.SelectedIndexChanged
        Try
            btnCalendar.Text = "Selected : " & FormatedDateTimeValue 'calDate.SelectedDate.ToString(DateTimeFormatString) & " " & ddlHour.SelectedValue & ":" & ddlMin.SelectedValue & ":00"
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.ddlHour_SelectionChanged : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlMin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMin.SelectedIndexChanged
        Try
            btnCalendar.Text = "Selected : " & FormatedDateTimeValue 'calDate.SelectedDate.ToString(DateTimeFormatString) & " " & ddlHour.SelectedValue & ":" & ddlMin.SelectedValue & ":00"
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.ddlMin_SelectionChanged : " & ex.ToString)
        End Try
    End Sub

    Protected Sub calDate_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles calDate.SelectionChanged
        Try
            ddlYear.SelectedValue = calDate.SelectedDate.Year
            ddlMonth.SelectedValue = calDate.SelectedDate.Month
            btnCalendar.Text = "Selected : " & FormatedDateTimeValue 'calDate.SelectedDate.ToString(DateTimeFormatString) & " " & ddlHour.SelectedValue & ":" & ddlMin.SelectedValue & ":00"
            txtDate.Text = FormatedDateTimeValue
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.calDate_SelectionChanged : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCalendar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalendar.Click
        Try
            pnlDate.Visible = False
            txtDate.Text = FormatedDateTimeValue 'Mid(Trim(btnCalendar.Text), 12, Len(Trim(btnCalendar.Text)))
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.btnCalendar_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Sub ExceptionMsg
    ' Purpose	:	This function will add the error message and pop up the message viewer
    ' Parameters:	[in]  : 
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        'Call error Message Viewer
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg
            'ltlMsg.Text = "alert('" & strMsg & "');"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>alert('" & strMsg & "');</script>")

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing
        Catch ex As Exception

        Finally
        End Try
    End Sub

    Protected Sub calDate_VisibleMonthChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MonthChangedEventArgs) Handles calDate.VisibleMonthChanged
        Try
            ddlYear.SelectedValue = calDate.VisibleDate.Year
            ddlMonth.SelectedValue = calDate.VisibleDate.Month
        Catch ex As Exception
            ExceptionMsg("wuc_txtDate.calDate_VisibleMonthChanged : " & ex.ToString)
        End Try
    End Sub
End Class

