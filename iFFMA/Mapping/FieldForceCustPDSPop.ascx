<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FieldForceCustPDSPop.ascx.vb" Inherits="iFFMA_Mapping_FieldForceCustPDSPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:92%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:right; width:8%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                    
                    <!-- Begin Customized Content -->
                    <asp:Panel ID="pnlEditMode" runat="server">
                        <span style="float:left; width:30%; padding-top:2px;" class="cls_label_header">Product Specialty</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_err">*</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:60%;">
                            <%--<asp:DropDownList ID="ddlPrdSpecialty" runat="server" CssClass="cls_dropdownlist" />--%>
                            <asp:TextBox ID="txtPrdSpecialty" runat="server" CssClass="cls_textbox" MaxLength="100" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:30%; padding-top:2px;" class="cls_label_header">Priority</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_err">*</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:60%;">
                            <asp:TextBox ID="txtPriority" runat="server" CssClass="cls_textbox" MaxLength="50" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:100%; padding-top:10px; padding-bottom:10px">
                            <center>
                                <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" />
                                <%--<asp:Button ID="btnCancel" runat="server" CssClass="cls_button" Text="Cancel" />--%>
                            </center>
                        </span>
                    </asp:Panel>
                    
                    <asp:Panel ID="pnlViewMode" runat="server" Visible="false">
                        <span style="float:left; width:30%; padding-top:2px;" class="cls_label_header">Product Specialty</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:60%;">
                            <asp:Label ID="lblPrdSpecialty" runat="server" CssClass="cls_label" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:30%; padding-top:2px;" class="cls_label_header">Priority</span>
                        <span style="float:left; width:2%; padding-top:2px;" class="cls_label_header">:</span>
                        <span style="float:left; width:60%;">
                            <asp:Label ID="lblPriority" runat="server" CssClass="cls_label" />
                        </span>
                        <br />
                        
                        <span style="float:left; width:100%; padding-top:10px; padding-bottom:10px">
                            <center>
                                <%--<asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" />--%>
                            </center>
                        </span>
                    </asp:Panel>
                    
                    <!-- End Customized Content -->
                    <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdSalesrepCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdContCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdType" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>