﻿Imports System.Data


Partial Class iFFMA_TRA_TRAList_TRAList
    Inherits System.Web.UI.Page

    Private intPageSize As Integer
    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Public Property dtTRAList() As DataTable
        Get
            Return ViewState("SAPList")
        End Get
        Set(ByVal value As DataTable)
            ViewState("SAPList") = value
        End Set
    End Property

    Public Property TxnNo() As String
        Get
            Return Session("_TxnNo")
        End Get
        Set(ByVal value As String)
            Session("_TxnNo") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("UserID") = "" Then
                Dim strScript As String = ""
                strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
            End If

            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "Goods Return List"
                .DataBind()
                .Visible = True
            End With

            ''Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then

                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

                GetTeam()
                LoadSearchView()
                LoadStatusDdl()
                LoadDefaultCalendarRange()
                ' LoadDefaultPrintStatus()
                LoadPrintStatusDDL()
                'BindGrid(ViewState("SortCol"))
                If dgList.Rows.Count > 0 Then wuc_dgpaging.Visible = True Else wuc_dgpaging.Visible = False

            End If

        Catch ex As Exception
            ExceptionMsg("SAPList.Page_Load : " & ex.ToString)
        End Try
    End Sub
    'Private Sub LoadDefaultPrinStatus()
    '    txtPrintStatusFrom.Text = "All"
    '    txtPrintStatusTo.Text = "All"
    'End Sub
    Private Sub LoadDefaultCalendarRange()
        wuc_txtCalendarRange1.DateStart = Format("yyyy-MM-dd", Now())
        wuc_txtCalendarRange1.DateEnd = Format("yyyy-MM-dd", Now())
    End Sub

    Private Sub LoadSearchView()
       

        Try
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"
            Wuc_txtDate1.Text = ""
            Wuc_txtDate2.Text = ""

            lblSearchValue.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            Wuc_txtDate1.Visible = False
            Wuc_txtDate2.Visible = False

            Select Case ddlSearchType.SelectedValue
                Case "ALL"
                    btnSearch.Visible = True

                Case "TXN_DATE"
                    lblSearchValue.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    lblTo.Visible = True
                    btnSearch.Visible = True
                    Wuc_txtDate1.Visible = True
                    Wuc_txtDate2.Visible = True

                Case "TXN_STATUS"
                    lblSearchValue.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True
                    txtSearchValue.Text = ""



                Case Else
                    lblSearchValue.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "SAPList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub LoadStatusDdl()

        Dim dt As DataTable
        Dim drRow As DataRow

        Dim clsTRA As New adm_TRA.clsTRA

        With clsTRA
            .clsProperties.user_code = Trim(Session("UserCode"))
            dt = .GetTxnStatusList
        End With

        'Get Status Record
        With ddltxnStatus
            .Items.Clear()
            If dt.Rows.Count > 0 Then
                .Items.Add(New ListItem("All", "All"))
                For Each drRow In dt.Rows
                    .Items.Add(New ListItem(drRow("TXN_STATUS"), drRow("TXN_STATUS")))
                Next
            End If
        End With
        clsTRA = Nothing
        UpdatePanel.Update()
    End Sub

    Private Sub GetTeam()
        Dim clsTRA As New adm_TRA.clsTRA
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            ddlTeam.Items.Clear()
            With clsTRA
                .clsProperties.user_code = Trim(Session("UserCode"))
                dt = .GetTeamList
            End With
            If dt.Rows.Count > 0 Then
                ddlTeam.Items.Add(New ListItem("All", "ALL"))
                For Each drRow In dt.Rows
                    ddlTeam.Items.Add(New ListItem(drRow("TEAM_NAME"), drRow("TEAM_CODE")))
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadPrintStatusDDL()
        Dim clsTRA As New adm_TRA.clsTRA
        Dim dt As DataTable

        Try 
            With clsTRA
                .clsProperties.user_id = Trim(Portal.UserSession.UserID)
                dt = .GetPrintStatusDDL
            End With

            With ddlprintstatus
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "PRINT_STATUS_NAME"
                .DataValueField = "PRINT_STATUS_CODE"
                .DataBind()
            End With
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim clsTRA As New adm_TRA.clsTRA
        Dim dt As DataTable

        Try
            If intPassFlag <> 1 Or IsNothing(dtTRAList) Then
                With clsTRA
                    .clsProperties.search_type = Trim(ddlSearchType.SelectedValue)
                    .clsProperties.team_code = Trim(ddlTeam.SelectedValue)
                    .clsProperties.user_code = Trim(Session("UserCode"))
                    Select Case ddlSearchType.SelectedValue
                        Case "TXN_DATE"
                            .clsProperties.search_value = Trim(Replace(Wuc_txtDate1.Text, "*", "%"))
                            .clsProperties.search_value_1 = Trim(Replace(Wuc_txtDate2.Text, "*", "%"))
                        Case "TXN_STATUS"
                            .clsProperties.search_value = Trim(ddlList.SelectedValue)
                        Case Else
                            .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                    End Select
                    .clsProperties.strTxnStatus = ddltxnStatus.SelectedValue
                    '.clsProperties.strPrinstatusFrom = txtPrintStatusFrom.Text
                    '.clsProperties.strPrinstatusTo = txtPrintStatusTo.Text
                    .clsproperties.strPrinStatus = ddlprintstatus.SelectedValue
                    .clsProperties.strTxnStartDate = wuc_txtCalendarRange1.DateStart
                    .clsProperties.strTxnEndDate = wuc_txtCalendarRange1.DateEnd
                    dt = .GetTRAHdrList
                End With
                dtTRAList = dt
            Else
                dt = dtTRAList
            End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
                Master_Row_Count = 0
                btnPrint.Visible = False
            Else
                Master_Row_Count = dt.Rows.Count
                btnPrint.Visible = True
            End If

            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With
            If dgList.Rows.Count > 0 Then wuc_dgpaging.Visible = True Else wuc_dgpaging.Visible = False

    

        Catch ex As Exception
            ExceptionMsg("SAPList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            clsTRA = Nothing
        End Try
    End Sub

#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "dgList"


    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim objSAP As mst_SAP.clsSAP
        Dim intCurrentPage As Integer

        Try
            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim strTxn_no As String = dgList.DataKeys(index).Item("TXN_NO")

            Select Case e.CommandName

                Case "Approve"
                    If strTxn_no = "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                        Exit Sub
                    Else
                        Dim clsTRA = New adm_TRA.clsTRA
                        With clsTRA
                            .clsProperties.txn_no = strTxn_no
                            .clsProperties.txn_status = "A"
                            .clsProperties.user_code = Session("UserCode")
                            .UpdTRAStatus()
                        End With

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn No. " & strTxn_no & " has approved.');", True)
                    End If


                Case "Print"

                    If strTxn_no = "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                        Exit Sub
                    Else
                        PrintReport(strTxn_no)
                    End If

                Case "Reject"

                    If strTxn_no = "" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Invalid Txn No.');", True)
                        Exit Sub
                    Else
                        Dim clsTRA = New adm_TRA.clsTRA
                        With clsTRA
                            .clsProperties.txn_no = strTxn_no
                            .clsProperties.txn_status = "R"
                            .clsProperties.user_code = Session("UserCode")
                            .UpdTRAStatus()
                        End With

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Txn No. " & strTxn_no & " has rejected.');", True)
                    End If


            End Select

            intCurrentPage = wuc_dgpaging.CurrentPageIndex
            dgList.PageIndex = 0

            BindGrid(ViewState("SortCol"))

            If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            Else
                wuc_dgpaging.CurrentPageIndex = intCurrentPage
            End If
            wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            BindGrid(ViewState("SortCol"), 1)

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowCommand : " + ex.ToString)
        Finally
            objSAP = Nothing
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated

        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim strTxnNo = dgList.DataKeys(e.Row.RowIndex).Item("TXN_NO")

                Dim str As String
                str = "parent.document.getElementById('DetailBarIframe').src='../../Admin/TRA/TRAList/TRADtl.aspx?TXN_NO=" + strTxnNo + "';"
                e.Row.Cells(6).Attributes.Add("onclick", str)
                
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_RowDataBound
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim lblStatus As Label
        Dim imgApprove, imgReject, imgPrint As ImageButton
        Dim chkSelection As CheckBox

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                If Master_Row_Count > 0 Then
                    If e.Row.RowIndex >= 0 Then
                        drv = CType(e.Row.DataItem, DataRowView)

                        'Status
                        lblStatus = New Label
                        imgApprove = New ImageButton
                        imgReject = New ImageButton
                        imgPrint = New ImageButton
                        chkSelection = New CheckBox

                        imgApprove = CType(e.Row.FindControl("imgApprove"), ImageButton)
                        imgReject = CType(e.Row.FindControl("imgReject"), ImageButton)
                        imgPrint = CType(e.Row.FindControl("imgPrint"), ImageButton)
                        chkSelection = CType(e.Row.FindControl("chkSelection"), CheckBox)

                        Dim strTxnStatus As String
                        strTxnStatus = drv("txn_status")

                        If strTxnStatus.ToUpper = "P" Then

                            imgApprove.Visible = True
                            imgReject.Visible = True
                            imgPrint.Visible = False
                            chkSelection.Visible = False
                        ElseIf strTxnStatus.ToUpper = "A" Then

                            imgApprove.Visible = False
                            imgReject.Visible = False
                            imgPrint.Visible = True
                            chkSelection.Visible = True
                        ElseIf strTxnStatus.ToUpper = "R" Then

                            imgApprove.Visible = False
                            imgReject.Visible = False
                            imgPrint.Visible = False
                            chkSelection.Visible = False
                        Else

                            imgApprove.Visible = False
                            imgReject.Visible = False
                            imgPrint.Visible = False
                            chkSelection.Visible = False
                        End If

                    End If
                End If
               

            End If

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowDataBound : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "Event Handler"

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If Page.IsValid Then
                dgList.PageIndex = 0
                BindGrid(ViewState("SortCol"))
                UpdateDatagrid.Update()
            End If


        Catch ex As Exception
            lblErr.Text = "SAPList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            LoadSearchView()

        Catch ex As Exception
            lblErr.Text = "SAPList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        TxnNo = GetBatchTxnNo()
        PrintReportBatch()
    End Sub

#End Region

    Private Function GetBatchTxnNo() As String

        'Try
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList

        If Master_Row_Count > 0 Then
            Dim i As Integer = 0
            Dim strTxnNo As String


            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows

                DK = dgList.DataKeys(i)
                If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                    Dim chkSelection As CheckBox = CType(dgList.Rows(i).FindControl("chkSelection"), CheckBox)

                    If chkSelection.Checked = True Then
                        strTxnNo = Trim(dgList.DataKeys(i).Item("TXN_NO"))
                        sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(strTxnNo) & "'")
                        aryList.Add(Trim(strTxnNo))

                    End If

                End If

                i += 1
            Next
            Return sbString.ToString
        Else
            Return ""
        End If

        'Catch ex As Exception
        '    ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)

        'End Try
    End Function

    Private Sub PrintReport(ByVal strTxnNo As String)
        'Dim StrScript As String
        'StrScript = "document.getElementById('PrintBarIframe').src='../../Admin/TRA/TRAList/TRAPrint.aspx?TXN_NO=" + strTxnNo + "';"

        TxnNo = strTxnNo
        PrintReportBatch()

        'StrScript = "window.open(href='../../../Admin/TRA/TRAList/TRAPrint.aspx?TXN_NO=" + strTxnNo + "','Monitor','width=450,height=200,resizable=yes');"
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "OpenUrl", StrScript, True)
    End Sub
   

    Private Sub PrintReportBatch()
        Dim StrScript As String
        'StrScript = "document.getElementById('PrintBarIframe').src='../../Admin/TRA/TRAList/TRAPrint.aspx?TXN_NO=" + strTxnNo + "';"
        StrScript = "window.open(href='../../../Admin/TRA/TRAList/TRAPrintBatch.aspx','Monitor','width=450,height=200,resizable=yes');"
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "OpenUrl", StrScript, True)
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class


