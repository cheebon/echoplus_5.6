﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DataFileList_V2.aspx.vb"
    Inherits="iFFMR_DataFiles_DataFileList_V2" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Files List</title>
    <link rel="stylesheet" href="../../include/DKSH.css">
    <!--#include File="~/include/commonutil.js"-->
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmDataFilelistV2" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <fieldset class="" style="width: 98%;">
        <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
        <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
        <div class="div_report">
            <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                <tr class="Bckgroundreport">
                    <td valign="top" align="center">
                        <div class="div_report">
                            <div class="div_normal">
                                <span class="cls_label_header">Data File Name : </span>
                                <asp:TextBox ID="txtDataFileName" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                &nbsp; &nbsp;
                                <asp:Button ID="btnSearch" CssClass="cls_button" runat="server" Text="Search" ValidationGroup="Search" />
                                <asp:RequiredFieldValidator ID="rfvRequireField" runat="server" ControlToValidate="txtDataFileName"
                                    ErrorMessage="Please key in value to filter" Display="Dynamic" CssClass="cls_validator"
                                    ValidationGroup="Search" />
                            </div>
                            <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                Width="98%" FreezeHeader="True" GridHeight="100%" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="ID" RowHighlightColor="AntiqueWhite">
                                <Columns>
                                    <asp:TemplateField HeaderText="Data File">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDataFile" runat="server" Height="30px" Width="30px" CommandName="DATA"
                                                CommandArgument="<%# container.DataItemIndex %>" ImageUrl="~/images/Ico_Excel_Data_X.gif" />
                                            <%--<asp:ImageButton ID="imgDataFile" runat="server" Height="30px" Width="30px" CommandName="DATA"
                                    ImageUrl="~/images/Ico_Excel_Data_X.gif" />--%>
                                            <br />
                                            <b>Last Updated:</b><%# DataBinder.Eval(Container.DataItem, "DATA_FILE_DATE") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pivot File">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgPivotFile" runat="server" ImageUrl="~/images/Ico_Excel_Pivot_X.gif"
                                                CommandName="PIVOT" Height="30px" Width="30px"></asp:ImageButton>
                                            <br />
                                            <b>Last Updated:</b><%#DataBinder.Eval(Container.DataItem, "PIVOT_FILE_DATE")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="REPORT_NAME" HeaderText="Report Name" ReadOnly="True"
                                        SortExpression="REPORT_NAME">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="REPORT_DESC" HeaderText="Description" ReadOnly="True"
                                        SortExpression="REPORT_DESC">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle CssClass="GridFooter" />
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="GridAlternate" />
                                <RowStyle CssClass="GridNormal" />
                            </ccGV:clsGridView>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>
    </form>
</body>
</html>
