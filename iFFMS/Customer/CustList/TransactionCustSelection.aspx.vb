Imports System.Data

Partial Class TransactionCustSelection
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "TransactionCustSelection"
        End Get
    End Property
    Public Property MaxBackDate() As Integer
        Get
            If ViewState("intMaxBackDate") IsNot Nothing Then
                Return ViewState("intMaxBackDate")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            Viewstate("intMaxBackDate") = value
        End Set
    End Property
    Public Property CurrentMthBackDate() As Integer
        Get
            If ViewState("intCurrentMthBackDate") IsNot Nothing Then
                Return ViewState("intCurrentMthBackDate")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("intCurrentMthBackDate") = value
        End Set
    End Property
    Public Property BackDateEnabled() As Boolean
        Get
            If ViewState("blnBackDateEnabled") IsNot Nothing Then
                Return ViewState("blnBackDateEnabled")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("blnBackDateEnabled") = value
        End Set
    End Property

    Public Property DefaultBackDate() As String
        Get
            If ViewState("blnBackDateEnabled") IsNot Nothing Then
                Return ViewState("strdefaultbackdate")
            Else
                Return DateTime.Now.ToString("yyyy-MM-dd")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("strdefaultbackdate") = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            'Call Header
            With wuc_lblheader
                .Title = "Contact List By Customer"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then

                If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

                GetVisitAdmConfig()

                TimerControl1.Enabled = True
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            End If


            lblDateValidation.Text = ""
            UpdateCallDate.Update()

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub
    Private Sub GetVisitAdmConfig()
        Dim dt As DataTable
        Try
            Dim objConfig = New txn_WebActy.clsCommon

            dt = objConfig.GetVisitAdmConfig("VISIT_BACKDATE", Session("UserID"))

            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("VALUE").ToString = "1" Then
                        pnlCallDate.Visible = True
                        BackDateEnabled = True
                        MaxBackDate = Portal.Util.GetValue(Of String)(dt.Rows(0)("VALUE2").ToString)
                        CurrentMthBackDate = Portal.Util.GetValue(Of String)(dt.Rows(0)("VALUE4").ToString)
                    Else
                        pnlCallDate.Visible = False
                        BackDateEnabled = False
                        MaxBackDate = 0
                    End If
                Else
                    pnlCallDate.Visible = False
                    BackDateEnabled = False
                    MaxBackDate = 0
                End If
            Else
                pnlCallDate.Visible = False
                BackDateEnabled = False
                MaxBackDate = 0
            End If

            GetDefaultBackDate()
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetVisitAdmConfig : " & ex.ToString)
        End Try
    End Sub
    Private Sub GetDefaultBackDate()
        Dim dt As DataTable

        If BackDateEnabled Then
            Dim objConfig = New txn_WebActy.clsCommon

            dt = objConfig.GetDefaultDate

            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    DefaultBackDate = dt.Rows(0)("DEFAULTBACKDATE").ToString
                Else
                    DefaultBackDate = DateTime.Now.ToString("yyyy-MM-dd")
                End If
            Else
                DefaultBackDate = DateTime.Now.ToString("yyyy-MM-dd")
            End If

            txtCallDate.Text = DefaultBackDate
        Else
            DefaultBackDate = DateTime.Now.ToString("yyyy-MM-dd")
        End If
    End Sub
    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Sub onpageload()
        Dim strSalesmanCode, strcustcode As String
        'Put user code to initialize the page here
        Try
            strcustcode = Request.QueryString("custcode")
            Session("customercode") = strcustcode
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            bindGridView(strSalesmanCode, strcustcode)
            'transactionNothing()
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_Customer.clsCustInfo

        Try
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                GridView1.DataKeyNames = New String() {"cont_acc_code"}
                GridView1.DataSource = dt
                GridView1.DataBind()
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                GridView1.DataSource = dtnew
                GridView1.DataBind()
                GridView1.Columns(6).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub gridview_Selected(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand

        Try

            If e.CommandName = "funny" Then

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".redirect : " & ex.ToString)
        End Try
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim strContactCode As String
                Dim strSalesmanCode, strCustCOde As String

                strSalesmanCode = Trim(Session("dflsalesrepcode"))
                strCustCOde = Trim(Session("customercode"))
                strContactCode = GridView1.DataKeys(e.Row.RowIndex).Item("cont_acc_code")
                Session("contactcode") = strContactCode

                Dim StrScript As String
                StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMenu.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "&visitdate=" + Now().ToString("yyyy-MM-dd") + "&visittime=" + Now().ToString("HH:mm") + "';"
                Dim strAction As String
                strAction = "ResizeFrameWidth('TopBar','100%');HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar'); ResizeFrameHeight('DetailBarIframe','45px');"
                Dim strRefresh As String
                strRefresh = "Refresh();"

                'e.Row.Cells(7).Attributes.Add("onclick", strAction + strRefresh + StrScript)
                e.Row.Cells(7).Attributes.Add("style", "cursor:pointer;")
                e.Row.Cells(7).Attributes.Add("onclick", "javascript:__doPostBack('gridview1','Select$" & e.Row.RowIndex & "')")
                'e.Row.Attributes.Add("onclick", "javascript:__doPostBack('gridview1','Select$" & e.Row.RowIndex & "')")


                '---------------
                Dim str0 As String '5 is the column to add attributes the link
                str0 = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Customer/CustList/ContactDtl.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "';"
                e.Row.Cells(0).Attributes.Add("onclick", str0)
                e.Row.Cells(0).Attributes.Add("style", "cursor:pointer;")

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        Try
            Page.Validate()

            If Page.IsValid Then
                If ValidateBackDate() Then
                    Dim strContactCode As String
                    Dim strSalesmanCode, strCustCOde, strCallDate As String

                    strSalesmanCode = Trim(Session("dflsalesrepcode"))
                    strCustCOde = Trim(Session("customercode"))
                    strContactCode = GridView1.SelectedDataKey.Value.ToString.Trim
                    strCallDate = GetCallDate()
                    Session("contactcode") = strContactCode
                    Dim StrScript As String
                    'StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMenu.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "&visitdate=" + Now().ToString("yyyy-MM-dd") + "&visittime=" + Now().ToString("HH:mm") + "';"
                    StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMenu.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "&visitdate=" + strCallDate + "&visittime=" + Now().ToString("HH:mm") + "';"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "HideTopShowDetail", "ResizeFrameWidth('TopBar','100%');HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar'); ResizeFrameHeight('DetailBarIframe','45px');Refresh();", True)
                End If

            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
        
    End Sub
    Private Function ValidateBackDate() As Boolean
        Dim CallDate As DateTime = Convert.ToDateTime(GetCallDate())
        Dim ts As TimeSpan
        

        If BackDateEnabled Then
            If CallDate.Date > DateTime.Now.Date Then
                lblDateValidation.Text = "Call date cannot be greater than current date."

                UpdateCallDate.Update()
                Return False
            End If

            ts = DateTime.Now.Subtract(CallDate)

            If CurrentMthBackDate = 1 Then
                Dim CurMth As String = DateTime.Now.Month
                Dim SelMth As String = CallDate.Month

                If CurMth = SelMth Then
                    If ts.Days > MaxBackDate Then
                        lblDateValidation.Text = "Currrent maximum backdate is " & MaxBackDate & " days."

                        UpdateCallDate.Update()
                        Return False
                    Else
                        Return True
                    End If
                Else

                    lblDateValidation.Text = "Selected backdate is not within this month."

                    UpdateCallDate.Update()
                    Return False
                End If
               

            Else
                If ts.Days > MaxBackDate Then
                    lblDateValidation.Text = "Currrent maximum backdate is " & MaxBackDate & " days."

                    UpdateCallDate.Update()
                    Return False
                Else
                    Return True
                End If
            End If


        Else
            Return True
        End If
    End Function
    Private Function GetCallDate() As String
        If txtCallDate.Text.Trim = "" Then
            Return Now().ToString("yyyy-MM-dd")
        Else
            Return txtCallDate.Text
        End If
    End Function
    'Sub transactionsInserted()
    '    Try
    '        Session("DRCInserted") = "No"
    '        Session("SalesOrderInserted") = "No"
    '        Session("SFMSInserted") = "No"
    '        Session("MSSInserted") = Nothing
    '        Session("TradeReturnInserted") = "No"
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Sub transactionsSaved()
    '    Try
    '        Session("DRCSaved") = "No"
    '        Session("SalesOrderSaved") = "No"
    '        Session("SFMSSaved") = "No"
    '        Session("MSSSaved") = Nothing
    '        Session("TradeReturnSaved") = "No"
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Sub transactionNothing()
    '    Session("DRCList") = Nothing
    '    Session("DRCHdr") = Nothing
    '    Session("cartdt") = Nothing
    '    Session("SalesHdr") = Nothing
    '    Session("SFMSSession") = Nothing
    '    Session("SFMSHdr") = Nothing
    '    Session("TradeReturn") = Nothing
    '    Session("TradeRetHdr") = Nothing
    '    Session("MSSList") = Nothing
    '    Session("MSSHdr") = Nothing
    'End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



