'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/06/2005
'	Purpose	    :	COM+ Template for Properties 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On 
Option Strict On

Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports System.ComponentModel

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    Inherits ServicedComponent

    Public Class clsTemplate
        Public FieldID As Long         '1
        Public FieldValue As String    '2        
    End Class

    Public Class clsTemplateQuery
        Public FieldID As Long         '1
        Public FieldValue As String    '2        
    End Class
End Class
