Imports System.Data

Partial Class iFFMA_Contact_ContactAssignList
    Inherits System.Web.UI.UserControl

    Public Event ContAcctSaveButton_Click As EventHandler
    Public Event AddButton_Click As EventHandler

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum assignTypeName As Long
        SalesAcct = 0
        FieldForce = 1
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

        'Call Paging
        'With wuc_dgAssignPaging
        '    '.PageCount = dgAssignList.PageCount
        '    '.CurrentPageIndex = dgAssignList.PageIndex
        '    .DataBind()
        '    .Visible = True
        'End With

        If AssignType = assignTypeName.SalesAcct Then
            ''lblTitle.Text = "Sales Account List"
            lblCustCode.Visible = True
            txtCustCode.Visible = True
            lblCustName.Visible = True
            txtCustName.Visible = True

            lblSalesrepCode.Visible = False
            txtSalesrepCode.Visible = False
            lblSalesrepName.Visible = False
            txtSalesrepName.Visible = False

        ElseIf AssignType = assignTypeName.FieldForce Then
            'lblTitle.Text = "Field Force List"

            lblCustCode.Visible = True
            txtCustCode.Visible = True
            lblCustName.Visible = True
            txtCustName.Visible = True

            lblSalesrepCode.Visible = True
            txtSalesrepCode.Visible = True
            lblSalesrepName.Visible = True
            txtSalesrepName.Visible = True


            pnlCtrlAction.Visible = False
        End If

        'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CONTACT, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If
        '----------------------------------------------------------------------------------------------------
    End Sub

#Region "PROPERTY"
    'Public Property Total() As String
    '    Get
    '        Return Trim(lblTotal.Text)
    '    End Get
    '    Set(ByVal value As String)
    '        lblTotal.Text = value
    '    End Set
    'End Property

    Public Property AssignType() As Long
        Get
            Return Trim(hdAssignType.Value)
        End Get
        Set(ByVal value As Long)
            hdAssignType.Value = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(hdCode.Value)
        End Get
        Set(ByVal value As String)
            hdCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub ClearTextBox()

        txtCustCode.Text = ""
        txtCustName.Text = ""
        txtSalesrepCode.Text = ""
        txtSalesrepName.Text = ""
    End Sub

    Public Sub RenewDataBind()
        'HL:20080527
        dgAssignList.PageIndex = 0
        wuc_dgAssignPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsContact As New mst_Contact.clsContact
            Dim strCustCode As String = Trim(txtCustCode.Text)
            Dim strCustName As String = Trim(txtCustName.Text)
            Dim strSalesrepCode As String = Trim(txtSalesrepCode.Text)
            Dim strSalesrepName As String = Trim(txtSalesrepName.Text)

            If AssignType = assignTypeName.SalesAcct Then
                DT = clsContact.GetContAcctList(ContCode, strCustCode, strCustName)
                dgAssignList.DataKeyNames = New String() {"CUST_CODE"}
            ElseIf AssignType = assignTypeName.FieldForce Then
                DT = clsContact.GetContRepList(ContCode, strCustCode, strCustName, strSalesrepCode, strSalesrepName)
                dgAssignList.DataKeyNames = New String() {"SALESREP_CODE", "CUST_CODE"}
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgAssignList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgAssignList.PageIndex = 0
            '    wuc_dgAssignPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgAssignList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Total = Master_Row_Count 'dtCurrentTable.Rows.Count

            'Call Paging
            With wuc_dgAssignPaging
                .PageCount = dgAssignList.PageCount
                .CurrentPageIndex = dgAssignList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlContactAssign.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgAssignList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgAssignList.Columns.Clear()

            'ADD BUTTON EDIT
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgAssignList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_ContactAssignList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_ContactAssignList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_ContactAssignList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_ContactAssignList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_ContactAssignList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgAssignList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgAssignList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgAssignList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgAssignList.RowEditing
        Try
            Dim strSelectedCustCode As String = sender.datakeys(e.NewEditIndex).item("CUST_CODE")
            Dim strSelectedSalesrepCode As String = sender.datakeys(e.NewEditIndex).item("SALESREP_CODE")

            If AssignType = assignTypeName.SalesAcct Then
                With (wuc_ContactAcctPop)
                    .ContCode = ContCode
                    .CustCode = strSelectedCustCode
                    .LoadDvEditMode()
                    .BindDetailsView()
                End With
            ElseIf AssignType = assignTypeName.FieldForce Then
                With (wuc_ContactRepPop)
                    .ContCode = ContCode
                    .SalesrepCode = strSelectedSalesrepCode
                    .CustCode = strSelectedCustCode
                    .LoadDvEditMode()
                    .BindDetailsView()
                End With
            End If
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgAssignList() As GridView
        Dim dgAssignListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgAssignList.AllowSorting
            Dim blnAllowPaging As Boolean = dgAssignList.AllowPaging

            dgAssignList.AllowSorting = False
            dgAssignList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgAssignListToExport = dgAssignList

            dgAssignList.AllowPaging = blnAllowPaging
            dgAssignList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgAssignListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Go_Click
        Try
            dgAssignList.PageIndex = CInt(wuc_dgAssignPaging.PageNo - 1)

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Previous_Click
        Try
            If dgAssignList.PageIndex > 0 Then
                dgAssignList.PageIndex = dgAssignList.PageIndex - 1
            End If
            wuc_dgAssignPaging.PageNo = dgAssignList.PageIndex + 1

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgAssignPaging.Next_Click
        Try
            If dgAssignList.PageCount - 1 > dgAssignList.PageIndex Then
                dgAssignList.PageIndex = dgAssignList.PageIndex + 1
            End If
            wuc_dgAssignPaging.PageNo = dgAssignList.PageIndex + 1

            dgAssignList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        With (wuc_ContactAcctPop)
            .ContCode = ContCode
            .CustCode = ""
            .LoadDvInsertMode()
            .BindDetailsView()
        End With
    End Sub

    Protected Sub btnContAcctPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ContactAcctPop.SaveButton_Click
        RenewDataBind()
        RaiseEvent ContAcctSaveButton_Click(sender, e)
    End Sub

    Protected Sub btnContRepPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ContactRepPop.SaveButton_Click
        RenewDataBind()
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RenewDataBind()
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgAssignList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgAssignList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgAssignList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class CF_ContactAssignList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "LOC"
                strFieldName = "Location"
            Case "DEPARTMENT_NAME"
                strFieldName = "Department"
            Case "SPECIALITY_NAME"
                strFieldName = "Specialty"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
