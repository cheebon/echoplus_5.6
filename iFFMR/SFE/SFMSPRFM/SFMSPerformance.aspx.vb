Imports System.Data

Partial Class SFMSPerformance
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SFMSPRFM")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SFMSPRFM") = value
        End Set
    End Property

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "SFMSPerformance"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim dt As DataTable
        ' Try

        If Not IsPostBack Then

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.SFMSPRFM) '"SFMS Performance "
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.SFMSPRFM
                .DataBind()
                .Visible = True
            End With

            GroupingValue = ""
            '***********************************
            'Edited by Alex Chia 2007-06-07
            'Additional AddIn, to make the report more meaning full
            wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, "")
            wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow), "SFMS Category", "CAT_CODE")
            '***********************************
            TimerControl1.Enabled = True
        End If

        lblErr.Text = ""
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try

    End Sub

#Region "Event Handle"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        ' Try
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        '  Try
        'ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        ' Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        'Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.GroupingFieldChanged
        '   Try
        GroupingValue = String.Empty
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        '   Try
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        '  Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'wuc_ctrlPanel.ExportToFile(dgList, "SFMS_Performance")
        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        '  ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        'Try
        ' If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        ElseIf dtCurrentTable.Rows.Count = 0 Then
            dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        Else
            PreRenderMode(dtCurrentTable)
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        'End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        '   Try
        'Add Data Grid Columns
        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_SFMSPRFM.GetFieldColumnType(ColumnName)
                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_SFMSPRFM.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SFMSPRFM.ColumnStyle(ColumnName).HorizontalAlign


                    dgColumn.HeaderText = CF_SFMSPRFM.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        ViewState("DataItem") = aryDataItem
        'Catch ex As Exception
        '    'lblErr.Text = "wucTemplateList.InitPage : " + ex.ToString()
        'End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '     Try
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow


            Case DataControlRowType.Footer

                Dim iIndex As Integer
                Dim intRowCount As Integer = dgList.Rows.Count

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_SFMSPRFM.GetOutputFormatString(li.Text), CDbl(li.Value))
                    End If
                Next

                If (aryDataItem.IndexOf("SFMS_ACH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("SFMS_ACH")).Text = String.Format(CF_SFMSPRFM.GetOutputFormatString("SFMS_ACH"), (DIVISION(licItemFigureCollector.FindByText("AC_TTL"), licItemFigureCollector.FindByText("SFMS_TGT")) * 100))

        End Select
        ViewState("DataItem") = aryDataItem

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        '     Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        '    Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0
        'Try
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
        Return dblValue
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")

        Dim strGroupCode, strGroupName, strSalesRepList As String
        strGroupCode = Session("NODE_GROUP_NAME")
        strGroupName = strGroupCode.Replace("_CODE", " (Tree)").Replace("_", " ")
        strSalesRepList = Session("SALESREP_LIST")

        Dim strPointerText As String
        strPointerText = strGroupName 'IIf(String.IsNullOrEmpty(strTeamCode), "Team", IIf(String.IsNullOrEmpty(strRegionCode), "Region", "Sales Rep."))
        'Add in salesrep_code when first visit, or when grouping is nothing
        If (wuc_ctrlpanel.pnlField_GetListBoxItemCount(ListBoxType.lsbShow) = 0) Then
            wuc_ctrlpanel.pnlField_InsertPointer(ListBoxType.lsbShow, 0, strPointerText)
        End If

        Dim strGroupField As String
        If String.IsNullOrEmpty(GroupingValue) Then
            strGroupField = wuc_ctrlpanel.pnlField_GetGroupingValue
        Else
            strGroupField = GroupingValue
            GroupingValue = String.Empty
        End If

        'select group field to show
        'Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        'If Not String.IsNullOrEmpty(strTeamCode) Then
        '    If Not String.IsNullOrEmpty(strRegionCode) Then
        '        'group by salesrep
        '        strGroupField = strGroupField.Replace(strPointer, "SALESREP_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Sales Rep.")
        '    Else
        '        'Group by Region
        '        strGroupField = strGroupField.Replace(strPointer, "REGION_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Region")
        '    End If
        'Else
        '    'group by Team
        '    strGroupField = strGroupField.Replace(strPointer, "TEAM_CODE")
        '    wuc_ctrlpanel.pnlField_EditPointerText("Team")
        'End If

        Dim strPointer As String = wuc_ctrlpanel.GroupingPointer
        strGroupField = strGroupField.Replace(strPointer, "TREE_" & strGroupCode)
        wuc_ctrlpanel.pnlField_EditPointerText(strGroupName)

        Dim clsCallDB As New rpt_SFMS.clsSFMSQuery
        DT = clsCallDB.GetSFMSPRMS(strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode, strSalesRepList, strGroupField)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        PreRenderMode(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '  Try
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomHeader()
        Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_CustomHeader()
        ' Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            'If strColumnName Like "TARGET_CALLS*" Then
            If strColumnName = "AC_A" OrElse strColumnName = "AC_B" OrElse _
            strColumnName = "AC_C" OrElse strColumnName = "AC_OTH" Then
                strColumnName = "SFMS by Class"
            Else
                strColumnName = CF_SFMSPRFM.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_CustomHeader : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        '  Try
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim iCounter As Integer = 0
            Dim iActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    iActualIndex = IIf(iCounter > 0, iCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(iActualIndex).RowSpan = 2
                        e.Row.Cells(iActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(iActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    iCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        ' Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "AC_*" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            ElseIf strColumnName Like "SFMS_TGT" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        ' Try
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        '    Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub


End Class

Public Class CF_SFMSPRFM
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "AC_A"
                strFieldName = "A"
            Case "AC_B"
                strFieldName = "B"
            Case "AC_C"
                strFieldName = "C"
            Case "AC_OTH"
                strFieldName = "OTH"
            Case "AC_TTL"
                strFieldName = "Total"
            Case "SFMS_TGT"
                strFieldName = "Target"
            Case "SFMS_ACH"
                strFieldName = "SFMS %"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '  Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If
        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper

        'Try
        Select Case strNewName
            'OBCS, STK_TRANSFER, ACT_QTY, CLS_BALANCE, ACT_TOTAL,OFF_TAKE
            Case "AC_A", "AC_B", "AC_C", "AC_OTH", "AC_TTL", "SFMS_TGT"
                strStringFormat = "{0:0}"
            Case "SFMS_ACH"
                strStringFormat = "{0:#,0.0}"
            Case Else
                strStringFormat = ""
        End Select

        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        '      Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function
End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    InvisibleColumn = 99
'End Enum