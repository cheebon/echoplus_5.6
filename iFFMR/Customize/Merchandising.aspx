﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Merchandising.aspx.vb" Inherits="iFFMR_Customize_Merchandising" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblDate" Src="~/include/wuc_lblDate.ascx" %>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script type="text/javascript" src="../../scripts/jquery-2.1.4.min.js"></script>
    <link href="../../styles/select2.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/select2.min.js"></script>

    <style type="text/css">
        .speciality {
            min-width: 200px;
        }

        .dropdown-multiple {
            /*width: 100% !important;*/
            height: 15px !important;
            min-height: 15px !important;
            max-height: 15px !important;
            font-size: 10px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">

        function loadBrandDDL() {
            $(function () {
                $("#ddlbrand").select2({
                    allowClear: true
                });
            });
        }

        function loadActivityDDL() {
            $(function () {
                $("#ddlActivity").select2({
                    allowClear: true
                });
            });
        }

        function UpdateLoadDDL() {
            if (typeof (Sys) != 'undefined') {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    loadBrandDDL();
                    loadActivityDDL();
                });
            }
        }

        //function refreshActivityDDL() {
        //    $('#btnLoadActivity').trigger('click');
        //}
      
        $(document).ready(function () {
            UpdateLoadDDL();
            loadBrandDDL();
            loadActivityDDL();  
        });
    </script>

</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmMerchandising" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" align="left">
                    <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTreeMenuControl" runat="server">
                                <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_lblheader ID="wuc_lblheader" runat="server"></customToolkit:wuc_lblheader>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err" meta:resourcekey="lblErrResource1"></asp:Label>
                                    <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                    <asp:UpdatePanel runat="server" ID="UpdateDatagrid" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                            </asp:Timer>
                                            <div style="width: 100%">
                                                <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pSearchCriteria" Width="100%" runat="server" CssClass="cls_panel">
                                                            <table style="width: 98%;">
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button"></asp:Button>
                                                                        <asp:Button ID="btnReset" runat="server" CssClass="cls_button" Text="Reset" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:150px">
                                                                        <asp:Label ID="lblDate" runat="server" Text="Filter activity date :" CssClass="cls_label_header"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <customToolkit:wuc_txtCalendarRange ID="txtDate" runat="server" RequiredValidation="true"
                                                                            RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:150px">
                                                                        <asp:Label ID="lblBrand" runat="server" Text="Filter Brand/Product :" CssClass="cls_label_header"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <select id="ddlbrand" runat="server" class="dropdown-multiple" style="width:100%"></select>
                                                                    </td>
                                                                    <td>
                                                                        <asp:button ID ="btnLoadActivity" runat="server" Text="Load Activity" ToolTip="Click to Filter Activity" CssClass ="cls_button"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:150px">
                                                                        <asp:Label ID="lblActivity" runat="server" Text="Filter Activity :" CssClass="cls_label_header"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <select id="ddlActivity" runat="server" class="dropdown-multiple" style="width:100%"></select>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <br />
                                            <asp:UpdatePanel runat="server" ID="updateDG" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="width: 98%;">
                                                        <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="True" AllowSorting="True"
                                                            AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="455"
                                                            AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                            FreezeRows="0" GridWidth="">
                                                            <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                            <EmptyDataTemplate>
                                                                There is no data to display.
                                                            </EmptyDataTemplate>
                                                        </ccGV:clsGridView>
                                                    </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
