<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAOrderHdrListV2.aspx.vb" Inherits="iFFMS_Order_TRAOrderHdrList" EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register Src="~/include/wuc_dgpaging.ascx" TagPrefix="uc1" TagName="wuc_dgpaging" %>
<%@ Register Src="../../include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtDateRange" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_pnlRecordNotFound.ascx" TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TRA Order List</title>
    <link rel="stylesheet" href='../../include/DKSH.css' />
    <script type="text/javascript" src="../../include/jquery-1.4.2.min.js"></script>
    <script language="javascript" type="text/javascript">

        function closePopup() {
            $find('ModalPopupCall').hide();
        }
        function callPopUp() {
            //$find('lblDdlMsgPop_pnlMsgPop').show();
            $("#lblDdlMsgPop_pnlMsgPop").css("display", "block");
            $find('ModalPopupCall').show();
        }
        function go_AddOrder() {
            if (parent) {
                parent.frames['ContentBar'].location.href = 'TRAOrder.aspx';
                if (self.parent.CollapseExpand)
                    self.parent.CollapseExpand();
            }
        }

        function ValidateCheckBoxStates(straction) {
            var dglist = document.getElementById('dgList');
            var selected; selected = 0;
            for (var i = 1; i < dglist.rows.length; i++) {
                if (dglist.rows[i].cells[0].childNodes[0]) {
                    if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
                        if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;
                    }
                }
            }
            if (selected > 0) {
                if (straction == 'SUBMIT') { var agree = confirm('Are you sure you want to SUBMIT the TRA Transaction?'); if (agree) return true; else return false; }
                else { var agree = lblDdlMsgPop.Show(); if (agree) return true; else return false; }
            }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmTRAOrderList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <table id="tbl1" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
                        <tr>
                            <td>
                                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td>
                                            <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <div style="width: 100%;">
                                                <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                                                    width="100%" border="0" style="height: 30px">
                                                    <tr align="left" valign="bottom">
                                                        <td align="left">
                                                            <asp:Image ID="imgExpandCollapse" ImageUrl="~/images/ico_Field.gif" runat="server"
                                                                CssClass="cls_button" ToolTip="Criteria Selection" EnableViewState="false" />
                                                            <%--<asp:Image ID="Image1" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />    
                                                        <asp:Image ID="imgExpandGlossary" ImageUrl="~/images/ico_header.gif" runat="server"
                                                            CssClass="cls_button" ToolTip="Criteria Selection" EnableViewState="false" />--%>
                                                            <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server" CssClass="cls_button" ToolTip="Export"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Panel ID="pSearchCriteria" runat="server" CssClass="cls_ctrl_panel">
                                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0"
                                                        style="padding-bottom: 5px; padding-left: 5px;">
                                                        <tr>
                                                            <td>
                                                                <table width="1000px" border="0" style="text-align: left;">
                                                                    <tr align="left">
                                                                        <td style="width: 150px">
                                                                            <span class="cls_label_header">Txn No.</span>
                                                                        </td>
                                                                        <td style="width: 5px">
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td style="width: 180px">
                                                                            <asp:TextBox ID="txttxnno" runat="server" CssClass="cls_textbox" Width="150px" ValidationGroup="SearchCriteria" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 150px"></td>
                                                                        <td style="width: 5px"></td>
                                                                        <td style="width: 180px"></td>
                                                                    </tr>
                                                                    <tr align="left">
                                                                        <td>
                                                                            <span class="cls_label_header">Txn Date</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td colspan="4">
                                                                            <customToolkit:wuc_txtDateRange ID="wuc_txtDate" runat="server" DateFormatString="yyyy-MM-dd"
                                                                                RequiredValidation="True" RequiredValidationGroup="SearchCriteria" />
                                                                        </td>
                                                                        <%-- <td ></td>
                                                                        <td ></td>
                                                                        <td > </td>--%>
                                                                    </tr>
                                                                    <tr align="left">
                                                                        <td>
                                                                            <span class="cls_label_header">Txn Status</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddltxnstatus" runat="server" CssClass="cls_dropdownlist" ValidationGroup="SearchCriteria" Width="150px">
                                                                                <asp:ListItem Text="P - Pending Approval" Value="P"></asp:ListItem>
                                                                                <asp:ListItem Text="K - KIV TRA" Value="K"></asp:ListItem>
                                                                                <asp:ListItem Text="V - Pending Revision" Value="V"></asp:ListItem>
                                                                                <asp:ListItem Text="C - Cancelled TRA" Value="C"></asp:ListItem>
                                                                                <asp:ListItem Text="S - Submitted to SAP" Value="S"></asp:ListItem>
                                                                                <asp:ListItem Text="ALL" Value="" Selected="True"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td><span class="cls_label_header">SAP No.</span></td>
                                                                        <td><span class="cls_label_header">:</span></td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsapno" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="cls_label_header">Payer Code</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtPayerCode" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">Ref. No.</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtrefno" runat="server" CssClass="cls_textbox" Width="150px" ValidationGroup="SearchCriteria" MaxLength="50"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="left">
                                                                        <td>
                                                                            <span class="cls_label_header">Payer Name</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtpayername" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">Sales Office Code</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtSalesAreaCode" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="left">
                                                                        <td>
                                                                            <span class="cls_label_header">Salesrep Code</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsalesrepcode" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">Sales Team Code</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtSalesTeamCode" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="left">
                                                                        <td>
                                                                            <span class="cls_label_header">Salesrep Name</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtsalesrepname" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">Created By User Name</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="cls_label_header">:</span>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtUserName" runat="server" CssClass="cls_textbox" Width="150px" MaxLength="50"
                                                                                ValidationGroup="SearchCriteria"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="left">
                                                                        <td colspan="6">
                                                                            <asp:Button ID="btnSearchCriteria" runat="server" Text="Search" CssClass="cls_button"
                                                                                ValidationGroup="SearchCriteria"></asp:Button>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPESearchCriteria" runat="server" TargetControlID="pSearchCriteria"
                                                        ExpandControlID="imgExpandCollapse" CollapseControlID="imgExpandCollapse" Collapsed="false"
                                                        SuppressPostBack="false">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                </asp:Panel>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="width: 100%; padding-left: 10px; padding-top: 10px;">
                                                <div style="width: 900px; float: left;">
                                                    <input id="btnOrdAdd" type="button" value="Add" onclick="go_AddOrder()" class="cls_button"
                                                        style="width: 100px" />
                                                    <asp:Button ID="btnSumbilt" runat="server" Text="Submit Selected" CssClass="cls_button"
                                                        Width="100px" OnClientClick="return ValidateCheckBoxStates('SUBMIT');" />
                                                    <input type="button" value="Cancel Selected" class="cls_button" onclick="callPopUp();" />
                                                </div>
                                                <div style="width: 99px; float: right;">
                                                    <%-- <asp:Button ID="btnCancel" runat="server" Text="Cancel Selected" CssClass="cls_button"
                                                Width="80px" OnClientClick="javascript:callPopUp();" />--%>
                                                    <%--<input type="button" value="Cancel Selected" class="cls_button" onclick="callPopUp();" />--%>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td valign="top" class="Bckgroundreport">
                                                        <br />
                                                        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" style="width: 95%;">
                                                                            <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" style="width: 95%;">
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" AddEmptyHeaders="0" CellPadding="2" GridHeight="440"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="true" PagerSettings-Visible="false" DataKeyNames="TXN_NO,TXN_DATE,SALESREP_CODE,CUST_CODE,VISIT_ID,VOUCHER_NO,TXN_STATUS">
                                                                                <Columns>
                                                                                    <%--   <asp:CommandField ShowEditButton="True"/>
                                                                                    <asp:TemplateField HeaderText="Select">
                                                                                        <itemstyle horizontalalign="Center" />
                                                                                        <itemtemplate>
                                                                                        <asp:CheckBox id="chkselect" runat="server" Enabled="False" ></asp:CheckBox>
                                                                                        </itemtemplate>
                                                                                    </asp:TemplateField>--%>
                                                                                </Columns>
                                                                                <EmptyDataTemplate>
                                                                                    <uc1:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                </EmptyDataTemplate>
                                                                            </ccGV:clsGridView>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
                    <!--The confirm_delete function is used in delete button-->
                    <%--<customToolkit:wuc_lblDdlMsgPop ID="lblDdlMsgPop" Title="Do you want to cancel selected item?" runat="server" />--%>
                    <asp:Button runat="server" ID="btnHidden" Style="display: none" />
                    <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px"
                        CssClass="modalPopup">
                        <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px; text-align: center">
                            <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="Do you want to cancel selected item?" />
                        </asp:Panel>
                        <div onkeypress="javascript:return WebForm_FireDefaultButton(event, 'lblPopNotify_btnClose')"
                            id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">

                            <fieldset style="padding-left: 10px; width: 98%">
                                <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="Please select cancellation reason code: " />

                                <asp:DropDownList ID="ddlreasoncode" runat="server" CssClass="cls_dropdownlist">
                                </asp:DropDownList>
                                <br />
                                <center>
                                    <input type="button" value="Back" class="cls_button" onclick="closePopup();" />
                                    <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Proceed" />
                                </center>
                            </fieldset>
                        </div>
                    </asp:Panel>

                    <ajaxToolkit:ModalPopupExtender
                        ID="ModalPopupMessage" runat="server"
                        BehaviorID="ModalPopupCall"
                        TargetControlID="btnHidden"
                        PopupControlID="pnlMsgPop"
                        BackgroundCssClass="modalBackground"
                        DropShadow="True"
                        RepositionMode="RepositionOnWindowResizeAndScroll" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>

        <script id="scriptResubmit" language="javascript" type="text/javascript">
            //        function confirm_delete(ind)
            //        {
            //	        if (confirm("Do you want to resubmit ?")==true){
            //	            iFFMA_SAP_SAPList.UpdateMethod(UpdateMethod_CallBack);     
            //		        return true;
            //		    }
            //	        else
            //		        return false;
            //        }       
        </script>
    </form>
</body>
</html>
<%  'List function called by in-line scripts%>
