﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanTxnMenu.aspx.vb" Inherits="iFFMS_PlanCuz_PlanTxnMenu" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plan Customer Transaction Menu</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script language="javascript" type="text/javascript" src="../../../include/layout.js"></script>
    
    <script language="javascript" type="text/javascript">

        function RefreshContentBar(page) {
            var salesrepcode; var custcode; var contcode; var datein; var timein; var visitid; var sessionid;
            salesrepcode = document.getElementById("lblsalesrepcode").innerHTML;
            custcode = document.getElementById('lblcustcode').innerHTML;
            contcode = document.getElementById('lblcontcode').innerHTML;
            datein = document.getElementById('lbldateIn').innerHTML;
            timein = document.getElementById('lbltimeIn').innerHTML;
            visitid = document.getElementById('lblvisitid').innerHTML;
            sessionid = document.getElementById('lblsessionid').innerHTML;
            if (page == 'home') { parent.document.getElementById('SubDetailBarIframe').src = "../../iFFMS/Customer/CustTxn/TxnDefault.aspx?salesrepcode=" + salesrepcode + "&custcode=" + custcode + "&contcode=" + contcode + "&datein=" + datein + "&timein=" + timein + "&visitid=" + visitid + "&sessionid=" + sessionid; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'drc') { parent.document.getElementById('SubDetailBarIframe').src = "../../iFFMS/Customer/CustTxn/TxnDrc.aspx?salesrepcode=" + salesrepcode + "&custcode=" + custcode + "&contcode=" + contcode + "&datein=" + datein + "&timein=" + timein + "&visitid=" + visitid + "&sessionid=" + sessionid; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'mss') { parent.document.getElementById('SubDetailBarIframe').src = "../../iFFMS/Customer/CustTxn/TxnMss.aspx?salesrepcode=" + salesrepcode + "&custcode=" + custcode + "&contcode=" + contcode + "&datein=" + datein + "&timein=" + timein + "&visitid=" + visitid + "&sessionid=" + sessionid; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'so') { parent.document.getElementById('SubDetailBarIframe').src = "../../iFFMS/Customer/CustTxn/TxnSO.aspx?salesrepcode=" + salesrepcode + "&custcode=" + custcode + "&contcode=" + contcode + "&datein=" + datein + "&timein=" + timein + "&visitid=" + visitid + "&sessionid=" + sessionid; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'tra') { var src; src = '../../iFFMS/Customer/CustTxn/TxnTra.aspx?salesrepcode=' + salesrepcode + '&custcode=' + custcode + '&contcode=' + contcode + '&datein=' + datein + '&timein=' + timein + '&visitid=' + visitid + '&sessionid=' + sessionid; parent.document.getElementById('SubDetailBarIframe').src = src; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'sfms') { var src; src = '../../iFFMS/Customer/CustTxn/TxnSFMS.aspx?salesrepcode=' + salesrepcode + '&custcode=' + custcode + '&contcode=' + contcode + '&datein=' + datein + '&timein=' + timein + '&visitid=' + visitid + '&sessionid=' + sessionid; parent.document.getElementById('SubDetailBarIframe').src = src; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'sfmsCuz') { var src; src = '../../iFFMS/Customer/CustTxn/TxnSFMSCuz.aspx?salesrepcode=' + salesrepcode + '&custcode=' + custcode + '&contcode=' + contcode + '&datein=' + datein + '&timein=' + timein + '&visitid=' + visitid + '&sessionid=' + sessionid; parent.document.getElementById('SubDetailBarIframe').src = src; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'end') { var src; src = '../../iFFMS/Customer/CustTxn/TxnEnd.aspx?EndType=Plan&salesrepcode=' + salesrepcode + '&custcode=' + custcode + '&contcode=' + contcode + '&datein=' + datein + '&timein=' + timein + '&visitid=' + visitid + '&sessionid=' + sessionid; parent.document.getElementById('SubDetailBarIframe').src = src; parent.document.getElementById('SubDetailConfBarIframe').src = ""; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
            if (page == 'main') { HideElement('SubDetailConfBar'); HideElement('SubDetailBar'); HideElement('DetailBar'); ShowElement('TopBar'); ShowElement('ContentBar'); MaximiseFrameHeight('TopBarIframe'); MaximiseFrameHeight('ContentBarIframe'); ResizeFrameWidth('ContentBar', '50%'); ; ResizeFrameWidth('TopBar', '50%') };
        }

        function setClass(element)
        { if (element) { var pElement = element.parentElement.parentElement; for (var i = 0; i < pElement.children.length; i++) { var innerElement = pElement.children[i].children[0]; if (innerElement) { if (innerElement == element) { innerElement.className = 'current'; } else { innerElement.className = ''; } } } } }

        function Refresh() {
            var ifra = self.parent.document.getElementById('TopBarIframe');
            ifra.contentWindow.ClickBtn();
        }

        var valueChanged = false;
        function changeValue(value)
        { valueChanged = value; }

        function HideNavigation() {
            var height = screen.height;
            if (height < 700) {
                self.parent.parent.document.getElementById('fraDefault').rows = "20,*";
            }
            else
            { self.parent.parent.parent.document.getElementById('fraDefault').rows = "87,*"; }
        }

        function ShowNavigation() {
            var height = screen.height;
            if (height < 700) {
                var fraDefault = self.parent.parent.parent.document.getElementById('fraDefault').rows = "65,*";
            }
            else
            { self.parent.parent.parent.document.getElementById('fraDefault').rows = "132,*"; }
        } 
    </script>
    
    <style type="text/css">
#styleone{position:relative;display:block;height:21px;font-size:11px;font-weight:bold;background:transparent url(../../images/bgOFF.gif) repeat-x top left;font-family:Arial,Verdana,Helvitica,sans-serif;border-bottom:1px solid #d9d9d9;}
#styleone ul{margin:0;padding:0;list-style-type:none;width:auto;}
#styleone ul li{display:block;float:left;margin:0 1px 0 0;}
#styleone ul li a{display:block;float:left;color:#034895;text-decoration:none;padding:3px 20px 0 20px;height:18px;}
#styleone ul li a:hover,#styleone ul li a.current{color:#034895;background:transparent url(../../images/bgON.gif) repeat-x top left;}
</style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmPlanCustTxnMenu" runat="server">
     <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <asp:Label runat="server"  id="lblerr" CssClass="cls_label_err" Text="" ></asp:Label>
                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="500" OnTick="TimerControl1_Tick" />
                <div style="width: 100%; position: relative; padding-left: 1px; margin: 0;" class="Bckgroundreport">
                    <table id="tbldtl" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td>
                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                <div style="display:none;">
                                <span class="cls_label">VISIT INFO - Salesrep Code :</span><asp:Label runat="server"
                                    ID="lblsalesrepcode" CssClass="cls_label"></asp:Label>| <span class="cls_label">Customer
                                        Code :</span><asp:Label runat="server" ID="lblcustcode" CssClass="cls_label"></asp:Label>|
                                <span class="cls_label">Contact Code :</span><asp:Label runat="server" ID="lblcontcode"
                                    CssClass="cls_label"></asp:Label>| <span class="cls_label">Date in:</span>
                                <asp:Label runat="server" ID="lbldateIn" CssClass="cls_label"></asp:Label>| <span
                                    class="cls_label">Time in:</span>
                                <asp:Label runat="server" ID="lbltimeIn" CssClass="cls_label"></asp:Label>| <span
                                    class="cls_label">Visit Id:</span>
                                <asp:Label runat="server" ID="lblvisitid" CssClass="cls_label"></asp:Label>| <span
                                    class="cls_label">Session Id:</span>
                                <asp:Label runat="server" ID="lblsessionid" CssClass="cls_label"></asp:Label>|
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="styleone">
                                    <ul>
                                        <%-- <li><a href="#" id="btnHome" onclick="RefreshContentBar('home');setClass(this)">Home</a></li>--%>
                                        <%  If GetAccessRight(3, 19, "'1'") Then%>
                                        <li><a id="btnTxnDrc" href="#" onclick="RefreshContentBar('drc');setClass(this);changeValue(true);">Inventory
                                            Forecast.</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 22, "'1'") Then%>
                                        <li><a id="btnTxnSalesOrd" href="#" onclick="RefreshContentBar('so');setClass(this);changeValue(true);">
                                            Sales Order</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 23, "'1'") Then%>
                                        <li><a id="btnTxnTradeRtn" href="#" onclick="RefreshContentBar('tra');setClass(this);changeValue(true);">
                                            Goods Return</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 20, "'1'") Then%>
                                        <li><a id="btnsfms" href="#" onclick="RefreshContentBar('sfms');setClass(this);changeValue(true);">Field
                                            Activity</a></li>
                                        <%  End If%>
                                         <%  If GetAccessRight(3, 239, "'1'") Then%>
                                        <li><a id="btnsfmsCuz" href="#" onclick="RefreshContentBar('sfmsCuz');setClass(this);changeValue(true);">Field
                                            Activity</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 21, "'1'") Then%>
                                        <li><a id="btnTxnMss" href="#" onclick="RefreshContentBar('mss');setClass(this);changeValue(true);">Market
                                            Survey</a></li>
                                        <%  End If%>
                                        <li><a id="btnTxnEnd" href="#" onclick="RefreshContentBar('end');setClass(this);changeValue(false);">End
                                            Visit</a></li>
                                        <%-- <li>
                                            <asp:LinkButton runat="server" ID="btnEndVisit" Text="End Visit" OnClientClick="RefreshContentBar('main');setClass(this)" /></li>--%>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
<script type="text/javascript" language="javascript">
    window.document.body.onbeforeunload = function() { if (valueChanged == true) { return "Kindly click end visit to exit!"; } }
      
</script>
</html>
