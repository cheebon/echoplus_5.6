'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	6/11/2006
'	Purpose	    :	Class to build datatables for getting and storing Sales Order transaction details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

'Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSalesOrder
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))



    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSalesOrder"
        End Get
    End Property

    Function getWareHouseCodeDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESWAREHOUSELISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getWareHouseCodeDT:" & ex.Message))
        End Try
    End Function

    Function getProductCodeDT(ByVal strWarehouseCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPRODUCTLISTING"

                .addItem("warehousecode", strWarehouseCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getProductCodeDT:" & ex.Message))
        End Try
    End Function

    Function productListing(ByVal strWarehouseCode As String, ByVal strPrdGrp As String, ByVal strCoast As String, ByVal strSalesmancode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDLISTING"
                .addItem("warehouse_code", strWarehouseCode, 2)
                .addItem("prod_grp", strPrdGrp, 2)
                .addItem("coast", strCoast, 2)
                .addItem("salesmancode", strSalesmancode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".productListing :" & ex.Message))
        End Try
    End Function

    Function salesmanProductGroup(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SLSMANPRDGROUP"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".salesmanProductGroup :" & ex.Message))
        End Try
    End Function

    Function getItemCodeDT(ByVal strProductCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESITEMLISTING"
                .addItem("prdgrp", strProductCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getItemCodeDT:" & ex.Message))
        End Try
    End Function



    Function getUOMCodeDT(ByVal strProductCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESUOM"
                .addItem("productcode", strProductCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getUOMCodeDT:" & ex.Message))
        End Try
    End Function

    Function getUOMPriceDT(ByVal strProductCode As String, ByVal strUOMCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESUOMPRICE"
                .addItem("productcode", strProductCode, 2)
                .addItem("uomcode", strUOMCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getUOMPriceDT:" & ex.Message))
        End Try

    End Function

    Function getLineTypeDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESLINETYPE"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
                '.CmdText = "SELECT line_typecd,line_typedesc FROM line_type_code"
                'Return .Retrieve("SELECT line_typecd,line_typedesc FROM line_type_code")
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getLineTypeDT:" & ex.Message))
        End Try
    End Function

    Function getItemPrice(ByVal strItemCode As String) As Double
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESGETPRICE"
                .addItem("itemcode", strItemCode, 2)
                dt = .spRetrieve

                Return Double.Parse(dt.Rows(0)("list_price").ToString)
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getItemPrice:" & ex.Message))
        End Try
    End Function

    Function getCustSpecBonusDT(ByVal strItemCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESBONUSCUSTSPEC"
                .addItem("itemcode", strItemCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCustSpecBonusDT:" & ex.Message))
        End Try
    End Function

    Function getPriceGroupBonusDT(ByVal strItemCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESBONUSPRICEGROUP"
                .addItem("itemcode", strItemCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPriceGroupBonusDT:" & ex.Message))
        End Try
    End Function

    Function getCustGroupBonusDT(ByVal strItemCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESBONUSCUSTGRP"
                .addItem("itemcode", strItemCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPriceGroupBonusDT:" & ex.Message))
        End Try
    End Function

    Function getCrossBonusDT(ByVal strItemCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESBONUSCROSS"
                .addItem("itemcode", strItemCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCrossBonusDT:" & ex.Message))
        End Try
    End Function

    Function getStandardBonusDT(ByVal strItemCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESBONUSSTANDARD"
                .addItem("itemcode", strItemCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getStandardBonusDT:" & ex.Message))
        End Try
    End Function

    Function getPriceCustSpec(ByVal strItemCode As String, ByVal strCustCode As String, ByVal strUOM As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPRICECUSTSPEC"
                .addItem("itemcode", strItemCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("uomcode", strUOM, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPriceCustSpec:" & ex.Message))
        End Try
    End Function

    Function getPricePriceGrp(ByVal strItemCode As String, ByVal strUOM As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPRICEPRICEGRP"
                .addItem("itemcode", strItemCode, 2)
                .addItem("uomcode", strUOM, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPricePriceGrp:" & ex.Message))
        End Try
    End Function

    Function getPriceCustGrp(ByVal strItemCode As String, ByVal strCustCode As String, ByVal strUOM As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPRICECUSTGRP"
                .addItem("itemcode", strItemCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("uomcode", strUOM, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPriceCustGrp:" & ex.Message))
        End Try
    End Function

    Function getPriceStandard(ByVal strItemCode As String, ByVal strUOM As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPRICESTANDARD"
                .addItem("itemcode", strItemCode, 2)
                .addItem("uomcode", strUOM, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPrice:" & ex.Message))
        End Try
    End Function

    Function getPaymentTermDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPAYMENT"
                .addItem("SalesmanCode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPrice:" & ex.Message))
        End Try
    End Function

    Sub insertSalesDetail(ByVal strTxnNo As String, ByVal strLineNo As String, ByVal strItemCode As String, ByVal strOrdType As String, ByVal intQty As Integer, ByVal intFOCQty As Integer, ByVal dblPrice As Double, ByVal dblDiscount As Double, ByVal dblNetPrice As Double, ByVal dblAmt As Double, ByVal strUom As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESINSERTDETAIL"
                .addItem("txnno", strTxnNo, 2)
                .addItem("lineno", strLineNo, 2)
                .addItem("itemcode", strItemCode, 2)
                .addItem("ordtype", strOrdType, 2)
                .addItem("qty", intQty, 1)
                .addItem("focqty", intFOCQty, 1)
                .addItem("listprice", dblPrice, 5)
                .addItem("discountamt", dblDiscount, 5)
                .addItem("netprice", dblNetPrice, 5)
                .addItem("amt", dblAmt, 5)
                .addItem("uom", strUom, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertSalesDetail:" & ex.Message))
        End Try
    End Sub

    Sub insertSalesFOC(ByVal strTxnNo As String, ByVal strLineNo As String, ByVal strReason As String, ByVal strItemCode As String, ByVal intQty As Integer, ByVal strUOM As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESINSERTFOC"
                .addItem("txnno", strTxnNo, 2)
                .addItem("lineno", strLineNo, 2)
                .addItem("reasoncode", strReason, 2)
                .addItem("itemcode", strItemCode, 2)
                .addItem("focqty", intQty, 1)
                .addItem("uomcode", strUOM, 2)
                .spInsert()
            End With
        Catch ex As Exception

        End Try
    End Sub

    Sub insertSalesHeader(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strDelCode As String, ByVal dtmDW As DateTime, ByVal strVisitID As String, ByVal strPO As String, ByVal dblTotal As Double, ByVal dblAmt As Double, ByVal strRemark As String, ByVal strPayTermCode As String, Optional ByVal dtmPayDate As DateTime = Nothing)
        Dim obj As New cor_DB.clsDB
        'strPayTermCode = "CI6"

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESINSERTHEADER"
                .addItem("txnno", strTxnNo, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)
                .addItem("branchcode", strBranchCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("delcode", strDelCode, 2)
                .addItem("dw", dtmDW, 3)
                .addItem("visitid", strVisitID, 2)
                .addItem("txntime", FormatDateTime(DateTime.Now, DateFormat.ShortTime), 2)
                .addItem("po", strPO, 2)
                .addItem("total", dblTotal, 5)
                .addItem("amt", dblAmt, 5)
                .addItem("remark", strRemark, 2)
                .addItem("paytermcode", strPayTermCode, 2)
                .addItem("paydate", dtmPayDate, 3)

                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertSalesHeader:" & ex.Message))
        End Try
    End Sub

    Function getSalesOrderEditDT(ByVal strSalesmanCode As String, ByVal strTxnno As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESORDEREDIT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("txnno", strTxnno, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSalesOrderListingDT:" & ex.Message))
        End Try
    End Function

    Function getFOCEditDT(ByVal strTxnno As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESORDERGETFOC"
                .addItem("txnno", strTxnno, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getFOCEditDT:" & ex.Message))
        End Try
    End Function

    Sub deleteSalesOrder(ByVal strTxnNo As String, ByVal strInd As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESORDERDELETE"
                .addItem("txnno", strTxnNo, 2)
                .addItem("type", strInd, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteSalesOrder:" & ex.Message))
        End Try
    End Sub

    Sub deleteFOC(ByVal strTxnNo As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESDELETEFOC"
                .addItem("txnno", strTxnNo, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteFOC:" & ex.Message))
        End Try
    End Sub

    Function getPaymentInfo(ByVal strTxnNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESPAYMENTINFO"
                .addItem("txnno", strTxnNo, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPaymentInfo:" & ex.Message))
        End Try

    End Function

    Sub updateSalesOrderHeader(ByVal strTxnno As String, ByVal dtmDW As DateTime, ByVal strPayTermCode As String, ByVal dblAmm As Double, ByVal strRemark As String, ByVal strPO As String, ByVal strAdd As String, Optional ByVal dtmPayDate As DateTime = Nothing)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESUPDATEHEADER"
                .addItem("txnno", strTxnno, 2)
                .addItem("dw", dtmDW, 3)
                .addItem("paytermcode", strPayTermCode, 2)
                .addItem("amount", dblAmm, 5)
                .addItem("paydate", dtmPayDate, 3)
                .addItem("remark", strRemark, 2)
                .addItem("po", strPO, 2)
                .addItem("add", strAdd, 2)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateSalesOrder:" & ex.Message))
        End Try
    End Sub

    Function getExtraFOC(ByVal strItemCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SALESEXTRAFOC"
                .addItem("itemcode", strItemCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getExtraFOC:" & ex.Message))
        End Try

    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
