<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerARAgingDetail" CodeFile="CustomerARAgingDetail.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Goods AR Aging </title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmARAgingDetail" method="post" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                    </asp:Timer>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td class="Bckgroundreport">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="left" style="width: 123px; height: 10px">
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="left" style="width: 752px; height: 21px;" valign="top">
                                                                                        <asp:Label ID="Label3" runat="server" CssClass="cls_label_header">Txn No :</asp:Label></td>
                                                                                    <td align="left" colspan="2" style="width: 752px; height: 21px;" valign="top">
                                                                                        <asp:Label ID="lblTxnNo" runat="server" CssClass="cls_label" Width="100px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" style="width: 752px; height: 21px;" valign="top">
                                                                                        <asp:Label ID="Label2" runat="server" CssClass="cls_label_header">Amount : </asp:Label></td>
                                                                                    <td align="left" colspan="2" style="width: 752px; height: 21px;" valign="top">
                                                                                        <asp:Label ID="lblAmount" runat="server" CssClass="cls_label" Width="100px"></asp:Label></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" style="width: 752px; height: 21px;" valign="top">
                                                                                        <asp:Label ID="Label1" runat="server" CssClass="cls_label_header" Text="Remark : "
                                                                                            Width="117px"></asp:Label></td>
                                                                                    <td align="left" colspan="2" style="width: 752px; height: 21px;" valign="top">
                                                                                        <asp:Label ID="lblRemark" runat="server" CssClass="cls_label" Width="102px"></asp:Label></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 123px; height: 10px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 123px; height: 10px;">
                                                                            <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" Width="60px" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 123px; height: 10px">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <ccGV:clsGridView ID="gridview1" runat="server" ShowFooter="false" AllowSorting="True"
                                                                    AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="250px"
                                                                    AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                                    FreezeRows="0" GridWidth="" RowHighlightColor="AntiqueWhite">
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="item_code" HeaderText="Product ID" />
                                                                        <asp:BoundField DataField="item_desc" HeaderText="Product Desc" />
                                                                        <asp:BoundField DataField="qty_desc" HeaderText="Qty" />
                                                                        <asp:BoundField DataField="amt" HeaderText="Amount" />
                                                                    </Columns>
                                                                    <FooterStyle CssClass="GridFooter" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                                                    <RowStyle CssClass="GridNormal" />
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
