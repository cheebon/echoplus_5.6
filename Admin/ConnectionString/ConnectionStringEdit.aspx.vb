'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	14/04/2007
'	Purpose	    :	Connection String Edit
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Public Class Admin_ConnectionString_ConnectionStringEdit
    Inherits System.Web.UI.Page
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_Comm_Svr_Setting.clsCommSvrQuery
        Dim objCrypto As cor_Crypto.clsDecryption
        Dim strDecryptPwd, strUserCode, strUserID As String
        Dim dt As DataTable
        Dim strSessionUserCode As String

        Try
            lblErr.Text = ""

            If Not IsPostBack Then
                'constant
                strSessionUserCode = Trim(Session("ConnectionStringCode"))

                'Call Header
                With wuc_lblheader
                    .Title = "Editing Connection" 'strTitle
                    .DataBind()
                    .Visible = True
                End With

                'request
                strUserCode = Trim(Request.QueryString("comm_svr_setting_code"))
                strUserID = Trim(Request.QueryString("comm_svr_setting_id"))

                txtPassword.Enabled = False
                txtConfirmedPassword.Enabled = False
                rfvPassword.Visible = False
                rfvConfirmedPassword.Visible = False
                lblMark3.Visible = False
                lblMark4.Visible = False

                'Get Details
                objUserQuery = New adm_Comm_Svr_Setting.clsCommSvrQuery
                With objUserQuery
                    .clsProperties.strCommID = strUserID
                    dt = .GetCommSvrDetail
                End With

                If dt.Rows.Count > 0 Then
                    'Decrypt Password
                    objCrypto = New cor_Crypto.clsDecryption
                    With objCrypto
                        strDecryptPwd = .TripleDESDecrypt(Trim(dt.Rows(0)("db_user_pwd")))
                    End With

                    ddlStatusID.SelectedValue = Trim(dt.Rows(0)("active_flag"))
                    ViewState("db_user_pwd") = strDecryptPwd
                    txtPassword.Attributes.Add("value", ViewState("db_user_pwd").ToString)
                    txtConfirmedPassword.Attributes.Add("value", ViewState("db_user_pwd").ToString)
                    lblUserIDValue.Text = Trim(dt.Rows(0)("comm_svr_setting_id"))
                    txtCommCode.Text = Trim(dt.Rows(0)("comm_svr_setting_code"))
                    txtCommName.Text = Trim(dt.Rows(0)("comm_svr_setting_name"))
                    txtServerIP.Text = Trim(dt.Rows(0)("server_ip"))
                    txtServerName.Text = Trim(dt.Rows(0)("server_name"))
                    txtDBIP.Text = Trim(dt.Rows(0)("db_ip"))
                    txtDBName.Text = Trim(dt.Rows(0)("db_name"))
                    If IsDBNull(dt.Rows(0)("db_port")) Then
                        txtDBPort.Text = ""
                    Else
                        txtDBPort.Text = Trim(dt.Rows(0)("db_port"))
                    End If
                    If IsDBNull(dt.Rows(0)("link_server_name")) Then
                        txtLinkServerName.Text = ""
                    Else
                        txtLinkServerName.Text = Trim(dt.Rows(0)("link_server_name"))
                    End If
                    txtDBUserID.Text = Trim(dt.Rows(0)("db_user_id"))
                    txtURL.Text = Trim(dt.Rows(0)("url"))
                    If IsDBNull(dt.Rows(0)("proxy")) Then
                        txtProxy.Text = ""
                    Else
                        txtProxy.Text = Trim(dt.Rows(0)("proxy"))
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionMsg("ConnectionStringEdit.Page_Load : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
        End Try
    End Sub

#Region "Button operation"
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSave_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objUser As adm_Comm_Svr_Setting.clsCommSvr
        Dim objUserQuery As adm_Comm_Svr_Setting.clsCommSvrQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String = ""
        Dim bUserExist As Boolean

        Try
            'Check login or ConnectionString code exist or not
            objUserQuery = New adm_Comm_Svr_Setting.clsCommSvrQuery
            With objUserQuery
                .clsProperties.strCommCode = Trim(txtCommCode.Text)
                bUserExist = .UserExist()
            End With
            objUserQuery = Nothing

            If bUserExist = False Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('User does not exists!');</script>")
                Exit Sub
            End If

            If chkPassword.Checked Then
                'Check Password match
                If Trim(txtPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtPassword.Text <> "") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                    Exit Sub
                End If

                'Encrypt Password
                objCrypto = New cor_Crypto.clsEncryption
                With objCrypto
                    strEncryptPwd = .TripleDESEncrypt(Trim(txtPassword.Text))
                End With
            End If

            'Insert into ConnectionString Table
            objUser = New adm_Comm_Svr_Setting.clsCommSvr
            With objUser
                If chkPassword.Checked Then
                    .clsProperties.strDBPassword = strEncryptPwd
                    .clsProperties.bChangePwd = True
                Else
                    .clsProperties.bChangePwd = False
                End If
                .clsProperties.strCommID = Trim(lblUserIDValue.Text)
                .clsProperties.strCommCode = Trim(txtCommCode.Text)
                .clsProperties.strCommName = Trim(txtCommName.Text)
                .clsProperties.strDBIP = Trim(txtDBIP.Text)
                .clsProperties.strDBName = Trim(txtDBName.Text)
                .clsProperties.intDBPort = CInt(Trim(txtDBPort.Text))
                .clsProperties.strDBUserID = Trim(txtDBUserID.Text)
                .clsProperties.strLinkServerName = Trim(txtLinkServerName.Text)
                .clsProperties.strProxy = Trim(txtProxy.Text)
                .clsProperties.strURL = Trim(txtURL.Text)
                .clsProperties.strServerIP = Trim(txtServerIP.Text)
                .clsProperties.strServerName = Trim(txtServerName.Text)
                .clsProperties.strActiveFlag = ddlStatusID.SelectedValue
                .clsProperties.strSyncOperation = "A"
                If Session("UserID") <> "" Then
                    .clsProperties.strChangedUserID = Session("UserID")
                Else
                    .clsProperties.strChangedUserID = ""
                End If
                .clsProperties.strChangedDate = Format(Date.Now, "MM/dd/yyyy hh:mm:ss")
                .clsProperties.strActiveFlag = ddlStatusID.SelectedValue
                .clsProperties.strSyncOperation = "A"
                .Update()
            End With
            Response.Redirect("ConnectionStringDtl.aspx?user_code=" & Trim(txtCommCode.Text), False)

        Catch ex As Exception
            ExceptionMsg("ConnectionStringEdit.btnSave_Click : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("ConnectionStringList.aspx", False)
    End Sub
#End Region

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub chkPassword_CheckedChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub chkPassword_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPassword.CheckedChanged
        Try
            If chkPassword.Checked Then
                txtPassword.Enabled = True
                txtConfirmedPassword.Enabled = True
                rfvPassword.Visible = True
                rfvConfirmedPassword.Visible = True
                lblMark3.Visible = True
                lblMark4.Visible = True
            Else
                txtPassword.Enabled = False
                txtConfirmedPassword.Enabled = False
                rfvPassword.Visible = False
                rfvConfirmedPassword.Visible = False
                lblMark3.Visible = False
                lblMark4.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg("ConnectionStringEdit.chkPassword_CheckedChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class