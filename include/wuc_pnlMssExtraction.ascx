﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_pnlMssExtraction.ascx.vb" Inherits="include_wuc_pnlMssExtraction" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtCalendarRange" TagPrefix="customToolkit" %>
<%@ Register src="../iFFMR/Common/wuc_CustSearch.ascx" tagname="wuc_CustSearch" tagprefix="uc1" %>
<link href="~/include/DKSH.css" rel="stylesheet" />

<table id="tblCtrlPnlGen" cellspacing="0" cellpadding="0" width="98%" border="0" style="float:left; padding-left:10px;">
    <tr align="center">
        <td>
             <asp:Button ID="btnCollapse" Text="Click to Expand or Collapse" runat="server" CssClass="cls_button" width="100%"/>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdatePnlExtraction" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMSSExtraction" runat="server">
                        <table cellspacing="0" cellpadding="0" width="98%" border="0">
                            <tr>
                                <td style="width:45%;"></td>
                                <td style="width:10%;"></td>
                                <td style="width:45%;"></td>
                            </tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                    <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                    <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                    <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                    <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblTitle" class="cls_label_header">MSS Title</span><br>
                                    <asp:ListBox ID="lsbTitle" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddTitle" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveTitle" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllTitle" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllTitle" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedTitle" class="cls_label_header">Selected MSS Title</span><br>
                                    <asp:ListBox ID="lsbSelectedTitle" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblQues" class="cls_label_header">MSS Question</span><br>
                                    <asp:ListBox ID="lsbQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddQues" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveQues" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllQues" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllQues" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedQues" class="cls_label_header">Selected MSS Question</span><br>
                                    <asp:ListBox ID="lsbSelectedQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td valign="top" align="center">
                                    <span id="lblSubQues" class="cls_label_header">MSS Sub Question</span><br>
                                    <asp:ListBox ID="lsbSubQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                                <td valign="middle" align="center">
                                    <table>
                                        <tr><td align="center"><asp:Button ID="lnkAddSubQues" runat="server" CssClass="cls_button" Width="35" Text=">" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveSubQues" runat="server" CssClass="cls_button" Width="35" Text="<" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkAddAllSubQues" runat="server" CssClass="cls_button" Width="35" Text=">>" /></td></tr>
                                        <tr><td align="center"><asp:Button ID="lnkRemoveAllSubQues" runat="server" CssClass="cls_button" Width="35" Text="<<" /></td></tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <span id="lblSelectedSubQues" class="cls_label_header">Selected MSS Sub Question</span><br>
                                    <asp:ListBox ID="lsbSelectedSubQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                             <tr>
                                <td valign="top" align="left" style="white-space: nowrap" colspan="3" >
                                    <table width="50%">
                                        <tr>
                                            <td valign="top" ><span class="cls_label_header">Customer </span></td>
                                            <td>
                                                <asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                            &nbsp;<asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" /><br />
                                                            <asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" Width="418px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr valign="top">
                                <td valign="top" align="left" style="white-space: nowrap" colspan="3">
                                    <table width="50%">
                                        <tr>
                                            <td>
                                                <span class="cls_label_header">Txn. Date </span>
                                            </td>
                                            <td>
                                                <customToolkit:wuc_txtCalendarRange ID="Wuc_MSSExtract_Date" runat="server" RequiredValidation="true"
                                                    RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                           
                            <tr>
                                <td colspan="3">
                                    <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                    <asp:Button ID="btnRefresh" CssClass="cls_button" runat="server" Text="Refresh" ValidationGroup="Search"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    
                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlMSSExtraction" runat="server"
                        CollapseControlID="btnCollapse"
                        ExpandControlID="btnCollapse" 
                        TargetControlID="pnlMSSExtraction" 
                        CollapsedSize="0"
                        Collapsed="false" 
                        ExpandDirection="Vertical" 
                        SuppressPostBack="true">
                    </ajaxToolkit:CollapsiblePanelExtender>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<uc1:wuc_CustSearch ID="wuc_CustSearch" Title="Customer Search" runat="server" />