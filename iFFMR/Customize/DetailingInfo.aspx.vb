﻿
Imports System.Data
Imports System.IO

Partial Class iFFMR_Customize_DetailingInfo
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Protected WithEvents ddlTxnNo_Temp As DropDownList

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property
    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
    Public Property strHaveImageInd() As Boolean
        Get
            Return ViewState("strHaveImageInd")
        End Get
        Set(ByVal value As Boolean)
            ViewState("strHaveImageInd") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "DetailingInfo"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.DETINFO) '"SFMS Activities Statistic Information "
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.DETINFO
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""


            If Not IsPostBack Then
                'Param pass from Previous: CUST_CODE , VISIT_ID, SALESREP_CODE , TXN_DATE, PAGE_INDICATOR
                ViewState("dtCurrentView") = Nothing
                'ViewstaeValue : CUST_CODE , VISIT_ID, SALESREP_CODE , TXN_DATE
                ViewState("CUST_CODE") = Trim(Request.QueryString("CUST_CODE"))
                ViewState("VISIT_ID") = Trim(Request.QueryString("VISIT_ID"))
                ViewState("SALESREP_CODE") = Trim(Request.QueryString("SALESREP_CODE"))
                ViewState("TXN_DATE") = Trim(Request.QueryString("TXN_DATE"))
                ViewState("CONT_CODE") = Trim(Request.QueryString("CONT_CODE"))
                Dim strPageIndicator As String = Trim(Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(strPageIndicator) Then
                    Dim strQueryString As String = String.Empty
                    If strPageIndicator = "CALLBYCUSTOMER" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/SFE/CallAnalysis/CallAnalysisListByCustomer.aspx"
                        strQueryString = Session("CallAnalysisListByCustomer_QueryString")
                        'If Not String.IsNullOrEmpty(strQueryString) Then ViewState("POSTBACK_URL") += "?" & strQueryString
                    ElseIf strPageIndicator = "CALLENQUIRY" Then 'CALLENQUIRY
                        ViewState("POSTBACK_URL") = "~/iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx"
                        strQueryString = Session("CallEnquiry_QueryString")
                    End If
                    If Not String.IsNullOrEmpty(strQueryString) Then ViewState("POSTBACK_URL") += "?" & strQueryString
                Else
                    Dim strPreviousPageURL As String = Session("DetailingInfo_QueryString")
                    If Not String.IsNullOrEmpty(strPreviousPageURL) Then ViewState("POSTBACK_URL") = btnBack.PostBackUrl & "?" & strPreviousPageURL
                End If
                TimerControl_1.Enabled = True
                'RenewDataBind()
            End If

            If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then btnBack.PostBackUrl = ViewState("POSTBACK_URL")
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    Try
    '        wuc_ctrlPanel.RefreshDetails()
    '        wuc_ctrlPanel.UpdateControlPanel()
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "Collection")
            Dim aryDgList As New ArrayList
            'dvList.BorderWidth = 1

            tblControl.Visible = False

            aryDgList.Add(tblReport)
            wuc_ctrlPanel.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting

            RefreshDatabinding()
            'Dim blnAllowSorting As Boolean = dgList.AllowSorting
            'Dim blnAllowPaging As Boolean = dgList.AllowPaging

            'dgList.AllowSorting = False
            'dgList.AllowPaging = False
            'RefreshDatabinding()

            'wuc_ctrlPanel.ExportToFile(dgList, "DailyCallActivity")

            'dgList.AllowPaging = blnAllowPaging
            'dgList.AllowSorting = blnAllowSorting
            'RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        RefreshDataBinding2()
        wuc_ctrlPanel.RefreshDetails()
        wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    'Public Sub RefreshDatabinding()
    '    Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
    '    Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
    '    Try
    '        If dtCurrentTable Is Nothing Then
    '            dtCurrentTable = GetRecList()
    '            ViewState("dtCurrentView") = dtCurrentTable
    '            ViewState("strSortExpression") = Nothing
    '        End If
    '        If dtCurrentTable Is Nothing Then
    '            dtCurrentTable = New DataTable
    '        ElseIf dtCurrentTable.Rows.Count = 0 Then
    '            dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
    '        End If
    '        Dim dvCurrentView As New DataView(dtCurrentTable)
    '        If Not String.IsNullOrEmpty(strSortExpression) Then
    '            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
    '            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
    '        End If

    '        dvList.DataSource = dvCurrentView
    '        dvList.DataBind()
    '        dgList.DataSource = dvCurrentView
    '        dgList.DataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
    '    Finally
    '        UpdateDatagrid_Update()
    '    End Try
    'End Sub


    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim dtDataList As DataTable = CType(ViewState("dtDataList"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            dgList.SelectedRowStyle.Reset()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
                dtDataList = GetDataList()
                ViewState("dtDataList") = dtDataList
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
                Master_Row_Count = 0
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            'dvList.DataSource = dtDataList
            'dvList.DataBind()

            Dim dtTxnNo As DataTable = ViewState("DT_TXN_NO")
            If dtTxnNo Is Nothing Then dtTxnNo = Get_TXN_NO_List()

            Dim ddl As DropDownList = Nothing
            'If dvList.Rows.Count > 0 AndAlso dvList.Rows(0).Cells(1).FindControl("ddlTXNNO2") IsNot Nothing Then ddl = dvList.Rows(0).Cells(1).FindControl("ddlTXNNO2")
            ddl = ddlTXNNO2
            If ddl IsNot Nothing Then
                With ddl
                    .DataSource = dtTxnNo
                    .DataTextField = "TXN_NO"
                    .DataValueField = "TXN_NO"
                    .DataBind()
                    If ddl.Items.Count > 0 Then
                        Dim strValue As String = ViewState("TXN_NO")
                        If Not String.IsNullOrEmpty(strValue) AndAlso ddl.Items.FindByValue(strValue) IsNot Nothing Then
                            ddl.SelectedValue = strValue
                        End If
                    End If
                End With
            End If

            PreRenderMode(dtCurrentTable)

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "BIND TXN_NO"

    Protected Sub ddlTXNNO2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddl As DropDownList = Nothing
            'If dvList.Rows.Count > 0 AndAlso dvList.Rows(0).Cells(1).FindControl("ddlTXNNO2") IsNot Nothing Then ddl = dvList.Rows(0).Cells(1).FindControl("ddlTXNNO2")
            ddl = ddlTXNNO2
            If ddl IsNot Nothing Then
                ViewState("TXN_NO") = ddl.SelectedValue
                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlTXNNO2_SelectedIndexChanged : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub ddl_TXNNO_DataBind()
        Try
            Dim dt As DataTable = Get_TXN_NO_List()
            ViewState("DT_TXN_NO") = dt

            ddlTxnNo_Temp = New DropDownList
            With (ddlTxnNo_Temp)
                .DataSource = dt
                .DataTextField = "TXN_NO"
                .DataValueField = "TXN_NO"
                .DataBind()

                If dt.Rows.Count > 0 Then
                    .SelectedIndex = 0
                    ViewState("TXN_NO") = ddlTxnNo_Temp.SelectedValue
                    'RenewDataBind()
                End If
            End With


            Dim ddl As DropDownList = Nothing
            'If dvList.Rows.Count > 0 AndAlso dvList.Rows(0).Cells(1).FindControl("ddlTXNNO2") IsNot Nothing Then ddl = dvList.Rows(0).Cells(1).FindControl("ddlTXNNO2")
            ddl = ddlTXNNO2
            If ddl IsNot Nothing Then
                With ddl
                    .DataSource = dt
                    .DataTextField = "TXN_NO"
                    .DataValueField = "TXN_NO"
                    .DataBind()

                    If dt.Rows.Count > 0 Then
                        .SelectedIndex = 0
                        ddlTXNNO2_SelectedIndexChanged(ddl, Nothing)
                    End If
                End With
            End If

            RefreshDataBinding2()

        Catch ex As Exception
            ExceptionMsg(PageName & ".ddl_TXNNO_DataBind : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Function Get_TXN_NO_List() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : CUST_CODE , VISIT_ID , SALESREP_CODE , TXN_DATE
            Dim strUserID, strPrincipalID, strPrincipalCode, strCustCode, strVisitID, strSalesrepCode, strTXNDate As String
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            strVisitID = ViewState("VISIT_ID")
            strCustCode = ViewState("CUST_CODE")
            strTXNDate = ViewState("TXN_DATE")

            Dim clsCallDB As New rpt_CALL.clsCallQuery
            DT = clsCallDB.GetDetInfo_HDR_DetNO(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strVisitID, strCustCode, strTXNDate)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Get_TXN_NO_List : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            'dgList.Columns.Clear()
            'aryDataItem.Clear()

            aryDataItem.Clear()
            If strHaveImageInd Then
                While dgList.Columns.Count > 1
                    dgList.Columns.RemoveAt(1)
                End While

                aryDataItem.Add("BTN_VIEW")
                dgList.Columns(0).HeaderText = "View Image"
                dgList.Columns(0).HeaderStyle.Width = "25"
                dgList.Columns(0).HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            Else
                dgList.Columns.Clear()
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_DetInfo.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_DetInfo.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_DetInfo.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_DetInfo.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        ''Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
            aryDataItem = _aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        End Try
    End Sub

    Private Function GetDataList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : CUST_CODE , VISIT_ID , SALESREP_CODE , TXN_DATE
            Dim strUserID, strPrincipalID, strPrincipalCode, strCustCode, strVisitID, strSalesrepCode, strTXNDate, strTXN_NO As String
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            strVisitID = ViewState("VISIT_ID")
            strCustCode = ViewState("CUST_CODE")
            strTXNDate = ViewState("TXN_DATE")
            strTXN_NO = ViewState("TXN_NO")

            If String.IsNullOrEmpty(strTXN_NO) Then Exit Try
            Dim clsCallDB As New rpt_CALL.clsCallQuery
            DT = clsCallDB.GetDetInfo_HDR(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strVisitID, strCustCode, strTXNDate, strTXN_NO)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetDataList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE,TXNNO
            Dim strUserID, strPrincipalID, strPrincipalCode, strCustCode, strTeamCode, strVisitID, strSalesrepCode, strTXNDate, strTXN_NO As String
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            strVisitID = ViewState("VISIT_ID")
            strTeamCode = Session("TEAM_CODE")
            strCustCode = ViewState("CUST_CODE")
            strTXNDate = ViewState("TXN_DATE")
            strTXN_NO = ViewState("TXN_NO")

            If Not String.IsNullOrEmpty(strTXNDate) Then wuc_ctrlPanel.SelectedDateTimeValue = Date.ParseExact(strTXNDate, "yyyy-MM-dd", Nothing)
            ViewState("SelectedDate") = wuc_ctrlPanel.SelectedDateTimeString

            If String.IsNullOrEmpty(strTXN_NO) Then Exit Try
            Dim clsCallDB As New rpt_CALL.clsCallQuery
            DT = clsCallDB.GetDetInfo_DET(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strVisitID, strTXNDate, strTXN_NO)

            If DT.Columns.Contains("IMAGE") Then
                strHaveImageInd = True
            Else
                strHaveImageInd = False
            End If

            'dgList_Init(DT)
            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then



                        If strHaveImageInd Then
                            Dim dt As DataTable = ViewState("dtCurrentView")
                            Dim imageExistsind As Boolean = False
                            imageExistsind = IIf(dt.Rows(e.Row.RowIndex)("IMAGE") = "1", True, False)

                            Dim btnViewImg As ImageButton = CType(e.Row.FindControl("View"), ImageButton)
                            If btnViewImg Is Nothing Then
                                btnViewImg = New ImageButton
                                btnViewImg.ID = "btnView"
                                btnViewImg.CommandName = "Edit"
                                btnViewImg.CommandArgument = e.Row.RowIndex
                                btnViewImg.CssClass = "cls_linkbutton"
                                btnViewImg.Width = 20
                                btnViewImg.ImageUrl = "~/images/ico_Edit.gif"
                                btnViewImg.Visible = imageExistsind
                                'btnDelete.Attributes.Add("onclick", "SetViewDetail('ForceShow'); ShowViewDetail(); PositionViewDetail();")
                                e.Row.Cells(0).Controls.Add(btnViewImg)
                                Dim lit As New Literal
                                lit.Text = " "
                                e.Row.Cells(0).Controls.Add(lit)
                            End If
                        End If

                    End If
                Case DataControlRowType.Footer

            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RenewDataBind()
    End Sub

    Protected Sub TimerControl_1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl_1.Tick
        If TimerControl_1.Enabled Then ddl_TXNNO_DataBind()
        TimerControl_1.Enabled = False
    End Sub

#Region "DETAILVIEW"

    Private Function GetRecList2() As Data.DataTable
        Dim DT As DataTable

        DT = GetDataList()

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub RefreshDataBinding2()
        Dim dtCurrenttable2 As Data.DataTable = Nothing
        dtCurrenttable2 = GetRecList2()

        Dim dvCurrentView2 As New Data.DataView(dtCurrenttable2)
        If dtCurrenttable2 IsNot Nothing Then dv_Init(dtCurrenttable2)
        dtviewleft.DataSource = dvCurrentView2
        dtviewleft.DataBind()
        dtviewright.DataSource = dvCurrentView2
        dtviewright.DataBind()


    End Sub

    Protected Sub dv_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim j As Integer = 0
        Dim ColumnNamedv As String = ""

        Try
            dtviewleft.Fields.Clear()
            dtviewright.Fields.Clear()

            For i = 0 To dtToBind.Columns.Count - 1
                ColumnNamedv = dtToBind.Columns(i).ColumnName

                Select Case CF_DetInfo.GetFieldColumnTypeHdr(ColumnNamedv, True)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        j = j + 1
                        Dim dvcolumn As New BoundField

                        dvcolumn = New BoundField

                        Dim strFormatStringdv As String = CF_DetInfo.GetOutputFormatString(ColumnNamedv)
                        If String.IsNullOrEmpty(strFormatStringdv) = False Then
                            dvcolumn.DataFormatString = strFormatStringdv
                            dvcolumn.HtmlEncode = False
                        End If

                        dvcolumn.DataField = ColumnNamedv
                        dvcolumn.HeaderText = CF_DetInfo.GetDisplayColumnName(ColumnNamedv)
                        dvcolumn.ReadOnly = True
                        dvcolumn.SortExpression = ColumnNamedv

                        If j Mod 2 = 0 Then
                            dtviewright.Fields.Add(dvcolumn)
                        Else
                            dtviewleft.Fields.Add(dvcolumn)
                        End If

                        dvcolumn = Nothing
                End Select
            Next

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

End Class

Public Class CF_DetInfo
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strTblName As String = ""
        'CATEGORY , SUB_CAT_CODE , SUB_CAT_NAME , QTY , DTLREMARK , CUST_CODE , CUST_NAME , 
        'CONT_CODE , CONT_NAME , POSITION , GNREMARK 
        Dim strColumnName As String = ColumnName.ToUpper
        Select Case strColumnName
            Case "DET_NAME"
                strTblName = "Detailing"
            Case "DET_CODE"
                strTblName = "Detailing Code"
            Case "SUB_DET_CODE"
                strTblName = "Sub Detailing Code"
            Case "SUB_DET_NAME"
                'strTblName = "Description"
                strTblName = "Sub Detailing"
            Case "REMARKS"  '"DTLREMARK", "GNREMARK"
                strTblName = "Remarks"
            Case "CUST_CODE"
                strTblName = "Customer Code"
            Case "CUST_NAME"
                strTblName = "Customer Name"
            Case "CONT_CODE"
                strTblName = "Contact Code"
            Case "CONT_NAME"
                strTblName = "Contact Name"
            Case "POSITION"
                strTblName = "Position"
            Case "SPECIALITY_NAME"
                strTblName = "Specialty"
            Case "DEPARTMENT_CODE"
                strTblName = "Department"
            Case "TXN_DATE"
                strTblName = "Txn. Date"
            Case "ACT_TXN_DATE"
                strTblName = "Input Date"
                'Case "DESCRIPTION"
                '    strTblName = "Description"
            Case "TXN_DATE"
                strTblName = "Txn Date"
            Case "ACT_TXN_DATE"
                strTblName = "Actual Txn Date"
            Case "EXTRA_CAT_NAME"
                strTblName = "Extra Cat Name"
            Case "EXTRA_DET_NAME"
                strTblName = "Extra Detailing Name"
            Case "EXTRA_DET_CODE"
                strTblName = "Extra Detailing Code"
            Case Else
                strTblName = strColumnName
        End Select

        Return strTblName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByRef ViewOption As Boolean = False) As FieldColumntype
        Try
            'Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            'Dim strColumnName As String = ColumnName.ToUpper
            ''CATEGORY , SUB_CAT_CODE , SUB_CAT_NAME , QTY , DTLREMARK , CUST_CODE , CUST_NAME , 
            ''CONT_CODE , CONT_NAME , POSITION , GNREMARK 
            'Select Case strColumnName
            '    Case "CUST_CODE", "CUST_NAME", "CONT_CODE", "CONT_NAME", "POSITION", "GNREMARK"
            '        FCT = FieldColumntype.InvisibleColumn
            '        'Case "CUST_NAME"
            '        '    FCT = FieldColumntype.HyperlinkColumn
            '    Case Else 'SALESREP_NAME, DATE, CUST_CODE, CUST_NAME, CONT_NAME, DTLREMARK
            '        FCT = FieldColumntype.BoundColumn
            'End Select
            'Return FCT
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If ViewOption = False Then

                Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
                strColumnName = strColumnName.ToUpper
                Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
                If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
                   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                    FCT = FieldColumntype.InvisibleColumn
                ElseIf strColumnName = "TXN_NO" OrElse strColumnName = "IMAGE" Then
                    FCT = FieldColumntype.InvisibleColumn
                Else

                    FCT = FieldColumntype.BoundColumn
                End If

            ElseIf strColumnName = "TXN_NO" OrElse strColumnName = "IMAGE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If



            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetFieldColumnTypeHdr(ByVal strColumnName As String, Optional ByRef ViewOption As Boolean = False) As FieldColumntype
        Try

            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If ViewOption = False Then

                Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
                strColumnName = strColumnName.ToUpper
                Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
                If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
                   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                    FCT = FieldColumntype.InvisibleColumn
                Else

                    FCT = FieldColumntype.BoundColumn
                End If

            Else
                FCT = FieldColumntype.BoundColumn
            End If



            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName
                Case "QTY"
                    strStringFormat = "{0:#,0}"
                Case "TXN_DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case "ACT_TXN_DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
