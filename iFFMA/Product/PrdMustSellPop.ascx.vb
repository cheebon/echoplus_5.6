﻿Imports System.Data

Partial Class iFFMA_Product_PrdMustSellPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property

    Public Property ParamType() As String
        Get
            Return Trim(hdParamType.Value)
        End Get
        Set(ByVal value As String)
            hdParamType.Value = value
        End Set
    End Property

    Public Property ParamValue() As String
        Get
            Return Trim(hdParamValue.Value)
        End Get
        Set(ByVal value As String)
            hdParamValue.Value = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return Trim(hdStartDate.Value)
        End Get
        Set(ByVal value As String)
            hdStartDate.Value = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
    End Sub

    Private Sub LoadTeamCodeDDL(ByVal DT As DataTable)
        Dim dtTeam As DataTable
        Dim clsPrdMustSell As New mst_Product.clsPrdMustSell

        Try
            Dim ddlTeamCode As DropDownList = CType(DetailsView1.FindControl("ddlTeamCode"), DropDownList)
            If ddlTeamCode IsNot Nothing Then
                dtTeam = clsPrdMustSell.GetTeamDDL
                With ddlTeamCode
                    .Items.Clear()
                    .DataSource = dtTeam.DefaultView
                    .DataTextField = "TEAM_NAME"
                    .DataValueField = "TEAM_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("TEAM_NAME")), "", DT.Rows(0)("TEAM_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadSalesrepDDL()
        Dim dtSalesrep As DataTable
        Dim clsPrdMustSell As New mst_Product.clsPrdMustSell

        Try
            Dim ddlTeamCode As DropDownList = CType(DetailsView1.FindControl("ddlTeamCode"), DropDownList)
            Dim ddlSalesrepCode As DropDownList = CType(DetailsView1.FindControl("ddlSalesrepCode"), DropDownList)
            'Dim LeftTeamCodeString As String = ddlTeamCode.SelectedValue.Split("-")(0)
            dtSalesrep = clsPrdMustSell.GetSalesrepDDL(ddlTeamCode.SelectedValue)
            With ddlSalesrepCode
                .Items.Clear()
                .DataSource = dtSalesrep.DefaultView
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadParamTypeDDL(ByVal DT As DataTable)
        Dim dtParamType As DataTable
        Dim clsPrdMustSell As New mst_Product.clsPrdMustSell

        Try
            Dim ddlParamType As DropDownList = CType(DetailsView1.FindControl("ddlParamType"), DropDownList)
            If ddlParamType IsNot Nothing Then
                dtParamType = clsPrdMustSell.GetParamTypeDDL
                With ddlParamType
                    .Items.Clear()
                    .DataSource = dtParamType.DefaultView
                    .DataTextField = "PARAM_NAME"
                    .DataValueField = "PARAM_TYPE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("PARAM_TYPE")), "", DT.Rows(0)("PARAM_NAME"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadParamValueDDL()

        Dim dtParamValue As DataTable
        Dim clsPrdMustSell As New mst_Product.clsPrdMustSell

        Try
            Dim ddlParamType As DropDownList = CType(DetailsView1.FindControl("ddlParamType"), DropDownList)
            Dim ddlParamValue As DropDownList = CType(DetailsView1.FindControl("ddlParamValue"), DropDownList)
            'Dim LeftParamTypeString As String = ddlParamType.SelectedValue.Split("-")(0)
            dtParamValue = clsPrdMustSell.GetParamValueDDL(ddlParamType.SelectedValue)
            With ddlParamValue
                .Items.Clear()
                .DataSource = dtParamValue.DefaultView
                .DataTextField = "PARAM_DESC"
                .DataValueField = "PARAM_VALUE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            Dim txtParamValue As TextBox = CType(DetailsView1.FindControl("txtParamValue"), TextBox)
            Dim rfvCustCode As RequiredFieldValidator = CType(DetailsView1.FindControl("rfvCustCode"), RequiredFieldValidator)
            Dim rfvParamValue As RequiredFieldValidator = CType(DetailsView1.FindControl("rfvParamValue"), RequiredFieldValidator)
            Dim btnSearchCust As Button = CType(DetailsView1.FindControl("btnSearchCust"), Button)
            If ddlParamType.SelectedValue = "cust code" Then
                btnSearchCust.Visible = True
                txtParamValue.Visible = True
                rfvCustCode.Visible = True
                ddlParamValue.Visible = False
                rfvParamValue.Visible = False
            Else
                btnSearchCust.Visible = False
                txtParamValue.Visible = False
                rfvCustCode.Visible = False
                ddlParamValue.Visible = True
                rfvParamValue.Visible = True
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlInsertMode.Visible = False
        btnSave.Visible = False
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        ResetPage()
        pnlViewMode.Visible = False
        pnlInsertMode.Visible = True
        btnSave.Visible = True
        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    'Public Sub LoadDvEditMode()
    '    pnlViewMode.Visible = False
    '    pnlEditMode.Visible = True

    '    DetailsView1.ChangeMode(DetailsViewMode.Edit)
    'End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsPrdMustSell As New mst_Product.clsPrdMustSell

            DT = clsPrdMustSell.GetPrdMustSellDetails(TeamCode, SalesrepCode, ParamType, ParamValue, PrdCode)
            If DT.Rows.Count > 0 Then

                txtDate.DateStart = DT.Rows(0)("START_DATE")
                txtDate.DateEnd = DT.Rows(0)("END_DATE")

                lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("START_DATE")), "", DT.Rows(0)("START_DATE")))
                lblEndDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("END_DATE")), "", DT.Rows(0)("END_DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()
            txtDate.DateStart = ""
            txtDate.DateEnd = ""

            If DT IsNot Nothing Then
                LoadTeamCodeDDL(DT)
                LoadSalesrepDDL()
                LoadParamTypeDDL(DT)
                LoadParamValueDDL()
                'LoadPrdCodeDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub

    Public Sub RenewDataBind()
        ViewState.Clear()

    End Sub

#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Protected Sub ddlTeamCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadSalesrepDDL()
        RenewDataBind()
        Show()
    End Sub

    Protected Sub ddlParamType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadParamValueDDL()
        RenewDataBind()
        Show()
    End Sub

    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdSearch)
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click
        Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
        Try
            txtPrdCode.Text = wuc_PrdSearch.PrdCode + " - " + wuc_PrdSearch.PrdName
            PrdCode = wuc_PrdSearch.PrdCode
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_CustSearch)
                .ResetPage()
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SelectButton_Click
        Try
            Dim txtParamValue As TextBox = CType(DetailsView1.FindControl("txtParamValue"), TextBox)
            txtParamValue.Text = wuc_CustSearch.CustCode

            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ddlTeamCode As DropDownList = CType(DetailsView1.FindControl("ddlTeamCode"), DropDownList)
            Dim ddlSalesrepCode As DropDownList = CType(DetailsView1.FindControl("ddlSalesrepCode"), DropDownList)
            Dim ddlParamType As DropDownList = CType(DetailsView1.FindControl("ddlParamType"), DropDownList)
            Dim ddlParamValue As DropDownList = CType(DetailsView1.FindControl("ddlParamValue"), DropDownList)
            Dim txtParamValue As TextBox = CType(DetailsView1.FindControl("txtParamValue"), TextBox)
            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            Dim txtTarget As TextBox = CType(DetailsView1.FindControl("txtTarget"), TextBox)

            Dim clsPrdMustSell As New mst_Product.clsPrdMustSell

            'If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
            '    Dim DT As DataTable
            '    DT = clsPrdMustSell.UpdatePrdMustSell(TeamCode, ParamType, Trim(txtParamValue.Text), PrdCode)

            '    Dim isExist As Integer
            '    If DT.Rows.Count > 0 Then
            '        isExist = DT.Rows(0)("IS_EXIST")
            '    End If

            '    If isExist = 1 Then
            '        lblInfo.Text = "Param Value not found! Please re-enter!"
            '        LoadDvEditMode()
            '        Show()
            '        Exit Sub
            '    Else
            '        lblInfo.Text = "The record is successfully saved."
            '        LoadDvViewMode()
            '    End If

            'Else

            Dim prdCode As String = txtPrdCode.Text.Split("-")(0)
            Dim DT As DataTable

            If ddlParamType.SelectedValue = "cust code" Then
                DT = clsPrdMustSell.CreatePrdMustSell(Trim(ddlTeamCode.SelectedValue), Trim(ddlSalesrepCode.SelectedValue), Trim(ddlParamType.SelectedValue), Trim(txtParamValue.Text), Trim(prdCode), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), Trim(txtTarget.Text))
            Else
                DT = clsPrdMustSell.CreatePrdMustSell(Trim(ddlTeamCode.SelectedValue), Trim(ddlSalesrepCode.SelectedValue), Trim(ddlParamType.SelectedValue), Trim(ddlParamValue.SelectedValue), Trim(prdCode), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), Trim(txtTarget.Text))
            End If

            Dim isDuplicate As Integer
            Dim isExist As Integer
            If DT.Rows.Count > 0 Then
                isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                isExist = DT.Rows(0)("IS_EXIST")
            End If

            If (isDuplicate = 1 And isExist = 1) Then
                lblInfo.Text = "Record already exists! / Date overlapped!"
                TeamCode = ""
                SalesrepCode = ""
                ParamType = ""
                ParamValue = ""
                prdCode = ""
                LoadDvInsertMode()
                Show()
                Exit Sub
            ElseIf (isDuplicate = 1 And isExist = 0) Then
                lblInfo.Text = "Record already exists! / Date overlapped!"
                TeamCode = ""
                SalesrepCode = ""
                ParamType = ""
                ParamValue = ""
                prdCode = ""
                LoadDvInsertMode()
                Show()
                Exit Sub
            ElseIf (isDuplicate = 0 And isExist = 1) Then
                lblInfo.Text = "Param Value not found! Please re-enter!"
                LoadDvInsertMode()
                Show()
                Exit Sub
            Else
                If ddlParamType.SelectedValue = "cust code" Then
                    lblInfo.Text = "The record is successfully created."
                    'BindDetailsView()
                    TeamCode = ddlTeamCode.SelectedValue.ToString
                    SalesrepCode = ddlSalesrepCode.SelectedValue.ToString
                    ParamType = ddlParamType.SelectedValue
                    ParamValue = txtParamValue.Text.ToString
                    prdCode = txtPrdCode.Text.ToString
                    LoadDvViewMode()
                Else
                    lblInfo.Text = "The record is successfully created."
                    'BindDetailsView()
                    TeamCode = ddlTeamCode.SelectedValue.ToString
                    SalesrepCode = ddlSalesrepCode.SelectedValue.ToString
                    ParamType = ddlParamType.SelectedValue
                    ParamValue = ddlParamValue.SelectedValue.ToString
                    prdCode = txtPrdCode.Text.ToString
                    LoadDvViewMode()
                End If

            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class