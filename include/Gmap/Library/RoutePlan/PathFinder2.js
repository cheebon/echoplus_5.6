﻿var _pf; //reference itself, instead of using 'this' as 'this' can change with context.
var mainCallBackFunction = function () { };
var mainFailCallBackFunction = function () { };

var StartPoint; //store start point to travel
var CurrentPoint; //store current point (cust code)
var count = 0;

function PathFinder(_map, _plannedroute) //Constructor
{
    //Properties
    this.Graph = new Graph(_map);
    this.PlannedRoute = _plannedroute;
    _pf = this;

}

//
//Functions prototype
PathFinder.prototype.setStartPoint = setStartPoint;
PathFinder.prototype.FindShortestPath = findShortestRoute;
PathFinder.prototype.MarkDestination = markDestination;
PathFinder.prototype.hasUnvisited = hasUnvisited;
PathFinder.prototype.isPlannedRoute = isPlannedRoute;
PathFinder.prototype.InsertStartpoint = insertStartPoint;
PathFinder.prototype.getCurrentCustInfo = getCurrentCustInfo;


//
//Function Definition
function setStartPoint(_startPoint) {

    CurrentPoint = _startPoint;
}

function findShortestRoute(_callbackfunc, _failcallbackfunc) {
    if (typeof _callbackfunc == "function") {
        mainCallBackFunction = _callbackfunc;
    }
    if (typeof _callbackfunc == "function") {
        mainFailCallBackFunction = _failcallbackfunc;
    }
  


    if (_pf.hasUnvisited())//if still have neighbours to visit
    {
        _pf.InsertStartpoint(); //To insert starting point to shortest path array

        var nextpoint = _pf.isPlannedRoute(); //check if current point falls in path defined by user; return the defined node and set them to visited

        if (nextpoint != -1) //if current point falls under user defined path, just insert it into the shortest path array
        {
            var wayp = new Node(nextpoint.CustCode, nextpoint.Latitude, nextpoint.Longitude);
            _pf.Graph.ShortestPath.push(wayp); // Insert defined destination to shortest path array
            CurrentPoint = nextpoint.CustCode; // Set current point

            _pf.FindShortestPath(""); //continue loop 
        }
        else //if not fall under defined path, have to find nearest neighbour
        {
            var _current_cust = _pf.getCurrentCustInfo();

            if (_current_cust) {
                _pf.Graph.PopulateNeighbours(_current_cust);
                _pf.Graph.GetNextDestination(CallbackOnGetNextDestination, FailCallBack, _current_cust);
            }
        }

        count++;
        ShowMessage("map_canvas", "Message", "Calculating route " + count + "/" + _pf.Graph.RawGraph.length, "off");    
    }
    else //completed, return results to calling function
    {
        mainCallBackFunction();
    }
}


function markDestination() {
    for (var i = 0; i < _pf.Graph.RawGraph.length; ++i) {
        _pf.Graph.RawGraph[i].IsDestination = false;
        _pf.Graph.RawGraph[i].Visited = false;

        for (j = 0; j < _pf.PlannedRoute.VisitPoints.length; ++j) {
            if (_pf.Graph.RawGraph[i].CustCode == _pf.PlannedRoute.VisitPoints[j].To) {
                _pf.Graph.RawGraph[i].IsDestination = true;
            }
        }
    }
}

function CallbackOnGetNextDestination(result) {
    CurrentPoint = result;
    _pf.FindShortestPath("");
}
function FailCallBack(ErrorCode) {
    mainFailCallBackFunction(ErrorCode);
}

function hasUnvisited() {
    for (var i = 0; i < _pf.Graph.RawGraph.length; ++i) {
        if (!_pf.Graph.RawGraph[i].Visited) {
            return true;
        }
    }
    return false;
}

/*********************************************************************
//
// Check if current point falls in user defined route. If it does, return
// the node info, and set the node to visited
//
**********************************************************************/
function isPlannedRoute() {
    for (var i = 0; i < _pf.PlannedRoute.VisitPoints.length; ++i) {
        if (_pf.PlannedRoute.VisitPoints[i].From == CurrentPoint) {
            /// Locate that node
            for (var j = 0; j < _pf.Graph.RawGraph.length; ++j) {
                if (_pf.Graph.RawGraph[j].CustCode == _pf.PlannedRoute.VisitPoints[i].To) {
                    _pf.Graph.RawGraph[j].Visited = true;
                    return _pf.Graph.RawGraph[j];
                }
            }
        }
    }
    return -1;
}
function insertStartPoint() {
    if (_pf.Graph.ShortestPath.length == 0) {
        for (var i = 0; i < _pf.Graph.RawGraph.length; ++i) {
            if (_pf.Graph.RawGraph[i].CustCode == CurrentPoint) {
                var wayp = new Node(_pf.Graph.RawGraph[i].CustCode, _pf.Graph.RawGraph[i].Latitude, _pf.Graph.RawGraph[i].Longitude);
                _pf.Graph.ShortestPath.push(wayp);
                _pf.Graph.RawGraph[i].Visited = true;
            }
        }
    }
}
function getCurrentCustInfo() {
    for (var i = 0; i < _pf.Graph.RawGraph.length; ++i) {
        if (_pf.Graph.RawGraph[i].CustCode == CurrentPoint) return _pf.Graph.RawGraph[i];
    }
}