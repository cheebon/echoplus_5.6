
'************************************************************************
'	Author	    :	
'	Date	    :	12/06/2008
'	Purpose	    :	Class to build Query for report Call Acty Enquiry
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCProdActivSup
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    'Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallProdcActivSup"
        End Get
    End Property

    Public Function GetCallProdcActivSup(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                            ByVal strYear As String, ByVal strAgencyCode As String, ByVal strTeamCode As String, _
                                            ByVal strSalesrepCode As String, ByVal strGroupingCode As String) As DataTable
        Dim clsCallProdcActivSupDB As clsDB
        Try
            clsCallProdcActivSupDB = New clsDB
            With clsCallProdcActivSupDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_SUPPLIER_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("AGENCY_LIST", strAgencyCode, clsDB.DataType.DBString, True)
                .addItem("TEAM_LIST", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_LIST", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("GROUPING_LIST", strGroupingCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProdcActivSup :" & ex.Message))
        Finally
            clsCallProdcActivSupDB = Nothing
        End Try
    End Function
    Public Function GetCallProd_ACTDTLList_CUSTOMIZE(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, _
    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String, ByVal strCustCode As String, ByVal strCgCode As String, _
    ByVal strCatCode As String, ByVal strSubCat As String, ByVal strAgencyCode As String) As DataTable
        Dim clsCallProdDB As clsDB
        Try
            clsCallProdDB = New clsDB
            With clsCallProdDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_PRDY_ACTY_DTL_CUSTOMIZE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCgCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCat, clsDB.DataType.DBString)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallProd_ACTDTLList :" & ex.Message))
        Finally
            clsCallProdDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
