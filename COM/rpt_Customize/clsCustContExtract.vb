﻿
'************************************************************************
'	Author	    :	Lee Chee Yean
'	Date	    :	26/10/2015
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustContExtract
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsVisitExtract"
        End Get
    End Property

    Public Function GetCustContExtractList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strTeamCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsVisitExtractDB As clsDB
        Try
            clsVisitExtractDB = New clsDB
            With clsVisitExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_EXTRACTION"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)


                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetVisitExtractList :" & ex.Message))
        Finally
            clsVisitExtractDB = Nothing
        End Try
    End Function

    Public Function GetCustContPrdExtractList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strTeamCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsVisitExtractDB As clsDB
        Try
            clsVisitExtractDB = New clsDB
            With clsVisitExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_PRD_EXTRACTION"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)


                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetVisitExtractList :" & ex.Message))
        Finally
            clsVisitExtractDB = Nothing
        End Try
    End Function

    Public Function GetSalesTeam(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsVisitExtractDB As clsDB
        Try
            clsVisitExtractDB = New clsDB
            With clsVisitExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_TEAM"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeam :" & ex.Message))
        Finally
            clsVisitExtractDB = Nothing
        End Try
    End Function

    Public Function GetSalesrep(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsVisitExtractDB As clsDB
        Try
            clsVisitExtractDB = New clsDB
            With clsVisitExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SALESREP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesrep :" & ex.Message))
        Finally
            clsVisitExtractDB = Nothing
        End Try
    End Function



    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class


