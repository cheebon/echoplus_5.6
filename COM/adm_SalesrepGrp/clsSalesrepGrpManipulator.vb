﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsSalesrepGrpManipulator
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")
    Private _salesrepgrp As clsSalesrepGrp

    Public Sub New(ByVal salesrepgrp As clsSalesrepGrp)
        _salesrepgrp = salesrepgrp
    End Sub

    Public Function Create(ByVal strUserID As String) As Long
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_GRP_CREATE"
                .addItem("PRINCIPAL_ID", _salesrepgrp.Principal, cor_DB.clsDB.DataType.DBInt)
                .addItem("SALESREP_GRP_NAME", _salesrepgrp.SalesrepGroupName, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_GRP_DESC", _salesrepgrp.SalesrepGroupDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_VALUE", _salesrepgrp.CriteriaValue, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CREATOR_USER_ID", strUserID, cor_DB.clsDB.DataType.DBDouble)
                Create = .spInsert()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SalesrepGrp.clsSalesrepGrpManipulator.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub Update(ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_GRP_UPDATE"
                .addItem("PRINCIPAL_ID", _salesrepgrp.Principal, cor_DB.clsDB.DataType.DBInt)
                .addItem("SALESREP_GRP_NAME", _salesrepgrp.SalesrepGroupName, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_GRP_DESC", _salesrepgrp.SalesrepGroupDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_VALUE", _salesrepgrp.CriteriaValue, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SALESREP_GRP_ID", _salesrepgrp.SalesrepGrpID, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SalesrepGrp.clsSalesrepGrpManipulator.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CheckNameExists() As Boolean
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_GRP_CHECK"
                .addItem("SALESREP_GRP_NAME", _salesrepgrp.SalesrepGroupName, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve()
                '.spInsert()
            End With
            If ReadValue(dt.Rows(0)("cnt")) > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SalesrepGrp.clsSalesrepGrpManipulator.CheckNameExists" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function RetrieveBySalesrepGrpID() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_GRP_DTL"
                .addItem("SALESREP_GRP_ID", _salesrepgrp.SalesrepGrpID, cor_DB.clsDB.DataType.DBInt)
                RetrieveBySalesrepGrpID = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SalesrepGrp.clsSalesrepGrpManipulator.RetrieveBySalesrepGrpID" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub Delete()
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_GRP_DELETE"
                .addItem("SALESREP_GRP_ID", _salesrepgrp.SalesrepGrpID, cor_DB.clsDB.DataType.DBDouble)
                .spDelete()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SalesrepGrp.clsSalesrepGrpManipulator.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Private Function ReadValue(ByVal obj As Object) As Integer
        If IsDBNull(obj) Then
            Return 0
        ElseIf IsNothing(obj) Then
            Return 0
        ElseIf obj.ToString = "" Then
            Return 0
        Else
            Return Convert.ToInt32(obj)
        End If
    End Function
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
