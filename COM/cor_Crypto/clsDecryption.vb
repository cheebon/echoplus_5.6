' **********************************************************************************************************
'
'	Author	:	
'	Date	:	08-June-2005
'	Purpose	:	To decrypted the data either using triple des decryption method or
'               rsa decryption method
'
'	Revision	: 	
'	---------------------------------------------------------------------------------------------------------
'	|No	|Date Change	|Author		    |Remarks				                                           |	
'	---------------------------------------------------------------------------------------------------------
'	|1	|               |   	        |                                                                  |
'	|2	|			    |	    	    |					                                               |
'	---------------------------------------------------------------------------------------------------------
'
' ***********************************************************************************************************

Option Explicit On 
Option Strict Off

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Interface Icor_Decryption
    Function TripleDESDecrypt(ByVal strData2Decrypt As String) As String
    Function RSADecrypt(ByVal strData2Decrypt As String) As String
End Interface


'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsDecryption
    'Inherits ServicedComponent
    Implements Icor_Decryption

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Public clsProperties As New clsProperties.clsDecryption


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function TripleDESDecrypt
    ' Purpose	:	This function will perform the triple des decryption method
    ' Parameters:	[in]  : strData2Decrypt    
    '		        [out] : Return decrypted data as a string data type
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function TripleDESDecrypt(ByVal strData2Decrypt As String) As String Implements Icor_Decryption.TripleDESDecrypt
        Dim objCrypto As clsCrypto
        Dim m_des As TripleDESCryptoServiceProvider
        Dim m_utf8 As UTF8Encoding = Nothing
        Dim bytKey() As Byte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, _
                              15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
        Dim bytIv() As Byte = {8, 7, 6, 5, 4, 3, 2, 1}
        Dim bytInput() As Byte
        Dim bytOutput() As Byte = Nothing

        Try
            If strData2Decrypt <> "" Then
                objCrypto = New clsCrypto
                m_des = New TripleDESCryptoServiceProvider
                m_utf8 = New UTF8Encoding
                bytInput = Convert.FromBase64String(strData2Decrypt)
                With objCrypto.clsProperties
                    .DocTypeID = clsProperties.DocTypeID
                    .SiteID = clsProperties.SiteID
                End With
                bytOutput = objCrypto.DESTransform(bytInput, m_des.CreateDecryptor(bytKey, bytIv))
            End If
            Return m_utf8.GetString(bytOutput)

            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsDecryption.TripleDESDecrypt : " + ex.ToString))
        Finally
            'set obj = Nothing
            objCrypto = Nothing
            m_des = Nothing
            m_utf8 = Nothing
        End Try
    End Function


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function RSADecrypt
    ' Purpose	:	This function will perform the decryption method using private key
    ' Parameters:	[in]  : strData2Decrypt    
    '		        [out] : Return decrypted data as a string data type
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function RSADecrypt(ByVal strData2Decrypt As String) As String Implements Icor_Decryption.RSADecrypt
        Dim rsa As RSACryptoServiceProvider
        Dim objCrypto As clsCrypto
        Dim reader As StreamReader
        Dim strPublicPrivateKeyXML As String
        Dim strGetData2Decrypt As String
        Dim strDecryptResult As String = ""
        Dim strNonfixlen As String
        Dim strTempshow As String
        Dim bytGetpassword() As Byte
        Dim bytPlain() As Byte
        Dim intChkTerminators As Integer
        Dim intCount As Integer
        Dim arrData2Decrypt As ArrayList

        Try
            RSADecrypt = ""
            If strData2Decrypt <> "" Then
                objCrypto = New clsCrypto
                rsa = New RSACryptoServiceProvider

                With objCrypto
                    .clsProperties.DocTypeID = clsProperties.DocTypeID
                    .clsProperties.SiteID = clsProperties.SiteID
                End With
                rsa = objCrypto.RSAAssignParameter(rsa)

                reader = New StreamReader("C:\privatekey.xml")
                strPublicPrivateKeyXML = reader.ReadToEnd()
                reader.Close()

                'strPublicPrivateKeyXML = rsa.ToXmlString(True)
                arrData2Decrypt = New ArrayList
                arrData2Decrypt.Clear()
                strGetData2Decrypt = strData2Decrypt
                strNonfixlen = Mid(strGetData2Decrypt, 1, Len(strGetData2Decrypt))

                intChkTerminators = InStr(1, strNonfixlen, "|@|")
                While intChkTerminators >= 1
                    strTempshow = Mid(strNonfixlen, 1, intChkTerminators - 1)
                    strNonfixlen = Mid(strNonfixlen, intChkTerminators + 3, Len(strNonfixlen))
                    intChkTerminators = InStr(1, strNonfixlen, "|@|")
                    arrData2Decrypt.Add(strTempshow)
                End While

                For intCount = 0 To arrData2Decrypt.Count - 1
                    Try
                        bytGetpassword = Convert.FromBase64String(arrData2Decrypt.Item(intCount))
                    Catch ex As SystemException
                        Throw (New ExceptionMsg(ex.ToString))
                        Exit Function
                    End Try
                    rsa.FromXmlString(strPublicPrivateKeyXML)
                    bytPlain = rsa.Decrypt(bytGetpassword, False)
                    strDecryptResult = strDecryptResult & System.Text.Encoding.UTF8.GetString(bytPlain)
                Next
                Return strDecryptResult
            End If
            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsDecryption.RSADecrypt : " + ex.ToString))
        Finally
            objCrypto = Nothing
            rsa = Nothing
            arrData2Decrypt = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)

            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
