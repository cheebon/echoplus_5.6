﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsSalesrepGrpQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function SalesrepGrpSearch(ByVal searchtype As String, ByVal searchvalue As String, ByVal usercode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_GRP_LIST"
                .addItem("SEARCH_TYPE", searchtype, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", searchvalue, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_CODE", usercode, cor_DB.clsDB.DataType.DBString)
                SalesrepGrpSearch = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SalesrepGrp.clsSalesrepGrpQuery.SalesrepGrpSearch" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
