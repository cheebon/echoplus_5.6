﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SOVarianceDtl.aspx.vb" Inherits="iFFMR_Customize_SOVariance_SOVarianceDtl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SO Variance Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script type="text/javascript" language="javascript">
        function resizeLayout2() { var dgList = $get('div_dgList'); if (dgList) { dgList.scrollTop = 0; dgList.style.height = Math.max((getFrameHeight('ContentBarIframe') - 70), 0) + 'px'; } }
        window.onresize = function() { resizeLayout2(); }

        function ScrollTop() {
            var dgList = document.getElementById('div_dgList');
            dgList.scrollTop = 0;
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('DetailBar');HideElement('TopBar');ShowElement('ContentBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td valign="top" align="left">
                <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:Panel ID="pnlTreeMenuControl" runat="server" >
                            <customtoolkit:wuc_menu id="wuc_Menu" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <customtoolkit:wuc_ctrlpanel id="wuc_ctrlpanel" runat="server"></customtoolkit:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%"
                                    align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3" style="width: 100%">
                                            <customtoolkit:wuc_lblheader id="wuc_lblHeader" runat="server"></customtoolkit:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3">
                                           
                                         </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <customtoolkit:wuc_updateprogress id="wuc_UpdateProgress1" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                     <div style="padding:5px 5px 5px 5px;">
                                                           <input id="btnBack" type="button" class="cls_button" 
                                                         value="Back" onclick="HideElement('DetailBar');HideElement('ContentBar');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');resetSize('div_dgList','TopBarIframe');" />
                                                    </div>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <ccgv:clsgridview id="dgList" runat="server" showfooter="true" allowpaging="false"
                                                                    allowsorting="true" autogeneratecolumns="false" width="98%" freezeheader="true"
                                                                    gridheight="455" rowselectionenabled="true">
                                                                    </ccgv:clsgridview>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport" style="height: 10px">
                                        <td colspan="3">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
