<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_lblMsgPop.ascx.vb" Inherits="include_wuc_lblMsgPop" %>
<asp:UpdatePanel ID="updPnlMsgPop" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px"
            CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" />
            </asp:Panel>
            <div onkeypress="javascript:return WebForm_FireDefaultButton(event, 'lblPopNotify_btnClose')"
                id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 98%">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <center>
                        <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" />
                    </center>
                </fieldset>
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender 
            ID="ModalPopupMessage" runat="server" 
            BehaviorID="programmaticModalPopupBehavior"
            TargetControlID="btnHidden" 
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>