﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAReport.aspx.vb" Inherits="iFFMR_Customize_TRAReport_TRAReport" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ucpnlRecordNotFound" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="ucctrlpanel" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="ucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ucpnlExtraction" TagName="wuc_pnlExtraction" Src="~/include/wuc_pnlExtraction.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtCalendarRange"
    TagPrefix="customToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TRA Report</title>
    <link href="../../../include/DKSH.css" rel="stylesheet" type="text/css" />
    <script src="../../../include/layout.js" type="text/javascript"></script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="frmTRAReport" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <ucctrlpanel:wuc_ctrlpanel ID="wuc_ctrlpanelTRAReport" runat="server" EnableViewState="true">
                                </ucctrlpanel:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td style="width: 100%">
                                            <uclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uclblHeader:wuc_lblHeader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePnlExtraction" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlExtraction" runat="server">
                                                        <table cellspacing="10" cellpadding="2" border="0">
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <asp:Label ID="lblRTVDate" runat="server" Text="RTV Date" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td valign="top" align="left" style="white-space: nowrap" colspan="3">
                                                                    <customToolkit:wuc_txtCalendarRange ID="wucRTVDate" runat="server" RequiredValidation="false"
                                                                        RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <span id="lblRTVNumber" class="cls_label_header">RTV number</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromRTVNumber" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Label ID="Label11" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtToRTVNumber" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label ID="lblUplDate" runat="server" Text="Upload Date" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td valign="top" align="left" style="white-space: nowrap" colspan="3">
                                                                    <customToolkit:wuc_txtCalendarRange ID="wucUploadDate" runat="server" RequiredValidation="true"
                                                                        RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <span id="lblTeam" class="cls_label_header">Sales Team</span>
                                                                </td>
                                                                
                                                                <td valign="top" align="left">
                                                                    <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
                                                                </td>
                                                              

                                                            </tr>
                                                            
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <span id="lblCustGrp1" class="cls_label_header">Cust Grp 1</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <br />
                                                                    <asp:ListBox ID="lsbCustGrp1" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddCustGrp1" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveCustGrp1" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllCustGrp1" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllCustGrp1" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedCustGrp1" class="cls_label_header">Selected Cust Grp 1</span><br>
                                                                    <asp:ListBox ID="lsbSelectedCustGrp1" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                                <td valign="top">
                                                                    <span id="lblSalesrep" class="cls_label_header">Field Force</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <br />
                                                                    <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                                                    <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <span id="lblSoldTo" class="cls_label_header">Sold To</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromSoldTo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Label ID="Label1" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtToSoldTo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <span id="lblPayer" class="cls_label_header">Payer</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromPayer" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Label ID="Label2" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtToPayer" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <span id="lblShipto" class="cls_label_header">Ship To</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromShipto" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Label ID="Label3" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtToShipto" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <span id="lblVendorCode" class="cls_label_header">Vendor Code</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromVendorCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Label ID="Label9" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtToVendorCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="left">
                                                                    <span id="lblStatus" class="cls_label_header">Status</span>
                                                                </td>
                                                                <td>
                                                                <asp:DropDownList ID="ddltxnstatus" runat="server" CssClass="cls_dropdownlist" ValidationGroup="SearchCriteria">
                                                                            <asp:ListItem Text="P - Pending Approval" Value="P"></asp:ListItem>
                                                                            <asp:ListItem Text="K - KIV TRA" Value="K"></asp:ListItem>
                                                                            <asp:ListItem Text="C - Cancelled TRA" Value="C"></asp:ListItem>
                                                                            <asp:ListItem Text="S - Submitted to SAP" Value="S"></asp:ListItem>
                                                                            <asp:ListItem Text="ALL" Value="" Selected="True"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                </td>
                                                                <td></td><td></td>
                                                                <%--<td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromStatus" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="top" align="center">
                                                                    <asp:Label ID="Label10" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td valign="top" align="center">
                                                                    <asp:TextBox ID="txtToStatus" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>--%>
                                                                <td valign="top" align="left">
                                                                    <span id="lblMatCode" class="cls_label_header">Material Code</span>
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <asp:TextBox ID="txtFromMatCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Label ID="Label8" runat="server" Text="To" CssClass="cls_label_header"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtToMatCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                    <td valign="top" align="left">
                                                                        <span id="lblPrincipal" class="cls_label_header">Principal</span>
                                                                    </td>
                                                                    <td valign="top" align="left">
                                                                        <br />
                                                                        <asp:ListBox ID="lsbPrincipal" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                            Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                    </td>
                                                                    <td valign="middle" align="center">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkAddPrincipal" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkRemovePrincipal" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text="<" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkAddAllPrincipal" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text=">>" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkRemoveAllPrincipal" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text="<<" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center">
                                                                        <span id="lblSelectedPrincipal" class="cls_label_header">Selected Principal</span><br>
                                                                        <asp:ListBox ID="lsbSelectedPrincipal" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                            Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                    </td>
                                                                    <td valign="top" align="left">
                                                                        <span id="lblSalesOffice" class="cls_label_header">Sales Office</span>
                                                                    </td>
                                                                    <td valign="top" align="left">
                                                                        <br />
                                                                        <asp:ListBox ID="lsbSalesOffice" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                            Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                    </td>
                                                                    <td valign="middle" align="center">
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkAddSalesOffice" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text=">" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkRemoveSalesOffice" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text="<" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkAddAllSalesOffice" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text=">>" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Button ID="lnkRemoveAllSalesOffice" runat="server" CssClass="cls_button" Width="35"
                                                                                        Text="<<" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="center">
                                                                        <span id="lblSelectedSalesOffice" class="cls_label_header">Selected Sales Office</span><br>
                                                                        <asp:ListBox ID="lsbSelectedSalesOffice" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                            Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                    </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Button ID="btnReset" CssClass="cls_button" runat="server" Text="Reset"></asp:Button>
                                                                    <asp:Button ID="btnRefresh" CssClass="cls_button" runat="server" Text="Refresh" ValidationGroup="Search">
                                                                    </asp:Button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlSFMSExtraction" runat="server" CollapseControlID="wuc_ctrlpanelTRAReport_imgExpandCollapse"
                                                        ExpandControlID="wuc_ctrlpanelTRAReport_imgExpandCollapse" TargetControlID="pnlExtraction"
                                                        CollapsedSize="0" Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" style="padding: 10px 5px 10px 5px;">
                                            <asp:Button ID="btnExportAsCsv" runat="server" Text="Export As CSV" CssClass="cls_button"
                                                Style="display: none;" />
                                            <asp:Button ID="btnExportAsWebExcel" runat="server" Text="Direct Export to Excel"
                                                CssClass="cls_button" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="center" style="width: 95%;">
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                <ucdgpaging:wuc_dgpaging ID="wuc_dgpaging" runat="server"></ucdgpaging:wuc_dgpaging>
                                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                                                                    <EmptyDataTemplate>
                                                                        <ucpnlRecordNotFound:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server"
                                                                            ShowPanel="true" />
                                                                    </EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td style="height: 5px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
