'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	15/05/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPackage
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPackage"
        End Get
    End Property

#Region "PACKAGE HDR"
    Public Function GetPackageHdrList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_HDR_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPackageHdrDetails(ByVal strTeamCode As String, ByVal strPackageCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_HDR_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePackageHdr(ByVal strTeamCode As String, ByVal strPackageCode As String, ByVal strPackageName As String, ByVal strType As String, _
                                    ByVal strQty As String, ByVal strAmt As String, ByVal strGoodsMul As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strPackageSpec As String, ByVal strRegionCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_HDR_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PACKAGE_NAME", strPackageName, cor_DB.clsDB.DataType.DBString)
                .addItem("TYPE", strType, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("AMT", strAmt, cor_DB.clsDB.DataType.DBString)
                .addItem("GOODS_MUL", strGoodsMul, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("PACKAGE_SPEC", strPackageSpec, cor_DB.clsDB.DataType.DBString)
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdatePackageHdr(ByVal strTeamCode As String, ByVal strPackageCode As String, ByVal strPackageName As String, ByVal strType As String, _
                                    ByVal strQty As String, ByVal strAmt As String, ByVal strGoodsMul As String, _
                                    ByVal strStartDate As String, ByVal strEndDate As String, ByVal strPackageSpec As String, ByVal strRegionCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_HDR_UPDATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PACKAGE_NAME", strPackageName, cor_DB.clsDB.DataType.DBString)
                .addItem("TYPE", strType, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("AMT", strAmt, cor_DB.clsDB.DataType.DBString)
                .addItem("GOODS_MUL", strGoodsMul, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("PACKAGE_SPEC", strPackageSpec, cor_DB.clsDB.DataType.DBString)
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeletePackageHdr(ByVal strTeamCode As String, ByVal strPackageCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_HDR_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "PACKAGE PRD"
    Public Function GetPackagePrdList(ByVal strPackageCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_PRD_LIST"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPackagePrdDetails(ByVal strPackageCode As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_PRD_DTL"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePackagePrd(ByVal strPackageCode As String, ByVal strPrdCode As String, ByVal strUOMCode As String, _
                                    ByVal strQty As String, ByVal strMustInd As String, ByVal strPrdListPrice As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_PRD_CREATE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("MUST_IND", strMustInd, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_LISTPRICE", strPrdListPrice, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdatePackagePrd(ByVal strPackageCode As String, ByVal strPrdCode As String, ByVal strUOMCode As String, _
                                    ByVal strQty As String, ByVal strMustInd As String, ByVal strPrdListPrice As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_PRD_UPDATE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("MUST_IND", strMustInd, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_LISTPRICE", strPrdListPrice, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeletePackagePrd(ByVal strPackageCode As String, ByVal strPrdCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_PRD_DELETE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "PACKAGE FOC"
    Public Function GetPackageFOCList(ByVal strPackageCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_FOC_LIST"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPackageFOCDetails(ByVal strPackageCode As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_FOC_DTL"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePackageFOC(ByVal strPackageCode As String, ByVal strPrdCode As String, ByVal strUOMCode As String, _
                                    ByVal strQty As String, ByVal strMustInd As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_FOC_CREATE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("MUST_IND", strMustInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdatePackageFOC(ByVal strPackageCode As String, ByVal strPrdCode As String, ByVal strUOMCode As String, _
                                    ByVal strQty As String, ByVal strMustInd As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_FOC_UPDATE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QTY", strQty, cor_DB.clsDB.DataType.DBString)
                .addItem("MUST_IND", strMustInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeletePackageFOC(ByVal strPackageCode As String, ByVal strPrdCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_FOC_DELETE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "PACKAGE CUSTOMER"
    Public Function GetPackageCustList(ByVal strPackageCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_CUST_LIST"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPackageCustDetails(ByVal strPackageCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_CUST_DTL"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePackageCust(ByVal strPackageCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_CUST_CREATE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeletePackageCust(ByVal strPackageCode As String, ByVal strCustCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PACKAGE_CUST_DELETE"
                .addItem("PACKAGE_CODE", strPackageCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class


