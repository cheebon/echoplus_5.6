﻿
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsTRAReportEDI
    Implements IDisposable
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTRAReportEDI"
        End Get
    End Property

    Public Function GetTRAReportEDI(ByVal strUserID As String, _
    ByVal STRTVDateFRom As String, ByVal strRTVDateTo As String, ByVal strUploadDateFrom As String, ByVal strUploadDateTo As String, ByVal strRTVNumberFrom As String, ByVal strRTVNumberTo As String, ByVal strSalesTeam As String, ByVal strCustGrp1From As String, ByVal strCustGrp1To As String, _
    ByVal strSoldToFrom As String, ByVal strSoldToTo As String, ByVal strPayerFrom As String, ByVal strPayerTo As String, ByVal strShipToFrom As String, ByVal strShipToTo As String, _
    ByVal strSalesOfficeFrom As String, ByVal strSalesOfficeTo As String, ByVal strSalesrepCodeFrom As String, ByVal strSalesrepCodeTo As String, ByVal strMatGrpFrom As String, ByVal strMatGrpTo As String, _
    ByVal strMatCodeFrom As String, ByVal strMatCodeTo As String, ByVal strVendorCodeFrom As String, ByVal strVendorCodeTo As String, ByVal strStatus As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_TRA_REPORT_EDI"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("RTVDATE_FROM", STRTVDateFRom, clsDB.DataType.DBString)
                .addItem("RTVDATE_TO", strRTVDateTo, clsDB.DataType.DBString)
                .addItem("UPLOADDATE_FROM", strUploadDateFrom, clsDB.DataType.DBString)
                .addItem("UPLOADDATE_TO", strUploadDateTo, clsDB.DataType.DBString)
                .addItem("RTVNUMBER_FROM", strRTVNumberFrom, clsDB.DataType.DBString)
                .addItem("RTVNUMBER_TO", strRTVNumberTo, clsDB.DataType.DBString)
                .addItem("SALESTEAM", strSalesTeam, clsDB.DataType.DBString)
                .addItem("CUSTGRP1_FROM", strCustGrp1From, clsDB.DataType.DBString)
                .addItem("CUSTGRP1_TO", strCustGrp1To, clsDB.DataType.DBString)
                .addItem("SOLDTO_FROM", strSoldToFrom, clsDB.DataType.DBString)
                .addItem("SOLDTO_TO", strSoldToTo, clsDB.DataType.DBString)
                .addItem("PAYER_FROM", strPayerFrom, clsDB.DataType.DBString)
                .addItem("PAYER_TO", strPayerTo, clsDB.DataType.DBString)
                .addItem("SHIPTO_FROM", strShipToFrom, clsDB.DataType.DBString)
                .addItem("SHIPTO_TO", strShipToTo, clsDB.DataType.DBString)
                .addItem("SALESOFFICE_FROM", strSalesOfficeFrom, clsDB.DataType.DBString)
                .addItem("SALESOFFICE_TO", strSalesOfficeTo, clsDB.DataType.DBString)
                .addItem("SALEREPCODE_FROM", strSalesrepCodeFrom, clsDB.DataType.DBString)
                .addItem("SALEREPCODE_TO", strSalesrepCodeTo, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_FROM", strMatGrpFrom, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_TO", strMatGrpTo, clsDB.DataType.DBString)
                .addItem("MATCODE_FROM", strMatCodeFrom, clsDB.DataType.DBString)
                .addItem("MATCODE_TO", strMatCodeTo, clsDB.DataType.DBString)
                .addItem("VENDORCODE_FROM", strVendorCodeFrom, clsDB.DataType.DBString)
                .addItem("VENDORCODE_TO", strVendorCodeTo, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".clsTRAReportEDI :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetTRAReportSalesOfficeDDL(ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_TRA_REPORT_EDI_SALESOFFICE_DDL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTRAReportSalesOfficeDDL :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetTRAReportPrincipalDDL(ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_TRA_REPORT_EDI_PRINCIPAL_DDL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTRAReportSalesOfficeDDL :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetTRAReportSalesrepDDL(ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_TRA_REPORT_EDI_SALESREP_DDL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTRAReportSalesOfficeDDL :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetTRAReportCustGrpDDL(ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_TRA_REPORT_EDI_CUSTGRP_DDL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTRAReportSalesOfficeDDL :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
