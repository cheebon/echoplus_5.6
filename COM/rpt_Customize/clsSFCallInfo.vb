'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	2008-04-10
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSFCallInfo
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFCallInfo"
        End Get
    End Property
    Public Function GetSFCallInfoList() As DataTable
        Dim clsIntDB As clsDB
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESFORCE_CALL_INFO_BY_MONTH"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", properties.TeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", properties.SalesrepCode, clsDB.DataType.DBString, True)
                .addItem("PTL_CODE", properties.PTLCode, clsDB.DataType.DBString, True)

                GetSFCallInfoList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFCallInfoList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return GetSFCallInfoList
    End Function

    Public Function GetPrdSpecialty(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_PRD_SPECIALTY"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdSpecialty :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetDailyCallActy() As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DAILY_CALL_ACTY"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("MONTH", properties.Month, clsDB.DataType.DBString)
                .addItem("SALESREP_LIST", properties.SalesrepCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", properties.SubCatCode, clsDB.DataType.DBString)
                .addItem("CLASS", properties.strClass, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDailyCallActy :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
