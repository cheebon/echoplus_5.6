Imports System.Data

Partial Class iFFMA_Mapping_FieldForceActyPop
    Inherits System.Web.UI.UserControl

    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

    Public Event SelectButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

            'Call Paging
            'With wuc_dgActyPoppaging
            '    '.PageCount = dgActyPopList.PageCount
            '    '.CurrentPageIndex = dgActyPopList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not Page.IsPostBack Then
                DataBind()
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "PROPERTY"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()

        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgActyPopList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgActyPopList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgActyPopList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function

    Public Sub ResetPage()
        LoadDDLCat()
        LoadDDLSubCat()

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDLCat()
        Dim dtSFMSCat As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSFMSCat = clsCommon.GetSFMSCatDDL(TeamCode)
            With ddlCat
                .Items.Clear()
                .DataSource = dtSFMSCat.DefaultView
                .DataTextField = "CAT_NAME"
                .DataValueField = "CAT_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLSubCat()
        Dim dtSFMSSubCat As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSFMSSubCat = clsCommon.GetSFMSSubCatDDL(TeamCode, ddlCat.SelectedValue)
            With ddlSubCat
                .Items.Clear()
                .DataSource = dtSFMSSubCat.DefaultView
                .DataTextField = "SUB_CAT_NAME"
                .DataValueField = "SUB_CAT_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Public Overrides Sub DataBind()

        lblContent.Text = strMessage
        lblTitle.Text = strTitle

        updPnlMaintenance.Update()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RenewDataBind()
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    RaiseEvent CloseButton_Click(sender, e)
    'End Sub

    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            lblInfo.Text = ""

            Dim clsFieldForceActy As New mst_Mapping.clsFieldForceActy
            Dim strCatCode As String = ""
            Dim strSubCatCode As String = ""

            Dim strTarget As TextBox = CType(pnlMsgPop.FindControl("txtTarget"), TextBox)

            If dgActyPopList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0

                For Each DR As GridViewRow In dgActyPopList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        strCatCode = dgActyPopList.DataKeys(i)("CAT_CODE")
                        strSubCatCode = dgActyPopList.DataKeys(i)("SUB_CAT_CODE")

                        clsFieldForceActy.CreateFieldForceActy(strSubCatCode, TeamCode, strCatCode, SalesrepCode, strTarget.Text)
                    End If
                    i += 1
                Next
            End If

            If strCatCode = "" Then lblInfo.Text = "Please select at least 1 item to proceed!" : Show() : Exit Sub

            lblInfo.Text = "The selected item(s) was successfully added! Please click on Close button when finish assigning."
            RenewDataBind()

            RaiseEvent SelectButton_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCat.SelectedIndexChanged
        LoadDDLSubCat()
        RenewDataBind()
        Show()
    End Sub

    Protected Sub ddlSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubCat.SelectedIndexChanged
        RenewDataBind()
        Show()
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgActyPopList.PageIndex = 0
        wuc_dgActyPoppaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldForceActy As New mst_Mapping.clsFieldForceActy

            DT = clsFieldForceActy.GetFieldForceActyExcludeList(ddlCat.SelectedValue, ddlSubCat.SelectedValue, TeamCode, SalesrepCode)
            dgActyPopList.DataKeyNames = New String() {"CAT_CODE", "SUB_CAT_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgActyPopList"
    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgActyPopList.PageIndex = 0
            '    wuc_dgActyPoppaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgActyPopList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(Master_Row_Count > 0, True, False)
                .DataBind()
            End With

            'Call Paging
            With wuc_dgActyPoppaging
                .PageCount = dgActyPopList.PageCount
                .CurrentPageIndex = dgActyPopList.PageIndex
                .DataBind()
                .RowCount = dvCurrentView.Count
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            updPnlMaintenance.Update()
            RaiseEvent CloseButton_Click(Page, Nothing)
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgActyPopList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgActyPopList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()

            'CHECKBOX
            aryDataItem.Add("chkSelect")
            While dgActyPopList.Columns.Count > 1
                dgActyPopList.Columns.RemoveAt(1)
            End While
            dgActyPopList.Columns(0).HeaderStyle.Width = "25"
            dgActyPopList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldForceActyPop.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldForceActyPop.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldForceActyPop.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldForceActyPop.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldForceActyPop.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgActyPopList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgActyPopList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgActyPopList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgActyPopList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgActyPopList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If chk Is Nothing Then
                    chk = New CheckBox
                    chk.ID = "chkSelect"
                    e.Row.Cells(0).Controls.Add(chk)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub dgActyPopList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgActyPopList.SelectedIndexChanged
    '    Try
    '        lblInfo.Text = ""

    '        Dim strCatCode As String = sender.SelectedDataKey.Item("CAT_CODE").ToString
    '        Dim strSubCatCode As String = sender.SelectedDataKey.Item("SUB_CAT_CODE").ToString

    '        Dim clsFieldForceActy As New mst_Mapping.clsFieldForceActy

    '        clsFieldForceActy.CreateFieldForceActy(strSubCatCode, TeamCode, strCatCode, SalesrepCode)

    '        lblInfo.Text = "The selected item(s) was successfully added! Please click on Close button when finish assigning."
    '        RenewDataBind()

    '        RaiseEvent SelectButton_Click(sender, e)

    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try

    'End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgActyPoppaging.Go_Click
        Try
            dgActyPopList.PageIndex = CInt(wuc_dgActyPoppaging.PageNo - 1)

            dgActyPopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgActyPoppaging.Previous_Click
        Try
            If dgActyPopList.PageIndex > 0 Then
                dgActyPopList.PageIndex = dgActyPopList.PageIndex - 1
            End If
            wuc_dgActyPoppaging.PageNo = dgActyPopList.PageIndex + 1

            dgActyPopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgActyPoppaging.Next_Click
        Try
            If dgActyPopList.PageCount - 1 > dgActyPopList.PageIndex Then
                dgActyPopList.PageIndex = dgActyPopList.PageIndex + 1
            End If
            wuc_dgActyPoppaging.PageNo = dgActyPopList.PageIndex + 1

            dgActyPopList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_FieldForceActyPop
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "CAT_CODE"
                strFieldName = "Category Code"
            Case "CAT_NAME"
                strFieldName = "Category Name"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            FCT = FieldColumntype.BoundColumn

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*QTY" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" Then
                strFormatString = "{0:#,0.00}"
            ElseIf strColumnName Like "*DBL" Then
                strFormatString = "{0:#,0.0000}"
            ElseIf strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName = "BLANK" Then
                strFormatString = "{0:#,0}"
            Else
                strFormatString = ""
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
