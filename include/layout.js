﻿// 2008-10-24 //Control the design layout
function ShowElement(element){var TopDiv =self.parent.document.getElementById(element);  TopDiv.style.display='';}
function HideElement(element){var TopDiv =self.parent.document.getElementById(element);  TopDiv.style.display='none';}
function ShowHideElement(element){ var TopDiv =self.parent.document.getElementById(element); var value;if (TopDiv){   if(TopDiv.style.display=='none'){TopDiv.style.display='';}else {TopDiv.style.display='none';}}}
function MaximiseFrameHeight(element,addonValue){if(!addonValue){addonValue=0;}parent.parent.parent.MaximimzeWindow();var GridFrame =self.parent.document.getElementById(element);if (GridFrame)  {GridFrame.height=Math.max((getViewportParentSize()[1] - GridFrame.offsetTop  + addonValue),0)+'px';}}
function getViewportParentSize() { var size = [0, 0]; if (typeof self.parent.window.innerWidth != 'undefined') { size = [ self.parent.window.innerWidth, self.parent.window.innerHeight ]; } else if (typeof self.parent.document.documentElement != 'undefined' && typeof self.parent.document.documentElement.clientWidth != 'undefined' && self.parent.document.documentElement.clientWidth != 0) { size = [ self.parent.document.documentElement.clientWidth, self.parent.document.documentElement.clientHeight ]; } else { size = [ self.parent.document.getElementsByTagName('body')[0].clientWidth, self.parent.document.getElementsByTagName('body')[0].clientHeight ]; } return size; }
function getViewportSize() { var size = [0, 0]; if (typeof window.innerWidth != 'undefined') { size = [ window.innerWidth, window.innerHeight ]; } else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0) { size = [ document.documentElement.clientWidth, document.documentElement.clientHeight ]; } else { size = [ document.getElementsByTagName('body')[0].clientWidth, document.getElementsByTagName('body')[0].clientHeight ]; } return size; }
function getFrameHeight(element){var GridFrame =self.parent.document.getElementById(element);if (GridFrame)  {return Math.max((getViewportParentSize()[1] - GridFrame.offsetTop - 15),0);}}
function resetSize(element,frameElement){parent.parent.parent.MaximimzeWindow();var dgList = $get(element);if (dgList){var offset=70;if(frameElement=='DetailBarIframe'){offset=80;} dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight(frameElement) - offset),0)+'px';}}
function resizeLayout(){var dgList = $get('div_dgList');if (dgList){dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight('fraContent') - 90),0)+'px';}}
//window.onresize=function(){resizeLayout();}
function resizeLayout2(element){var dgList = $get(element);if (dgList){dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight('fraContent') - 90),0)+'px';}}
function resizeLayout3(gridElement,frameElement,defaultSize){var dgList = $get(gridElement);if (dgList){dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight(frameElement) - defaultSize),0)+'px';}}
//window.onresize=function(){resizeLayout();}
function refreshDatabind(){javascript:__doPostBack('btnRefresh','');}
function ResizeFrameHeight(element,size){var GridFrame =self.parent.document.getElementById(element);if (GridFrame)  {GridFrame.style.height=size;}}
function ResizeFrameWidth(element,size){var GridFrame =self.parent.document.getElementById(element);if (GridFrame)  {GridFrame.style.width=size;}}
function ReloadIframe(element){if(self.parent.document.getElementById(element).src){self.parent.document.getElementById(element).src=self.parent.document.getElementById(element).src;}}
function MaximiseGridHeight(element, offset) { /*$(element).height(0);*/$(element).height(getGridDocHeight(element) - offset); }
function getGridDocHeight() { return $(window).height(); }
