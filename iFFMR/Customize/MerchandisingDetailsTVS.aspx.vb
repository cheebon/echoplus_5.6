﻿Imports System.Data
Imports System.IO

Partial Class iFFMR_Customize_MerchandisingDetails
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
    Public Property strHaveImageInd() As Boolean
        Get
            Return ViewState("strHaveImageInd")
        End Get
        Set(ByVal value As Boolean)
            ViewState("strHaveImageInd") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_MerchandisingDetails"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MERCHANDISINGDETAIL) '"SFMS Activities Statistic Information "
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.MERCHANDISINGDETAIL
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""


            If Not IsPostBack Then
                'Param pass from Previous: CUST_CODE , VISIT_ID, SALESREP_CODE , TXN_DATE, PAGE_INDICATOR
                ViewState("dtCurrentView") = Nothing
                'ViewstaeValue : CUST_CODE , VISIT_ID, SALESREP_CODE , TXN_DATE

                ViewState("SALESREP_CODE") = Trim(Request.QueryString("SALESREP_CODE"))
                ViewState("PRD_CODE") = Trim(Request.QueryString("PRD_CODE"))
                ViewState("ACTIVITY_CODE") = Trim(Request.QueryString("ACTIVITY_CODE"))
                Dim strPageIndicator As String = Trim(Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(strPageIndicator) Then
                    Dim strQueryString As String = String.Empty
                    If strPageIndicator = "MERCHANDISING" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/Merchandising.aspx"
                        strQueryString = Session("Merchandising_QueryString")
                    End If
                    If Not String.IsNullOrEmpty(strQueryString) Then ViewState("POSTBACK_URL") += "?" & strQueryString
                Else
                    Dim strPreviousPageURL As String = Session("MerchandisingDetailsTVS_QueryString")
                    If Not String.IsNullOrEmpty(strPreviousPageURL) Then ViewState("POSTBACK_URL") = btnBack.PostBackUrl & "?" & strPreviousPageURL
                End If
                TimerControl1.Enabled = True
                'RenewDataBind()
            End If

            If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then btnBack.PostBackUrl = ViewState("POSTBACK_URL")
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "MerchandisingDetailsTVS"
        End Get
    End Property

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

#Region "Event Handler"

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
        RenewDataBind()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        RefreshDataBind()
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_ctrlpanel.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            dgList.SelectedRowStyle.Reset()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
                Master_Row_Count = 0
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If

            'If dtCurrentTable Is Nothing Then
            '    dtCurrentTable = GetRecList()
            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            'End If
            'If dtCurrentTable Is Nothing Then
            '    dtCurrentTable = New DataTable
            'Else
            '    PreRenderMode(dtCurrentTable)
            '    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            'End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            PreRenderMode(dtCurrentTable)

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 10 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'aryDataItem.Clear()
        If strHaveImageInd Then
            While dgList.Columns.Count > 1
                dgList.Columns.RemoveAt(1)
            End While

            aryDataItem.Add("BTN_VIEW")
            dgList.Columns(0).HeaderText = "View Image"
            dgList.Columns(0).HeaderStyle.Width = "25"
            dgList.Columns(0).HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center
        Else
            dgList.Columns.Clear()
        End If

        Try
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_MerchandisingDetailsTVS.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MerchandisingDetailsTVS.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MerchandisingDetailsTVS.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_MerchandisingDetailsTVS.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        ''Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
            aryDataItem = _aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As DataTable

        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : SALESREP_CODE, VISIT_ID, CUST_CODE, TXN_DATE,TXNNO
            Dim strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strPrdCode, strActivityCode, strYear, strMonth As String
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            strPrdCode = ViewState("PRD_CODE")
            strActivityCode = ViewState("ACTIVITY_CODE")
            strYear = Session("YEAR")
            strMonth = Session("MONTH")

            Dim clsMerchandisingDetails As New rpt_Customize.clsMerchandising
            DT = clsMerchandisingDetails.GetMerchandisingDetailsTVS(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strPrdCode, strActivityCode, strYear, strMonth)

            If DT.Columns.Contains("IMAGE") Then
                strHaveImageInd = True
            Else
                strHaveImageInd = False
            End If

            'dgList_Init(DT)
            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        If strHaveImageInd Then
                            Dim dt As DataTable = ViewState("dtCurrentView")
                            Dim imageExistsind As Boolean = False
                            imageExistsind = IIf(dt.Rows(e.Row.RowIndex)("IMAGE") = "1", True, False)

                            Dim btnViewImg As ImageButton = CType(e.Row.FindControl("View"), ImageButton)
                            If btnViewImg Is Nothing Then
                                btnViewImg = New ImageButton
                                btnViewImg.ID = "btnView"
                                btnViewImg.CommandName = "Edit"
                                btnViewImg.CommandArgument = e.Row.RowIndex
                                btnViewImg.CssClass = "cls_linkbutton"
                                btnViewImg.Width = 20
                                btnViewImg.ImageUrl = "~/images/ico_Edit.gif"
                                btnViewImg.Visible = imageExistsind
                                'btnDelete.Attributes.Add("onclick", "SetViewDetail('ForceShow'); ShowViewDetail(); PositionViewDetail();")
                                e.Row.Cells(0).Controls.Add(btnViewImg)
                                Dim lit As New Literal
                                lit.Text = " "
                                e.Row.Cells(0).Controls.Add(lit)
                            End If
                        End If

                    End If
                Case DataControlRowType.Footer

            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try

            Dim strPrdCode As String = sender.datakeys(e.NewEditIndex).item("PRD_CODE")
            Dim strActivityCode As String = sender.datakeys(e.NewEditIndex).item("HD_ACTIVITY_CODE")
            Dim strCustCode As String = sender.datakeys(e.NewEditIndex).item("CUST_CODE")
            Dim strTxnNo As String = sender.datakeys(e.NewEditIndex).item("TXN_NO")

            hfDetailImgCatCode.Value = strPrdCode
            hfDetailImgSubCatCode.Value = strActivityCode
            hfDetailImgCustCode.Value = strCustCode
            hfDetailImgTxnNo.Value = strTxnNo
            LoadDtlImage()

            UpdateViewDetailImg.Update()

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "OpenDetail", "SetViewDetailImg('ForceShow'); ShowViewDetailImg(); PositionViewDetailImg(); ", True)

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowEditing : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#Region "Image"

    Private Sub LoadDtlImage()
        Dim dt As DataTable = Nothing
        Dim cls_Mss As New rpt_Customize.clsMerchandising
        Try

            Dim strPrdCode As String = hfDetailImgCatCode.Value
            Dim strActivityCode As String = hfDetailImgSubCatCode.Value
            Dim strCustCode As String = hfDetailImgCustCode.Value
            Dim strTxnNo As String = hfDetailImgTxnNo.Value
            Dim strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode As String
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = ViewState("SALESREP_CODE")
            'strPrdCode = ViewState("PRD_CODE")
            'strActivityCode = ViewState("ACTIVITY_CODE")

            With cls_Mss
                dt = .GetMerchandisingDetailsImage(strUserID, strPrincipalID, strPrincipalCode, strSalesrepCode, strPrdCode, strActivityCode, strCustCode, strTxnNo)
            End With

            If dt.Rows.Count > 0 Then
                If Not IsNothing(Portal.Util.GetValue(Of Byte())(dt.Rows(0)("IMAGE"))) Then
                    Dim strfn As String = strTxnNo + "_" + strPrdCode + "_" + strActivityCode + "_" + Portal.UserSession.UserID
                    Dim strFileName As String = strfn
                    strfn = System.Web.HttpContext.Current.Server.MapPath("~/") + "Documents\ReportTmp\" + strfn + ".jpg"
                    If (System.IO.File.Exists(strfn)) Then
                        System.IO.File.Delete(strfn)
                    End If
                    Dim fs As New FileStream(strfn, FileMode.CreateNew, FileAccess.Write)
                    Dim blob As Byte() = DirectCast(Portal.Util.GetValue(Of Byte())(dt.Rows(0)("IMAGE")), Byte())
                    fs.Write(blob, 0, blob.Length)
                    imgMssDtl.ImageUrl = "~/Documents/ReportTmp/" + strFileName + ".jpg"
                    imgMssDtl.Visible = True
                    fs.Close()
                    fs = Nothing
                Else
                    imgMssDtl.Visible = False
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadDtlImage : " & ex.ToString)
        End Try
    End Sub


#End Region
End Class

Public Class CF_MerchandisingDetailsTVS
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strTblName As String = ""
        'CATEGORY , SUB_CAT_CODE , SUB_CAT_NAME , QTY , DTLREMARK , CUST_CODE , CUST_NAME , 
        'CONT_CODE , CONT_NAME , POSITION , GNREMARK 
        Dim strColumnName As String = ColumnName.ToUpper
        Select Case strColumnName
            Case "CUST_CODE"
                strTblName = "Outlet Code"
            Case "CUST_NAME"
                strTblName = "Outlet Name"
            Case "PRD_CODE"
                strTblName = "Product Category"
            Case "PRD_NAME"
                strTblName = "Product/Brand"
            Case "ACTIVITY_CODE"
                strTblName = "Activity Code"
            Case "ACTIVITY_NAME"
                strTblName = "Activity"
            Case "START_DATE"
                strTblName = "Start Date"
            Case "END_DATE"
                strTblName = "End Date"
            Case "EXTRA_CAT_CODE"
                strTblName = "Extra Sub Cat Code"
            Case "EXTRA_CAT_NAME"
                strTblName = "Condition"
            Case Else
                strTblName = strColumnName
        End Select

        Return strTblName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByRef ViewOption As Boolean = False) As FieldColumntype
        Try
            'Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            'Dim strColumnName As String = ColumnName.ToUpper
            ''CATEGORY , SUB_CAT_CODE , SUB_CAT_NAME , QTY , DTLREMARK , CUST_CODE , CUST_NAME , 
            ''CONT_CODE , CONT_NAME , POSITION , GNREMARK 
            'Select Case strColumnName
            '    Case "CUST_CODE", "CUST_NAME", "CONT_CODE", "CONT_NAME", "POSITION", "GNREMARK"
            '        FCT = FieldColumntype.InvisibleColumn
            '        'Case "CUST_NAME"
            '        '    FCT = FieldColumntype.HyperlinkColumn
            '    Case Else 'SALESREP_NAME, DATE, CUST_CODE, CUST_NAME, CONT_NAME, DTLREMARK
            '        FCT = FieldColumntype.BoundColumn
            'End Select
            'Return FCT
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If ViewOption = False Then

                Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
                strColumnName = strColumnName.ToUpper
                Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
                If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") Then
                    FCT = FieldColumntype.InvisibleColumn
                ElseIf strColumnName = "IMAGE" Or strColumnName = "TXN_NO" Or strColumnName = "HD_ACTIVITY_CODE" Then
                    FCT = FieldColumntype.InvisibleColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If

            ElseIf strColumnName = "IMAGE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If


            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetFieldColumnTypeHdr(ByVal strColumnName As String, Optional ByRef ViewOption As Boolean = False) As FieldColumntype
        Try

            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If ViewOption = False Then

                Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
                strColumnName = strColumnName.ToUpper
                Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
                If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") Then
                    FCT = FieldColumntype.InvisibleColumn
                Else

                    FCT = FieldColumntype.BoundColumn
                End If

            Else
                FCT = FieldColumntype.BoundColumn
            End If



            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Try
            Select Case strColumnName
                Case "*_TGT"
                    strStringFormat = "{0:#}"
                Case "START_DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case "END_DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case Else
                    strStringFormat = ""
            End Select
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class