﻿
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Text.RegularExpressions
Imports System.Reflection

''' <summary>
'''  ************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-09-06
'''	Purpose	    :	Class Easy Loader
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>
Public Class clsEasyLoaderV2
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))
    Public clsProperties As New mst_Utility.clsProperties.clsEasyLoader

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsEasyLoader"
        End Get
    End Property

#Region "DropDownList"

    Public Function GetCatDdlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_CAT_DDL"
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetActDdlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_ACT_DDL"
                .addItem("UPLOAD_TYPE", clsProperties.Upload_Type, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

#End Region

#Region "HEADER AND DETAIL"
    Public Function GetELList() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_LIST"
                .addItem("CAT_CODE", clsProperties.cat_code, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetELDesc(ByVal strELCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_DESC"
                .addItem("EL_CODE", strELCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetELDtl(ByVal strELCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_DTL"
                .addItem("EL_CODE", strELCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    'Public Function GetELValidRowCount(ByVal strELCode As String) As DataTable
    '    Dim objDB As cor_DB.clsDB
    '    Try
    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_TMP_EL_" & strELCode & "_VALID_COUNT"
    '            .addItem("VALID_COUNT_ACTION", clsProperties.Valid_Count_Action, cor_DB.clsDB.DataType.DBString)
    '            .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
    '            Return .spRetrieve()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function
#End Region

#Region "UPLOAD"
    Public Function GetList(ByVal strELCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_TMP_EL_" & strELCode & "_LIST"
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    'Public Sub Insert(ByVal strELCode As String, ByVal strColumnName As ArrayList, ByVal strColumn As ArrayList)
    '    Dim objDB As cor_DB.clsDB
    '    Try
    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_TMP_EL_" & strELCode & "_INSERT"

    '            For i As Integer = 0 To strColumnName.Count - 1
    '                .addItem(strColumnName.Item(i).ToString, strColumn.Item(i).ToString, cor_DB.clsDB.DataType.DBString)
    '            Next
    '            .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
    '            .spInsert()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Sub

    'Public Sub BulkInsert(ByVal strELCode As String, ByVal dtRecords As DataTable)
    '    Dim objDB As cor_DB.clsDB
    '    Try
    '        Using destinationConnection As New SqlConnection(strConn)
    '            destinationConnection.Open()

    '            Using bulkCopy As New SqlBulkCopy(destinationConnection)
    '                bulkCopy.DestinationTableName = "TBL_TMP_EL_" & strELCode

    '                '// Field names are case-sensitive in bulkcopy.
    '                For Each col As DataColumn In dtRecords.Columns
    '                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName)
    '                Next
    '                bulkCopy.BatchSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings("BulkCopySize"))
    '                bulkCopy.WriteToServer(dtRecords)
    '            End Using

    '            destinationConnection.Close()
    '        End Using
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Sub

    Public Function BulkInsert(ByVal strELCode As String, ByVal dtRecords As DataTable, ByVal strELTmpTable As String) As String
        Dim objDB As cor_DB.clsDB
        Using destinationConnection As New SqlConnection(strConn)
            Using bulkCopy As New SqlBulkCopy(destinationConnection)

                Try
                    destinationConnection.Open()

                    bulkCopy.DestinationTableName = strELTmpTable '"TBL_TMP_EL_" & strELCode

                    '// Field names are case-sensitive in bulkcopy.
                    For Each col As DataColumn In dtRecords.Columns
                        bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName)
                    Next
                    bulkCopy.BatchSize = Convert.ToInt32(ConfigurationManager.AppSettings("BulkCopySize"))
                    bulkCopy.NotifyAfter = 1
                    AddHandler bulkCopy.SqlRowsCopied, AddressOf OnSqlRowsCopied
                    bulkCopy.WriteToServer(dtRecords)

                    destinationConnection.Close()
                Catch ex As SqlException
                    Dim pattern As String = "\d+"
                    Dim match As Match = Regex.Match(ex.Message.ToString(), pattern)
                    Dim index = Convert.ToInt32(match.Value) - 1

                    Dim fi As FieldInfo = GetType(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic Or BindingFlags.Instance)
                    Dim sortedColumns = fi.GetValue(bulkCopy)
                    Dim items = DirectCast(sortedColumns.[GetType]().GetField("_items", BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(sortedColumns), [Object]())

                    Dim itemdata As FieldInfo = items(index).[GetType]().GetField("_metadata", BindingFlags.NonPublic Or BindingFlags.Instance)
                    Dim metadata = itemdata.GetValue(items(index))

                    Dim column = metadata.[GetType]().GetField("column", BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(metadata)
                    Dim length = metadata.[GetType]().GetField("length", BindingFlags.[Public] Or BindingFlags.NonPublic Or BindingFlags.Instance).GetValue(metadata)

                    Return String.Format("Column: {0} contains data with a length greater than: {1}. Please check again. Affected row(s) line no: {2}", column, length, (sqlRowIndex).ToString)

                Catch ex As InvalidOperationException
                    Dim msg = ex.InnerException.ToString
                    Select Case True
                        Case msg.Contains("String or binary data would be truncated.")
                            Return String.Format("One or more item has exceeded the allowed data size. Please check again. Affected row(s) line no: {0}", (sqlRowIndex + 1).ToString)
                        Case Else
                            Return String.Format("An error has occurred. Please check again. Affected row(s) line no: {0}", (sqlRowIndex + 1).ToString)
                    End Select
                Catch ex As Exception
                    Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
                Finally
                    objDB = Nothing
                End Try
            End Using
        End Using

        Return String.Empty
    End Function

    Public sqlRowIndex As Integer = 0
    Private Sub OnSqlRowsCopied(ByVal sender As Object, _
        ByVal args As SqlRowsCopiedEventArgs)
        sqlRowIndex = args.RowsCopied
    End Sub

    Public Sub Validate(ByVal strELCode As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_TMP_EL_" & strELCode & "_VALIDATE"
                .addItem("ACTION_ID", clsProperties.action_id, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub Upload(ByVal strELCode As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_TMP_EL_" & strELCode & "_UPLOAD"
                .addItem("ACTION_ID", clsProperties.action_id, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub Delete(ByVal strELCode As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_TMP_EL_" & strELCode & "_DELETE"
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetMsg(ByVal strELCode As String, ByVal strMode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_TMP_EL_" & strELCode & "_RESULTS_MSG"
                .addItem("MODE", clsProperties.strMode, cor_DB.clsDB.DataType.DBString)
                .addItem("ACTION", clsProperties.action_id, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

    Public Function GetELColumnStyle() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_TMP_EL_COLUMN_STYLE"
                .addItem("EL_CODE", clsProperties.Upload_Type, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetDLFileName() As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_DL_FILE_PATH"
                .addItem("EL_CODE", clsProperties.Upload_Type, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function


#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception

        Public Sub New(ByVal msg As Object)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
