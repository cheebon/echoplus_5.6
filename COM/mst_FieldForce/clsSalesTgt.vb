'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	11/04/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSalesTgt
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSalesTgt"
        End Get
    End Property

    Public Function GetSalesTgtList(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strYear As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_TGT_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSalesTgtDetails(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdGrpCode As String, ByVal strYear As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_TGT_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateSalesTgt(ByVal strPrdGrpCode As String, ByVal strYear As String, _
                        ByVal strTeamCode As String, ByVal strSalesrepCode As String, _
                        ByVal strJanTgt As String, ByVal strFebTgt As String, ByVal strMarTgt As String, _
                        ByVal strAprTgt As String, ByVal strMayTgt As String, ByVal strJuneTgt As String, _
                        ByVal strJulyTgt As String, ByVal strAugTgt As String, ByVal strSeptTgt As String, _
                        ByVal strOctTgt As String, ByVal strNovTgt As String, ByVal strDecTgt As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_TGT_CREATE"
                .addItem("PRD_GRP_CODE", strPrdGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("YEAR", strYear, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("JAN_TGT", strJanTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("FEB_TGT", strFebTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("MAR_TGT", strMarTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("APR_TGT", strAprTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("MAY_TGT", strMayTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("JUNE_TGT", strJuneTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("JULY_TGT", strJulyTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("AUG_TGT", strAugTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("SEPT_TGT", strSeptTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("OCT_TGT", strOctTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("NOV_TGT", strNovTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("DEC_TGT", strDecTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateSalesTgt(ByVal strPrdGrpCode As String, ByVal strYear As String, _
                        ByVal strTeamCode As String, ByVal strSalesrepCode As String, _
                        ByVal strJanTgt As String, ByVal strFebTgt As String, ByVal strMarTgt As String, _
                        ByVal strAprTgt As String, ByVal strMayTgt As String, ByVal strJuneTgt As String, _
                        ByVal strJulyTgt As String, ByVal strAugTgt As String, ByVal strSeptTgt As String, _
                        ByVal strOctTgt As String, ByVal strNovTgt As String, ByVal strDecTgt As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_TGT_UPDATE"
                .addItem("PRD_GRP_CODE", strPrdGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("YEAR", strYear, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("JAN_TGT", strJanTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("FEB_TGT", strFebTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("MAR_TGT", strMarTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("APR_TGT", strAprTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("MAY_TGT", strMayTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("JUNE_TGT", strJuneTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("JULY_TGT", strJulyTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("AUG_TGT", strAugTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("SEPT_TGT", strSeptTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("OCT_TGT", strOctTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("NOV_TGT", strNovTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("DEC_TGT", strDecTgt, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteSalesTgt(ByVal strYear As String, _
                        ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strCodeList As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALES_TGT_DELETE"
                .addItem("YEAR", strYear, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



