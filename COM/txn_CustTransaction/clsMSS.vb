'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	30/10/2006
'	Purpose	    :	Class to build datatables for getting and storing MSS transaction details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

'Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsMSS
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))



    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMSS"
        End Get
    End Property

    Function getMSSQuestion(ByVal strTitleCode As String, ByVal strSubCatCode As String) As String
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSSUBCATQUESTION"
                .addItem("titlecode", strTitleCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                dt = .spRetrieve
                Return dt.Rows(0)("ques_desc").ToString
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getMSSQuestion :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getMSSCategoryListingDT(ByVal strTitleCode As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_MSSCATEGORYLISTING"
                .addItem("titlecode", strTitleCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getMSSCategoryListingDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getMSSSubCatListingDT(ByVal strTitleCode As String, ByVal strSalesmanCode As String) As DataTable

        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSSUBCATLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("titlecode", strTitleCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getMSSSubCatListingDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function getNumOfControlDT(ByVal strTitleCode As String, ByVal strCatCode As String, ByVal strSubcatCode As String) As DataTable
        Dim objdb As New cor_DB.clsDB

        Dim intCount As Integer = 0
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETCONTROL"
                .addItem("titlecode", strTitleCode, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubcatCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getNumOfControlDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function checkQuestionStatus(ByVal strCustCode As String, ByVal strSalesmanCode As String, ByVal strTitleCode As String, ByVal strCatCode As String, ByVal strSubcatCode As String, ByVal strType As String) As Boolean
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_CHECKINSERTEDITEMS"
                .addItem("custcode", strCustCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("titlecode", strTitleCode, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubcatCode, 2)
                .addItem("type", strType, 2)
                dt = .spRetrieve
                If dt.Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".checkQuestionStatus :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    'Function getCriteriaOption(ByVal strTitleCode As String, ByVal strSubCatCode As String) As DataTable
    '    Dim dt As Data.DataTable = Nothing
    '    Dim objdb As New cor_DB.clsDB
    '    Try
    '        With objdb
    '            .ConnectionString = strFFMSConn
    '            .CmdText = "SPP_TXN_MSSGETCRITERIALISTING"
    '            .addItem("titlecode", strTitleCode, 2)
    '            .addItem("subcatcode", strSubCatCode, 2)
    '            dt = .spRetrieve
    '        End With
    '        Return dt
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ClassName & ".getCriteriaOption() :" & ex.Message))
    '    Finally
    '        objdb = Nothing
    '    End Try

    'End Function

    Function getCriteriaOption(ByVal strTitleCode As String, ByVal strSubCatCode As String, ByVal strSalesmanCode As String) As DataTable
        Dim dt As Data.DataTable = Nothing
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSGETCRITERIALISTING"
                .addItem("titlecode", strTitleCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .addItem("salesman_code", strSalesmanCode, 2)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCriteriaOption() :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Function

    Sub insertListboxField(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitNo As String, ByVal strTxnTime As String, ByVal strTitleCode As String, ByVal strGenComment As String, ByVal strCatcode As String, ByVal strSubCatCode As String, ByVal strCriteriaCode As String)
        Dim objdb As New cor_DB.clsDB

        Try
            If strCatcode = "All" Then
                strCatcode = getCatCode(strSubCatCode)
            End If

            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_INSERTMSSLISTBOX"
                .addItem("txnno", strTxnNo, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)
                .addItem("branchcode", strBranchCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("visitid", strVisitNo, 2)

                .addItem("txntime", strTxnTime, 2)
                .addItem("titlecode", strTitleCode, 2)
                .addItem("gencomment", strGenComment, 2)
                .addItem("catcode", strCatcode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .addItem("criteriacode", strCriteriaCode, 2)

                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertListboxField :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Sub

    Sub insertDDLTextBoxValue(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitNo As String, ByVal strTxnTime As String, ByVal strTitleCode As String, ByVal strGenComment As String, ByVal strCatcode As String, ByVal strSubCatCode As String, ByVal strCriteriaCode As String, ByVal strType As String)
        Dim objdb As New cor_DB.clsDB
        Dim dtHdr As DataTable = Nothing
        Dim dtDtl As DataTable = Nothing

        Try
            If strCatcode = "All" Then
                strCatcode = getCatCode(strSubCatCode)
            End If
            dtHdr = getHeaderDT(strTxnNo, strCustCode, strSalesmanCode)
            dtDtl = getDetailDT(strTxnNo, strCatcode, strSubCatCode)

            If dtHdr.Rows.Count > 0 And dtDtl.Rows.Count > 0 Then 'Header n detail have been created
                With objdb
                    .ConnectionString = strFFMSConn
                    .CmdText = "SPP_WEB_UPDATEMSSDTL"
                    .addItem("catcode", strCatcode, 2)
                    .addItem("subcatcode", strSubCatCode, 2)
                    .addItem("expec", strCriteriaCode, 2)
                    .addItem("type", strType, 2)
                    .spUpdate()
                End With
            ElseIf dtHdr.Rows.Count > 0 And dtDtl.Rows.Count = 0 Then 'Header created but detail not yet created
                With objdb
                    .ConnectionString = strFFMSConn
                    .CmdText = "SPP_WEB_INSERTMSSDTL"
                    .addItem("txnno", strTxnNo, 2)
                    .addItem("catcode", strCatcode, 2)
                    .addItem("subcatcode", strSubCatCode, 2)
                    .addItem("expec", strCriteriaCode, 2)
                    .addItem("type", strType, 2)
                    .spInsert()
                End With
            ElseIf dtHdr.Rows.Count = 0 And dtDtl.Rows.Count = 0 Then
                With objdb
                    .ConnectionString = strFFMSConn
                    .CmdText = "SPP_WEB_INSERTMSSHDRDTL"
                    .addItem("txnno", strTxnNo, 2)
                    .addItem("custcode", strCustCode, 2)
                    .addItem("contactcode", strContactCode, 2)
                    .addItem("branchcode", strBranchCode, 2)
                    .addItem("salesmancode", strSalesmanCode, 2)
                    .addItem("visitid", strVisitNo, 2)
                    .addItem("txntime", strTxnTime, 2)
                    .addItem("titlecode", strTitleCode, 2)
                    .addItem("gencomment", strGenComment, 2)
                    .addItem("catcode", strCatcode, 2)
                    .addItem("subcatcode", strSubCatCode, 2)
                    .addItem("expec", strCriteriaCode, 2)
                    .addItem("type", strType, 2)
                    .spInsert()
                End With
            End If
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertTextboxField :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Sub insertDate(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitNo As String, ByVal strTxnTime As String, ByVal strTitleCode As String, ByVal strGenComment As String, ByVal strCatcode As String, ByVal strSubCatCode As String, ByVal dtmDate As Date)
        Dim objdb As New cor_DB.clsDB
        Dim dtHdr As DataTable = Nothing
        Dim dtDtl As DataTable = Nothing
        Try
            If strCatcode = "All" Then
                strCatcode = getCatCode(strSubCatCode)
            End If
            dtHdr = getHeaderDT(strTxnNo, strCustCode, strSalesmanCode)
            dtDtl = getDetailDT(strTxnNo, strCatcode, strSubCatCode)

            If dtHdr.Rows.Count > 0 And dtDtl.Rows.Count > 0 Then
                With objdb
                    .ConnectionString = strFFMSConn
                    .CmdText = "SPP_WEB_UPDATEMSSDATE"
                    .addItem("catcode", strCatcode, 2)
                    .addItem("subcatcode", strSubCatCode, 2)
                    'dtmDate = "10/31/2006 8:40:00 AM"
                    .addItem("expec", DateTime.Now, 3)

                    .spUpdate()
                End With
            ElseIf dtHdr.Rows.Count > 0 Then
                With objdb
                    .ConnectionString = strFFMSConn
                    .CmdText = "SPP_WEB_INSERTMSSDATEDTL"
                    .addItem("txnno", strTxnNo, 2)
                    .addItem("catcode", strCatcode, 2)
                    .addItem("subcatcode", strSubCatCode, 2)
                    .addItem("expec", dtmDate, 3)

                    .spInsert()
                End With
            Else
                With objdb
                    .ConnectionString = strFFMSConn
                    .CmdText = "SPP_WEB_INSERTMSSDATEHDRDTL"
                    .addItem("txnno", strTxnNo, 2)
                    .addItem("custcode", strCustCode, 2)
                    .addItem("contactcode", strContactCode, 2)
                    .addItem("branchcode", strBranchCode, 2)
                    .addItem("salesmancode", strSalesmanCode, 2)
                    .addItem("visitid", strVisitNo, 2)

                    .addItem("txntime", strTxnTime, 2)
                    .addItem("titlecode", strTitleCode, 2)
                    .addItem("gencomment", strGenComment, 2)
                    .addItem("catcode", strCatcode, 2)
                    .addItem("subcatcode", strSubCatCode, 2)
                    .addItem("expec", dtmDate, 3)
                    .spInsert()
                End With
            End If

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertDate:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Function getCatCode(ByVal strSubCatCode As String) As String
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETMSSCATCODE"
                .addItem("subcatcode", strSubCatCode, 2)
                dt = .spRetrieve
            End With

            Return dt.Rows(0)("cat_code").ToString

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCatCode :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getListBoxDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEBMSSTITLE"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getListBoxDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function


    Function getHeaderDT(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETMSSHDR"
                .addItem("txnno", strTxnNo, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getHeaderDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getDetailDT(ByVal strTxnNo As String, ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETMSSDTL"
                .addItem("txnno", strTxnNo, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                Return .spRetrieve

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDetailDT:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getContactDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_CUSTOMERCONTACT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactDT:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    'Function testExistance(ByVal strCustCode As String, ByVal strSalesmanCode As String, ByVal strTitleCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strType As String) As Boolean

    '    Dim dtTestHdrExistance As DataTable = Nothing
    '    Dim dtTestDtlExistance As DataTable = Nothing

    '    Try
    '        If strCatCode = "All" Then
    '            strCatCode = getCatCode(strSubCatCode)
    '        End If

    '        dtTestHdrExistance = headerExistance(strCustCode, strSalesmanCode, strTitleCode, strCatCode, strSubCatCode, strType)
    '        dtTestDtlExistance = detailExistance(strCustCode, strSalesmanCode, strTitleCode, strCatCode, strSubCatCode, strType)

    '        If dtTestHdrExistance.Rows.Count > 0 And dtTestDtlExistance.Rows.Count > 0 Then 'MSS header exists and the control has not been created before
    '            Return True
    '        ElseIf dtTestHdrExistance.Rows.Count = 0 And dtTestDtlExistance.Rows.Count = 0 Then 'MSS header does not exist and the control has not been created before
    '            Return True
    '        Else
    '            Return False
    '        End If

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ClassName & ".testExistance:" & ex.Message))
    '    End Try
    'End Function

    Function testExistance(ByVal strTxnno As String, ByVal strCustCode As String, ByVal strSalesmanCode As String, ByVal strTitleCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strType As String) As DataTable

        Dim dtTestHdrExistance As DataTable = Nothing
        Dim dtTestDtlExistance As DataTable = Nothing

        Try
            If strCatCode = "All" Then
                strCatCode = getCatCode(strSubCatCode)
            End If

            dtTestHdrExistance = headerExistance(strCustCode, strSalesmanCode, strTitleCode, strCatCode, strSubCatCode, strType)
            dtTestDtlExistance = detailExistance(strCustCode, strSalesmanCode, strTitleCode, strCatCode, strSubCatCode, strType, strTxnno)

            Return dtTestDtlExistance

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".testExistance:" & ex.Message))
        Finally

        End Try
    End Function
    Function headerExistance(ByVal strCustCode As String, ByVal strSalesmanCode As String, ByVal strTitleCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strType As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSTESTEXISTANCE"
                .addItem("custcode", strCustCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("titlecode", strTitleCode, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .addItem("type", strType, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".headerExistance:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function detailExistance(ByVal strCustCode As String, ByVal strSalesmanCode As String, ByVal strTitleCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strType As String, ByVal strTxnNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSTESTFIELDEXISTANCE"
                .addItem("custcode", strCustCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("titlecode", strTitleCode, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .addItem("type", strType, 2)
                .addItem("txnno", strTxnNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".detailExistance:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Sub updateMSS(ByVal strTxnNo As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strInd As String, ByVal strNewValue As String, Optional ByVal dtmNewDate As DateTime = Nothing)
        Dim obj As New cor_DB.clsDB

        Try
            If strCatCode = "All" Then
                strCatCode = getCatCode(strSubCatCode)
            End If
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSUPDATE"
                .addItem("txnno", strTxnNo, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .addItem("value", strNewValue, 2)
                .addItem("date", dtmNewDate, 3)
                .addItem("type", strInd, 2)

                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateMSS:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub deleteMSS(ByVal strTxnno As String, ByVal strCatCode As String, ByVal strSubcatcode As String, ByVal strInd As String)
        Dim obj As New cor_DB.clsDB

        Try
            If strCatCode = "All" Then
                strCatCode = getCatCode(strSubcatcode)
            End If
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSDELETE"
                .addItem("txnno", strTxnno, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubcatcode, 2)
                .addItem("type", strInd, 2)

                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteMSS:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Function getMSSTitleCode(ByVal strSalesmanCode As String, ByVal strTxnNo As String) As String
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSGETTITLECODE"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("txnno", strTxnNo, 2)
                dt = .spRetrieve
            End With

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)("title_code").ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getMSSTitleCode:" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Sub insertHeader(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitNo As String, ByVal strTitleCode As String, ByVal strGenComment As String)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSINSERTHDR"
                .addItem("txnno", strTxnNo, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)
                .addItem("branchcode", strBranchCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("visitid", strVisitNo, 2)
                .addItem("titlecode", strTitleCode, 2)
                .addItem("gencomment", strGenComment, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertHeader:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Sub insertDetail(ByVal strTxnNo As String, ByVal strCatCode As String, ByVal strSubCat As String, ByVal strType As String, ByVal strAnswer1 As String, Optional ByVal strAnswer2 As String = Nothing, Optional ByVal dtmAnswer3 As DateTime = Nothing)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSINSERTDTL"
                .addItem("txnno", strTxnNo, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCat, 2)
                .addItem("answer1", strAnswer1, 2)
                .addItem("answer2", strAnswer2, 2)
                .addItem("answer3", dtmAnswer3, 3)
                .addItem("type", strType, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertDetail:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Function getInsertedMSSItems(ByVal strTxnNo As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSGETINSERTEDITEMS"
                .addItem("txnno", strTxnNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getInsertedMSSItems:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function getCriteriaDesc(ByVal strTitleCode As String, ByVal strSubCat As String, ByVal strCriteria As String) As String
        Dim objdb As New cor_DB.clsDB
        Dim dt As DataTable
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSGETCRITERIANAME"
                .addItem("titlecode", strTitleCode, 2)
                .addItem("subcatcode", strSubCat, 2)
                .addItem("criteriacode", strCriteria, 2)
                dt = .spRetrieve
                Return dt.Rows(0)("criteria_desc")
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCriteriaDesc:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function getListBoxDesc(ByVal strTxnno As String, ByVal strCatCode As String, ByVal strSubCat As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSGETLISTBOXDESC"
                .addItem("txnno", strTxnno, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCat, 2)

                Return .spRetrieve

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getListBoxDesc:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function getGeneralComment(ByVal strTxnNo As String, ByVal strSalesmanCode As String, ByVal strTitleCode As String) As String
        Dim objdb As New cor_DB.clsDB
        Dim dt As DataTable
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSGETGENERALCOMMENT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("txnno", strTxnNo, 2)
                .addItem("titlecode", strTitleCode, 2)
                dt = .spRetrieve
                Return dt.Rows(0)("gen_comment")

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getListBoxDesc:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Sub updateHeader(ByVal strTxnNo As String, ByVal strComment As String)
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSUPDATEHDR"
                .addItem("txnno", strTxnNo, 2)
                .addItem("comment", strComment, 2)
                .spUpdate()

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateHeader:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Sub deleteDetail(ByVal strTxnNo As String)
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_MSSDELETEDTL"
                .addItem("txnno", strTxnNo, 2)
                .spDelete()

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateHeader:" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
