﻿function Customer(_CustCode, _CustName, _Latitude, _Longitude, _isDestination)
{
    this.CustCode = _CustCode;
    this.CustName = _CustName;
    this.Latitude = _Latitude;
    this.Longitude = _Longitude;
    
    this.IsStartPoint = false;     
    this.Visited = false;
    this.IsDestination = _isDestination;
}

//This is an extended customer class to hold detailed information about the cust. Primarily used for show gmap bubble
function CustomerDtl(_CustCode, _CustName, _Latitude, _Longitude, _ContName, _MtdSales, _YtdSales, _NoSKU, _ImgLoc, _Class, _CustType) {
    this.CustCode = _CustCode;
    this.CustName = _CustName;
    this.CustType = _CustType;
    this.ContName = _ContName;
    this.Latitude = _Latitude;
    this.Longitude = _Longitude;

    this.MtdSales = _MtdSales;
    this.YtdSales = _YtdSales;
    this.NoSKU = _NoSKU;
    this.ImgLoc = _ImgLoc;
    this.Class = _Class;
}