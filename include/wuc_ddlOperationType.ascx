<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlOperationType.ascx.vb" Inherits="include_wuc_ddlOperationType" %>
<asp:DropDownList id="ddlOperationTypeID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvOperationTypeID" ControlToValidate="ddlOperationTypeID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select OperationType."></asp:CompareValidator>
