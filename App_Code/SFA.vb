Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data

Public MustInherit Class BaseEntity
    ' Methods
    Protected Sub New()
    End Sub
End Class

#Region "Class Entity"

Public Class ATChart
    Inherits BaseEntity
#Region "LocalVariable"
    Private _agencyCode As String = String.Empty
    Private _agencyName As String = String.Empty
    Private _salesAmt As Double = 0
    Private _salesMTG As Double = 0
    Private _salesRR As Double = 0
    Private _salesTGT As Double = 0

#End Region
#Region "Public Porperty"
    Public Property AGENCY_CODE() As String
        Get
            Return _agencyCode
        End Get
        Set(ByVal value As String)
            _agencyCode = value
        End Set
    End Property
    Public Property AGENCY_NAME() As String
        Get
            Return _agencyName
        End Get
        Set(ByVal value As String)
            _agencyName = value
        End Set
    End Property

    Public Property SALES_AMT() As Double
        Get
            Return _salesAmt
        End Get
        Set(ByVal value As Double)
            _salesAmt = value
        End Set
    End Property
    Public Property SALES_MTG() As Double
        Get
            Return _salesMTG
        End Get
        Set(ByVal value As Double)
            _salesMTG = value
        End Set
    End Property
    Public Property SALES_RR() As Double
        Get
            Return _salesRR
        End Get
        Set(ByVal value As Double)
            _salesRR = value
        End Set
    End Property
    Public Property SALES_TGT() As Double
        Get
            Return _salesTGT
        End Get
        Set(ByVal value As Double)
            _salesTGT = value
        End Set
    End Property

#End Region
End Class

Public Class SalesPrfm
    Private _srCode As String = String.Empty
    Private _srName As String = String.Empty
    Private _stMT, _stTgt, _spMT, spTgt As Integer

    'ST_MT, ST_TGT, ST_ACH, AT_ACT_AMT, AT_TGT_AMT, AT_ACH, SP_MT, SP_TGT, SP_ACH, 
    'DP_ACT, DP_TGT, DP_ACH, IC_ST, IC_AT, IC_SP, IC_TTL
End Class
#End Region


Public Class clsCONN
    Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
        If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
        If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

        mFieldName = "@" & mFieldName
        Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
        sqlCmd.Parameters.Add(sqlPrm)

        If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
            sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
        Else
            sqlCmd.Parameters(mFieldName).value = mFieldValue
        End If
    End Sub

    Public Enum dtType As Integer
        dbINT = 8
        dbString = 22
        dbDateTime = 4
        dbLong = 0
        dbFloat = 6
    End Enum
End Class


<WebService([Namespace]:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ScriptService()> _
Public Class ws_SFA
    Inherits System.Web.Services.WebService

    'Private Shared ReadOnly ConnectionString As String = HttpContext.Current.Session("ffmr_conn")
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffmr_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Private Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class

#Region "AT Chart"
    <WebMethod(EnableSession:=True)> _
    Public Function RPT_ATChart2(ByVal iMaxRow As Integer) As PagedResult(Of ATChart)
        If iMaxRow = 0 Then iMaxRow = 100

        Dim rptRows As New List(Of ATChart)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of ATChart)
            rptRows.Add(NewRow_ATChartRPT)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_RPT_SALES_AT_2"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", Session("PRINCIPAL_ID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRINCIPAL_CODE", "", clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "YEAR", Session("YEAR"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "MONTH", 8, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "TREE_SALESREP_LIST", Session("SALESREP_LIST"), clsCONN.dtType.dbString, True) '"20000219,20000834,20000066,20001117,20000123,20000035", clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "MAX_ROW", iMaxRow, clsCONN.dtType.dbINT)
                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_ATChartRPT(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of ATChart)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    <WebMethod(EnableSession:=True)> _
    Public Function RPT_ATChart() As PagedResult(Of ATChart)
        Dim rptRows As New List(Of ATChart)()
        Dim rptResult
        If Session.Count = 0 Then
            rptResult = New PagedResult(Of ATChart)
            rptRows.Add(NewRow_ATChartRPT)
            rptResult.Rows = rptRows
            rptResult.Message = "Session Timeout!"
            Return rptResult
        End If

        Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
            Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                sqlCmd.CommandText = "SPP_RPT_SALES_AT"
                sqlCmd.CommandType = CommandType.StoredProcedure
                clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", Session("PRINCIPAL_ID"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "PRINCIPAL_CODE", "", clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "YEAR", Session("YEAR"), clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "MONTH", 8, clsCONN.dtType.dbString)
                clsCONN.addInputParam(sqlCmd, "TREE_SALESREP_LIST", Session("SALESREP_LIST"), clsCONN.dtType.dbString, True) '"20000219,20000834,20000066,20001117,20000123,20000035", clsCONN.dtType.dbString)
                Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                    While sqlReader.Read
                        rptRows.Add(Build_ATChartRPT(sqlReader))
                    End While
                End Using
            End Using
        End Using

        rptResult = New PagedResult(Of ATChart)
        With rptResult
            .Rows = rptRows
            .Total = rptRows.Count
        End With

        Return rptResult
    End Function
    Public Shared Function Build_ATChartRPT(ByRef reader As IDataReader) As ATChart
        Dim rptRow As New ATChart
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)
                    Case "AGENCY_CODE"
                        SetValue(Of String)(.AGENCY_CODE, reader("AGENCY_CODE"), String.Empty)
                    Case "AGENCY_NAME"
                        SetValue(Of String)(.AGENCY_NAME, reader("AGENCY_NAME"), String.Empty)
                    Case "SALES_AMT"
                        SetValue(Of Double)(.SALES_AMT, reader("SALES_AMT"), 0)
                    Case "SALES_MTG"
                        SetValue(Of Double)(.SALES_MTG, reader("SALES_MTG"), 0)
                    Case "SALES_RR"
                        SetValue(Of Double)(.SALES_RR, reader("SALES_RR"), 0)
                    Case "SALES_TGT"
                        SetValue(Of Double)(.SALES_TGT, reader("SALES_TGT"), 0)
                End Select
            Next
        End With
        Return rptRow
    End Function
    Public Shared Function NewRow_ATChartRPT() As ATChart
        Dim rptRow As New ATChart
        With rptRow
            .AGENCY_CODE = String.Empty
            .AGENCY_NAME = String.Empty
            .SALES_AMT = 0
            .SALES_MTG = 0
            .SALES_RR = 0
            .SALES_TGT = 0
        End With
        Return rptRow
    End Function

#End Region

End Class


