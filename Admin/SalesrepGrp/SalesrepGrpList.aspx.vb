﻿Imports System.Data

Partial Class Admin_SalesrepGrp_SalesrepGrpList
    Inherits System.Web.UI.Page

#Region "Property"
    Private intPageSize As Integer

    Public ReadOnly Property UserCode() As String
        Get
            Return Session("UserCode")
        End Get
    End Property
    Public ReadOnly Property SessionUserCode() As String
        Get
            Return Trim(Session("UserCode"))
        End Get
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
            IsBsAdmin()
            cmdCreate.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.SRGROUP, SubModuleAction.Create)
            'Call Header
            With wuc_lblheader
                .Title = "Salesrep Group List"
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = False
            End With

            If Not Page.IsPostBack Then
                ddlSearchType.SelectedValue = "salesrepgrp_name"
                AcceptIncomingRequest()

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub
#Region "Function"
    Private Sub IsBsAdmin()
        'If SessionUserCode = "" Or IsNothing(SessionUserCode) Then
        '    Response.Redirect("../../index.aspx")
        'ElseIf Not SessionUserCode.ToUpper.Contains("BSADMIN") Then
        '    Response.Redirect("../../index.aspx")
        'End If

        If SessionUserCode = "" Or IsNothing(SessionUserCode) Then
            Response.Redirect("../../index.aspx")
        ElseIf Not SessionUserCode.ToUpper.Contains("BSADMIN") Then
            cmdCreate.Enabled = False
            dgList.Columns(3).Visible = False
            dgList.Columns(4).Visible = False
            dgList.Columns(5).Visible = False
        End If

    End Sub
    Private Sub AcceptIncomingRequest()
        Dim strSearchType, strSearchValue As String

        strSearchType = Trim(Request.QueryString("search_type"))
        strSearchValue = Trim(Request.QueryString("search_value"))

        If strSearchType <> "" Then
            ddlSearchType.SelectedValue = strSearchType
            txtSearchValue.Text = strSearchValue
            BindGrid(ViewState("SortCol"))
        End If

    End Sub

    Private Sub DeleteSalesrepGrp(ByVal salesrepgrp_id As String)
        Dim obj As adm_SalesrepGrp.clsSalesrepGrp
        Dim objmani As adm_SalesrepGrp.clsSalesrepGrpManipulator
        Try
            obj = New adm_SalesrepGrp.clsSalesrepGrp
            obj.SalesrepGrpID = salesrepgrp_id

            objmani = New adm_SalesrepGrp.clsSalesrepGrpManipulator(obj)
            objmani.Delete()


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"))
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SalesrepGrpList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"))
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SalesrepGrpList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"))
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SalesrepGrpList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region
#Region "Bind"
    Private Sub BindGrid(ByVal SortExpression As String)
        Dim dt As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)

        Try
            'If dtCurrentTable Is Nothing Then
            dt = GetRecList()

            'If dt Is Nothing Then
            '    dt = New DataTable
            'Else
            If dt.Rows.Count = 0 Then
                dt.Rows.Add(dt.NewRow())
            End If
            'End If

            dt.DefaultView.Sort = SortExpression


            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
                .Visible = IIf(dgList.PageSize > dt.Rows.Count, False, True)
            End With

            For Each COL As DataControlField In dgList.Columns
                Select Case COL.HeaderText.Trim.ToUpper
                    Case "COPY"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.SRGROUP, SubModuleAction.Create)
                    Case "DELETE"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.SRGROUP, SubModuleAction.Delete)
                    Case "EDIT"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.SRGROUP, SubModuleAction.Edit)
                End Select

            Next
        Catch ex As Exception
            ExceptionMsg("SalesrepGrpList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
        End Try
    End Sub
    Private Function GetRecList() As DataTable
        Dim obj As New adm_SalesrepGrp.clsSalesrepGrpQuery
        Dim dt As DataTable = Nothing

        Try
            
            dt = obj.SalesrepGrpSearch(ddlSearchType.SelectedValue.ToUpper, txtSearchValue.Text.Trim, UserCode)

            If dt Is Nothing OrElse dt.Columns.Count = 0 Then Return dt


        Catch ex As Exception
            ExceptionMsg("SalesrepGrpList.GetRecList : " & ex.ToString)
        Finally
            'dt = Nothing
            obj = Nothing
        End Try
        Return dt
    End Function
#End Region
#Region "dglist"
    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Try
            Dim index As Integer = 0
            Dim row As GridViewRow

            If Integer.TryParse(e.CommandArgument, index) = True Then
                row = IIf(index >= 0 AndAlso index <= dgList.Rows.Count, dgList.Rows(index), Nothing)

                Select Case e.CommandName
                    Case "Delete"
                        'If Trim(lblStatusValue.Text) = "Active" Then
                        '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Please set status to In-Active before proceed.');</script>")
                        '    Exit Sub
                        'End If

                        'objUser = New adm_User.clsUser
                        'With objUser
                        '    .clsProperties.user_id = "0" 'drv("user_id")
                        '    .clsProperties.user_code = Trim(dgList.DataKeys(row.RowIndex).Value)
                        '    .Delete()
                        'End With

                        'Response.Redirect("UserList.aspx", False)
                    Case "Copy"
                        Response.Redirect("SalesrepGrpDtl.aspx?action=COPY&salesrepgrp_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
                    Case "Details"
                        Response.Redirect("SalesrepGrpDtl.aspx?action=VIEW&salesrepgrp_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Edit"
                        Response.Redirect("SalesrepGrpDtl.aspx?action=EDIT&salesrepgrp_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                End Select

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        DeleteSalesrepGrp(Trim(dgList.DataKeys(e.RowIndex).Value))
        BindGrid(ViewState("SortCol"))
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '    Dim drv As DataRowView
        '    Dim imgEdit, imgDelete As ImageButton

        '    Try
        '        If e.Row.RowType = DataControlRowType.DataRow Then
        '            drv = CType(e.Row.DataItem, DataRowView)


        '            If IsDBNull(drv("salesrep_grp_name")) Then
        '                imgDelete = CType(e.Row.Cells(2).Controls(0), ImageButton)
        '                imgEdit = CType(e.Row.Cells(3).Controls(0), ImageButton)
        '                imgDelete.Visible = False
        '                imgEdit.Visible = False
        '            End If
        '        End If

        '    Catch ex As Exception
        '        ExceptionMsg("SalesrepGrpList.dgList_RowDataBound : " + ex.ToString)
        '    End Try
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid(ViewState("SortCol"))
    End Sub



    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        If ddlSearchType.SelectedItem.Text.ToUpper.Trim = "ALL" Then
            lblSearchValue.Visible = False
            txtSearchValue.Visible = False
            btnSearch.Visible = False
            lblDot.Visible = False
            BindGrid(ViewState("SortCol"))
        Else
            lblSearchValue.Visible = True
            txtSearchValue.Visible = True
            btnSearch.Visible = True
            lblDot.Visible = True
        End If
    End Sub

    Protected Sub cmdCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCreate.Click
        Response.Redirect("SalesrepGrpDtl.aspx?action=CREATE&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
    End Sub
End Class