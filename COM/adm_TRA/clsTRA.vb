﻿Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsTRA
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Public clsProperties As New adm_TRA.clsProperties.clsTRAList
    Private strFFMAConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))


    Public Function GetTRAHdrList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_GET_TRA_LIST_INT"
                .addItem("search_type", clsProperties.search_type, cor_DB.clsDB.DataType.DBString)
                .addItem("team_code", clsProperties.team_code, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value", clsProperties.search_value, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value_1", clsProperties.search_value_1, cor_DB.clsDB.DataType.DBString)
                .addItem("TXN_STATUS", clsProperties.strTxnStatus, cor_DB.clsDB.DataType.DBString)
                '.addItem("PRINT_STATUS_FROM", clsProperties.strPrinstatusFrom, cor_DB.clsDB.DataType.DBString)
                '.addItem("PRINT_STATUS_TO", clsProperties.strPrinstatusTo, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINT_STATUS", clsProperties.strPrinStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("TXN_START_DATE", clsProperties.strTxnStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("TXN_END_DATE", clsProperties.strTxnEndDate, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTeamList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_GETTRATEAMLIST"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPTeamList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTxnStatusList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_GETTRATXNSTATUSLIST"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetSAPTxnStatusList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTRAListDtl() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_GET_TRA_DTL_INT"
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetTRAListDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdTRAStatus()
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_UPD_TRA_STATUS_INT"
                .addItem("TXN_NO", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("TXN_STATUS", clsProperties.txn_status, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_CODE", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.UpdTRAStatus" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetTRAListHdrReport() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_RPT_SAP_TRA_HDR"
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                '.addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetTRAListHdrReport" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTRAListDtlReport() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_RPT_SAP_TRA_DTL"
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                '.addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetTRAListDtlReport" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdTRAPrintNo()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_UPD_TRA_PRINTNO_INT"
                .addItem("TXN_NO", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_CODE", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.UpdTRAPrintNo" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub


    'Batch printing ===============================================================
    Public Function GetTRAListHdrReportBatch() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_RPT_SAP_TRA_HDR_BATCH"
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString, True)
                '.addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetTRAListHdrReportBatch" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTRAListDtlReportBatch() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_RPT_SAP_TRA_DTL_BATCH"
                .addItem("txn_no", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString, True)
                '.addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetTRAListDtlReportBatch" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdTRAPrintNoBatch()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_UPD_TRA_PRINTNO_INT_BATCH"
                .addItem("TXN_NO", clsProperties.txn_no, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_CODE", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.UpdTRAPrintNoBatch" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    'Batch printing ===============================================================

    Public Function GetPrintStatusDDL() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_GET_TRA_PRINT_STATUS"
                .addItem("USER_ID", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("mst_SAP.clsSAPQuery.GetPrintStatusDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
