'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/06/2005
'	Purpose	    :	Class XML Writer 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |22-Nov-2005  	|Grace    	        |Put additional code  	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On 
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.xml
Imports System.IO

Interface IcorOB_WriteXml
    Function CreateDocument() As XmlElement
    Function AddElement() As XmlElement
    Sub AddAttribute()
    Function XmlString() As String
    Sub SaveXml()
End Interface

'<Guid("368F4EC0-D9A0-4399-B793-494F9D165904"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsWriteXml
    'Inherits ServicedComponent
    'Implements IcorOB_WriteXml

    Private handle As IntPtr                ' Pointer to an external unmanaged resource.
    Private Components As Component         ' Other managed resource this class uses.
    Private disposed As Boolean = False     ' Track whether Dispose has been called.

    'Public clsProperties As New clsProperties.clsTemplate
    Private xmlRoot As XmlElement
    Public xmlDoc As XmlDocument          ' XML dcoument
    'Private xslDoc As DOMDocument
    Private xmlElement As XmlElement  ' the current element

    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function CreateXmlDocument
    ' Purpose	:	This function will create and xml document header
    ' Parameters:	[in]  : 
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    Public Function CreateXmlDocument() As XmlDocument
        Dim xmlDoc As XmlDocument
        Dim xmlDecl As XmlDeclaration
        'Dim createXmlNode As XmlNode

        Try
            xmlDoc = New XmlDocument
            xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "")
            xmlDoc.InsertBefore(xmlDecl, xmlDoc.DocumentElement)
        Catch ex As Exception
            Throw (New ExceptionMsg("corOB_XmlUtil.clsWriteXml.CreateXmlDocument : " & ex.ToString))
        End Try

        Return xmlDoc
    End Function


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function CreateXmlAttribute
    ' Purpose	:	This function will create and xml attribue 
    ' Parameters:	[in]  : 
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    Public Function CreateXmlAttribute(ByRef pXmlNode As XmlNode, ByVal strAttribute As String, ByVal strValue As String) As XmlAttribute
        Dim xmlDoc As XmlDocument
        Dim xmlAttr As XmlAttribute

        Try
            xmlDoc = pXmlNode.OwnerDocument
            xmlAttr = xmlDoc.CreateAttribute(strAttribute)
            xmlAttr.Value = strValue
            pXmlNode.Attributes.SetNamedItem(xmlAttr)
        Catch ex As Exception
            Throw (New ExceptionMsg("corOB_XmlUtil.clsWriteXml.CreateXmlAttribute : " & ex.ToString))
        End Try

        Return xmlAttr
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function CreateDocument
    ' Purpose	    :   Creates a document object and creates the root node. 
    '                   Returns the root element object.
    ' Parameters    :	[in] sender: strRootName
    '   		        [in]      e: -
    '		            [out]      : XMLElement
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function CreateDocument(ByVal strRootName As String) As xmlElement 'Implements IcorOB_WriteXml.CreateDocument
        Try
            ' Create the Document object
            xmlDoc = New XmlDocument
            ' Create the root node element object
            xmlRoot = xmlDoc.CreateElement(strRootName)
            xmlDoc.AppendChild(xmlRoot)
            ' Add the root node element object to the document object
            CreateDocument = xmlRoot
            ' set the current element for AddAttibute
            xmlElement = xmlRoot

            'ContextUtil.SetComplete()
            Exit Function

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Xml.clsWriteXml.CreateDocument : " & ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function LoadDocument
    ' Purpose	    :   Load a document object and creates the root node. 
    '                   Returns the root element object.
    ' Parameters    :	[in] sender: strRootName
    '   		        [in]      e: -
    '		            [out]      : XMLElement
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function LoadDocument(ByVal strFileName As String, ByVal strRootName As String) As xmlElement 'Implements IcorOB_WriteXml.CreateDocument
        Dim objFileStream As New FileStream(strFileName, FileMode.Open, FileAccess.ReadWrite)
        Dim objStream As New StreamReader(objFileStream)

        Try
            ' Load the Document object
            xmlDoc = New XmlDocument
            xmlDoc.Load(objStream)
            ' Create the root node element object
            xmlRoot = xmlDoc.SelectSingleNode(strRootName)
            xmlDoc.AppendChild(xmlRoot)
            ' Add the root node element object to the document object
            LoadDocument = xmlRoot
            ' set the current element for AddAttibute
            xmlElement = xmlRoot

            'ContextUtil.SetComplete()
            Exit Function

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Xml.clsWriteXml.LoadDocument : " & ex.ToString))
        Finally
            objFileStream.Close()
            objStream.Close()
            objFileStream = Nothing
            objStream = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function AddElement
    ' Purpose	    :   Adds a new element as a child of the specified element node, 
    '                   and returns the new new elment object
    ' Parameters    :	[in] sender: parentElement,elementName,str
    '   		        [in]      e: -
    '		            [out]      : XMLElement
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function AddElement(ByVal xmlParentElement As xmlElement, ByVal strElementName As String, Optional ByVal str As String = "") As xmlElement 'Implements IcorOB_WriteXml.AddElement
        Dim xmlCurrElement As XmlElement

        Try
            xmlCurrElement = xmlDoc.CreateElement(strElementName)
            xmlElement = xmlCurrElement
            AddElement = xmlParentElement.AppendChild(xmlCurrElement)
            AddElement.InnerText = str

            'ContextUtil.SetComplete()
            Exit Function

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Xml.clsWriteXml.AddElement : " & ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function AddElement
    ' Purpose	    :   Adds attribute name and value to the most-recently-created element. 
    ' Parameters    :	[in] sender: attributeName,attributeValue
    '   		        [in]      e: -
    '		            [out]      : -
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub AddAttribute(ByVal strAttributeName As String, ByVal strAttributeValue As String) 'Implements IcorOB_WriteXml.AddAttribute
        Try
            Call xmlElement.SetAttribute(strAttributeName, strAttributeValue)

            'ContextUtil.SetComplete()
            Exit Sub

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Xml.clsWriteXml.AddAttribute : " & ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function xmlStr
    ' Purpose	    :   Return XML String
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: -
    '		            [out]      : -
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function XmlString() As String 'Implements IcorOB_WriteXml.XmlString
        Try
            XmlString = xmlDoc.OuterXml

            'ContextUtil.SetComplete()
            Exit Function

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Xml.clsWriteXml.XmlString : " & ex.ToString))
        Finally
            'set obj = Nothing
        End Try
    End Function
    '---------------------------------------------------------------------
    ' Procedure 	: 	Function xmlStr
    ' Purpose	    :   Save XML 
    ' Parameters    :	[in] sender: strDest
    '   		        [in]      e: -
    '		            [out]      : -
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub SaveXML(ByVal strDest As String) 'Implements IcorOB_WriteXml.SaveXml

        If IO.Directory.Exists(IO.Directory.GetParent(strDest).FullName) = False Then IO.Directory.CreateDirectory(IO.Directory.GetParent(strDest).FullName)
        Dim xmlWriter As New System.Xml.XmlTextWriter(strDest, Nothing)

        Try
            With xmlWriter
                .Indentation = 8
                .IndentChar = " "
                .Formatting = .Indentation
                xmlDoc.WriteTo(xmlWriter)
            End With
            xmlWriter.Flush()
            xmlWriter.Close()

            'ContextUtil.SetComplete()
            Exit Sub

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Xml.clsWriteXml.SaveXml : " & ex.ToString))
        Finally
            xmlWriter.Close()
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class


