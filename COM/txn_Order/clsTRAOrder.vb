'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsTRAOrder
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTRAOrder"
        End Get
    End Property
#Region "DDL"
    Public Function GetSRWhsList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_WHSCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCustList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_CUSTCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCustContList(ByVal strSalesRepCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_CONTCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRPrdList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_PRDCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRMReasonList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_MREASONCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetReasonList() As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_REASONCODE"
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetStorLocalList(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_STORLOCALCODE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetCollByList() As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_COLLCODE"
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetUOMCode(ByVal strPrdCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_UOMCODE"
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetUOMPrice(ByVal strPrdCode As String, ByVal strUOMCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_UOMPRICE"
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUOMCode, clsDB.DataType.DBString)
            .addOutput("UOM_PRICE", 2)
            Return .spRetrieveValue("UOM_PRICE")
        End With
        objDB = Nothing
    End Function

    Public Function GetCustDivision(strCustCode As String) As DataTable
        Dim objDB As clsDB
        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_CUST_DIVISION"
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

#Region "TXN NO"

    Public Function GetSRVouchNo(ByVal strSalesRepCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_VOUCHNO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addOutput("VOUCH_NO", 2)
            Return .spRetrieveValue("VOUCH_NO")
        End With
        objDB = Nothing
    End Function

    Public Function GetSRTxnNo(ByVal strSalesRepCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_TXNNO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addOutput("TXN_NO", 2)
            Return .spRetrieveValue("TXN_NO")
        End With
        objDB = Nothing
    End Function

    Public Function GetSRVisitID(ByVal strSalesRepCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_VISITID"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addOutput("VISIT_ID", 2)
            Return .spRetrieveValue("VISIT_ID")
        End With
        objDB = Nothing
    End Function

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCode(ByVal strUserId As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_SALESREPCODE"
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            .addOutput("SALESREP_CODE", 2)
            Return .spRetrieveValue("SALESREP_CODE")
        End With
        objDB = Nothing
    End Function

    Public Function GetSessionID(ByVal strUserId As String, ByVal strSalesrepCode As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_SESSIONID"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "TMP TRA DTL "

    Public Function GetTMPTRADetails(ByVal strTXNNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL"
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTMPTRADetailsEdit(ByVal strTXNNo As String, ByVal strLineNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_EDIT"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Sub InstTMPTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal strUomCode As String, _
    ByVal strBNo As String, ByVal strRetQty As String, ByVal strListPrice As String, ByVal strExpDate As String, ByVal strRetAmt As String _
    , ByVal strReasonCode As String, ByVal strRemarks As String, ByVal strUserId As String, ByVal strUserName As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("BNO", strBNo, clsDB.DataType.DBString, True)
            .addItem("RET_QTY", strRetQty, clsDB.DataType.DBString)
            .addItem("LIST_PRICE", strListPrice, clsDB.DataType.DBString)
            .addItem("EXP_DATE", strExpDate, clsDB.DataType.DBString)
            .addItem("RET_AMT", strRetAmt, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub UpdTMPTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal strUomCode As String, _
   ByVal strBNo As String, ByVal strRetQty As String, ByVal strListPrice As String, ByVal strExpDate As String, ByVal strRetAmt As String _
   , ByVal strReasonCode As String, ByVal strRemarks As String, ByVal strUserId As String, ByVal strLineNo As String, ByVal strUserName As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_UPDATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("BNO", strBNo, clsDB.DataType.DBString, True)
            .addItem("RET_QTY", strRetQty, clsDB.DataType.DBString)
            .addItem("LIST_PRICE", strListPrice, clsDB.DataType.DBString)
            .addItem("EXP_DATE", strExpDate, clsDB.DataType.DBString)
            .addItem("RET_AMT", strRetAmt, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .spUpdate()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelTMPTRADetails(ByVal strTXNNo As String, ByVal strLineNo As String, ByVal strUserId As String, ByVal strsessionid As String, ByVal strUserName As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_DELETE"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub

#End Region

#Region "TMP TRA HDR"
    Public Sub InstTMPTRAHeader(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVisitid As String, ByVal strVoucherNo As String, _
        ByVal strCustCode As String, ByVal strContCode As String, ByVal strPayerCode As String, ByVal strCollCode As String, ByVal strReasonCode As String, ByVal strSlocCode As String _
        , ByVal strCtnNo As String, ByVal strRefno As String, ByVal strRemarks As String, ByVal strSalesrepCode As String, ByVal strUserId As String, ByVal strUserName As String, ByVal strSAPBilling As String, strCustDivision As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_HDR_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitid, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("PAYER_CODE", strPayerCode, clsDB.DataType.DBString)
            .addItem("COLL_CODE", strCollCode, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("SLOC_CODE", strSlocCode, clsDB.DataType.DBString)
            .addItem("CTN_NO", strCtnNo, clsDB.DataType.DBString)
            .addItem("REF_NO", strRefno, clsDB.DataType.DBString, True)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString, True)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("SAP_BILLING", strSAPBilling, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .addItem("CUST_DIVISION", strCustDivision, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub InstTMPTRAHeaderV2(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVisitid As String, ByVal strVoucherNo As String, _
        ByVal strCustCode As String, ByVal strContCode As String, ByVal strPayerCode As String, ByVal strCollCode As String, ByVal strReasonCode As String, ByVal strSlocCode As String _
        , ByVal strCtnNo As String, ByVal strRefno As String, ByVal strRemarks As String, ByVal strSalesrepCode As String, ByVal strUserId As String, ByVal strUserName As String _
        , ByVal strVendorNo As String, ByVal strContractNo As String, ByVal strDepartmentCode As String, ByVal strDisposalDate As String, ByVal strSAPBilling As String, strCustDivision As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_HDR_CREATE_V2"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitid, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("PAYER_CODE", strPayerCode, clsDB.DataType.DBString)
            .addItem("COLL_CODE", strCollCode, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("SLOC_CODE", strSlocCode, clsDB.DataType.DBString)
            .addItem("CTN_NO", strCtnNo, clsDB.DataType.DBString)
            .addItem("REF_NO", strRefno, clsDB.DataType.DBString, True)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString, True)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .addItem("VENDOR_NO", strVendorNo, clsDB.DataType.DBString)
            .addItem("CONTRACT_NO", strContractNo, clsDB.DataType.DBString)
            .addItem("DEPARTMENT_CODE", strDepartmentCode, clsDB.DataType.DBString)
            .addItem("DISPOSAL_DATE", strDisposalDate, clsDB.DataType.DBString)
            .addItem("SAP_BILLING", strSAPBilling, clsDB.DataType.DBString)
            .addItem("CUST_DIVISION", strCustDivision, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub


    Public Function GetTMPTRAHeader(ByVal strTXNNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_HDR"
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function UpdTMPTRAHeader(ByVal strTXNNo As String, ByVal strUserId As String, ByVal strsessionid As String, ByVal strUserName As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_HDR_DTL_UPDATE"
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "TRA SUBMIT"

    Public Function InstTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVoucherNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strStatus As String, ByVal strUserName As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        Try
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_SUBMIT"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function KIVTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVoucherNo As String, ByVal strVisitID As String, _
       ByVal strSalesrepCode As String, ByVal strStatus As String, ByVal strUserName As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_KIV"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing

    End Function
#End Region

#Region "TRA Reset"
    Public Sub ResetTRAHdrDtl(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_HDR_DTL_RESET"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub

    Public Sub ReloadTRAHdrDtl(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, ByVal strUserName As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_TXN_TRADE_RET_DTL_KIV_RELOAD"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub
#End Region

#Region "Validation"
    Public Function ValidatePrd(ByVal strUserId As String, ByVal strPrdCode As String, ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_PRD_VALIDATE"
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function ValidateCust(ByVal strUserId As String, ByVal strCustCode As String, ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_CUST_VALIDATE"
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function ValidateSalesrep(ByVal strUserId As String, ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_GET_TRA_SALESREP_VALIDATE"
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "TRA KIV SUBMIT"

    Public Function InstTRAKIVDetails(ByVal strTxnNo As String, ByVal strVoucherNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strStatus As String, ByVal strUserName As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_KIV_TXN_TRADE_RET_DTL_SUBMIT"
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("Reason_Code", "", clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function InstTRAKIVDetailsV2(ByVal strTxnNo As String, ByVal strVoucherNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strStatus As String, ByVal strReasonCode As String, ByVal strUserName As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_SFA_TMP_KIV_TXN_TRADE_RET_DTL_SUBMIT"
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("Reason_Code", strReasonCode, clsDB.DataType.DBString)
            .addItem("USERNAME", strUserName, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function


#End Region

#Region "Search Salesrep"
    Public Function GetSRList(ByVal strSalesrepCode As String, ByVal strSalesrepName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_SFA_GET_SRCODE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SALESREP_NAME", strSalesrepName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


#End Region

#Region "EXCEPTION"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
