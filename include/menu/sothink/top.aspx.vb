Imports System.Data
Partial Class top
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private dtAR As DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not IsPostBack Then
            Dim objcookie As New HttpCookie("menu")
            TheDateTime.Text = DateTime.Now.ToString("dddd: dd MMMM yyyy")
            lblUserName.Text = "Welcome " & Session("UserName") & " ! "
            If Not IsNothing(objcookie) Then
                If GetAccessRight(4) Then
                    objcookie.Values.Add("mainmenu", "FFMR")
                    lnkFFMR_Click(sender, e)
                ElseIf GetAccessRight(3) Then
                    objcookie.Values.Add("mainmenu", "FFMS")
                    lnkFFMS_Click(sender, e)
                ElseIf GetAccessRight(2) Then
                    objcookie.Values.Add("mainmenu", "FFMA")
                    lnkFFMA_Click(sender, e)
                ElseIf GetAccessRight(1) Then
                    objcookie.Values.Add("mainmenu", "ADMIN")
                    lnkAdmin_Click(sender, e)
                End If

                If GetAccessRight(4) = False Then mnu1FFMRL.Visible = False
                If GetAccessRight(3) = False Then mnu1FFMSL.Visible = False
                If GetAccessRight(2) = False Then mnu1FFMAL.Visible = False
                If GetAccessRight(1) = False Then mnu1ADMINL.Visible = False
                'If GetAccessRight(1, 259, "'1'") = False Then mnu1EPOndemand.Visible = False

                Response.AppendCookie(objcookie)
            End If

            Dim objtargetcookie As New HttpCookie("targetframe")
            If Not IsNothing(objtargetcookie) Then
                objtargetcookie.Values.Add("frame", "fraMain")
                Response.AppendCookie(objtargetcookie)
            End If
            GetMenuItems()
        End If
    End Sub

    Sub ClearCookie()
        'Removes Cookie by setting Expires date to the past. 
        'Called when Remove Cookie Button is set 
        Dim objcookie As New HttpCookie("menu")
        objcookie = Request.Cookies("menu")
        If Not IsNothing(objcookie) Then
            objcookie.Expires = New DateTime(2000, 1, 1)
            Response.AppendCookie(objcookie)
        End If
        Dim objtargetcookie As New HttpCookie("targetframe")
        objtargetcookie = Request.Cookies("frame")
        If Not IsNothing(objtargetcookie) Then
            objtargetcookie.Expires = New DateTime(2000, 1, 1)
            Response.AppendCookie(objtargetcookie)
        End If
    End Sub


#Region "Sothink"
    Private Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        Try
            dtAR = Session("UserAccessRight")
            If Not IsNothing(dtAR) Then
                If dblSubModuleID = 0 Then
                    drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
                Else
                    drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
                End If

                If drCurrRow.Length > 0 Then
                    blnValue = True
                End If

                Return blnValue
            End If
        Catch ex As Exception

        End Try
    End Function
    Private Sub GetMenuItems()
        'Alex Chia, dynamic sothink menu dispaly settings
        Dim sbJavaString As New System.Text.StringBuilder
        Dim strMenu_1, strLink_1, strEffect_1 As String
        Dim strMenu_2, StrLink_2, strEffect_2 As String

        strMenu_1 = ",'','',-1,-1,0,'','','','','','',0,10,0,'arrow_r.gif','arrow_r.gif',7,7,0,0,1,'',1,'#E1ECF6',0,'newline.gif','newline.gif',3,3,1,1,'#0099cc','#0066CC','#777777','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0"
        strMenu_2 = ",'','',-1,-1,0,'','','','','','',0,10,0,path +'include/menu/sothink/arrow_r.gif',path +'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'',1,'#E1ECF6',0,path +'include/menu/sothink/newline.gif',path +'include/menu/sothink/newline.gif',3,3,1,1,'#0099cc','#0066CC','#777777','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0"
        strLink_1 = ",'fraMain','','','','',5,0,0,'','',0,0,0,0,1,'#FAFFFF',1,'#E1ECF6',0,'newline.gif','',3,0,1,1,'#0099cc','#0066CC','#777777','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana'"
        StrLink_2 = ",'fraMain','','','','',5,0,0,'','',0,0,0,0,1,'#FAFFFF',1,'#E1ECF6',0,path +'include/menu/sothink/newline.gif',''"
        strEffect_1 = "1,4,0,0,2,3,5,0,100,'stEffect(\'slip\')',-2,'stEffect(\'slip\')',-2,80,0,0,'#999999','#FFFFFF',path +'include/menu/sothink/bg_02.gif',3,0,0,'#d0d0d0 #d0d0d0 #d0d0d0 #d0d0d0'"
        strEffect_2 = "1,2,0,0,2,3,5,0,100,'stEffect(\'slip\')',-2,'stEffect(\'slip\')',-2,80,0,0,'#999999','#FFFFFF',path +'include/menu/sothink/bg_02.gif',3,0,0,'#d0d0d0 #d0d0d0 #d0d0d0 #d0d0d0'"
        Try
            txtMenuItems.Text = ""
            txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript'>"
            txtMenuItems.Text = txtMenuItems.Text & "path='../../../../../../" & ConfigurationManager.AppSettings("ServerName") & "/';"
            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"

            'txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
            ''path='../../../../../../Echoplus/';
            ''txtMenuItems.Text = txtMenuItems.Text & "path='" & ResolveClientUrl("~/") & "';"
            ''txtMenuItems.Text = txtMenuItems.Text & "path='" & Request.ApplicationPath & "/';"


            'txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
            ''FFMR
            If GetAccessRight(4) Then
                txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu37fe',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,19,100,500,1,0,0,'','',83886139,0,1,1,'hand','hand',''],this);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,1,3,0,7,100,'',-2,'',-2,52,0,0,'#999999','transparent','',1,0,0,'#b8cddb']);"

                'DRC
                If GetAccessRight(4, 5, "'1','8'") Or GetAccessRight(4, 63, "'1','8'") Or GetAccessRight(4, 184, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'INVENTORY FORECASTING'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p1',[" & strEffect_1 & "]);"
                    If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i0','p0i0',[0,'Inventory Forecasting Analysis','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 63, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i1','p1i0',[0,'Inventory Forecasting Enquiry','','',-1,-1,0,path + 'iFFMR/DRC/DRCEnquiryList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 184, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i2','p1i0',[0,'Inventory Forecasting Extraction','','',-1,-1,0,path + 'iFFMR/DRC/DRCExtractList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                Else
                    'txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'','','',-1,-1,0,'','','','','','',0,0,0,'','',0,0,0,0,1,'',1,'',1,'','',0,0,0,0,'#0099cc','#0066CC','#777777','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"

                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'','','',-1,-1,0,'','','','','','',0,0,0,'','',0,0,0,0,1,'',1,'',1,'','',0,0,0,0,'#0099cc','#0066CC','#777777','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p1',[1,4,0,0,2,3,5,0,100,'',-2,'',-2,80,0,0,'#999999','#FFFFFF','',0,0,0,'']);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i0','p0i0',[0,'','','',-1,-1,0,'','_self']);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'SALES
                If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Or GetAccessRight(4, 73, "'1','8'") Or GetAccessRight(4, 72, "'1','8'") Or GetAccessRight(4, 120, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i1','p0i0',[0,'SALES'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p1',[" & strEffect_1 & "]);"
                    If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p2i0','p0i1',[0,'Sales Analysis','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 73, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p2i1','p0i1',[0,'Key Product Sales','','',-1,-1,0,path + 'iFFMR/Sales/SalesInfoByKeyPrd.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 72, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p2i2','p0i1',[0,'Top 80% Customer Sales','','',-1,-1,0,path + 'iFFMR/Sales/SalesTop80CustByRep.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 120, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p2i3','p0i1',[0,'Sales By Customer Count','','',-1,-1,0,path + 'iFFMR/Sales/SalesByCustCount.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p2i4','p0i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'SALES FORCE EFFECTIVENESS
                If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 61, "'1','8'") Or _
                    GetAccessRight(4, 58, "'1','8'") Or GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Or _
                    GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 62, "'1','8'") Or GetAccessRight(4, 14, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i2','p0i0',[0,'FIELD FORCE EFFECTIVENESS'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p1',[1,4,0,0,2,3,5,7]);"
                    If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i0','p0i2',[0,'Call Analysis'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p3',[" & strEffect_2 & "]);"
                        If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p4i0','p3i0',[0,'Monthly Call Analysis','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p4i1','p3i0',[0,'Daily Call Analysis','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 61, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p4i2','p3i0',[0,'Strike Call Analysis','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByStrike.aspx'" & StrLink_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If
                    If GetAccessRight(4, 58, "'1','8'") Or GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Or GetAccessRight(4, 313, "'1','8'") Then
                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i1','p0i2',[0,'Call Coverage'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p3',[" & strEffect_2 & "]);"
                        If GetAccessRight(4, 58, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i0','p3i1',[0,'Actual Call Coverage','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallAnalysisListByContact.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i1','p3i1',[0,'Effective Call Coverage','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 313, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i2','p3i1',[0,'Effective Call Coverage (Adv)','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1Adv.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i3','p3i1',[0,'Hit Call Coverage','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx'" & StrLink_2 & "]);"

                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If
                    If GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 62, "'1','8'") Or GetAccessRight(4, 14, "'1','8'") Then
                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i2','p0i2',[0,'Field Force Activity'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p6','p3',[" & strEffect_2 & "]);"
                        If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i0','p3i2',[0,'Monthly Field Force Activity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 62, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i1','p3i2',[0,'Field Force Activity By Class','','',-1,-1,0,path + 'iFFMR/SFE/SFMSPRFM/SFMSPerformance.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i2','p3i2',[0,'Activity List','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx'" & StrLink_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If
                    If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i3','p0i2',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i4','p0i2',[0,'Call Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 311, "'1','8'") Or GetAccessRight(4, 319, "'1','8'") Then
                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i2','p0i2',[0,'Detailing'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p6','p3',[" & strEffect_2 & "]);"
                        If GetAccessRight(4, 311, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i0','p3i2',[0,'Coverage by Specialty by Product','','',-1,-1,0,path + 'iFFMR/Detailing/SpecialtyCoverageByProd.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 319, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i1','p3i2',[0,'Call by Area by Product','','',-1,-1,0,path + 'iFFMR/Detailing/CallByAreaByProduct.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(4, 328, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i2','p3i2',[0,'Coverage and Call By Class By Product','','',-1,-1,0,path + 'iFFMR/Detailing/CallByClassByProduct.aspx'" & StrLink_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'KEY PEROFRMANCE INDEX
                If GetAccessRight(4, 38, "'1','8'") Or GetAccessRight(4, 71, "'1','8'") Or _
                   GetAccessRight(4, 40, "'1','8'") Or GetAccessRight(4, 37, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i3','p0i0',[0,'KEY PERFORMANCE INDEX'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p7','p1',[" & strEffect_1 & "]);"
                    If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i0','p0i3',[0,'Monthly KPI','','',-1,-1,0,path + 'iFFMR/KPI/MthOverview.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 71, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i1','p0i3',[0,'Customer KPI','','',-1,-1,0,path + 'iFFMR/KPI/CustStatusList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 40, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i2','p0i3',[0,'Field Force KPI','','',-1,-1,0,path + 'iFFMR/KPI/ActSalesBySalesrep.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i3','p0i3',[0,'Relative KPI','','',-1,-1,0,path + 'iFFMR/KPI/RelPerformance.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 315, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i4','p0i3',[0,'Contact KPI','','',-1,-1,0,path + 'iFFMR/KPI/ContStatusList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 312, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i5','p0i3',[0,'SFE KPI','','',-1,-1,0,path + 'iFFMR/KPI/SFEKPIList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'COMMUNICATION
                If GetAccessRight(4, 4, "'1','8'") Or GetAccessRight(4, 60, "'1','8'") Or GetAccessRight(4, 118, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i4','p0i0',[0,'COMMUNICATION'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p8','p1',[" & strEffect_1 & "]);"
                    If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i0',p0i4,[0,'Refresh & Upload','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 60, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i1',p0i4,[0,'Transaction Upload','','',-1,-1,0,path + 'iFFMR/Comm/CommUploadInfo.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 118, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i2',p0i4,[0,'Audit Log','','',-1,-1,0,path + 'iFFMR/Comm/AuditLog.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'EXTRACTION
                If GetAccessRight(4, 123, "'1','8'") Or GetAccessRight(4, 220, "'1','8'") Or GetAccessRight(4, 220, "'1','8'") Or GetAccessRight(4, 247, "'1','8'") Or GetAccessRight(4, 288, "'1','8'") Or GetAccessRight(4, 300, "'1','8'") Or GetAccessRight(4, 301, "'1','8'") Or GetAccessRight(4, 302, "'1','8'") Or GetAccessRight(4, 303, "'1','8'") Then   'GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i5','p0i0',[0,'EXTRACTION'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p9','p1',[" & strEffect_1 & "]);"
                    'If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i0','p0i5',[0,'Customer','','',-1,-1,0,path + 'blank.aspx'" & StrLink_2 & "]);"
                    'If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i1','p0i5',[0,'DRC','','',-1,-1,0,path + 'blank.aspx'" & StrLink_2 & "]);"
                    'If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i2','p0i5',[0,'MSS','','',-1,-1,0,path + 'blank.aspx'" & StrLink_2 & "]);"
                    'If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i3','p0i5',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx'" & StrLink_2 & "]);"
                    'If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 123, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i0','p0i5',[0,'Field Activities Extraction','','',-1,-1,0,path + 'iFFMR/Customize/SFMSExtraction.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 220, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i1','p0i5',[0,'Field Activities Extraction 2','','',-1,-1,0,path + 'iFFMR/Customize/SFMSExtraction2.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 247, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i2','p0i5',[0,'Preplan Activities Extraction','','',-1,-1,0,path + 'iFFMR/Customize/PrePlanExtraction.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 258, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i3','p0i5',[0,'MSS Sampling Report','','',-1,-1,0,path + 'iFFMR/Customize/MSSExtraction.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 288, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i4','p0i5',[0,'Field Activities Extraction Extra Cat','','',-1,-1,0,path + 'iFFMR/Customize/SFMSExtractionExtraCat.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 300, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i4','p0i5',[0,'Field Activities Extraction','','',-1,-1,0,path + 'iFFMR/Customize/SFMSExtract.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 301, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i4','p0i5',[0,'Visit Extraction','','',-1,-1,0,path + 'iFFMR/Customize/VisitExtract.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 302, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i4','p0i5',[0,'Customer Contact Extraction','','',-1,-1,0,path + 'iFFMR/Customize/CustContExtract.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 303, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i4','p0i5',[0,'Customer Contact Product Extraction','','',-1,-1,0,path + 'iFFMR/Customize/CustContPrdExtract.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'CUSTOMIZATION
                If GetAccessRight(4, 64, "'1','8'") Or GetAccessRight(4, 68, "'1','8'") Or _
                   GetAccessRight(4, 69, "'1','8'") Or GetAccessRight(4, 70, "'1','8'") Or _
                   GetAccessRight(4, 103, "'1','8'") Or GetAccessRight(4, 104, "'1','8'") Or _
                   GetAccessRight(4, 105, "'1','8'") Or GetAccessRight(4, 124, "'1','8'") Or _
                   GetAccessRight(4, 125, "'1','8'") Or GetAccessRight(4, 165, "'1','8'") Or _
                   GetAccessRight(4, 166, "'1','8'") Or GetAccessRight(4, 168, "'1','8'") Or _
                   GetAccessRight(4, 169, "'1','8'") Or GetAccessRight(4, 170, "'1','8'") Or _
                    GetAccessRight(4, 175, "'1','8'") Or GetAccessRight(4, 182, "'1','8'") Or _
                    GetAccessRight(4, 186, "'1','8'") Or GetAccessRight(4, 192, "'1','8'") Or _
                    GetAccessRight(4, 193, "'1','8'") Or GetAccessRight(4, 196, "'1','8'") Or _
                     GetAccessRight(4, 200, "'1','8'") Or GetAccessRight(4, 201, "'1','8'") Or _
                   GetAccessRight(4, 202, "'1','8'") Or GetAccessRight(4, 199, "'1','8'") Or _
                    GetAccessRight(4, 214, "'1','8'") Or GetAccessRight(4, 215, "'1','8'") Or _
                   GetAccessRight(4, 216, "'1','8'") Or GetAccessRight(4, 217, "'1','8'") Or _
                    GetAccessRight(4, 224, "'1','8'") Or GetAccessRight(4, 223, "'1','8'") Or _
                    GetAccessRight(4, 229, "'1','8'") Or GetAccessRight(4, 230, "'1','8'") Or _
                    GetAccessRight(4, 234, "'1','8'") Or GetAccessRight(4, 235, "'1','8'") Or _
                    GetAccessRight(4, 236, "'1','8'") Or GetAccessRight(4, 237, "'1','8'") Or _
                    GetAccessRight(4, 238, "'1','8'") Or GetAccessRight(4, 241, "'1','8'") Or _
                    GetAccessRight(4, 243, "'1','8'") Or GetAccessRight(4, 244, "'1','8'") Or _
                    GetAccessRight(4, 245, "'1','8'") Or GetAccessRight(4, 246, "'1','8'") Or _
                     GetAccessRight(4, 249, "'1','8'") Or GetAccessRight(4, 250, "'1','8'") Or _
                     GetAccessRight(4, 251, "'1','8'") Or GetAccessRight(4, 289, "'1','8'") Or _
                     GetAccessRight(4, SubModuleType.CUZMSSENQUIRY, "'1','8'") Or GetAccessRight(4, SubModuleType.CUZSFMSENQUIRY, "'1','8'") Or _
                    GetAccessRight(4, SubModuleType.CUSTPROFILEENQ, "'1','8'") Or GetAccessRight(4, SubModuleType.MISSCALLANALYSIS, "'1','8'") Or _
                    GetAccessRight(4, SubModuleType.SALES_BY_CHAIN, "'1','8'") Or GetAccessRight(4, SubModuleType.REMAIN_CALL, "'1','8'") Or _
                     GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_BIMONTH, "'1','8'") Or GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUARTER, "'1','8'") Or _
                       GetAccessRight(4, SubModuleType.FIELDFORCEMTHACTYPrd, "'1','8'") Or GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUADMONTH, "'1','8'") Or _
                       GetAccessRight(4, SubModuleType.MUSTSELL, "'1','8'") Or GetAccessRight(4, SubModuleType.ONLINEGALLERY, "'1','8'") Or _
                       GetAccessRight(4, SubModuleType.MERCHANDISING, "'1','8'") Or GetAccessRight(4, SubModuleType.TRAREPORT_EDI, "'1','8'") Or _
                    GetAccessRight(4, SubModuleType.COMPETITOR, "'1','8'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i6','p0i0',[0,'CUSTOMIZATION'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p10','p1',[1,4,0,0,2,3,0,0]);"
                    If GetAccessRight(4, 64, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i0','p0i6',[0,'Payment Collection Cheque Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/CollCheqEnquiryList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 68, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i1','p0i6',[0,'Promotion Acceptance Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/PAFEnquiryList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 69, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i2','p0i6',[0,'Goods Return By Month End Closing','','',-1,-1,0,path + 'iFFMR/Customize/TRARetByMth.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 70, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i3','p0i6',[0,'Field Force Stock Allocation','','',-1,-1,0,path + 'iFFMR/Customize/SalesRepStockAllocation.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 103, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i4','p0i6',[0,'Call Rate','','',-1,-1,0,path + 'iFFMR/Customize/CallRate.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 104, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i5','p0i6',[0,'Sales Target Vs Actual','','',-1,-1,0,path + 'iFFMR/Customize/SalesTgtVsActual.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 105, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i6','p0i6',[0,'Sales Estimation Vs Actual','','',-1,-1,0,path + 'iFFMR/Customize/SalesEstVsActual.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 124, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i7','p0i6',[0,'Call Info By Month','','',-1,-1,0,path + 'iFFMR/Customize/CallInfoByMonth.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 125, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i8','p0i6',[0,'Salesforce Capacity By Month','','',-1,-1,0,path + 'iFFMR/Customize/SalesForceCapacity.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 165, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i9','p0i6',[0,'Call Analysis by DKSH Class','','',-1,-1,0,path + 'iFFMR/Customize/CallByDKSHClass.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 166, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i10','p0i6',[0,'Call Analysis by Supplier Class','','',-1,-1,0,path + 'iFFMR/Customize/CallBySupplierClass.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 168, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i11','p0i6',[0,'Call Activity Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/CallActyEnquiryList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 169, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i12','p0i6',[0,'Field Force Monthly Activity' ,'','',-1,-1,0,path + 'iFFMR/Customize/SalesrepMthActy.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 170, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i13','p0i6',[0,'Field Force Productivity Freq','','',-1,-1,0,path + 'iFFMR/Customize/SalesrepPrdFreq.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 175, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i14','p0i6',[0,'Call Analysis Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/CallAnalysisEnquiry.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 182, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i15','p0i6',[0,'Monthly Field Force Activity by Supplier','','',-1,-1,0,path + 'iFFMR/Customize/CallProdSupList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 186, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i16','p0i6',[0,'Technician Itinerary','','',-1,-1,0,path + 'iFFMR/Customize/TechnicianItineraryList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 192, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i17','p0i6',[0,'Sales Order with Product Matrix','','',-1,-1,0,path + 'iFFMR/Customize/SalesOrderPrdMatrix.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 193, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i18','p0i6',[0,'Field Force Call Rate','','',-1,-1,0,path + 'iFFMR/Customize/SalesrepCallRate.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 196, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i19','p0i6',[0,'Service Statistic','','',-1,-1,0,path + 'iFFMR/Customize/ServiceStatisticHdr.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 196, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i19','p0i6',[0,'Service Statistic','','',-1,-1,0,path + 'iFFMR/Customize/ServiceStatisticHdr.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 199, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i20','p0i6',[0,'Collection Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/CollEnq.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 200, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i21','p0i6',[0,'Inventory Forecasting by Customer','','',-1,-1,0,path + 'iFFMR/Customize/DRCByCUST.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 201, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i22','p0i6',[0,'Inventory Forecasting by Customer Class','','',-1,-1,0,path + 'iFFMR/Customize/DRCByCustClass.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 202, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i23','p0i6',[0,'Inventory Forecasting by District','','',-1,-1,0,path + 'iFFMR/Customize/DRCByDistrict.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 214, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i24','p0i6',[0,'Monthly Productive Frequency By Field Force','','',-1,-1,0,path + 'iFFMR/Customize/PrdFreqMthly.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 215, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i25','p0i6',[0,'Monthly Territory ','','',-1,-1,0,path + 'iFFMR/Customize/TerritoryMthly.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 216, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i26','p0i6',[0,'Monthly Specialty','','',-1,-1,0,path + 'iFFMR/Customize/SpecialityMthly.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 217, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i27','p0i6',[0,'Field Force Score Card','','',-1,-1,0,path + 'iFFMR/Customize/ScoreCardMthly.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 223, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i27','p0i6',[0,'Package Analysis','','',-1,-1,0,path + 'iFFMR/Customize/PckgAnalysis.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 224, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i28','p0i6',[0,'Speciality Call Analysis','','',-1,-1,0,path + 'iFFMR/Customize/CallDtlByGroup.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 229, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i29','p0i6',[0,'Sales of Customer Per Product','','',-1,-1,0,path + 'iFFMR/Customize/SalesCustPerProduct.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 230, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i30','p0i6',[0,'Sales By Customer Group','','',-1,-1,0,path + 'iFFMR/Customize/SalesListCuz.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 234, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i31','p0i6',[0,'Customer Sales Analysis','','',-1,-1,0,path + 'iFFMR/Customize/CustSalesAnalysis.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 235, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i32','p0i6',[0,'Customer Product Sales Analysis','','',-1,-1,0,path + 'iFFMR/Customize/CustPrdSalesAnalysis.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 236, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i33','p0i6',[0,'Sales Order Variance','','',-1,-1,0,path + 'iFFMR/Customize/SOVariance.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 237, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i34','p0i6',[0,'Preplan Vs Actual By Customer','','',-1,-1,0,path + 'iFFMR/Customize/PlanVsActual.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 238, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i35','p0i6',[0,'Call Rate By Brand Report','','',-1,-1,0,path + 'iFFMR/Customize/CallRateByBrand.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 241, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i36','p0i6',[0,'Goods Return Report','','',-1,-1,0,path + 'iFFMR/Customize/TRAReport.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 243, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i37','p0i6',[0,'Merchandiser Stock and Display Check Report','','',-1,-1,0,path + 'iFFMR/Customize/MerchSfms/MerchSfms.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 244, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i38','p0i6',[0,'Salesrep KPI Report','','',-1,-1,0,path + 'iFFMR/Customize/SalesrepKPI/SalesrepKPI.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 245, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i39','p0i6',[0,'Daily Call Distribution Report','','',-1,-1,0,path + 'iFFMR/Customize/DailyCallDistribution/DailyCallDistribution.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 246, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i40','p0i6',[0,'Inventory Forecasting Analysis Cuz. Report','','',-1,-1,0,path + 'iFFMR/Customize/DRCCuzList/DRCCuzList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 249, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i41','p0i6',[0,'Call Achievement Report','','',-1,-1,0,path + 'iFFMR/Customize/CallAchievement/CallAchievement.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 250, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i42','p0i6',[0,'Daily Call Analysis Adv Report','','',-1,-1,0,path + 'iFFMR/Customize/CallByMonthAdv/CallByDayAdv.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 251, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i43','p0i6',[0,'Monthly Call Analysis Adv Report','','',-1,-1,0,path + 'iFFMR/Customize/CallByMonthAdv/CallByMonthAdv.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.CUZMSSENQUIRY, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i44','p0i6',[0,'Customer Market Survey Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/Geomap/MSSEnquiry.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.CUZSFMSENQUIRY, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i45','p0i6',[0,'Customer Field Activity Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/Geomap/SFMSEnquiry.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.CUSTPROFILEENQ, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i46','p0i6',[0,'Customer Profile Enquiry','','',-1,-1,0,path + 'iFFMR/Customize/Geomap/CustProfileEnq.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.MISSCALLANALYSIS, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i47','p0i6',[0,'Missing Call Analysis','','',-1,-1,0,path + 'iFFMR/Customize/MissCallAnalysis/MissCallAnalysis.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.SALES_BY_CHAIN, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i48','p0i6',[0,'Sales Report By Chain','','',-1,-1,0,path + 'iFFMR/Customize/SalesByChainCustMain.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.REMAIN_CALL, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i49','p0i6',[0,'Call Remaining Report','','',-1,-1,0,path + 'iFFMR/Customize/CallRemainingReport.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_BIMONTH, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i50','p0i6',[0,'Stock Consumption By Bi Month','','',-1,-1,0,path + 'iFFMR/Customize/StkConsump/StkConsumpByBiMthMain.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUARTER, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i51','p0i6',[0,'Stock Consumption By Quarter','','',-1,-1,0,path + 'iFFMR/Customize/StkConsump/StkConsumpByQtrMain.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.FIELDFORCEMTHACTYPrd, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i52','p0i6',[0,'Field Force Monthly Activity By Product','','',-1,-1,0,path + 'iFFMR/Customize/SalesrepMthActyPrd.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.STK_CONSUMP_BY_QUADMONTH, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i53','p0i6',[0,'Stock Consumption By 4 Month','','',-1,-1,0,path + 'iFFMR/Customize/StkConsump/StkConsumpByQuadMthMain.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, 289, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i54','p0i6',[0,'Actual Call KPI','','',-1,-1,0,path + 'iFFMR/Customize/ActCallKPI/ActualCallKPI.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.TRAREPORT_EDI, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i55','p0i6',[0,'TRA Report','','',-1,-1,0,path + 'iFFMR/Customize/TRAReport/TRAReportMain.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.MUSTSELL, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i56','p0i6',[0,'Product Must Sell','','',-1,-1,0,path + 'iFFMR/Customize/MustSell.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.ONLINEGALLERY, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i57','p0i6',[0,'Online Gallery','','',-1,-1,0,path + 'iFFMR/OnlineGallery.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.MERCHANDISING, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i58','p0i6',[0,'Merchandising','','',-1,-1,0,path + 'iFFMR/Customize/Merchandising.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.COMPETITOR, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i59','p0i6',[0,'Competitor Activity and Pricing by Product','','',-1,-1,0,path + 'iFFMR/Customize/Competitor.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.CUSTOMERCOVERAGE, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i74','p0i6',[0,'Customer Coverage','','',-1,-1,0,path + 'iFFMR/Customize/Geomap/CustomerCoverage.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.DAILYTRACKING, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i74','p0i6',[0,'Daily Tracking','','',-1,-1,0,path + 'iFFMR/Customize/Geomap/DailyTracking.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.HISTORICALTRACKING, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i74','p0i6',[0,'Historical Tracking','','',-1,-1,0,path + 'iFFMR/Customize/Geomap/HistoricalTracking.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(4, SubModuleType.VIEW_SALESMAN_ACTIVITY, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i77','p0i6',[0,'View Salesman Activity','','',-1,-1,0,path + 'iFFMR/Customize/ViewSalesmanActivity/ViewSalesmanActivity.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                'SFA
                If GetAccessRight(4, 212, "'1','8'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i7','p0i0',[0,'SFA','','',-1,-1,0,path + 'iFFMR/SFA/Redirect.aspx'" & strLink_1 & "]);"
                End If

                If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i8','p0i0',[0,'DATA FILE','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx'" & strLink_1 & "]);"
                If GetAccessRight(4, 228, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i9','p0i0',[0,'DATA FILE LIST','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFileList_V2.aspx'" & strLink_1 & "]);"

                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
                txtMenuItems.Text = txtMenuItems.Text & "}"
            End If
            'FFMS
            If GetAccessRight(3) Then
                txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,0,0,'#000000']);"
                If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                'If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 43, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'MESSAGING','','',-1,-1,0,path + 'iFFMS/Messaging/MessagingList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 45, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'PERFORMANCE','','',-1,-1,0,path + 'iFFMS/Performance/PerformanceOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 44, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'REPORTING','','',-1,-1,0,path + 'iFFMS/Report/ReportHeader.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 46, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'SUMMARY','','',-1,-1,0,path + 'iFFMS/Summary/SummaryListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 47, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'PRE PLAN','','',-1,-1,0,path + 'iFFMS/Plan/PlanMenu.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 248, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'PRE PLAN Advance','','',-1,-1,0,path + 'iFFMS/PlanCuz/PlanCuzMenu.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 102, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i8',[0,'DATA FILE','','',-1,-1,0,path + 'iFFMS/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 190, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i9',[0,'TRA List','','',-1,-1,0,path + 'iFFMS/Order/TRAOrderList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(3, 221, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i10',[0,'LEAVE','','',-1,-1,0,path + 'iFFMS/Leave/LeaveList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
                txtMenuItems.Text = txtMenuItems.Text & "}"
            End If

            'FFMA
            If GetAccessRight(ModuleID.FFMA) Then
                txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMA'){"
                'txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu37fe',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,19,100,500,1,0,0,'','',83886139,0,1,1,'hand','hand',''],this);"
                'txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,0,0,'#000000']);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,1,3,0,7,100,'',-2,'',-2,52,0,0,'#999999','transparent','',1,0,0,'#b8cddb']);"

                If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANY, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.SALESAREA, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.WAREHOUSE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.REGION, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'GENERAL'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANY, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i0','p0i0',[0,'Company','','',-1,-1,0,path + 'iFFMA/General/CompanyList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESAREA, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i1','p0i0',[0,'Sales Area','','',-1,-1,0,path + 'iFFMA/General/SalesAreaList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.WAREHOUSE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i2','p0i0',[0,'Warehouse','','',-1,-1,0,path + 'iFFMA/General/WarehouseList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.REGION, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i3','p0i0',[0,'Region','','',-1,-1,0,path + 'iFFMA/General/RegionList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                Else
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'','','',-1,-1,0,'','','','','','',0,0,0,'','',0,0,0,0,1,'',1,'',1,'','',0,0,0,0,'#0099cc','#0066CC','#777777','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p1',[1,4,0,0,2,3,5,0,100,'',-2,'',-2,80,0,0,'#999999','#FFFFFF','',0,0,0,'']);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p1i0','p0i0',[0,'','','',-1,-1,0,'','_self']);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i1','p0i0',[0,'SALESTEAM'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p2i0','p0i1',[0,'SalesTeam Info','','',-1,-1,0,path + 'iFFMA/Salesteam/SalesteamList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.SHIPTO, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTPROFILEMAINTAIN, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTTGTPRD, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.CUST_TEAM_CLASS, "'1'") Then
                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i2','p0i0',[0,'SALES ACCOUNT'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i0','p0i2',[0,'Customer Info','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustomerList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.SHIPTO, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i1','p0i2',[0,'Shipto Info','','',-1,-1,0,path + 'iFFMA/SalesAccount/ShipToList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTINFO, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i2','p0i2',[0,'Other Customer Info','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustomerInfoList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTCLASS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i3','p0i2',[0,'Class Info','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustomerClassList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTSALESTGT, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i4','p0i2',[0,'Sales Target By Customer','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustomerSalesTgtList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTPROFILEMAINTAIN, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i5','p0i2',[0,'Customer Profile Maintenance','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustProfileMaintenance.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTTGTPRD, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i5','p0i2',[0,'Customer Target Product','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustomerTgtPrdList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CUST_TEAM_CLASS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p3i5','p0i2',[0,'Customer Team Class','','',-1,-1,0,path + 'iFFMA/SalesAccount/CustomerTeamClassList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.CONTACT, "'1'") Or GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT, "'1'") Or GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT_DTL, "'1'") Or _
                 GetAccessRight(ModuleID.FFMA, SubModuleType.MESSAGING, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i3','p0i0',[0,'CONTACT'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.CONTACT, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p4i0','p0i3',[0,'Contact Info','','',-1,-1,0,path + 'iFFMA/Contact/ContactList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p4i1','p0i3',[0,'Mss Contact Update','','',-1,-1,0,path + 'iFFMA/HCD/MssCont.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_CONT_DTL, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p4i1','p0i3',[0,'Mss Contact Details','','',-1,-1,0,path + 'iFFMA/HCD/MssContDtl.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTGROUP, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.SUPPLIER, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKALLOC, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT_MATRIX, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTMUSTSELL, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i4','p0i0',[0,'PRODUCT'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i0','p0i4',[0,'Product Info','','',-1,-1,0,path + 'iFFMA/Product/ProductList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTGROUP, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i1','p0i4',[0,'Product Group Info','','',-1,-1,0,path + 'iFFMA/Product/ProductGroupList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.SUPPLIER, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i2','p0i4',[0,'Supplier Info','','',-1,-1,0,path + 'iFFMA/Product/SupplierList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKALLOC, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i3','p0i4',[0,'Stock Allocation','','',-1,-1,0,path + 'iFFMA/Product/StockAllocList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT_MATRIX, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i4','p0i4',[0,'Product Matrix','','',-1,-1,0,path + 'iFFMA/Product/ProductMatrixList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i4','p0i4',[0,'Product Sequence','','',-1,-1,0,path + 'iFFMA/Others/PrdSeqList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTMUSTSELL, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i4','p0i4',[0,'Product Must Sell','','',-1,-1,0,path + 'iFFMA/Product/PrdMustSellList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTMUSTSELLADV, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p5i5','p0i4',[0,'Product Must Sell (Adv.)','','',-1,-1,0,path + 'iFFMA/Product/PrdMustSellListAdv.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDPRICE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALPRICE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPPRICE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPPRICE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDBONUS, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALBONUS, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPBONUS, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPBONUS, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PACKAGE, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i5','p0i0',[0,'TRADE DEAL'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p6','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDPRICE, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALPRICE, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPPRICE, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPPRICE, "'1'") Then

                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i0','p0i5',[0,'Price'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p7','p6',[" & strEffect_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDPRICE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i0','p6i0',[0,'Standard','','',-1,-1,0,path + 'iFFMA/TradeDeal/Discount/StandardPriceList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALPRICE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i1','p6i0',[0,'Customer Special','','',-1,-1,0,path + 'iFFMA/TradeDeal/Discount/SpecialPriceList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPPRICE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i2','p6i0',[0,'Customer Group','','',-1,-1,0,path + 'iFFMA/TradeDeal/Discount/CustGrpPriceList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPPRICE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p7i3','p6i0',[0,'Price Group','','',-1,-1,0,path + 'iFFMA/TradeDeal/Discount/PriceGrpPriceList.aspx'" & StrLink_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDBONUS, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALBONUS, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPBONUS, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPBONUS, "'1'") Then

                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i1','p0i5',[0,'Bonus'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p8','p6',[" & strEffect_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.STANDARDBONUS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i0','p6i1',[0,'Standard','','',-1,-1,0,path + 'iFFMA/TradeDeal/Bonus/StandardBonusList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.SPECIALBONUS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i1','p6i1',[0,'Customer Special','','',-1,-1,0,path + 'iFFMA/TradeDeal/Bonus/SpecialBonusList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGRPBONUS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i2','p6i1',[0,'Customer Group','','',-1,-1,0,path + 'iFFMA/TradeDeal/Bonus/CustGrpBonusList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPBONUS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p8i3','p6i1',[0,'Price Group','','',-1,-1,0,path + 'iFFMA/TradeDeal/Bonus/PriceGrpBonusList.aspx'" & StrLink_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PACKAGE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p6i2','p0i5',[0,'Assortment (Package)','','',-1,-1,0,path + 'iFFMA/TradeDeal/PackageList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_TEAM_MAPPING, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i6','p0i0',[0,'FIELD FORCE ACTIVITY'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p9','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i0','p0i6',[0,'Field Activity','','',-1,-1,0,path + 'iFFMA/FieldForceActy/FieldActyCatList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i1','p0i6',[0,'Market Survey','','',-1,-1,0,path + 'iFFMA/FieldForceActy/MSSAdvList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i2','p0i6',[0,'Market Survey 2','','',-1,-1,0,path + 'iFFMA/FieldForceActy/MSSAdv2/MSSAdv2List.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.MSS_TEAM_MAPPING, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i3','p0i6',[0,'Market Survey Team Mapping','','',-1,-1,0,path + 'iFFMA/FieldForceActy/MSSTeamMap.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p9i4','p0i6',[0,'Detailing','','',-1,-1,0,path + 'iFFMA/Detailing/DetailingCatList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTGT, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i7','p0i0',[0,'FIELD FORCE'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p10','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i0','p0i7',[0,'Field Force Info','','',-1,-1,0,path + 'iFFMA/FieldForce/FieldForceList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTGT, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p10i1','p0i7',[0,'Sales Target','','',-1,-1,0,path + 'iFFMA/FieldForce/SalesTgtList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.REASON, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.ROUTE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGROUP, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.LEAD, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i8','p0i0',[0,'OTHERS'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p11','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.REASON, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i0','p0i8',[0,'Reason Code','','',-1,-1,0,path + 'iFFMA/Others/ReasonList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.ROUTE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i1','p0i8',[0,'Route','','',-1,-1,0,path + 'iFFMA/Others/RouteList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGROUP, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i2','p0i8',[0,'Price Group','','',-1,-1,0,path + 'iFFMA/Others/PriceGroupList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.PROMO_SO, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i4','p0i8',[0,'Promotion Sales Order','','',-1,-1,0,path + 'iFFMA/Others/PromotionList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.LEAD, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i4','p0i8',[0,'Lead','','',-1,-1,0,path + 'iFFMA/Others/LeadList.aspx'" & StrLink_2 & "]);"

                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If


                If GetAccessRight(ModuleID.FFMA, SubModuleType.INBOX, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.OUTBOX, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.UPLOADITINERARY, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADER, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADERV2, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i9','p0i0',[0,'UTILITY'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p12','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.INBOX, "'1'") Or _
                        GetAccessRight(ModuleID.FFMA, SubModuleType.OUTBOX, "'1'") Then

                        txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p12i0','p0i9',[0,'Messaging'" & strMenu_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p13','p12',[" & strEffect_2 & "]);"

                        If GetAccessRight(ModuleID.FFMA, SubModuleType.INBOX, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p13i0','p12i0',[0,'Inbox','','',-1,-1,0,path + 'iFFMA/Utility/Messaging/InboxMsgList.aspx'" & StrLink_2 & "]);"
                        If GetAccessRight(ModuleID.FFMA, SubModuleType.OUTBOX, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p13i1','p12i0',[0,'Outbox','','',-1,-1,0,path + 'iFFMA/Utility/Messaging/OutboxMsgList.aspx'" & StrLink_2 & "]);"
                        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                    End If

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.UPLOADITINERARY, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p12i1','p0i9',[0,'Upload Itinerary','','',-1,-1,0,path + 'iFFMA/Utility/Upload/Itinerary.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADER, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p12i2','p0i9',[0,'EasyLoader','','',-1,-1,0,path + 'iFFMA/Utility/EasyLoader/EasyLoader.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.EASYLOADERV2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p12i3','p0i9',[0,'EasyLoaderV2','','',-1,-1,0,path + 'iFFMA/Utility/EasyLoader/EasyLoaderV2.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEROUTE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPRDGRP, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEACTY, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEMSS, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPACKAGE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEITINERARYTRANSFER, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESGRP, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANFUNCLOC, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESOFF, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i10','p0i0',[0,'MAPPING'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p14','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i0','p0i10',[0,'Field Force Customer','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForceCustList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEROUTE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i1','p0i10',[0,'Field Force Route','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForceRouteList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPRDGRP, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i2','p0i10',[0,'Field Force Product Group','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForcePrdGrpList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEACTY, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i3','p0i10',[0,'Field Force Activity','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForceActyList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEMSS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i4','p0i10',[0,'Field Force Market Survey','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForceMSSList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEPACKAGE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i5','p0i10',[0,'Field Force Package','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForcePackageList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEITINERARYTRANSFER, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i6','p0i10',[0,'Field Force Itinerary Transfer','','',-1,-1,0,path + 'iFFMA/Mapping/FieldForceItineraryTransfer.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESGRP, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i7','p0i10',[0,'Technician Sales Group','','',-1,-1,0,path + 'iFFMA/Mapping/TechnicianSalesGrpList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANFUNCLOC, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i8','p0i10',[0,'Technician Functional Location','','',-1,-1,0,path + 'iFFMA/Mapping/TechnicianFuncLocList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.TECHNICIANSALESOFF, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p14i9','p0i10',[0,'Technician Sales Office','','',-1,-1,0,path + 'iFFMA/Mapping/TechnicianSalesOffList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKTRANSFERLIST, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.NEWEQPLIST, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.APPLYSTOCKLIST, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.MANUFACTURER, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.EQPHISTLIST, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i11','p0i0',[0,'SERVICES'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p15','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKTRANSFERLIST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p15i0','p0i11',[0,'Confirm Stock List','','',-1,-1,0,path + 'iFFMA/Services/StockTransferList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.NEWEQPLIST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p15i1','p0i11',[0,'New Equipment List','','',-1,-1,0,path + 'iFFMA/Services/NewEqpList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.APPLYSTOCKLIST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p15i2','p0i11',[0,'Apply Stock List','','',-1,-1,0,path + 'iFFMA/Services/ApplyStockList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.MANUFACTURER, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p15i3','p0i11',[0,'Manufacturer List','','',-1,-1,0,path + 'iFFMA/Services/ManufacturerList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.EQPHISTLIST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p15i4','p0i11',[0,'Equipment History List','','',-1,-1,0,path + 'iFFMA/Services/EqpHistList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKADJUST, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKHIST, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKSTATUS, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i12','p0i0',[0,'GCIM'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p16','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKADJUST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p16i0','p0i12',[0,'Stock Adjustment List','','',-1,-1,0,path + 'iFFMA/GCIM/StockAdjustList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKHIST, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p16i1','p0i12',[0,'Stock History List','','',-1,-1,0,path + 'iFFMA/GCIM/StockHistList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.STOCKSTATUS, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p16i2','p0i12',[0,'Stock Status List','','',-1,-1,0,path + 'iFFMA/GCIM/StockStatusList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BUPROFILE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BANKPROFILE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORPROFILE, "'1'") Or _
                    GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORCUSTOMERMAPPING, "'1'") Then

                    txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p0i8','p0i0',[0,'COLLECTION'" & strMenu_1 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p11','p1',[" & strEffect_1 & "]);"

                    If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BUPROFILE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i0','p0i8',[0,'Company Code - BU Profile','','',-1,-1,0,path + 'iFFMA/AR/CompBUList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.COMPANYCODE_BANKPROFILE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i1','p0i8',[0,'Company Code - Bank Profile','','',-1,-1,0,path + 'iFFMA/AR/CompBankList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORPROFILE, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i1','p0i8',[0,'Collector Profile','','',-1,-1,0,path + 'iFFMA/AR/CollectorProfileList.aspx'" & StrLink_2 & "]);"
                    If GetAccessRight(ModuleID.FFMA, SubModuleType.COLLECTORCUSTOMERMAPPING, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_aix('p11i1','p0i8',[0,'Collector - Customer Mapping','','',-1,-1,0,path + 'iFFMA/AR/CollectorCustMappingList.aspx'" & StrLink_2 & "]);"
                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                End If

                txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
                txtMenuItems.Text = txtMenuItems.Text & "}"
            End If

            'Admin
            If GetAccessRight(1) Then
                txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,0,0,'#000000']);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
                If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 92, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Connection','','',-1,-1,0,path + 'Admin/ConnectionString/ConnectionStringList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 122, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i2',[0,'SAP','','',-1,-1,0,path + 'iFFMA/SAP/SAPList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 225, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i3',[0,'SAP','','',-1,-1,0,path + 'iFFMA/SAP/SAPList_V2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 227, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i4',[0,'SAP','','',-1,-1,0,path + 'iFFMA/SAP/SAP_v3.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 242, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i5',[0,'SAP','','',-1,-1,0,path + 'iFFMA/SAP/SAPList_v4.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 226, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i6',[0,'Salesrep Group','','',-1,-1,0,path + 'Admin/SalesrepGrp/SalesrepGrpList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i7',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 240, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i8',[0,'Goods Return List','','',-1,-1,0,path + 'Admin/TRA/TRAList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(1, 91, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i9',[0,'User Mobile','','',-1,-1,0,path + 'Admin/UserMobile/UserMobileList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i10',[0,'Easy Loader V2 Accessright','','',-1,-1,0,path + 'Admin/ELAccessright/ELAccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(ModuleID.ADM, SubModuleType.BCP, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i11',[0,'BCP','','',-1,-1,0,path + 'Admin/BCP/BCPTemplateList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(ModuleID.ADM, SubModuleType.SFMS_CFG, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i12',[0,'SFMS Configuration','','',-1,-1,0,path + 'Admin/BCP/SFMSConfig.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
                If GetAccessRight(ModuleID.ADM, SubModuleType.MSS_CFG, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i13',[0,'MSS Configuration','','',-1,-1,0,path + 'Admin/BCP/MSSConfig.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"

                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"

                txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
                txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
                txtMenuItems.Text = txtMenuItems.Text & "}"
            End If
            txtMenuItems.Text = txtMenuItems.Text & "}"
            txtMenuItems.Text = txtMenuItems.Text & "</script>"





            'txtMenuItems.Text = ""
            'txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
            'txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
            ''FFMR
            'If GetAccessRight(4) Then
            '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
            '    If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'DRC','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'SALES','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
            '        If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Sales List','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
            '        GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
            '        GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'SFE','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p0',[1,4]);"
            '        If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i0',[0,'Call Analysis','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p1',[1,2,0,0,2,3,0,0]);"
            '            If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'By Month','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i1',[0,'By Day','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i2',[0,'Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '        End If
            '        If GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Coverage','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p1',[1,2,0,0,2,3,0,0]);"
            '            If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i0',[0,'Call Coverage 1','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Call Coverage 2','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '        End If
            '        If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i2',[0,'Call Productivity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i3',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i4',[0,'SFMS Activity Setup','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'KPI','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p0',[1,4,0,0,2,3,0,0]);"
            '        If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i0',[0,'Relative Performance','','',-1,-1,0,path + 'iFFMR/KPI/RelPerformance.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i1',[0,'Monthly Overview','','',-1,-1,0,path + 'iFFMR/KPI/MthOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'EXTRACTION','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p6','p0',[1,4,0,0,2,3,0,0]);"
            '        If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i0',[0,'Customer','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i1',[0,'DRC','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i2',[0,'MSS','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i3',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'CUSTOM','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'DATA FILES','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
            '    txtMenuItems.Text = txtMenuItems.Text & "}"
            'End If
            ''FFMS
            'If GetAccessRight(3) Then
            '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
            '    If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
            '    txtMenuItems.Text = txtMenuItems.Text & "}"
            'End If
            ''Admin
            'If GetAccessRight(1) Then
            '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
            '    If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
            '    txtMenuItems.Text = txtMenuItems.Text & "}"
            'End If
            'txtMenuItems.Text = txtMenuItems.Text & "}"
            'txtMenuItems.Text = txtMenuItems.Text & "</script>"














            'txtMenuItems.Text = ""
            'txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
            'txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
            ''FFMR
            'If GetAccessRight(4) Then
            '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
            '    If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'DRC','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'SALES','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
            '        If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Sales List','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
            '        GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
            '        GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'SFE','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p0',[1,4]);"
            '        If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i0',[0,'Call Analysis','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p0',[1,2,0,0,2,3,0,0]);"
            '            If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'By Month','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i1',[0,'By Day','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i2',[0,'Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '        End If
            '        If GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'Call Coverage','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p0',[1,2,0,0,2,3,0,0]);"
            '            If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Call Coverage 1','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i2',[0,'Call Coverage 2','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '        End If
            '        If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Productivity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i2',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i3',[0,'SFMS Activity Setup','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'KPI','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p0',[1,4,0,0,2,3,0,0]);"
            '        If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i0',[0,'Relative Performance','','',-1,-1,0,path + 'iFFMR/KPI/RelPerformance.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Monthly Overview','','',-1,-1,0,path + 'iFFMR/KPI/MthOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'EXTRACTION','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p0',[1,4,0,0,2,3,0,0]);"
            '        If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i0',[0,'Customer','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i1',[0,'DRC','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i2',[0,'MSS','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i3',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    End If
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'CUSTOM','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'DATA FILES','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
            '    txtMenuItems.Text = txtMenuItems.Text & "}"
            'End If
            ''FFMS
            'If GetAccessRight(3) Then
            '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
            '    If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
            '    txtMenuItems.Text = txtMenuItems.Text & "}"
            'End If
            ''Admin
            'If GetAccessRight(1) Then
            '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
            '    If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
            '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
            '    txtMenuItems.Text = txtMenuItems.Text & "}"
            'End If
            'txtMenuItems.Text = txtMenuItems.Text & "}"
            'txtMenuItems.Text = txtMenuItems.Text & "</script>"

        Catch ex As Exception

        End Try
    End Sub
    'Private Sub GetMenuItems()
    '    Try
    '        txtMenuItems.Text = ""
    '        txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
    '        txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
    '        'FFMR
    '        If GetAccessRight(4) Then
    '            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '            If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'DEARLER RECORD CARD','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'SALES','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '                If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Sales List','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '                GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '                GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'SALES FORCE EFFECTIVENESS','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p0',[1,4]);"
    '                If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i0',[0,'Call Analysis','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p1',[1,2,0,0,2,3,0,0]);"
    '                    If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'By Month','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i1',[0,'By Day','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i2',[0,'Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '                End If
    '                If GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Coverage','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p1',[1,2,0,0,2,3,0,0]);"
    '                    If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i0',[0,'Call Coverage 1','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Call Coverage 2','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '                End If
    '                If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i2',[0,'Call Productivity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i3',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i4',[0,'SFMS Activity','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'KEY PERFORMANCE INDEX','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p0',[1,4,0,0,2,3,0,0]);"
    '                If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i0',[0,'Relative Performance','','',-1,-1,0,path + 'iFFMR/KPI/RelPerformance.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i1',[0,'Monthly Overview','','',-1,-1,0,path + 'iFFMR/KPI/MthOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 40, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i2',[0,'Activity & Sales','','',-1,-1,0,path + 'iFFMR/KPI/ActSalesBySalesrep.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'COMM. REPORT','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'EXTRACTION','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p6','p0',[1,4,0,0,2,3,0,0]);"
    '                If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i0',[0,'Customer','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i1',[0,'DRC','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i2',[0,'MSS','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i3',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'CUSTOM','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'DATA FILES','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '            txtMenuItems.Text = txtMenuItems.Text & "}"
    '        End If
    '        'FFMS
    '        If GetAccessRight(3) Then
    '            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '            If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            'If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 43, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'MESSAGING','','',-1,-1,0,path + 'iFFMS/Messaging/MessagingList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 45, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'PERFORMANCE','','',-1,-1,0,path + 'iFFMS/Performance/PerformanceOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 44, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'REPORTING','','',-1,-1,0,path + 'iFFMS/Report/ReportHeader.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '            txtMenuItems.Text = txtMenuItems.Text & "}"
    '        End If
    '        'Admin
    '        If GetAccessRight(1) Then
    '            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '            If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '            txtMenuItems.Text = txtMenuItems.Text & "}"
    '        End If
    '        txtMenuItems.Text = txtMenuItems.Text & "}"
    '        txtMenuItems.Text = txtMenuItems.Text & "</script>"





    '        'txtMenuItems.Text = ""
    '        'txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
    '        'txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
    '        ''FFMR
    '        'If GetAccessRight(4) Then
    '        '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '        '    If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'DRC','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'SALES','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '        '        If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Sales List','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '        '        GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '        '        GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'SFE','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p0',[1,4]);"
    '        '        If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i0',[0,'Call Analysis','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p1',[1,2,0,0,2,3,0,0]);"
    '        '            If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'By Month','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i1',[0,'By Day','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i2',[0,'Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '        End If
    '        '        If GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Coverage','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p1',[1,2,0,0,2,3,0,0]);"
    '        '            If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i0',[0,'Call Coverage 1','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Call Coverage 2','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '        End If
    '        '        If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i2',[0,'Call Productivity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i3',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i4',[0,'SFMS Activity Setup','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'KPI','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p0',[1,4,0,0,2,3,0,0]);"
    '        '        If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i0',[0,'Relative Performance','','',-1,-1,0,path + 'iFFMR/KPI/RelPerformance.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i1',[0,'Monthly Overview','','',-1,-1,0,path + 'iFFMR/KPI/MthOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'EXTRACTION','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p6','p0',[1,4,0,0,2,3,0,0]);"
    '        '        If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i0',[0,'Customer','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i1',[0,'DRC','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i2',[0,'MSS','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i3',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p6i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'CUSTOM','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'DATA FILES','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'End If
    '        ''FFMS
    '        'If GetAccessRight(3) Then
    '        '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '        '    If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'End If
    '        ''Admin
    '        'If GetAccessRight(1) Then
    '        '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '        '    If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'End If
    '        'txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'txtMenuItems.Text = txtMenuItems.Text & "</script>"














    '        'txtMenuItems.Text = ""
    '        'txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
    '        'txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
    '        ''FFMR
    '        'If GetAccessRight(4) Then
    '        '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '        '    If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'DRC','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'SALES','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '        '        If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Sales List','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '        '        GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '        '        GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'SFE','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p0',[1,4]);"
    '        '        If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i0',[0,'Call Analysis','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p0',[1,2,0,0,2,3,0,0]);"
    '        '            If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'By Month','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i1',[0,'By Day','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i2',[0,'Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '        End If
    '        '        If GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'Call Coverage','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p0',[1,2,0,0,2,3,0,0]);"
    '        '            If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Call Coverage 1','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i2',[0,'Call Coverage 2','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '        End If
    '        '        If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Productivity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i2',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i3',[0,'SFMS Activity Setup','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'KPI','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p0',[1,4,0,0,2,3,0,0]);"
    '        '        If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i0',[0,'Relative Performance','','',-1,-1,0,path + 'iFFMR/KPI/RelPerformance.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Monthly Overview','','',-1,-1,0,path + 'iFFMR/KPI/MthOverview.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'EXTRACTION','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p0',[1,4,0,0,2,3,0,0]);"
    '        '        If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i0',[0,'Customer','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i1',[0,'DRC','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i2',[0,'MSS','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i3',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '        txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    End If
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'CUSTOM','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'DATA FILES','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'End If
    '        ''FFMS
    '        'If GetAccessRight(3) Then
    '        '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '        '    If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'End If
    '        ''Admin
    '        'If GetAccessRight(1) Then
    '        '    txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '        '    If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '        '    txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'End If
    '        'txtMenuItems.Text = txtMenuItems.Text & "}"
    '        'txtMenuItems.Text = txtMenuItems.Text & "</script>"

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Sub GetMenuItems()
    '    Try

    '        txtMenuItems.Text = ""
    '        txtMenuItems.Text = txtMenuItems.Text & "<script type='text/javascript' language='JavaScript1.2'>"
    '        txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')!=''){"
    '        'FFMR
    '        If GetAccessRight(4) Then
    '            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMR'){"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '            If GetAccessRight(4, 5, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'DRC','','',-1,-1,0,path + 'iFFMR/DRC/DRCList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(4, 4, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PDA COMM.','','',-1,-1,0,path + 'iFFMR/Comm/CommList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(4, 7, "'1','8'") Or GetAccessRight(4, 8, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'SALES','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '                If GetAccessRight(4, 7, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Sales List','','',-1,-1,0,path + 'iFFMR/Sales/SalesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 8, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'Sales Enquiry','','',-1,-1,0,path + 'iFFMR/Sales/SalesEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '                GetAccessRight(4, 12, "'1','8'") Or GetAccessRight(4, 13, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Or _
    '                GetAccessRight(4, 35, "'1','8'") Or GetAccessRight(4, 36, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i3',[0,'SFE','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p2','p0',[1,4]);"
    '                If GetAccessRight(4, 10, "'1','8'") Or GetAccessRight(4, 9, "'1','8'") Or GetAccessRight(4, 25, "'1','8'") Then
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i0',[0,'Call Analysis','','',-1,-1,0,'','','','','','',0,0,0,path +'include/menu/sothink/arrow_r.gif',path + 'include/menu/sothink/arrow_r.gif',7,7,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p3','p0',[1,2,0,0,2,3,0,0]);"
    '                    If GetAccessRight(4, 10, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i0',[0,'By Month','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    If GetAccessRight(4, 9, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i1',[0,'By Day','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    If GetAccessRight(4, 25, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p3i2',[0,'Enquiry','','',-1,-1,0,path + 'iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                    txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '                End If
    '                If GetAccessRight(4, 35, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Coverage 1','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage1.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 36, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i1',[0,'Call Coverage 2','','',-1,-1,0,path + 'iFFMR/SFE/CallCoverage/CallCoverage2.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 12, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i2',[0,'Call Productivity','','',-1,-1,0,path + 'iFFMR/SFE/CallProd/CallProdList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 13, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i3',[0,'Preplan Call','','',-1,-1,0,path + 'iFFMR/SFE/Preplan/PreplanList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 14, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p2i4',[0,'SFMS Activity Setup','','',-1,-1,0,path + 'iFFMR/SFE/SFMSActy/SFMSActyList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            If GetAccessRight(4, 37, "'1','8'") Or GetAccessRight(4, 38, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i4',[0,'KPI','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p4','p0',[1,4,0,0,2,3,0,0]);"
    '                If GetAccessRight(4, 37, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i0',[0,'Relative Performance','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 38, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p4i1',[0,'Monthly Overview','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            If GetAccessRight(4, 31, "'1','8'") Or GetAccessRight(4, 32, "'1','8'") Or GetAccessRight(4, 33, "'1','8'") Or GetAccessRight(4, 34, "'1','8'") Or GetAccessRight(4, 6, "'1','8'") Then
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i5',[0,'EXTRACTION','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p5','p0',[1,4,0,0,2,3,0,0]);"
    '                If GetAccessRight(4, 31, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i0',[0,'Customer','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 32, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i1',[0,'DRC','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 33, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i2',[0,'MSS','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 34, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i3',[0,'Sales Order','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                If GetAccessRight(4, 6, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p5i4',[0,'SFMS','','',-1,-1,0,path + 'iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '                txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            End If
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i6',[0,'CUSTOM','','',-1,-1,0,path + 'blank.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(4, 30, "'1','8'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i7',[0,'DATA FILES','','',-1,-1,0,path + 'iFFMR/DataFiles/DataFilesList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '            txtMenuItems.Text = txtMenuItems.Text & "}"
    '        End If
    '        'FFMS
    '        If GetAccessRight(3) Then
    '            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='FFMS'){"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,0,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '            If GetAccessRight(3, 15, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'CUSTOMER','','',-1,-1,0,path + 'iFFMS/Customer/CustomerListing.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 24, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i1',[0,'PRODUCT','','',-1,-1,0,path + 'iFFMS/Product/ProductList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(3, 19, "'1'") Or GetAccessRight(3, 20, "'1'") Or GetAccessRight(3, 21, "'1'") Or GetAccessRight(3, 22, 1) Or GetAccessRight(3, 23, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i2',[0,'TRANSACTION','','',-1,-1,0,path + 'iFFMS/Transaction/TransactionSelection.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '            txtMenuItems.Text = txtMenuItems.Text & "}"
    '        End If
    '        'Admin
    '        If GetAccessRight(1) Then
    '            txtMenuItems.Text = txtMenuItems.Text & "if (get_cookie('mainmenu')=='ADMIN'){"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bm(['menu1d89',700,'',path +'include/menu/sothink/blank.gif',0,'','',0,0,250,0,1000,1,0,0,'','',0,0,1,2,'default','hand',''],this);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bp('p0',[0,4,0,0,2,3,0,7,100,'',-2,'',-2,50,0,0,'#999999','#FFFFF7','',3,1,1,'#000000']);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p0i0',[0,'ADMIN','','',-1,-1,0,'','','','','','',0,0,0,'arrow_r.gif','arrow_r.gif',7,7,0,1,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_bpx('p1','p0',[1,4,0,0,2,3,0,0]);"
    '            If GetAccessRight(1, 2, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i0',[0,'Access Right','','',-1,-1,0,path + 'Admin/AccessRight/AccessRightList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            If GetAccessRight(1, 1, "'1'") Then txtMenuItems.Text = txtMenuItems.Text & "stm_ai('p1i1',[0,'User','','',-1,-1,0,path + 'Admin/User/UserList.aspx','fraMain','','','','',0,0,0,'','',0,0,0,0,1,'#FFFFF7',0,'#EDF2FF',0,'newline.gif','',3,3,1,1,'#B1BEF2','#ffffff','#848484','#448599','bold 8pt Arial,Verdana','bold 8pt Arial,Verdana',0,0]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_ep();"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_cf([0,0,0,'fraMain','fraTop',1]);"
    '            txtMenuItems.Text = txtMenuItems.Text & "stm_em();"
    '            txtMenuItems.Text = txtMenuItems.Text & "}"
    '        End If
    '        txtMenuItems.Text = txtMenuItems.Text & "}"
    '        txtMenuItems.Text = txtMenuItems.Text & "</script>"

    '    Catch ex As Exception

    '    End Try
    'End Sub
#End Region

    Private Sub lnkFFMR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFFMR.Click
        Dim strScript As String
        lnkFFMR.CssClass = "menuL1"
        lnkFFMS.CssClass = "menuL1P"
        lnkFFMA.CssClass = "menuL1P"
        lnkAdmin.CssClass = "menuL1P"
        'mnu1FFMRL.Style.Value = "background-attachment : fixed;background-image : url(../../../images/mnuActive1L.gif);"
        mnu1FFMRL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuActive1L.gif);"
        mnu1FFMSL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMAL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1ADMINL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"

        strScript = "<script>"
        strScript += "self.parent.fraMain.location='../../../index.aspx';"
        strScript += "</" + "script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)

        'If Request.Cookies("menu") Is Nothing Then
        Dim objcookie As New HttpCookie("menu")
        'objcookie.Values.Add("mainmenu", "FFMR")
        'Response.AppendCookie(objcookie)
        'End If
        ClearCookie()
        objcookie.Values.Add("mainmenu", "FFMR")
        Response.AppendCookie(objcookie)

        'GetMenuItems(2)
    End Sub

    Private Sub lnkFFMS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFFMS.Click
        Dim strScript As String
        Dim objcookie As New HttpCookie("menu")
        lnkFFMS.CssClass = "menuL1"
        lnkFFMR.CssClass = "menuL1P"
        lnkFFMA.CssClass = "menuL1P"
        lnkAdmin.CssClass = "menuL1P"
        mnu1FFMSL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuActive1L.gif);"
        mnu1FFMRL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMAL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1ADMINL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"

        strScript = "<script>"
        strScript += "self.parent.fraMain.location='../../../index.aspx';"
        strScript += "</" + "script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)

        ClearCookie()
        objcookie.Values.Add("mainmenu", "FFMS")
        Response.AppendCookie(objcookie)

        'GetMenuItems(3)
    End Sub

    Private Sub lnkAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdmin.Click
        Dim strScript As String
        Dim objcookie As New HttpCookie("menu")
        lnkAdmin.CssClass = "menuL1"
        lnkFFMR.CssClass = "menuL1P"
        lnkFFMS.CssClass = "menuL1P"
        lnkFFMA.CssClass = "menuL1P"

        mnu1ADMINL.Style.Value = "background-attachment :scroll;background-image : url(../../../images/mnuActive1L.gif);"
        mnu1FFMRL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMSL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMAL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"

        strScript = "<script>"
        strScript += "self.parent.fraMain.location='../../../index.aspx';"
        strScript += "</" + "script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)

        ClearCookie()
        objcookie.Values.Add("mainmenu", "ADMIN")
        Response.AppendCookie(objcookie)

        'GetMenuItems(4)
    End Sub

    Private Sub lnkLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLogout1.Click
        Dim strScript As String
        Dim objUser As adm_User.clsUser
        objUser = New adm_User.clsUser

        Try
            With objUser
                .clsProperties.user_id = Session("UserID")
                .AuditLogout(Session("AuditID"))
            End With
        Catch ex As Exception
            objUser = Nothing
        End Try

        Session.Clear()
        Session.Abandon()
        strScript = "<script>"
        'strScript += "if (!self.parent.window.opener.closed){"
        strScript += "self.parent.parent.location='../../../logout.aspx';"
        'strScript += "}"
        strScript += "</" + "script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Logout", strScript)
    End Sub

    Protected Sub lnkProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkProfile.Click
        'Dim strScript As String

        'strScript = "<script>"
        'strScript += "self.parent.fraMain.location='Admin/User/UserDtl.aspx?type=1&user_code=" & Trim(Session("UserCode")) & "';"
        'strScript += "</" + "script>"
        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)

        Dim strScript As String
        Dim objcookie As New HttpCookie("menu")

        '***HL:20070621***************
        'lnkAdmin.CssClass = "menuL1" 
        'lnkFFMR.CssClass = "menuL1P"
        lnkAdmin.CssClass = "menuL1P"
        lnkFFMR.CssClass = "menuL1"
        '*****************************
        lnkFFMS.CssClass = "menuL1P"
        lnkFFMA.CssClass = "menuL1P"

        '***HL:20070621********************************************
        'mnu1ADMINL.Style.Value = "background-attachment :fixed;background-image : url(../../../images/mnuActive1L.gif);"
        'mnu1FFMRL.Style.Value = "background-attachment : fixed;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1ADMINL.Style.Value = "background-attachment : fixed;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMRL.Style.Value = "background-attachment :fixed;background-image : url(../../../images/mnuActive1L.gif);"
        '**********************************************************
        mnu1FFMSL.Style.Value = "background-attachment : fixed;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMAL.Style.Value = "background-attachment : fixed;background-image : url(../../../images/mnuPending1L.gif);"

        strScript = "<script>"
        '***HL:20070621- redirect to user profile page instead of user edit page**********
        'strScript += "self.parent.fraMain.location='../../../Admin/User/UserDtl.aspx?type=1&user_code=" & Trim(Session("UserCode")) & "';"
        strScript += "self.parent.fraMain.location='../../../Admin/User/UserProfile.aspx?type=1&user_code=" & Trim(Session("UserCode")) & "';"
        '*********************************************************************************
        strScript += "</" + "script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)

        ClearCookie()

        '***HL:20070621- back to FFMR instead of ADMIN*************
        'objcookie.Values.Add("mainmenu", "ADMIN")
        objcookie.Values.Add("mainmenu", "FFMR")
        '**********************************************************
        Response.AppendCookie(objcookie)

        'GetMenuItems(4)
    End Sub

    Protected Sub lnkFFMA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMA.Click
        Dim strScript As String
        Dim objcookie As New HttpCookie("menu")
        lnkFFMS.CssClass = "menuL1P"
        lnkFFMR.CssClass = "menuL1P"
        lnkFFMA.CssClass = "menuL1"
        lnkAdmin.CssClass = "menuL1P"
        mnu1FFMAL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuActive1L.gif);"
        mnu1FFMRL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1FFMSL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"
        mnu1ADMINL.Style.Value = "background-attachment : scroll;background-image : url(../../../images/mnuPending1L.gif);"

        strScript = "<script>"
        strScript += "self.parent.fraMain.location='../../../index.aspx';"
        strScript += "</" + "script>"
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)

        ClearCookie()
        objcookie.Values.Add("mainmenu", "FFMA")
        Response.AppendCookie(objcookie)

        'GetMenuItems(3)
    End Sub
End Class
