'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	16/04/2007
'	Purpose	    :	Class CommSvr
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.ComponentModel
Imports System.Configuration


Public Class clsCommSvr

    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Private strConn As String = Web.HttpContext.Current.Session("echoplus_conn") '"server=" & ConfigurationSettings.AppSettings("server") & ";database=" & ConfigurationSettings.AppSettings("database") & ";User ID=" & ConfigurationSettings.AppSettings("userid") & ";password=" & ConfigurationSettings.AppSettings("password")


    Public clsProperties As New clsProperties.clsComm

    '------------------------------------------------------------------
    ' Procedure 	: 	Sub Create
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Create()

        Dim objDB As cor_DB.clsDB

        Try
            'Insert into User
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .TableName = "TBL_ADM_COMM_SVR_SETTING"
                .addItem("comm_svr_setting_code", clsProperties.strCommCode, cor_DB.clsDB.DataType.DBString)
                .addItem("comm_svr_setting_name", clsProperties.strCommName, cor_DB.clsDB.DataType.DBString)
                .addItem("server_ip", clsProperties.strServerIP, cor_DB.clsDB.DataType.DBString)
                .addItem("server_name", clsProperties.strServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("db_ip", clsProperties.strDBIP, cor_DB.clsDB.DataType.DBString)
                .addItem("db_port", clsProperties.intDBPort, cor_DB.clsDB.DataType.DBInt)
                .addItem("db_name", clsProperties.strDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("link_server_name", clsProperties.strLinkServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("db_user_id", clsProperties.strDBUserID, cor_DB.clsDB.DataType.DBString)
                .addItem("db_user_pwd", clsProperties.strDBPassword, cor_DB.clsDB.DataType.DBString)
                .addItem("url", clsProperties.strURL, cor_DB.clsDB.DataType.DBString)
                .addItem("proxy", clsProperties.strProxy, cor_DB.clsDB.DataType.DBString)
                .addItem("created_date", clsProperties.strCreatedDate, cor_DB.clsDB.DataType.DBString)
                .addItem("creator_user_id", clsProperties.strCreatorUserID, cor_DB.clsDB.DataType.DBInt)
                .addItem("changed_date", clsProperties.strCreatedDate, cor_DB.clsDB.DataType.DBString)
                .addItem("changed_user_id", clsProperties.strCreatorUserID, cor_DB.clsDB.DataType.DBInt)
                .addItem("sync_operation", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
                .addItem("active_flag", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
                .Insert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvr.Create : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Update()

        Dim objDB As cor_DB.clsDB

        Try
            'Insert into User
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .TableName = "TBL_ADM_COMM_SVR_SETTING"
                .addItem("comm_svr_setting_code", clsProperties.strCommCode, cor_DB.clsDB.DataType.DBString)
                .addItem("comm_svr_setting_name", clsProperties.strCommName, cor_DB.clsDB.DataType.DBString)
                .addItem("server_ip", clsProperties.strServerIP, cor_DB.clsDB.DataType.DBString)
                .addItem("server_name", clsProperties.strServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("db_ip", clsProperties.strDBIP, cor_DB.clsDB.DataType.DBString)
                .addItem("db_port", clsProperties.intDBPort, cor_DB.clsDB.DataType.DBInt)
                .addItem("db_name", clsProperties.strDBName, cor_DB.clsDB.DataType.DBString)
                .addItem("link_server_name", clsProperties.strLinkServerName, cor_DB.clsDB.DataType.DBString)
                .addItem("db_user_id", clsProperties.strDBUserID, cor_DB.clsDB.DataType.DBString)
                If clsProperties.bChangePwd Then
                    .addItem("db_user_pwd", clsProperties.strDBPassword, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("url", clsProperties.strURL, cor_DB.clsDB.DataType.DBString)
                .addItem("proxy", clsProperties.strProxy, cor_DB.clsDB.DataType.DBString)
                .addItem("changed_date", clsProperties.strChangedDate, cor_DB.clsDB.DataType.DBString)
                .addItem("changed_user_id", clsProperties.strChangedUserID, cor_DB.clsDB.DataType.DBInt)
                .addItem("sync_operation", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
                .addItem("active_flag", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
                .addFilter("comm_svr_setting_id", clsProperties.strCommID, "=", cor_DB.clsDB.DataType.DBInt)
                .Update()

            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvr.Update : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Suspend
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Suspend()

        Dim objDB As cor_DB.clsDB

        Try
            'Insert into User
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .TableName = "TBL_ADM_COMM_SVR_SETTING"
                .addItem("sync_operation", clsProperties.strSyncOperation, cor_DB.clsDB.DataType.DBString)
                .addItem("active_flag", clsProperties.strActiveFlag, cor_DB.clsDB.DataType.DBString)
                .addFilter("comm_svr_setting_id", clsProperties.strCommID, "=", cor_DB.clsDB.DataType.DBInt)
                .Update()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_Comm_Svr_Setting.clsCommSvr.Suspend : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class

