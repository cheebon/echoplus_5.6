﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EasyLoaderV2.aspx.vb" Inherits="iFFMA_Utility_EasyLoader_EasyLoaderV2" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Easy Loader</title>
    <link href="~/include/DKSH.css" rel="stylesheet" /> 
    <script src="../../../include/jquery-1.4.2.min.js" type="text/javascript"></script> 
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmEasyLoader" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
   <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
    <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <customToolkit:wuc_lblInfo ID="Wuc_lblInfo1" runat="server" Title="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                <fieldset class="" style="width: 98%;">
                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader1" runat="server" />
                    <br style="font-size: 3px;" />
                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                                width="100%" border="0" style="height: 30px">
                                <td align="left">
                                    <asp:ImageButton ID="imgExport" runat="server" ImageUrl="~/images/ico_update.gif"
                                        AlternateText="Export" />
                                </td>
                            </table>
                            <table class="Bckgroundreport" cellpadding="5" cellspacing="0" width="98%" border="0"
                                style="text-align: left; padding-left: 15px; padding-bottom: 10px;">
                                <tr>
                                    <td style="width: 10%;">
                                    </td>
                                    <td style="width: 20%">
                                    </td>
                                    <td style="width: 10%;">
                                    </td>
                                    <td style="width: 60%">
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlValidate" runat="server">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblCategory" runat="server" Text="Category" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCat" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAction" runat="server" Text="Action" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAction" runat="server" CssClass="cls_dropdownlist">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvddlAction" runat="server" ErrorMessage="Please select action type"
                                                ControlToValidate="ddlAction" ValidationGroup="Fileupload" Display="Dynamic"
                                                CssClass="cls_label_validation" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUploadTypeDesc" runat="server" Text="Upload Type :" CssClass="cls_label_header"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlUploadType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true" />
                                            <%--<asp:button ID="btnHelpFile" runat="server" Text="Download Help" CssClass="cls_linkbutton" ValidationGroup="Fileupload"  ></asp:button>--%>
                                            <asp:RequiredFieldValidator ID="rfvUploadType" runat="server" ErrorMessage="Please select upload type"
                                                ControlToValidate="ddlUploadType" ValidationGroup="Fileupload" Display="Dynamic"
                                                CssClass="cls_label_validation" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btndltemplate" runat="server" Text="Download Template" CssClass="cls_linkbutton"
                                                CausesValidation="false"></asp:Button>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblFileLocationDesc" runat="server" Text="File Location :" CssClass="cls_label_header" ></asp:Label> 
                                        </td>
                                        <td colspan="3">
                                            <asp:FileUpload ID="fuLocation" runat="server" CssClass="cls_textbox" Width="500px"
                                                Height="20px" />
                                            <%--<asp:RequiredFieldValidator ID="rfvfuprd" runat="server" ErrorMessage="Please browse a File" ControlToValidate="fuLocation" ValidationGroup="Fileupload" Display="Dynamic" CssClass="cls_validator" />--%>
                                            <br />
                                            <asp:Label ID="lblMsg" runat="server" CssClass="cls_label_validation" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <br />
                                            <asp:Button ID="btnValidate" runat="server" Text="Validate" CssClass="cls_button"
                                                ValidationGroup="Fileupload" />
                                            <asp:HiddenField ID="hfCSVFileNameCheckingFlag" runat="server" />
                                            <asp:HiddenField ID="hfCSVFilename" runat="server" />
                                             <asp:HiddenField ID="hfDLFilename" runat="server" />
                                            <asp:HiddenField ID="hfdelimiter" runat="server" />
                                            <asp:HiddenField ID="hfrowbreak" runat="server" />
                                            <asp:HiddenField ID="hftmptable" runat="server" />
                                            <asp:HiddenField ID="hfHelpFileName" runat="server" />
                                        </td>
                                    </tr>
                                    <asp:Panel ID="pnlDesc" runat="server" Visible="false">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTipsInstructionsDesc" runat="server" Text="Tips & Instructions:"
                                                    CssClass="cls_label_header" Style="text-decoration: underline;"></asp:Label>
                                                <br />
                                                <asp:Label ID="lblDesc" runat="server" CssClass="cls_label_header" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lblErr1DEsc" runat="server" Text="Please upload text file with fields in the following order 
                                                sequence and separated by tab:" CssClass="cls_label_header"></asp:Label>
                                                <asp:Label ID="lblColumns" runat="server" CssClass="cls_label" />
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSystemError" runat="server" Visible="false">
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Label ID="lblErr2Desc" runat="server" Text="There is some system error for the selected upload type, 
                                                please contact administrator." CssClass="cls_label_validation"></asp:Label>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpload" runat="server" Visible="false">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="lblUploadInfo" runat="server" CssClass="cls_label_header" />
                                        </td>
                                    </tr>
                                    <asp:Panel ID="pnlFileError" runat="server" Visible="false">
                                        <tr>
                                            <td colspan="4">
                                                <asp:Label ID="lblErr3DEsc" runat="server" Text="" CssClass="cls_label_validation"></asp:Label>
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <tr>
                                        <td colspan="2">
                                            <br />
                                            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="cls_button" />
                                            <asp:Button ID="btnBack" runat="server" Text="Back to Validation Page" CssClass="cls_button" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div style="width: 100%;">
                                                <asp:Label ID="lblErrMsgValidate" runat="server" CssClass="cls_label_validation" /></div>
                                        </td>
                                    </tr>
                                    <tr style="padding-left: 0px;">
                                        <td width="100%" colspan="2" align="center">
                                            <div style="width: 100%; padding: 2px; text-align:left;background-color: White;"> 
                                                <asp:Label ID="lblTtlRecords" runat="server" CssClass="cls_label_header" Visible="false"  ></asp:Label> 
                                            </div>
                                            <div style="width: 100%; padding: 2px;">
                                                <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="false" ShowRecordCountStatus="false" />
                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                    Width="98%" FreezeHeader="True" GridHeight="310" AddEmptyHeaders="0" CellPadding="2"
                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                    ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                                                    <Columns>
                                                    </Columns>
                                                </ccGV:clsGridView>
                                            </div>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnValidate" />
                            <asp:PostBackTrigger ControlID="btndltemplate" />
                             <asp:PostBackTrigger ControlID="imgExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
