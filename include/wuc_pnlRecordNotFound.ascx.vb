
Partial Class include_wuc_pnlRecordNotFound
    Inherits System.Web.UI.UserControl
    Public Property ShowPanel() As Boolean
        Get
            Return Panel_RecordNotFound.Visible
        End Get
        Set(ByVal blnVisible As Boolean)
            Panel_RecordNotFound.Visible = blnVisible
        End Set
    End Property
End Class
