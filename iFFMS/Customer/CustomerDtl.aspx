<%@ Page Language="vb" AutoEventWireup="false" Inherits="FFMS_WebOrder.FFMS_WebOrder_CustomerDtl"
    CodeFile="CustomerDtl.aspx.vb"  %>

<%@ Register TagPrefix="uc2" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wucctrlPanel" Src="~/include/wuc_ctrlPanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wucHeader" Src="~/include/wuc_lblHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>FFMS - Web Order - Information</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>

<script language="javascript" type="text/javascript">
<!--

function applnname_onblur() {

}

// -->
</script>

<body class="cls_bgcolor">
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300">
        </asp:ScriptManager>
        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td colspan="5">
                   <uc1:wucctrlPanel  id="Wucctrlpanel" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                                  </td>
            </tr>
            <tr>
                <td colspan="5">
                    <uc1:wucHeader ID="WucHeader" runat="server"></uc1:wucHeader>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                </td>
            </tr>
            <tr>
                <td width="100%" bgcolor="#ccccff" colspan="5" height="15">
                    <uc2:TabStrip ID="custtab" runat="server" Width="100%" AutoPostBack="True" TabDefaultStyle="color:#aaaaaa;background-color:#EEEEEE;
                    border-color:#AAAAAA;border-width:1px;border-style:Solid;
                       font-weight:bold;font-family:Verdana;font-size:11px;height:21;
                       width:70;text-align:center;" TabHoverStyle="color:blue" TabSelectedStyle="color:#000000;background-color:#FFFFFF;
                       border-bottom:none" SepDefaultStyle="background-color:#FFFFFF;border-color:#AAAAAA;
                       border-width:1px;border-style:solid;border-top:none;border-left:none;
                       border-right:none">
                    </uc2:TabStrip>
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="5" height="600">
                    <iframe id="applnname" style="width: 100%; height: 100%" marginwidth="0" src="<%=sIFrameSrc%>"
                        frameborder="no" scrolling="no" height="100%" language="javascript" onblur="return applnname_onblur()">
                    </iframe>
                </td>
            </tr>
        </table>
<%--
                </td>
            </tr>
        </table>      --%>
    

    </form>
</body>
</html>
