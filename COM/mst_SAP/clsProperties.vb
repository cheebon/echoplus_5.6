'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	01/10/2007
'	Purpose	    :	Class User for Properties 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
'Imports System.ComponentModel
Imports system.Web.UI.WebControls

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
'<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    'Inherits ServicedComponent

    Public Class clsSAPQuery
        Public doc_type As String
        Public search_type As String
        Public team_code As String
        Public search_value As String
        Public search_value_1 As String
        Public txn_no As String
        Public user_code As String

        Public error_reason As String
        Public last_update As String
        Public txn_date As String
        Public txn_status As String
        Public sales_office As String
        Public salesrep_code As String
        Public status As String
        Public saving_no As String
        Public note As String
        Public last_update_start As String
        Public txn_date_start As String
        Public last_update_end As String
        Public txn_date_end As String
    End Class

    Public Class clsSAP
        Public user_id As String
        Public user_name As String
        Public doc_type As String
        Public txn_no As String
        Public ctn_no As String

        Property po_no As String

        Property billing_no As String

    End Class
    Public Class clsRetDtl
        Public user_id As String
        Public user_name As String
        Public txn_no As String
        Public prd_code As String
        Public line_no As String
        Public batch_no As String
        Public unit_price As String
        Public reason_code As String
        Public expired_date As String
        Public doc_type As String

    End Class

    Public Class clsRetHdr
        Public user_id As String
        Public user_name As String
        Public txn_no As String
        Public coll_code As String
        Public ref_no As String
        Public ctn_no As String
    End Class
End Class
