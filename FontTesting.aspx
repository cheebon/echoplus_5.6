<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FontTesting.aspx.vb" Inherits="FontTesting" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Font Testing</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <span style="font-family:Verdana;">The quick brown fox jumps over the lazy dog. - Verdana</span><br />
    <span style="font-family:'Frutiger LT 45 Light'">The quick brown fox jumps over the lazy dog. - Frutiger</span><br />
    <span>The quick brown fox jumps over the lazy dog. - Browser Default</span>
    </div>
    </form>
</body>
</html>