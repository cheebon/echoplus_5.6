'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	30/10/2006
'	Purpose	    :	Class to build datatables for retrieving customer information.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustInfo
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCustInfo"
        End Get
    End Property

    Function getGeneralInfoDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERGENERALINFO"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getGeneralInfoDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getContactGeneralInfoDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strType As String, Optional ByVal strContactCode As String = Nothing) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERCONTACTDETAIL"
                '.addItem("contactname", strContactName, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("type", strType, 2)
                .addItem("contactcode", strContactCode, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactGeneralInfoDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getDeliveryNameDT(ByVal strCustCode As String, ByVal strType As String, Optional ByVal strDelCode As String = Nothing) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERDELIVERYNAME"
                .addItem("custcode", strCustCode, 2)
                .addItem("type", strType, 2)
                .addItem("deliverycode", strDelCode, 2)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDeliveryDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getDeliveryDetailDT(ByVal strCustCode As String, ByVal strDeliveryCode As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERDELIVERYDETAIL"
                .addItem("custcode", strCustCode, 2)
                .addItem("deliverycode", strDeliveryCode, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDeliveryDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getOpenItemDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMEROPENITEM"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getOpenItemDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getOpenItemDetailDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strTxnNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMEROPENITEMDETAIL"
                .addItem("custcode", strCustCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("txnno", strTxnNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getOpenItemDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getARAgingDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERARAGING"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getARAgingDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getARAgingHeaderDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strAgingID As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERARAGINGHDR"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("agingid", strAgingID, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getARAgingHeaderDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function


    Function getMthSalesHistDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERMTHSALESHIST"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getMthSalesHistDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getMthSalesHistDetailDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strCycleID As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERMTHSALESHISTDET"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("cycleid", strCycleID, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getMthSalesHistDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getSFMSHistDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERSFMSHIST"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSFMSHistDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getSFMSHistDetailDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strTxnNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERSFMSHISTDET"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("txnno", strTxnNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSFMSHistDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getInvoiceHistDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERINVHIST"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getInvoiceHistDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getInvoiceHistDetailDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strTxnNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERINVHISTDET"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("txnno", strTxnNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getInvoiceHistDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getDealerRecordDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERDEALERREC"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDealerRecordDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getOrderStatusDT(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERORDERSTATUS"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getOrderStatusDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getCustDiscDT(ByVal strCustCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERDISC"
                .addItem("custcode", strCustCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCustDiscDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getOrderStatusDetailDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strSONo As String, ByVal strInvNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERORDERSTATUSDET"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("sono", strSONo, 2)
                .addItem("invno", strInvNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getOrderStatusDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getPromoCampDT(ByVal strCustCode As String, ByVal strType As String, Optional ByVal strCampaignCode As String = Nothing) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERPROMOCAMP"

                .addItem("custcode", strCustCode, 2)
                .addItem("type", strType, 2)
                .addItem("campaigncode", strCampaignCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPromoCampDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getPlanListRouteDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strType As String, Optional ByVal strDateRoute As String = Nothing) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERPLANLISTROUTE"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("type", strType, 2)
                .addItem("routedate", strDateRoute, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPlanListRouteDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getOperationTimeDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strInd As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMEROPERATIONTIME"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("oprind", strInd, 2)


                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getOperationTimeDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getPdsDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strContactCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERPDS"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getPdsDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getBestTocallDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strContactCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERBTCALL"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getBestTocallDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
