'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	17/1011/2006
'	Purpose	    :	Class to build Preplan Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsActyQuery
    Implements IDisposable
#Region "Default Variable"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

#End Region

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsActyQuery"
        End Get
    End Property

    Public Function GetActivityList(ByVal strUserID As String, ByVal strPrincipal As String) As DataTable
        '
        Dim clsActyDB As clsDB
        Try
            clsActyDB = New clsDB
            With clsActyDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_ACTY_LIST"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipal, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActivityList :" & ex.Message))
        Finally
            clsActyDB = Nothing
        End Try
    End Function

    Public Function GetActivity_MasterList(ByVal strUserID As String, ByVal strPrincipal As String) As DataTable
        '
        Dim clsACTDB As clsDB
        Try
            clsACTDB = New clsDB
            With clsACTDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_ACTY_MST"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipal, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActivity_MasterList :" & ex.Message))
        Finally
            clsACTDB = Nothing
        End Try
    End Function

#Region "Default Method"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
