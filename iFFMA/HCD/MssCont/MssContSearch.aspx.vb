Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Partial Class iFFMA_HCD_MssCont_MssContSearch
    Inherits System.Web.UI.Page
    Public ReadOnly Property ClassName() As String
        Get
            Return "MssContSearch"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.MSS_CONT)
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

        End If
    End Sub

#Region "Event Handler"

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        txtSalesrepCode.Text = ""
        txtTxnNo.Text = ""
        txtCustCode.Text = ""
        ddlstatus.SelectedIndex = 0
        txtContCode.Text = ""
        UpdateSearch.Update()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub



End Class
