<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApplyStockDtl.ascx.vb" Inherits="iFFMA_Services_ApplyStockDtl" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlApplyStockDtl" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr><td align="center" style="width:95%;"><customToolkit:wuc_dgpaging ID="wuc_dgSTDtlPaging" runat="server" /></td></tr>
            <tr>
                <td align="center" style="width:95%;">
                    <ccGV:clsGridView ID="dgSTDtlList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
                <asp:HiddenField ID="hdTxnNo" runat="server" Value="" />
            </tr>
        </table>
        
    </ContentTemplate>
</asp:UpdatePanel>
