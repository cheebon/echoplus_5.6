'clsRelPerform
'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	18/12/2005
'	Purpose	    :	Class to build Relative Performance Report from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsActivitySales
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsActivitySales"
        End Get
    End Property

#Region "Activity & Sales - Individual Sales Rep"
    'SPP_RPT_ACTY_SALES_BY_SALESREP @USER_ID, @PRINCIPAL_ID, @YEAR, @MONTH, @TEAM_CODE, @SALESREP_CODE
    Public Function GetActSalesBySalesRep(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                    ByVal strYear As String, ByVal strMonth As String, _
                                    ByVal strRegionCode As String, ByVal strSalesrepCode As String, _
                                    ByVal intNetValue As Integer) As DataTable
        Dim clsKpiDB As clsDB
        Try
            clsKpiDB = New clsDB
            With clsKpiDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ACTY_SALES_BY_SALESREP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActSalesBySalesRep :" & ex.Message))
        Finally
            clsKpiDB = Nothing
        End Try
    End Function

    'SPP_RPT_ACTY_SALES_BY_DATE @USER_ID, @PRINCIPAL_ID, @YEAR, @MONTH, @TEAM_CODE, @SALESREP_CODE
    Public Function GetActSalesByDate(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, _
                                    ByVal intNetValue As Integer) As DataTable
        Dim clsKpiDB As clsDB
        Try
            clsKpiDB = New clsDB
            With clsKpiDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ACTY_SALES_BY_DATE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActSalesByDate :" & ex.Message))
        Finally
            clsKpiDB = Nothing
        End Try
    End Function

#End Region

#Region "Activity & Sales - Daily Detail"
    'SPP_RPT_ACTY_SALES_DAILYDTL @USER_ID, @PRINCIPAL_ID, @DATE, @SALESREP_CODE
    'SPP_RPT_ACTY_SALES_DAILY_DTL @USER_ID,@PRINCIPAL_ID,@PRINCIPAL_CODE,@DATE,@SALESREP_CODE
    Public Function GetActSalesByDailyDtl(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strDate As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsKpiDB As clsDB
        Try
            clsKpiDB = New clsDB
            With clsKpiDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ACTY_SALES_DAILY_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("DATE", strDate, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActSalesByDailyDtl :" & ex.Message))
        Finally
            clsKpiDB = Nothing
        End Try
    End Function

#End Region

#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class

