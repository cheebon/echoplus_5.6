﻿Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsPrePlanCuz
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPrePlanCuz"
        End Get
    End Property

#Region "Plan Customer"
    Function GetPlanAddonYearCustCont(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_YEAR"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonDateCustCont :" & ex.Message))
        End Try
    End Function

    Function GetPlanAddonMonthCustCont(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_MONTH"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonDateCustCont :" & ex.Message))
        End Try
    End Function

    Function GetPlanAddonDateCustCont(ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_DATE"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonDateCustCont :" & ex.Message))
        End Try
    End Function

    Function GetPlanAddonCustCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_CUST"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonCustCont :" & ex.Message))
        End Try
    End Function

    Public Sub InstPlanAddonCustCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_PLAN_ADDON_CREATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".InstPlanAddonCustCont :" & ex.Message))
        End Try
    End Sub

    Public Sub DelPlanAddonCustCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_PLAN_ADDON_DELETE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spDelete()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DelPlanAddonCustCont :" & ex.Message))
        End Try
    End Sub

#End Region

#Region "Plan MSS Title Adv"
    Function GetPlanAddonMSSCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_MSS_TITLE_ADV"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonCont :" & ex.Message))
        End Try
    End Function

    Function GetPlanAddonContByMSSCont(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String, ByVal strRouteDate As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_MSS_TITLE_ADV_BY_CUST"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonCont :" & ex.Message))
        End Try
    End Function

    Public Sub InstPlanAddonMSSCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strTitleCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_MSS_PLAN_ADDON_CREATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".InstPlanAddonMSSCont :" & ex.Message))
        End Try
    End Sub

    Public Sub DelPlanAddonMSSCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strTitleCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_MSS_PLAN_ADDON_DELETE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spDelete()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DelPlanAddonMSS :" & ex.Message))
        End Try
    End Sub
#End Region

#Region "Plan SFMS"
    Function GetPlanAddonSFMSCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_SFMS"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonCont :" & ex.Message))
        End Try
    End Function

    Function GetPlanAddonContBySFMSCont(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String, ByVal strRouteDate As String, ByVal strCatCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_SFMS_BY_CUST"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonCont :" & ex.Message))
        End Try
    End Function

    Public Sub InstPlanAddonSFMSCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_SFMS_PLAN_ADDON_CREATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".InstPlanAddonSFMS :" & ex.Message))
        End Try
    End Sub

    Public Sub DelPlanAddonSFMSCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_SFMS_PLAN_ADDON_DELETE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spDelete()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DelPlanAddonSFMS :" & ex.Message))
        End Try
    End Sub
#End Region

#Region "Plan SFMS Cuz"
    Function GetPlanAddonSFMSCuzCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_SFMS"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonSFMSCuz :" & ex.Message))
        End Try
    End Function

    Function GetPlanAddonContBySFMSCuzCont(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String, ByVal strRouteDate As String, ByVal strCatCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_SFMS_BY_CUST"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonContBySFMSCuz :" & ex.Message))
        End Try
    End Function

    Public Sub InstPlanAddonSFMSCuzCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_SFMS_PLAN_ADDON_CREATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".InstPlanAddonSFMSCuz :" & ex.Message))
        End Try
    End Sub

    Public Sub DelPlanAddonSFMSCuzCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_SFMS_PLAN_ADDON_DELETE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spDelete()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DelPlanAddonSFMSCuz :" & ex.Message))
        End Try
    End Sub

    Public Sub UpdPlanAddonSFMSCuzCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strUserId As String, ByVal strQty As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_SFMS_PLAN_ADDON_UPDATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
                .addItem("QTY", strQty, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".UpdPlanAddonSFMSCuz :" & ex.Message))
        End Try
    End Sub
#End Region

#Region "Plan Remarks"
    Function GetPlanAddonRmksCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GET_PLAN_CUST_CONT_ADDON_RMKS"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPlanAddonRmks :" & ex.Message))
        End Try
    End Function

    Public Sub InstPlanAddonRmksCont(ByVal strSalesrepCode As String, ByVal strRouteDate As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strRemarks As String, ByVal strUserId As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_TMP_TXN_CUST_CONT_RMKS_PLAN_ADDON_CREATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".InstPlanAddonRmks :" & ex.Message))
        End Try
    End Sub


#End Region

#Region "Plan Visit"

    Function getContactGeneralInfoDT(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strTypeInd As String, ByVal strDeptName As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLAN_CUSTOMERCONTACTDETAIL"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("DEPT_NAME", strDeptName, clsDB.DataType.DBString)
                .addItem("TYPE_IND", strTypeInd, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactGeneralInfoDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getContactInd(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLAN_CUSTOMERIND"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactInd :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getDeptName(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLAN_CUSTOMER_DEPTNAME"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactInd :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function UpdVisitInd(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strRouteDate As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLAN_CUSTOMERVISIT"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("ROUTE_DATE", strRouteDate, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getContactInd :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function


#End Region


#Region "Exception"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
