'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	User Grp List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_UserGrp_UserGrpList
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "User Group List"
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request

                'Prepare Recordset,Createobject as needed
                ddlSearchType.SelectedValue = "all"
                ddlSearchType_onChanged()
                BindGrid(ViewState("SortCol"))
            End If
        Catch ex As Exception
            ExceptionMsg("UserGrpList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        'Dim objUserGrpQuery As adm_UserGrp.clsUserGrpQuery
        Dim dt As DataTable = Nothing

        Try
            If intPassFlag <> 1 Or IsNothing(ViewState("UserGrpList")) Then
                'objUserGrpQuery = New adm_UserGrp.clsUserGrpQuery
                'With objUserGrpQuery
                '    .clsProperties.search_type = Trim(ddlSearchType.SelectedValue)
                '    Select Case ddlSearchType.SelectedValue
                '        'Case "Date"
                '        '    .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                '        '    .clsProperties.search_value_1 = Trim(Replace(txtSearchValue1.Text, "*", "%"))
                '        'Case "StatusID"
                '        '    .clsProperties.search_value = Trim(ddlList.SelectedValue)
                '        Case Else
                '            .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                '    End Select
                '    .clsProperties.delete_flag = 1
                '    dt = .GetUserGrpList
                'End With
                ViewState("UserGrpList") = dt
            Else
                dt = ViewState("UserGrpList")
            End If
            'dt.DefaultView.Sort = SortExpression
            'If dt.Rows.Count < 1 Then
            '    dt.Rows.Add(dt.NewRow)
            'End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            'dgList.PageSize = intPageSize
            'dgList.GridWidth = Unit.Percentage(100)
            'dgList.DataSource = dt.DefaultView
            'dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With

        Catch ex As Exception
            ExceptionMsg("UserGrpList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            'objUserGrpQuery = Nothing
        End Try
    End Sub

#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via UserGrp conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("UserGrpList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via UserGrp conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("UserGrpList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via UserGrp conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("UserGrpList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "dgList"
    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        'Dim objUserGrp As adm_UserGrp.clsUserGrp
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgList.Rows(index)

        Try
            Select Case e.CommandName
                Case "Delete"
                    'objUserGrp = New adm_UserGrp.clsUserGrp
                    'With objUserGrp
                    '    .clsProperties.UserGrp_id = Trim(dgList.DataKeys(row.RowIndex).Value)
                    '    .Delete()
                    'End With

                    Response.Redirect("UserGrpList.aspx", False)

                Case "Edit"
                    Response.Redirect("UserGrpEdit.aspx?UserGrp_id=" & Trim(dgList.DataKeys(row.RowIndex).Value), False)

            End Select

        Catch ex As Exception
            ExceptionMsg("UserGrpList.dgList_RowCommand : " + ex.ToString)
        Finally
            'objUserGrp = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_RowDataBound
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim imgEdit, imgDelete As ImageButton

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                drv = CType(e.Row.DataItem, DataRowView)

                If IsDBNull(drv("UserGrp_name")) Then
                    imgDelete = CType(e.Row.Cells(5).Controls(0), ImageButton)
                    imgEdit = CType(e.Row.Cells(6).Controls(0), ImageButton)
                    imgDelete.Visible = False
                    imgEdit.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionMsg("UserGrpList.dgList_RowDataBound : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("UserGrpList.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        'Dim objUserGrp As adm_UserGrp.clsUserGrp
        'Dim intCurrentPage As Integer

        Try
            '    dgList.EditIndex = -1

            '    objUserGrp = New adm_UserGrp.clsUserGrp
            '    With objUserGrp
            '        .clsProperties.UserGrp_id = "0" 'drv("UserGrp_id")
            '        .clsProperties.UserGrp_code = Trim(dgList.DataKeys(e.RowIndex).Value)
            '        .Delete()
            '    End With

            '    'Response.Redirect("UserGrpList.aspx", False)

            '    intCurrentPage = wuc_dgpaging.CurrentPageIndex
            '    dgList.PageIndex = 0

            '    BindGrid(ViewState("SortCol"))

            '    If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
            '        wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            '    Else
            '        wuc_dgpaging.CurrentPageIndex = intCurrentPage
            '    End If
            '    wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            '    dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            '    BindGrid(ViewState("SortCol"), 1)

        Catch ex As Exception
            ExceptionMsg("UserGrpList.dgList_RowDeleting : " + ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

    End Sub
#End Region

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
        Catch ex As Exception
            lblErr.Text = "UserGrpList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnCreate_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            Response.Redirect("UserGrpCreate.aspx", False)
        Catch ex As Exception
            ExceptionMsg("UserGrpList.btnCreate_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            ddlSearchType_onChanged()
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "UserGrpList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_onChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ddlSearchType_onChanged()
        Try
            txtSearchValue.Text = ""
            txtSearchValue1.Text = ""
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"

            lblSearchValue.Visible = False
            lblDot.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            'lblDate1.Visible = False
            'lblDate2.Visible = False

            Select Case ddlSearchType.SelectedValue
                Case "all"

                    'Case "Date"
                    '    lblSearchValue.Visible = True
                    '    lblDot.Visible = True
                    '    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    '    lblTo.Visible = True
                    '    txtSearchValue.Visible = True
                    '    txtSearchValue.Text = ""
                    '    txtSearchValue.ReadOnly = True
                    '    txtSearchValue1.Visible = True
                    '    txtSearchValue1.ReadOnly = True
                    '    txtSearchValue1.Text = ""
                    '    btnSearch.Visible = True
                    '    'lblDate1.Visible = True
                    '    'lblDate2.Visible = True

                    'Case "status"
                    '    lblSearchValue.Visible = True
                    '    lblDot.Visible = True
                    '    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    '    btnSearch.Visible = True
                    '    ddlList.Visible = True

                    '    'Get Status Record
                    '    ddlList.Items.Clear()
                    '    ddlList.Items.Add(New ListItem("Select", ""))
                    '    ddlList.Items.Add(New ListItem("Active", "1"))
                    '    ddlList.Items.Add(New ListItem("In-Active", "0"))

                Case Else
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    txtSearchValue.Text = ""
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "UserGrpList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
