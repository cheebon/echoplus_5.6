<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerGeneralInfo" CodeFile="CustomerGeneralInfo.aspx.vb" %>

<%@ Register TagPrefix="uc1" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer General Infomation</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCustomerGenInfo" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                        </asp:Timer>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td class="Bckgroundreport">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td align="left" style="height: 115px; width: 858px" colspan="3">
                                                                                <table id="Table4" border="0" cellpadding="0" cellspacing="0" width="800px">
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblSalesAcc" runat="server" CssClass="cls_label_header">Sales Account :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblCustCode" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="width: 209px; height: 21px">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label3" runat="server" CssClass="cls_label_header">Tel No :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblTelNo" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label4" runat="server" CssClass="cls_label_header">Fax No :</asp:Label></td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                            <asp:Label ID="lblFaxNo" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label5" runat="server" CssClass="cls_label_header">Status :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblStatus" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label6" runat="server" CssClass="cls_label_header">P-Term :</asp:Label></td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                            <asp:Label ID="lblPTerm" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label7" runat="server" CssClass="cls_label_header">Out Balance :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblOutBalance" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label8" runat="server" CssClass="cls_label_header">Crd Limit :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblCrdLimit" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label9" runat="server" CssClass="cls_label_header">Type :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblType" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label10" runat="server" CssClass="cls_label_header" Width="85px">Best Time Call :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:DropDownList ID="ddlBTC" runat="server" CssClass="cls_dropdownlist">
                                                                                            </asp:DropDownList></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label11" runat="server" CssClass="cls_label_header">Visit Frequency :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblVisitFreq" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label13" runat="server" CssClass="cls_label_header">Email :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblEmail" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label14" runat="server" CssClass="cls_label_header">Open Date :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:Label ID="lblOpenDate" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label15" runat="server" CssClass="cls_label_header">Visit Date :</asp:Label></td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                            <asp:Label ID="lblVisitDate" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label16" runat="server" CssClass="cls_label_header">Opening :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:DropDownList ID="ddlOpening" runat="server" CssClass="cls_dropdownlist">
                                                                                            </asp:DropDownList></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label17" runat="server" CssClass="cls_label_header">Closing :</asp:Label></td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                            <asp:DropDownList ID="ddlClosing" runat="server" CssClass="cls_dropdownlist">
                                                                                            </asp:DropDownList></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label18" runat="server" CssClass="cls_label_header">Del Date :</asp:Label></td>
                                                                                        <td style="width: 250px; height: 21px">
                                                                                            <asp:DropDownList ID="ddlDelDate" runat="server" CssClass="cls_dropdownlist">
                                                                                            </asp:DropDownList></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 209px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px" valign="top">
                                                                                            <asp:Label ID="Label19" runat="server" CssClass="cls_label_header">Remarks :</asp:Label></td>
                                                                                        <td colspan="3" style="height: 21px; width: 209px;">
                                                                                            <asp:Label ID="lblRemarks" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
