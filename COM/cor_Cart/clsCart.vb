Imports System.Diagnostics

Public Class clsCart
    Private objEventLog As New EventLog("Application", ".", "HIS - .Net Version 2003")
    Private objSession As System.Web.HttpContext
    Private Const mSessionName As String = "Cart"
    Private mCartID As Object
    Private mDocID As Object
    Private mDocTypeID As Object
    Private mStatusID As Object
    Private mUserID As Object
    Private mCompanyID As Object
    Private mBranchID As Object
    Private mPrevID As Object
    Private mEditType As Object
    Private mSourceFlag As Object
    Private mComments As Object
    Private mTotal As Object
    Private mField1 As Object
    Private mField2 As Object
    Private mField3 As Object
    Private mField4 As Object
    Private mField5 As Object
    Private mField6 As Object
    Private mField7 As Object
    Private mField8 As Object
    Private mField9 As Object
    Private mField10 As Object
    Private mField11 As Object
    Private mField12 As Object
    Private mField13 As Object
    Private mField14 As Object
    Private mField15 As Object
    Private mField16 As Object
    Private mField17 As Object
    Private mField18 As Object
    Private mField19 As Object
    Private mField20 As Object
    Private mField21 As Object
    Private mField22 As Object
    Private mField23 As Object
    Private mField24 As Object
    Private mField25 As Object
    Private mField26 As Object
    Private mField27 As Object
    Private mField28 As Object
    Private mField29 As Object
    Private mField30 As Object
    Private mflag1 As Object
    Private mflag2 As Object
    Private mflag3 As Object
    Private mflag4 As Object
    Private mflag5 As Object
    Private mflag6 As Object
    Private mflag7 As Object
    Private mflag8 As Object
    Private mflag9 As Object
    Private mflag10 As Object
    Private mID1 As Object
    Private mID2 As Object
    Private mID3 As Object
    Private mID4 As Object
    Private mID5 As Object
    Private mID6 As Object
    Private mID7 As Object
    Private mID8 As Object
    Private mID9 As Object
    Private mID10 As Object
    Private mfloat1 As Object
    Private mfloat2 As Object
    Private mfloat3 As Object
    Private mfloat4 As Object
    Private mfloat5 As Object
    Private mmoney1 As Object
    Private mmoney2 As Object
    Private mmoney3 As Object
    Private mmoney4 As Object
    Private mmoney5 As Object
    Private mExtended As Object

    Public Property CartID() As Object
        Get
            Return mCartID
        End Get
        Set(ByVal Value As Object)
            mCartID = Value
        End Set
    End Property

    Public Property DocID() As Object
        Get
            Return mDocID
        End Get
        Set(ByVal Value As Object)
            mDocID = Value
        End Set
    End Property

    Public Property DocTypeID() As Object
        Get
            Return mDocTypeID
        End Get
        Set(ByVal Value As Object)
            mDocTypeID = Value
        End Set
    End Property

    Public Property StatusID() As Object
        Get
            Return mStatusID
        End Get
        Set(ByVal Value As Object)
            mStatusID = Value
        End Set
    End Property

    Public Property UserID() As Object
        Get
            Return mUserID
        End Get
        Set(ByVal Value As Object)
            mUserID = Value
        End Set
    End Property

    Public Property CompanyID() As Object
        Get
            Return mCompanyID
        End Get
        Set(ByVal Value As Object)
            mCompanyID = Value
        End Set
    End Property

    Public Property BranchID() As Object
        Get
            Return mBranchID
        End Get
        Set(ByVal Value As Object)
            mBranchID = Value
        End Set
    End Property

    Public Property PrevID() As Object
        Get
            Return mPrevID
        End Get
        Set(ByVal Value As Object)
            mPrevID = Value
        End Set
    End Property

    Public Property EditType() As Object
        Get
            Return mEditType
        End Get
        Set(ByVal Value As Object)
            mEditType = Value
        End Set
    End Property

    Public Property SourceFlag() As Object
        Get
            Return mSourceFlag
        End Get
        Set(ByVal Value As Object)
            mSourceFlag = Value
        End Set
    End Property

    Public Property Comments() As Object
        Get
            Return mComments
        End Get
        Set(ByVal Value As Object)
            mComments = Value
        End Set
    End Property

    Public Property Total() As Object
        Get
            Return mTotal
        End Get
        Set(ByVal Value As Object)
            mTotal = Value
        End Set
    End Property

    Public Property Field1() As Object
        Get
            Return mField1
        End Get
        Set(ByVal Value As Object)
            mField1 = Value
        End Set
    End Property

    Public Property Field2() As Object
        Get
            Return mField2
        End Get
        Set(ByVal Value As Object)
            mField2 = Value
        End Set
    End Property

    Public Property Field3() As Object
        Get
            Return mField3
        End Get
        Set(ByVal Value As Object)
            mField3 = Value
        End Set
    End Property

    Public Property Field4() As Object
        Get
            Return mField4
        End Get
        Set(ByVal Value As Object)
            mField4 = Value
        End Set
    End Property

    Public Property Field5() As Object
        Get
            Return mField5
        End Get
        Set(ByVal Value As Object)
            mField5 = Value
        End Set
    End Property

    Public Property Field6() As Object
        Get
            Return mField6
        End Get
        Set(ByVal Value As Object)
            mField6 = Value
        End Set
    End Property

    Public Property Field7() As Object
        Get
            Return mField7
        End Get
        Set(ByVal Value As Object)
            mField7 = Value
        End Set
    End Property

    Public Property Field8() As Object
        Get
            Return mField8
        End Get
        Set(ByVal Value As Object)
            mField8 = Value
        End Set
    End Property

    Public Property Field9() As Object
        Get
            Return mField9
        End Get
        Set(ByVal Value As Object)
            mField9 = Value
        End Set
    End Property

    Public Property Field10() As Object
        Get
            Return mField10
        End Get
        Set(ByVal Value As Object)
            mField10 = Value
        End Set
    End Property

    Public Property Field11() As Object
        Get
            Return mField11
        End Get
        Set(ByVal Value As Object)
            mField11 = Value
        End Set
    End Property

    Public Property Field12() As Object
        Get
            Return mField12
        End Get
        Set(ByVal Value As Object)
            mField12 = Value
        End Set
    End Property

    Public Property Field13() As Object
        Get
            Return mField13
        End Get
        Set(ByVal Value As Object)
            mField13 = Value
        End Set
    End Property

    Public Property Field14() As Object
        Get
            Return mField14
        End Get
        Set(ByVal Value As Object)
            mField14 = Value
        End Set
    End Property

    Public Property Field15() As Object
        Get
            Return mField15
        End Get
        Set(ByVal Value As Object)
            mField15 = Value
        End Set
    End Property

    Public Property Field16() As Object
        Get
            Return mField16
        End Get
        Set(ByVal Value As Object)
            mField16 = Value
        End Set
    End Property

    Public Property Field17() As Object
        Get
            Return mField17
        End Get
        Set(ByVal Value As Object)
            mField17 = Value
        End Set
    End Property

    Public Property Field18() As Object
        Get
            Return mField18
        End Get
        Set(ByVal Value As Object)
            mField18 = Value
        End Set
    End Property

    Public Property Field19() As Object
        Get
            Return mField19
        End Get
        Set(ByVal Value As Object)
            mField19 = Value
        End Set
    End Property

    Public Property Field20() As Object
        Get
            Return mField20
        End Get
        Set(ByVal Value As Object)
            mField20 = Value
        End Set
    End Property

    Public Property Field21() As Object
        Get
            Return mField21
        End Get
        Set(ByVal Value As Object)
            mField21 = Value
        End Set
    End Property

    Public Property Field22() As Object
        Get
            Return mField22
        End Get
        Set(ByVal Value As Object)
            mField22 = Value
        End Set
    End Property

    Public Property Field23() As Object
        Get
            Return mField23
        End Get
        Set(ByVal Value As Object)
            mField23 = Value
        End Set
    End Property

    Public Property Field24() As Object
        Get
            Return mField24
        End Get
        Set(ByVal Value As Object)
            mField24 = Value
        End Set
    End Property

    Public Property Field25() As Object
        Get
            Return mField25
        End Get
        Set(ByVal Value As Object)
            mField25 = Value
        End Set
    End Property

    Public Property Field26() As Object
        Get
            Return mField26
        End Get
        Set(ByVal Value As Object)
            mField26 = Value
        End Set
    End Property

    Public Property Field27() As Object
        Get
            Return mField27
        End Get
        Set(ByVal Value As Object)
            mField27 = Value
        End Set
    End Property

    Public Property Field28() As Object
        Get
            Return mField28
        End Get
        Set(ByVal Value As Object)
            mField28 = Value
        End Set
    End Property

    Public Property Field29() As Object
        Get
            Return mField29
        End Get
        Set(ByVal Value As Object)
            mField29 = Value
        End Set
    End Property

    Public Property Field30() As Object
        Get
            Return mField30
        End Get
        Set(ByVal Value As Object)
            mField30 = Value
        End Set
    End Property

    Public Property flag1() As Object
        Get
            Return mflag1
        End Get
        Set(ByVal Value As Object)
            mflag1 = Value
        End Set
    End Property

    Public Property flag2() As Object
        Get
            Return mflag2
        End Get
        Set(ByVal Value As Object)
            mflag2 = Value
        End Set
    End Property

    Public Property flag3() As Object
        Get
            Return mflag3
        End Get
        Set(ByVal Value As Object)
            mflag3 = Value
        End Set
    End Property

    Public Property flag4() As Object
        Get
            Return mflag4
        End Get
        Set(ByVal Value As Object)
            mflag4 = Value
        End Set
    End Property

    Public Property flag5() As Object
        Get
            Return mflag5
        End Get
        Set(ByVal Value As Object)
            mflag5 = Value
        End Set
    End Property

    Public Property flag6() As Object
        Get
            Return mflag6
        End Get
        Set(ByVal Value As Object)
            mflag6 = Value
        End Set
    End Property

    Public Property flag7() As Object
        Get
            Return mflag7
        End Get
        Set(ByVal Value As Object)
            mflag7 = Value
        End Set
    End Property

    Public Property flag8() As Object
        Get
            Return mflag8
        End Get
        Set(ByVal Value As Object)
            mflag8 = Value
        End Set
    End Property

    Public Property flag9() As Object
        Get
            Return mflag9
        End Get
        Set(ByVal Value As Object)
            mflag9 = Value
        End Set
    End Property

    Public Property flag10() As Object
        Get
            Return mflag10
        End Get
        Set(ByVal Value As Object)
            mflag10 = Value
        End Set
    End Property

    Public Property ID1() As Object
        Get
            Return mID1
        End Get
        Set(ByVal Value As Object)
            mID1 = Value
        End Set
    End Property

    Public Property ID2() As Object
        Get
            Return mID2
        End Get
        Set(ByVal Value As Object)
            mID2 = Value
        End Set
    End Property

    Public Property ID3() As Object
        Get
            Return mID3
        End Get
        Set(ByVal Value As Object)
            mID3 = Value
        End Set
    End Property

    Public Property ID4() As Object
        Get
            Return mID4
        End Get
        Set(ByVal Value As Object)
            mID4 = Value
        End Set
    End Property

    Public Property ID5() As Object
        Get
            Return mID5
        End Get
        Set(ByVal Value As Object)
            mID5 = Value
        End Set
    End Property

    Public Property ID6() As Object
        Get
            Return mID6
        End Get
        Set(ByVal Value As Object)
            mID6 = Value
        End Set
    End Property

    Public Property ID7() As Object
        Get
            Return mID7
        End Get
        Set(ByVal Value As Object)
            mID7 = Value
        End Set
    End Property

    Public Property ID8() As Object
        Get
            Return mID8
        End Get
        Set(ByVal Value As Object)
            mID8 = Value
        End Set
    End Property

    Public Property ID9() As Object
        Get
            Return mID9
        End Get
        Set(ByVal Value As Object)
            mID9 = Value
        End Set
    End Property

    Public Property ID10() As Object
        Get
            Return mID10
        End Get
        Set(ByVal Value As Object)
            mID10 = Value
        End Set
    End Property

    Public Property float1() As Object
        Get
            Return mfloat1
        End Get
        Set(ByVal Value As Object)
            mfloat1 = Value
        End Set
    End Property

    Public Property float2() As Object
        Get
            Return mfloat2
        End Get
        Set(ByVal Value As Object)
            mfloat2 = Value
        End Set
    End Property

    Public Property float3() As Object
        Get
            Return mfloat3
        End Get
        Set(ByVal Value As Object)
            mfloat3 = Value
        End Set
    End Property

    Public Property float4() As Object
        Get
            Return mfloat4
        End Get
        Set(ByVal Value As Object)
            mfloat4 = Value
        End Set
    End Property

    Public Property float5() As Object
        Get
            Return mfloat5
        End Get
        Set(ByVal Value As Object)
            mfloat5 = Value
        End Set
    End Property

    Public Property money1() As Object
        Get
            Return mmoney1
        End Get
        Set(ByVal Value As Object)
            mmoney1 = Value
        End Set
    End Property

    Public Property money2() As Object
        Get
            Return mmoney2
        End Get
        Set(ByVal Value As Object)
            mmoney2 = Value
        End Set
    End Property

    Public Property money3() As Object
        Get
            Return mmoney3
        End Get
        Set(ByVal Value As Object)
            mmoney3 = Value
        End Set
    End Property

    Public Property money4() As Object
        Get
            Return mmoney4
        End Get
        Set(ByVal Value As Object)
            mmoney4 = Value
        End Set
    End Property

    Public Property money5() As Object
        Get
            Return mmoney5
        End Get
        Set(ByVal Value As Object)
            mmoney5 = Value
        End Set
    End Property

    Public Property Extended() As Object
        Get
            Return mExtended
        End Get
        Set(ByVal Value As Object)
            mExtended = Value
        End Set
    End Property

    Public Sub Populate()
        Dim dtTable As New DataTable("Cart")
        Dim i As Integer

        Try
            Dim dcKey As New DataColumn
            dcKey.ColumnName = "CartID"
            dcKey.DataType = GetType(Long)
            dcKey.AutoIncrement = True
            dcKey.AutoIncrementSeed = 1
            dcKey.AutoIncrementStep = 1
            dtTable.Columns.Add(dcKey)
            dtTable.Constraints.Add("key1", dcKey, True)
            dcKey.Dispose()

            Dim dcDocID As New DataColumn
            dcDocID.ColumnName = "DocID"
            dcDocID.DataType = GetType(Long)
            dtTable.Columns.Add(dcDocID)
            dcDocID.Dispose()

            Dim dcDocTypeID As New DataColumn
            dcDocTypeID.ColumnName = "DocTypeID"
            dcDocTypeID.DataType = GetType(Long)
            dtTable.Columns.Add(dcDocTypeID)
            dcDocTypeID.Dispose()

            Dim dcStatusID As New DataColumn
            dcStatusID.ColumnName = "StatusID"
            dcStatusID.DataType = GetType(Long)
            dtTable.Columns.Add(dcStatusID)
            dcStatusID.Dispose()

            Dim dcUserID As New DataColumn
            dcUserID.ColumnName = "UserID"
            dcUserID.DataType = GetType(Long)
            dtTable.Columns.Add(dcUserID)
            dcUserID.Dispose()

            Dim dcCompanyID As New DataColumn
            dcCompanyID.ColumnName = "CompanyID"
            dcCompanyID.DataType = GetType(Long)
            dtTable.Columns.Add(dcCompanyID)
            dcCompanyID.Dispose()

            Dim dcBranchID As New DataColumn
            dcBranchID.ColumnName = "BranchID"
            dcBranchID.DataType = GetType(Long)
            dtTable.Columns.Add(dcBranchID)
            dcBranchID.Dispose()

            Dim dcPrevID As New DataColumn
            dcPrevID.ColumnName = "PrevID"
            dcPrevID.DataType = GetType(Long)
            dtTable.Columns.Add(dcPrevID)
            dcPrevID.Dispose()

            Dim dcEditType As New DataColumn
            dcEditType.ColumnName = "EditType"
            dcEditType.DataType = GetType(Integer)
            dtTable.Columns.Add(dcEditType)
            dcEditType.Dispose()

            Dim dcSourceFlag As New DataColumn
            dcSourceFlag.ColumnName = "SourceFlag"
            dcSourceFlag.DataType = GetType(Integer)
            dtTable.Columns.Add(dcSourceFlag)
            dcSourceFlag.Dispose()

            Dim dcComments As New DataColumn
            dcComments.ColumnName = "Comments"
            dcComments.DataType = GetType(String)
            dtTable.Columns.Add(dcComments)
            dcComments.Dispose()

            Dim dcTotal As New DataColumn
            dcTotal.ColumnName = "Total"
            dcTotal.DataType = GetType(Double)
            dtTable.Columns.Add(dcTotal)
            dcTotal.Dispose()

            Dim dcExtended As New DataColumn
            dcExtended.ColumnName = "Extended"
            dcExtended.DataType = GetType(String)
            dtTable.Columns.Add(dcExtended)
            dcExtended.Dispose()

            'Fields
            For i = 1 To 30
                Dim dcField As New DataColumn
                dcField.ColumnName = "Field" & CStr(i)
                dcField.DataType = GetType(String)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'flag
            For i = 1 To 10
                Dim dcField As New DataColumn
                dcField.ColumnName = "flag" & CStr(i)
                dcField.DataType = GetType(Integer)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'ID
            For i = 1 To 10
                Dim dcField As New DataColumn
                dcField.ColumnName = "ID" & CStr(i)
                dcField.DataType = GetType(Long)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'float
            For i = 1 To 5
                Dim dcField As New DataColumn
                dcField.ColumnName = "float" & CStr(i)
                dcField.DataType = GetType(Double)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            'money
            For i = 1 To 5
                Dim dcField As New DataColumn
                dcField.ColumnName = "money" & CStr(i)
                dcField.DataType = GetType(String)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.Populate : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Function Create() As Long
        Dim dtTable As DataTable
        Dim drRow As DataRow
       
        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            drRow = dtTable.NewRow
            drRow = PopulateRow(drRow)

            ' Add the new row to the table.
            dtTable.Rows.Add(drRow)

            Web.HttpContext.Current.Session(mSessionName) = dtTable
            Create = drRow("CartID")

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.Create : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function

    Public Sub Update()
        Dim dtTable As DataTable
        Dim drRow As DataRow

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            drRow = dtTable.Rows.Find(mCartID)

            If Not (drRow Is Nothing) Then
                drRow.BeginEdit()
                drRow = PopulateRow(drRow)
                drRow.EndEdit()
            End If

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.Update : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub Delete()
        Dim dtTable As DataTable
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow
        Dim strSQL As String

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            strSQL = " CartID=" & IIf(mCartID = Nothing Or IsDBNull(mCartID), 0, mCartID)
            drCurrRow = dtTable.Select(strSQL, "CartID ASC")

            For Each drRow In drCurrRow
                dtTable.Rows.Remove(drRow)
            Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.Delete : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Public Sub Clear()
        Dim dtTable As DataTable
        Dim drCurrRow() As DataRow
        Dim drRow As DataRow
        Dim strSQL As String

        Try
            dtTable = Web.HttpContext.Current.Session(mSessionName)
            strSQL = " DocTypeID=" & IIf(mDocTypeID = Nothing Or IsDBNull(mDocTypeID), 0, mDocTypeID)
            drCurrRow = dtTable.Select(strSQL, "CartID ASC")

            For Each drRow In drCurrRow
                dtTable.Rows.Remove(drRow)
            Next

            Web.HttpContext.Current.Session(mSessionName) = dtTable

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.Clear : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Sub

    Private Function PopulateRow(ByVal drRow As DataRow) As DataRow
        Try
            drRow("DocID") = IIf(mDocID = Nothing Or IsDBNull(mDocID), 0, mDocID)
            drRow("DocTypeID") = IIf(mDocTypeID = Nothing Or IsDBNull(mDocTypeID), 0, mDocTypeID)
            drRow("StatusID") = IIf(mStatusID = Nothing Or IsDBNull(mStatusID), 0, mStatusID)
            drRow("UserID") = IIf(mUserID = Nothing Or IsDBNull(mUserID), 0, mUserID)
            drRow("CompanyID") = IIf(mCompanyID = Nothing Or IsDBNull(mCompanyID), 0, mCompanyID)
            drRow("BranchID") = IIf(mBranchID = Nothing Or IsDBNull(mBranchID), 0, mBranchID)
            drRow("PrevID") = IIf(mPrevID = Nothing Or IsDBNull(mPrevID), 0, mPrevID)
            drRow("EditType") = IIf(mEditType = Nothing Or IsDBNull(mEditType), 0, mEditType)
            drRow("SourceFlag") = IIf(mSourceFlag = Nothing Or IsDBNull(mSourceFlag), 0, mSourceFlag)
            drRow("Comments") = mComments
            drRow("Total") = IIf(mTotal = Nothing Or IsDBNull(mTotal), 0, mTotal)
            drRow("Extended") = mExtended

            'Fields
            drRow("Field1") = mField1
            drRow("Field2") = mField2
            drRow("Field3") = mField3
            drRow("Field4") = mField4
            drRow("Field5") = mField5
            drRow("Field6") = mField6
            drRow("Field7") = mField7
            drRow("Field8") = mField8
            drRow("Field9") = mField9
            drRow("Field10") = mField10
            drRow("Field11") = mField11
            drRow("Field12") = mField12
            drRow("Field13") = mField13
            drRow("Field14") = mField14
            drRow("Field15") = mField15
            drRow("Field16") = mField16
            drRow("Field17") = mField17
            drRow("Field18") = mField18
            drRow("Field19") = mField19
            drRow("Field20") = mField20
            drRow("Field21") = mField21
            drRow("Field22") = mField22
            drRow("Field23") = mField23
            drRow("Field24") = mField24
            drRow("Field25") = mField25
            drRow("Field26") = mField26
            drRow("Field27") = mField27
            drRow("Field28") = mField28
            drRow("Field29") = mField29
            drRow("Field30") = mField30

            'flag
            drRow("flag1") = IIf(mflag1 = Nothing Or IsDBNull(mflag1), 0, mflag1)
            drRow("flag2") = IIf(mflag2 = Nothing Or IsDBNull(mflag2), 0, mflag2)
            drRow("flag3") = IIf(mflag3 = Nothing Or IsDBNull(mflag3), 0, mflag3)
            drRow("flag4") = IIf(mflag4 = Nothing Or IsDBNull(mflag4), 0, mflag4)
            drRow("flag5") = IIf(mflag5 = Nothing Or IsDBNull(mflag5), 0, mflag5)
            drRow("flag6") = IIf(mflag6 = Nothing Or IsDBNull(mflag6), 0, mflag6)
            drRow("flag7") = IIf(mflag7 = Nothing Or IsDBNull(mflag7), 0, mflag7)
            drRow("flag8") = IIf(mflag8 = Nothing Or IsDBNull(mflag8), 0, mflag8)
            drRow("flag9") = IIf(mflag9 = Nothing Or IsDBNull(mflag9), 0, mflag9)
            drRow("flag10") = IIf(mflag10 = Nothing Or IsDBNull(mflag10), 0, mflag10)

            'ID
            drRow("ID1") = IIf(mID1 = Nothing Or IsDBNull(mID1), 0, mID1)
            drRow("ID2") = IIf(mID2 = Nothing Or IsDBNull(mID2), 0, mID2)
            drRow("ID3") = IIf(mID3 = Nothing Or IsDBNull(mID3), 0, mID3)
            drRow("ID4") = IIf(mID4 = Nothing Or IsDBNull(mID4), 0, mID4)
            drRow("ID5") = IIf(mID5 = Nothing Or IsDBNull(mID5), 0, mID5)
            drRow("ID6") = IIf(mID6 = Nothing Or IsDBNull(mID6), 0, mID6)
            drRow("ID7") = IIf(mID7 = Nothing Or IsDBNull(mID7), 0, mID7)
            drRow("ID8") = IIf(mID8 = Nothing Or IsDBNull(mID8), 0, mID8)
            drRow("ID9") = IIf(mID9 = Nothing Or IsDBNull(mID9), 0, mID9)
            drRow("ID10") = IIf(mID10 = Nothing Or IsDBNull(mID10), 0, mID10)

            'float
            drRow("float1") = IIf(mfloat1 = Nothing Or IsDBNull(mfloat1), 0, mfloat1)
            drRow("float2") = IIf(mfloat2 = Nothing Or IsDBNull(mfloat2), 0, mfloat2)
            drRow("float3") = IIf(mfloat3 = Nothing Or IsDBNull(mfloat3), 0, mfloat3)
            drRow("float4") = IIf(mfloat4 = Nothing Or IsDBNull(mfloat4), 0, mfloat4)
            drRow("float5") = IIf(mfloat5 = Nothing Or IsDBNull(mfloat5), 0, mfloat5)

            'money
            drRow("money1") = IIf(mmoney1 = Nothing Or IsDBNull(mmoney1), 0, mmoney1)
            drRow("money2") = IIf(mmoney2 = Nothing Or IsDBNull(mmoney2), 0, mmoney2)
            drRow("money3") = IIf(mmoney3 = Nothing Or IsDBNull(mmoney3), 0, mmoney3)
            drRow("money4") = IIf(mmoney4 = Nothing Or IsDBNull(mmoney4), 0, mmoney4)
            drRow("money5") = IIf(mmoney5 = Nothing Or IsDBNull(mmoney5), 0, mmoney5)

            PopulateRow = drRow

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.PopulateRow : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function RetrieveFilterCartByDocID
    ' Purpose	:	This function filter the record according to the DocID and DocTypeID
    ' Parameters:	[in]  : 
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function RetrieveFilterCart() As DataTable
        Dim dtCartTable As DataTable
        Dim drRefRow() As DataRow
        Dim strCartSql As String

        Try
            dtCartTable = Web.HttpContext.Current.Session(mSessionName)

            'If Not IsNothing(dtCartTable) Then
            strCartSql = " DocTypeID=" & DocTypeID & " And DocID=" & DocID

            drRefRow = dtCartTable.Select(strCartSql, "CartID ASC")
            If drRefRow.Length > 0 Then
                'For Each drRow In drRefRow
                '    dtCartTable.ImportRow(drRow)
                'Next
            End If
            RetrieveFilterCart = dtCartTable
            'End If

        Catch ex As Exception
            objEventLog.WriteEntry("cor_Cart.clsCart.RetrieveFilterCart : " + ex.ToString())
            Throw New System.Exception(ex.ToString)
        End Try
    End Function

End Class