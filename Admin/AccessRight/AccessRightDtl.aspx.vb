'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	08/11/2006
'	Purpose	    :	Access Right Details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_AccessRight_AccessRightDtl
    Inherits System.Web.UI.Page

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim strAccessRightID As String
        Dim dt, dtDtl As DataTable

        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Access Right Details"
                .DataBind()
                .Visible = True
            End With

            btnDelete.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.AR, SubModuleAction.Delete)
            btnEdit.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.AR, SubModuleAction.Edit)

            If Not IsPostBack Then
                'constant

                'request
                strAccessRightID = Trim(Request.QueryString("accessright_id"))
                txtSearchType.Text = Trim(Request.QueryString("search_type"))
                txtSearchValue.Text = Trim(Request.QueryString("search_value"))

                'Prepare Recordset,Createobject as needed
                'Get AR
                objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
                With objAccessRightQuery
                    .clsProperties.accessright_id = strAccessRightID
                    dt = .GetAccessRightDtl()
                End With

                'Get ARDtl
                objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
                With objAccessRightQuery
                    .clsProperties.accessright_id = strAccessRightID
                    .clsProperties.type = "1"
                    dtDtl = .GetAccessRightDtlList()
                End With

                If dt.Rows.Count > 0 Then
                    txtAccessRightID.Text = Trim(dt.Rows(0)("accessright_id"))
                    lblNameValue.Text = Trim(dt.Rows(0)("accessright_name"))
                    lblName1Value.Text = Trim(dt.Rows(0)("accessright_name_1"))
                    lblDescValue.Text = Trim(dt.Rows(0)("accessright_desc"))
                    lblDesc1Value.Text = Trim(dt.Rows(0)("accessright_desc_1"))
                    lblCountryValue.Text = Trim(dt.Rows(0)("country_name"))
                    lblPrincipalValue.Text = Trim(dt.Rows(0)("principal_name"))
                End If

                'PHL:20070830*************************
                If dtDtl.Rows.Count > 0 Then
                    For i As Integer = 0 To dtDtl.Rows.Count - 1
                        If dtDtl.Rows(i)("submodule_id").ToString() = SubModuleType.DATA Or dtDtl.Rows(i)("submodule_id").ToString() = SubModuleType.DATA_V2 Then
                            btnSetDF.Visible = "True"
                        End If
                        If dtDtl.Rows(i)("submodule_id").ToString() = SubModuleType.EASYLOADER Then
                            btnSetEL.Visible = "True"
                        End If
                    Next
                End If
                '*************************************

                If dtDtl.Rows.Count < 1 Then
                    dtDtl.Rows.Add(dtDtl.NewRow)
                End If

                With dgARList
                    .DataSource = dtDtl
                    .DataBind()
                End With



            End If

        Catch ex As Exception
            ExceptionMsg("AccessRightDtl.Page_Load : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'PHL:20070830*************************
    Private Sub btnSetDf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetDF.Click
        Dim strScript As String

        Try
            strScript = "window.open('DFAccessRight.aspx?AccessRightID=" + txtAccessRightID.Text + "', '', 'height=550,innerHeight=550,width=800,innerWidth=800,scrollbars=yes,resizable=yea,left=107,screenX=107,top=81,screenY=81');"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>" + strScript + "</script>")
        Catch ex As Exception
            ExceptionMsg("AccessRightDtl.btnSetDf_Click : " & ex.ToString)
        End Try
    End Sub
    '*************************************


    Private Sub btnSetEL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetEL.Click
        Dim strScript As String

        Try
            strScript = "window.open('ELAccessRight.aspx?AccessRightID=" + txtAccessRightID.Text + "', '', 'height=550,innerHeight=550,width=800,innerWidth=800,scrollbars=yes,resizable=yea,left=107,screenX=107,top=81,screenY=81');"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>" + strScript + "</script>")
        Catch ex As Exception
            ExceptionMsg("AccessRightDtl.btnSetEL_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnViewTray_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnViewTray_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTray.Click
        Try
            Response.Redirect("AccessRightList.aspx?search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("AccessRightDtl.btnViewTray_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnEdit_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            Response.Redirect("AccessRightEdit.aspx?accessright_id=" & Trim(txtAccessRightID.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("AccessRightDtl.btnEdit_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnDelete_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objAccessRight As adm_AccessRight.clsAccessRight

        Try
            objAccessRight = New adm_AccessRight.clsAccessRight
            With objAccessRight
                .clsProperties.accessright_id = txtAccessRightID.Text
                .Delete()
            End With

            Response.Redirect("AccessRightList.aspx?search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("AccessRightDtl.btnDelete_Click : " & ex.ToString)
        Finally
            objAccessRight = Nothing
        End Try
    End Sub
End Class
