﻿/*Generate PopUp Script
    Created by: Alex Chia
    Date : 19 Sept 2006
    Use in "pnlSalesEnquirySearch" to get the search criteria
*/
    function launchCenter(url, name, height, width, addons) 
    {
      var str = "height=" + height + ",innerHeight=" + height;
      str += ",width=" + width + ",innerWidth=" + width + addons;
      if (window.screen) {
        var ah = screen.availHeight - 30;
        var aw = screen.availWidth - 10;

        var xc = (aw - width) / 2;
        var yc = (ah - height) / 2;

        str += ",left=" + xc + ",screenX=" + xc;
        str += ",top=" + yc + ",screenY=" + yc;
      }
      return window.open( url, name, str);
    }
    
/*    All About Sales Enquiry  */
    function SalesEnq_updateProductInfo(prdCode,prdName)
    {
        window.document.getElementById("wuc_ctrlpanel_txtProductNo").value = prdCode;
        window.document.getElementById("wuc_ctrlpanel_txtProductName").value = prdName;
    }

    function SalesEnq_updateCustomerInfo(custCode,custName)
    {
        document.getElementById("wuc_ctrlpanel_txtCustCode").value = custCode;
        document.getElementById("wuc_ctrlpanel_txtCustName").value = custName;
    }

    function SalesEnq_updateSalesrepInfo(slmCode,slmName)
    {
        document.getElementById("wuc_ctrlpanel_txtSalesmanNo").value = slmCode;
        document.getElementById("wuc_ctrlpanel_txtSalesmanName").value = slmName;
    }    

    function SalesEnq_openProduct()
    {
       myProduChild = launchCenter("../../Core/Catalog/ProductList.aspx?CATEGORY=SALES","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
    function SalesEnq_openCustomer()
    {
         myProduChild= launchCenter("../../Core/Catalog/CustomerList.aspx?CATEGORY=SALES","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function SalesEnq_openSalesman()
    {
        myProduChild = launchCenter("../../Core/Catalog/SalesrepList.aspx?CATEGORY=SALES","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
    
    /* All About Call Enq */
    function CallEnq_updateCustomerInfo(custCode,custName)
    {
        document.getElementById("wuc_ctrlpanel_txtCallEnq_CustCode").value = custCode;
        document.getElementById("wuc_ctrlpanel_txtCallEnq_CustName").value = custName;
    }

    function CallEnq_updateContactInfo(contCode,contName)
    {
        document.getElementById("wuc_ctrlpanel_txtCallEnq_ContCode").value = contCode;
        document.getElementById("wuc_ctrlpanel_txtCallEnq_ContName").value = contName;
    }

    function CallEnq_updateSalesrepInfo(slmCode,slmName)
    {   
        document.getElementById("wuc_ctrlpanel_txtCallEnq_Salesman").value = slmCode;
        document.getElementById("wuc_ctrlpanel_txtCallEnq_SalesmanName").value = slmName;
    }    

    function CallEnq_openCustomer()
    {
         myProduChild= launchCenter("../../../Core/Catalog/CustomerList.aspx?CATEGORY=CALL","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function CallEnq_openContact()
    {
         myProduChild= launchCenter("../../../Core/Catalog/ContactList.aspx?CATEGORY=CALL","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function CallEnq_openSalesman()
    {
        myProduChild = launchCenter("../../../Core/Catalog/SalesrepList.aspx?CATEGORY=CALL","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
    
    /*All About DRC Enquiry  */
    function DRCEnq_updateProductInfo(prdCode,prdName)
    {
        window.document.getElementById("wuc_ctrlpanel_txtDRCEnq_ProductNo").value = prdCode;
        window.document.getElementById("wuc_ctrlpanel_txtDRCEnq_ProductName").value = prdName;
    }

    function DRCEnq_updateCustomerInfo(custCode,custName)
    {
        document.getElementById("wuc_ctrlpanel_txtDRCEnq_CustCode").value = custCode;
        document.getElementById("wuc_ctrlpanel_txtDRCEnq_CustName").value = custName;
    }

    function DRCEnq_updateSalesrepInfo(slmCode,slmName)
    {
        document.getElementById("wuc_ctrlpanel_txtDRCEnq_SalesmanNo").value = slmCode;
        document.getElementById("wuc_ctrlpanel_txtDRCEnq_SalesmanName").value = slmName;
    }    

    function DRCEnq_openProduct()
    {
       myProduChild = launchCenter("../../Core/Catalog/ProductList.aspx?CATEGORY=DRC","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
    function DRCEnq_openCustomer()
    {
         myProduChild= launchCenter("../../Core/Catalog/CustomerList.aspx?CATEGORY=DRC","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function DRCEnq_openSalesman()
    {
        myProduChild = launchCenter("../../Core/Catalog/SalesrepList.aspx?CATEGORY=DRC","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
    /*All About Coll Cheque Enquiry  */
    function CollCheqEnq_updateCustomerInfo(custCode,custName)
    {
        document.getElementById("wuc_ctrlpanel_txtCollCheqEnq_CustCode").value = custCode;
        document.getElementById("wuc_ctrlpanel_txtCollCheqEnq_CustName").value = custName;
    }

    function CollCheqEnq_updateSalesrepInfo(slmCode,slmName)
    {
        document.getElementById("wuc_ctrlpanel_txtCollCheqEnq_SalesmanNo").value = slmCode;
        document.getElementById("wuc_ctrlpanel_txtCollCheqEnq_SalesmanName").value = slmName;
    }    

    function CollCheqEnq_openCustomer()
    {
         myProduChild= launchCenter("../../Core/Catalog/CustomerList.aspx?CATEGORY=COLLCHEQ","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function CollCheqEnq_openSalesman()
    {
        myProduChild = launchCenter("../../Core/Catalog/SalesrepList.aspx?CATEGORY=COLLCHEQ","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
      /*All About Coll Enquiry  */
    function CollEnq_updateCustomerInfo(custCode,custName)
    {
        document.getElementById("wuc_ctrlpanel_txtCollEnq_CustCode").value = custCode;
        document.getElementById("wuc_ctrlpanel_txtCollEnq_CustName").value = custName;
    }

    function CollEnq_updateSalesrepInfo(slmCode,slmName)
    {
        document.getElementById("wuc_ctrlpanel_txtCollEnq_SalesrepCode").value = slmCode;
        document.getElementById("wuc_ctrlpanel_txtCollEnq_SalesrepName").value = slmName;
    }    

    function CollEnq_openCustomer()
    {
         myProduChild= launchCenter("../../Core/Catalog/CustomerList.aspx?CATEGORY=COLLENQ","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function CollEnq_openSalesman()
    {
        myProduChild = launchCenter("../../Core/Catalog/SalesrepList.aspx?CATEGORY=COLLENQ","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
    
    /*All About PAF Enquiry  */
    function PAFEnq_updateCustomerInfo(custCode,custName)
    {
        document.getElementById("wuc_ctrlpanel_txtPAFEnq_CustCode").value = custCode;
        document.getElementById("wuc_ctrlpanel_txtPAFEnq_CustName").value = custName;
    }

    function PAFEnq_updateSalesrepInfo(slmCode,slmName)
    {
        document.getElementById("wuc_ctrlpanel_txtPAFEnq_SalesmanNo").value = slmCode;
        document.getElementById("wuc_ctrlpanel_txtPAFEnq_SalesmanName").value = slmName;
    }    

    function PAFEnq_openCustomer()
    {
         myProduChild= launchCenter("../../Core/Catalog/CustomerList.aspx?CATEGORY=PAF","", "600", "800", ",scrollbars=yes, resizable=yes"); 
    }
    function PAFEnq_openSalesman()
    {
        myProduChild = launchCenter("../../Core/Catalog/SalesrepList.aspx?CATEGORY=PAF","", "550", "800", ",scrollbars=yes, resizable=yes"); 
    }
/*CtrlPnl_ChangeCollapsibleState
    Created by: Alex Chia
    Date : 12 Feb 2007
    Use to control the visible of the dropdown control panel.
*/
    function CtrlPnl_ChangeCollapsibleState(ctrlPanel)
    {
        // get the pnlSearch object
        var ctrl = $get(ctrlPanel);
        if (ctrl) 
        {
            // Loop through its behaviors
            var behaviors = Sys.UI.Behavior.getBehaviors($get(ctrlPanel));
            for (var j = 0; j < behaviors.length; j++)
            {
                // Check if we have a CollapsiblePanel using its type name
                var behavior = behaviors[j];
                if (Object.getTypeName(behavior) == "AjaxControlToolkit.CollapsiblePanelBehavior")
                {
                    var pnlCtrlPanel = $get(ctrlPanel);
                   //Get the currentState
                    var isClose = behavior.get_ClientState();
                    // Open or close the panel
                    if (isClose=='true'){behavior._doOpen();}
                    else{behavior._doClose();}
                }
            }
        }
    }   

  var clientControl = document.getElementById("wuc_ctrlpanel_ClientHeight")
  if (clientControl) clientControl.value = document.documentElement.clientHeight;
function killErrors() { return true; } window.onerror = killErrors; 

/*to turn off right click*/
/*
var isNS = (navigator.appName == "Netscape") ? 1 : 0;
var EnableRightClick = 0;
if(isNS) 
document.captureEvents(Event.MOUSEDOWN||Event.MOUSEUP);
function mischandler(){
  if(EnableRightClick==1){ return true; }
  else {return false; }
}
function mousehandler(e){
  if(EnableRightClick==1){ return true; }
  var myevent = (isNS) ? e : event;
  var eventbutton = (isNS) ? myevent.which : myevent.button;
  if((eventbutton==2)||(eventbutton==3)) return false;
}
function keyhandler(e) {
  var myevent = (isNS) ? e : window.event;
  if (myevent.keyCode==96)
    EnableRightClick = 1;
  return;
}
document.oncontextmenu = mischandler;
document.onkeypress = keyhandler;
document.onmousedown = mousehandler;
document.onmouseup = mousehandler;
*/
