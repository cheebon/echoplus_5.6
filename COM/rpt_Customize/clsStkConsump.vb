﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsStkConsump
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsStkConsump"
        End Get
    End Property


    Public Function GetStkConsumpByBiMonthly(ByVal strYear As String, ByVal strSalesrepList As String, ByVal strPrdList As String, _
   ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_LIST", strPrdList, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpByBiMonthly :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function


    Public Function GetStkConsumpByBiMonthlyDtl(ByVal strYear As String, ByVal strSalesrepList As String, ByVal strPrdList As String, _
        ByVal strBiMonth As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_DTL"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_LIST", strPrdList, clsDB.DataType.DBString, True)
                .addItem("BI_WEEK_NO", strBiMonth, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpByBiMonthlyDtl :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function


    Public Function GetStkConsumpByQuarter(ByVal strYear As String, ByVal strSalesrepList As String, ByVal strPrdList As String, _
  ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_QUARTER"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_LIST", strPrdList, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpByQuarter :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function


    Public Function GetStkConsumpByBiQuarterDtl(ByVal strYear As String, ByVal strSalesrepList As String, ByVal strPrdList As String, _
        ByVal strQuarter As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_QUARTER_DTL"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_LIST", strPrdList, clsDB.DataType.DBString, True)
                .addItem("QUARTER_NO", strQuarter, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpByBiQuarterDtl :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetStkConsumpPrdList(ByVal strSalesrepList As String, ByVal strPrincipalId As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_GET_PRDLIST_MULTI"
                .addItem("SALESREP_CODE ", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRINCIPAL_ID", strPrincipalId, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpPrdList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function


    Public Function GetStkConsumpSelectedPrdList(ByVal strPrdList As String, ByVal strPrincipalId As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_GET_SELECTED_PRD"
                .addItem("PRD_CODE ", strPrdList, clsDB.DataType.DBString, True)
                .addItem("PRINCIPAL_ID", strPrincipalId, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpSelectedPrdList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetStkConsumpSelectedSalesrepList(ByVal strSrList As String, ByVal strPrincipalId As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_GET_SELECTED_SALESREP"
                .addItem("SALESREP_CODE ", strSrList, clsDB.DataType.DBString, True)
                .addItem("PRINCIPAL_ID", strPrincipalId, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpSelectedSalesrepList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetStkConsumpSelectedTeamList(ByVal strTeamList As String, ByVal strPrincipalId As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_GET_SELECTED_TEAM"
                .addItem("TEAM_CODE  ", strTeamList, clsDB.DataType.DBString, True)
                .addItem("PRINCIPAL_ID", strPrincipalId, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpSelectedTeamList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function



    Public Function GetStkConsumpByQuadMonthly(ByVal strYear As String, ByVal strSalesrepList As String, ByVal strPrdList As String, _
ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_QUAD"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_LIST", strPrdList, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpByBiMonthly :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function


    Public Function GetStkConsumpByQuadMonthlyDtl(ByVal strYear As String, ByVal strSalesrepList As String, ByVal strPrdList As String, _
        ByVal strQuadMonth As String, ByVal strUserID As String) As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_STK_CONSUMP_QUAD_DTL"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_LIST", strPrdList, clsDB.DataType.DBString, True)
                .addItem("QUAD_WEEK_NO", strQuadMonth, clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetStkConsumpByBiMonthlyDtl :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function


#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
