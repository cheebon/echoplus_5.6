Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_Plan_PlanDept
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection


    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountSelectedDept() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSelectedDept"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSelectedDept") = value
        End Set
    End Property

    Private Property Master_Row_CountNewDept() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountNewDept"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountNewDept") = value
        End Set
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Assign Contact Department" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControlSelectedDept.Enabled = True
            TimerControlNewDept.Enabled = True
        End If
    End Sub


#Region "DGLISTSelectedDept"
    Protected Sub TimerControlSelectedDept_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlSelectedDept.Tick
        If TimerControlSelectedDept.Enabled Then RefreshDatabindingSelectedDept()
        TimerControlSelectedDept.Enabled = False
    End Sub

    Public Sub RenewDataBindSelectedDept()
        RefreshDataBindSelectedDept()
    End Sub

    Public Sub RefreshDataBindSelectedDept()
        RefreshDatabindingSelectedDept()
    End Sub

    Public Sub RefreshDatabindingSelectedDept(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableSelectedDept As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionSelectedDept As String = CType(ViewState("strSortExpressionSelectedDept"), String)


        dtCurrentTableSelectedDept = GetRecListSelectedDept()

        If dtCurrentTableSelectedDept Is Nothing Then
            dtCurrentTableSelectedDept = New DataTable
        Else
            If dtCurrentTableSelectedDept.Rows.Count = 0 Then
                Master_Row_CountSelectedDept = 0
                btncheckalldelete.Visible = False
                btnuncheckalldelete.Visible = False
                btndelete.Visible = False
                'dtCurrentTableSelectedCont.Rows.Add(dtCurrentTableSelectedCont.NewRow())
            Else

                Master_Row_CountSelectedDept = dtCurrentTableSelectedDept.Rows.Count
                btncheckalldelete.Visible = True
                btnuncheckalldelete.Visible = True
                btndelete.Visible = True
            End If
        End If

        Dim dvCurrentViewSelectedDept As New DataView(dtCurrentTableSelectedDept)
        If Not String.IsNullOrEmpty(strSortExpressionSelectedDept) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionSelectedDept.Replace(" DESC", "")
            dvCurrentViewSelectedDept.Sort = IIf(dtCurrentTableSelectedDept.Columns.Contains(strSortExpressionSelectedDept), strSortExpressionSelectedDept, "")
        End If

        With dglistSelectedDept
            .DataSource = dvCurrentViewSelectedDept
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountSelectedDept > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateSelectedDept()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListSelectedDept() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strUserId As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonDept(strSalesrepCode, strRouteDate, strCustCode, strUserId)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateSelectedDept()

        UpdateSelectedDept.Update()

    End Sub

    Protected Sub dglistSelectedDept_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedDept.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedDept > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListSelectedDept_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistSelectedDept.Sorting
        Dim strSortExpressionSelectedDept As String = ViewState("strSortExpressionSelectedDept")

        If strSortExpressionSelectedDept IsNot Nothing AndAlso strSortExpressionSelectedDept.Length > 0 Then
            If strSortExpressionSelectedDept Like (e.SortExpression & "*") Then
                If strSortExpressionSelectedDept.IndexOf(" DESC") > 0 Then
                    strSortExpressionSelectedDept = e.SortExpression
                Else
                    strSortExpressionSelectedDept = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionSelectedDept = e.SortExpression
            End If
        Else
            strSortExpressionSelectedDept = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionSelectedDept") = strSortExpressionSelectedDept

        RefreshDatabindingSelectedDept()

    End Sub

#End Region

#Region "DGLISTNewCust"
    Protected Sub TimerControlNewDept_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlNewDept.Tick
        If TimerControlNewDept.Enabled Then RefreshDatabindingNewDept()
        TimerControlNewDept.Enabled = False
    End Sub

    Public Sub RenewDataBindNewDept()
        RefreshDataBindNewDept()
    End Sub

    Public Sub RefreshDataBindNewDept()
        RefreshDatabindingNewDept()
    End Sub

    Public Sub RefreshDatabindingNewDept(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableNewDept As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionNewDept As String = CType(ViewState("strSortExpressionNewDept"), String)


        dtCurrentTableNewDept = GetRecListNewDept()

        If dtCurrentTableNewDept Is Nothing Then
            dtCurrentTableNewDept = New DataTable
        Else
            If dtCurrentTableNewDept.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountSelectedDept = 0
                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btnadd.Visible = False
            Else
                Master_Row_CountSelectedDept = dtCurrentTableNewDept.Rows.Count
                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btnadd.Visible = True
            End If
        End If

        Dim dvCurrentViewNewDept As New DataView(dtCurrentTableNewDept)
        If Not String.IsNullOrEmpty(strSortExpressionNewDept) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionNewDept.Replace(" DESC", "")
            dvCurrentViewNewDept.Sort = IIf(dtCurrentTableNewDept.Columns.Contains(strSortExpressionNewDept), strSortExpressionNewDept, "")
        End If

        With dglistNewDept
            .DataSource = dvCurrentViewNewDept
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountNewDept > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateNewDept()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListNewDept() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strCustCode As String, strUserId As String, strRouteDate As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonContByDept(strSalesrepCode, strCustCode, strUserId, strRouteDate)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateNewDept()

        UpdateNewDept.Update()

    End Sub

    Protected Sub dglistNewDept_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistNewDept.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountNewDept > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListNewDept_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistNewDept.Sorting
        Dim strSortExpressionNewDept As String = ViewState("strSortExpressionNewDept")

        If strSortExpressionNewDept IsNot Nothing AndAlso strSortExpressionNewDept.Length > 0 Then
            If strSortExpressionNewDept Like (e.SortExpression & "*") Then
                If strSortExpressionNewDept.IndexOf(" DESC") > 0 Then
                    strSortExpressionNewDept = e.SortExpression
                Else
                    strSortExpressionNewDept = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionNewDept = e.SortExpression
            End If
        Else
            strSortExpressionNewDept = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionNewCust") = strSortExpressionNewDept

        RefreshDatabindingNewDept()

    End Sub

#End Region


#Region "Event Handler"

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        AddNewDept()
        TimerControlSelectedDept.Enabled = True
        TimerControlNewDept.Enabled = True
    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        DelNewDept()
        TimerControlSelectedDept.Enabled = True
        TimerControlNewDept.Enabled = True
    End Sub

#End Region

    Private Sub AddNewDept()
        Try
            If dglistNewDept.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strDeptName As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistNewDept.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistNewDept.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strDeptName = dglistNewDept.DataKeys(i).Item("DEPT_NAME")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.InstPlanAddonDept(strSalesrepCode, strRouteDate, strCustCode, strDeptName, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblNmsg.Text = "Record(s) added !"
            lblsmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelNewDept()
        Try
            If dglistSelectedDept.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strDeptName As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistSelectedDept.Rows
                    chkSelected = CType(DR.FindControl("chkDelete"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistSelectedDept.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strDeptName = dglistSelectedDept.DataKeys(i).Item("DEPT_NAME")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.DelPlanAddonDept(strSalesrepCode, strRouteDate, strCustCode, strDeptName, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblSmsg.Text = "Record(s) deleted !"
            lblNmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
