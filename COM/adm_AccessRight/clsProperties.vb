'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/11/2006
'	Purpose	    :	Class AccessRight for Properties 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
'Imports System.ComponentModel

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
'<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    'Inherits ServicedComponent

    Public Class clsAccessRight
        Public accessright_id As Double
        Public accessright_name As String
        Public accessright_name_1 As String
        Public accessright_desc As String
        Public accessright_desc_1 As String
        Public country_id As Double
        Public principal_id As Double
        Public created_date As DateTime
        Public creator_user_id As Double
        Public changed_date As DateTime
        Public changed_user_id As Double
        Public delete_flag As Integer
        Public dtDtl As DataTable
    End Class

    Public Class clsAccessRightDtl
        Public accessrightdtl_id As Double
        Public accessright_id As Double
        Public module_id As Double
        Public submodule_id As Double
        Public action_id As Double
        Public created_date As DateTime
        Public creator_user_id As Double
        Public changed_date As DateTime
        Public changed_user_id As Double
        Public delete_flag As Integer
    End Class

    Public Class clsAccessRightQuery
        Public accessright_id As Double
        Public accessright_name As String
        Public module_id As Double
        Public submodule_id As Double
        Public country_id As Double
        Public country_code As String
        Public principal_id As Double
        Public principal_code As String
        Public search_type As String
        Public search_value As String
        Public search_value_1 As String
        Public delete_flag As Integer
        Public type As Integer
        Public user_code As String  'HL:20070622
    End Class
End Class
