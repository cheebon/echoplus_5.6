
Partial Class include_wuc_lblDate
    Inherits System.Web.UI.UserControl

    Private _strCurrentFormat As String '= "dd/MM/yyyy HH:mm:ss"
    Private _strDefaultDateTimeFormatString As String = "yyyy-MM-dd HH:mm:ss"
    Private _strDefaultDateFormatString As String = "yyyy-MM-dd"
    Private _ControlType As DateTimeControlType = DateTimeControlType.DateAndTime 'default
    Private _dtmSelectedDateTime As DateTime = Now

    Enum DateTimeControlType
        DateAndTime = 0
        DateOnly = 1
    End Enum

    Public Property FormatString() As String
        Get
            If String.IsNullOrEmpty(_strCurrentFormat) Then
                If ControlType = DateTimeControlType.DateOnly Then
                    _strCurrentFormat = _strDefaultDateFormatString
                Else
                    _strCurrentFormat = _strDefaultDateTimeFormatString
                End If
            End If
            Return _strCurrentFormat
        End Get
        Set(ByVal value As String)
            If String.IsNullOrEmpty(value) Then
                _strCurrentFormat = value
            Else
                _strCurrentFormat = String.Empty
            End If
        End Set
    End Property

    Public Property SelectedDateTime() As DateTime
        Get
            Return _dtmSelectedDateTime
        End Get
        Set(ByVal dtmvalue As DateTime)
            If Not dtmvalue = Nothing Then
                _dtmSelectedDateTime = dtmvalue
                'Text = dtmvalue.ToString(FormatString)
                lblDate.Text = dtmvalue.ToString(FormatString)
            End If
        End Set
    End Property

    Public Property ControlType() As DateTimeControlType
        Get
            Return _ControlType
        End Get
        Set(ByVal value As DateTimeControlType)
            _ControlType = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return lblDate.Text
        End Get
        Set(ByVal Value As String)
            If Not String.IsNullOrEmpty(Value) AndAlso IsDate(Value) Then
                Try
                    Dim dtm As Date = Date.Parse(Value)
                    lblDate.Text = dtm.ToString(FormatString)
                Catch ex As Exception
                End Try
            Else
                lblDate.Text = Value
            End If
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Set Default DateTime String Format
            If String.IsNullOrEmpty(_strCurrentFormat) Then
                If ControlType = DateTimeControlType.DateOnly Then
                    _strCurrentFormat = _strDefaultDateFormatString
                Else
                    _strCurrentFormat = _strDefaultDateTimeFormatString
                End If
            End If

        Catch ex As Exception
            ExceptionMsg("wuc_lblDate.Page_Load : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Sub ExceptionMsg
    ' Purpose	:	This function will add the error message and pop up the message viewer
    ' Parameters:	[in]  : 
    '		        [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        'Call error Message Viewer
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg
            'ltlMsg.Text = "alert('" & strMsg & "');"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>alert('" & strMsg & "');</script>")

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing
        Catch ex As Exception

        Finally
        End Try
    End Sub
End Class
