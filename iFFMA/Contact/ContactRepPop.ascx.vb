Imports System.Data

Partial Class iFFMA_Contact_ContactRepPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(hdContCode.Value)
        End Get
        Set(ByVal value As String)
            hdContCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtDepartment As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)
            If ddlStatus IsNot Nothing Then
                ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS_CODE")), "", DT.Rows(0)("STATUS_CODE"))
            End If

            Dim ddlDepartment As DropDownList = CType(DetailsView1.FindControl("ddlDepartment"), DropDownList)
            If ddlDepartment IsNot Nothing Then
                dtDepartment = clsCommon.GetDepartmentDDL
                With ddlDepartment
                    .Items.Clear()
                    .DataSource = dtDepartment.DefaultView
                    .DataTextField = "DEPARTMENT_NAME"
                    .DataValueField = "DEPARTMENT_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("DEPARTMENT_CODE")), "", DT.Rows(0)("DEPARTMENT_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadSpecialtyDDL(ByVal DT As DataTable)
        Dim dtSpecialityName As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)
            If ddlStatus IsNot Nothing Then
                ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS_CODE")), "", DT.Rows(0)("STATUS_CODE"))
            End If

            Dim ddlSpecialityName As DropDownList = CType(DetailsView1.FindControl("ddlSpecialityName"), DropDownList)
            If ddlSpecialityName IsNot Nothing Then
                dtSpecialityName = clsCommon.GetContSpecialtyDDL(ContCode)
                With ddlSpecialityName
                    .Items.Clear()
                    .DataSource = dtSpecialityName.DefaultView
                    .DataTextField = "SPECIALTY_NAME"
                    .DataValueField = "SPECIALTY_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("SPECIALITY_NAME")), "", DT.Rows(0)("SPECIALITY_NAME"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadClassDDL(ByVal DT As DataTable)

        Try

            Dim ddlClass As DropDownList = CType(DetailsView1.FindControl("ddlClass"), DropDownList)
            If ddlClass IsNot Nothing Then
                With ddlClass
                    .Items.Clear()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .Items.Insert(1, New ListItem("A", "A"))
                    .Items.Insert(2, New ListItem("B", "B"))
                    .Items.Insert(3, New ListItem("C", "C"))
                    .Items.Insert(4, New ListItem("OTH", "OTH"))
                    .SelectedIndex = 0
                    .SelectedValue = Portal.Util.GetValue(Of String)(DT.Rows(0)("CLASS"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsContact As New mst_Contact.clsContact

            DT = clsContact.GetContRepDetails(ContCode, SalesrepCode, CustCode)
            If DT.Rows.Count > 0 Then
                'If DT.Rows(0)("LOCK_FLAG") <> "0" Then
                '    RaiseEvent PopLockAlert()
                '    Exit Sub
                'End If
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            'Dim lblContCode As Label = CType(DetailsView1.FindControl("lblContCode"), Label)
            'lblContCode.Text = ContCode

            If DT IsNot Nothing Then
                LoadDDL(DT)
                LoadSpecialtyDDL(DT)
                LoadClassDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim lblSalesrepCode As Label = CType(DetailsView1.FindControl("lblSalesrepCode"), Label)
            Dim lblCustCode As Label = CType(DetailsView1.FindControl("lblCustCode"), Label)
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)
            Dim ddlSpecialityName As DropDownList = CType(DetailsView1.FindControl("ddlSpecialityName"), DropDownList)
            Dim ddlDepartment As DropDownList = CType(DetailsView1.FindControl("ddlDepartment"), DropDownList)
            Dim txtAsstName As TextBox = CType(DetailsView1.FindControl("txtAsstName"), TextBox)
            Dim txtPriorityName As TextBox = CType(DetailsView1.FindControl("txtPriorityName"), TextBox)
            Dim txtVisitFreq As TextBox = CType(DetailsView1.FindControl("txtVisitFreq"), TextBox)
            Dim txtPersonalInterest As TextBox = CType(DetailsView1.FindControl("txtPersonalInterest"), TextBox)
            Dim txtMedInterest As TextBox = CType(DetailsView1.FindControl("txtMedInterest"), TextBox)
            Dim txtMktgPref As TextBox = CType(DetailsView1.FindControl("txtMktgPref"), TextBox)
            Dim txtXfield1 As TextBox = CType(DetailsView1.FindControl("txtXfield1"), TextBox)
            Dim txtXfield2 As TextBox = CType(DetailsView1.FindControl("txtXfield2"), TextBox)
            Dim txtNotes As TextBox = CType(DetailsView1.FindControl("txtNotes"), TextBox)
            Dim ddlClass As DropDownList = CType(DetailsView1.FindControl("ddlClass"), DropDownList)

            Dim clsContact As New mst_Contact.clsContact

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then

                clsContact.UpdateContRep(ContCode, Trim(lblSalesrepCode.Text), Trim(lblCustCode.Text), Trim(ddlStatus.SelectedValue), _
                                        Trim(ddlSpecialityName.SelectedValue), Trim(ddlDepartment.SelectedValue), Trim(txtAsstName.Text), _
                                        Trim(txtPriorityName.Text), IIf(Trim(txtVisitFreq.Text) = "", 0, Trim(txtVisitFreq.Text)), Trim(txtPersonalInterest.Text), Trim(txtMedInterest.Text), _
                                        Trim(txtMktgPref.Text), Trim(txtXfield1.Text), Trim(txtXfield2.Text), Trim(txtNotes.Text), Trim(ddlClass.SelectedValue))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class