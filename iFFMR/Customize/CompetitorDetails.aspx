﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CompetitorDetails.aspx.vb" Inherits="iFFMR_Customize_CompetitorDetails" %>

<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CompetitorDetails</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../include/jquery-1.10.2.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            
        });
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCompetitorDetails" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">

                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px"></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table id="Table1" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress2" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Button ID="btnBack" runat="server" CssClass="cls_button" PostBackUrl="~/iFFMR/Customize/Competitor.aspx"
                                                                        ToolTip="Back To Previous Page." Text="Back"></asp:Button>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="455" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    RowHighlightColor="AntiqueWhite">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                                    <%--<ccGV:clsGridView ID="dgList" runat="server" ShowFooter="True" AllowSorting="True"
                                                                        AutoGenerateColumns="True" Width="98%" FreezeHeader="True" GridHeight="455"
                                                                        AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                                        FreezeRows="0" GridWidth="">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
    
</body>
</html>
