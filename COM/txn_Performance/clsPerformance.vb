'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	8/12/2006
'	Purpose	    :	Class to build datatables for getting report details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPerformance
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsReport"
        End Get
    End Property


    Function GetTgtDurationDDl(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERF_TGTDURATION"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTgtDurationDDl :" & ex.Message))
        End Try
    End Function

    Function GetPerfHdr(ByVal strSalesrepCode As String, ByVal strTgtDuration As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERF_HDR"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("TGT_DURATION", strTgtDuration, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPerfHdr :" & ex.Message))
        End Try
    End Function

    Function GetPerfDtl(ByVal strSalesrepCode As String, ByVal strTgtDuration As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERF_DTL"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("TGT_DURATION", strTgtDuration, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPerfDtl :" & ex.Message))
        End Try
    End Function


#Region "OLDSPP"

    Function getTargetDurDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERFORMANCEDURATION"
                .addItem("salesmancode", strSalesmanCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getTargetDurDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getDefaultDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERFORMANCEDEFAULT"
                .addItem("salesmancode", strSalesmanCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDefaultDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getSelectedDurationDT(ByVal strSalesmanCode As String, ByVal strDuration As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERFORMANCESELECTDURATION"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("duration", strDuration, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSelectedDurationDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getHeaderDT(ByVal strSalesmanCode As String, ByVal strDuration As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERFORMANCEHEADER"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("duration", strDuration, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getHeaderDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getDetailDT(ByVal strSalesmanCode As String, ByVal strDuration As String, ByVal strProdCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PERFORMANCEDETAIL"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("duration", strDuration, 2)
                .addItem("prodcode", strProdCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getDetailDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
#End Region

#Region "Exception"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region


End Class
