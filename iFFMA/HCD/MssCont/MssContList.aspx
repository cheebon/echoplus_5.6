<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MssContList.aspx.vb" Inherits="iFFMA_HCD_MssCont_MssContHdr" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_pnlStdBar" Src="~/include/wuc_pnlStdBar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSS Contact Update</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');ResizeFrameHeight('ContentBarIframe',600);">
    <form id="frmMssContUpd" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_pnlStdBar ID="wuc_pnlStdBar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                                ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Mss Contact Header List">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                Width="98%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="TXN_NO,SALESREP_CODE,CUST_CODE,CONT_CODE,TITLE_CODE">
                                                                <Columns>
                                                                    <%--<asp:ButtonField CommandName="Select" Text="<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                                        HeaderText="Edit">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:ButtonField>--%>
                                                                    <asp:TemplateField HeaderText="Edit">
                                                                        <itemstyle horizontalalign="Center" />
                                                                        <itemtemplate><asp:ImageButton ID="btnedit" runat="server" Visible="TRUE" CommandName="Select" src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'></asp:ImageButton>
                                                                        </itemtemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TXN_NO" HeaderText="Txn No" ReadOnly="True" SortExpression="TXN_NO">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TXN_DATE" HeaderText="Txn Date" ReadOnly="True" SortExpression="TXN_DATE">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="SALESREP_CODE" HeaderText="Salesrep Code" ReadOnly="True"
                                                                        SortExpression="SALESREP_CODE">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="SALESREP_NAME" HeaderText="Salesrep Name" ReadOnly="True"
                                                                        SortExpression="SALESREP_NAME">
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code" ReadOnly="True"
                                                                        SortExpression="CUST_CODE">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True"
                                                                        SortExpression="CUST_NAME">
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CONT_CODE" HeaderText="Contact Code" ReadOnly="True" SortExpression="CONT_CODE">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CONT_NAME" HeaderText="Contact Name" ReadOnly="True" SortExpression="CONT_NAME">
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TITLE_CODE" HeaderText="Title Code" ReadOnly="True" SortExpression="TITLE_CODE">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TITLE_NAME" HeaderText="Title Name" ReadOnly="True" SortExpression="TITLE_NAME">
                                                                        <itemstyle horizontalalign="Left" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="STATUS" HeaderText="Status" ReadOnly="True" SortExpression="STATUS">
                                                                        <itemstyle horizontalalign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </ccGV:clsGridView>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td style="height: 5px">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
