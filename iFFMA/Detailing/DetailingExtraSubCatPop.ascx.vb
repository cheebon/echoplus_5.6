'************************************************************************
'	Author	    :	YONG SOO FONG
'	Date	    :	26/02/2016
'	Purpose	    :	Detailing Extra Sub Cat Pop
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data
Imports System.IO
Imports System.Web

Partial Class iFFMA_Detailing_DetailingExtraSubCatPop
    Inherits System.Web.UI.UserControl
    Public Event SaveButton_Click As EventHandler
    Public Event ReloadTable_Click As EventHandler
    Public Event ShowMainPop_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty
    Private strPath As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property DetCode() As String
        Get
            Return Trim(hdDetCode.Value)
        End Get
        Set(ByVal value As String)
            hdDetCode.Value = value
        End Set
    End Property

    Public Property SubDetCode() As String
        Get
            Return Trim(hdSubDetCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubDetCode.Value = value
        End Set
    End Property
    Public Property ExtraDetCode() As String
        Get
            Return Trim(hdExtraDetCode.Value)
        End Get
        Set(ByVal value As String)
            hdExtraDetCode.Value = value
        End Set
    End Property

    Public Property DetailingPath() As String
        Get
            Return strPath
        End Get
        Set(ByVal value As String)
            strPath = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance2.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance2.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance1.Update()
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView2.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView2.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView2.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsDetailing As New mst_Detailing.clsDetailing

            DT = clsDetailing.GetDetailingExtraCatDetails(DetCode, SubDetCode, ExtraDetCode)
            If DT.Rows.Count > 0 Then
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView2.DataSource = dvDetailView
            DetailsView2.DataBind()

            Show()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView2_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance2.BehaviorID = ModalPopupMaintenance2.UniqueID
            Else
                lblInfo.Text = ""
            End If

            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If

            Dim scriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
            scriptManager1.RegisterPostBackControl(btnSave)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Page.Validate()
            If Page.IsValid = False Then
                Show()
                Exit Sub
            End If

            Dim txtExtraDetCode As TextBox = CType(DetailsView2.FindControl("txtExtraDetCode"), TextBox)
            Dim lblExtraDetCode As Label = CType(DetailsView2.FindControl("lblExtraDetCode"), Label)
            Dim txtExtraDetName As TextBox = CType(DetailsView2.FindControl("txtExtraDetName"), TextBox)
            Dim txtPath As TextBox = CType(DetailsView2.FindControl("txtPath"), TextBox)
            Dim clsDetailing As New mst_Detailing.clsDetailing

            If DetailsView2.CurrentMode = DetailsViewMode.Edit Then

                Dim FlUpload As FileUpload = DirectCast(DetailsView2.FindControl("Upload"), FileUpload)

                If (FlUpload.HasFile) Then
                    'KL - 12052016 - comment out checking for file existence (users allowed to create without file upload)
                    'If String.IsNullOrEmpty(Trim(FlUpload.PostedFile.FileName)) Then
                    '    lblInfo.Text = "Please select a file!"
                    '    LoadDvInsertMode()

                    '    RaiseEvent ShowMainPop_Click(sender, e)
                    '    Show()
                    '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "UpdateMsg", "function fireResize() {if (document.createEvent) {var ev = document.createEvent('Event');ev.initEvent('resize', true, true);window.dispatchEvent(ev);}else {element = document.documentElement;var event = document.createEventObject();element.fireEvent('onresize', event);}}; $( document ).ready(function() {window.setTimeout(fireResize,0);});", True)
                    '    Exit Sub
                    'Else
                    If Not String.IsNullOrEmpty(Trim(FlUpload.PostedFile.FileName)) Then
                        If Not (FlUpload.PostedFile.ContentType.Contains("image/") Or _
                              FlUpload.PostedFile.ContentType.Contains("video/") Or _
                              FlUpload.PostedFile.ContentType.Contains("audio/") Or _
                              FlUpload.PostedFile.ContentType = "text/plain" Or _
                              FlUpload.PostedFile.ContentType = "text/html" Or _
                              FlUpload.PostedFile.ContentType = "application/vnd.ms-excel" Or _
                              FlUpload.PostedFile.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" Or _
                              FlUpload.PostedFile.ContentType = "application/vnd.ms-powerpoint" Or _
                              FlUpload.PostedFile.ContentType = "application/pdf" Or _
                              FlUpload.PostedFile.ContentType = "application/msword" Or _
                              FlUpload.PostedFile.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document" Or _
                              FlUpload.PostedFile.ContentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation") Then
                            lblInfo.Text = "File type not supported! Please select another file."
                            LoadDvInsertMode()
                            RaiseEvent ShowMainPop_Click(sender, e)
                            Show()
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "UpdateMsg", "function fireResize() {if (document.createEvent) {var ev = document.createEvent('Event');ev.initEvent('resize', true, true);window.dispatchEvent(ev);}else {element = document.documentElement;var event = document.createEventObject();element.fireEvent('onresize', event);}}; $( document ).ready(function() {window.setTimeout(fireResize,0);});", True)
                            Exit Sub
                        End If
                        UpLoadFile(FlUpload) 'Upload File
                    End If
                    'clsDetailing.UpdateDetailingExtraCat(DetCode, Trim(lblExtraDetCode.Text), Trim(txtExtraDetName.Text), "", "")
                    clsDetailing.UpdateDetailingExtraCat(DetCode, SubDetCode, Trim(lblExtraDetCode.Text), Trim(txtExtraDetName.Text), DetailingPath)
                    lblInfo.Text = "The record is successfully saved."
                    LoadDvViewMode()
                    'Show()
                Else
                    clsDetailing.UpdateDetailingExtraCat(DetCode, SubDetCode, Trim(lblExtraDetCode.Text), Trim(txtExtraDetName.Text), Trim(txtPath.Text))
                    lblInfo.Text = "The record is successfully saved."
                    LoadDvViewMode()
                End If


            Else
                'Add'
                Dim FlUpload As FileUpload = DirectCast(DetailsView2.FindControl("Upload"), FileUpload)

                'KL - 12052016 - comment out checking for file existence (users allowed to create without file upload)
                'If String.IsNullOrEmpty(Trim(FlUpload.PostedFile.FileName)) Then
                '    lblInfo.Text = "Please select a file!"
                '    LoadDvInsertMode()

                '    RaiseEvent ShowMainPop_Click(sender, e)
                '    Show()
                '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "UpdateMsg", "function fireResize() {if (document.createEvent) {var ev = document.createEvent('Event');ev.initEvent('resize', true, true);window.dispatchEvent(ev);}else {element = document.documentElement;var event = document.createEventObject();element.fireEvent('onresize', event);}}; $( document ).ready(function() {window.setTimeout(fireResize,0);});", True)
                '    Exit Sub
                'Else
                If Not String.IsNullOrEmpty(Trim(FlUpload.PostedFile.FileName)) Then
                    If Not (FlUpload.PostedFile.ContentType.Contains("image/") Or _
                          FlUpload.PostedFile.ContentType.Contains("video/") Or _
                          FlUpload.PostedFile.ContentType.Contains("audio/") Or _
                          FlUpload.PostedFile.ContentType = "text/plain" Or _
                          FlUpload.PostedFile.ContentType = "text/html" Or _
                          FlUpload.PostedFile.ContentType = "application/vnd.ms-excel" Or _
                          FlUpload.PostedFile.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" Or _
                          FlUpload.PostedFile.ContentType = "application/vnd.ms-powerpoint" Or _
                          FlUpload.PostedFile.ContentType = "application/pdf" Or _
                          FlUpload.PostedFile.ContentType = "application/msword" Or _
                          FlUpload.PostedFile.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document" Or _
                          FlUpload.PostedFile.ContentType = "application/vnd.openxmlformats-officedocument.presentationml.presentation") Then
                        lblInfo.Text = "File type not supported! Please select another file."
                        LoadDvInsertMode()
                        RaiseEvent ShowMainPop_Click(sender, e)
                        Show()
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "UpdateMsg", "function fireResize() {if (document.createEvent) {var ev = document.createEvent('Event');ev.initEvent('resize', true, true);window.dispatchEvent(ev);}else {element = document.documentElement;var event = document.createEventObject();element.fireEvent('onresize', event);}}; $( document ).ready(function() {window.setTimeout(fireResize,0);});", True)
                        Exit Sub
                    End If
                    UpLoadFile(FlUpload) 'Upload File
                End If

                Dim DT As DataTable
                Dim strIsDeleted As String = ""
                DT = clsDetailing.CreateDetailingExtraCat(DetCode, SubDetCode, Trim(txtExtraDetCode.Text), Trim(txtExtraDetName.Text), DetailingPath)

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                    strIsDeleted = DT.Rows(0)("IS_DELETED")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Extra Category Code already exists!"
                    ExtraDetCode = ""
                    txtExtraDetCode.Text = ""
                    LoadDvInsertMode()

                    'have to trigger popup's parent's Show() via RaiseEvent in order to display this popup (child)
                    RaiseEvent ShowMainPop_Click(sender, e)

                    Show()

                    'KL 01042016 - for original code without triggering resize() the popup will have issues
                    'Thus, on document ready, triggers the window resize event to "readjust" the popup to center
                    'This should be a common issue when having multiple popups (parents and children)
                    'Execute script for now, to be replaced with better code in future if any
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "UpdateMsg", "function fireResize() {if (document.createEvent) {var ev = document.createEvent('Event');ev.initEvent('resize', true, true);window.dispatchEvent(ev);}else {element = document.documentElement;var event = document.createEventObject();element.fireEvent('onresize', event);}}; $( document ).ready(function() {window.setTimeout(fireResize,0);});", True)
                    Exit Sub
                ElseIf strIsDeleted = "Y" Then
                    lblInfo.Text = "This Extra Category Code is used before, please create a new one!"
                    ExtraDetCode = ""
                    txtExtraDetCode.Text = ""
                    LoadDvInsertMode()
                    'have to trigger popup's parent's Show() via RaiseEvent in order to display this popup (child)
                    RaiseEvent ShowMainPop_Click(sender, e)

                    Show()

                    'Refer to comment above
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "UpdateMsg", "function fireResize() {if (document.createEvent) {var ev = document.createEvent('Event');ev.initEvent('resize', true, true);window.dispatchEvent(ev);}else {element = document.documentElement;var event = document.createEventObject();element.fireEvent('onresize', event);}}; $( document ).ready(function() {window.setTimeout(fireResize,0);});", True)
                    Exit Sub
                Else
                    ExtraDetCode = Trim(txtExtraDetCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    LoadDvViewMode()
                End If
            End If

                'BindDetailsView()                       'Rebind the details view

                RaiseEvent ReloadTable_Click(sender, e)  'Renew the main listing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "Upload File"
    Private Sub UpLoadFile(ByVal FlUpload As FileUpload)
        Dim UploadFile As HttpPostedFile
        Dim strFileNameExt As String

        'KL - 01042016 - Avoid using HasFile as it doesn't recognize 0kb files
        'If FlUpload.HasFile Then

        UploadFile = FlUpload.PostedFile

        Dim strPostedFileName As String = UploadFile.FileName

        If (strPostedFileName <> String.Empty) Then
            Dim strExtn As String = System.IO.Path.GetExtension(strPostedFileName).ToLower

            Dim strRootPath As String = "/Documents/Root/" & CStr(Web.HttpContext.Current.Session("CURRENT_PRINCIPAL_CODE")) & "/Detailing/" & DetCode + "/"
            Dim strDetailingPath As String = ""

            '// Get FileName without Extension
            strFileNameExt = System.IO.Path.GetFileName(strPostedFileName).Replace(strExtn, "")
            '// Get FileName + Username + DateTime + Extension
            strFileNameExt = strFileNameExt

            If Not Directory.Exists(Server.MapPath("~" & strRootPath)) Then
                Directory.CreateDirectory(Server.MapPath("~" & strRootPath))
            End If

            'FlUpload.SaveAs(Server.MapPath("~" & strRootPath) & strPostedFileName)
            FlUpload.SaveAs(Server.MapPath("~" & strRootPath) & strFileNameExt & strExtn)
            'strPath = strPostedFileName
            strPath = strFileNameExt & strExtn

            lblInfo.Text = "Document uploaded successfully..."
        Else
            lblInfo.Text = "There is no file to Upload."
        End If

    End Sub

#End Region


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class



