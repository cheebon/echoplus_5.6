<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnTRA.aspx.vb" Inherits="iFFMS_Customer_CustTxn_TxnTRA"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Prd_PrgGrpSearch" Src="~/iFFMS/COMMON/wuc_Prd_PrgGrpSearch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Transaction TRA</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        //ValidateProduct
        function ValidateProduct() {
            $("#lbluompricedisplay").html('Loading'); $("#txtPrdName").val('Loading'); $("#lbluomload").html('Loading');
            if ($("#txtPrdCode").val()) {
                ws_CustTxn.ValidateProduct($("#txtPrdCode").val(), getQueryVariable('salesrepcode'), onValidateProductLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
            } else { $("#txtPrdName").val(''); $("#lbluomload").html(''); $("#lbluomprice").val(''); bindDDLUOM(); }
        }
        
        function onValidateProductLoadSuccess(result) {
            if (result.Total > 0) {
                $("#txtPrdName").val(result.Rows[0]['PRD_NAME']); bindDDLUOM();
            }
            else
            { PrdSearchPopup_Show(); $("#txtPrdCode").val(''); $("#txtPrdName").val('Product information not found!'); }
        }
        //ValidateProduct


        //ddluom
        function bindDDLUOM() {
            $("#lbluompricedisplay").html('Loading');
            ws_CustTxn.GetUOMCode($("#txtPrdCode").val(), onUOMLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
        }

        function onUOMLoadSuccess(result) {
            $("#ddluom").find('option').remove().end()
            for (var idx = 0; idx < result.Total; idx++) {
                addDropDownValue($("#ddluom"), result.Rows[idx]["UOM_CODE"], result.Rows[idx]["UOM_PRICE"]);
            }
            if (result.Total > 0) { $("select#ddluom").get(0).selectedIndex = 0; UOMPrice(); }
            $("#lbluomload").html('');
        }
        
        function addDropDownValue(ddlCtrl, text, value) {
            ddlCtrl.append($(document.createElement("option")).attr("value", value).text(text));
        }
        //ddluom

        function UOMPrice() {
            $("#lbluomprice").val($("#ddluom option:selected").val());
            if ($("select#ddluom").get(0).selectedIndex == -1) { $("#txtselecteduom").val(0); } else { $("#txtselecteduom").val($("#ddluom option:selected").text()); }
        }

        //SearchProduct
        function SearchProduct() {
            $("#message").css('display', 'none');
            if ($("#txtPrdCode").val()) {
                ws_CustTxn.ValidateProduct($("#txtPrdCode").val(), getQueryVariable('salesrepcode'), onSearchProductLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
            } else { PrdSearchPopup_Show(); }
        }

        function onSearchProductLoadSuccess(result) {
            if (result.Total > 0) {
                alert('Product found!');
            } else { alert('Product not found!'); PrdSearchPopup_Show(); }
        }
        //SearchProduct

        function CalculateAmt() {
            var amt = ($("#txtqty").val() * $("#txtprice").val()).toFixed(2);
            $("#lblamount").val(amt);
        }

        function EnterToTab() { var keycode = window.event.keyCode; if (keycode == 13) { window.event.keyCode = 9; } }

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) { return pair[1]; }
            }
        }

        function ValidateCheckBoxStates() {
            var flag = false;
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() {
                if (this.checked == true) flag = true;
            });
            if (flag) { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function ChangeAllCheckBoxStates(element) {
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStates(element) {
            var flag = true;
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() { if (this.checked == false) flag = false; });
            $("#dgList").find("input:checkbox[Id*=DeleteAllCheckBox]").attr('checked', flag);
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe')"
    style="background-color: #DDDDDD;">
    <form id="frmtxntra" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/DataServices/ws_CustTxn.asmx" />
        </Services>
    </ajaxToolkit:ToolkitScriptManager>
    <fieldset style="width: 98%;">
        <div id="title">
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
            <span id="message" style="display: none; float: left;" class="cls_label_err"></span>
        </div>
        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
            width="100%" border="0" style="height: 30px">
            <tr align="left" valign="bottom">
                <td>
                    <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                        CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
            <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Field Force Name</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 300px">
                                        <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Customer Name</span>
                                        <td style="width: 5px">
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td style="width: 300px">
                                            <asp:Label ID="lblCustName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                        </td>
                                        <td style="width: 100px">
                                            <span class="cls_label_header">Contact Name</span>
                                            <td style="width: 5px">
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:Label ID="lblContName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                            </td>
                            </tr>
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Visit ID</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblVisitId" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Map</span>
                                    </td>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </asp:Panel>
        <div id="txntra">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:Button runat="server" ID="btnHidden" Style="display: none;" Text="" Visible="false" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                    <div id="Proce" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                        background-color: Gray">
                        <span style="float: right;">
                            <asp:Button ID="btnsubmit" runat="server" Text="Proceed >>" CssClass="cls_button"
                                ValidationGroup="TRAHdr" Width="80px" TabIndex="19" />
                        </span>
                    </div>
                    <asp:UpdatePanel ID="UPHdr" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <div id="dheader" class="S_DivHeader">
                                General Information
                            </div>
                            <asp:Panel ID="pTRAHdr" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                                    <table class="cls_form_table">
                                        <tr>
                                            <td width="10%">
                                                <span class="cls_label">Txn No:</span>
                                            </td>
                                            <td width="40%">
                                                <asp:Label ID="lbltxnno" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td width="10%">
                                                <span class="cls_label">Collect By: </span>
                                            </td>
                                            <td width="40%">
                                                <asp:DropDownList ID="ddlCollBy" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRAHdr"
                                                    TabIndex="3">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvddlCollBy" runat="server" ControlToValidate="ddlCollBy"
                                                    Display="Dynamic" CssClass="cls_label_err" ErrorMessage="Select coll by" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">Voucher No: </span>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblvouchno" runat="server" Text="" CssClass="cls_label" TabIndex="0"></asp:Label>
                                            </td>
                                            <td>
                                                <span class="cls_label">Reason: </span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlmreason" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRADtl"
                                                    TabIndex="4">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvddlmreason" runat="server" ControlToValidate="ddlmreason"
                                                    Display="Dynamic" CssClass="cls_label_err" ErrorMessage="Select reason" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">Ref No: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtrefNo" runat="server" CssClass="cls_textbox" ValidationGroup="TRADtl"
                                                    MaxLength="15" TabIndex="1"></asp:TextBox>
                                            </td>
                                            <td>
                                                <span class="cls_label">Storage : </span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlstorage" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRADtl"
                                                    TabIndex="5">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvddlstorage" runat="server" ControlToValidate="ddlstorage"
                                                    Display="Dynamic" CssClass="cls_label_err" ErrorMessage="Select storage" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">Remarks: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtremarks" runat="server" CssClass="cls_textbox" Width="300px"
                                                    MaxLength="240" ValidationGroup="TRADtl" TabIndex="2"></asp:TextBox>
                                            </td>
                                            <td>
                                                <span class="cls_label">Carton: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtcarton" runat="server" CssClass="cls_textbox" ValidationGroup="TRADtl"
                                                    TabIndex="6" MaxLength="4"></asp:TextBox>
                                                <asp:CompareValidator ID="cvtxtcarton" runat="server" ControlToValidate="txtcarton"
                                                    ValidationGroup="TRADtl" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                    ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                </asp:CompareValidator>
                                                <asp:RequiredFieldValidator ID="rfvtxtcarton" runat="server" Display="Dynamic" ControlToValidate="txtcarton"
                                                    CssClass="cls_label_err" ErrorMessage="Enter carton no" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpepTRAHdr" runat="server" TargetControlID="pTRAHdr"
                                ExpandControlID="dheader" CollapseControlID="dheader">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdPnlDtl" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <div id="ddtl" class="S_DivHeader">
                                Detail Information - Add New/Edit
                            </div>
                            <asp:Panel ID="pTRADtl" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                                    <table class="cls_form_table">
                                        <tr>
                                            <td align="left" colspan="4">
                                                <asp:Button ID="btnresetdtl" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="false"
                                                    TabIndex="17" Width="80px" ValidationGroup="TRADtl" OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;" />
                                                <asp:Button ID="btnsavedtl" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="TRADtl"
                                                    TabIndex="18" Width="80px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <span class="cls_label">Product Code: </span>
                                                <asp:Label ID="lbllineno" runat="server" Text="0" Visible="false" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="txtPrdCode" runat="server" CssClass="cls_textbox" ValidationGroup="TRADtl"
                                                    MaxLength="45" onBlur="ValidateProduct();" onChange="ValidateProduct();"></asp:TextBox>
                                                <input id="btnSearchPrd" type="button" class="cls_button" value="Search" onclick="SearchProduct();"
                                                    style="width: 80px;" />
                                                <asp:RequiredFieldValidator ID="rfvprdcode" ControlToValidate="txtPrdCode" runat="server"
                                                    Display="Dynamic" ErrorMessage="Select a product" CssClass="cls_label_err" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                            </td>
                                            <td width="10%">
                                                <span class="cls_label">Batch No: </span>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="txtbno" runat="server" CssClass="cls_textbox" ValidationGroup="TRADtl"
                                                    TabIndex="12" MaxLength="45"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">Product Name: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPrdName" runat="server" CssClass="cls_textbox" Width="300px"
                                                    TabIndex="7" MaxLength="95" Enabled="false" ValidationGroup="TRADtl"></asp:TextBox>
                                            </td>
                                            <td>
                                                <span class="cls_label">Expiry Date: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtexpdate" runat="server" CssClass="cls_textbox" ValidationGroup="TRADtl"
                                                    TabIndex="13" MaxLength="10"></asp:TextBox>
                                                <asp:ImageButton ID="imgexpdate" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                                    CssClass="cls_button" CausesValidation="false" />
                                                <ajaxToolkit:CalendarExtender ID="ceexpdate" TargetControlID="txtexpdate" runat="server"
                                                    PopupButtonID="imgexpdate" Animated="false" Format="yyyy-MM-dd" PopupPosition="TopLeft">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="rfvFormatDate" runat="server" Display="Dynamic" ControlToValidate="txtexpdate"
                                                    CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="TRADtl" />
                                                <asp:CompareValidator ID="rfvCheckDataType" runat="server" CssClass="cls_validator"
                                                    ControlToValidate="txtexpdate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                    ErrorMessage=" Invalid Date!" ValidationGroup="TRADtl" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">UOM: </span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddluom" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRADtl"
                                                    OnChange="UOMPrice();" onblur="UOMPrice();" TabIndex="8">
                                                </asp:DropDownList>
                                                <asp:Label ID="lbluomload" runat="server" Text="" CssClass="cls_label" />
                                                <div style="display: none;">
                                                    <asp:TextBox ID="txtselecteduom" runat="server"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="rfvddluom" runat="server" ControlToValidate="txtselecteduom"
                                                    Display="Dynamic" CssClass="cls_label_err" ValidationGroup="TRADtl" ErrorMessage="Select UOM"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <span class="cls_label">Quantity: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtqty" runat="server" CssClass="cls_textbox" onchange='CalculateAmt();'
                                                    TabIndex="14" MaxLength="8" onblur='CalculateAmt();' ValidationGroup="TRADtl"></asp:TextBox>
                                                <asp:CompareValidator ID="cvtxtqty" runat="server" ControlToValidate="txtqty" ValidationGroup="TRADtl"
                                                    Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ErrorMessage="Enter number only with no decimal"
                                                    CssClass="cls_label_header">
                                                </asp:CompareValidator>
                                                <asp:RequiredFieldValidator ID="rfvtxtqty" ControlToValidate="txtqty" runat="server"
                                                    Display="Dynamic" ErrorMessage="Enter quantity" CssClass="cls_label_err" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cvtxtqty2" ControlToValidate="txtqty" Operator="GreaterThan"
                                                    ValueToCompare="0" Type="Double" runat="server" ErrorMessage="Amount equal to 0"
                                                    Display="Dynamic" ValidationGroup="TRADtl" CssClass="cls_label_err"></asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">UOM.PRC: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="lbluomprice" runat="server" Text="" CssClass="cls_textbox" Enabled="false"
                                                    ValidationGroup="TRADtl" TabIndex="9"></asp:TextBox>
                                            </td>
                                            <td>
                                                <span class="cls_label">Price: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtprice" runat="server" CssClass="cls_textbox" onchange='CalculateAmt();'
                                                    TabIndex="15" MaxLength="8" onblur='CalculateAmt();' ValidationGroup="TRADtl"></asp:TextBox>
                                                <asp:CompareValidator ID="cvtxtprice" runat="server" ControlToValidate="txtprice"
                                                    ValidationGroup="TRADtl" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                    ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                </asp:CompareValidator>
                                                <asp:CompareValidator ID="cvlblamount" runat="server" ErrorMessage="Amount must be less then UOM Price"
                                                    ValidationGroup="TRADtl" CssClass="cls_label_err" ControlToValidate="txtprice"
                                                    Display="Dynamic" ControlToCompare="lbluomprice" Operator="LessThanEqual" Type="Double"></asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">Reason: </span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlreason" runat="server" CssClass="cls_dropdownlist" ValidationGroup="TRADtl"
                                                    TabIndex="10">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvddlreason" ControlToValidate="ddlreason" runat="server"
                                                    Display="Dynamic" ErrorMessage="Select a reason" CssClass="cls_label_err" ValidationGroup="TRADtl"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                <span class="cls_label">Amount: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="lblamount" runat="server" Text="0" CssClass="cls_textbox" ValidationGroup="TRADtl"
                                                    TabIndex="16" MaxLength="8" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="cls_label">Remarks: </span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtdetailremarks" runat="server" CssClass="cls_textbox" Width="300px"
                                                    TabIndex="11" MaxLength="240" ValidationGroup="TRADtl"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeTRADtl" runat="server" TargetControlID="pTRADtl"
                                ExpandControlID="ddtl" CollapseControlID="ddtl">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpPnlDtlSum" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <div id="Div1" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                                background-color: Gray">
                                <span style="float: right;">
                                    <asp:Button ID="btnProceeed" runat="server" Text="Proceed >>" CssClass="cls_button"
                                        Width="80px" TabIndex="19" />
                                </span>
                            </div>
                            <div id="divsumm" class="S_DivHeader">
                                Detail Information - View/Delete
                            </div>
                            <asp:Panel ID="pgridview" runat="server" CssClass="cls_panel_header" Width="99.8%">
                                <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                    <asp:Button ID="btndeletesum" runat="server" Text="Delete" CssClass="cls_button"
                                        Visible="false" Width="80px" CausesValidation="false" ValidationGroup="DRCSum"
                                        OnClientClick="return ValidateCheckBoxStates();" />
                                </div>
                                <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                    padding-bottom: 10px;">
                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="100%"
                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TXN_NO,LINE_NO">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="DeleteAllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkdelete" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:CommandField HeaderText="Edit" ShowEditButton="True" ShowHeader="True" EditText="<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:CommandField>
                                            <asp:BoundField DataField="LINE_NO" HeaderText="Txn. No." ReadOnly="True" SortExpression="LINE_NO">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TXN_NO" HeaderText="Line No" ReadOnly="True" SortExpression="TXN_NO">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" ReadOnly="True" SortExpression="PRD_CODE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRD_NAME" HeaderText="Product" ReadOnly="True" SortExpression="PRD_NAME">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BATCH_NO" HeaderText="Batch No" ReadOnly="True" SortExpression="BATCH_NO">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RET_QTY" HeaderText="Quantity" ReadOnly="True" SortExpression="RET_QTY">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LIST_PRICE" HeaderText="List Price" ReadOnly="True" SortExpression="LIST_PRICE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RET_AMT" HeaderText="Amount" ReadOnly="True" SortExpression="RET_AMT">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EXP_DATE" HeaderText="Exp. Date" ReadOnly="True" SortExpression="EXP_DATE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REASON_CODE" HeaderText="Reason Code" ReadOnly="True"
                                                SortExpression="REASON_CODE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UOM_CODE" HeaderText="UOM Code" ReadOnly="True" SortExpression="UOM_CODE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                    </ccGV:clsGridView>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpepgridview" runat="server" TargetControlID="pgridview"
                                ExpandControlID="divsumm" CollapseControlID="divsumm">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="width: 99.5%;">
                        <span style="float: left;">
                            <customToolkit:wuc_Prd_PrgGrpSearch ID="wuc_Prd_PrgGrpSearch" Title="Product Search"
                                runat="server" />
                        </span>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    </form>
</body>
</html>
