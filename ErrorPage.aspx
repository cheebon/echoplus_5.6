<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ErrorPage.aspx.vb" Inherits="ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PageError</title>
</head>
<body style="vertical-align: middle; text-align: center; background-color:#D3E6EA;">
    <form id="form1" runat="server">
    <div style=" width:55%; padding:15px 15px 100px 15px; background-color:#D3E6EA; vertical-align:top;">
        <center><img alt="error" src="images/ErrorPage.png" />&nbsp;</center>
        <center>
            <br />
            &nbsp;</center>
        <div style="padding:15px 15px 15px 15px  ; vertical-align: middle; width: 80%; font-family: Tahoma, Verdana; background-color: #ffcc66; text-align: left" >
        The web application you are attempting to access on this web server is currently unavailable.
        <br />
        <br />
        Please hit the "Refresh" button in your web browser to retry, or contact
        your local administrator.
        </div>
      </div>
    </form>
</body>
</html>
