<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPList.aspx.vb" Inherits="iFFMA_SAP_SAPList" EnableEventValidation="true" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
     <link rel="stylesheet" href='~/include/DKSH.css' />   
     <script src="../../include/datefc.js" type="text/javascript"></script>   
</head>
 <!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmSAPList" runat="server">
    <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
    <div>
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                                <td valign="top" class="Bckgroundreport">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <%--<customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                                <ContentTemplate>--%>
                                                <table cellpadding="1" cellspacing="1" width="60%" class="cls_table">
                                                    <tr>
                                                        <TD width="10%" valign="top"><asp:label ID="lblDocType" Runat="server" CssClass="cls_label_header">Document Type</asp:label></TD>
                                                        <TD width="10%" valign="top"><asp:label ID="lblTeam" Runat="server" CssClass="cls_label_header">Team</asp:label></TD>
                                                        <TD width="10%" valign="top"><asp:label ID="lblSearch" Runat="server" CssClass="cls_label_header">Search By</asp:label></TD>
                                                        <TD valign="top"><asp:label id="lblSearchValue" runat="server" CssClass="cls_label_header"></asp:label></TD>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:dropdownlist id="ddlDocType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="False">
							                                    <asp:ListItem Value="SO">SALES ORDER</asp:ListItem>
							                                    <asp:ListItem Value="TRA">TRADE RETURN</asp:ListItem>
						                                    </asp:dropdownlist>
						                                </td>
						                                <td valign="top">
					                                        <asp:dropdownlist id="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="False">
						                                        <asp:ListItem Value="ALL">All</asp:ListItem>
					                                        </asp:dropdownlist>
					                                    </td>
						                                <td valign="top">						                                    
						                                    <asp:dropdownlist id="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
							                                    <asp:ListItem Value="all">All</asp:ListItem>
							                                    <asp:ListItem Value="REMARKS">Err Reason</asp:ListItem>
							                                    <asp:ListItem Value="LASTUPDATE">Last Update</asp:ListItem>
							                                    <asp:ListItem Value="txn_no">Txn No</asp:ListItem>							                                    
							                                    <asp:ListItem Value="txn_timestamp">Txn Date</asp:ListItem>
							                                    <asp:ListItem Value="txn_status">Txn Status</asp:ListItem>							                                    
							                                    <asp:ListItem Value="VKBUR">Sales Office</asp:ListItem>
							                                    <asp:ListItem Value="salesrep_code">Salesrep Code</asp:ListItem>
							                                    <asp:ListItem Value="STATUS">Status</asp:ListItem>
							                                    <asp:ListItem Value="DOC_NO">Saving No</asp:ListItem>
							                                    <asp:ListItem Value="NOTE">Note</asp:ListItem>
						                                    </asp:dropdownlist>					                                    
						                                   
						                                 </td>
						                                 <td valign="top" nowrap>		
					                                        <table border="0" cellpadding="0" cellspacing="0" width="350px">
					                                            <tr>
					                                                <td nowrap valign="top">
					                                                    <asp:dropdownlist id="ddlList" runat="server" CssClass="cls_dropdownlist"></asp:dropdownlist>
			                                                            <asp:textbox id="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:textbox>
		                                                                <customToolkit:wuc_txtDate ID="Wuc_txtDate1" runat="server" ControlType="DateOnly" />
		                                                            </td>
		                                                            <td width="1%" valign="top"><asp:label id="lblTo" runat="server" CssClass="cls_label_header">To</asp:label></td>
		                                                            <td nowrap valign="top">
		                                                                <asp:textbox id="txtSearchValue1" runat="server" CssClass="cls_textbox"></asp:textbox>
			                                                            <customToolkit:wuc_txtDate ID="Wuc_txtDate2" runat="server" ControlType="DateOnly" />
			                                                        </td>
			                                                        <td valign="top"><asp:button id="btnSearch" runat="server" Text="Search" CssClass="cls_button"></asp:button></td>
					                                            </tr>
					                                        </table>	
		                                                </td>
                                                    </tr>
                                                </table> 
                                                 <%--</ContentTemplate>
						                      </asp:UpdatePanel>            --%>                                    
                                            </td>
                                        </tr>                                                                                
                                    <%--
                                        <tr class="Bckgroundreport">
                                            <td>&nbsp;</td>                                                   
                                        </tr>
                                     <TR>
					                    <TD width="20%" valign="top"><asp:label ID="lblDocType" Runat="server" CssClass="cls_label_header">Document Type</asp:label></TD>
					                    <TD width="2%" valign="top"><asp:label ID="lblDot2" Runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td valign="top"><asp:dropdownlist id="ddlDocType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
							                    <asp:ListItem Value="SO">SALES ORDER</asp:ListItem>
							                    <asp:ListItem Value="TRA">TRADE RETURN</asp:ListItem>
						                    </asp:dropdownlist>
						                </td>
					                    <td valign="top">
					                        <asp:dropdownlist id="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
						                        <asp:ListItem Value="all">All</asp:ListItem>
					                        </asp:dropdownlist>
					                    </td>
				                    </TR>
				                    <TR>
					                    <TD width="20%" valign="top"><asp:label ID="lblTeam" Runat="server" CssClass="cls_label_header">Team</asp:label></TD>
					                    <TD width="2%" valign="top"><asp:label ID="lblDot20" Runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td valign="top"><asp:dropdownlist id="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
							                    <asp:ListItem Value="all">All</asp:ListItem>
						                    </asp:dropdownlist>
						                </td>
				                    </TR>
				                    <TR>
					                    <TD width="20%" valign="top"><asp:label ID="lblSearch" Runat="server" CssClass="cls_label_header">Search By</asp:label></TD>
					                    <TD width="2%" valign="top"><asp:label ID="lblDot1" Runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td valign="top"><asp:dropdownlist id="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
							                    <asp:ListItem Value="all">All</asp:ListItem>
							                    <asp:ListItem Value="REMARKS">Err Reason</asp:ListItem>
							                    <asp:ListItem Value="LASTUPDATE">Last Update</asp:ListItem>
							                    <asp:ListItem Value="txn_no">Txn No</asp:ListItem>
							                    <asp:ListItem Value="txn_timestamp">Txn Date</asp:ListItem>
							                    <asp:ListItem Value="txn_status">Txn Status</asp:ListItem>
							                    <asp:ListItem Value="VKBUR">Sales Office</asp:ListItem>
							                    <asp:ListItem Value="salesrep_code">Salesrep Code</asp:ListItem>
							                    <asp:ListItem Value="STATUS">Status</asp:ListItem>
							                    <asp:ListItem Value="NOTE">Note</asp:ListItem>
						                    </asp:dropdownlist></td>
				                    </TR>
				                    <tr>
					                    <TD valign="top"><asp:label id="lblSearchValue" runat="server" CssClass="cls_label_header"></asp:label></TD>
					                    <TD valign="top"><asp:label id="lblDot" runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td valign="top" nowrap>		
					                        <table border="0" cellpadding="0" cellspacing="0">
					                            <tr>
					                                <td nowrap valign="top">
					                                    <asp:dropdownlist id="ddlList" runat="server" CssClass="cls_dropdownlist"></asp:dropdownlist>
			                                            <asp:textbox id="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:textbox>
		                                                <customToolkit:wuc_txtDate ID="Wuc_txtDate1" runat="server" ControlType="DateOnly" />
		                                            </td>
		                                            <td width="1%" valign="top"><asp:label id="lblTo" runat="server" CssClass="cls_label_header">To</asp:label></td>
		                                            <td nowrap valign="top">
		                                                <asp:textbox id="txtSearchValue1" runat="server" CssClass="cls_textbox"></asp:textbox>
			                                            <customToolkit:wuc_txtDate ID="Wuc_txtDate2" runat="server" ControlType="DateOnly" />
			                                        </td>
			                                        <td valign="top"><asp:button id="btnSearch" runat="server" Text="Search" CssClass="cls_button"></asp:button></td>
					                            </tr>
					                        </table>	
		                                </td>
				                    </tr>--%>
				                    <tr class="Bckgroundreport">                                        
                                        <td colspan="4">&nbsp;</td>                                                                                                                         
                                    </tr>      
                                    <tr>                                      
                                        <td class="Bckgroundreport" colspan="4">
                                            <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress2" runat="server" />                                                
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                                            <ContentTemplate>
                                                <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>
				                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True"
                                                    AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="300px"
                                                    AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                    FreezeRows="0" GridWidth="" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="txn_no" BorderColor="Black" BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                    <Columns>    
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton id="imgResubmit" CommandName="Delete" runat="server" ImageUrl="~/images/icoCopy.gif" CausesValidation="False" AlternateText="Resubmit" ImageAlign="Middle" visible="False"></asp:ImageButton>
                                                                <cc1:ConfirmButtonExtender ID="cbeResubmit" runat="server" TargetControlID="imgResubmit" ConfirmText="Do you want to resubmit?" />            
                                                            </ItemTemplate> 
                                                        </asp:TemplateField>    
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton id="imgCancel" CommandName="Cancel" runat="server" ImageUrl="~/images/icoCancel.gif" CausesValidation="False" AlternateText="Cancel" ImageAlign="Middle" visible="False"></asp:ImageButton>
                                                                <cc1:ConfirmButtonExtender ID="cbeCancel" runat="server" TargetControlID="imgCancel" ConfirmText="Do you want to cancel?" />            
                                                            </ItemTemplate> 
                                                        </asp:TemplateField>       
                                                        <asp:TemplateField ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:ImageButton id="imgUnBlock" CommandName="UnBlock" CommandArgument='<%# Eval("txn_no") %>' runat="server" ImageUrl="~/images/ico_Unblock.gif" CausesValidation="False" AlternateText="UnBlock" ImageAlign="Middle" visible="False"></asp:ImageButton>
                                                                <cc1:ConfirmButtonExtender ID="cbeUnblock" runat="server" TargetControlID="imgUnBlock" ConfirmText="Do you want to unblock ?" />
                                                                <asp:LinkButton id="lnkUnBlock_SAP" runat="server" CommandArgument='<%# Eval("txn_no") %>' CommandName="UnBlockSAP" text="<img src='../../images/ico_Unblock.gif' alt='Unblock' border='0'/>" OnClientClick="if (confirm('Do you want to unblock ?') == false) { window.event.returnValue = false; return false; } " CausesValidation="False" visible="False"></asp:LinkButton>
                                                            </ItemTemplate> 
                                                        </asp:TemplateField>       
                                                        <asp:ButtonField CommandName="Details" DatatextField="txn_no" HeaderText="Txn No" SortExpression="txn_no">
                                                           <itemstyle horizontalalign="Center" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="txn_status" HeaderText="Txn Status" ReadOnly="True" SortExpression="txn_status">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="STATUS" HeaderText="Status" ReadOnly="True" SortExpression="STATUS">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="ln_status" HeaderText="LN Status" ReadOnly="True" SortExpression="ln_status">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField> 
                                                        <asp:BoundField DataField="ln_reason" HeaderText="LN Remarks" ReadOnly="True" SortExpression="ln_reason">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField> 
                                                        <asp:ButtonField CommandName="Details" DatatextField="DOC_NO" HeaderText="Saving No" SortExpression="DOC_NO">
                                                           <itemstyle horizontalalign="Center" />
                                                        </asp:ButtonField>              
                                                        <asp:BoundField DataField="txn_timestamp" HeaderText="Txn Date" ReadOnly="True" SortExpression="txn_timestamp">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>                                                                                                                         
                                                        <asp:BoundField DataField="LASTUPDATE" HeaderText="Last Update" ReadOnly="True" SortExpression="LASTUPDATE">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>                                                                                                                
                                                        <asp:BoundField DataField="AUART" HeaderText="Order Type" ReadOnly="True" SortExpression="AUART">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="VKORG" HeaderText="Sales Org" ReadOnly="True" SortExpression="VKORG">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="VTWEG" HeaderText="Distribution Channel" ReadOnly="True" SortExpression="VTWEG">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="SPART" HeaderText="Division" ReadOnly="True" SortExpression="SPART">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="VKBUR" HeaderText="Sales Office" ReadOnly="True" SortExpression="VKBUR">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="VKGRP" HeaderText="Sales Group" ReadOnly="True" SortExpression="VKGRP">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="KUNNR" HeaderText="Soldto Party" ReadOnly="True" SortExpression="KUNNR">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="KUNDE" HeaderText="Shipto Party" ReadOnly="True" SortExpression="KUNDE">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="PAYER" HeaderText="Payer Code" ReadOnly="True" SortExpression="PAYER">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="salesrep_code" HeaderText="Salesrep Code" ReadOnly="True" SortExpression="salesrep_code">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="BSTNK" HeaderText="PO No" ReadOnly="True" SortExpression="BSTNK">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>  
                                                        <asp:BoundField DataField="BSTDK" HeaderText="PO Date" ReadOnly="True" SortExpression="BSTDK">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>   
                                                         <asp:BoundField DataField="KETDAT" HeaderText="Request Delivery Date" ReadOnly="True" SortExpression="KETDAT">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField> 
                                                         <asp:BoundField DataField="RESENDCOUNTER" HeaderText="Resubmit Counter" ReadOnly="True" SortExpression="RESENDCOUNTER">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField> 
                                                        <asp:BoundField DataField="REMARKS" HeaderText="Err Reason" ReadOnly="True" SortExpression="REMARKS">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>                                                        
                                                        <asp:BoundField DataField="NOTE" HeaderText="Note" ReadOnly="True" SortExpression="NOTE">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>       
                                                    </Columns>                                                
                                                    <FooterStyle CssClass="GridFooter" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                                    <RowStyle CssClass="GridNormal" />
                                                    <PagerSettings Visible="False" />
                                                </ccGV:clsGridView>
                                               </ContentTemplate>
                                                </asp:UpdatePanel>                                              
                                           <%--
                                           <asp:ButtonField CommandName="Edit" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit"/>                                                                                                                                                       
                                           <asp:BoundField DataField="salesrep_code" HeaderText="Sales Rep" ReadOnly="True" SortExpression="salesrep_code">
                                                <itemstyle horizontalalign="Center" />
                                            </asp:BoundField>
                                            <asp:ButtonField CommandName="Resubmit" HeaderText="" ImageUrl="~/images/icoCopy.gif" ButtonType="Image"/>                                                                                                                                                      
                                                                
                                            <asp:TemplateField>
                                                            <ItemTemplate>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                            --%>           
					                    </TD>
				                    </TR>      
				                    <TR>
					                    <TD colSpan="4">&nbsp;</TD>
				                    </TR>                   
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                    <tr class="Bckgroundreport">
                                        <td align="right" colspan="3">
                                           &nbsp;                        					
                                        </td>
                                    </tr>
                                </table>
                            </td>                            
                        </tr>                        
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>                        
                    </table> 
			    </td>
			</tr>			
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 
    </div>
        <!--The confirm_delete function is used in delete button-->
        <script id="scriptResubmit" language="javascript" type="text/javascript">
//        function confirm_delete(ind)
//        {
//	        if (confirm("Do you want to resubmit ?")==true){
//	            iFFMA_SAP_SAPList.UpdateMethod(UpdateMethod_CallBack);     
//		        return true;
//		    }
//	        else
//		        return false;
//        }       
        </script>
    </form>
</body>
</html>
<%'List function called by in-line scripts%>	

