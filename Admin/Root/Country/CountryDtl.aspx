﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CountryDtl.aspx.vb" Inherits="Admin_Root_CountryDtl" StyleSheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Country Details</title>
</head>

<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmCountryDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /><br />  
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <!--Begin Modify Here -->
                <div id="MainFrame" class="MainFrame">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" EnableTheming="true" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 0px 15px 0px;">
                                            <tr>
                                                <td>
                                                    <div class="border_std">
                                                    <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="False"
                                                        UseSubmitBehavior="false" Width="60px" />
                                                    </div>
                                                </td>
                                            </tr>                                            
                                            <tr class="ajax__tab_portal">
                                                <td class="ajax__tab_PNL">
                                                    <div class="border_std">
                                                    <customToolkit:wuc_lblInfo ID="lblErrDtl" runat="server" />                                                  
                                                    <table width="100%">
                                                        <tr valign="top">
                                                            <td>
                                                                <asp:Button ID="btnEditDtl" runat="server" Text="Edit"  CausesValidation="False"
                                                                    Width="60px" />&nbsp;
                                                                <asp:Button ID="btnSaveDtl" runat="server" Text="Save" CausesValidation="true"
                                                                    ValidationGroup="ValidateDtl" Width="60px" />&nbsp;
                                                                <asp:Button ID="btnCancelDtl" runat="server" Text="Cancel" 
                                                                    CausesValidation="false" Width="60px" />&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="width:80%">
                                                                <asp:DetailsView ID="dvCountry" runat="server" AutoGenerateRows="False" Width="100%"
                                                                   DataKeyNames="COUNTRY_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="Country Code" >
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtCountryCode" runat="server" Enabled="false" Text='<%# Bind("COUNTRY_CODE") %>' MaxLength="20" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtCountryCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtCountryCode" runat="server" Text='' MaxLength="20" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountryCode" runat="server" ControlToValidate="txtCountryCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCountryCode" runat="server" Text='<%# Bind("COUNTRY_CODE") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Country Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtCountryName" runat="server" Enabled="true" Text='<%# Bind("COUNTRY_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="txtCountryName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtCountryName" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="txtCountryName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCountryName" runat="server" Text='<%# Bind("COUNTRY_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Country Name 1">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtCountryName1" runat="server" Enabled="true" Text='<%# Bind("COUNTRY_NAME_1") %>' MaxLength="50" />
                                                                                <%--<asp:RequiredFieldValidator ID="rfvCountryName1" runat="server" ControlToValidate="txtCountryName1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />--%>
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtCountryName1" runat="server" Enabled="true" Text='' MaxLength="50" />
                                                                                <%--<asp:RequiredFieldValidator ID="rfvCountryName1" runat="server" ControlToValidate="txtCountryName1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />--%>
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCountryName1" runat="server" Text='<%# Bind("COUNTRY_NAME_1") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Country Description">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtCountryDesc" runat="server" Enabled="true" Text='<%# Bind("COUNTRY_DESC") %>' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountryDesc" runat="server" ControlToValidate="txtCountryDesc"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtCountryDesc" runat="server" Enabled="true" Text='' MaxLength="100" />
                                                                                <asp:RequiredFieldValidator ID="rfvCountryDesc" runat="server" ControlToValidate="txtCountryDesc"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCountryDesc" runat="server" Text='<%# Bind("COUNTRY_DESC") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Country Description 1">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtCountryDesc1" runat="server" Enabled="true" Text='<%# Bind("COUNTRY_DESC_1") %>' MaxLength="100" />
                                                                                <%--<asp:RequiredFieldValidator ID="rfvCountryDesc1" runat="server" ControlToValidate="txtCountryDesc1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />--%>
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtCountryDesc1" runat="server" Enabled="true" Text='' MaxLength="100" />
                                                                                <%--<asp:RequiredFieldValidator ID="rfvCountryDesc1" runat="server" ControlToValidate="txtCountryDesc1"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />--%>
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCountryDesc1" runat="server" Text='<%# Bind("COUNTRY_DESC_1") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                            </td>
                                                            
                                                        </tr>
                                                    </table>
                                                    </div>
                                                </td>
                                            </tr>
                                           
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--End Modify Here -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
