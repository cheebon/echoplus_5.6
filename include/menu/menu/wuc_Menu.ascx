<%@ Control Language="vb" AutoEventWireup="false" Inherits="wuc_Menu" CodeFile="wuc_Menu.ascx.vb" %>
<asp:Panel ID="pnlTreeMain" runat="server" Width="0" HorizontalAlign="Left" ScrollBars="None" CssClass="alwaysVisible" style="padding: 3px 1px 3px 1px">
    <fieldset style="padding-top:5px">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
 <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">

                        <tr>
                            <td valign="top" class="Bckgroundreport">
                                <asp:Panel ID="pnlSearch_Hdr" runat="server" Width="100%">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr onclick="pnlSearch_ChangeCollapsibleState();" style="cursor: pointer">
                                            <td class="bckgroundTitleBar">
                                                <span class="ContentXLight">SEARCH</span></td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" style="height: 5px">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlSearch" runat="server" Visible="true" DefaultButton="btnGo">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr class="Bckgroundreport">
                                            <td colspan="3"> &nbsp;</td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td>
                                                <span class="frmlabel">&nbsp;Country</span></td>
                                            <td>
                                                <span class="frmlabel">:&nbsp;</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlCountryID" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true" /></td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td>
                                                <span class="frmlabel">&nbsp;Principal</span></td>
                                            <td>
                                                <span class="frmlabel">:&nbsp;</span></td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlPrincipalID" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true" /></td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td>
                                                <span class="frmlabel">&nbsp;Sales Rep.</span></td>
                                            <td>
                                                <span class="frmlabel">:&nbsp;</span></td>
                                            <td style="white-space: nowrap;" align="left">
                                                <asp:TextBox ID="txtSearch" runat="server" CssClass="cls_textbox" Width="100" />
                                                <asp:Button ID="btnGo" runat="server" CssClass="cls_button" Text="Go" /></td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td colspan="3"> &nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Bckgroundreport">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                        <tr onclick="pnlSearch_ChangeCollapsibleState();" style="cursor: pointer">
                            <td class="bckgroundTitleBar">
                                <span class="ContentXLight">ORGANIZATION</span></td>
                        </tr>
                        <tr>
                            <td class="BckgroundBenealthTitle" style="height: 5px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" align="left" style="background-color: #EEEEEE;">
                    <asp:Panel ID="pnlTreeMenu" runat="server" Width="99%" ScrollBars="Auto" Wrap="false" HorizontalAlign="Left" CssClass="Bckgroundreport">
                        <div style="padding: 2px 5px 2px 2px; width: 100%">
                            <asp:TreeView ID="tvUserViewList" runat="server" ExpandDepth="1" CollapseImageUrl="~/images/folderopen.gif"
                                ExpandImageUrl="~/images/folder.gif" NoExpandImageUrl="~/images/page.gif" ShowLines="True">
                                <SelectedNodeStyle Font-Bold="True" />
                            </asp:TreeView>
                        </div>
                    </asp:Panel>
                </td>
            </tr>

        </table>
    </fieldset>
</asp:Panel>

<ajaxToolkit:CollapsiblePanelExtender ID="CPE_Search" runat="server" CollapseControlID="" ExpandControlID="" TargetControlID="pnlSearch" CollapsedSize="0" Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true" />
<ajaxToolkit:CollapsiblePanelExtender ID="CPE_Menu" runat="server" CollapseControlID="" ExpandControlID="" TargetControlID="pnlTreeMain" CollapsedSize="0" ExpandedSize="300" Collapsed="true" ExpandDirection="Horizontal" SuppressPostBack="true" />

