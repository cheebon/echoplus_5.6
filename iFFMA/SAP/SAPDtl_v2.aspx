<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPDtl_v2.aspx.vb" Inherits="iFFMA_SAP_SAPDtl_v2" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SAP Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
 <!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout">
    <form id="frmSAPDtl" method="post" runat="server">
    <div class="PagePadder">
    <fieldset class="" style="width: 98%;">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="100%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">                                    
                                     <tr class="Bckgroundreport">
				                        <td align="left">
                                            &nbsp;<asp:button id="btnViewTray" runat="server" Text="View Tray" CssClass="cls_button"></asp:button>
				                            <asp:button id="btnRefresh" runat="server" Text="Refresh" CssClass="cls_button"></asp:button>
					                        <asp:button id="cmdConfirm" runat="server" Text="Confirm" CssClass="cls_button"></asp:button>				         		         
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport"><td>&nbsp;</td></tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">	
					                             <tr>
							                        <td width="20%"><asp:label id="lblTxnNo" runat="server" CssClass="cls_label_header">Txn No</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot35" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnNoValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblTxnStatus" runat="server" CssClass="cls_label_header">Txn Status</asp:label></td>
							                        <td width="2%"><asp:label id="Label2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <asp:panel id="pnlLN" runat="server" Visible="false">
						                         <tr>
							                        <td width="20%"><asp:label id="lblLNStatus" runat="server" CssClass="cls_label_header">LN Status</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot40" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblLNStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
							                     </tr>
							                     <tr>
							                        <td width="20%"><asp:label id="lblLNRemarks" runat="server" CssClass="cls_label_header">LN Remarks</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot41" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblLNRemarksValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>						                    
						                        </asp:panel>        
					                            
						                        <tr><td>&nbsp;</td></tr>						                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblSRCode" runat="server" CssClass="cls_label_header">Salesrep Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblSRCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblVisitID" runat="server" CssClass="cls_label_header">Visit ID</asp:label></td>
							                        <td width="2%"><asp:label id="Label4" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblVisitIDValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblTxnDateIn" runat="server" CssClass="cls_label_header">Txn Date In</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnDateInValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblTxnDateOut" runat="server" CssClass="cls_label_header">Txn Date Out</asp:label></td>
							                        <td width="2%"><asp:label id="Label3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnDateOutValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCustCode" runat="server" CssClass="cls_label_header">Customer Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblCustCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                            <td width="20%"><asp:label id="lblContCode" runat="server" CssClass="cls_label_header">Contact Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot7" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblContCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblPONo" runat="server" CssClass="cls_label_header">PO No.</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblPONoValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblParDel" runat="server" CssClass="cls_label_header">Partial Delivery</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:checkbox id="lbllblParDelValue" runat="server" CssClass="cls_checkbox" Enabled="false"></asp:checkbox></td>
						                        </tr>
						                        <tr>
						                            <td vAlign="top" width="20%"><asp:label id="lblOrdAmt" runat="server" CssClass="cls_label_header">Total Order Amt</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot18" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblOrdAmtValue" runat="server" CssClass="cls_label"></asp:label></td>	
							                        <td width="20%"><asp:label id="lblUrgentFlag" runat="server" CssClass="cls_label_header">Urgent Flag</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot10" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:checkbox id="lblUrgentFlagValue" runat="server" CssClass="cls_checkbox" Enabled="false"></asp:checkbox></td>						                           
						                        </tr>	
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblCustDiscMode" runat="server" CssClass="cls_label_header">Customer Discount Mode</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot20" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblCustDiscModeValue" runat="server" CssClass="cls_label"></asp:label></td>			                        
							                        <td vAlign="top" width="20%"><asp:label id="lblDiscAmt" runat="server" CssClass="cls_label_header">Discount Amt</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot19" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDiscAmtValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        	<td vAlign="top" width="20%"><asp:label id="lblGstAmt" runat="server" CssClass="cls_label_header">GST Amt</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="Label8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblGstAmtValue" runat="server" CssClass="cls_label"></asp:label></td>
							                         <td width="20%"><asp:label id="lblCustDiscAmt" runat="server" CssClass="cls_label_header">Customer Discount Amt</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot11" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblCustDiscAmtValue" runat="server" CssClass="cls_label"></asp:label></td>					                        							                        
						                        </tr>
						                        
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblPaytermCode" runat="server" CssClass="cls_label_header">Payterm Code</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot12" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPaytermCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblPaytermName" runat="server" CssClass="cls_label_header">Payterm Name</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot13" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblPaytermNameValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>								                      
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblPayAmt" runat="server" CssClass="cls_label_header">Pay Amt</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot14" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPayAmtValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblPayDate" runat="server" CssClass="cls_label_header">Pay Date</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot15" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPayDateValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
						                            <td colspan="6">&nbsp;</td>
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblSalesAreaCode" runat="server" CssClass="cls_label_header">Sales Area Code</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot16" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblSalesAreaCodeValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblRouteCode" runat="server" CssClass="cls_label_header">Route Code</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot17" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblRouteCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
						                            <td colspan="6">&nbsp;</td>
						                        </tr>						                        
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblShiptoCode" runat="server" CssClass="cls_label_header">Shipto Code</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot21" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblShiptoCodeValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblShiptoDate" runat="server" CssClass="cls_label_header">Shipto Date</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot22" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblShiptoDateValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblAdd1" runat="server" CssClass="cls_label_header">Address 1</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot23" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblAdd1Value" runat="server" CssClass="cls_label"></asp:label></td>	
							                        <td vAlign="top" width="20%"><asp:label id="lblAdd2" runat="server" CssClass="cls_label_header">Address 2</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="Label5" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblAdd2Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblAdd3" runat="server" CssClass="cls_label_header">Address 3</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot24" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblAdd3Value" runat="server" CssClass="cls_label"></asp:label></td>	
							                        <td vAlign="top" width="20%"><asp:label id="lblAdd4" runat="server" CssClass="cls_label_header">Address 4</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="Label6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblAdd4Value" runat="server" CssClass="cls_label"></asp:label></td>					                        							                       
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblPostcode" runat="server" CssClass="cls_label_header">Postcode</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot25" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPostcodeValue" runat="server" CssClass="cls_label"></asp:label></td>	
							                        <td vAlign="top" width="20%"><asp:label id="lblCity" runat="server" CssClass="cls_label_header">City</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="Label7" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblCityValue" runat="server" CssClass="cls_label"></asp:label></td>					                        							                       
						                        </tr>
						                        <tr>
						                            <td colspan="6">&nbsp;</td>
						                        </tr>
						                        <tr>
						                            <td width="20%"><asp:label id="lblRemarks" runat="server" CssClass="cls_label_header">Remarks</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblRemarksValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblNote" runat="server" CssClass="cls_label_header">Note</asp:label></td>
							                        <td width="2%"><asp:label id="Label9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblNoteValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>					                       			
					                        </table>
				                        </td>
			                        </tr>	
			                        <tr><td>&nbsp;</td></tr>
			                        <tr>                                      
                                        <td class="Bckgroundreport" colspan="3">                                            
                                            <!--<uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>-->
                                            &nbsp;</TD>
				                    </TR>    
				                    <tr><td>&nbsp;</td></tr>		          
			                        <tr class="Bckgroundreport">
				                        <td align="right">
                                            <asp:HiddenField ID="hfDocType" runat="server" />
                                            <asp:HiddenField ID="hfSearchType" runat="server" />
                                            <asp:HiddenField ID="hfSearchValue" runat="server" />
                                            <asp:HiddenField ID="hfSearchValue1" runat="server" />
                                            <asp:HiddenField ID="hfTxnNo" runat="server" />
                                            <asp:HiddenField ID="hfSAPStatus" runat="server" />
                                            <asp:HiddenField ID="hfTxnTimestamp" runat="server" />
                                            <asp:HiddenField ID="hfTeam" runat="server" />
                                            <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="true"
                    AllowSorting="true" AutoGenerateColumns="false" GridWidth="" FreezeHeader="true" Width="98%" PagerSettings-Visible="false"
                    GridHeight="400px" RowSelectionEnabled="true" EnableViewState="true" OnSorting="dgList_Sorting">                    
                    <FooterStyle CssClass="GridFooter" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternate" />
                    <RowStyle CssClass="GridNormal" />
                    </ccGV:clsGridView>	                        
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</fieldset>
	</div>	
	</form>
</body>
</html>
