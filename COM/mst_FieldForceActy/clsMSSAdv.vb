'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	07/04/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsMSSAdv
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMSS"
        End Get
    End Property

    Public Function MSSUpdateHdrInfo(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_UPDATE_HDR_INFO"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "MSS ADV"
    Public Function GetMSSAdvList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_ADV_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSAdvDetails(ByVal strTeamCode As String, ByVal strTitleCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_ADV_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strTitleName As String, _
    ByVal strStartDate As String, ByVal strEndDate As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_ADV_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_NAME", strTitleName, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateMSSAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strTitleName As String, _
    ByVal strStartDate As String, ByVal strEndDate As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_ADV_UPDATE"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("TITLE_NAME", strTitleName, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub UpdateMSSAdvStatus(ByVal strTeamCode As String, ByVal strTitleCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_ADV_UPDATE_STATUS"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "MSS QUES ADV"
    Public Function GetMSSQuesAdvList(ByVal strTeamCode As String, ByVal strTitleCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSQuesAdvDetails(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSQuesAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strQuesName As String, ByVal strLvlInd As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_NAME", strQuesName, cor_DB.clsDB.DataType.DBString)
                .addItem("LVL_IND", strLvlInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateMSSQuesAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strQuesName As String, ByVal strLvlInd As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_UPDATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_NAME", strQuesName, cor_DB.clsDB.DataType.DBString)
                .addItem("LVL_IND", strLvlInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteMSSQuesAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "MSS QUES ADV - SEQ"
    Public Function GetMSSQuesAdvSeq(ByVal strTeamCode As String, ByVal strTitleCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_SEQ"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateMSSQuesAdvSeq(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSeq As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_SEQ_UPDATE"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SEQ", strSeq, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "MSS SUB QUES ADV - PREVIEW"
    Public Function GetMSSQuesAdvPreview(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_PREVIEW"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSQuesAdvPreviewCriteria(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_PREVIEW_CRITERIA"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSQuesAdvPreview2(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_ADV_PREVIEW_2"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#End Region

#Region "MSS SUB QUES ADV"
    Public Function GetMSSSubQuesAdvList(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSSubQuesAdvDetails(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSSubQuesAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strSubQuesName As String, ByVal strAnsTypeID As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_NAME", strSubQuesName, cor_DB.clsDB.DataType.DBString)
                .addItem("ANS_TYPE_ID", strAnsTypeID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSSubQuesAdvWMandatory(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strSubQuesName As String, ByVal strAnsTypeID As String, _
                                        ByVal strMandatory As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_CREATE_2"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_NAME", strSubQuesName, cor_DB.clsDB.DataType.DBString)
                .addItem("ANS_TYPE_ID", strAnsTypeID, cor_DB.clsDB.DataType.DBString)
                .addItem("MANDATORY", strMandatory, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateMSSSubQuesAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strSubQuesName As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_UPDATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_NAME", strSubQuesName, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub UpdateMSSSubQuesAdvWMandatory(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strSubQuesName As String, _
                                   ByVal strMandatory As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_UPDATE_2"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_NAME", strSubQuesName, cor_DB.clsDB.DataType.DBString)
                .addItem("MANDATORY", strMandatory, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Sub DeleteMSSSubQuesAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_ADV_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "MSS CRITERIA"
    Public Function GetMSSCriteriaAdvList(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CRITERIA_ADV_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSCriteriaAdvDetails(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CRITERIA_ADV_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE", strCriteriaCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSCriteriaAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaCode As String, ByVal strCriteriaName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CRITERIA_ADV_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE", strCriteriaCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_NAME", strCriteriaName, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateMSSCriteriaAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaCode As String, ByVal strCriteriaName As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CRITERIA_ADV_UPDATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE", strCriteriaCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_NAME", strCriteriaName, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteMSSCriteriaAdv(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CRITERIA_ADV_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE", strCriteriaCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "MSS QUES DISABLED"
    Public Function GetMSSQuesDisabledList(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_DISABLE_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSQuesDisabled(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaValue As String, ByVal strDisableCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_DISABLE_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_VALUE", strCriteriaValue, cor_DB.clsDB.DataType.DBString)
                .addItem("DISABLE_CODE", strDisableCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteMSSQuesDisabled(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaValue As String, ByVal strDisableCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_DISABLE_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_VALUE", strCriteriaValue, cor_DB.clsDB.DataType.DBString)
                .addItem("DISABLE_CODE", strDisableCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetMssQuesDisabledExcludeList(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strCriteriaValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_QUES_DISABLE_EXCLUDE_LIST"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_VALUE", strCriteriaValue, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "DDL"
    Public Function GetMSSAnsTypeDDL() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_ANS_TYPE_DDL"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSSubQuesCodeDDL(ByVal strTeamCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_SUB_QUES_CODE_DDL"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



