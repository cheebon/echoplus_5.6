Imports System.Data

Partial Class CustomerGeneralInfo
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerGeneralInfo"
        End Get
    End Property
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
        'Put user code to initialize the page here

            'strSalesmanCode = Trim(Session("dflSalesRepCode"))
            'strCustCode = Trim(Session("customercode"))

            'Call Header
            With wuc_lblheader
                .Title = "Customer General Information"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            If Not IsPostBack Then
            TimerControl1.Enabled = True

            End If

      
    End Sub

    Private Function getOperationTime(ByVal strind As String) As DataTable
        Dim dt As DataTable = Nothing

        Try
            Dim obj As New txn_Customer.clsCustInfo
            Dim strSalesmanCode, strCustCode As String
            'Dim dt As DataTable = Nothing

            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getOperationTimeDT(strSalesmanCode, strCustCode, strind)


            Return dt
        Catch ex As Exception
            ExceptionMsg(PageName & ".getOperationTime : " & ex.ToString)
        Finally
        End Try
        Return dt
    End Function

    Sub Onpageload()
        Try
            Dim obj As New txn_Customer.clsCustInfo
            Dim strSalesmanCode, strCustCode, strCallDay, strCallTime As String
            Dim dt As DataTable = Nothing

            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))

            lblCustCode.Text = strCustCode
            dt = obj.getGeneralInfoDT(strSalesmanCode, strCustCode)
            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    'lblCustCode.Text = IIf(IsDBNull(dt.Rows(i)("cust_code")), "Not Available", dt.Rows(i)("cust_code"))
                    'lblCustCode.Text = IIf(String.IsNullOrEmpty(lblCustCode.Text), "Not Available", lblCustCode.Text)

                    lblTelNo.Text = IIf(IsDBNull(dt.Rows(i)("contact_no")), "Not Available", dt.Rows(i)("contact_no"))
                    lblTelNo.Text = IIf(String.IsNullOrEmpty(lblTelNo.Text), "Not Available", lblTelNo.Text)

                    lblFaxNo.Text = IIf(IsDBNull(dt.Rows(i)("fax_no")), "Not Available", dt.Rows(i)("fax_no"))
                    lblFaxNo.Text = IIf(String.IsNullOrEmpty(lblFaxNo.Text), "Not Available", lblFaxNo.Text)

                    lblStatus.Text = IIf(IsDBNull(dt.Rows(i)("status")), "Not Available", dt.Rows(i)("status"))
                    lblStatus.Text = IIf(String.IsNullOrEmpty(lblStatus.Text), "Not Available", lblStatus.Text)

                    lblPTerm.Text = IIf(IsDBNull(dt.Rows(i)("cust_payterm")), "Not Available", dt.Rows(i)("cust_payterm"))
                    lblPTerm.Text = IIf(String.IsNullOrEmpty(lblPTerm.Text), "Not Available", lblPTerm.Text)

                    lblOutBalance.Text = IIf(IsDBNull(dt.Rows(i)("out_bal")), "Not Available", dt.Rows(i)("out_bal"))
                    lblOutBalance.Text = IIf(String.IsNullOrEmpty(lblOutBalance.Text), "Not Available", lblOutBalance.Text)

                    lblCrdLimit.Text = IIf(IsDBNull(dt.Rows(i)("credit_limit")), "Not Available", dt.Rows(i)("credit_limit"))
                    lblCrdLimit.Text = IIf(String.IsNullOrEmpty(lblCrdLimit.Text), "Not Available", lblCrdLimit.Text)

                    lblType.Text = IIf(IsDBNull(dt.Rows(i)("cust_type")), "Not Available", dt.Rows(i)("cust_type"))
                    lblType.Text = IIf(String.IsNullOrEmpty(lblType.Text), "Not Available", lblType.Text)

                    lblVisitFreq.Text = IIf(IsDBNull(dt.Rows(i)("visit_frequency")), "Not Available", dt.Rows(i)("visit_frequency"))
                    lblVisitFreq.Text = IIf(String.IsNullOrEmpty(lblVisitFreq.Text), "Not Available", lblVisitFreq.Text)

                    'txtPrCode.Text = dt.Rows(i)("priority_code").ToString
                    lblEmail.Text = IIf(IsDBNull(dt.Rows(i)("cust_email")), "Not Available", dt.Rows(i)("cust_email"))
                    lblEmail.Text = IIf(String.IsNullOrEmpty(lblEmail.Text), "Not Available", lblEmail.Text)

                    lblOpenDate.Text = IIf(IsDBNull(dt.Rows(i)("cust_open_date")), "Not Available", dt.Rows(i)("cust_open_date"))
                    lblOpenDate.Text = IIf(String.IsNullOrEmpty(lblOpenDate.Text), "Not Available", lblOpenDate.Text)

                    lblVisitDate.Text = IIf(IsDBNull(dt.Rows(i)("cust_strvisit_date")), "Not Available", dt.Rows(i)("cust_strvisit_date"))
                    lblVisitDate.Text = IIf(String.IsNullOrEmpty(lblVisitDate.Text), "Not Available", lblVisitDate.Text)

                    strCallDay = IIf(IsDBNull(dt.Rows(i)("call_day")), "", dt.Rows(i)("call_day"))
                    strCallTime = IIf(IsDBNull(dt.Rows(i)("call_time")), "", dt.Rows(i)("call_time"))
                    ddlBTC.Items.Add(IIf(String.IsNullOrEmpty(String.Concat(strCallDay, strCallTime)), "None", String.Concat(strCallDay, strCallTime)))

                    lblRemarks.Text = IIf(IsDBNull(dt.Rows(i)("remarks")), "Not Available", dt.Rows(i)("remarks"))
                    lblRemarks.Text = IIf(String.IsNullOrEmpty(lblRemarks.Text), "Not Available", lblRemarks.Text)
                Next
            Else
                ddlBTC.Items.Insert(0, "None")
                'lblCustCode.Text = "Not Available"
                lblTelNo.Text = "Not Available"
                lblFaxNo.Text = "Not Available"
                lblStatus.Text = "Not Available"
                lblPTerm.Text = "Not Available"
                lblOutBalance.Text = "Not Available"
                lblCrdLimit.Text = "Not Available"
                lblType.Text = "Not Available"
                lblVisitFreq.Text = "Not Available"
                lblEmail.Text = "Not Available"
                lblOpenDate.Text = "Not Available"
                lblVisitDate.Text = "Not Available"
                lblRemarks.Text = "Not Available"
            End If


            dt = getOperationTime("O")

            If dt.Rows.Count > 0 Then
                ddlOpening.DataSource = dt
                ddlOpening.DataTextField = "Otime"
                ddlOpening.DataBind()
            Else
                ddlOpening.Items.Insert(0, "None")
            End If

            dt = getOperationTime("C")
            If dt.Rows.Count > 0 Then
                ddlClosing.DataSource = dt
                ddlClosing.DataTextField = "Otime"
                ddlClosing.DataBind()
            Else
                ddlClosing.Items.Insert(0, "None")
            End If

            dt = getOperationTime("D")
            If dt.Rows.Count > 0 Then
                ddlDelDate.DataSource = dt
                ddlDelDate.DataTextField = "Otime"
                ddlDelDate.DataBind()
            Else
                ddlDelDate.Items.Insert(0, "None")
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally
            'obj = Nothing
        End Try
    End Sub


    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False
            Onpageload()
        End If

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



