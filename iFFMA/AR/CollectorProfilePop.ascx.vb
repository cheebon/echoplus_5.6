﻿Imports System.Data

Partial Class iFFMA_AR_CollectorProfilePop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property CountryCode() As String
        Get
            Return Trim(hdCountryCode.Value)
        End Get
        Set(ByVal value As String)
            hdCountryCode.Value = value
        End Set
    End Property


    Public Property CollectorCode() As String
        Get
            Return Trim(hdCollectorCode.Value)
        End Get
        Set(ByVal value As String)
            hdCollectorCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadBUDDL(ByVal DT As DataTable)
        Dim dtBU As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlBUCode As DropDownList = CType(DetailsView1.FindControl("ddlBUCode"), DropDownList)
            If ddlBUCode IsNot Nothing Then
                dtBU = clsCommon.GetBUDDL
                With ddlBUCode
                    .Items.Clear()
                    .DataSource = dtBU.DefaultView
                    .DataTextField = "BU_NAME"
                    .DataValueField = "BU_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("BU_CODE")), "", DT.Rows(0)("BU_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCountryDDL(ByVal DT As DataTable)
        Dim dtCountry As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlCountryCode As DropDownList = CType(DetailsView1.FindControl("ddlCountryCode"), DropDownList)
            If ddlCountryCode IsNot Nothing Then
                dtCountry = clsCommon.GetCountryDDL
                With ddlCountryCode
                    .Items.Clear()
                    .DataSource = dtCountry.DefaultView
                    .DataTextField = "COUNTRY_NAME"
                    .DataValueField = "COUNTRY_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("COUNTRY_CODE")), "", DT.Rows(0)("COUNTRY_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCompCodeDDL(ByVal DT As DataTable)
        Dim dtCompCode As DataTable
        Dim clsCollector As New mst_AR.clsCollector

        Try
            Dim ddlBUCode As DropDownList = CType(DetailsView1.FindControl("ddlBUCode"), DropDownList)
            Dim ddlCompCode As DropDownList = CType(DetailsView1.FindControl("ddlCompCode"), DropDownList)
            'Dim LeftTeamCodeString As String = ddlTeamCode.SelectedValue.Split("-")(0)
            dtCompCode = clsCollector.GetCompCodeDDL(ddlBUCode.SelectedValue)
            With ddlCompCode
                .Items.Clear()
                .DataSource = dtCompCode.DefaultView
                .DataTextField = "COMP_CODE"
                .DataValueField = "COMP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(IsDBNull(DT.Rows(0)("COMP_CODE")), "", DT.Rows(0)("COMP_CODE"))
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsCollector As New mst_AR.clsCollector

            DT = clsCollector.GetCollectorDetails(CountryCode, CollectorCode)
            'If DT.Rows.Count > 0 Then
            '    If DT.Rows(0)("LOCK_FLAG") <> "0" Then
            '        RaiseEvent PopLockAlert()
            '        Exit Sub
            '    End If
            'Else
            '    DT.Rows.Add(DT.NewRow())
            'End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadBUDDL(DT)
                LoadCountryDDL(DT)
                LoadCompCodeDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub

    Public Sub RenewDataBind()
        ViewState.Clear()
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlBU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim clsCollector As New mst_AR.clsCollector
        Dim DT As DataTable
        DT = clsCollector.GetCollectorDetails(CountryCode, CollectorCode)
        LoadCompCodeDDL(DT)
        RenewDataBind()
        Show()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim clsCollector As New mst_AR.clsCollector
            'Dim strSelectedCode As String = clsCollector.GetNewCollectorCode()
            Dim ddlCountryCode As DropDownList = CType(DetailsView1.FindControl("ddlCountryCode"), DropDownList)
            Dim lblCollectorCode As Label = CType(DetailsView1.FindControl("lblCollectorCode"), Label)
            Dim txtCollectorName As TextBox = CType(DetailsView1.FindControl("txtCollectorName"), TextBox)
            Dim txtCollectorSAPCode As TextBox = CType(DetailsView1.FindControl("txtCollectorSAPCode"), TextBox)
            Dim ddlBUCode As DropDownList = CType(DetailsView1.FindControl("ddlBUCode"), DropDownList)
            Dim ddlCompCode As DropDownList = CType(DetailsView1.FindControl("ddlCompCode"), DropDownList)

            'Dim strCollectorCode As String
            'Dim dtCollectorCode As DataTable
            'Dim strCountryCode As String = ddlCountryCode.SelectedValue
            'Dim dtCollectorCode As DataTable = clsCollector.GetNewCollectorCode(strCountryCode)
            'Dim lblCollectorCodeEdit As String
            'Dim strCollectorCodeEdit As String = lblCollectorCodeEdit

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                'If strCollectorCode <> "" Then
                clsCollector.UpdateCollector(CountryCode, lblCollectorCode.Text, Trim(txtCollectorName.Text), Trim(txtCollectorSAPCode.Text), Trim(ddlBUCode.SelectedValue), Trim(ddlCompCode.SelectedValue))
                'ViewState("COLLECTOR_CODE") = lblCollectorCode.Text
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
                'Hide()
                'End If
            Else
                Dim strCountryCode As String = ddlCountryCode.SelectedValue
                Dim dtCollectorCode As DataTable = clsCollector.GetNewCollectorCode(strCountryCode)
                Dim strCollectorCode As String = dtCollectorCode.Rows(0)(0)
                Dim DT As DataTable
                DT = clsCollector.CreateCollector(ddlCountryCode.SelectedValue, strCollectorCode, Trim(txtCollectorName.Text), Trim(txtCollectorSAPCode.Text), Trim(ddlBUCode.SelectedValue), Trim(ddlCompCode.SelectedValue))

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Record already exists!"
                    CountryCode = ddlCountryCode.SelectedValue.ToString
                    CollectorCode = CollectorCode
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else

                    lblInfo.Text = "The record is successfully created."
                    CountryCode = ddlCountryCode.SelectedValue.ToString
                    CollectorCode = strCollectorCode
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
