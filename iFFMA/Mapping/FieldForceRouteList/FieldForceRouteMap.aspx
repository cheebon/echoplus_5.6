﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FieldForceRouteMap.aspx.vb" Inherits="iFFMA_Mapping_FieldForceRouteList_FieldForceRouteMap" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Field Force Route Map</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/Gmap/Library/MapIconMaker/mapiconmaker2.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/MessagerCustom.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/ColorPicker.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/RoutePlan/Customer.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/RoutePlan/Graph2.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/RoutePlan/Node.js" type="text/javascript"></script>   
    <script src="../../../include/Gmap/Library/RoutePlan/PathFinder2.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/RoutePlan/RouteList.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/RoutePlan/UserDefinedRoute.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/RoutePlan/VisitPoint.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/GenericMarker/GenericMarker2.js" type="text/javascript"></script>
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>
    <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo&sensor=true" type="text/javascript"> </script>
<%-- <script src="https://maps.google.com/maps?file=api&amp;v=3&amp;sensor=true&amp;key=<%= GoogleMap.Key %>" type="text/javascript"></script>--%>
    <script src="../../../include/Gmap/Library/MapIconMaker/markerwithlabel.js" type="text/javascript"></script>
    <style>
        .ctrlPanel {border: solid 1px #0099ff; width:245px; padding: 5px 0px 5px 5px}
    </style>
     <%--css for the label on the top of the marker on the map --%>
     <style type="text/css">
     .labels {
     color: #000000;
     background-color: #DA7187;
     font-family: "Lucida Grande", "Arial", sans-serif;
     font-size: 12px;
     text-align: center;
     font-weight: bold;
     width: 10px;     
     white-space: nowrap;
     }
    </style>

    <script type="text/javascript">
        var childwin;

        //Cust Default Location
        var Default_Latitude;
        var Default_Longitude;
        var Default_Zoomlevel;

        var SalesrepCode;
        var RouteCode;
        var SalesrepName;
        var RouteName;

        var root;
        var map;
        var dir;
        var pfinder;
        var mypoint = [];
        var myCustpoint = [];
        var pointname = [];
        var startingpoint;
        var gDir = []; //store reference to declared GDirections objects
        var udRoute; //store reference to visit defined by user
        var distance = [];
        var marker; //store reference to all markers created.
        var markers = [];
        var totaldistance;

        //Step Directions
        var stepDir; //store Step GDirections objects
        var stepPath; //store array of shortest path for step direction
        var stepDescription; //Store the HTML Description returned by GDirections.
        //
        // var directionsService = new google.maps.DirectionsService();
        //Step Description


        function initialize() {
            var mapOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE
                },
                mapTypeControl: true
            };
            directionsDisplay = new google.maps.DirectionsRenderer();
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
           
                directionsDisplay.setMap(map);

                QueryLocation();

                //Plot Customers loaded
                PlotCustomer();
          
        }
    
        // Add a marker to the map and push to the array.
        function addMarker(Latitude, Longitude) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Latitude, Longitude),
                map: map,
                draggable: true
            });
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the overlays from the map, but keeps them in the array.
        function clearOverlays() {
            setAllMap(null);
        }

        // Shows any overlays currently in the array.
        function showOverlays() {
            setAllMap(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteOverlays() {
            clearOverlays();
        }
      
        /**********************************************************************
        / Initial plot of customer location
        /
        **********************************************************************/
        function PlotCustomer() {
            if (mypoint) {
                for (var i = 0; i < mypoint.length; i++) {
                    var _marker = CreateMarker(mypoint[i].Latitude, mypoint[i].Longitude);

                    var str = "<div class=\"cls_label\">";
                    str += "<span class='map_header_text'>Customer:</span> " + myCustpoint[i].CustName + "<br />";
                    str += "<span class='map_header_text'>Contact:</span> " + myCustpoint[i].ContName + "<br />";
                    str += "<span class='map_header_text'>Customer Class:</span> " + GetClassName(myCustpoint[i].Class) + "<br />";
                    str += "<span class='map_header_text'>Customer Type:</span> " + myCustpoint[i].CustType + "<br />";
                    str += "<span class='map_header_text'>Customer MTD Sales:</span> " + myCustpoint[i].MtdSales + "<br />";
                    str += "<span class='map_header_text'>Customer YTD Sales:</span> " + myCustpoint[i].YtdSales + "<br />";
                    str += "<span class='map_header_text'>Customer No of SKU:</span> " + myCustpoint[i].NoSKU + "<br />";
                    str += "<a href='#' onclick='PopImage(\"" + root + "" + myCustpoint[i].ImgLoc + "\", \"" + myCustpoint[i].CustName + "\", \"\");'>Outlet Photo</a>";
                    str += "</div>";

                    AddInfoWindow(_marker, str);
                    _marker.setMap(map);
                }

                if (mypoint.length > 0) {
                    var iCenter = parseInt(mypoint.length / 2);
                    map.setCenter(new google.maps.LatLng(mypoint[iCenter].Latitude, mypoint[iCenter].Longitude), 12);
                }
            }
        }
        function GetClassName(Class) {
            switch (Class) {
                case "A":
                    return "A";
                    break;
                case "B":
                    return "B";
                    break;
                case "C":
                    return "C";
                    break;
                case "D":
                    return "D";
                    break;
                default:
                    return "OTH";
                    break;
            }
        }
        /**********************************************************************
        / Get default location based on principal
        /
        **********************************************************************/
        function QueryLocation() {
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {
            if (result) {
                if (result.Total > 0) {
                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;

                    SetMapDefaultPoint();
                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {

                var point = new google.maps.LatLng(Default_Latitude, Default_Longitude);
                map.setCenter(point, parseInt(Default_Zoomlevel));
                map.setZoom(10);
            }
        }
        function FindPath() {
            ShowMessage("map_canvas", "Message", "Calculating route.", "off");
            ddlstartpoint = document.getElementById("ddlStartPoint");
            toggleScreenOnOff('off');

            if (ddlstartpoint) {
                if (mypoint.length != 0) {
                    startingpoint = ddlstartpoint.options[ddlstartpoint.selectedIndex].value;

                    pfinder = new PathFinder(map, udRoute);
                    pfinder.setStartPoint(startingpoint);
                    pfinder.Graph.BulkAddWaypoint(mypoint);
                    pfinder.MarkDestination();
                    pfinder.FindShortestPath(onSuccessFindPathCallback, onFailCallback);

                }
                else {
                    //Error message
                    ShowMessage("map_canvas", "Message", "No route information loaded.", "on");
                }
            }
        }
        function gDirLoad(_gdir) {
            var _dl = _gdir.getPolyline();
            _dl.color = colorpicker.GetNextColor();
            _dl.opacity = 0.7;
            _dl.weight = 3;
            map.addOverlay(_dl);
        }

       
        function onFailCallback(ErrorCode) {
            ShowMessage("map_canvas", "Message", "Google maps encounter problem while processing data. Error code returned: " + ErrorCode, "on");
        }

        function showSteps(directionResult) {
            // For each step, place a marker, and add the text to the marker's
            // info window. Also attach the marker to an array so we
            // can keep track of it and remove it when calculating new
            // routes.
            var color;
            var colorpicker = new ColorPicker();
            var plotcount = 0;
            var label;

            deleteOverlays();
            var path = directionResult.routes[0].overview_path;
            var legs = directionResult.routes[0].legs;

            for (i = 0; i < legs.length; i++) {
                var polyline = new google.maps.Polyline({
                    path: [],
                    strokeColor: colorpicker.GetNextColor(),
                    strokeOpacity: 0.7,
                    strokeWeight: 3
                });
                totaldistance += parseFloat(legs[i].distance.value);
                var steps = legs[i].steps;
                stepDescription += "<div class='cls_label' style='background-color:#d4d4d4;padding-left:10px;width:490px'><img src=\"../../../images/ico_general_info.gif\" />&nbsp;<b>" + GetCustName(pfinder.Graph.ShortestPath[i].CustCode) + "</b></div>";
                color = getBackground(i);
                for (var j = 0; j < steps.length; j++) {
                    var nextSegment = steps[j].path;
                    var step = j + 1;
                    color = getBackground(i * 10 + j);
                    stepDescription += "<div style='background-color:" + color + ";width:500px'>"
                                                            + "<div class='cls_label' style='border-top: solid 1px #e5e5e5; width:20px; float: left; padding-left: 40px; padding-top: 5px; padding-bottom: 5px; background-color:" + color + "'>"
                                                            + (i + 1) + "</div>" + "<div class='cls_label' style='border-top: solid 1px #e5e5e5; width:320px; float: left; padding-left: 20px;padding-top: 5px; padding-bottom: 5px; background-color:" + color + "'>"
                                                            + steps[j].instructions + "</div><div class='cls_label' style='border-top: solid 1px #e5e5e5; width:80px; float: left; padding-left: 20px;padding-top: 5px; padding-bottom: 5px; background-color:" + color + "'>"
                                                            + steps[j].distance.text + "</div></div>";
                    for (k = 0; k < nextSegment.length; k++) {
                        polyline.getPath().push(nextSegment[k]);
                    }
                }
                polyline.setMap(map);
            }
            $("#lblDistance").text(totaldistance);
            $("#hfDistance").val(totaldistance);

            for (var count = 0; count < pfinder.Graph.ShortestPath.length; ++count) {
                label = "" + (count + 1) + "";

                var cust = GetCustObj(pfinder.Graph.ShortestPath[count].CustCode);
                var myRoute = directionResult.routes[0].legs[0];

                lat = pfinder.Graph.ShortestPath[count].Latitude;
                lng = pfinder.Graph.ShortestPath[count].Longitude;

                marker[count] = new MarkerWithLabel({
                    position: new google.maps.LatLng(lat, lng),
                    labelContent: label,
                    labelAnchor: new google.maps.Point(32 / 4 - 3, 30),
                    labelClass: "labels", // the CSS class for the label 
                    labelStyle: {opacity: 0.75}
                });

                var cust = GetCustObj(pfinder.Graph.ShortestPath[count].CustCode);
                var str = "<div class=\"cls_label\">";
                str += "<span class='map_header_text'>Customer:</span> " + cust.CustName + "<br />";
                str += "<span class='map_header_text'>Contact:</span> " + cust.ContName + "<br />";
                str += "<span class='map_header_text'>Customer Class:</span> " + GetClassName(cust.Class) + "<br />";
                str += "<span class='map_header_text'>Customer Type:</span> " + cust.CustType + "<br />";
                str += "<span class='map_header_text'>Customer MTD Sales:</span> " + cust.MtdSales + "<br />";
                str += "<span class='map_header_text'>Customer YTD Sales:</span> " + cust.YtdSales + "<br />";
                str += "<span class='map_header_text'>Customer No of SKU:</span> " + cust.NoSKU + "<br />";
                str += "<a href='#' onclick='PopImage(\"" + root + "" + cust.ImgLoc + "\", \"" + cust.CustName + "\", \"\");'>Outlet Photo</a>";
                str += "</div>";
                AddInfoWindow(marker[count], str);
                marker[count].setMap(map);
                WriteRecordCount(++plotcount);
            }
            $("#btnPrintRoute").attr('disabled', '');
            toggleScreenOnOff('on');
            HideMessage("Message");
            $("#map_step").html(stepDescription);
            $("#map_step").show();
        }


        function onSuccessFindPathCallback() {
            var i = 0;
            gDir = new Array(pfinder.Graph.ShortestPath.length - 1);
            var newIcon;
            var coordinate, lat, lng;
            var label;
            var plotcount = 0;
            var waypts = [];
            distance = [];
            totaldistance = 0;
            marker = [];
            WriteRecordCount(plotcount);
            stepDescription = "";
            var waypts = [];
            var directionsService = new google.maps.DirectionsService();
            
            //// request one time DirectionsService after get all the location
            for (var count = 1; count < pfinder.Graph.ShortestPath.length - 1; ++count) {
                waypts.push({
                    location: new google.maps.LatLng(pfinder.Graph.ShortestPath[count].Latitude, pfinder.Graph.ShortestPath[count].Longitude),
                    stopover: true
                });
            }
            var request = {
                origin: new google.maps.LatLng(pfinder.Graph.ShortestPath[0].Latitude, pfinder.Graph.ShortestPath[0].Longitude),
                destination: new google.maps.LatLng(pfinder.Graph.ShortestPath[pfinder.Graph.ShortestPath.length - 1].Latitude, pfinder.Graph.ShortestPath[pfinder.Graph.ShortestPath.length - 1].Longitude),
                waypoints: waypts,
                travelMode: google.maps.TravelMode.DRIVING
            };

            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    // directionsDisplay.setDirections(response);
                    showSteps(response);
                }
            });
            SetCenterMap();
        }

        /********************************************************************
        / Method to build an array of waypoints to load direction at 1 go to
        / avoid connection drop by google. Remember GDirections only support 10 
        / waypoints in a request. 
        *********************************************************************/
        function prepPathForDesc() {
            var paths = [];
            var partialpath;
            for (var i = 0; i < pfinder.Graph.ShortestPath.length; ++i) {
                if (i % 10 == 0) {
                    if (i == 0)
                    { partialpath = []; }
                    else {
                        paths.push(partialpath);
                        partialpath = [];
                    }
                }

                partialpath.push(pfinder.Graph.ShortestPath[i].Latitude + ", " + pfinder.Graph.ShortestPath[i].Longitude);

            }
            paths.push(partialpath);
            return paths;
        }

        function LoadStepDescription() {
            var color;
            stepDir = [];
            stepDescription = "";

            stepPath = prepPathForDesc();

            for (var i = 0; i < stepPath.length; ++i) {
                stepDir[i] = new GDirections();

                google.maps.event.addListener(stepDir[i], "load", (function (i) {
                    return function () {

                        for (var j = 0; j < stepDir[i].getNumRoutes(); j += 1) {
                            var counter = i * 10 + j + 1;
                            stepDescription += "<div class='cls_label' style='background-color:#4BF54B;padding-left:15px;width:305px'>" + GetCustName(pfinder.Graph.ShortestPath[counter].CustCode) + " &nbsp;<a href='#' onclick='PanMarker(" + counter + ")'>Pan</a></div>";
                            for (var k = 0; k < stepDir[i].getRoute(j).getNumSteps(); ++k) {
                                color = getBackground(i * 10 + j);
                                stepDescription += "<div style='background-color:" + color + ";width:320px'>" + "<div class='cls_label' style='border-top: solid 1px gray; width:200px; float: left; padding-left: 20px;background-color:" + color + "'>" + stepDir[i].getRoute(j).getStep(k).getDescriptionHtml() + "</div><div class='cls_label' style='border-top: solid 1px gray; width:80px; float: left; padding-left: 20px;background-color:" + color + "'>" + stepDir[i].getRoute(j).getStep(k).getDistance().html + "</div></div>";
                            }
                        }

                        if (path.length - 1 > i) {
                            stepDir[i + 1].loadFromWaypoints(stepPath[i + 1], { getSteps: true });
                        }
                        else {
                            $("#map_step").html(stepDescription);
                            $("#map_step").show();
                        }

                    };
                }
                )(i));
            }
            stepDir[0].loadFromWaypoints(stepPath[0], { getSteps: true });

        }
        function GetCustName(CustCode) {
            if (pfinder.Graph.RawGraph) {
                for (var i = 0; i < pfinder.Graph.RawGraph.length; ++i) {
                    if (pfinder.Graph.RawGraph[i].CustCode == CustCode) {
                        return pfinder.Graph.RawGraph[i].CustName;
                    }
                }
            }
        }
        function GetCustObj(CustCode) {


            if (myCustpoint) {
                for (var i = 0; i < myCustpoint.length; ++i) {
                    if (myCustpoint[i].CustCode == CustCode) {
                        return myCustpoint[i];
                    }
                }
            }
        }
        function getBackground(i) {
            if ((i % 2) == 0) { return "#E0E0E0"; }
            else { return "#ffffff"; }
        }
        function SetCenterMap() {
            if (pfinder.Graph.RawGraph) {
                if (pfinder.Graph.RawGraph.length > 0) {
                    var iCenter = parseInt(pfinder.Graph.RawGraph.length / 2);

                    map.setCenter(new google.maps.LatLng(pfinder.Graph.RawGraph[0].Latitude, pfinder.Graph.RawGraph[0].Longitude), 12);
                }
            }

        }
        function PanMarker(counter) {
            map.panTo(marker[counter].getLatLng());
        }

        /*******************************************************************
        / On off criteria
        ********************************************************************/
        function toggleMap(flag) {
            if (flag) {
                switch (flag) {
                    case "on":
                        //$("#pnlPopUp").css("visibility", "visible");
                        $("#pnlRouteSeq").show("slow");
                        break;
                    case "off":
                        //$("#pnlPopUp").css("visibility", "hidden");
                        $("#pnlRouteSeq").hide("slow");
                        break;
                }
            }
        }

        /********************************************************************
        / Save method
        *********************************************************************/
        function Save() {
            if (pfinder) {
                if (pfinder.Graph.ShortestPath.length > 0) {
                    toggleScreenOnOff('off');
                    ws_RoutePlanning.SaveRouteSeq(pfinder.Graph.ShortestPath, SalesrepCode, RouteCode, OnSuccessSave, OnFailSave);
                }
            }
            else {
                alert("Plan route sequence first.");
            }
        }
        function OnSuccessSave() {
            $("#lblSuccess").html("<img src='../../../images/ico_tick.gif' border='0' /> Records successfully saved!<br />").css("color", "#2BD52B");
            $("#SummTable").slideUp("slow", function () { $("#lblSuccess").slideDown(); });
            HideAllControl("off");


            //alert("Success");
        }
        function OnFailSave(Error) {
            $("#lblSuccess").html("<img src='../../../images/Close.png' border='0' /> Some errors have occurred during save. Please try again.<br /> Error: " + Error.get_message()).css("color", "#D52B55");
            $("#lblSuccess").slideDown();
            toggleScreenOnOff('on');
            //alert("Failed");
        }
        /****************************************************************************************************************************/

        function toggleScreenOnOff(flag) {
            switch (flag) {
                case "on":
                    $("#pnlStartPoint").attr('disabled', '');
                    $("#pnlCuzSeq").attr('disabled', '');
                    $("#btnSave").attr('disabled', '');
                    $("#pnlViewPrevRoute").attr('disabled', '');
                    //$("#btnPrintRoute").attr('disabled', '');
                    break;
                case "off":
                    $("#pnlStartPoint").attr('disabled', 'disabled');
                    $("#pnlCuzSeq").attr('disabled', 'disabled');
                    $("#btnSave").attr('disabled', 'disabled');
                    $("#pnlViewPrevRoute").attr('disabled', 'disabled');
                    $("#btnPrintRoute").attr('disabled', 'disabled');
                    break;
            }

        }

        function HideAllControl(flag) {
            switch (flag) {
                case "on":
                    $("#pnlStartPoint").show();
                    $("#pnlCuzSeq").show();
                    $("#btnSave").show();
                    $("#pnlViewPrevRoute").show();
                    $("#btnPrintRoute").show();
                    break;
                case "off":
                    $("#pnlStartPoint").hide();
                    $("#pnlCuzSeq").hide();
                    $("#btnSave").hide();
                    $("#pnlViewPrevRoute").hide();
                    $("#btnPrintRoute").hide();
                    break;
            }
        }
        function OpenPopup() {
            var childwin = window.open('FieldForceRoutePreviousPlan.aspx?salesrep=' + SalesrepCode + '&route=' + RouteCode, '', 'location=0,menubar=0,scrollbars=yes,resizable=yes,status=1,left=0,top=0,maximize=1');
        }

        function OpenPrinter() {
            childwin = window.open('FieldForceRoutePrint.aspx', '', 'location=0,menubar=0,scrollbars=yes,resizable=yes,status=1,left=0,top=0,maximize=1');

            childwin.focus();
        }

        function GetSteps() {
            if (childwin) {
                childwin.Steps = stepDescription;
                childwin.SalesrepName = SalesrepName;
                childwin.RouteName = RouteName;
            }
        }

        /***********************************************
        / Show cust image
        ***********************************************/
        function PopImage(ImgLoc, CustName, Address) {
            mp_ToggleModalPopup("pnlPictureDiv", "divModalMask2", "on");


            LoadImage(ImgLoc);
            BindCust(CustName, Address);
        }
        function BindCust(CustName, Address) {
            $("#PicDiv_CustName").text(CustName);
            $("#PicDiv_Address").text(Address);
        }
        function LoadImage(ImgLoc) {
            //$("#CustImg").error(Image_OnErrorHide);
            resetCustImg();

            $("#CustImg").attr({
                src: ImgLoc,
                title: "",
                alt: "Customer Image"
            });
        }
        /***********************************************
        / Hide broken image
        ***********************************************/
        function Image_OnErrorHide() {
            $("#CustImg").hide();
            $("#ImgLoadFailMsg").show();
        }
        function resetCustImg() {
            $("#CustImg").show();
            $("#ImgLoadFailMsg").hide();

            $("#CustImg").attr({
                src: "../../images/indicator.gif",
                title: "",
                alt: "Loading"
            });

            $("#PicDiv_CustName").text("");
            $("#PicDiv_Address").text("");
        }
        function WriteRecordCount(count) {
            $("#spnCount").text(count);
        }
    </script>
</head>
<body onload="initialize();" class="Bckgroundreport">
    <form id="form1" runat="server">
    <ajaxtoolkit:toolkitscriptmanager ID="ToolkitScriptManager1" 
        runat="server" AsyncPostBackTimeout="300" ScriptMode="Release">
        <Services>           
            <asp:ServiceReference
            path="~/DataServices/ws_PrincipalLoc.asmx" />
            <asp:ServiceReference
            path="~/DataServices/ws_RoutePlanning.asmx" />
        </Services>            
    </ajaxtoolkit:toolkitscriptmanager>
    <div style="width:900px">
        <asp:Label ID="lblErr" runat="server" Text="" class="cls_validator"></asp:Label>
        <div style="margin-left: 20px;width:593px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898"><span class="cls_label" id="spnCount" style="font-weight:bold">0</span><span class="cls_label" style="font-weight:bold"> customers plot on map.</span></div>
        <div id="map_canvas" style="width: 600px; height: 450px; padding: 0px 0px 0px 0px; margin-left: 20px; float:left">
         <%-- <script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>" type="text/javascript"></script>--%>
  </div>
        <div style="float:right; width: 250px">
            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                <ContentTemplate>
                    <div id="lblSuccess" class="cls_label" style="float:left; display:none; color:Green;font-weight:bold; margin-right:20px">Route sequence successfully saved!</div>
                    <table id="SummTable" border="0" cellpadding="0" cellspacing="0" width="250px">
                        
                        <tr>
                            <td>
                                <asp:Panel ID="pnlSysMesg" runat="server" Visible="false" >
                                <fieldset>
                                    <div class="cls_label"> 
                                        <img src="../../../images/Close.png" /> <b>System Message:</b><br />
                                        <asp:Label ID="lblMsg" runat="server" Text="" CssClass="cls_validator"></asp:Label>
                                    </div>
                                </fieldset></asp:Panel>
                                <br />
                            </td>
                             
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <div class="bckgroundTitleBar" style="width:247px"><font class="cls_label"><b>Total distance (meter): </b></font></div>
                                    <div class="ctrlPanel">
                                        <asp:Label ID="lblDistance" runat="server" CssClass="cls_label" Text="-"></asp:Label>
                                    </div>
                                    <%--<div id="lblDistance" class="cls_label">-</div>--%>
                                </fieldset>
                            <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset id="pnlStartPoint">
                                <div class="bckgroundTitleBar" style="width:247px">
                                    <span class="cls_label"><b>Pick a starting point:</b></span>
                                </div>    
                                <div class="ctrlPanel">
                                    <asp:DropDownList ID="ddlStartPoint" runat="server" Width="200" AutoPostBack="false" CssClass="cls_dropdownlist">
                                        <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                                    </asp:DropDownList> 
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ValidationGroup="Route" CssClass="cls_validator" Display="Dynamic" ControlToValidate="ddlStartPoint" ></asp:RequiredFieldValidator> 
                                                                   
                                    <%--<asp:DropDownList ID="ddlEndPoint" runat="server" AutoPostBack="false" 
                                        CssClass="cls_dropdownlist" Width="200">
                                        <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                                    </asp:DropDownList>--%>
                                                                   
                                    <asp:Button ID="btnGetRoute" runat="server" Text="Plan Route" cssclass="cls_button" ValidationGroup="Route" />
                                    &nbsp;<asp:Label ID="lblGetRouteErr" runat="server" Text="" CssClass="cls_validator"></asp:Label>
                                </div>
                                </fieldset>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset id="pnlCuzSeq">
                                    <div class="bckgroundTitleBar" style="width:247px">
                                        <div class="cls_label"><b>Define custom route sequence:</b></div>
                                    </div>
                                    <div class="ctrlPanel">
                                        <asp:Button ID="btnRouteSeq" runat="server" Text="Custom Sequence" cssclass="cls_button" OnClientClick="toggleMap('on')" />
                                    </div>
                                </fieldset>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset id="pnlViewPrevRoute" style="display:none" runat="server">
                                    <div class="bckgroundTitleBar" style="width:247px">
                                        <div class="cls_label"><b>Others:</b></div>
                                    </div>
                                    <%--<input id="btnPrevPlan" type="button" class="cls_button" value="Previous Planned Sequence" onclick="OpenPopup();" />--%>
                                    <div class="ctrlPanel">
                                        <asp:Button ID="btnPrevPlan" runat="server" Text="Previous Planned Sequence" cssclass="cls_button"/>
                                    </div>
                                </fieldset>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                               
                            </td>
                        </tr>
                    </table>
                 </ContentTemplate>
             </asp:UpdatePanel>
                <input id="btnSave" type="button" value="Save" class="cls_button" onclick="Save()" />
                <input id="btnPrintRoute" type="button" value="Print" class="cls_button" onclick="OpenPrinter()" disabled="disabled" />
        </div>
        <div id="Message" class="cls_label" style="width:594px;display:none; z-index:30; position:absolute; padding:3px; background:#3C3C3C; color: #ffffff"></div>
    
        <div id="pnlRouteSeq" style="width:720px;display:none; position:absolute; top:120px; left:80px; background-color:#DCDCDC; padding:5px; border: solid 1px #000000">
        <asp:UpdatePanel ID="UpdateRouteSeq" runat="server" UpdateMode="Conditional" RenderMode="inline">
        <ContentTemplate>    
            <input id="btnClose" type="button" value="Close" class="cls_button" onclick="toggleMap('off')" />
            <table border="0" cellpadding="0" cellspacing="10">
                <tr>
                    <td>
                        <asp:Label ID="Label1" cssclass="cls_label" runat="server" Text="Start Point: "></asp:Label>
                        <asp:DropDownList ID="ddlPlanRouteStartPoint" runat="server" Width="300" CssClass="cls_dropdownlist">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label2" cssclass="cls_label" runat="server" Text="End Point: "></asp:Label>
                        <asp:DropDownList ID="ddlPlanRouteEndPoint" runat="server" Width="300" CssClass="cls_dropdownlist">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnAdd" runat="server" Text="Add Path" CssClass="cls_button" />             
                    </td>
                </tr>   
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblRouteSeqErr" runat="server" Text="" CssClass="cls_validator"></asp:Label>
                    </td>
                </tr>  
                <tr>
                    <td colspan="3">
                        <asp:ListBox ID="lboxRoutePlanned" runat="server" Width="600" CssClass="cls_dropdownlist"></asp:ListBox>
                        <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="cls_button" />
                    </td>
                </tr>                  
            </table>
        </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
    <asp:HiddenField ID="hfDistance" runat="server" />
    
    <div id="pnlPictureDiv" class="modalPopup" style="z-index:9001;display: none; padding:10px; width:700px; height:450px; overflow:auto; position:absolute;">
        <input id="btnClosePciture" type="button" value="Close" class="cls_button" OnClick="mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');" />
        <br />
        <span class="cls_label_header">Customer: </span><span id="PicDiv_CustName" class="cls_label"></span><br />
        <br />
        
        <div id="ImgLoadFailMsg" class="cls_label" style="display:none">Unable to load customer picture. This can be due to no image has been uploaded for this customer, or the image file has been removed. </div>
        <img id="CustImg" src="../../../images/indicator.gif" alt='Loading' onerror='Image_OnErrorHide();' />
    </div>
    <div id="divModalMask2" style="position:absolute; left:0; top:0; z-index: 8000; display:none; background-color:#fff"></div>
    </form>
          
    
    
</body>


</html>
