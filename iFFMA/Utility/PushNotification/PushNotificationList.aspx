<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PushNotificationList.aspx.vb" Inherits="iFFMA_Utility_PushNotificationList" %>

<%@ Register TagPrefix="wuctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="wuclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="wuclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="wucUpdateProgress" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="wucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Salesrep List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css?parameter=1">
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--%>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {
            $("#ddlAction").change(function () {
                //alert(this.value);
                if (this.value == 'L') {
                    $("#urlColumn,#urlLblColumn").css('display', '');
                }
                else {
                    $("#urlColumn,#urlLblColumn").css('display', 'none');
                }
                //
            });
        });

        function CheckAllEmp(Checkbox) {
            var GridVwHeaderChckbox = document.getElementById("<%=dgSalesrepList.ClientID %>");
            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
            }
        }

        function clearText() {
            $("#txtMsg").val("");
        }

        function showCreateMessage() {
            $("#edit").modal('show');
        }
        function showSalesrepList() {
            $("#div_dgSalesrepList").css("height", "300px");
            $("#divSalesrep").modal('show');
        }
        function showHistoryList() {
            $("#div_dgHistoryList").css("height", "300px");
            $("#divHistory").modal('show');
        }
    </script>

    <style type="text/css">
        .modal-content {
            border-radius: 0;
        }

        #dgSalesrepList > tbody > tr > th:first-child, #dgSalesrepList > tbody > tr > td:first-child {
            text-align: center;
        }
    </style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmPushNotificationList" runat="server">
        <div>
            <table id="tbl1" cellspacing="0" cellpadding="0" width="98%" border="0">
                <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td>
                </tr>
                <tr>
                    <td class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport" style="    margin-left: 10px;">
                            <tr>
                                <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                                <td colspan="3">
                                    <wuclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                </td>
                                <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                            </tr>
                            <tr>
                                <td class="BckgroundBenealthTitle" colspan="3" height="5"></td>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td valign="top" class="Bckgroundreport">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr class="Bckgroundreport">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="5%">
                                                <asp:Label ID="lblSearch" runat="server" CssClass="cls_label_header">Team</asp:Label></td>
                                            <td width="2%">
                                                <asp:Label ID="lblDot1" runat="server" CssClass="cls_label_header">:</asp:Label></td>
                                            <td style="width: 529px">
                                                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblSearchValue" runat="server" CssClass="cls_label_header">Search Value</asp:Label></td>
                                            <td>
                                                <asp:Label ID="lblDot" runat="server" CssClass="cls_label_header">:</asp:Label></td>
                                            <td style="width: 529px">
                                                <asp:TextBox ID="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                            </tr>
                            <tr class="Bckgroundreport">
                                <td align="left" colspan="3">

                                    <asp:Button ID="btnCreate" runat="server" Text="Create Message" CssClass="cls_button" CausesValidation="False"></asp:Button>&nbsp;                        					
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="cls_button" CausesValidation="False" Visible="false"></asp:Button>&nbsp;                        					
                                </td>
                            </tr>

                            <tr>
                                <td class="Bckgroundreport" colspan="3">
                                    <label class="cls_label" runat="server" id="lblMsg" visible="False">No result found.</label>
                                    <%--<uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server" visble="false" ></uc1:wuc_dgpaging>--%>
                                    <ccGV:clsGridView ID="dgList" runat="server"
                                        AutoGenerateColumns="False" Width="98%" FreezeHeader="True"
                                        AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                        FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="ID" RowHighlightColor="AntiqueWhite" Style="margin-top: 10px;">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Select">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkboxSelectAll" runat="server" onclick="CheckAllEmp(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chk_box" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="salesrep_name" HeaderText="Salesrep name" SortExpression="team">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="device_name" HeaderText="Device name" SortExpression="team">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            
                                        </Columns>

                                        <HeaderStyle CssClass="GridHeader" />
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="GridAlternate" />
                                        <RowStyle CssClass="GridNormal" />
                                        <FooterStyle CssClass="GridFooter" />
                                    </ccGV:clsGridView>
                                    <%-- <asp:BoundField DataField="salesrep_code" HeaderText="Sales Rep" ReadOnly="True" SortExpression="salesrep_code">
                                                <itemstyle horizontalalign="Center" />
                                            </asp:BoundField>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div class="modal fade" id="divSalesrep" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearText();"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="H1">Send Message</h4>
                    </div>
                    <div class="modal-body" style="display: inline-block; width: 100%;">
                        <div>
                            <ccGV:clsGridView ID="dgSalesrepList" runat="server"
                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True"
                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="salesrep_code" RowHighlightColor="AntiqueWhite" Style="margin-top: 10px;">
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkboxSelectAll" runat="server" onclick="CheckAllEmp(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chk_box" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="salesrep_name" HeaderText="Salesrep" SortExpression="salesrep_name">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>

                                <HeaderStyle CssClass="GridHeader" />
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="GridAlternate" />
                                <RowStyle CssClass="GridNormal" />
                                <FooterStyle CssClass="GridFooter" />
                            </ccGV:clsGridView>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: left; margin-top: 0;">
                        <asp:Button ID="btnSendMsg" runat="server" Text="Send Message" CssClass="cls_button" CausesValidation="False"></asp:Button>&nbsp;  
                        <asp:HiddenField ID="hfSelectedMsgId" runat="server" />     
                        <asp:HiddenField ID="hfSelectedTokenId" runat="server" />                    					
                        <%--<button type="button" class="btn btn-danger btn-lg" style="width: 100%; padding: 0 16px; height: 30px; background-color: #ab1032; font-size: 14px; font-weight: bold;"><span class="glyphicon glyphicon-ok-sign dtl-custom"></span>Create</button>--%>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="divHistory" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearText();"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="H2">History</h4>
                    </div>
                    <div class="modal-body" style="display: inline-block; width: 100%;">
                        <div>
                            <ccGV:clsGridView ID="dgHistoryList" runat="server"
                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True"
                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" RowHighlightColor="AntiqueWhite" Style="margin-top: 10px;">
                                <Columns>
                                    <asp:BoundField DataField="salesrep_name" HeaderText="Salesrep" SortExpression="salesrep_name">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="status" HeaderText="Status" SortExpression="status">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="received_date" HeaderText="Received Date" SortExpression="received_date">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>

                                <HeaderStyle CssClass="GridHeader" />
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="GridAlternate" />
                                <RowStyle CssClass="GridNormal" />
                                <FooterStyle CssClass="GridFooter" />
                            </ccGV:clsGridView>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            <div class="modal-dialog" style="width: 400px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="clearText();"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">New Message</h4>
                    </div>
                    <div class="modal-body" style="display: inline-block; width: 100%;">
                        <div>
                            <table style="width: 100%">
                                <tr>
                                    <td><label>Title:</label></td>
                                </tr>
                                <tr>
                                    <td><asp:TextBox runat="server" ID="txtTitle" /></td>
                                </tr>
                                <tr>
                                    <td><br />
                                        <label>Action:</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <asp:DropDownList runat="server" ID="ddlAction" AutoPostBack="false">
                                        <asp:ListItem Value="M" Text="Message" />
                                        <asp:ListItem Value="L" Text="Link" />
                                        <asp:ListItem Value="A" Text="Upload" />
                                    </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="urlLblColumn" style="display:none">
                                    <td><br /><label>URL:</label></td>
                                </tr>
                                <tr id="urlColumn" style="display:none">
                                    
                                    <td><asp:TextBox runat="server" ID="txtURL"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td><br /><label>Message:</label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea runat="server" id="txtMsg" style="width: 100%; height: 150px"></textarea></td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <label>Expiry days:</label>
                                        <asp:DropDownList runat="server" ID="ddlExpiryDays" AutoPostBack="false" style="width:50px">
                                            <asp:ListItem Value="28" Text="28" selected="true" />
                                            <asp:ListItem Value="1" Text="1" />
                                            <asp:ListItem Value="2" Text="2" />
                                            <asp:ListItem Value="3" Text="3" />
                                            <asp:ListItem Value="4" Text="4" />
                                            <asp:ListItem Value="5" Text="5" />
                                            <asp:ListItem Value="6" Text="6" />
                                            <asp:ListItem Value="7" Text="7" />

                                            <asp:ListItem Value="8" Text="8" />
                                            <asp:ListItem Value="9" Text="9" />
                                            <asp:ListItem Value="10" Text="10" />
                                            <asp:ListItem Value="11" Text="11" />
                                            <asp:ListItem Value="12" Text="12" />
                                            <asp:ListItem Value="13" Text="13" />
                                            <asp:ListItem Value="14" Text="14" />
                                            <asp:ListItem Value="15" Text="15" />
                                            <asp:ListItem Value="16" Text="16" />
                                            <asp:ListItem Value="17" Text="17" />
                                            <asp:ListItem Value="18" Text="18" />
                                            <asp:ListItem Value="19" Text="19" />
                                            <asp:ListItem Value="20" Text="20" />
                                            <asp:ListItem Value="21" Text="21" />
                                            <asp:ListItem Value="22" Text="22" />
                                            <asp:ListItem Value="23" Text="23" />
                                            <asp:ListItem Value="24" Text="24" />
                                            <asp:ListItem Value="25" Text="25" />
                                            <asp:ListItem Value="26" Text="26" />
                                            <asp:ListItem Value="27" Text="27" />
                                            
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer" style="text-align: left; margin-top: 0;">
                        <asp:Button ID="btnCreateMsg" runat="server" Text="Send message" CssClass="cls_button" CausesValidation="False"></asp:Button>&nbsp;                        					
                        <%--<button type="button" class="btn btn-danger btn-lg" style="width: 100%; padding: 0 16px; height: 30px; background-color: #ab1032; font-size: 14px; font-weight: bold;"><span class="glyphicon glyphicon-ok-sign dtl-custom"></span>Create</button>--%>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </form>
</body>
</html>














