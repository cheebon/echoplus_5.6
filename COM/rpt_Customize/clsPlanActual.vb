﻿Option Explicit On
Option Strict On


Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPlanActual

    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPlanActual"
        End Get
    End Property

    Public Function GetPlanActual(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strYear As String, _
     ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsIntDB As clsDB
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_PLAN_ACT_BY_MONTH"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("CLASS", strClass, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFPrdFreqList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try

    End Function



#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class
