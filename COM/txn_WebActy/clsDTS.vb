'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsDTS
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDTS"
        End Get
    End Property

    Public Sub InsertDailyTimeSumm(ByVal StrSalesrepCode As String, ByVal StrCustCode As String, ByVal StrContCode As String, ByVal StrCallDate As String, ByVal StrTimeIn As String, _
    ByVal StrVisitId As String, ByVal StrTxnStatus As String, ByVal StrUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_DAILY_TIME_SUMM_SUBMIT"
            .addItem("SALESREP_CODE", StrSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", StrCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", StrContCode, clsDB.DataType.DBString)
            .addItem("CALL_DATE", StrCallDate, clsDB.DataType.DBString)
            .addItem("TIME_IN", StrTimeIn, clsDB.DataType.DBString)
            .addItem("VISIT_ID", StrVisitId, clsDB.DataType.DBString)
            .addItem("TXN_STATUS", StrTxnStatus, clsDB.DataType.DBString)
            .addItem("USERID", StrUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub UpdateDailyTimeSumm(ByVal StrSalesrepCode As String, ByVal StrCustCode As String, ByVal StrContCode As String, ByVal StrCallDate As String, ByVal StrTimeOut As String, _
    ByVal StrVisitId As String, ByVal StrTxnStatus As String, ByVal strReasonCode As String, ByVal strRemarks As String, ByVal StrUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_DAILY_TIME_SUMM_UPDATE"
            .addItem("SALESREP_CODE", StrSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", StrCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", StrContCode, clsDB.DataType.DBString)
            .addItem("CALL_DATE", StrCallDate, clsDB.DataType.DBString)
            .addItem("TIME_OUT", StrTimeOut, clsDB.DataType.DBString)
            .addItem("VISIT_ID", StrVisitId, clsDB.DataType.DBString)
            .addItem("TXN_STATUS", StrTxnStatus, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", StrUserId, clsDB.DataType.DBString)
            .spUpdate()
        End With
        objDB = Nothing
    End Sub


    Public Function GetDTSReasonCode(ByVal strSalesRepCode As String,ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_DTS_REASON_CODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function


#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
