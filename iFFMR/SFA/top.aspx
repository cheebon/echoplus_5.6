<%@ Page Language="vb" AutoEventWireup="false" Inherits="top" CodeFile="top.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Top</title>
<link href="../../include/DKSHMenu.css" rel="stylesheet" />
<style type="text/css">
.blue #waxcontainer{position:relative;height:48px;background:#69BEEE;width:100%;font-family:"Trebuchet MS",Arial,Verdana,Helvitica,sans-serif;}
.blue #waxnav{position:relative;height:25px;font-size:11px;font-weight:bold;background:#fff url(images/menu/bluewax_bottom.gif) repeat-x bottom left;}
.blue #waxnav ul{margin:0px;padding:0 0 4px 0;list-style-type:none;width:auto;float:left;}
.blue #waxnav ul li{display:block;float:left;margin:0 1px;}
.blue #waxnav ul li a{display:block;float:left;color:#003858;text-decoration:none;padding:0 0 0 12px;height:21px;line-height:195%;}
.blue #waxnav ul li a span{padding:0 12px 0 0;}
.blue #waxnav ul li a:hover,.blue #waxnav ul li a.selectedMenu{color:#fff;background:#1D6893 url(images/menu/bluewax_navtopleft.gif) no-repeat top left;}
.blue #waxnav ul li a:hover span,.blue #waxnav ul li a.selectedMenu span{display:block;width:auto;background:#1D6893 url(images/menu/bluewax_navtopright.gif) no-repeat top right;}
</style>
<script type="text/javascript" language="JavaScript">
function clock() {if (!document.layers && !document.all) {return;}var digital = new Date();var fdate = digital.toLocaleDateString(); var dateArray=fdate.split(","); var day = dateArray[0]; var ddateArray = dateArray[1].split(" "); var date = ddateArray[2];var month = ddateArray[1];var year = dateArray[2]; var hours = digital.getHours();var minutes = digital.getMinutes();var seconds = digital.getSeconds();var amOrPm = "AM";if (hours > 11) {amOrPm = "PM";}if (hours > 12) {hours = hours - 12;}if (hours == 0) {hours = 12;}if (minutes <= 9) {minutes = "0" + minutes;}if (seconds <= 9) {seconds = "0" + seconds;} dispTime = day + ": " + date + " " + month + " " + year + " " + hours + ":" + minutes + ":" + seconds + " " + amOrPm;if (document.layers) {document.layers.pendule.document.write(dispTime);document.layers.pendule.document.close();}else if (document.all){pendule.innerHTML = dispTime;}setTimeout("clock()", 1000);}
function setClass(element){if(element){var pElement = element.parentElement.parentElement;for(var i=0;i<pElement.children.length;i++){var innerElement= pElement.children[i].children[0];if(innerElement){if(innerElement==element){innerElement.className='selectedMenu';}else{innerElement.className='';}}}}}
function urlChange(URL){self.parent.frames['fraMain'].NavigatePage(URL);}
setTimeout("clock()", 100);
</script>
</head>
<body class="BckgroundInside">
    <form id="frmTop" method="post" runat="server">
    <div>
        <div style="position:absolute; top: 08px; font-size: small; color: black; font-family: Franklin Gothic Medium, Arial;">
            <span class="greeting" style="float: left">Welcome
                <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
            </span>
            <div style="float: right">
                <span id="pendule" class="greeting" style="right:320px"></span>&nbsp;&nbsp;
                <asp:LinkButton ID="lnkProfile" runat="server" CssClass="menuL1P">EDIT PROFILE&nbsp;<IMG src="../../images/ico_editprofile.gif" border="0" align="absbottom">|</asp:LinkButton>
                <asp:LinkButton ID="lnkLogout1" runat="server" CssClass="menuL1P">LOGOUT&nbsp;<img src="../../images/logout.gif" border="0" align="absbottom"></asp:LinkButton>
            </div>
        </div>
        <div style="height:29px"></div>
         <div style="top: 0px; left: 0px; width: 100%; background: url('../../images/ffms_logo.gif');height: 65px;"></div>
       <div class="blue">
            <div id="waxcontainer">
                <div id="waxnav">
                    <ul>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_ATCHART, 1) Then%>
                        <li><a class="selectedMenu" href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/ATChart.aspx');" title="At Chart"><span>AT Chart</span></a> </li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_PERF_M, 1) Then%>    
                        <li><a href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SRPerf.aspx?IND=M');" title="Performance" ><span>Performance</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_PERF_Q, 1) Then%>    
                        <li><a href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SRPerf.aspx?IND=Q');" title="Quater Performance"><span>Quater Perf.</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_PERF2_Q, 1) Then%>    
                        <li><a href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SRPerf2.aspx?IND=Q');" title="Quater Performance"><span>Quater Perf. 2</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_SALESBYAGENCY, 1) Then%>    
                        <li><a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SalesByAgency.aspx');" title="Actual vs Target"><span>Actual Sales</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_SALESBYCUST, 1) Then%>    
                        <li><a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SalesByCust.aspx');" title="Sales by Customer"><span>Customer Sales</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_SALESBYPRODUCT, 1) Then%>    
                        <li><a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SalesByPrd.aspx');" title="Sales by Product"><span>Product Sales</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_SALESBYCHANNEL, 1) Then%>    
                        <li><a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SalesByChannel.aspx');" title="Sales by Channel"><span>Channel Sales</span></a></li>
                    <%  End If%>
                    <%  If GetAccessRight(ModuleID.FFMR, SubModuleType.SFA_SALESBYREGION, 1) Then%>     
                        <li><a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/SalesByRegion.aspx');" title="Sales by Region"><span>Region Sales</span></a></li>
                    <%  End If%>
                       
                        <li><%--<a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/ATChart.aspx');" title="DPF Detail"><span>DPF Detail</span></a>--%></li>
                    
                        <li><%--<a  href="#" target="" onclick="setClass(this);urlChange('../../iFFMR/SFA/RPT/ATChart.aspx');" title="Customer Profile"><span>Customer Profile</span></a>--%></li>
                   
                    </ul>
                </div>
            </div>
</div>
</div>
    </form>
</body>
</html>
