Imports System.Data
Imports System.Web

Partial Class CallCoverage2
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _aryAllColumnName As ArrayList
    Private _licCustomHeaderCollector As ListItemCollection

    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_CACOVERAGE2")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_CACOVERAGE2") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CallCoverage2"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'Try
        If Not IsPostBack Then
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CALLCOV2) '"Call Coverage 2"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CALLCOV2
                .DataBind()
                .Visible = True
                .blnMakeGroupingFiledDistinct = True
                .intListBoxIndexToFilter = 0
            End With

            GroupingValue = ""

            Dim intMonth As Integer = CInt(Session.Item("Month"))
            TimerControl1.Enabled = True
        End If
        lblErr.Text = ""
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        '      Try
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
        ' Try
        'ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        '   Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        '  Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub GroupingFieldChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.GroupingFieldChanged
        '  Try
        GroupingValue = String.Empty
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        '  Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'wuc_ctrlpanel.ExportToFile(dgList, "CallCoverage2")
        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        '  Try
        '  If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        ' ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
        End If

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        ElseIf dtCurrentTable.Rows.Count = 0 Then
            dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        'End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        '   Try
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Dim aryAllColumnName As ArrayList = GenerateAllColumnName(dtToBind)

        '   Try
        'Add Data Grid Columns
        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
            Select Case CF_CACOVERAGE2.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                Case FieldColumntype.InvisibleColumn
                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_CACOVERAGE2.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CACOVERAGE2.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CACOVERAGE2.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        aryDataItem = _aryDataItem
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'End Try
    End Sub
    
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        '   Try
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '  Try
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        li.Value = SUM(li.Value, DataBinder.Eval(e.Row.DataItem, li.Text))
                    End If
                Next
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString(li.Text), ConvertToDouble(li.Value))
                    End If
                Next

                If (aryDataItem.IndexOf("ACH_A")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_A")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("ACH_A"), (DIVISION(licItemFigureCollector.FindByText("VISIT_A"), licItemFigureCollector.FindByText("CUST_A")) * 100))
                If (aryDataItem.IndexOf("ACH_B")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_B")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("ACH_B"), (DIVISION(licItemFigureCollector.FindByText("VISIT_B"), licItemFigureCollector.FindByText("CUST_B")) * 100))
                If (aryDataItem.IndexOf("ACH_C")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_C")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("ACH_C"), (DIVISION(licItemFigureCollector.FindByText("VISIT_C"), licItemFigureCollector.FindByText("CUST_C")) * 100))
                If (aryDataItem.IndexOf("ACH_OTH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_OTH")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("ACH_OTH"), (DIVISION(licItemFigureCollector.FindByText("VISIT_OTH"), licItemFigureCollector.FindByText("CUST_OTH")) * 100))
                If (aryDataItem.IndexOf("ACH_TTL")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("ACH_TTL")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("ACH_TTL"), (DIVISION(licItemFigureCollector.FindByText("VISIT_TTL"), licItemFigureCollector.FindByText("CUST_TTL")) * 100))

                If (aryDataItem.IndexOf("HIT_A")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("HIT_A")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("HIT_A"), (DIVISION(licItemFigureCollector.FindByText("CALL_A"), licItemFigureCollector.FindByText("CUST_A")) * 100))
                If (aryDataItem.IndexOf("HIT_B")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("HIT_B")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("HIT_B"), (DIVISION(licItemFigureCollector.FindByText("CALL_B"), licItemFigureCollector.FindByText("CUST_B")) * 100))
                If (aryDataItem.IndexOf("HIT_C")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("HIT_C")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("HIT_C"), (DIVISION(licItemFigureCollector.FindByText("CALL_C"), licItemFigureCollector.FindByText("CUST_C")) * 100))
                If (aryDataItem.IndexOf("HIT_OTH")) > 0 Then e.Row.Cells(aryDataItem.IndexOf("HIT_OTH")).Text = String.Format(CF_CACOVERAGE2.GetOutputFormatString("HIT_OTH"), (DIVISION(licItemFigureCollector.FindByText("CALL_OTH"), licItemFigureCollector.FindByText("CUST_OTH")) * 100))

        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        ' Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        '  Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        '  Try
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")

        Dim strSalesRepList As String
        strSalesRepList = Session("SALESREP_LIST")

        'Dim strPointerText As String
        'strPointerText = IIf(strTeamCode.Length > 0, "Sales Rep.", "Team")
        'If wuc_ctrlPanel.pnlField_IsPointerExist(0) = False Then
        '    wuc_ctrlPanel.pnlField_InsertPointer(True, 0, strPointerText)
        'End If

        'Dim strGroupField As String = IIf(String.IsNullOrEmpty(ViewState("GroupingValue")), wuc_ctrlpanel.pnlField_GetGroupingValue, ViewState("GroupingValue"))

        ''select group field to show
        'If Not String.IsNullOrEmpty(strTeamCode) Then
        '    If Not String.IsNullOrEmpty(strRegionCode) Then
        '        'group by salesrep
        '        strGroupField = strGroupField.Replace("@POINTER@", "SALESREP_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Sales Rep.")
        '    Else
        '        'Group by Region
        '        strGroupField = strGroupField.Replace("@POINTER@", "REGION_CODE")
        '        wuc_ctrlpanel.pnlField_EditPointerText("Region")
        '    End If
        'Else
        '    'group by Team
        '    strGroupField = strGroupField.Replace("@POINTER@", "TEAM_CODE")
        '    wuc_ctrlpanel.pnlField_EditPointerText("Team")
        'End If

        Dim clsCallDB As New rpt_CALL.clsCallQuery
        DT = clsCallDB.GetCallCoverage2(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, ddlReportType.SelectedValue)
        PreRenderMode(DT)
        'dgList_Init(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '   Try
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_ItemFigureCollector()
        Cal_CustomHeader()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Sub Cal_CustomHeader()
        '   Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            'If strColumnName Like "TARGET_CALLS*" Then
            If strColumnName Like "CUST_*" Then
                strColumnName = "Customer"
            ElseIf strColumnName Like "VISIT_*" Then
                strColumnName = "Coverage" '"Visit"
            ElseIf strColumnName Like "CALL_*" Then
                strColumnName = "Hit"
            ElseIf strColumnName Like "ACH_*" Then
                strColumnName = "Coverage Rate %"  '"Visit Rate %"
            ElseIf strColumnName Like "HIT_*" Then
                strColumnName = "Hit Rate %"
            Else
                strColumnName = CF_CACOVERAGE2.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_CustomHeader : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Sub Cal_ItemFigureCollector()
        'used to get the collection list of column that need to sum the figure
        '  Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            Select Case strColumnName
                Case "CUST_A", "CUST_B", "CUST_C", "CUST_OTH", "CUST_TTL", _
                     "VISIT_A", "VISIT_B", "VISIT_C", "VISIT_OTH", "VISIT_TTL", _
                     "CALL_A", "CALL_B", "CALL_C", "CALL_OTH"
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
            End Select
        Next

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        '  Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        ' Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '      Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub

End Class

Public Class CF_CACOVERAGE2
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "CUST_A", "VISIT_A", "ACH_A", "CALL_A", "HIT_A"
                strFieldName = "A"
            Case "CUST_B", "VISIT_B", "ACH_B", "CALL_B", "HIT_B"
                strFieldName = "B"
            Case "CUST_C", "VISIT_C", "ACH_C", "CALL_C", "HIT_C"
                strFieldName = "C"
            Case "CUST_OTH", "VISIT_OTH", "ACH_OTH", "CALL_OTH", "HIT_OTH"
                strFieldName = "OTH"
            Case "CUST_TTL", "VISIT_TTL", "ACH_TTL"
                strFieldName = "Total"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '       Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If
        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        ' Try
        Select Case strColumnName.ToUpper
            Case "ACH_A", "ACH_B", "ACH_C", "ACH_OTH", "ACH_TTL", "HIT_A", "HIT_B", "HIT_C", "HIT_OTH"
                strStringFormat = "{0:0.0}"
            Case "CUST_A", "CUST_B", "CUST_C", "CUST_OTH", "CUST_TTL", _
                 "CALL_A", "CALL_B", "CALL_C", "CALL_OTH", "CALL_TTL", _
                 "VISIT_A", "VISIT_B", "VISIT_C", "VISIT_OTH", "VISIT_TTL"
                strStringFormat = "{0:#,0}"
            Case Else
                strStringFormat = ""
        End Select
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        '    Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        ' End Try
        Return CS
    End Function
End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    BoundColumn_V2 = 6
'    InvisibleColumn = 99
'End Enum