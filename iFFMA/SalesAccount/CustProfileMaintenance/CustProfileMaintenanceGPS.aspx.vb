﻿
Imports System.Data
Imports mst_SalesAccount
Imports System.IO
Imports System.Configuration

Partial Class iFFMA_SalesAccount_CustProfileMaintenance_CustProfileMaintenanceGPS
    Inherits System.Web.UI.Page
    Public Property CustCode() As String
        Get
            Return ViewState("CustCode")
        End Get
        Set(ByVal value As String)
            ViewState("CustCode") = value
        End Set
    End Property
    Public Property UploadedFlag() As Boolean
        Get
            Return ViewState("UploadedFlag")
        End Get
        Set(ByVal value As Boolean)
            ViewState("UploadedFlag") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            'ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            CustCode = IIf(IsNothing(Request.QueryString("CustCode").Trim), "", Request.QueryString("CustCode").Trim)
            TimerControl1.Enabled = True
        End If
        lblValidInfo.Text = ""
    End Sub



#Region "GPS"
    Private Sub BindCustGPS(ByVal strCustCode As String)
        Dim objCust As clsCustProfileMaintain
        Dim dt As DataTable
        Try
            objCust = New clsCustProfileMaintain
            dt = objCust.GetCustGPS(strCustCode)

            txtLatitude.Text = ""
            txtLongitude.Text = ""

            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("LATITUDE").ToString <> "" Or dt.Rows(0)("LONGITUDE").ToString <> "" Then

                        lblCustMsg.Text = "Update " & dt.Rows(0)("CUST_NAME").ToString & " GPS Coordinate: <br />"
                        lblCustMsg.Text += "Current Coordinate: <a href='javascript:DropMarker(" & dt.Rows(0)("LATITUDE").ToString & ", " & dt.Rows(0)("LONGITUDE").ToString & ", 10);'>" & dt.Rows(0)("LATITUDE").ToString & ", " & dt.Rows(0)("LONGITUDE").ToString & "</a>"

                        UploadedFlag = True

                        If IsNumeric(dt.Rows(0)("LATITUDE").ToString) And IsNumeric(dt.Rows(0)("LONGITUDE").ToString) Then
                            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(Page), "PlotMarker", "DropMarker(" & dt.Rows(0)("LATITUDE").ToString & ", " & dt.Rows(0)("LONGITUDE").ToString & ", 10)", True)
                       
                        End If
                    Else
                        lblCustMsg.Text = "Update " & dt.Rows(0)("CUST_NAME").ToString & " GPS Coordinate: <br />"
                        lblCustMsg.Text += "No Current Coordinate. "

                        UploadedFlag = False
                    End If

                    EmbedConfirmation(UploadedFlag)
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            updPnlMaintenanceCustomerGPS.Update()
        End Try
    End Sub
    Private Function InjectRefreshPage() As Boolean
        Try
            If Not btnback.Attributes("onclick").Contains("InvokeRefresh();") Then
                btnback.Attributes("onclick") = btnback.Attributes("onclick") + "InvokeRefresh();"
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub EmbedConfirmation(ByVal blnFlag As Boolean)
        Select Case blnFlag
            Case True
                btnSave.OnClientClick = "return confirm('This customer has an existing valid coordinate. Saving new coordinate will replace it. Do you wish to continue?')"
            Case False
                btnSave.OnClientClick = "return confirm('System will save the coordinate as valid coordinate for this customer. Do you wish to continue?')"
        End Select

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            
            If ValidateCoordinate() Then
                SaveCustGPS(CustCode, txtLatitude.Text.Trim, txtLongitude.Text.Trim)
                InjectRefreshPage()
                BindCustGPS(CustCode)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Function ValidateCoordinate() As Boolean
        Try
            If txtLatitude.Text.Trim = "" Or txtLongitude.Text.Trim = "" Then
                lblValidInfo.Text = "Latitude and Longitude must not be empty."

                Return False
            ElseIf Not (IsNumeric(txtLatitude.Text.Trim) And IsNumeric(txtLongitude.Text.Trim)) Then
                lblValidInfo.Text = "Latitude and Longitude must be numeric."

                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub SaveCustGPS(ByVal strCustCode As String, ByVal strLatitude As String, ByVal strLongitude As String)
        Dim objCust As clsCustProfileMaintain
        Try
            objCust = New clsCustProfileMaintain
            objCust.SaveCustGPS(strCustCode, strLatitude, strLongitude)
        Catch ex As Exception
            Throw
        Finally
            objCust = Nothing
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then
            TimerControl1.Enabled = False

            BindCustGPS(CustCode)
        End If
    End Sub
End Class
