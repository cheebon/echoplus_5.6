'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	15/09/2006
'	Purpose	    :	List product info by search criteria 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On
Imports System.data
Partial Class ContactList
    Inherits System.Web.UI.Page

    Private Shared intPageSize As Integer

    Public ReadOnly Property PageName() As String
        Get
            Return "ContactList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        'Call Paging
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
            .DataBind()
            '.Visible = True
        End With

        'lblErr.Text = ""
        Page.SetFocus(btnSearch)
        Try
            If Not IsPostBack Then
                Dim strCatGrp As String = Trim(Request.QueryString("CATEGORY"))
                If Not String.IsNullOrEmpty(strCatGrp) Then
                    Dim strJavaScript As New System.Text.StringBuilder
                    strJavaScript.AppendLine("<script type=""text/javascript"" language=""JavaScript""> ")
                    strJavaScript.AppendLine("function PickCodeUom(code, name) { ")
                    strJavaScript.AppendLine("		    origcode = /\^/gi; ")
                    strJavaScript.AppendLine("		    newname = name.replace(origcode, ""'""); ")
                    strJavaScript.AppendLine("        if (window.opener && !window.opener.closed)")
                    strJavaScript.AppendLine("        {")
                    'If strCatGrp.ToUpper = "SALES" Then
                    strJavaScript.AppendLine("		    window.opener.CallEnq_updateContactInfo(code, newname );")
                    'Else 'CALL
                    'strJavaScript.AppendLine("		    window.opener.CallEnq_updateProductInfo(code, newname );")
                    'End If

                    strJavaScript.AppendLine("		}")
                    strJavaScript.AppendLine("		    //close the window")
                    strJavaScript.AppendLine("		    window.close();")
                    strJavaScript.AppendLine("	    } ")
                    strJavaScript.AppendLine("    </script>")
                    Me.ltr_ScriptPlaceholder.Text = strJavaScript.ToString
                    'ClientScript.RegisterStartupScript(Me.GetType, "PickCode", strJavaScript.ToString)
                End If

            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDatabase(Optional ByVal strCriteriaName As String = "", Optional ByVal strCriteriaValue As String = "")
        'Dim clsProductList As rpt_Product.clsProductQuery = Nothing
        Dim clsContactList As rpt_Contact.clsContactQuery = Nothing
        Dim dt As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)

        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dt Is Nothing Then
                clsContactList = New rpt_Contact.clsContactQuery
                dt = clsContactList.GetSearchResultDT(strCriteriaName, strCriteriaValue)
                ViewState("dtCurrentView") = dt
                ViewState("strSortExpression") = Nothing
            End If

            Dim dv As New Data.DataView(dt)
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                dv.Sort = strSortExpression
            End If

            dgList.DataSource = dv
            dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
            End With

            If dgList.Rows.Count = 0 Then
                'Record Not Found
                'Panel_RecordNotFound.Visible = True
                'Wuc_pnlRecordNotFound1.ShowPanel = True
                wuc_dgpaging.Visible = False
                'dgList.Visible = False
            Else
                ''Show records
                ''Panel_RecordNotFound.Visible = False
                'Wuc_pnlRecordNotFound1.ShowPanel = False
                wuc_dgpaging.Visible = True
                'dgList.Visible = True
            End If
            'UpdatePanel_RecprdNotFound.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadDatabase : " & ex.ToString)
        Finally
            clsContactList = Nothing
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            ViewState("dtCurrentView") = Nothing
            With ddlSearchCriteria
                LoadDatabase(.SelectedValue, txtSearchCriteria.Text)
            End With
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnSearch_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Try
            If e.CommandName.ToUpper = "SELECT" Then
                Dim strContCode As String = CStr(e.CommandArgument)
                Dim dtMasterTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
                Dim DR As DataRow() = dtMasterTable.Select(dgList.DataKeyNames(0) & " = '" & Trim(strContCode) & "'")
                If DR IsNot Nothing AndAlso DR(0) IsNot Nothing Then
                    Dim strContName As String = CStr(DR(0).Item("CONT_NAME"))
                    strContName = Trim(strContName.Replace("\", "\\").Replace("""", "\""")).Replace("'", "\'")

                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SelectItem", "<script>PickCodeUom('" & strContCode & "','" & strContName & "');</script>", False)
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCommand : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = CStr(ViewState("strSortExpression"))

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        LoadDatabase()

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            LoadDatabase()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            LoadDatabase()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            LoadDatabase()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region



End Class


''---------------------------------------------------------------------
'' Procedure 	    : 	Sub btnSave_Click
'' Purpose	        :	Saves Template information.
'' Calling Methods   :   1) cor_Template.clsTemplate.Create
''                       2) ...
'' Page              :	[from] : 
''   		            [to]   : 
''----------------------------------------------------------------------
'Private Sub btnSave_Click()
'    Try

'    Catch ex As Exception
'        ExceptionMsg(PageName & ".btnSave_Click : " & ex.ToString)
'    Finally
'    End Try
'End Sub