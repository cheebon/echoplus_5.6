﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StkConsumpByBiMth.aspx.vb"
    Inherits="iFFMR_Customize_StkConsump_StkConsumpByBiMth" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc2" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc3" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="uc4" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stock Consumption By Bi Month</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script src="../../../include/layout.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ShowHideElement(strBar) {
            alert('yes');
        }
         
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="frmStkConspByBiMonth" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <fieldset class="" style="width: 98%;">
        <uc3:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uc3:wuc_lblHeader>
        <uc2:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true"></uc2:wuc_ctrlpanel>
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                    <tr>
                        <td> 
                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        </td>
                    </tr> 
                    <tr>
                        <td class="BckgroundInsideContentLayout" align="left">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btnCollapse" Text="Click to Expand or Collapse" runat="server" CssClass="cls_button"
                                            Width="100%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                        <asp:Timer ID="TimerControl1" runat="server" Interval="100" OnTick="TimerControl1_Tick" />
                                        <asp:Panel ID="PnlSearch" runat="server" Visible="true" class="cls_panel_header">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr valign="top">
                                                    <td align="left" style="width: 50%;" valign="top">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 45%;">
                                                                </td>
                                                                <td style="width: 10%;">
                                                                </td>
                                                                <td style="width: 45%;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="center">
                                                                    <span id="lblTeam" class="cls_label_header">Sales Team</span><br>
                                                                    <asp:ListBox ID="lsbTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="280px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddTeam" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveTeam" runat="server" CssClass="cls_button" Width="35" Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllTeam" runat="server" CssClass="cls_button" Width="35" Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllTeam" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedTeam" class="cls_label_header">Selected Sales Team</span><br>
                                                                    <asp:ListBox ID="lsbSelectedTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="280px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="center">
                                                                    <span id="lblProduct" class="cls_label_header">Product</span><br>
                                                                    <asp:ListBox ID="lsbProduct" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="280px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddProduct" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveProduct" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllProduct" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllProduct" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedProduct" class="cls_label_header">Selected Product</span><br>
                                                                    <asp:ListBox ID="lsbSelectedProduct" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="280px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="left" style="width: 50%;" valign="top">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 45%;">
                                                                </td>
                                                                <td style="width: 10%;">
                                                                </td>
                                                                <td style="width: 45%;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="center">
                                                                    <span id="lblSalesrep" class="cls_label_header">Field Force</span><br>
                                                                    <asp:ListBox ID="lsbSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="280px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddSalesrep" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllSalesrep" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedSalesrep" class="cls_label_header">Selected Field Force</span><br>
                                                                    <asp:ListBox ID="lsbSelectedSalesrep" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="280px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblYear" runat="server" Text="Year" CssClass="cls_label_header"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="cls_dropdownlist">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlSearch" runat="server" CollapseControlID="btnCollapse"
                                            ExpandControlID="btnCollapse" TargetControlID="PnlSearch" CollapsedSize="0" Collapsed="false"
                                            ExpandDirection="Vertical" SuppressPostBack="true">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table> 
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport" style=" padding-top:10px;">
                    <tr>
                        <td align="center"  >
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="100%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="False" AllowPaging="False" PagerSettings-Visible="False">
                                <EmptyDataTemplate>
                                    <uc1:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                </EmptyDataTemplate>
                            </ccGV:clsGridView>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </fieldset>
    </form>
</body>
</html>
