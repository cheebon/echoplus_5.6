'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	2/11/2006
'	Purpose	    :	Class to build datatables for getting product details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsProduct
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsProduct"
        End Get
    End Property

    Function productListing(ByVal strWarehouseCode As String, ByVal strPrdGrpCode As String, ByVal strSalesrep_code As String, ByVal strSearchBy As String, ByVal strSearchValue As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDLISTING"
                .addItem("WAREHOUSE_CODE", strWarehouseCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrep_code, clsDB.DataType.DBString)
                .addItem("SEARCH_BY", strSearchBy, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".productListing :" & ex.Message))
        End Try
    End Function

    Function salesmanProductGroup(ByVal strSalesrepCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SLSMANPRDGROUP"
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".salesmanProductGroup :" & ex.Message))
        End Try
    End Function

    Function salesmanWarehouse(ByVal strSalespreCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SLSWAREHOUSE"
                .addItem("SALESREP_CODE", strSalespreCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".salesmanProductGroup :" & ex.Message))
        End Try
    End Function

    Function productInfo(ByVal strItemCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRODUCTINFO"
                .addItem("itemcode", strItemCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".productInfo :" & ex.Message))
        End Try
    End Function

    Function GetPrdDetails(ByVal strPrdCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDDETAIL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdDetails :" & ex.Message))
        End Try
    End Function

    Function GetPrdUOM(ByVal strPrdCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDUOM"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdUOM :" & ex.Message))
        End Try
    End Function

    Function GetPrdEAN(ByVal strPrdCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDEAN"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdEAN :" & ex.Message))
        End Try
    End Function

    Function GetPrdBatch(ByVal strPrdCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDBATCH"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdBatch :" & ex.Message))
        End Try
    End Function

    Function GetPrdStdBonus(ByVal strPrdCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PRDSTDBONUS"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdStdBonus :" & ex.Message))
        End Try
    End Function

#Region "Exception"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region


End Class
