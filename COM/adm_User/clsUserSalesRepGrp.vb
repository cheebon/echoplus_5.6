﻿Imports System.ComponentModel

Public Class clsUserSalesRepGrp
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New clsProperties.clsUserSalesRepGrp

    Public Sub Create()
        Dim objDB As cor_DB.clsDB
        'Dim lngUserSalesRepID As Long

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERSALESREPGRP_CREATE"
                '.addItem("usersalesrep_id", clsProperties.usersalesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SALESREP_GRP_ID", clsProperties.salesrepgrp_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
               
                'lngUserSalesRepID = .spInsert()
                .spInsert()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserSalesRepGrp.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub Delete()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERSALESREPGRP_DELETE"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("CHANGED_USER_ID", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserSalesRepGrp.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub







    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserSalesRepID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
