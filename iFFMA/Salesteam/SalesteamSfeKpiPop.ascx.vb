﻿Imports System.Data

Partial Class iFFMA_Salesteam_SalesteamSfeKpiPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    ''' 
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SfeKpiId() As String
        Get
            Return Trim(hdSfeKpiId.Value)
        End Get
        Set(ByVal value As String)
            hdSfeKpiId.Value = value
        End Set
    End Property

    Public Property Total() As Integer
        Get
            Return Trim(hdTotal.Value)
        End Get
        Set(ByVal value As Integer)
            hdTotal.Value = value
        End Set
    End Property
#End Region
#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        LoadDDLSfeKpiExclude()
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        ''pnlViewMode.Visible = True
        ''pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        ''pnlViewMode.Visible = False
        ''pnlEditMode.Visible = True
        If Total() >= 5 Then
            btnSave.Visible = False
        Else
            btnSave.Visible = True
        End If
        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        ''pnlViewMode.Visible = False
        ''pnlEditMode.Visible = True
        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            Dim SfeKpiDesc As String = String.Empty

            DT = clsSalesteam.GetSalesteamSfeKpiDetails(SfeKpiId, TeamCode)
            If DT.Rows.Count > 0 Then
                SfeKpiDesc = DT.Rows(0).Item("SFE_KPI_DESC")
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                    'KL - No Edit Feature
                Else
                    LoadDDLSfeKpiExclude()
                End If
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty
            Page.Form.Attributes.Add("enctype", "multipart/form-data")

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
                
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = String.Empty
            Dim ddlSfeKpi As DropDownList = CType(DetailsView1.FindControl("ddlSfeKpi"), DropDownList)
            If ddlSfeKpi.SelectedValue = "" Or ddlSfeKpi.SelectedValue = "0" Then
                lblInfo.Text = "SFE KPI is required!"
                Show()
                Exit Sub
            End If

            Dim DT As DataTable
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                'KL - No Edit Feature
            Else
                DT = clsSalesteam.CreateSalesteamSfeKpi(Trim(ddlSfeKpi.SelectedValue), TeamCode)
            End If

            Dim isDuplicate As Integer

            If DT.Rows.Count > 0 Then
                isDuplicate = DT.Rows(0)("IS_DUPLICATE")
            End If

            If isDuplicate = 1 Then
                lblInfo.Text = "The record already exists!"
                Show()
                Exit Sub
            Else
                Total += 1 'KL - Increment by 1 for each successful creation
                lblInfo.Text = "The record is saved successfully."
            End If

            If DetailsView1.CurrentMode = DetailsViewMode.Insert Then
                ResetPage()
                If Total() >= 5 Then
                    btnSave.Visible = False
                Else
                    btnSave.Visible = True
                End If
            End If

            Show()

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub LoadDDLSfeKpiExclude()
        Try
            Dim DT As DataTable
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            Dim ddlSfeKpi As DropDownList = CType(DetailsView1.FindControl("ddlSfeKpi"), DropDownList)
            If ddlSfeKpi IsNot Nothing Then
                DT = clsSalesteam.GetSalesteamSfeKpiExcludeList(TeamCode)
                With ddlSfeKpi
                    .Items.Clear()
                    .DataSource = DT
                    .DataTextField = "SFE_KPI_DESC"
                    .DataValueField = "SFE_KPI_ID"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", "0"))
                    .SelectedIndex = 0
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
