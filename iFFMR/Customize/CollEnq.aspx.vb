Imports System.Data

Partial Class iFFMR_Customize_CollEnq
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            _aryDataItem = value
            ViewState("DataItem") = value
        End Set
    End Property

    Private _licItemFigureCollector As ListItemCollection
    Protected Property licItemFigureCollector() As ListItemCollection
        Get
            If _licItemFigureCollector Is Nothing Then _licItemFigureCollector = ViewState("CollEnq")
            If _licItemFigureCollector Is Nothing Then _licItemFigureCollector = New ListItemCollection
            Return _licItemFigureCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licItemFigureCollector = value
            ViewState("CollEnq") = value
        End Set
    End Property
    Protected Property intPageSize() As Integer
        Get
            Return CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        End Get
        Set(ByVal value As Integer)
            Session("PageSize") = value
        End Set
    End Property

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "Customize_CollEnq.aspx"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.COLLENQ)
            .DataBind()
            .Visible = True
        End With

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
            .DataBind()
            .Visible = True
        End With

        With wuc_ctrlPanel
            .SubModuleID = SubModuleType.COLLENQ
            .DataBind()
            .Visible = True
        End With
        If Not IsPostBack Then

            wuc_ctrlPanel.SearchPanelVisibility = True

            TimerControl1.Enabled = True
        End If

        lblErr.Text = ""
    End Sub
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then BindDefault()
        TimerControl1.Enabled = False
    End Sub

#Region "EVENT HANDLER"
    Sub btnRrefreshButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.CollEnqyBtn_Click
        If Page.IsValid = False Then Exit Sub
        RenewDataBind()
    End Sub

    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.EnqResetBtn_Click
        BindDefault()
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region
#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "CollCheqEnquiry")
            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("strSortExpression") = Nothing
                ViewState("dtCurrentView") = dtCurrentTable
                dgList.PageIndex = 0
            End If
            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New Data.DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.PageSize = GridPageSize.PageSize

            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
            End With

            wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 10 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            aryDataItem.Clear()
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SETTING.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SETTING.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SETTING.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SETTING.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        aryDataItem.Add(ColumnName)
                End Select
            Next
            ViewState("DataItem") = aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Sub BindDefault()
        Try
            wuc_ctrlpanel.SearchPanelVisibility = True
            Dim dt As Data.DataTable = GetRecList("1900-01-01", "1900-01-01", "", "", "", "")
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            With wuc_ctrlpanel
                .UpdateCollEnqSearchDtl()
                DT = GetRecList(.CollEnq_TxnDateFrom, .CollEnq_TxnDateto, .CollEnq_SalesrepCode, .CollEnq_CustomerCode, .CollEnq_SoNo, .CollEnq_InvNo)
            End With

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecList(ByVal strStartDate As String, ByVal strEndDate As String, Optional ByVal strSalesrepCode As String = "", Optional ByVal strCustCode As String = "", Optional ByVal strSoNo As String = "" _
   , Optional ByVal strInvNo As String = "") As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            Dim clsCollEnq As New rpt_Customize.clsCollCheqQuery
            DT = clsCollEnq.GetCollEnqList(strUserID, strPrincipalId, strPrincipalCode, strStartDate, strEndDate, strCustCode, strSalesrepCode, strSoNo, strInvNo)
            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        'Cal_ItemFigureCollector(DT)

        aryDataItem = _aryDataItem
        'licItemFigureCollector = _licItemFigureCollector
    End Sub

    'Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
    '    'used to get the collection list of column that need to sum the figure
    '    Try
    '        licItemFigureCollector = New ListItemCollection
    '        Dim strColumnName As String
    '        Dim liColumnField As ListItem
    '        For Each strColumnName In aryDataItem
    '            strColumnName = strColumnName.ToUpper

    '            If (strColumnName Like "*QTY" OrElse _
    '             strColumnName Like "*AMT") Then
    '                liColumnField = New ListItem(strColumnName, 0)
    '                licItemFigureCollector.Add(liColumnField)
    '            End If
    '        Next

    '        For Each DR As DataRow In DT.Rows
    '            For Each li As ListItem In licItemFigureCollector
    '                li.Value = SUM(li.Value, DR(li.Text))
    '            Next
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Try
            Dim strSortExpression As String = CStr(ViewState("strSortExpression"))

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Paging Control"
    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub
    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub
    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
    Public Class CF_SETTING
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""
            Select Case ColumnName.ToUpper
                Case "ORDER_AMT"
                    strFieldName = "Order Amt."
                Case "TXN_NO"
                    strFieldName = "Txn. No."
                Case "BILL_NO"
                    strFieldName = "Billing No."
                Case "BILL_DATE"
                    strFieldName = "Billing Date"
                Case "JS_INV_DATE"
                    strFieldName = "JS.Inv. Date"
                Case "JS_INV_NO"
                    strFieldName = "JS.Inv. No."
                Case "SO_DATE"
                    strFieldName = "SO. Date"
                Case "PAY_TYPE"
                    strFieldName = "Pay. Type"
                Case "PAY_DATE"
                    strFieldName = "Pay. Date"
                Case Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
            End Select

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
            Try
                Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
                Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
                strColumnName = strColumnName.ToUpper
                Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
                If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
                   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                    FCT = FieldColumntype.InvisibleColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If

                If strColumnName = "BANK_CODE" Then
                    FCT = FieldColumntype.BoundColumn
                End If

                Return FCT
            Catch ex As Exception

            End Try
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strFormatString As String = ""
            Dim strNewName As String = strColumnName.ToUpper
            If strNewName Like "*AMT" Then
                strFormatString = "{0:#,0.00}"
            ElseIf strNewName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            Else
                strFormatString = ""
            End If

            Return strFormatString
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            Try
                With CS
                    Dim strColumnName As String = ColumnName.ToUpper
                    .FormatString = GetOutputFormatString(ColumnName)

                    If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                    OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME_*" Then
                        .HorizontalAlign = HorizontalAlign.Center
                    ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                        .HorizontalAlign = HorizontalAlign.Right
                    Else
                        .HorizontalAlign = HorizontalAlign.Left
                    End If

                End With

            Catch ex As Exception

            End Try
            Return CS
        End Function
    End Class
End Class
