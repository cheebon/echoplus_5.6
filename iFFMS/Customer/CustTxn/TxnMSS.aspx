<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnMSS.aspx.vb" Inherits="iFFMS_Customer_CustTxn_TxnMSS"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Transaction MSS</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">

        function ClickBtnUP() {
            SetScrollPosition();
            __doPostBack('btnRefreshUp');
        }
        function ClickBtnDOWN() {
            SetScrollPosition();
            __doPostBack('btnRefreshDown');
        }
        function SetScrollPosition() {
            var dgListQues = document.getElementById('div_dgListQues');
            if (dgListQues) {
                var scrollPosition = dgListQues.scrollTop;
                var hdfScrollPosition = document.getElementById('hdfScrollPosition');
                //alert(scrollPosition);
                if (hdfScrollPosition) {
                    hdfScrollPosition.value = scrollPosition;
                }
                if (dgListQues) {
                    dgListQues.scrollTop = 0;
                }
            } 
        }


        function GetScrollPosition() {
            var hdfScrollPosition = document.getElementById('hdfScrollPosition');
            if (hdfScrollPosition) {
                var scrollPosition = hdfScrollPosition.value;
            }
            var dgListQues = document.getElementById('div_dgListQues');
            //alert(scrollPosition);
            if (scrollPosition) {
                if (dgListQues) {
                    dgListQues.scrollTop = parseInt(scrollPosition);
                } 
            }
        }
        function GetScrollPosition() {
            var hdfScrollPosition = document.getElementById('hdfScrollPosition');
            if (hdfScrollPosition) {
                var scrollPosition = hdfScrollPosition.value;
            }
            var dgListQues = document.getElementById('div_dgListQues');
            //alert(scrollPosition);
            if (scrollPosition) {
                if (dgListQues) {
                    dgListQues.scrollTop = parseInt(scrollPosition);
                } 
            }
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe')"
    style="background-color: #DDDDDD;">
    <form id="frmtxnsfms" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <fieldset class="" style="width: 98%;">
        <div id="title">
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
        </div>
        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
            width="100%" border="0" style="height: 30px">
            <tr align="left" valign="bottom">
                <td>
                    <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                        CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
            <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Field Force Name</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 300px">
                                        <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Customer Name</span>
                                        <td style="width: 5px">
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td style="width: 300px">
                                            <asp:Label ID="lblCustName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                        </td>
                                        <td style="width: 100px">
                                            <span class="cls_label_header">Contact Name</span>
                                            <td style="width: 5px">
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:Label ID="lblContName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                            </td>
                            </tr>
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Visit ID</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblVisitId" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Map</span>
                                    </td>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </asp:Panel>
        <div id="txnmss">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:Button runat="server" ID="btnHidden" Style="display: none;" Text="" Visible="false" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <asp:UpdatePanel ID="UpdateTitle" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                <asp:Panel ID="pMSSTitle" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div style="width: 99%; position: relative; padding-left: 5px; margin: 0; float: left"
                                        class="S_DivHeader">
                                        Select a title:
                                    </div>
                                    <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                                        padding-bottom: 10px; margin: 0;">
                                        <ccGV:clsGridView ID="dgListTitle" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="100%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TITLE_CODE,TITLE_NAME">
                                            <Columns>
                                                <asp:BoundField DataField="TITLE_CODE" HeaderText="Title Code" ReadOnly="True" SortExpression="TITLE_CODE">
                                                    <ItemStyle HorizontalAlign="left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TITLE_NAME" HeaderText="Title Name" ReadOnly="True" SortExpression="TITLE_NAME">
                                                    <ItemStyle HorizontalAlign="left" />
                                                </asp:BoundField>
                                                <asp:CommandField HeaderText="Select" ShowEditButton="True" ShowHeader="True" EditText="<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:CommandField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateHdr" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Panel ID="pMSSHdr" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div id="Proce" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                                        background-color: Gray">
                                        <span style="float: right;">
                                            <asp:Button ID="btnback" runat="server" Text="<< Back" CssClass="cls_button" CausesValidation="false"
                                                Width="80px" ValidationGroup="MSSHdr" OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree){ResizeFrameWidth('SubDetailBar','100%');HideElement('SubDetailConfBar');return true;}else {return false;}" />
                                            <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="cls_button" ValidationGroup="MSSHdr"
                                                Width="80px" OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree){return true;}else {return false;}" />
                                            <asp:Button ID="btnkiv" runat="server" Text="KIV" CssClass="cls_button" OnClientClick="var agree=confirm('Are you sure you wish to continue?');if(agree)return true;else return false;"
                                                Width="80px" />
                                        </span>
                                    </div>
                                    <div id="dheader" class="S_DivHeader">
                                        General Information
                                    </div>
                                    <div style="width: 100%; position: relative; padding-left: 5px; margin: 0;">
                                        <table class="cls_form_table" width="99.8%">
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Txn No:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lbltxnno" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Title Code:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lbltitlecode" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Title Name:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lbltitlename" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">General Comment:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:TextBox ID="txtgencomment" runat="server" Text="" CssClass="cls_textbox" Width="250px"
                                                        MaxLength="250"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%">
                                                    <div style="display: none">
                                                        <asp:Button ID="btnRefreshUp" Text="" runat="server" CssClass="cls_button" />
                                                        <asp:Button ID="btnRefreshDown" Text="" runat="server" CssClass="cls_button" />
                                                    </div>
                                                    <div style="width: 97%; position: relative; padding-left: 10px; padding-top: 10px;
                                                        padding-bottom: 10px; padding-right: 10px; margin: 0; float: left;">
                                                        <ccGV:clsGridView ID="dgListQues" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                            Width="97%" FreezeHeader="True" GridHeight="250" AddEmptyHeaders="0" CellPadding="2"
                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" SelectedIndex="0"
                                                             DataKeyNames="QUES_CODE">
                                                            <Columns>
                                                                <asp:BoundField DataField="IND" HeaderText="Ind" ReadOnly="True" SortExpression="IND">
                                                                    <ItemStyle HorizontalAlign="left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="QUES_CODE" HeaderText="Question Code" ReadOnly="True"
                                                                    SortExpression="QUES_CODE">
                                                                    <ItemStyle HorizontalAlign="left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="QUES_NAME" HeaderText="Question Name" ReadOnly="True"
                                                                    SortExpression="QUES_NAME">
                                                                    <ItemStyle HorizontalAlign="left" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Select">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="imgButton" runat="server" ImageUrl="../../../images/ico_edit.gif"
                                                                            Height='18px' Width='18px'></asp:Image>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />
                                                        </ccGV:clsGridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    <asp:HiddenField ID="hdfScrollPosition" runat="server" />
    </form>
</body>
</html>
