﻿Option Explicit On

Imports System.Data
Imports adm_SuperAdmin

Partial Class Admin_Root_User_BsadminAccCreate
    Inherits System.Web.UI.Page

#Region "Property"
    Private _PrincipalCodeList As ListItemCollection
    Public Property PrincipalCodeList() As ListItemCollection
        Get
            If _PrincipalCodeList Is Nothing Then _PrincipalCodeList = Session("PrincipalCodeList")
            If _PrincipalCodeList Is Nothing Then _PrincipalCodeList = New ListItemCollection

            Return _PrincipalCodeList
        End Get
        Set(ByVal value As ListItemCollection)
            _PrincipalCodeList = value
            Session("PrincipalCodeList") = value
        End Set
    End Property

    Public Property SelectedPrincipalCode() As String
        Get
            Return ViewState("SelectedPrincipalCode")
        End Get
        Set(ByVal value As String)
            ViewState("SelectedPrincipalCode") = value
        End Set
    End Property

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "BSADMIN Account Create"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            lblErr.Text = ""
            lblMesg.Text = ""
            wuc_lblHeader.Title = "BSADMIN Account Create"

            If Not IsPostBack Then
                PrincipalCodeList = New ListItemCollection
                LoadCountryDDL()
                'RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#Region "Function"
    Private Sub LoadCountryDDL()
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            obj = New adm_SuperAdmin.clsSubmodulePrincipal

            dt = obj.LoadCountryDDL(Session("UserID"))

            With ddlCountry
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "COUNTRY_NAME"
                .DataValueField = "COUNTRY_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With

            With ddlPrincipal
                .Items.Clear()
                .Items.Add(New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub CreateBsadmin()
        Dim principal_code As String
        Dim bs_id As String
        Dim dt As DataTable
        Dim obj As New adm_SuperAdmin.clsBsadmin
        Try
            principal_code = txtLogin.Text.Trim

            If principal_code <> "" Then
                dt = obj.CreateBsadminUser(ddlPrincipal.SelectedValue.Trim, ddlCountry.SelectedValue.Trim, principal_code, txtPassword1.Text.Trim, Session("UserID"))

                If dt.Rows(0)("ISEXISTED").ToString = 0 Then
                    bs_id = dt.Rows(0)("USER_ID").ToString
                    Response.Redirect("BsadminAccDtl.aspx?userid=" & bs_id & "&mode=VIEW")
                Else
                    lblMesg.Text = "Login/UserCode already exists."
                End If
            End If



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Function GenBsadminName(ByVal strPrincipalCode As String) As String
        Dim bsadmin As String
        Try
            bsadmin = strPrincipalCode.Replace("-", "_")
            bsadmin = bsadmin & "_BSADMIN"

            Return bsadmin
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
#Region "Event"
    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            If ddlCountry.SelectedIndex = 0 Then
                ddlPrincipal.Items.Clear()
                ddlPrincipal.Items.Add(New ListItem("-- Select --", ""))
            Else
                obj = New adm_SuperAdmin.clsSubmodulePrincipal
                dt = obj.LoadPrincipalDDL(ddlCountry.SelectedValue, Session("UserID"))

                With ddlPrincipal
                    .Items.Clear()
                    .DataSource = dt
                    .DataValueField = "PRINCIPAL_ID"
                    .DataTextField = "PRINCIPAL_NAME"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- Select --", ""))
                    .SelectedIndex = 0
                End With

                For Each dr As DataRow In dt.Rows
                    PrincipalCodeList.Add(New ListItem(dr("PRINCIPAL_CODE").ToString, dr("PRINCIPAL_ID").ToString))
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub ddlPrincipal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrincipal.SelectedIndexChanged
        Dim principal_code As String
        Dim principal As ListItem
        Try
            principal = PrincipalCodeList.FindByValue(ddlPrincipal.SelectedValue)
            principal_code = principal.Text

            txtLogin.Text = GenBsadminName(principal_code.Trim)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
    
    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            'principal_code = ddlPrincipal.Text.Substring(0, )
            CreateBsadmin()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    
End Class
