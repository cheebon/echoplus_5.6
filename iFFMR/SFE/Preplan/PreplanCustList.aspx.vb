Imports System.Data

Partial Class PreplanCustList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryAllColumnName As ArrayList
    Protected Property aryAllColumnName() As ArrayList
        Get
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = ViewState("AllDataItem")
            If _aryAllColumnName Is Nothing Then _aryAllColumnName = New ArrayList
            Return _aryAllColumnName
        End Get
        Set(ByVal value As ArrayList)
            ViewState("AllDataItem") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property


#End Region


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "PreplanCustList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.PREPLANCUST) '"Preplan List By Customer"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.PREPLANCUST
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""

            If Not IsPostBack Then
                'Param pass from Previous: SALESREP_CODE , DATE , STATUS
                ViewState("dtCurrentView") = Nothing
                'ViewstaeValue : SALESREP_CODE , DATE , STATUS
                ViewState("DATE") = Trim(Request.QueryString("DATE"))
                ViewState("STATUS") = Trim(Request.QueryString("STATUS"))
                
                Dim strNodeName As String
                Dim strNodeValue As String

                strNodeName = Session("NODE_GROUP_NAME")
                strNodeValue = Trim(Request.QueryString("TREE_" + strNodeName))

                Report.UpdateTreePath(strNodeName, strNodeValue)

                wuc_Menu.RestoreSessionState = True

                'RenewDataBind()
                'TimerControl1.Enabled = True
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            wuc_ctrlPanel.RefreshDetails()
            wuc_ctrlPanel.UpdateControlPanel()
            If Not IsPostBack Then
                TimerControl1.Enabled = True
            Else
                RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlPanel.ExportToFile(dgList, "PreplanByCustomer")
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Else
                PreRenderMode(dtCurrentTable)
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Private Function GenerateAllColumnName(ByRef dtToBind As DataTable) As ArrayList
        Dim aryList As New ArrayList
        Try
            For Each DC As DataColumn In dtToBind.Columns
                aryList.Add(DC.ColumnName.ToUpper)
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".GenerateAllColumnName : " & ex.ToString)
        End Try
        Return aryList
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            aryDataItem.Clear()
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case DBPreplanByCust_ColField.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                        dgColumn.ItemStyle.HorizontalAlign = DBPreplanByCust_ColField.ColumnStyle(ColumnName).HorizontalAlign

                        If String.IsNullOrEmpty(DBPreplanByCust_ColField.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = DBPreplanByCust_ColField.GetOutputFormatString(ColumnName)
                        End If

                        dgColumn.HeaderText = DBPreplanByCust_ColField.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        'dgColumn.ItemStyle.HorizontalAlign = CF_PREPLAN.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.ItemStyle.HorizontalAlign = DBPreplanByCust_ColField.ColumnStyle(ColumnName).HorizontalAlign
                        Dim strFormatString As String = DBPreplanByCust_ColField.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If


                        dgColumn.HeaderText = DBPreplanByCust_ColField.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        ''Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
            aryDataItem = _aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'ViewstaeValue : SALESREP_CODE , DATE , STATUS
            Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strStatus, strSelectedDate As String
            strUserID = Session.Item("UserID")
            strPrincipalID = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            strYear = Session.Item("Year").ToString
            strMonth = Session.Item("Month").ToString
            strStatus = ViewState("STATUS")
            strSelectedDate = ViewState("DATE")

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")

            Dim dttSelectedDate As Date
            If Not String.IsNullOrEmpty(strSelectedDate) AndAlso _
            Date.TryParseExact(strSelectedDate, "yyyy-MM-dd", Nothing, System.Globalization.DateTimeStyles.None, dttSelectedDate) _
            Then wuc_ctrlpanel.SelectedDateTimeValue = dttSelectedDate


            Select Case strStatus.ToUpper
                Case "VISITED", "VISITED_CALL"
                    strStatus = "VISITED"
                Case "UNPLAN"
                    strStatus = "UN-PLAN"
                Case "UNVISIT", "UNVISIT_CALL"
                    strStatus = "UN-VISIT"
                Case "PLANNED"
                    strStatus = "PLAN"
                Case Else
                    strStatus = ""
            End Select

            Dim clsPreplanDB As New rpt_Preplan.clsPreplanQuery
            DT = clsPreplanDB.GetPreplanByCust(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, strStatus, strSelectedDate)

            aryAllColumnName = GenerateAllColumnName(DT)
            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim strValue As String = String.Empty
                Dim intIndex As Integer = aryDataItem.IndexOf("STATUS")
                If (intIndex > 0) AndAlso Not e.Row.Cells(intIndex) Is Nothing Then
                    strValue = e.Row.Cells(intIndex).Text
                    Select Case strValue.ToUpper
                        Case "UN-PLAN"
                            e.Row.Cells(intIndex).ForeColor = Drawing.Color.Orange
                        Case "UN-VISIT", "UN-CALL"
                            e.Row.Cells(intIndex).ForeColor = Drawing.Color.Red
                        Case Else
                    End Select
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
End Class

Public Class DBPreplanByCust_ColField
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case "DEPT_CODE" , "DEPARTMENT_CODE"
                strFieldName = "Department Code"
            Case "DEPARTMENT_NAME", "DEPT_NAME"
                strFieldName = "Department"
            Case "STATUS"
                strFieldName = "Status"
            Case "CUST_CLASS"
                strFieldName = "Customer Class"
            Case "CONT_CLASS"
                strFieldName = "Contact Class"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If (strColumnName = "ROUTE_CODE") OrElse (strColumnName = "REGION_CODE") OrElse (strColumnName = "TEAM_CODE") Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "ROUTE_NAME" OrElse strColumnName = "REASON_NAME" Then
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        Try
            'TEAM_CODE , DATE , SALESREP_CODE , SALESREP_NAME , 
            'TTL_CUST, VISITED, TOTPLAN, UNVISIT, UNPLAN, TOTCALL, ACT_ACH

            Select Case strNewName
                Case "PLAN_ACH", "ACT_ACH"
                    strStringFormat = "{0:0.0}"
                Case "TTL_CUST", "VISITED", "UNVISIT", "UNPLAN", "TTL_CALL", "VISITED_CALL", "UNVISIT_CALL", "TTL_CALL_CALL"
                    strStringFormat = "{0:0}"
                Case "DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case Else
                    strStringFormat = ""
            End Select

        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

'Public Enum FieldColumntype
'    BoundColumn = 0
'    ButtonColumn = 1
'    EditCommandColumn = 2
'    HyperlinkColumn = 3
'    TemplateColumn = 4
'    TemplateColumn_Percentage = 5
'    BoundColumn_V2 = 6
'    InvisibleColumn = 99
'End Enum