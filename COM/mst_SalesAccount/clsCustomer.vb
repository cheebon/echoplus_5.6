'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	18/03/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustomer
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCustomer"
        End Get
    End Property

#Region "CUSTOMER"
    Public Function GetCustList(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String, ByVal strGPSStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("GPS_STATUS", strGPSStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustGPSList(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GPS_ENQ"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetCustListExport(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_EXPORT_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustDetails(ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_DTL"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateCust(ByVal strCustCode As String, ByVal strCustName As String, ByVal strStatus As String, _
                          ByVal strAbbv As String, ByVal strAdd1 As String, ByVal strAdd2 As String, _
                          ByVal strAdd3 As String, ByVal strAdd4 As String, ByVal strPostcode As String, _
                          ByVal strCity As String, ByVal strRegionCode As String, ByVal strRouteCode As String, _
                          ByVal strTelNo1 As String, ByVal strTelNo2 As String, ByVal strFaxNo1 As String, _
                          ByVal strFaxNo2 As String, ByVal strEmail As String, ByVal strCustGrpCode As String, _
                          ByVal strPoison As String, ByVal strPayTermCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CREATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("ABBV", strAbbv, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_1", strAdd1, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_2", strAdd2, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_3", strAdd3, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_4", strAdd4, cor_DB.clsDB.DataType.DBString)
                .addItem("POSTCODE", strPostcode, cor_DB.clsDB.DataType.DBString)
                .addItem("CITY", strCity, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_1", strTelNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_2", strTelNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO_1", strFaxNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO_2", strFaxNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("EMAIL", strEmail, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("POISON_IND", strPoison, cor_DB.clsDB.DataType.DBString)
                .addItem("PAY_TERM_CODE", strPayTermCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateCust(ByVal strCustCode As String, ByVal strCustName As String, ByVal strStatus As String, _
                          ByVal strAbbv As String, ByVal strAdd1 As String, ByVal strAdd2 As String, _
                          ByVal strAdd3 As String, ByVal strAdd4 As String, ByVal strPostcode As String, _
                          ByVal strCity As String, ByVal strRegionCode As String, ByVal strRouteCode As String, _
                          ByVal strTelNo1 As String, ByVal strTelNo2 As String, ByVal strFaxNo1 As String, _
                          ByVal strFaxNo2 As String, ByVal strEmail As String, ByVal strCustGrpCode As String, _
                          ByVal strPoison As String, ByVal strPayTermCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_UPDATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("ABBV", strAbbv, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_1", strAdd1, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_2", strAdd2, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_3", strAdd3, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_4", strAdd4, cor_DB.clsDB.DataType.DBString)
                .addItem("POSTCODE", strPostcode, cor_DB.clsDB.DataType.DBString)
                .addItem("CITY", strCity, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_1", strTelNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO_2", strTelNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO_1", strFaxNo1, cor_DB.clsDB.DataType.DBString)
                .addItem("FAX_NO_2", strFaxNo2, cor_DB.clsDB.DataType.DBString)
                .addItem("EMAIL", strEmail, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("POISON_IND", strPoison, cor_DB.clsDB.DataType.DBString)
                .addItem("PAY_TERM_CODE", strPayTermCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "CUSTOMER - CONTACT SHIPTO LIST"
    Public Function GetCustContList(ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONTACT_LIST"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetCustContListExport(ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONTACT_EXPORT_LIST"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#End Region

#Region "CUSTOMER  - PRICE GROUP"
    Public Function GetCustPriceGrpList(ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PRICE_GRP_LIST"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustPriceGrpExcludeList(ByVal strPriceGrpCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PRICE_GRP_EXCLUDE_LIST"
                .addItem("PRICE_GRP_CODE", strPriceGrpCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub CreateCustPriceGrp(ByVal strCodeList As String, ByVal strCustCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PRICE_GRP_CREATE"
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteCustPriceGrp(ByVal strCustCode As String, ByVal strPriceGrpCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PRICE_GRP_DELETE"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRICE_GRP_CODE", strPriceGrpCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region


#Region "CUSTOMER - CUSTOMER INFO"
    Public Function GetCustInfoList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_INFO_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustInfoDetails(ByVal strSalesrepCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_INFO_DTL"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateCustInfoDetails(ByVal strSalesrepCode As String, ByVal strCustCode As String, _
          ByVal strXFIELD1 As String, ByVal strXFIELD2 As String, ByVal strXFIELD3 As String, ByVal strXFIELD4 As String, ByVal strRemarks As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_INFO_UPDATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("XFIELD1", strXFIELD1, clsDB.DataType.DBString)
                .addItem("XFIELD2", strXFIELD2, clsDB.DataType.DBString)
                .addItem("XFIELD3", strXFIELD3, clsDB.DataType.DBString)
                .addItem("XFIELD4", strXFIELD4, clsDB.DataType.DBString)
                .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "CUSTOMER - CUSTOMER CLASS INFO"
    Public Function GetCustClassList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CLASS_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateCustClass(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
                     ByVal strClass As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CLASS_UPDATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CLASS", strClass, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "CUSTOMER - CUSTOMER SALES TGT"
    Public Function GetCustSalesTgtList(ByVal strTeamCode As String, ByVal strYear As String, ByVal strMonth As String, _
                                        ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_SALES_TGT_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateCustSalesTgt(ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strSalesTgt As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_SALES_TGT_UPDATE"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("SALES_TGT", strSalesTgt, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "CUSTOMER - CONTACT"
    Public Function GetCustContExcludeList(ByVal strContCode As String, ByVal strContName As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONT_EXCLUDE_LIST"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub CreateCustCont(ByVal strCodeList As String, ByVal strCustCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONT_CREATE"
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetCustContIncludeList(ByVal strContCode As String, ByVal strContName As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONT_INCLUDE_LIST"
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteCustCont(ByVal strCodeList As String, ByVal strCustCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONT_DELETE"
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

    'kiwi 29/11/2013 - add map at cust
#Region "CUSTOMER - GPS"
    Public Function getCustAddressList(ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GPS_LIST"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub setCustAddressList(ByVal strCustCode As String, ByVal strAccuracy As String, ByVal strlatitude As String, ByVal strlongitude As String, ByVal strGpsStatus As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn

                .CmdText = "SPP_MST_CUST_GPS_UPDATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("accuracy", strAccuracy, clsDB.DataType.DBString)
                .addItem("latitude", strlatitude, clsDB.DataType.DBString)
                .addItem("longitude", strlongitude, clsDB.DataType.DBString)
                .addItem("gps_status", strGpsStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "CUSTOMER - TARGET PRODUCT"
    Public Function GetCustomerTargetProductList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TGT_PRD_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCustomerAndContactList(ByVal strCustCode As String, ByVal strCustName As String, ByVal strContCode As String, ByVal strContName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_CONTACT_2_LIST"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateCustomerTargetProduct(ByVal strCustCode As String, ByVal strContCode As String, ByVal strPrd1 As String, ByVal strPrd2 As String, ByVal strPrd3 As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TGT_PRD_CREATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("PRD_1", strPrd1, clsDB.DataType.DBString)
                .addItem("PRD_2", strPrd2, clsDB.DataType.DBString)
                .addItem("PRD_3", strPrd3, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateCustomerTargetProduct(ByVal strCustCode As String, ByVal strContCode As String, ByVal strPrd1 As String, ByVal strPrd2 As String, ByVal strPrd3 As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TGT_PRD_UPDATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("PRD_1", strPrd1, clsDB.DataType.DBString)
                .addItem("PRD_2", strPrd2, clsDB.DataType.DBString)
                .addItem("PRD_3", strPrd3, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteCustomerTargetProduct(ByVal strCustCode As String, ByVal strContCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TGT_PRD_DELETE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetCustomerTargetProductDetail(ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TGT_PRD_DTL"
                .addItem("CUST_CODE", Trim(strCustCode), cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", Trim(strContCode), clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "CUSTOMER - TEAM CLASS"
    Public Function GetCustomerTeamClassList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TEAM_CLASS_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateCustomerTeamClass(ByVal strCustCode As String, ByVal strContCode As String, ByVal strType As String, ByVal strTeamClass As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TEAM_CLASS_CREATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("TYPE", strType, clsDB.DataType.DBString)
                .addItem("TEAM_CLASS", strTeamClass, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateCustomerTeamClass(ByVal strCustCode As String, ByVal strContCode As String, ByVal strOldType As String, ByVal strNewType As String, ByVal strTeamClass As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TEAM_CLASS_UPDATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("OLD_TYPE", strOldType, clsDB.DataType.DBString)
                .addItem("NEW_TYPE", strNewType, clsDB.DataType.DBString)
                .addItem("TEAM_CLASS", strTeamClass, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteCustomerTeamClass(ByVal strCustCode As String, ByVal strContCode As String, ByVal strType As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TEAM_CLASS_DELETE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("TYPE", strType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetCustomerTeamClassDetail(ByVal strCustCode As String, ByVal strContCode As String, ByVal strType As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_TEAM_CLASS_DTL"
                .addItem("CUST_CODE", Trim(strCustCode), cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", Trim(strContCode), clsDB.DataType.DBString)
                .addItem("TYPE", strType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class


