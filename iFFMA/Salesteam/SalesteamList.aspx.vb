'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	04/03/2008
'	Purpose	    :	Salesteam List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_Salesteam_SalesteamList
    Inherits System.Web.UI.Page
    Private dtAR As DataTable

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum assignTypeName As Long
        FieldForce = 0
        Supplier = 1
        PrdGrp = 2
        Prd = 3
        Prdv2 = 4
        DashboardXField = 5
        SfeKpi = 6
        Detailing = 7
    End Enum

    Public ReadOnly Property PageName() As String
        Get
            Return "SalesteamList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.SALESTEAM)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.SALESTEAM
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    '.PageCount = dgList.PageCount
            '    '.CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                End If

                'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, SubModuleAction.Edit) Then
                    btnSave.Visible = False
                End If
                '----------------------------------------------------------------------------------------------------

            End If

            lblErr.Text = ""

            wuc_AssignFieldForce.AssignType = assignTypeName.FieldForce
            wuc_AssignSupplier.AssignType = assignTypeName.Supplier
            wuc_AssignPrdGrp.AssignType = assignTypeName.PrdGrp
            wuc_AssignPrd.AssignType = assignTypeName.Prd
            wuc_AssignPrdV2.AssignType = assignTypeName.Prdv2
            wuc_AssignDashboardXField.AssignType = assignTypeName.DashboardXField
            wuc_AssignSfeKpi.AssignType = assignTypeName.SfeKpi
            wuc_AssignDetailing.AssignType = assignTypeName.Detailing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
            'If Page.IsValid = False Then
            '    Exit Sub
            'Else
            '    ActivateFirstTab()
            '    RenewDataBind()
            'End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("TEAM_CODE") = ""
        txtTeamCode.Text = ""
        txtTeamCode.Enabled = True
        txtTeamName.Text = ""
        ddlStatus.SelectedIndex = 0

        btnSave.Visible = True 'HL: 20080429 (AR)
        ActivateAddTab()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = ""

            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            Dim strSelectedCode As String = ViewState("TEAM_CODE").ToString

            If strSelectedCode <> "" Then
                clsSalesteam.UpdateSalesteam(strSelectedCode, Trim(txtTeamName.Text), ddlStatus.SelectedValue)
                ViewState("TEAM_CODE") = strSelectedCode
                lblInfo.Text = "The record is successfully saved."
            Else
                Dim DT As DataTable
                DT = clsSalesteam.CreateSalesteam(Trim(txtTeamCode.Text), Trim(txtTeamName.Text), ddlStatus.SelectedValue)

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Team Code already exists!"
                Else
                    ViewState("TEAM_CODE") = Trim(txtTeamCode.Text)

                    'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                    'HL: 20080429 (AR)
                    'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, SubModuleAction.Edit) Then
                    '    ActivateEditTab()
                    'Else
                    '    btnSave.Visible = False
                    'End If

                    ActivateEditTab()
                    If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, SubModuleAction.Edit) Then
                        btnSave.Visible = False
                    End If
                    '----------------------------------------------------------------------------------------------------

                    lblInfo.Text = "The record is successfully created."
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "TAB SUPPLIER"
    Protected Sub btnAddSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_AssignSupplier.AddButton_Click
        With (wuc_SalesteamAssignPop)
            .AssignType = assignTypeName.Supplier
            .teamCode = Trim(txtTeamCode.Text)
            .ResetPage()
            .RenewDataBind()
            .Show()
        End With
    End Sub

    Protected Sub btnDeleteSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_AssignSupplier.DeleteButton_Click
        wuc_AssignPrdGrp.RenewDataBind()
        wuc_AssignPrd.RenewDataBind()
    End Sub
#End Region

#Region "TAB PRD GRP"
    Protected Sub btnAddPrdGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_AssignPrdGrp.AddButton_Click
        With (wuc_SalesteamAssignPop)
            .AssignType = assignTypeName.PrdGrp
            .teamCode = Trim(txtTeamCode.Text)
            .ResetPage()
            .RenewDataBind()
            .Show()
        End With
        wuc_SalesteamAssignPop.LoadDDLSupplier()
    End Sub

    Protected Sub btnDeletePrdGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_AssignPrdGrp.DeleteButton_Click
        wuc_AssignPrd.RenewDataBind()
    End Sub
#End Region

#Region "TAB PRD"
    Protected Sub btnAddPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_AssignPrd.AddButton_Click
        With (wuc_SalesteamAssignPop)
            .AssignType = assignTypeName.Prd
            .teamCode = Trim(txtTeamCode.Text)
            .ResetPage()
            .RenewDataBind()
            .Show()
        End With
        wuc_SalesteamAssignPop.LoadDDLSupplier()
    End Sub
#End Region

#Region "TAB PRD V2"
    Protected Sub btnAddPrdV2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_AssignPrdV2.AddButton_Click
        With (wuc_SalesteamAssignPop)
            .AssignType = assignTypeName.Prdv2
            .teamCode = Trim(txtTeamCode.Text)
            .ResetPage()
            .RenewDataBind()
            .Show()
        End With
        wuc_SalesteamAssignPop.LoadDDLSupplier()
        wuc_SalesteamAssignPop.LoadDDLRegion()
    End Sub
#End Region

    Protected Sub btnAssignPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_SalesteamAssignPop.SelectButton_Click
        Try
            If wuc_SalesteamAssignPop.AssignType = assignTypeName.Supplier Then
                wuc_AssignSupplier.RenewDataBind()
            ElseIf wuc_SalesteamAssignPop.AssignType = assignTypeName.PrdGrp Then
                wuc_AssignPrdGrp.RenewDataBind()
                wuc_AssignPrd.RenewDataBind()
            ElseIf wuc_SalesteamAssignPop.AssignType = assignTypeName.Prd Then
                wuc_AssignPrd.RenewDataBind()
            ElseIf wuc_SalesteamAssignPop.AssignType = assignTypeName.Prdv2 Then
                wuc_AssignPrdV2.RenewDataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            With (wuc_toolbar)
                DT = clsSalesteam.GetSalesteamList(.SearchType, .SearchValue, .Status)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

            'ADD BUTTON EDIT
            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details-------------------------------------------------
            'If Master_Row_Count > 0 AndAlso _
            '    Report.GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM, SubModuleAction.Edit) Then 'HL: 20080429 (AR)
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SalesteamList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SalesteamList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SalesteamList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_SalesteamList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_SalesteamList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            If ViewState("TEAM_CODE") IsNot Nothing AndAlso ViewState("TEAM_CODE").ToString <> "" Then
                RefreshAllTab()
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strSelectedCode As String = sender.datakeys(e.NewEditIndex).item("TEAM_CODE")
            Dim DT As DataTable
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam

            DT = clsSalesteam.GetSalesteamDetails(strSelectedCode)

            If DT.Rows.Count > 0 AndAlso DT.Rows(0)("LOCK_FLAG") = "0" Then
                ViewState("TEAM_CODE") = strSelectedCode
                txtTeamCode.Text = IIf(IsDBNull(DT.Rows(0)("TEAM_CODE")), "", DT.Rows(0)("TEAM_CODE"))
                txtTeamName.Text = IIf(IsDBNull(DT.Rows(0)("TEAM_NAME")), "", DT.Rows(0)("TEAM_NAME"))
                ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS")), "", DT.Rows(0)("STATUS"))
                wuc_AssignFieldForce.Total = DT.Rows(0)("TTL_SALESREP")
                wuc_AssignSupplier.Total = DT.Rows(0)("TTL_SUPPLIER")
                wuc_AssignPrdGrp.Total = DT.Rows(0)("TTL_PRD_GRP")
                wuc_AssignPrd.Total = DT.Rows(0)("TTL_PRD")
                wuc_AssignPrdV2.Total = DT.Rows(0)("TTL_PRD")

                If GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, "'1'") Then
                    wuc_AssignDetailing.Total = DT.Rows(0)("TTL_DETAILING")
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, "'1'") Then
                    wuc_AssignDashboardXField.Total = DT.Rows(0)("TTL_DASHBOARDXFIELD")
                End If

                If GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, "'1'") Then
                    wuc_AssignSfeKpi.Total = DT.Rows(0)("TTL_SFE_KPI")
                End If

                ActivateEditTab()
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "<script>alert('This record is locked by other user!');</script>", False)
                RenewDataBind()
            End If
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Salesteam List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""
        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""
        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""
        TabPanel5.Visible = False
        TabPanel5.HeaderText = ""
        TabPanel6.Visible = False
        TabPanel6.HeaderText = ""
        TabPanel7.Visible = False
        TabPanel7.HeaderText = ""
        TabPanel8.Visible = False
        TabPanel8.HeaderText = ""
        TabPanel9.Visible = False
        TabPanel9.HeaderText = ""

        wuc_toolbar.show()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateAddTab()
        TabPanel1.HeaderText = "Salesteam Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""
        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""
        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""
        TabPanel5.Visible = False
        TabPanel5.HeaderText = ""
        TabPanel6.Visible = False
        TabPanel6.HeaderText = ""
        TabPanel7.Visible = False
        TabPanel7.HeaderText = ""
        TabPanel8.Visible = False
        TabPanel8.HeaderText = ""
        TabPanel9.Visible = False
        TabPanel9.HeaderText = ""

        wuc_toolbar.hide() 'wuc_toolbar.EnqCollapsed = True
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "Salesteam Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = True
        TabPanel2.HeaderText = "Field Force"
        TabPanel3.Visible = True
        TabPanel3.HeaderText = "Supplier"
        TabPanel4.Visible = True
        TabPanel4.HeaderText = "Product Group"

        If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM_SUB, "'1'") Then
            TabPanel6.Visible = True
            TabPanel6.HeaderText = "Product"
        Else
            TabPanel5.Visible = True
            TabPanel5.HeaderText = "Product"
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, "'1'") Then
            TabPanel7.Visible = True
            TabPanel7.HeaderText = "Dashboard XField"
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, "'1'") Then
            TabPanel8.Visible = True
            TabPanel8.HeaderText = "SFE KPI"
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, "'1'") Then
            TabPanel9.Visible = True
            TabPanel9.HeaderText = "Detailing"
        End If

        txtTeamCode.Enabled = False

        'KL - 01032016 : Put Trimming Under teamCode var 
        Dim teamCode As String = Trim(txtTeamCode.Text)
        wuc_AssignFieldForce.TeamCode = teamCode
        wuc_AssignSupplier.TeamCode = teamCode
        wuc_AssignPrdGrp.TeamCode = teamCode
        wuc_AssignPrd.TeamCode = teamCode
        wuc_AssignPrdV2.TeamCode = teamCode
        wuc_AssignDashboardXField.TeamCode = teamCode
        wuc_AssignSfeKpi.TeamCode = teamCode
        wuc_AssignDetailing.TeamCode = teamCode

        RenewAllTab()

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub

    Private Sub RenewAllTab()
        wuc_AssignFieldForce.RenewDataBind()
        wuc_AssignSupplier.RenewDataBind()
        wuc_AssignPrdGrp.RenewDataBind()
        wuc_AssignPrd.RenewDataBind()
        If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM_SUB, "'1'") Then
            wuc_AssignPrdV2.RenewDataBind()
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, "'1'") Then
            wuc_AssignDashboardXField.RenewDataBind()
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, "'1'") Then
            wuc_AssignSfeKpi.RenewDataBind()
        End If

        wuc_AssignDetailing.RenewDataBind()
    End Sub

    Private Sub RefreshAllTab()
        wuc_AssignFieldForce.RefreshDataBind()
        wuc_AssignSupplier.RefreshDataBind()
        wuc_AssignPrdGrp.RefreshDataBind()
        wuc_AssignPrd.RefreshDataBind()
        If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM_SUB, "'1'") Then
            wuc_AssignPrdV2.RefreshDataBind()
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.DASHBOARDXFIELD, "'1'") Then
            wuc_AssignDashboardXField.RefreshDataBind()
        End If

        If GetAccessRight(ModuleID.FFMA, SubModuleType.SFE_KPI, "'1'") Then
            wuc_AssignSfeKpi.RefreshDataBind()
        End If

        wuc_AssignDetailing.RefreshDataBind()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList)
            If TabPanel2.Visible Then
                aryDgList.Add(wuc_AssignFieldForce.GetdgAssignList())
                aryDgList.Add(wuc_AssignSupplier.GetdgAssignList())
                aryDgList.Add(wuc_AssignPrdGrp.GetdgAssignList())
                If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM_SUB, "'1'") Then
                    aryDgList.Add(wuc_AssignPrdV2.GetdgAssignList())
                Else
                    aryDgList.Add(wuc_AssignPrd.GetdgAssignList())
                End If
            End If

            aryDgList.Add(wuc_AssignDashboardXField.GetdgAssignList())
            aryDgList.Add(wuc_AssignSfeKpi.GetdgAssignList())
            aryDgList.Add(wuc_AssignDetailing.GetdgAssignList())
            wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            If TabPanel2.Visible Then
                RefreshAllTab()
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_SalesteamList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "CREATED_DATE"
                strFieldName = "Created Date"
            Case "CREATED_BY"
                strFieldName = "Created By"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class


