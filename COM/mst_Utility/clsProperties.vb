﻿'************************************************************************
'	Author	    :	EuJin
'	Date	    :	2012-03-07
'	Purpose	    :	Class Easy Loader Properties
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Public Class clsProperties
    Public Class clsEasyLoader
        Public user_id As String
        Public cat_code As String
        Public action_id As String
        Public Valid_Count_Action As String
        Public Upload_Type As String
        Public strMode As String
    End Class
End Class
