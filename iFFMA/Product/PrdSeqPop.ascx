﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PrdSeqPop.ascx.vb" Inherits="iFFMA_Others_PrdSeqPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdSearch" Src="ProdSearchPop.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 500px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px; text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 92%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: right; width: 8%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; /*width: 98%*/">
                <fieldset style="padding-left: 10px; width: 100%; box-sizing: border-box;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />

                    <table cellspacing="1" cellpadding="2" rules="all" border="0" style="border-width: 0px; border-style: None; width: 100%;">
                        <tbody runat="server" id="pnlInsert" visible="False">
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Product</td>
                                <td colspan="2" style="border-right: none">
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtPrd" runat="server" CssClass="cls_textbox" Enabled="False" Width="230px"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                                <td align="middle" style="border-left: none;">
                                    <asp:Button ID="btnSearchPrd" runat="server" CssClass="cls_button" Text="Search" OnClick="btnSearchPrd_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Sequence</td>
                                <td colspan="3">
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtSeq" runat="server" Text="0" CssClass="cls_textbox" Width="50px" MaxLength="6"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator ID="rgExpValforNumericOnly" ControlToValidate="txtSeq" Display="Dynamic" runat="server" ErrorMessage="Only numbers are allowed" class="cls_label_err" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </tbody>
                        <tbody runat="server" id="pnlEdit" visible="false">
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Product</td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtPrdForEdit" runat="server" CssClass="cls_textbox" Enabled="False" Width="300px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Sequence</td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtSeqForEdit" runat="server" Text="0" CssClass="cls_textbox" Width="50px" MaxLength="6"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator ID="rgExpValforNumericOnly1" ControlToValidate="txtSeqForEdit" Display="Dynamic" runat="server" ErrorMessage="Only numbers are allowed" class="cls_label_err" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                        <center>
                            <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" />
                        </center>
                    </span>
                    <!-- End Customized Content -->

                    <asp:HiddenField ID="hdPrdCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdProduct" runat="server" Value="" />
                    <asp:HiddenField ID="hdSeq" runat="server" Value="" />
                    <asp:HiddenField ID="hdIsEdit" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server"
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden"
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop"
            BackgroundCssClass="modalBackground"
            DropShadow="True"
            RepositionMode="RepositionOnWindowResizeAndScroll" />

        <span style="float: left;">
            <customToolkit:wuc_PrdSearch ID="wuc_PrdSearch" Title="Product Search" runat="server" />
        </span>
    </ContentTemplate>
</asp:UpdatePanel>