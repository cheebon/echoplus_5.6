﻿Imports System.Data
Partial Class iFFMS_Plan_PlanTxnMenu
    Inherits System.Web.UI.Page
    Private dtAR As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        'Call Header
        With wuc_lblHeader
            .Title = "New Transaction" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With
        If Not IsPostBack Then
            Session("TxnValid") = "0" 'Reset Trasaction after change customer
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True
        End If

    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick

        If TimerControl1.Enabled Then
            lblsalesrepcode.Text = Trim(Request.QueryString("salesrepcode"))
            lblcustcode.Text = Trim(Request.QueryString("custcode"))
            lblcontcode.Text = Trim(Request.QueryString("contcode"))
            lbldateIn.Text = Trim(Request.QueryString("visitdate"))
            lbltimeIn.Text = Trim(Request.QueryString("visittime"))
            lblsessionid.Text = GetGuid

            If Not IsNothing(Request.QueryString("salesrepcode")) Or Not IsNothing(Session("UserId")) Then
                LoadTxnNo()
                UpdateVisitInd()
                InsertDailyTimeSumm("P")
            End If

            UpdateDatagrid.Update()
        End If
        TimerControl1.Enabled = False

    End Sub

    Public Property GetGuid()
        Get
            If String.IsNullOrEmpty(Session("GUID")) Then Session("GUID") = Session("USER_ID") & Guid.NewGuid.ToString.ToUpper.Substring(24, 8)
            Return Session("GUID")
        End Get
        Set(ByVal value)
            Session("GUID") = value
        End Set
    End Property

    Private Sub LoadTxnNo()
        Dim strVisitId As String
        Dim clsTRA As New txn_WebActy.clsCommon
        strVisitId = clsTRA.GetSRVisitID(Trim(Request.QueryString("salesrepcode")))
        lblvisitid.Text = strVisitId
    End Sub

    Private Sub InsertDailyTimeSumm(ByVal StrTxnStatus As String)
        Dim StrSalesrepCode As String, StrCustCode As String, StrContCode As String, StrCallDate As String, StrTimeIn As String, _
        StrVisitId As String, StrUserId As String

        StrSalesrepCode = Trim(lblsalesrepcode.Text)
        StrCustCode = Trim(lblcustcode.Text)
        StrContCode = Trim(lblcontcode.Text)
        StrCallDate = Trim(lbldateIn.Text)
        StrTimeIn = Trim(lbltimeIn.Text)
        StrVisitId = Trim(lblvisitid.Text)
        StrUserId = Trim(Session("UserID"))

        Dim clsDTS As New txn_WebActy.clsDTS
        clsDTS.InsertDailyTimeSumm(StrSalesrepCode, StrCustCode, StrContCode, StrCallDate, StrTimeIn, StrVisitId, StrTxnStatus, StrUserId)

    End Sub

    Private Sub UpdateVisitInd()
        Dim StrSalesrepCode As String, StrCustCode As String, StrContCode As String, _
        strRouteDate As String, StrUserId As String

        StrSalesrepCode = Trim(lblsalesrepcode.Text)
        StrCustCode = Trim(lblcustcode.Text)
        StrContCode = Trim(lblcontcode.Text)
        strRouteDate = Trim(Request.QueryString("RouteDate"))
        StrUserId = Trim(Session("UserID"))

        Dim clsPrePlan As New txn_PrePlan.clsPrePlan
        clsPrePlan.UpdVisitInd(StrSalesrepCode, StrCustCode, StrContCode, strRouteDate, StrUserId)

        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "Refresh", "Refresh();", True)
    End Sub

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function

End Class
