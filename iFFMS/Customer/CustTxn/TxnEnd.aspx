<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnEnd.aspx.vb" Inherits="iFFMS_Customer_CustTxn_TxnEnd" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Txn End Visit</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
        function Redirect()
        { HideElement('SubDetailConfBar'); HideElement('SubDetailBar'); HideElement('DetailBar'); ShowElement('TopBar'); HideElement('ContentBar'); ResizeFrameWidth('TopBar', '100%'); ResizeFrameWidth('ContentBar', '100%'); MaximiseFrameHeight('TopBarIframe'); MaximiseFrameHeight('ContentBarIframe'); }

        function ShowNavigation() {
            var height = screen.height;
            if (height < 700) {
                var fraDefault = self.parent.parent.parent.document.getElementById('fraDefault').rows = "65,*";
            }
            else
            { self.parent.parent.parent.document.getElementById('fraDefault').rows = "132,*"; } 
        } 
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe')"
    style="background-color: #DDDDDD;">
    <form id="frmendvisit" runat="server">
    <fieldset class="" style="width: 98%;">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <div id="title">
            <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" Visible="false"></customToolkit:wuc_lblheader>
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        </div>
        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
            width="100%" border="0" style="height: 30px">
            <tr align="left" valign="bottom">
                <td>
                    <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                        CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
            <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Field Force Name</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 300px">
                                        <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Customer Name</span>
                                        <td style="width: 5px">
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td style="width: 300px">
                                            <asp:Label ID="lblCustName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                        </td>
                                        <td style="width: 100px">
                                            <span class="cls_label_header">Contact Name</span>
                                            <td style="width: 5px">
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:Label ID="lblContName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                            </td>
                            </tr>
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Visit ID</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblVisitId" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Map</span>
                                    </td>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <%--<td align="right">
                                         <asp:Image ID="imgGeneralInfo3" runat="server" ImageUrl="~/images/ico_close.gif" CssClass="cls_image_Close"
                                                     EnableViewState="false" />
                                    </td>--%>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </asp:Panel>
        <div style="width: 100%; position: relative; padding: 0; margin: 0;">
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                    <table class="cls_form_table">
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnsave" runat="server" Text="Exit" CssClass="cls_button" ValidationGroup="dts"
                                    Width="80px" OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree){ShowNavigation();Redirect();}else {return false;}" />
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <span class="cls_label">Reason :</span>
                            </td>
                            <td width="40%">
                                <asp:DropDownList ID="ddlReasonCode" runat="server" CssClass="cls_dropdownlist">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%">
                                <span class="cls_label">Remarks :</span>
                            </td>
                            <td width="40%">
                                <asp:TextBox ID="txtremarks" runat="server" CssClass="cls_textbox" TextMode="MultiLine"
                                    Width="250px" Height="50px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    </form>
</body>
</html>
