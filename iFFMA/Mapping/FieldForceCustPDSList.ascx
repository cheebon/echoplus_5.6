<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FieldForceCustPDSList.ascx.vb" Inherits="iFFMA_Mapping_FieldForceCustPDSList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldForceCustPDSPop" Src="FieldForceCustPDSPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlFieldForceCustPDS" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            
            <tr style="height:15px" align="left" valign="middle">
                <td>
                    <asp:HiddenField ID="hdType" runat="server" Value="0" />
                    <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdSalesrepCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdContCode" runat="server" Value="" />
                </td>
            </tr>
            
            <asp:Panel ID="pnlCtrlAction" runat="server"> 
                <tr>
                    <td align="left" style="padding-left:15px">
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </td>
                </tr>
            </asp:Panel> 
            
            <tr><td align="center" style="width:95%;"><customToolkit:wuc_dgpaging ID="wuc_dgCustPDSPaging" runat="server" /></td></tr>
            <tr>
                <td align="center" style="width:95%;">
                    <ccGV:clsGridView ID="dgCustPDSList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
            </tr>
        </table>
        
        <customToolkit:wuc_FieldForceCustPDSPop ID="wuc_FieldForceCustPDSPop" Title="PDS Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
