
'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	12/06/2008
'	Purpose	    :	Class to build Query for report Call Acty Enquiry
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCallAnalyEnquiryList
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    'Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallAnalyEnquiryList"
        End Get
    End Property

    Public Function GetCallAnalyEnquiryList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                            ByVal strYear As String, ByVal strMonth As String, ByVal strAgencyCode As String, ByVal strTeamCode As String, _
                                            ByVal strSalesrepCode As String, ByVal strGroupingCode As String) As DataTable
        Dim clsCallAnalyEnquiryListDB As clsDB
        Try
            clsCallAnalyEnquiryListDB = New clsDB
            With clsCallAnalyEnquiryListDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_ANALYSIS_ENQUIRY_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("CLASSIFICATION_LIST", strAgencyCode, clsDB.DataType.DBString, True)
                .addItem("TEAM_LIST", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_LIST", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("GROUPING_LIST", strGroupingCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallAnalyEnquiryList :" & ex.Message))
        Finally
            clsCallAnalyEnquiryListDB = Nothing
        End Try
    End Function

    Public Function GetCallAnalyByCont(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                            ByVal strYear As String, ByVal strMonth As String, ByVal strAgencyCode As String, ByVal strTeamCode As String, _
                                            ByVal strSalesrepCode As String, ByVal strNSMCode As String, ByVal strDSMCode As String, ByVal strCustCode As String) As DataTable
        Dim clsCallAnalyByContDB As clsDB
        Try
            clsCallAnalyByContDB = New clsDB
            With clsCallAnalyByContDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_ANALYSIS_BY_CONT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("CLASSIFICATION_LIST", strAgencyCode, clsDB.DataType.DBString, True)
                .addItem("TEAM_LIST", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_LIST", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("NSM_LIST", strNSMCode, clsDB.DataType.DBString, True)
                .addItem("DSM_LIST", strDSMCode, clsDB.DataType.DBString, True)
                .addItem("CUSTOMER_LIST", strCustCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallActyEnquiryList :" & ex.Message))
        Finally
            clsCallAnalyByContDB = Nothing
        End Try
    End Function


#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
