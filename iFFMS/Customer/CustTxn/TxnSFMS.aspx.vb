Imports System.Data
Imports txn_WebActy


Partial Class iFFMS_Customer_CustTxn_TxnSFMS
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Public Enum dgcoldtl As Integer
        SUB_CAT_NAME = 3

    End Enum
    Public Enum dgcol As Integer
        SUB_CAT_NAME = 6

    End Enum

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property


    Private Property Master_Row_CountDtl() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountDtl"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountDtl") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustTxnSFMS"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
     
        LoadTxnDescName()

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

            TimerControl2.Enabled = True

        End If
        lblErr.Text = ""

    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        If TimerControl2.Enabled Then
            If Request.QueryString("salesrepcode") IsNot Nothing Then
                LoadTxnNo()
                LoadSFMSSummHdr()
            End If
        End If

        TimerControl2.Enabled = False

    End Sub


#Region "DGLIST Dtl"
    Public Sub RefreshDatabindingDtl(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableDtl As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionDtl As String = CType(ViewState("strSortExpressionDtl"), String)


        dtCurrentTableDtl = GetRecListDtl()

        If dtCurrentTableDtl Is Nothing Then
            dtCurrentTableDtl = New DataTable
        Else
            If dtCurrentTableDtl.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountDtl = 0
            Else
                Master_Row_CountDtl = dtCurrentTableDtl.Rows.Count
            End If
        End If

        Dim dvCurrentViewDtl As New DataView(dtCurrentTableDtl)
        If Not String.IsNullOrEmpty(strSortExpressionDtl) Then
            Dim strSortExpressionNameDtl As String = strSortExpressionDtl.Replace(" DESC", "")
            dvCurrentViewDtl.Sort = IIf(dtCurrentTableDtl.Columns.Contains(strSortExpressionNameDtl), strSortExpressionDtl, "")
        End If

        With dglistdtl
            .DataSource = dvCurrentViewDtl
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountDtl > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateDtl()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListDtl() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strCatCode As String, strSalesrepCode As String
            strSalesrepCode = Request.QueryString("salesrepcode")
            strCatCode = ddlcatcode.SelectedValue.ToString

            Dim clsSFMS As New txn_WebActy.clsSFMS
            DT = clsSFMS.GetSubCatcode(strSalesrepCode, strCatCode)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateDtl()

        If dglistdtl.Rows.Count < 15 Then
            Dim GridHeight As Integer = dglistdtl.Rows.Count * 25 + 25
            dglistdtl.GridHeight = GridHeight
        Else
            dglistdtl.GridHeight = 200
        End If

        UpdateDtl.Update()

    End Sub

    Protected Sub dglistdtl_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistdtl.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountDtl > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListDtl_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistdtl.Sorting
        Dim strSortExpressionDtl As String = ViewState("strSortExpressionDtl")

        If strSortExpressionDtl IsNot Nothing AndAlso strSortExpressionDtl.Length > 0 Then
            If strSortExpressionDtl Like (e.SortExpression & "*") Then
                If strSortExpressionDtl.IndexOf(" DESC") > 0 Then
                    strSortExpressionDtl = e.SortExpression
                Else
                    strSortExpressionDtl = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionDtl = e.SortExpression
            End If
        Else
            strSortExpressionDtl = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionDtl") = strSortExpressionDtl

        RefreshDatabindingDtl()

    End Sub

#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        dtCurrentTable = GetRecList()

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
            'btncheckall.Visible = False
            'btnuncheckall.Visible = False
            btndeletesum.Visible = False
            btnsavesum.Visible = False
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'btncheckall.Visible = False
                'btnuncheckall.Visible = False
                btndeletesum.Visible = False
                btnsavesum.Visible = False
                Master_Row_Count = 0
            Else
                'btncheckall.Visible = True
                'btnuncheckall.Visible = True
                btndeletesum.Visible = True
                btnsavesum.Visible = True
                Master_Row_Count = dtCurrentTable.Rows.Count
            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgList
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With


        UpdateDatagrid_Update()
        UpdatePage.Update()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strTXNNo As String, strUserid As String, strsessionid As String, strSalesrepCode As String
            strTXNNo = Trim(lbltxnno.Text)
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strsessionid = Trim(Request.QueryString("sessionid"))
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))


            Dim clsSFMS As New txn_WebActy.clsSFMS
            DT = clsSFMS.GetSFMSSumDtl(strsessionid, strTXNNo, strUserid, strSalesrepCode)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_Update()



        UpdateDatagrid.Update()

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub
#End Region

#Region "EVENT HANDLER"

    Protected Sub btnsavedtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavedtl.Click
        If Page.IsValid Then
            InsertSFMSHdr()
            InsertSFMSDtl()
         
        End If
        UpdatePage.Update()

    End Sub

    Protected Sub ddlcatcode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcatcode.SelectedIndexChanged
        RefreshDatabindingDtl()
        UpdateDtl.Update()
    End Sub

    Protected Sub btnsavesum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsavesum.Click
        If Page.IsValid Then
            SaveSFMSSumDtl()
        End If
        RefreshDatabinding()
        UpdatePage.Update()


        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('Your changes have been saved!')", True)
    End Sub

    Protected Sub btndeletesum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeletesum.Click
        DelSFMSSumDtl()
        RefreshDatabinding()
        UpdatePage.Update()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Try
            If dgList.Rows.Count > 0 Then
                InsertSFMSHdr()
                Dim strSessionId As String, strVisitID As String, strSalesrepCode As String, strTxnNo As String, strCustCode As String, strContCode As String


                strSessionId = Trim(Request.QueryString("sessionid"))
                strVisitID = Trim(Request.QueryString("visitid"))
                strSalesrepCode = Trim(Session("SALESREP_CODE"))
                strCustCode = Trim(Request.QueryString("CustCode"))
                strContCode = Trim(Request.QueryString("ContCode"))
                strTxnNo = Trim(lbltxnno.Text)

                Dim strurl = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnSFMSSub.aspx?sessionid=" + strSessionId + "&visitid=" + strVisitID + "&SALESREP_CODE=" + strSalesrepCode + "&TXNNO=" + strTxnNo + "&CustCode=" + strCustCode + "&ContCode=" + strContCode + "';"
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "HideElement('SubDetailBar');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", "alert('Kindly add at least one detail record!')", True)
            End If


        Catch ex As Exception
            ExceptionMsg("btnsubmit_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnProceeed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceeed.Click
        btnsubmit_Click(sender, e)
    End Sub
#End Region
    
#Region "Header Function"

    Private Sub LoadTxnDescName()
        Try
            Dim strSalesrepCode As String, strCustCode As String, strContCode As String, strvisitid As String, strMapPath As String

            strSalesrepCode = Trim(Request.QueryString("salesrepcode"))
            strCustCode = Trim(Request.QueryString("custcode"))
            strContCode = Trim(Request.QueryString("contcode"))
            strvisitid = Trim(Request.QueryString("visitid"))
            strMapPath = "Ondemand > New Transaction > Field Activity"

            Dim dt As DataTable
            Dim clstxncommon As New txn_common.clstxncommon
            dt = clstxncommon.GetTxnDescName(strSalesrepCode, strCustCode, strContCode)

            If dt.Rows.Count > 0 Then
                lblFieldForceName.Text = Trim(dt.Rows(0)("SALESREP_NAME"))
                lblCustName.Text = Trim(dt.Rows(0)("CUST_NAME"))
                lblContName.Text = Trim(dt.Rows(0)("CONT_NAME"))
                lblVisitId.Text = strvisitid

                lblMapPath.Text = strMapPath
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub CreateHdrNewRecord()
        UpdateHdr.Update()
    End Sub

    Private Sub LoadTxnNo()
        Try
            Dim strTxnNo As String = Trim(Request.QueryString("TxnNo"))
            If IsNothing(strTxnNo) Or strTxnNo = "" Then
                Dim DTTXNno As DataTable
                Dim clsSFMS As New txn_WebActy.clsSFMS

                DTTXNno = clsSFMS.GetTRANo(Request.QueryString("salesrepcode"))
                lbltxnno.Text = DTTXNno.Rows(0)("TXN_NO")
            Else
                lbltxnno.Text = strTxnNo
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
        ''----------------------------------------------------------------------------
        'Dim myServiceDoc As New XmlDocument()
        'Dim neNode As System.Xml.XmlNode

        ''Adding the resulting XML from WebMethod to a user created XmlNode 
        'Dim ws_WebActy As New localhost.ws_WebActy

        'neNode = ws_WebActy.GetSFMSTxnNo(Request.QueryString("salesrepcode")) ' myService1.GetDataFromDB()
        ''Creating a Dataset 

        'Dim myDataSet As New DataSet()
        ''The XmlNode is added to a byte[] 

        'Dim buf As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(neNode.OuterXml)
        'Dim ms As New System.IO.MemoryStream(buf)
        ''The XML is readed from the MemoryStream 
        'myDataSet.ReadXml(ms)

        'GridView1.DataSource = myDataSet.Tables(0)
        'GridView1.DataBind()
    End Sub

    Private Sub LoadSFMSSummHdr()
        try
        Dim DT As DataTable
        Dim clsSFMS As New txn_WebActy.clsSFMS
        Dim strTXNNo As String, strUserid As String, strsessionid As String, strCustCode As String, strContCode As String
        strTXNNo = lbltxnno.Text
        strUserid = Web.HttpContext.Current.Session("UserID")
        strsessionid = Request.QueryString("sessionid")
        strCustCode = Trim(Request.QueryString("custcode"))
        strContCode = Trim(Request.QueryString("contcode"))

        DT = clsSFMS.GetSFMSSumHdrs(strsessionid, strTXNNo, strUserid, strCustCode, strContCode)

        If DT.Rows.Count > 0 Then
            lbltxnno.Text = DT.Rows(0)("TXN_NO")
            txtrmks.Text = DT.Rows(0)("REMARKS")
            RefreshDatabinding()
            LoadCatDDL()
            RefreshDatabindingDtl()
            UpdateHdr.Update()
        Else
            CreateHdrNewRecord()
            CreateDtlNewRecord()
        End If

            UpdatePage.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub InsertSFMSHdr()
        Try
            Dim strSessionId As String, strTxnNo As String, _
            strVisitId As String, strSalesrepCode As String, strCustCode As String, strContCode As String, _
            strRemarks As String, strUserId As String, strTxnDate As String

            strSessionId = Request.QueryString("sessionid")
            strTxnNo = lbltxnno.Text
            strVisitId = Request.QueryString("visitid")
            strSalesrepCode = Request.QueryString("salesrepcode")
            strCustCode = Request.QueryString("custcode")
            strContCode = Request.QueryString("contcode")
            strRemarks = txtrmks.Text
            strUserId = Web.HttpContext.Current.Session("UserID").ToString
            strTxnDate = Request.QueryString("datein") + " " + Request.QueryString("timein")

            Dim clsSFMS As New txn_WebActy.clsSFMS
            clsSFMS.InstTMPSFMSHeader(strSessionId, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContCode, _
              strRemarks, strUserId, strTxnDate)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Detail Function"

    Private Sub CreateDtlNewRecord()
        LoadCatDDL()
        RefreshDatabindingDtl()
        UpdateDtl.Update()

    End Sub

    Private Sub LoadCatDDL()
        Try
            Dim DT As DataTable
            Dim clsSFMS As New txn_WebActy.clsSFMS

            DT = clsSFMS.GetCatcode(Request.QueryString("salesrepcode"))
            With ddlcatcode
                .Items.Clear()
                .DataSource = DT.DefaultView
                .DataTextField = "CAT_NAME"
                .DataValueField = "CAT_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub InsertSFMSDtl()
        Try
            If dglistdtl.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim prompt As Integer = 0
                Dim strTxnNo As String, strUserId As String, strsessionid As String, strCatCode As String, strSubCatCode As String, strSubCatName As String
                Dim strRemarks As String, dbQty As Double

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistdtl.Rows

                    DK = dglistdtl.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim txtqty As TextBox = CType(dglistdtl.Rows(i).FindControl("txtQty"), TextBox)
                        dbQty = CDbl(IIf(txtqty.Text = "", 0, txtqty.Text))
                        Dim txtdtlrmks As TextBox = CType(dglistdtl.Rows(i).FindControl("txtdtlrmks"), TextBox)
                        strRemarks = txtdtlrmks.Text
                        Dim chkSelection As CheckBox = CType(dglistdtl.Rows(i).FindControl("chkSelection"), CheckBox)

                        'If dbQty > 0 Or Len(strRemarks) > 0 Then
                        If chkSelection.Checked = True Then
                            prompt = prompt + 1

                            strTxnNo = lbltxnno.Text
                            strUserId = Web.HttpContext.Current.Session("UserID")
                            strsessionid = Request.QueryString("sessionid")
                            strCatCode = dglistdtl.DataKeys(i).Item("CAT_CODE")
                            strSubCatCode = dglistdtl.DataKeys(i).Item("SUB_CAT_CODE")
                            strSubCatName = dglistdtl.Rows(i).Cells(dgcoldtl.SUB_CAT_NAME).Text

                            Dim clsSFMS As New txn_WebActy.clsSFMS
                            clsSFMS.InstTMPSFMSDetails(strsessionid, strTxnNo, strCatCode, strSubCatCode, strSubCatName, strRemarks, dbQty, strUserId)
                        End If

                    End If

                    i += 1
                Next

                If prompt > 0 Then
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successsfully added " + prompt.ToString + " the transaction(s)!')", True)
                    RefreshDatabinding()
                    ddlcatcode.SelectedIndex = 0
                    dglistdtl.DataSource = Nothing
                    dglistdtl.DataBind()
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('Kindly select at least 1 transaction(s)!')", True)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub SaveSFMSSumDtl()
        Try
            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strTxnNo As String, strUserId As String, strsessionid As String, strCatCode As String, strSubCatCode As String, strSubCatName As String
                Dim strRemarks As String, dbQty As Double

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then


                        Dim txtqty As TextBox = CType(dgList.Rows(i).FindControl("txtqtysum"), TextBox)
                        dbQty = CDbl(txtqty.Text)
                        Dim txtdtlrmks As TextBox = CType(dgList.Rows(i).FindControl("txtdtlrmksSum"), TextBox)
                        strRemarks = txtdtlrmks.Text

                        If dbQty > 0 Or Len(strRemarks) > 1 Then
                            strTxnNo = lbltxnno.Text
                            strUserId = Web.HttpContext.Current.Session("UserID")
                            strsessionid = Request.QueryString("sessionid")
                            strCatCode = dgList.DataKeys(i).Item("CAT_CODE")
                            strSubCatCode = dgList.DataKeys(i).Item("SUB_CAT_CODE")
                            strSubCatName = dgList.Rows(i).Cells(dgcol.SUB_CAT_NAME).Text

                            Dim clsSFMS As New txn_WebActy.clsSFMS
                            clsSFMS.InstTMPSFMSDetails(strsessionid, strTxnNo, strCatCode, strSubCatCode, strSubCatName, strRemarks, dbQty, strUserId)
                        End If

                    End If

                    i += 1
                Next


            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelSFMSSumDtl()
        Try
            If dgList.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strTxnNo As String, strUserId As String, strsessionid As String, strCatCode As String, strSubCatCode As String


                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dgList.Rows(i).FindControl("chkdelete"), CheckBox)

                        If chkdelete.Checked = True Then
                            strTxnNo = lbltxnno.Text
                            strUserId = Web.HttpContext.Current.Session("UserID")
                            strsessionid = Request.QueryString("sessionid")
                            strCatCode = dgList.DataKeys(i).Item("CAT_CODE")
                            strSubCatCode = dgList.DataKeys(i).Item("SUB_CAT_CODE")

                            Dim clsSFMS As New txn_WebActy.clsSFMS
                            clsSFMS.DelTMPSFMSDetails(strsessionid, strTxnNo, strUserId, strCatCode, strSubCatCode)
                        End If

                    End If

                    i += 1
                Next

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
   
End Class



