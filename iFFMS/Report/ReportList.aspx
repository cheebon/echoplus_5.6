<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportList.aspx.vb" Inherits="iFFMS_Report_ReportList" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ResizeFrameWidth('TopBar','100%');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');">
    <form id="frmreportlist" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <asp:UpdatePanel runat="server" ID="UpdatePage" UpdateMode="Conditional">
                <ContentTemplate>
                    <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel runat="server" ID="updateDG" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                    </asp:Timer>
                    <div style="padding-top: 10px; padding-left: 10px; width: 98%">
                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="420" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="REPORT_CODE">
                            <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                            <EmptyDataTemplate>
                                There is no data to display.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:CommandField HeaderText="View Details" ShowSelectButton="True" ShowHeader="True"
                                    Selecttext="<img src='../../images/ico_edit.gif' alt='View' border='0' height='18px' width='18px'/>">
                                    <itemstyle horizontalalign="Center" />
                                </asp:CommandField>
                                <asp:BoundField DataField="REPORT_CODE" HeaderText="Report Code" SORTEXPRESSION="REPORT_CODE">
                                    <itemstyle horizontalalign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="REPORT_NAME" HeaderText="Report Name" SORTEXPRESSION="REPORT_NAME">
                                    <itemstyle horizontalalign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="REPORT_DATE" HeaderText="Report Date" SORTEXPRESSION="REPORT_DATE">
                                    <itemstyle horizontalalign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="REPORT_FREQ" HeaderText="Report Freq." SORTEXPRESSION="REPORT_FREQ">
                                    <itemstyle horizontalalign="Center" />
                                </asp:BoundField>
                            </Columns>
                        </ccGV:clsGridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</html>
