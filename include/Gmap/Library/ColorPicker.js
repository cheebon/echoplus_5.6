﻿var CurrentIndex;
var ColorRange;
var _cp;

function ColorPicker()
{
    CurrentIndex = 0;
    ColorRange = ["#00FF00", "#d87185", "#00FF00", "#FF0000", "#5555FF", "#7FFFFF", "#FF7F00", "#00FF00", "#FFFF00", "#FF00FF"]
    _cp = this;    
}

ColorPicker.prototype.GetFirstColor = getFirstColor;
ColorPicker.prototype.GetNextColor = getNextColor;

function getFirstColor()
{
    CurrentIndex=0;
    return ColorRange[CurrentIndex];
}
function getNextColor()
{
    if (CurrentIndex == 9) { CurrentIndex = 0; }
    else { CurrentIndex += 1; }
    
    return ColorRange[CurrentIndex];
}