'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSFMS
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFMS"
        End Get
    End Property

#Region "TXN_NO"

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SFMS_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "DDL/GRID"

    Public Function GetCatcode(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SFMS_CATCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSubCatcode(ByVal strSalesRepCode As String, ByVal strCatCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SUB_CATCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "SFMS HDR"
    Public Sub InstTMPSFMSHeader(ByVal strSessionId As String, ByVal strTxnNo As String, _
    ByVal strVisitId As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
     ByVal strRemarks As String, ByVal strUserId As String, ByVal strTxnDate As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SFMS_HDR_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBDouble)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing

    End Sub
#End Region

#Region "SFMS DTL"
    Public Sub InstTMPSFMSDetails(ByVal strSessionId As String, ByVal strTxnNo As String, _
    ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal strSubCatName As String, ByVal strRemarks As String, _
     ByVal dblQty As Double, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SFMS_DTL_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
            .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
            .addItem("SUB_CAT_NAME", strSubCatName, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("QTY", dblQty, clsDB.DataType.DBDouble)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelTMPSFMSDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, ByVal strCatCode As String, ByVal strSubCatCode As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SFMS_DTL_DELETE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
            .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub
#End Region

#Region "SFMS Summ List"

    Public Function GetSFMSSumHdrs(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String _
    , ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SFMS_HDR"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSFMSSumDtl(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SFMS_DTL"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

#Region "SFMS SUBMIT"

    Public Function InstSFMSDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strStatus As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_SFMS_DTL_SUBMIT"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region


#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
