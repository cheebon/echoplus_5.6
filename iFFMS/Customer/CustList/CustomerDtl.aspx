<%@ Page Language="vb" AutoEventWireup="false" Inherits="FFMS_WebOrder_CustomerDtl"
    CodeFile="CustomerDtl.aspx.vb" %>

<%@ Register TagPrefix="uc2" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226" %>
<%@ Register TagPrefix="uc1" TagName="wucctrlPanel" Src="~/include/wuc_ctrlPanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wucHeader" Src="~/include/wuc_lblHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Detail</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <style type="text/css">
#styleone{position:relative;display:block;height:21px;font-size:11px;font-weight:bold;background:transparent url(../../../images/bgOFF.gif) repeat-x top left;font-family:Arial,Verdana,Helvitica,sans-serif;border-bottom:1px solid #d9d9d9;}
#styleone ul{margin:0;padding:0;list-style-type:none;width:auto;}
#styleone ul li{display:block;float:left;margin:0 1px 0 0;}
#styleone ul li a{display:block;float:left;color:#034895;text-decoration:none;padding:3px 20px 0 20px;height:18px;}
#styleone ul li a:hover,#styleone ul li a.current{color:#034895;background:transparent url(../../../images/bgON.gif) repeat-x top left;}
#styleone1{position:relative;display:block;height:21px;font-size:11px;font-weight:bold;background:transparent url(../../../images/bgOFF.gif) repeat-x top left;font-family:Arial,Verdana,Helvitica,sans-serif;border-bottom:1px solid #d9d9d9;}
#styleone1 ul{margin:0;padding:0;list-style-type:none;width:auto;}
#styleone1 ul li{display:block;float:left;margin:0 1px 0 0;}
#styleone1 ul li a{display:block;float:left;color:#034895;text-decoration:none;padding:3px 20px 0 20px;height:18px;}
#styleone1 ul li a:hover,#styleone ul li a.current{color:#034895;background:transparent url(../../../images/bgON.gif) repeat-x top left;}
</style>

    <script language="javascript" type="text/javascript">
    function RefreshContentBar(page)
    {
    if (page == 'GENINFO'){ document.getElementById('applnname').src ='../CustomerGeneralInfo.aspx' }
    if (page == 'CONT'){ document.getElementById('applnname').src ='../CustomerContactGeneralInfo.aspx' }
    if (page == 'DELADD'){ document.getElementById('applnname').src ='../CustomerShipto.aspx' }
    if (page == 'OPENITEM'){ document.getElementById('applnname').src ='../CustomerOpenItem.aspx' }
    if (page == 'ARAGE'){ document.getElementById('applnname').src ='../CustomerARAging.aspx' }
    if (page == 'MTHSAL'){ document.getElementById('applnname').src ='../CustomerMthSalesHist.aspx' }
    if (page == 'INVHIS'){ document.getElementById('applnname').src ='../CustomerInvoiceHist.aspx' }
    if (page == 'SFMSHIS'){ document.getElementById('applnname').src ='../CustomerSFMSHist.aspx' }
    if (page == 'DRC'){ document.getElementById('applnname').src ='../CustomerDealerRec.aspx' }
    if (page == 'ORSTAT'){ document.getElementById('applnname').src ='../CustomerOrderStatus.aspx' }
    if (page == 'CUSTDIS'){ document.getElementById('applnname').src ='../CustomerCustDisc.aspx' }
    if (page == 'PROMOCAMP'){ document.getElementById('applnname').src ='../CustomerPromoCamp.aspx' }
    if (page == 'PLANHIS'){ document.getElementById('applnname').src ='../CustomerPlanListDetail.aspx' }
    }
    function setClass(element)
    {if(element){var pElement = element.parentElement.parentElement;for(var i=0;i<pElement.children.length;i++){var innerElement= pElement.children[i].children[0];if(innerElement){if(innerElement==element){innerElement.className='current';}else{innerElement.className='';}}}}}

    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');ShowElement('ContentBar');MaximiseFrameHeight('ContentBarIframe');ResizeFrameWidth('ContentBar','100%');">
    <form id="frmMainCustomerDetails" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%">
            <div style="width: 100%; margin: 0; padding: 0;">
                <uc1:wucHeader ID="WucHeader" runat="server"></uc1:wucHeader>
            </div>
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" class="BckgroundInsideContentLayout">
                <tr>
                    <td colspan="5">
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <%--<input type="button" runat="server" id="btnback" value="Back" onclick="HideElement('ContentBar');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');ResizeFrameWidth('TopBar','100%')"
                            style="width: 60px;" class="cls_button" />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <div id="styleone">
                            <ul>
                                <li><a id="A1" href="#" onclick="RefreshContentBar('GENINFO');">General Info</a></li>
                                <li><a id="A2" href="#" onclick="RefreshContentBar('CONT');">Contact</a></li>
                                <li><a id="A3" href="#" onclick="RefreshContentBar('DELADD');">Delivery Address</a></li>
                                <li><a id="A4" href="#" onclick="RefreshContentBar('OPENITEM');">Open Item</a></li>
                                <li><a id="A5" href="#" onclick="RefreshContentBar('ARAGE');">AR Aging</a></li>
                                <li><a id="A6" href="#" onclick="RefreshContentBar('MTHSAL');">Mth Sales Hist</a></li>
                                <li><a id="A7" href="#" onclick="RefreshContentBar('INVHIS');">Invoice Hist</a></li>
                                <li><a id="A8" href="#" onclick="RefreshContentBar('SFMSHIS');">SFMS Hist</a></li>
                            </ul>
                        </div>
                        <div id="styleone1">
                            <ul>
                                <li><a id="A9" href="#" onclick="RefreshContentBar('DRC');">Dealer Rec</a></li>
                                <li><a id="A10" href="#" onclick="RefreshContentBar('ORSTAT');">Order Status</a></li>
                                <li><a id="A11" href="#" onclick="RefreshContentBar('CUSTDIS');">Cust Disc</a></li>
                                <li><a id="A12" href="#" onclick="RefreshContentBar('PROMOCAMP');">Promo Camp</a></li>
                                <li><a id="A13" href="#" onclick="RefreshContentBar('PLANHIS');">Plan History</a></li>
                                <li><a id="A14" href="#" onclick="HideElement('ContentBar');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');ResizeFrameWidth('TopBar','100%')">Back</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <%-- <tr>
                    <td width="100%" bgcolor="#ccccff" colspan="5" height="15" visible="false">
                        <uc2:TabStrip ID="custtab" runat="server" Width="100%" AutoPostBack="True" TabDefaultStyle="color:#aaaaaa;background-color:#E7E7FF; 
                        border-color:#AAAAAA;border-width:1px;border-style:Solid;
                       font-weight:bold;font-family:Verdana, Arial, Times New Roman;font-size:10px;height:21; 
                       width:70;text-align:center;" TabHoverStyle="color:#000000;background-color:#EFF8FC"
                            TabSelectedStyle="color:#000000;background-color:#FFC082;
                        border-color:#000000;border-width:1px;border-style:Solid;
                       font-weight:bold;font-family:Verdana, Arial, Times New Roman;font-size:10px;height:21; 
                       width:70;text-align:center;" SepDefaultStyle="background-color:#FFFFFF;border-color:#AAAAAA;
                       border-width:1px;border-style:solid;border-top:none;border-left:none;
                       border-right:none">
                        </uc2:TabStrip>
                    </td>
                </tr>src="<%=sIFrameSrc%>"--%>
                <tr>
                    <td colspan="5" width="100%">
                        <iframe id="applnname" style="width: 100%; height: 480px" marginwidth="0" src="../CustomerGeneralInfo.aspx"
                            frameborder="0" height="480px" scrolling="auto" ></iframe>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
