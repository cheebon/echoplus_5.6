Namespace Echoplus

Partial Class Timer
    Inherits System.Web.UI.Page

    Public frmName As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents ltlAlert As System.Web.UI.WebControls.Literal
    Protected WithEvents Table1 As System.Web.UI.WebControls.Table
    Protected WithEvents lblErr As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim intMonth As Integer
        Dim intYear As Integer
        Dim i As Integer
        Dim strHour, strMin As String

        frmName = Trim(Request.QueryString("frmName").ToString())
        'control.Value = trim(Request.QueryString("textbox").ToString())

        If IsPostBack = False Then
            tbSelYear.Items.Clear()
            For i = 1900 To 2100
                tbSelYear.Items.Add(i)
            Next

            'Assign the month and year to date
            intMonth = Month(Now)
            intYear = Year(Now)
            SetMonthYear(intMonth, intYear)
            btnCalendar.Text = "Today : " & Now.ToString("dd/MM/yyyy")

            ddHour.Items.Clear()
            For i = 0 To 23
                If i >= 0 And i <= 9 Then
                    strHour = "0" & CStr(i)
                Else
                    strHour = CStr(i)
                End If
                ddHour.Items.Add(strHour)
            Next

            ddMin.Items.Clear()
            For i = 0 To 59
                If i >= 0 And i <= 9 Then
                    strMin = "0" & CStr(i)
                Else
                    strMin = CStr(i)
                End If
                ddMin.Items.Add(strMin)
            Next

            'Set Today Hour & Min
            ddHour.SelectedValue = Format(TimeOfDay, "HH")
            ddMin.SelectedValue = Format(TimeOfDay, "mm")
        End If
    End Sub

    Private Sub UpdateCalendar(ByVal Month As Integer, ByVal Year As Integer)
        'Dim strWeek() As String = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}
        Dim i As Integer
        Dim j As Integer
        Dim strDate As String
        Dim datDate As Date
        Dim intDayOfWeek As Integer
        Dim intDaysInPrevMonth As Integer
        Dim intDaysInCurrentMonth As Integer
            'Dim intDaysInNextMonth As Integer
        Dim intPrevMonthDay As Integer
        Dim intCurrentMonthDay As Integer
        Dim intNextMonthDay As Integer
        Dim strArray(6, 7) As String
        Dim values As New ArrayList

        'put in the week
        'Dim tempRow As New TableRow
        'For i = 0 To 6
        '    Dim tempCell As New TableCell
        '    tempCell.Text = strWeek(i)
        '    tempCell.Font.Bold = True
        '    tempCell.ForeColor = Color.DarkBlue
        '    tempCell.Width = Unit.Pixel(25)
        '    tempCell.HorizontalAlign = HorizontalAlign.Center
        '    tempCell.VerticalAlign = VerticalAlign.Middle
        '    tempCell.BackColor = Color.White
        '    tempRow.Cells.Add(tempCell)
        'Next
        'Table1.Rows.Add(tempRow)

        If Month = 1 Then
            intDaysInPrevMonth = System.DateTime.DaysInMonth(Year - 1, 12)
        Else
            intDaysInPrevMonth = System.DateTime.DaysInMonth(Year, Month - 1)
        End If
        intDaysInCurrentMonth = System.DateTime.DaysInMonth(Year, Month)
        'intDaysInNextMonth = System.DateTime.DaysInMonth(Year, Month + 1)

        strDate = Month & "/" & "1" & "/" & Year
        datDate = CDate(strDate)
        intDayOfWeek = datDate.DayOfWeek()

        intPrevMonthDay = intDaysInPrevMonth - (intDayOfWeek - 1)
        intCurrentMonthDay = 1
        intNextMonthDay = 1

        For i = 0 To 5
            'Dim tempRow1 As New TableRow
            For j = 0 To 6
                'Dim tempCell As New TableCell
                If intPrevMonthDay <= intDaysInPrevMonth Then
                    strArray(i, j) = ""
                    'strArray(i, j) = "<font id=cellText Victor='Prev' color =#999999>" & intPrevMonthDay & "<font>"
                    'tempCell.Text = strArray(i, j)
                    'tempCell.Font.Bold = True
                    'tempCell.ForeColor = Color.LightGray
                    'tempCell.Width = Unit.Pixel(25)
                    'tempCell.HorizontalAlign = HorizontalAlign.Center
                    'tempCell.VerticalAlign = VerticalAlign.Middle
                    'tempCell.BackColor = Color.White
                    intPrevMonthDay = intPrevMonthDay + 1
                ElseIf intCurrentMonthDay <= intDaysInCurrentMonth Then
                    ''strArray(i, j) = intCurrentMonthDay
                    'tempCell.Text = strArray(i, j)
                    'tempCell.Font.Bold = True
                    'tempCell.Width = Unit.Pixel(25)
                    'tempCell.HorizontalAlign = HorizontalAlign.Center
                    'tempCell.VerticalAlign = VerticalAlign.Middle
                    'tempCell.BackColor = Color.White
                    ''If j = 0 Or j = 6 Then
                    ''    'tempCell.ForeColor = Color.Red
                    ''    strArray(i, j) = "<font id=cellText Victor='Now' color =Red>" & intCurrentMonthDay & "<font>"
                    ''Else
                    ''    strArray(i, j) = "<font id=cellText Victor='Now'>" & intCurrentMonthDay & "<font>"
                    ''End If
                    strArray(i, j) = intCurrentMonthDay
                    intCurrentMonthDay = intCurrentMonthDay + 1
                Else
                    strArray(i, j) = ""
                    'strArray(i, j) = "<font id=cellText Victor='Next' color =#999999>" & intNextMonthDay & "<font>"
                    'tempCell.Text = strArray(i, j)
                    'tempCell.Font.Bold = True
                    'tempCell.ForeColor = Color.LightGray
                    'tempCell.Width = Unit.Pixel(25)
                    'tempCell.HorizontalAlign = HorizontalAlign.Center
                    'tempCell.VerticalAlign = VerticalAlign.Middle
                    'tempCell.BackColor = Color.White
                    intNextMonthDay = intNextMonthDay + 1
                End If
                'tempRow1.Cells.Add(tempCell)
            Next
            'Table1.Rows.Add(tempRow1)
            values.Add(New PositionData(strArray(i, 0), strArray(i, 1), strArray(i, 2), strArray(i, 3), strArray(i, 4), strArray(i, 5), strArray(i, 6)))
        Next


        Repeater1.DataSource = values
        Repeater1.DataBind()
    End Sub

    Private Sub SetMonthYear(ByVal Month As Integer, ByVal Year As Integer)
        tbSelMonth.SelectedValue = Month
        tbSelYear.SelectedValue = Year

        UpdateCalendar(Month, Year)
    End Sub

    Private Sub PrevMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrevMonth.Click
        Dim intMonth As String
        Dim intYear As String

        intMonth = tbSelMonth.SelectedValue
        intYear = tbSelYear.SelectedValue

        If tbSelMonth.SelectedValue = 1 Then
            intMonth = 12
            intYear = intYear - 1
        Else
            intMonth = intMonth - 1
        End If
        SetMonthYear(intMonth, intYear)
    End Sub

    Private Sub NextMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextMonth.Click
        Dim intMonth As String
        Dim intYear As String

        intMonth = tbSelMonth.SelectedValue
        intYear = tbSelYear.SelectedValue

        If tbSelMonth.SelectedValue = 12 Then
            intMonth = 1
            intYear = intYear + 1
        Else
            intMonth = intMonth + 1
        End If
        SetMonthYear(intMonth, intYear)
    End Sub

    Private Sub tbSelMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbSelMonth.SelectedIndexChanged
        Dim intMonth As String
        Dim intYear As String

        intMonth = tbSelMonth.SelectedValue
        intYear = tbSelYear.SelectedValue
        SetMonthYear(intMonth, intYear)
    End Sub

    Private Sub tbSelYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbSelYear.SelectedIndexChanged
        Dim intMonth As String
        Dim intYear As String

        intMonth = tbSelMonth.SelectedValue
        intYear = tbSelYear.SelectedValue
        SetMonthYear(intMonth, intYear)
    End Sub

    Public Class PositionData

        Private myday1 As String
        Private myday2 As String
        Private myday3 As String
        Private myday4 As String
        Private myday5 As String
        Private myday6 As String
        Private myday7 As String

        Public Sub New(ByVal newday1 As String, ByVal newday2 As String, ByVal newday3 As String, ByVal newday4 As String, ByVal newday5 As String, ByVal newday6 As String, ByVal newday7 As String)
            Me.myday1 = newday1
            Me.myday2 = newday2
            Me.myday3 = newday3
            Me.myday4 = newday4
            Me.myday5 = newday5
            Me.myday6 = newday6
            Me.myday7 = newday7
        End Sub

        Public ReadOnly Property day1() As String
            Get
                Return myday1
            End Get
        End Property

        Public ReadOnly Property day2() As String
            Get
                Return myday2
            End Get
        End Property

        Public ReadOnly Property day3() As String
            Get
                Return myday3
            End Get
        End Property

        Public ReadOnly Property day4() As String
            Get
                Return myday4
            End Get
        End Property

        Public ReadOnly Property day5() As String
            Get
                Return myday5
            End Get
        End Property

        Public ReadOnly Property day6() As String
            Get
                Return myday6
            End Get
        End Property

        Public ReadOnly Property day7() As String
            Get
                Return myday7
            End Get
        End Property
    End Class

    Private Sub Repeater1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles Repeater1.ItemCommand
        Dim strDay As String
        Dim strDate As String
        Dim strMonth As String
        Dim strScript As String

        strDay = CStr(e.CommandArgument)
        If strDay <> "" Then
            strMonth = tbSelMonth.SelectedItem.Value()
            If Len(Trim(strDay)) = 1 Then
                strDay = "0" & strDay
            End If
            If Len(Trim(strMonth)) = 1 Then
                strMonth = "0" & strMonth
            End If
            strDate = strDay & "/" & strMonth & "/" & tbSelYear.SelectedItem.Value

            'strScript = "<script>window.opener.document.forms(0)." + control.Value + ".value = '"
            'strScript += strDate
            'strScript += "';self.close()"
            'strScript += "</" + "script>"
            'RegisterClientScriptBlock("anything", strScript)
            'clientScript()

            strScript = "<script>"
            'If Format(calDate.SelectedDate, "dd/MM/yyyy") <> "01/01/0001" Then
            strScript += "window.opener." + frmName + ".value = '"
            strScript += strDate + " " + CStr(ddHour.SelectedValue) + ":" + CStr(ddMin.SelectedValue) + ":00"
            strScript += "';"
            'End If
            strScript += "window.close()"
            strScript += "</" + "script>"

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)
        End If
    End Sub

    Private Sub btnCalendar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalendar.Click
        'Dim strScript As String

        'strScript = "<script>window.opener.document.forms(0)." + control.Value + ".value = '"
        'strScript += Now.ToString("dd/MM/yyyy")
        'strScript += "';self.close()"
        'strScript += "</" + "script>"
        'RegisterClientScriptBlock("anything", strScript)

        Dim strScript As String

        strScript = "<script>"
        'If Format(calDate.SelectedDate, "dd/MM/yyyy") <> "01/01/0001" Then
        strScript += "window.opener." + frmName + ".value = '"
        strScript += Now.ToString("dd/MM/yyyy") + " " + CStr(ddHour.SelectedValue) + ":" + CStr(ddMin.SelectedValue) + ":00"
        strScript += "';"
        'End If
        strScript += "window.close()"
        strScript += "</" + "script>"

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "anything", strScript)
    End Sub
End Class

End Namespace
