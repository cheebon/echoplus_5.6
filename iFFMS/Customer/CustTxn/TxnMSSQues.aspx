<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnMSSQues.aspx.vb" Inherits="TxnMSSQues" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtDate" Src="~/include/wuc_txtDate.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSS Question</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function Refresh(strDirection) {
            var ifra = self.parent.document.getElementById('SubDetailBarIframe');
            if (strDirection == 'UP') { ifra.contentWindow.ClickBtnUP(); } else { ifra.contentWindow.ClickBtnDOWN(); }
        }

        function ConfirmNext() {
            if (confirm('You have not save the current transaction! Are you sure you want to continue?')) {
                document.getElementById('btnNextConfirm').click();
            }
        }

        function ConfirmPrevious() {
            if (confirm('You have not save the current transaction! Are you sure you want to continue?')) {
                document.getElementById('btnPreviousConfirm').click();
            }
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="MaximiseFrameHeight('SubDetailConfBarIframe');">
    <form id="frmMssQues" runat="server">
    <fieldset class="" style="width: 98%">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <div id="title">
                    <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                </div>
                <div style="border: 0; padding: 0; margin: 0; width: 99.8%; border: solid 1px black;
                    background-color: Gray">
                    <asp:Button runat="server" ID="btnprevious" Text="<<" CssClass="cls_button" Width="80px" />
                    <asp:Button ID="btnPreviousConfirm" runat="server" Text="Button" Style="display: none"
                        OnClick="btnPreviousConfirm_Click" />
                    <asp:Button runat="server" ID="btnnext" Text=">>" CssClass="cls_button" Width="80px" />
                    <asp:Button ID="btnNextConfirm" runat="server" Text="Button" Style="display: none"
                        OnClick="btnNextConfirm_Click" />
                    <asp:Button runat="server" ID="btnsave" Text="Save" CssClass="cls_button" Width="80px"
                        ValidationGroup="Save" />
                    <input type="button" runat="server" id="btnclose" value="Close" onclick="HideElement('SubDetailConfBar');ResizeFrameWidth('SubDetailBar','100%');MaximiseFrameHeight('SubDetailBarIframe');"
                        class="cls_button" style="width: 80px" />
                </div>
                <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 0px; width: 98%;"
                    class="BckgroundInsideContentLayout">
                    <fieldset style="padding-left: 10px; width: 100%; padding-bottom: 5px;" class="S_DivHeader">
                        <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    </fieldset>
                    <div id="S1Div" runat="server">
                        <fieldset runat="server" id="S1" style="padding-left: 10px; width: 100%; height: 50px;
                            padding-bottom: 5px; border: solid 1px black">
                            <span style="float: left; width: 100%; padding-top: 2px; padding-bottom: 2px;">
                                <asp:Label ID="lblS1Code" runat="server" CssClass="cls_label_header" />
                                <asp:Label ID="lblS1" runat="server" CssClass="cls_label_header" />
                            </span>
                            <asp:TextBox ID="txtS1" runat="server" CssClass="cls_textbox" Visible="false" />
                            <asp:DropDownList ID="ddlS1" runat="server" CssClass="cls_dropdownlist" Visible="false" />
                            <customToolkit:wuc_txtDate ID="txtDateS1" runat="server" RequiredValidation="true"
                                RequiredValidationGroup="Save" DateTimeFormatString="yyyy-MM-dd" Visible="false" />
                            <asp:RadioButtonList ID="rdbListS1" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Value="1" Text="True" Selected="True" />
                                <asp:ListItem Value="0" Text="False" />
                            </asp:RadioButtonList>
                            <ccGV:clsGridView ID="dgTextS1" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false"
                                DataKeyNames="CRITERIA_CODE">
                                <Columns>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="Criteria" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtColumnS1" runat="server" class="cls_textbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </ccGV:clsGridView>
                            <ccGV:clsGridView ID="dgSelectS1" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false"
                                DataKeyNames="CRITERIA_CODE">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center" HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectS1" runat="server" class="cls_checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="Criteria" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </ccGV:clsGridView>
                        </fieldset>
                    </div>
                    <div id="S2Div" runat="server">
                        <fieldset runat="server" id="S2" style="padding-left: 10px; width: 100%; height: 50px;
                            padding-bottom: 5px; border: solid 1px black">
                            <span style="float: left; width: 100%; padding-top: 2px; padding-bottom: 2px;">
                                <asp:Label ID="lblS2Code" runat="server" CssClass="cls_label_header" />
                                <asp:Label ID="lblS2" runat="server" CssClass="cls_label_header" />
                            </span>
                            <asp:TextBox ID="txtS2" runat="server" CssClass="cls_textbox" Visible="false" />
                            <asp:DropDownList ID="ddlS2" runat="server" CssClass="cls_dropdownlist" Visible="false" />
                            <customToolkit:wuc_txtDate ID="txtDateS2" runat="server" RequiredValidation="true"
                                RequiredValidationGroup="Save" DateTimeFormatString="yyyy-MM-dd" Visible="false" />
                            <asp:RadioButtonList ID="rdbListS2" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Value="1" Text="True" Selected="True" />
                                <asp:ListItem Value="0" Text="False" />
                            </asp:RadioButtonList>
                            <ccGV:clsGridView ID="dgTextS2" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false"
                                DataKeyNames="CRITERIA_CODE">
                                <Columns>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="Criteria" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtColumnS2" runat="server" class="cls_textbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </ccGV:clsGridView>
                            <ccGV:clsGridView ID="dgSelectS2" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false"
                                DataKeyNames="CRITERIA_CODE">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center" HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectS2" runat="server" class="cls_checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="Criteria" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </ccGV:clsGridView>
                        </fieldset>
                    </div>
                    <div id="S3Div" runat="server">
                        <fieldset runat="server" id="S3" style="padding-left: 10px; width: 100%; height: 50px;
                            padding-bottom: 5px; border: solid 1px black">
                            <span style="float: left; width: 100%; padding-top: 2px; padding-bottom: 2px;">
                                <asp:Label ID="lblS3Code" runat="server" CssClass="cls_label_header" />
                                <asp:Label ID="lblS3" runat="server" CssClass="cls_label_header" />
                            </span>
                            <asp:TextBox ID="txtS3" runat="server" CssClass="cls_textbox" Visible="false" />
                            <asp:DropDownList ID="ddlS3" runat="server" CssClass="cls_dropdownlist" Visible="false" />
                            <customToolkit:wuc_txtDate ID="txtDateS3" runat="server" RequiredValidation="true"
                                RequiredValidationGroup="Save" DateTimeFormatString="yyyy-MM-dd" Visible="false" />
                            <asp:RadioButtonList ID="rdbListS3" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Value="1" Text="True" Selected="True" />
                                <asp:ListItem Value="0" Text="False" />
                            </asp:RadioButtonList>
                            <ccGV:clsGridView ID="dgTextS3" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false"
                                DataKeyNames="CRITERIA_CODE">
                                <Columns>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="Criteria" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center" HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtColumnS3" runat="server" class="cls_textbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </ccGV:clsGridView>
                            <ccGV:clsGridView ID="dgSelectS3" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false"
                                DataKeyNames="CRITERIA_CODE">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center" HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectS3" runat="server" class="cls_checkbox" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="Criteria" ReadOnly="True">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                </Columns>
                            </ccGV:clsGridView>
                        </fieldset>
                    </div>
                    <div id="S4Div" runat="server">
                        <fieldset runat="server" id="S4" style="padding-left: 10px; width: 100%; height: 50px;
                            padding-bottom: 5px; border: solid 1px black">
                            <span style="float: left; width: 100%; padding-top: 2px; padding-bottom: 2px;">
                                <asp:Label ID="lblS4Code" runat="server" CssClass="cls_label_header" />
                                <asp:Label ID="lblS4" runat="server" CssClass="cls_label_header" />
                            </span>
                            <asp:TextBox ID="txtS4" runat="server" CssClass="cls_textbox" Visible="false" />
                            <asp:DropDownList ID="ddlS4" runat="server" CssClass="cls_dropdownlist" Visible="false" />
                            <customToolkit:wuc_txtDate ID="txtDateS4" runat="server" RequiredValidation="true"
                                RequiredValidationGroup="Save" DateTimeFormatString="yyyy-MM-dd" Visible="false" />
                            <asp:RadioButtonList ID="rdbListS4" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Value="1" Text="True" Selected="True" />
                                <asp:ListItem Value="0" Text="False" />
                            </asp:RadioButtonList>
                        </fieldset>
                    </div>
                    <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdQuesCode" runat="server" Value="" />
                    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </fieldset>
    </form>
</body>
</html>
