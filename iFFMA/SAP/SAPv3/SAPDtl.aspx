﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPDtl.aspx.vb" Inherits="iFFMA_SAP_New_SAPDtl" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SAP Detail</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    
<script src="/Echoplus/include/layout.js" type="text/javascript"></script>

</head>
<!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout" onload="HideElement('ContentBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe');resetSize('div_dgList','DetailBarIframe');">
    <form id="frmSAPDtl" runat="server">
     <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" LoadScriptsBeforeUI="true" />
   <br />
     <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always" RenderMode="Inline" >
            <ContentTemplate>
    <table border="0" cellpadding="0" cellspacing="0" width="98%">
        <tr>
            <td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td>
        </tr>
        <tr>
            <td><uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader></td>
        </tr>
        <tr><td class="BckgroundBenealthTitle" height="5"></td></tr>
        <tr class="Bckgroundreport">
        <td>
            <asp:button id="btnResubmit" runat="server" Text="Resubmit" CssClass="cls_button" Visible="false"></asp:button>		
            <asp:button id="btnUnBlock" runat="server" Text="UnBlock" CssClass="cls_button" Visible="false"></asp:button>
            <asp:button id="btnCancel" runat="server" Text="Cancel" CssClass="cls_button" Visible="false"></asp:button>
            <input type="button"  runat="server"  id="btnback" value="Back"  style="width:80px"
                onclick="ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');" class="cls_button" />
        </td>
        </tr>
        <tr class="Bckgroundreport">
            <td style="padding:0px 0px 0px 10px">
                <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">	
                 <tr>
                    <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label></td>
                    <td width="2%"><asp:label id="lblDot35" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblTxnStatus" runat="server" CssClass="cls_label_header">Txn Status</asp:label></td>
                    <td width="2%"><asp:label id="Label2" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblTxnStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <asp:panel id="pnlLN" runat="server">
                 <tr>
                    <td width="20%"><asp:label id="lblLNStatus" runat="server" CssClass="cls_label_header">LN Status</asp:label></td>
                    <td width="2%"><asp:label id="lblDot40" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblLNStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
                 </tr>
                 <tr>
                    <td width="20%"><asp:label id="lblLNRemarks" runat="server" CssClass="cls_label_header">LN Remarks</asp:label></td>
                    <td width="2%"><asp:label id="lblDot41" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblLNRemarksValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>						                    
                </asp:panel>        
                <tr>
                    <td width="20%"><asp:label id="lblTxnNo" runat="server" CssClass="cls_label_header">Txn No</asp:label></td>
                    <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblTxnNoValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblDocNo" runat="server" CssClass="cls_label_header">Saving No</asp:label></td>
                    <td width="2%"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblDocNoValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <tr><td>&nbsp;</td></tr>						                        
                <tr>
                    <td width="20%"><asp:label id="lblSRCode" runat="server" CssClass="cls_label_header">Salesrep Code</asp:label></td>
                    <td width="2%"><asp:label id="lblDot3" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td colspan="4"><asp:label id="lblSRCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <tr>
                    <td width="20%"><asp:label id="lblLastUpdate" runat="server" CssClass="cls_label_header">Last Update</asp:label></td>
                    <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblLastUpdateValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblTxnDate" runat="server" CssClass="cls_label_header">Txn Date</asp:label></td>
                    <td width="2%"><asp:label id="Label3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblTxnDateValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <tr>
                    <td width="20%"><asp:label id="lblOrderType" runat="server" CssClass="cls_label_header">Order Type</asp:label></td>
                    <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblOrderTypeValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblSalesOrg" runat="server" CssClass="cls_label_header">Sales Org</asp:label></td>
                    <td width="2%"><asp:label id="lblDot7" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblSalesOrgValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <tr>
                    <td width="20%"><asp:label id="lblDistChannel" runat="server" CssClass="cls_label_header">Distribution Channel</asp:label></td>
                    <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblDistChannelValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblDivCode" runat="server" CssClass="cls_label_header">Division Code</asp:label></td>
                    <td width="2%"><asp:label id="lblDot4" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblDivCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <tr>
                    <td width="20%"><asp:label id="lblSalesOff" runat="server" CssClass="cls_label_header">Sales Office</asp:label></td>
                    <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblSalesOffValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblSalesGrp" runat="server" CssClass="cls_label_header">Sales Group</asp:label></td>
                    <td width="2%"><asp:label id="lblDot10" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblSalesGrpValue" runat="server" CssClass="cls_label"></asp:label></td>						                           
                </tr>	
                <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblPlant" runat="server" CssClass="cls_label_header">Plant</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot18" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblPlantValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
                    <td vAlign="top" width="20%"><asp:label id="lblSloc" runat="server" CssClass="cls_label_header">Storage Location</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot19" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblSlocValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblShipPoint" runat="server" CssClass="cls_label_header">Shipping Point</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot20" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblShipPointValue" runat="server" CssClass="cls_label"></asp:label></td>	
                     <td width="20%"><asp:label id="lblSoldTo" runat="server" CssClass="cls_label_header">Soldto Party</asp:label></td>
                    <td width="2%"><asp:label id="lblDot11" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblSoldToValue" runat="server" CssClass="cls_label"></asp:label></td>					                        							                        
                </tr>
                <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblShipto" runat="server" CssClass="cls_label_header">Shipto Party</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot12" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblShipToValue" runat="server" CssClass="cls_label"></asp:label></td>
                    <td width="20%"><asp:label id="lblPayerCode" runat="server" CssClass="cls_label_header">Payer Code</asp:label></td>
                    <td width="2%"><asp:label id="lblDot13" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td><asp:label id="lblPayerCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>								                      
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblPONo" runat="server" CssClass="cls_label_header">PO No</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot14" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblPONoValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
                    <td vAlign="top" width="20%"><asp:label id="lblPODate" runat="server" CssClass="cls_label_header">PO Date</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot15" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblPODateValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblPOType" runat="server" CssClass="cls_label_header">PO Type</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot16" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblPOTypeValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
                    <td vAlign="top" width="20%"><asp:label id="lblPOSupp" runat="server" CssClass="cls_label_header">PO Supplement</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot17" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblPOSuppValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>						                        
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblBName" runat="server" CssClass="cls_label_header">Name of Orderer</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot21" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblBNameValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
                    <td vAlign="top" width="20%"><asp:label id="lblIntRef" runat="server" CssClass="cls_label_header">Customer / Vendor Internal Ref</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot22" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblIntRefValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblHdrShipText1" runat="server" CssClass="cls_label_header">Header shipping text 1</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot23" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top" colspan="4"><asp:label id="lblHdrShipText1Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblHdrShipText2" runat="server" CssClass="cls_label_header">Header shipping text 2</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot24" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top" colspan="4"><asp:label id="lblHdrShipText2Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblHdrShipText3" runat="server" CssClass="cls_label_header">Header shipping text 3</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot25" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top" colspan="4"><asp:label id="lblHdrShipText3Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblHdrDiscType1" runat="server" CssClass="cls_label_header">Header disc. type 1</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot26" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblHdrDiscType1Value" runat="server" CssClass="cls_label"></asp:label></td>		
                    <td vAlign="top" width="20%"><asp:label id="lblHdrDiscVal1" runat="server" CssClass="cls_label_header">Header disc. value 1</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot27" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblHdrDiscVal1Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblHdrDiscType2" runat="server" CssClass="cls_label_header">Header disc. type 2</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot28" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblHdrDiscType2Value" runat="server" CssClass="cls_label"></asp:label></td>		
                    <td vAlign="top" width="20%"><asp:label id="lblHdrDiscVal2" runat="server" CssClass="cls_label_header">Header disc. value 2</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot29" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblHdrDiscVal2Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
                </tr>	
                <tr>
                    <td vAlign="top"><asp:label id="lblDelDate" runat="server" CssClass="cls_label_header">Delivery Date</asp:label></td>
                    <td vAlign="top"><asp:label id="lblDot30" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblDelDateValue" runat="server" CssClass="cls_label"></asp:label></td>
                     <td vAlign="top"><asp:label id="lblUsageInd" runat="server" CssClass="cls_label_header">Usage Indicator</asp:label></td>
                    <td vAlign="top"><asp:label id="Labe31" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblUsageIndValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>					                        
                <tr>
                    <td vAlign="top"><asp:label id="lblResendCnt" runat="server" CssClass="cls_label_header">Resubmit Counter</asp:label></td>
                    <td vAlign="top"><asp:label id="lblDot32" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblResendCntValue" runat="server" CssClass="cls_label"></asp:label></td>
                     <td vAlign="top"><asp:label id="lblOrderBy" runat="server" CssClass="cls_label_header">Order By</asp:label></td>
                    <td vAlign="top"><asp:label id="lblDot33" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top"><asp:label id="lblOrderByValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>
                 <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblRemarks" runat="server" CssClass="cls_label_header">Remarks</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot34" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top" colspan="4"><asp:label id="lblRemarksValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>		
                <tr>
                    <td vAlign="top" width="20%"><asp:label id="lblNote" runat="server" CssClass="cls_label_header">Note</asp:label></td>
                    <td vAlign="top" width="2%"><asp:label id="lblDot38" runat="server" CssClass="cls_label_header">:</asp:label></td>
                    <td vAlign="top" colspan="4"><asp:label id="lblNoteValue" runat="server" CssClass="cls_label"></asp:label></td>
                </tr>							                       			
            </table>
            </td>
        </tr>
        <tr class="Bckgroundreport">
            <td style="padding:0px 0px 0px 10px">
                <br />
                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="true"
                    AllowSorting="true" AutoGenerateColumns="false" GridWidth="" FreezeHeader="true" Width="98%" PagerSettings-Visible="false"
                    GridHeight="400px" RowSelectionEnabled="true" EnableViewState="true" OnSorting="dgList_Sorting">                    
                    <FooterStyle CssClass="GridFooter" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternate" />
                    <RowStyle CssClass="GridNormal" />
                    </ccGV:clsGridView>
            </td>
        </tr>
    </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
