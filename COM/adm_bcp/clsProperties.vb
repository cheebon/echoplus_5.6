﻿Option Explicit On
Option Strict On

Public Class clsProperties
    Public Class clsBCP
        Public bcp_template_id As String
        Public bcp_template_name As String
        Public bcp_template_desc As String
        Public in_seq As String
        Public search_type As String
        Public search_value As String
        Public creator_user_id As String
        Public changed_user_id As String

        Public step_no As String
        Public step_name As String
        Public step_desc As String
        Public mand_step As String
        Public step_ind As String
    End Class
End Class