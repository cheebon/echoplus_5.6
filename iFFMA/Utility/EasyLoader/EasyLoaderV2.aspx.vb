﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports System.IO
Imports System.Text.RegularExpressions
'`	***********************************************************************
'`	Author	:	EuJin
'`	Date	:	2012-03-06
'`	Purpose	:	 
''
'`	Revision	: 	
'`	---------------------------------------------------------------------------------------------------------
'`	|No	|Date Change	|Author		|Remarks				|	
'`	---------------------------------------------------------------------------------------------------------
'`	|1	|			| 		|					|
'`	|2	|			|		|					|
'`	---------------------------------------------------------------------------------------------------------
'`	***********************************************************************
Partial Class iFFMA_Utility_EasyLoader_EasyLoaderV2
    Inherits System.Web.UI.Page

    Private intPageSize As Integer
    Private _aryDataItem As ArrayList

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property EasyLoaderMode() As String
        Get
            Return Session("EasyLoaderMode")
        End Get
        Set(ByVal value As String)
            Session("EasyLoaderMode") = value
        End Set
    End Property

    Private Property dtDatabaseUploadColumnStyle() As DataTable
        Get
            Return CType(ViewState("dtDatabaseUploadColumnStyle"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtDatabaseUploadColumnStyle") = value
        End Set
    End Property

    Public Property Action() As String
        Get
            Return Session("Action")
        End Get
        Set(ByVal value As String)
            Session("Action") = value
        End Set
    End Property

    Public ReadOnly Property PageName() As String
        Get
            Return "Easy Loader v2"
        End Get
    End Property

    Private ReadOnly Property strFilePath() As String
        Get
            Return ConfigurationManager.AppSettings("DataTemplatePath")
        End Get
    End Property

    Private ReadOnly Property strHelpFilePath() As String
        Get
            Return ConfigurationManager.AppSettings("HelpFilePath")
        End Get
    End Property

    Public Property strRegex() As String
        Get
            Return ViewState("strRegex")
        End Get
        Set(ByVal value As String)
            ViewState("strRegex") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "../../../include/common.js")

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        Dim dtFormName As DataTable = Application("dtFormName")
        wuc_lblHeader1.Title = "Upload File" 'Portal.FormName.GetFormName("2", "1", "1", "100", dtFormName) '

        If Not IsPostBack Then
            btnUpload.Visible = False 
            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

            LoadCatDdl()
            LoadActDdl()
            LoadDDL()

        ElseIf IsPostBack Then
            Dim strPassedArgument As String = Request.Params.Get("__EVENTARGUMENT")
            If strPassedArgument = "Export" Then
                Export()
            End If
        End If

        lblMsg.Text = ""
    End Sub

#Region "EVENTS"
    Protected Sub ddlUploadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUploadType.SelectedIndexChanged
        Dim dtUploadType As DataTable
        Dim clsEasyLoaderV2 As mst_Utility.clsEasyLoaderV2
        clsEasyLoaderV2 = New mst_Utility.clsEasyLoaderV2

        Try
            resetDDL()

            dtUploadType = ViewState("dtUploadType") 
            If ddlUploadType.SelectedValue <> "" Then
                LoadActDdl()
                Dim drCurrRow As DataRow()
                drCurrRow = dtUploadType.Select("EL_CODE = '" & ddlUploadType.SelectedValue & "'")

                If drCurrRow.Length > 0 Then

                    If drCurrRow(0)("COLUMN_HDR").ToString = "" Then
                        proceedValidation(False)
                    Else
                        proceedValidation(True)

                        lblDesc.Text = drCurrRow(0)("EL_DESC").ToString
                        lblColumns.Text = "<ol style='margin-top: 5px; margin-bottom: 0px'>" & drCurrRow(0)("COLUMN_HDR").ToString & "</ol>"
                        hfCSVFileNameCheckingFlag.Value = drCurrRow(0)("CSVFileNameCheckingFlag").ToString
                        hfCSVFilename.Value = drCurrRow(0)("CSVFilename").ToString
                        hfdelimiter.Value = drCurrRow(0)("DELIMITER").ToString
                        hfrowbreak.Value = drCurrRow(0)("ROWBREAK").ToString
                        hftmptable.Value = drCurrRow(0)("TMP_TABLE").ToString
                        hfHelpFileName.Value = drCurrRow(0)("HELPFILE_NAME").ToString
                        hfDLFilename.Value = drCurrRow(0)("dl_filename").ToString
                        strRegex = drCurrRow(0)("REGEX").ToString
                    End If
                End If


            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim clsEasyLoaderV2 As mst_Utility.clsEasyLoaderV2

        Try
            Action = "Upload"

            clsEasyLoaderV2 = New mst_Utility.clsEasyLoaderV2
            With clsEasyLoaderV2.clsProperties
                .action_id = ddlAction.SelectedValue
                .user_id = Portal.UserSession.UserID  'Portal.PortalSession.UserProfile.User.UserID 
            End With
            ' Throw New Exception("Test")
            clsEasyLoaderV2.Upload(ddlUploadType.SelectedValue)

            EasyLoaderMode = "Upload"
            RenewDataBind()

            btnUpload.Visible = False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click

        Action = "Validate"
        lblMsg.Text = ""
        pnlFileError.Visible = False

        If fuLocation.HasFile Then
            'Dim tmpExt As String = fuLocation.FileName.Split(".", 3, StringSplitOptions.RemoveEmptyEntries)(1)
            Dim FileExt As String = System.IO.Path.GetExtension(fuLocation.FileName)
            Dim strFileName As String = ddlUploadType.SelectedValue + "_" + fuLocation.FileName.Replace(FileExt, "") + "_" + Portal.UserSession.UserID + "_" + Now().ToString("yyyy") + Now().ToString("MM") + Now().ToString("dd") + Now().ToString("HH") + Now().ToString("mm") + Now().ToString("ss") + FileExt
            Dim tmpExt As String = FileExt.Replace(".", "")
            Dim strDocPath As String = System.Configuration.ConfigurationManager.AppSettings("UploadPath")

            Try
                If tmpExt.ToUpper = "TXT" Or tmpExt.ToUpper = "CSV" Then
                    System.IO.File.Delete(Server.MapPath(strDocPath + strFileName))

                    fuLocation.SaveAs(Server.MapPath(strDocPath + strFileName))

                    lblUploadInfo.Text = "Upload Type : " & ddlUploadType.SelectedItem.Text & "<br>" & _
                                         "Upload File Name : " & fuLocation.PostedFile.FileName & "<br>" & _
                                         "Type : " & fuLocation.PostedFile.ContentType & "<br>" & _
                                         "File Size : " & fuLocation.PostedFile.ContentLength & " kb<br>"

                    If tmpExt.ToUpper = "CSV" Or tmpExt.ToUpper = "TXT" Then
                        LoadFile(strDocPath + strFileName)
                    End If

                    ShowResult(True)
                Else
                    lblMsg.Text = "Kindly upload csv files only."
                    'lblTtlRecords.Visible = False
                End If

            Catch ex As Exception
                lblMsg.Text = "Error: " & ex.ToString & Server.MapPath(strDocPath + strFileName)
            Finally
                Dim filePath = Server.MapPath(strDocPath + strFileName)
                System.IO.File.Delete(filePath)
            End Try
        Else
            lblMsg.Text = "Please select a file to upload."
            'lblTtlRecords.Visible = False
        End If

        fuLocation.Dispose()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim clsEasyLoaderV2 As mst_Utility.clsEasyLoaderV2

        Try
            Action = Nothing
            lblErr.Text = ""
            resetList()

            clsEasyLoaderV2 = New mst_Utility.clsEasyLoaderV2
            With clsEasyLoaderV2.clsProperties
                .user_id = Portal.UserSession.UserID
            End With
            clsEasyLoaderV2.Delete(ddlUploadType.SelectedValue)

            ShowResult(False)
            LoadDDL()
            pnlFileError.Visible = False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btndltemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndltemplate.Click
        Try
            Dim strFileName As String = hfDLFilename.value 'GetFileName() 
            Dim strPath As String = Request.PhysicalApplicationPath & strFilePath + strFileName

            Dim success As Boolean = downloadFile(strPath, strFileName)
            If Not success Then
                lblMsg.Text = "Template do not exists!"
                UpdateDatagrid.Update()
            End If

         


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub 

    Private Function downloadFile(ByVal strPath As String, ByVal strFileName As String) As Boolean
        Try
            'Response.ClearContent()
            'Response.AddHeader("content-disposition", "attachment;filename=" + strFileName)
            'Response.TransmitFile(strPath)
            'Response.ContentType = "application/vnd.txt"
            'EnableViewState = False
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            'Response.[End]()

            Response.ContentType = "application/octet-stream"
            Response.AppendHeader("Content-Disposition", "attachment;filename=" + strFileName)
            Response.TransmitFile(strPath)
            Response.End()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function 

    Protected Sub ddlCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCat.SelectedIndexChanged
        LoadDDL()
        UpdateDatagrid.Update()
    End Sub

    Protected Sub imgExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        Export()
    End Sub

#End Region

#Region "DropDownList"

    Private Sub LoadCatDdl()

        Dim dt As DataTable
        Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2
        Try

            With clsEasyLoaderV2.clsProperties
                .user_id = Portal.UserSession.UserID
            End With
            dt = clsEasyLoaderV2.GetCatDdlList()
            With ddlCat
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "CAT_NAME"
                .DataValueField = "CAT_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub LoadActDdl()
        Dim dt As DataTable
        Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2
        Try

            With clsEasyLoaderV2.clsProperties
                .Upload_Type = ddlUploadType.SelectedValue
                .user_id = Portal.UserSession.UserID
            End With
            dt = clsEasyLoaderV2.GetActDdlList()
            With ddlACtion
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "ACTION_NAME"
                .DataValueField = "ACTION_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub LoadDDL()
        Dim dtUploadType As DataTable
        Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2

        Try
            With clsEasyLoaderV2.clsProperties
                .cat_code = ddlCat.SelectedValue
                .user_id = Portal.UserSession.UserID
            End With

            dtUploadType = clsEasyLoaderV2.GetELList()
            With ddlUploadType
                .Items.Clear()
                .DataSource = dtUploadType.DefaultView
                .DataTextField = "EL_NAME"
                .DataValueField = "EL_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            dtUploadType.Columns.Add("COLUMN_HDR")

            For Each DR As DataRow In dtUploadType.Rows
                Dim dtColumnHdr As DataTable
                Dim strColumnHdr As String = ""
                With clsEasyLoaderV2.clsProperties
                    .user_id = Portal.UserSession.UserID
                End With
                dtColumnHdr = clsEasyLoaderV2.GetELDtl(DR("EL_CODE"))
                For Each DR2 As DataRow In dtColumnHdr.Rows
                    strColumnHdr = strColumnHdr & "<li>" & DR2("COLUMN_CODE") & "<span> - </span> " & DR2("COLUMN_DESC")
                Next
                Dim TT = DR("EL_CODE")
                DR("COLUMN_HDR") = strColumnHdr
            Next

            ViewState("dtUploadType") = dtUploadType
            resetDDL()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "FUNCTIONS"

    Private Sub ShowResult(ByVal boolVisible As Boolean)
        pnlValidate.Visible = Not boolVisible
        pnlUpload.Visible = boolVisible
    End Sub

    Private Sub LoadFile(ByVal strFilePathName As String)
        Dim dt As DataTable = ParseCSVFile(Server.MapPath(strFilePathName))
        Dim dt2 As DataTable = New DataTable
        Dim dtColumnHdr As DataTable
        Dim clsEasyLoaderV2 As mst_Utility.clsEasyLoaderV2
        Dim aryColumnHdr, aryColumn As ArrayList

        Try
            'Copy data start from 2nd row from dt(text file) to dt2 and then rename column name follow column in first row in dt
            'dt2 = dt.Clone()
            If dt.Rows.Count > 0 Then
                If dt.Rows.Count > 1 Then
                    dt2 = dt.AsEnumerable().Where(Function(r) dt.Rows.IndexOf(r) > 0).CopyToDataTable()

                    'Dim dr As DataRow
                    'For i = 1 To dt.Rows.Count - 1
                    '    dr = dt.Rows(i)
                    '    dt2.ImportRow(dr)
                    'Next
                End If

                Dim columnNames = dt.AsEnumerable().Where(Function(r) dt.Rows.IndexOf(r) = 0).Select(Function(s) s.ItemArray).ToList()(0)
                'For Each item In dt.Rows
                '    If dt2.Columns Then
                '         Then Then

                '    End If
                'Next

                Dim hasEmptyValue As Boolean = columnNames.Where(Function(e) e.ToString = "").Count
                Dim hasDuplicate As Boolean = columnNames.GroupBy(Function(x) x).Where(Function(g) g.Count > 1).Select(Function(g) g.Key).Count()

                If (hasEmptyValue) Then
                    lblErr3DEsc.Text = "The number of fields in the file is not matched, please check again."
                    pnlFileError.Visible = True
                    Exit Sub
                ElseIf (hasDuplicate) Then
                    lblErr3DEsc.Text = "One or more of the file column names are duplicated, please check again."
                    pnlFileError.Visible = True
                    Exit Sub
                End If


                For i As Integer = 0 To dt.Columns.Count - 1
                    dt2.Columns(i).ColumnName = dt.Rows(0).Item(i).ToString
                    'If dt2.Columns.Contains(dt.Rows(0).Item(i).ToString) Or String.IsNullOrEmpty(dt.Rows(0).Item(i).ToString) Then
                    '    lblErr3DEsc.Text = "The number of fields in the file is not matched, please check again."
                    'Else
                    '    dt2.Columns(i).ColumnName = dt.Rows(0).Item(i).ToString
                    'End If
                Next
            End If

            'Get Column Name from EL Dtl
            clsEasyLoaderV2 = New mst_Utility.clsEasyLoaderV2
            dtColumnHdr = clsEasyLoaderV2.GetELDtl(ddlUploadType.SelectedValue)
            aryColumnHdr = New ArrayList
            For Each DR As DataRow In dtColumnHdr.Rows
                aryColumnHdr.Add(DR("COLUMN_CODE"))
            Next

            'Compare column name in dt2 with EL Dtl (aryColumnHdr)
            lblErr3DEsc.Text = ""
            If dt2.Columns.Count = aryColumnHdr.Count Then
                For i = 0 To dt2.Columns.Count - 1
                    If dt2.Columns(i).ColumnName <> aryColumnHdr(i).ToString Then
                        lblErr3DEsc.Text = "The column name not matched and must be lower case, please check again." + "(Hints column: " + dt2.Columns(i).ColumnName + ")"
                        Exit For
                    End If
                Next
            Else
                lblErr3DEsc.Text = "The number of fields in the file is not matched, please check again."
            End If

            If lblErr3DEsc.Text = "" Then
                Dim dc As DataColumn
                dc = New DataColumn("UploadDate")
                dc.DefaultValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                dt2.Columns.Add(dc)
                dc = New DataColumn("UploadBy")
                dc.DefaultValue = Portal.UserSession.UserID
                dt2.Columns.Add(dc)

                With clsEasyLoaderV2.clsProperties
                    .action_id = ddlAction.SelectedValue
                    .user_id = Portal.UserSession.UserID
                End With
                clsEasyLoaderV2.Delete(ddlUploadType.SelectedValue) 'Clear temporary table first
                ' clsEasyLoaderV2.BulkInsert(ddlUploadType.SelectedValue, dt2)
                Dim strErrorMsg = clsEasyLoaderV2.BulkInsert(ddlUploadType.SelectedValue, dt2, hftmptable.Value)

                'For i = 0 To dt.Rows.Count - 1
                '    If dt.Rows(i).Item(0).ToString.ToUpper Like "*" & aryColumnHdr.Item(0).ToString.ToUpper & "*" Then
                '        'Skip header row if detected
                '    Else
                '        aryColumn = New ArrayList
                '        For j As Integer = 0 To dt.Columns.Count - 1
                '            aryColumn.Insert(j, dt.Rows(i).Item(j))
                '        Next

                '        If aryColumnHdr.Count = aryColumn.Count Then
                '            clsEasyLoader.Insert(ddlUploadType.SelectedValue, aryColumnHdr, aryColumn) 'insert into temp table
                '        Else
                '            pnlFileError.Visible = True
                '            lblErrMsgValidate.Text = ""
                '            Exit Sub
                '        End If
                '    End If
                'Next

                If Not String.IsNullOrEmpty(strErrorMsg) Then
                    lblErr3DEsc.Text = strErrorMsg
                    pnlFileError.Visible = True
                    Exit Sub
                End If

                clsEasyLoaderV2.Validate(ddlUploadType.SelectedValue)
                EasyLoaderMode = "Validate"
                RenewDataBind()

                pnlFileError.Visible = False
                pnlSystemError.Visible = False
            Else
                pnlFileError.Visible = True
                lblErrMsgValidate.Text = ""
            End If

            'btnUpload.Visible = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub OnSqlRowsCopied(ByVal sender As Object, _
        ByVal args As System.Data.SqlClient.SqlRowsCopiedEventArgs)

        value = args.RowsCopied
        'Console.WriteLine("Copied {0} so far...", args.RowsCopied)
    End Sub

    Public value As String = ""

    Private Function ParseCSV(ByVal inputString As String) As DataTable
        Dim dt As New DataTable()

        ' declare the Regular Expression that will match versus the input string 
        'Dim re As New Regex("((?<field>[^""\|\r\n]+)|""(?<field>([^""]|"""")+)"")(\||(?<rowbreak>\r\n|\n|$))")
        'Dim re As New Regex("((?<field>[^""\t\r\n]+)|""(?<field>([^""]|"""")+)"")(\t|(?<rowbreak>\r\n|\n|$))")
        'Dim re As New Regex("((?<field>[^"",\r\n]+)|""(?<field>([^""]|"""")+)"")(,|(?<rowbreak>\r\n|\n|$))")
        'deliminator \|
        Dim re As New Regex(strRegex)

        Dim strrowbreak As String = hfrowbreak.Value
        If Not IsDBNull(strrowbreak) And strrowbreak <> String.Empty Then
            Dim strnewline As String = System.Environment.NewLine
            inputString = inputString.Replace(strrowbreak, strnewline)  'Added to solve missing column data
        End If

        Dim colArray As New ArrayList()
        Dim rowArray As New ArrayList()

        Dim colCount As Integer = 0
        Dim maxColCount As Integer = 0
        Dim rowbreak As String = ""
        Dim field As String = ""

        ' inputString = inputString.Replace("||", "| |") 'Added to solve missing column data
        Dim mc As MatchCollection = re.Matches(inputString)

        For Each m As Match In mc

            ' retrieve the field and replace two double-quotes with a single double-quote  
            field = m.Result("${field}").Replace("""""", """")

            rowbreak = m.Result("${rowbreak}")

            If field.Length > 0 Then
                colArray.Add(field)
                colCount += 1
            End If

            If rowbreak.Length > 0 Then

                ' add the column array to the row Array List 
                rowArray.Add(colArray.ToArray())

                ' create a new Array List to hold the field values 
                colArray = New ArrayList()

                If colCount > maxColCount Then
                    maxColCount = colCount
                End If

                colCount = 0
            End If

        Next

        If rowbreak.Length = 0 Then
            ' this is executed when the last line doesn't end with a line break 
            rowArray.Add(colArray.ToArray())
            If colCount > maxColCount Then
                maxColCount = colCount
            End If
        End If

        ' create the columns for the table 
        For i As Integer = 0 To maxColCount - 1
            dt.Columns.Add([String].Format("col{0:000}", i))
        Next

        ' convert the row Array List into an Array object for easier access 
        Dim ra As Array = rowArray.ToArray()
        For i As Integer = 0 To ra.Length - 1

            ' create a new DataRow 
            Dim dr As DataRow = dt.NewRow()

            ' convert the column Array List into an Array object for easier access 
            Dim ca As Array = DirectCast((ra.GetValue(i)), Array)

            ' add each field into the new DataRow 
            For j As Integer = 0 To ca.Length - 1
                dr(j) = ca.GetValue(j)
            Next

            ' add the new DataRow to the DataTable 
            dt.Rows.Add(dr)
        Next

        If dt.Columns.Count = 0 Then
            dt.Columns.Add("NoData")
        End If

        Return dt
    End Function

    Private Function ParseCSVFile(ByVal path As String) As DataTable
        Dim inputLine As String = ""
        Dim inputString As String = ""
        Dim strdelimiter As String = hfdelimiter.Value

        If strdelimiter = "\t" Then strdelimiter = ControlChars.Tab


        Dim writer As New StringWriter()

        If File.Exists(path) Then   'check that the file exists before opening it 
            Dim sr As New StreamReader(path)
            'inputString = sr.ReadToEnd()

            While Not sr.EndOfStream
                inputLine = sr.ReadLine

                'inputLine = inputLine.Replace("	", "|").Replace("||", "| |")
                inputLine = inputLine.Replace(ControlChars.Quote, String.Empty)
                Dim strStringtoReplace As String = strdelimiter + strdelimiter
                Dim strNewString As String = strdelimiter + " " + strdelimiter
                inputLine = inputLine.Replace("	", strdelimiter).Replace(strStringtoReplace, strNewString)
                While inputLine.Contains(strStringtoReplace)
                    inputLine = inputLine.Replace("	", strdelimiter).Replace(strStringtoReplace, strNewString)
                End While
                If inputLine.EndsWith(strdelimiter) Then
                    inputLine = inputLine.Insert(inputLine.Length(), " ")
                End If

                'If inputLine.StartsWith("|") Then
                If inputLine.StartsWith(strdelimiter) Then
                    inputLine = inputLine.Insert(0, " ")
                End If

                writer.WriteLine(inputLine)
            End While

            sr.Close()
        End If

        Return ParseCSV(writer.ToString())
    End Function

    Private Sub proceedValidation(ByVal boolProceed As Boolean)
        If boolProceed Then
            pnlDesc.Visible = True
            pnlSystemError.Visible = False
            btnValidate.Enabled = True
        Else
            pnlDesc.Visible = False
            pnlSystemError.Visible = True
            btnValidate.Enabled = False
        End If
    End Sub

    Private Sub resetDDL()
        pnlDesc.Visible = False
        lblDesc.Text = ""
        lblColumns.Text = ""

        pnlSystemError.Visible = False

        btnValidate.Enabled = True
    End Sub

    Private Sub resetList()
        dgList.DataSource = Nothing
        dgList.DataBind()

        btnUpload.Visible = False

        lblTtlRecords.Visible = False
        wuc_dgpaging.Visible = False
    End Sub 

    Private Function GetFileName() As String
        Try
            Dim dt As DataTable
            Dim strFileName As String
            Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2
            With clsEasyLoaderV2.clsProperties
                .Upload_Type = ddlUploadType.SelectedValue
                .user_id = Portal.UserSession.UserID
            End With
            dt = clsEasyLoaderV2.GetDLFileName()
            strFileName = dt.Rows(0)(0)
            Return strFileName
        Catch TAE As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function
 
#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDatabinding()
    End Sub

    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrentTable = GetRecList()

            LoadUploadColumnStyle() ' LOAD IN COLUMN STYLE

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            End If

            dgList_Init(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            Master_Row_Count = dtCurrentTable.Rows.Count

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .DataBind()
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
            End With

            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                '.PageSize = dgList.PageSize
                '.RecordCount = Master_Row_Count
                .DataBind()
                '.RowCount = dvCurrentView.Count
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

            With lblTtlRecords

                .Text = GetActionMessage() '"No of row(s) effected " + CStr(Master_Row_Count)
                .Visible = True
            End With

            'Dim intValidRowCount As Integer
            'intValidRowCount = GetValidRowCount()

            'If dtCurrentTable.Rows.Count = 0 OrElse dtCurrentTable.Rows.Count <> intValidRowCount Then
            If dtCurrentTable.Rows.Count = 0 Then
                If Action = "Validate" And pnlFileError.Visible = False Then
                    lblErrMsgValidate.Text = "There is no record in the upload file."
                ElseIf Action = "Upload" And pnlFileError.Visible = False Then
                    lblErrMsgValidate.Text = "File contains errors. Please refer 'valid_data' column for more information"
                Else
                    lblErrMsgValidate.Text = ""
                End If
                wuc_dgpaging.Visible = False
                btnUpload.Visible = False
            Else
                wuc_dgpaging.Visible = True
                lblErrMsgValidate.Text = ""

                If Action = "Validate" Then
                    btnUpload.Visible = True
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Function GetActionMessage() As String
        Dim strActionMessage As String = Nothing
        Dim DT As DataTable = Nothing
        Try
            Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2
            With clsEasyLoaderV2.clsProperties
                .strMode = EasyLoaderMode
                .action_id = ddlAction.SelectedValue
                .user_id = Portal.UserSession.UserID
            End With
            DT = clsEasyLoaderV2.GetMsg(ddlUploadType.SelectedValue, EasyLoaderMode)
            If DT.Rows.Count > 0 Then
                strActionMessage = Portal.Util.GetValue(Of String)(DT.Rows(0)(0))
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return strActionMessage
    End Function

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2
            With clsEasyLoaderV2.clsProperties
                .user_id = Portal.UserSession.UserID
            End With
            DT = clsEasyLoaderV2.GetList(ddlUploadType.SelectedValue)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_EasyLoaderV2.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_EasyLoaderV2.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If

                        'change column style
                        Dim strColumnStyle As String, strSelect As String
                        strSelect = "column_code like '" + ColumnName + "'"
                        If dtDatabaseUploadColumnStyle.Select(strSelect).Length > 0 Then
                            strColumnStyle = dtDatabaseUploadColumnStyle.Select(strSelect)(0)("Column_Align")
                            If IsDBNull(strColumnStyle) Then strColumnStyle = "Left"
                            dgColumn.ItemStyle.HorizontalAlign = CF_EasyLoaderV2.ColumnStyleCustomised(ColumnName, strColumnStyle).HorizontalAlign
                            dgColumn.ItemStyle.Wrap = CF_EasyLoaderV2.ColumnStyleCustomised(ColumnName, strColumnStyle).Wrap
                        Else
                            dgColumn.ItemStyle.HorizontalAlign = CF_EasyLoaderV2.ColumnStyle(ColumnName).HorizontalAlign
                            dgColumn.ItemStyle.Wrap = CF_EasyLoaderV2.ColumnStyle(ColumnName).Wrap
                        End If
                        'change column style

                        dgColumn.HeaderText = CF_EasyLoaderV2.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName.ToUpper)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header

                Case DataControlRowType.DataRow

                   
                Case DataControlRowType.Footer



            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
 

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header
  
                Case DataControlRowType.DataRow

                    LoadValidData(e)
 
                Case DataControlRowType.Footer



            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadValidData(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim tc As TableCell
            Dim strValue As String
            tc = e.Row.Cells(aryDataItem.IndexOf("VALID_DATA"))
            strValue = Trim(e.Row.Cells(aryDataItem.IndexOf("VALID_DATA")).Text)
            If EasyLoaderMode = "Validate" Then
                If strValue.ToUpper <> "VALID" Then
                    Dim lblMsgColor As New Label
                    With lblMsgColor
                        .ID = "lblMsgColor"
                        .Text = strValue
                        .CssClass = "cls_label_err"
                    End With
                    tc.Controls.Add(lblMsgColor)
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub LoadUploadColumnStyle()
        Dim DT As DataTable = Nothing
        Try
            Dim clsEasyLoaderV2 As New mst_Utility.clsEasyLoaderV2
            With clsEasyLoaderV2.clsProperties
                .user_id = Portal.UserSession.UserID
                .Upload_Type = ddlUploadType.SelectedValue
            End With

            DT = clsEasyLoaderV2.GetELColumnStyle
            dtDatabaseUploadColumnStyle() = DT

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "EXPORT"
    Private Sub Export()
        Try
            dgList.AllowPaging = False

            RefreshDatabinding(True)

            Excel.ExportToFile(dgList, Response, PageName)
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception

        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub




End Class

Public Class CF_EasyLoaderV2
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case Else
                strFieldName = ColumnName.ToUpper
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype

        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        strColumnName = strColumnName.ToUpper

        'If strColumnName Like "*ID" Then
        '    FCT = FieldColumntype.InvisibleColumn
        'Else
        '    FCT = FieldColumntype.BoundColumn
        'End If
        FCT = FieldColumntype.BoundColumn

        Return FCT

    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd h:mm tt}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle


        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)



                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

    Public Shared Function ColumnStyleCustomised(ByVal ColumnName As String, ByVal strColumnStyle As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Dim intHorAlign As Integer

        Try
            If strColumnStyle.ToUpper = "LEFT" Then
                intHorAlign = 1
            ElseIf strColumnStyle.ToUpper = "CENTER" Then
                intHorAlign = 2
            ElseIf strColumnStyle.ToUpper = "RIGHT" Then
                intHorAlign = 3
            Else 'else all left
                intHorAlign = 1
            End If


            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)
                .HorizontalAlign = intHorAlign
                .Wrap = False   'HL:20070711
            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class