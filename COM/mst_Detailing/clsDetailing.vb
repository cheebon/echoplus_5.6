﻿'************************************************************************
'	Author	    :	YONG SOO FONG
'	Date	    :	26-02-2016
'	Purpose	    :	DETAILING EXTRA CAT MASTER MAINTENANCE
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsDetailing
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDetailing"
        End Get
    End Property
#Region "Drop down List"
    Public Function GetDetCodeDDL() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DET_CODE_DDL"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetSubDetCodeDDL(ByVal strDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SUB_DET_CODE_DDL"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region
#Region "Detailing Preview"
    Public Function GetCatPreview(ByVal strDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_PREVIEW"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetSubCatPreview(ByVal strDetCode As String, ByVal strSubDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SUB_DETAILING_PREVIEW"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region
#Region "Detailing Brand"
    Public Function GetDetailingBrandList(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_BRAND_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetDetailingBrandDetails(ByVal strDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_BRAND_DTL"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub UpdateDetailingBrand(ByVal strDetCode As String, ByVal strDetName As String, _
                                         ByVal strStartDate As String, ByVal strEndDate As String)

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_BRAND_UPDATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("DET_NAME", strDetName, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CreateDetailingBrand(ByVal strDetCode As String, ByVal strDetName As String, _
                                         ByVal strStartDate As String, ByVal strEndDate As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_BRAND_CREATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("DET_NAME", strDetName, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function DeleteDetailingBrand(ByVal strDetCode As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_BRAND_DELETE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region
#Region "Detailing Activity"
    Public Function GetDetailingActivityList(ByVal strDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_SUB_CAT_LIST"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetDetailingActivityDetails(ByVal strDetCode As String, ByVal strSubDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_SUB_CAT_DTL"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub UpdateDetailingActivity(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strSubDetName As String, _
                                                ByVal strStartDate As String, ByVal strEndDate As String)

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_SUB_CAT_UPDATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_NAME", strSubDetName, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CreateDetailingActivity(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strSubDetName As String, _
                                                ByVal strStartDate As String, ByVal strEndDate As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_SUB_CAT_CREATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_NAME", strSubDetName, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function DeleteDetailingActivity(ByVal strDetCode As String, ByVal strSubDetCode As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_SUB_CAT_DELETE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region
#Region "Detailing Extra Category"
    Public Function GetDetailingExtraCatList(ByVal strDetCode As String, ByVal strSubDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_EXTRA_CAT_LIST"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetDetailingExtraCatDetails(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strExtraDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_EXTRA_CAT_DTL"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("EXTRA_DET_CODE", strExtraDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub UpdateDetailingExtraCat(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strExtraDetCode As String, ByVal strExtraDetName As String, ByVal strPath As String)

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_EXTRA_CAT_UPDATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("EXTRA_DET_CODE", strExtraDetCode, clsDB.DataType.DBString)
                .addItem("EXTRA_DET_NAME", strExtraDetName, clsDB.DataType.DBString)
                .addItem("PATH", strPath, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CreateDetailingExtraCat(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strExtraDetCode As String, ByVal strExtraDetName As String, ByVal strPath As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_EXTRA_CAT_CREATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("EXTRA_DET_CODE", strExtraDetCode, clsDB.DataType.DBString)
                .addItem("EXTRA_DET_NAME", strExtraDetName, clsDB.DataType.DBString)
                .addItem("PATH", strPath, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function DeleteDetailingExtraCat(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strExtraDetCode As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_EXTRA_CAT_DELETE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("EXTRA_DET_CODE", strExtraDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region


#Region "Detailing Compliance"
    Public Function GetDetailingComplianceList(ByVal strDetCode As String, ByVal strSubDetCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_COMPLIANCE_LIST"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetDetailingComplianceDetails(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strComplianceCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_COMPLIANCE_DTL"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("COMPLIANCE_CODE", strComplianceCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub UpdateDetailingCompliance(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strComplianceCode As String, ByVal strComplianceName As String)

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_COMPLIANCE_UPDATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("COMPLIANCE_CODE", strComplianceCode, clsDB.DataType.DBString)
                .addItem("COMPLIANCE_NAME", strComplianceName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CreateDetailingCompliance(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strComplianceCode As String, ByVal strComplianceName As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_COMPLIANCE_CREATE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("COMPLIANCE_CODE", strComplianceCode, clsDB.DataType.DBString)
                .addItem("COMPLIANCE_NAME", strComplianceName, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function DeleteDetailingCompliance(ByVal strDetCode As String, ByVal strSubDetCode As String, ByVal strComplianceCode As String) As DataTable

        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_DETAILING_COMPLIANCE_DELETE"
                .addItem("DET_CODE", strDetCode, clsDB.DataType.DBString)
                .addItem("SUB_DET_CODE", strSubDetCode, clsDB.DataType.DBString)
                .addItem("COMPLIANCE_CODE", strComplianceCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
