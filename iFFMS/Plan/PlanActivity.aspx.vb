Imports System.Data

Partial Class SFMSActyList
    Inherits System.Web.UI.Page
    

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "PlanCustomerList"
        End Get
    End Property
    Dim strDate, strCustCode, strSalesmanCode As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Try

            'Call Header
            With wuc_lblheader
                .Title = "Pre Plan Activity"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.CUSTPREPLAN
                .DataBind()
                .Visible = True
            End With
            strSalesmanCode = Trim(Session("dflSalesRepName"))

            If Not IsPostBack Then
                strDate = Request.QueryString("date")
                ViewState("dateselected") = strDate
                strCustCode = Request.QueryString("custcode")
                ViewState("customer") = strCustCode
                optionArray()
                buildSession()
                bindGridView()
                buildFreeText()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub buildFreeText()
        Dim strCustCode, strDate As String
        Dim dt As DataTable
        Dim drcurr(), dr As DataRow
        Try
            dt = Session("PlanFreeText")
            strDate = CType(ViewState("dateselected"), String)
            strCustCode = CType(ViewState("customer"), String)
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
            If drcurr.Length = 0 Then
                dr = dt.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = ""
                dt.Rows.Add(dr)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".buildFreeText : " & ex.ToString)
        End Try
    End Sub

    Sub optionArray()
        Dim strPlan(7) As String
        Try
            strPlan(0) = "Contact Department"
            strPlan(1) = "Contact"
            strPlan(2) = "Product Group"
            strPlan(3) = "SFMS"
            strPlan(4) = "MSS"
            strPlan(5) = "Promotion Campaign"
            strPlan(6) = "Other Activities"
            strPlan(7) = "Free Text"
            lstActivity.DataSource = strPlan
            lstActivity.DataBind()
            'lstActivity.Items(0).Selected = True
            lstActivity.Items(2).Selected = True
            ViewState("PrevChoice") = lstActivity.SelectedIndex
        Catch ex As Exception
            ExceptionMsg(PageName & ".optionArray : " & ex.ToString)
        End Try
    End Sub

    Sub buildSession()
        Dim obj As New txn_Plan.clsPlan
        Try
            'Create 2 sessions to store selected and unselected cont dept
            If IsNothing(Session("PlanContDept")) = True And IsNothing(Session("PlanContDeptSelected")) = True Then
                obj.createSession("PlanContDept", 3)
                obj.createSession("PlanContDeptSelected", 3)
            End If

            If IsNothing(Session("PlanCont")) = True And IsNothing(Session("PlanContSelected")) = True Then
                obj.createSession("PlanCont", 4)
                obj.createSession("PlanContSelected", 4)
            End If

            If IsNothing(Session("PlanPrdGrp")) = True And IsNothing(Session("PlanPrdGrpSelected")) = True Then
                obj.createSession("PlanPrdGrp", 4)
                obj.createSession("PlanPrdGrpSelected", 4)
            End If

            If IsNothing(Session("PlanSFMS")) = True And IsNothing(Session("PlanSFMSSelected")) = True Then
                obj.createSession("PlanSFMS", 5)
                obj.createSession("PlanSFMSSelected", 5)
            End If

            If IsNothing(Session("PlanMSS")) = True And IsNothing(Session("PlanMSSSelected")) = True Then
                obj.createSession("PlanMSS", 4)
                obj.createSession("PlanMSSSelected", 4)
            End If

            If IsNothing(Session("PlanPromoCamp")) = True And IsNothing(Session("PlanPromoCampSelected")) = True Then
                obj.createSession("PlanPromoCamp", 5)
                obj.createSession("PlanPromoCampSelected", 5)
            End If

            If IsNothing(Session("PlanOther")) = True And IsNothing(Session("PlanOtherSelected")) = True Then
                obj.createSession("PlanOther", 4)
                obj.createSession("PlanOtherSelected", 4)
            End If

            If IsNothing(Session("PlanFreeText")) = True Then
                obj.createSession("PlanFreeText", 3)
            End If

            If IsNothing(Session("PlanCheckStatus")) = True Then
                obj.createSession("PlanCheckStatus", 4)
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".buildSession : " & ex.ToString)
        End Try
    End Sub

    Sub bindGridView()

        Try
            Select Case lstActivity.SelectedIndex
                Case 0
                    getSessionDetail("PlanContDept", "PlanContDeptSelected", 0)
                Case 1
                    getSessionDetail("PlanCont", "PlanContSelected", 1)
                Case 2
                    getSessionDetail("PlanPrdGrp", "PlanPrdGrpSelected", 2)
                Case 3
                    getSessionDetail("PlanSFMS", "PlanSFMSSelected", 3)
                Case 4
                    getSessionDetail("PlanMSS", "PlanMSSSelected", 4)
                Case 5
                    getSessionDetail("PlanPromoCamp", "PlanPromoCampSelected", 5)
                Case 6
                    getSessionDetail("PlanOther", "PlanOtherSelected", 6)
                Case 7

            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        End Try
    End Sub

    Sub getSessionDetail(ByVal strSession1 As String, ByVal strSession2 As String, ByVal intIndex As Integer)
        Dim drcurr1(), drcurr2() As DataRow
        Dim dt1, dt2, dt As DataTable
        Dim obj As New txn_Plan.clsPlan
        Dim strdate, strcustcode, strsalesmancode As String
        Try
            strcustcode = CType(ViewState("customer"), String)
            strdate = CType(ViewState("dateselected"), String)
            strsalesmancode = Trim(Session("dflsalesrepcode"))
            dt1 = Session(strSession1)
            dt2 = Session(strSession2)
            drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
            drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")

            If drcurr1.Length = 0 AndAlso drcurr2.Length = 0 Then
                Select Case intIndex
                    Case 0
                        dt = obj.DeptPlan(strSalesmanCode, strCustCode)
                        insertContDeptSession(dt, strCustCode, strDate)
                        createOneBoundField("Contact Department", "field3")
                    Case 1
                        dt = obj.contactPlan(strsalesmancode, strcustcode)
                        insertContactSession(dt, strcustcode, strdate)
                        createOneBoundField("Contact Person", "field4")
                    Case 2
                        dt = obj.productPlan()
                        insertPrdGrpSession(dt, strcustcode, strdate)
                        createOneBoundField("Product Group Desc", "field4")
                    Case 3
                        dt = obj.sfmsPlan((strSalesmanCode))
                        insertSFMSSession(dt, strCustCode, strDate)
                        createOneBoundField("SFMS Desc", "field5")
                    Case 4
                        dt = obj.mssPlan(strSalesmanCode)
                        insertMSSSession(dt, strCustCode, strDate)
                        createOneBoundField("MSS Title Desc", "field4")
                    Case 5
                        dt = obj.promoCampPlan(strcustcode)
                        insertPromoCampSession(dt, strCustCode, strDate)
                        createTwoBoundFields("Promotion Campaign Desc", "field4", "Agency Desc", "field5")
                    Case 6
                        dt = obj.otherActivityPlan(strSalesmanCode)
                        insertOtherSession(dt, strCustCode, strDate)
                        createOneBoundField("Activities Desc", "field4")
                End Select
            Else
                Select Case lstActivity.SelectedIndex
                    Case 0
                        createOneBoundField("Contact Department", "field3")
                    Case 1
                        createOneBoundField("Contact Person", "field4")
                    Case 2
                        createOneBoundField("Product Group Desc", "field4")
                    Case 3
                        createOneBoundField("SFMS Desc", "field5")
                    Case 4
                        createOneBoundField("MSS Title Desc", "field4")
                    Case 5
                        createTwoBoundFields("Promotion Campaign Desc", "field4", "Agency Desc", "field5")
                    Case 6
                        createOneBoundField("Activities Desc", "field4")
                End Select
            End If
            dt1 = Session(strSession1)
            dt2 = Session(strSession2)
            drcurr1 = dt1.Select("field1='" & strcustcode & "' AND field2='" & strdate & "'")
            drcurr2 = dt2.Select("field1='" & strcustcode & "' AND field2='" & strdate & "'")
            If drcurr1.Length > 0 Then
                Dim dv1 As New DataView(dt1)
                dv1.RowFilter = "field1='" & strcustcode & "' AND field2='" & strdate & "'"
                Select Case lstActivity.SelectedIndex
                    Case 3
                        dv1.Sort = "field4"
                    Case Else
                        dv1.Sort = "field3"
                End Select
                dgUnselected.DataSource = dv1
            Else
                Dim dtnew1 As DataTable
                dtnew1 = dt1.Copy
                dtnew1.Rows.Clear()
                dtnew1.Rows.Add(dtnew1.NewRow)
                dgUnselected.DataSource = dtnew1
            End If

            If drcurr2.Length > 0 Then
                Dim dv2 As New DataView(dt2)
                dv2.RowFilter = "field1='" & strcustcode & "' AND field2='" & strdate & "'"
                Select Case lstActivity.SelectedIndex
                    Case 3
                        dv2.Sort = "field4"
                    Case Else
                        dv2.Sort = "field3"
                End Select
                dgSelected.DataSource = dv2
            Else
                Dim dtnew2 As DataTable
                dtnew2 = dt2.Copy
                dtnew2.Rows.Clear()
                dtnew2.Rows.Add(dtnew2.NewRow)
                dgSelected.DataSource = dtnew2
            End If
            Select Case intIndex
                Case 0, 1, 2, 4, 5, 6
                    dgUnselected.DataKeyNames = New String() {"field1", "field2", "field3"}
                    dgSelected.DataKeyNames = New String() {"field1", "field2", "field3"}
                Case 3
                    dgSelected.DataKeyNames = New String() {"field1", "field2", "field4"}
                    dgUnselected.DataKeyNames = New String() {"field1", "field2", "field4"}
            End Select

            dgUnselected.DataBind()
            dgSelected.DataBind()
            If Not Page.IsPostBack Then
                If dgUnselected.Rows.Count > 0 Then
                    dgUnselected.SelectedIndex = 0
                End If

                If dgSelected.Rows.Count > 0 Then
                    dgSelected.SelectedIndex = 0
                End If
            End If

            If dt1.Rows.Count = 0 AndAlso dt2.Rows.Count = 0 Then
                changeVisibility(True, False, 2)
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".getSessionDetail : " & ex.ToString)
        End Try
    End Sub

    Sub insertContDeptSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanContDept")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("department")
                dtsession.Rows.Add(dr)
            Next
            Session("PlanContDept") = dtsession
        Catch ex As Exception
            ExceptionMsg(PageName & ".insertContDeptSession : " & ex.ToString)
        End Try
    End Sub

    Sub insertContactSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanCont")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("cont_acc_code")
                dr("field4") = dt.Rows(intloop)("contact_name")
                dtsession.Rows.Add(dr)
            Next
            Session("PlanCont") = dtsession
        Catch ex As Exception
            ExceptionMsg(PageName & ".insertContactSession : " & ex.ToString)
        End Try
    End Sub

    Sub insertPrdGrpSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanPrdGrp")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("prod_grp")
                dr("field4") = dt.Rows(intloop)("prod_desc")
                dtsession.Rows.Add(dr)
            Next

            Session("PlanPrdGrp") = dtsession

        Catch ex As Exception
            ExceptionMsg(PageName & ".insertPrdGrpSession : " & ex.ToString)
        End Try
    End Sub

    Sub insertSFMSSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanSFMS")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("cat_code")
                dr("field4") = dt.Rows(intloop)("sub_cat_code")
                dr("field5") = dt.Rows(intloop)("sfms_desc")
                dtsession.Rows.Add(dr)
            Next
            Session("PlanSFMS") = dtsession
        Catch ex As Exception
            ExceptionMsg(PageName & ".insertSFMSSession : " & ex.ToString)
        End Try
    End Sub

    Sub insertMSSSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanMSS")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("title_code")
                dr("field4") = dt.Rows(intloop)("title_desc")
                dtsession.Rows.Add(dr)
            Next
            Session("PlanMSS") = dtsession
        Catch ex As Exception
            ExceptionMsg(PageName & ".insertMSSSession : " & ex.ToString)
        End Try
    End Sub

    Sub insertPromoCampSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanPromoCamp")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("campaign_cd")
                dr("field4") = dt.Rows(intloop)("campaign_title")
                dr("field5") = dt.Rows(intloop)("agency_desc")
                dtsession.Rows.Add(dr)
            Next
            Session("PlanPromoCamp") = dtsession
        Catch ex As Exception
            ExceptionMsg(PageName & ".insertPromoCampSession : " & ex.ToString)
        End Try
    End Sub

    Sub insertOtherSession(ByVal dt As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtsession As DataTable
        Dim dr As DataRow
        Try
            dtsession = Session("PlanOther")
            For intloop As Integer = 0 To dt.Rows.Count - 1
                dr = dtsession.NewRow
                dr("field1") = strCustCode
                dr("field2") = strDate
                dr("field3") = dt.Rows(intloop)("act_cd")
                dr("field4") = dt.Rows(intloop)("act_desc")
                dtsession.Rows.Add(dr)
            Next
            Session("PlanOther") = dtsession
        Catch ex As Exception
            ExceptionMsg(PageName & ".insertOtherSession : " & ex.ToString)
        End Try
    End Sub

    Sub createOneBoundField(ByVal strHeader1 As String, ByVal strDataField1 As String)
        Dim dbg1 As New BoundField
        Try
            dbg1.HeaderText = strHeader1
            dbg1.DataField = strDataField1

            dgUnselected.Columns.Add(dbg1)
            dgSelected.Columns.Add(dbg1)
        Catch ex As Exception
            ExceptionMsg(PageName & ".createOneBoundField : " & ex.ToString)
        End Try
    End Sub

    Sub createTwoBoundFields(ByVal strHeader1 As String, ByVal strDataField1 As String, ByVal strHeader2 As String, ByVal strDataField2 As String)
        Dim dbg1 As New BoundField
        Dim dbg2 As New BoundField
        Try
            dbg1.HeaderText = strHeader1
            dbg1.DataField = strDataField1

            dbg2.HeaderText = strHeader2
            dbg2.DataField = strDataField2

            dgUnselected.Columns.Add(dbg1)
            dgSelected.Columns.Add(dbg1)
            dgUnselected.Columns.Add(dbg2)
            dgSelected.Columns.Add(dbg2)
        Catch ex As Exception
            ExceptionMsg(PageName & ".createTwoBoundFields : " & ex.ToString)
        End Try
    End Sub

    Sub clearGridviewColumn()
        Try
            dgUnselected.Columns.Clear()
            dgSelected.Columns.Clear()
        Catch ex As Exception
            ExceptionMsg(PageName & ".clearGridviewColumn : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Sub addClickEvent(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dgSelected.RowCreated

        Try
            If e.Row.RowIndex <> -1 Then

                Dim PostBack As String = Page.ClientScript.GetPostBackEventReference(dgSelected, "Select$" & e.Row.RowIndex)
                e.Row.Attributes.Add("OnClick", PostBack)
                e.Row.Attributes.Add("Style", "cursor:hand")

            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".addClickEvent : " & ex.ToString)
        End Try


    End Sub

    Sub addClickEvent1(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dgUnselected.RowCreated
        Try
            If e.Row.RowIndex <> -1 Then

                Dim PostBack As String = Page.ClientScript.GetPostBackEventReference(dgUnselected, "Select$" & e.Row.RowIndex)
                e.Row.Attributes.Add("OnClick", PostBack)
                e.Row.Attributes.Add("Style", "cursor:hand")

            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".addClickEvent1 : " & ex.ToString)
        End Try

    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Select Case lstActivity.SelectedIndex
                Case 0
                    findParameters(1, lstActivity.SelectedIndex, "PlanContDept", "PlanContDeptSelected")
                Case 1
                    findParameters(1, lstActivity.SelectedIndex, "PlanCont", "PlanContSelected")
                Case 2
                    findParameters(1, lstActivity.SelectedIndex, "PlanPrdGrp", "PlanPrdGrpSelected")
                Case 3
                    findParameters(1, lstActivity.SelectedIndex, "PlanSFMS", "PlanSFMSSelected")
                Case 4
                    findParameters(1, lstActivity.SelectedIndex, "PlanMSS", "PlanMSSSelected")
                Case 5
                    findParameters(1, lstActivity.SelectedIndex, "PlanPromoCamp", "PlanPromoCampSelected")
                Case 6
                    findParameters(1, lstActivity.SelectedIndex, "PlanOther", "PlanOtherSelected")
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnAdd_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Select Case lstActivity.SelectedIndex
                Case 0
                    findParameters(2, lstActivity.SelectedIndex, "PlanContDept", "PlanContDeptSelected")
                Case 1
                    findParameters(2, lstActivity.SelectedIndex, "PlanCont", "PlanContSelected")
                Case 2
                    findParameters(2, lstActivity.SelectedIndex, "PlanPrdGrp", "PlanPrdGrpSelected")
                Case 3
                    findParameters(2, lstActivity.SelectedIndex, "PlanSFMS", "PlanSFMSSelected")
                Case 4
                    findParameters(2, lstActivity.SelectedIndex, "PlanMSS", "PlanMSSSelected")
                Case 5
                    findParameters(2, lstActivity.SelectedIndex, "PlanMSS", "PlanMSSSelected")
                Case 6
                    findParameters(2, lstActivity.SelectedIndex, "PlanOther", "PlanOtherSelected")
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRemove_Click : " & ex.ToString)
        End Try
    End Sub

    Sub findParameters(ByVal intInd As Integer, ByVal intIndex As Integer, ByVal strSession1 As String, ByVal strSession2 As String)
        Dim dt1 As DataTable = Nothing
        Dim dt2 As DataTable = Nothing
        Dim dr, dr1 As DataRow
        Dim drCurr() As DataRow = Nothing
        Dim strkey1, strkey2, strkey3, strCustCode, strDate As String
        Try
            strCustCode = CType(ViewState("customer"), String)
            strDate = CType(ViewState("dateselected"), String)
            If intInd = 1 Then
                dt1 = Session(strSession1)
                dt2 = Session(strSession2)
                drCurr = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                If drCurr.Length > 0 Then
                    strkey1 = dgUnselected.SelectedDataKey.Values.Item(0)
                    strkey2 = dgUnselected.SelectedDataKey.Values.Item(1)
                    strkey3 = dgUnselected.SelectedDataKey.Values.Item(2)
                Else
                    Exit Sub
                End If
            ElseIf intInd = 2 Then
                dt1 = Session(strSession2)
                dt2 = Session(strSession1)
                drCurr = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                If drCurr.Length > 0 Then
                    strkey1 = dgSelected.SelectedDataKey.Values.Item(0)
                    strkey2 = dgSelected.SelectedDataKey.Values.Item(1)
                    strkey3 = dgSelected.SelectedDataKey.Values.Item(2)
                Else
                    Exit Sub
                End If

            End If

            Select Case intIndex
                Case 0
                    drCurr = dt1.Select("field1='" & strkey1 & "' AND field2='" & strkey2 & "' AND field3='" & strkey3 & "'")
                    For Each dr In drCurr
                        dr1 = dt2.NewRow
                        dr1("field1") = dr("field1")
                        dr1("field2") = dr("field2")
                        dr1("field3") = dr("field3")
                        dt2.Rows.Add(dr1)
                        dt1.Rows.Remove(dr)
                    Next
                Case 1, 2, 4, 6
                    drCurr = dt1.Select("field1='" & strkey1 & "' AND field2='" & strkey2 & "' AND field3='" & strkey3 & "'")
                    For Each dr In drCurr
                        dr1 = dt2.NewRow
                        dr1("field1") = dr("field1")
                        dr1("field2") = dr("field2")
                        dr1("field3") = dr("field3")
                        dr1("field4") = dr("field4")
                        dt2.Rows.Add(dr1)
                        dt1.Rows.Remove(dr)
                    Next
                Case 3, 5
                    If intIndex = 3 Then
                        drCurr = dt1.Select("field1='" & strkey1 & "' AND field2='" & strkey2 & "' AND field4='" & strkey3 & "'")
                    ElseIf intIndex = 5 Then
                        drCurr = dt1.Select("field1='" & strkey1 & "' AND field2='" & strkey2 & "' AND field3='" & strkey3 & "'")
                    End If
                    For Each dr In drCurr
                        dr1 = dt2.NewRow
                        dr1("field1") = dr("field1")
                        dr1("field2") = dr("field2")
                        dr1("field3") = dr("field3")
                        dr1("field4") = dr("field4")
                        dr1("field5") = dr("field5")
                        dt2.Rows.Add(dr1)
                        dt1.Rows.Remove(dr)
                    Next
            End Select

            If intInd = 1 Then
                Session(strSession1) = dt1
                Session(strSession2) = dt2
                databindGridview(dt1, dt2, strkey1, strkey2)
            ElseIf intInd = 2 Then
                Session(strSession1) = dt2
                Session(strSession2) = dt1
                databindGridview(dt2, dt1, strkey1, strkey2)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".findParameters : " & ex.ToString)
        End Try
    End Sub

    Sub databindGridview(ByVal dt1 As DataTable, ByVal dt2 As DataTable, ByVal strCustCode As String, ByVal strDate As String)
        Dim dtnew As DataTable
        Try
            If dt1.Rows.Count > 0 Then
                Dim dv1 As New DataView(dt1)
                dv1.RowFilter = "field1='" & strCustCode & "' AND field2='" & strDate & "'"
                Select Case lstActivity.SelectedIndex
                    Case 3
                        dv1.Sort = "field4"
                    Case Else
                        dv1.Sort = "field3"
                End Select
                dgUnselected.DataSource = dv1
                dgUnselected.DataBind()
                dgUnselected.Visible = True
            Else
                dtnew = dt1.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                dgUnselected.DataSource = dtnew
                dgUnselected.DataBind()
                dgUnselected.Visible = True
            End If

            If dt2.Rows.Count > 0 Then
                Dim dv2 As New DataView(dt2)
                dv2.RowFilter = "field1='" & strCustCode & "' AND field2='" & strDate & "'"
                Select Case lstActivity.SelectedIndex
                    Case 3
                        dv2.Sort = "field4"
                    Case Else
                        dv2.Sort = "field3"
                End Select
                dgSelected.DataSource = dv2
                dgSelected.DataBind()
                dgSelected.Visible = True
            Else
                dtnew = dt2.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                dgSelected.DataSource = dtnew
                dgSelected.DataBind()
                dgSelected.Visible = True
            End If

            If dgSelected.Rows.Count > 0 Then
                dgSelected.SelectedIndex = 0
            End If
            If dgUnselected.Rows.Count > 0 Then
                dgUnselected.SelectedIndex = 0
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".databindGridview : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dt1, dt2 As DataTable
        Dim strCustCode, strDate As String
        Dim drcurr1(), drcurr2() As DataRow
        Try
            strCustCode = CType(ViewState("customer"), String)
            strDate = CType(ViewState("dateselected"), String)
            Select Case lstActivity.SelectedIndex
                Case 7
                    dt1 = Session("planfreetext")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        For Each dr As DataRow In drcurr1
                            dr.BeginEdit()
                            dr("field3") = ""
                            dr.EndEdit()
                        Next
                    End If
                    txtFreeText.Text = ""
                Case 0
                    dt1 = Session("PlanContDept")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanContDeptSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
                Case 1
                    dt1 = Session("PlanCont")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanContSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
                Case 2
                    dt1 = Session("PlanPrdGrp")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanPrdGrpSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
                Case 3
                    dt1 = Session("PlanSFMS")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanSFMSSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
                Case 4
                    dt1 = Session("PlanMSS")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanMSSSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
                Case 5
                    dt1 = Session("PlanPromoCamp")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanPromoCampSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
                Case 6
                    dt1 = Session("PlanOther")
                    drcurr1 = dt1.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr1.Length > 0 Then
                        remove(dt1, drcurr1)
                    End If
                    dt2 = Session("PlanOtherSelected")
                    drcurr2 = dt2.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                    If drcurr2.Length > 0 Then
                        remove(dt2, drcurr2)
                    End If
            End Select
            clearGridviewColumn()
            buildSession()
            bindGridView()

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        End Try
    End Sub

    Sub remove(ByVal dt As DataTable, ByVal drcurr() As DataRow)
        Try
            For Each dr As DataRow In drcurr
                dt.Rows.Remove(dr)
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".remove : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim obj As New txn_Plan.clsPlan
        Dim dt As DataTable
        Dim strdate As String
        Dim drcurr() As DataRow
        Try

            strdate = CType(ViewState("dateselected"), String)
            strSalesmanCode = Trim(Session("dflSalesRepCode"))
            strCustCode = Trim(Session("customercode"))
            obj.deletePlan(strSalesmanCode, strCustCode, Date.Parse(strdate))
            If lstActivity.SelectedIndex = 7 AndAlso txtFreeText.Text.Length > 0 Then
                dt = Session("PlanFreeText")
                drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
                For Each dr As DataRow In drcurr
                    dr.BeginEdit()
                    dr("field3") = txtFreeText.Text
                    dr.EndEdit()
                Next
            End If
            dt = Session("PlanContDeptSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "1", dr("field3"))
                Next
            End If

            dt = Session("PlanContSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "2", dr("field3"))
                Next
            End If

            dt = Session("PlanPrdGrpSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "3", dr("field3"))
                Next
            End If

            dt = Session("PlanSFMSSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "4", dr("field3"), dr("field4"))
                Next
            End If

            dt = Session("PlanMSSSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "5", dr("field3"))
                Next
            End If

            dt = Session("PlanPromoCampSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "6", dr("field3"))
                Next
            End If

            dt = Session("PlanOtherSelected")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "'")
            If drcurr.Length > 0 Then
                For Each dr As DataRow In drcurr
                    obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(dr("field2")), "7", dr("field3"))
                Next
            End If

            dt = Session("PlanFreeText")
            drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strdate & "' AND field3<>''")
            If drcurr.Length > 0 Then
                obj.insertPlan(strSalesmanCode, strCustCode, Date.Parse(strdate), "8", drcurr(0).Item(2))
            End If
            Response.Redirect("../plan/plancustomerlist.aspx")
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnSave_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub lstActivity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstActivity.SelectedIndexChanged
        Dim strPrevChoice As String
        Dim dt As DataTable
        Dim dr, drcurr() As DataRow
        Dim strDate, strCustomer As String
        Try
            strDate = CType(ViewState("dateselected"), String)
            strCustomer = CType(ViewState("customer"), String)
            strPrevChoice = CType(ViewState("PrevChoice"), String)
            dt = Session("PlanFreeText")
            If strPrevChoice = 7 Then
                drcurr = dt.Select("field1='" & strCustomer & "' AND field2='" & strDate & "'")
                For Each dr In drcurr
                    dr.BeginEdit()
                    dr("field3") = txtFreeText.Text
                    dr.EndEdit()
                Next
            End If
            Select Case lstActivity.SelectedIndex
                Case 7
                    changeVisibility(False, True, 1)
                    drcurr = dt.Select("field1='" & strCustomer & "' AND field2='" & strDate & "'")
                    If Not IsDBNull(drcurr(0).Item(2)) AndAlso drcurr.Length > 0 Then
                        txtFreeText.Text = drcurr(0).Item(2)
                    End If
                Case Else
                    changeVisibility(True, False, 1)
                    clearGridviewColumn()
                    bindGridView()
            End Select
            ViewState("PrevChoice") = lstActivity.SelectedIndex
        Catch ex As Exception
            ExceptionMsg(PageName & ".lstActivity_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Sub changeVisibility(ByVal booAnswer1 As Boolean, ByVal booAnswer2 As Boolean, ByVal intInd As Integer)
        Try
            dgSelected.Visible = booAnswer1
            dgUnselected.Visible = booAnswer1
            If intInd = 1 Then
                btnAdd.Visible = booAnswer1
                btnRemove.Visible = booAnswer1
                btnRemoveAll.Visible = booAnswer1
                btnAddAll.Visible = booAnswer1
            ElseIf intInd = 2 Then
                btnAdd.Visible = booAnswer2
                btnRemove.Visible = booAnswer2
                btnRemoveAll.Visible = booAnswer2
                btnAddAll.Visible = booAnswer2
            End If
            txtFreeText.Visible = booAnswer2
            lblTitle.Visible = booAnswer2
        Catch ex As Exception
            ExceptionMsg(PageName & ".changeVisibility : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strCustCode, strDate As String
        Dim dt As DataTable
        Dim dr, drcurr() As DataRow
        Try
            strDate = CType(ViewState("dateselected"), String)
            strCustCode = CType(ViewState("customer"), String)
            dt = Session("PlanFreeText")
            If lstActivity.SelectedIndex = 7 Then
                drcurr = dt.Select("field1='" & strCustCode & "' AND field2='" & strDate & "'")
                For Each dr In drcurr
                    dr.BeginEdit()
                    dr("field3") = txtFreeText.Text
                    dr.EndEdit()
                Next
            End If
            Response.Redirect("../plan/plancustomerlist.aspx", False)
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnOK_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Select Case lstActivity.SelectedIndex
                Case 0
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanContDept", "PlanContDeptSelected")
                Case 1
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanCont", "PlanContSelected")
                Case 2
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanPrdGrp", "PlanPrdGrpSelected")
                Case 3
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanSFMS", "PlanSFMSSelected")
                Case 4
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanMSS", "PlanMSSSelected")
                Case 5
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanPromoCamp", "PlanPromoCampSelected")
                Case 6
                    findAllParameters(1, lstActivity.SelectedIndex, "PlanOther", "PlanOtherSelected")
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnAddAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Select Case lstActivity.SelectedIndex
                Case 0
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanContDept", "PlanContDeptSelected")
                Case 1
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanCont", "PlanContSelected")
                Case 2
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanPrdGrp", "PlanPrdGrpSelected")
                Case 3
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanSFMS", "PlanSFMSSelected")
                Case 4
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanMSS", "PlanMSSSelected")
                Case 5
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanPromoCamp", "PlanPromoCampSelected")
                Case 6
                    findAllParameters(2, lstActivity.SelectedIndex, "PlanOther", "PlanOtherSelected")
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRemoveAll_Click : " & ex.ToString)
        End Try
    End Sub

    Sub findAllParameters(ByVal intInd As Integer, ByVal intIndex As Integer, ByVal strSession1 As String, ByVal strSession2 As String)
        Dim dt1, dt2 As DataTable
        Dim dr1 As DataRow
        Dim strCustCode, strDate As String
        Try
            strCustCode = CType(ViewState("customer"), String)
            strDate = CType(ViewState("dateselected"), String)
            If intInd = 1 Then
                dt1 = Session(strSession1)
                dt2 = Session(strSession2)
            ElseIf intInd = 2 Then
                dt2 = Session(strSession1)
                dt1 = Session(strSession2)
            End If

            Select Case intIndex
                Case 0
                    For Each dr As DataRow In dt1.Rows
                        dr1 = dt2.NewRow
                        dr1("field1") = dr("field1")
                        dr1("field2") = dr("field2")
                        dr1("field3") = dr("field3")
                        dt2.Rows.Add(dr1)

                    Next
                Case 1, 2, 4, 6
                    For Each dr As DataRow In dt1.Rows
                        dr1 = dt2.NewRow
                        dr1("field1") = dr("field1")
                        dr1("field2") = dr("field2")
                        dr1("field3") = dr("field3")
                        dr1("field4") = dr("field4")
                        dt2.Rows.Add(dr1)

                    Next
                Case 3, 5
                    For Each dr As DataRow In dt1.Rows
                        dr1 = dt2.NewRow
                        dr1("field1") = dr("field1")
                        dr1("field2") = dr("field2")
                        dr1("field3") = dr("field3")
                        dr1("field4") = dr("field4")
                        dr1("field5") = dr("field5")
                        dt2.Rows.Add(dr1)

                    Next

                    'dt1.Dispose()
            End Select
            dt1.Rows.Clear()
            If intInd = 1 Then
                databindGridview(dt1, dt2, strCustCode, strDate)
            ElseIf intInd = 2 Then
                databindGridview(dt2, dt1, strCustCode, strDate)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".findAllParameters : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgSelected_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgSelected.RowDataBound
        e.Row.Height = Unit.Pixel(5)
    End Sub

    Protected Sub dgUnselected_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgUnselected.RowDataBound
        e.Row.Height = Unit.Pixel(5)
    End Sub

End Class



