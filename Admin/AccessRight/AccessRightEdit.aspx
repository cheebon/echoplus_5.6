<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AccessRightEdit.aspx.vb" Inherits="Admin_AccessRight_NewAccessRight_AccessRightEdit" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register Src="../../include/wuc_ddlCountry.ascx" TagName="wuc_ddlCountry" TagPrefix="uc2" %>
<%@ Register Src="../../include/wuc_ddlPrincipal.ascx" TagName="wuc_ddlPrincipal" TagPrefix="uc5" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Editing Access Right</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script language="javascript" type="text/javascript">
    
        function ViewControlAll(state)
        {
            if (ViewCheckBoxIDs != null)
            {             
                for (var i = 1; i < ViewCheckBoxIDs.length; i++)
                {
                    var cb = document.getElementById(ViewCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }
        function CreateControlAll(state)
        {
            if (CreateCheckBoxIDs != null)
            {
                for (var i = 1; i < CreateCheckBoxIDs.length; i++)
                {
                    var cb = document.getElementById(CreateCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }
        function EditControlAll(state)
        {
            if (EditCheckBoxIDs != null)
            {
                for (var i = 1; i < EditCheckBoxIDs.length; i++)
                {
                    var cb = document.getElementById(EditCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }
        function DeleteControlAll(state)
        {
            if (DeleteCheckBoxIDs != null)
            {
                for (var i = 1; i < DeleteCheckBoxIDs.length; i++)
                {
                    var cb = document.getElementById(DeleteCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }
        function ValidateViewHeader(state)
        {
        
            if (ViewCheckBoxIDs != null)
            {             
                if(state)
                {
                    for (var i = 1; i < ViewCheckBoxIDs.length; i++)
                    {
                   
                        var cb = document.getElementById(ViewCheckBoxIDs[i]);
                        if(!cb.checked)
                        {
                        
                            document.getElementById(ViewCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else
                {
                    document.getElementById(ViewCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(ViewCheckBoxIDs[0]).checked = true;
            }
        }
        function ValidateCreateHeader(state)
        {
            if (CreateCheckBoxIDs != null)
            {             
                if(state)
                {
                    for (var i = 1; i < CreateCheckBoxIDs.length; i++)
                    {
                        var cb = document.getElementById(CreateCheckBoxIDs[i]);
                        if(!cb.checked)
                        {
                            document.getElementById(CreateCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else
                {
                    document.getElementById(CreateCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(CreateCheckBoxIDs[0]).checked = true;
            }
        }
        function ValidateEditHeader(state)
        {
            if (EditCheckBoxIDs != null)
            {             
                if(state)
                {
                    for (var i = 1; i < EditCheckBoxIDs.length; i++)
                    {
                        var cb = document.getElementById(EditCheckBoxIDs[i]);
                        if(!cb.checked)
                        {
                            document.getElementById(EditCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else
                {
                    document.getElementById(EditCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(EditCheckBoxIDs[0]).checked = true;
            }
        }
        function ValidateDeleteHeader(state)
        {
            if (DeleteCheckBoxIDs != null)
            {             
                if(state)
                {
                    for (var i = 1; i < DeleteCheckBoxIDs.length; i++)
                    {
                        var cb = document.getElementById(DeleteCheckBoxIDs[i]);
                        if(!cb.checked)
                        {
                            document.getElementById(DeleteCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else
                {
                    document.getElementById(DeleteCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(DeleteCheckBoxIDs[0]).checked = true;
            }
        }
    </script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmAccessRightEdit" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300">
        </asp:ScriptManager>
        <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="UpdPanelMod" runat="server">
        <ContentTemplate>
            <table id="tbl1" cellspacing="0" cellpadding="0" width="98%" border="0">
                <tr><td>&nbsp;</td></tr>
		        <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
                <tr>			    
                    <td class="BckgroundInsideContentLayout">
                    <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                    </td>
                </tr>
                <tr><td class="BckgroundBenealthTitle" height="5"></td></tr>
                <tr class="Bckgroundreport"><td>
                    &nbsp;&nbsp;<asp:Button ID="btnEdit" runat="server" Text="Edit Accessright details" OnClick="btnEdit_Click" CssClass="cls_button" />&nbsp;<asp:Button
                        ID="btnBack" runat="server" Text="Back" OnClientClick="history.go(-1)" CssClass="cls_button" Visible="False" />
                    <asp:Button ID="btnDone" runat="server" Text="Done" CssClass="cls_button" Visible="False" OnClick="btnDone_Click" />        
                        <br /><br /></td></tr>
                    
                <tr class="Bckgroundreport">
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
                            <tr>
                                <td vAlign="top" width="20%">
                                    
                                    <ccGV:clsGridView ID="dgModuleList" runat="server" ShowFooter="false" AllowSorting="True"
                                        AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="139px"
                                        AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                        FreezeRows="0" GridWidth="" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="module_id" OnRowCommand="dgModuleList_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="module_id" HeaderText="Module ID" ReadOnly="True" SortExpression="module_id" visible="false">
                                                <itemstyle horizontalalign="Center" />
                                            </asp:BoundField>           
				                            <asp:ButtonField DataTextField="module_name" HeaderText="Module" CommandName="Select" AccessibleHeaderText="Select" ButtonType="Link" />                                                                                                                                                      
                                        </Columns>                                                
                                    </ccGV:clsGridView>
                                    
                                    <!-- Reference part -->
                                    <asp:Panel ID="pnlRef" runat="server">
                                    
                                    <div class="border_std" style="width:90%; padding-left:10px;padding-top:10px; padding-bottom:10px; border:solid 1px black; margin-left:5px">
                                    <asp:Label ID="lblRef" CssClass="cls_label" runat="server" Text="Pick any accessright as a reference:"></asp:Label><br /><br />
                                    <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label1" CssClass="cls_label" runat="server" Text="Select Accessright : "></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlAccessright" CssClass="cls_dropdownlist" runat="server" AutoPostBack="true" width="150px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>                                   
                                    </table>
                                    <asp:Button ID="btnRef" runat="server" Text="Reference" CssClass="cls_button" />
                                    </div>
                                    
                                    </asp:Panel>
                                     <!-- Reference part end -->
                                </td>
                                <td vAlign="top" width="50%">
                                <asp:Label ID="lblMesg" runat="server" CssClass="cls_label">Please select a module from left to edit.</asp:Label>
                                <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" Visible="False" /><br /><br />
                                     <ccGV:clsGridView ID="dgSubModuleList" runat="server" ShowFooter="false" AllowSorting="True"
                                        AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                        AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                        FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="SUBMODULE_ID" Visible="False" OnRowDataBound="dgSubModuleList_RowDataBound" OnDataBound="dgSubModuleList_DataBound">
                                        <Columns>                                             
                                            <asp:BoundField DataField="SUBMODULE_ID" HeaderText="Sub Module ID" ReadOnly="True" SortExpression="SUBMODULE_ID" visible="False">
                                                <itemstyle horizontalalign="Center" />
                                            </asp:BoundField>                                                                   
					                        <asp:BoundField DataField="SUBMODULE_NAME" HeaderText="Sub Module Name" ReadOnly="True">
					                            <itemstyle horizontalalign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField headertext = "View">
                                                <headertemplate>
                                                  <asp:checkbox id="ViewAllCheckBox"
                                                    CssClass="cls_checkbox"
                                                    runat="server"/> View
                                                </headertemplate>
                                                <headerstyle horizontalalign="Left" />
                                                <ItemTemplate>                                               
                                                     <asp:CheckBox runat="server"  CssClass="cls_checkbox" ID="ViewCheckBox" checked="False"/>                                      
                                                </ItemTemplate>
                                            </asp:TemplateField> 
					                        <asp:TemplateField headertext = "Create">
					                            <headertemplate>
                                                  <asp:checkbox id="CreateAllCheckBox"
                                                    CssClass="cls_checkbox"
                                                    runat="server"/> Create
                                                </headertemplate>
                                                <headerstyle horizontalalign="Left" />
                                                <ItemTemplate>
                                                     <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="CreateCheckBox"  checked="False"/>                                                
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:TemplateField headertext = "Edit">
                                                <headertemplate>
                                                  <asp:checkbox id="EditAllCheckBox"
                                                    CssClass="cls_checkbox"
                                                    runat="server"/> Edit
                                                </headertemplate>
                                                <headerstyle horizontalalign="Left" />
                                                <ItemTemplate>
                                                     <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="EditCheckBox" checked="False" />                                                
                                                </ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:TemplateField headertext = "Delete">
                                                <headertemplate>
                                                  <asp:checkbox id="DeleteAllCheckBox"
                                                    CssClass="cls_checkbox"
                                                    runat="server"/> Delete
                                                </headertemplate>
                                                <headerstyle horizontalalign="Left" />
                                                <ItemTemplate>                                                
                                                     <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="DeleteCheckBox" checked="False" />                                                
                                                </ItemTemplate>
                             
                                            </asp:TemplateField> 
                                        </Columns>                                                
                                         <FooterStyle CssClass="GridFooter" />
                                         <HeaderStyle CssClass="GridHeader" />
                                         <AlternatingRowStyle CssClass="GridAlternate" />
                                         <RowStyle CssClass="GridNormal" />
                                         <PagerSettings Visible="False" />
                                    </ccGV:clsGridView>	</td>		
                                    
                            </tr>
                            <tr><td> </td></tr>
                            <tr>
                                <td></td>
                                <td align="right"><br /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                 
            </table>            
        </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:Button id="btnShowPopup" runat="server" style="display:none" />
            <ajaxToolKit:ModalPopupExtender 
                ID="mdlARPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pnlEditARPopup" 
                CancelControlID="btnClose" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="pnlEditARPopup" runat="server" Width="500px" style="display:none" CssClass="modalPopup">
                <asp:UpdatePanel ID="updPnlARDetail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
                            <tr>
                            <td colspan="3"><asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="Edit Accessright Details" />
            </asp:Panel></td>
                            </tr>
                            <tr>
		                        <td width="20%"><asp:label id="lblName" runat="server" CssClass="cls_label_header">Name</asp:label><asp:label id="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:label></td>
		                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
		                        <td><asp:TextBox ID="txtName" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox>
			                        <asp:RequiredFieldValidator id="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name cannot be Blank !"
				                        Display="Dynamic" CssClass="cls_validator" ValidationGroup="ARDetails"></asp:RequiredFieldValidator>
                                    <asp:Label ID="lblDupError" runat="server" Text="Name already exists!" Visible="false" CssClass="cls_validator"></asp:Label>
			                    </td>
	                        </tr>
	                         <tr>
		                        <td width="20%"><asp:label id="lblName1" runat="server" CssClass="cls_label_header">Name 1</asp:label></td>
		                        <td width="2%"><asp:label id="lblDot2" runat="server"  CssClass="cls_label_header">:</asp:label></td>
		                        <td><asp:TextBox ID="txtName1" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
	                        </tr>
	                         <tr>
		                        <td width="20%"><asp:label id="lblDesc" runat="server" CssClass="cls_label_header">Description</asp:label></td>
		                        <td width="2%"><asp:label id="lblDot3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
		                        <td><asp:TextBox ID="txtDesc" runat="server" CssClass="cls_textbox" MaxLength="100" Width="300"></asp:TextBox></td>
	                        </tr>
	                         <tr>
		                        <td width="20%"><asp:label id="lblDesc1" runat="server" CssClass="cls_label_header">Description 1</asp:label></td>
		                        <td width="2%"><asp:label id="lblDot4" runat="server"  CssClass="cls_label_header">:</asp:label></td>
		                        <td><asp:TextBox ID="txtDesc1" runat="server" CssClass="cls_textbox" MaxLength="100" Width="300"></asp:TextBox></td>
	                        </tr>
	                        <tr>
		                        <td width="20%"><asp:label id="lblCountry" runat="server" CssClass="cls_label_header">Country</asp:label><asp:label id="lblMark8" runat="server" CssClass="cls_mandatory">*</asp:label></td>
		                        <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
		                        <td>
                                    <asp:Label ID="lblCounName" runat="server" Text="" CssClass="cls_label"></asp:Label>
		                        </td>
		                    </tr>
		                    <tr>
	                            <td width="20%"><asp:label id="lblPrincipal" runat="server" CssClass="cls_label_header">Principal</asp:label><asp:label id="lblMark9" runat="server" CssClass="cls_mandatory">*</asp:label></td>
		                        <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
		                        <td>
                                    <asp:Label ID="lblPrinName" runat="server" Text="" CssClass="cls_label"></asp:Label>
		                        </td>
	                        </tr>
	                        <tr>
	                            <td colspan="3"><asp:Button 
                        ID="btnSaveARDetails" runat="server" Text="Save"                          
                        Width="50px" OnClick="btnSaveARDetails_Click" ValidationGroup="ARDetails" CssClass="cls_button" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="cls_button" OnClick="btnClose_Click" /></td>
	                        </tr>	
	                    </table>
                    
                    </ContentTemplate>                
                </asp:UpdatePanel>       
            </asp:Panel>

    </div>
    </form>
</body>
</html>
