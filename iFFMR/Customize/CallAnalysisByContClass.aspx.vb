Imports System.Data

Partial Class iFFMR_Customize_CallAnalysisByContClass
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim licItemFigureCollector As ListItemCollection

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CallAnalysisByContClass"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

#End Region
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "Call Analysis List By Contact Class"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.CALLANALYBYCONTCLASS) '"Call Analysis List By Customer"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.CALLANALYBYCONTCLASS
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""

            If Not IsPostBack Then

                Dim gotpreviouspage As String
                gotpreviouspage = IIf(Request.QueryString("PAGE_INDICATOR") Is Nothing, Request.QueryString("PAGECALL"), Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(gotpreviouspage) Then
                    TimerControl1.Enabled = True

                Else
                    TimerControl1.Enabled = True
                    CriteriaCollector = Nothing
                End If


            End If

            If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then btnBack.PostBackUrl = ViewState("POSTBACK_URL")

        Catch ex As Exception

        End Try
    End Sub


#Region "EVENT HANDLER"

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region


#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            dtCurrentTable = GetRecList()
            'ViewState("dtCurrentView") = dtCurrentTable
            ViewState("strSortExpression") = Nothing

            'If dtCurrentTable Is Nothing Then

            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Else
                PreRenderMode(dtCurrentTable)
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallAnalysisByContClass.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CallAnalysisByContClass.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CallAnalysisByContClass.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallAnalysisByContClass.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallAnalysisByContClass.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        'dgColumn.DataTextFormatString = "{0}" ' & Eval(ColumnName)
                        dgColumn.SortExpression = ColumnName


                        Dim strUrlFields(2) As String
                        strUrlFields(0) = "SALESREP_CODE"
                        strUrlFields(1) = "CUST_CODE"
                        strUrlFields(2) = "CONT_CODE"
                        'FORM WHEN ROW CREATED, HERE IS NOT USED
                        Dim strUrlFormatString As String = "~\iFFMR\SFE\CallAnalysis\CallAnalysisListByCustomer.aspx?SALESREP_CODE={0}&CUST_CODE={1}&CONT_CODE={2}&PAGE_INDICATOR=CALLANALYBYCONTCLASS"
                        strUrlFormatString = strUrlFormatString & "&YEAR=" & Session("Year") & "&MONTH=" & Session("Month")
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CallAnalysisByContClass.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            'dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallAnalysisByContClass.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallAnalysisByContClass.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            'Dim strYear, strMonth, strUserID, strPrincipalCode, strPrincipalID, strTeamCode, strSalesrepCode, _
            'strAgencyCode, strCustCode, strNSMCode, strDSMCode As String

            'strYear = CInt(Session("Year"))
            'strMonth = CInt(Session("Month"))
            'strUserID = Session.Item("UserID")
            'strPrincipalID = session("PRINCIPAL_ID")
            'strPrincipalCode = session("PRINCIPAL_CODE")

            'strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
            'strAgencyCode = IIf(Trim(Request.QueryString("AGENCY_CODE")) Is Nothing Or Trim(Request.QueryString("AGENCY_CODE")) = "", _
            '"DKSH", Trim(Request.QueryString("AGENCY_CODE")))
            'strCustCode = Trim(Request.QueryString("CUST_CODE"))
            'strNSMCode = Trim(Request.QueryString("NSM_CODE"))
            'strDSMCode = Trim(Request.QueryString("DSM_CODE"))
            'strTeamCode = ""
            Dim gotpreviouspage As String
            gotpreviouspage = IIf(Request.QueryString("PAGE_INDICATOR") Is Nothing, Request.QueryString("PAGECALL"), Request.QueryString("PAGE_INDICATOR"))
            If String.IsNullOrEmpty(gotpreviouspage) Then
                With CriteriaCollector
                    .Year = CInt(Session("Year"))
                    .Month = CInt(Session("Month"))
                    .PrincipalID = Session("PRINCIPAL_ID")
                    .PrincipalCode = Session("PRINCIPAL_CODE")
                    If Page.IsPostBack Then
                        .SalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
                        .TeamCode = Trim(Request.QueryString("NSM_CODE"))
                        .RegionCode = Trim(Request.QueryString("DSM_CODE"))
                        .AgencyCode = IIf(Trim(Request.QueryString("AGENCY_CODE")) Is Nothing Or Trim(Request.QueryString("AGENCY_CODE")) = "", _
                        "DKSH", Trim(Request.QueryString("AGENCY_CODE")))
                        .CustCode = Trim(Request.QueryString("CUST_CODE"))
                    End If
                End With
            End If
            'Stored Criteria into Static Value Collector


            Dim clsCallAnalyEnquiryList As New rpt_Customize.clsCallAnalyEnquiryList

            With CriteriaCollector
                DT = clsCallAnalyEnquiryList.GetCallAnalyByCont(Session.Item("UserID"), .PrincipalID, .PrincipalCode, .Year, .Month, .AgencyCode, "", _
                .SalesrepCode, .TeamCode, .RegionCode, .CustCode)
            End With

            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except ACH_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If (strColumnName Like "TTL_COVERAGE" OrElse _
                 strColumnName Like "TTL_*") AndAlso Not strColumnName Like "ACH_*" Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer
                    Dim iIndex As Integer
                    If licItemFigureCollector IsNot Nothing Then
                        For Each li As ListItem In licItemFigureCollector
                            iIndex = aryDataItem.IndexOf(li.Text)
                            If iIndex >= 0 Then
                                e.Row.Cells(iIndex).Text = String.Format(CF_CallAnalysisByContClass.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                            End If
                        Next
                    End If


                    'For All "ACH_"
                    'e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_A")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_A"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_A"), licItemFigureCollector.FindByText("CONT_A")) * 100))
                    'e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_B")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_B"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_B"), licItemFigureCollector.FindByText("CONT_B")) * 100))
                    'e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_C")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_C"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_C"), licItemFigureCollector.FindByText("CONT_C")) * 100))
                    'e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_OTH")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_OTH"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_OTH"), licItemFigureCollector.FindByText("CONT_OTH")) * 100))
                    'e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_TTL")).Text = String.Format(CF_CallAnalysisEnquiry.GetOutputFormatString("ACH_COVERAGE_TTL"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_TTL"), licItemFigureCollector.FindByText("CONT_TTL")) * 100))

     
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub


    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function
#End Region


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

End Class

Public Class CF_CallAnalysisByContClass
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "DEPT_NAME"
                strFieldName = "Department"
            Case "TTL_COVERAGE"
                strFieldName = "Coverage"
            Case "TTL_ACTUAL_CALL"
                strFieldName = "Actual Call"
            Case "TTL_HIT_CALL"
                strFieldName = "Hit Call"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf (strColumnName Like "TTL_ACTUAL_CALL") Then
                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CABYCUST, "'1','8'") Then
                    FCT = FieldColumntype.HyperlinkColumn
                End If
            Else
                FCT = FieldColumntype.BoundColumn

            End If

            Select Case strColumnName.ToUpper
                Case "REASON_CODE", "VISIT_ID", "DATE"
                    FCT = FieldColumntype.InvisibleColumn
                Case "REASON_NAME"
                    FCT = FieldColumntype.BoundColumn
                
            End Select



            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            If strColumnName.ToUpper = "DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName.ToUpper Like "TTL_*" Then
                strFormatString = "{0:0.#}"
            Else
                strFormatString = ""
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
