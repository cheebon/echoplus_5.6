<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_txtCalendarRange.ascx.vb" Inherits="include_wuc_txtCalendarRange" %>
<table cellspacing="0" cellpadding="0" border="0">
    <tr valign="top" align="left">
        <td valign="top" align="left" style="white-space: nowrap">
            <span class="cls_label_header">&nbsp;</span>
        </td>
        <td style="white-space: nowrap; width: 180">
            <div style="vertical-align: text-top; float: left;">
                <asp:TextBox ID="txtDateStart" runat="server" CssClass="cls_textbox"></asp:TextBox>
                <asp:Image ID="imgDateStart" runat="server" CssClass="clsButton" ImageUrl="~/images/icoCalendar.gif"
                    AlternateText="Click to show calendar" />
                <asp:CustomValidator ID="rfvFormatDateStart" runat="server" Display="Dynamic" ControlToValidate="txtDateStart"
                    CssClass="cls_validator" ValidateEmptyText="false" />
                <asp:RequiredFieldValidator ID="rfvDateStart" runat="server" Display="Dynamic" ControlToValidate="txtDateStart"
                    ErrorMessage="<br />Date cannot be blank !" CssClass="cls_validator" />
                <asp:CompareValidator ID="rfvCheckDateStartDataType" runat="server" CssClass="cls_validator"
                    ControlToValidate="txtDateStart" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                    ErrorMessage="<BR/>Invalid Date!" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDateStart"
                    PopupButtonID="imgDateStart" Format="yyyy-MM-dd" CssClass="CalendarExtender">
                </ajaxToolkit:CalendarExtender>
            </div>
        </td>
        <td style="white-space: nowrap; width: 10; padding-left: 20px; padding-right: 20px">
            <asp:Label ID="Label21" CssClass="cls_label_header" runat="server">To </asp:Label>
        </td>
        <td style="white-space: nowrap; width: 180">
            <div style="vertical-align: text-top; float: left;">
                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="cls_textbox"></asp:TextBox>
                <asp:Image ID="imgDateEnd" runat="server" CssClass="clsButton" ImageUrl="~/images/icoCalendar.gif"
                    AlternateText="Click to show calendar" />
                <asp:CustomValidator ID="rfvFormatDateEnd" runat="server" Display="Dynamic" ControlToValidate="txtDateEnd"
                    CssClass="cls_validator" ValidateEmptyText="false" />
                <asp:RequiredFieldValidator ID="rfvDateEnd" runat="server" Display="Dynamic" ControlToValidate="txtDateEnd"
                    ErrorMessage="<br />Date cannot be blank !" CssClass="cls_validator" />
                <asp:CompareValidator ID="rfvCheckDateEndDataType" runat="server" CssClass="cls_validator"
                    ControlToValidate="txtDateEnd" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                    ErrorMessage="<BR/>Invalid Date!" />
                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDateEnd"
                    PopupButtonID="imgDateEnd" Format="yyyy-MM-dd" CssClass="CalendarExtender">
                </ajaxToolkit:CalendarExtender>
            </div>
            <asp:CompareValidator ID="CV_DateRangeCompare" runat="server" Display="Dynamic" ErrorMessage="Invalid date range!"
                ControlToCompare="txtDateStart" ControlToValidate="txtDateEnd" Operator="GreaterThanEqual"
                Type="Date" CssClass="cls_validator" />
        </td>
    </tr>
</table>
