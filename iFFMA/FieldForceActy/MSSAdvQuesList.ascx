<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MSSAdvQuesList.ascx.vb" Inherits="iFFMA_FieldForceActy_MSSAdvQuesList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesPop" Src="MSSAdvQuesPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesSeqPop" Src="MSSAdvQuesSeqPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesPreviewPop" Src="MSSAdvQuesPreviewPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesPreview2Pop" Src="MSSAdvQuesPreview2Pop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlQues" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <span style="float:left; width:12%; " class="cls_label_header">Team </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblTeamCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <span style="float:left; width:12%; " class="cls_label_header">Title </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblTitleCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                        <asp:Button ID="btnArrangeSeq" CssClass="cls_button" runat="server" Text="Questions Sequence" />
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <customToolkit:wuc_dgpaging ID="wuc_dgQuesPaging" runat="server" />
                        <ccGV:clsGridView ID="dgQuesList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                        </ccGV:clsGridView>
                    </center>
                </td>
            </tr>
        </table>
        
        <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
        <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
        
        <customToolkit:wuc_MSSAdvQuesPop ID="wuc_MSSAdvQuesPop" Title="MSS Question Maintenance" runat="server" />
        <customToolkit:wuc_MSSAdvQuesSeqPop ID="wuc_MSSAdvQuesSeqPop" Title="Arrange Questions' Sequence" runat="server" />
        <customToolkit:wuc_MSSAdvQuesPreviewPop ID="wuc_MSSAdvQuesPreviewPop" Title="MSS Question Preview" runat="server" />
        <customToolkit:wuc_MSSAdvQuesPreview2Pop ID="wuc_MSSAdvQuesPreview2Pop" Title="MSS Question Preview" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
