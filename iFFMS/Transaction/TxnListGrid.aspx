<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnListGrid.aspx.vb" Inherits="iFFMS_Transaction_TxnListGrid" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transaction List Grid</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dgList');
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}}
       
         function ValidateCheckBoxStates()
   { var dglist = document.getElementById('dgList');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
       
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('DetailBar');HideElement('SubDetail');HideElement('SubDetailConfBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe');"
    class="BckgroundInsideContentLayout">
    <form id="frmtxnlistgrid" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%">
            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                    <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" Visible="false" ></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </div>
                    <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                        width="100%" border="0" style="height: 30px">
                        <tr align="left" valign="bottom">
                            <td>
                                <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                                    CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
                        <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                                        <tr align="left">
                                            <td style="width: 8%">
                                                <span class="cls_label_header">Field Force Name</span>
                                            <td>
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td align="left" style="width: 10%">
                                                <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                            <td style="width: 5%">
                                                <span class="cls_label_header">Map</span>
                                            </td>
                                            <td>
                                                <span class="cls_label_header">:</span></td>
                                            <td align="left" colspan="4">
                                                <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" /></td>
                                            <td align="right">
                                              <%--  <asp:Image ID="imgGeneralInfo3" runat="server" ImageUrl="~/images/ico_close.gif" CssClass="cls_image_Close"
                                                     EnableViewState="false" />--%></td>
                                        </tr>
                                   
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                            ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                            Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
                        </ajaxToolkit:CollapsiblePanelExtender>
                    </asp:Panel>
                    <br />
                    <div>
                        <%--<asp:Button ID="btnadddetail" runat="server" Text="Add Detail" CssClass="cls_button" Width="80px"
                                                    ValidationGroup="SFMSDtl" />--%>
                        <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                            validationgroup="SFMSSum" style="width: 80px" runat="server" visible="false" />
                        <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                            validationgroup="SFMSSum" style="width: 80px" runat="server" visible="false" />
                        <asp:Button ID="btncancel" runat="server" Text="Cancel Txn." CssClass="cls_button"
                            Width="80px" Visible="false" OnClientClick="return ValidateCheckBoxStates();" />
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit Txn." CssClass="cls_button"
                            Width="80px" Visible="false" OnClientClick="return ValidateCheckBoxStates();" />
                        <%--  <asp:Button ID="btnsavesum" runat="server" Text="Save" CssClass="cls_button" Width="80px"
                            ValidationGroup="SFMSSum" Visible="false" />--%>
                    </div>
                    <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                        <div style="width: 98%; padding-left: 5px; padding-right: 5px; padding-top: 5px;
                            padding-bottom: 5px;">
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="100%"
                                ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TXN_NO,CUST_CODE,CONT_CODE">
                                <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    There is no data to display.</EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Select">
                                        <itemstyle horizontalalign="Center" />
                                        <itemtemplate><asp:CheckBox id="chkdelete" runat="server" visible ="false"></asp:CheckBox></itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <itemstyle horizontalalign="Center" />
                                        <itemtemplate><asp:ImageButton ID="btnedit" runat="server" Visible="false" CommandName="edit" src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'></asp:ImageButton>
                                        </itemtemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField HeaderText="View" ShowselectButton="True" ShowHeader="True" selecttext="<img src='../../images/ico_edit.gif' alt='View' border='0' height='18px' width='18px'/>">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="TXN_STATUS" HeaderText="Txn. Status" ReadOnly="True" SortExpression="TXN_STATUS">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TXN_NO" HeaderText="Txn. No." ReadOnly="True" SortExpression="TXN_NO">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code" ReadOnly="True"
                                        SortExpression="CUST_CODE">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True"
                                        SortExpression="CUST_NAME">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONT_CODE" HeaderText="Contact Code" ReadOnly="True" SortExpression="CONT_CODE">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CONT_NAME" HeaderText="Contact Name" ReadOnly="True" SortExpression="CONT_NAME">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="VISIT_ID" HeaderText="Visit Id" ReadOnly="True" SortExpression="VISIT_ID">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TXN_DATE" HeaderText="Txn. Date" ReadOnly="True" SortExpression="TXN_DATE">
                                        <itemstyle horizontalalign="Center" />
                                    </asp:BoundField>
                                </Columns>
                            </ccGV:clsGridView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
