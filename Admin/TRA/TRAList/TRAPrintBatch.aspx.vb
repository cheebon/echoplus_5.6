﻿Imports System.Data

Imports System
Imports System.IO
Imports System.Text
Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms

Partial Class Admin_TRA_TRAList_TRAPrintBatch
    Inherits System.Web.UI.Page

    Public Property TxnNo() As String
        Get
            Return Session("_TxnNo")
        End Get
        Set(ByVal value As String)
            Session("_TxnNo") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UpdatePrintNo()
            Print()

        End If
    End Sub

    Public Sub UpdatePrintNo()
        Dim strTxnNo As String
        strTxnNo = TxnNo()
        Dim clsTRA As New adm_TRA.clsTRA
        With clsTRA
            .clsProperties.txn_no = strTxnNo
            .clsProperties.user_code = Session("UserCode")
            .UpdTRAPrintNoBatch()
        End With

    End Sub

    Public Sub Print()
        Try

            Dim strTxnNo As String
            strTxnNo = TxnNo()

            Dim clsTRA As New adm_TRA.clsTRA
            Dim dtDtl As DataTable
            Dim dtHdr As DataTable

            With clsTRA
                .clsProperties.txn_no = strTxnNo
                .clsProperties.user_code = Session("UserCode")
                dtHdr = .GetTRAListHdrReportBatch
            End With

            With clsTRA
                .clsProperties.txn_no = strTxnNo
                .clsProperties.user_code = Session("UserCode")
                dtDtl = .GetTRAListDtlReportBatch
            End With
            ReportViewer1.ProcessingMode = ProcessingMode.Local
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.Dispose()
            ReportViewer1.LocalReport.ReportPath = "Admin/TRA/TRAList/TRAHdr.rdlc"
            ReportViewer1.LocalReport.DataSources.Clear()
            AddHandler ReportViewer1.LocalReport.SubreportProcessing, AddressOf SetSubDataSource
            ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WebForms.ReportDataSource("SAP_TRA_SPP_RPT_SAP_TRA_HDR", dtHdr))
            ReportViewer1.ZoomMode = ZoomMode.Percent
            ReportViewer1.LocalReport.Refresh()


        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetSubDataSource(ByVal sender As Object, ByVal e As SubreportProcessingEventArgs)
        Dim strTxnNo As String
        strTxnNo = TxnNo()

        Dim clsTRA As New adm_TRA.clsTRA
        Dim dtDtl As DataTable
        Dim dtHdr As DataTable

        With clsTRA
            .clsProperties.txn_no = strTxnNo
            .clsProperties.user_code = Session("UserCode")
            dtHdr = .GetTRAListHdrReportBatch
        End With

        With clsTRA
            .clsProperties.txn_no = strTxnNo
            .clsProperties.user_code = Session("UserCode")
            dtDtl = .GetTRAListDtlReportBatch
        End With
        e.DataSources.Add(New ReportDataSource("SAP_TRA_SPP_RPT_SAP_TRA_DTL", dtDtl))
        e.DataSources.Add(New ReportDataSource("SAP_TRA_SPP_RPT_SAP_TRA_HDR", dtHdr))

    End Sub
End Class
