'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	20/07/2006
'	Purpose	    :	Class Logging for Standard Function 
'
'	Revision	: 	
' -----------------------------------------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			                                    |	
' -----------------------------------------------------------------------------------------------------------
' |1	    |             	|    	    	    |                                                           |
' |2	    |			    |		            |		    		                                        |
' -----------------------------------------------------------------------------------------------------------
'************************************************************************************************************
Option Explicit On 
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices

Imports System.Xml
Imports System.Diagnostics
Imports System.IO
Imports System.ComponentModel
Imports System.Configuration

'<Guid("368F4EC0-D9A0-4399-B793-494F9D165904"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsLog
    'Inherits ServicedComponent

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Public clsProperties As New clsProperties.clsLog

    Private strErrFileName As String = System.AppDomain.CurrentDomain.BaseDirectory.ToString & ConfigurationSettings.AppSettings("LogFileName")

    Private Function GetSeverityIndex(ByVal lngSeverityID As Integer) As String
        Select Case lngSeverityID
            Case 1
                GetSeverityIndex = "Debug"
            Case 2
                GetSeverityIndex = "Information"
            Case 3
                GetSeverityIndex = "Warning"
            Case 4
                GetSeverityIndex = "Error"
            Case 5
                GetSeverityIndex = "Critical System"
            Case Else
                GetSeverityIndex = "Error"
        End Select
        Exit Function
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Log
    ' Purpose	    :	Start Logging.
    ' Parameters    :	[in] sender: -
    '   		        [in]      e: -
    '		            [out]     
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Log()
        Try
            Select Case clsProperties.LogTypeID
                Case 1
                    'logging into xml
                    addXmlLog(strErrFileName)
                Case 2
                    'logging into event viewer
                    addEventViewer()
                Case Else
                    'logging into xml
                    addXmlLog(strErrFileName)
            End Select
            Exit Sub

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Log.clsLog.Log : " & ex.ToString))
        Finally
            'objSession = Nothing
        End Try
    End Sub

    'logging into event viewer
    Private Sub addEventViewer()
        Dim objEventLog As EventLog

        objEventLog = New EventLog("Application", ".", "Echoplus")
        objEventLog.Source = "Test"
        'EventLog.CreateEventSource(objEventLog.Source, "Application")

        Try
            'Severity ID 1=Debug, 2=Info, 3=Warning, 4 = Error, 5= Critical System
            Select Case clsProperties.SeverityID
                Case "3" 'Warning
                    EventLog.WriteEntry("Echoplus", "cor_Log.clsLog.Log : " & clsProperties.LogMsg, EventLogEntryType.Warning)
                Case "4" 'Error
                    EventLog.WriteEntry("Echoplus", "cor_Log.clsLog.Log : " & clsProperties.LogMsg, EventLogEntryType.Error)
                Case Else
                    EventLog.WriteEntry("Echoplus", "cor_Log.clsLog.Log : " & clsProperties.LogMsg, EventLogEntryType.Information)
            End Select

            Exit Sub

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Log.clsLog.addEventViewer : " & ex.ToString))
        Finally
            objEventLog = Nothing
        End Try
    End Sub

    'logging into xml file
    Private Sub addXmlLog(ByVal mXmlFileName As String)
        Dim objXML As cor_Xml.clsWriteXml
        Dim objRoot, objErr As Object
        'Dim strXML As String

        Try
            'if the xml file not exist, create a new xml file 
            objXML = New cor_Xml.clsWriteXml
            If File.Exists(mXmlFileName) = False Then
                objRoot = objXML.CreateDocument("Log")
            Else
                objRoot = objXML.LoadDocument(mXmlFileName, "Log")
            End If

            objErr = objXML.AddElement(objRoot, "ErrMsg", "")
            Call objXML.AddAttribute("Severity", GetSeverityIndex(clsProperties.SeverityID))
            Call objXML.AddAttribute("DateLogIn", Now)
            Call objXML.AddAttribute("DateLogOut", Now)
            Call objXML.AddAttribute("LogMsg", clsProperties.LogMsg)
            Call objXML.AddAttribute("Remark", clsProperties.Remarks)
            objXML.SaveXML(mXmlFileName)

            Exit Sub

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_Log.clsLog.addXMLLog : " & ex.ToString))
        Finally
            objXML = Nothing
            objRoot = Nothing
            objErr = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
