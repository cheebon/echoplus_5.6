﻿var _UserDefinedRoute;

function UserDefinedRoute()
{
    this.VisitPoints = [];
    _UserDefinedRoute = this;
}

UserDefinedRoute.prototype.AddPoint = addpoint;
UserDefinedRoute.prototype.Clear = clear;
UserDefinedRoute.prototype.RemovePoint = removepoint;
UserDefinedRoute.prototype.isDestination = isDestination;

function addpoint(_From, _To)
{
    var vp = new VisitPoint(_From, _To);
    
    _UserDefinedRoute.VisitPoints.push(vp);
}

function clear()
{
    this.VisitPoints = [];
}

function removepoint(_From)
{
    for(var i=0; i<_UserDefinedRoute.VisitPoints.length;++i)
    {
       if(_UserDefinedRoute.VisitPoints[i].From == _From)
       {
            _UserDefinedRoute.VisitPoints.splice(i,1);
            return;
       }
    }
}

function isDestination(_Destination)
{
    for(var i=0; i<_UserDefinedRoute.VisitPoints.length;++i)
    {
       if(_UserDefinedRoute.VisitPoints[i].To == _Destination)
       {
            return true;
       }
    }
    
    return false;
}