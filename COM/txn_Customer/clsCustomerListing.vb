'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	27/10/2006
'	Purpose	    :	Class to build datatables for routes and customer listings.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustomerListing
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCustomerListing"
        End Get
    End Property

   

    Function getRouteDT(ByVal StrSalesrepCode As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERROUTE"
                .addItem("SALESREP_CODE", StrSalesrepCode, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRouteDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function getVisit_CustListDT(ByVal strSalesrepCode As String, ByVal strRouteCode As String, ByVal strSearchValue As String, ByVal strSearchBy As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_VISIT_CUSTOMERLISTING"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("SEARCH_BY", strSearchBy, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRouteDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

    Function getPlan_CustListDT(ByVal strSalesrepCode As String, ByVal strRouteCode As String, ByVal strSearchValue As String, ByVal strSearchBy As String, ByVal strPlanDate As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLAN_CUSTOMERLISTING"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("SEARCH_BY", strSearchBy, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("PLAN_DATE", strPlanDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRouteDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

#Region "Plan Customize"
    Function getPlan_CustContListDT(ByVal strSalesrepCode As String, ByVal strSearchValue As String, ByVal strSearchBy As String, ByVal strPlanDate As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLAN_CUSTOMER_CONTACT_LISTING"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SEARCH_BY", strSearchBy, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("PLAN_DATE", strPlanDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRouteDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function

#End Region

#Region "OLDSPP"

    Function getCustListDT(ByVal strSalesmanCode As String, ByVal strrouteCode As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_CUSTOMERLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("routecode", strrouteCode, 2)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRouteDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function
    Sub InsertDailyTimeSum(ByVal strBranchCode As String, ByVal strSalesMan_code As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strCont_Acc_code As String, ByVal strTimeIn As String)
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_INSERTDAILYTIMESUM"
                .addItem("branch_code", strBranchCode, 2)
                .addItem("salesman_code", strSalesMan_code, 2)
                .addItem("visit_id", strVisitID, 2)
                .addItem("cust_code", strCustCode, 2)
                .addItem("cont_acc_code", strCont_Acc_code, 2)
                '.addItem("call_date", DateTime.Now.ToString(), 3)
                .addItem("time_in", strTimeIn, 2)
                .addItem("server_type", "EXPRESS", 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".InsertDailyTimeSum :" & ex.Message))
        End Try
    End Sub

    Function getVisitID(ByVal strTableName As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_GETVISITID"

                .addItem("TableName", strTableName, 2)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getRouteDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try
    End Function
#End Region


#Region "Exception Msg"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region


End Class
