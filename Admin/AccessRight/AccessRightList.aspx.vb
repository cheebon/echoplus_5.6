'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	AccessRight List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_AccessRight_AccessRightList
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSearchType, strSearchValue As String

        Try
            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "Access Right List"
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            btnCreate.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.AR, SubModuleAction.Create)

            If Not IsPostBack Then
                'constant

                'request
                strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "accessright_name")
                strSearchValue = Trim(Request.QueryString("search_value"))
                If strSearchValue <> "" Then
                    txtSearchValue.Text = strSearchValue
                End If

                'Prepare Recordset,Createobject as needed
                ddlSearchType.SelectedValue = strSearchType
                ddlSearchType_onChanged()
                BindGrid(ViewState("SortCol"))
            End If
        Catch ex As Exception
            ExceptionMsg("AccessRightList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim dt As DataTable

        Try
            If intPassFlag <> 1 Or IsNothing(ViewState("AccessRightList")) Then
                objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
                With objAccessRightQuery
                    .clsProperties.search_type = Trim(ddlSearchType.SelectedValue)
                    Select Case ddlSearchType.SelectedValue
                        'Case "Date"
                        '    .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                        '    .clsProperties.search_value_1 = Trim(Replace(txtSearchValue1.Text, "*", "%"))
                        'Case "StatusID"
                        '    .clsProperties.search_value = Trim(ddlList.SelectedValue)
                        Case Else
                            .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                    End Select
                    .clsProperties.delete_flag = 1
                    .clsProperties.user_code = Trim(Session("UserCode"))   'HL:20070622
                    dt = .GetAccessRightList
                End With
                ViewState("AccessRightList") = dt
            Else
                dt = ViewState("AccessRightList")
            End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With

            For Each COL As DataControlField In dgList.Columns
                Select Case COL.HeaderText.Trim.ToUpper
                    Case "COPY"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.AR, SubModuleAction.Create)
                    Case "DELETE"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.AR, SubModuleAction.Delete)
                    Case "EDIT"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.AR, SubModuleAction.Edit)
                End Select

            Next

        Catch ex As Exception
            ExceptionMsg("AccessRightList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            objAccessRightQuery = Nothing
        End Try
    End Sub

#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via AccessRight conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("AccessRightList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via AccessRight conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("AccessRightList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via AccessRight conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("AccessRightList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "dgList"
    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim objAccessRight As adm_AccessRight.clsAccessRight

        Try
            'Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            'Dim row As GridViewRow = dgList.Rows(index)

            Dim index As Integer = 0
            Dim row As GridViewRow

            If Integer.TryParse(e.CommandArgument, index) = True Then
                row = IIf(index >= 0 AndAlso index <= dgList.Rows.Count, dgList.Rows(index), Nothing)

                Select Case e.CommandName
                    Case "Copy"
                        Response.Redirect("AccessRightCreate.aspx?accessright_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Delete"
                        objAccessRight = New adm_AccessRight.clsAccessRight
                        With objAccessRight
                            .clsProperties.accessright_id = Trim(dgList.DataKeys(row.RowIndex).Value)
                            .Delete()
                        End With

                        Response.Redirect("AccessRightList.aspx?search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Details"
                        Response.Redirect("AccessRightDtl.aspx?accessright_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Edit"
                        Response.Redirect("AccessRightEdit.aspx?accessright_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&ori=edit&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                End Select
            End If



        Catch ex As Exception
            ExceptionMsg("AccessRightList.dgList_RowCommand : " + ex.ToString)
        Finally
            objAccessRight = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_RowDataBound
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim imgCopy, imgEdit, imgDelete As ImageButton

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                drv = CType(e.Row.DataItem, DataRowView)

                If IsDBNull(drv("accessright_name")) Then
                    imgCopy = CType(e.Row.Cells(5).Controls(0), ImageButton)
                    imgDelete = CType(e.Row.Cells(6).Controls(0), ImageButton)
                    imgEdit = CType(e.Row.Cells(7).Controls(0), ImageButton)
                    imgCopy.Visible = False
                    imgDelete.Visible = False
                    imgEdit.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionMsg("AccessRightList.dgList_RowDataBound : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("AccessRightList.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        'Dim objAccessRight As adm_AccessRight.clsAccessRight
        'Dim intCurrentPage As Integer

        Try
            '    dgList.EditIndex = -1

            '    objAccessRight = New adm_AccessRight.clsAccessRight
            '    With objAccessRight
            '        .clsProperties.AccessRight_id = "0" 'drv("AccessRight_id")
            '        .clsProperties.AccessRight_code = Trim(dgList.DataKeys(e.RowIndex).Value)
            '        .Delete()
            '    End With

            '    'Response.Redirect("AccessRightList.aspx", False)

            '    intCurrentPage = wuc_dgpaging.CurrentPageIndex
            '    dgList.PageIndex = 0

            '    BindGrid(ViewState("SortCol"))

            '    If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
            '        wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            '    Else
            '        wuc_dgpaging.CurrentPageIndex = intCurrentPage
            '    End If
            '    wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            '    dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            '    BindGrid(ViewState("SortCol"), 1)

        Catch ex As Exception
            ExceptionMsg("AccessRightList.dgList_RowDeleting : " + ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

    End Sub
#End Region

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
        Catch ex As Exception
            lblErr.Text = "AccessRightList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnCreate_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            Response.Redirect("AccessRightCreate.aspx?search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
        Catch ex As Exception
            ExceptionMsg("AccessRightList.btnCreate_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            ddlSearchType_onChanged()
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "AccessRightList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_onChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ddlSearchType_onChanged()
        Try
            'txtSearchValue.Text = ""
            'txtSearchValue1.Text = ""
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"

            lblSearchValue.Visible = False
            lblDot.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            'lblDate1.Visible = False
            'lblDate2.Visible = False

            Select Case ddlSearchType.SelectedValue
                Case "all"

                    'Case "Date"
                    '    lblSearchValue.Visible = True
                    '    lblDot.Visible = True
                    '    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    '    lblTo.Visible = True
                    '    txtSearchValue.Visible = True
                    '    txtSearchValue.Text = ""
                    '    txtSearchValue.ReadOnly = True
                    '    txtSearchValue1.Visible = True
                    '    txtSearchValue1.ReadOnly = True
                    '    txtSearchValue1.Text = ""
                    '    btnSearch.Visible = True
                    '    'lblDate1.Visible = True
                    '    'lblDate2.Visible = True

                    'Case "status"
                    '    lblSearchValue.Visible = True
                    '    lblDot.Visible = True
                    '    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    '    btnSearch.Visible = True
                    '    ddlList.Visible = True

                    '    'Get Status Record
                    '    ddlList.Items.Clear()
                    '    ddlList.Items.Add(New ListItem("Select", ""))
                    '    ddlList.Items.Add(New ListItem("Active", "1"))
                    '    ddlList.Items.Add(New ListItem("In-Active", "0"))

                Case Else
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "AccessRightList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
