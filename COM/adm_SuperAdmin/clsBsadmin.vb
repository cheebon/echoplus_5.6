﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsBsadmin
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function BsadminList(ByVal strCountryID As String, ByVal strPrincipalID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_LIST"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                BsadminList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.BsadminList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function BsadminList(ByVal strBsadminID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_BSADMIN_LIST"
                .addItem("BSADMIN_ID", strBsadminID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                BsadminList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.BsadminList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub DeleteBsadmin(ByVal strBsadminID As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_BSADMIN_DELETE"
                .addItem("BSADMIN_ID", strBsadminID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.BsadminList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Public Function CreateBsadminUser(ByVal strPrincipalID As String, ByVal strCountryID As String, ByVal strLogin As String, ByVal strPassword As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim crypto As New cor_Crypto.clsEncryption
        Dim bsadmin As String
        Dim pwd As String
        Try

            objDB = New cor_DB.clsDB
            pwd = crypto.TripleDESEncrypt(strPassword)
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_CREATE_BSADMIN"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBString)
                .addItem("LOGIN", strLogin, cor_DB.clsDB.DataType.DBString)
                .addItem("PASSWORD", pwd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                CreateBsadminUser = .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsBsadmin.CreateBsadminUser" & ex.ToString))
        Finally
            objDB = Nothing
            crypto = Nothing
        End Try
    End Function
    

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
