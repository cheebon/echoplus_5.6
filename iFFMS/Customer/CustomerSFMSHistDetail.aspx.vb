Imports System.Data

Partial Class CustomerSFMSHistDetail
    Inherits System.Web.UI.Page



    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerSFMSHistDtl"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Call Header
        With wuc_lblheader
            .Title = "Field Activity History Detail"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If


    End Sub
    Sub onpageload()
        Dim strTxnNo, strSalesmanCode, strCustCode As String
        Try
            strTxnNo = Request.QueryString("txnno")
            strSalesmanCode = Session("SALESREP_CODE")
            strCustCode = Session("customercode")
            bindGridView(strSalesmanCode, strCustCode, strTxnNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            onpageload()
            TimerControl1.Enabled = False
        End If

    End Sub
    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strTxnNo As String)
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getSFMSHistDetailDT(strSalesmanCode, strCustCode, strTxnNo)
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()

                lbldate.Text = IIf(IsDBNull(dt.Rows(0)("txn_date")), "Not Available", dt.Rows(0)("txn_date"))
                lbldate.Text = IIf(String.IsNullOrEmpty(lbldate.Text), "NotAvailable", lbldate.Text)

                lblremark.Text = IIf(IsDBNull(dt.Rows(0)("headerremark")), "Not Available", dt.Rows(0)("headerremark"))
                lblremark.Text = IIf(String.IsNullOrEmpty(lblremark.Text), "NotAvailable", lblremark.Text)
                'txtAmount.Text = dt.Rows(0)("ord_amt").ToString

            Else
                dt.Rows.Add(dt.NewRow)
                gridview1.DataSource = dt
                gridview1.DataBind()
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../Customer/CustomerSFMSHist.aspx", False)
    End Sub
End Class



