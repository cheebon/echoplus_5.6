'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Class Common for Properties 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
'Imports System.ComponentModel

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
'<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    'Inherits ServicedComponent

    Public Class clsCommon
             
    End Class

    Public Class clsCommonQuery
        Public CountryCode As String
        Public PrincipalCode As String
        Public PrincipalID As String
        Public SalesTeamCode As String
        Public UserCode As String
        Public accessright_id As String
    End Class
End Class
