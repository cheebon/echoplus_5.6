'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	21/07/2005
'	Purpose	    :	Paging User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On
'Imports corOB_CommonUtil
Imports System.Data

Namespace Echoplus
    Partial Class wuc_dgPaging
        Inherits System.Web.UI.UserControl

        Private dt, dtLanguage As DataTable
        Private strLanguageRefNo As String
        Private Shared lngLanguageID As Long
        Private Shared lngModuleID As Long
        Private Shared lngUserID As Long
        Private Shared lngSiteID As Long
        Private Shared lngDocTypeID As Long
        Private Shared intPageCount As Integer
        Private Shared intCurrentPageIndex As Integer

        'For Field Manager
        Private lngTableID As Long
        Private strRedirectPath As String
        Private strDocID As String

        Private Const intInfoSeverity As Integer = 2
        Private Const intWarningSeverity As Integer = 3
        Private Const intErrSeverity As Integer = 4

        'Protected WithEvents lblPage As System.Web.UI.WebControls.Label
        'Protected WithEvents lblTotPage As System.Web.UI.WebControls.Label
        'Protected WithEvents lblGotoPage As System.Web.UI.WebControls.Label
        'Protected WithEvents txtPageNo As System.Web.UI.WebControls.TextBox
        'Protected WithEvents btnGo As System.Web.UI.WebControls.Button
        'Protected WithEvents lblColon1 As System.Web.UI.WebControls.Label
        'Protected WithEvents imgPrevious As System.Web.UI.WebControls.ImageButton
        'Protected WithEvents lnkPrevious As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnkNext As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents imgNext As System.Web.UI.WebControls.ImageButton
        'Protected WithEvents txtShowMsgViewerPath As System.Web.UI.WebControls.TextBox
        'Protected WithEvents ltlMsg As System.Web.UI.WebControls.Literal
        Public Event Go_Click As EventHandler
        Public Event Previous_Click As EventHandler
        Public Event Next_Click As EventHandler

        'Private objCommFunction As corOB_CommonUtil.clsCommonFunction
        'Private objCommQuery As clsCommonQuery
        'Private lngLockID As Long
        'Private blnLock As Boolean
        'Private strUserName As String

        Private blnSuccess As Boolean

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ddlTestLevelID As System.Web.UI.WebControls.DropDownList
        Protected WithEvents cfvTestLevelID As System.Web.UI.WebControls.CompareValidator

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Sub Go_OnClick(ByVal e As EventArgs)
            RaiseEvent Go_Click(Me, e)
            Exit Sub
        End Sub

        Sub Go_ControlOnClick(ByVal s As Object, ByVal e As EventArgs) Handles btnGo.Click
            Go_OnClick(e)
            Exit Sub
        End Sub

        Sub Previous_OnClick(ByVal e As EventArgs)
            RaiseEvent Previous_Click(Me, e)
            Exit Sub
        End Sub

        Public Sub Previous_ControlOnClick(ByVal s As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Previous_OnClick(e)
            Exit Sub
        End Sub

        Sub Next_OnClick(ByVal e As EventArgs)
            RaiseEvent Next_Click(Me, e)
            Exit Sub
        End Sub

        Public Sub Next_ControlOnClick(ByVal s As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
            Next_OnClick(e)
            Exit Sub
        End Sub

        Public Sub imgNext_ControlOnClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgNext.Click
            Next_OnClick(e)
            Exit Sub
        End Sub

        Private Sub imgPrevious_ControlOnClick(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrevious.Click
            Previous_OnClick(e)
            Exit Sub
        End Sub

        Public Property PageNo() As Long
            Get
                If txtPageNo.Text = String.Empty Then
                    Return 1
                End If
                If validPage() Then
                    txtPageNo.Text = txtPageNo.Text
                Else
                    txtPageNo.Text = 1
                End If
                Return Long.Parse(txtPageNo.Text)
            End Get
            Set(ByVal Value As Long)
                txtPageNo.Text = Value.ToString()
            End Set
        End Property


        Public Property DocTypeID() As Long
            Get
                Return lngDocTypeID
            End Get
            Set(ByVal Value As Long)
                lngDocTypeID = Value
            End Set
        End Property

        Public Property SiteID() As Long
            Get
                Return lngSiteID
            End Get
            Set(ByVal Value As Long)
                lngSiteID = Value
            End Set
        End Property

        Public Property PageCount() As Integer
            Get
                Return intPageCount
            End Get
            Set(ByVal Value As Integer)
                If Value = 0 Then
                    Value = 1
                End If
                intPageCount = Value
                lblTotPage.Text = (intCurrentPageIndex + 1) & " of " & intPageCount.ToString
            End Set
        End Property

        Public Property ShowMsgViewerPath() As String
            Get
                Return txtShowMsgViewerPath.Text
            End Get
            Set(ByVal Value As String)
                txtShowMsgViewerPath.Text = Value
            End Set
        End Property

        Public Property CurrentPageIndex() As Integer
            Get
                Return intCurrentPageIndex
            End Get
            Set(ByVal Value As Integer)
                intCurrentPageIndex = Value
                lblTotPage.Text = (intCurrentPageIndex + 1) & " of " & intPageCount.ToString
            End Set
        End Property

        'For Field Manager --------------------------------
        Public Property SearchDocID() As String
            Get
                Return strDocID
            End Get
            Set(ByVal Value As String)
                strDocID = Value
            End Set
        End Property

        Public Property RedirectPath() As String
            Get
                Return strRedirectPath
            End Get
            Set(ByVal Value As String)
                strRedirectPath = Value
            End Set
        End Property

        Public Property TableID() As Long
            Get
                Return lngTableID
            End Get
            Set(ByVal Value As Long)
                lngTableID = Value
            End Set
        End Property
        '---------------------------------------------------

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Try
                'ltlMsg.Text = ""

            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.Page_Load : " & ex.ToString)
            End Try
        End Sub

        Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload
            Try

            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.Page_UnLoad : " & ex.ToString)
            End Try
        End Sub

        Public Overrides Sub DataBind()
            Try
                intCurrentPageIndex = IIf(IsNothing(intCurrentPageIndex), "1", intCurrentPageIndex)
                intPageCount = IIf(IsNothing(intPageCount), "1", intPageCount)

                lblTotPage.Text = (intCurrentPageIndex + 1) & " of " & intPageCount.ToString
                Exit Sub

            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.DataBind : " & ex.ToString)
            Finally

            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                If validPage() Then
                    txtPageNo.Text = txtPageNo.Text
                Else
                    txtPageNo.Text = 1
                End If
            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.btnGo_Click : " & ex.ToString)
            Finally
            End Try
        End Sub

        Private Function validPage() As Boolean
            Dim blnResult As Boolean = True
            'Dim intMsgID As Integer
            Dim strMsg As String = ""

            Try
                txtPageNo.Text = Trim(txtPageNo.Text)
                If Trim(txtPageNo.Text) = Nothing Or IsDBNull(Trim(txtPageNo.Text)) Then
                    strMsg = "alert('Page No cannot be Blank !');"
                    'intMsgID = 23
                    'Call objMessage.AddMsgList(intErrSeverity, intMsgID, )
                    blnResult = False
                Else
                    If Not IsNumeric(Trim(txtPageNo.Text)) Then
                        strMsg = "alert('Page No must be numeric !');"
                        'intMsgID = 24
                        'Call objMessage.AddMsgList(intErrSeverity, intMsgID, )

                        blnResult = False
                    Else
                        If Trim(txtPageNo.Text) <= "0" Or CDbl(txtPageNo.Text) > CDbl(intPageCount) Then
                            strMsg = "alert('Page No must be greater than 0.\nPlease re-enter.');"
                            'intMsgID = 25
                            'Call objMessage.AddMsgList(intErrSeverity, intMsgID, )

                            blnResult = False
                        End If
                    End If
                End If
                If blnResult <> False Then
                    txtPageNo.Text = Trim(CType(txtPageNo.Text, Integer))
                    'ltlMsg.Text = ""
                    blnResult = True
                End If

                If blnResult = False Then
                    'ltlMsg.Text = strMsg
                    Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>" & strMsg & "</script>")
                    'Call ShowMsgViewer()
                End If
                Return blnResult

                Exit Function

            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.validPage : " & ex.ToString)
            End Try
        End Function

        Private Sub ShowMsgViewer()
            Dim strPopupScript As String

            Try
                strPopupScript = "<script language='javascript'>" & _
                                 "OpenPopUpMsg('" & txtShowMsgViewerPath.Text & "')" & _
                                 "</script>"
                Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", strPopupScript)
            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.ShowMsgViewer : " & ex.ToString)
            Finally

            End Try
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                'If intCurrentPageIndex > 0 Then
                '    dgList.CurrentPageIndex = dgList.CurrentPageIndex - 1
                'End If
                'txtPageNo.Text = dgList.CurrentPageIndex + 1
                'BindGrid(viewstate("SortCol"))
            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.lnkPrevious_Click : " & ex.ToString)
            End Try
        End Sub

        Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                'If dgList.PageCount - 1 > dgList.CurrentPageIndex Then
                '    dgList.CurrentPageIndex = dgList.CurrentPageIndex + 1
                'End If
                'txtPageNo.Text = dgList.CurrentPageIndex + 1
                'BindGrid(viewstate("SortCol"))
            Catch ex As Exception
                ExceptionMsg("wuc_dgPaging.lnkNext_Click : " & ex.ToString)
            End Try
        End Sub

        Public Sub TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            If Not IsNumeric(txtPageNo.Text) And txtPageNo.Text <> "" Then
                txtPageNo.Text = Mid(txtPageNo.Text, 1, Len(txtPageNo.Text) - 1)
            End If
        End Sub

        '---------------------------------------------------------------------------------------------------------
        ' Procedure :   Sub ExceptionMsg
        ' Purpose	:	This function will add the error message and pop up the message viewer
        ' Parameters:	[in]  : 
        '		        [out] : 
        '---------------------------------------------------------------------------------------------------------
        Private Sub ExceptionMsg(ByVal strMsg As String)
            'Call error Message Viewer
            Try
                'lblErr.Text = ""
                'lblErr.Text = strMsg
                'ltlMsg.Text = "alert('" & strMsg & "');"
                Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>alert('" & strMsg & "');</script>")

                'Call error log class
                Dim objLog As cor_Log.clsLog
                objLog = New cor_Log.clsLog
                With objLog
                    .clsProperties.LogTypeID = 1
                    .clsProperties.DateLogIn = Now
                    .clsProperties.DateLogOut = Now
                    .clsProperties.SeverityID = 4
                    .clsProperties.LogMsg = strMsg
                    .Log()
                End With
                objLog = Nothing
            Catch ex As Exception

            Finally
            End Try
        End Sub

        Public Property RowCount() As Integer
            Get
                Return CInt(lblRecordCount.Text)
            End Get
            Set(ByVal value As Integer)
                RecordCount.Visible = True
                lblRecordCount.Text = value
                lblRecordCount.ForeColor = IIf(value = 0, Drawing.Color.Red, Drawing.Color.Blue)
            End Set
        End Property
    End Class

End Namespace

