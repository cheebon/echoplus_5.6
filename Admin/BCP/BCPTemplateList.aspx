﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BCPTemplateList.aspx.vb" Inherits="Admin_BCP_BCPTemplateList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../scripts/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../../scripts/popper.min.js"></script>
    <script type="text/javascript" src="../../scripts/bootstrap-4.3.1.min.js"></script>
    <link rel="stylesheet" href="../../styles/bootstrap-4.3.1.min.css" />

    <script type="text/javascript" src="../../scripts/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../scripts/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../../scripts/dataTables.select.min.js"></script>

    <link rel="stylesheet" href="../../styles/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="../../styles/buttons.dataTables.min.css" />
    <link rel="stylesheet" href="../../styles/select.dataTables.min.css" />

    <script type="text/javascript" src="../../scripts/jquery.ui.core.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.ui.widget.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.ui.mouse.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.ui.sortable.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            LoadTemplateTable(function () {
                $('#tblTemplate').DataTable();
            });
        })

        function LoadTemplateTable(callback) {
            $('#BCPTemplateModal').modal('hide');
            $('#tblTemplate tbody').empty();
            $.ajax({
                type: "POST",
                url: "BCPTemplateList.aspx/GetTemplateList",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var objData = $.parseJSON(data.d);
                    var html = ''; var indicator = "Edit";
                    $.each(objData, function (i, v) {
                        html += '<tr id=' + v.bcp_template_id + '>';
                        html += '<td>' + v.bcp_template_id + '</td>';
                        html += '<td>' + v.bcp_template_name + '</td>';
                        html += '<td>' + v.bcp_template_desc + '</td>';
                        html += '<td>' + v.in_seq + '</td>';
                        html += '<td><a href=# onclick="OpenTemplate(' + "'" + indicator + "'" + ', ' + v.bcp_template_id + ', ' + "'" + v.bcp_template_name + "'" + ', ' + "'" + v.bcp_template_desc + "'" + ')">Edit Template</a> | <a href=# onclick="OpenSteps(' + v.bcp_template_id + ', ' + "'" + v.bcp_template_name + "'" + ')">Edit Steps</a> | <a href=# onclick="DeleteTemplate(' + v.bcp_template_id + ')">Delete</a>';
                    })
                    $('#tblTemplate tbody').append(html);
                    callback();
                },
                failure: function (response) {
                    console.log("error");
                }
            });
        }

        function OpenTemplate(page_indicator, bcp_template_id, bcp_template_name, bcp_template_desc) {
            if (page_indicator == 'Edit') {
                $('#btnSave').css('display', 'none'); $('#btnUpdate').css('display', ''); $('#txtBCPTemplateName').prop('disabled', true);
                $('#txtBCPTemplateName').val(bcp_template_name); $('#txtBCPTemplateDesc').val(bcp_template_desc); 
                $('#lblBCPTemplateID').text(bcp_template_id);
            }
            else {
                $('#btnSave').css('display', ''); $('#btnUpdate').css('display', 'none'); $('#txtBCPTemplateName').prop('disabled', false);
                $('#txtBCPTemplateName').val(''); $('#txtBCPTemplateDesc').val(''); $('#lblBCPTemplateID').text(''); $('#ulModuleList').empty();
            }

            var options = { "backdrop": "static" };
            $('#BCPTemplateModal').modal(options);
            $('#BCPTemplateModal').modal('show');
        }

        function DeleteTemplate(bcp_template_id) {
            var r = confirm("Are you sure to delete?");
            if (r == true) {
                $.ajax({
                    type: "POST",
                    url: "BCPTemplateList.aspx/DeleteTemplateList",
                    data: "{'strBCPTemplateID':'" + bcp_template_id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        LoadTemplateTable(function () {
                            $('#tblTemplate').DataTable();
                        });
                    },
                    failure: function (response) {
                        console.log("error");
                    }
                });
            }
        }

        function CreateTemplate() {
            var strBCPTemplateName = $('#txtBCPTemplateName').val(),
                strBCPTemplateDesc = $('#txtBCPTemplateDesc').val(),
                strInSeq = $('#ddlInSeq').val();

            var data = {
                "strBCPTemplateName": $('#txtBCPTemplateName').val(),
                "strBCPTemplateDesc": $('#txtBCPTemplateDesc').val(),
                "strInSeq": $('#ddlInSeq').val()
            };

            $.ajax({
                type: "POST",
                url: "BCPTemplateList.aspx/GetBCPTemplateDuplicate",
                data: "{'strBCPTemplateName': '" + strBCPTemplateName + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var objData = $.parseJSON(data.d);
                    if (objData[0].cnt > 0) {
                        alert("Template name exists");
                    }
                    else {
                        $.ajax({
                            type: "POST",
                            url: "BCPTemplateList.aspx/CreateBCPTemplate",
                            data: "{'strBCPTemplateName': '" + strBCPTemplateName + "', 'strBCPTemplateDesc': '" + strBCPTemplateDesc + "','strInSeq':'" + strInSeq + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                LoadTemplateTable(function () {
                                    $('#tblTemplate').DataTable();
                                });
                            },
                            failure: function (response) {
                                console.log("error");
                            }
                        });
                    }
                },
                failure: function (response) {
                    console.log("error");
                }
            });
        }

        function UpdateTemplate() {
            var strBCPTemplateID = $('#lblBCPTemplateID').text(),
                strBCPTemplateDesc = $('#txtBCPTemplateDesc').val(),
                strInSeq = $('#ddlInSeq').val();

            $.ajax({
                type: "POST",
                url: "BCPTemplateList.aspx/UpdateBCPTemplate",
                data: "{'strBCPTemplateID': '" + strBCPTemplateID + "', 'strBCPTemplateDesc': '" + strBCPTemplateDesc + "','strInSeq':'" + strInSeq + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    LoadTemplateTable(function () {
                        $('#tblTemplate').DataTable();
                    });
                },
                failure: function (response) {
                    console.log("error");
                }
            });
        }

        function OpenSteps(bcp_template_id, bcp_template_name) {
            $('#lblBCPTemplateID_Step').text(bcp_template_id); $('#lblBCPTemplateName_Step').text(bcp_template_name);

            $('#lblStepNo').text(''); $('#txtStepName').val(''); $('#txtStepDesc').val(''); $('#ddlMandatoryStep').val(1); $('#ulModuleList').empty();
            LoadStepsTable(bcp_template_id);

            var options = { "backdrop": "static" };
            $('#BCPStepModal').modal(options);
            $('#BCPStepModal').modal('show');
        }

        function LoadStepsTable(bcp_template_id) {
            $('#tblBCPSteps tbody').empty();
            $.ajax({
                type: "POST",
                url: "BCPTemplateList.aspx/GetStepList",
                data: "{'strBCPTemplateID':'" + bcp_template_id + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var objData = $.parseJSON(data.d);
                    var html = '', indicator = "Edit";
                    $.each(objData, function (i, v) {
                        html += '<tr id=' + v.step_no + '>';
                        html += '<td>' + v.step_no + '</td>';
                        html += '<td>' + v.step_name + '</td>';
                        html += '<td>' + v.step_desc + '</td>';
                        html += '<td>' + v.mand_step + '</td>';
                        html += '<td>' + v.module_list + '</td>';
                        html += '<td><a href=# onclick="ShowStepModuleDiv(' + "'" + indicator + "'" + ', ' + v.step_no + ')">Edit</a> | <a href=# onclick="DeleteStep(' + v.step_no + ')">Delete</a></td>';
                    })
                    $('#tblBCPSteps tbody').append(html);
                },
                failure: function (response) {
                    console.log("error");
                }
            });
        }

        function PopulateModuleDDL(callback) {
            $('#ddlModule').empty();
            $.ajax({
                type: "POST",
                url: "BCPTemplateList.aspx/GetModuleDDL",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var objData = $.parseJSON(data.d);
                    var html = '';
                    $.each(objData, function (i, v) {
                        html += '<option value="' + v.module_link_code + '">' + v.module + '</option>';
                    })
                    $('#ddlModule').append(html);
                    callback();
                },
                failure: function (response) {
                    console.log("error");
                }
            });
        }

        function ShowStepModuleDiv(page_indicator, step_no) {
            var strBCPTemplateID = $('#lblBCPTemplateID_Step').text()
            $('#ulModuleList').empty();
            PopulateModuleDDL(function () {
                if (page_indicator == "Create") {
                    $('#lblStepNo').text(''); $('#txtStepName').val(''); $('#txtStepDesc').val(''); $('#ddlMandatoryStep').val(1);
                    $('#btnSaveSteps').css('display', ''); $('#btnUpdateSteps').css('display', 'none'); 
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "BCPTemplateList.aspx/GetStepDtl",
                        data: "{'strBCPTemplateID': '" + strBCPTemplateID + "', 'strStepNo': '" + step_no + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            var result = $.parseJSON(result.d);
                            $('#lblStepNo').text(result[0].step_no); $('#txtStepName').val(result[0].step_name); $('#txtStepDesc').val(result[0].step_desc); $('#ddlMandatoryStep').val(result[0].mand_step);

                            $.ajax({
                                type: "POST",
                                url: "BCPTemplateList.aspx/GetStepModuleDtl",
                                data: "{'strBCPTemplateID': '" + strBCPTemplateID + "', 'strStepNo': '" + step_no + "'}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    var data = $.parseJSON(data.d);
                                    $.each(data, function (index, value) {
                                        GenerateULList($('#ulModuleList'), data[index].module_link, data[index].module_name)
                                        MakeULDraggable($('#ulModuleList'))
                                    });
                                },
                                failure: function (response) {
                                    console.log("error");
                                }
                            });

                            $('#btnSaveSteps').css('display', 'none'); $('#btnUpdateSteps').css('display', '');
                        },
                        failure: function (response) {
                            console.log("error");
                        }
                    });
                }

                $('#dvSteps').css('display', '');
            });
        }

        function DeleteStep(step_no) {
            var strBCPTemplateID = $('#lblBCPTemplateID_Step').text();
            var r = confirm("Are you sure to delete?");
            if (r == true) {
                $.ajax({
                    type: "POST",
                    url: "BCPTemplateList.aspx/DeleteBCPStepModule",
                    data: "{'strBCPTemplateID':'" + strBCPTemplateID + "', 'strStepNo': '" + step_no + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        LoadStepsTable(strBCPTemplateID);
                    },
                    failure: function (response) {
                        console.log("error");
                    }
                });
            }
        }

        function AddModuleID() {
            if ($('#ddlModule').val() != '') {
                if (CheckULDuplication($('#ulModuleList'), $('#ddlModule').val())) {
                    alert("Unable to add same module again");
                } else {
                    GenerateULList($('#ulModuleList'), $('#ddlModule').val(), $('#ddlModule').find(":selected").text());
                    MakeULDraggable($('#ulModuleList'));
                }
            }
        }

        function CheckULDuplication(ul, value) {
            for (var i = 0; i < ul[0].children.length ; i++) {
                if (value == ul[0].children[i].attributes["data-value"].value) {
                    return true;
                }
            }
            return false;
        }

        function GenerateULList(ul, strValue, strText) {
            ul.append('<li data-value="' + strValue + '" class="drag-item">' + strText + '</li>');
        }

        function MakeULDraggable(ul) {
            var removeIntent = false;
            ul.sortable({
                over: function () { removeIntent = false; },
                out: function () { removeIntent = true; },
                beforeStop: function (event, ui) { if (removeIntent == true) { ui.item.remove(); } }
            });
        }

        function doSaveModule(action) {
            var strBCPTemplateID = $('#lblBCPTemplateID_Step').text(),
                strStepNo = $('#lblStepNo').text(),
                strStepName = $('#txtStepName').val(),
                strStepDesc = $('#txtStepDesc').val(),
                strMandStep = $('#ddlMandatoryStep').val();

            if ($("#ulModuleList")[0].children.length == 0) {
                alert("There is no module assigned,");
                return false;
            }

            var strModuleIDList = '';
            for (var i = 0; i < $("#ulModuleList")[0].children.length ; i++) {
                if (strModuleIDList != '') strModuleIDList = strModuleIDList + ',';
                strModuleIDList = strModuleIDList + $("#ulModuleList")[0].children[i].attributes["data-value"].value;
            }
            
            if (action == "Create") {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: "BCPTemplateList.aspx/InsertBCPStepModule",
                    data: "{ 'strBCPTemplateID' : '" + strBCPTemplateID + "','strStepName' : '" + strStepName + "','strStepDesc' : '" + strStepDesc + "','strMandStep' : '" + strMandStep + "', 'strModuleIdList' : '" + strModuleIDList + "'}",
                    success: function (result) {
                        alert("Record successfully created");
                        LoadStepsTable(strBCPTemplateID);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Record unable to be created");
                    },
                });
            }
            else {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    url: "BCPTemplateList.aspx/UpdateBCPStepModule",
                    data: "{ 'strBCPTemplateID' : '" + strBCPTemplateID + "','strStepNo' : '" + strStepNo + "','strStepName' : '" + strStepName + "','strStepDesc' : '" + strStepDesc + "','strMandStep' : '" + strMandStep + "', 'strModuleIdList' : '" + strModuleIDList + "'}",
                    success: function (result) {
                        alert("Record successfully updated");
                        LoadStepsTable(strBCPTemplateID);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Record unable to be updated");
                    },
                });
            }
            
        }


    </script>

    <style>
        .tableStyle {
            font-family: Arial;
            font-size: 8pt;
        }

        .headerStyle {
            background-color: #ab1032;
            color: white;
        }

        .dksh {
            background-color: #ab1032;
            color: white;
            border-radius: 0px;
            font-family: Arial;
            font-size: 8pt;
        }

        @media (min-width: 992px) {
            .modal-add {
                max-width: 1200px;
            }
        }

        .drag-item {
            list-style-type: none;
            display: block;
            padding: 5px;
            border: 1px solid #ccc;
            margin: 2px;
            width: 260px;
            font-size: 10px;
            background: #fafafa;
            color: #444;
        }
    </style>
</head>
<body style="padding:10px">
    <h5>BCP</h5>
    <form id="form1" runat="server">
        <div>
            <table id="tblTemplate" class="table table-bordered tableStyle">
                <thead class="headerStyle">
                    <tr>
                        <td>Template ID</td>
                        <td>Template Name</td>
                        <td>Template Description</td>
                        <td>In Sequence</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <button id="btnCreate" class="btn btn-primary dksh" type="button" onclick="OpenTemplate('Create','','','')">Create</button>
        </div>
    </form>

    <div id="BCPTemplateModal" class="modal fade tableStyle" role="dialog">
        <div class="modal-dialog modal-lg modal-add">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #ab1032">
                    <h4 class="modal-title" style="font-size: 8pt; color: white">BCP Template</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                </div>
                <form id="BCPTemplateForm" class="needs-validation">
                    <div class="row modal-body" style="margin-top: 8px">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label for="lblBCPTemplateID" class="col-sm-2 col-form-label">Template ID</label>
                                <div class="col-sm-10">
                                    <label id="lblBCPTemplateID"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label for="txtBCPTemplateName" class="col-sm-2 col-form-label">Template Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control tableStyle" id="txtBCPTemplateName" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label for="txtBCPTemplateDesc" class="col-sm-2 col-form-label">Template Description</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control tableStyle" id="txtBCPTemplateDesc" />
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group row">
                                <label for="ddlInSeq" class="col-sm-2 col-form-label">In Sequence</label>
                                <div class="col-sm-10">
                                    <select id="ddlInSeq" class="form-control tableStyle">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row modal-footer">
                        <button id="btnSave" class="btn btn-primary dksh" type="button" style="display: none; margin-right: 3px" onclick="CreateTemplate()">Save</button>
                        <button id="btnUpdate" class="btn btn-primary dksh" type="button" style="display: none; margin-right: 3px" onclick="UpdateTemplate()">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div id="BCPStepModal" class="modal fade tableStyle" role="dialog">
        <div class="modal-dialog modal-lg modal-add">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #ebebeb">
                    <h4 class="modal-title" style="font-size: 8pt; color: black">BCP Steps</h4>
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 8pt; color: black">&times;</button>
                </div>
                <form id="BCPStepsForm" class="needs-validation">
                    <div class="row modal-body" style="margin-top: 8px">
                        <div class="col-lg-6">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label for="lblBCPTemplateID_Step" class="col-sm-2 col-form-label">Template ID</label>
                                    <div class="col-sm-10">
                                        <label id="lblBCPTemplateID_Step"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label for="lblBCPTemplateName_Step" class="col-sm-2 col-form-label">Template Name</label>
                                    <div class="col-sm-10">
                                        <label id="lblBCPTemplateName_Step"></label>
                                    </div>
                                </div>
                            </div>

                            <button id="btnCreateStep" class="btn btn-primary dksh pull-right" type="button" style="margin-right: 3px" onclick="ShowStepModuleDiv('Create','')">Create</button>
                            <br /><br />
                            <table id="tblBCPSteps" class="table table-bordered tableStyle">
                                <thead class="headerStyle">
                                    <tr>
                                        <td>Step No</td>
                                        <td>Step Name</td>
                                        <td>Step Description</td>
                                        <td>Mandatory</td>
                                        <td>Module List</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                        <div class="col-lg-6">
                            <div id="dvSteps" style="display: none">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="lblStepNo" class="col-sm-2 col-form-label">Step No</label>
                                        <div class="col-sm-10">
                                            <label id="lblStepNo"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="txtStepName" class="col-sm-2 col-form-label">Step Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control tableStyle" id="txtStepName" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="txtStepDesc" class="col-sm-2 col-form-label">Step Description</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control tableStyle" id="txtStepDesc" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="ddlMandatoryStep" class="col-sm-2 col-form-label">Mandatory Step</label>
                                        <div class="col-sm-10">
                                            <select id="ddlMandatoryStep" class="form-control tableStyle">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="ddlModule" class="col-sm-2 col-form-label">Module</label>
                                        <div class="col-sm-8">
                                            <select id="ddlModule" class="form-control tableStyle"></select>
                                        </div>
                                        <div class="col-sm-2">
                                            <button id="btnAddModule" class="btn btn-primary dksh" type="button" style="float:right; margin-right: 3px" onclick="AddModuleID()">Add</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="lblModuleList" class="col-sm-2 col-form-label">Module List</label>
                                        <div class="col-sm-10" style="border: 1px solid #ccc; width: 300px; height: auto; float: left; margin: 5px;">
                                            <ul id="ulModuleList" style="margin: 0; padding: 0; margin-left: 10px;"></ul>
                                        </div>
                                    </div>
                                </div>
                                <button id="btnSaveSteps" class="btn btn-primary dksh" type="button" style="float:right; display: none; margin-right: 3px" onclick="doSaveModule('Create')">Save</button>
                                <button id="btnUpdateSteps" class="btn btn-primary dksh" type="button" style="float:right; display: none; margin-right: 3px" onclick="doSaveModule('Update')">Update</button>
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>




</body>
</html>
