﻿Imports System.Xml
Imports System.IO
Imports System.ComponentModel
Imports cor_DB

Public Class clsCallToXML
    Public xmlDoc As XmlDocument          ' XML dcoument
    Private xmlElement As XmlElement  ' the current element

    Private _sbCoordinate As New System.Text.StringBuilder
    Public Property sbCoordinates() As System.Text.StringBuilder
        Get
            Return _sbCoordinate
        End Get
        Set(ByVal value As System.Text.StringBuilder)
            _sbCoordinate = value
        End Set
    End Property

#Region "Inner Properties"

    Private _defaultStyleName As String = "style570"
    Public Property StyleName() As String
        Get
            Return _defaultStyleName
        End Get
        Set(ByVal value As String)
            _defaultStyleName = value
        End Set
    End Property
    Private _salesrepCode As String
    Public Property SalesrepCode() As String
        Get
            Return _salesrepCode
        End Get
        Set(ByVal value As String)
            _salesrepCode = value
        End Set
    End Property
    Private _SalesreName As String
    Public Property SalesrepName() As String
        Get
            Return _SalesreName
        End Get
        Set(ByVal value As String)
            _SalesreName = value
        End Set
    End Property
    Private _CallDate As String
    Public Property CallDate() As String
        Get
            Return _CallDate
        End Get
        Set(ByVal value As String)
            _CallDate = value
        End Set
    End Property

#End Region
#Region "Main Information Tag"
    Public Sub addMainInforTag()
        Try
            addSalesrepInfo()
            addDefaultStyle()
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
    End Sub
    Private Sub addSalesrepInfo()
        addNewXmlElementWithValue("name", SalesrepName, xmlElement)
        addNewXmlElementWithValue("open", "1", xmlElement)
        addNewXmlElementWithValue("description", SalesrepCode & vbCrLf & CallDate, xmlElement)
    End Sub
    Private Sub addDefaultStyle()
        Dim _xmlStyle, _xmlIconStyle, _xmlIcon, _xmlLblStyle, _xmlBallonStyle As XmlElement
        'Main Style Tag
        _xmlStyle = addNewXmlElement("Style", xmlElement)
        _xmlStyle.SetAttribute("id", StyleName)
        'IconStyle
        _xmlIconStyle = addNewXmlElement("IconStyle", _xmlStyle)
        addNewXmlElementWithValue("color", "ff0000ff", _xmlIconStyle)
        _xmlIcon = addNewXmlElement("Icon", _xmlIconStyle)
        addNewXmlElementWithValue("href", "http://maps.google.com/mapfiles/kml/pal4/icon57.png", _xmlIcon)
        'LabelStyle
        _xmlLblStyle = addNewXmlElement("LabelStyle", _xmlStyle)
        addNewXmlElementWithValue("color", "ff0000ff", _xmlLblStyle)
        'BallonStyle
        _xmlBallonStyle = addNewXmlElement("BalloonStyle", _xmlStyle)
        addNewXmlElementWithXmlText("text", "<![CDATA[<p align=""left""><font size=""+1""><b>$[name]</b></font></p> <p align=""left"">$[description]</p>]]>", _xmlBallonStyle)
    End Sub
#End Region
#Region "TrackPoints"
    Public Sub addTrackPoints(ByRef drRows As DataRowCollection)
        Dim _xmlGroup, _xmlPlaceMark As XmlElement
        _xmlGroup = addNewXmlElement("Folder", xmlElement)
        addNewXmlElementWithValue("name", "TrackPoint", _xmlGroup)
        addNewXmlElementWithValue("open", "1", _xmlGroup)
        For Each drRow As DataRow In drRows
            _xmlPlaceMark = addNewXmlElement("Placemark", _xmlGroup)
            addPlaceMark(_xmlPlaceMark, drRow)
        Next

    End Sub

    Private Sub addPlaceMark(ByRef pxmlParent As XmlElement, ByRef drRow As DataRow)
        Dim _xmlPoint As XmlElement
        addNewXmlElementWithValue("name", GetValue(Of String)(drRow("CUST_NAME"), String.Empty), pxmlParent)
        addNewXmlElementWithValue("description", GetValue(Of String)(drRow("DESCRIPTION"), String.Empty), pxmlParent)
        addNewXmlElementWithValue("styleUrl", "#" & StyleName, pxmlParent)
        _xmlPoint = addNewXmlElement("Point", pxmlParent)
        addNewXmlElementWithValue("coordinates", GetValue(Of String)(drRow("COORDINATE"), String.Empty), _xmlPoint)
        sbCoordinates.Append(GetValue(Of String)(drRow("COORDINATE") & " ", String.Empty))
    End Sub
#End Region
#Region "TrackLines"
    Public Sub addTrackLine(ByVal strCoordinateString As String)
        Dim _xmlGroup, _xmlPlaceMark As XmlElement
        Dim _xmlLineSytle, _xmlLineString As XmlElement

        _xmlGroup = addNewXmlElement("Folder", xmlElement)
        addNewXmlElementWithValue("name", "TrackRoute", _xmlGroup)
        addNewXmlElementWithValue("open", "1", _xmlGroup)
        _xmlPlaceMark = addNewXmlElement("Placemark", _xmlGroup)
        addNewXmlElementWithXmlText("Name", String.Format("{0} - {1}_{2}", SalesrepName, SalesrepCode, CallDate), _xmlPlaceMark)
        addNewXmlElementWithXmlText("description", String.Format("{0} - {1}_{2}", SalesrepName, SalesrepCode, CallDate), _xmlPlaceMark)
        _xmlLineSytle = addNewXmlElement("LineStyle", addNewXmlElement("Style", _xmlPlaceMark))
        addNewXmlElementWithValue("color", "ffe600e6", _xmlLineSytle)
        addNewXmlElementWithValue("width", "2", _xmlLineSytle)
        _xmlLineString = addNewXmlElement("LineString", _xmlPlaceMark)
        addNewXmlElementWithValue("tessellate", "1", _xmlLineString)
        addNewXmlElementWithValue("coordinates", strCoordinateString, _xmlLineString)
    End Sub
#End Region
    Public Function generateKMLFile(ByVal strSalesrepCode As String, ByVal strCallDate As String) As String
        Dim dtRpt As DataTable = getCallByCustExport(strSalesrepCode, strCallDate)
        Dim strFilePath As String = String.Format("{0}{1}{2}_{3}_{4}.kml", System.AppDomain.CurrentDomain.BaseDirectory.ToString, "Documents\iFFMR\KML\", strSalesrepCode, CallDate, Guid.NewGuid.ToString.Substring(1, 5))
        Try
            addMainInforTag()
            addTrackPoints(dtRpt.Rows)
            addTrackLine(sbCoordinates.ToString)
            SaveKML(strFilePath)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
        Return strFilePath
    End Function

    Private Function getCallByCustExport(ByVal strSalesrepCode As String, ByVal strCallDate As String) As DataTable
        Dim clsObj As clsDB
        Dim dsObj As DataSet = Nothing
        Dim dtRpt As DataTable = Nothing
        Try
            clsObj = New clsDB
            With clsObj
                .ConnectionString = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
                .CmdText = "SPP_RPT_CALL_BY_CUST_EXPORT"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CALL_DATE", strCallDate, clsDB.DataType.DBString)
                dsObj = .spRetrieveDS()
                If dsObj.Tables.Count > 0 Then
                    dtRpt = dsObj.Tables(0)
                    If dsObj.Tables.Count > 1 AndAlso dsObj.Tables(1).Rows.Count > 0 Then getSalerepInfo(dsObj.Tables(1).Rows(0))
                End If

            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            clsObj = Nothing
        End Try
        Return dtRpt
    End Function
    Private Sub getSalerepInfo(ByRef drInfo As DataRow)
        If drInfo IsNot Nothing Then
            SalesrepCode = GetValue(Of String)(drInfo("SALESREP_CODE"), String.Empty)
            SalesrepName = GetValue(Of String)(drInfo("SALESREP_NAME"), String.Empty)
            CallDate = GetValue(Of String)(drInfo("CALL_DATE"), String.Empty)
        End If
    End Sub

#Region "Core Functions"
    Sub New()
        InitialiseClass()
    End Sub
    Sub New(ByVal strSalesrepCode As String, ByVal strSalesrepName As String, ByVal strCallDate As String)
        InitialiseClass()
        SalesrepCode = strSalesrepCode
        SalesrepName = strSalesrepName
        CallDate = strCallDate
    End Sub
    Private Sub InitialiseClass()
        Dim xmlRoot, xmlSubRoot As XmlElement
        Try
            xmlDoc = New XmlDocument
            xmlDoc.InsertBefore(xmlDoc.CreateXmlDeclaration("1.0", "utf-8", ""), xmlDoc.DocumentElement)

            xmlRoot = xmlDoc.CreateElement("kml")
            xmlRoot.SetAttribute("xmlns", "http://earth.google.com/kml/2.2")

            xmlSubRoot = xmlDoc.CreateElement("Document")
            With xmlSubRoot
                .SetAttribute("xmlns:gx", "http://www.google.com/kml/ext/2.2")
                .SetAttribute("xmlns:kml", "http://www.opengis.net/kml/2.2")
                .SetAttribute("xmlns:atom", "http://www.w3.org/2005/Atom")
            End With
            xmlRoot.AppendChild(xmlSubRoot)
            xmlDoc.AppendChild(xmlRoot)
            xmlElement = xmlSubRoot
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try

    End Sub
    Public Function addNewXmlElement(ByVal strElementName As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
        addNewXmlElement = xmlDoc.CreateElement(strElementName)
        If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(addNewXmlElement)
        Return addNewXmlElement
    End Function
    Public Function addNewXmlElementWithValue(ByVal strElementName As String, ByVal strElementValue As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
        Dim _xmlElement As XmlElement = xmlDoc.CreateElement(strElementName)
        If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(_xmlElement)
        If Not String.IsNullOrEmpty(strElementValue) Then _xmlElement.InnerText = strElementValue
        Return _xmlElement
    End Function
    Public Function addNewXmlElementWithXmlText(ByVal strElementName As String, ByVal strXmlText As String, Optional ByRef pXmlParent As XmlElement = Nothing) As XmlElement
        Dim _xmlElement As XmlElement = xmlDoc.CreateElement(strElementName)
        If pXmlParent IsNot Nothing Then pXmlParent.AppendChild(_xmlElement)
        If Not String.IsNullOrEmpty(strXmlText) Then _xmlElement.InnerXml = strXmlText
        Return _xmlElement
    End Function
    Private Sub SaveKML(ByVal strDest As String)
        If IO.Directory.Exists(IO.Directory.GetParent(strDest).FullName) = False Then IO.Directory.CreateDirectory(IO.Directory.GetParent(strDest).FullName)
        Try
            xmlDoc.Save(strDest)
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        End Try
    End Sub
    Private Shared Function GetValue(Of T)(ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing) As T
        Dim rtValue As T
        If IsDBNull(dbOri) = False Then
            rtValue = dbOri
        Else
            rtValue = dfValue
        End If
        Return rtValue
    End Function
#End Region
#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            Exit Sub
        End Sub
    End Class

    'Public Overloads Sub Dispose()
    '    Dispose(True)
    '    GC.SuppressFinalize(Me)
    'End Sub

    'Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
    '    If Not (Me.disposed) Then
    '        If (disposing) Then
    '            Components.Dispose()
    '        End If
    '        CloseHandle(handle)
    '        handle = IntPtr.Zero
    '    End If
    '    Me.disposed = True
    'End Sub

    '<System.Runtime.InteropServices.DllImport("Kernel32")> _
    ' Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    'End Function

    'Protected Overrides Sub Finalize()
    '    Dispose(False)
    'End Sub

    'Public Sub Close()
    '    Dispose()
    'End Sub

#End Region
End Class
