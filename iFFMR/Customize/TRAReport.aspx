﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAReport.aspx.vb" Inherits="iFFMR_Customize_TRAReport" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customControl" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TRA Report</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmTraReport" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <customControl:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true">
                                </customControl:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" >
                                <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                            <table width="98%" class="Bckgroundreport">
                                                <tr>
                                                    <td width="150px" align="left">
                                                        <asp:Label ID="lblSalesrepCode" CssClass=" cls_label" runat="server" Text="Field Force Code:" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:TextBox ID="txtSalesrepCode" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:Label ID="lblSalesOrgCode" CssClass=" cls_label" runat="server" Text="Sales Org. Code:" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:TextBox ID="txtSalesOrgCode" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                   
                                                </tr>
                                                <tr>
                                                    <td  align="left">
                                                        <asp:Label ID="lblSalesrepName" CssClass=" cls_label" runat="server" Text="Field Force Name:" />
                                                    </td>
                                                    <td  align="left"> 
                                                        <asp:TextBox ID="txtSalesrepname" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:Label ID="lblSalesTeamCode" CssClass=" cls_label" runat="server" Text="Sales Team Code:" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:TextBox ID="txtSalesTeamcode" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td  align="left">
                                                        <asp:Label ID="lblSalesAreaCode" CssClass=" cls_label" runat="server" Text="Sales Area Code:" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:TextBox ID="txtSalesAreaCode" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                   <td  align="left">
                                                        <asp:Label ID="lblPrdGrpName" CssClass=" cls_label" runat="server" Text="Product Group Name:" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:TextBox ID="txtPrdGrpName" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                     
                                                </tr>
                                                <tr>
                                                  <td  align="left">
                                                        <asp:Label ID="lblCustCode" CssClass=" cls_label" runat="server" Text="Customer Code:" />
                                                    </td>
                                                    <td  align="left">
                                                        <asp:TextBox ID="txtCustCode" CssClass=" cls_textbox" runat="server" Width="100px"
                                                            MaxLength="100" />
                                                    </td>
                                                

                                                      <td  align="left">
                                                        <asp:Label ID="lblTxnDate" CssClass=" cls_label" runat="server" Text="Txn Date:" />
                                                    </td>
                                                    <td  align="left">
                                                        <customToolkit:wuc_txtCalendarRange ID="txtDate" runat="server" RequiredValidation="true"
                                                            RequiredValidationGroup="Search" DateFormatString="yyyy-MM-dd" CompareDateRangeValidation="true" />
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td colspan="4"  align="left">
                                                        <asp:Button ID="btnsearch" runat="server" CssClass=" cls_button" Text="Search" ValidationGroup="Search" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr align="right">
                                                            <td>
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                            </td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="width: 98%">
                                                                <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 95%;">
                                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    ShowFooter="false" AllowPaging="true" PagerSettings-Visible="false">
                                                                    <EmptyDataTemplate>
                                                                        <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                    </EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td style="height: 5px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
