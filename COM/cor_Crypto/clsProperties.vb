' **********************************************************************************************************
'
'	Author	:	
'	Date	:	26-May-2005
'	Purpose	:	To declare the property
'
'	Revision	: 	
'	---------------------------------------------------------------------------------------------------------
'	|No	|Date Change	|Author		|Remarks				        |	
'	---------------------------------------------------------------------------------------------------------
'	|1	|	            |	        |	                            |
'	|2	|			    |		    |					            |
'	---------------------------------------------------------------------------------------------------------
'
' ***********************************************************************************************************

Option Explicit On 
Option Strict On

Public Class clsProperties
    ' Declare the property for clsCrypto
    Public Class clsCrypto
        Public DocTypeID As Long
        Public SiteID As Long
    End Class

    Public Class clsDecryption
        Public DocTypeID As Long
        Public SiteID As Long
    End Class

    Public Class clsEncryption
        Public DocTypeID As Long
        Public SiteID As Long
    End Class

End Class
