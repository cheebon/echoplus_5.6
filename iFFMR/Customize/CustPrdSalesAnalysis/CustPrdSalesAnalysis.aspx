﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustPrdSalesAnalysis.aspx.vb" Inherits="iFFMR_Customize_CustPrdSalesAnalysis_CustPrdSalesAnalysis" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>Customer Product Sales Analysis</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function resizeLayout2() { var dgList = $get('div_dgList'); if (dgList) { dgList.scrollTop = 0; dgList.style.height = Math.max((getFrameHeight('TopBarIframe') - 70), 0) + 'px'; } }
        window.onresize = function() { resizeLayout2(); }

        function ScrollTop() {
            var dgList = document.getElementById('div_dgList');
            dgList.scrollTop = 0;
        }
        
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('DetailBar');HideElement('ContentBar');MaximiseFrameHeight('TopBarIframe');resetSize('div_dgList','TopBarIframe');">
    <form id="fromCustPrdSalesAnalysis" runat="server">
       <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" align="left">
                    <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional"  RenderMode="Inline" >
                        <ContentTemplate>
                            <asp:Panel ID="pnlTreeMenuControl" runat="server" >
                                <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                             <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                <div style=" padding:5px 5px 5px 5px;">
                                                    <table >
                                                        <tr>
                                                            <td width="50px"><asp:label id="lblcust" cssclass=" cls_label" runat="server" Text="Customer:"/></td>
                                                            <td width="100px"><asp:RadioButtonList ID="rblSearchCustType" runat="server"  RepeatDirection="Horizontal" CssClass=" cls_radiobutton" >
                                                                    <asp:ListItem Selected="True"  Text="Code" Value="CUST_CODE"></asp:ListItem>
                                                                    <asp:ListItem Text="Name" Value="CUST_NAME"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td width="100px"><asp:TextBox ID="txtSearchCustValue" CssClass=" cls_textbox" runat="server" width="100px" MaxLength="100"></asp:TextBox></td>
                                                            <td> </td>
                                                       <%-- </tr>
                                                        <tr>--%>
                                                            <td><asp:label id="lblPrdGrp" cssclass=" cls_label" runat="server" Text="Product Group:"/></td>
                                                            <td colspan="2"><asp:DropDownList ID="ddlprdgrp" runat="server" CssClass=" cls_dropdownlist">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td> </td>
                                                        <%--</tr>
                                                        <tr>--%>
                                                            <td><asp:label id="lblprd" cssclass=" cls_label" runat="server" Text="Product:"/></td>
                                                            <td><asp:RadioButtonList ID="rblSearchPrdType" runat="server"  RepeatDirection="Horizontal" CssClass=" cls_radiobutton" >
                                                                    <asp:ListItem Selected="True"  Text="Code" Value="PRD_CODE"></asp:ListItem>
                                                                    <asp:ListItem Text="Name" Value="PRD_NAME"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                            <td width="100px"><asp:TextBox ID="txtSearchPrdValue" CssClass=" cls_textbox" runat="server" width="100px" MaxLength="100"></asp:TextBox></td>
                                                            <td ><asp:Button ID="btnsearch" runat="server" CssClass=" cls_button" Text="Search" /></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                </ContentTemplate> 
                                               </asp:UpdatePanel> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                                    <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Timer  id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                        AllowSorting="true" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                        GridHeight="455" RowSelectionEnabled="true">
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
