﻿
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCallAchievement
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallAchievement"
        End Get
    End Property


    Public Function GetCallAchievement(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strYear As String, ByVal strMonth As String, ByVal strTreeSalesrepList As String) As DataTable
        Dim clsCallAchievementDB As clsDB
        Try
            clsCallAchievementDB = New clsDB
            With clsCallAchievementDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_ACHIEVEMENT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strTreeSalesrepList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetAchievement :" & ex.Message))
        Finally
            clsCallAchievementDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class
