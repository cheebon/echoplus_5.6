Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports System.IO
Imports System.Text.RegularExpressions

Partial Class iFFMA_Utility_Upload_Itinerary
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "Itinerary Upload"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.UPLOADITINERARY)
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            dgList.Visible = False
            btnupload.Visible = False
        Else
        End If

        lblmsg.Text = ""


    End Sub

    Private Sub LoadFile(ByVal strFilePathName As String)

        ' call the parser 
        Dim dt As DataTable = ParseCSVFile(Server.MapPath(strFilePathName))

        Dim dtreturn As DataTable

        ' bind the resulting DataTable to a DataGrid Web Control 
        'dgList.DataSource = dt
        'dgList.DataBind()

        Dim clsUpload As New mst_Utility.clsUpload
        Dim strSalresRepCode As String, strCustCode As String, strContCode As String, strRouteCode As String

        clsUpload.ItineraryDelete() ' Clear temporary database first
        Try
            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item(0).ToString.ToUpper Like "*SALESREP*" OrElse dt.Rows(i).Item(0).ToString.ToUpper Like "*SALESMAN*" Then
                    'Skip header row if detected
                Else
                    strSalresRepCode = dt.Rows(i).Item(0)
                    strCustCode = dt.Rows(i).Item(1)
                    strContCode = dt.Rows(i).Item(2)
                    strRouteCode = dt.Rows(i).Item(3)
                    clsUpload.ItineraryUpload(strSalresRepCode, strCustCode, strContCode, strRouteCode) 'insert into temp table
                End If

            Next

            lblttlrecords.Text = "No of row(s) effected " + CStr(dt.Rows.Count)

            dtreturn = clsUpload.ItineraryValidate ' display validated data where data still in temp table
            dgList.DataSource = dtreturn
            dgList.DataBind()
            dgList.Visible = True
            lblttlrecords.Visible = True

            btnupload.Visible = True
        Catch
        End Try
    End Sub

    Public Function ParseCSV(ByVal inputString As String) As DataTable

        Dim dt As New DataTable()

        ' declare the Regular Expression that will match versus the input string 
        Dim re As New Regex("((?<field>[^"",\r\n]+)|""(?<field>([^""]|"""")+)"")(,|(?<rowbreak>\r\n|\n|$))")
        'Dim options As System.Text.RegularExpressions.RegexOptions = ((System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace Or System.Text.RegularExpressions.RegexOptions.Multiline) Or System.Text.RegularExpressions.RegexOptions.IgnoreCase)

        Dim colArray As New ArrayList()
        Dim rowArray As New ArrayList()

        Dim colCount As Integer = 0
        Dim maxColCount As Integer = 0
        Dim rowbreak As String = ""
        Dim field As String = ""

        Dim mc As MatchCollection = re.Matches(inputString)

        For Each m As Match In mc

            ' retrieve the field and replace two double-quotes with a single double-quote 
            field = m.Result("${field}").Replace("""""", """")
            'field = m.Result("${field}").Replace("\""\""", "\""")

            rowbreak = m.Result("${rowbreak}")

            If field.Length > 0 Then
                colArray.Add(field)
                colCount += 1
            End If

            If rowbreak.Length > 0 Then

                ' add the column array to the row Array List 
                rowArray.Add(colArray.ToArray())

                ' create a new Array List to hold the field values 
                colArray = New ArrayList()

                If colCount > maxColCount Then
                    maxColCount = colCount
                End If

                colCount = 0
            End If
        Next

        If rowbreak.Length = 0 Then
            ' this is executed when the last line doesn't 
            ' end with a line break 
            rowArray.Add(colArray.ToArray())
            If colCount > maxColCount Then
                maxColCount = colCount
            End If
        End If

        ' create the columns for the table 
        For i As Integer = 0 To maxColCount - 1
            dt.Columns.Add([String].Format("col{0:000}", i))
        Next

        ' convert the row Array List into an Array object for easier access 
        Dim ra As Array = rowArray.ToArray()
        For i As Integer = 0 To ra.Length - 1

            ' create a new DataRow 
            Dim dr As DataRow = dt.NewRow()

            ' convert the column Array List into an Array object for easier access 
            Dim ca As Array = DirectCast((ra.GetValue(i)), Array)

            ' add each field into the new DataRow 
            For j As Integer = 0 To ca.Length - 1
                dr(j) = ca.GetValue(j)
            Next

            ' add the new DataRow to the DataTable 
            dt.Rows.Add(dr)
        Next

        ' in case no data was parsed, create a single column 
        If dt.Columns.Count = 0 Then
            dt.Columns.Add("NoData")
        End If

        Return dt
    End Function

    Public Function ParseCSVFile(ByVal path As String) As DataTable

        Dim inputString As String = ""

        ' check that the file exists before opening it 
        If File.Exists(path) Then

            Dim sr As New StreamReader(path)
            inputString = sr.ReadToEnd()
            sr.Close()

        End If

        Return ParseCSV(inputString)
    End Function

    Protected Sub btnupload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupload.Click

        Dim clsUpload As New mst_Utility.clsUpload
        Dim dtreturn As DataTable
        dtreturn = clsUpload.ItineraryInsert
        dgList.DataSource = dtreturn
        dgList.DataBind()
        dgList.Visible = True
        lblttlrecords.Text = "No of row(s) effected " + CStr(dtreturn.Rows.Count)
        lblttlrecords.Visible = True
        btnupload.Visible = False
    End Sub

    Protected Sub btnvalidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnvalidate.Click
        If fulocation.HasFile Then
            Dim tmpExt As String = fulocation.FileName.Split(".", 3, StringSplitOptions.RemoveEmptyEntries)(1)

            Try
                If tmpExt.ToUpper = "CSV" Then

                    'Check to make sure it is the correct file type

                    ' alter path for your project
                    fulocation.SaveAs(Server.MapPath("~/Documents/CSVImport" + Session("userid") + "." + tmpExt))

                    lblmsg.Text = "Upload File Name: " & _
                         fulocation.PostedFile.FileName & "<br>" & _
                         "Type: " & _
                         fulocation.PostedFile.ContentType & _
                         " File Size: " & _
                         fulocation.PostedFile.ContentLength & " kb<br>"


                    If tmpExt.ToUpper = "CSV" Or tmpExt.ToUpper = "TXT" Then
                        LoadFile("~/Documents/CSVImport" + Session("userid") + "." + tmpExt)
                    End If



                Else
                    lblmsg.Text = "Kindly upload csv(csv) files only."
                    dgList.DataSource = Nothing 'objDataSet.Tables(0).DefaultView
                    dgList.DataBind()
                    dgList.Visible = False
                    lblttlrecords.Visible = False
                End If


            Catch ex As Exception
                lblmsg.Text = "Error: " & ex.ToString & Server.MapPath("~/Documents/CSVImport" + Session("userid") + "." + tmpExt)
            Finally
                Dim filePath = Server.MapPath("~/Documents/CSVImport" + Session("userid") + "." + tmpExt)
                System.IO.File.Delete(filePath)

            End Try
        Else
            lblmsg.Text = "Please select a file to upload."
            dgList.DataSource = Nothing 'objDataSet.Tables(0).DefaultView
            dgList.DataBind()
            dgList.Visible = False
            lblttlrecords.Visible = False
        End If
        fulocation.Dispose()

    End Sub
End Class
