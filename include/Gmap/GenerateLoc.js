﻿var geocoder;
var delay = 500;
var SuccessResult = 0;
var FailResult = 0;
var Timer;
var GenList=null;
var NewGenList = null;
var total = 0;
//var ddlBase = "#ddlBase";


function QueryLocation() {
    try {
     
        if ($.cookie("GenList") != null) {
            var GenList = JSON.parse($.cookie("GenList"));
            var GenListLength = $.cookie('GenListLength');
        }
        SuccessResult = 0;
        FailResult = 0;
        if (GenList == null) {
          
            ws_CustLoc.ReturnCustAdd(onSuccessGetAddress);
        }
        else {
            Timer =null;
            Timer = self.setInterval(function () { CountResult(SuccessResult) }, 2000);
            GenerateLatLongSelected(GenList, GenListLength);
        }
    }
    catch (e) {
        //$.msgBox({
        //    type: "error",
        //    content: "Error: " + e
        //});
    }
}

function onSuccessGetAddress(result) {
   
    if (result) {
        if (result.Total > 0) {
          
            Timer = null;
            Timer = self.setInterval(function () { CountResult(SuccessResult) }, 2000);
           
            GenerateLatLong(result);
        }
        else {
            ShowMessage("No Record Returned.");
        }
    }
}
function GenerateLatLong(result) {
  
    var count = 0;
    geocoder = new google.maps.Geocoder();
   
    for (var i = 0; i < result.Total; ++i) {
        // Check address
       
        if ((result.Rows[i].ADDRESS != undefined && result.Rows[i].ADDRESS != "" && result.Rows[i].CUST_CODE != undefined && result.Rows[i].CUST_CODE != "")) {
            var returnRequest = function () {
                requestRoute(i, result.Rows[i].CUST_CODE, result.Rows[i].ADDRESS);
            };
            returnRequest();
        }
        else {
            ShowMessage("Certain customers does not have coordinates information.");
        }
    }
}

function GenerateLatLongSelected(result, GenListLength) {
    var count = 0;
    geocoder = new google.maps.Geocoder();
    for (var i = 0; i < GenListLength; ++i) {
        // Check address
          
        if ((result[i].ADDRESS != undefined && result[i].ADDRESS != "" && result[i].CUST_CODE != undefined && result[i].CUST_CODE != "")) {
            var returnRequest = function () {
                requestRoute(i, result[i].CUST_CODE, result[i].ADDRESS);
            };
            returnRequest();
        }
        else {
            ShowMessage("Certain customers does not have coordinates information.");
        }
    }
}
function requestRoute(index, cust_code, address) {
    //var text;
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            //text = index + ' ' + cust_code + ' ' + results[0].geometry.location + results[0].geometry.location.lat() + results[0].geometry.location.lng();
            ws_CustLoc.GenerateCustLoc(cust_code, results[0].geometry.location.lat(), results[0].geometry.location.lng(), results[0].geometry.location_type);
            SuccessResult++;
        }
        else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            setTimeout(function () {
                requestRoute(index, cust_code, address);
            }, delay * 2);
        }
        else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
            ws_CustLoc.GenerateCustLocUndefined(cust_code);
            FailResult++;
        }
        else {
            ws_CustLoc.GenerateCustLocUndefined(cust_code);
            FailResult++;
        }
    });
}
function msgtest(SuccessResult)
{
    var divobj = document.getElementById('divCountResult');
    $('#divCountResult').html(SuccessResult);
    divobj.style.visibility = "visible";
}
function CountResult(SuccessResult) {
    total = 0;
    total =parseFloat(FailResult) + parseFloat(SuccessResult);
    var divobj = document.getElementById('divCountResult');
    $('#divCountResult').html(SuccessResult + " records are success generated" + "<br/>" + FailResult + " records failed to get coordinate based on the address given." + "<br/>Total " + total);
    divobj.style.visibility = "visible";

    //QueryLocation() each time get 1000 records from database. Recall the SP if the records more than 1000
    if (total % 1000 == 0) {
        QueryLocation();
    }
}

var divisShow = true;

jQuery(document).ready(function () {
    $.removeCookie('GenList', { path: '/' });
    $.removeCookie('GenListLength', { path: '/' });
   init();
 
});
function init() {
    //getDDLBASE();
    $("input:checkbox").click(function () {
    
        if ($(this).is(":checked")) {
            var ID = this.id;
            if (ID.indexOf("chkAll") >= 0) {
            }
            else {
                ID = ID.replace("chkSelected", "");

                var lblFullAddress = document.getElementById(ID + 'lblFullAddress');
                var lblCustCode = document.getElementById(ID + 'lblCustCode');

                if (GenList == null) {

                    GenList = [{ CUST_CODE: lblCustCode.innerHTML, ADDRESS: lblFullAddress.innerHTML }];
                } else {

                    GenList.push({ CUST_CODE: lblCustCode.innerHTML, ADDRESS: lblFullAddress.innerHTML });
                }
            }
          
        } else {
            var ID = this.id;
            if (ID.indexOf("chkAll") >= 0) {
            }
            else {
                ID = ID.replace("chkSelected", "");
                var lblFullAddress = document.getElementById(ID + 'lblFullAddress');
                var lblCustCode = document.getElementById(ID + 'lblCustCode');

                if (GenList == null) {
                }
                else {

                    for (var i = 0; i < GenList.length ; i++) {
                        if (GenList[i].CUST_CODE != lblCustCode.innerHTML && GenList[i].ADDRESS != lblFullAddress.innerHTML) {
                            if (NewGenList == null) {
                                NewGenList = [{ CUST_CODE: GenList[i].CUST_CODE, ADDRESS: GenList[i].ADDRESS }];
                            } else {
                                var ResultTotal = '0';
                                ResultTotal = (parseInt(NewGenList.length) + 1).toString;
                                NewGenList.push({ CUST_CODE: GenList[i].CUST_CODE, ADDRESS: GenList[i].ADDRESS });
                            }
                        }
                    }
                    if (NewGenList != null) {
                        GenList = NewGenList;
                    }
                }
            }
        }
        if (ID.indexOf("chkAll") >= 0) {
        }
        else {
            $.removeCookie('GenList', { path: '/' });
            $.removeCookie('GenListLength', { path: '/' });
            $.cookie("GenList", JSON.stringify(GenList), { path: '/' });
            $.cookie("GenListLength", GenList.length, { path: '/' });
        }
      
    });

}


