<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProductGrid.aspx.vb" Inherits="iFFMS_Product_Product" %>

<%@ Register TagPrefix="uc1" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Product List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
  function SetScrollPosition()
    {
    var dgListQues = document.getElementById('div_DGDPRDLIST');
    var scrollPosition = dgListQues.scrollTop;
    var hdfScrollPosition = document.getElementById('hdfScrollPosition');
    //alert(scrollPosition);
    if (hdfScrollPosition){
    hdfScrollPosition.value = scrollPosition;}
    dgListQues.scrollTop = 0;
    }
    function GetScrollPosition()
    {
    var hdfScrollPosition = document.getElementById('hdfScrollPosition');
    if (hdfScrollPosition){
    var scrollPosition = hdfScrollPosition.value; }
    var dgListQues = document.getElementById('div_DGDPRDLIST');
    //alert(scrollPosition);
    if (scrollPosition){
    dgListQues.scrollTop = parseInt(scrollPosition);}
    }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ResizeFrameWidth('TopBar','100%');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');">
    <form id="frmproductlist" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <asp:UpdatePanel runat="server" ID="UpdatePage" UpdateMode="Conditional">
                <ContentTemplate>
                      <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </div>
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                            width="100%" border="0" style="height: 30px">
                            <tr align="left" valign="bottom">
                                <td>
                                    <asp:Image ID="imgExpandCollapse" ImageUrl="~/images/ico_Field.gif" runat="server"
                                        CssClass="cls_button" ToolTip="Criteria Selection" EnableViewState="false" />
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Panel ID="pSearchCriteria" Width="100%" runat="server" CssClass="cls_ctrl_panel">
                                                <table class="cls_panel" cellspacing="0" border="0" cellpadding="1" width="100%">
                                                    <tr align="left" valign="bottom">
                                                        <td valign="top">
                                                            <span class="cls_label_header">Search By</span></td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlsearchby" runat="server" CssClass="cls_dropdownlist" Width="144px">
                                                                <asp:ListItem Text="All" Value="ALL" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Product Name" Value="PRD_NAME"></asp:ListItem>
                                                                <asp:ListItem Text="Product Code" Value="PRD_CODE"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr align="left" valign="bottom">
                                                        <td valign="top" width="10%">
                                                            <span class="cls_label_header">Search Value</span></td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:TextBox ID="txtsearch" runat="server" CssClass="cls_textbox"> </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr align="left" valign="bottom">
                                                        <td valign="top" width="10%">
                                                            <span class="cls_label_header">Warehouse</span></td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="DDLWAREHOUSE" runat="server" AutoPostBack="True" Width="216px"
                                                                CssClass="cls_dropdownlist">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr align="left" valign="bottom">
                                                        <td valign="top" width="10%">
                                                            <span class="cls_label_header">Product Group</span></td>
                                                        <td valign="top">
                                                            <span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="DDLPRDGRP" runat="server" AutoPostBack="True" Width="216px"
                                                                CssClass="cls_dropdownlist">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr align="left" valign="bottom">
                                                        <td>
                                                            <asp:Button ID="btnprd_Refresh" CssClass="cls_button" runat="server" Text="Search"
                                                                ValidationGroup="PrdSearch"></asp:Button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CPESearchCriteria" runat="server" TargetControlID="pSearchCriteria"
                                                ExpandControlID="imgExpandCollapse" CollapseControlID="imgExpandCollapse" Collapsed="false">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="updateDG" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                            </asp:Timer>
                                            <div style="padding-top: 10px; padding-left: 10px; width: 98%">
                                                <ccGV:clsGridView ID="DGDPRDLIST" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                    Width="98%" FreezeHeader="True" GridHeight="370" AddEmptyHeaders="0" CellPadding="2"
                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                    ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="PRD_CODE">
                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                    <EmptyDataTemplate>
                                                        There is no data to display.
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <%--<asp:CommandField HeaderText="View Detials" ShowSelectButton="True" ShowHeader="True"
                                                            Selecttext="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:CommandField>--%>
                                                        <asp:BoundField DataField="PRD_GRP_CODE" HeaderText="Product Group Code" SORTEXPRESSION="PRD_GRP_CODE">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PRD_GRP_NAME" HeaderText="Product Group Name" SORTEXPRESSION="PRD_GRP_NAME">
                                                            <itemstyle horizontalalign="Left" />
                                                        </asp:BoundField>
                                                        <%--  <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" SORTEXPRESSION="PRD_CODE">
                                                            <itemstyle horizontalalign="Center" />
                                                        </asp:BoundField>--%>
                                                        <asp:HyperLinkField DataNavigateUrlFields="PRD_CODE" DataNavigateUrlFormatString="#"
                                                            DataTextField="PRD_CODE" HeaderText="Product Code" SortExpression="PRD_CODE">
                                                        <itemstyle horizontalalign="Center" />    
                                                        </asp:HyperLinkField>
                                                        <asp:BoundField DataField="PRD_NAME" HeaderText="Product Name" SORTEXPRESSION="PRD_NAME">
                                                            <itemstyle horizontalalign="left" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="STK_LEVEL" HeaderText="Stock Level" SORTEXPRESSION="STK_LEVEL">
                                                            <itemstyle horizontalalign="Right" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />
                                                </ccGV:clsGridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div> <asp:HiddenField ID="hdfScrollPosition" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
