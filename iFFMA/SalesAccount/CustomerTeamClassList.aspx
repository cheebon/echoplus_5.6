﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerTeamClassList.aspx.vb" Inherits="iFFMA_SalesAccount_CustomerTeamClassList" %>

<%@ Register TagPrefix="wuctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="wuclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="wuclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="wucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="wucCustomerTeamClassPop" TagName="wuc_CustomerTeamClassPop" Src="CustomerTeamClassPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Team Class List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../scripts/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="Javascript">

        var dgListIndex = [];

        function SelectRow(chkSelected, rowIndex) {
            if (chkSelected.checked == false) {

                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    var e = document.forms[0].elements[i];

                    if (e.type == 'checkbox') {
                        e.checked = chkSelected.checked;
                        break;
                    }
                }
                var arrayIndex = $.inArray(rowIndex, dgListIndex);
                if (arrayIndex > -1)
                    dgListIndex.splice(arrayIndex, 1);
            } else {
                dgListIndex.push(rowIndex);
            }
        }

        function SelectAllRows(chkAll, rowCount) {
            dgListIndex = [];
            if (chkAll.checked == true) {
                for (i = 0; i < rowCount; i++)
                    dgListIndex.push(i);
            }

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox') {
                    e.checked = chkAll.checked;
                }
            }
        }

        function checkDelete() {
            var isChecked = false;

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox' && e.checked) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                if (!confirm('Are you sure want to delete?')) {
                    dgListIndex = []; //Reset array
                    return false;
                }
            }
            else {
                alert('Please select at least one of the record to delete!');
                dgListIndex = []; //Reset array
                return false;
            }
            document.getElementById('hdDgListIndex').value = dgListIndex;
            dgListIndex = []; //Reset array
            return true;
        }
    </script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesAccountList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <wuclblInfo:wuc_lblInfo ID="lblErr" runat="server" />
                                    <wuclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <wuctoolbar:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                        <asp:Button ID="btnDelete" CssClass="cls_button" runat="server" Text="Delete" OnClientClick="if (!checkDelete()){return false; }" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <wucdgpaging:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0"
                                                                                GridWidth="" AllowPaging="True" DataKeyNames="CUST_CODE, CONT_CODE, TYPE, TEAM_CLASS" BorderColor="Black"
                                                                                BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px"
                                                                                RowHighlightColor="AntiqueWhite">
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                            
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>

                            <tr class="Bckgroundreport">
                                <td style="height: 5px"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <wucCustomerTeamClassPop:wuc_CustomerTeamClassPop ID="wuc_CustomerTeamClassPop" Title="Customer Team Class Assignment" runat="server" />
        <asp:HiddenField runat="server" ID="hdDgListIndex" />
    </form>
</body>
</html>
