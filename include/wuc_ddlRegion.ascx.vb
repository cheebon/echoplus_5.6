'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Region User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlRegion
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlRegionID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    Public Property SelectedValue() As Long
        Get
            If ddlRegionID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return Long.Parse(ddlRegionID.SelectedValue)
        End Get
        Set(ByVal Value As Long)
            Dim checkExist As ListItem
            checkExist = ddlRegionID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlRegionID.SelectedValue = "0"
            Else
                ddlRegionID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlRegionID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlRegionID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlRegionID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlRegionID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlRegionID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlRegionID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlRegionID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlRegionID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlRegionID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvRegionID.Visible 'cfvRegionID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvRegionID.Enabled = Value
            cfvRegionID.Visible = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            If ddlRegionID.Items.Count <= 0 Then
                'Get Region List
                objCommonQuery = New cor_Common.clsCommonQuery
                With objCommonQuery
                    '    .clsProperties.RegionID = lngRegionID
                    '    .clsProperties.DocTypeID = lngDocTypeID
                    'dt = .GetRegionList()
                End With
                objCommonQuery = Nothing

                'ddlRegionID.Items.Clear()
                ddlRegionID.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlRegionID.Items.Add(New ListItem(Trim(drRow("region_name")), Trim(drRow("region_id")) & "@" & Trim(drRow("region_code"))))
                    Next
                End If
            End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlRegion.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


