'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/11/2006
'	Purpose	    :	Class AccessRightDtl 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports adm_AccessRight

'Interface Icor_AccessRightDtl
'    Function GetTemplateList(ByVal AccessRightDtlID As Long) As DataTable
'End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
<Serializable()> _
Public Class clsAccessRightDtl
    'Inherits ServicedComponent
    'Implements Icor_AccessRightDtl
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New clsProperties.clsAccessRightDtl

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Create
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Create()
        Dim objDB As cor_DB.clsDB
        'Dim lngAccessRightDtlID As Long

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTDTLCREATE"
                '.addItem("accessrightdtl_id", clsProperties.accessrightdtl_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("module_id", clsProperties.module_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("submodule_id", clsProperties.submodule_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("action_id", clsProperties.action_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                'lngAccessRightDtlID = .spInsert()
                .spInsert()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRightDtl.clsAccessRightDtl.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    'New function for new access right edit page
    Public Sub Alter(ByVal _view As String, ByVal _create As String, ByVal _edit As String, ByVal _delete As String)
        Dim objDB As cor_DB.clsDB
        'Dim lngAccessRightDtlID As Long

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTDTL_ALTER"
                '.addItem("accessrightdtl_id", clsProperties.accessrightdtl_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("module_id", clsProperties.module_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("submodule_id", clsProperties.submodule_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("view_flag", _view, cor_DB.clsDB.DataType.DBInt)
                .addItem("create_flag", _create, cor_DB.clsDB.DataType.DBInt)
                .addItem("edit_flag", _edit, cor_DB.clsDB.DataType.DBInt)
                .addItem("delete_flag", _delete, cor_DB.clsDB.DataType.DBInt)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                'lngAccessRightDtlID = .spInsert()
                .spInsert()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRightDtl.clsAccessRightDtl.Alter" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Update()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTDTLUPDATE"
                .addItem("accessrightdtl_id", clsProperties.accessrightdtl_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("module_id", clsProperties.module_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("submodule_id", clsProperties.submodule_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("action_id", clsProperties.action_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRightDtl.clsAccessRightDtl.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Delete()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ACCESSRIGHTDTLDELETE"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRightDtl.clsAccessRightDtl.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    'PHL:20070903**************************************
    Public Sub UpdateDFAccessRight(ByVal strDFPrincipalID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_DFACCESSRIGHT"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("df_principal_id", strDFPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRightDtl.clsAccessRightDtl.UpdateDFAccessRight" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    '**************************************************

    Public Sub UpdateELAccessRight(ByVal strDFPrincipalID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("el_id", strDFPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRightDtl.clsAccessRightDtl.UpdateDFAccessRight" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.AccessRightDtlID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
