<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContactList.aspx.vb" Inherits="iFFMA_Contact_ContactList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ContactAssignList" Src="ContactAssignList.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Contact List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
  
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" > 
    <form id="frmContactList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                            
                                            <%--<span style="float:left; width:100%;">--%>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="CONT_CODE">
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                             </table>
                                                        </asp:Panel> 
                                                        
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Contact Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:Label ID="lblContCode" runat="server" Text="[Auto Generate]" Font-Italic="true"  CssClass="cls_label"></asp:Label>
                                                                                <asp:TextBox ID="txtContCode" runat="server" CssClass="cls_textbox" MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvContCode" runat="server" ControlToValidate="txtContCode"
                                                                                    ErrorMessage="Contact Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                    <asp:CheckBox ID="chkContCodeManual" runat="server"  Text="Manual" CssClass="cls_checkbox"  AutoPostBack="true"/> 
                                                                                 </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Contact Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtContName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" />
                                                                                <asp:RequiredFieldValidator ID="rfvtxtContName" runat="server" ControlToValidate="txtContName"
                                                                                    ErrorMessage="Contact Name is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Status</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td> 
                                                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                                                                        <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Title</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtTitle" runat="server" CssClass="cls_textbox" MaxLength="100" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Last Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtLastName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">First Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtFirstName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Christian Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtChristianName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Gender</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlGender" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="">-- SELECT --</asp:ListItem>
                                                                                        <asp:ListItem Value="M">Male</asp:ListItem>
                                                                                        <asp:ListItem Value="F">Female</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">D.O.B</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtBirthDate" runat="server" CssClass="cls_textbox" />
                                                                                    <asp:comparevalidator ID="cv_BirthDate" runat="server" ControlToValidate="txtBirthDate" type="date" Operator="DataTypeCheck" 
                                                                                        ErrorMessage="Invalid Date" ValidationGroup="Save" Display="Dynamic" CssClass="cls_validator" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Marital Status</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlMaritalStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="">-- SELECT --</asp:ListItem>
                                                                                        <asp:ListItem Value="S">Single</asp:ListItem>
                                                                                        <asp:ListItem Value="M">Married</asp:ListItem>
                                                                                        <asp:ListItem Value="O">Others</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Medical School</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtMedSchool" runat="server" CssClass="cls_textbox" MaxLength="50" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Graduation Date</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtGradDate" runat="server" CssClass="cls_textbox" />
                                                                                    <asp:comparevalidator ID="cv_GradDate" runat="server" ControlToValidate="txtGradDate" type="date" Operator="DataTypeCheck" 
                                                                                        ErrorMessage="Invalid Date" ValidationGroup="Save" Display="Dynamic" CssClass="cls_validator" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Email</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtEmail" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Tel No. 1</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtTelNo1" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Tel No. 2</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtTelNo2" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                            </tr>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel> 
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_ContactAssignList ID="wuc_ContAcctList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_ContactAssignList ID="wuc_ContRepList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                                       
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table> 
       
    </form>
</body>
</html>
