<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StockTransferList.aspx.vb" Inherits="iFFMA_Services_StockTransferList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_StockTransferDtl" Src="StockTransferDtl.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Confirm Stock List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script type="text/javascript" language="Javascript">
    	function SelectRow(chkSelected)
    	{
            //var chkSelected = window.event.srcElement;
            if (chkSelected.checked == false)
            {
                /*
                if (window.document.getElementById('tcResult$TabPanel1$dglist$ctl01$chkSelectAll') != null)
                {
                    window.document.getElementById('tcResult$TabPanel1$dglist$ctl01$chkSelectAll').checked = false
                }
                */
                for (var i=0; i < document.forms[0].elements.length; i++) 
                {
                    var e = document.forms[0].elements[i];
                                            
                    if (e.type == 'checkbox')
                    {
                        e.checked = chkSelected.checked;
                        break;
                    }
                }
            }
   	    }
    	  	
    	function SelectAllRows(chkAll)
    	{
    	    //var chkAll = window.event.srcElement; 
    	    
    	    for (var i=0; i < document.forms[0].elements.length; i++) 
            {
                var e = document.forms[0].elements[i];
                
                if (e.type == 'checkbox')
                {
                    e.checked = chkAll.checked;
                }
            }
        }
        
        function checkSelected()
        {
            var isChecked = false;
            
            for (var i=0; i < document.forms[0].elements.length; i++) 
            {
                var e = document.forms[0].elements[i];
                
                if (e.type == 'checkbox' && e.checked)
                {
                    isChecked = true;
                    break;
                }
            }
        
            if (isChecked)
            {
                if (!confirm('Are you sure want to move the selected record(s) to archive?'))
                {
                    //window.event.returnValue = false; 
                    return false;
                }
            }
            else
            {
                alert('Please select at least one of the record!');
                //window.event.returnValue = false; 
                return false;
            }
            
            return true;
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmStockTransferList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                                  
                                            <%--<span style="float:left; width:100%;">--%>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnArchiveSelected" CssClass="cls_button" runat="server" Text="Move to Archive" OnClientClick="if (!checkSelected()){return false; }"/>    
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="TXN_NO">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <itemtemplate>
                                                                                            <%--<asp:checkbox ID="chkSelect" runat="server" class="cls_checkbox"/>--%>
                                                                                        </itemtemplate> 
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Panel ID="pnlBtnArchive" runat="server" >
                                                                            <asp:Button ID="btnArchive" runat="server" CssClass="cls_button" Text="Move to Archive" OnClientClick="if (!confirm('Are you sure want to move the selected record(s) to archive?')){return false; }" /><br />
                                                                        </asp:Panel>
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_StockTransferDtl ID="wuc_StockTransferDtl" runat="server" />
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>                                                            
                                                            <asp:HiddenField ID="hdTxnNo" runat="server" Value="" />
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>                                                      
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        
        <customToolkit:wuc_lblMsgPop ID="wuc_lblMsgPop" Title="Message" runat="server" /> 
    </form>
</body>
</html>
