<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ProductDetails.aspx.vb"
    Inherits="iFFMS_Product_ProductDetails" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Product Detail</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
     <script language="javascript" type="text/javascript">
     function Refresh()
    {
    var ifra = self.parent.document.getElementById('TopBarIframe');
   ifra.contentWindow.GetScrollPosition();
    }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmproductdetail" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <asp:UpdatePanel runat="server" ID="UpdatePage" RenderMode="Block" UpdateMode="Conditional">
                <ContentTemplate>
                 <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <uc1:wuc_lblHeader ID="wuc_lblheader" runat="server"></uc1:wuc_lblHeader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </div>
                    <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                        margin: 0;">
                        <input type="button" runat="server" id="btnback" value="<< Back" 
                        onclick="HideElement('ContentBar');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');Refresh();"
                            class="cls_button" style="width: 80px" />
                    </div>
                    <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                        float: left; margin: 0;">
                        <div style="width: 800px;">
                            <span style="float: left; width: 400px">
                                <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                    Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                    <RowStyle CssClass="cls_DV_Row_MST" />
                                    <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                    <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                        Width="35%" Wrap="False" />
                                </asp:DetailsView>
                            </span><span style="float: left; width: 400px">
                                <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                    Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                    <RowStyle CssClass="cls_DV_Row_MST" />
                                    <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                    <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                        Width="35%" Wrap="False" />
                                </asp:DetailsView>
                            </span>
                        </div>
                    </div>
                    <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                        float: left; margin: 0;">
                        <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                            ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                            <ajaxToolkit:TabPanel ID="TabPanelUOM" runat="server" HeaderText="Product UOM">
                                <ContentTemplate>
                                    <asp:UpdatePanel runat="server" ID="UpdateUOM" RenderMode="Block" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                <ccGV:clsGridView ID="dgListUOM" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                    Width="98%" FreezeHeader="True" GridHeight="340" AddEmptyHeaders="0" CellPadding="2"
                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                    ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                </ccGV:clsGridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanelEAN" runat="server" HeaderText="Product EAN">
                                <ContentTemplate>
                                    <asp:UpdatePanel runat="server" ID="UpdateEAN" RenderMode="Block" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                <ccGV:clsGridView ID="dgListEAN" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                    Width="98%" FreezeHeader="True" GridHeight="340" AddEmptyHeaders="0" CellPadding="2"
                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                    ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                </ccGV:clsGridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanelBatch" runat="server" HeaderText="Product Batch">
                                <ContentTemplate>
                                    <asp:UpdatePanel runat="server" ID="UpdateBatch" RenderMode="Block" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                <ccGV:clsGridView ID="dgListBatch" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                    Width="98%" FreezeHeader="True" GridHeight="340" AddEmptyHeaders="0" CellPadding="2"
                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                    ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                </ccGV:clsGridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanelSTDBONUS" runat="server" HeaderText="Product Std. Bonus">
                                <ContentTemplate>
                                    <asp:UpdatePanel runat="server" ID="UpdateSTDBONUS" RenderMode="Block" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                <ccGV:clsGridView ID="dgListSTDBONUS" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                    Width="98%" FreezeHeader="True" GridHeight="340" AddEmptyHeaders="0" CellPadding="2"
                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                    ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                </ccGV:clsGridView>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>
                    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
