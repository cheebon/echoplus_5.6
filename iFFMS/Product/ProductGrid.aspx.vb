Imports System.Data
Partial Class iFFMS_Product_Product
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "Product_Product"
        End Get
    End Property
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Session("dflSalesRepCode") = "100610"
        'Session("dflCountryCode") = "SG"
        Try
            If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()
            'Call Header
            With wuc_lblheader
                .Title = "Product Listing"
                .DataBind()
                .Visible = True
            End With

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "setTimeout", "setTimeout('GetScrollPosition();',100);", True)
            'Call Panel
            'With Wuc_ctrlpanel
            '    .SubModuleID = 24
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
                BindWarehouseDLL()
                BindPrdGrpDLL()
                'TimerControl1.Enabled = True

            End If



        Catch ex As Exception
            ExceptionMsg("ProductList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        If DDLWAREHOUSE.Items(0).Text = "None" Or DDLPRDGRP.Items(0).Text = "None" Then
            dtCurrenttable = GetRecList(Nothing, Nothing)
        Else
            dtCurrenttable = GetRecList(DDLWAREHOUSE.SelectedItem.Value, DDLPRDGRP.SelectedItem.Value)
        End If

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        DGDPRDLIST.DataSource = dvCurrentView
        DGDPRDLIST.DataBind()

        updateDG.Update()
        UpdatePage.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Private Function GetRecList(ByVal strWarehousecode As String, ByVal strPrdGrpCode As String) As Data.DataTable

        Dim DT As New DataTable
        Dim obj As New txn_Product.clsProduct

        Dim strSalesrepCode As String = Trim(Session("SALESREP_CODE"))
        Dim strSearchBy As String = Trim(ddlsearchby.SelectedValue)
        Dim strSearchValue As String = Trim(txtsearch.Text)

        Try
            DT = obj.productListing(strWarehousecode, strPrdGrpCode, strSalesrepCode, strSearchBy, strSearchValue)
            If DT.Rows.Count = 0 Then
                'DT.Rows.Add(DT.NewRow)
            End If

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub DGDPRDLIST_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles DGDPRDLIST.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub

    Sub BindWarehouseDLL()

        Dim obj As New txn_Product.clsProduct
        Dim dt As DataTable = Nothing

        Try
            dt = obj.salesmanWarehouse(Trim(Session("SALESREP_CODE")))
            If dt.Rows.Count > 0 Then

                DDLWAREHOUSE.DataSource = dt
                DDLWAREHOUSE.DataValueField = "whs_cd"
                DDLWAREHOUSE.DataTextField = "whs_desc"
                DDLWAREHOUSE.DataBind()
                DDLWAREHOUSE.Items.Insert(0, "ALL")
            Else
                DDLWAREHOUSE.Items.Insert(0, "None")
                DDLWAREHOUSE.Items.Insert(0, "ALL")
            End If

            UpdateSearch.Update()
        Catch ex As Exception

        End Try

        'Session("Warehouse") = dt
    End Sub

    Sub BindPrdGrpDLL()

        Dim obj As New txn_Product.clsProduct
        Dim dt As DataTable = Nothing

        Try
            dt = obj.salesmanProductGroup(Trim(Session("SALESREP_CODE")))
            If dt.Rows.Count > 0 Then
                DDLPRDGRP.DataSource = dt
                DDLPRDGRP.DataValueField = "prod_grp"
                DDLPRDGRP.DataTextField = "prod_desc"
                DDLPRDGRP.DataBind()
                DDLPRDGRP.Items.Insert(0, "ALL")
            Else
                DDLPRDGRP.Items.Insert(0, "None")
            End If

            UpdateSearch.Update()
        Catch ex As Exception

        End Try



        'Session("Prdgrp") = dt
    End Sub

    Protected Sub DDLPRDGRP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLPRDGRP.SelectedIndexChanged
        '  TimerControl1.Enabled = True

    End Sub

    Protected Sub DDLWAREHOUSE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLWAREHOUSE.SelectedIndexChanged
        '   TimerControl1.Enabled = True
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDatabinding()
            TimerControl1.Enabled = False

        End If

    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Protected Sub DGDPRDLIST_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGDPRDLIST.SelectedIndexChanged
        'Dim strPrdCode As String
        'strPrdCode = DGDPRDLIST.SelectedDataKey.Item("PRD_CODE")

        'Dim StrScript As String
        'StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Product/ProductDetails.aspx?PrdCode=" + strPrdCode + "';"
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "HideTopShowDetail", "HideElement('TopBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe')", True)
        'UpdatePage.Update()
    End Sub

    Protected Sub DGDPRDLIST_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles DGDPRDLIST.RowCreated

        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim strPrdCode = DGDPRDLIST.DataKeys(e.Row.RowIndex).Item("PRD_CODE")

                Dim str3 As String '2 is the column to add attributes the link
                str3 = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Product/ProductDetails.aspx?PrdCode=" + strPrdCode + "';"
                Dim strAction As String
                strAction = "HideElement('TopBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe');"
                Dim strPosition As String = "SetScrollPosition();"
                e.Row.Cells(2).Attributes.Add("onclick", str3 + strAction + strPosition)
                e.Row.Cells(2).Attributes.Add("style", "cursor:pointer;")
                e.Row.Attributes.Add("onclick", "javascript:__doPostBack('DGDPRDLIST','Select$" & e.Row.RowIndex & "')")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnprd_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprd_Refresh.Click
        TimerControl1.Enabled = True
    End Sub
End Class

