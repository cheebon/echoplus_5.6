'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	09/04/2007
'	Purpose	    :	Class to build Collection Report from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCollDailyActy
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCOLLDailyActy"
        End Get
    End Property

#Region "Collection"
    'EXEC SPP_RPT_COLL_DAILY_ACTY_DTL_TXNNO @USER_ID, @PRINCIPAL_ID,@PRINCIPAL_CODE, @SALESREP_CODE, @VISIT, @CUSTCODE, @TXNDATE 
    Public Function GET_COLL_DAILY_ACTY_HDR_TXNNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_COLL_DAILY_ACTY_DTL_TXNNO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_COLL_DAILY_ACTY_HDR_TXNNO :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_COLL_DAILY_ACTY_HDR @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @SALESREP_CODE, @VISIT_ID, @CUST_CODE, @TXN_DATE, @TXN_NO
    Public Function GET_COLL_DAILY_ACTY_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisit As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTXNNO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_COLL_DAILY_ACTY_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("VISIT_ID", strVisit, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXNNO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_COLL_DAILY_ACTY_HDR :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_COLL_DAILY_ACTY_PAY @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @TXN_NO
    Public Function GET_COLL_DAILY_ACTY_PAY(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strTXN_NO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_COLL_DAILY_ACTY_PAY"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_COLL_DAILY_ACTY_PAY :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_COLL_DAILY_ACTY_INV @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @TXN_NO
    Public Function GET_COLL_DAILY_ACTY_INV(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strTXN_NO As String) As DataTable
        Dim clsTRDB As clsDB
        Try
            clsTRDB = New clsDB
            With clsTRDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_COLL_DAILY_ACTY_INV"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXN_NO, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_COLL_DAILY_ACTY_INV :" & ex.Message))
        Finally
            clsTRDB = Nothing
        End Try
    End Function

#End Region

#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class
