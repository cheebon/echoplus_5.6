'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	31/03/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsProduct
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsProduct"
        End Get
    End Property

#Region "PRODUCT"
    Public Function GetPrdList(ByVal strTeamCode As String, ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdDetails(ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePrd(ByVal strTeamCode As String, ByVal strSupplierCode As String, ByVal strPrdGrpCode As String, _
                        ByVal strPrdCode As String, ByVal strPrdName As String, _
                        ByVal strStatus As String, ByVal strAbbv As String, ByVal strPoisonInd As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUPPLIER_CODE", strSupplierCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_NAME", strPrdName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("ABBV", strAbbv, cor_DB.clsDB.DataType.DBString)
                .addItem("POISON_IND", strPoisonInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


    Public Function CreatePrdInfo(ByVal strPrdCode As String, ByVal strPrdInfo As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_INFO_CREATE"


                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_INFO", strPrdInfo, cor_DB.clsDB.DataType.DBString)

                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdatePrd(ByVal strPrdCode As String, ByVal strPrdName As String, ByVal strStatus As String, ByVal strAbbv As String, ByVal strPoisonInd As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_NAME", strPrdName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("ABBV", strAbbv, cor_DB.clsDB.DataType.DBString)
                .addItem("POISON_IND", strPoisonInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "PRODUCT MAPPING"
    Public Function GetPrdMappingList(ByVal strPrdCode As String, ByVal strTeamCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MAPPING_LIST"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdMappingExcludeList(ByVal strSupplierCode As String, ByVal strSupplierName As String, ByVal strPrdGrpCode As String, ByVal strPrdGrpName As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MAPPING_EXCLUDE_LIST"
                .addItem("SUPPLIER_CODE", strSupplierCode, clsDB.DataType.DBString)
                .addItem("SUPPLIER_NAME", strSupplierName, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_NAME", strPrdGrpName, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub CreatePrdMapping(ByVal strSupplierCode As String, ByVal strPrdGrpCode As String, ByVal strPrdCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MAPPING_CREATE"
                .addItem("SUPPLIER_CODE", strSupplierCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeletePrdMapping(ByVal strSupplierCode As String, ByVal strPrdGrpCode As String, ByVal strPrdCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_MAPPING_DELETE"
                .addItem("SUPPLIER_CODE", strSupplierCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "PRODUCT UOM"
    Public Function GetPrdUOMList(ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_UOM_LIST"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdUOMDetails(ByVal strPrdCode As String, ByVal strUOMCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_UOM_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreatePrdUOM(ByVal strPrdCode As String, ByVal strUOMCode As String, _
                    ByVal strConvFactor As String, ByVal strPrdListPrice As String, ByVal strSalesOrgCode As String, ByVal strRegionCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_UOM_CREATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("SALES_ORG_CODE", strSalesOrgCode, cor_DB.clsDB.DataType.DBString)
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONV_FACTOR", strConvFactor, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_LISTPRICE", strPrdListPrice, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdatePrdUOM(ByVal strPrdCode As String, ByVal strUOMCode As String, ByVal strConvFactor As String, ByVal strPrdListPrice As String, ByVal strSalesOrgCode As String, ByVal strRegionCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_UOM_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("SALES_ORG_CODE", strSalesOrgCode, cor_DB.clsDB.DataType.DBString)
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONV_FACTOR", strConvFactor, cor_DB.clsDB.DataType.DBString)
                .addItem("PRD_LISTPRICE", strPrdListPrice, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeletePrdUOM(ByVal strPrdCode As String, ByVal strUOMCode As String, ByVal strSalesOrgCode As String, ByVal strRegionCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_UOM_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                'HL: 20111222: For CR-MY01-20110628-000056 - MY OTC migration
                If Web.HttpContext.Current.Session("PRINCIPAL_ID").Equals("5") Then
                    .addItem("SALES_ORG_CODE", strSalesOrgCode, cor_DB.clsDB.DataType.DBString)
                    .addItem("REGION_CODE", strRegionCode, cor_DB.clsDB.DataType.DBString)
                End If
                .addItem("UOM_CODE", strUOMCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "Product Image"

    Public Function GetPrdImg(ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_IMG_GET"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CheckPrdImgExists(ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_IMG_EXISTS"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeletePrdImg(ByVal strPrdCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_IMG_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



