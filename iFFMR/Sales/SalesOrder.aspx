<%@ Page Language="vb" AutoEventWireup="false" Inherits="SalesOrder" CodeFile="SalesOrder.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>Sales Order</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesOrder" method="post" runat="server">
       
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    &nbsp;</td>
                                                                <td align="left">
                                                                    <table id="tblControl" runat="server" border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Button ID="btnBack" runat="server" CssClass="cls_button" PostBackUrl="~/iFFMR/SFE/CallAnalysis/CallAnalysisListByCustomer.aspx"
                                                                                    ToolTip="Back To Previous Page." Text="Back"></asp:Button></td>
                                                                            <td align="left" style="width: 100%">
                                                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Timer id="TimerControl_1" runat="server" interval="100"></asp:Timer>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td valign="top" style="width: 50px" align="left">
                                                                                <asp:DetailsView ID="dvList_Left" runat="server" AutoGenerateRows="False" CssClass="Grid"
                                                                                    Width="100px" BorderWidth="0px" RowStyle-HorizontalAlign="Left" AlternatingRowStyle-HorizontalAlign="Left">
                                                                                    <Fields>
                                                                                        <asp:TemplateField HeaderText="Txn. No.">
                                                                                            <ItemTemplate>
                                                                                                <asp:DropDownList ID="ddlTXNNO2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTXNNO2_SelectedIndexChanged"
                                                                                                    CssClass="cls_dropdownlist">
                                                                                                </asp:DropDownList>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="CUST_CODE" HeaderText="Customer A/C" ReadOnly="True" SortExpression="CUST_CODE" />
                                                                                        <asp:BoundField DataField="CUST_NAME" HeaderText="Name" ReadOnly="True" SortExpression="CUST_NAME" />
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                        <asp:BoundField DataField="PO_NO" HeaderText="P.O. NO." ReadOnly="True" SortExpression="PO_NO" />
                                                                                        <asp:BoundField DataField="TTL_ORD_AMT" HeaderText="Total Order Value" ReadOnly="True" SortExpression="TTL_ORD_AMT" DataFormatString="{0:#,0.00}" HtmlEncode="False" />
                                                                                        <asp:BoundField DataField="DISC_AMT" HeaderText="Discount Amount" ReadOnly="True" SortExpression="DISC_AMT" DataFormatString="{0:#,0.00}" HtmlEncode="False" />
                                                                                        <asp:BoundField DataField="GST_AMT" HeaderText="GST Amount" ReadOnly="True" SortExpression="GST_AMT" DataFormatString="{0:#,0.00}" HtmlEncode="False" />
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                        <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS" HtmlEncode="false" ItemStyle-Wrap="true" ItemStyle-Width="100px" ControlStyle-Width="100px"/>
                                                                                    </Fields>
                                                                                    <RowStyle CssClass="cls_label" />
                                                                                    <FieldHeaderStyle CssClass="cls_label_header" HorizontalAlign="Left" Width="15%"
                                                                                        Wrap="False" />
                                                                                </asp:DetailsView>
                                                                            </td>
                                                                            <td valign="top" style="width: 50%" align="left">
                                                                                <asp:DetailsView ID="dvList_Right" runat="server" AutoGenerateRows="False" CssClass="Grid"
                                                                                    Width="100%" BorderWidth="0px" RowStyle-HorizontalAlign="Left" AlternatingRowStyle-HorizontalAlign="Left">
                                                                                    <Fields>
                                                                                        <asp:BoundField DataField="SHIPTO_DATE" HeaderText="Delivery Date" ReadOnly="True"
                                                                                            SortExpression="SHIPTO_DATE" DataFormatString="{0:yyyy-MM-dd}" HtmlEncode="False" />
                                                                                        <asp:BoundField DataField="SHIPTO_NAME" HeaderText="Delivery Name" ReadOnly="True"
                                                                                            SortExpression="SHIPTO_NAME" />
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                        <asp:BoundField DataField="ADDRESS" HeaderText="Delivery Address" ReadOnly="True"
                                                                                            SortExpression="ADDRESS" />
                                                                                        <asp:TemplateField></asp:TemplateField>
                                                                                        <asp:BoundField DataField="PAY_TERM_CODE" HeaderText="Pay Term Code" ReadOnly="True" SortExpression="PAY_TERM_CODE" />
                                                                                        <asp:BoundField DataField="PAY_TERM_NAME" HeaderText="Pay Term Name" ReadOnly="True" SortExpression="PAY_TERM_NAME" />
                                                                                    </Fields>
                                                                                    <RowStyle CssClass="cls_label" />
                                                                                    <FieldHeaderStyle CssClass="cls_label_header" HorizontalAlign="Left" Width="15%"
                                                                                        Wrap="False" />
                                                                                </asp:DetailsView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="height: 10px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;</td>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                        AllowSorting="True" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                        GridHeight="455" RowSelectionEnabled="true">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.</EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>        
    </form>
</body>
</html>
