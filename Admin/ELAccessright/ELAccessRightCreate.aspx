﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ELAccessRightCreate.aspx.vb"
    Inherits="Admin_ELAccessright_ELAccessRightCreate" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EL Accessright Create</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>

    <script src="../../include/layout.js" type="text/javascript"></script>

    <style type="text/css">
        .valPos
        {
            float: right;
            width: 10px;
        }
    </style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmELAccessRightCreate" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" CombineScripts="False" />
    <fieldset class="" style="width: 98%; ">
    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
    <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <customToolkit:wuc_lblInfo ID="lblErr" runat="server" Title="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <customToolkit:wuc_lblHeader ID="lblHeader" runat="server" Title="" />
    <asp:UpdatePanel ID="updPnlAccessright" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="ContainerDiv" style="height: 100%">
                <div>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="cls_button"
                        CausesValidation="False" />&nbsp;
                    <asp:Button ID="btnCreate" runat="server" Text="Create"
                        CssClass="cls_button" /><br />
                    <br />
                    <customToolkit:wuc_lblInfo ID="lblErrDtl" runat="server" />
                    <table width="800px" cellpadding="5" cellspacing="0" align="left" border="1" class="DV"
                        style="border-collapse: collapse;">
                        <tr class="cls_DV_Row_MST">
                            <td width="20%" class="cls_DV_Header_MST">
                                <div style="float: left">
                                    <asp:Label ID="lblName" runat="server" Text="Name" meta:resourcekey="lblNameResource1"></asp:Label></div>
                                <div class="cls_label_required_hdr valPos" style="">
                                </div>
                            </td>
                            <td>
                                <span class="cls_label_required">
                                    <asp:TextBox ID="txtName" runat="server" CssClass="cls_textbox" MaxLength="50" meta:resourcekey="txtNameResource1"></asp:TextBox></span>
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                    ErrorMessage="Name cannot be Blank !" Display="Dynamic" CssClass="cls_label_err"
                                    meta:resourcekey="rfvNameResource1"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblDupError" runat="server" Text="Name already exists!" Visible="False"
                                    CssClass="cls_label_err" meta:resourcekey="lblDupErrorResource1"></asp:Label>
                            </td>
                        </tr>
                        <tr class="cls_DV_Alt_Row_MST">
                            <td width="20%" class="cls_DV_Header_MST">
                                <asp:Label ID="lblName1" runat="server" Text="Name 1" meta:resourcekey="lblName1Resource1"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtName1" runat="server" CssClass="cls_textbox" MaxLength="50" meta:resourcekey="txtName1Resource1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cls_DV_Row_MST">
                            <td width="20%" class="cls_DV_Header_MST">
                                <asp:Label ID="lblDesc" runat="server" Text="Description" meta:resourcekey="lblDescResource1"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesc" runat="server" CssClass="cls_textbox" MaxLength="100" Width="300px"
                                    meta:resourcekey="txtDescResource1"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="cls_DV_Alt_Row_MST">
                            <td width="20%" class="cls_DV_Header_MST">
                                <asp:Label ID="lblDesc1" runat="server" Text="Description 1" meta:resourcekey="lblDesc1Resource1"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDesc1" runat="server" CssClass="cls_textbox" MaxLength="100"
                                    Width="300px" meta:resourcekey="txtDesc1Resource1"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </fieldset>
    </form>
</body>
</html>
