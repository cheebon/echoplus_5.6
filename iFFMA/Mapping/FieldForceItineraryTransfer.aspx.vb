Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports mst_Mapping

Partial Class iFFMA_Mapping_FieldForceItineraryTransfer
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "FieldForceItineraryTransfer.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.FIELDFORCEITINERARYTRANSFER)
            .Visible = True
        End With

        'Apply for all reports, check for the session
        If Session("UserID") Is Nothing OrElse Session("UserID") = "" Then
            Dim strScript As String = ""
            strScript = "self.parent.parent.location='" & ResolveClientUrl("~/login.aspx") & "?ErrMsg=Session Time Out, Please login again !!!';"
            ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Timeout", strScript, True)
        End If
        'End Session Check

        If Not IsPostBack Then
            TimerControl1.Enabled = True

            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCEITINERARYTRANSFER, SubModuleAction.Edit) Then
                btnproceed.Visible = False
            End If

            DataBind()
        End If

        lblErr.Text = ""

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Overloads Sub DataBind()
        LoadDDLTeam()
        LoadDDLFieldForceSource()
        LoadDDLFieldForceDestination()
        LoadDDLFieldForceAction()
    End Sub

#Region "DDL"
    Private Sub LoadDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL
            With ddlTeam
                .Items.Clear()
                .DataSource = dtTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(Session("SELECTED_TEAM") = "", "", Session("SELECTED_TEAM"))
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLFieldForceSource()
        Dim dtSalesrep As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSalesrep = clsCommon.GetSalesrepDDL(ddlTeam.SelectedValue)
            With ddlFieldForcesource
                .Items.Clear()
                .DataSource = dtSalesrep.DefaultView
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(Session("SELECTED_SR") = "", "", Session("SELECTED_SR"))
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLFieldForceDestination()
        Dim dtSalesrep As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtSalesrep = clsCommon.GetSalesrepDDL(ddlTeam.SelectedValue)
            With ddlFieldForcedestination
                .Items.Clear()
                .DataSource = dtSalesrep.DefaultView
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                .SelectedValue = IIf(Session("SELECTED_SR") = "", "", Session("SELECTED_SR"))
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDLFieldForceAction()

        Try
            Dim action1, action2 As String
            action1 = "DUPLICATE"
            action2 = "UPDATE"

            With ddlFieldForceAction
                .Items.Clear()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .Items.Insert(1, New ListItem(action1, action1))
                .Items.Insert(2, New ListItem(action2, action2))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "EVENT HANDLER"

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged

        Session("SELECTED_TEAM") = ddlTeam.SelectedValue
        DataBind()

    End Sub

    Protected Sub btnproceed_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Page.IsValid Then
            EditFieldForceItineraryTransfer()
            wuc_lblMsgPop.Message = "Itinerary transfer completed!"
            wuc_lblMsgPop.Show()
        End If
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        ' If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub
#End Region
    
    Protected Sub EditFieldForceItineraryTransfer()
   
        Dim strTeamCode, strSourceFieldForceCode, strDestinationFieldForceCode, strActionCode As String
        strTeamCode = ddlTeam.SelectedValue.ToString
        strSourceFieldForceCode = ddlFieldForcesource.SelectedValue.ToString
        strDestinationFieldForceCode = ddlFieldForcedestination.SelectedValue.ToString
        strActionCode = ddlFieldForceAction.SelectedValue.ToString

        Dim obj As mst_Mapping.clsFieldForceItineraryTransfer
        obj = New mst_Mapping.clsFieldForceItineraryTransfer

        With obj
            .EditFieldForceItineraryTransfer(strTeamCode, strSourceFieldForceCode, strDestinationFieldForceCode, strActionCode)
        End With
    End Sub
   
End Class
