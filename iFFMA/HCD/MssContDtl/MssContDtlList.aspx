<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MssContDtlList.aspx.vb"
    Inherits="iFFMA_HCD_MssContDtl_MssContDtlList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_pnlStdBar" Src="~/include/wuc_pnlStdBar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mss Contact Details List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script type="text/javascript" language="javascript">
function resizeLayout2(){var dgList = $get('tcResult_TabPanel1_div_dgList');if (dgList){dgList.scrollTop=0;dgList.style.height=Math.max((getFrameHeight('ContentBarIframe') - 100),0)+'px';}}
window.onresize=function(){resizeLayout2();}
    </script>

</head>
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');MaximiseFrameHeight('ContentBarIframe');">
    <form id="frmMssContDtlList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="left">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 99.5%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_lblInfo ID="wuc_lblInfo" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_pnlStdBar ID="wuc_pnlStdBar" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" Height="" ScrollBars="None"
                                                Width="98%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Summary">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                                            <tr>
                                                                <td class="BckgroundBenealthTitle">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" class="Bckgroundreport">
                                                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                                        <ContentTemplate>
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                <tr align="right">
                                                                                    <td>
                                                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr align="left">
                                                                                    <td style="width: 98%">
                                                                                        <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" style="width: 95%;">
                                                                                        <asp:Panel ID="pnlList" runat="server">
                                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                ShowFooter="False" AllowPaging="true" PagerSettings-Visible="false" DataKeyNames="SALESREP_CODE,CUST_CODE,CONT_CODE">
                                                                                            </ccGV:clsGridView>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr class="Bckgroundreport">
                                                                <td style="height: 5px">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Details">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                                            <tr>
                                                                <td class="BckgroundBenealthTitle">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" class="Bckgroundreport">
                                                                    <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                                    <asp:UpdatePanel ID="UpdateDatagridDtl" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                                        <ContentTemplate>
                                                                            <asp:Panel ID="pnlDetails" runat="server">
                                                                                <table border="0" cellpadding="5" cellspacing="5" width="100%">
                                                                                    <tr align="right">
                                                                                        <td>
                                                                                            <asp:Timer ID="TimerControldtl" runat="server" Enabled="False" Interval="100" OnTick="TimerControldtl_Tick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span class="cls_label_header">Contact Best to Call</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" style="width: 95%;">
                                                                                            <ccGV:clsGridView ID="dgListBTC" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="58%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                                                                <EmptyDataTemplate>
                                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                                </EmptyDataTemplate>
                                                                                            </ccGV:clsGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span class="cls_label_header">Contact Product Specialty</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" style="width: 95%;">
                                                                                            <ccGV:clsGridView ID="dgListPDS" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="48%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                                                                <EmptyDataTemplate>
                                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                                </EmptyDataTemplate>
                                                                                            </ccGV:clsGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span class="cls_label_header">Contact Specialty</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" style="width: 95%;">
                                                                                            <ccGV:clsGridView ID="dgListSpecialty" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="68%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                                                                <EmptyDataTemplate>
                                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                                </EmptyDataTemplate>
                                                                                            </ccGV:clsGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span class="cls_label_header">Contact Investment</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" style="width: 95%;">
                                                                                            <ccGV:clsGridView ID="dgListInvestment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="78%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                                                                <EmptyDataTemplate>
                                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                                </EmptyDataTemplate>
                                                                                            </ccGV:clsGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <span class="cls_label_header">Contact Potential Info</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left" style="width: 95%;">
                                                                                            <ccGV:clsGridView ID="dgListPotentialInfo" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                                                                <EmptyDataTemplate>
                                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                                </EmptyDataTemplate>
                                                                                            </ccGV:clsGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr class="Bckgroundreport">
                                                                <td style="height: 5px">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
