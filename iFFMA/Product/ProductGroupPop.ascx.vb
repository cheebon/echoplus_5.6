Imports System.Data

Partial Class iFFMA_Product_ProductGroupPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property SupplierCode() As String
        Get
            Return Trim(hdSupplierCode.Value)
        End Get
        Set(ByVal value As String)
            hdSupplierCode.Value = value
        End Set
    End Property

    Public Property PrdGrpCode() As String
        Get
            Return Trim(hdPrdGrpCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdGrpCode.Value = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtSupplier As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)
            If ddlStatus IsNot Nothing Then
                ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS_CODE")), "", DT.Rows(0)("STATUS_CODE"))
            End If

            Dim ddlSupplier As DropDownList = CType(DetailsView1.FindControl("ddlSupplier"), DropDownList)
            If ddlSupplier IsNot Nothing Then
                dtSupplier = clsCommon.GetSupplierDDL(TeamCode)
                With ddlSupplier
                    .Items.Clear()
                    .DataSource = dtSupplier.DefaultView
                    .DataTextField = "SUPPLIER_NAME"
                    .DataValueField = "SUPPLIER_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("SUPPLIER_CODE")), "", DT.Rows(0)("SUPPLIER_CODE"))
                End With
            End If
            
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsProductGroup As New mst_Product.clsProductGroup

            DT = clsProductGroup.GetPrdGrpDetails(SupplierCode, PrdGrpCode)
            If DT.Rows.Count > 0 Then
                
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCTGROUP, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ddlSupplier As DropDownList = CType(DetailsView1.FindControl("ddlSupplier"), DropDownList)
            Dim txtPrdGrpCode As TextBox = CType(DetailsView1.FindControl("txtPrdGrpCode"), TextBox)
            Dim txtPrdGrpName As TextBox = CType(DetailsView1.FindControl("txtPrdGrpName"), TextBox)
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)

            Dim clsProductGroup As New mst_Product.clsProductGroup
            Dim DT As DataTable

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                Dim IsPrdExists As Integer
                DT = clsProductGroup.UpdatePrdGrp(SupplierCode, PrdGrpCode, Trim(txtPrdGrpName.Text), ddlStatus.SelectedValue)
                If DT.Rows.Count > 0 Then
                    IsPrdExists = DT.Rows(0)("IS_PRD_EXISTS")
                End If

                If IsPrdExists = 1 Then
                    lblInfo.Text = "Please deactive the active product(s) in this product group before proceed!"
                    LoadDvEditMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully saved."
                    LoadDvViewMode()
                End If
            Else
                DT = clsProductGroup.CreatePrdGrp(TeamCode, Trim(ddlSupplier.SelectedValue), Trim(txtPrdGrpCode.Text), Trim(txtPrdGrpName.Text), ddlStatus.SelectedValue)

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Product Group Code already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully created."
                    SupplierCode = ddlSupplier.SelectedValue.ToString
                    PrdGrpCode = txtPrdGrpCode.Text.ToString
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class