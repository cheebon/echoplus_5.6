Imports System.Data

Partial Class CallEnquiryList
    Inherits System.Web.UI.Page
    'Private Shared intPageSize As Integer
#Region "Local Variable"
    Protected Property boolRestore() As Boolean
        Get
            Return CBool(ViewState("blnRestore"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("blnRestore") = value
        End Set
    End Property

    Protected Property intPageSize() As Integer
        Get
            Return CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        End Get
        Set(ByVal value As Integer)
            Session("PageSize") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_" & Me.ToString

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

#End Region
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CallEnquiryList.aspx"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CALLENQ) '"Call Enquiry Report"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CALLENQ
                .DataBind()
                .Visible = True
            End With

            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
            lblErr.Text = ""
            'Call Paging
            With Wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                Dim strDateStart, strDateEnd, strSalesrepCode, strCustCode, strContCode As String
                'Query String: DATE_START, DATE_END, SALESREP_CODE, CUST_CODE
                strDateStart = Trim(Request.QueryString("DATE_START"))
                strDateEnd = Trim(Request.QueryString("DATE_END"))
                strSalesrepCode = Trim(Request.QueryString("SALESREP_CODE"))
                strCustCode = Trim(Request.QueryString("CUST_CODE"))
                strContCode = Trim(Request.QueryString("CONT_CODE"))
                If Not String.IsNullOrEmpty(strDateStart) AndAlso IsDate(strDateStart) _
                   AndAlso Not String.IsNullOrEmpty(strDateEnd) AndAlso IsDate(strDateEnd) Then
                    With wuc_ctrlpanel
                        .SearchPanelVisibility = False
                        .CallEnq_DateStart = strDateStart
                        .CallEnq_DateEnd = strDateEnd
                        .CallEnq_Salesman = strSalesrepCode
                        .CallEnq_CustCode = strCustCode
                        .CallEnq_ContCode = strContCode
                    End With

                    boolRestore = True

                    TimerControl1.Enabled = True
                Else
                    BindDefault() 'Show blank grid view
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Sub btnRrefreshButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.CallEnquiryBtn_Click
        Try
            If Page.IsValid = False Then Exit Sub
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRrefreshButton_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.EnqResetBtn_Click
        Try
            BindDefault()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "CallEnquiry")
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.UpdateCallEnquirySearchDetail()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.UpdateCallEnquirySearchDetail()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                If boolRestore Then
                    ViewState("strSortExpression") = CriteriaCollector.SortExpression
                    strSortExpression = ViewState("strSortExpression")
                    dgList.PageIndex = CriteriaCollector.PageIndex
                    boolRestore = False
                Else
                    ViewState("strSortExpression") = Nothing
                    dgList.PageIndex = 0
                End If
            End If
            'If dtCurrentTable Is Nothing Then
            '    dtCurrentTable = New DataTable
            'ElseIf dtCurrentTable.Rows.Count = 0 Then
            '    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            'End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.PageSize = intPageSize
            dgList.DataSource = dvCurrentView
            dgList.DataBind()

            'Call Paging
            With Wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                CriteriaCollector.PageIndex = dgList.PageIndex
            End With

            Wuc_dgpaging.Visible = IIf(dgList.Rows.Count > 0, True, False)


        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 10 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallEnqList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CallEnqList.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CallEnqList.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallEnqList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.HeaderText = CF_CallEnqList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing

                        strUrlFormatString = FormUrlFormatString(ColumnName, dtToBind, strUrlFields)
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CallEnqList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            'dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If

                        dgColumn.ItemStyle.HorizontalAlign = CF_CallEnqList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_CallEnqList.ColumnStyle(ColumnName).Wrap   'HL:20070711

                        dgColumn.HeaderText = CF_CallEnqList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Pass in the datatable, and generate the url format string based on the
    ''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    ''' to form the hyperlink fields need.
    ''' </summary>
    ''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    ''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    ''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    ''' <returns>URLFormatString, as url to pass with parameters</returns>
    ''' <remarks></remarks>
    Private Function FormUrlFormatString(ByVal strColumnName As String, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Try
            ReDim strUrlFields(4)
            strUrlFields(0) = "SALESREP_CODE"
            strUrlFields(1) = "VISIT_ID"
            strUrlFields(2) = "CUST_CODE"
            strUrlFields(3) = "DATE"
            strUrlFields(4) = "CONT_CODE"
            Select Case strColumnName
                Case "SFMS_IND" 'Generate SFMS_IND
                    strUrlFormatString = "~/iFFMR/SFE/CallProd/CallProdDailyActivity.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=CALLENQUIRY"
                Case "SALES_IND"
                    strUrlFormatString = "~/iFFMR/Sales/SalesOrder.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=CALLENQUIRY"
                Case "COLL_IND"
                    strUrlFormatString = "~/iFFMR/COLL/CollDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=CALLENQUIRY"
                Case "TRA_IND"
                    strUrlFormatString = "~/iFFMR/TRA/TraRetDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=CALLENQUIRY"
                Case "DRC_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/DRCDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLENQUIRY"
                Case "MSS_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/MSSDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLENQUIRY"
                Case "DN_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/DNDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLENQUIRY"
                Case "HIST_IND"
                    strUrlFormatString = "~/iFFMR/Customize/CallActyHistByCont.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=CALLENQUIRY"
                Case "DET_IND"
                    strUrlFormatString = "~/iFFMR/Customize/DetailingInfo.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLENQUIRY"
                Case Else
                    strUrlFields = Nothing
                    strUrlFormatString = ""
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        End Try
        Return strUrlFormatString
    End Function

    'Protected Sub dgList_Init()
    '    Try
    '        aryDataItem.Clear()
    '        For Each Column As DataControlField In dgList.Columns
    '            aryDataItem.Add(Column.SortExpression)
    '        Next


    '        Dim strUrlFields(3) As String
    '        strUrlFields(0) = "SALESREP_CODE"
    '        strUrlFields(1) = "VISIT_ID"
    '        strUrlFields(2) = "CUST_CODE"
    '        strUrlFields(3) = "CALL_DATE"

    '        Dim dgColumn_SALES_INDICATOR As HyperLinkField = dgList.Columns(aryDataItem.IndexOf("SALES_INDICATOR"))
    '        If dgColumn_SALES_INDICATOR IsNot Nothing Then
    '            Dim strUrlFormatString As String = "~/iFFMR/Sales/SalesOrder.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLENQUIRY"
    '            dgColumn_SALES_INDICATOR.DataNavigateUrlFields = strUrlFields
    '            dgColumn_SALES_INDICATOR.DataNavigateUrlFormatString = strUrlFormatString
    '            dgColumn_SALES_INDICATOR.Target = "_self"
    '        End If

    '        Dim dgColumn_SFMS_INDICATOR As HyperLinkField = dgList.Columns(aryDataItem.IndexOf("SFMS_INDICATOR"))
    '        If dgColumn_SFMS_INDICATOR IsNot Nothing Then
    '            Dim strUrlFormatString As String = "~/iFFMR/SFE/CallProd/CallProdDailyActivity.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLENQUIRY"
    '            dgColumn_SFMS_INDICATOR.DataNavigateUrlFields = strUrlFields
    '            dgColumn_SFMS_INDICATOR.DataNavigateUrlFormatString = strUrlFormatString
    '            dgColumn_SFMS_INDICATOR.Target = "_self"
    '        End If


    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
    '    End Try
    'End Sub

    Private Sub BindDefault()
        Try
            wuc_ctrlpanel.SearchPanelVisibility = True
            Dim dt As Data.DataTable = GetRecList("1900-01-01", "1900-01-01", "", "", "")
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            With (wuc_ctrlpanel)
                DT = GetRecList(.CallEnq_DateStart, .CallEnq_DateEnd, .CallEnq_Salesman, .CallEnq_CustCode, .CallEnq_ContCode)
            End With
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        Return DT
    End Function

    Private Function GetRecList(ByVal strDate_Start As String, ByVal strDate_End As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")


            Dim clsCallDB As New rpt_CALL.clsCallQuery
            DT = clsCallDB.GetCallEnquiryList(strUserID, strPrincipalId, strPrincipalCode, strDate_Start, strDate_End, strSalesrepCode, strCustCode, strContCode)
            'dgList_Init(DT)
            'PreRenderMode(DT)
            'Query String: DATE_START, DATE_END, SALESREP_CODE, CUST_CODE
            Session("CallEnquiry_QueryString") = "DATE_START=" & strDate_Start & "&DATE_END=" & strDate_End & "&SALESREP_CODE=" & strSalesrepCode & "&CUST_CODE=" & strCustCode & "&CONT_CODE=" & strContCode
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles Wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(Wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles Wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles Wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
End Class


Public Class CF_CallEnqList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "DEPARTMENT"
                strFieldName = "Department"
            Case "POSITION"
                strFieldName = "Position"
            Case "TIME_IN"
                strFieldName = "Time In"
            Case "TIME_OUT"
                strFieldName = "Time Out"
            Case "TIME_SPEND"
                strFieldName = "Time Spend"
            Case "VISIT_IND"
                strFieldName = "Type"
            Case "COOR_LOC"
                strFieldName = "Location"
            Case "TOTAL_SALES"
                strFieldName = "Total Sales"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Select Case strColumnName
                Case "SFMS_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CPRODACTYDTL, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "SALES_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESORD, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "TRA_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.TRAINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "COLL_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.COLLINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DRC_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DRCINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "MSS_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.MSSINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DN_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DNINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "HIST_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CALLACTYHISTBYCONT, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DET_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DETINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "REASON_CODE", "VISIT_ID"
                    FCT = FieldColumntype.InvisibleColumn
                Case "REASON_NAME", "DATE"
                    FCT = FieldColumntype.BoundColumn
            End Select

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}" '{0:#,0.00}
                Case "COOR_LOC"
                    strFormatString = "{0}"
                Case "TOTAL_SALES"
                    strFormatString = "{0:#,0.00}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    'Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
    '    Dim CS As New ColumnStyle
    '    Try
    '        With CS
    '            Dim strColumnName As String = ColumnName.ToUpper
    '            .FormatString = GetOutputFormatString(ColumnName)

    '            Select Case strColumnName
    '                Case "DATE", "SALESREP_CODE", "CUST_CODE", "CONT_CODE", "TIME_IN", "TIME_OUT", "TIME_SPEND" _
    '                , "SFMS_IND", "SALES_IND", "COLL_IND", "TRA_IND"
    '                    .HorizontalAlign = HorizontalAlign.Center
    '                Case Else
    '                    If Not String.IsNullOrEmpty(.FormatString) Then
    '                        .HorizontalAlign = HorizontalAlign.Right
    '                    Else
    '                        .HorizontalAlign = HorizontalAlign.Left
    '                    End If
    '            End Select
    '        End With

    '    Catch ex As Exception

    '    End Try
    '    Return CS
    'End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf strColumnName Like "*_SALES" Then
                    .HorizontalAlign = HorizontalAlign.Right
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class


