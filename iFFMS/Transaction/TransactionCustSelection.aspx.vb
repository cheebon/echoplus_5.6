Imports System.Data

Partial Class TransactionCustSelection
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "TransactionCustSelection"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim strSalesmanCode, strcustcode As String
        'Put user code to initialize the page here
        Try
            strcustcode = Request.QueryString("custcode")
            Session("customercode") = strcustcode
            'Session("dflSalesRepCode") = "100610"
            'Session("customercode") = "C000070"
            'Call Header
            With wuc_lblheader
                .Title = "Contact Selection"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.CUSTLIST
                .DataBind()
                .Visible = True
            End With

            strSalesmanCode = Trim(Session("dflSalesRepCode"))

            Session("DRCList") = Nothing
            Session("DRCHdr") = Nothing
            Session("cartdt") = Nothing
            Session("SalesHdr") = Nothing
            Session("SFMSSession") = Nothing
            Session("SFMSHdr") = Nothing
            Session("TradeReturn") = Nothing
            Session("TradeRetHdr") = Nothing
            Session("MSSList") = Nothing
            Session("MSSHdr") = Nothing

            If Not IsPostBack Then
                bindGridView(strSalesmanCode, strcustcode)
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub addClickEvent(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", "javascript:__doPostBack('gridview1','Select$" & e.Row.RowIndex & "')")
            e.Row.Attributes.Add("style", "cursor:hand;")

        End If
    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_Customer.clsCustInfo

        Try
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                GridView1.DataKeyNames = New String() {"cont_acc_code"}
                GridView1.DataSource = dt
                GridView1.DataBind()
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                GridView1.DataSource = dtnew
                GridView1.DataBind()
                GridView1.Columns(6).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub gridview_Selected(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand
        redirect()
    End Sub

    Sub changeMessage(ByVal sender As Object, ByVal e As EventArgs) Handles GridView1.SelectedIndexChanged
        redirect()
    End Sub

    Sub redirect()
        Dim strContactCode As String
        Dim key As DataKey
        Dim dt As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Dim strSalesmanCode, strCustCOde As String
        Try
            strSalesmanCode = Trim(Session("dflsalesrepcode"))
            strCustCOde = Trim(Session("customercode"))
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCOde, "1")
            If dt.Rows.Count > 0 Then
                transactionsInserted()
                transactionsSaved()
                key = GridView1.DataKeys(GridView1.SelectedRow.RowIndex)
                strContactCode = key.Value
                Session("contactcode") = strContactCode
                Response.Redirect("../TxnModule/TxnModuleDRC.aspx", False)
            Else

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".redirect : " & ex.ToString)
        End Try
    End Sub

    Sub transactionsInserted()
        Try
            Session("DRCInserted") = "No"
            Session("SalesOrderInserted") = "No"
            Session("SFMSInserted") = "No"
            Session("MSSInserted") = Nothing
            Session("TradeReturnInserted") = "No"
        Catch ex As Exception

        End Try
    End Sub

    Sub transactionsSaved()
        Try
            Session("DRCSaved") = "No"
            Session("SalesOrderSaved") = "No"
            Session("SFMSSaved") = "No"
            Session("MSSSaved") = Nothing
            Session("TradeReturnSaved") = "No"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



