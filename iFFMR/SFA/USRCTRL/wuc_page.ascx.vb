
Partial Class iFFMR_SFA_USRCTRL_wuc_page
    Inherits System.Web.UI.UserControl

    Public Property StartRec() As Integer
        Get
            If Not ViewState("StartRec") Is Nothing Then
                Return ViewState("StartRec")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("StartRec") = value
        End Set
    End Property
    Public Property EndRec() As Integer
        Get
            If Not ViewState("EndRec") Is Nothing Then
                Return ViewState("EndRec")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("EndRec") = value
        End Set
    End Property
    Public Property CurPage() As Integer
        Get
            If Not ViewState("curpage") Is Nothing Then
                Return ViewState("curpage")
            Else
                Return 1
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("curpage") = value
        End Set
    End Property
    Public Property TotPage() As Integer
        Get
            If Not ViewState("totpage") Is Nothing Then
                Return ViewState("totpage")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("totpage") = value
        End Set
    End Property
    Public Property TotRows() As Integer
        Get
            If Not ViewState("totRows") Is Nothing Then
                Return ViewState("totRows")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("totRows") = value
        End Set
    End Property
    Public Event Previous_Click As EventHandler
    Public Event Next_Click As EventHandler
    Public Event Go_Click As EventHandler

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Overrides Sub Databind()
        TotPage = Convert.ToInt32(System.Math.Floor(TotRows / 50)) + IIf(TotRows Mod 50 = 0, 0, 1)

        ''If ddlPage.Items.Count = 0 Then
        'FillPageDDL()
        ''End If

        StartRec = (CurPage - 1) * 50 + 1
        If (CurPage * 50) > TotRows Then
            EndRec = TotRows
        Else
            EndRec = CurPage * 50
        End If


        lblStart.Text = StartRec.ToString()
        lblEnd.Text = EndRec.ToString()
        lblPage.Text = TotPage.ToString()
        txtPage.Text = CurPage.ToString
        UpdatePager.Update()
    End Sub
    
    Protected Sub IBPrev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CurPage = IIf(CurPage - 1 < 1, 1, CurPage - 1)
        RaiseEvent Previous_Click(Me, e)
    End Sub


    Protected Sub IBNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        CurPage = IIf(CurPage + 1 > TotPage, TotPage, CurPage + 1)
        RaiseEvent Next_Click(Me, e)
    End Sub

    'Private Sub FillPageDDL()
    '    ddlPage.Items.Clear()
    '    For i As Integer = 1 To TotPage Step 1
    '        ddlPage.Items.Add(New ListItem(i, i))
    '    Next
    'End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim inputpage As Integer
        Try
            If IsNumeric(Trim(txtPage.Text)) Then
                inputpage = Convert.ToInt16(txtPage.Text)

                If inputpage < 1 Then
                    CurPage = 1
                ElseIf inputpage > TotPage Then
                    CurPage = TotPage
                Else
                    CurPage = inputpage
                End If
            Else
                Exit Sub
            End If

            RaiseEvent Go_Click(Me, e)
        Catch ex As Exception

        End Try

    End Sub
End Class
