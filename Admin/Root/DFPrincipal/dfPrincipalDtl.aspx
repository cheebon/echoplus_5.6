﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="dfPrincipalDtl.aspx.vb" Inherits="Admin_Root_DFPrincipal_dfPrincipalDtl" StyleSheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data File Principal Dtl</title>
    <script src="../../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
     <script type="text/javascript">

        //dgList
        function ValidateCheckBoxStates() {
            var flag = false;
            $("#dgList").find("input:checkbox[Id*=chkSelectInd]").each(function() {
                if (this.checked == true) flag = true;
            });
            if (flag) { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function ChangeAllCheckBoxStates(element) {
            $("#dgList").find("input:checkbox[Id*=chkSelectInd]").each(function() { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStates(element) {
            var flag = true;
            $("#dgList").find("input:checkbox[Id*=chkSelectInd]").each(function() { if (this.checked == false) flag = false; });
            $("#dgList").find("input:checkbox[Id*=chkAllSelectInd]").attr('checked', flag);
        }
        //dgList
    </script>
</head>

<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmDfPrincipalDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />   
      <div stle="width:98%">   
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
        <asp:UpdatePanel ID="UpdatePanelCriteria" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
          <table border="0" cellpadding="0" cellspacing="0" width="98%">
              <tr>
                  <td>
                      <asp:Label ID="lblTeamCode" runat="server" Text="Team Code "></asp:Label>
                  </td>
                  <td>
                      <asp:DropDownList ID="ddlTeam" runat="server" >
                      </asp:DropDownList>
                  </td>
                  <td>
                    <asp:Label ID="lblCountryCode" runat="server" Text="Country Code "></asp:Label>
                  </td>
                  <td>
                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" Width="200px">
                            </asp:DropDownList>
                  </td>
              </tr>
              <tr>
                  <td>
                      <asp:Label ID="lblPivotCode" runat="server" Text="Pivot Code "></asp:Label>
                  </td>
                  <td>
                      <asp:DropDownList ID="ddlPivotCode" runat="server" >
                      </asp:DropDownList>
                  </td>
                  <td>
                     <asp:Label ID="lblPrincipalCode" runat="server" Text="Principal Code "></asp:Label>
                  </td>
                  <td>
                    <asp:DropDownList ID="ddlPrincipal" runat="server" Width="200px">
                            </asp:DropDownList>
                  </td>
              </tr>
              <tr>
                <td colspan="2">
                <asp:Button ID="btnBack" runat="server" Text="Back" />
                <asp:Button ID="btnSearch" runat="server" Text="Search" />
                 <asp:Button ID="btnSave" runat="server" Text="Save" Visible="false"  />
                </td>
                <td colspan="2">
                <asp:Button ID="btnreference" runat="server" Text="Reference" />
               </td>
              </tr>
          </table>
           </ContentTemplate>
        </asp:UpdatePanel>
         <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <span style="width: 98%;">
                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="false" />
                </span>
                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    Width="98%" FreezeHeader="True" GridHeight="450" AddEmptyHeaders="0" CellPadding="2"
                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                    ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="SELECT_IND,DF_ID">
                    <Columns>
                    </Columns>
                </ccGV:clsGridView>
            </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </form>
</body>
</html>
