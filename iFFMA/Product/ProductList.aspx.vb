'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	31/03/2008
'	Purpose	    :	Product List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports System.IO

Partial Class iFFMA_Product_ProductList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
      
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "ProductList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.PRODUCT)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.PRODUCT
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                LoadImgExists()
                ActivateFirstTab()
                TimerControl1.Enabled = True

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                End If

                'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, SubModuleAction.Edit) Then
                    btnSave.Visible = False
                    btnSavePrdInfo.Visible = False
                End If
                '----------------------------------------------------------------------------------------------------


            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DDL"
    Private Sub BindDDLSupplier()
        Dim dtSupplier As DataTable
        Try
            Dim clsCommon As New mst_Common.clsDDL

            dtSupplier = clsCommon.GetSupplierDDL(wuc_toolbar.TeamCode)
            With ddlSupplier
                .Items.Clear()
                .DataSource = dtSupplier.DefaultView
                .DataTextField = "SUPPLIER_NAME"
                .DataValueField = "SUPPLIER_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub BindDDLPrdGrp()
        Dim dtPrdGrp As DataTable
        Try
            Dim clsCommon As New mst_Common.clsDDL

            dtPrdGrp = clsCommon.GetPrdGrpDDL(wuc_toolbar.TeamCode, ddlSupplier.SelectedValue)
            With ddlPrdGrp
                .Items.Clear()
                .DataSource = dtPrdGrp.DefaultView
                .DataTextField = "PRD_GRP_NAME"
                .DataValueField = "PRD_GRP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            wuc_toolbar.InfoString = ""

            ActivateFirstTab()
            RenewDataBind()

            If wuc_toolbar.TeamCode = "" Then
                wuc_toolbar.InfoString = "Please select the team."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        If wuc_toolbar.TeamCode = "" Then
            wuc_toolbar.InfoString = "Please select the team."
            wuc_toolbar.updatePanel()
            Exit Sub
        End If

        BindDDLSupplier()
        BindDDLPrdGrp()

        ViewState("PRD_CODE") = ""

        ddlSupplier.SelectedIndex = 0
        ddlSupplier.Enabled = True
        ddlPrdGrp.SelectedIndex = 0
        ddlPrdGrp.Enabled = True
        txtPrdCode.Text = ""
        txtPrdCode.Enabled = True
        txtPrdName.Text = ""
        ddlStatus.SelectedIndex = 0
        txtAbbv.Text = ""
        ddlPoison.SelectedIndex = 0
        txtPrdInfo.Text = ""

        btnSave.Visible = True 'HL: 20080429 (AR)
        ActivateAddTab()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = ""
            Dim DT As DataTable
            Dim clsProduct As New mst_Product.clsProduct
            Dim strSelectedPrdCode As String = ViewState("PRD_CODE").ToString

            If strSelectedPrdCode <> "" Then
                'Dim isDuplicate As Long = 0
                DT = clsProduct.UpdatePrd(Trim(txtPrdCode.Text), Trim(txtPrdName.Text), ddlStatus.SelectedValue, _
                                        Trim(txtAbbv.Text), Trim(ddlPoison.SelectedValue))

                'If DT.Rows.Count > 0 Then
                '    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                'End If

                'If isDuplicate = 1 Then
                '    lblInfo.Text = "Product Code in this product group already exists!"
                'Else
                ViewState("PRD_CODE") = strSelectedPrdCode
                lblInfo.Text = "The record is successfully saved."
                'End If

            Else
                DT = clsProduct.CreatePrd(wuc_toolbar.TeamCode, Trim(ddlSupplier.SelectedValue), Trim(ddlPrdGrp.SelectedValue), Trim(txtPrdCode.Text), Trim(txtPrdName.Text), ddlStatus.SelectedValue, _
                                        Trim(txtAbbv.Text), Trim(ddlPoison.SelectedValue))

                Dim isDuplicate As Long = 0

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    'lblInfo.Text = "Product Code in this product group already exists!"
                    lblInfo.Text = "This Product Code already exists!"
                Else
                    ViewState("PRD_CODE") = Trim(txtPrdCode.Text)

                    'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                    'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, SubModuleAction.Edit) Then
                    '    ActivateEditTab()
                    'Else
                    '    btnSave.Visible = False
                    'End If

                    ActivateEditTab()
                    If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, SubModuleAction.Edit) Then
                        btnSave.Visible = False
                    End If
                    '----------------------------------------------------------------------------------------------------

                    lblInfo.Text = "The record is successfully created."
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnSavePrdInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePrdInfo.Click
        Try
            lblSavePrdInfo.Text = ""
            Dim DT As DataTable
            Dim clsProduct As New mst_Product.clsProduct
            Dim strSelectedPrdCode As String = ViewState("PRD_CODE").ToString

            If strSelectedPrdCode <> "" Then
                If txtPrdInfo.Text.Length > 500 Then
                    lblSavePrdInfo.Text = "Only allow 500 characters"
                Else
                    clsProduct.CreatePrdInfo(Trim(txtPrdCode.Text), Trim(txtPrdInfo.Text))

                    lblSavePrdInfo.Text = "The record is saved successfully."
                End If
                
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub ddlSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSupplier.SelectedIndexChanged
        BindDDLPrdGrp()
    End Sub

    Protected Sub btnAddPrdGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPrdGrp.Click
        With (wuc_PrdGrpPop)
            .SupplierCode = ""
            .PrdGrpCode = ""
            .TeamCode = wuc_toolbar.TeamCode
            .LoadDvInsertMode()
            .BindDetailsView()
        End With
    End Sub

    Protected Sub btnPrdGrpPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdGrpPop.SaveButton_Click
        BindDDLPrdGrp()
        UpdateDatagrid.Update()
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsProduct As New mst_Product.clsProduct
            With (wuc_toolbar)
                DT = clsProduct.GetPrdList(.TeamCode, .SearchType, .SearchValue, .Status)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

            'ADD BUTTON EDIT
            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details-------------------------------------------------
            'If Master_Row_Count > 0 AndAlso _
            '    Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRODUCT, SubModuleAction.Edit) Then 'HL: 20080429 (AR)
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_ProductList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_ProductList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_ProductList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_ProductList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_ProductList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strSelectedPrdCode As String = sender.datakeys(e.NewEditIndex).item("PRD_CODE")
            Dim DT As DataTable
            Dim clsProduct As New mst_Product.clsProduct
            Dim lstSelected As ListItem
            lstSelected = Nothing

            DT = clsProduct.GetPrdDetails(strSelectedPrdCode)

            If DT.Rows.Count > 0 AndAlso DT.Rows(0)("LOCK_FLAG") = "0" Then
                ViewState("PRD_CODE") = strSelectedPrdCode

                'BindDDLSupplier()
                'ddlSupplier.SelectedIndex = -1
                'lstSelected = ddlSupplier.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("SUPPLIER_CODE")), "", DT.Rows(0)("SUPPLIER_CODE")))
                'If lstSelected IsNot Nothing Then lstSelected.Selected = True

                'BindDDLPrdGrp()
                'ddlPrdGrp.SelectedIndex = -1
                'lstSelected = ddlPrdGrp.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("PRD_GRP_CODE")), "", DT.Rows(0)("PRD_GRP_CODE")))
                'If lstSelected IsNot Nothing Then lstSelected.Selected = True

                txtPrdCode.Text = IIf(IsDBNull(DT.Rows(0)("PRD_CODE")), "", DT.Rows(0)("PRD_CODE"))
                txtPrdName.Text = IIf(IsDBNull(DT.Rows(0)("PRD_NAME")), "", DT.Rows(0)("PRD_NAME"))
                ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS")), "", DT.Rows(0)("STATUS"))
                txtAbbv.Text = IIf(IsDBNull(DT.Rows(0)("ABBV")), "", DT.Rows(0)("ABBV"))
                txtPrdInfo.Text = IIf(IsDBNull(DT.Rows(0)("PRODUCT_INFO")), "", DT.Rows(0)("PRODUCT_INFO"))

                ddlPoison.SelectedIndex = -1
                lstSelected = ddlPoison.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("POISON_IND")), "", DT.Rows(0)("POISON_IND")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                Dim sap_ind As String 'EUJIN CHECK IF ITS COME FROM SAP
                sap_ind = IIf(IsDBNull(DT.Rows(0)("SAP_IND")), "S", DT.Rows(0)("SAP_IND"))
                txtsapind.Text = sap_ind
                If sap_ind.ToUpper = "S" Then
                    txtPrdName.Enabled = False
                    ddlPoison.Enabled = False
                Else
                    txtPrdName.Enabled = True
                    ddlPoison.Enabled = True
                End If

                If LoadImgExists() Then
                    LoadImg()
                End If
                 
                ActivateEditTab()
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "<script>alert('This record is locked by other user!');</script>", False)
                RenewDataBind()
            End If

            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Product List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""

        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""

        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""

        TabPanel5.Visible = False
        TabPanel5.HeaderText = ""


        wuc_toolbar.show()
        lblInfo.Text = ""
        lblSavePrdInfo.Text = ""
    End Sub

    Private Sub ActivateAddTab()
        TabPanel1.HeaderText = "Product Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""

        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""

        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""

        TabPanel5.Visible = False
        TabPanel5.HeaderText = ""


        rfvSupplier.Enabled = True
        rfvPrdGrp.Enabled = True
        pnlPrdGrp.visible = True

        txtPrdName.Enabled = True
        ddlPoison.Enabled = True

        wuc_toolbar.hide()
        lblInfo.Text = ""
        lblSavePrdInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "Product Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = True
        TabPanel2.HeaderText = "Product Mapping"

        TabPanel3.Visible = True
        TabPanel3.HeaderText = "Product UOM"

        If LoadImgExists() Then
            TabPanel4.Visible = True
            TabPanel4.HeaderText = "Product Image"
            fraImageProduct.Attributes("src") = "ProductImage.aspx?PrdCode=" + Trim(txtPrdCode.Text)
            fraImageProduct.Visible = True
        Else
            fraImageProduct.Visible = False
        End If

        TabPanel5.Visible = True
        TabPanel5.HeaderText = "Product Info"


        'ddlSupplier.Enabled = False
        'ddlPrdGrp.Enabled = True
        rfvSupplier.Enabled = False
        rfvPrdGrp.Enabled = False
        pnlPrdGrp.visible = False
        txtPrdCode.Enabled = False

        wuc_ProductUOMList.PrdCode = Trim(txtPrdCode.Text)
        wuc_ProductMappingList.TeamCode = wuc_toolbar.TeamCode
        wuc_ProductMappingList.PrdCode = Trim(txtPrdCode.Text)
        RenewAllTab()

        wuc_toolbar.hide()
        lblInfo.Text = ""
        lblSavePrdInfo.Text = ""
    End Sub

    Private Sub RenewAllTab()
        wuc_ProductUOMList.RenewDataBind()
        wuc_ProductMappingList.RenewDataBind()
        UpdateDatagrid.Update()
    End Sub

    Private Sub RefreshAllTab()
        wuc_ProductUOMList.RefreshDataBind()
        wuc_ProductMappingList.RefreshDataBind()
        UpdateDatagrid.Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList)
            If TabPanel2.Visible Then
                aryDgList.Add(wuc_ProductMappingList.GetdgMappingList())
            End If
            If TabPanel3.Visible Then
                aryDgList.Add(wuc_ProductUOMList.GetdgUOMList())
            End If

            wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            If TabPanel2.Visible Then
                RefreshAllTab()
            End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "Prd Image"
    Private Sub LoadImg()
        Try

            Dim clsProduct As New mst_Product.clsProduct
            Dim dt As DataTable = Nothing

            Dim strUserId As String = Portal.UserSession.UserID
            Dim strPrdCode As String = txtPrdCode.Text.Trim
            dt = clsProduct.GetPrdImg(strPrdCode)
            If dt.Rows.Count > 0 Then
                LoadPrdImg(dt) 
                imgPrd.Visible = True
                btnDelete.Visible = True
            Else 
                imgPrd.Visible = False
                btnDelete.Visible = False
            End If
           
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub DeleteImg()
        Try

            Dim clsProduct As New mst_Product.clsProduct
            
            Dim strUserId As String = Portal.UserSession.UserID
            Dim strPrdCode As String = txtPrdCode.Text.Trim
            clsProduct.DeletePrdImg(strPrdCode) 

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadPrdImg(ByVal dt As DataTable)
        Try
            'Modified- Soo Fong -VN-100289: FFMA - Product Info is not displayed
            'Dim strGUID As String = System.Guid.NewGuid().ToString()
            Dim strPrdCode As String = Portal.Util.GetValue(Of String)(dt.Rows(0)("PRD_CODE")).Trim
            Dim strFilePath As String = Portal.Util.GetValue(Of String)(dt.Rows(0)("FILE_PATH")).Trim
            'Dim strfn As String = strPrdCode + "-" + Portal.UserSession.UserID + "-" + strGUID

            'If strfn <> "" Then
            '    strfn = System.Web.HttpContext.Current.Server.MapPath("~/") + "iFFMA\Product\StoreIMG\" + strfn + ".jpg"
            '    'e.x C:\somedirectory\*.tmp
            '    Dim namePattern As String = System.Web.HttpContext.Current.Server.MapPath("~/") + "iFFMA\Product\StoreIMG\"
            '    Dim strSearchPattern As String = Portal.Util.GetValue(Of String)(dt.Rows(0)("PRD_CODE")).Trim + "-" + Portal.UserSession.UserID + "*.jpg"
            '    For Each f As String In Directory.GetFiles(namePattern, strSearchPattern)
            '        File.Delete(f)
            '    Next

            '    'Dim TheFile As FileInfo = New FileInfo(strfn)
            '    'If TheFile.Exists Then
            '    '    File.Delete(strfn)
            '    'End If

            '    Dim fs As New FileStream(strfn, FileMode.CreateNew, FileAccess.Write)
            '    Dim blob As Byte() = DirectCast(Portal.Util.GetValue(Of Byte())(dt.Rows(0)("PRD_IMG")), Byte())
            '    fs.Write(blob, 0, blob.Length)
            '    imgPrd.ImageUrl = "~/iFFMA/Product/StoreIMG/" + Portal.Util.GetValue(Of String)(dt.Rows(0)("PRD_CODE")).Trim + "-" + Portal.UserSession.UserID + "-" + strGUID + ".jpg"
            '    imgPrd.Visible = True
            '    fs.Close()
            '    fs = Nothing
            'Else
            '    imgPrd.Visible = False
            'End If

            imgPrd.ImageUrl = "/" & System.Configuration.ConfigurationManager.AppSettings("ServerName") & strFilePath
            imgPrd.Visible = True

            UpdatePanelPhoto.Update()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub 

    Private Function LoadImgExists() As Boolean
        Dim strExists As Boolean = False
        Try
            Dim clsProduct As New mst_Product.clsProduct
            Dim dt As DataTable = Nothing
            Dim strPrdCode As String = txtPrdCode.Text.Trim
            dt = clsProduct.CheckPrdImgExists(strPrdCode)

            strExists = IIf(Portal.Util.GetValue(Of String)(dt.Rows(0)(0)) = 1, True, False)


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
        Return strExists

    End Function 

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        LoadImg()
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        DeleteImg()
        LoadImg()

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "<script>alert('Image deleted!');</script>", False)
    End Sub
#End Region


    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_ProductList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper

            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function



  
End Class


