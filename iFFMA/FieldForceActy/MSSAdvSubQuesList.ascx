<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MSSAdvSubQuesList.ascx.vb" Inherits="iFFMA_FieldForceActy_MSSAdvSubQuesList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvSubQuesPop" Src="MSSAdvSubQuesPopMandatory.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesPreviewPop" Src="MSSAdvQuesPreviewPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesPreview2Pop" Src="MSSAdvQuesPreview2Pop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlSubQues" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <span style="float:left; width:12%; " class="cls_label_header">Team </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblTeamCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <span style="float:left; width:12%; " class="cls_label_header">Title </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblTitleCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <span style="float:left; width:12%; " class="cls_label_header">Question </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblQuesCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br /><br />
                    <span class="cls_label_header" id="lblMaxSubQues" runat="server">Only Maximum 4 sub questions allowed.</span>        
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left:15px">
                     <asp:Panel ID="pnlCtrlAction" runat="server">
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                        <asp:Button ID="btnPreview" CssClass="cls_button" runat="server" Text="Preview" />
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <ccGV:clsGridView ID="dgSubQuesList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false">
                        </ccGV:clsGridView>
                    </center>
                </td>
            </tr>
        </table>
        
        <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
        <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
        <asp:HiddenField ID="hdQuesCode" runat="server" Value="" />
       
        <customToolkit:wuc_MSSAdvSubQuesPop ID="wuc_MSSAdvSubQuesPop" Title="MSS Sub Question Maintenance" runat="server" />
        <customToolkit:wuc_MSSAdvQuesPreviewPop ID="wuc_MSSAdvQuesPreviewPop" Title="MSS Question Preview" runat="server" />
        <customToolkit:wuc_MSSAdvQuesPreview2Pop ID="wuc_MSSAdvQuesPreview2Pop" Title="MSS Question Preview" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
