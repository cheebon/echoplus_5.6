﻿Imports System.Data
Imports IZ.WebFileManager
Imports System.IO

Partial Class iFFMA_SyncImage_SyncImageToDb
    Inherits System.Web.UI.Page

    Public Property TeamCode As String = String.Empty
    Public Property dtFullTeamCode As DataTable
    Public Property FolderPath As String = String.Empty
    Dim dt As New DataTable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.SYNCIMAGE)
                .DataBind()
                .Visible = True
            End With

            'Portal.UserSession.
            Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            FolderPath = "~/Documents/FileManagerRoot/" + strPrincipalCode 'Portal.PortalSession.UserProfile.User.PrincipalCode
            hfUserId.Value = Portal.UserSession.UserID 'Portal.PortalSession.UserProfile.User.UserID
            hfPrincipalId.Value = strPrincipalID 'Portal.PortalSession.UserProfile.User.PrincipalID
            hfAppName.Value = ConfigurationManager.AppSettings("ServerName").ToString()
            'lblUserName.Text = Portal.UserSession.UserName 'Portal.PortalSession.UserProfile.User.UserName

            Dim clsDocLib As New mst_Salesteam.clsSalesteam

            dtFullTeamCode = clsDocLib.GetSalesTeamFullList(Portal.UserSession.UserID)

            If Not IsPostBack Then
                dt.Clear()
                InitialiseFileManager()
            End If

        Catch ex As Exception
            ExceptionMsg(ex.ToString)
        End Try
    End Sub

    Public Sub InitialiseFileManager()
        Try
            FileManager1.Height = 800
            FileManager1.Width = 650
            FileManager1.RootDirectories.Clear()
            CheckFolderExistsElseCreate()

            Dim rootDirectory As New RootDirectory(),
                result = From t In dtFullTeamCode.AsEnumerable() Where t.Item("status") = "1" Select t.Item("team_code")

            With rootDirectory

                If result.Count = dtFullTeamCode.Rows.Count Then
                    hfTeamAccessStatus.Value = 0
                    .DirectoryPath = FolderPath
                    .Text = "Documents"
                ElseIf result.Count = 1 And result.Count < dtFullTeamCode.Rows.Count Then
                    .DirectoryPath = FolderPath + "/FileManagerRoot/" + result(0)
                    .Text = result(0)
                    hfTeamAccessStatus.Value = 1
                    hfTeamName.Value = result(0)
                ElseIf result.Count > 1 And result.Count < dtFullTeamCode.Rows.Count Then
                    .DirectoryPath = FolderPath + "/FileManagerRoot"
                    .Text = "FileManagerRoot"
                    hfTeamAccessStatus.Value = 2
                ElseIf result.Count = 0 Then
                    btnUpdateDocumentLibrary.Visible = False
                    .DirectoryPath = FolderPath
                    .Text = "Document"
                    hfTeamAccessStatus.Value = 3

                    'KL 20170113 - completely hide the Team folder
                    FileManager1.HiddenFilesAndFoldersPrefix = "FileManagerRoot"
                End If
            End With

            FileManager1.RootDirectories.Add(rootDirectory)
            Portal.UserSession.strSyncImageLoc = rootDirectory.DirectoryPath
        Catch ex As Exception
            ExceptionMsg(ex.Message)
        End Try
    End Sub

    Public Sub CheckFolderExistsElseCreate()
        Try
            Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")

            Dim path As String = "~/Documents/FileManagerRoot/" + strPrincipalCode
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/Customer"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/Contact"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/MSS"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/ProductCatalog"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/ProductCatalogRating"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/ProductImage"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If

            For Each row As DataRow In dtFullTeamCode.Rows
                If row("STATUS") = 1 Then
                    path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/FileManagerRoot/" + row("TEAM_CODE")
                    If Not Directory.Exists(Server.MapPath(path)) Then
                        Directory.CreateDirectory(Server.MapPath(path))
                    End If
                End If
            Next

            path = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/FileManagerRoot/Training"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
        Catch ex As Exception
            lblErr.Text = "An error has occurred. "
        End Try
    End Sub


    Protected Sub FileManager1_FolderRename(sender As Object, e As RenameCancelEventArgs)
        Dim filePath As String = e.FileManagerItem.VirtualPath,
                filePathSplit = filePath.Split("/"),
                folderName = filePathSplit.Last(),
                parentName = filePathSplit(filePathSplit.Length - 2)

        Dim rowIndex = dtFullTeamCode.AsEnumerable().ToList().FindIndex(Function(c) c.Item("team_code") = folderName)

        If (rowIndex > -1) Then
            Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")

            If dtFullTeamCode.AsEnumerable().ToList()(rowIndex)("status") = 0 _
            And parentName = "FileManagerRoot" _
            And Not folderName = strPrincipalCode Then 'Portal.PortalSession.UserProfile.User.PrincipalCode Then
                e.Cancel = True
                e.ClientMessage = "You've no permission to rename this folder."
            End If
        End If
    End Sub

    Protected Sub FileManager1_FolderCreate(sender As Object, e As NewFolderCancelEventArgs)
        Dim destinationPath As String = e.DestinationDirectory.VirtualPath,
            destinationPathSplit = destinationPath.Split("/"),
            folderName = destinationPathSplit.Last(),
            parentName = destinationPathSplit(destinationPathSplit.Length - 2)

        If hfTeamAccessStatus.Value <> 0 Then
            Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")
            If folderName = "FileManagerRoot" And parentName = strPrincipalCode Then 'Portal.PortalSession.UserProfile.User.PrincipalCode Then
                e.Cancel = True
                e.ClientMessage = "You're not allowed to create folder in Root Directory"
            End If
        End If
    End Sub

    Protected Sub FileManager1_SelectedItemsAction(sender As Object, e As SelectedItemsActionCancelEventArgs)
        Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")
        Select Case e.Action
            Case SelectedItemsAction.Open, SelectedItemsAction.Delete, SelectedItemsAction.Copy
                Dim filePath As String = e.SelectedItems(0).VirtualPath,
                filePathSplit = filePath.Split("/"),
                folderName = filePathSplit.Last(),
                parentName = filePathSplit(filePathSplit.Length - 2)

                Dim rowIndex = dtFullTeamCode.AsEnumerable().ToList().FindIndex(Function(c) c.Item("team_code") = folderName)
                If (rowIndex > -1) Then

                    If dtFullTeamCode.AsEnumerable().ToList()(rowIndex)("status") = 0 _
                    And parentName = "FileManagerRoot" _
                    And Not folderName = strPrincipalCode Then 'Portal.PortalSession.UserProfile.User.PrincipalCode Then
                        e.Cancel = True
                        Select Case e.Action
                            Case SelectedItemsAction.Open
                                e.ClientMessage = "You've no permission to access this folder."
                            Case SelectedItemsAction.Delete
                                e.ClientMessage = "You've no permission to delete this folder."
                            Case SelectedItemsAction.Copy
                                e.ClientMessage = "You've no permission to copy this folder."
                        End Select
                        Exit Sub
                    End If
                    'Else
                    '    Dim item As FileManagerItemInfo = e.SelectedItems(0)
                    '    Portal.PortalSession.UserProfile.User.SyncImageLoc = item.VirtualPath
                End If

                Dim item As FileManagerItemInfo = e.SelectedItems(0)

                'If delete, it will update path wth the filename (to be avoided)
                If Not e.Action = SelectedItemsAction.Delete Then
                    Portal.UserSession.strSyncImageLoc = item.VirtualPath
                End If

            Case SelectedItemsAction.Move
                Dim targetPath As String = e.SelectedItems(0).VirtualPath,
                destinationPath As String = e.DestinationDirectory.VirtualPath,
                destinationPathSplit = destinationPath.Split("/"),
                folderName = destinationPathSplit.Last(),
                parentName = destinationPathSplit(destinationPathSplit.Length - 2),
                targetPathSplit = targetPath.Split("/"),
                targetName = targetPathSplit.Last(),
                targetParentName = targetPathSplit(targetPathSplit.Length - 2)

                If folderName = "FileManagerRoot" And parentName = strPrincipalCode Then
                    e.Cancel = True
                    e.ClientMessage = "You're not allowed to create/move folder & files in Root Directory"
                    Exit Sub
                End If

                Dim folderIndex As Integer = -1,
                    targetIndex As Integer = -1
                If targetName.Contains(".") Then
                    'A File
                    folderIndex = dtFullTeamCode.AsEnumerable().ToList().FindIndex(Function(c) c.Item("team_code") = folderName)
                    If (folderIndex > -1) Then

                        If dtFullTeamCode.AsEnumerable().ToList()(folderIndex)("status") = 0 _
                        And parentName = "FileManagerRoot" _
                        And Not folderName = strPrincipalCode Then 'Portal.PortalSession.UserProfile.User.PrincipalCode Then
                            e.Cancel = True
                            e.ClientMessage = "You've no permission to access this folder."
                        End If
                    Else
                        Dim item As FileManagerItemInfo = e.SelectedItems(0)
                        Portal.UserSession.strSyncImageLoc = item.VirtualPath
                    End If
                Else
                    'A Folder
                    folderIndex = dtFullTeamCode.AsEnumerable().ToList().FindIndex(Function(c) c.Item("team_code") = folderName)
                    targetIndex = dtFullTeamCode.AsEnumerable().ToList().FindIndex(Function(c) c.Item("team_code") = targetName)

                    If (folderIndex > -1 Or targetIndex > -1) Then

                        Dim checkFolderIndex As Boolean = If(folderIndex > -1, dtFullTeamCode.AsEnumerable().ToList()(folderIndex)("status") = 0, False),
                            checkTargetIndex As Boolean = If(targetIndex > -1, dtFullTeamCode.AsEnumerable().ToList()(targetIndex)("status") = 0, False)

                        If (checkFolderIndex Or checkTargetIndex) _
                        And parentName = "FileManagerRoot" _
                        And Not folderName = strPrincipalCode Then 'Portal.PortalSession.UserProfile.User.PrincipalCode Then
                            e.Cancel = True
                            e.ClientMessage = "You've no permission to access this folder."
                        End If

                    End If
                End If
        End Select
    End Sub

    Protected Sub FileManager1_SelectedItemsActionComplete(sender As Object, e As SelectedItemsActionEventArgs)
    End Sub

    Protected Sub FileManager1_FileUploaded(sender As Object, e As UploadFileEventArgs)
    End Sub

    Protected Sub btnUpdateDocumentLibrary_Click(sender As Object, e As EventArgs) Handles btnUpdateDocumentLibrary.Click
        'Dim clsSyncToDb As New mst_Salesteam.clsSyncToDb,
        Dim clsDocLib As New mst_Salesteam.clsSalesteam
        Try
            Dim dt As New DataTable,
                dtShared As New DataTable,
                dtTeamCode As New DataTable,
                lstTeamCode As New List(Of String)

            With dt.Columns
                .Add("item_name", GetType(String))
                .Add("relative_path", GetType(String))
                .Add("team_code", GetType(String))
            End With
            With dtShared.Columns
                .Add("item_name", GetType(String))
                .Add("relative_path", GetType(String))
                .Add("team_code", GetType(String))
            End With

            Dim strFileRootPath = FolderPath + "\FileManagerRoot"

            If hfTeamAccessStatus.Value = 1 Then
                TeamCode = hfTeamName.Value
                DirSearch(strFileRootPath + "\" + hfTeamName.Value, dt)
            Else
                dtTeamCode = clsDocLib.GetSalesteamList(Portal.UserSession.UserID)

                For Each fName In Directory.GetDirectories(Server.MapPath(strFileRootPath))
                    TeamCode = Path.GetFileName(fName)
                    If (dtTeamCode.AsEnumerable().Any(Function(x) x.ItemArray.Contains(TeamCode) Or TeamCode = "Training")) Then
                        If TeamCode = "Training" Then
                            DirSearch(strFileRootPath + "\" + TeamCode, dtShared)
                        Else
                            lstTeamCode.Add(TeamCode)
                            DirSearch(strFileRootPath + "\" + TeamCode, dt)
                        End If
                    End If
                Next

                If dtShared.Rows.Count > 0 Then
                    For Each name In dtTeamCode.Rows
                        dtShared.AsEnumerable().ToList().ForEach(Function(c) InlineAssignHelper(c("team_code"), name(0)))
                        dt.Merge(dtShared)
                    Next
                End If
            End If

            Dim strItemName = String.Join("|", dt.AsEnumerable().[Select](Function(x) x("item_name").ToString()).ToArray()),
                strItemRelPath = String.Join("|", dt.AsEnumerable().[Select](Function(x) x("relative_path").ToString()).ToArray()),
                strItemTeamCode = String.Join("|", dt.AsEnumerable().[Select](Function(x) x("team_code").ToString()).ToArray())

            clsDocLib.GetSyncDocumentLibraryToDb(strItemName, strItemRelPath, strItemTeamCode, Portal.UserSession.UserID)

            lblMsg.InnerHtml = "Database updated"
            lblMsg.Visible = True
            updPnlMsg.Update()
        Catch ex As Exception
            ExceptionMsg(ex.Message)
        End Try
    End Sub

    Private Function InlineAssignHelper(Of T)(ByRef target As T, ByVal value As T) As T
        target = value
        Return value
    End Function

    Private Sub DirSearch(sDir As String, dTbl As DataTable)
        Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")
        Dim filePaths() As String = Directory.GetFiles(Server.MapPath(sDir))
        Dim i As Integer = 0
        Dim strItemCode As String = String.Empty
        For Each filePath As String In filePaths
            Dim dtRow As DataRow = dTbl.NewRow()
            strItemCode = Path.GetFileName(filePath)
            dtRow("item_name") = Path.GetFileName(filePath)
            Dim relPath As String = filePaths(i)
            dtRow("relative_path") = (relPath.Substring(relPath.IndexOf("Documents"), _
                                                        relPath.Length - relPath.IndexOf("Documents")) _
                                                        .Replace("\", "/").Replace("Documents/FileManagerRoot/", "") _
                                                        .Replace("/" + Path.GetFileName(filePath), "") _
                                                        .Replace(strPrincipalCode + "/FileManagerRoot", "")) + "/"
            dtRow("team_code") = TeamCode
            dTbl.Rows.Add(dtRow)
            i = i + 1
        Next

        For Each d As String In Directory.GetDirectories(Server.MapPath(sDir))
            d = "~/" + d.Substring(d.IndexOf("Documents"), d.Length - d.IndexOf("Documents")).Replace("\", "/")
            DirSearch(d, dTbl)
        Next

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub
End Class
