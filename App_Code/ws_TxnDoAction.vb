﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Web.Script.Serialization
Imports System.Runtime.Serialization.Json
Imports System.IO
Imports cor_DB
'Imports ws_Shared

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<System.Web.Script.Services.ScriptService()>
Public Class ws_TxnDoAction
    Inherits System.Web.Services.WebService


    Private strConn As String = Web.HttpContext.Current.Session("db_conn")
    'Private strCountryConn As String = Web.HttpContext.Current.Session(ConfigurationManager.AppSettings("PrincipalCon")) 'Web.HttpContext.Current.Session("country_db_conn")
    'Private strPath As String = Web.HttpContext.Current.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings("Attachment")) + "\"
    'Private strPath As String = "\\\\10.208.7.136\\FileAttachment\\Echoplus\\" + Session("CURRENT_PRINCIPAL_CODE") + "\\TRA\\"
    Private strPath As String = ConfigurationManager.AppSettings("UploadPath") + Context.Session("CURRENT_PRINCIPAL_CODE") + "\\TRA\\"
    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=System.Web.Script.Services.ResponseFormat.Json)> _
    <WebMethod(EnableSession:=True)> _
    Public Function UpdateTempAttachment(strTxnNo As String, strFileName As String) As String
        Try
            Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_TXN_TRA_ATTACHMENT_ADD"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "user_id", Session("UserID"), clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "txn_no", strTxnNo, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "filename", strFileName, clsCONN.dtType.dbString)
                    sqlCmd.ExecuteScalar()
                End Using
            End Using
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_TxnDoAction.UpdateTempAttachment :" & ex.Message))
        End Try

    End Function

    <System.Web.Script.Services.ScriptMethod(ResponseFormat:=System.Web.Script.Services.ResponseFormat.Json)> _
<WebMethod(EnableSession:=True)> _
    Public Function DeleteTempAttachment(strTxnNo As String, strFileName As String) As String
        Try
            Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_TXN_TRA_ATTACHMENT_DEL"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "user_id", Session("UserID"), clsCONN.dtType.dbINT)
                    clsCONN.addInputParam(sqlCmd, "txn_no", strTxnNo, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "filename", strFileName, clsCONN.dtType.dbString)
                    sqlCmd.ExecuteScalar()
                End Using
            End Using
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_TxnDoAction.DeleteTempAttachment :" & ex.Message))
        End Try
    End Function

#Region "Helper class"
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffms_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class
    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class
    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
#End Region

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class