Option Explicit On

Imports System.Data

Partial Class iFFMA_SAP_SAPDtl_v2
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection

    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SalesList")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SalesList") = value
        End Set
    End Property

    Public Property SapStatus() As String
        Get
            If IsNothing(ViewState("sapstatus")) Then
                Return String.Empty
            Else
                Return ViewState("sapstatus").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("sapstatus") = value
        End Set
    End Property

    Public Property SearchType() As String
        Get
            If IsNothing(ViewState("searchtype")) Then
                Return String.Empty
            Else
                Return ViewState("searchtype").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("searchtype") = value
        End Set
    End Property

    Public Property SearchValue() As String
        Get
            If IsNothing(ViewState("searchvalue")) Then
                Return String.Empty
            Else
                Return ViewState("searchvalue").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("searchvalue") = value
        End Set
    End Property

    Public Property SearchValue1() As String
        Get
            If IsNothing(ViewState("searchvalue1")) Then
                Return String.Empty
            Else
                Return ViewState("searchvalue1").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("searchvalue1") = value
        End Set
    End Property

    Public Property TxnNo() As String
        Get
            If IsNothing(ViewState("txnno")) Then
                Return String.Empty
            Else
                Return ViewState("txnno").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("txnno") = value
        End Set
    End Property

    Public Property DocType() As String
        Get
            If IsNothing(ViewState("doctype")) Then
                Return String.Empty
            Else
                Return ViewState("doctype").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("doctype") = value
        End Set
    End Property

    Public Property TxnTimestamp() As String
        Get
            If IsNothing(ViewState("txntimestamp")) Then
                Return String.Empty
            Else
                Return ViewState("txntimestamp").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("txntimestamp") = value
        End Set
    End Property

    Public Property Team() As String
        Get
            If IsNothing(ViewState("team")) Then
                Return String.Empty
            Else
                Return ViewState("team").ToString()
            End If
        End Get
        Set(ByVal value As String)
            ViewState("team") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim strTitle As String


        Try
            lblErr.Text = ""

            'Prepare Recordset,Createobject as needed
            strTitle = "SAP Details"
            'Call Header
            With wuc_lblheader
                .Title = strTitle '"SAP Details"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request
                AcceptIncomingRequest()
                SetHeader()
                SetDetail()

                If SapStatus.Trim = "" Then
                    cmdConfirm.Visible = True
                Else
                    cmdConfirm.Visible = False
                End If

            End If

            'If lblTxnStatusValue.Text = "P" And lblStatusValue.Text = "" Then
            '    btnCancel.Visible = True
            'Else
            '    btnCancel.Visible = False
            'End If

            'If lblTxnStatusValue.Text = "B" And lblStatusValue.Text = "" Then
            '    btnUnBlock.Visible = True
            'Else
            '    btnUnBlock.Visible = False
            'End If

            'If lblStatusValue.Text = "E" Then
            '    btnResubmit.Visible = True
            'Else
            '    btnResubmit.Visible = False
            'End If



        Catch ex As Exception
            ExceptionMsg("SAPDtl.Page_Load : " & ex.ToString)
        Finally
            objSAPQuery = Nothing
        End Try
    End Sub

#Region "Function"
    Private Function ReadCheckBox(ByVal obj As Object) As Boolean
        If IsDBNull(obj) Then
            Return False
        ElseIf IsNothing(obj) Then
            Return False
        ElseIf obj.ToString.Trim = "Y" Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function ReadValue(ByVal obj As Object) As String
        If IsDBNull(obj) Then
            Return String.Empty
        ElseIf IsNothing(obj) Then
            Return String.Empty
        Else
            Return obj.ToString.Trim
        End If
    End Function
    Private Sub AcceptIncomingRequest()
        DocType = Trim(Request.QueryString("doc_type"))
        SearchType = Trim(Request.QueryString("search_type"))
        SearchValue = Trim(Request.QueryString("search_value"))
        SearchValue1 = Trim(Request.QueryString("search_value_1"))
        TxnNo = Trim(Request.QueryString("txn_no"))
        Team = Trim(Request.QueryString("team"))
    End Sub
    Private Sub SetHeader()
        Dim dt As DataTable

        Try
            dt = RetrieveHeader()

            If dt.Rows.Count > 0 Then
                lblTxnNoValue.Text = ReadValue(dt.Rows(0)("txn_no"))
                lblTxnStatusValue.Text = ReadValue(dt.Rows(0)("txn_status"))
                lblSRCodeValue.Text = ReadValue(dt.Rows(0)("salesrep_code"))
                lblVisitIDValue.Text = ReadValue(dt.Rows(0)("visit_id"))
                lblTxnDateInValue.Text = ReadValue(dt.Rows(0)("txn_date_in"))
                lblTxnDateOutValue.Text = ReadValue(dt.Rows(0)("txn_date_out"))
                lblCustCodeValue.Text = ReadValue(dt.Rows(0)("cust_code"))
                lblContCodeValue.Text = ReadValue(dt.Rows(0)("cont_code"))
                lblPONoValue.Text = ReadValue(dt.Rows(0)("po_no"))
                lbllblParDelValue.Checked = ReadCheckBox(dt.Rows(0)("partial_dlvy"))
                lblRemarksValue.Text = ReadValue(dt.Rows(0)("remarks"))
                lblUrgentFlagValue.Checked = ReadCheckBox(dt.Rows(0)("urgent_flag"))
                lblOrdAmtValue.Text = ReadValue(dt.Rows(0)("ttl_ord_amt"))
                lblDiscAmtValue.Text = ReadValue(dt.Rows(0)("disc_amt"))
                lblCustDiscModeValue.Text = ReadValue(dt.Rows(0)("cust_disc_mode"))
                lblCustDiscAmtValue.Text = ReadValue(dt.Rows(0)("cust_disc_amt"))
                lblGstAmtValue.Text = ReadValue(dt.Rows(0)("gst_amt"))
                lblPaytermCodeValue.Text = ReadValue(dt.Rows(0)("pay_term_code"))
                lblPaytermNameValue.Text = ReadValue(dt.Rows(0)("pay_term_name"))
                lblPayAmtValue.Text = ReadValue(dt.Rows(0)("pay_amt"))
                lblPayDateValue.Text = ReadValue(dt.Rows(0)("pay_date"))
                lblSalesAreaCodeValue.Text = ReadValue(dt.Rows(0)("sales_area_code"))
                lblRouteCodeValue.Text = ReadValue(dt.Rows(0)("route_code"))
                lblShiptoCodeValue.Text = ReadValue(dt.Rows(0)("shipto_code"))
                lblShiptoDateValue.Text = ReadValue(dt.Rows(0)("shipto_date"))
                lblAdd1Value.Text = ReadValue(dt.Rows(0)("add_1"))
                lblAdd2Value.Text = ReadValue(dt.Rows(0)("add_2"))
                lblAdd3Value.Text = ReadValue(dt.Rows(0)("add_3"))
                lblAdd4Value.Text = ReadValue(dt.Rows(0)("add_4"))
                lblPostcodeValue.Text = ReadValue(dt.Rows(0)("postcode"))
                lblCityValue.Text = ReadValue(dt.Rows(0)("city"))
                lblNoteValue.Text = ReadValue(dt.Rows(0)("note"))

                SapStatus = ReadValue(dt.Rows(0)("SAP_STATUS"))
                TxnTimestamp = ReadValue(dt.Rows(0)("txn_timestamp"))

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub SetDetail()
        Try
            Bind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function RetrieveHeader() As DataTable
        Dim obj As New mst_SAP.clsSAPQuery
        Dim dt As DataTable

        Try
            With obj
                .clsProperties.doc_type = DocType
                .clsProperties.txn_no = TxnNo
                dt = .GetSAPDtl()
                'dtDtl = .GetSAPDtlList()
            End With
            Return dt
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Function
    Private Sub RetrieveInsertDetails()

    End Sub
    Private Sub ConfirmSAP()
        Dim obj As New mst_SAP.clsSAP

        Try
            obj.SubmitSAP(TxnNo, TxnTimestamp)
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Sub
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function
#End Region
#Region "dglist"
    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "Binding"
    Private Sub Bind()
        Try
            'If Wuc_txtDateStart.Text.Trim <> "" And Wuc_txtDateEnd.Text.Trim <> "" Then
            BindGrid(ViewState("SortCol"))
            'End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindGrid(ByVal SortExpression As String)
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = SortExpression

        Try

            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()
            '    ViewState("dtCurrentView") = dtCurrentTable
            '    SortingExpression = Nothing
            'End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                'PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            dtCurrentTable = Nothing
        End Try
    End Sub
    Private Function GetRecList() As DataTable
        Dim obj As mst_SAP.clsSAPQuery
        Dim dt As DataTable = Nothing

        Try
            obj = New mst_SAP.clsSAPQuery
            With obj
                .clsProperties.doc_type = DocType
                .clsProperties.txn_no = TxnNo
                dt = .GetSAPDtlList()
            End With
            If dt Is Nothing OrElse dt.Columns.Count = 0 Then Return dt

            PreRenderMode(dt)

        Catch ex As Exception
            ExceptionMsg("PckgAnalysis.GetRecList : " & ex.ToString)
        Finally
            'dt = Nothing
            obj = Nothing
        End Try
        Return dt
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
    End Sub
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()
        'ClearDGListColumns()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_SAPList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    'Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    'dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    'dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    'dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    'If String.IsNullOrEmpty(CF_SAPList.GetOutputFormatString(ColumnName)) = False Then
                    '    dgColumn.DataTextFormatString = CF_SAPList.GetOutputFormatString(ColumnName)
                    'End If
                    'dgColumn.ItemStyle.HorizontalAlign = CF_SAPList.ColumnStyle(ColumnName).HorizontalAlign
                    'dgColumn.ItemStyle.Wrap = CF_SAPList.ColumnStyle(ColumnName).Wrap

                    'dgColumn.HeaderText = CF_SAPList.GetDisplayColumnName(ColumnName)
                    'dgColumn.DataTextField = ColumnName
                    'dgColumn.SortExpression = ColumnName

                    'Dim strUrlFormatString As String
                    'Dim strUrlFields(1) As String
                    'Dim strUrlField(3) As String
                    'strUrlField(0) = "PACKAGE_CODE"
                    'strUrlField(1) = "PACKAGE_NAME"
                    'strUrlField(2) = "SALESREP_CODE"
                    'strUrlField(3) = "SALESREP_NAME"

                    'strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields)
                    'dgColumn.DataNavigateUrlFields = strUrlField
                    'dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    'dgColumn.Target = "_self"
                    'dgList.Columns.Add(dgColumn)
                    'dgColumn = Nothing

                    ''Add the field name
                    'aryDataItem.Add(ColumnName)

                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_SAPList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_SAPList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SAPList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SAPList.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SAPList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub
    Private Sub ClearDGListColumns()
        For i As Integer = dgList.Columns.Count - 1 To 0 Step -1
            dgList.Columns.RemoveAt(i)
        Next
    End Sub
    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "MTD*" Then
                strColumnName = "MTD Sales"
            ElseIf strColumnName Like "QTD*" Then
                strColumnName = "QTD Sales"
            ElseIf strColumnName Like "YTD*" Then
                If strColumnName Like "YTDGROSS" Then
                    strColumnName = "PYTD Sales"
                Else
                    strColumnName = "YTD Sales"
                End If
            ElseIf strColumnName Like "PYTD*" Then
                strColumnName = "PYTD Sales"
            Else
                strColumnName = CF_SAPList.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "*QTY") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub cmdConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdConfirm.Click
        Try
            ConfirmSAP()
            cmdConfirm.Visible = False
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "", "alert('Record updated!');", True)
        Catch ex As Exception
            ExceptionMsg("SAPDtl.cmdConfirm_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnViewTray_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTray.Click
        Response.Redirect("SAPList_v2.aspx?doc_type=" & Trim(DocType) & "&search_type=" & Trim(SearchType) & "&search_value=" & Trim(SearchValue) & "&search_value_1=" & Trim(SearchValue1) & "&team=" & Trim(Team), False)
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Response.Redirect("SAPDtl_v2.aspx?txn_no=" & Trim(TxnNo) & "&doc_type=" & Trim(DocType) & "&search_type=" & Trim(SearchType) & "&search_value=" & Trim(SearchValue) & "&search_value_1=" & Trim(SearchValue1) & "&team=" & Trim(Team), False)
    End Sub
End Class


Public Class CF_SAPList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'SALESREP_CODE ,SALESREP_NAME ,
        'CHAIN_CODE ,CHAIN_NAME ,CHANNEL_CODE ,CHANNEL_NAME ,
        'CUST_CODE ,CUST_NAME ,CUST_GRP_CODE ,CUST_GRP_NAME ,
        'PRD_CODE ,PRD_NAME ,PRD_GRP_CODE ,PRD_GRP_NAME ,
        'SALES_AREA_CODE ,SALES_AREA_NAME ,
        'SHIPTO_CODE ,SHIPTO_NAME

        'TEAM_CODE ,REGION_CODE ,REGION_NAME 
        'MTD_SALES ,MTD_FOC_QTY ,MTD_SALES_QTY ,MTD_TGT ,MTDVAR, 
        'QTD_SALES ,QTD_FOC_QTY ,QTD_SALES_QTY ,QTD_TGT ,QTDVAR ,
        'YTD_SALES ,YTD_FOC_QTY ,YTD_SALES_QTY ,YTD_TGT ,YTDVAR ,
        'PYTD_SALES,PYTD_FOC_QTY ,PYTD_SALES_QTY ,YTDGROSS

        Select Case ColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES"
                strFieldName = "Sales"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY"
                strFieldName = "FOC"
            Case "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strFieldName = "QTY"
            Case "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strFieldName = "Target"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strFieldName = "+/-%"
            Case "LINE_NO"
                strFieldName = "Line No"
            Case "ORD_TYPE"
                strFieldName = "ORD Type"
            Case "FOC_QTY"
                strFieldName = "FOC"
            Case "LIST_PRICE"
                strFieldName = "List Price"
            Case "DISC_AMT"
                strFieldName = "Disc AMT"
            Case "NET_PRICE"
                strFieldName = "Net Price"
            Case "BATCH_NO"
                strFieldName = "Batch No"
            Case "FOC_IND"
                strFieldName = "FOC Ind"
            Case "DISC_TYPE"
                strFieldName = "Disc Type"
            Case "DISC_REASON"
                strFieldName = "Disc Reason"
            Case "EAN_NO"
                strFieldName = "EAN No"
            Case "UOM_CODE"
                strFieldName = "UOM Code"
            Case "LINE_TYPE"
                strFieldName = "Line Type"
            Case "LIST_PRICE_IND"
                strFieldName = "List Price Ind"
            Case "PACKAGE_CODE"
                strFieldName = "Package Code"
            Case "PACKAGE_NAME"
                strFieldName = "Package"
            Case "PACKAGE_QTY"
                strFieldName = "Package QTY"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            Case "MTD_SALES"
                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESINFOBYDATE, "'1','8'") Then
                    FCT = FieldColumntype.HyperlinkColumn
                End If

            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                FCT = FieldColumntype.TemplateColumn_Percentage
        End Select
        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
                 "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strStringFormat = "{0:#,0.0}"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
                 "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strStringFormat = "{0:#,0}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class