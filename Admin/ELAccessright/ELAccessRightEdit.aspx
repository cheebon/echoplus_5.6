﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ELAccessRightEdit.aspx.vb"
    Inherits="Admin_ELAccessright_ELAccessRightEdit" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EL Accessright Edit</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>

    <script src="../../include/layout.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function AddNewUpdateControlAll(state) {
            if (AddNewUpdateCheckBoxIDs != null) {
                for (var i = 1; i < AddNewUpdateCheckBoxIDs.length; i++) {
                    var cb = document.getElementById(AddNewUpdateCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }
        function DeleteControlAll(state) {
            if (DeleteCheckBoxIDs != null) {
                for (var i = 1; i < DeleteCheckBoxIDs.length; i++) {
                    var cb = document.getElementById(DeleteCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }
        function FullRefreshControlAll(state) {
            if (FullRefreshCheckBoxIDs != null) {
                for (var i = 1; i < FullRefreshCheckBoxIDs.length; i++) {
                    var cb = document.getElementById(FullRefreshCheckBoxIDs[i]);
                    cb.checked = state;
                }
            }
        }

        function ValidateAddNewUpdateHeader(state) {

            if (AddNewUpdateCheckBoxIDs != null) {
                if (state) {
                    for (var i = 1; i < AddNewUpdateCheckBoxIDs.length; i++) {

                        var cb = document.getElementById(AddNewUpdateCheckBoxIDs[i]);
                        if (!cb.checked) {

                            document.getElementById(AddNewUpdateCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else {
                    document.getElementById(AddNewUpdateCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(AddNewUpdateCheckBoxIDs[0]).checked = true;
            }
        }
        function ValidateDeleteHeader(state) {
            if (DeleteCheckBoxIDs != null) {
                if (state) {
                    for (var i = 1; i < DeleteCheckBoxIDs.length; i++) {
                        var cb = document.getElementById(DeleteCheckBoxIDs[i]);
                        if (!cb.checked) {
                            document.getElementById(DeleteCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else {
                    document.getElementById(DeleteCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(DeleteCheckBoxIDs[0]).checked = true;
            }
        }
        function ValidateFullRefreshHeader(state) {
            if (FullRefreshCheckBoxIDs != null) {
                if (state) {
                    for (var i = 1; i < FullRefreshCheckBoxIDs.length; i++) {
                        var cb = document.getElementById(FullRefreshCheckBoxIDs[i]);
                        if (!cb.checked) {
                            document.getElementById(FullRefreshCheckBoxIDs[0]).checked = false;
                            return;
                        }
                    }
                }
                else {
                    document.getElementById(FullRefreshCheckBoxIDs[0]).checked = false;
                    return;
                }
                document.getElementById(FullRefreshCheckBoxIDs[0]).checked = true;
            }
        }
        
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmELAccessRightEdit" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" CombineScripts="False" />
    <fieldset class="" style="width: 98%; ">    
    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
    <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <customToolkit:wuc_lblInfo ID="lblErr" runat="server" Title="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <customToolkit:wuc_lblHeader ID="lblHeader" runat="server" Title="" />
    <div class="ContainerDiv" style="display: inline-block">
        <asp:UpdatePanel ID="updPnlAccessright" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="btnEdit" runat="server" Text="Edit Accessright details &nbsp;" OnClick="btnEdit_Click"
                    CssClass="cls_button" meta:resourcekey="btnEditResource1" />
                <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" Visible="False" />
                <asp:Button ID="btnDone" runat="server" Text="Done" CssClass="cls_button" Visible="False" />
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="cls_button" Visible="False" />
                <br />
                <table width="100%" cellpadding="0" cellspacing="10" align="left" border="0">
                    <tr>
                        <td valign="top" width="20%">
                            <div style="border-right: solid 1px #b9b8b8; padding: 0px 10px 10px 0px">
                                <asp:Label ID="Label2" CssClass="cls_label_header" runat="server" Text="Options : "
                                    meta:resourcekey="Label2Resource1"></asp:Label>
                                <br />
                                <br />
                                <ccGV:clsGridView ID="dgModuleList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    Width="360px" FreezeHeader="True" GridHeight="110px" AddEmptyHeaders="0" CellPadding="2"
                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                    DataKeyNames="cat_code,cat_name" OnRowCommand="dgModuleList_RowCommand" BorderColor="Black"
                                    BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" meta:resourcekey="dgModuleListResource1"
                                    RowHighlightColor="AntiqueWhite">
                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                    <Columns>
                                        <asp:BoundField DataField="cat_code" HeaderText="cat_code ID" ReadOnly="True" SortExpression="cat_code"
                                            Visible="False" meta:resourcekey="BoundFieldResource1">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:ButtonField DataTextField="cat_name" HeaderText="Category" CommandName="Select"
                                            AccessibleHeaderText="Select" meta:resourcekey="ButtonFieldResource1" />
                                    </Columns>
                                    <FooterStyle CssClass="GridFooter" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <PagerSettings Visible="False" />
                                    <RowStyle CssClass="GridNormal" />
                                </ccGV:clsGridView>
                                <!-- Reference part -->
                                <asp:Panel ID="pnlRef" runat="server" meta:resourcekey="pnlRefResource1">
                                    <div class="border_std" style="width: 358; padding: 2 0; border-top: solid 1px #b9b8b8;
                                        border-left: solid 1px #b9b8b8; border-right: solid 1px #b9b8b8; background-color: #2850a0;
                                        text-align: center">
                                        <span>
                                            <asp:Label ID="lblRef" CssClass="cls_label" runat="server" Text="Pick any accessright as a reference:"
                                                Style="color: #ffffff" meta:resourcekey="lblRefResource1"></asp:Label></span>
                                    </div>
                                    <div class="border_std" style="width: 348; padding-left: 10px; padding-top: 10px;
                                        padding-bottom: 10px; border: solid 1px #b9b8b8; background-color: #fafafa">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" CssClass="cls_label" runat="server" Text="Select Accessright : "
                                                        meta:resourcekey="Label1Resource1"></asp:Label>&nbsp;
                                                    <asp:DropDownList ID="ddlAccessright" CssClass="cls_dropdownlist" runat="server"
                                                        Width="150px" meta:resourcekey="ddlAccessrightResource1">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                    <asp:Button ID="btnRef" runat="server" Text="Reference" CssClass="cls_button" meta:resourcekey="btnRefResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <!-- Reference part end -->
                            </div>
                        </td>
                        <td valign="top" width="50%">
                            <asp:Label ID="lblMesg" runat="server" CssClass="cls_label" Text="&lt;br /&gt;&lt;br /&gt;"
                                meta:resourcekey="lblMesgResource1"></asp:Label>
                            <ccGV:clsGridView ID="dgSubModuleList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                Width="98%" FreezeHeader="True" GridHeight="100%" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                DataKeyNames="el_code" Visible="False" OnRowDataBound="dgSubModuleList_RowDataBound"
                                OnDataBound="dgSubModuleList_DataBound" BorderColor="Black" BorderWidth="1" GridBorderColor="Black"
                                GridBorderWidth="1px" meta:resourcekey="dgSubModuleListResource1" RowHighlightColor="AntiqueWhite">
                                <Columns>
                                    <asp:BoundField DataField="el_code" HeaderText="EL Code" ReadOnly="True" SortExpression="el_code"
                                        Visible="False" meta:resourcekey="BoundFieldResource2">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="el_name" HeaderText="Easy Loader Name" ReadOnly="True"
                                        meta:resourcekey="BoundFieldResource3">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="AddNew/Update" meta:resourcekey="TemplateFieldResource1">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="AddNewUpdateAllCheckBox" CssClass="cls_checkbox" Text="AddNew/Update"
                                                Style="color: White;" runat="server" meta:resourcekey="ViewAllCheckBoxResource1" />
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="AddNewUpdateCheckBox" meta:resourcekey="ViewCheckBoxResource1" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" meta:resourcekey="TemplateFieldResource2">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="DeleteAllCheckBox" CssClass="cls_checkbox" Text="Delete" Style="color: White;"
                                                runat="server" meta:resourcekey="CreateAllCheckBoxResource1" />
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="DeleteCheckBox" meta:resourcekey="CreateCheckBoxResource1" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FullRefresh" meta:resourcekey="TemplateFieldResource3">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="FullRefreshAllCheckBox" CssClass="cls_checkbox" Text="Full Refresh"
                                                Style="color: White;" runat="server" meta:resourcekey="EditAllCheckBoxResource1" />
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="FullRefreshCheckBox" meta:resourcekey="EditCheckBoxResource1" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="GridFooter" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAlternate" />
                                <RowStyle CssClass="GridNormal" />
                                <PagerSettings Visible="False" />
                            </ccGV:clsGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <br />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- Edit Accessright Details -->
    <asp:UpdatePanel ID="updPnlARDtl" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="btnShowPopup" runat="server" Style="display: none" meta:resourcekey="btnShowPopupResource1" />
            <ajaxToolkit:ModalPopupExtender ID="mdlARPopup" runat="server" TargetControlID="btnShowPopup"
                PopupControlID="pnlEditARPopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground"
                DynamicServicePath="" Enabled="True" />
            <asp:Panel ID="pnlEditARPopup" runat="server" Width="500px" Style="display: none"
                CssClass="modalPopup" meta:resourcekey="pnlEditARPopupResource1">
                <asp:UpdatePanel ID="updPnlARDetail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" cellpadding="2" cellspacing="0" align="left" border="0">
                            <tr>
                                <td colspan="3">
                                    <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                                        border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                                        text-align: center" meta:resourcekey="pnlMsgPopDragZoneResource1">
                                        <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="Edit Accessright Details"
                                            meta:resourcekey="lblTitleResource1" />
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <asp:Label ID="lblName" runat="server" CssClass="cls_label_header" Text="Name" meta:resourcekey="lblNameResource1"></asp:Label>
                                </td>
                                <td width="2%">
                                    <asp:Label ID="lblDot1" runat="server" CssClass="cls_label_header" meta:resourcekey="lblDot1Resource1">:</asp:Label>
                                </td>
                                <td>
                                    <span class="cls_label_required">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="cls_textbox" MaxLength="50" meta:resourcekey="txtNameResource1"></asp:TextBox></span>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                        ErrorMessage="Name cannot be Blank !" Display="Dynamic" CssClass="cls_label_validation"
                                        ValidationGroup="ARDetails" meta:resourcekey="rfvNameResource1"></asp:RequiredFieldValidator>
                                    <asp:Label ID="lblDupError" runat="server" Text="Name already exists!" Visible="False"
                                        CssClass="cls_label_validation" meta:resourcekey="lblDupErrorResource1"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <asp:Label ID="lblName1" runat="server" CssClass="cls_label_header" Text="Name 1"
                                        meta:resourcekey="lblName1Resource1"></asp:Label>
                                </td>
                                <td width="2%">
                                    <asp:Label ID="lblDot2" runat="server" CssClass="cls_label_header" meta:resourcekey="lblDot2Resource1">:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName1" runat="server" CssClass="cls_textbox" MaxLength="50" meta:resourcekey="txtName1Resource1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <asp:Label ID="lblDesc" runat="server" CssClass="cls_label_header" Text="Description"
                                        meta:resourcekey="lblDescResource1"></asp:Label>
                                </td>
                                <td width="2%">
                                    <asp:Label ID="lblDot3" runat="server" CssClass="cls_label_header" meta:resourcekey="lblDot3Resource1">:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDesc" runat="server" CssClass="cls_textbox" MaxLength="100" Width="300px"
                                        meta:resourcekey="txtDescResource1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <asp:Label ID="lblDesc1" runat="server" CssClass="cls_label_header" Text="Description 1"
                                        meta:resourcekey="lblDesc1Resource1"></asp:Label>
                                </td>
                                <td width="2%">
                                    <asp:Label ID="lblDot4" runat="server" CssClass="cls_label_header" meta:resourcekey="lblDot4Resource1">:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDesc1" runat="server" CssClass="cls_textbox" MaxLength="100"
                                        Width="300px" meta:resourcekey="txtDesc1Resource1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div style="text-align: center">
                                        <asp:Button ID="btnSaveARDetails" runat="server" Text="Save" Width="50px" OnClick="btnSaveARDetails_Click"
                                            ValidationGroup="ARDetails" CssClass="cls_button" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" CssClass="cls_button"
                                            OnClick="btnClose_Click" meta:resourcekey="btnCloseResource1" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </fieldset>
    </form>
</body>
</html>
