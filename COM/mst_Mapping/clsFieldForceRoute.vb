Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsFieldForceRoute
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsFieldForceRoute"
        End Get
    End Property

    Public Function GetRoutePlanningList(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strRouteCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetRoutePlanningExcludeList(ByVal strCustCode As String, ByVal strCustName As String, ByVal strContCode As String, ByVal strContName As String, _
                                                    ByVal strAddr1 As String, ByVal strAddr2 As String, ByVal strAddr3 As String, ByVal strAddr4 As String, _
                                                    ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strRouteCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_EXCLUDE_LIST"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, clsDB.DataType.DBString)
                .addItem("ADD_1", strAddr1, clsDB.DataType.DBString)
                .addItem("ADD_2", strAddr2, clsDB.DataType.DBString)
                .addItem("ADD_3", strAddr3, clsDB.DataType.DBString)
                .addItem("ADD_4", strAddr4, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub CreateRoutePlanning(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strRouteCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_CREATE"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub CreateRoutePlanningByCust(ByVal strCodeList As String, ByVal strVisitFreq As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strBCPTemplate As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_BY_CUST_CREATE"
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("VISIT_FREQ", strVisitFreq, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
                .addItem("BCP_TEMPLATE_ID", strBCPTemplate, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteRoutePlanning(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strRouteCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetCustByRouteDay(ByVal strSalesrep As String, ByVal strRouteCode As String, ByVal intNetValue As Integer, ByVal strDate As String) As DataTable
        Dim obj As clsDB
        Try
            obj = New clsDB
            With obj
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MAP_CUSTLIST_BY_ROUTE"
                .addItem("SALESREP_CODE", strSalesrep, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            obj = Nothing
        End Try
    End Function

    Public Function CheckPreviousRoutePlan(ByVal strSalesrep As String, ByVal strRouteCode As String) As DataTable
        Dim obj As clsDB
        Try
            obj = New clsDB
            With obj
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_SEQ_COMPARE"
                .addItem("SALESREP_CODE", strSalesrep, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            obj = Nothing
        End Try
    End Function
    Public Function GetPreviousRoutePlan(ByVal strSalesrep As String, ByVal strRouteCode As String) As DataTable
        Dim obj As clsDB
        Try
            obj = New clsDB
            With obj
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ROUTE_PLANNING_SEQ_LIST"
                .addItem("SALESREP_CODE", strSalesrep, clsDB.DataType.DBString)
                .addItem("ROUTE_CODE", strRouteCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            obj = Nothing
        End Try
    End Function
#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
