'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	14/05/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsStockAlloc
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsStockAlloc"
        End Get
    End Property

    Public Function GetStockAllocList(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STK_ALLOC_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetStockAllocDetails(ByVal strPrdCode As String, ByVal strSalesrepCode As String, ByVal strUomCode As String, ByVal strStartDate As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STK_ALLOC_DTL"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateStockAlloc(ByVal strPrdCode As String, ByVal strSalesrepCode As String, ByVal strUOMCode As String, ByVal strAllocQty As String, ByVal strStartDate As String, ByVal strEndDate As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STK_ALLOC_CREATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ALLOC_QTY ", strAllocQty, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateStockAlloc(ByVal strPrdCode As String, ByVal strSalesrepCode As String, ByVal strPKUOMCode As String, ByVal strPKStartDate As String, ByVal strUOMCode As String, ByVal strAllocQty As String, ByVal strStartDate As String, ByVal strEndDate As String) As DataTable
        Dim objDB As clsDB
        Dim DT As DataTable = Nothing

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STK_ALLOC_UPDATE"
                .addItem("PRD_CODE", strPrdCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_UOM_CODE", strPKUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_START_DATE", strPKStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUOMCode, cor_DB.clsDB.DataType.DBString)
                .addItem("ALLOC_QTY", strAllocQty, cor_DB.clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                DT = .spRetrieve()
            End With
        Catch ex As Exception
            'Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

        Return DT
    End Function

    Public Sub DeleteStockAlloc(ByVal strPrdCode As String, ByVal strSalesrepCode As String, ByVal strUomCode As String, ByVal strStartDate As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_STK_ALLOC_DELETE"
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



