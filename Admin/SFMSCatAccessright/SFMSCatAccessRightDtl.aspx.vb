﻿Imports Portal
Imports System.Data
Imports System.Threading
Imports System.Globalization

''' <summary>
'''  ************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-08-15
'''	Purpose	    :	Easy Loader Accessright Dtl
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>
''' 
Partial Class Admin_SFMSCatAccessright_SFMSCatAccessRightDtl
    Inherits System.Web.UI.Page
#Region "Local Properties"

    Public Property AccessrightID() As String
        Get
            Return ViewState("AccessrightID")
        End Get
        Set(ByVal value As String)
            ViewState("AccessrightID") = value
        End Set
    End Property

    Private Property UrlParams() As String
        Get
            Return ViewState("UrlParams")
        End Get
        Set(ByVal value As String)
            ViewState("UrlParams") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblErr.Text = ""
        updPnlErr.Update()

        Try

            lblHeader.Title = "Field Activity Category Accessright Detail"


            If Not Page.IsPostBack Then
                ' lblHeader.Title = "Accessright Details"
                UrlParams = "search_type=" & Trim(Request.QueryString("search_type")) & "&search_value=" & Trim(Request.QueryString("search_value"))
                AccessrightID = Trim(Request.QueryString("accessright_id"))

                If AccessrightID <> "" Then
                    LoadARDtl()

                    btnDelete.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.Delete)
                    btnEdit.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.Edit)
                Else
                    btnDelete.Visible = False
                    btnEdit.Visible = False
                End If


            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub LoadARDtl()
        Dim objAccessRightQuery As New adm_SFMSCatAccessRight.clsSFMSCatAccessRightQuery
        Dim dt, dtDtl As DataTable

        With objAccessRightQuery
            .clsQuery.accessright_id = Me.AccessrightID
            dt = .GetAccessRightDtl()
        End With

        With objAccessRightQuery
            .clsQuery.accessright_id = Me.AccessrightID
            .clsQuery.type = "1"
            dtDtl = .GetAccessRightDtlList()
        End With

        dvHdrLeft.DataSource = dt
        dvHdrLeft.DataBind()

        If dtDtl.Rows.Count = 0 Then
            Dim dr As DataRow = dtDtl.NewRow
            dtDtl.Rows.Add(dr)
        End If
        With dgARList
            .DataSource = dtDtl
            .DataBind()
        End With
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Update Panel"
    Private Sub UpdateErrorPanel()
        updPnlErr.Update()
    End Sub
    Private Sub UpdateARDtlPanel()
        updPnlARDtl.Update()
    End Sub
#End Region

    Protected Sub btnViewTray_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTray.Click
        Response.Redirect("SFMSCatAccessRightList.aspx?" & UrlParams, True)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objAccessRight As adm_SFMSCatAccessRight.clsSFMSCatAccessRight
        Try
            objAccessRight = New adm_SFMSCatAccessRight.clsSFMSCatAccessRight
            With objAccessRight
                .clsPpt.accessright_id = Me.AccessrightID
                .DeleteAR()
            End With

            Response.Redirect("SFMSCatAccessRightList.aspx?" & UrlParams, False)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Response.Redirect("SFMSCatAccessRightEdit.aspx?accessright_id=" & Me.AccessrightID & "&ori=edit&" & UrlParams, False)
    End Sub

End Class
