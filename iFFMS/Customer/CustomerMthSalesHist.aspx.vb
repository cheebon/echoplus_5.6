Imports System.Data

Partial Class CustomerMthSalesHist
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerSalesHistory"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        'Call Header
        With wuc_lblheader
            .Title = "Monthly Sales History"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If

    End Sub
    Sub onpageload()
        Try
            Dim strCustCode, strSalesmanCode As String
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            bindGridView(strSalesmanCode, strCustCode)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            onpageload()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getMthSalesHistDT(strSalesmanCode, strCustCode)
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                gridview1.DataSource = dtnew
                gridview1.DataBind()
                gridview1.Columns(5).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub addClickEvent(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gridview1.RowCreated
        Dim dt As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Dim strCustCode, strSalesmanCode As String
        Try
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            dt = obj.getMthSalesHistDT(strSalesmanCode, strCustCode)
            If dt.Rows.Count > 0 Then
                If e.Row.RowIndex <> -1 Then
                    Dim PostBack As String = Page.ClientScript.GetPostBackEventReference(gridview1, "Select$" & e.Row.RowIndex)
                    e.Row.Attributes.Add("OnClick", PostBack)
                    e.Row.Attributes.Add("Style", "cursor:hand")
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dg_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridview1.SelectedIndexChanged
        Dim strDocNo As String
        Try
            strDocNo = gridview1.SelectedRow.Cells(0).Text
            redirect(strDocNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".dg_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Sub redirect(ByVal strNo As String)
        Dim strSalesmanCode, strCustCode As String
        Dim dt As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getMthSalesHistDT(strSalesmanCode, strCustCode)
            If dt.Rows.Count > 0 Then
                Response.Redirect("CustomerMthSalesHistDetail.aspx?cycleno=" + strNo)
            Else
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub

    Sub gridview_Click(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand
        Dim strDocNo As String

        Try
            strDocNo = gridview1.Rows(e.CommandArgument).Cells(0).Text
            redirect(strDocNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".gridview_Click : " & ex.ToString)
        End Try

    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



