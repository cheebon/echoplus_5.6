'************************************************************************
'	Author	    :	YONG SOO FONG
'	Date	    :	26/02/2016
'	Purpose	    :	Detailing Sub Cat Pop
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data

Partial Class iFFMA_Detailing_DetailingSubCatPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property DetCode() As String
        Get
            Return Trim(hdDetCode.Value)
        End Get
        Set(ByVal value As String)
            hdDetCode.Value = value
        End Set
    End Property

    Public Property SubDetCode() As String
        Get
            Return Trim(hdSubDetCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubDetCode.Value = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True
        tcResult.ActiveTabIndex = 0
        TabPanel2.Visible = False
        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True
        TabPanel2.Visible = True
        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try

            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsDetailing As New mst_Detailing.clsDetailing

            DT = clsDetailing.GetDetailingActivityDetails(DetCode, SubDetCode)
            If DT.Rows.Count > 0 Then

                txtDate.DateStart = DT.Rows(0)("START_DATE")
                txtDate.DateEnd = DT.Rows(0)("END_DATE")

                lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("START_DATE")), "", DT.Rows(0)("START_DATE")))
                lblEndDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("END_DATE")), "", DT.Rows(0)("END_DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Show()

            DetailingExtraSubCatList.DetCode = DetCode
            DetailingExtraSubCatList.SubDetCode = SubDetCode
            DetailingExtraSubCatList.BindExtrCatCodeList()


            DetailingComplianceList.DetCode = DetCode
            DetailingComplianceList.SubDetCode = SubDetCode
            DetailingComplianceList.BindComplianceList()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Page.Validate()
            If Page.IsValid = False Then
                Show()
                Exit Sub
            End If

            Dim txtSubDetCode As TextBox = CType(DetailsView1.FindControl("txtSubDetCode"), TextBox)
            Dim lblSubDetCode As Label = CType(DetailsView1.FindControl("lblSubDetCode"), Label)
            Dim txtSubDetName As TextBox = CType(DetailsView1.FindControl("txtSubDetName"), TextBox)

            Dim clsDetailing As New mst_Detailing.clsDetailing

            'Update
            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsDetailing.UpdateDetailingActivity(DetCode, Trim(lblSubDetCode.Text), Trim(txtSubDetName.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()

            Else
                'Add
                Dim DT As DataTable
                Dim strIsDeleted As String = ""
                DT = clsDetailing.CreateDetailingActivity(DetCode, Trim(txtSubDetCode.Text), Trim(txtSubDetName.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd))
                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                    strIsDeleted = DT.Rows(0)("IS_DELETED")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Sub Category Code already exists!"
                    SubDetCode = ""
                    txtSubDetCode.Text = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                ElseIf strIsDeleted = "Y" Then
                    lblInfo.Text = "This Sub Category Code is used before, please create a new one!"
                    SubDetCode = ""
                    txtSubDetCode.Text = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    SubDetCode = Trim(txtSubDetCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            TabPanel2.Visible = True

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub imgCal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.imgCal_Click
        Show()
    End Sub

    Protected Sub ReloadTable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DetailingExtraSubCatList.SaveButton_Click
        DetailingExtraSubCatList.DetCode = DetCode
        DetailingExtraSubCatList.SubDetCode = SubDetCode
        DetailingExtraSubCatList.BindExtrCatCodeList()
    End Sub

    Protected Sub DisplayPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DetailingExtraSubCatList.DisplayMain_Click
        Show()
    End Sub

    Protected Sub imgClose_Click(sender As Object, e As ImageClickEventArgs) Handles imgClose.Click
        ModalPopupMaintenance.Hide()
    End Sub
End Class



