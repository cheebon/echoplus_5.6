'************************************************************************
'	Author	    :	Chee Kin Fatt
'	Date	    :	04/04/2007
'	Purpose	    :	Properties for Class PDA User 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

Public Class clsProperties

    Public Class clsPDAUser
        Public strMobileUserID As String
        Public strUserID As String
        Public strSalesRepCode As String
        Public strSalesRepName As String
        Public strPassword As String
        Public strMobileCode As String
        Public strCreatorUserID As String
        Public strCreatedDate As String
        Public strChangedUserID As String
        Public strChangedDate As String
        Public strSyncOperation As String
        Public strActiveFlag As String
        Public strAddress As String
        Public strContactNo As String
        Public strEmail As String
        Public strConnCode As String
        Public bChangePwd As Boolean
    End Class

End Class

