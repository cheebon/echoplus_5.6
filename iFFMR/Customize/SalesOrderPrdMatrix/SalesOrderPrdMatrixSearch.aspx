<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesOrderPrdMatrixSearch.aspx.vb"
    Inherits="iFFMR_Customize_SalesOrderPrdMatrix" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Order with Product Matrix</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
function launchCenter(url, name, height, width, addons) 
{
var str = "height=" + height + ",innerHeight=" + height;
str += ",width=" + width + ",innerWidth=" + width + addons;
if (window.screen) {
var ah = screen.availHeight - 30;
var aw = screen.availWidth - 10;
var xc = (aw - width) / 2;
var yc = (ah - height) / 2;
str += ",left=" + xc + ",screenX=" + xc;
str += ",top=" + yc + ",screenY=" + yc;
}
return window.open( url, name, str);
}
function OrdPrdMatrix_updateCustomerInfo(custCode,custName)
{
document.getElementById("txtCustCode").value = custCode
document.getElementById("txtCustName").value = custName
}
function OrdPrdMatrix_updateSalesrepInfo(SalesrepCode,SalesrepName)
{
document.getElementById("txtSalesrepCode").value = SalesrepCode
document.getElementById("txtSalesrepName").value = SalesrepName
}
function OrdPrdMatrix_updateProductInfo(ProductCode,ProductName)
{
document.getElementById("txtPrdCode").value = ProductCode
document.getElementById("txtPrdName").value = ProductName
}
function OrdPrdMatrix_openCustomer(){ myProduChild= launchCenter("../../../Core/Catalog/CustomerList.aspx?CATEGORY=ORDPRDMATRIX","", "600", "800", ",scrollbars=yes, resizable=yes");}
function OrdPrdMatrix_openSalesrep(){ myProduChild= launchCenter("../../../Core/Catalog/SalesrepList.aspx?CATEGORY=ORDPRDMATRIX","", "600", "800", ",scrollbars=yes, resizable=yes");}
function OrdPrdMatrix_openProduct(){myProduChild= launchCenter("../../../Core/Catalog/ProductList.aspx?CATEGORY=ORDPRDMATRIX","", "600", "800", ",scrollbars=yes, resizable=yes");}
function RefreshContentBar()
{
var TranNo  =  document.getElementById("txtTranNo").value;
var StartDate  =  document.getElementById("txtdatestart").value;
var EndDate  =  document.getElementById("txtdateend").value;
var SoNo  =  document.getElementById("txtSONo").value;
var TeamCode  =  document.getElementById("ddlteam").value;
var CustCode  =  document.getElementById("txtCustCode").value;
var PrdCode  =  document.getElementById("txtPrdCode").value;
var SalesrepCode  =  document.getElementById("txtSalesrepCode").value;
parent.document.getElementById('ContentBarIframe').src="../../iFFMR/Customize/SalesOrderPrdMatrix/SalesOrderPrdMatrixGrid.aspx?TXN_NO="+TranNo + "&Cust_Code=" + CustCode + "&Prd_Code=" + PrdCode + "&Salesrep_Code=" + SalesrepCode + "&Start_Date=" + StartDate  + "&End_Date=" + EndDate  + "&So_No=" + SoNo  + "&Team_Code=" + TeamCode;
}
function HideElement(element){var TopDiv =self.parent.document.getElementById(element);  TopDiv.style.display='none';}   
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmOrdPrdMatrix" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellspacing="1" cellpadding="1" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td colspan="5">
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                </td>
                            </tr>
                           
                            <tr>
                                <td align="left" width="15%">
                                    <span class="cls_label_header">Transaction Date :</span></td>
                                <td align="left">
                                    <%--<span class="cls_label_header">From :</span>&nbsp;--%>
                                    <asp:TextBox ID="txtdatestart" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                    <asp:ImageButton ID="imgDateStart" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                        CssClass="cls_button" CausesValidation="false" />
                                    <ajaxToolkit:CalendarExtender ID="ceexpdatestart" TargetControlID="txtdatestart"
                                        runat="server" Animated="false" Format="yyyy-MM-dd" PopupPosition="Right" PopupButtonID="imgDateStart">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:CustomValidator ID="rfvFormatDateStart" runat="server" Display="Dynamic" ControlToValidate="txtdatestart"
                                        CssClass="cls_validator" ValidateEmptyText="false" />
                                    <asp:CompareValidator ID="rfvCheckDataTypeStart" runat="server" CssClass="cls_validator"
                                        ControlToValidate="txtdatestart" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                        ErrorMessage=" Invalid Date!" />
                                    <span class="cls_label_header">To :</span>&nbsp;<asp:TextBox ID="txtdateend" runat="server"
                                        CssClass="cls_textbox"></asp:TextBox>
                                    <asp:ImageButton ID="imgDateEnd" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                        CssClass="cls_button" CausesValidation="false" />
                                    <ajaxToolkit:CalendarExtender ID="ceexpdateend" TargetControlID="txtdateend" runat="server"
                                        PopupButtonID="imgDateEnd" Animated="false" Format="yyyy-MM-dd" PopupPosition="right">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:CustomValidator ID="rfvFormatDateEnd" runat="server" Display="Dynamic" ControlToValidate="txtdateend"
                                        CssClass="cls_validator" ValidateEmptyText="false" />
                                    <asp:CompareValidator ID="rfvCheckDataTypeEnd" runat="server" CssClass="cls_validator"
                                        ControlToValidate="txtdateend" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                        ErrorMessage=" Invalid Date!" />
                                </td>
                                <td align="left" width="15%">
                                    <span class="cls_label_header">Field Force Code :</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtSalesrepCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                    &nbsp;<input id="btnsalesrepcode" class="cls_Button" onclick="OrdPrdMatrix_openSalesrep()"
                                        size="22" type="button" value="..." style="border-style: solid; border-width: thin;
                                        border-right: #666666 1px solid; border-top: #666666 1px solid; border-left: #666666 1px solid;
                                        border-bottom: #666666 1px solid; cursor: pointer; font-weight: normal; font-size: x-small;
                                        color: Black; font-family: Tahoma, Verdana, Arial;" />
                                </td>
                                  <td align="right" >
                                    <INPUT type=image  ID="Image1" runat="server" SRC="~/images/ico_close.gif"
                                        EnableViewState="false" onclick="HideElement('TopBar');"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="cls_label_header">Transaction No :</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtTranNo" runat="server" CssClass="cls_textbox" Width="200px"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <%--<span class="cls_label_header">Field Force Name :</span>--%>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtSalesrepName" runat="server" CssClass="cls_textbox" Width="250px"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="cls_label_header">Sales Order No :</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtSONo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                </td>
                                <td align="left">
                                    <span class="cls_label_header">Customer Code :</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                    &nbsp;<input id="brncustcode" class="cls_Button" onclick="OrdPrdMatrix_openCustomer()"
                                        size="22" type="button" value="..." style="border-style: solid; border-width: thin;
                                        border-right: #666666 1px solid; border-top: #666666 1px solid; border-left: #666666 1px solid;
                                        border-bottom: #666666 1px solid; cursor: pointer; font-weight: normal; font-size: x-small;
                                        color: Black; font-family: Tahoma, Verdana, Arial;" />
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <span class="cls_label_header">Team :</span></td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlteam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left">
                                    <%--<span class="cls_label_header">Customer Name :</span>--%>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" Width="250px"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <span class="cls_label_header">Product Code :</span></td>
                                <td align="left">
                                    <asp:TextBox ID="txtPrdCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                    &nbsp;<input id="btnprdcode" class="cls_Button" onclick="OrdPrdMatrix_openProduct()"
                                        size="22" type="button" value="..." style="border-style: solid; border-width: thin;
                                        border-right: #666666 1px solid; border-top: #666666 1px solid; border-left: #666666 1px solid;
                                        border-bottom: #666666 1px solid; cursor: pointer; font-weight: normal; font-size: x-small;
                                        color: Black; font-family: Tahoma, Verdana, Arial;" />
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="left">
                                    <%--<span class="cls_label_header">Product Name :</span>--%>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPrdName" runat="server" CssClass="cls_textbox" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                    <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" />
                                    <input id="btnrefresh" type="button" onclick="RefreshContentBar()" visible="false"
                                        value="Search" class="cls_button" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
