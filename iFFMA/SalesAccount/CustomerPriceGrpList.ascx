<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustomerPriceGrpList.ascx.vb" Inherits="iFFMA_SalesAccount_CustomerPriceGrpList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerPriceGrpPop" Src="CustomerPriceGrpPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlSalesteamAssign" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr><td>&nbsp;</td></tr>
            <asp:Panel ID="pnlCtrlAction" runat="server"> 
                <tr>
                    <td colspan="3" align="left" style="padding-left:15px">
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </td>
                </tr>
            </asp:Panel> 
            <tr><td align="center" style="width:95%;"><customToolkit:wuc_dgpaging ID="wuc_dgPriceGrpPaging" runat="server" /></td></tr>
            <tr>
                <td align="center" style="width:95%;">
                    <ccGV:clsGridView ID="dgPriceGrpList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
                <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
            </tr>
        </table>
        
        <customToolkit:wuc_CustomerPriceGrpPop ID="wuc_CustomerPriceGrpPop" Title="Customer Price Group Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
