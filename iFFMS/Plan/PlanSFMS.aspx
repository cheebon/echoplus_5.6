<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanSFMS.aspx.vb" Inherits="iFFMS_Plan_PlanSFMS" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plan Addon SFMS</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dglistNewSFMS');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
        function ChangeAllCheckBoxStates2(checkState)
   { var dglist = document.getElementById('dglistSelectedSFMS');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
       
     
                      function ValidatedglistNewSFMSCheckBoxStates()
   { var dglist = document.getElementById('dglistNewSFMS');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
        function ValidatedglistSelectedSFMSCheckBoxStates()
   { var dglist = document.getElementById('dglistSelectedSFMS');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('TopBar');HideElement('DetailBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe')"
    class="BckgroundInsideContentLayout">
    <form id="frmplanaddonSFMS" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" Visible="true"
                            Width="80px" OnClientClick="HideElement('ContentBar');ShowElement('TopBar'); MaximiseFrameHeight('TopBarIframe')" />
                    </div>
                    <div id="txnContent">
                        <asp:UpdatePanel ID="UpdateSelectedSFMS" runat="server" UpdateMode="Conditional"
                            RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlSelectedSFMS" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlSelectedSFMS_Tick" />
                                <div id="SelectedSFMS" class="S_DivHeader">
                                    Assigned Field Activity
                                </div>
                                <asp:Panel ID="pSelectedSFMS" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblSmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckalldelete" value="Check All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(true);" visible="false" style="width: 80px"
                                                validationgroup="deleteSFMS" runat="server" />
                                            <input type="Button" id="btnuncheckalldelete" value="Uncheck All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(false);" visible="false" style="width: 80px"
                                                validationgroup="deleteSFMS" runat="server" />
                                            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="cls_button" ValidationGroup="deleteSFMS"
                                                Visible="false" Width="80px" OnClientClick="return  ValidatedglistSelectedSFMSCheckBoxStates();" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistSelectedSFMS" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="CAT_CODE,SUB_CAT_CODE">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                 There is no data added.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkdelete" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CAT_CODE" HeaderText="Cat. code" ReadOnly="True" SortExpression="CAT_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CAT_NAME" HeaderText="Cat. Name" ReadOnly="True" SortExpression="CAT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SUB_CAT_CODE" HeaderText="Sub Cat. Code" ReadOnly="True"
                                                    SortExpression="SUB_CAT_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SUB_CAT_NAME" HeaderText="Sub Cat. Name" ReadOnly="True" SortExpression="SUB_CAT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                               
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeSelectedSFMS" runat="server" TargetControlID="pSelectedSFMS"
                                    ExpandControlID="SelectedSFMS" CollapseControlID="SelectedSFMS">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateNewSFMS" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlNewSFMS" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlNewSFMS_Tick" />
                                <div id="NewSFMS" class="S_DivHeader">
                                    Add New Field Activity
                                </div>
                                <asp:Panel ID="PNewSFMS" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblNmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                visible="false" style="width: 80px" validationgroup="newSFMS" runat="server" />
                                            <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                visible="false" style="width: 80px" validationgroup="newSFMS" runat="server" />
                                            <asp:Button ID="btnadd" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="newSFMS"
                                                Visible="false" Width="80px" OnClientClick="return ValidatedglistNewSFMSCheckBoxStates();" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 5px;">
                                        <span class="cls_label_header">Category:
                                            <asp:DropDownList ID="ddlcatcode" runat="server" CssClass="cls_dropdownlist" Width="150px"
                                                ValidationGroup="NewSFMS" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </span>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistNewSFMS" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="CAT_CODE,SUB_CAT_CODE">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkselect" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                   <asp:BoundField DataField="CAT_CODE" HeaderText="Cat. code" ReadOnly="True" SortExpression="CAT_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CAT_NAME" HeaderText="Cat. Name" ReadOnly="True" SortExpression="CAT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SUB_CAT_CODE" HeaderText="Sub Cat. Code" ReadOnly="True"
                                                    SortExpression="SUB_CAT_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SUB_CAT_NAME" HeaderText="Sub Cat. Name" ReadOnly="True" SortExpression="SUB_CAT_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeNewSFMS" runat="server" TargetControlID="PNewSFMS"
                                    ExpandControlID="NewSFMS" CollapseControlID="NewSFMS">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
