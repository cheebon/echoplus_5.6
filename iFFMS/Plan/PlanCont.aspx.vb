Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_Plan_PlanCont
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Public Enum dgcolSelectedCust As Integer
        ROUTE_DATE = 2
        CUST_CODE = 3
        CUST_NAME = 4
    End Enum

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountSelectedCont() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSelectedCont"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSelectedCont") = value
        End Set
    End Property

    Private Property Master_Row_CountNewCont() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountNewCont"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountNewCont") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Assign Contact" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControlSelectedCont.Enabled = True
            TimerControlNewCont.Enabled = True
        End If
    End Sub


#Region "DGLISTSelectedCont"
    Protected Sub TimerControlSelectedCont_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlSelectedCont.Tick
        If TimerControlSelectedCont.Enabled Then RefreshDatabindingSelectedCont()
        TimerControlSelectedCont.Enabled = False
    End Sub

    Public Sub RenewDataBindSelectedCont()
        RefreshDataBindSelectedCont()
    End Sub

    Public Sub RefreshDataBindSelectedCont()
        RefreshDatabindingSelectedCont()
    End Sub

    Public Sub RefreshDatabindingSelectedCont(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableSelectedCont As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionSelectedCont As String = CType(ViewState("strSortExpressionSelectedCont"), String)


        dtCurrentTableSelectedCont = GetRecListSelectedCont()

        If dtCurrentTableSelectedCont Is Nothing Then
            dtCurrentTableSelectedCont = New DataTable
        Else
            If dtCurrentTableSelectedCont.Rows.Count = 0 Then
                Master_Row_CountSelectedCont = 0
                btncheckalldelete.Visible = False
                btnuncheckalldelete.Visible = False
                btndelete.Visible = False
                'dtCurrentTableSelectedCont.Rows.Add(dtCurrentTableSelectedCont.NewRow())
            Else

                Master_Row_CountSelectedCont = dtCurrentTableSelectedCont.Rows.Count
                btncheckalldelete.Visible = True
                btnuncheckalldelete.Visible = True
                btndelete.Visible = True
            End If
        End If

        Dim dvCurrentViewSelectedCont As New DataView(dtCurrentTableSelectedCont)
        If Not String.IsNullOrEmpty(strSortExpressionSelectedCont) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionSelectedCont.Replace(" DESC", "")
            dvCurrentViewSelectedCont.Sort = IIf(dtCurrentTableSelectedCont.Columns.Contains(strSortExpressionSelectedCont), strSortExpressionSelectedCont, "")
        End If

        With dglistSelectedCont
            .DataSource = dvCurrentViewSelectedCont
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountSelectedCont > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateSelectedCont()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListSelectedCont() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strUserId As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonCont(strSalesrepCode, strRouteDate, strCustCode, strUserId)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateSelectedCont()

        UpdateSelectedCont.Update()

    End Sub

    Protected Sub dglistSelectedCont_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedCont.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedCont > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListSelectedCont_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistSelectedCont.Sorting
        Dim strSortExpressionSelectedCont As String = ViewState("strSortExpressionSelectedCont")

        If strSortExpressionSelectedCont IsNot Nothing AndAlso strSortExpressionSelectedCont.Length > 0 Then
            If strSortExpressionSelectedCont Like (e.SortExpression & "*") Then
                If strSortExpressionSelectedCont.IndexOf(" DESC") > 0 Then
                    strSortExpressionSelectedCont = e.SortExpression
                Else
                    strSortExpressionSelectedCont = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionSelectedCont = e.SortExpression
            End If
        Else
            strSortExpressionSelectedCont = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionSelectedCont") = strSortExpressionSelectedCont

        RefreshDatabindingSelectedCont()

    End Sub

#End Region

#Region "DGLISTNewCust"
    Protected Sub TimerControlNewCont_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlNewCont.Tick
        If TimerControlNewCont.Enabled Then RefreshDatabindingNewCont()
        TimerControlNewCont.Enabled = False
    End Sub

    Public Sub RenewDataBindNewCont()
        RefreshDataBindNewCont()
    End Sub

    Public Sub RefreshDataBindNewCont()
        RefreshDatabindingNewCont()
    End Sub

    Public Sub RefreshDatabindingNewCont(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableNewCont As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionNewCont As String = CType(ViewState("strSortExpressionNewCont"), String)


        dtCurrentTableNewCont = GetRecListNewCont()

        If dtCurrentTableNewCont Is Nothing Then
            dtCurrentTableNewCont = New DataTable
        Else
            If dtCurrentTableNewCont.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountSelectedCont = 0
                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btnadd.Visible = False
            Else
                Master_Row_CountSelectedCont = dtCurrentTableNewCont.Rows.Count
                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btnadd.Visible = True
            End If
        End If

        Dim dvCurrentViewNewCont As New DataView(dtCurrentTableNewCont)
        If Not String.IsNullOrEmpty(strSortExpressionNewCont) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionNewCont.Replace(" DESC", "")
            dvCurrentViewNewCont.Sort = IIf(dtCurrentTableNewCont.Columns.Contains(strSortExpressionNewCont), strSortExpressionNewCont, "")
        End If

        With dglistNewCont
            .DataSource = dvCurrentViewNewCont
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountNewCont > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateNewCont()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListNewCont() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strCustCode As String, strUserId As String, strRouteDate As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonContByCust(strSalesrepCode, strCustCode, strUserId, strRouteDate)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateNewCont()

        UpdateNewCont.Update()

    End Sub

    Protected Sub dglistNewCont_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistNewCont.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountNewCont > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListNewCont_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistNewCont.Sorting
        Dim strSortExpressionNewCont As String = ViewState("strSortExpressionNewCont")

        If strSortExpressionNewCont IsNot Nothing AndAlso strSortExpressionNewCont.Length > 0 Then
            If strSortExpressionNewCont Like (e.SortExpression & "*") Then
                If strSortExpressionNewCont.IndexOf(" DESC") > 0 Then
                    strSortExpressionNewCont = e.SortExpression
                Else
                    strSortExpressionNewCont = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionNewCont = e.SortExpression
            End If
        Else
            strSortExpressionNewCont = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionNewCont") = strSortExpressionNewCont

        RefreshDatabindingNewCont()

    End Sub

#End Region


#Region "Event Handler"

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        AddNewCont()
        TimerControlSelectedCont.Enabled = True
        TimerControlNewCont.Enabled = True
    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        DelNewCont()
        TimerControlSelectedCont.Enabled = True
        TimerControlNewCont.Enabled = True
    End Sub

#End Region

    Private Sub AddNewCont()
        Try
            If dglistNewCont.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strContCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistNewCont.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistNewCont.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strContCode = dglistNewCont.DataKeys(i).Item("CONT_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.InstPlanAddonCont(strSalesrepCode, strRouteDate, strCustCode, strContCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblNmsg.Text = "Record(s) added !"
            lblsmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelNewCont()
        Try
            If dglistSelectedCont.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strContCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistSelectedCont.Rows
                    chkSelected = CType(DR.FindControl("chkDelete"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistSelectedCont.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strContCode = dglistSelectedCont.DataKeys(i).Item("CONT_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.DelPlanAddonCont(strSalesrepCode, strRouteDate, strCustCode, strContCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblSmsg.Text = "Record(s) deleted !"
            lblNmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

   
End Class
