Imports system.data

Partial Class iFFMR_Customize_PckgAnalysis
    Inherits System.Web.UI.Page
#Region "Properties"
    Private intPageSize As Integer
    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection
    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SalesList")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SalesList") = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            If IsNothing(ViewState("startdate")) Or ViewState("startdate").ToString.Trim = "" Then
                Return DateTime.Now.ToString("yyyy-MM-dd")
            Else
                Return ViewState("startdate")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("startdate") = value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            If IsNothing(ViewState("enddate")) Or ViewState("enddate").ToString.Trim = "" Then
                Return DateTime.Now.ToString("yyyy-MM-dd")
            Else
                Return ViewState("enddate")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("enddate") = value
        End Set
    End Property

    Public Property Salesrep() As String
        Get
            If IsNothing(ViewState("salesrep")) Or ViewState("salesrep").ToString.Trim = "" Then
                Return ""
            Else
                Return ViewState("salesrep")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("salesrep") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblErr.Text = ""
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        With wuc_lblheader
            .Title = "Package Analysis Report"
            .DataBind()
            .Visible = True
        End With

        Try
            If Not Page.IsPostBack Then


                'With wuc_dgpaging
                '    .PageCount = dgList.PageCount
                '    .CurrentPageIndex = dgList.PageIndex
                '    .DataBind()
                '    .Visible = True
                'End With

                InitPage()
                AcceptIncomingRequest()
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Function"
    Private Sub InitPage()
        Try
            LoadSRDDL()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadSRDDL()
        Dim dt As DataTable
        Dim obj As New rpt_Customize.clsPackage

        Try
            dt = obj.GetSRList(Session("UserID").ToString, Session("PRINCIPAL_ID").ToString)

            Dim dr As DataRow = dt.NewRow
            dr("SALESREP_CODE") = ""
            dr("SALESREP_NAME") = "All Sales Rep"

            dt.Rows.InsertAt(dr, 0)

            With ddlSR
                .DataSource = dt
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub AcceptIncomingRequest()
        Dim startdate As String, enddate As String, salesrep As String

        startdate = Trim(Request.QueryString("startdate"))
        enddate = Trim(Request.QueryString("enddate"))
        salesrep = Trim(Request.QueryString("salesrep"))

        Wuc_txtDateStart.Text = IIf(startdate = "", DateTime.Now.ToString("yyyy-MM-dd"), startdate)
        Wuc_txtDateEnd.Text = IIf(enddate = "", DateTime.Now.ToString("yyyy-MM-dd"), enddate)
        ddlSR.SelectedValue = salesrep

        If startdate <> "" And enddate <> "" Then
            BindGrid(ViewState("SortCol"))
        End If

    End Sub
    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Dim StartDate, EndDate, Salesrep As String

        Select Case intIndex
            Case 0 'Generate sales info by date links
                StartDate = Wuc_txtDateStart.Text
                EndDate = Wuc_txtDateEnd.Text
                Salesrep = ddlSR.SelectedValue
                

                strUrlFormatString = "PckgAnalysisDtl.aspx?packagecode={0}&packagename={1}&salesrepcode={2}&salesrepname={3}&startdate=" & StartDate & "&enddate=" & EndDate & "&salesrep=" & Salesrep

            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select
        Return strUrlFormatString
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function
#End Region
#Region "dglist"
   
#End Region
#Region "Binding"
    Private Sub Bind()
        Try
            If Wuc_txtDateStart.Text.Trim <> "" And Wuc_txtDateEnd.Text.Trim <> "" Then
                BindGrid(ViewState("SortCol"))
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindGrid(ByVal SortExpression As String)
        Dim dt As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        'Dim strSortExpression As String = CType(SortingExpression, String)

        Try

            'If dtCurrentTable Is Nothing Then
            dt = GetRecList()

            If dt Is Nothing Then
                dt = New DataTable
            Else
                If dt.Rows.Count = 0 Then
                    dt.Rows.Add(dt.NewRow())
                End If
            End If

            dt.DefaultView.Sort = SortExpression


            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            ''Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount.ToString
            '    .CurrentPageIndex = dgList.PageIndex
            'End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            dt = Nothing
        End Try
    End Sub
    Private Function GetRecList() As DataTable
        Dim obj As rpt_Customize.clsPackage
        Dim dt As DataTable = Nothing

        Try
            obj = New rpt_Customize.clsPackage
            With obj
                dt = .GetPackageList(Wuc_txtDateStart.Text.Trim, Wuc_txtDateEnd.Text.Trim, ddlSR.SelectedValue)
            End With
            If dt Is Nothing OrElse dt.Columns.Count = 0 Then Return dt

            PreRenderMode(dt)

        Catch ex As Exception
            ExceptionMsg("PckgAnalysis.GetRecList : " & ex.ToString)
        Finally
            'dt = Nothing
            obj = Nothing
        End Try
        Return dt
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
    End Sub
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()
        'ClearDGListColumns()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_PackageList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_PackageList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_PackageList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_PackageList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_PackageList.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_PackageList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields(1) As String
                    Dim strUrlField(3) As String
                    strUrlField(0) = "PACKAGE_CODE"
                    strUrlField(1) = "PACKAGE_NAME"
                    strUrlField(2) = "SALESREP_CODE"
                    strUrlField(3) = "SALESREP_NAME"

                    strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields)
                    dgColumn.DataNavigateUrlFields = strUrlField
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_PackageList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_packageList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_PackageList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_PackageList.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_PackageList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub
    Private Sub ClearDGListColumns()
        For i As Integer = dgList.Columns.Count - 1 To 0 Step -1
            dgList.Columns.RemoveAt(i)
        Next
    End Sub
    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "MTD*" Then
                strColumnName = "MTD Sales"
            ElseIf strColumnName Like "QTD*" Then
                strColumnName = "QTD Sales"
            ElseIf strColumnName Like "YTD*" Then
                If strColumnName Like "YTDGROSS" Then
                    strColumnName = "PYTD Sales"
                Else
                    strColumnName = "YTD Sales"
                End If
            ElseIf strColumnName Like "PYTD*" Then
                strColumnName = "PYTD Sales"
            Else
                strColumnName = CF_PackageList.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "*QTY") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

#End Region
    '#Region "Paging"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnGo_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
    '        Try
    '            dgList.PageIndex = wuc_dgpaging.PageNo - 1

    '            dgList.EditIndex = -1
    '            BindGrid(ViewState("SortCol"))
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg("PckgAnalysis.btnGo_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkPrevious_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
    '        Try
    '            If dgList.PageIndex > 0 Then
    '                dgList.PageIndex = dgList.PageIndex - 1
    '            End If
    '            wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            BindGrid(ViewState("SortCol"))
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg("PckgAnalysis.lnkPrevious_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkNext_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
    '        Try
    '            If dgList.PageCount - 1 > dgList.PageIndex Then
    '                dgList.PageIndex = dgList.PageIndex + 1
    '            End If
    '            wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            BindGrid(ViewState("SortCol"))
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg("PckgAnalysis.lnkNext_OnClick : " + ex.ToString)
    '        End Try
    '    End Sub

    '#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Try
            Bind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class

Public Class CF_PackageList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'SALESREP_CODE ,SALESREP_NAME ,
        'CHAIN_CODE ,CHAIN_NAME ,CHANNEL_CODE ,CHANNEL_NAME ,
        'CUST_CODE ,CUST_NAME ,CUST_GRP_CODE ,CUST_GRP_NAME ,
        'PRD_CODE ,PRD_NAME ,PRD_GRP_CODE ,PRD_GRP_NAME ,
        'SALES_AREA_CODE ,SALES_AREA_NAME ,
        'SHIPTO_CODE ,SHIPTO_NAME

        'TEAM_CODE ,REGION_CODE ,REGION_NAME 
        'MTD_SALES ,MTD_FOC_QTY ,MTD_SALES_QTY ,MTD_TGT ,MTDVAR, 
        'QTD_SALES ,QTD_FOC_QTY ,QTD_SALES_QTY ,QTD_TGT ,QTDVAR ,
        'YTD_SALES ,YTD_FOC_QTY ,YTD_SALES_QTY ,YTD_TGT ,YTDVAR ,
        'PYTD_SALES,PYTD_FOC_QTY ,PYTD_SALES_QTY ,YTDGROSS

        Select Case ColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES"
                strFieldName = "Sales"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY"
                strFieldName = "FOC"
            Case "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strFieldName = "QTY"
            Case "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strFieldName = "Target"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strFieldName = "+/-%"
            Case "TXN_NO"
                strFieldName = "Txn No"
            Case "TXN_STATUS"
                strFieldName = "Txn Status"
            Case "SHIPTO_DATE"
                strFieldName = "Shipto Date"
            Case "VISIT_ID"
                strFieldName = "Visit ID"
            Case "TXN_DATE_IN"
                strFieldName = "Txn Date In"
            Case "TXN_DATE_OUT"
                strFieldName = "Txn Date Out"
            Case "TTL_ORD_AMT"
                strFieldName = "Ord Amt"
            Case "DISC_AMT"
                strFieldName = "Disc Amt"
            Case "PAY_AMT"
                strFieldName = "Pay Amt"
            Case "URGENT_FLAG"
                strFieldName = "Urgent Flag"
            Case "GST_AMT"
                strFieldName = "GST Amt"
            Case "PAY_TERM_NAME"
                strFieldName = "Payterm Name"
            Case "PAY_DATE"
                strFieldName = "Pay Date"
            Case "CUST_DISC_AMT"
                strFieldName = "Cust Disc Amt"
            Case "PARTIAL_DLVY"
                strFieldName = "Partial Dlvy"
            Case "PACKAGE_CODE"
                strFieldName = "Package Code"
            Case "PACKAGE_NAME"
                strFieldName = "Package Name"
            Case "PACKAGE_QTY"
                strFieldName = "Package Quantity"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            Case "PACKAGE_QTY"
                'If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESINFOBYDATE, "'1','8'") Then
                FCT = FieldColumntype.HyperlinkColumn
                'End If


        End Select
        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
                 "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strStringFormat = "{0:#,0.0}"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
                 "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strStringFormat = "{0:#,0}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class