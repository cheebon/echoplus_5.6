
Partial Class WebUserControl_wuc_MaxNoHits
    Inherits System.Web.UI.UserControl

    Public Property ddlNoRec_SelectedValue() As String
        Get
            Return Trim(ddlNoRec.SelectedValue)
        End Get
        Set(ByVal value As String)
            'ddlNoRec.SelectedValue = value
        End Set
    End Property

    Public Property SelectedIndex() As Integer
        Get
            Return ddlNoRec.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            ddlNoRec.SelectedIndex = value
        End Set
    End Property

End Class
