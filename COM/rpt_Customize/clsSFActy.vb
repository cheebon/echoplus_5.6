
'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	2008-06-10
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSFActy
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFActy"
        End Get
    End Property

    Public Function GetSFActyList() As DataTable
        Dim clsIntDB As clsDB
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_MTH_ACTIVITY_BY_MONTH"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", properties.TeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", properties.SalesrepCode, clsDB.DataType.DBString, True)

                GetSFActyList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFActyList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return GetSFActyList
    End Function
    Public Function GetCatName(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clstDB As clsDB
        Try
            clstDB = New clsDB
            With clstDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFMS_GET_SFMS_CAT_NAME"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCatName :" & ex.Message))
        Finally
            clstDB = Nothing
        End Try
        Return GetCatName
    End Function

    Public Function GetSFSFMSActyList() As DataTable
        Dim clsIntDB As clsDB
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_SFMS_ACTIVITY"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("MONTH", properties.Month, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", properties.TeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", properties.SalesrepCode, clsDB.DataType.DBString, True)

                GetSFSFMSActyList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFSFMSActyList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return GetSFSFMSActyList
    End Function


    '--=====================================================MONTHLY ACTIVITY BY PRODUCT--=====================================================



    Public Function GetSFActyListPrdDdl() As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_MTH_ACTIVITY_BY_MONTH_PRD_DDL"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString) 
                .addItem("TEAM_CODE", properties.TeamCode, clsDB.DataType.DBString, True)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFActyListPrdDdl :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    Public Function GetSFActyListPrd() As DataTable
        Dim clsIntDB As clsDB
        Dim dt As DataTable
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_MTH_ACTIVITY_BY_MONTH"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("YEAR", properties.Year, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", properties.TeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", properties.SalesrepCode, clsDB.DataType.DBString, True) 
                .addItem("PRODUCT_CODE", properties.ProductCode, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFActyListPrd :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return dt
    End Function

    '--=====================================================MONTHLY ACTIVITY BY PRODUCT--=====================================================
#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
