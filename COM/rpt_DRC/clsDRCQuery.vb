'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	30/10/2005
'	Purpose	    :	Class to build DRC Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsDRCQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    'Sub New()
    '    TableName = ""
    'End Sub

    'Sub New(ByVal strTableName As String)
    '    TableName = strTableName
    'End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDRCQuery"
        End Get
    End Property
    'SPP_RPT_DRC_LIST 
    '@USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, 
    '@YEAR, @MONTH, @TEAM_CODE, @SALESREP_CODE, @GRP_FIELD
    Public Function GetDRCList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strGroupField As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDRCList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    'SPP_RPT_DRC_ENQUIRY_LIST 
    '@USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @DATE_START, @DATE_END, @PRD_CODE, @CUST_CODE, @SALESREP_CODE
    Public Function GetDRCEnquiryList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strDateStart As String, ByVal strDateEnd As String, ByVal strPrdCode As String, ByVal strCustCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_ENQUIRY_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("DATE_START", strDateStart, clsDB.DataType.DBString)
                .addItem("DATE_END", strDateEnd, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDRCList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function


    Public Function GetDRCExtractionList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
   ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strGroupField As String, _
   ByVal strTeamList As String, ByVal strPrdGrpList As String) As DataTable
        Dim clsDRCDB As clsDB
        Try
            clsDRCDB = New clsDB
            With clsDRCDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_EXTRACTION_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TEAM_LIST", strTeamList, clsDB.DataType.DBString, True)
                .addItem("SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                .addItem("PRD_GRP_LIST", strPrdGrpList, clsDB.DataType.DBString, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDRCExtractionList :" & ex.Message))
        Finally
            clsDRCDB = Nothing
        End Try
    End Function

    Public Function GetPrdGrp(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strSalesrepList As String) As DataTable
        Dim clsDRCDB As clsDB
        Try
            clsDRCDB = New clsDB
            With clsDRCDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_DRC_GET_PRD_GRP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("SALESREP_LIST", strSalesrepList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdGrp :" & ex.Message))
        Finally
            clsDRCDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
