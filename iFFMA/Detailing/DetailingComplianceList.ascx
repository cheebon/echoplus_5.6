﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DetailingComplianceList.ascx.vb" Inherits="iFFMA_Detailing_DetailingComplianceList" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolKit" TagName="wuc_DetailingCompliancePop" Src="~/iFFMA/Detailing/DetailingCompliancePop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<asp:UpdatePanel ID="UpdatedgList" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                      <customtoolkit:wuc_dgpaging id="wuc_dgCompliancePaging" runat="server" />
                        <ccgv:clsgridview id="dgComplianceList" runat="server" allowsorting="True" autogeneratecolumns="False"
                            width="98%" freezeheader="True" gridheight="" addemptyheaders="0" cellpadding="2"
                            cssclass="Grid" emptyheaderclass="" freezecolumns="0" freezerows="0" gridwidth=""
                            showfooter="false" allowpaging="True" pagersettings-visible="false"> 
                        </ccgv:clsgridview>
                    </center>
                </td>
            </tr>
        </table>        
        <asp:HiddenField ID="hdDetCode" runat="server" Value="" />
        <asp:HiddenField ID="hdSubDetCode" runat="server" Value="" />
        <customToolkit:wuc_DetailingCompliancePop ID="wuc_DetailingCompliancePop" Title="Compliance Maintenance (Detailing)" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
