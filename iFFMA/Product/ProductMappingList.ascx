<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductMappingList.ascx.vb" Inherits="iFFMA_Product_ProductMappingList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ProductMappingPop" Src="ProductMappingPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlSalesteamAssign" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr><td>&nbsp;</td></tr>
            <asp:Panel ID="pnlCtrlAction" runat="server"> 
                <tr>
                    <td colspan="3" align="left" style="padding-left:15px">
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </td>
                </tr>
            </asp:Panel> 
            <tr><td align="center" style="width:95%;"><customToolkit:wuc_dgpaging ID="wuc_dgMappingPaging" runat="server" /></td></tr>
            <tr>
                <td align="center" style="width:95%;">
                    <ccGV:clsGridView ID="dgMappingList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
                <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                <asp:HiddenField ID="hdPrdCode" runat="server" Value="" />
            </tr>
        </table>
        
        <customToolkit:wuc_ProductMappingPop ID="wuc_ProductMappingPop" Title="Product Mapping Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
