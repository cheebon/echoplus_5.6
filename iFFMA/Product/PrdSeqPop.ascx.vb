﻿Imports System.Data

Partial Class iFFMA_Others_PrdSeqPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    ''' 
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property Product() As String
        Get
            Return Trim(hdProduct.Value)
        End Get
        Set(ByVal value As String)
            hdProduct.Value = value
        End Set
    End Property

    Public Property Seq() As String
        Get
            Return Trim(hdSeq.Value)
        End Get
        Set(ByVal value As String)
            hdSeq.Value = value
        End Set
    End Property

    Public Property IsEdit() As Boolean
        Get
            Return Trim(hdIsEdit.Value)
        End Get
        Set(ByVal value As Boolean)
            hdIsEdit.Value = value
        End Set
    End Property

#End Region
#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        txtPrd.Text = String.Empty
        txtSeq.Text = "0"
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdSearch)
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click
        Try
            txtPrd.Text = wuc_PrdSearch.PrdCode + " - " + wuc_PrdSearch.PrdName
            PrdCode = wuc_PrdSearch.PrdCode
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = String.Empty
            Dim seq As String = IIf(IsEdit, Trim(txtSeqForEdit.Text), Trim(txtSeq.Text))
            If (String.IsNullOrEmpty(PrdCode)) Then
                lblInfo.Text = "Required!"
                Show()
                Exit Sub
            ElseIf Not String.IsNullOrEmpty(seq) Then
                If Not IsNumeric(seq) Then
                    lblInfo.Text = "You've entered invalid value in Sequence field!"
                    Show()
                    Exit Sub
                ElseIf CInt(seq) < 0 Then
                    lblInfo.Text = "Negative value is not allowed in Sequence field!"
                    Show()
                    Exit Sub
                End If
            End If

            Dim DT As DataTable
            Dim clsPrdSeq As New mst_others.clsPrdSeq

            If (IsEdit) Then
                DT = clsPrdSeq.UpdatePrdSeq(PrdCode, Trim(txtSeqForEdit.Text))
            Else
                DT = clsPrdSeq.CreatePrdSeq(PrdCode, Trim(txtSeq.Text))
            End If

            Dim isDuplicatePrd, isDuplicateSeq As Integer

            If DT.Rows.Count > 0 Then
                isDuplicateSeq = DT.Rows(0)("IS_DUPLICATE_SEQ")
                If Not (IsEdit) Then
                    isDuplicatePrd = DT.Rows(0)("IS_DUPLICATE_PRD")
                End If
            End If

            If (isDuplicatePrd = 1 And isDuplicateSeq = 1) Then
                lblInfo.Text = "The product and sequence number has already exist!"
                Show()
                Exit Sub
            ElseIf (isDuplicatePrd = 1) Then
                lblInfo.Text = "The record has already exists!"
                Show()
                Exit Sub
            ElseIf (isDuplicateSeq = 1) Then
                lblInfo.Text = "The sequence number has already exists!"
                Show()
                Exit Sub
            Else
                lblInfo.Text = "The record is saved successfully."
            End If

            If Not (IsEdit) Then
                ResetPage()
            End If
            Show()

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub LoadInsertPanel()
        pnlInsert.Visible = True
        pnlEdit.Visible = False
        ResetPage()
    End Sub

    Public Sub LoadEditPanel()
        pnlEdit.Visible = True
        pnlInsert.Visible = False
        txtPrdForEdit.Text = Product
        txtSeqForEdit.Text = Seq
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
