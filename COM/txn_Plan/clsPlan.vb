'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	20/12/2006
'	Purpose	    :	Class to build datatables for getting plan details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPlan
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = Web.HttpContext.Current.Session("ffms_conn")

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPlan"
        End Get
    End Property

    Sub createSession(ByVal strSessionName As String, ByVal intNumOfCol As Integer)
        Dim dtTable As New DataTable(strSessionName)
        Try
            For intLoop As Integer = 1 To intNumOfCol
                Dim dcField As New DataColumn
                dcField.ColumnName = "Field" & CStr(intLoop)
                dcField.DataType = GetType(String)
                dtTable.Columns.Add(dcField)
                dcField.Dispose()
            Next
            Web.HttpContext.Current.Session(strSessionName) = dtTable
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".createSession :" & ex.Message))
        End Try

    End Sub

    Function DeptPlan(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTDEPT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".DeptPlan :" & ex.Message))
        End Try
    End Function

    Function contactPlan(ByVal strSalesmanCode As String, ByVal strCustCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTCONTACT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".contactPlan :" & ex.Message))
        End Try

    End Function

    Function productPlan() As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTPRODGRP"
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".productPlan :" & ex.Message))
        End Try

    End Function

    Function sfmsPlan(ByVal strSalesmanCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTSFMS"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".sfmsPlan :" & ex.Message))
        End Try

    End Function

    Function mssPlan(ByVal strSalesmanCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTMSS"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".mssPlan :" & ex.Message))
        End Try

    End Function

    Function promoCampPlan(ByVal strCustCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTPROMOCAMP"
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".promoCampPlan :" & ex.Message))
        End Try

    End Function

    Function getCustInfoDT(ByVal strCustCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANCUSTOMERLISTING"
                .addItem("custcode", strCustCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCustInfoDT :" & ex.Message))
        End Try

    End Function

    Function otherActivityPlan(ByVal strSalesmanCode As String) As DataTable
        Dim dt As DataTable = Nothing
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANLISTOTHER"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".otherActivityPlan :" & ex.Message))
        End Try

    End Function

    Sub insertPlan(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal dtmDate As Date, ByVal strType As String, ByVal strAnswer1 As String, Optional ByVal strAnswer2 As String = Nothing)
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                strAnswer2 = IIf(String.IsNullOrEmpty(strAnswer2), String.Empty, strAnswer2)
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANINSERT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("date", dtmDate, 3)
                .addItem("type", strType, 2)
                .addItem("answer1", strAnswer1, 2)
                .addItem("answer2", strAnswer2, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertPlan :" & ex.Message))
        End Try
    End Sub

    Sub deletePlan(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal dtmDate As Date)
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_PLANDELETE"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("routedate", dtmDate, 3)
                .addItem("custcode", strCustCode, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deletePlan :" & ex.Message))
        End Try
    End Sub
   
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
