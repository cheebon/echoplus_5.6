<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PackageFOCList.ascx.vb" Inherits="iFFMA_TradeDeal_PackageFOCList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PackageFOCPop" Src="PackageFOCPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlPackageFOC" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <customToolkit:wuc_dgpaging ID="wuc_dgFOCPaging" runat="server" />
                        <ccGV:clsGridView ID="dgFOCList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                        </ccGV:clsGridView>
                    </center>
                </td>
            </tr>
        </table>
                
        <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
        <asp:HiddenField ID="hdPackageCode" runat="server" Value="" />
        <asp:HiddenField ID="hdPackageType" runat="server" Value="" />
        <customToolkit:wuc_PackageFOCPop ID="wuc_PackageFOCPop" Title="Package FOC Maintenance" runat="server" />
        
    </ContentTemplate>
</asp:UpdatePanel>
