'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	SalesTeam User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlSalesTeam
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Private strPrincipalCode As String
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlSalesTeamID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlSalesTeamID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlSalesTeamID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlSalesTeamID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlSalesTeamID.SelectedValue = "0"
    '        Else
    '            ddlSalesTeamID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlSalesTeamID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return ddlSalesTeamID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlSalesTeamID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlSalesTeamID.SelectedValue = "0"
            Else
                ddlSalesTeamID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlSalesTeamID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlSalesTeamID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlSalesTeamID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlSalesTeamID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlSalesTeamID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlSalesTeamID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlSalesTeamID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlSalesTeamID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlSalesTeamID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvSalesTeamID.Visible 'cfvSalesTeamID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvSalesTeamID.Enabled = Value
            cfvSalesTeamID.Visible = Value
        End Set
    End Property

    Public Property PrincipalCode() As String
        Get
            Return strPrincipalCode
        End Get
        Set(ByVal Value As String)
            strPrincipalCode = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            'If ddlSalesTeamID.Items.Count <= 0 Then
            'Get SalesTeam List
            objCommonQuery = New cor_Common.clsCommonQuery
            With objCommonQuery
                .clsProperties.PrincipalCode = strPrincipalCode
                dt = .GetSalesTeamList
            End With
            objCommonQuery = Nothing

            ddlSalesTeamID.Items.Clear()
            ddlSalesTeamID.Items.Add(New ListItem("Select", 0))
            If dt.Rows.Count > 0 Then
                For Each drRow In dt.Rows
                    ddlSalesTeamID.Items.Add(New ListItem(Trim(drRow("salesteam_name")), Trim(drRow("salesteam_id")) & "@" & Trim(drRow("salesteam_code"))))
                Next
            End If
            'End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlSalesTeam.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


