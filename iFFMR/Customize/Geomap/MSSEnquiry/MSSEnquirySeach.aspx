﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MSSEnquirySeach.aspx.vb" Inherits="iFFMR_Customize_MSSEnquiry_MSSEnquirySeach" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSS Enquiry</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../../include/layout.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MapIconMaker/mapiconmaker2.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
        <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo&sensor=true" type="text/javascript"> </script>
    <script type='text/javascript'>
        /// Currently hard code color first. 
        var ClassA = "#bc0000";
        var ClassB = "#345bec";
        var ClassC = "#c88c18";
        var ClassD = "#3bba04";
        var Unclassified = "#999a9d";
        var marker;
        var markers = [];
        var map;
        var CustName, Address, District, CustGrpName, CustClass, CustType,
            TitleCode, QuesCode, SubQuesCode, StartDate, EndDate, MtdStart, MtdEnd,
            YtdStart, YtdEnd, NoSkuStart, NoSkuEnd, TeamCode, SalesrepCode, PrdCode;
        var Default_Latitude, Default_Longitude, Default_Zoomlevel;

        var ValidCoordinates = []; //Store valid coordinate \

        window.onresize = function() { resizeLayout2(); }

        /***********************************************************
        / Initialiazer
        /
        ************************************************************/
        function Initialize() {

//            if (GBrowserIsCompatible()) {
//                map = new GMap2(document.getElementById('map_canvas'));
            var mapOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE

                },
                mapTypeControl: true
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
                QueryLocation();

             //   AddControl();


          //  }
        }
        google.maps.event.addDomListener(window, 'load', Initialize);
        // Add a marker to the map and push to the array.
        function addMarker(Latitude, Longitude) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Latitude, Longitude),
                map: map,
                draggable: true
            });
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the overlays from the map, but keeps them in the array.
        function clearOverlays() {
            setAllMap(null);
        }

        // Shows any overlays currently in the array.
        function showOverlays() {
            setAllMap(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteOverlays() {
            clearOverlays();
        }
        function AddControl() {
            var mapTypeControl = new GMapTypeControl();
            var LargeMapControl3D = new GLargeMapControl3D();
            var bottomLeft = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 10));
            var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 10));

            map.addControl(mapTypeControl, bottomRight);
            map.addControl(LargeMapControl3D, bottomLeft);
            map.enableScrollWheelZoom();
        }

        function QueryLocation() {
            //map.setCenter(new google.maps.LatLng(25.324167, 109.160156), 3);
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {
            if (result) {
                if (result.Total > 0) {


                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;


                    SetMapDefaultPoint();

                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {

                var point = new google.maps.LatLng(Default_Latitude, Default_Longitude);
                map.setCenter(point, parseInt(Default_Zoomlevel));

            }
        }

        /***********************************************************
        / Method to search customer MSS.
        /
        ************************************************************/
        function SearchCustomerTitle() {

            ws_MSSEnq.ReturnCustomerMSS(CustName, Address, District, CustGrpName, CustClass, CustType, TitleCode, StartDate, EndDate,
                                            onSuccessLocateCustomerMSS, onFailLocateCustomerMSS);

            ShowMessage("Loading map data.");
        }

        function onSuccessLocateCustomerMSS(result) {
            if (result) {
                if (result.Total > 0) {
                    HideMessage();
                    PlotMap(result);
                    SetCenterMap();

                }
                else //Shit happens
                {

                    ShowMessage("No Record Returned.");

                    QueryLocation();
                }
            }
        }

        function onFailLocateCustomerMSS(ex) {
            //If webservice call fail
            ShowMessage("Error occurred when retrieving data." + ex.get_message());
        }

        function PlotMap(result) {
            var count = 0;
            deleteOverlays();
            WriteRecordCount(count);
            
            for (var i = 0; i < result.Total; ++i) {
                //Check coordinate contains value
                if ((result.Rows[i].LATITUDE != undefined && result.Rows[i].LATITUDE != "") && (result.Rows[i].LONGITUDE != undefined && result.Rows[i].LONGITUDE != "")) {
                    //check is numeric
                    if (!isNaN(result.Rows[i].LATITUDE) && !isNaN(result.Rows[i].LONGITUDE)) {
                        var icon = CreateIcon(result.Rows[i].CUST_CLASS);
                        alert(result.Rows[i].CUST_CLASS);
                         marker = CreateMarker(result.Rows[i].CUST_CODE, result.Rows[i].CUST_NAME,
                                                  result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE,
                                                  result.Rows[i].ADDRESS, result.Rows[i].DISTRICT,
                                                  result.Rows[i].CUST_GRP_NAME, result.Rows[i].CUST_TYPE,
                                                  result.Rows[i].CUST_CLASS, result.Rows[i].TXN_NO,
                                                  result.Rows[i].TXN_DATE, result.Rows[i].TITLE_CODE,
                                                  result.Rows[i].TITLE_NAME, icon);

                        marker.setMap(map);
                        WriteRecordCount(++count); //Display records plot on map
                        
                        ValidCoordinates.push(new google.maps.LatLng(result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE));
                    }
                    else
                    { ShowMessage("Certain customers have invalid coordinates."); }
                }
                else
                { ShowMessage("Certain customers does not have coordinates information."); }
            }
        }
        function WriteRecordCount(count) {
            $("#spnCount").text(count);
        }
        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateIcon(Class) {
            var Icon;

            switch (Class) {
                case "A":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassA, label: 'A' });
                    break;
                case "B":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassB, label: 'B' });
                    break;
                case "C":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassC, label: 'C' });
                    break;
                case "D":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassD, label: 'D' });
                    break;
                default:
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: Unclassified, label: 'O' });
                    break;
            }

            return Icon;
        }

        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateMarker(CustCode, CustName, Latitude, Longitude,
                            Address, District, CustGrpName, CustType,
                            Class, TxnNo, TxnDate, TitleCode, TitleName, Icon) {
            // marker = new GMarker(new google.maps.LatLng(Latitude, Longitude), Icon);
             marker = new google.maps.Marker({
                 position: new google.maps.LatLng(Latitude, Longitude),
                 map: map,
                 icon: Icon
             });
            google.maps.event.addListener(marker, "click", function() {
                var str = "<div class=\"cls_label\">";

                str += "Customer: " + CustName + "<br />";
                str += "District: " + District + "<br />";
                str += "Customer Class: " + GetClassName(Class) + "<br />";
                str += "Customer Type: " + CustType + "<br />";
                str += "Last MSS Date: " + TxnDate + "<br />";
                str += "Last Title Name: " + TitleName + "<br />";
                str += "<a href='javascript:NavigateAnswer(\"" + TxnNo + "\");'>View Details</a>";

                str += "</div>";

                /// Eye Candy Code. Pan marker to center then only show InfoWindow
                /// Just calling panning and InfoWindow sequentially cannot achieve such effects
                var moveEnd = google.maps.event.addListener(map, "moveend", function() {
                    marker.openInfoWindowHtml(str);
                    google.maps.event.removeListener(moveEnd);
                });

                map.panTo(marker.getLatLng());
                //////////////////////////////////////////////////////////////////////////////////
            }
            );

            return marker;
        }
        function GetClassName(Class) {
            switch (Class) {
                case "A":
                    return "A";
                    break;
                case "B":
                    return "B";
                    break;
                case "C":
                    return "C";
                    break;
                case "D":
                    return "D";
                    break;
                default:
                    return "OTH";
                    break;
            }
        }
        function SetCenterMap() {
            if (ValidCoordinates != undefined && ValidCoordinates.length > 0) {
                var iCenter = parseInt(ValidCoordinates.length / 2);

                map.setCenter(ValidCoordinates[iCenter], 10);
                map.setZoom(10);
            }
        }

        /*****************************************************************
        / Popup
        ******************************************************************/
        function togglePopup(divTarget, divMask, flag, fxCallback) {
            if (flag) {
                switch (flag) {
                    case "on":
                        mp_ToggleModalPopup(divTarget, divMask, 'on', fxCallback);

                        map.checkResize();
                        break;
                    case "off":
                        mp_ToggleModalPopup(divTarget, divMask, 'off', fxCallback);
                        break;
                }
            }
        }

        function NavigateAnswer(TxnNo, CustCode, Source) {
            $("#DetailBarIframe", parent.document).attr('src', "../../../iFFMR/Customize/Geomap/MSSEnquiry/MSSQuesList.aspx?txnno=" + TxnNo + "&custcode=" + CustCode + "&source=" + Source);
           // alert(item.count());
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('MapBar');HideElement('DetailBar');ShowElement('ContentBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');Initialize();" >
    <form id="frmMSSEnquiry" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">        
        <Services>     
            <asp:ServiceReference
           path="~/DataServices/ws_MSSEnq.asmx" />
           <asp:ServiceReference
           path="~/DataServices/ws_PrincipalLoc.asmx" />
        </Services>
    </ajaxToolkit:ToolkitScriptManager>
            
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                        <div style="display: none;">
                                            <asp:Button ID="btnClientRefresh" runat="server" Text="Button" /> 
                                            </div>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">                                                                
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                            Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" 
                                                                            GridWidth="" AllowPaging="True" DataKeyNames="TXN_NO,CUST_CODE" bordercolor="Black" 
                                                                            borderwidth="1" GridBorderColor="Black" GridBorderWidth="1px" 
                                                                            RowHighlightColor="AntiqueWhite">
                                                                                <Columns>
                                                                                    <asp:BoundField HeaderText="Customer Code" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CUST_CODE" SortExpression="CUST_CODE" Visible="false" />
                                                                                    <asp:BoundField HeaderText="Customer" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="CUST_NAME" SortExpression="CUST_NAME" />
                                                                                     <asp:BoundField HeaderText="Address" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="ADDRESS" SortExpression="ADDRESS" />
                                                                                     <asp:BoundField HeaderText="District" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="DISTRICT" SortExpression="DISTRICT" />
                                                                                     <asp:BoundField HeaderText="Customer Group" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="CUST_GRP_NAME" SortExpression="CUST_GRP_NAME" />
                                                                                     <asp:BoundField HeaderText="Cust Type" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CUST_TYPE" SortExpression="CUST_TYPE" />
                                                                                     <asp:BoundField HeaderText="Class" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CLASS" SortExpression="CLASS" />
                                                                                     <asp:BoundField HeaderText="Last MSS Date" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="TXN_DATE" SortExpression="TXN_DATE" DataFormatString="{0:yyyy-MM-dd}" />
                                                                                     <asp:BoundField HeaderText="Last MSS Title" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                                     HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="TITLE_NAME" SortExpression="TITLE_NAME" />
                                                                                     
                                                                                     <asp:TemplateField>
                                                                                        <HeaderTemplate>Action</HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkView" runat="server">View</asp:LinkButton>                                                                                            
                                                                                        </ItemTemplate>
                                                                                     </asp:TemplateField>
                                                                                    
                                                                                </Columns>
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                                <EmptyDataTemplate>
                                                                                    <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                </EmptyDataTemplate>
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                      
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>     
        <div id="pnlMapPopUp" class="modalPopup" style="z-index:9000;display: none; padding:10px; width:700px; height:450px; overflow:auto; position:absolute;">
            
            <input id="btnClose" type="button" value="Close" class="cls_button" OnClick="togglePopup('pnlMapPopUp','divModalMask', 'off');" />
                       
            <br />
            
            <div style="width:643px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898"><span class="cls_label" id="spnCount" style="font-weight:bold">0</span><span class="cls_label" style="font-weight:bold"> records plot on map.</span></div>
            <div id="map_canvas" style="width: 650px; height: 440px; padding: 0px 0px 0px 0px;z-index:20">
            <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAviZX_5ANQLbn5c6dZmkdkRSSF4RoMyN7orcJSVRUD19sX6pe3hQRouH06-TjWCldY1M8qE45jNoHDA" type="text/javascript"></script>--%>
           <%-- <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>" type="text/javascript"></script>--%>
            </div>
            <div id="Message" class="cls_label" style="width:644px; display:none; z-index:30; position:absolute; padding:3px; background:#3C3C3C; color: #ffffff"></div>
        </div>
        <div id="divModalMask" style="position:absolute; left:0; top:0; z-index: 8000; display:none; background-color:#fff"></div>   
    </form>
</body>
</html>
