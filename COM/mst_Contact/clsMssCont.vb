Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsMssCont
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))
    Private dsGen As DataSet

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMssContact"
        End Get
    End Property
#Region "MssContactList"

    Public Function GetMssContList(ByVal strTxnNo As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
    ByVal strTitleCode As String, ByVal strStatus As String, ByVal strDateStart As String, ByVal strDateEnd As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_HDR_LIST"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("DATE_START", strDateStart, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("DATE_END", strDateEnd, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContDtl(ByVal strTxnNo As String, ByVal strTitleCode As String, _
    ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_DTL_LIST"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContDtlcCol(ByVal StrCmdText As String, ByVal strColName As String, _
ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = StrCmdText
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("COL_NAME", strColName, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContCriteriaList(ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_CRITERIA_LIST"
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdMssContDtl(ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strQuesCode As String, _
    ByVal strSubQuesCode As String, ByVal strUpdateValue As String, ByVal strUpdateText As String, ByVal strUpdateType As String, ByVal strCriteriaCodeGrid As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_DTL_UPDATE"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE_GRID", strCriteriaCodeGrid, cor_DB.clsDB.DataType.DBString)
                .addItem("UPDATEVALUE", strUpdateValue, cor_DB.clsDB.DataType.DBString)
                .addItem("UPDATETEXT", strUpdateText, cor_DB.clsDB.DataType.DBString)
                .addItem("UPDATETYPE", strUpdateType, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub ApproveMssContDtl(ByVal strTxnNo As String, ByVal strTitleCode As String, ByVal strQuesCode As String, _
    ByVal strSubQuesCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCriteriaCodeGrid As String, ByVal strUpdateType As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_APPROVE"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CRITERIA_CODE_GRID", strCriteriaCodeGrid, cor_DB.clsDB.DataType.DBString)
                .addItem("UPDATETYPE", strUpdateType, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub ApproveMssContDtlDuplicate(ByVal strTxnNo As String, ByVal strTitleCode As String, _
   ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUpdateCustCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_APPROVE_DUPLICATE"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("UPDATE_CONT_CODE", strUpdateCustCode, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function ValidateMssContDtl(ByVal strTxnNo As String, ByVal strTitleCode As String,  ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_VALIDATE"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                dsGen = .spRetrieveDS
                Return dsGen.Tables(0)
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetDuplicatedList() As DataTable
        If dsGen.Tables.Count > 1 Then
            GetDuplicatedList = dsGen.Tables(1)
        Else
            GetDuplicatedList = Nothing
        End If
        Return GetDuplicatedList
    End Function

    Public Function GetActionDesc(ByVal strTxnNo As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
    ByVal strTitleCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_MSS_CONT_TXN_ACTION"
                .addItem("TXN_NO", strTxnNo, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "MssContactDtlList"
    Public Function GetMssContDtlList(ByVal strSpecialityName As String, ByVal strPositionCode As String, ByVal strDepartmentCode As String, ByVal strHospRankCode As String, _
       ByVal strClassification As String, ByVal strCustName As String, ByVal strContName As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_CONT_DTL"
                .addItem("SPECIALITY_NAME", strSpecialityName, cor_DB.clsDB.DataType.DBString)
                .addItem("POSITION_CODE", strPositionCode, cor_DB.clsDB.DataType.DBString)
                .addItem("DEPARTMENT_CODE", strDepartmentCode, cor_DB.clsDB.DataType.DBString)
                .addItem("HOSP_RANK_CODE", strHospRankCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CLASSIFICATION", strClassification, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContBTCList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_CONT_BTC_LIST"
                .addItem("SALESREP_cODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContPDSList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_CONT_PDS_LIST"
                .addItem("SALESREP_cODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContSpecialtyList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_CONT_SPECIALTY_LIST"
                .addItem("SALESREP_cODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContInvestmentList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_CONT_INVESTMENT_LIST"
                .addItem("SALESREP_cODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMssContPotentialInfoList(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_CONT_POTENTIAL_INFO_LIST"
                .addItem("SALESREP_cODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
