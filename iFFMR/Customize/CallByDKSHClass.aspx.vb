Imports System.Data

Partial Class iFFMR_Customize_CallByDKSHClass
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim _viewMode As ViewMode

    Protected Property SelectedViewMode() As ViewMode
        Get
            If ViewState("VIEWMODE") IsNot Nothing Then _viewMode = ViewState("VIEWMODE")
            Return _viewMode
        End Get
        Set(ByVal value As ViewMode)
            _viewMode = value
            ViewState("VIEWMODE") = value
        End Set
    End Property

    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CallByDKSHClass"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CALLBYDKSHCLASS)
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CALLBYDKSHCLASS
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                If PreviousPage IsNot Nothing Then
                    With CriteriaCollector
                        Session.Item("Year") = .Year
                        Session.Item("Month") = .Month
                        Session("PRINCIPAL_ID") = .PrincipalID
                        Session("PRINCIPAL_CODE") = .PrincipalCode
                        If .ReportType < ddlReportType.Items.Count Then ddlReportType.SelectedIndex = .ReportType
                        ViewState("strSortExpression") = .SortExpression
                        wuc_Menu.RestoreSessionState = True
                    End With
                Else
                    CriteriaCollector = Nothing ' New clsSharedValue
                End If
                TimerControl1.Enabled = True
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "CallByDKSHClass.aspx"
        End Get
    End Property

#Region "Event Handler"
    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            If IsPostBack Then RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 18 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Custom DGList"
   Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallByDKSHClass.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CallByDKSHClass.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataFormatString = CF_CallByDKSHClass.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallByDKSHClass.ColumnStyle(ColumnName).HorizontalAlign


                        dgColumn.HeaderText = CF_CallByDKSHClass.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer
                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next

                    'For All "ACH_"
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_A")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_COVERAGE_A"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_A"), licItemFigureCollector.FindByText("CONT_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_B")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_COVERAGE_B"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_B"), licItemFigureCollector.FindByText("CONT_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_C")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_COVERAGE_C"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_C"), licItemFigureCollector.FindByText("CONT_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_OTH")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_COVERAGE_OTH"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_OTH"), licItemFigureCollector.FindByText("CONT_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_TTL")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_COVERAGE_TTL"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_TTL"), licItemFigureCollector.FindByText("CONT_TTL")) * 100))

                    e.Row.Cells(aryDataItem.IndexOf("ACH_HIT_A")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_HIT_A"), (DIVISION(licItemFigureCollector.FindByText("HIT_A"), licItemFigureCollector.FindByText("CONT_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_HIT_B")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_HIT_B"), (DIVISION(licItemFigureCollector.FindByText("HIT_B"), licItemFigureCollector.FindByText("CONT_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_HIT_C")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_HIT_C"), (DIVISION(licItemFigureCollector.FindByText("HIT_C"), licItemFigureCollector.FindByText("CONT_C")) * 100))

                    If SelectedViewMode = ViewMode.GroupByMR Then
                        e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_A")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_CALL_A"), (DIVISION(licItemFigureCollector.FindByText("CALL_A"), licItemFigureCollector.FindByText("CALL_TGT_A"))))
                        e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_B")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_CALL_B"), (DIVISION(licItemFigureCollector.FindByText("CALL_B"), licItemFigureCollector.FindByText("CALL_TGT_B"))))
                        e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_C")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_CALL_C"), (DIVISION(licItemFigureCollector.FindByText("CALL_C"), licItemFigureCollector.FindByText("CALL_TGT_C"))))
                        e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_OTH")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_CALL_OTH"), (DIVISION(licItemFigureCollector.FindByText("CALL_OTH"), licItemFigureCollector.FindByText("CALL_TGT_OTH"))))
                        e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_TTL")).Text = String.Format(CF_CallByDKSHClass.GetOutputFormatString("ACH_CALL_TTL"), (DIVISION(licItemFigureCollector.FindByText("CALL_TTL"), licItemFigureCollector.FindByText("CALL_TGT_TTL"))))
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth As String
            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")

            Dim strSalesRepList As String
            strSalesRepList = Session("SALESREP_LIST")

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .GroupField = ""
                .PrincipalID = strPrincipalID
                .PrincipalCode = strPrincipalCode
                .Year = strYear
                .Month = strMonth
                .ReportType = ddlReportType.SelectedIndex
            End With

            Dim clsCallByDKSHClass As New rpt_Customize.clsCallByDKSHClass
            DT = clsCallByDKSHClass.GetCallByDKSHClass(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, ddlReportType.SelectedValue)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_CustomerHeader()
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "*CONT_*" Then
                    strColumnName = "Total Contact"
                ElseIf strColumnName Like "*COVERAGE_*" Then
                    strColumnName = "Coverage"
                ElseIf strColumnName Like "*CALL_TGT_*" Then
                    strColumnName = "Target Call"
                ElseIf strColumnName Like "*CALL_*" Then
                    strColumnName = "Actual Call"
                ElseIf strColumnName Like "*HIT_*" Then
                    strColumnName = "Hit Call"
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except ACH_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If (strColumnName Like "*_WD" OrElse _
                 strColumnName Like "*_CD*" OrElse _
                 strColumnName Like "*_A" OrElse strColumnName Like "*_B" OrElse _
                 strColumnName Like "*_C" OrElse strColumnName Like "*_OTH" OrElse _
                 strColumnName Like "*_TTL") AndAlso Not strColumnName Like "ACH_*" Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            SelectedViewMode = ddlReportType.SelectedIndex
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Public Enum ViewMode
        GroupByMR = 0
        ExpandCust = 1
    End Enum

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_CallByDKSHClass
    Public Shared Function FilterPrefixName(ByVal columnName As String) As String
        Dim strName As String = columnName
        Try
            If Not String.IsNullOrEmpty(columnName) AndAlso Not columnName Like "SALESREP_CODE" Then
                strName = columnName.ToUpper.Replace("CONT_", "").Replace("COVERAGE_", "").Replace("CALL_TGT_", "").Replace("CALL_", "").Replace("HIT_", "")
                If strName.StartsWith("ACH_") Then
                    strName = "ACH"
                End If
            End If
        Catch ex As Exception

        End Try
        Return strName
    End Function

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strNewName As String = FilterPrefixName(ColumnName.ToUpper)
        
        Select Case strNewName
            Case "NSM_CODE"
                strFieldName = "NSM Code"
            Case "NSM_NAME"
                strFieldName = "NSM"
            Case "DSM_CODE"
                strFieldName = "DSM Code"
            Case "DSM_NAME"
                strFieldName = "DSM"
            Case "FTE_WD"
                strFieldName = "WD"
            Case "FTE_CD"
                strFieldName = "CD"
            Case "CUST_COUNT"
                strFieldName = "Total Customer"
            Case "ACH"
                strFieldName = "%"
            Case "A"
                strFieldName = "A"
            Case "B"
                strFieldName = "B"
            Case "C"
                strFieldName = "C"
            Case "OTH"
                strFieldName = "OTH"
            Case "TTL"
                strFieldName = "Total"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper

            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)
        Try

            Select Case strNewName
                Case "ACH", "CD_HALF"
                    strStringFormat = "{0:0.0}"
                Case "A", "B", "C", "OTH", "TTL", "FTE_WD", "FTE_CD", "CUST_COUNT"
                    strStringFormat = "{0:0.#}"
                Case Else
                    strStringFormat = ""
            End Select

            If strColumnName Like "ACH*" Then strStringFormat = "{0:0.0}"
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With
        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class










