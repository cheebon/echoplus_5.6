Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext

Partial Class iFFMS_Leave_LeaveList
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection
    Private Const LEAVE_BACKDATE As String = "LEAVE_BACKDATE"

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Public Enum dgcol As Integer
        REASON_CODE = 1
        REASON_NAME = 2
        START_DATE = 3
        END_DATE = 4
        TXN_STATUS = 5
        VISIT_ID = 6
        REMARKS = 7
    End Enum
    
    
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Leave" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then
            If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

            GetBackFutureDate()



            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
            txtstartdate.Attributes.Add("onblur", "formatDate('" & txtstartdate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            txtenddate.Attributes.Add("onblur", "formatDate('" & txtenddate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            'cvtxtenddate3.DataBind()
            'CVtxtstartdate3.databind()
            LoadReasonCode()
            txtstartdate.Text = Format(Now(), "yyyy-MM-dd")
            txtenddate.Text = Format(Now(), "yyyy-MM-dd")
            TimerControlLeave.Enabled = True

        End If
    End Sub

#Region "DGLIST"

    Protected Sub TimerControlLeave_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControlLeave.Enabled Then

            RefreshDatabinding()

        End If
        TimerControlLeave.Enabled = False
    End Sub

    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        'RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        '  Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        ViewState("strSortExpression") = Nothing
        'ViewState("dtCurrentView") = dtCurrentTable
        dglist.PageIndex = 0
        'End If

        If dtCurrentTable.Rows.Count = 0 Then
            'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            btncheckall.Visible = False
            btnuncheckall.Visible = False
            btndeleteBatch.Visible = False
            Master_Row_Count = 0
        Else
            Master_Row_Count = dtCurrentTable.Rows.Count
            btncheckall.Visible = True
            btnuncheckall.Visible = True
            btndeleteBatch.Visible = True
        End If

        btnSubmitBatch.Visible = False 'controlled on row databound


        Dim dvCurrentView As New Data.DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dglist.DataSource = dvCurrentView
        dglist.PageSize = 20 'intPageSize
        dglist.DataBind()

        ''Call Paging
        'With wuc_dgpaging
        '    .PageCount = dgList.PageCount
        '    .CurrentPageIndex = dgList.PageIndex
        'End With

        'wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        ' Finally
        UpdateDatagrid_Update()

        '  End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dglist.Rows.Count < 15 Then dglist.GridHeight = Nothing
        UpdateLeave.Update()
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        '  Try
        Dim dt As Data.DataTable = GetRecList()
        dt.Rows.Add(dt.NewRow)
        ViewState("dtCurrentView") = dt
        RefreshDatabinding()
        ' Catch ex As Exception
        'ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        '  End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim clsLeave As New txn_WebActy.clsLeave

        Dim strSalesrepCode As String
        strSalesrepCode = Session("SALESREP_CODE")

        DT = clsLeave.GetLeaveList(strSalesrepCode)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        'ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        ' End Try
        Return DT
    End Function


    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglist.RowDataBound

        Select Case e.Row.RowType
            Case DataControlRowType.DataRow

                If Master_Row_Count > 0 Then
                    Dim strstatusCode As String
                    strstatusCode = Trim(e.Row.Cells(dgcol.TXN_STATUS).Text)
                    If strstatusCode.ToUpper = "K" Then
                        Dim chkdelete As CheckBox = CType(e.Row.FindControl("chkdelete"), CheckBox)
                        chkdelete.Enabled = True
                        btnSubmitBatch.Visible = True
                        'Dim btnedit As ImageButton = CType(e.Row.FindControl("btnedit"), ImageButton)
                        'btnedit.Visible = True
                    Else
                        Dim chkdelete As CheckBox = CType(e.Row.FindControl("chkdelete"), CheckBox)
                        chkdelete.Enabled = False
                    End If

                End If
        End Select
    End Sub
#End Region

#Region "Back/Future Date"
    Private Sub GetBackFutureDate()
        Dim dt As DataTable
        Dim blnBackdateFlag As Boolean = False
        Try
            'Set them off first
            CVBackStartDate.Enabled = False
            CVFutureStartDate.Enabled = False
            CVEndFutureDate.Enabled = False

            Dim objConfig = New txn_WebActy.clsCommon

            dt = objConfig.GetAdmConfig(LEAVE_BACKDATE, Session("UserID"))

            If dt IsNot Nothing Then
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("VALUE").ToString = "1" Then
                        blnBackdateFlag = True
                    Else
                        blnBackdateFlag = False
                    End If
                Else
                    blnBackdateFlag = False
                End If
            Else
                blnBackdateFlag = False
            End If

            If blnBackdateFlag Then
                'CVBackStartDate.ValueToCompare = DateTime.Now.AddDays(Convert.ToInt32(IIf(dt.Rows(0)("VALUE2").ToString = "", 0, dt.Rows(0)("VALUE2").ToString)) / -1).ToString("yyy-MM-dd")
                'CVFutureStartDate.ValueToCompare = DateTime.Now.AddDays(Convert.ToInt32(IIf(dt.Rows(0)("VALUE3").ToString = "", 0, dt.Rows(0)("VALUE3").ToString))).ToString("yyy-MM-dd")
                'CVEndFutureDate.ValueToCompare = DateTime.Now.AddDays(Convert.ToInt32(IIf(dt.Rows(0)("VALUE3").ToString = "", 0, dt.Rows(0)("VALUE3").ToString))).ToString("yyy-MM-dd")
                txtStartDateVal.Text = DateTime.Now.AddDays(Convert.ToInt32(IIf(dt.Rows(0)("VALUE2").ToString = "", 0, dt.Rows(0)("VALUE2").ToString)) / -1).ToString("yyy-MM-dd")
                txtEndDateVal.Text = DateTime.Now.AddDays(Convert.ToInt32(IIf(dt.Rows(0)("VALUE3").ToString = "", 0, dt.Rows(0)("VALUE3").ToString))).ToString("yyy-MM-dd")

                CVBackStartDate.Enabled = True
                CVFutureStartDate.Enabled = True
                CVEndFutureDate.Enabled = True
            Else
                CVBackStartDate.Enabled = False
                CVFutureStartDate.Enabled = False
                CVEndFutureDate.Enabled = False
            End If

        Catch ex As Exception
            ExceptionMsg("LeaveList.GetVisitAdmConfig : " & ex.ToString)
        End Try
    End Sub
#End Region
    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadReasonCode()
        Dim dtReasonCode As DataTable
        Dim clsLeave As New txn_WebActy.clsLeave

        Dim strSalesrepCode As String
        strSalesrepCode = Session("SALESREP_CODE")

        dtReasonCode = clsLeave.GetLeaveReasonCode(strSalesrepCode)
        With ddlReasonCode
            .Items.Clear()
            .DataSource = dtReasonCode.DefaultView
            .DataTextField = "REASON_NAME"
            .DataValueField = "REASON_CODE"
            .DataBind()
            .Items.Insert(0, New ListItem("-- SELECT --", ""))
            .SelectedIndex = 0
        End With
    End Sub

#Region "Event Handler"
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Page.IsValid Then
            Dim dt As DataTable
            Dim clsLeave As New txn_WebActy.clsLeave

            Dim strSalesrepCode As String, strReasonCode As String, strStartDate As String, strEndDate As String, strRemarks As String, strTxnStatus As String
            strSalesrepCode = Session("SALESREP_CODE")
            strReasonCode = ddlReasonCode.SelectedValue.ToString
            strStartDate = Trim(txtstartdate.Text)
            strEndDate = Trim(txtenddate.Text)
            strRemarks = Trim(txtremarks.Text)
            strTxnStatus = "P"
            dt = clsLeave.SubmitLeave(strSalesrepCode, strReasonCode, strStartDate, strEndDate, strRemarks, strTxnStatus)

            TimerControlLeave.Enabled = True
            btnReset_Click(sender, e)
            If dt.Rows(0)(0) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successfully submitted the leave!')", True)
            End If
        End If
    End Sub

    Protected Sub btnKIV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKIV.Click
        If Page.IsValid Then
            Dim dt As DataTable
            Dim clsLeave As New txn_WebActy.clsLeave

            Dim strSalesrepCode As String, strReasonCode As String, strStartDate As String, strEndDate As String, strRemarks As String, strTxnStatus As String
            strSalesrepCode = Session("SALESREP_CODE")
            strReasonCode = ddlReasonCode.SelectedValue.ToString
            strStartDate = Trim(txtstartdate.Text)
            strEndDate = Trim(txtenddate.Text)
            strRemarks = Trim(txtremarks.Text)
            strTxnStatus = "K"
            dt = clsLeave.SubmitLeave(strSalesrepCode, strReasonCode, strStartDate, strEndDate, strRemarks, strTxnStatus)

            TimerControlLeave.Enabled = True
            btnReset_Click(sender, e)

            If dt.Rows(0)(0) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successfully KIV the leave!')", True)
            End If
        End If
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ddlReasonCode.SelectedIndex = 0
        txtstartdate.Text = Format(Now(), "yyyy-MM-dd")
        txtenddate.Text = Format(Now(), "yyyy-MM-dd")
        txtremarks.Text = ""

    End Sub

    Protected Sub btndeleteBatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeleteBatch.Click
        Try
            If dglist.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strSalesrepCode As String, strReasonCode As String, strStartDate As String, strEndDate As String


                Dim DK As DataKey
                For Each DR As GridViewRow In dglist.Rows

                    DK = dglist.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dglist.Rows(i).FindControl("chkdelete"), CheckBox)

                        If chkdelete.Checked = True Then
                            strSalesrepCode = Session("SALESREP_CODE")
                            strReasonCode = Trim(dglist.DataKeys(i).Item("REASON_CODE"))
                            strStartDate = Trim(dglist.DataKeys(i).Item("START_DATE"))
                            strEndDate = Trim(dglist.DataKeys(i).Item("END_DATE"))

                            Dim clsLeave As New txn_WebActy.clsLeave
                            clsLeave.DeleteLeave(strSalesrepCode, strReasonCode, strStartDate, strEndDate)
                        End If

                    End If

                    i += 1
                Next

            End If
            TimerControlLeave.Enabled = True
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successfully deleted the leave!')", True)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSubmitBatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitBatch.Click
        Try
            If dglist.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strSalesrepCode As String, strReasonCode As String, strStartDate As String, strEndDate As String, strTxnStatus As String


                Dim DK As DataKey
                For Each DR As GridViewRow In dglist.Rows

                    DK = dglist.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dglist.Rows(i).FindControl("chkdelete"), CheckBox)

                        If chkdelete.Checked = True Then
                            strSalesrepCode = Session("SALESREP_CODE")
                            strReasonCode = Trim(dglist.DataKeys(i).Item("REASON_CODE"))
                            strStartDate = Trim(dglist.DataKeys(i).Item("START_DATE"))
                            strEndDate = Trim(dglist.DataKeys(i).Item("END_DATE"))
                            strTxnStatus = "P"


                            Dim clsLeave As New txn_WebActy.clsLeave
                            clsLeave.SubmitKIVLeave(strSalesrepCode, strReasonCode, strStartDate, strEndDate, strTxnStatus)
                        End If

                    End If

                    i += 1
                Next
                TimerControlLeave.Enabled = True
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have successfully submitted the leave!')", True)

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region


  
End Class
