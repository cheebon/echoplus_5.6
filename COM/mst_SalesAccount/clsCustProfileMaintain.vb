﻿'************************************************************************
'	Author	    :	Cheong Boo Lim
'	Date	    :	03/09/2010
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsCustProfileMaintain
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public Function SearchCustProfile(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal intNetValue As Integer, _
                                       ByVal strDate As String, ByVal strMtdStart As String, ByVal strMrdEnd As String, _
                                       ByVal strYtdStart As String, ByVal strYtdEnd As String, ByVal strNoSKUStart As String, ByVal strNoSKUEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PROFILE_SEARCH"
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("ADDRESS", strAddress, clsDB.DataType.DBString)
                .addItem("DISTRICT", strDistrict, clsDB.DataType.DBString)
                .addItem("CUST_GRP_NAME", strCustGrp, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("CUST_TYPE", strCustType, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", "", clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBString)
                .addItem("MTDSTART", strMtdStart, clsDB.DataType.DBString)
                .addItem("MTDEND", strMrdEnd, clsDB.DataType.DBString)
                .addItem("YTDSTART", strYtdStart, clsDB.DataType.DBString)
                .addItem("YTDEND", strYtdEnd, clsDB.DataType.DBString)
                .addItem("NOSKUSTART", strNoSKUStart, clsDB.DataType.DBString)
                .addItem("NOSKUEND", strNoSKUEnd, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Cust Photo"
    Public Function GetCustPhoto(ByVal strCustCust As String) As DataTable
        Dim objDB As clsDB
        Dim dt As DataTable
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PHOTO_GET"
                .addItem("CUST_CODE", strCustCust, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
            dt = Nothing
        End Try
    End Function
    Public Sub SaveCustPhoto(ByVal strCustCust As String, ByVal strImgLoc As String)
        Dim objDB As clsDB
        Dim dt As DataTable
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PHOTO_SAVE"
                .addItem("CUST_CODE", strCustCust, clsDB.DataType.DBString)
                .addItem("IMG_LOC", strImgLoc, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
            dt = Nothing
        End Try
    End Sub

    Public Sub DelCustPhoto(ByVal strCustCust As String, ByVal strImgLoc As String)
        Dim objDB As clsDB
        Dim dt As DataTable
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_PHOTO_DELETE"
                .addItem("CUST_CODE", strCustCust, clsDB.DataType.DBString)
                .addItem("IMG_LOC", strImgLoc, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
            dt = Nothing
        End Try
    End Sub
#End Region

#Region "Cust GPS"
    Public Function GetCustGPS(ByVal strCustCust As String) As DataTable
        Dim objDB As clsDB
        Dim dt As DataTable
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GPS_GET"
                .addItem("CUST_CODE", strCustCust, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
            dt = Nothing
        End Try
    End Function
    Public Sub SaveCustGPS(ByVal strCustCust As String, ByVal strLatitude As String, ByVal strLongitude As String)
        Dim objDB As clsDB
        Dim dt As DataTable
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_CUST_GPS_SAVE"
                .addItem("CUST_CODE", strCustCust, clsDB.DataType.DBString)
                .addItem("LATITUDE", strLatitude, clsDB.DataType.DBString)
                .addItem("LONGITUDE", strLongitude, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
            dt = Nothing
        End Try
    End Sub
#End Region
#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
