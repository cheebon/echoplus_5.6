﻿'************************************************************************
'	Author	    :	Lai Eu Jin
'	Date	    :	19/09/2013
'	Purpose	    :	SFMS Extraction
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data
Partial Class iFFMR_Customize_TRAReport_TRAReport
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_TRAReportEDI"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "TRAReport"
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.TRAREPORT_EDI)
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanelTRAReport
                .SubModuleID = SubModuleType.TRAREPORT_EDI
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                TimerControl1.Enabled = True
            End If

            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "MultiSelect"

#Region "SALES OFFICE"
    Private Sub LoadLsbSalesOffice()
        Try
            lsbSalesOffice.Items.Clear()
            lsbSelectedSalesOffice.Items.Clear()

            Dim clsTRAReportEDI As New rpt_Customize.clsTRAReportEDI

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID") 

            Dim dt As DataTable = clsTRAReportEDI.GetTRAReportSalesOfficeDDL(strUserID)
            With lsbSalesOffice
                .DataSource = dt
                .DataTextField = "SALES_OFFICE_NAME"
                .DataValueField = "SALES_OFFICE_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSalesOffice : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesOffice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesOffice.Click
        Try
            AddToListBox(lsbSalesOffice, lsbSelectedSalesOffice)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddSalesOffice_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesOffice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesOffice.Click
        Try
            AddToListBox(lsbSelectedSalesOffice, lsbSalesOffice)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveSalesOffice_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesOffice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesOffice.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesOffice.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesOffice, lsbSelectedSalesOffice)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllSalesOffice_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesOffice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesOffice.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesOffice.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesOffice, lsbSalesOffice)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllSalesOffice_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsTRAReportEDI As New rpt_Customize.clsTRAReportEDI

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim dt As DataTable = clsTRAReportEDI.GetTRAReportSalesrepDDL(strUserID)

            With lsbSalesrep
                .DataSource = dt
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "CUST GRP 1"
    Private Sub LoadLsbCustGrp1()
        Try
            lsbCustGrp1.Items.Clear()
            lsbSelectedCustGrp1.Items.Clear()

            Dim clsTRAReportEDI As New rpt_Customize.clsTRAReportEDI

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbCustGrp1
                .DataSource = clsTRAReportEDI.GetTRAReportCustGrpDDL(strUserID)
                .DataTextField = "CG1_NAME"
                .DataValueField = "CG1_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbCustGrp1 : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddCustGrp1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddCustGrp1.Click
        Try
            AddToListBox(lsbCustGrp1, lsbSelectedCustGrp1)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddCustGrp1_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveCustGrp1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveCustGrp1.Click
        Try
            AddToListBox(lsbSelectedCustGrp1, lsbCustGrp1)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveCustGrp1_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllCustGrp1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllCustGrp1.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbCustGrp1.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbCustGrp1, lsbSelectedCustGrp1)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllCustGrp1_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllCustGrp1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllCustGrp1.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedCustGrp1.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedCustGrp1, lsbCustGrp1)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllCustGrp1_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "PRINCIPAL"
    Private Sub LoadLsbPrincipal()
        Try
            lsbPrincipal.Items.Clear()
            lsbSelectedPrincipal.Items.Clear()

            Dim clsTRAReportEDI As New rpt_Customize.clsTRAReportEDI

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")


            With lsbPrincipal
                .DataSource = clsTRAReportEDI.GetTRAReportPrincipalDDL(strUserID)
                .DataTextField = "PRIN_NAME"
                .DataValueField = "PRIN_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbPrincipal : " & ex.ToString)
        Finally
        End Try
    End Sub


    Protected Sub lnkAddPrincipal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddPrincipal.Click
        Try
            AddToListBox(lsbPrincipal, lsbSelectedPrincipal)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddPrincipal_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemovePrincipal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemovePrincipal.Click
        Try
            AddToListBox(lsbSelectedPrincipal, lsbPrincipal)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemovePrincipal_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllPrincipal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllPrincipal.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbPrincipal.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbPrincipal, lsbSelectedPrincipal)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllPrincipal_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllPrincipal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllPrincipal.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedPrincipal.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedPrincipal, lsbPrincipal)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllPrincipal_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#End Region
#Region "Team"
    Private Sub LoadDdlTeam()

        Dim dtSalesTeam As DataTable
        Dim strUserID As String = Session.Item("UserID")
        Dim strPrincipalID As String = Session("PRINCIPAL_ID")

        Try
            Dim clsCommon As New mst_Common.clsDDL

            dtSalesTeam = clsCommon.GetTeamDDL()
            With ddlTeam
                .Items.Clear()
                .DataSource = dtSalesTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadDdlTeam : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region


#Region "EVENT HANDLER"
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanelTRAReport.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click1(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Try
            ResetField()
            BindDefault()
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnReset_Click : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnRefresh_Click1(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
        Try

            ViewState("dtCurrentView") = Nothing
            RefreshDatabinding()

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnRefresh_Click : " & ex.ToString)
        Finally

        End Try
    End Sub


#End Region

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrenttable Is Nothing Then
                dtCurrenttable = GetRecList()

                ViewState("strSortExpression") = Nothing
                ViewState("dtCurrentView") = dtCurrenttable
                dgList.PageIndex = 0
            End If
            PreRenderMode(dtCurrenttable)

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
            End With

            wuc_dgpaging.Visible = IIf(dtCurrenttable.Rows.Count = 0, False, True)
            'lblAltExportFormat.Visible = True
            btnExportAsWebExcel.Visible = True

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_TRAReport.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_TRAReport.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_TRAReport.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TRAReport.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_TRAReport.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = CStr(ViewState("strSortExpression"))

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        RefreshDatabinding()

    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanelTRAReport.UpdateControlPanel()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecListDefault()
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            Dim strRTVDateFRom As String = wucRTVDate.DateStart
            Dim strRTVDateTo As String = wucRTVDate.DateEnd
            Dim strUploadDateFrom As String = wucUploadDate.DateStart
            Dim strUploadDateTo As String = wucUploadDate.DateEnd
            Dim strRTVNumberFrom As String = txtFromRTVNumber.Text
            Dim strRTVNumberTo As String = txtToRTVNumber.Text
            Dim strSalesTeam As String = ddlTeam.SelectedValue
            Dim strCustGrp1From As String = String.Empty 'txtFromCustGrp1.Text
            Dim strCustGrp1To As String = GetItemsInString(lsbSelectedCustGrp1) 'txtToCustGrp1.Text
            Dim strSoldToFrom As String = txtFromSoldTo.Text
            Dim strSoldToTo As String = txtToSoldTo.Text
            Dim strPayerFrom As String = txtFromPayer.Text
            Dim strPayerTo As String = txtToPayer.Text
            Dim strShipToFrom As String = txtFromShipto.Text
            Dim strShipToTo As String = txtToShipto.Text
            Dim strSalesOfficeFrom As String = String.Empty 'txtFromSalesOffice.Text
            Dim strSalesOfficeTo As String = GetItemsInString(lsbSelectedSalesOffice) 'txtToSalesOffice.Text
            Dim strSalesrepCodeFrom As String = String.Empty 'txtFromSalesrep.Text
            Dim strSalesrepCodeTo As String = GetItemsInString(lsbSelectedSalesrep) 'txtToSalesrep.Text
            Dim strPrincipalFrom As String = String.Empty 'txtFromPrincipal.Text
            Dim strPrincipalTo As String = GetItemsInString(lsbSelectedPrincipal) 'txtToPrincipal.Text
            Dim strMatCodeFrom As String = txtFromMatCode.Text
            Dim strMatCodeTo As String = txtToMatCode.Text
            Dim strVendorCodeFrom As String = txtFromVendorCode.Text
            Dim strVendorCodeTo As String = txtToVendorCode.Text
            Dim strStatus As String = ddltxnstatus.SelectedValue


            Dim clsTRAReportEDI As New rpt_Customize.clsTRAReportEDI
            DT = clsTRAReportEDI.GetTRAReportEDI(strUserID, strRTVDateFRom, strRTVDateTo, strUploadDateFrom, strUploadDateTo, strRTVNumberFrom, strRTVNumberTo, strSalesTeam, strCustGrp1From, strCustGrp1To, _
                 strSoldToFrom, strSoldToTo, strPayerFrom, strPayerTo, strShipToFrom, strShipToTo, _
                 strSalesOfficeFrom, strSalesOfficeTo, strSalesrepCodeFrom, strSalesrepCodeTo, strPrincipalFrom, strPrincipalTo, _
                 strMatCodeFrom, strMatCodeTo, strVendorCodeFrom, strVendorCodeTo, strStatus)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListDefault() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalId, strPrincipalCode As String
            strUserID = Session.Item("UserID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")

            Dim strRTVDateFRom As String = "1900-01-01" 'wucRTVDate.DateStart
            Dim strRTVDateTo As String = "1900-01-01" 'wucRTVDate.DateEnd
            Dim strUploadDateFrom As String = "1900-01-01" 'wucUploadDate.DateStart
            Dim strUploadDateTo As String = "1900-01-01" 'wucUploadDate.DateEnd
            Dim strRTVNumberFrom As String = txtFromRTVNumber.Text
            Dim strRTVNumberTo As String = txtToRTVNumber.Text
            Dim strSalesTeam As String = ddlTeam.SelectedValue
            Dim strCustGrp1From As String = String.Empty 'txtFromCustGrp1.Text
            Dim strCustGrp1To As String = GetItemsInString(lsbSelectedCustGrp1) 'txtToCustGrp1.Text
            Dim strSoldToFrom As String = txtFromSoldTo.Text
            Dim strSoldToTo As String = txtToSoldTo.Text
            Dim strPayerFrom As String = txtFromPayer.Text
            Dim strPayerTo As String = txtToPayer.Text
            Dim strShipToFrom As String = txtFromShipto.Text
            Dim strShipToTo As String = txtToShipto.Text
            Dim strSalesOfficeFrom As String = String.Empty 'txtFromSalesOffice.Text
            Dim strSalesOfficeTo As String = GetItemsInString(lsbSelectedSalesOffice) 'txtToSalesOffice.Text
            Dim strSalesrepCodeFrom As String = String.Empty 'txtFromSalesrep.Text
            Dim strSalesrepCodeTo As String = GetItemsInString(lsbSelectedSalesrep) 'txtToSalesrep.Text
            Dim strPrincipalFrom As String = String.Empty 'txtFromPrincipal.Text
            Dim strPrincipalTo As String = GetItemsInString(lsbSelectedPrincipal) ' txtToPrincipal.Text
            Dim strMatCodeFrom As String = txtFromMatCode.Text
            Dim strMatCodeTo As String = txtToMatCode.Text
            Dim strVendorCodeFrom As String = txtFromVendorCode.Text
            Dim strVendorCodeTo As String = txtToVendorCode.Text
            Dim strStatus As String = ddltxnstatus.SelectedValue



            Dim clsTRAReportEDI As New rpt_Customize.clsTRAReportEDI
            DT = clsTRAReportEDI.GetTRAReportEDI(strUserID, strRTVDateFRom, strRTVDateTo, strUploadDateFrom, strUploadDateTo, strRTVNumberFrom, strRTVNumberTo, strSalesTeam, strCustGrp1From, strCustGrp1To, _
                 strSoldToFrom, strSoldToTo, strPayerFrom, strPayerTo, strShipToFrom, strShipToTo, _
                 strSalesOfficeFrom, strSalesOfficeTo, strSalesrepCodeFrom, strSalesrepCodeTo, strPrincipalFrom, strPrincipalTo, _
                 strMatCodeFrom, strMatCodeTo, strVendorCodeFrom, strVendorCodeTo, strStatus)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanelTRAReport.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanelTRAReport.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

    Private Sub ExportToXLSFromDataTable(ByVal dtExport As DataTable, ByVal filename As String)
        Try

            Dim dataToExport As New StringBuilder()
            dataToExport.Append("<meta http-equiv=Content-Type content='text/html; charset=utf-8'>")
            dataToExport.Append("<table>")
            dataToExport.Append("<tr>")

            For Each dCol As DataColumn In dtExport.Columns
                dataToExport.Append("<td>")
                dataToExport.Append(Server.HtmlEncode(CF_TRAReport.GetDisplayColumnName(dCol.ColumnName)).Replace(" ", "_"))
                dataToExport.Append("</td>")
            Next

            dataToExport.Append("</tr>")

            For Each dRow As DataRow In dtExport.Rows
                dataToExport.Append("<tr>")
                For Each obj As Object In dRow.ItemArray
                    dataToExport.Append("<td>")
                    dataToExport.Append(Server.HtmlEncode(obj.ToString()))
                    dataToExport.Append("</td>")
                Next
                dataToExport.Append("</tr>")
            Next

            dataToExport.Append("</table>")

            If Not String.IsNullOrEmpty(dataToExport.ToString()) Then
                Response.Clear()

                'HttpContext.Current.Response.ContentType = "application/ms-excel"
                HttpContext.Current.Response.ContentType = "text/html"
                HttpContext.Current.Response.Charset = "UTF8"
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" & wuc_lblHeader.Title.ToString.Replace(" ", "_") & ".xls")

                HttpContext.Current.Response.Write(dataToExport.ToString())
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.[End]()
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".ExportToXLSFromDataTable : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnExportAsCsv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportAsCsv.Click
        Dim dt As DataTable = GetRecList()
        ExportToCsv(dt, Response, PageName)

    End Sub

    Public Shared Sub ExportToCsv(ByVal dt As DataTable, ByRef MyResponse As System.Web.HttpResponse, ByVal strFileName As String)
        Dim context As HttpContext = HttpContext.Current
        context.Response.Clear()


        context.Response.Charset = "Unicode"
        context.Response.ContentEncoding = System.Text.Encoding.Unicode



        ' write the column names 
        For Each column As DataColumn In dt.Columns
            context.Response.Write(CF_TRAReport.GetDisplayColumnName(column.ColumnName) + ",")
        Next
        context.Response.Write(Environment.NewLine)


        ' write the rows 
        For Each row As DataRow In dt.Rows
            For i As Integer = 0 To dt.Columns.Count - 1
                context.Response.Write(row(i).ToString().Replace(";", String.Empty) & ",")
            Next
            context.Response.Write(Environment.NewLine)
        Next
        context.Response.ContentType = "text/csv"
        context.Response.AppendHeader("Content-Disposition", "attachment; filename=" & strFileName & ".csv")


        context.Response.Flush()
        context.Response.[End]()
    End Sub

    Protected Sub btnExportAsWebExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportAsWebExcel.Click
        Dim dt As DataTable = GetRecList()
        ExportToXLSFromDataTable(dt, PageName)
    End Sub

#End Region

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub ResetField()
        Try

            wucRTVDate.DateStart = String.Empty
            wucRTVDate.DateEnd = String.Empty
            wucUploadDate.DateStart = String.Empty
            wucUploadDate.DateEnd = String.Empty
            txtFromRTVNumber.Text = String.Empty
            txtToRTVNumber.Text = String.Empty
            ddlTeam.SelectedIndex = 0
            'txtFromCustGrp1.Text = String.Empty
            'txtToCustGrp1.Text = String.Empty
            txtFromSoldTo.Text = String.Empty
            txtToSoldTo.Text = String.Empty
            txtFromPayer.Text = String.Empty
            txtToPayer.Text = String.Empty
            txtFromShipto.Text = String.Empty
            txtToShipto.Text = String.Empty
            'txtFromSalesOffice.Text = String.Empty
            'txtToSalesOffice.Text = String.Empty
            'txtFromSalesrep.Text = String.Empty
            'txtToSalesrep.Text = String.Empty
            'txtFromPrincipal.Text = String.Empty
            'txtToPrincipal.Text = String.Empty
            txtFromMatCode.Text = String.Empty
            txtToMatCode.Text = String.Empty
            txtFromVendorCode.Text = String.Empty
            txtToVendorCode.Text = String.Empty
            ddltxnstatus.SelectedValue = String.Empty


            LoadDdlTeam()
            LoadLsbSalesrep()
            LoadLsbCustGrp1()
            LoadLsbSalesOffice()
            LoadLsbPrincipal()

            UpdatePnlExtraction.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & "ResetField()" & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            BindDefault()
            LoadDdlTeam()
            LoadLsbSalesrep()
            LoadLsbCustGrp1()
            LoadLsbSalesOffice()
            LoadLsbPrincipal()
            UpdatePnlExtraction.Update()
        End If
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class

Public Class CF_TRAReport
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn. No." 
            Case "SALES_AREA_CODE"
                strFieldName = "Sales Office Code"
            Case "SALES_AREA_NAME"
                strFieldName = "Sales Office Desc"
            Case "SALESREP_CODE"
                strFieldName = "Salesman Code"
            Case "SALESREP_NAME"
                strFieldName = "Salesman Name"
                'Case "AGENCY_CODE"
                '    strFieldName = "Agency Code"
                'Case "PRDGRPDESC"
                '    strFieldName = "Mat Grp Desc"
            Case "PRINCODE"
                strFieldName = "Principal Code"
            Case "PRINNAME"
                strFieldName = "Principal Desc"
            Case "RTV_NUMBER"
                strFieldName = "RTV No."
            Case "TXN_DATE_IN"
                strFieldName = "Transaction Date"
            Case "DISPOSAL_DATE"
                strFieldName = "Disposal Date"
            Case "OUTLET_CODE"
                strFieldName = "Outlet Code"
            Case "PAYER_CODE"
                strFieldName = "Payer Code"
            Case "PAYERNAME1"
                strFieldName = "Payer"
            Case "SHIPTONAME1"
                strFieldName = "Ship To Desc"
            Case "PRD_NUMBER"
                strFieldName = "Customer IKA Product/SKU Code"
            Case "CUST_CODE"
                strFieldName = "Ship To Code"
            Case "PRD_CODE"
                strFieldName = "SAP Material Code"
            Case "PRDNAME"
                strFieldName = "SAP Material Desc"
            Case "LINE_NO"
                strFieldName = "Line Number"
            Case "REASON_CODE"
                strFieldName = "SAP Reason Code"
            Case "REASON_NAME"
                strFieldName = "SAP Reason Desc"
            Case "RET_QTY"
                strFieldName = "Return Qty"
            Case "UOM_CODE"
                strFieldName = "UOM"
            Case "LIST_PRICE"
                strFieldName = "Return Cost"
            Case "RET_AMT"
                strFieldName = "Line Total"
            Case "TTL_RET_AMT"
                strFieldName = "TRA Total "
            Case "TXN_STATUS"
                strFieldName = "Status"
            Case "TRA_NO"
                strFieldName = "CO Number"
            Case "UPLOADDATE"
                strFieldName = "Upload Date"
            Case "SUBMITTED_DATE"
                strFieldName = "Approval Submission Date"
            Case Else
                strFieldName = ColumnName.ToUpper 'Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            ' Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            FCT = FieldColumntype.BoundColumn
            'Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            'If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
            '   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            '    FCT = FieldColumntype.InvisibleColumn
            'Else
            '    FCT = FieldColumntype.BoundColumn
            'End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "TXN_DATE_IN"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "DISPOSAL_DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "QTY"
                    strFormatString = "{0:#,0}"
                Case "MULTIPLIER"
                    strFormatString = "{0:#,0}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class