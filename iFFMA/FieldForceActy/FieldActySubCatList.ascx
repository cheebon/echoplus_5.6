<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FieldActySubCatList.ascx.vb" Inherits="iFFMA_FieldForceActy_FieldActySubCatList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_FieldActySubCatPop" Src="FieldActySubCatPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlSalesteamAssign" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                        <asp:Button ID="btnDelete" CssClass="cls_button" runat="server" Text="Delete" OnClientClick="if (!checkDelete()){return false; }"/>
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <customToolkit:wuc_dgpaging ID="wuc_dgSubCatPaging" runat="server" />
                        <ccGV:clsGridView ID="dgSubCatList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                            <Columns>
                                <asp:TemplateField>
                                    <itemtemplate>
                                        <%--<asp:checkbox ID="chkSelect" runat="server" class="cls_checkbox"/>--%>
                                    </itemtemplate> 
                                </asp:TemplateField>
                            </Columns>
                        </ccGV:clsGridView>
                    </center>
                </td>
            </tr>
        </table>
                
        <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
        <asp:HiddenField ID="hdCatCode" runat="server" Value="" />
        
        <customToolkit:wuc_FieldActySubCatPop ID="wuc_FieldActySubCatPop" Title="Sub Category Maintenance" runat="server" />
        
    </ContentTemplate>
</asp:UpdatePanel>
