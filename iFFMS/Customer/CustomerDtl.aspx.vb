Imports Microsoft.Web.UI.WebControls
Imports Microsoft.Web.UI


Namespace FFMS_WebOrder

    Public Class FFMS_WebOrder_CustomerDtl
        Inherits System.Web.UI.Page


        Dim tabname As String
        Public sIFrameSrc As String

        Public ReadOnly Property PageName() As String
            Get
                Return "CustomerDtl"
            End Get
        End Property

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Session("customercode") = Request("custcode")
            'Session("customercode") = "C000070"
            Dim strCustName As String
            Dim obj As New txn_Customer.clsDept
            Dim strCustCode As String
            Try
                strCustCode = Trim(Session("customercode"))
                strCustName = obj.getContactName(strCustCode)
                'Put user code to initialize the page here
                'Call Header
                With WucHeader
                    .Title = "Customer Details : -" & strCustName
                    .DataBind()
                    .Visible = True
                End With

                'Call Panel
                If Not IsPostBack Then
                    Dim tab As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab.Text = "General Info"
                    custtab.Items.Add(tab)

                    Dim tab1 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab1.Text = "Contact"
                    custtab.Items.Add(tab1)

                    Dim tab2 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab2.Text = "Shipto"
                    custtab.Items.Add(tab2)

                    Dim tab3 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab3.Text = "Open Item"
                    custtab.Items.Add(tab3)

                    Dim tab4 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab4.Text = "AR Aging"
                    custtab.Items.Add(tab4)

                    Dim tab5 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab5.Text = "Mth Sales Hist"
                    custtab.Items.Add(tab5)

                    Dim tab6 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab6.Text = "Invoice Hist"
                    custtab.Items.Add(tab6)

                    Dim tab7 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab7.Text = "SFMS Hist"
                    custtab.Items.Add(tab7)

                    Dim tab8 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab8.Text = "Dealer Rec"
                    custtab.Items.Add(tab8)

                    Dim tab9 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab9.Text = "Order Status"
                    custtab.Items.Add(tab9)

                    Dim tab10 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab10.Text = "Cust Disc"
                    custtab.Items.Add(tab10)

                    Dim tab11 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab11.Text = "Promo Camp"
                    custtab.Items.Add(tab11)

                    Dim tab12 As Microsoft.Web.UI.WebControls.Tab = New Microsoft.Web.UI.WebControls.Tab()
                    tab12.Text = "Plan History"
                    custtab.Items.Add(tab12)
                End If
                sIFrameSrc = "CustomerGeneralInfo.aspx"
                With WucctrlPanel
                    .SubModuleID = 16
                    .DataBind()
                    .Visible = True
                End With
               
            Catch ex As Exception
                ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
            Finally
                obj = Nothing
            End Try

        End Sub

        Private Sub custtab_SelectedIndexChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles custtab.SelectedIndexChange
            Try
                Dim strSubModule As String = 18
                Select Case custtab.SelectedIndex
                    Case 0
                        tabname = "General Info"
                        sIFrameSrc = "CustomerGeneralInfo.aspx"
                        strSubModule = 16
                    Case 1
                        tabname = "Contact"
                        sIFrameSrc = "CustomerContactGeneralInfo.aspx"
                        strSubModule = 17
                    Case 2
                        tabname = "Delivery Address"
                        sIFrameSrc = "CustomerShipto.aspx"
                        strSubModule = 18
                    Case 3
                        tabname = "Open Item"
                        sIFrameSrc = "CustomerOpenItem.aspx"
                        strSubModule = 48
                    Case 4
                        tabname = "AR Aging"
                        sIFrameSrc = "CustomerARAging.aspx"
                        strSubModule = 49
                    Case 5
                        tabname = "Mth Sales Hist"
                        sIFrameSrc = "CustomerMthSalesHist.aspx"
                        strSubModule = 50
                    Case 6
                        tabname = "Invoice Hist"
                        sIFrameSrc = "CustomerInvoiceHist.aspx"
                        strSubModule = 51
                    Case 7
                        tabname = "SFMS Hist"
                        sIFrameSrc = "CustomerSFMSHist.aspx"
                        strSubModule = 52
                    Case 8
                        tabname = "Dealer Rec"
                        sIFrameSrc = "CustomerDealerRec.aspx"
                        strSubModule = 53
                    Case 9
                        tabname = "Order Status"
                        sIFrameSrc = "CustomerOrderStatus.aspx"
                        strSubModule = 54
                    Case 10
                        tabname = "Cust Disc"
                        sIFrameSrc = "CustomerCustDisc.aspx"
                        strSubModule = 55
                    Case 11
                        tabname = "Promo Camp"
                        sIFrameSrc = "CustomerPromoCamp.aspx"
                        strSubModule = 56
                    Case 12
                        tabname = "Plan History"
                        sIFrameSrc = "CustomerPlanListDetail.aspx"
                        strSubModule = 57

                    Case Else
                        sIFrameSrc = ""
                End Select
                With WucctrlPanel
                    .SubModuleID = strSubModule
                    .DataBind()
                    .Visible = True
                End With
            Catch ex As Exception
                ExceptionMsg(PageName & ".custtab_SelectedIndexChange : " & ex.ToString)
            Finally

            End Try

        End Sub

        Private Sub ExceptionMsg(ByVal strMsg As String)
            Try
                lblErr.Text = ""
                lblErr.Text = strMsg

                'Call error log class
                Dim objLog As cor_Log.clsLog
                objLog = New cor_Log.clsLog
                With objLog
                    .clsProperties.LogTypeID = 1
                    .clsProperties.DateLogIn = Now
                    .clsProperties.DateLogOut = Now
                    .clsProperties.SeverityID = 4
                    .clsProperties.LogMsg = strMsg
                    .Log()
                End With
                objLog = Nothing

            Catch ex As Exception

            End Try
        End Sub
    End Class

End Namespace
