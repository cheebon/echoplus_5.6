﻿'************************************************************************
'	Author	    :	Cheong Boo Lim
'	Date	    :	20 Aug 2010
'	Purpose	    :	Class to make enquire customer profile
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustomerProfile
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public Function GetCustomerProfile(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strUserID As String, ByVal intNetValue As Integer, _
                                       ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try

            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_PROFILE_ENQ"
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("ADDRESS", strAddress, clsDB.DataType.DBString)
                .addItem("DISTRICT", strDistrict, clsDB.DataType.DBString)
                .addItem("CUST_GRP_NAME", strCustGrp, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("CUST_TYPE", strCustType, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBDatetime)

                .addItem("MTDSTART", strMtdStart, clsDB.DataType.DBString)
                .addItem("MTDEND", strMtdEnd, clsDB.DataType.DBString)
                .addItem("YTDSTART", strYtdStart, clsDB.DataType.DBString)
                .addItem("YTDEND", strYtdEnd, clsDB.DataType.DBString)
                .addItem("NOSKUSTART", strNoSkuStart, clsDB.DataType.DBString)
                .addItem("NOSKUEND", strNoSkuEnd, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

    End Function
    Public Function GetCustomerProfileExport(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strUserID As String, ByVal intNetValue As Integer, _
                                       ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try

            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_PROFILE_ENQ_EXPORT"
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("ADDRESS", strAddress, clsDB.DataType.DBString)
                .addItem("DISTRICT", strDistrict, clsDB.DataType.DBString)
                .addItem("CUST_GRP_NAME", strCustGrp, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("CUST_TYPE", strCustType, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBDatetime)

                .addItem("MTDSTART", strMtdStart, clsDB.DataType.DBString)
                .addItem("MTDEND", strMtdEnd, clsDB.DataType.DBString)
                .addItem("YTDSTART", strYtdStart, clsDB.DataType.DBString)
                .addItem("YTDEND", strYtdEnd, clsDB.DataType.DBString)
                .addItem("NOSKUSTART", strNoSkuStart, clsDB.DataType.DBString)
                .addItem("NOSKUEND", strNoSkuEnd, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

    End Function

    Public Function GetCustomerProfilePicture(ByVal strCustCode As String, ByVal strUserID As String)

        Dim objDB As clsDB
        Try

            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_PROFILE_ENQ_PHOTO"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
