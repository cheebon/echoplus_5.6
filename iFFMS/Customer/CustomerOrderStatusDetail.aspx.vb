Imports System.Data

Partial Class CustomerOrderStatusDetail
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerSalesOrderDetail"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Call Header
        With wuc_lblheader
            .Title = "Sales Order Status Detail"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If


    End Sub

    Sub onpageload()
        Dim strSONo, strInvNo, strSalesmanCode, strCustCode As String
        Try
            strSONo = Request.QueryString("sono")
            strInvNo = Request.QueryString("invno")
            strSalesmanCode = Session("SALESREP_CODE")
            strCustCode = Session("customercode")
            bindGridView(strSalesmanCode, strCustCode, strSONo, strInvNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            onpageload()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strSONo As String, ByVal strInvNo As String)
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getOrderStatusDetailDT(strSalesmanCode, strCustCode, strSONo, strInvNo)
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
                lblSalesOrdNo.Text = IIf(IsDBNull(dt.Rows(0)("so_no")), "Not Available", dt.Rows(0)("so_no"))
                lblSalesOrdNo.Text = IIf(String.IsNullOrEmpty(lblSalesOrdNo.Text), "Not Available", lblSalesOrdNo.Text)

                lblInvoiceNo.Text = IIf(IsDBNull(dt.Rows(0)("inv_no")), "Not Available", dt.Rows(0)("inv_no"))
                lblInvoiceNo.Text = IIf(String.IsNullOrEmpty(lblInvoiceNo.Text), "Not Available", lblInvoiceNo.Text)
            Else
                dt.Rows.Add(dt.NewRow)
                gridview1.DataSource = dt
                gridview1.DataBind()
                lblSalesOrdNo.Text = "Not Available"
                lblInvoiceNo.Text = "Not Available"
            End If
        Catch ex As Exception
            ExceptionMsg("bindGridView" & ".Page_Load : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../Customer/CustomerOrderStatus.aspx", False)
    End Sub
End Class



