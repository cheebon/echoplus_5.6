' **********************************************************************************************************
'
'	Author	:	
'	Date	:	09-June-2005
'	Purpose	:	To encrypted the data either using triple des encryption method or
'               rsa encryption method
'
'	Revision	: 	
'	---------------------------------------------------------------------------------------------------------
'	|No	|Date Change	|Author		|Remarks				        |	
'	---------------------------------------------------------------------------------------------------------
'	|1	|               |           |                               |
'	|2	|			    |	        |					            |
'	---------------------------------------------------------------------------------------------------------
'
' ***********************************************************************************************************

Option Explicit On 
Option Strict Off

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel
Imports System.Security.Cryptography
Imports System.IO
Imports System.Text

Interface Icor_Encryption
    Function TripleDESEncrypt(ByVal strData2Encrypt As String) As String
    Function RSAEncrypt(ByVal strData2Encrypt As String) As String
End Interface


'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsEncryption
    'Inherits ServicedComponent
    Implements Icor_Encryption

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

    Public clsProperties As New clsProperties.clsEncryption

    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function TripleDESEncryption
    ' Purpose	:	This function will perform the triple des encryption method
    ' Parameters:	[in]  : strData2Encrypt    
    '		        [out] : Return encrypted data as a string data type
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function TripleDESEncrypt(ByVal strData2Encrypt As String) As String Implements Icor_Encryption.TripleDESEncrypt
        Dim objCrypto As clsCrypto
        Dim m_des As TripleDESCryptoServiceProvider
        Dim m_utf8 As UTF8Encoding  ' Define the string handler
        Dim bytKey() As Byte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, _
                              15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
        Dim bytIv() As Byte = {8, 7, 6, 5, 4, 3, 2, 1}
        Dim bytInput() As Byte
        Dim bytOutput() As Byte = Nothing

        Try
            If strData2Encrypt <> "" Then
                objCrypto = New clsCrypto
                m_des = New TripleDESCryptoServiceProvider
                m_utf8 = New UTF8Encoding

                With objCrypto.clsProperties
                    .DocTypeID = clsProperties.DocTypeID
                    .SiteID = clsProperties.SiteID
                End With
                bytInput = m_utf8.GetBytes(strData2Encrypt)
                bytOutput = objCrypto.DESTransform(bytInput, m_des.CreateEncryptor(bytKey, bytIv))


            End If
            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsEncryption.TripleDESEncrypt : " + ex.ToString))
        Finally
            'set obj = Nothing
            objCrypto = Nothing
            m_des = Nothing
            m_utf8 = Nothing
        End Try
        Return Convert.ToBase64String(bytOutput)
    End Function


    '---------------------------------------------------------------------------------------------------------
    ' Procedure :   Function RSAEncrypt
    ' Purpose	:	This function will perform the encryption method using public key
    ' Parameters:	[in]  : strData2Encrypt    
    '		        [out] : Return encrypted data as a string data type
    '---------------------------------------------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function RSAEncrypt(ByVal strData2Encrypt As String) As String Implements Icor_Encryption.RSAEncrypt
        Dim rsa As RSACryptoServiceProvider
        Dim objCrypto As clsCrypto
        'Dim reader As StreamReader
        Dim strPublicOnlyKeyXML As String
        Dim strEncrypted As String = ""
        Dim strNonFixLen As String
        Dim arrData2Encrypt As ArrayList
        Dim bytPlain() As Byte
        Dim bytCipher() As Byte
        Dim intLength As Integer
        Dim intCount As Integer
        Dim strTemp As String

        Try
            RSAEncrypt = ""
            If strData2Encrypt <> "" Then
                objCrypto = New clsCrypto
                arrData2Encrypt = New ArrayList
                rsa = New RSACryptoServiceProvider

                With objCrypto.clsProperties
                    .DocTypeID = clsProperties.DocTypeID
                    .SiteID = clsProperties.SiteID
                End With
                rsa = objCrypto.RSAAssignParameter(rsa)
                objCrypto.RSAAssignNewKey(rsa)

                strPublicOnlyKeyXML = rsa.ToXmlString(False)

                arrData2Encrypt.Clear()
                strNonFixLen = strData2Encrypt

                intLength = strNonFixLen.Length
                While intLength >= 1
                    If intLength >= 57 Then
                        strTemp = Mid(strNonFixLen, 1, 57)
                        strNonFixLen = Mid(strNonFixLen, 58, strNonFixLen.Length)
                        intLength = strNonFixLen.Length
                        arrData2Encrypt.Add(strTemp)
                    Else
                        If intLength >= 1 Then
                            arrData2Encrypt.Add(strNonFixLen)
                        End If
                        Exit While
                    End If
                End While

                For intCount = 0 To arrData2Encrypt.Count - 1
                    rsa.FromXmlString(strPublicOnlyKeyXML)
                    'read plaintext, encrypt it to ciphertext
                    bytPlain = System.Text.Encoding.UTF8.GetBytes(arrData2Encrypt.Item(intCount))
                    bytCipher = rsa.Encrypt(bytPlain, False)

                    If intCount = 0 Then
                        strEncrypted = Convert.ToBase64String(bytCipher)
                    Else
                        strEncrypted = strEncrypted & "|@|" & Convert.ToBase64String(bytCipher)
                    End If
                Next
                Return strEncrypted & "|@|"
            End If
            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_CryptoUtil.clsEncryption.RSAEncrypt : " + ex.ToString))
        Finally
            'set obj = Nothing
            objCrypto = Nothing
            rsa = Nothing
            arrData2Encrypt = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class           
        End Sub
    End Class

    Public Overloads Sub Dispose()
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
