﻿Imports System.Data
Imports System.Threading
Imports System.Globalization
''' <summary>
''' '''  ************************************************************************
'''	Author	    :	Benny Muchsin
'''	Date	    :	2018/6/28
'''	Purpose	    :	 
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>
Partial Class CustomerCoverage
    Inherits System.Web.UI.Page
    Protected Overrides Sub InitializeCulture()

        Dim lang As String = "en-us"

        If Not String.IsNullOrEmpty(lang) Then
            Thread.CurrentThread.CurrentCulture = _
                       CultureInfo.CreateSpecificCulture(lang)
            Thread.CurrentThread.CurrentUICulture = New  _
                CultureInfo(lang)
        End If


        MyBase.InitializeCulture()
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "iFFMR_Geomap_CustomerCoverage"
        End Get
    End Property

    Dim licItemFigureCollector As ListItemCollection
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property
    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
    Private Property vsSortCol() As String
        Get
            If ViewState("SortCol") Is Nothing Then
                ViewState("SortCol") = ""
            End If
            Return ViewState("SortCol")
        End Get
        Set(ByVal value As String)
            ViewState("SortCol") = value
        End Set
    End Property
    Private Property vsSortDir() As String
        Get
            If ViewState("SortDir") Is Nothing Then
                ViewState("SortDir") = "ASC"
            End If
            Return ViewState("SortDir")
        End Get
        Set(ByVal value As String)
            ViewState("SortDir") = value
        End Set
    End Property

    Dim licItemFigureCollectorDetail As ListItemCollection
    Private _aryDataItemDetail As ArrayList
    Protected Property aryDataItemDetail() As ArrayList
        Get
            If _aryDataItemDetail Is Nothing Then _aryDataItemDetail = ViewState("DataItemDetail")
            If _aryDataItemDetail Is Nothing Then _aryDataItemDetail = New ArrayList
            Return _aryDataItemDetail
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemDetail") = value
        End Set
    End Property
    Private Property Master_Row_CountDetail() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountDetail"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountDetail") = value
        End Set
    End Property
    Private Property vsSortColDetail() As String
        Get
            If ViewState("SortColDetail") Is Nothing Then
                ViewState("SortColDetail") = ""
            End If
            Return ViewState("SortColDetail")
        End Get
        Set(ByVal value As String)
            ViewState("SortColDetail") = value
        End Set
    End Property
    Private Property vsSortDirDetail() As String
        Get
            If ViewState("SortDirDetail") Is Nothing Then
                ViewState("SortDirDetail") = "ASC"
            End If
            Return ViewState("SortDirDetail")
        End Get
        Set(ByVal value As String)
            ViewState("SortDirDetail") = value
        End Set
    End Property

    Private intPageSize As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            'Call Header
            With wuc_lblheader
                .Title = "Customer Coverage"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlPanel
                .SubModuleID = SubModuleType.CUSTOMERCOVERAGE
                .DataBind()
                .Visible = True
            End With

            '''''''' From DMS
            lblErr.Text = ""
            'UpdateErrorPanel()
            'lblHeader.Title = "Daily tracking"
            'Dim strSubmoduleId As Long = Trim(Request.QueryString("submoduleid"))

            'Me.Form.DefaultButton = Me.btnSearch.UniqueID
            If Not Page.IsPostBack Then

                Dim strKeyName, strKeyValue As String
                strKeyName = String.Empty
                strKeyValue = String.Empty
                For Each KEY As String In Request.QueryString
                    If KEY Like "TREE*CODE" Then
                        strKeyName = KEY
                        strKeyValue = Trim(Request.QueryString(strKeyName))
                        Exit For
                    End If
                Next
                If strKeyName <> String.Empty Then
                    Session("SALESREP_LIST") = Report.GetSalesrepList(strKeyName.Replace("TREE_", ""), strKeyValue)
                End If
                Report.UpdateTreePath("SALESREP_CODE", Session("SALESREP_LIST"))
               
            End If

            'Modified by Soo Fong 2017-01-18 :- Based on country_code to get latitute & longtitude 
            'Dim strCountryCode As String = 'Portal.PortalSession.UserProfile.UserSession.CountryCode
            'Dim strUserId As String = Portal.PortalSession.UserProfile.UserSession.UserID
            'Dim clsRouteTrack As New rpt_Customize.clsRouteTrack
            'Session("country") = strCountryCode


            'Dim dt As DataTable
            'dt = clsRouteTrack.GetDailyTrackingLocation("", "") 'this table only get the GPS country locaiton only
            'If dt.Rows.Count > 0 Then
            '    ' set thh default gps
            '    hfLatitude.Value = Portal.Util.GetValue(Of String)(dt(0)("latitude"))
            '    hfLongitude.Value = Portal.Util.GetValue(Of String)(dt(0)("longitude"))
            'End If

            hfLatitude.Value = 0 'Portal.Util.GetValue(Of String)(dt(0)("latitude"))
            hfLongitude.Value = 0 'Portal.Util.GetValue(Of String)(dt(0)("longitude"))

            'UpdateSearchPanel()
            '''''''' Untl DMS

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub


    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        If IsPostBack Then
            Dim strRegionCode As String, strSalesAreaCode As String, strTeamCode As String, strSalesRepCode As String

            strRegionCode = Session("REGION_CODE")
            strSalesAreaCode = Session("SALES_AREA_CODE")
            strTeamCode = Session("TEAM_CODE")
            strSalesRepCode = Session("SALESREP_CODE")
            'Dim test = "refreshMap('0005','I038','I038_B',''10300908','10300912','10300916','10300922','10300925','10300931','10300961','10301062','1294','904'');"
            'Page.ClientScript.RegisterStartupScript(Me.GetType, "AKey", "btnSeachClick();", True)
            If Not String.IsNullOrEmpty(Session("SALESREP_LIST")) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType, "Refresh map", "refreshMap('" + strRegionCode + "','" + strSalesAreaCode + "','" + strTeamCode + "',""" + Session("SALESREP_LIST") + """);", True)
                wuc_ctrlpanel.RefreshCustomerCoverageProduct()
            End If
        End If
    End Sub


End Class
