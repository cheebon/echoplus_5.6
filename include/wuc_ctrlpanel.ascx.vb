Imports System.Data

Partial Class wuc_ctrlPanel
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnRefresh As System.Web.UI.WebControls.Button
    Protected WithEvents btnRefresh1 As System.Web.UI.WebControls.Button
    Protected WithEvents btnGenerate As System.Web.UI.WebControls.Button


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private lngSubModuleID As SubModuleType
    'Private _strGoupingItems As String

    Public WithEvents btnVirtualUpdateDate As Button
    Public Event NetValue_Changed As EventHandler
    Public Event FieldConfig_Changed As EventHandler
    Public Event RefreshQueryDate_Click As EventHandler
    Public Event SFMSExtract_Click As EventHandler
    Public Event SearchBtn_Click As EventHandler
    Public Event EnqResetBtn_Click As EventHandler
    Public Event SalesEnquiryBtn_Click As EventHandler
    Public Event CallEnquiryBtn_Click As EventHandler
    Public Event DRCEnquiryBtn_Click As EventHandler
    Public Event CollCheqEnquiryBtn_Click As EventHandler
    Public Event CollEnqyBtn_Click As EventHandler
    Public Event PAFEnquiryBtn_Click As EventHandler
    Public Event GroupingFieldChanged As EventHandler
    Public Event LayoutChanged As EventHandler
    Public Event ExportBtn_Click As EventHandler

    Public blnMakeGroupingFiledDistinct As Boolean
    Private _strRestoreGroupingTextValue As String
    Private _strGroupingPointer As String = "@POINTER@"
    Public intListBoxIndexToFilter As Integer '0 = Filter ListBox_Hide, 1 = Filter ListBox_Show

    Public ReadOnly Property GetClientHeight() As Integer
        Get
            Return IIf(String.IsNullOrEmpty(ClientHeight.Value) = False AndAlso IsNumeric(ClientHeight.Value), ClientHeight.Value, 0)
        End Get
    End Property

    Public Property GroupingPointer() As String
        Get
            Return _strGroupingPointer
        End Get
        Set(ByVal value As String)
            _strGroupingPointer = value
        End Set
    End Property

    Public Property SubModuleID() As SubModuleType
        Get
            Return lngSubModuleID
        End Get
        Set(ByVal Value As SubModuleType)
            lngSubModuleID = Value
        End Set
    End Property

    Public Property strRestoreGroupingTextValue() As String
        Get
            Return _strRestoreGroupingTextValue
        End Get
        Set(ByVal value As String)
            _strRestoreGroupingTextValue = value
            GenerateFiledList(lngSubModuleID)
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Apply for all reports, check for the session
        'If Session("UserID") Is Nothing OrElse Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='" & ResolveClientUrl("~/login.aspx") & "?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Timeout", strScript, True)
        'End If
        'End Session Check

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "IncludedJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/wuc_ctrlPnl.js")
            BuildQueryYearMonth()
            GenerateFiledList(lngSubModuleID)
            NetValue_Refresh()
            FieldConfig_Refresh()
            BuildReportDefinition()
            BuildRemarkList()
        End If
        RefreshDetails(IsPostBack)
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        'End Try

    End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_ctrlPanel"
        End Get
    End Property

    Public Sub UpdateControlPanel()
        UpdateCtrlPanel.Update()
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".UpdateCtrlPanel : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Public Overloads Sub DataBind()
        imgExport.Visible = True
        imgDate.Visible = False
        imgTreeMenu.Visible = False
        imgGroupingField.Visible = False
        imgEnquiryDetails.Visible = False
        imgRemark.Visible = False
        imgNetValue.Visible = False
        imgFieldConfig.Visible = False
        imgExpandCollapse.Visible = False

        Ctrl_PnlGeneralInfo_Visible = True

        Select Case lngSubModuleID
            'Sales List
            Case SubModuleType.SALES, SubModuleType.TOP80CUSTBYREP, _
            SubModuleType.SALESBYCUSTCOUNT
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlGroupField_Visible = True
                Ctrl_PnlNetValue_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.DRC, SubModuleType.CPROD, _
            SubModuleType.PREPLAN, SubModuleType.CABYCONT, _
            SubModuleType.SFMSPRFM, SubModuleType.COMMUPDINFO
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlGroupField_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.RELPERFORM, SubModuleType.KEYPRD, _
            SubModuleType.MTHOVER, SubModuleType.ACTYSALESBYINDSR, _
            SubModuleType.STKRETBYMTH, SubModuleType.CUSTSTATUS, SubModuleType.SALES_CUST_PER_PRD
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlNetValue_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.PDACOMM, SubModuleType.CABYMTH, _
            SubModuleType.CABYDAY, SubModuleType.PREPLANCUST, _
            SubModuleType.CALLCOV1, SubModuleType.CALLCOV1ADV, SubModuleType.CALLCOV2, _
            SubModuleType.CABYSTRIKE, SubModuleType.STKALLOC, _
            SubModuleType.CALLRATE, SubModuleType.SALESTGTVSACT, SubModuleType.SALESESTVSACT, _
            SubModuleType.CALLBYDKSHCLASS, SubModuleType.CALLBYSUPPLIERCLASS, _
             SubModuleType.CALLDTLBYGROUP, SubModuleType.SALESREP_KPI, SubModuleType.DAYCALLDISTRIBUTION, _
             SubModuleType.CALLACHIEVEMENT, SubModuleType.CALLBYMONTHADV, SubModuleType.CALLBYDAYADV, SubModuleType.MISSCALLANALYSIS, SubModuleType.REMAIN_CALL
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.SALESENQ, SubModuleType.CALLENQ, _
            SubModuleType.DRCENQ
                imgTreeMenu.Visible = False
                Ctrl_PnlGroupField_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.CHEQENQ, SubModuleType.PAFENQ, SubModuleType.COLLENQ
                imgTreeMenu.Visible = False
                Ctrl_PnlGroupField_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = False

            Case SubModuleType.CABYCUST, SubModuleType.CPRODACTYDTL, _
            SubModuleType.CPRODDAILYACTY, SubModuleType.SFMSACTY, _
            SubModuleType.SALESORD, SubModuleType.COLLINFO, _
            SubModuleType.TRAINFO, SubModuleType.DRCINFO, _
            SubModuleType.MSSINFO, SubModuleType.DNINFO, SubModuleType.SFMSEXTRACTION, _
            SubModuleType.DAILYCALLANALYSIS, SubModuleType.DAILYCALLACTY, SubModuleType.CALLACTYHISTBYCONT, _
            SubModuleType.CALLACTYENQ, SubModuleType.CPRODDAILYACTYCUST, SubModuleType.PREPLANEXTRACT, SubModuleType.MSSEXTRACTION, SubModuleType.DETINFO, SubModuleType.MERCHANDISINGDETAIL
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.PROD, SubModuleType.CUSTLIST, _
            SubModuleType.TXNDRC, SubModuleType.TXNMSS, _
            SubModuleType.TXNSFMS, SubModuleType.TXNSO, SubModuleType.TXNTRA
                imgExport.Visible = False
                Ctrl_PnlFieldConfig_Visible = True

            Case SubModuleType.SALESINFOBYDATE
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlNetValue_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.SALESSUMM, SubModuleType.SOVARIANCE
                Ctrl_PnlNetValue_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.CUSTCONTINFO, SubModuleType.CALLANALYENQ, SubModuleType.CPRODACTYSUP, SubModuleType.DRCEXTRACTION, _
            SubModuleType.CALLANALYBYCONTCLASS, SubModuleType.MISSCALLANALYSISDTL, _
            SubModuleType.CUSTCONTCALLINFO, SubModuleType.CUSTCONTCOVERAGEINFO, SubModuleType.CUSTCONTNONCOVERAGEINFO, SubModuleType.CUSTCONTEFFECTIVECALLINFO, SubModuleType.CUSTCONTNONEFFECTIVEINFO
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.AUDITLOG
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.TRAORDER
                'Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True


            Case SubModuleType.CALLINFOBYMONTH, SubModuleType.SALESFORCECAPACITY, SubModuleType.FIELDFORCESFMSDETACTY, SubModuleType.FIELDFORCEMTHACTY, _
                SubModuleType.STK_CONSUMP_BY_BIMONTH, SubModuleType.STK_CONSUMP_BY_QUARTER, SubModuleType.FIELDFORCEMTHACTYPrd, SubModuleType.STK_CONSUMP_BY_QUADMONTH
                Ctrl_PnlGeneralInfo_Visible = False
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.SRCALLRATE, SubModuleType.ORDPRDMATRIX
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlDateQuery_Visible = True
                imgExpandCollapse.Visible = True

            Case SubModuleType.FIELDFORCEPRDFREQ, SubModuleType.CALLRATE_BYBRAND, SubModuleType.TRAREPORT_EDI
                Ctrl_PnlRemark_Visible = True
                imgExpandCollapse.Visible = True

            Case SubModuleType.SRCALLRATEDTL, SubModuleType.PLAN_ACTUAL
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.SRSCORECARD, SubModuleType.STK_CONSUMP_BY_BIMONTH_DTL, SubModuleType.STK_CONSUMP_BY_QUARTER_DTL, SubModuleType.STK_CONSUMP_BY_QUADMONTH_DTL
                Ctrl_PnlGeneralInfo_Visible = False
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True

            Case SubModuleType.MTHSPECIALITY, SubModuleType.MTHTERRITORY, SubModuleType.MTHPRDFREQ
                Ctrl_PnlGeneralInfo_Visible = False
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                imgExpandCollapse.Visible = True

            Case SubModuleType.SALES_CUZ, SubModuleType.CUSTSALESANALYSIS, SubModuleType.CUSTPRDSALESANALYSIS, SubModuleType.SALES_BY_CHAIN, SubModuleType.SALES_BY_CUST
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                'Ctrl_PnlGroupField_Visible = True
                Ctrl_PnlNetValue_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                Ctrl_PnlRemark_Visible = True
                'for testing purposes, to be replaced and removed
            Case "101", SubModuleType.TRAReport
                imgExport.Visible = True
                Ctrl_PnlGeneralInfo_Visible = False
            Case SubModuleType.MERCHSFMS
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlGroupField_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
                'Ctrl_PnlRemark_Visible = True
            Case SubModuleType.DRCCuzList
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
            Case SubModuleType.ActCallKPI
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.ActCallKPIbySR
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.CUST_CONT_PRD_EXTRACT, SubModuleType.CUST_CONT_EXTRACT, SubModuleType.SFMS_EXTRACT, SubModuleType.VISIT_EXTRACT
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.SFEKPIREPORT 'Modified - SFE KPI Report 
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.DET_CALL_BY_AREA
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.DET_COVERAGE_BY_SPECIALTY
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.MUSTSELL
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.DET_COVERAGE_CALL_BY_CLASS_BY_PRODUCT
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.MERCHANDISING
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.COMPETITOR
                imgTreeMenu.Visible = True
                Ctrl_PnlDateQuery_Visible = True
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlFieldConfig_Visible = True
            Case SubModuleType.DAILYTRACKING
                Ctrl_PnlDateQuery_Visible = False
                Ctrl_PnlRemark_Visible = False
                Ctrl_PnlDailyTrackingRemark_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = True
                imgDate.Visible = False
                imgExport.Visible = False
                imgTreeMenu.Visible = True
                imgGroupingField.Visible = False
                imgEnquiryDetails.Visible = True
                imgRemark.Visible = True
            Case SubModuleType.CUSTOMERCOVERAGE
                Ctrl_PnlDateQuery_Visible = False
                Ctrl_PnlRemark_Visible = False
                Ctrl_PnlCustomerCoverageRemark_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = True
                imgDate.Visible = False
                imgExport.Visible = False
                imgTreeMenu.Visible = True
                imgGroupingField.Visible = False
                imgEnquiryDetails.Visible = True
                imgRemark.Visible = True
            Case SubModuleType.HISTORICALTRACKING
                Ctrl_PnlDateQuery_Visible = False
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = True
                imgDate.Visible = False
                imgExport.Visible = False
                imgTreeMenu.Visible = True
                imgGroupingField.Visible = False
                imgEnquiryDetails.Visible = True
                imgRemark.Visible = True
            Case SubModuleType.VIEW_SALESMAN_ACTIVITY
                Ctrl_PnlDateQuery_Visible = False
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = False
                imgDate.Visible = False
                imgExport.Visible = True
                imgTreeMenu.Visible = True
                imgGroupingField.Visible = False
                imgEnquiryDetails.Visible = False
                imgRemark.Visible = True
            Case SubModuleType.VIEW_SALESMAN_ACTIVITY_DETAIL
                Ctrl_PnlDateQuery_Visible = False
                Ctrl_PnlRemark_Visible = True
                Ctrl_PnlEnquiryDetail_Visible = False
                imgDate.Visible = False
                imgExport.Visible = True
                imgTreeMenu.Visible = False
                imgGroupingField.Visible = False
                imgEnquiryDetails.Visible = False
                imgRemark.Visible = True
            Case Else
                Ctrl_PnlDateQuery_Visible = False
                imgDate.Visible = False
                imgExport.Visible = False
                imgTreeMenu.Visible = False
                imgGroupingField.Visible = False
                imgEnquiryDetails.Visible = False
                imgRemark.Visible = False

        End Select
    End Sub

#Region "Panel Visibility"
    Public WriteOnly Property Ctrl_PnlTree_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgTreeMenu.Visible = boolVisible
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlGeneralInfo_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgGeneralInfo.Visible = boolVisible
            pnlGeneralInfo.Visible = boolVisible
            CPE_PnlGeneralInfo.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlDateQuery_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgDate.Visible = boolVisible
            pnlDateQuery.Visible = boolVisible
            CPE_PnlQueryDate.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlGroupField_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            CPE_PnlGroupField.Enabled = boolVisible
            imgGroupingField.Visible = boolVisible
            Dim strPanelID As String = pnlGroupingField.ID
            Select Case lngSubModuleID
                Case SubModuleType.SALESENQ
                    pnlSalesEnquirySearch.Visible = boolVisible
                    strPanelID = pnlSalesEnquirySearch.ID
                    imgGroupingField.ToolTip = "Search"
                Case SubModuleType.CALLENQ
                    pnlCallEnquirySearch.Visible = boolVisible
                    strPanelID = pnlCallEnquirySearch.ID
                    imgGroupingField.ToolTip = "Search"
                Case SubModuleType.DRCENQ
                    pnlDRCEnquirySearch.Visible = boolVisible
                    strPanelID = pnlDRCEnquirySearch.ID
                    imgGroupingField.ToolTip = "Search"
                Case SubModuleType.CHEQENQ
                    pnlCollCheqEnquirySearch.Visible = boolVisible
                    strPanelID = pnlCollCheqEnquirySearch.ID
                    imgGroupingField.ToolTip = "Search"
                Case SubModuleType.COLLENQ
                    pnlCollEnqSearch.Visible = boolVisible
                    strPanelID = pnlCollEnqSearch.ID
                    imgGroupingField.ToolTip = "Search"
                Case SubModuleType.PAFENQ
                    pnlPAFEnquirySearch.Visible = boolVisible
                    strPanelID = pnlPAFEnquirySearch.ID
                    imgGroupingField.ToolTip = "Search"
                Case Else
                    pnlGroupingField.Visible = boolVisible
                    strPanelID = pnlGroupingField.ID
            End Select
            If boolVisible Then
                CPE_PnlGroupField.TargetControlID = strPanelID
            End If
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlEnquiryDetail_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgEnquiryDetails.Visible = boolVisible
            Dim strPanelID As String = pnlSalesEnquirySearchDetails.ID
            Select Case lngSubModuleID
                Case SubModuleType.SALESENQ 'Sales Enquiry
                    pnlSalesEnquirySearchDetails.Visible = boolVisible
                    strPanelID = pnlSalesEnquirySearchDetails.ID
                Case SubModuleType.CALLENQ 'Call Enquiry
                    pnlCallEnquirySearchDetails.Visible = boolVisible
                    strPanelID = pnlCallEnquirySearchDetails.ID
                Case SubModuleType.DRCENQ 'DRC Enquiry
                    pnlDRCEnquirySearchDetails.Visible = boolVisible
                    strPanelID = pnlDRCEnquirySearchDetails.ID
                Case SubModuleType.CHEQENQ 'Coll Cheque Enquiry
                    pnlCollCheqEnquirySearchDetails.Visible = boolVisible
                    strPanelID = pnlCollCheqEnquirySearchDetails.ID
                Case SubModuleType.COLLENQ
                    pnlCollEnqSearchDetails.Visible = boolVisible
                    strPanelID = pnlCollEnqSearchDetails.ID
                Case SubModuleType.PAFENQ 'PAF Enquiry
                    pnlPAFEnquirySearchDetails.Visible = boolVisible
                    strPanelID = pnlPAFEnquirySearchDetails.ID
                Case SubModuleType.DAILYTRACKING
                    pnlDailyTrackingSearchDetails.Visible = boolVisible
                    strPanelID = pnlDailyTrackingSearchDetails.ID
                Case SubModuleType.CUSTOMERCOVERAGE
                    pnlCustomerCoverageSearchDetails.Visible = boolVisible
                    strPanelID = pnlCustomerCoverageSearchDetails.ID
                Case SubModuleType.HISTORICALTRACKING
                    pnlHistoricalTrackingSearchDetails.Visible = boolVisible
                    strPanelID = pnlHistoricalTrackingSearchDetails.ID
            End Select
            If boolVisible Then
                CPE_PnlEnquiryDetail.TargetControlID = strPanelID
            End If
            CPE_PnlEnquiryDetail.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlNetValue_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            If boolVisible Then
                If Session("NV_Enable") IsNot Nothing AndAlso Session("NV_Enable") = 1 Then
                    imgNetValue.Visible = True
                    pnlNetValue.Visible = True
                    CPE_pnlNetValue.Enabled = True
                End If
            Else
                imgNetValue.Visible = False
                pnlNetValue.Visible = False
                CPE_pnlNetValue.Enabled = False
            End If
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlRemark_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            'imgRemark.Visible = boolVisible
            'Dim strPanelID As String = pnlCallAnalysisByMonthInfo.ID
            'Select Case lngSubModuleID
            '    Case SubModuleType.CABYMTH 'Call Analysis By Month
            '        pnlCallAnalysisByMonthInfo.Visible = boolVisible
            '        strPanelID = pnlCallAnalysisByMonthInfo.ID
            '    Case SubModuleType.CABYDAY 'Call Analysis By Day
            '        pnlCallAnalysisByDayInfo.Visible = boolVisible
            '        strPanelID = pnlCallAnalysisByDayInfo.ID
            'End Select
            'If boolVisible Then
            '    CPE_PnlRemark.TargetControlID = strPanelID
            'End If
            'CPE_PnlRemark.Enabled = boolVisible

            imgRemark.Visible = boolVisible
            pnlRemark.Visible = boolVisible
            CPE_PnlRemark.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlDailyTrackingRemark_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgRemark.Visible = boolVisible
            pnlDailyTrackingRemark.Visible = boolVisible
            CPE_PnlRemark.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlCustomerCoverageRemark_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            imgRemark.Visible = boolVisible
            pnlCustomerCoverageRemark.Visible = boolVisible
            CPE_PnlRemark.Enabled = boolVisible
        End Set
    End Property

    Public WriteOnly Property SearchPanelVisibility() As Boolean
        Set(ByVal boolVisible As Boolean)
            CPE_PnlGroupField.ClientState = (Not boolVisible).ToString.ToLower
            CPE_PnlGroupField.Collapsed = Not boolVisible
            'UpdateControlPanel()
        End Set
    End Property

    Public WriteOnly Property Ctrl_PnlFieldConfig_Visible() As Boolean
        Set(ByVal boolVisible As Boolean)
            If boolVisible Then
                If Session("FC_Enable") IsNot Nothing AndAlso Session("FC_Enable") = 1 Then
                    imgFieldConfig.Visible = True
                    pnlFieldConfig.Visible = True
                    CPE_pnlFieldConfig.Enabled = True
                End If
            Else
                imgFieldConfig.Visible = False
                pnlFieldConfig.Visible = False
                CPE_pnlFieldConfig.Enabled = False
            End If
        End Set
    End Property
#End Region

#Region "Handle Control Click Event"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        RaiseEvent ExportBtn_Click(Me, e)
        'Try
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnExport_Click : " & ex.ToString)
        'End Try
    End Sub

    'Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
    '    Try
    '        RaiseEvent SearchBtn_Click(Me, e)
    '        RaiseEvent LayoutChanged(sender, e)
    '        UpdateControlPanel()
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".imgSearch_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub imgGeneralInfo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgGeneralInfo.Click
    '    pnlGeneralInfo.Visible = Not pnlGeneralInfo.Visible
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        RaiseEvent ExportBtn_Click(Me, e)
        'Try
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnExport_Click : " & ex.ToString)
        'End Try
    End Sub

    'Private Sub imgDate_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDate.Click
    '    pnlDateQuery.Visible = Not pnlDateQuery.Visible
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgGroupField_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgGroupingField.Click
    '    Select Case lngSubModuleID
    '        Case SubModuleType.SFMSEXTRACT 'SFMS Extract
    '            pnlSFMSExtractSearch.Visible = Not pnlSFMSExtractSearch.Visible
    '        Case SubModuleType.SALESENQ
    '            pnlSalesEnquirySearch.Visible = Not pnlSalesEnquirySearch.Visible
    '            'Case SubModuleType.SFMSACTY
    '            '    pnlSFMSActySearch.Visible = Not pnlSFMSActySearch.Visible
    '        Case SubModuleType.CALLENQ
    '            pnlCallEnquirySearch.Visible = Not pnlCallEnquirySearch.Visible
    '        Case Else
    '            pnlGroupingField.Visible = Not pnlGroupingField.Visible
    '    End Select
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgRemark_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgRemark.Click
    '    Select Case lngSubModuleID
    '        Case SubModuleType.CABYDAY 'Call Analysis By Day
    '            pnlCallAnalysisByDayInfo.Visible = Not pnlCallAnalysisByDayInfo.Visible
    '        Case SubModuleType.CABYMTH 'Call Analysis By Month
    '            pnlCallAnalysisByMonthInfo.Visible = Not pnlCallAnalysisByMonthInfo.Visible
    '    End Select
    '    RaiseEvent LayoutChanged(sender, e)

    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgSearchDetails_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEnquiryDetails.Click
    '    Select Case lngSubModuleID
    '        Case SubModuleType.SALESENQ 'Sales Enquiry
    '            pnlSalesEnquirySearchDetails.Visible = Not pnlSalesEnquirySearchDetails.Visible
    '            'Case SubModuleType.SFMSACTY 'SFMS Acty List
    '            '    pnlSFMSActySearchDetails.Visible = Not pnlSFMSActySearchDetails.Visible
    '            'Case SubModuleType.CPROD 'Call Prod List
    '            '    pnlCallProdSearchDetails.Visible = Not pnlCallProdSearchDetails.Visible
    '        Case SubModuleType.CALLENQ                'Call Enquiry
    '            pnlCallEnquirySearchDetails.Visible = Not pnlCallEnquirySearchDetails.Visible
    '    End Select

    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrint.Click

    'End Sub

#Region "Close Panel"
    'Private Sub imgClose1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose1.Click
    '    pnlGeneralInfo.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgCloce_Export_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose_Export.Click
    '    pnlExport.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgClose3_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose3.Click
    '    pnlDateQuery.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Protected Sub imgCallEnq_Close_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCallEnq_Close.Click
    '    pnlCallEnquirySearch.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Protected Sub imgCallEnqDtl_Close_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCallEnqDtl_Close.Click
    '    pnlCallEnquirySearchDetails.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgClose4_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose4.Click
    '    pnlGroupingField.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgClose7_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose7.Click
    '    pnlSalesEnquirySearch.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgClose8_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose8.Click
    '    pnlSalesEnquirySearchDetails.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgClose12_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose12.Click
    '    pnlCallAnalysisByDayInfo.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    'End Sub

    'Private Sub imgClose13_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose13.Click
    '    pnlCallAnalysisByMonthInfo.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    'End Sub

    'Private Sub imgClose10_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose10.Click
    '    pnlSFMSActySearch.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub

    'Private Sub imgClose11_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose11.Click
    '    pnlSFMSActySearchDetails.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    '    UpdateControlPanel()
    'End Sub
    'Private Sub imgClose14_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose14.Click
    '    pnlCallProdSearchDetails.Visible = False
    '    RaiseEvent LayoutChanged(sender, e)
    'End Sub

#End Region

#End Region

#Region "Export"
    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        Excel.ExportToFile(dgList, Response, customFileName, SheetStyle)
    End Sub
    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        Excel.ExportToFile(aryDgList, Response, customFileName, SheetStyle)
    End Sub
    'HL:20080508
    Public Sub ExportToFile(ByRef PrintPanel As Object, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        Excel.ExportToFile(PrintPanel, Response, customFileName, SheetStyle)
    End Sub
#End Region

#Region "pnlDetails"
    'Public Property UserName() As String
    '    Get
    '        Return lblUserName.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblUserName.Text = Trim(value)
    '    End Set
    'End Property
    Public Property Country() As String
        Get
            Return lblCountryName.Text
        End Get
        Set(ByVal value As String)
            lblCountryName.Text = Trim(value)
        End Set
    End Property
    Public Property Principle() As String
        Get
            Return lblPrincipalName.Text
        End Get
        Set(ByVal value As String)
            lblPrincipalName.Text = Trim(value)
        End Set
    End Property
    'Public Property Team() As String
    '    Get
    '        Return lblMapPath.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblMapPath.Text = Trim(value)
    '    End Set
    'End Property
    'Public Property Region() As String
    '    Get
    '        Return lblRegionName.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblRegionName.Text = Trim(value)
    '    End Set
    'End Property
    ''Public Property Area() As String
    ''    Get
    ''        Return lblAreaName.Text
    ''    End Get
    ''    Set(ByVal value As String)
    ''        lblAreaName.Text = Trim(value)
    ''    End Set
    ''End Property
    'Public Property SalemanName() As String
    '    Get
    '        Return lblSalesmanName.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblSalesmanName.Text = Trim(value)
    '    End Set
    'End Property
    Public Property YearValue() As String
        Get
            Return lblYearValue.Text
        End Get
        Set(ByVal value As String)
            lblYearValue.Text = Trim(value)
        End Set
    End Property
    Public Property MonthValue() As String
        Get
            Return lblMonthValue.Text
        End Get
        Set(ByVal value As String)
            lblMonthValue.Text = Trim(value)
        End Set
    End Property
    Public Property SelectedDateTimeString() As String
        Get
            Return Wuc_lblSelectedDate.Text
        End Get
        Set(ByVal value As String)
            Wuc_lblSelectedDate.Text = Trim(value)
        End Set
    End Property
    Public Property SelectedDateTimeValue() As Date
        Get
            Return Wuc_lblSelectedDate.SelectedDateTime
        End Get
        Set(ByVal value As Date)
            Wuc_lblSelectedDate.SelectedDateTime = value
            'Session("SelectedDate") = Wuc_lblSelectedDate.SelectedDateTime
        End Set
    End Property
    'Public Property Report() As String
    '    Get
    '        Return lblTimestamp.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblTimestamp.Text = Trim(value)
    '    End Set
    'End Property
    Public Property ReportDateTimeString() As String
        Get
            Return Wuc_lblTimestamp.Text
        End Get
        Set(ByVal value As String)
            Wuc_lblTimestamp.Text = Trim(value)
        End Set
    End Property
    Public Property ReportDateTimeValue() As DateTime
        Get
            Return Wuc_lblTimestamp.SelectedDateTime
        End Get
        Set(ByVal value As Date)
            Wuc_lblTimestamp.SelectedDateTime = value
        End Set
    End Property
    'Public Property ModuleName() As String
    '    Get
    '        Return lblModule.Text
    '    End Get
    '    Set(ByVal value As String)
    '        lblModuleName.Text = Trim(value)
    '    End Set
    'End Property

    Public Sub RefreshDetails(Optional ByVal blnIsPostback As Boolean = False)
        'lblUserName.Text = HttpContext.Current.Session("UserName")
        lblCountryName.Text = HttpContext.Current.Session("COUNTRY_NAME")
        lblPrincipalName.Text = HttpContext.Current.Session("PRINCIPAL_NAME")
        'lblMapPath.Text = HttpContext.Current.Session("TEAM_NAME")
        ''lblTeamName.Text = HttpContext.Current.session("TEAM_NAME") & "|" & HttpContext.Current.session("REGION_NAME")
        'lblRegionName.Text = HttpContext.Current.Session("REGION_NAME")
        ''lblAreaName.Text = HttpContext.Current.Session("currentAreaName")
        'lblSalesmanName.Text = HttpContext.Current.Session("SALESREP_NAME")
        lblYearValue.Text = HttpContext.Current.Session("Year")
        lblMonthValue.Text = HttpContext.Current.Session("Month")
        Dim strDT As String = SelectedDateTimeString
        'SelectedDateTimeString = IIf(String.IsNullOrEmpty(strDT), Session("SelectedDate"), strDT)
        If blnIsPostback = False Then ReportDateTimeValue = Now
        'lblModuleName.Text = HttpContext.Current.Session("ModuleName")

        Dim strValuePath As String = Session("TREE_PATH")
        Dim strValues(), strName, strvalue As String
        Dim strMap As New StringBuilder
        If Not strValuePath = String.Empty Then
            strValues = strValuePath.Split("/")
            For Each strvalue In strValues
                strName = strvalue.Split("@")(0).Replace("_CODE", "_NAME")
                strvalue = Session(strName)
                If strMap.ToString <> String.Empty Then strMap.Append(" > ")
                strMap.Append(strvalue)
            Next
        End If
        lblMapPath.Text = strMap.ToString
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".UpdateDetails : " & ex.ToString)
        'Finally
        'End Try
    End Sub
#End Region

#Region "  iFFMR Sales_Enquiry"

    '---------------------------------------------------------------------
    ' Procedure 	    : 	Sub btnReset_Click
    ' Purpose	        :	Reset all the text box in Sales Enquiry Search Panel
    '                       (Does not reset the SalesEnquirySearchDetails panel)
    '----------------------------------------------------------------------
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtProductNo.Text = ""
        txtProductName.Text = ""
        txtCustCode.Text = ""
        txtCustName.Text = ""
        txtSalesmanNo.Text = ""
        txtSalesmanName.Text = ""
        txtInvNo.Text = ""
        WucSalesEnq_Calendar.DateStart = ""
        WucSalesEnq_Calendar.DateEnd = ""
        'Wuc_txtFromDate.Text = ""
        'Wuc_txtToDate.Text = ""
        RaiseEvent EnqResetBtn_Click(sender, e)
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	    : 	Sub Parse_btnRefresh5
    ' Purpose	        :	Parse the refresh button control(btnRefresh5) in SalesEnquirySearchPanel to another page.
    ' Page              :	[from] : This class
    '   		            [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------

    'Public Sub Parse_btnRefresh5(ByRef btnRefresh5Ref As Button)
    '    btnRefresh5Ref = btnRefresh5
    'End Sub
    Public Sub btnRefresh5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh5.Click
        Page.Validate()
        RaiseEvent SalesEnquiryBtn_Click(sender, e)
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	    : 	Sub UpdateSalesEnquirySearchDetails()
    ' Purpose	        :	Display the current search criteria in SalesEnquirySearchDetailsPanel
    '----------------------------------------------------------------------
    Public Sub UpdateSalesEnquirySearchDetails()
        lblCustomerDetails.Text = IIf(Trim(txtCustCode.Text).Length > 0, Trim(txtCustName.Text), "") & " - (" & Trim(txtCustCode.Text) & ")"
        lblProductDetails.Text = IIf(Trim(txtProductNo.Text).Length > 0, Trim(txtProductName.Text), "") & " - (" & Trim(txtProductNo.Text) & ")"
        lblSalesmanDetails.Text = IIf(Trim(txtSalesmanNo.Text).Length > 0, Trim(txtSalesmanName.Text), "") & " - (" & Trim(txtSalesmanNo.Text) & ")"
        lblInvoiceDetails.Text = IIf(Trim(txtInvNo.Text).Length > 0, Trim(txtInvNo.Text), " - ")
        lblNetValueDetails.Text = rblNetValue.SelectedValue
        lblDateFromDetails.Text = WucSalesEnq_Calendar.DateStart 'Wuc_txtFromDate.Text
        lblDateToDetails.Text = WucSalesEnq_Calendar.DateEnd 'Wuc_txtToDate.Text
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property isShiptoCodeExist()
    ' Purpose	        :	Check whether shiptocode is 1 of the search criteria
    '                       If yes then show product info, if no the show customer info.
    ' Page              :	SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public ReadOnly Property isShiptoCodeExist() As Boolean
        Get
            Return IIf(txtCustCode.Text.Length > 5, True, False)
        End Get
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property ProductCode()
    ' Purpose	        :	Return the product code in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property ProductCode() As String
        Get
            Return Trim(txtProductNo.Text)
        End Get
        Set(ByVal value As String)
            txtProductNo.Text = value
        End Set
    End Property

    Public Property ProductDescription() As String
        Get
            Return Trim(txtProductName.Text)
        End Get
        Set(ByVal value As String)
            txtProductName.Text = value
        End Set
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property ShipToCode()
    ' Purpose	        :	Return the shipto code in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property CustomerCode() As String
        Get
            Return Trim(txtCustCode.Text)
        End Get
        Set(ByVal value As String)
            txtCustCode.Text = value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return Trim(txtCustName.Text)
        End Get
        Set(ByVal value As String)
            txtCustName.Text = value
        End Set
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property Salesmancode()
    ' Purpose	        :	Return the salesman code in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property SalesmanCode() As String
        Get
            Return Trim(txtSalesmanNo.Text)
        End Get
        Set(ByVal value As String)
            txtSalesmanNo.Text = value
        End Set
    End Property

    Public Property SalesmanName() As String
        Get
            Return Trim(txtSalesmanName.Text)
        End Get
        Set(ByVal value As String)
            txtSalesmanName.Text = value
        End Set
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property InvoiceNumber()
    ' Purpose	        :	Return the invoice number in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property InvoiceNumber() As String
        Get
            Return Trim(txtInvNo.Text)
        End Get
        Set(ByVal value As String)
            txtInvNo.Text = value
        End Set
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	Property Sales Net Value()
    ' Purpose	        :	Return the invoice number in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property NetValue() As Integer
        Get
            If rblNetValue.SelectedIndex < 0 Then
                Dim intNetValue As Integer = CInt(Session("NetValue"))
                Dim li As ListItem = rblNetValue.Items.FindByValue(intNetValue)
                If li IsNot Nothing Then li.Selected = True
            End If
            Return CInt(rblNetValue.SelectedValue)
        End Get
        Set(ByVal value As Integer)
            Dim li As ListItem = rblNetValue.Items.FindByValue(value)
            If li IsNot Nothing Then li.Selected = True
        End Set
    End Property


    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property Datefrom()
    ' Purpose	        :	Return the datefrom string in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property DateFrom() As String
        Get
            Return WucSalesEnq_Calendar.DateStart
        End Get
        Set(ByVal value As String)
            WucSalesEnq_Calendar.DateStart = value
        End Set
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	    : 	ReadOnly Property Dateto()
    ' Purpose	        :	Return the dateto string in SalesEnquirySearchPanel
    ' Page              :   [to]   : SalesEnquiryList.aspx
    '----------------------------------------------------------------------
    Public Property Dateto() As String
        Get
            Return WucSalesEnq_Calendar.DateEnd
        End Get
        Set(ByVal value As String)
            WucSalesEnq_Calendar.DateEnd = value
        End Set
    End Property
#End Region

#Region "iFFMR SFE CALL Enquiry"
    Protected Sub btnCalEnq_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalEnq_Reset.Click
        txtCallEnq_CustCode.Text = ""
        txtCallEnq_CustName.Text = ""
        txtCallEnq_ContCode.Text = ""
        txtCallEnq_ContName.Text = ""
        txtCallEnq_Salesman.Text = ""
        txtCallEnq_SalesmanName.Text = ""
        WucCallEnq_Calendar.DateStart = ""
        WucCallEnq_Calendar.DateEnd = ""
        'WucCallEnq_txtFromDate.Text = ""
        'WucCallEnq_txtToDate.Text = ""

        RaiseEvent EnqResetBtn_Click(sender, e)
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnCalEnq_Reset_Click : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub btnCallEnq_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCallEnq_Refresh.Click
        Page.Validate()
        UpdateCallEnquirySearchDetail()
        RaiseEvent CallEnquiryBtn_Click(sender, e)
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnCallEnq_Refresh_Click : " & ex.ToString)
        'End Try
    End Sub

    Public Sub UpdateCallEnquirySearchDetail()
        lblCallEnq_Customer.Text = IIf(Trim(txtCallEnq_CustName.Text).Length > 0, Trim(txtCallEnq_CustName.Text), " ") & " - ( " & Trim(txtCallEnq_CustCode.Text) & " )"
        lblCallEnq_Contact.Text = IIf(Trim(txtCallEnq_ContName.Text).Length > 0, Trim(txtCallEnq_ContName.Text), " ") & " - ( " & Trim(txtCallEnq_ContCode.Text) & " )"
        lblCallEnq_Salesman.Text = IIf(Trim(txtCallEnq_SalesmanName.Text).Length > 0, Trim(txtCallEnq_SalesmanName.Text), " ") & " - ( " & Trim(txtCallEnq_Salesman.Text) & " )"
        WucCallEnq_lblDateFrom.Text = IIf(Trim(WucCallEnq_Calendar.DateStart).Length > 0, Trim(WucCallEnq_Calendar.DateStart), " - ")
        WucCallEnq_lblDateTo.Text = IIf(Trim(WucCallEnq_Calendar.DateEnd).Length > 0, Trim(WucCallEnq_Calendar.DateEnd), " - ")
        UpdateControlPanel()
        'Try
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".UpdateCallEnquirySearchDetail : " & ex.ToString)
        'End Try
    End Sub


    Public Property CallEnq_DateStart() As String
        Get
            Return WucCallEnq_Calendar.DateStart
        End Get
        Set(ByVal value As String)
            WucCallEnq_Calendar.DateStart = value
        End Set
    End Property
    Public Property CallEnq_DateEnd() As String
        Get
            Return WucCallEnq_Calendar.DateEnd
        End Get
        Set(ByVal value As String)
            WucCallEnq_Calendar.DateEnd = value
        End Set
    End Property
    Public Property CallEnq_Salesman() As String
        Get
            Return Trim(txtCallEnq_Salesman.Text)
        End Get
        Set(ByVal value As String)
            txtCallEnq_Salesman.Text = value
        End Set
    End Property
    Public Property CallEnq_CustCode() As String
        Get
            Return Trim(txtCallEnq_CustCode.Text)
        End Get
        Set(ByVal value As String)
            txtCallEnq_CustCode.Text = value
        End Set
    End Property
    Public Property CallEnq_ContCode() As String
        Get
            Return Trim(txtCallEnq_ContCode.Text)
        End Get
        Set(ByVal value As String)
            txtCallEnq_ContCode.Text = value
        End Set
    End Property
#End Region

#Region "  iFFMR DRC_Enquiry"
    Protected Sub btnDRCEnq_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDRCEnq_Reset.Click
        Try
            txtDRCEnq_ProductNo.Text = ""
            txtDRCEnq_ProductName.Text = ""
            txtDRCEnq_CustCode.Text = ""
            txtDRCEnq_CustName.Text = ""
            txtDRCEnq_SalesmanNo.Text = ""
            txtDRCEnq_SalesmanName.Text = ""
            WucDrcEnq_Calendar.DateStart = ""
            WucDrcEnq_Calendar.DateEnd = ""
            'WucDRCEnq_txtFromDate.Text = ""
            'WucDRCEnq_txtToDate.Text = ""
            RaiseEvent EnqResetBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnDRCEnq_Reset_Click : " & ex.ToString)
        End Try
    End Sub

    Public Sub btnDRCEnq_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDRCEnq_Refresh.Click
        Try
            Page.Validate()
            UpdateDRCEnquirySearchDetails()
            RaiseEvent DRCEnquiryBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnDRCEnq_Refresh_Click : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateDRCEnquirySearchDetails()
        Try
            lblDRCEnq_CustomerDetails.Text = IIf(Trim(txtDRCEnq_CustCode.Text).Length > 0, Trim(txtDRCEnq_CustName.Text), "") & " - (" & Trim(txtDRCEnq_CustCode.Text) & ")"
            lblDRCEnq_ProductDetails.Text = IIf(Trim(txtDRCEnq_ProductNo.Text).Length > 0, Trim(txtDRCEnq_ProductName.Text), "") & " - (" & Trim(txtDRCEnq_ProductNo.Text) & ")"
            lblDRCEnq_SalesmanDetails.Text = IIf(Trim(txtDRCEnq_SalesmanNo.Text).Length > 0, Trim(txtDRCEnq_SalesmanName.Text), "") & " - (" & Trim(txtDRCEnq_SalesmanNo.Text) & ")"
            WucDRCEnq_lblDateFrom.Text = WucDrcEnq_Calendar.DateStart
            WucDRCEnq_lblDateTo.Text = WucDrcEnq_Calendar.DateEnd
        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateDRCEnquirySearchDetails : " & ex.ToString)
        End Try
    End Sub

    Public Property DRCEnq_ProductCode() As String
        Get
            Return Trim(txtDRCEnq_ProductNo.Text)
        End Get
        Set(ByVal value As String)
            txtDRCEnq_ProductNo.Text = value
        End Set
    End Property

    Public Property DRCEnq_ProductDescription() As String
        Get
            Return Trim(txtDRCEnq_ProductName.Text)
        End Get
        Set(ByVal value As String)
            txtDRCEnq_ProductName.Text = value
        End Set
    End Property

    Public Property DRCEnq_CustomerCode() As String
        Get
            Return Trim(txtDRCEnq_CustCode.Text)
        End Get
        Set(ByVal value As String)
            txtDRCEnq_CustCode.Text = value
        End Set
    End Property

    Public Property DRCEnq_CustomerName() As String
        Get
            Return Trim(txtDRCEnq_CustName.Text)
        End Get
        Set(ByVal value As String)
            txtDRCEnq_CustName.Text = value
        End Set
    End Property

    Public Property DRCEnq_SalesmanCode() As String
        Get
            Return Trim(txtDRCEnq_SalesmanNo.Text)
        End Get
        Set(ByVal value As String)
            txtDRCEnq_SalesmanNo.Text = value
        End Set
    End Property

    Public Property DRCEnq_SalesmanName() As String
        Get
            Return Trim(txtDRCEnq_SalesmanName.Text)
        End Get
        Set(ByVal value As String)
            txtDRCEnq_SalesmanName.Text = value
        End Set
    End Property

    Public Property DRCEnq_DateFrom() As String
        Get
            Return WucDrcEnq_Calendar.DateStart
        End Get
        Set(ByVal value As String)
            WucDrcEnq_Calendar.DateStart = value
        End Set
    End Property

    Public Property DRCEnq_Dateto() As String
        Get
            Return WucDrcEnq_Calendar.DateEnd
        End Get
        Set(ByVal value As String)
            WucDrcEnq_Calendar.DateEnd = value
        End Set
    End Property
#End Region

#Region "  iFFMR Customer Coverage"
    Private Sub LoadCustomerCoverageProductGroups()
        Try
            Dim clsTxncommon As New txn_common.clstxncommon
            Dim data As DataTable = clsTxncommon.GetSalesRepPrdGrp(Session("SALESREP_LIST"))
            For Each row As DataRow In data.Rows
                Dim li As New ListItem(row("prd_grp_name").ToString, row("prd_grp_code"))
                customercoverage_ddlProductGroup.Items.Add(li)
            Next
        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadSalesRepProductGroups : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCustomerCoverageProductCode()
        Try
            Dim clsTxncommon As New txn_common.clstxncommon
            Dim data As DataTable = clsTxncommon.GetSalesRepPrdCode(Session("SALESREP_LIST"), customercoverage_ddlProductGroup.SelectedItem.Value)
            customercoverage_ddlProductCode.Items.Clear()
            customercoverage_ddlProductCode.Items.Add(New ListItem("All / Product code", ""))
            For Each row As DataRow In data.Rows
                Dim li As New ListItem(row("prd_name").ToString, row("prd_code"))
                customercoverage_ddlProductCode.Items.Add(li)
            Next
        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadSalesRepProductCode : " & ex.ToString)
        End Try
    End Sub

    Protected Sub customercoverage_ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            LoadCustomerCoverageProductCode()
            UpdateControlPanel()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".customercoverage_ddlProductGroup_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Public Sub RefreshCustomerCoverageProduct(Optional ByVal blnIsPostback As Boolean = False)
        LoadCustomerCoverageProductGroups()
        LoadCustomerCoverageProductCode()

        UpdateControlPanel()
    End Sub
    'Protected Sub btnDailyTracking_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDailyTracking_Reset.Click
    '    Try


    '        RaiseEvent EnqResetBtn_Click(sender, e)
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".btnDailyTracking_Reset_Click : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

#Region "  iFFMR CollCheq_Enquiry"
    Protected Sub btnCollCheqEnq_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCollCheqEnq_Reset.Click
        Try
            txtCollCheqEnq_CustCode.Text = ""
            txtCollCheqEnq_CustName.Text = ""
            txtCollCheqEnq_SalesmanNo.Text = ""
            txtCollCheqEnq_SalesmanName.Text = ""
            txtCollCheqEnq_CheqNo.Text = ""
            txtCollCheqEnq_ReceiptNo.Text = ""
            WucCollEnq_Calendar.DateStart = ""
            WucCollEnq_Calendar.DateEnd = ""
            WucCollEnq_TxnCalendar.DateStart = ""
            WucCollEnq_TxnCalendar.DateEnd = ""
            'WucCollCheqEnq_txtFromDate.Text = ""
            'WucCollCheqEnq_txtToDate.Text = ""
            RaiseEvent EnqResetBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnCollCheqEnq_Reset_Click : " & ex.ToString)
        End Try
    End Sub

    Public Sub btnCollCheqEnq_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCollCheqEnq_Refresh.Click
        Try
            Page.Validate()
            UpdateCollCheqEnquirySearchDetails()
            RaiseEvent CollCheqEnquiryBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnCollCheqEnq_Refresh_Click : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateCollCheqEnquirySearchDetails()
        Try
            lblCollCheqEnq_CustomerDetails.Text = IIf(Trim(txtCollCheqEnq_CustCode.Text).Length > 0, Trim(txtCollCheqEnq_CustName.Text), "") & " - (" & Trim(txtCollCheqEnq_CustCode.Text) & ")"
            lblCollCheqEnq_SalesmanDetails.Text = IIf(Trim(txtCollCheqEnq_SalesmanNo.Text).Length > 0, Trim(txtCollCheqEnq_SalesmanName.Text), "") & " - (" & Trim(txtCollCheqEnq_SalesmanNo.Text) & ")"
            lblCollCheqEnq_CheqDetails.Text = IIf(Trim(txtCollCheqEnq_CheqNo.Text).Length > 0, Trim(txtCollCheqEnq_CheqNo.Text), " - ")
            WucCollCheqEnq_lblDateFrom.Text = WucCollEnq_Calendar.DateStart
            WucCollCheqEnq_lblDateTo.Text = WucCollEnq_Calendar.DateEnd
        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateCollCheqEnquirySearchDetails : " & ex.ToString)
        End Try
    End Sub

    Public Property CollCheqEnq_CustomerCode() As String
        Get
            Return Trim(txtCollCheqEnq_CustCode.Text)
        End Get
        Set(ByVal value As String)
            txtCollCheqEnq_CustCode.Text = value
        End Set
    End Property

    Public Property CollCheqEnq_CustomerName() As String
        Get
            Return Trim(txtCollCheqEnq_CustName.Text)
        End Get
        Set(ByVal value As String)
            txtCollCheqEnq_CustName.Text = value
        End Set
    End Property

    Public Property CollCheqEnq_SalesmanCode() As String
        Get
            Return Trim(txtCollCheqEnq_SalesmanNo.Text)
        End Get
        Set(ByVal value As String)
            txtCollCheqEnq_SalesmanNo.Text = value
        End Set
    End Property

    Public Property CollCheqEnq_SalesmanName() As String
        Get
            Return Trim(txtCollCheqEnq_SalesmanName.Text)
        End Get
        Set(ByVal value As String)
            txtCollCheqEnq_SalesmanName.Text = value
        End Set
    End Property

    Public Property CollCheqEnq_CheqNo() As String
        Get
            Return Trim(txtCollCheqEnq_CheqNo.Text)
        End Get
        Set(ByVal value As String)
            txtCollCheqEnq_CheqNo.Text = value
        End Set
    End Property

    Public Property CollCheqEnq_DateFrom() As String
        Get
            Return WucCollEnq_Calendar.DateStart
        End Get
        Set(ByVal value As String)
            WucCollEnq_Calendar.DateStart = value
        End Set
    End Property

    Public Property CollCheqEnq_Dateto() As String
        Get
            Return WucCollEnq_Calendar.DateEnd
        End Get
        Set(ByVal value As String)
            WucCollEnq_Calendar.DateEnd = value
        End Set
    End Property

    Public Property CollCheqEnq_ReceiptNo() As String
        Get
            Return Trim(txtCollCheqEnq_ReceiptNo.Text)
        End Get
        Set(ByVal value As String)
            txtCollCheqEnq_ReceiptNo.Text = value
        End Set
    End Property

    Public Property CollCheqEnq_TxnDateFrom() As String
        Get
            Return WucCollEnq_TxnCalendar.DateStart
        End Get
        Set(ByVal value As String)
            WucCollEnq_TxnCalendar.DateStart = value
        End Set
    End Property

    Public Property CollCheqEnq_TxnDateto() As String
        Get
            Return WucCollEnq_TxnCalendar.DateEnd
        End Get
        Set(ByVal value As String)
            WucCollEnq_TxnCalendar.DateEnd = value
        End Set
    End Property

    Public Property CollCheqEnq_TeamCode() As String
        Get
            Return Trim(ddlteam.SelectedValue)
        End Get
        Set(ByVal value As String)
            ddlteam.SelectedValue = value
        End Set
    End Property

    Public Sub CollChecEnq_LoadDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL
            With ddlteam
                .Items.Clear()
                .DataSource = dtTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- ALL --", ""))
                .SelectedIndex = 0
                ' .SelectedValue = IIf(Session("SELECTED_TEAM") = "", "", Session("SELECTED_TEAM"))
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "  iFFMR CollEnq"
    Protected Sub btnCollEnq_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCollEnq_Reset.Click
        Try
            txtCollEnq_CustCode.Text = ""
            txtCollEnq_CustName.Text = ""
            txtCollEnq_SalesrepCode.Text = ""
            txtCollEnq_SalesrepName.Text = ""
            txtCollEnq_SoNo.Text = ""
            txtCollEnq_InvNo.Text = ""
            WucCollEnq_Calendar.DateStart = ""
            WucCollEnq_Calendar.DateEnd = ""
            WucCollEnq_TxnCalendar.DateStart = ""
            WucCollEnq_TxnCalendar.DateEnd = ""
            RaiseEvent EnqResetBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnCollEnq_Reset_Click : " & ex.ToString)
        End Try
    End Sub

    Public Sub btnCollEnq_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCollEnq_Refresh.Click
        Try
            Page.Validate()
            UpdateCollEnqSearchDtl()
            RaiseEvent CollEnqyBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnCollEnq_Refresh_Click : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateCollEnqSearchDtl()
        Try
            lblCollEnq_CustDtl.Text = IIf(Trim(txtCollEnq_CustCode.Text).Length > 0, Trim(txtCollEnq_CustName.Text), "") & " - (" & Trim(txtCollEnq_CustCode.Text) & ")"
            lblCollEnq_SalesrepDtl.Text = IIf(Trim(txtCollEnq_SalesrepCode.Text).Length > 0, Trim(txtCollEnq_SalesrepName.Text), "") & " - (" & Trim(txtCollEnq_SalesrepCode.Text) & ")"
            lblCollEnq_SONo.Text = IIf(Trim(txtCollEnq_SONo.Text).Length > 0, Trim(txtCollEnq_SONo.Text), " - ")
            WucCollEnq_lblDateFrom.Text = WucCollEnq_Calendar.DateStart
            WucCollEnq_lblDateTo.Text = WucCollEnq_Calendar.DateEnd
        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdateCollEnqSearchDtl : " & ex.ToString)
        End Try
    End Sub

    Public Property CollEnq_CustomerCode() As String
        Get
            Return Trim(txtCollEnq_CustCode.Text)
        End Get
        Set(ByVal value As String)
            txtCollEnq_CustCode.Text = value
        End Set
    End Property

    Public Property CollEnq_CustomerName() As String
        Get
            Return Trim(txtCollEnq_CustName.Text)
        End Get
        Set(ByVal value As String)
            txtCollEnq_CustName.Text = value
        End Set
    End Property

    Public Property CollEnq_SalesrepCode() As String
        Get
            Return Trim(txtCollEnq_SalesrepCode.Text)
        End Get
        Set(ByVal value As String)
            txtCollEnq_SalesrepCode.Text = value
        End Set
    End Property

    Public Property CollEnq_SalesrepName() As String
        Get
            Return Trim(txtCollEnq_SalesrepName.Text)
        End Get
        Set(ByVal value As String)
            txtCollEnq_SalesrepName.Text = value
        End Set
    End Property

    Public Property CollEnq_SoNo() As String
        Get
            Return Trim(txtCollEnq_SoNo.Text)
        End Get
        Set(ByVal value As String)
            txtCollEnq_SoNo.Text = value
        End Set
    End Property

    Public Property CollEnq_InvNo() As String
        Get
            Return Trim(txtCollEnq_InvNo.Text)
        End Get
        Set(ByVal value As String)
            txtCollEnq_InvNo.Text = value
        End Set
    End Property
    Public Property CollEnq_TxnDateFrom() As String
        Get
            Return WucCollEnq2_txnDate.DateStart
        End Get
        Set(ByVal value As String)
            WucCollEnq2_txnDate.DateStart = value
        End Set
    End Property

    Public Property CollEnq_TxnDateto() As String
        Get
            Return WucCollEnq2_txnDate.DateEnd
        End Get
        Set(ByVal value As String)
            WucCollEnq2_txnDate.DateEnd = value
        End Set
    End Property
#End Region

#Region "  iFFMR PAF_Enquiry"
    Protected Sub btnPAFEnq_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPAFEnq_Reset.Click
        Try
            txtPAFEnq_CustCode.Text = ""
            txtPAFEnq_CustName.Text = ""
            txtPAFEnq_SalesmanNo.Text = ""
            txtPAFEnq_SalesmanName.Text = ""
            txtPAFEnq_PAFNo.Text = ""
            txtPAFEnq_SVONo.Text = ""
            RaiseEvent EnqResetBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnPAFEnq_Reset_Click : " & ex.ToString)
        End Try
    End Sub
    Public Sub btnPAFEnq_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPAFEnq_Refresh.Click
        Try
            Page.Validate()
            UpdatePAFEnquirySearchDetails()
            RaiseEvent PAFEnquiryBtn_Click(sender, e)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnPAFEnq_Refresh_Click : " & ex.ToString)
        End Try
    End Sub
    Public Sub UpdatePAFEnquirySearchDetails()
        Try
            lblPAFEnq_CustomerDetails.Text = IIf(Trim(txtPAFEnq_CustCode.Text).Length > 0, Trim(txtPAFEnq_CustName.Text), "") & " - (" & Trim(txtPAFEnq_CustCode.Text) & ")"
            lblPAFEnq_SalesmanDetails.Text = IIf(Trim(txtPAFEnq_SalesmanNo.Text).Length > 0, Trim(txtPAFEnq_SalesmanName.Text), "") & " - (" & Trim(txtPAFEnq_SalesmanNo.Text) & ")"
            lblPAFEnq_PAFNoDetails.Text = IIf(Trim(txtPAFEnq_PAFNo.Text).Length > 0, Trim(txtPAFEnq_PAFNo.Text), " - ")
            lblPAFEnq_SVONoDetails.Text = IIf(Trim(txtPAFEnq_SVONo.Text).Length > 0, Trim(txtPAFEnq_SVONo.Text), " - ")
        Catch ex As Exception
            ExceptionMsg(ClassName & ".UpdatePAFEnquirySearchDetails : " & ex.ToString)
        End Try
    End Sub
    Public Property PAFEnq_CustomerCode() As String
        Get
            Return Trim(txtPAFEnq_CustCode.Text)
        End Get
        Set(ByVal value As String)
            txtPAFEnq_CustCode.Text = value
        End Set
    End Property
    Public Property PAFEnq_CustomerName() As String
        Get
            Return Trim(txtPAFEnq_CustName.Text)
        End Get
        Set(ByVal value As String)
            txtPAFEnq_CustName.Text = value
        End Set
    End Property
    Public Property PAFEnq_SalesmanCode() As String
        Get
            Return Trim(txtPAFEnq_SalesmanNo.Text)
        End Get
        Set(ByVal value As String)
            txtPAFEnq_SalesmanNo.Text = value
        End Set
    End Property
    Public Property PAFEnq_SalesmanName() As String
        Get
            Return Trim(txtPAFEnq_SalesmanName.Text)
        End Get
        Set(ByVal value As String)
            txtPAFEnq_SalesmanName.Text = value
        End Set
    End Property
    Public Property PAFEnq_PAFNo() As String
        Get
            Return Trim(txtPAFEnq_PAFNo.Text)
        End Get
        Set(ByVal value As String)
            txtPAFEnq_PAFNo.Text = value
        End Set
    End Property
    Public Property PAFEnq_SVONo() As String
        Get
            Return Trim(txtPAFEnq_SVONo.Text)
        End Get
        Set(ByVal value As String)
            txtPAFEnq_SVONo.Text = value
        End Set
    End Property
#End Region

#Region "pnlDateQuery"
    Private Sub BuildQueryYearMonth()
        Try
            'Build Year
            Dim liLastYear As New ListItem
            liLastYear.Text = Date.Now.AddYears(-1).Year
            liLastYear.Value = Date.Now.AddYears(-1).Year
            ddlYear.Items.Add(liLastYear)

            Dim liThisYear As New ListItem
            liThisYear.Text = Date.Now.Year
            liThisYear.Value = Date.Now.Year
            ddlYear.Items.Add(liThisYear)

            Dim strSessionYear As String = HttpContext.Current.Session("Year")
            If ddlYear.Items.Contains(New ListItem(strSessionYear, strSessionYear)) Then ddlYear.Items.FindByValue(strSessionYear).Selected = True

            'Build Month
            Dim strSessionMonth As String = HttpContext.Current.Session("Month")
            Dim liMonth As ListItem = ddlMonth.Items.FindByValue(strSessionMonth)
            If liMonth IsNot Nothing Then liMonth.Selected = True
        Catch ex As Exception
            ExceptionMsg(ClassName & ".BuildYear : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefreshQueryDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshQueryDate.Click
        Try
            'ScriptManager1.EnablePartialRendering = False

            Session("Year") = Trim(ddlYear.SelectedItem.Value)
            Session("Month") = Trim(ddlMonth.SelectedItem.Value)
            RefreshDetails()

            ''Refresh the dgList
            RaiseEvent RefreshQueryDate_Click(Me, e)


            'ScriptManager1.EnablePartialRendering = True
            ''Refresh the dgList
            ' '' Use JavaScript to trigger the redirect in the other window.
            'Dim strFrameScript As String = "<script language='javascript'>" & _
            '"window.parent.frames(1).location='" & Request.Url.ToString & _
            '  "';</script>" '"window.parent.frames(2).location='" & strNavigateUrl & _
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "FrameScript", strFrameScript)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefreshQueryDate_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub ParseUpdateDate(ByRef btnUpdateDateEvent As Button)
        Try
            btnVirtualUpdateDate = btnUpdateDateEvent

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefreshQueryDate_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Grouping Field"
    Public Sub pnlField_EditPointerText(ByVal strTextToDisplay As String, Optional ByVal strPointerValue As String = "")
        If String.IsNullOrEmpty(strPointerValue) Then strPointerValue = GroupingPointer

        Try
            Dim li As ListItem = lsbGroupField_Hide.Items.FindByValue(strPointerValue)
            If li IsNot Nothing Then
                li.Text = strTextToDisplay
            Else
                li = lsbGroupField_Show.Items.FindByValue(strPointerValue)
                If li IsNot Nothing Then
                    li.Text = strTextToDisplay
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_SwapPointer : " & ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="MyListBoxType">True for show item listbox, False for hide item listbox</param>
    ''' <param name="intInsertionIndex">Index as Position in the listbox</param>
    ''' <param name="strTextToDisplay">Text to display</param>
    ''' <param name="strPointerValue">Value for list item.</param>
    ''' <remarks></remarks>

    Public Sub pnlField_InsertPointer(ByVal MyListBoxType As ListBoxType, ByVal intInsertionIndex As Integer, ByVal strTextToDisplay As String, Optional ByVal strPointerValue As String = "")
        If String.IsNullOrEmpty(strPointerValue) Then strPointerValue = GroupingPointer

        Try
            Dim lsb As ListBox = IIf(ListBoxType.lsbShow, lsbGroupField_Show, lsbGroupField_Hide)
            If lsb IsNot Nothing Then
                If intInsertionIndex > lsb.Items.Count - 1 Then intInsertionIndex = lsb.Items.Count
                pnlField_RemovePointer(strPointerValue)
                pnlField_RemovePointerByName(strTextToDisplay, ListBoxType.lsbAll)
                Dim li As New ListItem(strTextToDisplay, strPointerValue)
                lsb.Items.Insert(intInsertionIndex, li)
                pnlField_MakeDistinct(MyListBoxType)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_InsertPointer : " & ex.ToString)
        End Try
    End Sub

    Public Sub pnlField_MoveGroupingField(ByVal MoveToListBox As ListBoxType, ByVal intInsertionIndex As Integer, ByVal strItemValue As String)
        Try
            Dim lsb As ListBox = IIf(MoveToListBox = ListBoxType.lsbShow, lsbGroupField_Show, lsbGroupField_Hide)
            Dim LI As ListItem = Nothing
            If lsb IsNot Nothing Then
                If intInsertionIndex > lsb.Items.Count - 1 Then intInsertionIndex = lsb.Items.Count
                li = pnlField_RemovePointer(strItemValue)
                If LI IsNot Nothing Then lsb.Items.Insert(intInsertionIndex, LI)
                pnlField_MakeDistinct(MoveToListBox)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_MoveGroupingField : " & ex.ToString)
        End Try
    End Sub


    ''' <summary>
    ''' Remove List Item by Value
    ''' </summary>
    ''' <param name="strPointerValue">List Item Value</param>
    ''' <param name="MyListBoxType">0=both, 1=ListBox_Hide, 2=ListBox_Show</param>
    ''' <remarks></remarks>

    Public Function pnlField_RemovePointer(Optional ByVal strPointerValue As String = "", Optional ByVal MyListBoxType As ListBoxType = ListBoxType.lsbAll) As ListItem
        If String.IsNullOrEmpty(strPointerValue) Then strPointerValue = GroupingPointer
        Dim LI As ListItem = Nothing
        Try

            Dim li_Hide As ListItem = lsbGroupField_Hide.Items.FindByValue(strPointerValue)
            Dim li_Show As ListItem = lsbGroupField_Show.Items.FindByValue(strPointerValue)
            Select Case MyListBoxType
                Case ListBoxType.lsbHide  '1
                    If li_Hide IsNot Nothing Then lsbGroupField_Hide.Items.Remove(li_Hide)
                Case ListBoxType.lsbShow  '2
                    If li_Show IsNot Nothing Then lsbGroupField_Show.Items.Remove(li_Show)
                Case Else
                    If li_Show IsNot Nothing Then lsbGroupField_Show.Items.Remove(li_Show)
                    If li_Hide IsNot Nothing Then lsbGroupField_Hide.Items.Remove(li_Hide)
            End Select
            LI = IIf(li_Show IsNot Nothing, li_Show, IIf(li_Hide IsNot Nothing, li_Hide, Nothing))
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_RemovePointer : " & ex.ToString)
        End Try
        Return LI
    End Function

    ''' <summary>
    ''' Remove Listitem by Text
    ''' </summary>
    ''' <param name="strListItemName">List Item Text</param>
    ''' <param name="MyListBoxType">0=both, 1=ListBox_Hide, 2=ListBox_Show</param>
    ''' <remarks></remarks>
    Public Sub pnlField_RemovePointerByName(ByVal strListItemName As String, Optional ByVal MyListBoxType As ListBoxType = ListBoxType.lsbAll)
        Try
            Dim li_Hide As ListItem = lsbGroupField_Hide.Items.FindByText(strListItemName)
            Dim li_Show As ListItem = lsbGroupField_Show.Items.FindByText(strListItemName)
            Select Case MyListBoxType
                Case ListBoxType.lsbHide  '1
                    If li_Hide IsNot Nothing Then lsbGroupField_Hide.Items.Remove(li_Hide)
                Case ListBoxType.lsbShow  '2
                    If li_Show IsNot Nothing Then lsbGroupField_Show.Items.Remove(li_Show)
                Case Else
                    If li_Show IsNot Nothing Then lsbGroupField_Show.Items.Remove(li_Show)
                    If li_Hide IsNot Nothing Then lsbGroupField_Hide.Items.Remove(li_Hide)
            End Select

        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_RemovePointerByName : " & ex.ToString)
        End Try
    End Sub

    Public Function pnlField_GetListBoxItemCount(ByVal MyListBoxType As ListBoxType) As Integer
        Dim intItemCount As Integer = 0
        Try
            Select Case MyListBoxType
                Case ListBoxType.lsbShow
                    intItemCount = lsbGroupField_Show.Items.Count
                Case ListBoxType.lsbHide
                    intItemCount = lsbGroupField_Hide.Items.Count
                Case Else
                    intItemCount = 0
            End Select
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_GetListBoxItemCount : " & ex.ToString)
        End Try
        Return intItemCount
    End Function


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="MyListBoxType">0 For Show_ListBox, 1 for Hide_ListBox</param>
    ''' <param name="strPointerValue">String To Find, Either Text or Value, determine by blnFindbyText</param>
    ''' <param name="blnFindByText">True=Find By Text, False=Find By Value</param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Function pnlField_IsPointerExist(ByVal MyListBoxType As ListBoxType, Optional ByVal strPointerValue As String = "", Optional ByVal blnFindByText As Boolean = False) As Boolean
        Dim isExist As Boolean = False
        Dim li As ListItem = Nothing
        If String.IsNullOrEmpty(strPointerValue) Then strPointerValue = GroupingPointer
        Try
            If blnFindByText Then 'Find By Text
                Select Case MyListBoxType
                    Case ListBoxType.lsbShow  '0
                        li = lsbGroupField_Show.Items.FindByText(strPointerValue)
                    Case Else
                        li = lsbGroupField_Hide.Items.FindByText(strPointerValue)
                End Select

            Else 'Find By Value
                Select Case MyListBoxType
                    Case ListBoxType.lsbShow  '0
                        li = lsbGroupField_Show.Items.FindByValue(strPointerValue)
                    Case Else
                        li = lsbGroupField_Hide.Items.FindByValue(strPointerValue)
                End Select
            End If

            If li IsNot Nothing Then isExist = True
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_IsPointerExist : " & ex.ToString)
        End Try
        Return isExist
    End Function

    Public ReadOnly Property pnlField_GetGroupingValue() As String
        Get
            Dim sbSelectedItems As New System.Text.StringBuilder

            For Each li As ListItem In lsbGroupField_Show.Items
                sbSelectedItems.Append(IIf(String.IsNullOrEmpty(sbSelectedItems.ToString), li.Value, "," & li.Value))
            Next
            Return sbSelectedItems.ToString
        End Get
    End Property

    Public ReadOnly Property pnlField_GetGroupingTextValue() As String
        Get
            Dim sbSelectedItemsText As New System.Text.StringBuilder
            Dim sbSelectedItemsValue As New System.Text.StringBuilder

            For Each li As ListItem In lsbGroupField_Show.Items
                sbSelectedItemsText.Append(IIf(String.IsNullOrEmpty(sbSelectedItemsText.ToString), li.Text, "," & li.Text))
                sbSelectedItemsValue.Append(IIf(String.IsNullOrEmpty(sbSelectedItemsValue.ToString), li.Value, ";" & li.Value))
            Next
            Dim strCombine As String = sbSelectedItemsText.ToString & "~" & sbSelectedItemsValue.ToString
            Return strCombine 'sbSelectedItemsText.ToString
        End Get
    End Property

    Public Sub pnlField_RestoreGroupingList(ByVal strGroupingTextValue As String)
        Try
            If String.IsNullOrEmpty(strGroupingTextValue) Then Exit Sub
            Dim strAryL1, strAryText, strAryValue As String()
            Dim strText As String = String.Empty
            Dim strValue As String = String.Empty

            strAryL1 = strGroupingTextValue.Split("~")
            If strAryL1 IsNot Nothing AndAlso strAryL1.Length = 2 Then
                strText = strAryL1(0)
                strValue = strAryL1(1)
            End If

            strAryText = strText.Split(",")
            strAryValue = strValue.Split(";")

            If strAryText IsNot Nothing AndAlso strAryValue IsNot Nothing _
            AndAlso strAryText.Length > 0 AndAlso strAryValue.Length > 0 _
            AndAlso strAryText.Length = strAryValue.Length Then
                For index As Integer = 0 To strAryText.Length - 1
                    pnlField_InsertPointer(ListBoxType.lsbShow, index, strAryText(index), strAryValue(index))
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_RestoreGroupingValue : " & ex.ToString)
        End Try
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="MyListBoxType">0 = Filter ListBox_Hide, 1 = Filter ListBox_Show</param>
    ''' <remarks>remove all the duplicae item from iif(intListBoxIndex=0, ListBox_Hide, ListBox_show)</remarks>
    Public Sub pnlField_MakeDistinct(Optional ByVal MyListBoxType As ListBoxType = ListBoxType.lsbHide)
        Dim li As ListItem = Nothing
        'Dim blnReCheck As Boolean = False
        Try
            Dim licCollector As New ListItemCollection
            Dim lsbBox As ListBox = Nothing
            'Do
            Select Case MyListBoxType
                Case ListBoxType.lsbHide  '0 'Filter ListBox_Hide
                    For Each li In lsbGroupField_Hide.Items
                        If pnlField_IsPointerExist(ListBoxType.lsbShow, li.Text, True) OrElse pnlField_IsPointerExist(ListBoxType.lsbShow, li.Value, False) Then
                            'lsbGroupField_Hide.Items.Remove(li)
                            licCollector.Add(li)
                        End If
                    Next
                    lsbBox = CType(lsbGroupField_Hide, ListBox)
                Case Else
                    For Each li In lsbGroupField_Show.Items
                        If pnlField_IsPointerExist(ListBoxType.lsbHide, li.Text, True) Then
                            licCollector.Add(li)
                        End If
                    Next
                    lsbBox = CType(lsbGroupField_Show, ListBox)
            End Select

            For Each li In licCollector
                lsbBox.Items.Remove(li)
            Next

        Catch ex As Exception
            ExceptionMsg(ClassName & ".pnlField_MakeDistinct : " & ex.ToString)
        End Try
    End Sub

    Private Sub GenerateFiledList(ByVal lngSubModuleID As Long, Optional ByVal blnClearBeforeFill As Boolean = False)
        'If Not (lsbGroupField_Hide.Items.Count = 0 AndAlso lsbGroupField_Show.Items.Count = 0) _
        'AndAlso blnClearBeforeFill = False Then Exit Sub
        If (Not String.IsNullOrEmpty(lsbGroupField_Hide.DataTextField)) _
        And blnClearBeforeFill = False Then Exit Sub

        Try
            Dim clsAdmUser As New adm_User.clsUserQuery
            'Session("GroupingValue") = ""
            With lsbGroupField_Hide
                .DataSource = clsAdmUser.GetGroupFieldList(lngSubModuleID).DefaultView
                .DataTextField = "field_desc" '"field_name"
                .DataValueField = "field_name" '"field_id"
                .DataBind()
            End With
            If String.IsNullOrEmpty(strRestoreGroupingTextValue) = False Then pnlField_RestoreGroupingList(strRestoreGroupingTextValue)
            If lsbGroupField_Hide.Items.Count > 0 Then lsbGroupField_Hide.SelectedIndex = 0
            'If blnMakeGroupingFiledDistinct = True Then pnlField_MakeDistinct(intListBoxIndexToFilter)
            'pnlField_MakeDistinct(0)
            pnlField_MakeDistinct(ListBoxType.lsbHide)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".GenerateFiledList : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub AddToGroupFiledShow()
        Try
            ''Multiple Selection
            If lsbGroupField_Hide.GetSelectedIndices().Length > 0 Then

                Dim liToShow As ListItem
                lsbGroupField_Show.SelectedIndex = -1
                'Add Into Show Box
                For Each intIndex As Integer In lsbGroupField_Hide.GetSelectedIndices()
                    liToShow = lsbGroupField_Hide.Items(intIndex)
                    If liToShow IsNot Nothing Then
                        lsbGroupField_Show.Items.Add(liToShow)
                    End If
                Next

                Dim liToHide As ListItem
                'Delete from the hide box
                For Each intindex As Integer In lsbGroupField_Show.GetSelectedIndices
                    liToHide = lsbGroupField_Show.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbGroupField_Hide.Items.Remove(liToHide)
                    End If
                Next
                lsbGroupField_Hide.SelectedIndex = IIf(lsbGroupField_Hide.Items.Count > 0, 0, -1)

            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToShow : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub RemoveFromGroupFieldShow()
        Try
            ''Multiple Selection
            If lsbGroupField_Show.GetSelectedIndices().Length > 0 Then

                Dim liToHide As ListItem
                lsbGroupField_Hide.SelectedIndex = -1
                'Add Into Hide Box
                For Each intIndex As Integer In lsbGroupField_Show.GetSelectedIndices()
                    liToHide = lsbGroupField_Show.Items(intIndex)
                    If liToHide IsNot Nothing Then
                        lsbGroupField_Hide.Items.Add(liToHide)
                    End If
                Next

                Dim liToShow As ListItem
                'Delete from the Show box
                For Each intindex As Integer In lsbGroupField_Hide.GetSelectedIndices
                    liToShow = lsbGroupField_Hide.Items(intindex)
                    If liToShow IsNot Nothing Then
                        lsbGroupField_Show.Items.Remove(liToShow)
                    End If
                Next
                lsbGroupField_Show.SelectedIndex = IIf(lsbGroupField_Show.Items.Count > 0, 0, -1)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".RemoveFromShow : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
    '    Try
    '        AddToGroupFiledShow()
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkAdd_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemove.Click
    '    Try
    '        RemoveFromGroupFieldShow()
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkRemove_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub lnkAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAll.Click
    '    Try
    '        Dim liToShow As ListItem
    '        For Each liToShow In lsbGroupField_Hide.Items
    '            liToShow.Selected = True
    '        Next
    '        AddToGroupFiledShow()
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkAddAll_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub lnkRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAll.Click
    '    Try
    '        Dim liToShow As ListItem
    '        For Each liToShow In lsbGroupField_Show.Items
    '            liToShow.Selected = True
    '        Next
    '        RemoveFromGroupFieldShow()
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkRemoveAll_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Private Sub ItemMoveUp(ByVal liItemToMove As ListItem)
        Try
            liItemToMove = lsbGroupField_Show.Items.FindByText(liItemToMove.Text)
            If liItemToMove IsNot Nothing Then
                Dim intIndex As Integer = lsbGroupField_Show.Items.IndexOf(liItemToMove)
                If intIndex > 0 Then
                    intIndex -= 1
                    If intIndex >= 0 AndAlso Not lsbGroupField_Show.Items(intIndex).Selected Then
                        lsbGroupField_Show.Items.Remove(liItemToMove)
                        lsbGroupField_Show.Items.Insert(intIndex, liItemToMove)
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ItemMoveUp : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ItemMoveDown(ByVal liItemToMove As ListItem)
        Try
            liItemToMove = lsbGroupField_Show.Items.FindByText(liItemToMove.Text)
            If liItemToMove IsNot Nothing Then
                Dim intIndex As Integer = lsbGroupField_Show.Items.IndexOf(liItemToMove)
                If intIndex < lsbGroupField_Show.Items.Count - 1 AndAlso lsbGroupField_Show.Items(intIndex + 1).Selected = False Then
                    intIndex += 1
                    lsbGroupField_Show.Items.Remove(liItemToMove)
                    lsbGroupField_Show.Items.Insert(intIndex, liItemToMove)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ItemMoveDown : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub lnkUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUp.Click
    '    Try

    '        If lsbGroupField_Show.GetSelectedIndices().Length > 0 Then
    '            Dim licItemCollector As New ListItemCollection
    '            Dim intIndex As Integer

    '            For Each intIndex In lsbGroupField_Show.GetSelectedIndices
    '                licItemCollector.Add(lsbGroupField_Show.Items(intIndex))
    '            Next

    '            Dim liItemToMove As ListItem
    '            For Each liItemToMove In licItemCollector
    '                ItemMoveUp(liItemToMove)
    '            Next
    '        End If
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkUp_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub lnkDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDown.Click
    '    Try
    '        If lsbGroupField_Show.GetSelectedIndices().Length > 0 Then
    '            Dim licItemCollector As New ListItemCollection
    '            Dim intIndex As Integer

    '            For Each intIndex In lsbGroupField_Show.GetSelectedIndices
    '                licItemCollector.Add(lsbGroupField_Show.Items(intIndex))
    '            Next

    '            Dim liItemToMove As ListItem
    '            intIndex = licItemCollector.Count - 1
    '            While intIndex >= 0
    '                liItemToMove = licItemCollector.Item(intIndex)
    '                If liItemToMove IsNot Nothing Then ItemMoveDown(liItemToMove)
    '                intIndex -= 1
    '            End While

    '        End If
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".lnkDown_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub btnResetField_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetField.Click
        Try
            Dim li As ListItem = Nothing
            Dim li2 As ListItem = Nothing
            li = lsbGroupField_Show.Items.FindByValue(GroupingPointer)
            lsbGroupField_Show.Items.Clear()
            li2 = lsbGroupField_Hide.Items.FindByValue(GroupingPointer)
            lsbGroupField_Hide.Items.Clear()

            GenerateFiledList(lngSubModuleID, True)
            pnlField_RemovePointer(GroupingPointer, ListBoxType.lsbAll)
            If li IsNot Nothing Then li.Selected = False : lsbGroupField_Show.Items.Add(li)
            If li2 IsNot Nothing Then li2.Selected = False : lsbGroupField_Hide.Items.Add(li2)


            RaiseEvent GroupingFieldChanged(Me, Nothing)
            'lsbGroupField_Hide.Items.Clear()
            'lsbGroupField_Show.Items.Clear()
            'GenerateFiledList(lngSubModuleID)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnResetField_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefresh4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh4.Click
        Try
            'ViewState("GroupingValue") = String.Empty
            'Session("GroupingValue") = pnlField_GetGroupingValue
            'ViewState("GroupingValue") = pnlField_GetGroupingValue
            RebindListItem(hdf_Hide, lsbGroupField_Hide)
            RebindListItem(hdf_Show, lsbGroupField_Show)
            RaiseEvent GroupingFieldChanged(Me, Nothing)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefresh4_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub RebindListItem(ByVal hdf As HiddenField, ByVal lsb As ListBox)
        lsb.Items.Clear()
        If String.IsNullOrEmpty(hdf.Value) = True Then Exit Sub

        If hdf.Value.IndexOf(",") = 0 Then hdf.Value = hdf.Value.Remove(0, 1)
        If hdf.Value.LastIndexOf(",") = hdf.Value.Length - 1 Then hdf.Value = hdf.Value.Remove(hdf.Value.Length - 1, 1)

        Dim strs As String() = hdf.Value.Split(",")
        Dim ItemValue As Values = Nothing
        For Each str As String In strs
            ItemValue = GetValues(str, "|")
            lsb.Items.Add(New ListItem(ItemValue.Name, ItemValue.Value))
        Next
        hdf.Value = ""
    End Sub

    Private Function GetValues(ByVal strValue As String, ByVal strSpliter As String) As Values
        GetValues = New Values
        Dim strs As String() = strValue.Split(strSpliter)
        If strs.Length > 0 Then GetValues.Name = strs(0)
        If strs.Length > 1 Then GetValues.Value = strs(1)
        Return GetValues
    End Function
    Private Class Values
        Public Name, Value As String
    End Class


    'Public ReadOnly Property GroupingItems() As String
    '    Get
    '        Return IIf(_strGoupingItems Is Nothing, "", _strGoupingItems)
    '    End Get
    'End Property
#End Region

#Region "Net Value Control"
    Public Sub NetValue_Refresh()
        Try
            radlNetValue.SelectedValue = Session("NetValue")
        Catch ex As Exception
            ExceptionMsg(ClassName & ".NetValue_Refresh : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnNetValue_refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNetValue_refresh.Click
        Try
            Dim strCurrentValue As String = Session("NetValue")
            If strCurrentValue <> radlNetValue.SelectedValue Then
                Session("NetValue") = radlNetValue.SelectedValue
                'RefreshDetails()
                RaiseEvent NetValue_Changed(Me, e)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnNetValue_refresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Field Config Control"
    Public Sub FieldConfig_Refresh()
        Try
            radlFieldConfig.SelectedValue = Session("FieldConfig")
        Catch ex As Exception
            ExceptionMsg(ClassName & ".FieldConfig_Refresh : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnFieldConfig_refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFieldConfig_refresh.Click
        Try
            Dim strCurrentValue As String = Session("FieldConfig")
            If strCurrentValue <> radlFieldConfig.SelectedValue Then
                Session("FieldConfig") = radlFieldConfig.SelectedValue
                RaiseEvent FieldConfig_Changed(Me, e)
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnFieldConfig_refresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "pnlRemark"
    Private Sub BuildReportDefinition()
        Try
            Dim DT As DataTable = Nothing

            Dim clsRDDB As New adm_Glossary.clsGlossary()
            DT = clsRDDB.GetReportDefinition(lngSubModuleID, Session("PRINCIPAL_ID"))

            dvReportDefinition.DataSource = DT
            dvReportDefinition.DataBind()

            DT.Dispose()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".BuildReportDefinition : " & ex.ToString)
        End Try
    End Sub

    Private Sub BuildRemarkList()
        Dim DT As DataTable = Nothing
        Dim i As Integer
        Try

            Dim clsGlossaryDB As New adm_Glossary.clsGlossary()
            DT = clsGlossaryDB.GetGlossaryList(lngSubModuleID, Session("PRINCIPAL_ID"))

            dgRemarkList.Columns.Clear()
            'dgRemarkList.RowStyle.BackColor = Nothing
            'dgRemarkList.AlternatingRowStyle.BackColor = Nothing

            Dim ColumnName As String = ""

            For i = 0 To DT.Columns.Count - 1
                ColumnName = DT.Columns(i).ColumnName
                Dim dgColumn As New BoundField

                dgColumn.ReadOnly = True
                dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                Dim strFormatString As String = CF_RemarkList.GetOutputFormatString(ColumnName)
                If String.IsNullOrEmpty(strFormatString) = False Then
                    dgColumn.DataFormatString = strFormatString
                End If
                dgColumn.ItemStyle.HorizontalAlign = CF_RemarkList.ColumnStyle(ColumnName).HorizontalAlign

                dgColumn.HtmlEncode = False
                dgColumn.HeaderText = CF_RemarkList.GetDisplayColumnName(ColumnName)
                dgColumn.DataField = ColumnName
                dgRemarkList.Columns.Add(dgColumn)
                dgColumn = Nothing
            Next

            dgRemarkList.DataSource = DT
            dgRemarkList.DataBind()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".BuildRemarkList : " & ex.ToString)
        End Try


    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

  
End Class


Public Class CF_RemarkList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strColumnName As String = ""
        Select Case ColumnName.ToUpper
            Case "GLOSSARY_NAME"
                strColumnName = "Name"
            Case "GLOSSARY_DESC"
                strColumnName = "Description"
            Case Else
                strColumnName = "UNKNOWN"
        End Select

        Return strColumnName
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        Try
            If strNewName Like "*QTY" Then
                strStringFormat = "{0:0}"
            ElseIf strNewName Like "*_AMT" Then
                strStringFormat = "{0:#,0.00}"
            ElseIf strNewName Like "*DATE*" Then
                strStringFormat = "{0:yyyy-MM-dd}"
            Else
                strStringFormat = ""
            End If
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

End Class

'    Private lngSubModuleID As Long
'    Public Event RefreshQueryDate_Click As EventHandler
'    Public Event SearchBtn_Click As EventHandler

'    Public Property SubModuleID() As Long
'        Get
'            Return lngSubModuleID
'        End Get
'        Set(ByVal Value As Long)
'            lngSubModuleID = Value
'        End Set
'    End Property

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        'Put user code to initialize the page here
'        lblTimestamp.Text = Now
'        lblMonthValue.Text = Now.ToString("MMM")
'        lblYearValue.Text = Now.Year
'    End Sub

'    Public ReadOnly Property ClassName() As String
'        Get
'            Return "wuc_ctrlPanel"
'        End Get
'    End Property

'    Public Sub UpdateControlPanel()
'        Try
'            UpdateCtrlPanel.Update()
'        Catch ex As Exception
'            ExceptionMsg(ClassName & ".UpdateCtrlPanel : " & ex.ToString)
'        Finally
'        End Try
'    End Sub

'    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
'        Try
'            RaiseEvent SearchBtn_Click(Me, e)
'        Catch ex As Exception
'            ExceptionMsg(ClassName & ".imgSearch_Click : " & ex.ToString)
'        Finally
'        End Try

'    End Sub

'    Private Sub imgDetails_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDetails.Click
'        If pnlDetails.Visible = True Then
'            pnlDetails.Visible = False
'        Else
'            pnlDetails.Visible = True
'        End If
'    End Sub

'    Private Sub imgUpdate_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUpdate.Click
'        If pnlUpdate.Visible = True Then
'            pnlUpdate.Visible = False
'        Else
'            pnlUpdate.Visible = True
'        End If
'    End Sub

'    Private Sub imgClose1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose1.Click
'        pnlDetails.Visible = False
'    End Sub

'    Private Sub imgClose2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose2.Click
'        pnlUpdate.Visible = False
'    End Sub

'    Private Sub imgDate_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDate.Click
'        If pnlDateQuery.Visible = True Then
'            pnlDateQuery.Visible = False
'        Else
'            pnlDateQuery.Visible = True
'        End If
'    End Sub

'    Private Sub imgClose3_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose3.Click
'        pnlDateQuery.Visible = False
'    End Sub

'    Private Sub imgField_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgField.Click
'        Select Case lngSubModuleID
'            Case 4 'SFMS Extract
'                If pnlSFMSExtractSearch.Visible = True Then
'                    pnlSFMSExtractSearch.Visible = False
'                Else
'                    pnlSFMSExtractSearch.Visible = True
'                End If
'            Case 5
'                If pnlSalesEnquirySearch.Visible = True Then
'                    pnlSalesEnquirySearch.Visible = False
'                Else
'                    pnlSalesEnquirySearch.Visible = True
'                End If
'            Case 6
'                If pnlSFMSActySearch.Visible = True Then
'                    pnlSFMSActySearch.Visible = False
'                Else
'                    pnlSFMSActySearch.Visible = True
'                End If
'            Case Else
'                If pnlField.Visible = True Then
'                    pnlField.Visible = False
'                Else
'                    pnlField.Visible = True
'                End If
'        End Select
'    End Sub

'    Private Sub imgClose4_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose4.Click
'        pnlField.Visible = False
'    End Sub

'    Public Overloads Sub DataBind()
'        Try
'            imgDate.Visible = True
'            imgUpdate.Visible = True
'            imgSearch.Visible = True
'            imgField.Visible = True
'            imgSearchDetails.Visible = False
'            imgInfo.Visible = False
'            lsbShow.Items.Clear()
'            Select Case lngSubModuleID
'                Case 1 'Sales List
'                    'lsbShow.Items.Add(New ListItem("Agency", 1))
'                    lsbShow.Items.Add(New ListItem("Chain", 2))
'                    'lsbShow.Items.Add(New ListItem("Chain Group", 3))
'                    lsbShow.Items.Add(New ListItem("Channel", 4))
'                    lsbShow.Items.Add(New ListItem("Customer", 5))
'                    lsbShow.Items.Add(New ListItem("Customer Group", 6))
'                    'lsbShow.Items.Add(New ListItem("Daily Process Inv", 7))
'                    'lsbShow.Items.Add(New ListItem("Daily Process Order", 8))
'                    'lsbShow.Items.Add(New ListItem("Daily Sales", 9))
'                    lsbShow.Items.Add(New ListItem("Principal", 10))
'                    lsbShow.Items.Add(New ListItem("Product/SKU", 11))
'                    lsbShow.Items.Add(New ListItem("Product By Customer", 12))
'                    lsbShow.Items.Add(New ListItem("Product Group", 13))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 1", 14))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 2", 15))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 3", 16))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 4", 17))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 5", 18))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 6", 19))
'                    lsbShow.Items.Add(New ListItem("Product Hierarchy 7", 20))
'                    'lsbShow.Items.Add(New ListItem("Sales Keeping Unit", 21))
'                    lsbShow.Items.Add(New ListItem("Ship To", 22))
'                    lsbShow.Items.Add(New ListItem("Team", 23))
'                Case 2 'Comm List
'                    imgField.Visible = False
'                Case 3 'DRC List
'                    lsbShow.Items.Add(New ListItem("Customer", 1))
'                    lsbShow.Items.Add(New ListItem("Product", 2))
'                Case 4 'SFMS Extract
'                    imgSearch.Visible = False
'                Case 5 'Sales Enquiry
'                    imgSearch.Visible = False
'                    imgSearchDetails.Visible = True
'                Case 6 'SFMS Acty List
'                    imgSearch.Visible = False
'                    imgSearchDetails.Visible = True
'                Case 7 'Call Analysis By Customer
'                    imgField.Visible = False
'                Case 8 'Call Analysis By Day
'                    imgField.Visible = False
'                    imgInfo.Visible = True
'                Case 9 'Call Analysis By SFMS
'                    imgField.Visible = False
'                Case 10 'Call Analysis By Month
'                    imgField.Visible = False
'                    imgInfo.Visible = True
'                Case 11 'Precall Plan
'                    imgField.Visible = False
'                Case 12 'Call Prod List
'                    imgSearchDetails.Visible = True
'                    lsbShow.Items.Add(New ListItem("Customer", 1))
'                    lsbShow.Items.Add(New ListItem("Customer Group", 2))
'                    lsbShow.Items.Add(New ListItem("SFMS Activity", 3))
'                    lsbShow.Items.Add(New ListItem("SFMS Category", 4))
'                Case 13 'Sales List With Target
'                    lsbShow.Items.Add(New ListItem("Salesman", 13))
'                    lsbShow.Items.Add(New ListItem("Team", 14))
'                Case 14, 15, 16, 17, 18 'Product,Customer,Performance,Transaction,TxnModule
'                    imgDate.Visible = False
'                    imgUpdate.Visible = False
'                    imgSearch.Visible = False
'                    imgField.Visible = False

'            End Select

'        Catch ex As Exception

'        End Try
'    End Sub

'    Private Sub imgSearchDetails_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearchDetails.Click
'        Select Case lngSubModuleID
'            Case 5 'Sales Enquiry
'                If pnlSalesEnquirySearchDetails.Visible = True Then
'                    pnlSalesEnquirySearchDetails.Visible = False
'                Else
'                    pnlSalesEnquirySearchDetails.Visible = True
'                End If
'            Case 6 'SFMS Acty List
'                If pnlSFMSActySearchDetails.Visible = True Then
'                    pnlSFMSActySearchDetails.Visible = False
'                Else
'                    pnlSFMSActySearchDetails.Visible = True
'                End If
'            Case 12 'Call Prod List
'                If pnlCallProdSearchDetails.Visible = True Then
'                    pnlCallProdSearchDetails.Visible = False
'                Else
'                    pnlCallProdSearchDetails.Visible = True
'                End If
'        End Select
'    End Sub

'    Private Sub imgClose6_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose6.Click
'        pnlSFMSExtractSearch.Visible = False
'    End Sub

'    Private Sub imgClose7_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose7.Click
'        pnlSalesEnquirySearch.Visible = False
'    End Sub

'    Private Sub imgClose8_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose8.Click
'        pnlSalesEnquirySearchDetails.Visible = False
'    End Sub

'    Private Sub imgClose10_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose10.Click
'        pnlSFMSActySearch.Visible = False
'    End Sub

'    Private Sub imgPrint_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrint.Click

'    End Sub

'    Private Sub imgClose11_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose11.Click
'        pnlSFMSActySearchDetails.Visible = False
'    End Sub

'    Private Sub imgInfo_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgInfo.Click
'        Select Case lngSubModuleID
'            Case 8 'Call Analysis By Day
'                If pnlCallAnalysisByDayInfo.Visible = True Then
'                    pnlCallAnalysisByDayInfo.Visible = False
'                Else
'                    pnlCallAnalysisByDayInfo.Visible = True
'                End If
'            Case 10 'Call Analysis By Month
'                If pnlCallAnalysisByMonthInfo.Visible = True Then
'                    pnlCallAnalysisByMonthInfo.Visible = False
'                Else
'                    pnlCallAnalysisByMonthInfo.Visible = True
'                End If
'        End Select
'    End Sub

'    Private Sub imgClose12_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose12.Click
'        pnlCallAnalysisByDayInfo.Visible = False
'    End Sub

'    Private Sub imgClose13_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose13.Click
'        pnlCallAnalysisByMonthInfo.Visible = False
'    End Sub

'    Private Sub imgClose14_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose14.Click
'        pnlCallProdSearchDetails.Visible = False
'    End Sub

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	iFFMR Sales_Enquiry (Region)
'    ' Purpose	        :	Gather all the function that used in Sales Enquiry Search and List
'    '----------------------------------------------------------------------
'#Region "  iFFMR Sales_Enquiry"

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	Sub btnReset_Click
'    ' Purpose	        :	Reset all the text box in Sales Enquiry Search Panel
'    '                       (Does not reset the SalesEnquirySearchDetails panel)
'    '----------------------------------------------------------------------
'    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
'        txtProductNo.Text = ""
'        txtProductName.Text = ""
'        txtCustCode.Text = ""
'        txtCustName.Text = ""
'        txtSalesmanNo.Text = ""
'        txtSalesmanName.Text = ""
'        txtInvNo.Text = ""
'        txtStartDate.Text = ""
'        txtEndDate.Text = ""
'    End Sub

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	Sub Parse_btnRefresh5
'    ' Purpose	        :	Parse the refresh button control(btnRefresh5) in SalesEnquirySearchPanel to another page.
'    ' Page              :	[from] : This class
'    '   		            [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public Sub Parse_btnRefresh5(ByRef btnRefresh5Ref As Button)
'        btnRefresh5Ref = btnRefresh5
'    End Sub

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	Sub UpdateSalesEnquirySearchDetails()
'    ' Purpose	        :	Display the current search criteria in SalesEnquirySearchDetailsPanel
'    '----------------------------------------------------------------------
'    Public Sub UpdateSalesEnquirySearchDetails()
'        lblCustomerDetails.Text = IIf(Trim(txtCustCode.Text).Length > 0, Trim(txtCustName.Text), "") & " - (" & Trim(txtCustCode.Text) & ")"
'        lblProductDetails.Text = IIf(Trim(txtProductNo.Text).Length > 0, Trim(txtProductName.Text), "") & " - (" & Trim(txtProductNo.Text) & ")"
'        lblSalesmanDetails.Text = IIf(Trim(txtSalesmanNo.Text).Length > 0, Trim(txtSalesmanName.Text), "") & " - (" & Trim(txtSalesmanNo.Text) & ")"
'        lblInvoiceDetails.Text = IIf(Trim(txtInvNo.Text).Length > 0, Trim(txtInvNo.Text), " - ")
'        lblDateFromDetails.Text = IIf(Trim(txtStartDate.Text).Length > 0, Trim(txtStartDate.Text), " - ")
'        lblDateToDetails.Text = IIf(Trim(txtEndDate.Text).Length > 0, Trim(txtEndDate.Text), " - ")
'    End Sub

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property isShiptoCodeExist()
'    ' Purpose	        :	Check whether shiptocode is 1 of the search criteria
'    '                       If yes then show product info, if no the show customer info.
'    ' Page              :	SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property isShiptoCodeExist() As Boolean
'        Get
'            Return IIf(txtCustCode.Text.Length > 5, True, False)
'        End Get
'    End Property

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property ProductCode()
'    ' Purpose	        :	Return the product code in SalesEnquirySearchPanel
'    ' Page              :   [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property ProductCode() As String
'        Get
'            Return Trim(txtProductNo.Text)
'        End Get
'    End Property

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property ShipToCode()
'    ' Purpose	        :	Return the shipto code in SalesEnquirySearchPanel
'    ' Page              :   [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property ShipToCode() As String
'        Get
'            Return Trim(txtCustCode.Text)
'        End Get
'    End Property

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property Salesmancode()
'    ' Purpose	        :	Return the salesman code in SalesEnquirySearchPanel
'    ' Page              :   [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property SalesmanCode() As String
'        Get
'            Return Trim(txtSalesmanNo.Text)
'        End Get
'    End Property

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property InvoiceNumber()
'    ' Purpose	        :	Return the invoice number in SalesEnquirySearchPanel
'    ' Page              :   [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property InvoiceNumber() As String
'        Get
'            Return Trim(txtInvNo.Text)
'        End Get
'    End Property

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property Datefrom()
'    ' Purpose	        :	Return the datefrom string in SalesEnquirySearchPanel
'    ' Page              :   [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property DateFrom() As String
'        Get
'            Return Trim(txtStartDate.Text)
'        End Get
'    End Property

'    '---------------------------------------------------------------------
'    ' Procedure 	    : 	ReadOnly Property Dateto()
'    ' Purpose	        :	Return the dateto string in SalesEnquirySearchPanel
'    ' Page              :   [to]   : SalesEnquiryList.aspx
'    '----------------------------------------------------------------------
'    Public ReadOnly Property Dateto() As String
'        Get
'            Return Trim(txtEndDate.Text)
'        End Get
'    End Property
'#End Region

'    Private Sub ExceptionMsg(ByVal strMsg As String)
'        Try
'            'lblErr.Text = ""
'            'lblErr.Text = strMsg

'            'Call error log class
'            Dim objLog As cor_Log.clsLog
'            objLog = New cor_Log.clsLog
'            With objLog
'                .clsProperties.LogTypeID = 1
'                .clsProperties.DateLogIn = Now
'                .clsProperties.DateLogOut = Now
'                .clsProperties.SeverityID = 4
'                .clsProperties.LogMsg = strMsg
'                .Log()
'            End With
'            objLog = Nothing

'        Catch ex As Exception

'        End Try
'    End Sub
'    Public Sub RefreshDetails()
'        'Try
'        '    lblUserName.Text = HttpContext.Current.Session("UserName").ToString
'        '    lblCountryName.Text = HttpContext.Current.session("COUNTRY_NAME").ToString
'        '    lblPrincipalName.Text = HttpContext.Current.session("PRINCIPAL_NAME").ToString
'        '    lblTeamName.Text = HttpContext.Current.session("TEAM_NAME").ToString
'        '    lblRegionName.Text = HttpContext.Current.session("REGION_NAME").ToString
'        '    lblAreaName.Text = HttpContext.Current.Session("currentAreaName").ToString
'        '    lblSalesmanName.Text = HttpContext.Current.session("SALESREP_NAME").ToString
'        '    lblYearValue.Text = HttpContext.Current.Session("Year").ToString
'        '    lblMonthValue.Text = HttpContext.Current.Session("Month").ToString
'        '    lblSelectedDate.Text = HttpContext.Current.Session("SelectedDate").ToString
'        '    lblTimestamp.Text = Now
'        '    lblModuleName.Text = HttpContext.Current.Session("ModuleName").ToString

'        'Catch ex As Exception
'        '    ExceptionMsg(ClassName & ".UpdateDetails : " & ex.ToString)
'        'Finally
'        'End Try
'    End Sub
