'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	09/04/2007
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustContInfo
    'Inherits clsDB
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCustContInfo"
        End Get
    End Property

    Public Function GetCustContInfoDT(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_INFO"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipalID, 2)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CLASS", strClass, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustContInfoDT :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function
    Public Function GetCustContCallInfoDT(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_CALL_INFO"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipalID, 2)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CLASS", strClass, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustContCallInfoDT :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try

    End Function
    Public Function GetCustContCoverageInfoDT(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_COVERAGE_INFO"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipalID, 2)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CLASS", strClass, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustContCoverageInfoDT :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function
    Public Function GetCustContNonCoverageInfoDT(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_NONCOVERAGE_INFO"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipalID, 2)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CLASS", strClass, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustContCoverageInfoDT :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function
    Public Function GetCustContEffetiveCallInfoDT(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_EFFECTIVE_INFO"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipalID, 2)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CLASS", strClass, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ". GetCustContEffetiveCallInfoDT :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function
    Public Function GetCustContNonEffetiveCallInfoDT(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepCode As String, ByVal strClass As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CUST_CONT_NONEFFECTIVE_INFO"
                .addItem("USER_ID", strUserID, 2)
                .addItem("PRINCIPAL_ID", strPrincipalID, 2)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 2)
                .addItem("YEAR", strYear, 2)
                .addItem("MONTH", strMonth, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CLASS", strClass, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCustContNonEffetiveCallInfoDT :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
