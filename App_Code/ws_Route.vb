﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports cor_DB
Imports System.Web.Script.Services
Imports System.Collections.Generic



' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class ws_Route
    Inherits System.Web.Services.WebService
 
    Private strConn As String = Session("ffms_conn")


    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class

#Region "Class Entity"

    Public Class RouteCoordinate
        Inherits BaseEntity
#Region "LocalVariable"
        Private _CustCode As String = String.Empty
        Private _CustName As String = String.Empty
        Private _Latitude As String = String.Empty
        Private _Longitude As String = String.Empty

#End Region
#Region "Public Porperty"
        Public Property CUST_CODE() As String
            Get
                Return _CustCode
            End Get
            Set(ByVal value As String)
                _CustCode = value
            End Set
        End Property
        Public Property CUST_NAME() As String
            Get
                Return _CustName
            End Get
            Set(ByVal value As String)
                _CustName = value
            End Set
        End Property

        Public Property LATITUDE() As String
            Get
                Return _Latitude
            End Get
            Set(ByVal value As String)
                _Latitude = value
            End Set
        End Property
        Public Property LONGITUDE() As String
            Get
                Return _Longitude
            End Get
            Set(ByVal value As String)
                _Longitude = value
            End Set
        End Property

#End Region
    End Class

    Public Class SRRoute
        Inherits BaseEntity
#Region "LocalVariable"
        Private _CALL_DATE As String = String.Empty
        Private _SALES_AREA_CODE As String = String.Empty
        Private _SALESREP_CODE As String = String.Empty
        Private _SALESREP_NAME As String = String.Empty
        Private _VISIT_ID As String = String.Empty
        Private _CUST_CODE As String = String.Empty
        Private _CUST_NAME As String = String.Empty
        Private _CONT_CODE As String = String.Empty
        Private _CONT_NAME As String = String.Empty
        Private _CUST_CLASS As String = String.Empty
        Private _TIME_IN As String = String.Empty
        Private _TIME_OUT As String = String.Empty
        Private _TIME_SPEND As String = String.Empty
        Private _IN_LONGITUDE As String = String.Empty
        Private _IN_LATITUDE As String = String.Empty
        Private _ORI_GPS_IND As String = String.Empty
        Private _VISIT_IND As String = String.Empty
        Private _IMG_LOC As String = String.Empty
#End Region
#Region "Public Porperty"
        Public Property CALL_DATE() As String
            Get
                Return _CALL_DATE
            End Get
            Set(ByVal value As String)
                _CALL_DATE = value
            End Set
        End Property
        Public Property SALES_AREA_CODE() As String
            Get
                Return _SALES_AREA_CODE
            End Get
            Set(ByVal value As String)
                _SALES_AREA_CODE = value
            End Set
        End Property
        Public Property SALESREP_CODE() As String
            Get
                Return _SALESREP_CODE
            End Get
            Set(ByVal value As String)
                _SALESREP_CODE = value
            End Set
        End Property
        Public Property SALESREP_NAME() As String
            Get
                Return _SALESREP_NAME
            End Get
            Set(ByVal value As String)
                _SALESREP_NAME = value
            End Set
        End Property
        Public Property VISIT_ID() As String
            Get
                Return _VISIT_ID
            End Get
            Set(ByVal value As String)
                _VISIT_ID = value
            End Set
        End Property
        Public Property CUST_CODE() As String
            Get
                Return _CUST_CODE
            End Get
            Set(ByVal value As String)
                _CUST_CODE = value
            End Set
        End Property
        Public Property CUST_NAME() As String
            Get
                Return _CUST_NAME
            End Get
            Set(ByVal value As String)
                _CUST_NAME = value
            End Set
        End Property
        Public Property CONT_CODE() As String
            Get
                Return _CONT_CODE
            End Get
            Set(ByVal value As String)
                _CONT_CODE = value
            End Set
        End Property
        Public Property CONT_NAME() As String
            Get
                Return _CONT_NAME
            End Get
            Set(ByVal value As String)
                _CONT_NAME = value
            End Set
        End Property
        Public Property CUST_CLASS() As String
            Get
                Return _CUST_CLASS
            End Get
            Set(ByVal value As String)
                _CUST_CLASS = value
            End Set
        End Property
        Public Property TIME_IN() As String
            Get
                Return _TIME_IN
            End Get
            Set(ByVal value As String)
                _TIME_IN = value
            End Set
        End Property
        Public Property TIME_OUT() As String
            Get
                Return _TIME_OUT
            End Get
            Set(ByVal value As String)
                _TIME_OUT = value
            End Set
        End Property
        Public Property TIME_SPEND() As String
            Get
                Return _TIME_SPEND
            End Get
            Set(ByVal value As String)
                _TIME_SPEND = value
            End Set
        End Property
        Public Property IN_LONGITUDE() As String
            Get
                Return _IN_LONGITUDE
            End Get
            Set(ByVal value As String)
                _IN_LONGITUDE = value
            End Set
        End Property
        Public Property IN_LATITUDE() As String
            Get
                Return _IN_LATITUDE
            End Get
            Set(ByVal value As String)
                _IN_LATITUDE = value
            End Set
        End Property
        Public Property VISIT_IND() As String
            Get
                Return _VISIT_IND
            End Get
            Set(ByVal value As String)
                _VISIT_IND = value
            End Set
        End Property
        Public Property ORI_GPS_IND() As String
            Get
                Return _ORI_GPS_IND
            End Get
            Set(ByVal value As String)
                _ORI_GPS_IND = value
            End Set
        End Property
        Public Property IMG_LOC() As String
            Get
                Return _IMG_LOC
            End Get
            Set(ByVal value As String)
                _IMG_LOC = value
            End Set
        End Property

#End Region
    End Class
#End Region

    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class

    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffmr_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class

    Class Person
        Public name As String
        Public age As Integer
    End Class

    <WebMethod()> _
    Public Function ReturnRoute(ByVal strSalesrep As String, ByVal strRoute As String) As PagedResult(Of RouteCoordinate)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of RouteCoordinate)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(strConn)
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_MST_CUST_GPS_LIST"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrep, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "ROUTE_CODE", strRoute, clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_Coordinate(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            'obj = New clsDB
            'With obj
            '    .ConnectionString = strConn
            '    .CmdText = "SPP_MST_CUST_GPS_LIST"
            '    .addItem("SALESREP_CODE", strSalesrep, clsDB.DataType.DBString, True)
            '    .addItem("ROUTE_CODE", strRoute, clsDB.DataType.DBString)
            '    DR = .spRetrieveDR()
            'End With

            'While DR.Read
            '    rptRows.Add(Build_ATChartRPT(DR))
            'End While

            rptResult = New PagedResult(Of RouteCoordinate)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_RoutePlan.ReturnRoute :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    Public Shared Function Build_Coordinate(ByRef reader As IDataReader) As RouteCoordinate
        Dim rptRow As New RouteCoordinate
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i)
                    Case "CUST_CODE"
                        SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
                    Case "CUST_NAME"
                        SetValue(Of String)(.CUST_NAME, reader("CUST_NAME"), String.Empty)
                    Case "LATITUDE"
                        SetValue(Of String)(.LATITUDE, reader("LATITUDE"), 0)
                    Case "LONGITUDE"
                        SetValue(Of String)(.LONGITUDE, reader("LONGITUDE"), 0)
                End Select
            Next
        End With
        Return rptRow
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function ReturnSRRoute(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strDay As String, ByVal strSalesrepCode As String) As PagedResult(Of SRRoute)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of SRRoute)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_TXN_MAP_VISITED_ROUTE"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", strUserID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", strPrincipalID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRINCIPAL_CODE", strPrincipalCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "YEAR", strYear, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MONTH", strMonth, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DAY", strDay, clsCONN.dtType.dbString)

                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_SRRoute(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            'obj = New clsDB
            'With obj
            '    .ConnectionString = strConn
            '    .CmdText = "SPP_MST_CUST_GPS_LIST"
            '    .addItem("SALESREP_CODE", strSalesrep, clsDB.DataType.DBString, True)
            '    .addItem("ROUTE_CODE", strRoute, clsDB.DataType.DBString)
            '    DR = .spRetrieveDR()
            'End With

            'While DR.Read
            '    rptRows.Add(Build_ATChartRPT(DR))
            'End While

            rptResult = New PagedResult(Of SRRoute)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_RoutePlan.ReturnSRRoute :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    Public Shared Function Build_SRRoute(ByRef reader As IDataReader) As SRRoute
        Dim rptRow As New SRRoute
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i).ToUpper
                    Case "CALL_DATE"
                        SetValue(Of String)(.CALL_DATE, reader("CALL_DATE"), String.Empty)
                    Case "SALES_AREA_CODE"
                        SetValue(Of String)(.SALES_AREA_CODE, reader("SALES_AREA_CODE"), String.Empty)
                    Case "SALESREP_CODE"
                        SetValue(Of String)(.SALESREP_CODE, reader("SALESREP_CODE"), 0)
                    Case "SALESREP_NAME"
                        SetValue(Of String)(.SALESREP_NAME, reader("SALESREP_NAME"), 0)
                    Case "VISIT_ID"
                        SetValue(Of String)(.VISIT_ID, reader("VISIT_ID"), String.Empty)
                    Case "CUST_CODE"
                        SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
                    Case "CUST_NAME"
                        SetValue(Of String)(.CUST_NAME, reader("CUST_NAME"), 0)
                    Case "CONT_CODE"
                        SetValue(Of String)(.CONT_CODE, reader("CONT_CODE"), String.Empty)
                    Case "CONT_NAME"
                        SetValue(Of String)(.CONT_NAME, reader("CONT_NAME"), 0)
                    Case "CLASS"
                        SetValue(Of String)(.CUST_CLASS, reader("CLASS"), 0)
                    Case "TIME_IN"
                        SetValue(Of String)(.TIME_IN, reader("TIME_IN"), 0)
                    Case "TIME_OUT"
                        SetValue(Of String)(.TIME_OUT, reader("TIME_OUT"), String.Empty)
                    Case "TIME_SPEND"
                        SetValue(Of String)(.TIME_SPEND, reader("TIME_SPEND"), String.Empty)
                    Case "IN_LONGITUDE"
                        SetValue(Of String)(.IN_LONGITUDE, reader("IN_LONGITUDE"), 0)
                    Case "IN_LATITUDE"
                        SetValue(Of String)(.IN_LATITUDE, reader("IN_LATITUDE"), 0)
                    Case "VISIT_IND"
                        SetValue(Of String)(.VISIT_IND, reader("VISIT_IND"), 0)
                    Case "ORI_GPS_IND"
                        SetValue(Of String)(.ORI_GPS_IND, reader("ORI_GPS_IND"), 0)
                    Case "IMG_LOC"
                        SetValue(Of String)(.IMG_LOC, reader("IMG_LOC"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class


