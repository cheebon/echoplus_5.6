﻿
Partial Class include_wuc_txtIntFieldRange
    Inherits System.Web.UI.UserControl

    'RequiredFieldValidator
    Private _strValidationGroup As String
    Private _blnRequiredValidation As Boolean = True
    Private _strValidationErrorMessage As String

    Private ReadOnly Property ControlName() As String
        Get
            Return "wuc_txtIntFieldRange"
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ScriptManager.RegisterExpandoAttribute(rfvValidateInputStart, rfvValidateInputStart.ClientID, "StartControlID", txtStart.ClientID, False)
        'ScriptManager.RegisterExpandoAttribute(rfvValidateInputStart, rfvValidateInputStart.ClientID, "EndControlID", txtEnd.ClientID, False)


        If Not IsPostBack Then

            'Set ValidationGoupFiled is exist
            If Not String.IsNullOrEmpty(_strValidationGroup) Then
                'rfvValidateInputStart.ValidationGroup = _strValidationGroup
                rfvInputStartDataType.ValidationGroup = _strValidationGroup
                rfvInputEndDataType.ValidationGroup = _strValidationGroup
                'rfvValidateInputEnd.ValidationGroup = _strValidationGroup
            End If

            'Set Validation Visibility is Exist
            'rfvValidateInputStart.Enabled = _blnRequiredValidation
            rfvInputStartDataType.Enabled = _blnRequiredValidation
            rfvInputEndDataType.Enabled = _blnRequiredValidation
            'rfvValidateInputEnd.Enabled = _blnRequiredValidation
        End If
    End Sub

#Region "Validation properties"
    Public Property RequiredValidation() As Boolean
        Get
            Return _blnRequiredValidation
        End Get
        Set(ByVal blnVisible As Boolean)
            _blnRequiredValidation = blnVisible
            'If Not rfvValidateInputStart Is Nothing Then rfvValidateInputStart.Enabled = _blnRequiredValidation
            If Not rfvInputStartDataType Is Nothing Then rfvInputStartDataType.Enabled = _blnRequiredValidation
            If Not rfvInputEndDataType Is Nothing Then rfvInputEndDataType.Enabled = _blnRequiredValidation
            ' If Not rfvValidateInputEnd Is Nothing Then rfvValidateInputEnd.Enabled = _blnRequiredValidation
            If Not CV_RangeCompare Is Nothing Then CV_RangeCompare.Enabled = _blnRequiredValidation
        End Set
    End Property

    Public Property RequiredValidationGroup() As String
        Get
            Return _strValidationGroup
        End Get
        Set(ByVal value As String)
            _strValidationGroup = value
            'If Not rfvValidateInputStart Is Nothing Then rfvValidateInputStart.ValidationGroup = _strValidationGroup
            If Not rfvInputStartDataType Is Nothing Then rfvInputStartDataType.ValidationGroup = _strValidationGroup
            If Not rfvInputEndDataType Is Nothing Then rfvInputEndDataType.ValidationGroup = _strValidationGroup
            'If Not rfvValidateInputEnd Is Nothing Then rfvValidateInputEnd.ValidationGroup = _strValidationGroup
            If Not CV_RangeCompare Is Nothing Then CV_RangeCompare.ValidationGroup = _strValidationGroup
        End Set
    End Property
#End Region

#Region "Properties"

    Public Property ValueStart() As String
        Get
            If txtStart.Text.Trim = "" Then
                txtStart.Text = txtEnd.Text.Trim
            End If
            Return txtStart.Text.Trim
        End Get
        Set(ByVal value As String)
            txtStart.Text = value
        End Set
    End Property

    Public Property ValueEnd() As String
        Get
            If txtEnd.Text.Trim = "" Then
                txtEnd.Text = txtStart.Text
            End If
            Return txtEnd.Text.Trim
        End Get
        Set(ByVal value As String)
            txtEnd.Text = value
        End Set
    End Property

#End Region

    'Protected Sub rfvValidateInputStart_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles rfvValidateInputStart.PreRender
    'Link client's validation to the tobe validated controls here.
    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Linker", String.Format("ValidatorHookupControlID('{0}', document.getElementById('{1}'));", txtStart.ClientID, rfvValidateInputStart.ClientID), True)
    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Linker2", String.Format("ValidatorHookupControlID('{0}', document.getElementById('{1}'));", txtEnd.ClientID, rfvValidateInputStart.ClientID), True)
    'End Sub
End Class
