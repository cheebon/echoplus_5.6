Imports System.Data
Partial Class SalesrepPrdFreqGrid
    Inherits System.Web.UI.Page
#Region "Local Variable"

    Private intPageSize As Integer
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollector2 As ListItemCollection

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_SalesrepPrdFreq"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "SalesrepPrdFreq.aspx"
        End Get
    End Property

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDatabinding()
            LoadHeadlabel()
        End If

        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.FIELDFORCEPRDFREQ)
            .DataBind()
            .Visible = True
        End With

        'Call Panel
        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.FIELDFORCEPRDFREQ
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            Dim gotpreviouspage As String
            gotpreviouspage = Request.QueryString("PAGE_INDICATOR")
            If Not String.IsNullOrEmpty(gotpreviouspage) Then
                TimerControl1.Enabled = True
            Else
                CriteriaCollector = Nothing ' New clsSharedValue    
                TimerControl1.Enabled = True
            End If
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

        End If
    End Sub

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtCurrenttable Is Nothing Then
                dtCurrenttable = GetRecList()

                ViewState("strSortExpression") = Nothing
                ViewState("dtCurrentView") = dtCurrenttable
                dgList.PageIndex = 0

            End If
            PreRenderMode(dtCurrenttable)

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.AllowSorting = True
            dgList.DataBind()
            dgList.ShowFooter = True



        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then dgList_Init(DT)
            Cal_ItemFigureCollector(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SFPRDFREQ.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_SFPRDFREQ.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_SFPRDFREQ.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFPRDFREQ.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFPRDFREQ.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName


                        Dim strUrlFields(3) As String
                        strUrlFields(0) = "ACTUAL_CALL"
                        strUrlFields(1) = "SALESREP_CODE"
                        strUrlFields(2) = "CUST_CODE"
                        strUrlFields(3) = "CONT_CODE"

                        Dim gotpreviouspage As String
                        gotpreviouspage = Request.QueryString("PAGE_INDICATOR")

                        Dim strsalesrepcode As String, strstartyear As String, strstartmonth As String, strendyear As String, strendmonth As String, _
                        strptlCode As String, strclass As String

                        strsalesrepcode = Trim(Request.QueryString("SALESREP_CODE"))
                        strstartyear = Trim(Request.QueryString("START_YEAR"))
                        strstartmonth = Trim(Request.QueryString("START_MONTH"))
                        strendyear = Trim(Request.QueryString("END_YEAR"))
                        strendmonth = Trim(Request.QueryString("END_MONTH"))
                        strptlCode = Trim(Request.QueryString("PTL_CODE"))
                        strclass = Trim(Request.QueryString("CLASS"))
                        Dim strUrlFormatString As String

                        If Not String.IsNullOrEmpty(gotpreviouspage) Then
                            strUrlFormatString = (" parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepPrdFreq/SalesrepPrdFreqDtl.aspx?" & _
                                                                            "STARTYEAR=" & CriteriaCollector.Year & "&STARTMONTH=" & CriteriaCollector.Month & "&ENDYEAR=" & CriteriaCollector.Year2 & "&ENDMONTH=" & CriteriaCollector.Month2 & "&" & _
                                                                            "SALESREP_CODE={1}&CUST_CODE={2}&CONT_CODE={3}&CAT_CODE=" & CriteriaCollector.PTLCode & "&PAGE_INDICATOR=FIELDFORCEPRDFREQ '")
                        Else
                            strUrlFormatString = (" parent.document.getElementById('DetailBarIframe').src='../../iFFMR/Customize/SalesrepPrdFreq/SalesrepPrdFreqDtl.aspx?" & _
                                                "STARTYEAR=" & strstartyear & "&STARTMONTH=" & strstartmonth & "&ENDYEAR=" & strendyear & "&ENDMONTH=" & strendmonth & "&" & _
                                                "SALESREP_CODE={1}&CUST_CODE={2}&CONT_CODE={3}&CAT_CODE=" & strptlCode & "&PAGE_INDICATOR=FIELDFORCEPRDFREQ '")
                        End If


                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SFPRDFREQ.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFPRDFREQ.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFPRDFREQ.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    For Each TC As TableCell In e.Row.Cells
                        If TC.Controls.Count > 0 Then
                            Dim HYL As HyperLink = CType(TC.Controls(0), HyperLink)
                            If HYL IsNot Nothing Then
                                Dim strlink As String = HYL.NavigateUrl
                                HYL.NavigateUrl = "#"
                                HYL.Target = String.Empty
                                HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();")
                            End If
                        End If
                    Next
                Case DataControlRowType.Footer

                    Dim iIndex As Integer
                    For Each li As ListItem In licItemFigureCollector2
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_SFPRDFREQ.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
            End Select

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try

    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        licItemFigureCollector2 = New ListItemCollection

        Dim strColumnName As String
        Dim liColumnField As ListItem

        If DT.Columns.Contains("ACHIEVED_IND") Then
            licItemFigureCollector.Add(New ListItem("TOTAL", 0))
            licItemFigureCollector.Add(New ListItem("OVER", 0))
            licItemFigureCollector.Add(New ListItem("NP", 0))
            licItemFigureCollector.Add(New ListItem("ROWCOUNT", 0))

            For Each DR As DataRow In DT.Rows
                Dim strValue As String = CStr(DR("ACHIEVED_IND"))
                Select Case strValue.ToUpper
                    Case "Y"
                        licItemFigureCollector.FindByText("TOTAL").Value = licItemFigureCollector.FindByText("TOTAL").Value + 1
                    Case "Y-OVER"
                        licItemFigureCollector.FindByText("TOTAL").Value = licItemFigureCollector.FindByText("TOTAL").Value + 1
                        licItemFigureCollector.FindByText("OVER").Value = licItemFigureCollector.FindByText("OVER").Value + 1
                    Case "NP"
                        licItemFigureCollector.FindByText("NP").Value = licItemFigureCollector.FindByText("NP").Value + 1
                End Select

                Dim strValue2 As String = CStr(DR("PLANNED_CALL"))
                If strValue2 > 0 Then
                    licItemFigureCollector.FindByText("ROWCOUNT").Value = licItemFigureCollector.FindByText("ROWCOUNT").Value + 1
                End If

            Next


            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If (strColumnName = "PLANNED_CALL" OrElse strColumnName = "ACTUAL_CALL") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector2.Add(liColumnField)
                End If
            Next

            licItemFigureCollector2.Add(New ListItem("ACHIEVED_IND", 0))

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector2
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next

            licItemFigureCollector2.FindByText("ACHIEVED_IND").Value = Format(ConvertToDouble(DIVISION(licItemFigureCollector.FindByText("TOTAL"), licItemFigureCollector.FindByText("ROWCOUNT")) * 100), "0.00")
        End If

    End Sub

    Private Sub LoadHeadlabel()

        lbltotal.Text = Format(ConvertToDouble(DIVISION(licItemFigureCollector.FindByText("TOTAL"), licItemFigureCollector.FindByText("ROWCOUNT")) * 100), "0.00")
        lblover.Text = Format(ConvertToDouble(DIVISION(licItemFigureCollector.FindByText("OVER"), licItemFigureCollector.FindByText("ROWCOUNT")) * 100), "0.00")
        lblnotplanned.Text = ConvertToDouble(licItemFigureCollector.FindByText("NP").Value)

    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strsalesrepcode As String, strstartyear As String, strstartmonth As String, strendyear As String, strendmonth As String, _
            strptlCode As String, strclass As String

            strsalesrepcode = Request.QueryString("SALESREP_CODE")
            strstartyear = Request.QueryString("START_YEAR")
            strstartmonth = Request.QueryString("START_MONTH")
            strendyear = Request.QueryString("END_YEAR")
            strendmonth = Request.QueryString("END_MONTH")
            strptlCode = Request.QueryString("PTL_CODE")
            strclass = Request.QueryString("CLASS")


            Dim gotpreviouspage As String
            gotpreviouspage = Request.QueryString("PAGE_INDICATOR")
            If String.IsNullOrEmpty(gotpreviouspage) Then
                'Stored Criteria into Static Value Collector
                With CriteriaCollector
                    .Year = Session("YEAR")
                    .PrincipalID = Session("PRINCIPAL_ID")
                    .PrincipalCode = Session("PRINCIPAL_CODE")
                    If Page.IsPostBack Then
                        .SalesrepCode = strsalesrepcode
                        .Year = strstartyear
                        .Month = strstartmonth
                        .Year2 = strendyear
                        .Month2 = strendmonth
                        .PTLCode = strptlCode
                        .Classification = strclass
                    End If
                End With
            End If



            'CriteriaCollector = CriteriaCollector
            Dim clsSFPrdFreq As New rpt_Customize.clsSFPrdFreq
            With clsSFPrdFreq.properties
                .UserID = Session.Item("UserID")
                .PrinID = Session("PRINCIPAL_ID")
                .PrinCode = Session("PRINCIPAL_CODE")
                .SalesrepCode = CriteriaCollector.SalesrepCode
                .StartYear = CriteriaCollector.Year
                .StartMonth = CriteriaCollector.Month
                .EndYear = CriteriaCollector.Year2
                .EndMonth = CriteriaCollector.Month2
                .PTLCode = CriteriaCollector.PTLCode
                .strClass = CriteriaCollector.Classification
            End With

            DT = clsSFPrdFreq.GetSFPrdFreqList

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

#Region "EVENT HANDLER"
 

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()
            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    'Private Function GetRecList() As Data.DataTable
    '    Dim DT As DataTable = Nothing
    '    Try
    '        With wuc_pnlprdfreq
    '            DT = GetRecList(.SalesrepCode, .StartYear, .StartMonth, .EndYear, .EndMonth, .PtlCode, .strClass)
    '        End With


    '        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
    '        PreRenderMode(DT)
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
    '    Finally
    '    End Try
    '    Return DT
    'End Function

#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try

            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()


        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

    Private Class CF_SFPRDFREQ
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""

            Select Case ColumnName.ToUpper
                Case "SUPPLIER_CLASS"
                    strFieldName = "Product Class"
                Case "PLANNED_CALL"
                    strFieldName = "Planned Call"
                Case "ACTUAL_CALL"
                    strFieldName = "Actual Call"
                Case "ACHIEVED_IND"
                    strFieldName = "Achieved"
                Case "SPECIALTY"
                    strFieldName = "Specialty"
                Case "MTD_SALES"
                    strFieldName = "MTD Sales"
                Case "AVG_MTD_SALES"
                    strFieldName = "Avg. MTD Sales"
                Case Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
            End Select

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
            Try
                Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

                If strColumnName Like "DESC_NAME" Then
                    FCT = FieldColumntype.InvisibleColumn
                ElseIf strColumnName = "ACTUAL_CALL" Then
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CABYCUST, SubModuleAction.View) Then
                        FCT = FieldColumntype.HyperlinkColumn
                    Else
                        FCT = FieldColumntype.BoundColumn
                    End If
                Else
                    FCT = FieldColumntype.BoundColumn
                End If
                Return FCT
            Catch ex As Exception

            End Try
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strFormatString As String = ""
            Try
                Select Case strColumnName.ToUpper
                    Case "DATE"
                        strFormatString = "{0:yyyy-MM-dd}"
                    Case "ACTUAL_CALL", "PLANNED_CALL"
                        strFormatString = "{0:0}"
                    Case "ACHIEVED_IND", "MTD_SALES", "AVG_MTD_SALES"
                        strFormatString = "{0:0.00}"
                    Case "AVGTOT"
                        strFormatString = "{0:#,0.00}"
                    Case Else
                        strFormatString = ""
                End Select
            Catch ex As Exception
            End Try

            Return strFormatString
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            Try
                With CS
                    Dim strColumnName As String = ColumnName.ToUpper
                    .FormatString = GetOutputFormatString(ColumnName)

                    If strColumnName = "PLANNED_CALL" Then
                        .HorizontalAlign = HorizontalAlign.Right
                    ElseIf strColumnName = "ACTUAL_CALL" Or strColumnName = "MTD_SALES" Or strColumnName = "AVG_MTD_SALES" Then
                        .HorizontalAlign = HorizontalAlign.Right
                    ElseIf strColumnName = "SUPPLIER_CLASS" Then
                        .HorizontalAlign = HorizontalAlign.Center
                    ElseIf strColumnName = "CLASS" Then
                        .HorizontalAlign = HorizontalAlign.Center
                    ElseIf strColumnName = "ACHIEVED_IND" Then
                        .HorizontalAlign = HorizontalAlign.Center
                    Else
                        .HorizontalAlign = HorizontalAlign.Left
                    End If

                End With

            Catch ex As Exception

            End Try
            Return CS
        End Function
    End Class
End Class

