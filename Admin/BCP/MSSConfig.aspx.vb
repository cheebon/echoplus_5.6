﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Partial Class Admin_BCP_MSSConfig
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "SFMSConfig.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MSS_CFG)
                .DataBind()
                .Visible = True
            End With

            With wuc_toolbar
                .SubModuleID = SubModuleType.MSS_CFG
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.MSS_CFG, SubModuleAction.Create) Then
                    btnAdd.Visible = True
                End If

                If Not Report.GetAccessRight(ModuleID.ADM, SubModuleType.MSS_CFG, SubModuleAction.Delete) Then
                    btnDelete.Visible = True
                End If
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub

    Private Sub LoadTeamCodeDDL()
        Dim dtTeam As DataTable
        Dim clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtTeam = clsPrdMustSell.GetTeamDDL
            ddlTeamCode.Items.Clear()
            ddlTeamCode.DataSource = dtTeam.DefaultView
            ddlTeamCode.DataTextField = "TEAM_NAME"
            ddlTeamCode.DataValueField = "TEAM_CODE"
            ddlTeamCode.DataBind()
            ddlTeamCode.Items.Insert(0, New ListItem("--SELECT--", ""))
            ddlTeamCode.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub

    Private Sub LoadSalesrepDDL()
        Dim dtSalesrep As DataTable
        Dim clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtSalesrep = clsPrdMustSell.GetSalesrepDDL(ddlTeamCode.SelectedValue)
            ddlSalesrepCode.Items.Clear()
            ddlSalesrepCode.DataSource = dtSalesrep.DefaultView
            ddlSalesrepCode.DataTextField = "SALESREP_NAME"
            ddlSalesrepCode.DataValueField = "SALESREP_CODE"
            ddlSalesrepCode.DataBind()
            ddlSalesrepCode.Items.Insert(0, New ListItem("--SELECT--", ""))
            ddlSalesrepCode.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub


    Private Sub LoadMSSTitleCodeDDL()
        Dim dtMSSTitleCode As DataTable
        Dim clsBCP As New adm_bcp.clsBCP
        Try
            dtMSSTitleCode = clsBCP.GetMSSTitleCodeDDL(ddlTeamCode.SelectedValue, ddlSalesrepCode.SelectedValue)
            ddlMSSTitleCode.Items.Clear()
            ddlMSSTitleCode.DataSource = dtMSSTitleCode.DefaultView
            ddlMSSTitleCode.DataTextField = "TITLE_NAME"
            ddlMSSTitleCode.DataValueField = "TITLE_CODE"
            ddlMSSTitleCode.DataBind()
            ddlMSSTitleCode.Items.Insert(0, New ListItem("--SELECT--", ""))
            ddlMSSTitleCode.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCustClassDDL()
        Dim dtCustClass As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtCustClass = clsPrdMustSell.GetParamValueDDL("cust class")
            ddlCustClass.Items.Clear()
            ddlCustClass.DataSource = dtCustClass.DefaultView
            ddlCustClass.DataTextField = "PARAM_DESC"
            ddlCustClass.DataValueField = "PARAM_VALUE"
            ddlCustClass.DataBind()
            ddlCustClass.Items.Insert(0, New ListItem("--SELECT--", ""))
            ddlCustClass.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub

    Private Sub LoadRegionDDL()
        Dim dtRegion As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtRegion = clsPrdMustSell.GetParamValueDDL("region")
            ddlRegion.Items.Clear()
            ddlRegion.DataSource = dtRegion.DefaultView
            ddlRegion.DataTextField = "PARAM_DESC"
            ddlRegion.DataValueField = "PARAM_VALUE"
            ddlRegion.DataBind()
            ddlRegion.Items.Insert(0, New ListItem("--SELECT--", ""))
            ddlRegion.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDistchannelDDL()
        Dim dtDistchannel As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtDistchannel = clsPrdMustSell.GetParamValueDDL("distchannel")
            ddlDistchannel.Items.Clear()
            ddlDistchannel.DataSource = dtDistchannel.DefaultView
            ddlDistchannel.DataTextField = "PARAM_DESC"
            ddlDistchannel.DataValueField = "PARAM_VALUE"
            ddlDistchannel.DataBind()
            ddlDistchannel.Items.Insert(0, New ListItem("--SELECT--", ""))
            ddlDistchannel.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & "." & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCGCodeDDL()
        Dim dtCGCode As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell

        Try
            dtCGCode = clsPrdMustSell.GetParamValueDDL("cgcode")
            ddlCGCode.Items.Clear()
            ddlCGCode.DataSource = dtCGCode.DefaultView
            ddlCGCode.DataTextField = "PARAM_DESC"
            ddlCGCode.DataValueField = "PARAM_VALUE"
            ddlCGCode.DataBind()
            ddlCGCode.Items.Insert(0, New ListItem(" -- SELECT --", ""))
            ddlCGCode.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCGCode1DDL()
        Dim dtCGCode1 As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtCGCode1 = clsPrdMustSell.GetParamValueDDL("cgcode1")
            ddlCGCode1.Items.Clear()
            ddlCGCode1.DataSource = dtCGCode1.DefaultView
            ddlCGCode1.DataTextField = "PARAM_DESC"
            ddlCGCode1.DataValueField = "PARAM_VALUE"
            ddlCGCode1.DataBind()
            ddlCGCode1.Items.Insert(0, New ListItem(" -- SELECT --", ""))
            ddlCGCode1.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCGCode2DDL()
        Dim dtCGCode2 As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtCGCode2 = clsPrdMustSell.GetParamValueDDL("cgcode2")
            ddlCGCode2.Items.Clear()
            ddlCGCode2.DataSource = dtCGCode2.DefaultView
            ddlCGCode2.DataTextField = "PARAM_DESC"
            ddlCGCode2.DataValueField = "PARAM_VALUE"
            ddlCGCode2.DataBind()
            ddlCGCode2.Items.Insert(0, New ListItem("-- SELECT --", ""))
            ddlCGCode2.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCGCode3DDL()
        Dim dtCGCode3 As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtCGCode3 = clsPrdMustSell.GetParamValueDDL("cgcode3")
            ddlCGCode3.Items.Clear()
            ddlCGCode3.DataSource = dtCGCode3.DefaultView
            ddlCGCode3.DataTextField = "PARAM_DESC"
            ddlCGCode3.DataValueField = "PARAM_VALUE"
            ddlCGCode3.DataBind()
            ddlCGCode3.Items.Insert(0, New ListItem("-- SELECT --", ""))
            ddlCGCode3.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCGCode4DDL()
        Dim dtCGCode4 As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtCGCode4 = clsPrdMustSell.GetParamValueDDL("cgcode4")
            ddlCGCode4.Items.Clear()
            ddlCGCode4.DataSource = dtCGCode4.DefaultView
            ddlCGCode4.DataTextField = "PARAM_DESC"
            ddlCGCode4.DataValueField = "PARAM_VALUE"
            ddlCGCode4.DataBind()
            ddlCGCode4.Items.Insert(0, New ListItem("-- SELECT --", ""))
            ddlCGCode4.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCGCode5DDL()
        Dim dtCGCode5 As DataTable,
            clsPrdMustSell As New mst_Product.clsPrdMustSell
        Try
            dtCGCode5 = clsPrdMustSell.GetParamValueDDL("cgcode5")
            ddlCGCode5.Items.Clear()
            ddlCGCode5.DataSource = dtCGCode5.DefaultView
            ddlCGCode5.DataTextField = "PARAM_DESC"
            ddlCGCode5.DataValueField = "PARAM_VALUE"
            ddlCGCode5.DataBind()
            ddlCGCode5.Items.Insert(0, New ListItem("-- SELECT --", ""))
            ddlCGCode5.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlTeamCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadSalesrepDDL()

        RenewDataBind()
    End Sub

    Protected Sub ddlSalesrepCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadMSSTitleCodeDDL()
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_CustSearch)
                .BindDefault()
                .Show()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SelectButton_Click
        Try
            txtCustCode.Text = wuc_CustSearch.CustCode
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.CloseButton_Click
        Try
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ddlSalesrepCode.Items.Clear()
        ddlMSSTitleCode.Items.Clear()
        txtCustCode.Text = ""
        LoadTeamCodeDDL()
        LoadCustClassDDL()
        LoadCGCodeDDL()
        LoadCGCode1DDL()
        LoadCGCode2DDL()
        LoadCGCode3DDL()
        LoadCGCode4DDL()
        LoadCGCode5DDL()
        LoadRegionDDL()
        LoadDistchannelDDL()
        btnSave.Visible = True
        ActivateAddTab()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim sbSelectedStr As New Text.StringBuilder

            If dgList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            Dim clsBCP As New adm_bcp.clsBCP
                            clsBCP.DeleteMSSConfig(DK(0), DK(1), DK(2), DK(3), DK(4), DK(5), DK(6), DK(7), DK(8), DK(9), DK(10), DK(11), DK(12))
                        End If
                    End If
                    i += 1
                Next
            End If

            RenewDataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim clsBCP As New adm_bcp.clsBCP, DT As DataTable

            DT = clsBCP.CreateMSSConfig(Trim(ddlTeamCode.SelectedValue), Trim(ddlSalesrepCode.SelectedValue), Trim(ddlMSSTitleCode.SelectedValue), _
                                                     Trim(txtCustCode.Text), Trim(ddlCustClass.SelectedValue), Trim(ddlRegion.SelectedValue), Trim(ddlDistchannel.SelectedValue), _
                                                     Trim(ddlCGCode.SelectedValue), Trim(ddlCGCode1.SelectedValue), Trim(ddlCGCode2.SelectedValue), Trim(ddlCGCode3.SelectedValue), _
                                                     Trim(ddlCGCode4.SelectedValue), Trim(ddlCGCode5.SelectedValue))
            Dim isExists As Integer
            If DT.Rows.Count > 0 Then
                isExists = DT.Rows(0)("IS_EXISTS")
            End If

            If (isExists = 1) Then
                lblInfo.Text = "Record existed"

                Exit Sub
            Else
                lblInfo.Text = "The record is successfully created"

                ActivateEditTab()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsBCP As New adm_bcp.clsBCP
            With (wuc_toolbar)
                DT = clsBCP.GetMSSConfigList(.SearchType, .SearchValue)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrentTable = GetRecList()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
                .DataBind()
            End With

            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = dvCurrentView.Count
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()

            If Report.GetAccessRight(ModuleID.ADM, SubModuleType.SFMS_CFG, SubModuleAction.Delete) Then
                aryDataItem.Add("chkSelect")
                While dgList.Columns.Count > 1
                    dgList.Columns.RemoveAt(1)
                End While
                dgList.Columns(0).HeaderStyle.Width = "25"
                dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            Else
                dgList.Columns.Clear()
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_MSSConfig.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MSSConfig.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MSSConfig.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_MSSConfig.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_MSSConfig.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If Report.GetAccessRight(ModuleID.ADM, SubModuleType.MSS_CFG, SubModuleAction.Delete) Then
                If e.Row.RowType = DataControlRowType.Header AndAlso Master_Row_Count > 0 Then
                    Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                    If chkAll Is Nothing Then
                        chkAll = New CheckBox
                        chkAll.ID = "chkSelectAll"
                        e.Row.Cells(0).Controls.Add(chkAll)
                    End If
                    chkAll.Attributes.Add("onClick", "SelectAllRows(this)")
                    chkAll.ToolTip = "Click to toggle the selection of ALL rows"
                ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                    Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                    If chk Is Nothing Then
                        chk = New CheckBox
                        chk.ID = "chkSelect"
                        e.Row.Cells(0).Controls.Add(chk)
                    End If
                    chk.Attributes.Add("onClick", "SelectRow(this)")
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function

    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "MSS Configuration List"
        pnlList.Visible = True
        pnlDetails.Visible = False

        wuc_toolbar.show()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateAddTab()
        TabPanel1.HeaderText = "MSS Configuration Create"
        pnlList.Visible = False
        pnlDetails.Visible = True

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "MSS Configuration Create"
        pnlList.Visible = False
        pnlDetails.Visible = True

        wuc_toolbar.hide()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True)

            wuc_toolbar.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class CF_MSSConfig
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "TEAM"
                strFieldName = "Team"
            Case "SALESREP"
                strFieldName = "Salesrep"
            Case "TITLE"
                strFieldName = "Title Code"
            Case "CUST_CLASS"
                strFieldName = "Customer Class"
            Case "REGION"
                strFieldName = "Region"
            Case "DISTCHANNEL"
                strFieldName = "Distchannel"
            Case "CGCODE1"
                strFieldName = "CG Code 1"
            Case "CGCODE2"
                strFieldName = "CG Code 2"
            Case "CGCODE3"
                strFieldName = "CG Code 3"
            Case "CGCODE4"
                strFieldName = "CG Code 4"
            Case "CGCODE5"
                strFieldName = "CG Code 5"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select
        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Or strColumnName = "TEAM_CODE" Or strColumnName = "SALESREP_CODE" Or strColumnName = "PARAM_TYPE" Or strColumnName = "TITLE_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class