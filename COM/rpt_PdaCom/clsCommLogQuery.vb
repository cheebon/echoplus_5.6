'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	30/10/2005
'	Purpose	    :	Class to build DRC Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCommLogQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))


    Sub New()
        If String.IsNullOrEmpty(strConn) Then Throw New NullReferenceException("Connection String does not exist!")
    End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCommLogQuery"
        End Get
    End Property

    'execute SPP_RPT_PDACOMLIST 
    '  @YEAR, 
    '  @MONTH, 
    '  @USER_ID, 
    '  @TEAM_CODE, 
    '  @SALESREP_CODE
    Public Function GetCommLogList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_COMM_LOG"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCommLogList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_COMM_UPLOAD_INFO @USER_ID, @PRINCIPAL_ID, @PRINCIPAL_CODE, @YEAR, @MONTH, @TEAM_CODE, @REGION_CODE, @SALESREP_CODE, @GRP_FIELD, @SELECTION
    Public Function GetCommUploadInfo(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String, _
    ByVal strGroupField As String, ByVal intSelectionValue As Integer) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_COMM_UPLOAD_INFO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("SELECTION", intSelectionValue.ToString, clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCommUploadInfo :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    'HL:20070905: RPT AUDIT LOG ****************************
    Public Function GetAuditLog(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strMonth As String) As DataTable
        Dim clsDB As clsDB
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_AUDIT_LOG"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetAuditLog :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function
    
    'HL:20070906: RPT AUDIT LOG DTL****************************
    Public Function GetAuditLogDtl(ByVal strUserID As String, ByVal strYear As String, ByVal strMonth As String, ByVal strDay As String) As DataTable
        Dim clsDB As clsDB
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_AUDIT_LOG_DTL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("DAY", strDay, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetAuditLogDtl :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function
    
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub


End Class
