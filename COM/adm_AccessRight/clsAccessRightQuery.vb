'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/11/2006
'	Purpose	    :	Class AccessRightQuery 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel

'Interface Icor_AccessRightQuery
'    Function GetTemplateList(ByVal AccessRightQueryID As Long) As DataTable
'End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsAccessRightQuery
    'Inherits ServicedComponent
    'Implements Icor_AccessRightQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")
    Public clsProperties As New clsProperties.clsAccessRightQuery

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetAccessRightList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETACCESSRIGHTLIST"
                .addItem("search_type", clsProperties.search_type, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value", clsProperties.search_value, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value_1", clsProperties.search_value_1, cor_DB.clsDB.DataType.DBString)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString) 'HL:20070622
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetAccessRightList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetAccessRightDuplicate
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetAccessRightDuplicate() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETACCESSRIGHTDUPLICATE"
                .addItem("accessright_name", clsProperties.accessright_name, cor_DB.clsDB.DataType.DBString)
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetAccessRightDuplicate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetAccessRightDtl
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetAccessRightDtl() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETACCESSRIGHTDTL"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetAccessRightDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetAccessRightDtlList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetAccessRightDtlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETACCESSRIGHTDTLLIST"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("type", clsProperties.type, cor_DB.clsDB.DataType.DBInt)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetAccessRightDtlList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetModuleList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetModuleList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETMODULELIST"
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetModuleList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSubModuleList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSubModuleList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETSUBMODULELIST"
                .addItem("module_id", clsProperties.module_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("country_id", clsProperties.country_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetSubModuleList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSubModuleListAction() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETSUBMODULE_ACTION"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("principal_id", clsProperties.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("module_id", clsProperties.module_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetSubModuleList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetActionListBySubModuleID
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetActionListBySubModuleID() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETACTIONLISTBYSUBMODULEID"
                .addItem("module_id", clsProperties.module_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("submodule_id", clsProperties.submodule_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetActionListBySubModuleID" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    'PHL:20070830**************************************
    Public Function GetDFList(ByVal strAccessRightID As Integer, ByVal strReportName As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETDFLIST"
                .addItem("accessright_id", strAccessRightID, cor_DB.clsDB.DataType.DBInt)
                .addItem("REPORT_NAME", strReportName, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetDFList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    '**************************************************

    Public Function GetELList(ByVal strAccessRightID As Integer, ByVal strELName As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETELLIST"
                .addItem("accessright_id", strAccessRightID, cor_DB.clsDB.DataType.DBInt)
                .addItem("EL_NAME", strELName, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_AccessRight.clsAccessRightQuery.GetELList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.AccessRightQueryID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
