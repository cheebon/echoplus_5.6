Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class SFA_RPT
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "ATChart"
        End Get
    End Property

    Public Function GET_ATCHART(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal intNetValue As Integer) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SALES_AT"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_ATCHART :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    Public Function GET_SR_PERF(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrep_code As String, ByVal strSalesrepList As String, ByVal intNetValue As Integer) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SR_PERF" 'X5_SPP_RPT_SR_PERF
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrep_code, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_SR_PERF :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function
    Public Function GET_SR_PERF_QTD(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
            ByVal strYear As String, ByVal strQuarter As String, ByVal strSalesrep_code As String, ByVal strSalesrepList As String, ByVal intNetValue As Integer) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SR_PERF_QTD" 'X5_SPP_RPT_SR_PERF
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("QUARTER", strQuarter, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrep_code, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_SR_PERF_QTD :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function
    Public Function GET_SR_PERF2_QTD(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
           ByVal strYear As String, ByVal strQuarter As String, ByVal strSalesrep_code As String, ByVal strSalesrepList As String, ByVal intNetValue As Integer) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SR_PERF_QTD_AGENCY_ACH" 'X5_SPP_RPT_SR_PERF
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("QUARTER", strQuarter, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrep_code, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_SR_PERF_QTD :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function
    Public Function GET_SalesByAgency(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strQTD As String, ByVal strSalesrepList As String, ByVal intNetValue As Integer) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SALES_BY_AGENCY" 'X5_SPP_RPT_SR_PERF
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("QUARTER", strQTD, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_SalesByAgency :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    Public Function GET_SalesByPrd(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strYear As String, ByVal strSalesrepList As String, ByVal strAgencyCode As String, ByVal strChannelCode As String, ByVal strCustRegionCode As String) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SALES_BY_PRD"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString)
                .addItem("CHANNEL_CODE", strChannelCode, clsDB.DataType.DBString)
                .addItem("CUST_REGION_CODE", strCustRegionCode, clsDB.DataType.DBString)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_PRDSALES :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    Public Function GET_SalesByChannel(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
   ByVal strYear As String, ByVal strSalesrepList As String, ByVal strAgencyCode As String) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SALES_BY_CHANNEL"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_PRDSALES :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function
    Public Function GET_SalesByRegion(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
  ByVal strYear As String, ByVal strSalesrepList As String, ByVal strAgencyCode As String) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SALES_BY_REGION"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_PRDSALES :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    Public Function GET_SalesByCust(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, _
    ByVal strSalesrepList As String, ByVal CUST_CODE As String, ByVal CUST_NAME As String, ByVal AGENCY_CODE As String, ByVal DISTRICT_NAME As String, _
    ByVal CHANNEL_NAME As String, ByVal HOSP_NAME As String, Optional ByVal intStart As String = "1", Optional ByVal intEnd As String = "50") As DataSet
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SFA_SALES_BY_CUST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("CUST_CODE", CUST_CODE, clsDB.DataType.DBString)
                .addItem("CUST_NAME", CUST_NAME, clsDB.DataType.DBString)
                .addItem("AGENCY_CODE", AGENCY_CODE, clsDB.DataType.DBString)
                .addItem("DISTRICT_NAME", DISTRICT_NAME, clsDB.DataType.DBString)
                .addItem("CHANNEL_NAME", CHANNEL_NAME, clsDB.DataType.DBString)
                .addItem("HOSP_NAME", HOSP_NAME, clsDB.DataType.DBString)
                .addItem("STARTINDEX", intStart, clsDB.DataType.DBInt)
                .addItem("ENDINDEX", intEnd, clsDB.DataType.DBInt)
                '.addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                Return .spRetrieveDS()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_PRDSALES :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    Public Function GET_SalesByCust_GETPrdDtl(ByVal strYear As String, ByVal strMonth As String, _
    ByVal strSalesrepList As String, ByVal CUST_CODE As String, ByVal AGENCY_CODE As String) As DataSet
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CUST_GET_PRDDTL"
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("CUST_CODE", CUST_CODE, clsDB.DataType.DBString)
                .addItem("AGENCY_CODE", AGENCY_CODE, clsDB.DataType.DBString)
                Return .spRetrieveDS()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GET_PRDSALES :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function
#Region "PreBuild"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
