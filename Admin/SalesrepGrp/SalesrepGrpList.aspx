﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesrepGrpList.aspx.vb" Inherits="Admin_SalesrepGrp_SalesrepGrpList" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Salesrep Group List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />  
    <script type="text/javascript">

        function pageLoad() {
        }
    
    </script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesrepGrpList" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
        
        <asp:UpdatePanel ID="UpdatePanelDtl" runat="server">
            <ContentTemplate>
            <fieldset class="" style="width: 98%;">
            <table id="tbl1" cellspacing="0" cellpadding="2" width="100%" border="0">
                <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
                <tr>			    
                    <td class="BckgroundInsideContentLayout">
                        <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                    </td>
                </tr>
                <tr><td class="BckgroundBenealthTitle" height="5"></td></tr>
                <tr class="Bckgroundreport">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                            <TD width="20%"><asp:label ID="lblSearch" Runat="server" CssClass="cls_label_header">Search By</asp:label></TD>
					                    <TD width="2%"><asp:label ID="lblDot1" Runat="server" CssClass="cls_label_header">:</asp:label></TD>
                                <td><asp:dropdownlist id="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True">
							                    <asp:ListItem Value="all">All</asp:ListItem>
							                    <asp:ListItem Value="salesrepgrp_name">Salesrep Group Name</asp:ListItem>
							                    
						                    </asp:dropdownlist></td>
                            </tr>
                            <tr>
                                <TD><asp:label id="lblSearchValue" runat="server" CssClass="cls_label_header" Text="Salesrep Group Name"></asp:label></TD>
					                    <TD><asp:label id="lblDot" runat="server" CssClass="cls_label_header">:</asp:label></TD>
					                    <td>
						                    <asp:textbox id="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:textbox>	
						                    <asp:button id="btnSearch" runat="server" Text="Search" CssClass="cls_button"></asp:button>					   
					                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="Bckgroundreport">
                    <td align="left">                        
                                <asp:Button ID="cmdCreate" runat="server" CssClass="cls_button" Text="Create" />
                           
                    </td>
                <tr class="Bckgroundreport" style="padding:3px;">
                    <td>
                        <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="false" ></uc1:wuc_dgpaging>
				                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True"
                                                AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="450px"
                                                AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                FreezeRows="0" GridWidth="" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="salesrep_grp_id" BorderColor="Black" BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                <Columns>                                                                                                
                                                    <%--<asp:BoundField DataField="salesrep_grp_name" HeaderText="Salesrep Group Name" ReadOnly="True" SortExpression="salesrep_grp_name">
                                                        <itemstyle horizontalalign="Left" />
                                                    </asp:BoundField>--%>                                                    
                                                    <asp:ButtonField CommandName="Details" DatatextField="salesrep_grp_name" HeaderText="Salesrep Group Name" SortExpression="salesrep_grp_name">
                                                       <itemstyle horizontalalign="Left" />
                                                    </asp:ButtonField>
                                                    <asp:ButtonField CommandName="Details" DatatextField="salesrep_grp_desc" HeaderText="Salesrep Group Desc" SortExpression="salesrep_grp_desc">
                                                       <itemstyle horizontalalign="Left" />
                                                    </asp:ButtonField>     
                                                    <asp:BoundField DataField="criteria_value" HeaderText="Criteria Value" ReadOnly="True" SortExpression="criteria_value">
                                                        <itemstyle horizontalalign="Left" />
                                                    </asp:BoundField>                                 
                                                    <asp:ButtonField CommandName="Copy" AccessibleHeaderText="Copy" ImageUrl="~/images/icoCopy.gif" ButtonType="Image" HeaderText="Copy"><itemstyle horizontalalign="Center" /></asp:ButtonField>                                                                                                                                                      
                                                    <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif" ButtonType="Image" HeaderText="Delete"><itemstyle horizontalalign="Center" /></asp:ButtonField>                                                                                                                                                   
											        <asp:ButtonField CommandName="Edit" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit"><itemstyle horizontalalign="Center" /></asp:ButtonField>                                                                                                                                                
                                                </Columns>                                                
                                                <FooterStyle CssClass="GridFooter" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                <RowStyle CssClass="GridNormal" />
                                                <PagerSettings Visible="False" />
                                            </ccGV:clsGridView>
                    </td>
                </tr>
                
                
                </tr>
                </table>
            </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
