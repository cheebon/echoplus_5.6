﻿<%@ WebHandler Language="VB" Class="HandlerSyncImageUpload" %>

Imports System
Imports System.Web
Imports System.IO

Public Class HandlerSyncImageUpload : Implements IHttpHandler
    Implements IRequiresSessionState
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If context.Request.Files.Count > 0 Then
            Dim fileUpload As HttpPostedFile = context.Request.Files(0)
            Dim strSelectedPath = Portal.UserSession.strSyncImageLoc
            If strSelectedPath.Substring(strSelectedPath.Length - 1) <> "/" Then
                strSelectedPath = strSelectedPath + "/"
            End If
            Dim strPath As String = strSelectedPath
            Dim uploadPath As String = context.Server.MapPath(strPath)
            Dim fileName As String = Path.GetFileName(fileUpload.FileName) 'fileUpload.FileName

            Dim chunk As Integer = If(context.Request("chunk") IsNot Nothing, Integer.Parse(context.Request("chunk")), 0)
            
            Using fs = New FileStream(Path.Combine(uploadPath, fileName), If(chunk = 0, FileMode.Create, FileMode.Append))
                Dim buffer = New Byte(fileUpload.InputStream.Length - 1) {}
                fileUpload.InputStream.Read(buffer, 0, buffer.Length)
                fs.Write(buffer, 0, buffer.Length)
            End Using
        End If
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class