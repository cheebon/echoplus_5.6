<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerList.aspx.vb" Inherits="iFFMA_SalesAccount_CustomerList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerPriceGrpList" Src="CustomerPriceGrpList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerContList" Src="CustomerContList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerContAddPop" Src="CustomerContAddPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerContDelPop" Src="CustomerContDelPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Customer List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

</head>
<!--#include File="~/include/commonutil.js"-->

<body class="BckgroundInsideContentLayout">
    <form id="frmCustomerList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                                  
                                            <%--<span style="float:left; width:100%;">--%>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="" AllowPaging="True" DataKeyNames="CUST_CODE" bordercolor="Black" borderwidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                                                <FooterStyle CssClass="GridFooter" />
                                                                                <HeaderStyle CssClass="GridHeader" />
                                                                                <PagerSettings Visible="False" />
                                                                                <RowStyle CssClass="GridNormal" />
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="False">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtCustCode" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvCustCode" runat="server" ControlToValidate="txtCustCode"
                                                                                    ErrorMessage="Customer Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" />
                                                                                 <asp:RequiredFieldValidator ID="rfvtxtCustName" runat="server" ControlToValidate="txtCustName"
                                                                                    ErrorMessage="Customer Name is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Status</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td> 
                                                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                                                                        <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Abbreviation</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtAbbv" runat="server" CssClass="cls_textbox" MaxLength="50" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Address 1</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtAddr1" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Address 2</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtAddr2" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Address 3</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtAddr3" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Address 4</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtAddr4" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Postcode</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtPostcode" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">City</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtCity" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Region</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlRegion" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Locator</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtRouteCode" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Tel No. 1</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtTelNo1" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Tel No. 2</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtTelNo2" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Fax No. 1</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtFaxNo1" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Fax No. 2</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtFaxNo2" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Email</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtEmail" runat="server" CssClass="cls_textbox" MaxLength="50"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer Group</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlCustGrp" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Poison Buyer</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlPoison" runat="server" CssClass="cls_dropdownlist" >
                                                                                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Pay Term</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlPayTerm" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel> 
                                                       
                                                       
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                                                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_CustomerPriceGrpList ID="wuc_CustomerPriceGrpList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                 <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                                                    <ContentTemplate>
                                                        <div style=" padding-left:10px; padding-bottom:10px;">
                                                            <asp:Button ID="btnAddContact" runat="server" Text="Add Contact" CssClass=" cls_button" />
                                                            <asp:Button ID="btnDeleteContact" runat="server" Text="Remove Contact" CssClass=" cls_button" />
                                                        </div>
                                                        <customToolkit:wuc_CustomerContList ID="wuc_CustomerContList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                   <ajaxToolkit:TabPanel ID="TabPanel4" runat="server">
                                                         <ContentTemplate>
                                                              <iframe src="" id="ifMap" runat="server" visible="false" width="99%" height="400px" ></iframe>
                                                             </ContentTemplate>
                                                   </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate> 
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <customToolkit:wuc_CustomerContAddPop ID="wuc_CustomerContAddPop" runat="server" />
        <customToolkit:wuc_CustomerContDelPop ID="wuc_CustomerContDelPop" runat="server" />
    </form>
</body>
</html>
