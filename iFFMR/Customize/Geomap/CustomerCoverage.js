﻿var map;
//var Default_Latitude = 16.0333;    // VN Latitude
//var Default_Longitude = 106.8500; // VN Longitude
var markers = [];
var infowindow = new google.maps.InfoWindow({
});
var infowindows = [];
var imageIconLightGreen = {
    url: '../../../images/markerlightgreen.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0),
    labelOrigin: new google.maps.Point(13, 13)
};

var imageIconGreen = { // to track the salesrep
    url: '../../../images/markergreen.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)


};

var imageIconRed = {
    url: '../../../images/markericonred.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconGray = {
    url: '../../../images/markergray.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconYellow = {
    url: '../../../images/markeryellow.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconPurple = {
    url: '../../../images/markerpurple.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};
var imageIconBlue = {
    url: '../../../images/markerblue.png',
    size: new google.maps.Size(24, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};


var imageIconSalesrep = {
    url: '../../../images/marker_salesman.png',
    size: new google.maps.Size(30, 40),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0, 0)
};

var strsalesRepCode, strRegionCode, strSalesAreaCode, strTeamCode;

function staticIcon(number) {
    var MIcon = imageIconGreen;

    if (number == 1) { MIcon = imageIconRed; }
    else if (number == 2) { MIcon = imageIconLightGreen; }
    else if (number == 3) { MIcon = imageIconGreen; }
    else if (number == 4) { MIcon = imageIconGray; }
    else if (number == 5) { MIcon = imageIconPurple; }
    else if (number == 6) { MIcon = imageIconBlue; }
    else if (number == 7) { MIcon = imageIconYellow; }
    else if (number == 8) { MIcon = imageIconSalesrep; }
    
    return MIcon;
}
function InitializeMap() {
    var Default_Longitude = 0; //hfLongitude.value;//'106.695';//hfLongitude.Get("hfLongitude")
    var Default_Latitude = 0; //hfLatitude.value;//'10.80444444';//hfLatitude.Get("hfLatitude")
    var mapOptions = {
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        },
        mapTypeControl: true
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    if (map) {
        var point = new google.maps.LatLng(Default_Latitude, Default_Longitude);
        map.setCenter(point, 7);
    }
}

function btnSeachbyPrdClick() {
    markers = [];
    infowindows = [];
    InitializeMap();
    debugger;
    //wuc_ctrlpanel_pnlDailyTrackingSearchDetails.CollapsiblePanelBehavior._collapsed = true;
    wuc_ctrlpanel_pnlCustomerCoverageSearchDetails.CollapsiblePanelBehavior.set_Collapsed(true);
    wuc_ctrlpanel_pnlCustomerCoverageRemark.CollapsiblePanelBehavior.set_Collapsed(false);

    searchPrdlist();
}

function refreshMap(regionCodeVal, salesAreaCodeVal, teamCodeVal, salesRepCodeVal) {
    
    markers = [];
    infowindows = [];
    InitializeMap();
    debugger;
    strsalesRepCode = salesRepCodeVal;
    strRegionCode = regionCodeVal;
    strSalesAreaCode = salesAreaCodeVal;
    strTeamCode = teamCodeVal;

    searchPrdlist();
}

function searchPrdlist(){
    var ddlPrdGrpCode = $("#wuc_ctrlpanel_customercoverage_ddlProductGroup")["0"].value;
    var ddlPrdCode = $("#wuc_ctrlpanel_customercoverage_ddlProductCode")[0].value;

    var txtDateStart = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateStart")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateStart")[0].value : "";
    var txtDateEnd = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateEnd")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateEnd")[0].value : "";

    strsalesRepCode = ""; //salesrep value will be set inside the function
    console.log('searchPrdlist ---> strRegionCode ' + strRegionCode + ' ,strSalesAreaCode ' + strSalesAreaCode + ' ,strTeamCode ' + strTeamCode + ' ,strsalesRepCode ' + strsalesRepCode);

    $.ajax({
        type: "POST",
        url: "../../../DataServices/ws_CustLoc.asmx/ReturnPrdTracking",
        data: "{'strRegionCode':'" + strRegionCode + "', 'strSalesAreaCode':'" + strSalesAreaCode + "', 'strTeamCode':'" + strTeamCode + "', 'strSalesrepCode':'" + strsalesRepCode + "', 'strPrdGrpCode':'" + ddlPrdGrpCode + "','strPrdCode':'" + ddlPrdCode + "','strStartDate':'" + txtDateStart + "','strEndDate':'" + txtDateEnd + "'}",
        //data: "{'strSalesrepCode':'" + salesRepCode + "', 'strPrdGrpCode':'" + ddlPrdGrpCode + "','strPrdCode':'" + ddlPrdCode + "','strStartDate':'" + txtDateStart + "','strEndDate':'" + txtDateEnd + "'}",
        contentType: "application/json; charset=utf-8",
        success: onSuccessSearchPrdList,
        dataType: "json",
        failure: ajaxCallFailed
    });
}

function onSuccessSearchPrdList(response) {

    debugger;
    var result = eval('(' + response.d + ')');
    var strCustCode = 'Customer Code'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strCustName = 'Customer Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var infowindow = new google.maps.InfoWindow();
    if (result) {
        var froto = [];

        for (var i = 0; i < result.length; i++) {
            var m = result[i];
            var tempLat = parseFloat(result[i].LATITUDE)
            var tempLng = parseFloat(result[i].LONGITUDE)
            var myLatLng = new google.maps.LatLng(parseFloat(result[i].LATITUDE), parseFloat(result[i].LONGITUDE));
            var marker = new google.maps.Marker({
                position: {
                    lat: tempLat,
                    lng: tempLng,
                },
                icon: staticIcon(parseInt(result[i].COLOR)),
                map: map,
                infoWindowIndex: i
            });

            //var marker = new google.maps.Marker({
            //    position: {
            //        lat: tempLat,
            //        lng: tempLng,
            //    }
            //});
            //marker['icon'] = staticIcon(parseInt(result[i].COLOR));
            //marker['map'] = map;
            //marker['infoWindowIndex'] = i;

            var strcontent = '<div class="map-content">';// + result[i].SALESREP_CODE ;

            strcontent += '<font color="blue"><b>' + strCustCode + ' : ' + '</font></b>' + result[i].CUST_CODE + '<br/>';
            strcontent += '<font color="blue"><b>' + strCustName + ' : ' + '</font></b>' + result[i].CUST_NAME + '<br/>';
            strcontent += '</div>'

            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    if (infowindow) {
                        infowindow.close();
                    }
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, strcontent, infowindow));


        }
        if (result.length > 0) {
            var CenterLatLng = new google.maps.LatLng(result[result.length - 1].LATITUDE, result[result.length - 1].LONGITUDE);
            map.setCenter(CenterLatLng, 10);
            map.setZoom(11);
        } else {
            alert("Records not found!");
        }

    }
}


function setContentcallback(lat, long, lastloc, callback) {
    var response;
    var strCustCode = 'Customer Code'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strCustName = 'Customer Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var strLocation = 'Location'//'<%= Resources.GEOMAP.lblLocation%>';
    var strSalesrepID = 'Salesman ID'//'<%= Resources.GEOMAP.lblCustCode%> ';
    var strSalesrepName = 'Salesman Name'//'<%= Resources.GEOMAP.lblCustName%> ';
    var strSalesrepLastDate = 'Salesman last refreshed date'//'<%= Resources.GEOMAP.lblLocation%>';

    var txtDateStart = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateStart")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateStart")[0].value : "";
    var txtDateEnd = (typeof $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateEnd")[0] != 'undefined') ? $("#wuc_ctrlpanel_Wuc_txtCalendarRange3_txtDateEnd")[0].value : "";

    //if (result[i].CUST_CODE) {
        //var strUserDistCode = txtUserDistCode.GetValue().substr(0, txtUserDistCode.GetValue().indexOf(' '));

    debugger;

    $.ajax({
        type: "POST",
        url: "../../../DataServices/ws_CustLoc.asmx/ReturnSalesrepCustStatusTracking",
        //data: "{'strSalesrepCode':'" + salesRepCode + "'}",
        data: "{'strSalesrepCode':'" + salesRepCode + "','strStartDate':'" + txtDateStart + "','strEndDate':'" + txtDateEnd + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var result = eval('(' + response.d + ')');

            if (result) {
                var count = parseFloat(result.length);
                for (var i = 0; i < result.length; i++) {
                    if (i = result.length - 1) {
                        //console.log("show the latest location");
                        strContent = '<font color="blue"><b>' + strSalesrepID + ' : ' + '</font></b>' + result[i].SALESREP_CODE + '<br/>';
                        strContent += '<font color="blue"><b>' + strSalesrepName + ' : ' + '</font></b>' + result[i].SALESREP_NAME + '<br/>';
                        strContent += '<font color="blue"><b>' + strSalesrepLastDate + ' : ' + '</font></b>' + result[i].SALESREP_LAST_LOC_DATE + '<br/>';
                        callback(strContent);
                    }
                    else{
                        strContent = '<font color="blue"><b>' + strCustCode + ' : ' + '</font></b>' + result[i].CUST_CODE + '<br/>';
                        strContent += '<font color="blue"><b>' + strCustName + ' : ' + '</font></b>' + result[i].CUST_NAME + '<br/>';
                        //strContent += '<font color="blue"><b>' + strLocation + ' : ' + '</font></b>' + result[i].LOCATION + '<br/>';
                        callback(strContent);
                    }
                }
            }
        },
        dataType: "json",
        failure: ajaxCallFailed
    });
    //}
}

function ajaxCallFailed(error) {
    console.log(error);
}

