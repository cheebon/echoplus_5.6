Imports System.Data

Partial Class iFFMA_Mapping_FieldForceCustPDSPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

    Public Enum TypeName As Long
        Customer = 0
        Contact = 1
    End Enum

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.Value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.Value = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(hdContCode.Value)
        End Get
        Set(ByVal value As String)
            hdContCode.Value = value
        End Set
    End Property

    Public Property Type() As Long
        Get
            Return Trim(hdType.Value)
        End Get
        Set(ByVal value As Long)
            hdType.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    'Private Sub LoadDDL()
    '    Dim dtPrdSpecialty As DataTable
    '    Dim clsCommon As New mst_Common.clsDDL

    '    Try
    '        dtPrdSpecialty = clsCommon.GetPrdSpecialtyDDL(TeamCode)
    '        With ddlPrdSpecialty
    '            .Items.Clear()
    '            .DataSource = dtPrdSpecialty.DefaultView
    '            .DataTextField = "PRD_SPECIALTY_NAME"
    '            .DataValueField = "PRD_SPECIALTY_CODE"
    '            .DataBind()
    '            .Items.Insert(0, New ListItem("-- SELECT --", ""))
    '            .SelectedIndex = 0
    '        End With

    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False
    End Sub

    Public Sub LoadInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True
    End Sub

    Public Sub BindDetailsView()
        Try
            'If ddlPrdSpecialty.SelectedIndex = -1 Then
            '    lblPrdSpecialty.Text = ""
            'Else
            '    lblPrdSpecialty.Text = ddlPrdSpecialty.SelectedItem.Text
            'End If
            lblPrdSpecialty.Text = txtPrdSpecialty.Text
            lblPriority.Text = txtPriority.Text

            'LoadDDL()
            Show()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ResetPage()
        'ddlPrdSpecialty.SelectedIndex = 0
        txtPrdSpecialty.Text = ""
        txtPriority.Text = ""

        lblPrdSpecialty.Text = ""
        lblPriority.Text = ""
    End Sub

#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txtPrdSpecialty.Text = "" Then 'ddlPrdSpecialty.SelectedIndex = 0 Then
                lblInfo.Text = "Product Specialty is required!" '"Please select the Product Specialty!"
                LoadInsertMode()
                Show()
                Exit Sub
            End If

            If txtPriority.Text = "" Then
                lblInfo.Text = "Priority is required!"
                LoadInsertMode()
                Show()
                Exit Sub
            End If

            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust

            Dim DT As DataTable = Nothing
            Dim isDuplicate As Integer

            If Type = TypeName.Customer Then
                DT = clsFieldForceCust.CreateCustPDS(SalesrepCode, CustCode, Trim(txtPrdSpecialty.Text), Trim(txtPriority.Text))
            ElseIf Type = TypeName.Contact Then
                DT = clsFieldForceCust.CreateContPDS(SalesrepCode, CustCode, ContCode, Trim(txtPrdSpecialty.Text), Trim(txtPriority.Text))
            End If

            If DT.Rows.Count > 0 Then
                isDuplicate = DT.Rows(0)("IS_DUPLICATE")
            End If

            If isDuplicate = 1 Then
                lblInfo.Text = "The selected product specialty already exists!"
                LoadInsertMode()
                Show()
                Exit Sub
            Else
                lblInfo.Text = "The record is successfully created."
                LoadViewMode()
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    
End Class

