﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FieldForceRoutePrint.aspx.vb" Inherits="iFFMA_Mapping_FieldForceRouteList_FieldForceRoutePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Route</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Steps;
        var SalesrepName;
        var RouteName;
        
        $(window).load(function() {
            window.opener.GetSteps();
            if (Steps) {
                if (Steps != "") {
                    $("#divPrintArea").html(Steps);
                    $("#divSalesrep").text(SalesrepName);
                    $("#divRoute").text(RouteName);
                    
                    window.print();
                }
            }             
        });
       
</script>
</head>


<body>
    <form id="form1" runat="server">
    <div style="font-family: Tahoma, Verdana, Arial; font-size:10pt; font-weight:bold">Salesrep: <span id="divSalesrep" style="font-family: Tahoma, Verdana, Arial; font-size:10pt; font-weight:normal"></span></div>
    <div style="font-family: Tahoma, Verdana, Arial; font-size:10pt; font-weight:bold">Route: <span id="divRoute" style="font-family: Tahoma, Verdana, Arial; font-size:10pt; font-weight:normal"></span></div>
    
    <br />
    
    <div style="width: 580px">
        <div id="divPrintArea" style="width:500px; height: 450px; float:left">
        
        </div>
        <div style="float:right">
            <img src="../../../images/ico_printer.gif" alt="Print" /> <a href="javascript:window.print();">Print</a>
        </div>
    </div>
    </form>
</body>
</html>
