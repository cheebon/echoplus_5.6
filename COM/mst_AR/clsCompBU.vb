﻿'************************************************************************
'	Author	    :	Shen Yee
'	Date	    :	23/08/2016
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCompBU
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCompBU"
        End Get
    End Property

    Public Function GetCompBUList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_COMP_BU_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetCompBUDetails(ByVal strCompCode As String, ByVal strBUCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_COMP_BU_DTL"
                .addItem("COMP_CODE", strCompCode, clsDB.DataType.DBString)
                .addItem("BU_CODE", strBUCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateCompBU(ByVal strCompCode As String, ByVal strBUCode As String, ByVal strCreConArea As String, ByVal strBankBACode As String, ByVal strBankPCCode As String, ByVal strBankBizPlaceCode As String, ByVal strARBACode As String, ByVal strARCCCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_COMP_BU_CREATE"
                .addItem("COMP_CODE", strCompCode, cor_DB.clsDB.DataType.DBString)
                .addItem("BU_CODE", strBUCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CREDIT_CONTROL_AREA", strCreConArea, cor_DB.clsDB.DataType.DBString)
                .addItem("BANK_BA_CODE", strBankBACode, cor_DB.clsDB.DataType.DBString)
                .addItem("BANK_PC_CODE", strBankPCCode, cor_DB.clsDB.DataType.DBString)
                .addItem("BANK_BIZPLACE_CODE", strBankBizPlaceCode, cor_DB.clsDB.DataType.DBString)
                .addItem("AR_BA_CODE", strARBACode, cor_DB.clsDB.DataType.DBString)
                .addItem("AR_CC_CODE", strARCCCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateCompBU(ByVal strCompCode As String, ByVal strBUCode As String, ByVal strCreConArea As String, ByVal strBankBACode As String, ByVal strBankPCCode As String, ByVal strBankBizPlaceCode As String, ByVal strARBACode As String, ByVal strARCCCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_COMP_BU_UPDATE"
                .addItem("COMP_CODE", strCompCode, cor_DB.clsDB.DataType.DBString)
                .addItem("BU_CODE", strBUCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CREDIT_CONTROL_AREA", strCreConArea, cor_DB.clsDB.DataType.DBString)
                .addItem("BANK_BA_CODE", strBankBACode, cor_DB.clsDB.DataType.DBString)
                .addItem("BANK_PC_CODE", strBankPCCode, cor_DB.clsDB.DataType.DBString)
                .addItem("BANK_BIZPLACE_CODE", strBankBizPlaceCode, cor_DB.clsDB.DataType.DBString)
                .addItem("AR_BA_CODE", strARBACode, cor_DB.clsDB.DataType.DBString)
                .addItem("AR_CC_CODE", strARCCCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteCompBU(ByVal strCompCode As String, ByVal strBUCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_COMP_BU_DELETE"
                .addItem("COMP_CODE", strCompCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("BU_CODE", strBUCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



