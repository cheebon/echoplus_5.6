﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCallByMonthAndDayAdv
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallByMonthAndDayAdv"
        End Get
    End Property


    Public Function GetCallByMonthAdv(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strYear As String, ByVal strSalesrepList As String, ByVal intSelectionValue As Integer, ByVal strType As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_MONTH_ADV"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("SELECTION", intSelectionValue.ToString, 1)
                .addItem("TYPE", strType, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByMonthAdv :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    Public Function GetCallByDayAdv(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strType As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_DAY_ADV"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("TYPE", strType, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByDayAdv :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

    Public Function GetCallByDayMonthAdvType(ByVal strUserID As String) As DataTable
        Dim clsCallDB As clsDB
        Try
            clsCallDB = New clsDB
            With clsCallDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_DAY_MONTH_ADV_TYPE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallByDayMonthAdvType :" & ex.Message))
        Finally
            clsCallDB = Nothing
        End Try
    End Function

#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
