﻿Imports System.Data
Imports System.IO

Partial Class iFFMA_Product_PrdCatalogRatingPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    ''' 
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property CatCode() As String
        Get
            Return Trim(hdCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdCatCode.Value = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property Product() As String
        Get
            Return Trim(hdProduct.Value)
        End Get
        Set(ByVal value As String)
            hdProduct.Value = value
        End Set
    End Property

    Public Property RateCode() As String
        Get
            Return Trim(hdRateCode.Value)
        End Get
        Set(ByVal value As String)
            hdRateCode.Value = value
        End Set
    End Property

    Public Property RateValue() As String
        Get
            Return Trim(hdRateValue.Value)
        End Get
        Set(ByVal value As String)
            hdRateValue.Value = value
        End Set
    End Property

    Public Property FilePath() As String
        Get
            Return Trim(hdFilePath.Value)
        End Get
        Set(ByVal value As String)
            hdFilePath.Value = value
        End Set
    End Property

    Public Property IsEdit() As Boolean
        Get
            Return Trim(hdIsEdit.Value)
        End Get
        Set(ByVal value As Boolean)
            hdIsEdit.Value = value
        End Set
    End Property

#End Region
#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        txtPrd.Text = String.Empty
        txtRateCode.Text = String.Empty
        txtRateValue.Text = String.Empty
        ddlFilePath.ClearSelection()
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdSearch)
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click
        Try
            txtPrd.Text = wuc_PrdSearch.PrdCode + " - " + wuc_PrdSearch.PrdName
            PrdCode = wuc_PrdSearch.PrdCode
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = String.Empty
            Dim rateValue As String = IIf(IsEdit, Trim(txtRateValueForEdit.Text), Trim(txtRateValue.Text))
            If (String.IsNullOrEmpty(PrdCode)) Then
                lblInfo.Text = "Required!"
                Show()
                Exit Sub
            ElseIf Not String.IsNullOrEmpty(rateValue) Then
                If Not IsNumeric(rateValue) Then
                    lblInfo.Text = "You've entered invalid value in Sequence field!"
                    Show()
                    Exit Sub
                ElseIf CInt(rateValue) < 1 Then
                    lblInfo.Text = "Zero and negative value is not allowed in Sequence field!"
                    Show()
                    Exit Sub
                End If
            End If

            Dim ddl As DropDownList = If(IsEdit, ddlFilePathForEdit, ddlFilePath)
            If ddl.SelectedValue = "" Then
                lblInfo.Text = "File is required!"
                Show()
                Exit Sub
            End If

            Dim DT As DataTable
            Dim clsPrdCatalog As New mst_Product.clsPrdCatalog
            'Dim strPrdCode As String
            If (IsEdit) Then
                DT = clsPrdCatalog.PrdCatalogRatingOperation(CatCode, PrdCode, txtRateCodeForEdit.Text, txtRateValueForEdit.Text, 2, ddl.SelectedValue)
            Else
                DT = clsPrdCatalog.PrdCatalogRatingOperation(CatCode, PrdCode, txtRateCode.Text, txtRateValue.Text, 1, ddl.SelectedValue)
            End If

            Dim isDuplicate As Integer
            If DT.Rows.Count > 0 Then
                isDuplicate = DT.Rows(0)("IS_DUPLICATE")
            End If

            If (isDuplicate = 1) Then
                lblInfo.Text = "The record has already exists!"
                Show()
                Exit Sub
            Else
                lblInfo.Text = "The record is saved successfully."
            End If

            If Not (IsEdit) Then
                ResetPage()
            End If
            Show()

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub LoadInsertPanel()
        pnlInsert.Visible = True
        pnlEdit.Visible = False
        ResetPage()
    End Sub

    Public Sub LoadEditPanel()
        pnlEdit.Visible = True
        pnlInsert.Visible = False
        txtPrdForEdit.Text = Product
        txtRateCodeForEdit.Text = RateCode
        txtRateValueForEdit.Text = RateValue
    End Sub

    Public Sub LoadFileNames()
        Dim dt As New DataTable
        Try
            dt.Columns.Add("No", GetType(Integer))
            dt.Columns.Add("doc_name", GetType(String))
            dt.Columns.Add("doc_act_path", GetType(String))
            dt.Columns.Add("doc_path", GetType(String))

            Dim strPrincipalCode As String = Session("CURRENT_PRINCIPAL_CODE")

            Dim strFileRoot As String = Server.MapPath("~")
            Dim strFileRootPath As String = "~/Documents/FileManagerRoot/" + strPrincipalCode + "/ProductCatalogRating"
            Dim filePaths() As String = Directory.GetFiles(Server.MapPath(strFileRootPath))
            Dim i As Integer = 0
            For Each filePath As String In filePaths
                Dim dtRow As DataRow = dt.NewRow()
                dtRow("No") = i + 1
                dtRow("doc_name") = Path.GetFileName(filePath)
                dtRow("doc_act_path") = filePaths(i)
                dtRow("doc_path") = filePaths(i).Replace(strFileRoot, "").Replace("\", "/").Replace("FileManagerRoot/", "")
                dt.Rows.Add(dtRow)
                i = i + 1
            Next

            Dim ddl As New DropDownList
            If (IsEdit) Then
                ddl = ddlFilePathForEdit
            Else
                ddl = ddlFilePath
            End If

            If dt.Rows.Count > 0 Then
                With ddl
                    .Items.Clear()
                    .DataSource = dt
                    .DataValueField = "doc_path"
                    .DataTextField = "doc_name"
                    .DataBind()
                    If (IsEdit) And Not String.IsNullOrEmpty(FilePath) Then
                        .SelectedValue = FilePath
                    Else
                        .Items.Insert(0, New ListItem("--SELECT--", ""))
                    End If

                End With
            Else
                With ddl
                    .Items.Insert(0, New ListItem("--SELECT--", ""))
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
