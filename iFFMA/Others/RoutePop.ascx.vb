Imports System.Data

Partial Class iFFMA_Others_RoutePop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty
    Dim strRouteCode As String


#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property RouteCode() As String
        Get
            Return Trim(hdRouteCode.value)
        End Get
        Set(ByVal value As String)
            hdRouteCode.value = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub ResetPage()
        RouteCode = ""
        txtDate.Text = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsRoute As New mst_others.clsRoute

            DT = clsRoute.GetRouteDetails(TeamCode, RouteCode)
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)("LOCK_FLAG") <> "0" Then
                    RaiseEvent PopLockAlert()
                    Exit Sub
                End If

                txtDate.Text = DT.Rows(0)("DATE")
                lblDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("DATE")), "", DT.Rows(0)("DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtRouteCode As TextBox = CType(DetailsView1.FindControl("txtRouteCode"), TextBox)
            Dim txtRouteName As TextBox = CType(DetailsView1.FindControl("txtRouteName"), TextBox)
            
            Dim clsRoute As New mst_others.clsRoute

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsRoute.UpdateRoute(TeamCode, RouteCode, Trim(txtRouteName.Text), Trim(txtDate.Text))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                DT = clsRoute.CreateRoute(TeamCode, Trim(txtRouteCode.Text), Trim(txtRouteName.Text), Trim(txtDate.Text))

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Route Code already exists!"
                    RouteCode = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully created."
                    TeamCode = TeamCode
                    RouteCode = txtRouteCode.Text.ToString
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        Try
            RaiseEvent CloseButton_Click(sender, e)
            ResetPage()
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub txtDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.Load
        If RouteCode <> "" Then
            Show()
        End If
    End Sub
End Class
