Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Imports txn_WebActy

Partial Class iFFMS_Transaction_TxnListGrid
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Public Enum dgcol As Integer
        TXN_STATUS = 3
        TXN_NO = 4
        CUST_CODE = 5
        CUST_NAME = 6
        CONT_CODE = 7
        CONT_NAME = 8
        VISIT_ID = 9
        TXN_DATE = 10
        TITLE_CODE = 11
        VOUCHER_NO = 11
    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        'Call Header
        With wuc_lblHeader

            .Title = GetTitle()
            .DataBind()
            lblFieldForceName.Text = GetSalesRepName()
            lblMapPath.Text = GetMapPath()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True

           
        End If
    End Sub
    Private Function GetTitle() As String
        Dim strTitle As String
        Dim dt As DataTable
        Dim clsCommon As New txn_WebActy.clsCommon
        dt = clsCommon.GetAdmConfig("CONFIG_LAST_TXN_DATE", Session("UserID"))
        If dt.Rows.Count > 0 And dt.Rows(0)(0) = "1" Then
            Dim strLastDays As String = dt.Rows(0)(1)
            strTitle = " Last " + strLastDays + " Days Transaction List"
        Else
            strTitle = " Last 7 Days Transaction List"  'Report.GetName(SubModuleType.TRAORDER)
        End If
        Return strTitle
    End Function

#Region "DGLIST"
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub
#Region "DATA BIND"
    Public Sub RenewDataBind()

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)


        dtCurrentTable = GetRecList()

        If Trim(Request.QueryString("txntype")).ToUpper = "MSS" Then
            AddColumn("TITLE_CODE", "Title Code")
        ElseIf Trim(Request.QueryString("txntype")).ToUpper = "TRA" Then
            AddColumn("VOUCHER_NO", "Voucher No")
        End If

        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
            btncheckall.Visible = False
            btnuncheckall.Visible = False
            btncancel.Visible = False
            btnSubmit.Visible = False
        Else
            If dtCurrentTable.Rows.Count = 0 Then
                'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())

                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btncancel.Visible = False
                btnSubmit.Visible = False
                Master_Row_Count = 0
            Else
                Master_Row_Count = dtCurrentTable.Rows.Count

                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btncancel.Visible = True
                btnSubmit.Visible = True
            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        With dgList
            .DataSource = dvCurrentView
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
            .DataBind()
        End With


        UpdateDatagrid_Update()

    End Sub

    Public Sub AddColumn(ByVal strcolumn As String, ByVal strName As String)

        Dim blnExist As Boolean = False

        For Each dgCol As DataControlField In dgList.Columns
            If dgCol.HeaderText = strName Then
                blnExist = True
                Exit For
            End If
        Next
        If Not blnExist Then
            Dim dgColumn As New BoundField
            dgColumn.ReadOnly = True

            dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
            dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            dgColumn.HeaderText = strName
            dgColumn.DataField = strcolumn
            dgColumn.SortExpression = strcolumn
            dgList.Columns.Add(dgColumn)
            dgColumn = Nothing
        End If

    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strtxnType As String, strUserid As String, strSalesrep_code As String
            strtxnType = Trim(Request.QueryString("TxnType"))
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strSalesrep_code = Trim(Request.QueryString("SalesrepCode"))


            Dim clsCommon As New txn_WebActy.clsCommon
            DT = clsCommon.GetSRKIVTxnList(strtxnType, strSalesrep_code, strUserid)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_Update()


        UpdateDatagrid.Update()

    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                If Master_Row_Count > 0 Then
                    Dim strstatusCode As String
                    strstatusCode = Trim(e.Row.Cells(dgcol.TXN_STATUS).Text)
                    If strstatusCode.ToUpper = "K" Then
                        Dim chkdelete As CheckBox = CType(e.Row.FindControl("chkdelete"), CheckBox)
                        chkdelete.Visible = True
                        Dim btnedit As ImageButton = CType(e.Row.FindControl("btnedit"), ImageButton)
                        btnedit.Visible = True
                    End If

                End If
        End Select
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Dim index As Integer
        index = Convert.ToInt32(e.NewEditIndex)
        Dim strSessionId As String, strVisitID As String, strSalesrepCode As String, strTxnNo As String, strCustCode As String, _
        strContCode As String, strTxnStatus As String, strWebUrl As String, strTxnType As String, strTitleCode As String = Nothing, strVoucherNo As String = Nothing


        Dim strUserid As String = Trim(Web.HttpContext.Current.Session("UserID"))
        strTxnType = Trim(Request.QueryString("txntype"))

        Select Case strTxnType
            Case "drc"
                strWebUrl = "TxnDRC.aspx"
            Case "mss"
                strWebUrl = "TxnMSS.aspx"
                strTitleCode = dgList.Rows(index).Cells(dgcol.TITLE_CODE).Text
            Case "so"
                strWebUrl = "TxnSO.aspx"
            Case "tra"
                strWebUrl = "TxnTRA.aspx"
                strVoucherNo = dgList.Rows(index).Cells(dgcol.VOUCHER_NO).Text
            Case "sfms"
                strWebUrl = "TxnSFMS.aspx"
            Case "sfmsCuz"
                strWebUrl = "TxnSFMSCuz.aspx"
            Case Else
                strWebUrl = "TxnDefault.aspx"
        End Select

        strSessionId = Trim(Request.QueryString("sessionid"))
        strVisitID = dgList.Rows(index).Cells(dgcol.VISIT_ID).Text
        strTxnStatus = dgList.Rows(index).Cells(dgcol.TXN_STATUS).Text
        strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
        strCustCode = sender.datakeys(e.NewEditIndex).item("CUST_CODE")
        strContCode = sender.datakeys(e.NewEditIndex).item("CONT_CODE")
        strTxnNo = sender.datakeys(e.NewEditIndex).item("TXN_NO")

        LoadKIVTxnToTMP(strTxnType, strSessionId, strTxnNo, strVisitID, strSalesrepCode, strCustCode, strContCode, strUserid)

        Dim strurl = "parent.document.getElementById('SubDetailBarIframe').src='../../iFFMS/Customer/CustTxn/" + strWebUrl + "?sessionid=" + strSessionId + "&visitid=" + strVisitID + "&salesrepcode=" + strSalesrepCode + "&txnno=" + strTxnNo + "&titlecode=" + strTitleCode + "&vouchno=" + strVoucherNo + "&custcode=" + strCustCode + "&contcode=" + strContCode + "';"
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "HideElement('ContentBar');ShowElement('SubDetail');ShowElement('SubDetailBar');MaximiseFrameHeight('SubDetailBarIframe');", True)


    End Sub

    Private Sub LoadKIVTxnToTMP(ByVal strTxnType As String, ByVal strSessionId As String, ByVal strTxnNo As String, _
    ByVal strVisitId As String, ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String)
        Dim dt As DataTable
        Dim clscommon As New txn_WebActy.clsCommon
        dt = clscommon.LoadSRKIVTxnList(strTxnType, strSessionId, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContCode, strUserId)


    End Sub

    Protected Sub dgList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgList.SelectedIndexChanged

        Dim index As Integer
        index = dgList.SelectedIndex
        Dim strSessionId As String, strVisitID As String, strSalesrepCode As String, strTxnNo As String, strCustCode As String, _
        strContCode As String, strTxnStatus As String, strTxnType As String, strTitleCode As String = Nothing, strVoucherNo As String = Nothing


        Dim strUserid As String = Trim(Web.HttpContext.Current.Session("UserID"))
        strTxnType = Trim(Request.QueryString("txntype"))

        strSessionId = Trim(Request.QueryString("sessionid"))
        strVisitID = dgList.Rows(index).Cells(dgcol.VISIT_ID).Text
        strTxnStatus = dgList.Rows(index).Cells(dgcol.TXN_STATUS).Text
        strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
        strCustCode = sender.datakeys(dgList.SelectedIndex).item("CUST_CODE")
        strContCode = sender.datakeys(dgList.SelectedIndex).item("CONT_CODE")
        strTxnNo = sender.datakeys(dgList.SelectedIndex).item("TXN_NO")

        Dim strurl = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/Transaction/TxnView.aspx?sessionid=" + strSessionId + "&visitid=" + strVisitID + "&SALESREPCODE=" + strSalesrepCode + "&TXNNO=" + strTxnNo + "&TxnType=" + strTxnType + "&CustCode=" + strCustCode + "&TxnStatus=" + strTxnStatus + "&ContCode=" + strContCode + "';"
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "HideElement('ContentBar');ShowElement('DetailBar');HideElement('SubDetail');MaximiseFrameHeight('DetailBarIframe');", True)

    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")

        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpression") = strSortExpression

        RefreshDatabinding()

    End Sub


#End Region

#End Region

    Private Function GetSalesRepName() As String
        Dim strSalesrepName As String
        Dim DT As DataTable = Nothing

        Dim strSalesrepCode As String
        strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))

        Dim clsCommon As New txn_common.clstxncommon
        DT = clsCommon.GetSalesrepName(strSalesrepCode)
        If DT.Rows.Count > 0 Then
            strSalesrepName = DT.Rows(0)(0)
        Else
            strSalesrepName = ""
        End If
        Return strSalesrepName
    End Function

    Private Function GetMapPath() As String
        Dim strMapPath As String, strTransactionType As String
        strTransactionType = Trim(Request.QueryString("TxnType"))
        If strTransactionType = "drc" Then
            strTransactionType = "Inventory Forecasting"
        ElseIf strTransactionType = "so" Then
            strTransactionType = "Sales Order"
        ElseIf strTransactionType = "tra" Then
            strTransactionType = "Goods Return"
        ElseIf strTransactionType = "sfms" Then
            strTransactionType = "Field Activity"
        ElseIf strTransactionType = "sfmsCuz" Then
            strTransactionType = "Field Activity Cuz"
        ElseIf strTransactionType = "mss" Then
            strTransactionType = "Market Survey"
        End If

        strMapPath = "Ondemand > Transaction > " + strTransactionType
        Return strMapPath
    End Function

#Region "EVENT HANDLER"

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If dgList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim dt As DataTable = Nothing
            Dim strTxnStatus As String, strTxnNo As String, strCustCode As String, strContcode As String, strVisitId As String, _
            strSessionID As String, strUserID As String, strTxnType As String, strSalesrepCode As String

            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                chkSelected = CType(DR.FindControl("chkdelete"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        strTxnNo = dgList.DataKeys(i).Item("TXN_NO")
                        strCustCode = dgList.DataKeys(i).Item("CUST_CODE")
                        strContcode = dgList.DataKeys(i).Item("CONT_CODE")
                        strTxnStatus = dgList.Rows(i).Cells(dgcol.TXN_STATUS).Text
                        strVisitId = dgList.Rows(i).Cells(dgcol.VISIT_ID).Text

                        strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
                        strTxnType = Trim(Request.QueryString("TxnType"))
                        strSessionID = Trim(Request.QueryString("SessionID"))
                        strUserID = Trim(Web.HttpContext.Current.Session("UserID"))

                        Dim clscommon As New txn_WebActy.clsCommon
                        dt = clscommon.SubmitHdrDtlSRTxnList(strTxnType, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContcode, strSessionID, strUserID)
                    End If
                End If

                i += 1

            Next
            Dim strerror As Integer
            If Not IsNothing(dt) Then
                strerror = dt.Rows(0)("ERROR")
                If strerror = 0 Then
                    TimerControl1.Enabled = True
                End If
            Else
                strerror = 1
            End If

        End If
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        If dgList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim dt As DataTable = Nothing
            Dim strTxnStatus As String, strTxnNo As String, strCustCode As String, strContcode As String, strVisitId As String, _
            strSessionID As String, strUserID As String, strTxnType As String, strSalesrepCode As String

            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                chkSelected = CType(DR.FindControl("chkdelete"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        strTxnNo = dgList.DataKeys(i).Item("TXN_NO")
                        strCustCode = dgList.DataKeys(i).Item("CUST_CODE")
                        strContcode = dgList.DataKeys(i).Item("CONT_CODE")
                        strTxnStatus = dgList.Rows(i).Cells(dgcol.TXN_STATUS).Text
                        strVisitId = dgList.Rows(i).Cells(dgcol.VISIT_ID).Text

                        strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
                        strTxnType = Trim(Request.QueryString("TxnType"))
                        strSessionID = Trim(Request.QueryString("SessionID"))
                        strUserID = Trim(Web.HttpContext.Current.Session("UserID"))

                        Dim clscommon As New txn_WebActy.clsCommon
                        dt = clscommon.CancelHdrDtlSRTxnList(strTxnType, strTxnNo, strVisitId, strSalesrepCode, strCustCode, strContcode, strSessionID, strUserID)
                    End If
                End If

                i += 1

            Next
            Dim strerror As Integer
            If Not IsNothing(dt) Then
                strerror = dt.Rows(0)("ERROR")
                If strerror = 0 Then
                    TimerControl1.Enabled = True
                End If
            Else
                strerror = 1
            End If


        End If
    End Sub


#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
