<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAOrderDtlList.aspx.vb"
    Inherits="iFFMS_Order_TRAOrderDtlList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TRA Details List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <%--onload="self.parent.CollapseExpand()"--%>
</head>
    <script language="javascript" type="text/javascript">
        function CollapseParent()
        {
            if (self.parent.CollapseMain)
            {
                self.parent.CollapseMain();
            }

        }
        function refreshParent()
        {
             if(parent)
             {
                parent.frames['SideBar'].location.href=parent.frames['SideBar'].location.href;
                 if (self.parent.CollapseMain){self.parent.CollapseExpand();}
             }
         }

         function printdiv(printpage) {
             var lblviewdatetime = document.getElementById('lblviewdatetime');
             lblviewdatetime.innerHTML = Date(); 
             var headstr = '<html><head><title></title></head><body>';
             var footstr = '</body>';
             var newstr = document.all.item(printpage).innerHTML;
             var oldstr = document.body.innerHTML;
             document.body.innerHTML = headstr + newstr + footstr;
             HideNavigation();
             window.print();
             ShowNavigation();
             document.body.innerHTML = oldstr;
             return false;
         }

         function HideNavigation() {
             var height = screen.height;
             if (height < 700) {
                 self.parent.parent.document.getElementById('fraDefault').rows = "0,*";
             }
             else
             { self.parent.parent.parent.document.getElementById('fraDefault').rows = "0,*"; }
         }

         function ShowNavigation() {
             var height = screen.height;
             if (height < 700) {
                 var fraDefault = self.parent.parent.parent.document.getElementById('fraDefault').rows = "65,*";
             }
             else
             { self.parent.parent.parent.document.getElementById('fraDefault').rows = "132,*"; }
         } 
    </script>

<body class="BckgroundInsideContentLayout" onload="CollapseParent()">
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td style="width: 100%">
                                                <uc1:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uc1:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" runat="server" id="btnback" value="Back" onclick="self.parent.CollapseExpand()"
                                                    class="cls_button" style="width: 80px" />
                                                    <asp:Button ID="btncancel" runat="server" Text="Cancel Txn"  cssclass="cls_button" width="80px" visible="false" 
                                                      OnClientClick="var agree=confirm('System will cancel the current transaction! After cancel the transaction will not be editable. Are you sure you wish to continue?');if(agree)return true;else return false;" />
                                                    <asp:Button ID="btnedit" runat="server" Text="Edit/Submit Txn"  cssclass="cls_button" width="80px"  />
                                                <%--  <input type="button"  runat="server"  id="Button1" value="Goto TRA List" onclick="refreshParent()" class="cls_button"  />--%>
                                                <input name="b_print" type="button" class="cls_button" onclick="printdiv('div_print');" value=" Print"  style="width:80px;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                    <div id="div_print" style=" margin: 0px 0px 0px 0px;">
                                                        <table border="0" cellpadding="0" cellspacing="0" Width="650px">
                                                            <tr>
                                                                <td colspan="2"> 
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                    <div>
                                                                    <span class="cls_label_header">TRADE RETURN ADVICE (WEB TRA)</span>
                                                                    <asp:Label ID="lblviewdatetime" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                                    <asp:Label ID="lblviewusername" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" >
                                                                        <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                            Width="325px" BorderColor="black" BorderStyle="solid" BorderWidth="1px" >
                                                                            <Fields>
                                                                            <asp:BoundField DataField="TXN_NO" HeaderText="Txn No." ReadOnly="True" SortExpression="TXN_NO" HtmlEncode="false" />
                                                                             <asp:BoundField DataField="SALESREP" HeaderText="Salesman" ReadOnly="True" SortExpression="SALESREP" HtmlEncode="false"  />
                                                                              <asp:BoundField DataField="SALES_TEAM" HeaderText="Sales Team" ReadOnly="True" SortExpression="SALES_TEAM" HtmlEncode="false"  />
                                                                               <asp:BoundField DataField="SHIPTO" HeaderText="Ship To" ReadOnly="True" SortExpression="SHIPTO" HtmlEncode="false"  />
                                                                                <asp:BoundField DataField="PAYER" HeaderText="Payer" ReadOnly="True" SortExpression="PAYER" HtmlEncode="false"  />
                                                                                 <asp:BoundField DataField="REFER_NO" HeaderText="Refer No." ReadOnly="True" SortExpression="REFER_NO" HtmlEncode="false"  />
                                                                                  <asp:BoundField DataField="VOUCHER_NO" HeaderText="Voucher No." ReadOnly="True" SortExpression="VOUCHER_NO" HtmlEncode="false"  />
                                                                                   <asp:BoundField DataField="CREATED_BY" HeaderText="Created By" ReadOnly="True" SortExpression="CREATED_BY" HtmlEncode="false"  />
                                                                                    <asp:BoundField DataField="CREATED_DATE" HeaderText="Created Date" ReadOnly="True" SortExpression="CREATED_DATE" HtmlEncode="false"  />
                                                                                     <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS" HtmlEncode="false"  />
                                                                            </Fields>
                                                                            <RowStyle CssClass="cls_DV_Row_MST"  />
                                                                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                Width="35%" Wrap="False" />
                                                                        </asp:DetailsView>
                                                                </td>
                                                                <td valign="top">
                                                                        <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                            Width="325px" BorderColor="black" BorderStyle="solid" BorderWidth="1px"  >
                                                                             <Fields>
                                                                            <asp:BoundField DataField="STATUS" HeaderText="Status" ReadOnly="True" SortExpression="STATUS" HtmlEncode="false"  />
                                                                             <asp:BoundField DataField="SALES_OFFICE" HeaderText="Sales Office" ReadOnly="True" SortExpression="SALES_OFFICE" HtmlEncode="false"  />
                                                                              <asp:BoundField DataField="STORAGE_LOCATION" HeaderText="Storage Location" ReadOnly="True" SortExpression="STORAGE_LOCATION" HtmlEncode="false"  />
                                                                               <asp:BoundField DataField="TTL_NO_CARTON" HeaderText="Total No. Of Carton" ReadOnly="True" SortExpression="TTL_NO_CARTON" HtmlEncode="false"  />
                                                                                <asp:BoundField DataField="HEADER_REASON" HeaderText="Header Reason" ReadOnly="True" SortExpression="HEADER_REASON" HtmlEncode="false"  />
                                                                                 <asp:BoundField DataField="COLLECTED_BY" HeaderText="Collected By" ReadOnly="True" SortExpression="COLLECTED_BY" HtmlEncode="false"  />
                                                                                  <asp:BoundField DataField="GRAND_TOTAL" HeaderText="Grand Total" ReadOnly="True" SortExpression="GRAND_TOTAL" HtmlEncode="false"  DataFormatString="{0:#,0.00}" />
                                                                                 <asp:BoundField DataField="SAP_BILLING" HeaderText="SAP Billing" ReadOnly="True" SortExpression="SAP_BILLING" HtmlEncode="false"  />
                                                                            </Fields>
                                                                            <RowStyle CssClass="cls_DV_Row_MST" />
                                                                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                Width="35%" Wrap="False" />
                                                                        </asp:DetailsView>
                                                                </td>
                                                        <%--        <td style="width: 95%;">
                                                                   <div style="width: 650px; ">
                                                                        <span style="float: left; width: 325px;">
                                                                            <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                    Width="35%" Wrap="False" />
                                                                            </asp:DetailsView>
                                                                        </span><span style="float: left; width: 325px;">
                                                                            <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                    Width="35%" Wrap="False" />
                                                                            </asp:DetailsView>
                                                                        </span>
                                                                    </div>
                                                                </td>--%>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" >&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td  style="width: 95%;" colspan="2">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                        AllowSorting="true" AutoGenerateColumns="false" Width="650px" FreezeHeader="false"
                                                                        GridHeight="455" RowSelectionEnabled="true"  >
                                                                    </ccGV:clsGridView>
                                                                    <%--<ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" AddEmptyHeaders="0" CellPadding="2" GridHeight="440"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <uc1:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td style="height: 5px" colspan="2">
                                              <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
