'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	12/06/2008
'	Purpose	    :	Class to build Query for report Call Acty Enquiry
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCallActyEnquiryList
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallActyEnquiryList"
        End Get
    End Property

    Public Function GetCallActyEnquiryList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
                                        ByVal strStartDate As String, ByVal strEndDate As String, ByVal strAgencyCode As String, ByVal strTeamCode As String, _
                                        ByVal strSalesrepCode As String, ByVal strCatCode As String, ByVal strSubCatCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_ACTY_ENQUIRY_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("START_DATE", strStartDate, clsDB.DataType.DBString)
                .addItem("END_DATE", strEndDate, clsDB.DataType.DBString)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString, True)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString, True)
                .addItem("SUB_CAT_CODE", strSubCatCode, clsDB.DataType.DBString, True)

                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallActyEnquiryList :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class

