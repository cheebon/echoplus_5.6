'************************************************************************
'	Author	    :	Chee Kin Fatt
'	Date	    :	04/04/2007
'	Purpose	    :	Class UserMobile
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.ComponentModel
Imports System.Configuration
Imports System.Data
Imports cor_DB


Public Class clsPDAUserQuery

    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = Web.HttpContext.Current.Session("echoplus_conn") '"server=" & ConfigurationSettings.AppSettings("server") & ";database=" & ConfigurationSettings.AppSettings("database") & ";User ID=" & ConfigurationSettings.AppSettings("userid") & ";password=" & ConfigurationSettings.AppSettings("password")

    Public clsProperties As New adm_PDAUser.clsProperties.clsPDAUser

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub GetUserList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserList() As DataTable
       
        Dim clsUserList As clsDB
        Try
            clsUserList = New clsDB
            With clsUserList
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_GET_USER_MOBILE_LIST"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", clsProperties.strSalesRepCode, clsDB.DataType.DBString)
                .addItem("SALESREP_NAME", clsProperties.strSalesRepName, clsDB.DataType.DBString)
                .addItem("MOBILE_CODE", clsProperties.strMobileCode, clsDB.DataType.DBString)
                .addItem("ACTIVE_FLAG", clsProperties.strActiveFlag, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.GetUserList : " & ex.Message))
        Finally
            clsUserList = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub GetUserDetail
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserDetail() As DataTable

        Dim clsDB As clsDB
        Dim dt As DataTable = Nothing
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_DETAIL"
                .addItem("USER_ID", clsProperties.strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("GetUserDetail() : " & ex.Message))
        Finally
            clsDB = Nothing
        End Try
        Return dt
    End Function
    'Public Function GetUserDetail() As DataTable
    '    Dim objDB As cor_DB.clsDB
    '    Dim dt As DataTable = Nothing
    '    Dim strSQL As String

    '    Try
    '        With clsProperties
    '            strSQL = "SELECT user_mobile_id, user_mobile_code, user_mobile_name, mobile_code, pwd, created_date, " & _
    '                            "creator_user_id, changed_date, changed_user_id, sync_operation, active_flag, " & _
    '                            "user_mobile_address, user_mobile_contactno, user_mobile_email,comm_svr_setting_code " & _
    '                     "FROM TBL_ADM_USER_MOBILE " & _
    '                     "WHERE user_mobile_id = " & .strUserID
    '        End With

    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            dt = .Retrieve(strSQL)
    '        End With
    '        Return dt

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.GetUserDetail : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function UserExist
    ' Purpose	    :	Verify if user record exist / check duplicate
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _


    Public Function UserExist() As Boolean

        Dim clsDB As clsDB
        Dim dt As DataTable = Nothing
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_USER_EXISTS"
                .addItem("SALESREP_CODE", clsProperties.strSalesRepCode, clsDB.DataType.DBString)
                .addItem("MOBILE_CODE", clsProperties.strMobileCode, clsDB.DataType.DBString)
                .addItem("USER_MOBILE_ID", clsProperties.strMobileUserID, clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("UserExist(): " & ex.Message))
        Finally
            clsDB = Nothing
        End Try
        Return (dt.Rows(0).Item(0) > 0)
    End Function
    'Public Function UserExist() As Boolean
    '    Dim objDB As cor_DB.clsDB
    '    Dim dt As DataTable = Nothing
    '    Dim strSQL As String
    '    UserExist = True

    '    Try
    '        With clsProperties
    '            strSQL = " SELECT COUNT(*) " & _
    '                     " FROM TBL_ADM_USER_MOBILE " & _
    '                     " WHERE user_mobile_code = '" & .strSalesRepCode & "'" & _
    '                     " AND mobile_code = '" & .strMobileCode & "'" & _
    '                     " AND user_mobile_id <> '" & .strMobileUserID & "'" & _
    '                     " AND sync_operation <> 'D'"
    '        End With

    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            dt = .Retrieve(strSQL)
    '        End With

    '        Return (dt.Rows(0).Item(0) > 0)

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.UserExist : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetPwd
    ' Purpose	    :	Return user password for validation
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _


    Public Function GetPwd() As String

        Dim clsDB As clsDB
        Dim dt As DataTable = Nothing
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_GET_PWD"
                .addItem("SALESREP_CODE", clsProperties.strSalesRepCode, clsDB.DataType.DBString)
                .addItem("MOBILE_CODE", clsProperties.strMobileCode, clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(" GetPwd(): " & ex.Message))
        Finally
            clsDB = Nothing
        End Try
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(0).ToString
        Else
            Return String.Empty
        End If
    End Function
    'Public Function GetPwd() As String
    '    Dim objDB As cor_DB.clsDB
    '    Dim dt As DataTable = Nothing
    '    Dim strSQL As String
    '    GetPwd = ""

    '    Try
    '        With clsProperties
    '            strSQL = "SELECT pwd, user_mobile_id " & _
    '                     "FROM TBL_ADM_USER_MOBILE " & _
    '                     "WHERE user_mobile_code = '" & .strSalesRepCode & "' and mobile_code = '" & .strMobileCode & _
    '                         "' and sync_operation <> 'D'"
    '        End With

    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            dt = .Retrieve(strSQL)
    '        End With

    '        If dt.Rows.Count > 0 Then GetPwd = dt.Rows(0).Item(0).ToString

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.GetPwd : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetData
    ' Purpose	    :	Return user password and user_mobile_id
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _

    Public Function GetData() As DataTable

        Dim clsDB As clsDB
        Dim dt As DataTable = Nothing
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_GET_DATA"
                .addItem("SALESREP_CODE", clsProperties.strSalesRepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", clsProperties.strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(" GetData(): " & ex.Message))
        Finally
            clsDB = Nothing
        End Try
        Return dt
    End Function
    'Public Function GetData() As DataTable
    '    Dim objDB As cor_DB.clsDB
    '    Dim dt As DataTable = Nothing
    '    Dim strSQL As String

    '    Try
    '        With clsProperties
    '            strSQL = "SELECT pwd, user_mobile_id " & _
    '                     "FROM TBL_ADM_USER_MOBILE " & _
    '                     "WHERE user_mobile_code = '" & .strSalesRepCode & _
    '                         "' and sync_operation <> 'D'"
    '        End With

    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            dt = .Retrieve(strSQL)
    '        End With

    '        GetData = dt

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.GetPwd : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetConnection String
    ' Purpose	    :	Return Connection String code fromTBL_ADM_COMM_SVR_SETTING
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _

    Public Function GetConnectionString() As DataTable

        Dim clsDB As clsDB
        Dim dt As DataTable = Nothing
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_USER_MOBILE_GET_CONNECTION_STRING" 
                .addItem("USER_ID", clsProperties.strUserID, clsDB.DataType.DBString)
                dt = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(" GetConnectionString(): " & ex.Message))
        Finally
            clsDB = Nothing
        End Try
        Return dt
    End Function
    'Public Function GetConnectionString() As DataTable
    '    Dim objDB As cor_DB.clsDB
    '    Dim dt As DataTable = Nothing
    '    Dim strSQL As String

    '    Try
    '        With clsProperties
    '            strSQL = "SELECT distinct comm_svr_setting_code " & _
    '                     "FROM TBL_ADM_COMM_SVR_SETTING " & _
    '                     "WHERE active_flag = '1' and sync_operation <> 'D'"
    '        End With

    '        objDB = New cor_DB.clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            dt = .Retrieve(strSQL)
    '        End With

    '        GetConnectionString = dt

    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("adm_PDAUser.clsPDAUser.GetConnectionString : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class

