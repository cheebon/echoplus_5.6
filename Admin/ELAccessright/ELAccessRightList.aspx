﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ELAccessRightList.aspx.vb"
    Inherits="Admin_ELAccessright_ELAccessRightList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>EL Accessright List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' /> 
    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>  
    <script src="../../include/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        function DLLEvent(e) {
            switch (this[this.selectedIndex].value) {
                case "all":
                    $('#hfDllVisibility').val("none");
                    var btn = document.getElementById("btnSearch");

                    if (btn) btn.click();
                    break;
                default:
                    $('#hfDllVisibility').val("inline");
                    $('#txtSearchValue').val("");

            }

            RefreshSearchValue();
        }
        function RefreshSearchValue() {
            $('#txtSearchValue').css("display", $('#hfDllVisibility').val());
            $('#lblSearchValue').css("display", $('#hfDllVisibility').val());
            $('#btnSearch').css("display", $('#hfDllVisibility').val());
        }
        $(window).bind('resize', function() {
            MaximiseGridHeight('#div_dgList', 220);
        });
        $(document).ready(function() {
            $('#ddlSearchType').change(DLLEvent);

            RefreshSearchValue();

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $('#ddlSearchType').change(DLLEvent);
                MaximiseGridHeight('#div_dgList', 220);
                RefreshSearchValue();
            }
        });
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmELAccessRightList" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" CombineScripts="False" />
    <fieldset class="" style="width: 98%; ">    
    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
    <asp:UpdatePanel ID="updPnlErr" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <customToolkit:wuc_lblInfo ID="lblErr" runat="server" Title="" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <customToolkit:wuc_lblHeader ID="lblHeader" runat="server" Title="" />
    <asp:UpdatePanel ID="updPnlSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="ContainerDiv">
                <div>
                    <table border="0" cellpadding="5" cellspacing="0">
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnCreate" runat="server" Text="Create" CssClass="cls_button" />
                            </td>
                        </tr>
                        <tr>
                            <td class="srchHdr">
                                <asp:Label CssClass="cls_label_header" ID="lblSearchType" runat="server" Text="Search Type"></asp:Label>
                            </td>
                            <td class="srchContent">
                                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="cls_dropdownlist">
                                    <asp:ListItem Value="all" Text="All"></asp:ListItem>
                                    <%--<asp:ListItem Value="country_name" Text="Country" Selected="True" 
                                            meta:resourcekey="ListItemResource2"></asp:ListItem>--%>
                                    <asp:ListItem Value="accessright_desc" Text="Description"></asp:ListItem>
                                    <asp:ListItem Value="accessright_name" Text="Accessright Name" Selected="True"></asp:ListItem>
                                    <%--<asp:ListItem Value="principal_name" Text="Principal Name" 
                                            meta:resourcekey="ListItemResource5"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="srchHdr">
                                <asp:Label ID="lblSearchValue" runat="server" Text="Search Value" CssClass="cls_label_header"></asp:Label>
                            </td>
                            <td class="srchContent">
                                <asp:TextBox ID="txtSearchValue" CssClass="cls_textbox" runat="server"  ></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Search"
                                    CssClass="cls_button" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="updPnlResult" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="False"></customToolkit:wuc_dgpaging>
            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                Width="98%" FreezeHeader="True" GridHeight="300px" AddEmptyHeaders="0" CellPadding="2"
                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                AllowPaging="True" DataKeyNames="accessright_id" BorderColor="Black" BorderWidth="1px"
                GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                <Columns>
                    <asp:BoundField DataField="accessright_id" HeaderText="Accessright ID" ReadOnly="True"
                        SortExpression="accessright_id" Visible="False">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:ButtonField CommandName="Details" DataTextField="accessright_name" HeaderText="Name"
                        SortExpression="accessright_name">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="Details" DataTextField="accessright_desc" HeaderText="Description"
                        SortExpression="login">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif"
                        ButtonType="Image" HeaderText="Delete">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:ButtonField CommandName="Edit" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif"
                        ButtonType="Image" HeaderText="Edit">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                </Columns>
                <FooterStyle CssClass="GridFooter" />
                <HeaderStyle CssClass="GridHeader" />
                <AlternatingRowStyle CssClass="GridAlternate" />
                <RowStyle CssClass="GridNormal" />
                <PagerSettings Visible="False" />
            </ccGV:clsGridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hfDllVisibility" runat="server" Value="none" />
    </fieldset>
    </form>
</body>
</html>
