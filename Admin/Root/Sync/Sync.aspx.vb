﻿Option Explicit On

Imports System.Data
Imports adm_SuperAdmin

Partial Class Admin_Root_Sync_Sync
    Inherits System.Web.UI.Page
    Private _PrincipalCodeList As ListItemCollection

    Public Property PrincipalCodeList() As ListItemCollection
        Get
            If _PrincipalCodeList Is Nothing Then _PrincipalCodeList = Session("PrincipalCodeList")
            If _PrincipalCodeList Is Nothing Then _PrincipalCodeList = New ListItemCollection

            Return _PrincipalCodeList
        End Get
        Set(ByVal value As ListItemCollection)
            _PrincipalCodeList = value
            Session("PrincipalCodeList") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            lblMesg.Text = ""
            wuc_lblHeader.Title = "BSADMIN Account Create"

            If Not IsPostBack Then
                PrincipalCodeList = New ListItemCollection
                LoadCountryDDL()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#Region "Function"
    Private Sub LoadCountryDDL()
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            obj = New adm_SuperAdmin.clsSubmodulePrincipal

            dt = obj.LoadCountryDDL(Session("UserID"))

            With ddlCountry
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "COUNTRY_NAME"
                .DataValueField = "COUNTRY_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With

            With ddlPrincipal
                .Items.Clear()
                .Items.Add(New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Dim dt As DataTable
        Dim obj As adm_SuperAdmin.clsSubmodulePrincipal
        Try
            If ddlCountry.SelectedIndex = 0 Then
                ddlPrincipal.Items.Clear()
                ddlPrincipal.Items.Add(New ListItem("-- Select --", ""))
            Else
                obj = New adm_SuperAdmin.clsSubmodulePrincipal
                dt = obj.LoadPrincipalDDL(ddlCountry.SelectedValue, Session("UserID"))

                With ddlPrincipal
                    .Items.Clear()
                    .DataSource = dt
                    .DataValueField = "PRINCIPAL_ID"
                    .DataTextField = "PRINCIPAL_NAME"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- Select --", ""))
                    .SelectedIndex = 0
                End With

                For Each dr As DataRow In dt.Rows
                    PrincipalCodeList.Add(New ListItem(dr("PRINCIPAL_CODE").ToString, dr("PRINCIPAL_ID").ToString))
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSync_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSync.Click
        Try
            If Sync() Then
                lblMesg.Text = "Database synchronised successfully."
            Else
                lblMesg.Text = "Database is not synchronised. Please check FFMA contains sufficient data."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnUpdSyn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdSyn.Click
        Try
            UpdSync()
            Sync()

            lblMesg.Text = "Database updated successfully."
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnUpdLvlDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdLvlDtl.Click
        Try
            UpdLevelDetail()

            lblMesg.Text = "Level detail updated successfully."
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnUpdUserSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdUserSalesrep.Click
        Try
            UpdUserSalesrep()

            lblMesg.Text = "User Salesrep updated successfully."
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Function"
    Private Function Sync() As Boolean
        Dim obj As New adm_SuperAdmin.clsSync
        Dim dt As DataTable
        Dim principal_code As String
        Dim principal As ListItem

        Try
            principal = PrincipalCodeList.FindByValue(ddlPrincipal.SelectedValue)
            principal_code = principal.Text

            dt = obj.Sync(principal_code)

            If dt.Rows(0)("RESULT").ToString = "1" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub UpdSync()
        Dim obj As New adm_SuperAdmin.clsSync
        Dim principal_code As String
        Dim principal As ListItem

        Try
            principal = PrincipalCodeList.FindByValue(ddlPrincipal.SelectedValue)
            principal_code = principal.Text

            obj.UpdSync(principal_code)


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub UpdLevelDetail()
        Dim obj As New adm_SuperAdmin.clsSync
        Try
            

            obj.UpdLevelDtl(ddlPrincipal.SelectedValue)


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub UpdUserSalesrep()
        Dim obj As New adm_SuperAdmin.clsSync
        Try


            obj.UpdUserSalesrep(ddlPrincipal.SelectedValue)


        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

   
    
    
End Class
