'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	20/09/2005
'	Purpose	    :	Class to query all the data related to master user
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsUserQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Public clsProperties As New adm_User.clsProperties.clsUserQuery
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")
    'Private strUserID As String

    Sub New()
        'Me.strUserID = strUserID
    End Sub

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsUserQuery"
        End Get
    End Property

    Public Function GetCountryList(ByVal strUserID As String) As DataTable
        Dim clsCountryDB As clsDB
        clsCountryDB = New clsDB
        Try
            With clsCountryDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETCOUNTRYLIST"
                .addItem("userID", strUserID, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getCountryList :" & ex.Message))
        Finally
            clsCountryDB = Nothing
        End Try
    End Function

    Public Function GetPrincipleList(ByVal strUserID As String, ByVal strCountryID As String) As DataTable
        Dim clsCountryDB As clsDB
        clsCountryDB = New clsDB
        Try
            'SPP_ADM_GetPrincipleList
            With clsCountryDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETPRINCIPLELIST"
                .addItem("userID", strUserID, 1)
                .addItem("countryID", strCountryID, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrincipleList :" & ex.Message))
        Finally
            clsCountryDB = Nothing
        End Try
    End Function
    ' ''---------------------------------------------------------------------
    ' '' Procedure 	: 	Function 
    ' '' Purpose	    :	
    ' '' Parameters    :	[in] 
    ' ''   		        [in]      e: -
    ' ''		            [out]      : 
    ' ''----------------------------------------------------------------------
    'Public Function GetSearchResultDT(ByVal strCriteriaName As String, ByVal strCriteriaValue As String) As DataTable
    '    Try
    '        Dim strColumns As String = "[Agency], [Salesman], [Salesman_Name]"
    '        Dim strsbCriteria As New Text.StringBuilder
    '        Dim strSqlQueryText As String = ""
    '        Dim strSortOrder As String = "[Salesman]"

    '        'strsbCriteria.Append("[Prdcode] NOT LIKE '%n%' ")
    '        If strCriteriaValue IsNot Nothing AndAlso strCriteriaValue.Length > 0 Then
    '            strCriteriaValue = strCriteriaValue.Replace("*", "%")
    '            'strsbCriteria.Append(" AND " & strCriteriaName & " LIKE '" & strCriteriaValue & "'")
    '            strsbCriteria.Append(strCriteriaName & " LIKE '" & strCriteriaValue & "'")
    '        End If

    '        strSqlQueryText = clsSqlQueryBuilder.BuildSqlQuery(Me.TableName, strColumns, strsbCriteria.ToString, strSortOrder)
    '        Return Retrieve(strSqlQueryText)
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ClassName & ".GetSearchResultDT :" & ex.Message))
    '    Finally
    '    End Try

    'End Function
    Public Function GetTreeMenuList(ByVal strUserID As String, ByVal strPrinciple As String) As DataTable ' ByVal strCountryID As String, ByVal strPrinciple As String) As DataTable
        Dim clsCountryDB As clsDB
        Try
            clsCountryDB = New clsDB
            With clsCountryDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETTREEMENULIST"
                .addItem("userID", strUserID, 1)
                .addItem("principleID", strPrinciple, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTreeMenuList :" & ex.Message))
        Finally
            clsCountryDB = Nothing
        End Try
    End Function

    Public Function GetLevelMenuList(ByVal strUserID As String, ByVal strPrincipalCode As String) As DataTable ' ByVal strCountryID As String, ByVal strPrinciple As String) As DataTable
        Dim clsCountryDB As clsDB
        Try
            clsCountryDB = New clsDB
            With clsCountryDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETLEVELMENULIST"
                .addItem("USER_ID", strUserID, 1)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetLevelMenuList :" & ex.Message))
        Finally
            clsCountryDB = Nothing
        End Try
    End Function

    Public Function GetSalesrepOrgList(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable ' ByVal strCountryID As String, ByVal strPrinciple As String) As DataTable
        Dim clsCountryDB As clsDB
        Try
            clsCountryDB = New clsDB
            With clsCountryDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SALESREP_ORG"
                .addItem("USER_ID", strUserID, 1)
                .addItem("PRINCIPAL_ID", strPrincipalID, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetLevelMenuList :" & ex.Message))
        Finally
            clsCountryDB = Nothing
        End Try
    End Function


    'Sales List Group Field
    Public Function GetGroupFieldList(ByVal lngSubModuleID As Long) As DataTable
        Dim clsCountryDB As clsDB
        Try
            clsCountryDB = New clsDB
            With clsCountryDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETGROUPFIELDLIST"
                .addItem("subModuleID", lngSubModuleID.ToString, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetGroupFieldList :" & ex.Message))
        Finally
            clsCountryDB = Nothing
        End Try
    End Function
    Public Function GetSalesTeam(ByVal strUserCode As String) As DataTable
        Dim clsSalesTeamDB As clsDB
        Try
            clsSalesTeamDB = New clsDB
            With clsSalesTeamDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETSALESTEAM"
                .addItem("usercode", strUserCode, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeam :" & ex.Message))
        Finally
            clsSalesTeamDB = Nothing
        End Try
    End Function
    'used in sfms extraction selection
    Public Function GetSalesTeamList(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsSalesTeamDB As clsDB
        Try
            clsSalesTeamDB = New clsDB
            With clsSalesTeamDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETSALESTEAMLIST"
                .addItem("userID", strUserID, 1)
                .addItem("principleID", strPrincipalID, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeamList :" & ex.Message))
        Finally
            clsSalesTeamDB = Nothing
        End Try
    End Function

    Public Function GetSalesrepList(ByVal strUserCode As String, ByVal strSalesTeamID As String) As DataTable
        Dim clsSalesTeamDB As clsDB
        Try
            clsSalesTeamDB = New clsDB
            With clsSalesTeamDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETSALESREP_BYTEAM"
                .addItem("SALESTEAM_ID", strSalesTeamID, 2)
                .addItem("usercode", strUserCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesrepList :" & ex.Message))
        Finally
            clsSalesTeamDB = Nothing
        End Try
    End Function

    Public Function GetSalesmanList(ByVal strUserID As String, ByVal strTeamCode As String) As DataTable
        Dim clsSalesTeamDB As clsDB
        Try
            clsSalesTeamDB = New clsDB
            With clsSalesTeamDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETSALESREPLIST_BYTEAM"
                .addItem("userID", strUserID, 2, True)
                .addItem("TeamCode", strTeamCode, 2, True)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesmanList :" & ex.Message))
        Finally
            clsSalesTeamDB = Nothing
        End Try
    End Function

    Public Function GetDbSetting(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsObj As clsDB
        Try
            clsObj = New clsDB
            With clsObj
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETDBSETTING"
                '.addItem("userID", strUserID, 2, True)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBInt, True)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDbSetting :" & ex.Message))
        Finally
            clsObj = Nothing
        End Try
    End Function



    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserTypeList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserTypeList() As DataTable
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERTYPELIST"
                GetUserTypeList = .spRetrieve
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserTypeList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserDuplicate
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserDuplicate() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERDUPLICATE"
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserDuplicate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserDtl
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserDtl() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERDTL"
                '.addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                '.addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERLIST"
                .addItem("search_type", clsProperties.search_type, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value", clsProperties.search_value, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value_1", clsProperties.search_value_1, cor_DB.clsDB.DataType.DBString)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString) 'HL:20070622
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserAccessRightListByUserCode
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserAccessRightListByUserCode() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERACCESSRIGHTLISTBYUSERCODE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserAccessRightListByUserCode" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetAccessRightListByUserCode
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetAccessRightListByUserCode() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETACCESSRIGHTLISTBYUSERCODE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetAccessRightListByUserCode" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserSalesRepListByUserCode
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserSalesRepListByUserCode() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERSALESREPLISTBYUSERCODE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserSalesRepListByUserCode" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserSalesRepListByUserCode
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserSalesRepGrpListByUserID(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERSALESREPGRPLISTBYUSERID"
                .addItem("user_id", strUserID, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserSalesRepGrpListByUserID" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserLogin
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserLogin() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERLOGIN"
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserLogin" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function Authenticate
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function Authenticate() As String
        Dim objUser As adm_User.clsUser
        Dim objCrypto As cor_Crypto.clsDecryption
        Dim objDB As cor_DB.clsDB
        Dim strDecryptPwd As String
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERLOGIN"
                .addItem("login", clsProperties.login, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With

            If dt.Rows.Count > 0 Then
                'Decrypt Password
                objCrypto = New cor_Crypto.clsDecryption
                With objCrypto
                    strDecryptPwd = .TripleDESDecrypt(Trim(dt.Rows(0)("pwd")))
                End With

                If Trim(clsProperties.pwd) = Trim(strDecryptPwd) Then
                    Authenticate = dt.Rows(0)("user_code")
                Else
                    'Reset Add Count to 1
                    objUser = New adm_User.clsUser
                    With objUser
                        .clsProperties.user_code = dt.Rows(0)("user_code")
                        .clsProperties.login_cnt = 1
                        .LoginUpdate()
                    End With

                    Authenticate = ""
                End If
            Else
                Authenticate = ""
            End If

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.Authenticate" & ex.ToString))
        Finally
            objUser = Nothing
            objDB = Nothing
            objCrypto = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetUserAccessRightDtlListByUserCode
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetUserAccessRightDtlListByUserCode() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_GETUSERACCESSRIGHTDTLLISTBYUSERCODE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserQuery.GetUserAccessRightDtlListByUserCode" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '----------PWS LIB LIST
    Public Function GetUsrPassLibList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_PASS_LIB_LIST"
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    '----------PWS LIB LIST

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class