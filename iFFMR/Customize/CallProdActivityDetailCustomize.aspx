<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallProdActivityDetailCustomize.aspx.vb"
    Inherits="iFFMR_Customize_CallProdActivityDetailCustomize" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ucUpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" %>
<%@ Register TagPrefix="ucctrlpanel9" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uclblheader" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ucMenu" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CallProductivityDetail</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallProductivityDetail" method="post" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <%--                <td valign="top" align="left">
                    <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTreeMenuControl" runat="server" Width="1px">
                                <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>--%>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <ucctrlpanel9:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" /> 
                            </td>
                        </tr>
                        <%-- <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>--%>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3" style="width: 100%">
                                            <uclblheader:wuc_lblheader ID="wuc_lblHeader" runat="server" /> 
                                        </td>
                                    </tr>
                                    <%--      <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>--%>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <ucUpdateProgress:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td align="left">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="right">
                                                                            <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" ToolTip="Back To Previous Page."
                                                                                PostBackUrl="CallProdSupList.aspx?PAGE_INDICATOR=CallProdActivityDetailCustomize.aspx" />
                                                                        </td>
                                                                        <td align="left" style="width: 98%">
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                            <%--<atlas:timercontrol id="TimerControl1" runat="server" enabled="False" interval="100"
                                                                                    ontick="TimerControl1_Tick"></atlas:timercontrol>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td align="center">
                                                                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="false" AllowPaging="false"
                                                                    AllowSorting="True" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                    GridHeight="440" RowSelectionEnabled="true">
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport" style="height: 10px">
                                        <td colspan="3">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
