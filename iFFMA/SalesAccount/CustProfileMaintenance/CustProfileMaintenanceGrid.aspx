﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustProfileMaintenanceGrid.aspx.vb" Inherits="iFFMA_SalesAccount_CustProfileMaintenance_CustProfileMaintenanceGrid" %>
<%@ Register TagPrefix="customControl" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customControl" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Profile Maintenance Grid</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>  
    <script src="../../../include/layout.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onresize = function() { resizeLayout2(); }

        function NavigatePhoto(CustCode) {
            $("#DetailBarIframe", parent.document).attr('src', "../../iFFMA/SalesAccount/CustProfileMaintenance/CustProfileMaintenancePhoto.aspx?CustCode=" + CustCode);
        }
        function NavigateGPS(CustCode) {
            $("#DetailBarIframe", parent.document).attr('src', "../../iFFMA/SalesAccount/CustProfileMaintenance/CustProfileMaintenanceGPS.aspx?CustCode=" + CustCode);

        }
       
    </script>
</head>
<body class="BckgroundInsideContentLayout" onload="MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');">
    <form id="frmCustProfileMntGrid" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">

                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <%--<asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="1000" OnTick="TimerControl1_Tick" />--%>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
        
                                                            <tr>
                                                                <td align="center" style="width: 95%;">
                                                                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="440px" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" 
                                                                    GridWidth="" AllowPaging="True" DataKeyNames="CUST_CODE" bordercolor="Black" 
                                                                    borderwidth="1" GridBorderColor="Black" GridBorderWidth="1px" 
                                                                    RowHighlightColor="AntiqueWhite">
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Customer Code" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CUST_CODE" SortExpression="CUST_CODE" />
                                                                            <asp:BoundField HeaderText="Customer" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="CUST_NAME" SortExpression="CUST_NAME" />
                                                                             <asp:BoundField HeaderText="District" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="DISTRICT" SortExpression="DISTRICT" />
                                                                             <asp:BoundField HeaderText="Customer Group" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Left" DataField="CUST_GRP_NAME" SortExpression="CUST_GRP_NAME" />
                                                                             <asp:BoundField HeaderText="Cust Type" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CUST_TYPE" SortExpression="CUST_TYPE" />
                                                                             <asp:BoundField HeaderText="Class" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="CLASS" SortExpression="CLASS" />
                                                                             
                                                                             <asp:TemplateField>
                                                                                <HeaderTemplate>Photo</HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkPhoto" runat="server">Manage</asp:LinkButton>
                                                                                    
                                                                                </ItemTemplate>
                                                                             </asp:TemplateField>
                                                                             
                                                                             <%--<asp:ButtonField HeaderText="Photo" ButtonType="Link" CommandName="Photo" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                              ItemStyle-HorizontalAlign="Center" SortExpression="" Text="Manage"  />--%>
                                                                             
                                                                             <asp:BoundField HeaderText="GPS Location" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="GPS_LOCATION" SortExpression="GPS_LOCATION" />
                                                                             <asp:BoundField HeaderText="GPS Status" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                             HtmlEncode="false" ItemStyle-HorizontalAlign="Center" DataField="GPS_STATUS" SortExpression="GPS_STATUS" />
                                                                             
                                                                             <asp:TemplateField>
                                                                                <HeaderTemplate>GPS Action</HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkGPS" runat="server">Manage</asp:LinkButton>
                                                                                    
                                                                                </ItemTemplate>
                                                                             </asp:TemplateField>
                                                                             <%--<asp:ButtonField ButtonType="Link" CommandName="GPS" HeaderText="GPS Action" HeaderStyle-VerticalAlign="Middle" ItemStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center"
                                                                              ItemStyle-HorizontalAlign="Center" SortExpression="" Text="Manage" />--%>
                                                                        </Columns>
                                                                        <AlternatingRowStyle CssClass="GridAlternate" />
                                                                        <FooterStyle CssClass="GridFooter" />
                                                                        <HeaderStyle CssClass="GridHeader" />
                                                                        <PagerSettings Visible="False" />
                                                                        <RowStyle CssClass="GridNormal" />
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
  
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
