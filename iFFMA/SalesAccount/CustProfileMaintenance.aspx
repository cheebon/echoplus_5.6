﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustProfileMaintenance.aspx.vb" Inherits="iFFMA_SalesAccount_CustProfileMaintenance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Profile Maintenance</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="margin: 0; border: 0; padding: 0;" class="BckgroundInsideContentLayout">
    <%--<span id="TopBar" style="display: inline; overflow: hidden; margin: 0; border: 0;
        padding: 0;">
        <iframe id="TopBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMA/SalesAccount/CustProfileMaintenance/CustProfileMaintenanceSearch.aspx"
            width="100%" height="180" scrolling="no" style="border: 0; position: relative;
            top: 0px;"></iframe>
    </span>--%>
    <span id="ContentBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="ContentBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMA/SalesAccount/CustProfileMaintenance/CustProfileMaintenanceSearch.aspx"
            width="100%" height="0" scrolling="auto" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span><span id="DetailBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="DetailBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" scrolling="auto" height="0" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span>
    
</body>
</html>
