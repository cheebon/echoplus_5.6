﻿Option Explicit On

Imports System.Data
Partial Class Admin_Root_Submodule_SubmoduleDtl
    Inherits System.Web.UI.Page
#Region "Properties"
    Private Enum MODE
        Normal
        Edit
        Create
    End Enum
    Private Property SubmoduleID() As String
        Get
            Return ViewState("Submodule_ID")
        End Get
        Set(ByVal value As String)
            ViewState("Submodule_ID") = value
        End Set
    End Property
    Private Property ActionMode() As MODE
        Get
            Return IIf(ViewState("ActionMode") Is Nothing, MODE.Normal, ViewState("ActionMode"))
        End Get
        Set(ByVal value As MODE)
            ViewState("ActionMode") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            lblErrDtl.Text = ""
            wuc_lblHeader.Title = "Submodule"
            If Not IsPostBack Then
                Dim strSubmoduleId As String
                strSubmoduleId = Trim(Request.QueryString("SUBMODULE_ID"))
                If IsNothing(strSubmoduleId) Or strSubmoduleId = "" Then
                    SubmoduleID = 0
                    'btnBack.Visible = False
                Else
                    SubmoduleID = strSubmoduleId
                End If

                Dim strAction As String = Trim(Request.QueryString("action"))
                Select Case strAction.ToUpper
                    Case "CREATE"
                        ChangeMode(MODE.Create)
                    Case "EDIT"
                        ChangeMode(MODE.Edit)
                    Case Else 'VIEW
                        ChangeMode(MODE.Normal)
                End Select
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#Region "DTL Button Function Handling"
    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditDtl.Click
        ChangeMode(MODE.Edit)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("SubmoduleList.aspx", False)
    End Sub

    Protected Sub btnSaveDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDtl.Click
        Dim actionlist As String
        If ActionMode = MODE.Create Then
            actionlist = GatherActions()

            If actionlist.Trim = "" Then
                lblErrDtl.Text = "Must select at least 1 action for submodule."
            Else
                dvHdr_Left.InsertItem(True)
                SubmoduleActionUpdate()
                ChangeMode(MODE.Normal)
            End If

        Else
            dvHdr_Left.UpdateItem(True)
            SubmoduleActionUpdate()
            ChangeMode(MODE.Normal)
        End If
    End Sub

    Protected Sub btnCancelDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDtl.Click
        If ActionMode = MODE.Create Then
            ActionMode = MODE.Normal
            btnBack_Click(sender, Nothing)
        Else
            ChangeMode(MODE.Normal)
        End If
    End Sub
#End Region
#Region "Databinding"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()

        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        RefreshActionDatabanding()
    End Sub
    Private Sub RefreshActionDatabanding()
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetActionRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dgActionList.DataSource = dtCurrenttable.DefaultView
            dgActionList.DataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable ' = CType(ViewState("dtCurrentView"), Data.DataTable)
        'Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrenttable Is Nothing Then
            'With wuc_ctrlpanel
            '    .UpdateSalesEnquirySearchDetails()
            '    dtCurrenttable = GetRecList(.DateFrom, .Dateto, .ProductCode, .SalesmanCode, .ShipToCode, .InvoiceNumber)
            'End With
            dtCurrenttable = GetRecList()

            'ViewState("strSortExpression") = Nothing
            'ViewState("dtCurrentView") = dtCurrenttable
            'dgList.PageIndex = 0
            'End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            'If Not String.IsNullOrEmpty(strSortExpression) Then
            '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            '    dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            'End If


            dvHdr_Left.DataSource = dtCurrenttable.DefaultView
            dvHdr_Left.DataBind()

            Dim strModuleID As String = ""
            If dtCurrenttable.Rows.Count > 0 Then
                strModuleID = dtCurrenttable.Rows(0)("MODULE_CODE").ToString
            End If
            LoadModule(strModuleID)


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid.Update()
        End Try
    End Sub
    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsSubmodule
        Try

            DT = obj.SubmoduleDtl(SubmoduleID, Session("UserID"))


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
    Private Function GetActionRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Dim obj = New adm_SuperAdmin.clsSubmodule
        Dim mode As Double
        Try
            If ActionMode = Admin_Root_Submodule_SubmoduleDtl.MODE.Edit Or ActionMode = Admin_Root_Submodule_SubmoduleDtl.MODE.Create Then
                mode = 1
            Else
                mode = 0
            End If
            DT = obj.SubmoduleActionDtl(SubmoduleID, mode)


            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            'PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

    Private Sub ChangeMode(ByVal actMode As MODE)
        Select Case actMode
            Case MODE.Create
                dvHdr_Left.ChangeMode(DetailsViewMode.Insert)
                ActionMode = MODE.Create
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
                pnlAction.Visible = True
            Case MODE.Edit
                dvHdr_Left.ChangeMode(DetailsViewMode.Edit)
                ActionMode = MODE.Edit
                btnEditDtl.Visible = False
                btnSaveDtl.Visible = True
                btnCancelDtl.Visible = True
                pnlAction.Visible = True


            Case Else 'Normal
                dvHdr_Left.ChangeMode(DetailsViewMode.ReadOnly)
                ActionMode = MODE.Normal
                btnEditDtl.Visible = True
                btnSaveDtl.Visible = False
                btnCancelDtl.Visible = False
                pnlAction.Visible = True
        End Select


        'If Not Accessright.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit) Then
        '    btnEditDtl.Visible = False
        '    btnCancelDtl.Visible = False
        'End If

        RenewDataBind()
        UpdateDatagrid.Update()
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub SubmoduleActionUpdate()
        Dim ActionList As String
        Dim obj As New adm_SuperAdmin.clsSubmodule
        Try
            ActionList = GatherActions()

            If ActionList.Trim = "" Then
                lblErrDtl.Text = "Must select at least 1 action for submodule."
            Else
                obj.SubmoduleActionCreate(SubmoduleID, ActionList, Session("UserID"))
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GatherActions() As String
        Dim row As GridViewRow
        Dim vcb, ccb, ecb, dcb As CheckBox
        Dim action As New StringBuilder
        Try
            row = dgActionList.Rows(0)

            vcb = CType(row.FindControl("ViewCheckBox"), CheckBox)
            ccb = CType(row.FindControl("CreateCheckBox"), CheckBox)
            ecb = CType(row.FindControl("EditCheckBox"), CheckBox)
            dcb = CType(row.FindControl("DeleteCheckBox"), CheckBox)

            action.Append(IIf(vcb.Checked, "'1'", ""))
            action.Append(IIf(ccb.Checked, "'2'", ""))
            action.Append(IIf(ecb.Checked, "'3'", ""))
            action.Append(IIf(dcb.Checked, "'4'", ""))

            action = action.Replace("''", "','")

            Return action.ToString
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub LoadModule(ByVal strModuleCode As String)
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim dt As DataTable
        Dim lstSelected As ListItem = Nothing

        objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
        With objAccessRightQuery
            dt = .GetModuleList
        End With

        Dim ddlModule As DropDownList = CType(dvHdr_Left.FindControl("ddlModuleCode"), DropDownList)

        If dt.Rows.Count >= 0 AndAlso ddlModule IsNot Nothing Then
            With ddlModule
                .Items.Clear()
                .DataSource = dt.DefaultView
                .DataTextField = "MODULE_CODE"
                .DataValueField = "MODULE_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- Select --", ""))
                lstSelected = .Items.FindByText(strModuleCode)
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                ddlModule.Attributes.Add("onChange", "alert('Changing module code can have undesirable consequences. Proceed with care.');")
            End With
        End If
    End Sub

    Protected Sub dvHdr_Left_ItemInserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewInsertEventArgs) Handles dvHdr_Left.ItemInserting
        Dim obj As New adm_SuperAdmin.clsSubmodule
        Dim dt As DataTable
        Dim SubmoduleCode, ModuleID As String
        Dim SubmoduleName, SubmoduleName1, SubmoduleDesc, SubmoduleDesc1 As String

        Try
            SubmoduleCode = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleCode"), TextBox).Text)
            ModuleID = Trim(CType(dvHdr_Left.Rows(0).FindControl("ddlModuleCode"), DropDownList).SelectedValue)
            SubmoduleName = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleName"), TextBox).Text)
            SubmoduleName1 = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleName1"), TextBox).Text)
            SubmoduleDesc = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleDesc"), TextBox).Text)
            SubmoduleDesc1 = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleDesc1"), TextBox).Text)

            dt = obj.Create(SubmoduleCode, ModuleID, SubmoduleName, SubmoduleName1, SubmoduleDesc, SubmoduleDesc1, Session("UserID"))

            If dt Is Nothing OrElse dt.Rows.Count = 0 OrElse dt.Rows(0)("isduplicate").ToString = "1" Then
                lblErrDtl.Text = "Submodule Code already exists."
                Return
            End If

            SubmoduleID = dt.Rows(0)("SUBMODULE_ID").ToString

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dvHdr_Left_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewUpdateEventArgs) Handles dvHdr_Left.ItemUpdating
        Dim obj As New adm_SuperAdmin.clsSubmodule
        Dim SubmoduleCode, ModuleID As String
        Dim SubmoduleName, SubmoduleName1, SubmoduleDesc, SubmoduleDesc1 As String

        Try
            SubmoduleCode = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleCode"), TextBox).Text)
            ModuleID = Trim(CType(dvHdr_Left.Rows(0).FindControl("ddlModuleCode"), DropDownList).SelectedValue)
            SubmoduleName = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleName"), TextBox).Text)
            SubmoduleName1 = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleName1"), TextBox).Text)
            SubmoduleDesc = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleDesc"), TextBox).Text)
            SubmoduleDesc1 = Trim(CType(dvHdr_Left.Rows(0).FindControl("txtSubmoduleDesc1"), TextBox).Text)

            obj.Update(SubmoduleID, ModuleID, SubmoduleName, SubmoduleName1, SubmoduleDesc, SubmoduleDesc1, Session("UserID"))


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class
