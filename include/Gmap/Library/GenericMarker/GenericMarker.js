﻿function CreateMarker(Latitude, Longitude)
{
    var marker = new GMarker(new GLatLng(Latitude, Longitude));
    
    return marker;
}

function CreateMarkerWithIcon(Latitude, Longitude, newIcon)
{
    var marker = new GMarker(new GLatLng(Latitude, Longitude), {icon: newIcon});
       
    return marker;
}

function AddInfoWindow(Marker, Description)
{
    GEvent.addListener(Marker, "click", function()
        {   
            if (Description != undefined && Description != "")
            {        
                var moveEnd = GEvent.addListener(map, "moveend", function(){
                    Marker.openInfoWindowHtml(Description);					
                    GEvent.removeListener(moveEnd);
                });                                
            }
            map.panTo(Marker.getLatLng());
        }      
    );
}