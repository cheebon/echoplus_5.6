
Option Explicit On

Imports System.Data
Partial Class iFFMS_Product_ProductDetails
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Protected Property aryDataItemUOM() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemUOM")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemUOM") = value
        End Set
    End Property
    Protected Property aryDataItemEAN() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemEAN")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemEAN") = value
        End Set
    End Property
    Protected Property aryDataItemBATCH() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemBATCH")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemBATCH") = value
        End Set
    End Property
    Protected Property aryDataItemSTDBONUS() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemSTDBONUS")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemSTDBONUS") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMS_PRD"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

#End Region

    Private intPageSize As Integer
    Public ReadOnly Property PageName() As String
        Get
            Return "Product Details"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        lblErr.Text = ""

        'Call Header
        With wuc_lblHeader
            .Title = "Product Details"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True
            btnback.Visible = True

        End If
    End Sub

#Region "DGLISTUOM"
    Private Sub RefreshDatabindingUOM()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewUOM"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionUOM"), String)

        dtCurrenttable = GetRecListUOM()

        PreRenderModeUOM(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListUOM.DataSource = dvCurrentView
        dgListUOM.DataBind()

        UpdateUOM.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_InitUOM(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgListUOM.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_PRDDetails.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_PRDDetails.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName

                        dgColumn.DataNavigateUrlFields = Nothing 'strUrlFields
                        dgColumn.DataNavigateUrlFormatString = Nothing 'strUrlFormatString
                        dgColumn.Target = "_blank" '"_self"
                        dgListUOM.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemUOM.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgListUOM.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemUOM.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecListUOM() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strPrdCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strPrdCode = Trim(Request.QueryString("PRDCode"))

            Dim clsProduct As New txn_Product.clsProduct
            DT = clsProduct.GetPrdUOM(strPrdCode, strUserid)

        End If

        Return DT
    End Function

    Private Sub PreRenderModeUOM(ByRef DT As DataTable)
        Try
            aryDataItemUOM.Clear()
            If DT IsNot Nothing Then
                dgList_InitUOM(DT)
                'Cal_ItemFigureCollector(DT)

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_SortingUOM(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListUOM.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionUOM")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionUOM") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingUOM()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DGLISTEAN"
    Private Sub RefreshDatabindingEAN()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewEAN"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionEAN"), String)

        dtCurrenttable = GetRecListEAN()

        PreRenderModeEAN(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListEAN.DataSource = dvCurrentView
        dgListEAN.DataBind()

        UpdateEAN.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_InitEAN(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgListEAN.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_PRDDetails.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_PRDDetails.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName

                        dgColumn.DataNavigateUrlFields = Nothing 'strUrlFields
                        dgColumn.DataNavigateUrlFormatString = Nothing 'strUrlFormatString
                        dgColumn.Target = "_blank" '"_self"
                        dgListEAN.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemEAN.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgListEAN.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemEAN.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecListEAN() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strPrdCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strPrdCode = Trim(Request.QueryString("PRDCode"))

            Dim clsProduct As New txn_Product.clsProduct
            DT = clsProduct.GetPrdEAN(strPrdCode, strUserid)

        End If

        Return DT
    End Function

    Private Sub PreRenderModeEAN(ByRef DT As DataTable)
        Try
            aryDataItemEAN.Clear()
            If DT IsNot Nothing Then
                dgList_InitEAN(DT)
                'Cal_ItemFigureCollector(DT)

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_SortingEAN(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListEAN.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionEAN")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionEAN") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingEAN()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DGLISTBatch"
    Private Sub RefreshDatabindingBATCH()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewBATCH"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionBATCH"), String)

        dtCurrenttable = GetRecListBATCH()

        PreRenderModeBATCH(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListBatch.DataSource = dvCurrentView
        dgListBatch.DataBind()

        UpdateBATCH.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_InitBATCH(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgListBatch.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_PRDDetails.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_PRDDetails.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName

                        dgColumn.DataNavigateUrlFields = Nothing 'strUrlFields
                        dgColumn.DataNavigateUrlFormatString = Nothing 'strUrlFormatString
                        dgColumn.Target = "_blank" '"_self"
                        dgListBatch.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemBATCH.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgListBatch.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemBATCH.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecListBATCH() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strPrdCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strPrdCode = Trim(Request.QueryString("PRDCode"))

            Dim clsProduct As New txn_Product.clsProduct
            DT = clsProduct.GetPrdBatch(strPrdCode, strUserid)

        End If

        Return DT
    End Function

    Private Sub PreRenderModeBATCH(ByRef DT As DataTable)
        Try
            aryDataItemBATCH.Clear()
            If DT IsNot Nothing Then
                dgList_InitBATCH(DT)
                'Cal_ItemFigureCollector(DT)

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_SortingBATCH(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListBatch.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionBATCH")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionBATCH") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingBATCH()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DGLISTSTDBONUS"
    Private Sub RefreshDatabindingSTDBONUS()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewSTDBONUS"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionSTDBONUS"), String)

        dtCurrenttable = GetRecListSTDBONUS()

        PreRenderModeSTDBONUS(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListSTDBONUS.DataSource = dvCurrentView
        dgListSTDBONUS.DataBind()

        UpdateSTDBONUS.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_InitSTDBONUS(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgListSTDBONUS.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_PRDDetails.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_PRDDetails.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName

                        dgColumn.DataNavigateUrlFields = Nothing 'strUrlFields
                        dgColumn.DataNavigateUrlFormatString = Nothing 'strUrlFormatString
                        dgColumn.Target = "_blank" '"_self"
                        dgListSTDBONUS.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemSTDBONUS.Add(ColumnName)

                        licItemFigureCollector.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_PRDDetails.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_PRDDetails.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgListSTDBONUS.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItemSTDBONUS.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecListSTDBONUS() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strPrdCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strPrdCode = Trim(Request.QueryString("PRDCode"))

            Dim clsProduct As New txn_Product.clsProduct
            DT = clsProduct.GetPrdStdBonus(strPrdCode, strUserid)

        End If

        Return DT
    End Function

    Private Sub PreRenderModeSTDBONUS(ByRef DT As DataTable)
        Try
            aryDataItemSTDBONUS.Clear()
            If DT IsNot Nothing Then
                dgList_InitSTDBONUS(DT)
                'Cal_ItemFigureCollector(DT)

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_SortingSTDBONUS(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListSTDBONUS.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionSTDBONUS")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionSTDBONUS") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingSTDBONUS()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region


#Region "DETAILVIEW"

    Private Function GetRecList2() As Data.DataTable
        Dim DT As DataTable
        Dim strPrdCode As String, strUserid As String
        strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
        strPrdCode = Trim(Request.QueryString("PRDCode"))

        Dim clsProduct As New txn_Product.clsProduct
        DT = clsProduct.GetPrdDetails(strPrdCode, strUserid)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub RefreshDataBinding2()
        Dim dtCurrenttable2 As Data.DataTable = Nothing
        dtCurrenttable2 = GetRecList2()

        Dim dvCurrentView2 As New Data.DataView(dtCurrenttable2)
        If dtCurrenttable2 IsNot Nothing Then dv_Init(dtCurrenttable2)
        dtviewleft.DataSource = dvCurrentView2
        dtviewleft.DataBind()
        dtviewright.DataSource = dvCurrentView2
        dtviewright.DataBind()


    End Sub

    Protected Sub dv_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim j As Integer = 0
        Dim ColumnNamedv As String = ""

        Try
            dtviewleft.Fields.Clear()
            dtviewright.Fields.Clear()

            For i = 0 To dtToBind.Columns.Count - 1
                ColumnNamedv = dtToBind.Columns(i).ColumnName

                Select Case CF_PRDDetails.GetFieldColumnType(ColumnNamedv, True)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        j = j + 1
                        Dim dvcolumn As New BoundField

                        dvcolumn = New BoundField

                        Dim strFormatStringdv As String = CF_PRDDetails.GetOutputFormatString(ColumnNamedv)
                        If String.IsNullOrEmpty(strFormatStringdv) = False Then
                            dvcolumn.DataFormatString = strFormatStringdv
                            dvcolumn.HtmlEncode = False
                        End If

                        dvcolumn.DataField = ColumnNamedv
                        dvcolumn.HeaderText = CF_PRDDetails.GetDisplayColumnName(ColumnNamedv)
                        dvcolumn.ReadOnly = True
                        dvcolumn.SortExpression = ColumnNamedv

                        If j Mod 2 = 0 Then
                            dtviewright.Fields.Add(dvcolumn)
                        Else
                            dtviewleft.Fields.Add(dvcolumn)
                        End If

                        dvcolumn = Nothing
                End Select
            Next

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDataBinding2()
            RefreshDatabindingUOM()
            RefreshDatabindingEAN()
            RefreshDatabindingBATCH()
            RefreshDatabindingSTDBONUS()
        End If
        TimerControl1.Enabled = False
        UpdatePage.update()
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class


Public Class CF_PRDDetails
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""


        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn No."
            Case "TXN_STATUS"
                strFieldName = "Status"
            Case "LINE_NO"
                strFieldName = "Line No."
            Case "UOM_CODE"
                strFieldName = "UOM Code"
            Case "COLL_CODE"
                strFieldName = "Collector Code"
            Case "BATCH_NO"
                strFieldName = "Batch No."
            Case "REASON_CODE"
                strFieldName = "Reason Code"
            Case "FOC_PRD_UOM"
                strFieldName = "FOC Prd. UOM"
            Case "LIST_PRICE"
                strFieldName = "Price"
            Case "FOC_PRD_CODE"
                strFieldName = "FOC Prd. Code"
            Case "REF_NO"
                strFieldName = "Ref No."
            Case "EXP_DATE"
                strFieldName = "Exp. Date"
            Case "MAX_FOC_QTY"
                strFieldName = "Max. FOC Qty."
            Case "FOC_TYPE"
                strFieldName = "FOC Type"
            Case "FOC_QTY"
                strFieldName = "FOC Qty."
            Case "END_DATE"
                strFieldName = "End Date"
            Case "START_DATE"
                strFieldName = "Start Date"
            Case "PURCHASE_QTY"
                strFieldName = "Purchase Qty."
            Case "EAN_NO"
                strFieldName = "EAN No."
            Case "MIN_QTY"
                strFieldName = "Min. Qty."
            Case "CONVERTION_FACTOR"
                strFieldName = "Conv. Factor"
            Case "UOM_PRICE"
                strFieldName = "UOM Price"
            Case "PRD_TYPE"
                strFieldName = "Prd. Type"
            Case "DISC_MIN_QTY"
                strFieldName = "Disc. Min. Qty."
            Case "MAX_QTY"
                strFieldName = "Max. Qty."
            Case "&NBSP;"
                strFieldName = " "
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "" Then

                FCT = FieldColumntype.HyperlinkColumn

            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "ICO"
                    strFormatString = "{0:#,0}"
                Case "AMS"
                    strFormatString = "{0:#,0}"
                Case "UOM_PRICE"
                    strFormatString = "{0:#,0.00}"
                    'Case "*_QTY"
                    '    strFormatString = "{0:#,0.00}"
                Case Else
                    strFormatString = ""
            End Select

            If strColumnName.ToUpper Like "*_QTY" Then
                strFormatString = "{0:#,0}"
            End If
            If strColumnName.ToUpper Like "*AMT" Then
                strFormatString = "{0:#,0.00}"
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*NAME" Or strColumnName Like "*REMARKS" Or strColumnName Like "*_NO" Then
                    .HorizontalAlign = HorizontalAlign.Left
                ElseIf strColumnName Like "*CODE" Or strColumnName = "TXN_STATUS" Or strColumnName Like "*DATE" Then
                    .HorizontalAlign = HorizontalAlign.Center
                Else
                    .HorizontalAlign = HorizontalAlign.Right
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class