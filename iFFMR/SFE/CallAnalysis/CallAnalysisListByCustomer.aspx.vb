Imports System.Data 
Imports System.IO 
Imports rpt_Customer

Partial Class CallAnalysisListByCustomer
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Public Property vSessionMonth() As String
        Get
            Return ViewState("SessionMonth")
        End Get
        Set(ByVal value As String)
            ViewState("SessionMonth") = value
        End Set
    End Property
    Public Property sSessionMonth() As String
        Get
            Return Session("MONTH")
        End Get
        Set(ByVal value As String)
            Session("MONTH") = value
        End Set
    End Property
    Public Property vMonthInThisPage() As String
        Get
            Return ViewState("MonthInThisPage")
        End Get
        Set(ByVal value As String)
            ViewState("MonthInThisPage") = value
        End Set
    End Property
    Public Property sSessionYear() As String
        Get
            Return Session("Year")
        End Get
        Set(ByVal value As String)
            Session("Year") = value
        End Set
    End Property
    Public Property vSessionYear() As String
        Get
            Return ViewState("SessionYear")
        End Get
        Set(ByVal value As String)
            ViewState("SessionYear") = value
        End Set
    End Property
    Public ReadOnly Property CallDate() As String
        Get
            Return String.Format("{0}-{1}-{2}", sSessionYear, vMonthInThisPage, vDayInThisPage)
        End Get
    End Property
    Public Property vYearInThisPage() As String
        Get
            Return ViewState("YearInThisPage")
        End Get
        Set(ByVal value As String)
            ViewState("YearInThisPage") = value
        End Set
    End Property
    Public Property vDayInThisPage() As String
        Get
            Return ViewState("DayInThisPage")
        End Get
        Set(ByVal value As String)
            ViewState("DayInThisPage") = value
        End Set
    End Property
    Public Property vSalesrepCode() As String
        Get
            Return Request.QueryString("SALESREP_CODE")
        End Get
        Set(ByVal value As String)
            Request.QueryString("SALESREP_CODE") = value
        End Set
    End Property
    Public Property PAGE_INDICATOR() As String
        Get
            Return Request.QueryString("PAGE_INDICATOR")
        End Get
        Set(ByVal value As String)
            Request.QueryString("PAGE_INDICATOR") = value
        End Set
    End Property
    Public Property isDownloadEnabled() As Boolean
        Get
            Return ViewState("isDownloadEnabled")
        End Get
        Set(ByVal value As Boolean)
            ViewState("isDownloadEnabled") = value
        End Set
    End Property

#End Region

#Region "Criteria Collector"
    Dim WithEvents clsCriteriaCollector As clsSharedValue
    Dim strCollectorName As String = "Collector_CallAnalysisListByCustomer"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CallAnalysisListByCustomer.aspx"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'set root for client
            Dim strRoot As String = ResolveClientUrl("~")
            strRoot = strRoot.Substring(0, strRoot.Length - 1)
            AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "RootVar", "root= '" & strRoot & "';", True)

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CABYCUST) '"Call Analysis List By Customer"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CABYCUST
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""


            Dim intYear As Integer '= sSessionYear
            Dim intMonth As Integer
            Dim intDay As Integer = 0

            If Not IsPostBack Then
                Session("CallAnalysisListByCustomer_QueryString") = Request.QueryString.ToString.Replace("+", "")

                intYear = CInt(Request.QueryString("YEAR")) 'HK ES
                intMonth = CInt(Request.QueryString("MONTH"))
                intDay = CInt(Request.QueryString("DAY"))

                If intMonth = 0 Then intMonth = sSessionMonth
                If intYear = 0 Then intYear = sSessionYear
                'Dim strSalesman As String = Trim(Request.QueryString("SALESREP_CODE"))
                'If Not String.IsNullOrEmpty(strSalesman) Then session("SALESREP_CODE") = strSalesman

                ViewState("dtCurrentView") = Nothing
                vSessionMonth = sSessionMonth
                vSessionYear = sSessionYear 'HK ES

                Dim strPageIndicator As String = Trim(Request.QueryString("PAGE_INDICATOR"))
                If Not String.IsNullOrEmpty(strPageIndicator) Then
                    Dim strQueryString As String = String.Empty
                    If strPageIndicator = "CALLBYDAY" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx"
                        strQueryString = Session("CallAnalysisListByDay_QueryString")
                    ElseIf strPageIndicator = "SALESACTY" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/KPI/ActSalesBySalesrep.aspx"
                        strQueryString = Session("CallEnquiry_QueryString")
                    ElseIf strPageIndicator = "DAILYCALLANALYSIS" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/DailyCallAnalysis.aspx"
                        strQueryString = Session("DailyCallAnalysis_QueryString")
                    ElseIf strPageIndicator = "FIELDFORCEPRDFREQ" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/SalesrepPrdFreq/SalesrepPrdFreqGrid.aspx"
                        strQueryString = "PAGE_INDICATOR=CALLANABYCUST"
                    ElseIf strPageIndicator = "CALLANALYBYCONTCLASS" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/CallAnalysisByContClass.aspx"
                        strQueryString = "PAGE_INDICATOR=CABYCUST"
                    ElseIf strPageIndicator = "CALLBYDAYADV" Then
                        ViewState("POSTBACK_URL") = "~/iFFMR/Customize/CallByMonthAdv/CallByDayAdv.aspx"
                        strQueryString = Session("CallAnalysisListByDayAdv_QueryString")
                    End If
                    If Not String.IsNullOrEmpty(strQueryString) Then ViewState("POSTBACK_URL") += "?" & strQueryString
                End If

                btnGPS.Visible = GetAccessRight(ModuleID.FFMR, SubModuleType.CABYCUST, "'13'")

                TimerControl1.Enabled = True
            Else
                If vSessionMonth <> sSessionMonth Then
                    vSessionMonth = sSessionMonth
                    vMonthInThisPage = sSessionMonth
                End If

                If vSessionYear <> sSessionYear Then
                    vSessionYear = sSessionYear
                    vYearInThisPage = sSessionYear
                End If

                intMonth = vMonthInThisPage
                If intMonth = 0 Then intMonth = sSessionMonth

                intYear = vYearInThisPage
                If intYear = 0 Then intYear = sSessionYear

                intDay = vDayInThisPage
            End If

            If intDay = 0 Then intDay = 1

            vYearInThisPage = intYear 'HK ES
            vDayInThisPage = intDay
            vMonthInThisPage = intMonth

            If Not String.IsNullOrEmpty(ViewState("POSTBACK_URL")) Then btnBack.PostBackUrl = ViewState("POSTBACK_URL")

            'Report.UpdateTreePath("SALESREP_CODE", Trim(Request.QueryString("SALESREP_CODE")))



        Catch ex As Exception

        End Try
    End Sub

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False
        Dim dtAR As DataTable

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function

#Region "Download KML"
    Protected Sub btnDownloadKML_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadKML.Click
        Dim strFilePath As String
        Try
            Dim clsXML As New rpt_CALL.clsCallToXML()
            strFilePath = clsXML.generateKMLFile(vSalesrepCode, CallDate)
            If IO.File.Exists(strFilePath) Then
                downloadKML(strFilePath)
            Else
                lblErr.Text = "Failed to initial download KML file, file does not exists or not readable."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.Message)
        End Try
    End Sub
    Private Sub downloadKML(ByVal strFilePath As String)
        Dim file As System.IO.FileInfo = New System.IO.FileInfo(strFilePath)
        If file.Exists Then
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End()
        End If
    End Sub

#End Region

#Region "Google Map"
    Protected Sub btnGPS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGPS.Click
        Try
            Dim strYear, strMonth, strDay, strUserID, strPrincipalCode, strPrincipalID, strSalesrepCode As String

            If vSessionMonth <> sSessionMonth Then
                vSessionMonth = sSessionMonth
                vMonthInThisPage = sSessionMonth
            End If

            strYear = CInt(sSessionYear)
            strMonth = CInt(vMonthInThisPage)
            strDay = CInt(vDayInThisPage)
            If strMonth = 0 Then strMonth = sSessionMonth

            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = Trim(vSalesrepCode)

            

            'AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('wei');", True)

            AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "LocalRoute", "SetParam('" & strUserID & "', '" & strPrincipalID & "', '" & strPrincipalCode & "', '" & strYear & "', '" & strMonth & "', '" & strDay & "', '" & strSalesrepCode & "')", True)
            AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "CheckResize", "map.checkResize();", True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.Message)
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    Try
    ''        wuc_ctrlPanel.RefreshDetails()
    ''        wuc_ctrlPanel.UpdateControlPanel()
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "CallAnalysisByCustomer")
            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()

        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Else
                PreRenderMode(dtCurrentTable)
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CallByCustomer.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_CallByCustomer.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_CallByCustomer.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallByCustomer.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallByCustomer.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName

                        Dim strUrlFormatString As String
                        Dim strUrlFields() As String = Nothing

                        strUrlFormatString = FormUrlFormatString(ColumnName, dtToBind, strUrlFields)
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CallByCustomer.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            'dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CallByCustomer.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_CallByCustomer.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function FormUrlFormatString(ByVal strColumnName As String, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Try
            ReDim strUrlFields(4)
            strUrlFields(0) = "SALESREP_CODE"
            strUrlFields(1) = "VISIT_ID"
            strUrlFields(2) = "CUST_CODE"
            strUrlFields(3) = "DATE"
            strUrlFields(4) = "CONT_CODE"

            Select Case strColumnName
                Case "SFMS_IND" 'Generate SFMS_IND
                    strUrlFormatString = "~/iFFMR/SFE/CallProd/CallProdDailyActivity.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "SALES_IND"
                    strUrlFormatString = "~/iFFMR/Sales/SalesOrder.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "TRA_IND"
                    strUrlFormatString = "~/iFFMR/TRA/TraRetDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "COLL_IND"
                    strUrlFormatString = "~/iFFMR/COLL/COLLDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "DRC_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/DRCDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "MSS_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/MSSDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "DN_IND"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/DNDailyActy.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "HIST_IND"
                    strUrlFormatString = "~/iFFMR/Customize/CallActyHistByCont.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&CONT_CODE={4}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case "DET_IND" 'Generate SFMS_IND
                    strUrlFormatString = "~/iFFMR/Customize/DetailingInfo.aspx?SALESREP_CODE={0}&VISIT_ID={1}&CUST_CODE={2}&TXN_DATE={3:yyyy-MM-dd}&PAGE_INDICATOR=CALLBYCUSTOMER"
                Case Else
                    strUrlFields = Nothing
                    strUrlFormatString = ""
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".FormUrlFormatString : " & ex.ToString)
        End Try
        Return strUrlFormatString
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            btnDownloadKML.Visible = (aryDataItem.IndexOf("LONGITUDE") > 0)
            'btnGPS.Visible = (aryDataItem.IndexOf("LONGITUDE") > 0)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strYear, strMonth, strDay, strUserID, strPrincipalCode, strPrincipalID, strSalesrepCode As String

            If vSessionMonth <> sSessionMonth Then
                vSessionMonth = sSessionMonth
                vMonthInThisPage = sSessionMonth
            End If
            If vSessionYear <> sSessionYear Then
                vSessionYear = sSessionYear
                vYearInThisPage = sSessionYear
            End If

            'strYear = CInt(sSessionYear)
            strYear = CInt(vYearInThisPage)
            strMonth = CInt(vMonthInThisPage)
            strDay = CInt(vDayInThisPage)
            If strMonth = 0 Then strMonth = sSessionMonth
            If strYear = 0 Then strYear = sSessionYear

            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strSalesrepCode = Trim(vSalesrepCode) 'session("SALESREP_CODE")
            'Wuc_lblDate1.ControlType = include_wuc_lblDate.DateTimeControlType.DateOnly
            'Wuc_lblDate1.SelectedDateTime = New Date(CInt(strYear), CInt(strMonth), CInt(strDay))

            wuc_ctrlpanel.SelectedDateTimeValue = New Date(CInt(strYear), CInt(strMonth), CInt(strDay))
            'ViewState("SelectedDate") = wuc_ctrlpanel.SelectedDateTimeString

            If PAGE_INDICATOR = "FIELDFORCEPRDFREQ" Then
                Dim clsSFPrdFreq As New rpt_Customize.clsSFPrdFreq
                With clsSFPrdFreq.properties
                    .UserID = Session.Item("UserID")
                    .PrinID = Session("PRINCIPAL_ID")
                    .PrinCode = Session("PRINCIPAL_CODE")
                    .SalesrepCode = vSalesrepCode
                    .CustCode = Request.QueryString("CUST_CODE")
                    .ContCode = Request.QueryString("CONT_CODE")
                    .StartYear = Request.QueryString("STARTYEAR")
                    .StartMonth = Request.QueryString("STARTMONTH")
                    .EndYear = Request.QueryString("ENDYEAR")
                    .EndMonth = Request.QueryString("ENDMONTH")
                    .PTLCode = Request.QueryString("CAT_CODE")
                End With

                DT = clsSFPrdFreq.GetSFPrdFreqSFMS

            ElseIf PAGE_INDICATOR = "CALLANALYBYCONTCLASS" Then

                Dim strSalesrepCode2 As String, strCustCode As String, strContCode As String, stryear2 As String, strmonth2 As String _
                , strday2 As String, strAgencyCode As String
                strSalesrepCode2 = vSalesrepCode
                strCustCode = Request.QueryString("CUST_CODE")
                strContCode = Request.QueryString("CONT_CODE")
                stryear2 = Request.QueryString("YEAR")
                strmonth2 = Request.QueryString("MONTH")
                strday2 = Request.QueryString("DAY")
                strAgencyCode = Request.QueryString("AGENCY_CODE")

                Dim clsCallDB As New rpt_CALL.clsCallQuery
                DT = clsCallDB.GetCallByCustomerADV(strUserID, strPrincipalID, strPrincipalCode, stryear2, strmonth2, strday2, strSalesrepCode2, _
                strCustCode, strContCode, strAgencyCode)

            Else
                Dim clsCallDB As New rpt_CALL.clsCallQuery
                DT = clsCallDB.GetCallByCustomer(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strDay, strSalesrepCode)
            End If
            dgList_Init(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    

    Private Sub RefreshDatabindingrpCust()

        Dim dt As DataTable
        dt = GetRecListrpCust()
        If dt.Rows.Count > 0 Then
            rpCust.DataSource = dt
            rpCust.DataBind()
            lblImgLoadFailMsg.visible = False
        Else
            rpCust.DataSource = Nothing
            rpCust.DataBind()
            lblImgLoadFailMsg.visible = True
        End If

        'lblCustCode.Text = hfPicCustCode.Value
        lblCustName.Text = hfPicCustName.Value
        'lblAddress.Text = hfPicCustAdd.Value
    End Sub

    Private Function GetRecListrpCust() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsCustomerProfile
            Dim strCustCode As String = hfPicCustCode.Value
            DT = clsCustomer.GetCustomerProfilePicture(strCustCode, Portal.UserSession.UserID)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub btnRefreshPic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshPic.Click
        RefreshDatabindingrpCust()
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "Show", "SetVisit('Show'); ShowVisit(); $('#Visit').center();", True)
    End Sub


    Protected Sub rpCust_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCust.ItemDataBound
        Dim imgCust As Image = DirectCast(e.Item.FindControl("imgCust"), Image)
        Dim hfCustCode As HiddenField = DirectCast(e.Item.FindControl("hfCustCode"), HiddenField)
        Dim hfCustName As HiddenField = DirectCast(e.Item.FindControl("hfCustName"), HiddenField)
        Dim hfImgLoc As HiddenField = DirectCast(e.Item.FindControl("hfImgLoc"), HiddenField)


        Dim strImgLoc As String = hfImgLoc.Value
        Dim strCustName As String = hfCustName.Value

        If strImgLoc.ToString.Trim <> "" Then
            If File.Exists(Server.MapPath("~" + strImgLoc.ToString)) Then
                imgCust.ImageUrl = "~" & strImgLoc.ToString & "?" & DateTime.Now.ToString("yyyyMMddHHmmss")

            Else
                imgCust.ImageUrl = "~" + "\images\anonymous.jpg"

            End If

            ' EmbedConfirmation(UploadedFlag)
        Else
            imgCust.ImageUrl = "~" + "\images\anonymous.jpg"

        End If
    End Sub
End Class

Public Class CF_CallByCustomer
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "DEPARTMENT"
                strFieldName = "Department"
            Case "POSITION"
                strFieldName = "Position"
            Case "TIME_IN"
                strFieldName = "Time In"
            Case "TIME_OUT"
                strFieldName = "Time Out"
            Case "TIME_SPEND"
                strFieldName = "Time Spend"
            Case "VISIT_IND"
                strFieldName = "Type"
            Case "COOR_LOC"
                strFieldName = "Location"
            Case "CALL_DATE"
                strFieldName = "Date"
            Case "LONGITUDE"
                strFieldName = "Longitude"
            Case "LATITUDE"
                strFieldName = "Latitude"
            Case "GPS_TIME"
                strFieldName = "GPS Time"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Select Case strColumnName.ToUpper
                Case "SFMS_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CPRODACTYDTL, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "SALES_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESORD, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "TRA_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.TRAINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "COLL_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.COLLINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DRC_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DRCINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "MSS_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.MSSINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DN_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DNINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "HIST_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CALLACTYHISTBYCONT, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "DET_IND"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.DETINFO, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "SALESREP_CODE", "REASON_CODE", "VISIT_ID", "DATE"
                    FCT = FieldColumntype.InvisibleColumn
                Case "REASON_NAME"
                    FCT = FieldColumntype.BoundColumn
            End Select

            Return FCT

        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "COOR_LOC"
                    strFormatString = "{0}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function


    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class