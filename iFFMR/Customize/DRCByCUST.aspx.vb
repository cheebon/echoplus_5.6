Imports System.Data

Partial Class iFFMR_Customize_DRCByCUST
    Inherits System.Web.UI.Page

    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection
    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Public ReadOnly Property PageName() As String
        Get
            Return "DRCByCUST"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblErr.Text = ""
        lblMesg.Text = ""
        If Not Page.IsPostBack Then
            'wuc_paneldrc.DataBind()
            With wuc_DRCCtrlPnl1
                .SubModuleID = SubModuleType.CUZDRCCUST
                .DataBind()
            End With
        End If
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_DRCCtrlPnl1.GenerateBtn_Click
        lblMesg.Text = ""
        RenewDataBind()
        PnlExport.Visible = True
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_DRCCtrlPnl1.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "CallEnquiry")
            wuc_DRCCtrlPnl1.ExportToFile(dgList, wuc_lblheader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            'Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".btnExport_Click : " & ex.ToString)
        End Try
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)

        dtCurrentTable = GetRecList()
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
    End Sub

    Public Sub UpdateDatagrid_Update()
        If dgList.Rows.Count < 10 Then
            dgList.GridHeight = Nothing
        End If
        UpdatePanel1.Update()
    End Sub

#End Region

#Region "Event Handle"
    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_DRCCtrlPnl1.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_IN.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_IN.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_IN.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_IN.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_IN.ColumnStyle(ColumnName).Wrap

                    'dgColumn.HeaderText = CF_IN.RemColPrefix(CF_IN.GetDisplayColumnName(ColumnName))
                    dgColumn.HeaderText = CF_IN.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName.ToUpper)
            End Select
        Next
    End Sub
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer
                Dim iIndex As Integer
                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_IN.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next
        End Select
    End Sub

#Region "Math Function"
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

#End Region

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsRpt As New rpt_Customize.clsDRC


            Dim intNetValue As Integer
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

            DT = clsRpt.GetDRCByCust(wuc_DRCCtrlPnl1.SelectedSalesRep, wuc_DRCCtrlPnl1.CalStartDate, wuc_DRCCtrlPnl1.CalEndDate, wuc_DRCCtrlPnl1.SelectedProduct, intNetValue)
            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        PreRenderMode(DT)
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper

            'If (strColumnName Like "SALES_*") Then
            '    liColumnField = New ListItem(strColumnName.ToUpper(), 0)
            '    licItemFigureCollector.Add(liColumnField)
            'End If
            If (strColumnName Like "SALES" Or strColumnName Like "*SALES" Or strColumnName Like "*QTY" Or strColumnName Like "STOCK") Then
                liColumnField = New ListItem(strColumnName.ToUpper(), 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        RefreshDatabinding()
    End Sub
#End Region

    Public Class CF_IN
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""
            Select Case ColumnName.ToUpper
                Case "CUSTOMER CODE"
                    strFieldName = "Customer Code"
                Case "CUSTOMER"
                    strFieldName = "Customer"
                Case "CLASS"
                    strFieldName = "Class"
                Case "DISTRICT_NAME"
                    strFieldName = "District"
                Case "FIELD FORCE"
                    strFieldName = "Field Force"
                Case "DRC_DATE"
                    strFieldName = "Stock Take Date"
                Case "MTD_SALES"
                    strFieldName = "Sales"
                Case "YTD_SALES"
                    strFieldName = "YTD Sales"
                Case "SALES_QTY"
                    strFieldName = "Sales QTY"
                Case "FOC_QTY"
                    strFieldName = "FOC QTY"
                Case "STK_QTY"
                    strFieldName = "Stock QTY"
                Case Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
            End Select

            Return strFieldName
        End Function

        Public Shared Function RemColPrefix(ByVal ColumnName As String) As String
            ColumnName = ColumnName.ToUpper.Replace("_AMT", "")

            Return ColumnName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            'Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            'strColumnName = strColumnName.ToUpper
            'Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            'If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
            '   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            '    FCT = FieldColumntype.InvisibleColumn
            'Else
            '    FCT = FieldColumntype.BoundColumn
            'End If

            FCT = FieldColumntype.BoundColumn

            Return FCT
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strStringFormat As String = ""
            'If strColumnName.ToUpper Like "SALES_*" Then
            '    strStringFormat = "{0:#,0.00}"
            'End If
            If strColumnName.ToUpper Like "*SALES" Then
                strStringFormat = "{0:#,0.00}"
            End If
            If strColumnName.ToUpper Like "*QTY" Then
                strStringFormat = "{0:#,0}"
            End If
            If strColumnName.ToUpper Like "STOCK" Then
                strStringFormat = "{0:#,0}"
            End If
            'Select Case strColumnName.ToUpper
            '    Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
            '         "MTD_TGT", "QTD_TGT", "YTD_TGT"
            '        strStringFormat = "{0:#,0.00}"
            '    Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
            '        strStringFormat = "{0:#,0.0}"
            '    Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
            '         "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
            '        strStringFormat = "{0:#,0}"
            '    Case Else
            '        strStringFormat = ""
            'End Select

            Return strStringFormat
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                    .Wrap = False
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

            Return CS
        End Function
    End Class
End Class
