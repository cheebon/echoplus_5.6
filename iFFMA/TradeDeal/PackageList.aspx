<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PackageList.aspx.vb" Inherits="iFFMA_TradeDeal_PackageList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PackagePrdList" Src="PackagePrdList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PackageFOCList" Src="PackageFOCList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PackageCustList" Src="PackageCustList.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Package List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout"> 
    <form id="frmPackageList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                            
                                            <%--<span style="float:left; width:100%;">--%>
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="PACKAGE_CODE">
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Package Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtPackageCode" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvPackageCode" runat="server" ControlToValidate="txtPackageCode"
                                                                                    ErrorMessage="Package Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Package Name</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtPackageName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvPackageName" runat="server" ControlToValidate="txtPackageName"
                                                                                    ErrorMessage="Package Name is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            <asp:Panel ID="pnlRegionCode" runat="server" Visible="false">
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Region Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlRegionCode" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvRegionCode" runat="server" ControlToValidate="ddlRegionCode"
                                                                                    ErrorMessage="Region Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
                                                                            </tr>
                                                                            </asp:Panel>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Type</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true">
                                                                                        <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                                                        <asp:ListItem Text="Selective" Value="S"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <asp:Panel ID="pnlPackageSpec" runat="server" Visible="false">
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Package Spec</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlPackageSpec" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                                                                        <asp:ListItem Text="Specific" Value="S"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="pnlTypeSelective" runat="server">
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Quantity</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>    
                                                                                    <asp:TextBox ID="txtQty" runat="server" CssClass="cls_textbox" />
                                                                                    <asp:RegularExpressionValidator ID="revQty" runat="server" ControlToValidate="txtQty" CssClass="cls_label_err"
                                                                                        ErrorMessage="Invalid, only (Integer) value allowed!" ValidationGroup="Save" ValidationExpression="^\d+$" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Amount</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtAmt" runat="server" CssClass="cls_textbox" />
                                                                                    <asp:RegularExpressionValidator ID="revAmt" runat="server" ControlToValidate="txtAmt" CssClass="cls_label_err"
                                                                                        ErrorMessage="Invalid, only number with decimal value allowed!" ValidationGroup="Save" ValidationExpression="^\d*(.\d{1,2})?$" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">FOC Quantity</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtFOCQty" runat="server" CssClass="cls_textbox" />
                                                                                    <asp:RegularExpressionValidator ID="revFOCQty" runat="server" ControlToValidate="txtFOCQty" CssClass="cls_label_err"
                                                                                        ErrorMessage="Invalid, only (Integer) value allowed!" ValidationGroup="Save" ValidationExpression="^\d+$" />
                                                                                </td>
                                                                            </tr>
                                                                            </asp:Panel>
                                                                            <tr>
                                                                                <td colspan="3" style="height:35px; vertical-align:bottom; text-decoration:underline; ">
                                                                                    <span class="cls_label_header">Please select date range for this package</span> <span class="cls_label_mark">*</span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" align="left" style="white-space: nowrap" colspan="3">
                                                                                    <customToolkit:wuc_txtCalendarRange 
                                                                                        ID="txtDate" 
                                                                                        runat="server" 
                                                                                        RequiredValidation="true"
                                                                                        RequiredValidationGroup="Save" 
                                                                                        DateFormatString="yyyy-MM-dd" 
                                                                                        CompareDateRangeValidation="true" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel>   
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                                                                               
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_PackagePrdList ID="wuc_PackagePrdList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel3" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_PackageFOCList ID="wuc_PackageFOCList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="TabPanel4" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_PackageCustList ID="wuc_PackageCustList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                                       
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table> 
        
        <%--<customToolkit:wuc_SalesteamAssignPop ID="wuc_SalesteamAssignPop" Title="Salesteam Assignment" runat="server" />--%>
    </form>
</body>
</html>
