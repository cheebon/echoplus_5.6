Imports System.Data


Partial Class ActSalesBySalesrep
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Dim aryDataItem As New ArrayList
    Dim aryDataItem_HDR As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_ActSalesBySalesrep"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try

            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.ACTYSALESBYINDSR) '"Activity & Sales - Individual Sales Rep."
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.ACTYSALESBYINDSR
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                If PreviousPage IsNot Nothing Then
                    With CriteriaCollector
                        Session.Item("Year") = .Year
                        Session.Item("Month") = .Month
                        session("PRINCIPAL_CODE") = .PrincipalCode
                        session("TEAM_CODE") = .TeamCode
                        session("SALESREP_CODE") = .SalesrepCode
                        session("PRINCIPAL_ID") = .PrincipalID
                        ViewState("strSortExpression") = .SortExpression
                    End With
                Else
                    If String.IsNullOrEmpty(session("SALESREP_CODE")) Then wuc_Menu.TreeMenu_Visibility = True 'ActivateSearchBtn_Click(Nothing, Nothing)
                    CriteriaCollector = Nothing
                End If
                wuc_Menu.RestoreSessionState = True
                TimerControl1.Enabled = True
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "Event Handler"
    Public ReadOnly Property PageName() As String
        Get
            Return "ActSalesBySalesrep.aspx"
        End Get
    End Property

    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            'wuc_ctrlpanel.RefreshDetails()
            'wuc_ctrlpanel.UpdateControlPanel()
            If IsPostBack Then RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            Dim blnAllowSorting_Hdr As Boolean = dgList_Hdr.AllowSorting
            Dim blnAllowPaging_Hdr As Boolean = dgList_Hdr.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList_Hdr)
            aryDgList.Add(dgList)

            wuc_ctrlpanel.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting

            dgList_Hdr.AllowPaging = blnAllowPaging_Hdr
            dgList_Hdr.AllowSorting = blnAllowSorting_Hdr


            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    'Private Sub PrepareGridViewForExport(ByRef gvList As GridView)
    '    Dim lb As LinkButton = New LinkButton
    '    Dim l As Literal = New Literal
    '    Dim name As String = String.Empty
    '    Dim i As Integer = 0
    '    While i < gvList.Controls.Count
    '        If gvList.Controls(i).GetType Is GetType(LinkButton) Then
    '            l.Text = (CType(gvList.Controls(i), LinkButton)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(DropDownList) Then
    '            l.Text = (CType(gvList.Controls(i), DropDownList)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(CheckBox) Then
    '            l.Text = (CType(gvList.Controls(i), CheckBox)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        End If

    '        'If gvList.Controls(i).HasControls Then
    '        '    PrepareGridViewForExport(gvList.Controls(i))
    '        'End If
    '        System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
    '    End While
    'End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        lblMessage.Visible = String.IsNullOrEmpty(session("SALESREP_CODE"))

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim dt_Hdr As DataTable = CType(ViewState("dt_Hdr"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing

                dt_Hdr = GetRecList_HDR()
                ViewState("dt_Hdr") = dt_Hdr
            End If

            If dt_Hdr Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode_HDR(dt_Hdr)
                If dt_Hdr.Rows.Count = 0 Then dt_Hdr.Rows.Add(dt_Hdr.NewRow())
            End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList_Hdr.DataSource = dt_Hdr
            dgList_Hdr.DataBind()

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 16 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_ACTSALES_SALESREP.GetFieldColumnType(ColumnName, CF_ACTSALES_SALESREP.GridType.DTL)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        dgColumn.ItemStyle.HorizontalAlign = CF_ACTSALES_SALESREP.ColumnStyle(ColumnName).HorizontalAlign
                        If String.IsNullOrEmpty(CF_ACTSALES_SALESREP.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_ACTSALES_SALESREP.GetOutputFormatString(ColumnName)
                        End If

                        dgColumn.HeaderText = CF_ACTSALES_SALESREP.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        'dgColumn.NavigateUrl= "TEAM_CODE" 'ColumnName '& session("TEAM_CODE") '"SalesInfoByDate.aspx"
                        Dim strUrlFields(1) As String
                        Dim strUrlFormatString As String = CF_ACTSALES_SALESREP.GetUrlFormatString(ColumnName) '"ActSalesByDailyDtl.aspx?DATE={0:yyyy-MM-dd}&SALESMAN={1}"
                        strUrlFields(0) = "DATE"
                        strUrlFields(1) = "SALESREP_CODE"
                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_ACTSALES_SALESREP.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataFormatString = CF_ACTSALES_SALESREP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_ACTSALES_SALESREP.ColumnStyle(ColumnName).HorizontalAlign

                        If ColumnName = "DATE" Then dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                        dgColumn.HeaderText = CF_ACTSALES_SALESREP.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer
                    e.Row.Cells(0).Text = "MTD"
                    Dim iIndex As Integer
                    Dim intRowCount As Integer = dgList.Rows.Count

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_ACTSALES_SALESREP.GetOutputFormatString(li.Text), CDbl(li.Value))
                        End If
                    Next

                    e.Row.Cells(aryDataItem.IndexOf("CALL_ACH")).Text = String.Format(CF_ACTSALES_SALESREP.GetOutputFormatString("CALL_ACH"), (DIVISION(licItemFigureCollector.FindByText("TTL_CALL"), licItemFigureCollector.FindByText("TGT_CALL")) * 100))

            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim intNetValue As Integer
            Dim strYear, strMonth, strUserID, strSalesRepCode, strPrincipalId, strPrincipalCode As String
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            strSalesRepCode = session("SALESREP_CODE")
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .Year = strYear
                .Month = strMonth
                .PrincipalID = strPrincipalId
                .PrincipalCode = strPrincipalCode
                .SalesrepCode = strSalesRepCode
                .NetValue = intNetValue
            End With

            Dim clsKpiDB As New rpt_KPI.clsActivitySales
            DT = clsKpiDB.GetActSalesByDate(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strSalesRepCode, intNetValue)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'SALES, TTL_CALL, TGT_CALL, CALL_ACH, FULL_CALL
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper

                If (strColumnName Like "*SALES" _
                OrElse strColumnName Like "*_CALL" _
                OrElse strColumnName Like "*_AMT") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Custom dgList_Hdr"
    'Private Sub CustomHeaderRender(ByVal HTMLWriter As HtmlTextWriter, ByVal ctl As Control)
    '    'intGrpItemCount
    '    'FIXED_ITEM_COUNT
    '    Dim iMTD, iQTD, iYTD, iPYTD As Integer

    '    For iIndex As Integer = intGrpItemCount To intTotalColumnCount - 1
    '        Try
    '            If aryDataItem(iIndex) Like "MTD*" Then
    '                iMTD += 1
    '            ElseIf aryDataItem(iIndex) Like "QTD*" Then
    '                iQTD += 1
    '            ElseIf aryDataItem(iIndex) Like "YTD*" Then
    '                If aryDataItem(iIndex) Like "YTDGROSS" Then
    '                    iPYTD += 1
    '                Else
    '                    iYTD += 1
    '                End If
    '            ElseIf aryDataItem(iIndex) Like "PYTD*" Then
    '                iPYTD += 1
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    Next
    '    '<th  valign='top' scope='col' >
    '    HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & intGrpItemCount & "'>&nbsp;</th>")
    '    If iMTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iMTD & "' align='center'>MTD Sales</th>")
    '    If iQTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iQTD & "' align='center'>QTD Sales</th>")
    '    If iYTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iYTD & "' align='center'>YTD Sales</th>")
    '    If iPYTD > 0 Then HTMLWriter.Write("<th  valign='top' scope='col'  border='0' colspan='" & iPYTD & "' align='center'>PYTD Sales</th>")
    '    HTMLWriter.Write("</TR>") 'Close the row




    '    'HTMLWriter.Write("<TD border='0' colspan='" & intGrpItemCount & "'></TD>")
    '    'If iMTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iMTD & "' align='center'>MTD Sales</TD>")
    '    'If iQTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iQTD & "' align='center'>QTD Sales</TD>")
    '    'If iYTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iYTD & "' align='center'>YTD Sales</TD>")
    '    'If iPYTD > 0 Then HTMLWriter.Write("<TD border='0' colspan='" & iPYTD & "' align='center'>PYTD Sales</TD>")
    '    'HTMLWriter.Write("</TR>") 'Close the row

    '    dgList.HeaderStyle.AddAttributesToRender(HTMLWriter)
    '    HTMLWriter.RenderBeginTag("TR") 'Reopen for the next row, don't need to close as the renderer will close it

    '    Dim i As Integer
    '    For i = 0 To ctl.Controls.Count - 1
    '        ctl.Controls(i).RenderControl(HTMLWriter)
    '    Next i

    'End Sub

    Protected Sub dgList_Hdr_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList_Hdr.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_ACTSALES_SALESREP.GetFieldColumnType(ColumnName, CF_ACTSALES_SALESREP.GridType.HDR)

                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_ACTSALES_SALESREP.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                            dgColumn.DataFormatString = CF_ACTSALES_SALESREP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        Else
                            'dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                        End If

                        dgColumn.HeaderText = CF_ACTSALES_SALESREP.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        'dgColumn.SortExpression = ColumnName
                        dgList_Hdr.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem_HDR.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Hdr_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Hdr_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList_Hdr.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList_Hdr 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList_HDR() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim intNetValue As Integer
            Dim strYear, strMonth, strUserID, strRegionCode, strSalesRepCode, strPrincipalId, strPrincipalCode As String
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")
            strUserID = Session.Item("UserID")
            strPrincipalId = session("PRINCIPAL_ID")
            strPrincipalCode = session("PRINCIPAL_CODE")
            strRegionCode = Session("REGION_CODE")
            strSalesRepCode = session("SALESREP_CODE")
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .Year = strYear
                .Month = strMonth
                .PrincipalID = strPrincipalId
                .PrincipalCode = strPrincipalCode
                .RegionCode = strRegionCode
                .SalesrepCode = strSalesRepCode
                .NetValue = intNetValue
            End With

            Dim clsKpiDB As New rpt_KPI.clsActivitySales
            DT = clsKpiDB.GetActSalesBySalesRep(strUserID, strPrincipalId, strPrincipalCode, strYear, strMonth, strRegionCode, strSalesRepCode, intNetValue)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode_HDR(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode_HDR(ByRef DT As DataTable)
        Try
            aryDataItem_HDR.Clear()
            dgList_Hdr_Init(DT)
            Cal_CustomerHeader()
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem_HDR will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            'collect the column name that ready to show on the screen
            'YEAR, Month, TEAM, SALESMAN, SMNAME, 
            'MTD_ACT_CALL, MTD_ACT_TGT, MTD_ACT_ACH, 
            'MTD_SALES_SALES, MTD_SALES_TGT, MTD_SALES_ACH, MTD_SALES_PMTD, MTD_SALES_PMTD_ACH, 
            'YTD_ACT_CALL, YTD_ACT_TGT, YTD_ACT_ACH, 
            'YTD_SALES_SALES, YTD_SALES_TGT, YTD_SALES_ACH, YTD_SALES_PYTD, YTD_SALES_PYTD_ACH, 
            'TTL_SALES_TGT, TTL_SALES_ACH

            For Each strColumnName In aryDataItem_HDR
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "TEAM_CODE" Then
                    strColumnName = "Sales Team"
                ElseIf strColumnName Like "SALESREP_NAME" Then
                    strColumnName = "Sales Rep."
                ElseIf strColumnName Like "MTD_DAY_*" Then
                    strColumnName = "Working Day"
                ElseIf strColumnName Like "MTD_ACT_*" Then
                    strColumnName = "MTD Activity"
                ElseIf strColumnName Like "MTD_SALES_*" Then
                    strColumnName = "MTD Sales"
                ElseIf strColumnName Like "YTD_ACT_*" Then
                    strColumnName = "YTD Activity"
                ElseIf strColumnName Like "YTD_SALES_*" Then
                    strColumnName = "YTD Sales"
                ElseIf strColumnName Like "TTL_SALES_*" Then
                    strColumnName = "Full Year"
                Else
                    strColumnName = "UKNOWN"
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class


Public Class CF_ACTSALES_SALESREP
    Enum GridType
        HDR = 1
        DTL = 2
    End Enum

    Public Shared Function FilterPrefixName(ByVal columnName As String) As String
        'YEAR ,MONTH ,SALESREP_CODE ,
        'SALESREP_NAME ,
        'MTD_DAY_WORK ,MTD_DAY_ACT ,MTD_DAY_LEFT ,
        'MTD_ACT_CALL ,MTD_ACT_TGT ,MTD_ACT_ACH ,
        'MTD_SALES_SALES ,MTD_SALES_TGT ,MTD_SALES_ACH ,MTD_SALES_PMTD ,MTD_SALES_PMTD_ACH ,
        'YTD_ACT_CALL ,YTD_ACT_TGT ,YTD_ACT_ACH ,
        'YTD_SALES_SALES ,YTD_SALES_TGT ,YTD_SALES_ACH ,YTD_SALES_PYTD ,YTD_SALES_PYTD_ACH ,
        'TTL_SALES_TGT ,TTL_SALES_ACH
        Dim strName As String = ""
        Try
            If Not String.IsNullOrEmpty(columnName) Then
                strName = columnName.ToUpper.Replace("MTD_DAY_", "DAY_").Replace("MTD_ACT_", "").Replace("MTD_SALES_", "").Replace("YTD_ACT_", "").Replace("YTD_SALES_", "").Replace("TTL_SALES_", "")
            End If
        Catch ex As Exception
        End Try
        Return strName
    End Function

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strNewName As String = FilterPrefixName(ColumnName.ToUpper)

        Select Case strNewName
            Case "RET_AMT"
                strFieldName = "Trade Return"
            Case "DAY_WORK"
                strFieldName = "WD"
            Case "DAY_ACT"
                strFieldName = "CD"
            Case "DAY_ACT_HALF"
                strFieldName = "CD"
            Case "DAY_LEFT"
                strFieldName = "Day Left"
            Case "NET_SALES"
                strFieldName = "Net Sales"
            Case "SALES"
                strFieldName = "Sales"
            Case "TTL_CALL"
                strFieldName = "Call"
            Case "TGT_CALL"
                strFieldName = "Target Call"
            Case "CALL_ACH"
                strFieldName = " Call % "
            Case "FULL_CALL"
                strFieldName = "Effective Call"
            Case "CALL"
                strFieldName = "Call"
            Case "TGT"
                strFieldName = "Tgt"
            Case "PMTD"
                strFieldName = "PM"
            Case "PYTD"
                strFieldName = "PY"
            Case "PMTD_ACH"
                strFieldName = "PM%"
            Case "PYTD_ACH"
                strFieldName = "PY%"
            Case "ACH", "PMTD_ACH", "PYTD_ACH"
                strFieldName = "%"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, ByVal type As GridType) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Select Case strColumnName
                Case "NET_SALES"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESSUMM, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "TTL_CALL"
                    If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CABYCUST, "'1','8'") Then
                        FCT = FieldColumntype.HyperlinkColumn
                    End If

                Case "YEAR", "MONTH"
                    FCT = FieldColumntype.InvisibleColumn
            End Select

            If strColumnName Like "*_CODE" And type = GridType.DTL Then FCT = FieldColumntype.InvisibleColumn

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)
        Try
            Select Case strNewName
                Case "NET_SALES", "SALES", "RET_AMT", "PMTD", "PYTD"
                    strStringFormat = "{0:#,0.00}"
                Case "DAY_WORK", "DAY_ACT", "DAY_LEFT", "TTL_CALL", "TGT_CALL", "FULL_CALL", "CALL", "TGT"
                    strStringFormat = "{0:0.#}"
                Case "CALL_ACH", "ACH", "PMTD_ACH", "PYTD_ACH", "DAY_ACT_HALF"
                    strStringFormat = "{0:0.0}"
                Case "DATE"
                    strStringFormat = "{0:yyyy-MM-dd}"
                Case Else
                    strStringFormat = ""
            End Select

            If strColumnName Like "*_SALES_TGT" Then strStringFormat = "{0:#,0.00}"
        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function GetUrlFormatString(ByVal strColumnName As String) As String
        Dim strUrlFormatString As String = String.Empty

        Try
            Select Case strColumnName.ToUpper
                Case "SALES", "NET_SALES" 'PAGE_INDICATOR=CALLBYDAY
                    strUrlFormatString = "~/iFFMR/Sales/SalesSummList.aspx?SELECTED_DATE={0:yyyy-MM-dd}&SALESREP_CODE={1}&PAGE_INDICATOR=ACTYSALESBYREP"
                Case "TTL_CALL"
                    ' strUrlFormatString = "~/iFFMR/KPI/ActSalesByDailyDtl.aspx?DATE={0:yyyy-MM-dd}&SALESREP_CODE={1}"
                    strUrlFormatString = "~/iFFMR/SFE/CallAnalysis/CallAnalysisListByCustomer.aspx?DAY={0:dd}&MONTH={0:MM}&SALESREP_CODE={1}&PAGE_INDICATOR=SALESACTY"
            End Select
        Catch ex As Exception
        End Try
        Return strUrlFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

    'Public Shared Function GetHyperLinkInfo(ByVal strColumnName As String) As System.Web.UI.WebControls.HyperLinkField
    '    Dim hlfLink As New HyperLinkField
    '    Dim strUrlFormatString As String = String.Empty
    '    Dim strUrlFields(1) As String
    '    strUrlFields(0) = "DATE"
    '    strUrlFields(1) = "SALESREP_CODE"

    '    Try
    '        Select Case strColumnName.ToUpper
    '            Case "SALES"
    '            Case "TTL_CALL"
    '            Case Else
    '                '                        Dim strUrlFormatString As String = 
    '                '"ActSalesByDailyDtl.aspx?DATE={0:yyyy-MM-dd}&SALESMAN={1}"
    '                '                        dgColumn.DataNavigateUrlFields = strUrlFields
    '                '                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString

    '        End Select
    '    Catch ex As Exception

    '    End Try
    'End Function
End Class









