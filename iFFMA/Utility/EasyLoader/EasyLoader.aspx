﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EasyLoader.aspx.vb" Inherits="iFFMA_Utility_EasyLoader_EasyLoader" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Easy Loader</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmEasyLoader" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr>
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                    
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0" width="98%" border="0" style="text-align:left; padding-left:15px; padding-bottom:10px;">
                                                <tr>
                                                    <td style="width: 10%;"></td>
                                                    <td style="width: 90%"></td>
                                                </tr>
                                                
                                                <asp:Panel ID="pnlValidate" runat="server">
                                                <tr>
                                                    <td class="cls_label_header">Upload Type :</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlUploadType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="true" />
                                                         <asp:button ID="btndltemplate" runat="server" Text="Download Template" CssClass="cls_linkbutton" ValidationGroup="Fileupload"  ></asp:button>
                                                        <asp:RequiredFieldValidator ID="rfvUploadType" runat="server" ErrorMessage="Please select upload type" ControlToValidate="ddlUploadType" ValidationGroup="Fileupload" Display="Dynamic" CssClass="cls_validator" />
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td class="cls_label_header">File Location :</td>
                                                    <td>
                                                        <asp:FileUpload ID="fuLocation" runat="server" CssClass="cls_textbox" Width="500px" Height="20px" />
                                                        <%--<asp:RequiredFieldValidator ID="rfvfuprd" runat="server" ErrorMessage="Please browse a File" ControlToValidate="fuLocation" ValidationGroup="Fileupload" Display="Dynamic" CssClass="cls_validator" />--%>
                                                        <br />
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="cls_validator" />
                                                    </td>
                                                </tr>
                                                
                                                <asp:Panel ID="pnlDesc" runat="server" Visible="false">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <span class="cls_label_header" style="text-decoration:underline;">Tips & Instructions:</span>
                                                        <br /><asp:Label ID="lblDesc" runat="server" CssClass="cls_label_header" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <span class="cls_label_header">Please upload CSV file with fields in the following order sequence and separated by commas:</span>
                                                        <asp:Label ID="lblColumns" runat="server" CssClass="cls_label" />
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                
                                                <asp:Panel ID="pnlSystemError" runat="server" Visible="false">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <span class="cls_label_err">There is some system error for the selected upload type, please contact administrator.</span>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                
                                                <tr>
                                                    <td colspan="2">
                                                        <br /><asp:Button ID="btnValidate" runat="server" Text="Validate" CssClass="cls_button" ValidationGroup="Fileupload" />
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                             
                                                <asp:Panel ID="pnlUpload" runat="server" Visible="false">
                                                <tr>
                                                    <td colspan="2"><asp:Label ID="lblUploadInfo" runat="server" CssClass="cls_label_header" /></td>
                                                </tr>
                                                
                                                <asp:Panel ID="pnlFileError" runat="server" Visible="false">
                                                <tr>
                                                    <td colspan="2">
                                                        <span class="cls_label_err">The number of fields in the file or the column name is not matched, please check again.</span>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                
                                                <tr>
                                                    <td colspan="2">
                                                        <br /><asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="cls_button" />
                                                        <asp:Button ID="btnBack" runat="server" Text="Back to Validation Page" CssClass="cls_button" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"> <div style="width: 100%;">
                                                            <asp:Label ID="lblErrMsgValidate" runat="server" CssClass="cls_label_err"   /></div></td>
                                                </tr>
                                                <tr style="padding-left:0px;">
                                                    <td width="100%" colspan="2" align="center">
                                                        <div style="width: 100%; padding: 2px;"><asp:Label ID="lblTtlRecords" runat="server" CssClass="cls_button" Visible="false"></asp:Label></div>
                                                        <div style="width: 100%; padding: 2px;">
                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                                                                <Columns>
                                                                </Columns>
                                                            </ccGV:clsGridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </asp:Panel>
                                                
                                            </table>
                                        </ContentTemplate>
                                        
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnValidate" />
                                            <asp:PostBackTrigger ControlID="btndltemplate"   />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

