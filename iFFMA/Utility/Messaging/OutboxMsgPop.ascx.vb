Imports System.Data

Partial Class iFFMA_Utility_Messaging_OutboxMsgPop
    Inherits System.Web.UI.UserControl

    Public Event SendButton_Click As EventHandler
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property MsgOutboxID() As String
        Get
            Return Trim(hdID.Value)
        End Get
        Set(ByVal value As String)
            hdID.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub
#End Region

#Region "DDL"
    Private Sub LoadDDLTeam()
        Dim dtTeam As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            dtTeam = clsCommon.GetTeamDDL
            With ddlTeam
                .Items.Clear()
                .DataSource = dtTeam.DefaultView
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "LISTBOX"
    Private Sub BindLsb()
        Dim dtSalesrep As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        lsbSalesrep.Items.Clear()
        lsbSelected.Items.Clear()

        Try
            dtSalesrep = clsCommon.GetSalesrepDDL(ddlTeam.SelectedValue)
            lsbSalesrep.DataSource = dtSalesrep.DefaultView
            lsbSalesrep.DataTextField = "SALESREP_NAME"
            lsbSalesrep.DataValueField = "SALESREP_CODE"
            lsbSalesrep.DataBind()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetFilterString(ByRef drRows() As DataRow, ByVal strColumnName As String) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList

        For Each ROW As Data.DataRow In drRows
            If aryList.IndexOf(Trim(ROW(strColumnName))) < 0 Then
                sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(ROW(strColumnName)) & "'")
                aryList.Add(Trim(ROW(strColumnName)))
            End If
        Next
        Return sbString.ToString
    End Function

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelected)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelected, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelected)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelected.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelected, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next

                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False
    End Sub

    Public Sub LoadEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        LoadDDLTeam()
        lsbSalesrep.Items.Clear()
        lsbSelected.Items.Clear()

    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim clsMessaging As New mst_Utility.clsMessaging

            DT = clsMessaging.GetMsgOutboxDetails(MsgOutboxID)
            If DT.Rows.Count > 0 Then
                lblDate.Text = String.Format("{0:yyyy-MM-dd h:mm tt}", IIf(IsDBNull(DT.Rows(0)("CREATED_DATE")), "", DT.Rows(0)("CREATED_DATE")))
                lblFrom.Text = DT.Rows(0)("CREATOR_USER_NAME")
                lblRecipients.Text = DT.Rows(0)("RECIPIENTS")
                lblSubject.Text = DT.Rows(0)("SUBJECT")
                lblMsgContent.Text = DT.Rows(0)("CONTENT")

                txtSubject.Text = DT.Rows(0)("SUBJECT")
                txtMsgContent.Text = DT.Rows(0)("CONTENT")
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ResetPage()
        lblDate.Text = ""
        lblFrom.Text = ""
        lblRecipients.Text = ""
        lblSubject.Text = ""
        lblMsgContent.Text = ""

        ddlTeam.SelectedIndex = 0
        txtSubject.Text = ""
        txtMsgContent.Text = ""
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.OUTBOX, SubModuleAction.Create) Then
                    btnForward.Visible = False
                End If
            Else
                lblInfo.Text = ""

                'Dim strJavaScript As New System.Text.StringBuilder
                'strJavaScript.AppendLine("<script>")
                'strJavaScript.AppendLine("function isMaxLength(txtBox) {")
                'strJavaScript.AppendLine("  if(txtBox) { ")
                'strJavaScript.AppendLine("      return ( txtBox.value.length <= 250);")
                'strJavaScript.AppendLine("  }")
                'strJavaScript.AppendLine("} ")
                'strJavaScript.AppendLine("</" + "script>")

                Me.txtMsgContent.Attributes.Add("onkeypress", "return isMaxLength(this);")
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "txtLength", strJavaScript.ToString, True)

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnForward_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnForward.Click
        LoadEditMode()
        Show()
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    If MsgOutboxID <> "" Then
    '        LoadViewMode()
    '        Show()
    '    End If
    'End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            If lsbSelected.Items.Count = 0 Then
                lblInfo.Text = "Recipient(s) is Required."
                Show()
                Exit Sub
            End If

            Dim clsMessaging As New mst_Utility.clsMessaging
            Dim strSalesrepList As String

            strSalesrepList = GetItemsInString(lsbSelected)
            clsMessaging.CreateMsgOutbox(Trim(txtSubject.Text), Trim(txtMsgContent.Text), strSalesrepList)

            lblMsgPop.Message = ""
            lblMsgPop.Show()

            RaiseEvent SendButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged
        BindLsb()
        Show()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

