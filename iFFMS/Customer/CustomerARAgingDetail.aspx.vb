Imports System.Data

Partial Class CustomerARAgingDetail
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerARAging"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here


        'Call Header
        With wuc_lblheader
            .Title = "AR Aging Detail"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If

    End Sub
    Sub onpageload()
        Try
            Dim strCustCode, strSalesmanCode, strDocNo As String
            strDocNo = Request.QueryString("txnno")
            strSalesmanCode = Session("SALESREP_CODE")
            strCustCode = Session("customercode")
            bindGridView(strSalesmanCode, strCustCode, strDocNo)
            lblTxnNo.Text = IIf(String.IsNullOrEmpty(strDocNo), "Not Available", strDocNo)

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            onpageload()
            TimerControl1.Enabled = False
        End If

    End Sub


    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strTxnNo As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getOpenItemDetailDT(strSalesmanCode, strCustCode, strTxnNo)
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
                'lblTxnNo.Text = iif(isdbnull(dt.Rows(0)("txn_no")), "Not Available", dt.Rows(0)("txn_no"))
                'lblTxnNo.Text = IIf(String.IsNullOrEmpty(lblTxnNo.Text), "Not Available", lblTxnNo.Text)

                lblAmount.Text = IIf(IsDBNull(dt.Rows(0)("ord_amt")), "Not Available", dt.Rows(0)("ord_amt"))
                lblAmount.Text = IIf(String.IsNullOrEmpty(lblAmount.Text), "Not Available", lblAmount.Text)

                lblRemark.Text = IIf(IsDBNull(dt.Rows(0)("remarks")), "Not Available", dt.Rows(0)("remarks"))
                lblRemark.Text = IIf(String.IsNullOrEmpty(lblRemark.Text), "Not Available", lblRemark.Text)
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                gridview1.DataSource = dtnew
                gridview1.DataBind()
                lblAmount.Text = "Not Available"
                lblRemark.Text = "Not Available"
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../customer/customeraragingheader.aspx?agingid=" + Session("agingid"), False)
    End Sub
End Class



