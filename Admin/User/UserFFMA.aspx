<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserFFMA.aspx.vb" Inherits="Admin_User_UserFFMA" %>

<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User FFMA</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmUserFFMA" method="post" runat="server">
        <table cellspacing="0" cellpadding="0" width="98%" border="0">
		    <tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td></tr>
                        <tr>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport"><td>&nbsp;</td></tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
					                            <tr>
							                        <td><span class="cls_label_header">Login</span></td>
							                        <td><span class="cls_label_header">:</span></td>
							                        <td colspan="3"><asp:label id="lblLogin" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
					                            <tr>
							                        <td><span class="cls_label_header">User Code</span></td>
							                        <td><span class="cls_label_header">:</span></td>
							                        <td colspan="3"><asp:label id="lblUserCode" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
                                                    <td valign="top"><span class="cls_label_header">Team</span></td>
                                                    <td valign="top"><span class="cls_label_header">:</span></td>
                                                    <td style="width: 166px"><asp:listbox id="lsbTeam" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></td>
                                                    <td>
                                                        <table width="100%" border="0">
                                                            <tr><td align="center"><asp:LinkButton ID="lnkAddTeam" runat="server" CausesValidation="false">></asp:LinkButton></td></tr>
                                                            <tr><td align="center"><asp:LinkButton ID="lnkRemoveTeam" runat="server" CausesValidation="false"><</asp:LinkButton></td></tr>
                                                            <tr><td align="center"><asp:LinkButton ID="lnkAddAllTeam" runat="server" CausesValidation="false">>></asp:LinkButton></td></tr>
                                                            <tr><td align="center"><asp:LinkButton ID="lnkRemoveAllTeam" runat="server" CausesValidation="false"><<</asp:LinkButton></td></tr>
                                                        </table>
                                                    </td>
                                                    <td><asp:listbox id="lsbSelectedTeam" runat="server" CssClass="cls_listbox" Width="160px" Height="205px" SelectionMode="Multiple"></asp:listbox></td>
                                               </tr>									
					                        </table>
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
    				                        <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
				                            <asp:TextBox ID="txtType" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
				                            <asp:TextBox ID="txtUserID" CssClass="cls_textbox" runat="server" Visible="false"></asp:TextBox>
				                            <asp:button id="btnViewTray" runat="server" Text="View Tray" CssClass="cls_button" CausesValidation="False"></asp:button>
					                        <asp:button id="btnSave" runat="server" CssClass="cls_button" Text="Save"></asp:button>
				                        </td>
			                        </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</table> 	
	</form>
</body>
</html>
