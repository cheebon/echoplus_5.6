'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Principal User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlPrincipal
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Private strCountryCode As String
    Private strUserCode As String
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlPrincipalID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlPrincipalID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlPrincipalID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlPrincipalID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlPrincipalID.SelectedValue = "0"
    '        Else
    '            ddlPrincipalID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlPrincipalID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return ddlPrincipalID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlPrincipalID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlPrincipalID.SelectedValue = "0"
            Else
                ddlPrincipalID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlPrincipalID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlPrincipalID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlPrincipalID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlPrincipalID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlPrincipalID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlPrincipalID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlPrincipalID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlPrincipalID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlPrincipalID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvPrincipalID.Visible 'cfvPrincipalID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvPrincipalID.Enabled = Value
            cfvPrincipalID.Visible = Value
        End Set
    End Property

    Public Property CountryCode() As String
        Get
            Return strCountryCode
        End Get
        Set(ByVal Value As String)
            strCountryCode = Value
        End Set
    End Property

    Public Property UserCode() As String
        Get
            Return strUserCode
        End Get
        Set(ByVal Value As String)
            strUserCode = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            'If ddlPrincipalID.Items.Count <= 0 Then
            'Get Principal List
            objCommonQuery = New cor_Common.clsCommonQuery
            With objCommonQuery
                .clsProperties.CountryCode = strCountryCode
                .clsProperties.UserCode = strUserCode
                dt = .GetPrincipalList
            End With
            objCommonQuery = Nothing

            ddlPrincipalID.Items.Clear()
            ddlPrincipalID.Items.Add(New ListItem("Select", 0))
            If dt.Rows.Count > 0 Then
                For Each drRow In dt.Rows
                    ddlPrincipalID.Items.Add(New ListItem(Trim(drRow("principal_name")), Trim(drRow("principal_id")) & "@" & Trim(drRow("principal_code"))))
                Next
                ddlPrincipalID.SelectedIndex = 1
            End If
            'End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlPrincipal.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


