﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustomerTeamClassPop.ascx.vb" Inherits="iFFMA_SalesAccount_CustomerTeamClassPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustContSearch" Src="CustSearchPop.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 500px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px; text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 92%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: right; width: 8%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; /*width: 98%*/">
                <fieldset style="padding-left: 10px; width: 100%; box-sizing: border-box;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />

                    <table cellspacing="1" cellpadding="2" rules="all" border="0" style="border-width: 0px; border-style: None; width: 100%;">
                        <tbody runat="server" id="pnlInsert">
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Customer
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtCust" runat="server" CssClass="cls_textbox" Enabled="False" Width="200px"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                                <td align="middle" rowspan="2" colspan="3" runat="server" visible ="False" id="searchCustContBtn">
                                    <asp:Button ID="btnSearchCustCont" runat="server" CssClass="cls_button" Text="Search" OnClick="btnSearchCustCont_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Contact
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtCont" runat="server" CssClass="cls_textbox" Enabled="False" Width="200px"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Type</td>
                                <td colspan="2">
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtType" runat="server" CssClass="cls_textbox" Width="200px"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Team Class</td>
                                <td colspan="2">
                                    <span class="cls_label_header">:</span>
                                    <asp:DropDownList ID="ddlTeamClass" runat="server" Width="200px" CssClass="cls_dropdownlist">
                                        <asp:ListItem Text="-- SELECT --" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                        <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                        <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                        <asp:ListItem Text="OTH" Value="OTH"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                        <center>
                            <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" />
                        </center>
                    </span>
                    <!-- End Customized Content -->

                    <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdContCode" runat="server" Value="" />

                    <asp:HiddenField ID="hdCustomer" runat="server" Value="" />
                    <asp:HiddenField ID="hdContact" runat="server" Value="" />
                    <asp:HiddenField ID="hdType" runat="server" Value="" />
                    <asp:HiddenField ID="hdTeamClass" runat="server" Value="" />

                    <asp:HiddenField ID="hdIsEdit" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server"
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden"
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop"
            BackgroundCssClass="modalBackground"
            DropShadow="True"
            RepositionMode="RepositionOnWindowResizeAndScroll" />

        <span style="float: left;">
            <customToolkit:wuc_CustContSearch ID="wuc_CustContSearch" Title="Customer Search" runat="server" />
        </span>
    </ContentTemplate>
</asp:UpdatePanel>
