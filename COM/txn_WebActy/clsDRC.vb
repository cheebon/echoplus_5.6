'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsDRC
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDRC"
        End Get
    End Property


#Region "TXN_NO"

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_DRC_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region
#Region "DDL/GRID"
    Public Function GetPrdGrpcode(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_DRC_PRDGRPCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetPrdcode(ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strPrdGrpCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_DRC_PRDCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

#Region "DRC Header"
    Public Sub InstTMPDRCHeader(ByVal strSessionId As String, ByVal strTxnNo As String, _
    ByVal strVisitId As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
      ByVal strUserId As String, ByVal strTxnDate As String)

        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_HDR_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBDouble)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing

    End Sub
#End Region

#Region "DRC Detail"

    Public Sub InstTMPDRCDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal dblOpenBalQty As String, _
        ByVal dblShelfQty As String, ByVal dblStoreQty As String, ByVal dblPOQty As String, ByVal dblICOQty As String, ByVal dblAMSQty As String, ByVal strUomCode As String, _
         ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_DTL_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("OPENBALQTY", dblOpenBalQty, clsDB.DataType.DBDouble)
            .addItem("SHELFQTY", dblShelfQty, clsDB.DataType.DBDouble)
            .addItem("STOREQTY", dblStoreQty, clsDB.DataType.DBDouble)
            .addItem("POQTY", dblPOQty, clsDB.DataType.DBDouble)
            .addItem("ICO", dblICOQty, clsDB.DataType.DBDouble)
            .addItem("AMS", dblAMSQty, clsDB.DataType.DBDouble)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelTMPDRCDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, ByVal strPrdCode As String, ByVal strLineNo As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_DTL_DELETE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub

    Public Sub UpdTMPDRCDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal dblOpenBalQty As String, _
       ByVal dblShelfQty As String, ByVal dblStoreQty As String, ByVal dblPOQty As String, ByVal dblICOQty As String, ByVal dblAMSQty As String, ByVal strUomCode As String, _
        ByVal strLineNo As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_DTL_UPDATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("OPENBALQTY", dblOpenBalQty, clsDB.DataType.DBDouble)
            .addItem("SHELFQTY", dblShelfQty, clsDB.DataType.DBDouble)
            .addItem("STOREQTY", dblStoreQty, clsDB.DataType.DBDouble)
            .addItem("POQTY", dblPOQty, clsDB.DataType.DBDouble)
            .addItem("ICO", dblICOQty, clsDB.DataType.DBDouble)
            .addItem("AMS", dblAMSQty, clsDB.DataType.DBDouble)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spUpdate()
        End With
        objDB = Nothing
    End Sub

#End Region

#Region "DRC SUMM LIST"

    Public Function GetDRCSumHdrs(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String _
, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_HDR"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetDRCSumDtl(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_DTL"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "DRC SUBMIT"

    Public Function InstDRCDetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strStatus As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_DRC_DTL_SUBMIT"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
