Imports System.Data
Imports System.Web.UI.WebControls

Partial Class Admin_User_NewUserDtl_UserDtl
    Inherits System.Web.UI.Page

    Dim dtPassLib As DataTable


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        cvPoorPass.ErrorMessage = Portal.Policy.GetPwdErrorMsg()
    End Sub


#Region "Enumeration"
    Enum Mode
        Normal = 0
        Edit = 1
        Create = 2
    End Enum
#End Region

#Region "private members"


#End Region

#Region "public property"

    Public Property sessionuser() As String
        Get
            Return ViewState("SessionUser")
        End Get
        Set(ByVal value As String)
            ViewState("SessionUser") = value
        End Set
    End Property
    Public ReadOnly Property SessionUserCode() As String
        Get
            Return Session("UserCode")
        End Get
    End Property
    Public Property CurrentMode() As Mode
        Get
            Return ViewState("CurrentMode")
        End Get
        Set(ByVal value As Mode)
            ViewState("CurrentMode") = value
        End Set
    End Property

    Public ReadOnly Property UserID() As String
        Get
            Return Session("UserID").ToString
        End Get
    End Property

    Public ReadOnly Property PrincipalID() As String
        Get
            Return Session("PRINCIPAL_ID").ToString
        End Get
    End Property



#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            With wuc_lblheader
                .Title = "User Details"
                .DataBind()
                .Visible = True
            End With

            'IsBSAdmin()

            If Not Page.IsPostBack Then
                AcceptIncomingRequest()
                'ObtainPermission()
                TogglePageMode()
                'Bind()
            End If
        Catch ex As Exception
            ExceptionMsg("UserDtl.Page_Load : " & ex.ToString)
        End Try
    End Sub


#Region "Load/Bind All Users Info methods"
    Private Sub Bind()
        Try
            '---------------
            Dim dt As DataTable = GetUserDtl(hfUserCode.Value)
            PrepareEditDDL(sessionuser, dt)
            BindUserDtl(dt)
            Wuc_ddlUserType_SelectedIndexChanged(Nothing, Nothing) 'call once so that SR only ddl's are on/off
            '---------------

            '---------------
            GetUserAccessright(hfUserCode.Value)
            '---------------

            '---------------
            GetSRList(hfUserCode.Value)
            PopulateSalesTeamDDL()
            '---------------

            '---------------
            GetUserSalesrepGrp(hfUserID.Value, sessionuser)
            '---------------

            '---------------

            If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.ONLINEGALLERY, SubModuleAction.View) Then
                GetUserSFMSCatAccessright(hfUserID.Value)
                SFMSCategory.Visible = True
            Else
                SFMSCategory.Visible = False
            End If

            If Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.View) Then
                GetUserELAccessright(hfUserID.Value)
                ELAccessRight.Visible = True
            Else
                ELAccessRight.Visible = False
            End If
            '---------------

            If GetEnableOptionInd() Then
                btnAgencyMappingEdit.Visible = True
                lblAgencyDesc.Visible = True
                lstAgencyMapping.Visible = True
                '---------------
                GetUserAgency(hfUserID.Value, sessionuser)
                '---------------
            Else
                btnAgencyMappingEdit.Visible = False
                lblAgencyDesc.Visible = False
                lstAgencyMapping.Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg("UserDtl.Bind : " & ex.ToString)
        End Try
    End Sub
    Private Sub PrepareEditDDL(ByVal SessionUserCode As String, ByVal dt As DataTable)
        'Call User Type
        With Wuc_ddlUserType
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Language
        With Wuc_ddlLanguage
            .RequiredValidation = True
            .AutoPostBack = False
            .DataBind()
            .Visible = True
        End With

        'Call Operation Type
        With Wuc_ddlOperationType
            .RequiredValidation = False
            .AutoPostBack = False
            .DataBind()
            .Visible = True
        End With

        'Call Country
        With Wuc_ddlCountry
            .UserCode = SessionUserCode
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Principal
        With Wuc_ddlPrincipal
            .CountryCode = ReadValue(dt.Rows(0)("country_code")) 'Wuc_ddlCountry.SelectedValue
            .UserCode = SessionUserCode
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Sales Team
        With Wuc_ddlSalesTeam
            .PrincipalCode = ReadValue(dt.Rows(0)("principal_code")) 'Wuc_ddlPrincipal.SelectedValue
            .RequiredValidation = False
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Sales Rep
        With Wuc_ddlSalesRep
            .SalesTeamCode = ReadValue(dt.Rows(0)("salesteam_code")) 'Wuc_ddlSalesTeam.SelectedValue
            .RequiredValidation = False
            .AutoPostBack = False
            .DataBind()
            .Visible = True
        End With
    End Sub
    Private Sub PrepareCreateDDL(ByVal SessionUserCode As String)
        Dim country() As String

        'Call User Type
        With Wuc_ddlUserType
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Language
        With Wuc_ddlLanguage
            .RequiredValidation = True
            .AutoPostBack = False
            .DataBind()
            .Visible = True
        End With

        'Call Operation Type
        With Wuc_ddlOperationType
            .RequiredValidation = False
            .AutoPostBack = False
            .DataBind()
            .Visible = True
        End With

        'Call Country
        With Wuc_ddlCountry
            .UserCode = SessionUserCode
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Principal
        With Wuc_ddlPrincipal
            country = Wuc_ddlCountry.SelectedValue.Split("@")
            .CountryCode = country(1) 'Wuc_ddlCountry.SelectedValue
            .UserCode = SessionUserCode
            .RequiredValidation = True
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Sales Team
        With Wuc_ddlSalesTeam
            .PrincipalCode = Wuc_ddlPrincipal.SelectedValue
            .RequiredValidation = False
            .AutoPostBack = True
            .DataBind()
            .Visible = True
        End With

        'Call Sales Rep
        With Wuc_ddlSalesRep
            .SalesTeamCode = Wuc_ddlSalesTeam.SelectedValue
            .RequiredValidation = False
            .AutoPostBack = False
            .DataBind()
            .Visible = True
        End With
    End Sub
    Private Function GetUserDtl(ByVal strUserCode As String) As DataTable
        Dim dt As DataTable
        Dim obj As New adm_User.clsUserQuery
        Try
            obj.clsProperties.user_code = strUserCode
            dt = obj.GetUserDtl()
            'BindUserDtl(dt)
            Return dt
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
            dt = Nothing
        End Try
    End Function
    Private Sub GetUserAccessright(ByVal strUserCode As String)
        Dim dt, dtAccessList As DataTable
        Dim obj As New adm_User.clsUserQuery
        Try
            obj.clsProperties.user_code = sessionuser
            dtAccessList = obj.GetAccessRightListByUserCode()

            obj.clsProperties.user_code = strUserCode
            dt = obj.GetUserAccessRightListByUserCode()
            BindUserAccessRight(dt, dtAccessList)
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Sub
    Private Sub GetUserELAccessright(ByVal strUserId As String)
        Dim dt, dtELAccessList As DataTable
        Dim clsELAccessRight As New adm_ELAccessRight.clsELAccessRight
        Try
            dt = clsELAccessRight.GetUserEasyLoaderAccessRight(strUserId)
            dtELAccessList = clsELAccessRight.GetEasyLoaderAccessrightList(strUserId)

            BindUserELAccessRight(dt, dtELAccessList)
        Catch ex As Exception
            Throw
        Finally
            clsELAccessRight = Nothing
        End Try
    End Sub
    Private Sub GetUserSFMSCatAccessright(ByVal strUserId As String)
        Dim dt, dtSFMSCatAccessList As DataTable
        Dim clsSFMSCatAccessRight As New adm_SFMSCatAccessRight.clsSFMSCatAccessRight
        Try
            dt = clsSFMSCatAccessRight.GetUserSFMSAccessRight(strUserId)
            dtSFMSCatAccessList = clsSFMSCatAccessRight.GetSFMSAccessrightList(strUserId)

            BindUserSFMSCatAccessRight(dt, dtSFMSCatAccessList)
        Catch ex As Exception
            Throw
        Finally
            clsSFMSCatAccessRight = Nothing
        End Try
    End Sub
    Private Sub GetUserSalesrepGrp(ByVal strUserID As String, ByVal strSessionUserCode As String)
        Dim dtSalesrepGrp, dtUserSalesrepGrp As DataTable
        Dim objSalesrepGrp As New adm_SalesrepGrp.clsSalesrepGrpQuery
        Dim objUserSalesrepGrp As New adm_User.clsUserQuery
        Try
            dtSalesrepGrp = objSalesrepGrp.SalesrepGrpSearch("ALL", "", strSessionUserCode)
            dtUserSalesrepGrp = objUserSalesrepGrp.GetUserSalesRepGrpListByUserID(strUserID)

            BindUserSalesrepGrp(dtSalesrepGrp, dtUserSalesrepGrp)


        Catch ex As Exception
            Throw
        Finally
            objSalesrepGrp = Nothing
            objUserSalesrepGrp = Nothing
        End Try
    End Sub
    Private Sub GetSRList(ByVal strUserCode As String)
        Dim dt As DataTable
        Dim obj As New adm_User.clsUserQuery
        Try
            obj.clsProperties.user_code = strUserCode
            dt = obj.GetUserSalesRepListByUserCode()
            BindUserSRList(dt)
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Sub
    Private Sub BindUserDtl(ByVal dt As DataTable)
        hfUserID.Value = ReadValue(dt.Rows(0)("user_id"))

        txtPasswordEdit.Text = ""
        txtPasswordConfirmEdit.Text = ""

        lblStatusValue.Text = IIf(ReadValue(dt.Rows(0)("active_flag")) = "1", "Active", "In-Active")
        lblLoginValue.Text = ReadValue(dt.Rows(0)("login"))
        lblUserNameValue.Text = ReadValue(dt.Rows(0)("user_name"))
        lblUserTypeValue.Text = ReadValue(dt.Rows(0)("user_type_name"))
        lblCountryValue.Text = ReadValue(dt.Rows(0)("country_name"))
        lblPrincipalValue.Text = ReadValue(dt.Rows(0)("principal_name"))
        lblSalesTeamValue.Text = ReadValue(dt.Rows(0)("salesteam_name"))
        lblSalesRepValue.Text = ReadValue(dt.Rows(0)("salesrep_code"))
        lblLanguageValue.Text = ReadValue(dt.Rows(0)("language_name"))
        lblOpTypeValue.Text = ReadValue(dt.Rows(0)("operation_type_name"))
        lblNewCustNoValue.Text = ReadValue(dt.Rows(0)("new_cust_no"))
        lblIDPrefixValue.Text = ReadValue(dt.Rows(0)("id_prefix"))
        lblPasswordExpiryValue.Text = ReadValue(dt.Rows(0)("pwd_expiry"))
        cbLoginExpiry.Checked = IIf(ReadValue(dt.Rows(0)("login_expiry_flag")) = "1", True, False)
        lblMaxRetryValue.Text = ReadValue(dt.Rows(0)("max_retry"))
        lblPageSizeValue.Text = ReadValue(dt.Rows(0)("page_size"))
        cbNetValue.Checked = IIf(ReadValue(dt.Rows(0)("nv_flag")) = "1", True, False)
        lblDefaultNetValueValue.Text = ReadValue(dt.Rows(0)("default_nv"))
        lblPwdExpiryRmdValue.Text = ReadValue(dt.Rows(0)("pwd_expiry_rmd"))

        ddlStatusEdit.SelectedValue = ReadValue(dt.Rows(0)("active_flag"))
        txtLoginEdit.Text = ReadValue(dt.Rows(0)("login"))
        txtUserNameEdit.Text = ReadValue(dt.Rows(0)("user_name"))
        Wuc_ddlUserType.SelectedValue = ReadValue(dt.Rows(0)("user_type_id")) & "@" & ReadValue(dt.Rows(0)("user_type_code"))
        Wuc_ddlCountry.SelectedValue = ReadValue(dt.Rows(0)("country_id")) & "@" & ReadValue(dt.Rows(0)("country_code"))
        Wuc_ddlPrincipal.SelectedValue = ReadValue(dt.Rows(0)("principal_id")) & "@" & ReadValue(dt.Rows(0)("principal_code"))
        If IsDBNull(dt.Rows(0)("salesteam_name")) Then
            Wuc_ddlSalesTeam.SelectedValue = "0"
        Else
            Wuc_ddlSalesTeam.SelectedValue = ReadValue(dt.Rows(0)("salesteam_id")) & "@" & ReadValue(dt.Rows(0)("salesteam_code"))
        End If
        Wuc_ddlSalesRep.SelectedValue = IIf(ReadValue(dt.Rows(0)("salesrep_name")) = String.Empty, "0", ReadValue(dt.Rows(0)("salesrep_id")) & "@" & ReadValue(dt.Rows(0)("salesrep_code")))
        Wuc_ddlLanguage.SelectedValue = (dt.Rows(0)("language_id")) & "@" & Trim(dt.Rows(0)("language_code"))
        Wuc_ddlOperationType.SelectedValue = IIf(ReadValue(dt.Rows(0)("operation_type_name")) = String.Empty, "0", ReadValue(dt.Rows(0)("operation_type_id")) & "@" & ReadValue(dt.Rows(0)("operation_type_code")))
        txtNewCustNoEdit.Text = ReadValue(dt.Rows(0)("new_cust_no"))
        txtIDPrefixEdit.Text = ReadValue(dt.Rows(0)("id_prefix"))
        txtPasswordExpiryEdit.Text = ReadValue(dt.Rows(0)("pwd_expiry"))
        cbLoginExpiryEdit.Checked = IIf(ReadValue(dt.Rows(0)("login_expiry_flag")) = "1", True, False)
        txtMaxRetryEdit.Text = ReadValue(dt.Rows(0)("max_retry"))
        txtPageSizeEdit.Text = ReadValue(dt.Rows(0)("page_size"))
        cbNetValueEdit.Checked = IIf(ReadValue(dt.Rows(0)("nv_flag")) = "1", True, False)
        ddlDefaultNetValueEdit.SelectedValue = ReadValue(dt.Rows(0)("default_nv"))
        txtPwdExpiryRmdEdit.Text = ReadValue(dt.Rows(0)("pwd_expiry_rmd"))
    End Sub
    Private Sub BindUserAccessRight(ByVal dt As DataTable, ByVal dtAccessList As DataTable)
        lboxAccessRightMapping.Items.Clear()
        lboxAccessRightAdded.Items.Clear()
        lboxAccessRight.Items.Clear()

        For i As Integer = 0 To dt.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dt.Rows(i)("accessright_name")), ReadValue(dt.Rows(i)("accessright_id")))
            lboxAccessRightMapping.Items.Add(ls)
            lboxAccessRightAdded.Items.Add(ls)
        Next
        For i As Integer = 0 To dtAccessList.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtAccessList.Rows(i)("accessright_name")), ReadValue(dtAccessList.Rows(i)("accessright_id")))
            If lboxAccessRightAdded.Items.FindByValue(ls.Value) Is Nothing Then
                lboxAccessRight.Items.Add(ls)
            End If
        Next
        'With lboxAccessRightMapping
        '    .DataSource = dt
        '    .DataTextField = "accessright_name"
        '    .DataValueField = "accessright_id"
        '    .DataBind()
        'End With

        'With lboxAccessRightAdded
        '    .DataSource = dt
        '    .DataTextField = "accessright_name"
        '    .DataValueField = "accessright_id"
        '    .DataBind()
        'End With
    End Sub
    Private Sub BindUserSRList(ByVal dt As DataTable)
        lboxSalesRepMapping.Items.Clear()
        lboxSalesrepAdded.Items.Clear()

        For i As Integer = 0 To dt.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dt.Rows(i)("salesrep_desc")), ReadValue(dt.Rows(i)("salesrep_id")))
            lboxSalesRepMapping.Items.Add(ls)
            lboxSalesrepAdded.Items.Add(ls)
        Next

        'With lboxSalesRepMapping
        '    .DataSource = dt
        '    .DataTextField = "salesrep_desc"
        '    .DataValueField = "salesrep_code"
        '    .DataBind()
        'End With

        'With lboxSalesrepAdded
        '    .DataSource = dt
        '    .DataTextField = "salesrep_desc"
        '    .DataValueField = "salesrep_code"
        '    .DataBind()
        'End With
    End Sub
    Private Sub BindUserSalesrepGrp(ByVal dtSalesrepGrp As DataTable, ByVal dtUserSalesrepGrp As DataTable)
        lboxSalesrepGrpMapping.Items.Clear()
        lboxSalesrepGrp.Items.Clear()
        lboxSalesrepGrpAdded.Items.Clear()

        For i As Integer = 0 To dtUserSalesrepGrp.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtUserSalesrepGrp.Rows(i)("salesrep_grp_name")), ReadValue(dtUserSalesrepGrp.Rows(i)("salesrep_grp_id")))
            lboxSalesrepGrpMapping.Items.Add(ls)
            lboxSalesrepGrpAdded.Items.Add(ls)
        Next
        For i As Integer = 0 To dtSalesrepGrp.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtSalesrepGrp.Rows(i)("salesrep_grp_name")), ReadValue(dtSalesrepGrp.Rows(i)("salesrep_grp_id")))
            If lboxSalesrepGrpAdded.Items.FindByValue(ls.Value) Is Nothing Then
                lboxSalesrepGrp.Items.Add(ls)
            End If
        Next
    End Sub
    Private Sub BindUserELAccessRight(ByVal dt As DataTable, ByVal dtELAccessList As DataTable)
        lboxELAccessRightMapping.Items.Clear()
        lboxELAccessRightAdded.Items.Clear()
        lboxELAccessRight.Items.Clear()

        For i As Integer = 0 To dt.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dt.Rows(i)("accessright_name")), ReadValue(dt.Rows(i)("accessright_id")))
            lboxELAccessRightMapping.Items.Add(ls)
            lboxELAccessRightAdded.Items.Add(ls)
        Next
        For i As Integer = 0 To dtELAccessList.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtELAccessList.Rows(i)("accessright_name")), ReadValue(dtELAccessList.Rows(i)("accessright_id")))
            If lboxELAccessRightAdded.Items.FindByValue(ls.Value) Is Nothing Then
                lboxELAccessRight.Items.Add(ls)
            End If
        Next

    End Sub
    Private Sub BindUserSFMSCatAccessRight(ByVal dt As DataTable, ByVal dtSFMSCatAccessList As DataTable)
        lboxSFMSCatAccessRightMapping.Items.Clear()
        lboxSFMSCatAccessRightAdded.Items.Clear()
        lboxSFMSCatAccessRight.Items.Clear()

        For i As Integer = 0 To dt.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dt.Rows(i)("accessright_name")), ReadValue(dt.Rows(i)("accessright_id")))
            lboxSFMSCatAccessRightMapping.Items.Add(ls)
            lboxSFMSCatAccessRightAdded.Items.Add(ls)
        Next
        For i As Integer = 0 To dtSFMSCatAccessList.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtSFMSCatAccessList.Rows(i)("accessright_name")), ReadValue(dtSFMSCatAccessList.Rows(i)("accessright_id")))
            If lboxSFMSCatAccessRightAdded.Items.FindByValue(ls.Value) Is Nothing Then
                lboxSFMSCatAccessRight.Items.Add(ls)
            End If
        Next


    End Sub
#End Region

#Region "Salesrep mapping"
    Private Sub PopulateSalesTeamDDL()
        Dim dt As DataTable
        Dim obj As New adm_User.clsUserQuery
        Try
            dt = obj.GetSalesTeam(SessionUserCode) '(hfUserCode.Value)

            'Dim dr As DataRow = dt.NewRow
            'dr("salesteam_id") = "-1"
            'dr("PRINCIPAL_NAME") = ""
            'dr("salesteam_code") = ""
            'dr("salesteam_name") = "-- Select --"

            'dt.Rows.InsertAt(dr, 0)

            'With ddlSalesTeam
            '    .DataSource = dt
            '    .DataTextField = "salesteam_name"
            '    .DataValueField = "salesteam_id"
            '    .DataBind()
            'End With

            With ddlSalesTeam
                .Items.Clear()

                .Items.Add(New ListItem("-- Select --", "-1"))
                .Items.Add(New ListItem("ALL", ""))

                For Each item As DataRow In dt.Rows
                    .Items.Add(New ListItem(item("PRINCIPAL_NAME").ToString() & " - " & item("salesteam_name").ToString() & "(" & item("salesteam_code").ToString & ")", item("salesteam_id").ToString.Trim))
                Next
            End With

            'AddHandler ddlSalesTeam.SelectedIndexChanged, AddressOf ddlSalesTeam_SelectedIndexChanged
        Catch ex As Exception
            ExceptionMsg("UserDtl.PopulateSalesTeamDDL : " & ex.ToString)
        End Try
    End Sub
    Private Sub ListSalesrepByTeam(ByVal salesteam_id As String)
        Dim dt, dtSRList As DataTable
        Dim obj As New adm_User.clsUserQuery
        Dim strAccessRightID As String
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim count() As DataRow
        Try
            'strAccessRightID = ""
            'For i = 0 To lboxAccessRightMapping.Items.Count - 1
            '    strAccessRightID = strAccessRightID & Trim(lboxAccessRightMapping.Items(i).Value) & ","
            'Next

            'If strAccessRightID <> "" Then
            'strAccessRightID = Mid(strAccessRightID, 1, Len(strAccessRightID) - 1)

            'objCommonQuery = New cor_Common.clsCommonQuery
            'With objCommonQuery
            '    .clsProperties.accessright_id = strAccessRightID
            '    dtSRList = .GetSalesRepListByAccessRightID()
            'End With

            dt = obj.GetSalesrepList(SessionUserCode.Trim, salesteam_id.Trim)

            lboxSalesrep.Items.Clear()

            For i As Integer = 0 To dt.Rows.Count - 1 Step 1
                Dim ls As New ListItem(ReadValue(dt.Rows(i)("salesrep_code")) & " - " & ReadValue(dt.Rows(i)("salesrep_name")), ReadValue(dt.Rows(i)("salesrep_id")))
                If lboxSalesrepAdded.Items.FindByValue(ls.Value) Is Nothing Then 'check already added
                    'count = dtSRList.Select("salesrep_id = '" & ls.Value & "'")

                    'If count.Length = 0 Then 'check has accessright to see
                    lboxSalesrep.Items.Add(ls)
                    'End If
                End If
            Next
            'End If

        Catch ex As Exception
            ExceptionMsg("UserDtl.ListSalesrepByTeam : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Function"
    Private Sub IsBSAdmin()
        If SessionUserCode = "" Or IsNothing(SessionUserCode) Then
            SRGrpID.Visible = False
        ElseIf Not SessionUserCode.ToUpper.Contains("BSADMIN") Then
            SRGrpID.Visible = False
        Else
            SRGrpID.Visible = True
        End If
    End Sub
    Private Sub TogglePageMode()
        If CurrentMode = Mode.Create Then
            PrepareCreateDDL(sessionuser)
            SwitchMode(Mode.Create)
            ObtainPermission(CurrentMode)
            lblPasswordEdit.Text = "Password"
            lblPasswordConfirmEdit.Text = "Password Confirm"
            MakePasswordFieldMandatory(True)

            Wuc_ddlPrincipal_SelectedIndexChanged(Nothing, Nothing)
        ElseIf CurrentMode = Mode.Normal Then
            Bind()
            SwitchMode(Mode.Normal)
            ObtainPermission(CurrentMode)
            lblPasswordEdit.Text = "Password (Blank if no changes)"
            lblPasswordConfirmEdit.Text = "Password Confirm (Blank if no changes)"
            MakePasswordFieldMandatory(False)
        End If
    End Sub
    Private Sub MakePasswordFieldMandatory(ByVal bool As Boolean)
        rfvPasswordBlank.Enabled = bool
        rfvPasswordConfirmBlank.Enabled = bool
    End Sub
    Private Sub AcceptIncomingRequest()
        'CurrentMode = Mode.Normal
        Dim action As String

        hfUserCode.Value = Trim(Request.QueryString("user_code"))
        hfSearchType.Value = Trim(Request.QueryString("search_type"))
        hfSearchValue.Value = Trim(Request.QueryString("search_value"))
        hfType.Value = Trim(Request.QueryString("type"))
        sessionuser = Trim(Session("UserCode"))

        action = Trim(Request.QueryString("action"))

        Select Case action.ToUpper
            Case "CREATE"
                CurrentMode = Mode.Create
            Case "EDIT"
                CurrentMode = Mode.Normal
        End Select
    End Sub
    Private Sub ObtainPermission(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                cmdDelete.Visible = False
                cmdFFMASetting.Visible = False
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                cmdFFMASetting.Visible = True
                cmdDelete.Visible = False 'Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Delete)
                cmdEditInfo.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)
                cmdAccessRightMappingEdit.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)
                cmdSalesRepMappingEdit.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)
        End Select

    End Sub
    Private Function ReadValue(ByVal data As Object) As String
        If IsDBNull(data) Then
            Return String.Empty
        ElseIf IsNothing(data) Then
            Return String.Empty
        Else
            Return data.ToString().Trim()
        End If
    End Function
    Private Sub SwitchMode(ByVal mode As Mode)
        'Select Case mode
        '    Case Admin_User_NewUserDtl_UserDtl.Mode.Create
        UserInfoSwitchMode(mode)
        AccessRightSwitchMode(mode)
        SalesrepSwitchMode(mode)
        SalesrepGrpSwitchMode(mode)
        ELAccessRightSwitchMode(mode)
        '    Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
        'UserInfoSwitchMode(mode)
        'AccessRightSwitchMode(mode)
        'SalesrepSwitchMode(mode)
        'End Select
    End Sub
    Private Sub UserInfoSwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                cmdCancelInfo.Visible = False
                cmdSaveInfo.Visible = False
                cmdEditInfo.Visible = True
                pnlUserDtlEdit.Visible = False
                pnlUserDtlInfo.Visible = True
                CurrentMode = mode
                DenyEdit("nothing")
                MappingPanelDiv.Visible = True
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                cmdCancelInfo.Visible = True
                cmdSaveInfo.Visible = True
                cmdEditInfo.Visible = False
                pnlUserDtlEdit.Visible = True
                pnlUserDtlInfo.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
                MappingPanelDiv.Visible = True
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                cmdCancelInfo.Visible = False
                cmdSaveInfo.Visible = True
                cmdEditInfo.Visible = False
                pnlUserDtlEdit.Visible = True
                pnlUserDtlInfo.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
                MappingPanelDiv.Visible = False
        End Select
    End Sub
    Private Sub AccessRightSwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                pnlMapping.Visible = True
                pnlAccessRight.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                pnlMapping.Visible = False
                pnlAccessRight.Visible = True
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                pnlMapping.Visible = False
                pnlAccessRight.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
        End Select
    End Sub
    Private Sub SalesrepSwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                pnlMapping.Visible = True
                pnlSalesrep.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                pnlMapping.Visible = False
                pnlSalesrep.Visible = True
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                pnlMapping.Visible = False
                pnlAccessRight.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
        End Select
    End Sub
    Private Sub SalesrepGrpSwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                pnlMapping.Visible = True
                pnlSalesrepGrp.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                pnlMapping.Visible = False
                pnlSalesrepGrp.Visible = True
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                pnlMapping.Visible = False
                pnlSalesrepGrp.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
        End Select
    End Sub
    Private Sub AgencySwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                pnlMapping.Visible = True
                pnlAgency.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                pnlMapping.Visible = False
                pnlAgency.Visible = True
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                pnlMapping.Visible = False
                pnlAgency.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
        End Select
    End Sub
    Private Sub DenyEdit(ByVal source As String)
        Select Case source
            Case "UserDtl"
                lblDtlEditViolation.Visible = True
            Case "Mapping"
                lblMappingEditViolation.Visible = True
            Case Else
                lblDtlEditViolation.Visible = False
                lblMappingEditViolation.Visible = False
        End Select
    End Sub
    Private Sub UpdateUsrDtl()
        Dim objUser As adm_User.clsUser
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String = ""
        Dim aryUT, aryCountry, aryPrincipal, aryST, arySR, aryLanguage, aryOT As Array
        Dim intCnt As Integer
        Dim dt As DataTable
        Dim boolChgPass As Boolean = False

        Try
            'Check login or user code exist or not
            lblLoginExists.Visible = False

            objUserQuery = New adm_User.clsUserQuery
            With objUserQuery
                .clsProperties.login = Trim(txtLoginEdit.Text)
                .clsProperties.user_code = Trim(txtLoginEdit.Text)
                .clsProperties.user_id = Trim(hfUserID.Value)
                dt = .GetUserDuplicate
                intCnt = IIf(dt.Rows(0)("cnt") = Nothing, 0, dt.Rows(0)("cnt"))
            End With
            objUserQuery = Nothing

            If intCnt > 0 Then
                'ScriptManager.RegisterStartupScript(Me, Me.GetType, "a", "<script language=javascript>alert('Login / User Code already exists!');</script>", False)
                lblLoginExists.Visible = True
                Exit Sub
            End If

            'If chkPassword.Checked Then
            If txtPasswordEdit.Text.Trim <> "" Or txtPasswordConfirmEdit.Text.Trim <> "" Then
                'Check Password match
                boolChgPass = True

                'If Trim(txtPasswordEdit.Text.Trim) <> Trim(txtPasswordConfirmEdit.Text.Trim) And Trim(txtPasswordEdit.Text <> "") Then
                '    ScriptManager.RegisterStartupScript(Me, Me.GetType, "checkpassword", "<script language=javascript>alert('Password not match. Please check your Password.');</script>", False)
                '    Exit Sub
                'End If

                'Encrypt Password
                objCrypto = New cor_Crypto.clsEncryption
                With objCrypto
                    strEncryptPwd = .TripleDESEncrypt(Trim(txtPasswordEdit.Text))
                End With
            End If

            'Get User Control Value
            aryUT = Split(Wuc_ddlUserType.SelectedValue, "@")
            aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")
            aryST = Split(Wuc_ddlSalesTeam.SelectedValue, "@")
            arySR = Split(Wuc_ddlSalesRep.SelectedValue, "@")
            aryLanguage = Split(Wuc_ddlLanguage.SelectedValue, "@")
            'aryBranch = Split(Wuc_ddlBranch.SelectedValue, "@")
            aryOT = Split(Wuc_ddlOperationType.SelectedValue, "@")

            'Insert into User Table
            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_id = Trim(hfUserID.Value)
                .clsProperties.user_type_id = Trim(aryUT(0))
                .clsProperties.principal_id = Trim(aryPrincipal(0))
                .clsProperties.country_id = Trim(aryCountry(0))
                .clsProperties.salesteam_id = Trim(aryST(0))
                If Wuc_ddlSalesTeam.SelectedValue <> "0" Then
                    .clsProperties.salesteam_code = aryST(1)
                Else
                    .clsProperties.salesteam_code = ""
                End If
                .clsProperties.salesrep_id = Trim(arySR(0))
                If Wuc_ddlSalesRep.SelectedValue <> "0" Then
                    .clsProperties.salesrep_code = arySR(1)
                Else
                    .clsProperties.salesrep_code = ""
                End If
                .clsProperties.region_id = 0
                .clsProperties.region_code = Nothing
                .clsProperties.area_id = 0
                .clsProperties.area_code = Nothing
                .clsProperties.language_id = Trim(aryLanguage(0))

                .clsProperties.login = Trim(txtLoginEdit.Text)
                .clsProperties.pwd_flag = IIf(boolChgPass, 1, 0)
                If boolChgPass Then
                    .clsProperties.pwd = strEncryptPwd
                End If
                .clsProperties.user_code = Trim(txtLoginEdit.Text)
                .clsProperties.user_name = Trim(txtUserNameEdit.Text)
                .clsProperties.user_name_1 = "" 'Trim(txtUserName1.Text)
                .clsProperties.email = "" 'Trim(txtEmail.Text)
                .clsProperties.pwd_expiry = CInt(Trim(txtPasswordExpiryEdit.Text))
                .clsProperties.pwd_expiry_rmd = CInt(Trim(txtPwdExpiryRmdEdit.Text))
                .clsProperties.login_expiry_flag = IIf(cbLoginExpiryEdit.Checked, 1, 0)
                .clsProperties.max_retry = CInt(Trim(txtMaxRetryEdit.Text))
                .clsProperties.page_size = CInt(txtPageSizeEdit.Text)
                .clsProperties.nv_flag = IIf(cbNetValueEdit.Checked, 1, 0)
                .clsProperties.default_nv = CInt(ddlDefaultNetValueEdit.SelectedValue)
                .clsProperties.operation_type_id = Trim(aryOT(0))
                If Wuc_ddlOperationType.SelectedValue <> "0" Then
                    .clsProperties.operation_type_code = aryOT(1)
                Else
                    .clsProperties.operation_type_code = ""
                End If
                .clsProperties.new_cust_no = Trim(txtNewCustNoEdit.Text)
                .clsProperties.dummy_cust_no = "" 'Trim(txtDummyCustNo.Text)
                .clsProperties.id_prefix = Trim(txtIDPrefixEdit.Text)
                .clsProperties.pwd_changed_date = Now '.ToString
                '.clsProperties.created_date = Now '.ToString
                '.clsProperties.creator_user_id = Session("UserID")
                .clsProperties.changed_date = Now
                .clsProperties.changed_user_id = Session("UserID")
                .clsProperties.delete_flag = 0
                .clsProperties.active_flag = ddlStatusEdit.SelectedValue
                '.clsProperties.liAR = lstAccessRightTo.Items
                '.clsProperties.liSR = lstSRTo.Items
                .UpdateUserDtl()


            End With
            'Response.Redirect("UserDtl.aspx?user_code=" & Trim(txtUserCode.Text) & "&type=" & Trim(txtType.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

            'Lastly Reload information
            dt = GetUserDtl(hfUserCode.Value)
            BindUserDtl(dt)

            UserInfoSwitchMode(Mode.Normal)
        Catch ex As Exception
            Throw
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Sub
    Private Function CreateUsrDtl() As Boolean
        Dim objUser As adm_User.clsUser
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String
        Dim aryUT, aryCountry, aryPrincipal, aryST, arySR, aryLanguage, aryBranch, aryOT As Array
        Dim intCnt As Integer
        Dim dt As DataTable

        Try
            'Check login or user code exist or not
            lblLoginExists.Visible = False

            objUserQuery = New adm_User.clsUserQuery
            With objUserQuery
                .clsProperties.login = Trim(txtLoginEdit.Text)
                .clsProperties.user_code = Trim(txtLoginEdit.Text)
                .clsProperties.user_id = "0"
                dt = .GetUserDuplicate
                intCnt = IIf(dt.Rows(0)("cnt") = Nothing, 0, dt.Rows(0)("cnt"))
            End With
            objUserQuery = Nothing

            If intCnt > 0 Then
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Login / User Code already exists!');</script>")
                lblLoginExists.Visible = True
                Return False
            End If

            'Check Password 
            'If Trim(txtPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtPassword.Text <> "") Then
            'Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
            'Exit Sub
            'End If

            'check password 
            'If txtPasswordEdit.Text.Trim = "" And txtPasswordConfirmEdit.Text.Trim = "" Then

            'End If




            'Encrypt Password
            objCrypto = New cor_Crypto.clsEncryption
            With objCrypto
                strEncryptPwd = .TripleDESEncrypt(Trim(txtPasswordEdit.Text))
            End With

            'Get User Control Value
            aryUT = Split(Wuc_ddlUserType.SelectedValue, "@")
            aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")
            aryST = Split(Wuc_ddlSalesTeam.SelectedValue, "@")
            arySR = Split(Wuc_ddlSalesRep.SelectedValue, "@")
            aryLanguage = Split(Wuc_ddlLanguage.SelectedValue, "@")
            'aryBranch = Split(Wuc_ddlBranch.SelectedValue, "@")
            aryOT = Split(Wuc_ddlOperationType.SelectedValue, "@")

            'Insert into User Table
            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_type_id = Trim(aryUT(0))
                .clsProperties.principal_id = Trim(aryPrincipal(0))
                .clsProperties.country_id = Trim(aryCountry(0))
                .clsProperties.salesteam_id = Trim(aryST(0))
                If Wuc_ddlSalesTeam.SelectedValue <> "0" Then
                    .clsProperties.salesteam_code = aryST(1)
                Else
                    .clsProperties.salesteam_code = ""
                End If
                .clsProperties.salesrep_id = Trim(arySR(0))
                If Wuc_ddlSalesRep.SelectedValue <> "0" Then
                    .clsProperties.salesrep_code = arySR(1)
                Else
                    .clsProperties.salesrep_code = ""
                End If
                .clsProperties.region_id = 0
                .clsProperties.region_code = Nothing
                .clsProperties.area_id = 0
                .clsProperties.area_code = Nothing
                .clsProperties.language_id = Trim(aryLanguage(0))

                .clsProperties.login = Trim(txtLoginEdit.Text)
                .clsProperties.pwd = strEncryptPwd
                .clsProperties.user_code = Trim(txtLoginEdit.Text)
                .clsProperties.user_name = Trim(txtUserNameEdit.Text)
                .clsProperties.user_name_1 = "" 'Trim(txtUserName1.Text)
                .clsProperties.email = "" 'Trim(txtEmail.Text)
                .clsProperties.pwd_expiry = CInt(Trim(txtPasswordExpiryEdit.Text))
                .clsProperties.pwd_expiry_rmd = CInt(Trim(txtPwdExpiryRmdEdit.Text))
                .clsProperties.login_expiry_flag = IIf(cbLoginExpiryEdit.Checked, 1, 0)
                .clsProperties.max_retry = CInt(Trim(txtMaxRetryEdit.Text))
                .clsProperties.page_size = CInt(txtPageSizeEdit.Text)
                .clsProperties.nv_flag = IIf(cbNetValueEdit.Checked, 1, 0)
                .clsProperties.default_nv = CInt(ddlDefaultNetValueEdit.SelectedValue)
                .clsProperties.operation_type_id = Trim(aryOT(0))
                If Wuc_ddlOperationType.SelectedValue <> "0" Then
                    .clsProperties.operation_type_code = aryOT(1)
                Else
                    .clsProperties.operation_type_code = ""
                End If
                .clsProperties.new_cust_no = Trim(txtNewCustNoEdit.Text)
                .clsProperties.dummy_cust_no = "" 'Trim(txtDummyCustNo.Text)
                .clsProperties.id_prefix = Trim(txtIDPrefixEdit.Text)
                .clsProperties.pwd_changed_date = Nothing '.ToString
                .clsProperties.created_date = Now '.ToString
                .clsProperties.creator_user_id = Session("UserID")
                .clsProperties.changed_date = Nothing
                .clsProperties.changed_user_id = 0
                .clsProperties.delete_flag = 0
                .clsProperties.active_flag = ddlStatusEdit.SelectedValue
                '.clsProperties.liAR = lstAccessRightTo.Items
                '.clsProperties.liSR = lstSRTo.Items
                .Create()
                Return True
            End With


        Catch ex As Exception
            Throw
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Function

    Private Sub SFMSCatAccessRightSwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                pnlMapping.Visible = True
                pnlSFMSCategory.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                pnlMapping.Visible = False
                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.ONLINEGALLERY, SubModuleAction.View) Then
                    pnlSFMSCategory.Visible = True
                Else
                    pnlSFMSCategory.Visible = False
                End If
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                pnlMapping.Visible = False
                pnlSFMSCategory.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
        End Select
    End Sub

    Private Sub ELAccessRightSwitchMode(ByVal mode As Mode)
        Select Case mode
            Case Admin_User_NewUserDtl_UserDtl.Mode.Normal
                pnlMapping.Visible = True
                pnlELAccessRight.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Edit
                pnlMapping.Visible = False
                If Report.GetAccessRight(ModuleID.ADM, SubModuleType.ELACCESSRIGHT, SubModuleAction.View) Then
                    pnlELAccessRight.Visible = True
                Else
                    pnlELAccessRight.Visible = False
                End If
                CurrentMode = mode
                DenyEdit("nothing")
            Case Admin_User_NewUserDtl_UserDtl.Mode.Create
                pnlMapping.Visible = False
                pnlELAccessRight.Visible = False
                CurrentMode = mode
                DenyEdit("nothing")
        End Select
    End Sub
#End Region

#Region "Events"
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlCountry_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlCountry.SelectedIndexChanged
        Dim ary As Array

        Try
            'If CurrentMode = Mode.Create Then
            ary = Split(Wuc_ddlCountry.SelectedValue, "@")

            'Call Principal
            With Wuc_ddlPrincipal
                If Wuc_ddlCountry.SelectedValue <> "0" Then
                    .CountryCode = ary(1)
                Else
                    .CountryCode = ary(0) 'Wuc_ddlCountry.SelectedValue
                End If
                .UserCode = Trim(Session("UserCode"))
                .RequiredValidation = True
                .AutoPostBack = True
                .DataBind()
                .Visible = True
            End With
            'End If

        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlCountry_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlPrincipal_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlPrincipal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlPrincipal.SelectedIndexChanged
        Dim ary As Array

        Try
            'If CurrentMode = Mode.Create Then
            ary = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            'Call Sales Team
            With Wuc_ddlSalesTeam
                If Wuc_ddlPrincipal.SelectedValue <> "0" Then
                    .PrincipalCode = ary(1)
                Else
                    .PrincipalCode = ary(0) 'Wuc_ddlPrincipal.SelectedValue
                End If
                .RequiredValidation = False
                .AutoPostBack = True
                .DataBind()
                .Visible = True
            End With
            'End If

        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlPrincipal_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlSalesTeam_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlSalesTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlSalesTeam.SelectedIndexChanged
        Dim ary As Array

        Try
            'If CurrentMode = Mode.Create Then
            ary = Split(Wuc_ddlSalesTeam.SelectedValue, "@")

            'Call Sales Rep
            With Wuc_ddlSalesRep
                If Wuc_ddlSalesTeam.SelectedValue <> "0" Then
                    .SalesTeamCode = ary(1)
                Else
                    .SalesTeamCode = ary(0) 'Wuc_ddlSalesTeam.SelectedValue
                End If
                .RequiredValidation = False
                .AutoPostBack = False
                .DataBind()
                .Visible = True
            End With
            'End If

        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlSalesTeam_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlUserType_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlUserType.SelectedIndexChanged
        Dim aryUT, aryCountry, aryST As Array
        Dim strUserType As String

        Try
            'If CurrentMode = Mode.Create Then
            aryUT = Split(Wuc_ddlUserType.SelectedValue, "@")
            aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            aryST = Split(Wuc_ddlSalesTeam.SelectedValue, "@")
            If aryUT.Length > 1 Then
                strUserType = aryUT(1)
            Else
                strUserType = aryUT(0)
            End If


            'Call Operation Type
            With Wuc_ddlOperationType
                .RequiredValidation = False
                .AutoPostBack = False
                .DataBind()
                .Visible = True
                .SelectedValue = IIf(Trim(strUserType) <> "SR", 0, Wuc_ddlOperationType.SelectedValue)
                .SetEnable = True 'IIf(Trim(strUserType) <> "SR", False, True)
            End With

            'Call Sales Rep
            With Wuc_ddlSalesRep
                If Wuc_ddlSalesTeam.SelectedValue = "0" Then
                    .SalesTeamCode = Wuc_ddlSalesTeam.SelectedValue
                Else
                    .SalesTeamCode = aryST(1)
                End If
                .RequiredValidation = IIf(Trim(strUserType) <> "SR", False, True)
                .AutoPostBack = False
                .DataBind()
                .Visible = True
                .SelectedValue = IIf(Trim(strUserType) <> "SR", 0, Wuc_ddlSalesRep.SelectedValue)
                '.SetEnable = IIf(Trim(strUserType) <> "SR", False, True)
            End With

            'End If

        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlUserType_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub
#End Region

    Protected Sub cmdEditInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEditInfo.Click
        If CurrentMode = Mode.Edit Then
            DenyEdit("UserDtl")

        ElseIf CurrentMode = Mode.Normal Then
            'CurrentMode = Mode.Edit
            'pnlUserDtlEdit.Visible = "true"
            'pnlUserDtlInfo.Visible = "false"
            Dim dt As DataTable = GetUserDtl(hfUserCode.Value)
            BindUserDtl(dt)
            UserInfoSwitchMode(Mode.Edit)
        End If
    End Sub

    Protected Sub cmdCancelInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelInfo.Click
        'CurrentMode = Mode.Normal
        'pnlUserDtlEdit.Visible = "false"
        'pnlUserDtlInfo.Visible = "true"
        UserInfoSwitchMode(Mode.Normal)
        DenyEdit("nothing")
    End Sub


    Protected Sub cmdSaveInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSaveInfo.Click
        Select Case CurrentMode
            Case Mode.Edit
                Try
                    If txtPasswordEdit.Text.Trim <> "" Or txtPasswordConfirmEdit.Text.Trim <> "" Then
                        If (RepeatedPassCheck(txtPasswordEdit.Text.Trim) And ConfigurationManager.AppSettings("DiffLast") = "1") Then
                            lblPasswordRepeat.Visible = True
                        Else
                            lblPasswordRepeat.Visible = False
                            UpdateUsrDtl()
                        End If
                    Else
                        UpdateUsrDtl()
                    End If

                Catch ex As Exception
                    ExceptionMsg("UserDtl.cmdSaveInfo_Click(Update User Dtl) : " & ex.ToString)
                End Try

            Case Mode.Create
                Try
                    If CreateUsrDtl() Then
                        CurrentMode = Mode.Normal
                        hfUserCode.Value = txtLoginEdit.Text.Trim
                        TogglePageMode()
                    End If
                Catch ex As Exception
                    ExceptionMsg("UserDtl.cmdSaveInfo_Click(Create User Dtl) : " & ex.ToString)
                End Try
        End Select

    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub




#Region "Accessright"
    Protected Sub cmdAccessRightMappingEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If CurrentMode = Mode.Edit Then
            DenyEdit("Mapping")
        Else
            '---------------
            GetUserAccessright(hfUserCode.Value)
            '---------------
            AccessRightSwitchMode(Mode.Edit)
        End If
    End Sub

    Protected Sub cmdARSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdARSave.Click
        Dim obj As New adm_User.clsUser

        Try
            With obj
                .clsProperties.user_id = Trim(hfUserID.Value)
                '.clsProperties.user_code = Trim(lblLoginValue.Text)
                .clsProperties.user_code = Trim(hfUserCode.Value)
                .clsProperties.changed_date = Now
                .clsProperties.changed_user_id = Session("UserID")
                .clsProperties.delete_flag = 0
                .clsProperties.liAR = lboxAccessRightAdded.Items
                .UpdateUserAccessRight()
            End With
            AccessRightSwitchMode(Mode.Normal)
            GetUserAccessright(hfUserCode.Value)
        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdARSave_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdARCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AccessRightSwitchMode(Mode.Normal)
    End Sub

    Protected Sub IBRight_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxAccessRight, lboxAccessRightAdded)
            Selector.SelectFirstItem(lboxAccessRight)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBRight_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxAccessRightAdded, lboxAccessRight)
            Selector.SelectFirstItem(lboxAccessRightAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBLeft_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBRightAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxAccessRight, lboxAccessRightAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBRightAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBLeftAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxAccessRightAdded, lboxAccessRight)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBLeftAll_Click : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Salesrep"

    Protected Sub cmdSalesRepMappingEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSalesRepMappingEdit.Click
        If CurrentMode = Mode.Edit Then
            DenyEdit("Mapping")
        Else

            '---------------
            PopulateSalesTeamDDL()
            ddlSalesTeam.SelectedIndex = 0
            GetSRList(hfUserCode.Value)
            lboxSalesrep.Items.Clear()
            '---------------
            SalesrepSwitchMode(Mode.Edit)
        End If
    End Sub

    Protected Sub cmdSRSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim obj As New adm_User.clsUser

        Try
            With obj
                .clsProperties.user_id = Trim(hfUserID.Value)
                .clsProperties.user_code = Trim(lblLoginValue.Text)
                .clsProperties.changed_date = Now
                .clsProperties.changed_user_id = Session("UserID")
                .clsProperties.delete_flag = 0
                .clsProperties.liSR = lboxSalesrepAdded.Items
                .UpdateUserSalesRep()
            End With

            SalesrepSwitchMode(Mode.Normal)
            GetSRList(hfUserCode.Value)
        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdSRSave_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdSRCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SalesrepSwitchMode(Mode.Normal)
    End Sub

    Protected Sub IBSRSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxSalesrep, lboxSalesrepAdded)
            Selector.SelectFirstItem(lboxSalesrep)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRSelect_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBSRDeselect_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxSalesrepAdded, lboxSalesrep)
            Selector.SelectFirstItem(lboxSalesrepAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRDeselect_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBSRSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxSalesrep, lboxSalesrepAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRSelectAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBSRDeselectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxSalesrepAdded, lboxSalesrep)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRDeselectAll_Click : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "SalesrepGrp"

    Protected Sub cmdSalesrepGrpMappingEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSalesrepGrpMappingEdit.Click
        If CurrentMode = Mode.Edit Then
            DenyEdit("Mapping")
        Else

            '---------------
            GetUserSalesrepGrp(hfUserID.Value, sessionuser)
            '---------------
            SalesrepGrpSwitchMode(Mode.Edit)
        End If
    End Sub

    Protected Sub cmdSRGrpSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSRGrpSave.Click
        Dim obj As New adm_User.clsUser

        Try
            With obj
                .clsProperties.user_id = Trim(hfUserID.Value)
                .clsProperties.changed_user_id = Session("UserID")
                .clsProperties.creator_user_id = Session("UserID")
                .clsProperties.liSRG = lboxSalesrepGrpAdded.Items
                .UpdateUserSalesRepGrp()
            End With

            SalesrepGrpSwitchMode(Mode.Normal)
            GetUserSalesrepGrp(hfUserID.Value, sessionuser)
        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdSRGrpSave_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdSRGrpCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSRGrpCancel.Click
        SalesrepGrpSwitchMode(Mode.Normal)
    End Sub

    Protected Sub IBSRGrpSelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles IBSRGrpSelect.Click
        Try
            Selector.SelectItem(lboxSalesrepGrp, lboxSalesrepGrpAdded)
            Selector.SelectFirstItem(lboxSalesrepGrp)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRGrpSelect_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBSRGrpDeselect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles IBSRGrpDeselect.Click
        Try
            Selector.SelectItem(lboxSalesrepGrpAdded, lboxSalesrepGrp)
            Selector.SelectFirstItem(lboxSalesrepGrpAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRGrpDeselect_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBSRGrpSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles IBSRGrpSelectAll.Click
        Try
            Selector.SelectAllItem(lboxSalesrepGrp, lboxSalesrepGrpAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRGrpSelectAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub IBSRGrDeselectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles IBSRGrDeselectAll.Click
        Try
            Selector.SelectAllItem(lboxSalesrepGrpAdded, lboxSalesrepGrp)
        Catch ex As Exception
            ExceptionMsg("UserDtl.IBSRGrDeselectAll_Click : " & ex.ToString)
        End Try
    End Sub
#End Region


    Protected Sub ddlSalesTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesTeam.SelectedIndexChanged
        Dim salesteam_id As String

        If ddlSalesTeam.SelectedIndex <> 0 And ddlSalesTeam.SelectedIndex <> -1 Then
            salesteam_id = ddlSalesTeam.SelectedValue
            ListSalesrepByTeam(salesteam_id)
        Else
            lboxSalesrep.Items.Clear()
        End If
    End Sub

    Protected Sub cmdFFMASetting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Response.Redirect("UserFFMA.aspx?user_code=" & hfUserCode.Value.Trim & "&type=" & hfType.Value.Trim & "&search_type=" & hfSearchType.Value.Trim & "&search_value=" & hfSearchValue.Value, False)

        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdFFMASetting_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objUser As adm_User.clsUser

        Try
            'If Trim(lblStatusValue.Text) = "Active" Then
            '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Please set status to In-Active before proceed.');</script>")
            '    Exit Sub
            'End If

            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_id = hfUserID.Value.Trim
                .clsProperties.user_code = hfUserCode.Value.Trim
                .Delete()
            End With

            Response.Redirect("UserList.aspx?search_type=" & hfSearchType.Value.Trim & "&search_value=" & hfSearchValue.Value.Trim, False)

        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdDelete_Click : " & ex.ToString)
        Finally
            objUser = Nothing
        End Try
    End Sub

    Protected Sub cmdReturn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReturn.Click
        Try
            Response.Redirect("UserList.aspx?search_type=" & hfSearchType.Value & "&search_value=" & hfSearchValue.Value, False)

        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdReturn_Click : " & ex.ToString)
        End Try
    End Sub


#Region "User Agency"

    Private Function GetEnableOptionInd() As Boolean
        Dim obj As New adm_User.clsUserAgency
        Dim dt As DataTable
        Dim strEnableOptionInd As Boolean = False
        Try
            With obj
                .clsProperties.strUserId = Trim(hfUserID.Value)
                .clsProperties.strPrincipalId = Session("PRINCIPAL_ID")
                .clsProperties.strSessionUserId = Portal.UserSession.UserID
                .clsProperties.strOption = "USER_AGENCY"
                dt = .GetUserAgencyOptionIndList
            End With

            If dt.Rows.Count > 0 Then
                If Portal.Util.GetValue(Of String)(dt.Rows(0)(0)) = 1 Then
                    strEnableOptionInd = True
                Else
                    strEnableOptionInd = False
                End If
            Else
                strEnableOptionInd = False
            End If

            Return strEnableOptionInd
        Catch ex As Exception
            ExceptionMsg("UserDtl.btnAgencySave_Click: " & ex.ToString)
        End Try
    End Function

    Protected Sub btnAgencyMappingEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgencyMappingEdit.Click
        If CurrentMode = Mode.Edit Then
            DenyEdit("Mapping")
        Else

            '---------------
            GetUserAgency(hfUserID.Value, sessionuser)
            '---------------
            AgencySwitchMode(Mode.Edit)
        End If
    End Sub

    Private Sub GetUserAgency(ByVal strUserID As String, ByVal strSessionUserCode As String)
        Dim dtAgencyList, dtUserAgencyList As DataTable
        Dim clsAgency As New adm_User.clsUserAgency

        Try
            With clsAgency.clsProperties
                .strPrincipalId = Session("PRINCIPAL_ID")
                .strSessionUserId = strSessionUserCode
                .strUserId = strUserID
            End With


            dtAgencyList = clsAgency.GetUserAgencyListAll
            dtUserAgencyList = clsAgency.GetUserAgencyList

            BindUserAgency(dtAgencyList, dtUserAgencyList)


        Catch ex As Exception
            Throw
        Finally
            clsAgency = Nothing
        End Try
    End Sub

    Private Sub BindUserAgency(ByVal dtAgencyList As DataTable, ByVal dtUserAgencyList As DataTable)
        lstAgency.Items.Clear()
        lstAgencyMapping.Items.Clear()
        lstAgencyAdded.Items.Clear()

        For i As Integer = 0 To dtAgencyList.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtAgencyList.Rows(i)("AGENCY_NAME")), ReadValue(dtAgencyList.Rows(i)("AGENCY_CODE")))
            lstAgency.Items.Add(ls)
        Next
        For i As Integer = 0 To dtUserAgencyList.Rows.Count - 1 Step 1
            Dim ls As New ListItem(ReadValue(dtUserAgencyList.Rows(i)("AGENCY_NAME")), ReadValue(dtUserAgencyList.Rows(i)("AGENCY_CODE")))
            If lstAgencyAdded.Items.FindByValue(ls.Value) Is Nothing Then
                lstAgencyAdded.Items.Add(ls)
                lstAgencyMapping.Items.Add(ls)
            End If
        Next
    End Sub

    Protected Sub lnkAgencySelect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAgencySelect.Click
        Try
            Selector.SelectItem(lstAgency, lstAgencyAdded)
            Selector.SelectFirstItem(lstAgency)
        Catch ex As Exception
            ExceptionMsg("lnkAgencySelect_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub lnkAgencyDeselect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAgencyDeSelect.Click
        Try
            Selector.SelectItem(lstAgencyAdded, lstAgency)
            Selector.SelectFirstItem(lstAgencyAdded)
        Catch ex As Exception
            ExceptionMsg(" lnkAgencyDeselect_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub lnkAgencySelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAgencySelectAll.Click
        Try
            Selector.SelectAllItem(lstAgency, lstAgencyAdded)
        Catch ex As Exception
            ExceptionMsg("lnkAgencySelectAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub lnkAgencyDeselectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAgencyDeselectAll.Click
        Try
            Selector.SelectAllItem(lstAgencyAdded, lstAgency)
        Catch ex As Exception
            ExceptionMsg("lnkAgencyDeselectAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnAgencyCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgencyCancel.Click
        AgencySwitchMode(Mode.Normal)
    End Sub

    Protected Sub btnAgencySave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgencySave.Click
        Dim obj As New adm_User.clsUserAgency
        Dim dt As DataTable
        Try
            With obj
                .clsProperties.strUserId = Trim(hfUserID.Value)
                .clsProperties.strPrincipalId = Session("PRINCIPAL_ID")
                .clsProperties.strSessionUserId = Portal.UserSession.UserID
                .clsProperties.strAgencyList = GetItemsInString(lstAgencyAdded)
                dt = .UpdateUserAgencyList()
            End With


            AgencySwitchMode(Mode.Normal)
            GetUserAgency(hfUserID.Value, sessionuser)
        Catch ex As Exception
            ExceptionMsg("UserDtl.btnAgencySave_Click: " & ex.ToString)
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function
#End Region

#Region "Easy Loader V2 Accessright"

    Protected Sub cmdELAccessRightMappingEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If CurrentMode = Mode.Edit Then
            DenyEdit("Mapping")
        Else
            '---------------
            GetUserELAccessright(Trim(hfUserID.Value))
            '---------------
            ELAccessRightSwitchMode(Mode.Edit)
        End If
    End Sub


    Protected Sub cmdELARSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdELARSave.Click
        Dim clsELAccessRight As New adm_ELAccessRight.clsELAccessRight

        Try

            Dim strUserId As String = Trim(hfUserID.Value)
            Dim strChangedUserId As String = Portal.UserSession.UserID
            clsELAccessRight.DeleteEasyLoaderUserAccessright(strUserId, strChangedUserId)
            For Each li As ListItem In lboxELAccessRightAdded.Items
                clsELAccessRight.CreateEasyLoaderUserAccessright(strUserId, li.Value, strChangedUserId)
            Next
            ELAccessRightSwitchMode(Mode.Normal)
            GetUserELAccessright(strUserId)
        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdELARSave_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdELARCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ELAccessRightSwitchMode(Mode.Normal)
    End Sub

    Protected Sub ELIBRight_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxELAccessRight, lboxELAccessRightAdded)
            Selector.SelectFirstItem(lboxELAccessRight)
        Catch ex As Exception
            ExceptionMsg("UserDtl.ELIBRight_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ELIBLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxELAccessRightAdded, lboxELAccessRight)
            Selector.SelectFirstItem(lboxELAccessRightAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.ELIBLeft_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ELIBRightAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxELAccessRight, lboxELAccessRightAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.ELIBRightAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ELIBLeftAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxELAccessRightAdded, lboxELAccessRight)
        Catch ex As Exception
            ExceptionMsg("UserDtl.ELIBLeftAll_Click : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Field Activity (SFMS) Category Accessright"

    Protected Sub cmdSFMSCatAccessRightMappingEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If CurrentMode = Mode.Edit Then
            DenyEdit("Mapping")
        Else
            '---------------
            GetUserSFMSCatAccessright(Trim(hfUserID.Value))
            '---------------
            SFMSCatAccessRightSwitchMode(Mode.Edit)
        End If
    End Sub

    Protected Sub cmdSFMSCatSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSFMSCatSave.Click
        Dim clsSFMSCatAccessRight As New adm_SFMSCatAccessRight.clsSFMSCatAccessRight
        Try
            Dim strUserId As String = Trim(hfUserID.Value)
            Dim strChangedUserId As String = Portal.UserSession.UserID
            clsSFMSCatAccessRight.DeleteSFMSUserAccessright(strUserId, strChangedUserId)
            For Each li As ListItem In lboxSFMSCatAccessRightAdded.Items
                clsSFMSCatAccessRight.CreateSFMSUserAccessright(strUserId, li.Value, strChangedUserId)
            Next
            SFMSCatAccessRightSwitchMode(Mode.Normal)
            GetUserSFMSCatAccessright(strUserId)
        Catch ex As Exception
            ExceptionMsg("UserDtl.cmdSFMSCatSave : " & ex.ToString)
        End Try
    End Sub

    Protected Sub cmdSFMSCatCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SFMSCatAccessRightSwitchMode(Mode.Normal)
    End Sub

    Protected Sub SFMSCatRight_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxSFMSCatAccessRight, lboxSFMSCatAccessRightAdded)
            Selector.SelectFirstItem(lboxSFMSCatAccessRight)
        Catch ex As Exception
            ExceptionMsg("UserDtl.SFMSCatRight_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub SFMSCatLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectItem(lboxSFMSCatAccessRightAdded, lboxSFMSCatAccessRight)
            Selector.SelectFirstItem(lboxSFMSCatAccessRightAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.SFMSCatLeft_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub SFMSCatRightAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxSFMSCatAccessRight, lboxSFMSCatAccessRightAdded)
        Catch ex As Exception
            ExceptionMsg("UserDtl.SFMSCatRightAll_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub SFMSCatLeftAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Selector.SelectAllItem(lboxSFMSCatAccessRightAdded, lboxSFMSCatAccessRight)
        Catch ex As Exception
            ExceptionMsg("UserDtl.SFMSCatLeftAll_Click : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Pwd Policy"
    Sub PolicyCheck(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        e.IsValid = Portal.Policy.CheckPwd(e.Value.Trim)
    End Sub

    Private Function RepeatedPassCheck(ByVal strPassword As String) As Boolean
        Dim clsUserQuery As New adm_User.clsUserQuery
        Dim dt As DataTable = Nothing
        Dim rept As Boolean = False
        Try
            If IsNothing(dtPassLib) Then
                With clsUserQuery.clsProperties
                    .user_id = hfUserID.Value 'Portal.UserSession.UserID
                End With
                dt = clsUserQuery.GetUsrPassLibList
                dtPassLib = dt
            End If


            Dim objCrypto As cor_Crypto.clsEncryption
            objCrypto = New cor_Crypto.clsEncryption


            Dim dv1 As New DataView(dtPassLib, "pwd='" + objCrypto.TripleDESEncrypt(Trim(strPassword)) + "' ", "pwd", DataViewRowState.CurrentRows)
            If dv1.Count > 0 Then
                rept = True
            End If

        Catch ex As Exception
            ExceptionMsg("RepeatedPassCheck  : " & ex.ToString)
        End Try

        Return rept
    End Function
#End Region

End Class

Class Selector
    Public Shared Sub SelectItem(ByRef fromlst As ListBox, ByRef tolst As ListBox)
        Dim index As Integer = 0
        Dim InsertPoint As Integer = tolst.Items.Count
        Try
            For i As Integer = fromlst.Items.Count - 1 To 0 Step -1
                If fromlst.Items(i).Selected Then
                    If Not CheckItemExists(fromlst.Items(i).Value, tolst) Then
                        fromlst.Items(i).Selected = False
                        'tolst.Items.Add(fromlst.Items(i))
                        tolst.Items.Insert(InsertPoint, fromlst.Items(i))
                        fromlst.Items.RemoveAt(i)
                    End If
                End If
                index = index + 1
            Next

            'For Each li As ListItem In fromlst.Items
            '    If li.Selected Then
            '        If Not CheckItemExists(li.Value, tolst) Then
            '            li.Selected = False
            '            tolst.Items.Add(li)
            '            fromlst.Items.RemoveAt(index)
            '        End If
            '    End If
            '    index = index + 1
            'Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Shared Sub SelectAllItem(ByRef fromlst As ListBox, ByRef tolst As ListBox)
        Dim index As Integer = 0
        Dim InsertPoint As Integer = tolst.Items.Count
        Try
            For i As Integer = fromlst.Items.Count - 1 To 0 Step -1

                'If Not CheckItemExists(fromlst.Items(i).Value, tolst) Then
                If tolst.Items.FindByValue(fromlst.Items(i).Value) Is Nothing Then
                    fromlst.Items(i).Selected = False
                    'tolst.Items.Add(fromlst.Items(i))
                    tolst.Items.Insert(InsertPoint, fromlst.Items(i))
                    fromlst.Items.RemoveAt(fromlst.Items.IndexOf(fromlst.Items(i)))
                End If
                index = index + 1
            Next

            'For Each li As ListItem In fromlst.Items
            '    If Not CheckItemExists(li.Value, tolst) Then
            '        li.Selected = False
            '        tolst.Items.Add(li)
            '        fromlst.Items.RemoveAt(index)
            '    End If
            '    index = index + 1
            'Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Shared Function CheckItemExists(ByVal key As String, ByVal lst As ListBox) As Boolean
        For Each item As ListItem In lst.Items
            If item.Value.Trim = key.Trim Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Sub SelectFirstItem(ByRef lbox As ListBox)
        If lbox.Items.Count > 0 Then
            lbox.Items(0).Selected = True
        End If
    End Sub
End Class