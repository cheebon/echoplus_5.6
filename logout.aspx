<%@ Page Language="vb" AutoEventWireup="false" Codefile="logout.aspx.vb" Inherits="logout" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Logout - Display Message</title>
</head>
<body >
    <form id="frmLogoutPage" runat="server" method="post">
        <font face="ARIAL">
            <h2>
                <font color="#0303a2">Logout</font></h2>
            <hr>
            <h3>
                Thank you for using Echoplus.</h3>
        </font>
    </form>

    <script type="text/javascript" language="javascript">
		self.parent.window.close();
       
		if (!self.parent.window.opener.closed){
		    self.parent.window.opener.location='login.aspx';
		    self.parent.window.opener.focus();
		}
		else if (self.parent.window.opener && self.parent.window.opener.closed) window.open('login.aspx');
    </script>

</body>
</html>
