<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPDtl.aspx.vb" Inherits="Admin_SAP_SAPDtl" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>SAP Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <style type="text/css">
        .auto-style1
        {
            height: 14px;
        }
    </style>
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmSAPDtl" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
            <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">                                    
                                     <tr class="Bckgroundreport">
				                        <td align="left">
				                            <asp:TextBox ID="txtDocType" CssClass="cls_textbox" runat="server" Visible=false></asp:TextBox>
				                            <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
				                            <asp:TextBox ID="txtSearchValue1" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
				                            <asp:TextBox ID="txtTxnNo" CssClass="cls_textbox" runat="server" Visible=false></asp:TextBox>
				                            <asp:button id="btnViewTray" runat="server" Text="View Tray" CssClass="cls_button"></asp:button>
				                            <asp:button id="btnRefresh" runat="server" Text="Refresh" CssClass="cls_button"></asp:button>
				                            <asp:button id="btnCancel" runat="server" Text="Cancel" CssClass="cls_button"></asp:button>				         
					                        <asp:button id="btnResubmit" runat="server" Text="Resubmit" CssClass="cls_button"></asp:button>		
					                        <asp:button id="btnUnBlock" runat="server" Text="UnBlock" CssClass="cls_button"></asp:button>	
                                            <asp:button id="btnEdit" runat="server" Text="Edit" CssClass="cls_button"></asp:button>				         		         
                                            <asp:button id="btnSave" runat="server" Text="Save" CssClass="cls_button" Visible="False"></asp:button>				         		         
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport"><td>&nbsp;</td></tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">	
					                             <tr>
							                        <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot35" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblTxnStatus" runat="server" CssClass="cls_label_header">Txn Status</asp:label></td>
							                        <td width="2%"><asp:label id="Label2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <asp:panel id="pnlLN" runat="server">
						                         <tr>
							                        <td width="20%"><asp:label id="lblLNStatus" runat="server" CssClass="cls_label_header">LN Status</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot40" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblLNStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
							                     </tr>
							                     <tr>
							                        <td width="20%"><asp:label id="lblLNRemarks" runat="server" CssClass="cls_label_header">LN Remarks</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot41" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblLNRemarksValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>						                    
						                        </asp:panel>        
					                            <tr>
							                        <td width="20%"><asp:label id="lblTxnNo" runat="server" CssClass="cls_label_header">Txn No</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnNoValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblDocNo" runat="server" CssClass="cls_label_header">Saving No</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblDocNoValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr><td>&nbsp;</td></tr>						                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblSRCode" runat="server" CssClass="cls_label_header">Salesrep Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblSRCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblLastUpdate" runat="server" CssClass="cls_label_header">Last Update</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblLastUpdateValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblTxnDate" runat="server" CssClass="cls_label_header">Txn Date</asp:label></td>
							                        <td width="2%"><asp:label id="Label3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblTxnDateValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblOrderType" runat="server" CssClass="cls_label_header">Order Type</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblOrderTypeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                            <td width="20%"><asp:label id="lblSalesOrg" runat="server" CssClass="cls_label_header">Sales Org</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot7" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblSalesOrgValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblDistChannel" runat="server" CssClass="cls_label_header">Distribution Channel</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblDistChannelValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblDivCode" runat="server" CssClass="cls_label_header">Division Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblDivCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
						                            <td width="20%"><asp:label id="lblSalesOff" runat="server" CssClass="cls_label_header">Sales Office</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblSalesOffValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblSalesGrp" runat="server" CssClass="cls_label_header">Sales Group</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot10" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblSalesGrpValue" runat="server" CssClass="cls_label"></asp:label></td>						                           
						                        </tr>	
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblPlant" runat="server" CssClass="cls_label_header">Plant</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot18" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPlantValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblSloc" runat="server" CssClass="cls_label_header">Storage Location</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot19" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblSlocValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblShipPoint" runat="server" CssClass="cls_label_header">Shipping Point</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot20" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblShipPointValue" runat="server" CssClass="cls_label"></asp:label></td>	
							                         <td width="20%"><asp:label id="lblSoldTo" runat="server" CssClass="cls_label_header">Soldto Party</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot11" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblSoldToValue" runat="server" CssClass="cls_label"></asp:label></td>					                        							                        
						                        </tr>
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblShipto" runat="server" CssClass="cls_label_header">Shipto Party</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot12" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblShipToValue" runat="server" CssClass="cls_label"></asp:label></td>
							                        <td width="20%"><asp:label id="lblPayerCode" runat="server" CssClass="cls_label_header">Payer Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot13" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:label id="lblPayerCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>								                      
						                         <tr>
							                        <td vAlign="top" width="20%" class="auto-style1"><asp:label id="lblPONo" runat="server" CssClass="cls_label_header">PO No</asp:label></td>
							                        <td vAlign="top" width="2%" class="auto-style1"><asp:label id="lblDot14" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" class="auto-style1"><asp:label id="lblPOnovalue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%" class="auto-style1"><asp:label id="lblPODate" runat="server" CssClass="cls_label_header">PO Date</asp:label></td>
							                        <td vAlign="top" width="2%" class="auto-style1"><asp:label id="lblDot15" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" class="auto-style1"><asp:label id="lblPODateValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblPOType" runat="server" CssClass="cls_label_header">PO Type</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot16" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPOTypeValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblPOSupp" runat="server" CssClass="cls_label_header">PO Supplement</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot17" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblPOSuppValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>						                        
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblBName" runat="server" CssClass="cls_label_header">Name of Orderer</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot21" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblBNameValue" runat="server" CssClass="cls_label"></asp:label></td>						                        
							                        <td vAlign="top" width="20%"><asp:label id="lblIntRef" runat="server" CssClass="cls_label_header">Customer / Vendor Internal Ref</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot22" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblIntRefValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrShipText1" runat="server" CssClass="cls_label_header">Header shipping text 1</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot23" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" ><asp:label id="lblHdrShipText1Value" runat="server" CssClass="cls_label"></asp:label></td>
                                                    <td vAlign="top" ><asp:label id="lblBillingNo" runat="server" CssClass="cls_label_header">Billing No</asp:label></td>
                                                    <td vAlign="top" width="2%"><asp:label id="lblDot42" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" ><asp:label id="lblBillingNoValue" runat="server" CssClass="cls_label"></asp:label>
                                                        <asp:TextBox id="txtBillingNoValue" runat="server" CssClass="cls_textbox" Enabled="False" Visible="False"></asp:TextBox>
							                        </td>						                        							                       
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrShipText2" runat="server" CssClass="cls_label_header">Header shipping text 2</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot24" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" colspan="4"><asp:label id="lblHdrShipText2Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrShipText3" runat="server" CssClass="cls_label_header">Header shipping text 3</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot25" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" colspan="4"><asp:label id="lblHdrShipText3Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrDiscType1" runat="server" CssClass="cls_label_header">Header disc. type 1</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot26" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblHdrDiscType1Value" runat="server" CssClass="cls_label"></asp:label></td>		
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrDiscVal1" runat="server" CssClass="cls_label_header">Header disc. value 1</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot27" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblHdrDiscVal1Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrDiscType2" runat="server" CssClass="cls_label_header">Header disc. type 2</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot28" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblHdrDiscType2Value" runat="server" CssClass="cls_label"></asp:label></td>		
							                        <td vAlign="top" width="20%"><asp:label id="lblHdrDiscVal2" runat="server" CssClass="cls_label_header">Header disc. value 2</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot29" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblHdrDiscVal2Value" runat="server" CssClass="cls_label"></asp:label></td>						                        							                       
						                        </tr>	
						                        <tr>
							                        <td vAlign="top"><asp:label id="lblDelDate" runat="server" CssClass="cls_label_header">Delivery Date</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDot30" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDelDateValue" runat="server" CssClass="cls_label"></asp:label></td>
							                         <td vAlign="top"><asp:label id="lblUsageInd" runat="server" CssClass="cls_label_header">Usage Indicator</asp:label></td>
							                        <td vAlign="top"><asp:label id="Labe31" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblUsageIndValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>					                        
						                        <tr>
							                        <td vAlign="top"><asp:label id="lblResendCnt" runat="server" CssClass="cls_label_header">Resubmit Counter</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDot32" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblResendCntValue" runat="server" CssClass="cls_label"></asp:label></td>
							                         <td vAlign="top"><asp:label id="lblOrderBy" runat="server" CssClass="cls_label_header">Order By</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblDot33" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top"><asp:label id="lblOrderByValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                         <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblRemarks" runat="server" CssClass="cls_label_header">Remarks</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot34" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" colspan="4"><asp:label id="lblRemarksValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>		
						                        <tr>
							                        <td vAlign="top" width="20%"><asp:label id="lblNote" runat="server" CssClass="cls_label_header">Note</asp:label></td>
							                        <td vAlign="top" width="2%"><asp:label id="lblDot38" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td vAlign="top" colspan="4"><asp:label id="lblNoteValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>							                       			
					                        </table>
				                        </td>
			                        </tr>	
			                        <tr><td>&nbsp;</td></tr>
			                        <tr>                                      
                                        <td class="Bckgroundreport" colspan="3">                                            
                                            <!--<uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>-->
			                                 <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="300px" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            AllowPaging="False" PagerSettings-Visible="false" DataKeyNames="MATNR" BorderColor="Black"
                                            BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite" OnRowDeleting="dgList_RowDeleting">
                                                 <Columns>
                                                    <asp:CommandField 
                                                    DeleteText="<img src='../../images/ico_Delete.gif'  alt='Delete' border='0'/>"
                                                    ShowDeleteButton="True" />
                                                   
                                                    <asp:BoundField DataField="POSEX" HeaderText="Line Number" ReadOnly="True" SortExpression="POSEX">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="MATNR" HeaderText="Product Code" ReadOnly="True" SortExpression="MATNR">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="KWMENG" HeaderText="Qty" ReadOnly="True" SortExpression="KWMENG">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="VRKME" HeaderText="UOM" ReadOnly="True" SortExpression="VRKME">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="PSTYV" HeaderText="Item Category" ReadOnly="True" SortExpression="PSTYV">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="KBETR" HeaderText="Manual Price Per UOM" ReadOnly="True" SortExpression="KBETR">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="ZTERM" HeaderText="Payment Term" ReadOnly="True" SortExpression="ZTERM">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="L_DISC_TYP_1" HeaderText="Line Disc. Type 1" ReadOnly="True" SortExpression="L_DISC_TYP_1">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                      <asp:BoundField DataField="L_DISC_VAL_1" HeaderText="Line Disc. Value 1" ReadOnly="True" SortExpression="L_DISC_VAL_1">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                    <asp:BoundField DataField="L_DISC_TYP_2" HeaderText="Line Disc. Type 2" ReadOnly="True" SortExpression="L_DISC_TYP_2">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                      <asp:BoundField DataField="L_DISC_VAL_2" HeaderText="Line Disc. Value 2" ReadOnly="True" SortExpression="L_DISC_VAL_2">
                                                        <itemstyle horizontalalign="Center" />
                                                    </asp:BoundField>  
                                                </Columns>                                                
                                                <FooterStyle CssClass="GridFooter" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAlternate" />
                                                <RowStyle CssClass="GridNormal" />
                                                <PagerSettings Visible="False" />
                                            </ccGV:clsGridView>                                               
					                    </TD>
				                    </TR>    
				                    <tr><td>&nbsp;</td></tr>		          
			                        <tr class="Bckgroundreport">
				                        <td align="right">
				                            &nbsp;	                        
				                        </td>
			                        </tr>
                                     <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
