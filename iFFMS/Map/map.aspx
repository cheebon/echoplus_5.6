<%@ Page Language="VB" AutoEventWireup="false" CodeFile="map.aspx.vb" Inherits="iFFMS_Map_map" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Map</title>
    <script type="text/javascript" src="http://clients.multimap.com/API/maps/1.2/dksh"></script>
    <%--<link rel="stylesheet" href="../../include/DKSH.css">	--%>
</head>
<body>
    <form id="form1" runat="server">        
         <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
         <table id="table2" cellpadding="0" cellspacing="0" width ="100%">
            <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdateCtrlPanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">        
                            <ContentTemplate>
                                <script language="javascript" src="map_02_js_control.js"></script>
                                <table id="table1" cellpadding="0" cellspacing="0" width ="100%">
                                    <tr>
                                        <td><asp:Label ID="txtScript" runat="server"></asp:Label></td>
                                    </tr>               
                                    <tr>    
                                        <td width="15%"><asp:label id="lblSalesrep" runat="server" CssClass="cls_label_header">Sales rep code</asp:label></td>
							             <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
                                        <td><asp:DropDownList ID="ddlSalesrepCode" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                    </tr>           
                                </table>
                            </ContentTemplate> 
                    </asp:UpdatePanel>         
                </td>
            </tr>
            <tr>
                <td>
                   <%-- <a href="#" onclick="showLayerTrack()" visible="false">test</a>--%>
                    <div id="wrapper">
                        <div id="right">
                            <div id="mapviewer"  style="width:800px; height:600px" ><%=txtScript.Text%></div>
                            
                        </div>
                    </div>   
                </td>
            </tr>
        </table> 
    </form>
</body>
</html>

<script language="javascript">

// FileName="Connection_odbc_conn_dsn.htm"
// Type="ADO" 
// DesigntimeType="ADO"
// HTTP="false"
// Catalog=""
// Schema=""
var MM_dksh_STRING = "dsn=SFA_DSN;UID=sa;Pwd=csscbs;"

function showLayerTrack() {
    var rsTrack__MMColParam = "10000760";
//    if (String(Request("MM_EmptyValue")) != "undefined" && 
//        String(Request("MM_EmptyValue")) != "") { 
//      rsTrack__MMColParam = String(Request("MM_EmptyValue"));
//    }

	var rsTrack = Server.CreateObject("ADODB.Recordset");
	rsTrack.ActiveConnection = MM_dksh_STRING;
	rsTrack.Source = "SELECT * FROM TBL_TXN_DAILY_TIME_SUMM WHERE salesrep_code = "+ rsTrack__MMColParam.replace(/'/g, "''") + "";
	rsTrack.CursorType = 0;
	rsTrack.CursorLocation = 2;
	rsTrack.LockType = 1;
	rsTrack.Open();
	var rsTrack_numRows = 0;
	
	var Repeat1__numRows = -1;
	var Repeat1__index = 0;
	rsTrack_numRows += Repeat1__numRows;

    while ((Repeat1__numRows-- != 0) && (!rsTrack.EOF)) 
    { 
        addposicon(rsTrack.Fields.Item("in_latitude").Value,rsTrack.Fields.Item("in_longitude").Value, rsTrack.Fields.Item("salesrep_code").Value)

        Repeat1__index++;
        rsTrack.MoveNext();
    }

    rsTrack.Close();
}
</script>
