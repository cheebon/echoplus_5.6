﻿Imports System.Data
Imports System.IO
Imports rpt_Customize

Partial Class iFFMR_Customize_SFMSEnquiry_SFMSEnquirySearch
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
    Public ReadOnly Property PageName() As String
        Get
            Return "MSSEnquirySearch.aspx"
        End Get
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            If Not Page.IsPostBack Then
                'Call Header
                With wuc_lblHeader
                    .Title = Report.GetName(SubModuleType.CUZSFMSENQUIRY)
                    .DataBind()
                    .Visible = True
                End With

                'Toolbar
                With wuc_toolbar
                    .SubModuleID = SubModuleType.CUZSFMSENQUIRY
                    If Not IsPostBack Then
                        .DataBind()
                    End If
                    .Visible = True
                End With

                'Call Paging
                With wuc_dgpaging
                    .PageCount = dgList.PageCount
                    .CurrentPageIndex = dgList.PageIndex
                    .DataBind()
                    .Visible = False
                End With
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Event"
    Protected Sub wuc_toolbar_EnqSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            RenewDataBind()

            If Master_Row_Count > 0 Then
                wuc_toolbar.GPSButtonVisibility = True
                wuc_toolbar.KMLButtonVisibility = True
            Else
                wuc_toolbar.GPSButtonVisibility = False
                wuc_toolbar.KMLButtonVisibility = False
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub wuc_toolbar_ExportBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        'With wuc_toolbar
        '    AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Search", "Search('" & .CustName & "', '" & .Address & "', '" & .District & "', '" & .CustGrp & "', '" & .CustClass & "', '" & .CustType & "', 'true');", True)
        'End With

        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
            wuc_toolbar.ExportToFile(dgList, PageName)

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

    Protected Sub wuc_toolbar_GPSBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.GPSBtn_Click
        Dim sb As StringBuilder
        Try
            sb = New StringBuilder

            With wuc_toolbar
                sb.AppendLine("CustName = '" & .CustName & "';")
                sb.AppendLine("Address = '" & .Address & "';")
                sb.AppendLine("District = '" & .District & "';")
                sb.AppendLine("CustGrpName = '" & .CustGrp & "';")
                sb.AppendLine("CustClass = '" & .CustClass & "';")
                sb.AppendLine("CustType = '" & .CustType & "';")
                sb.AppendLine("TitleCode = '" & .MSSTitle & "';")
                sb.AppendLine("StartDate = '" & .MssStartDate & "';")
                sb.AppendLine("EndDate = '" & .MssEndDate & "';")
            End With

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "SetMapVariable", sb.ToString, True)


            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShowMap", _
            '                                        "togglePopup('pnlMapPopUp', 'divModalMask', 'on', SearchCustomerTitle);", True)
            With wuc_toolbar
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShowMap", _
                                                    String.Format("$(""#MapBarIframe"", parent.document).attr('src', ""../../../iFFMR/Customize/Geomap/SFMSEnquiry/SFMSEnquiryMap.aspx?CustName=" & .CustName & "&Address=" & .Address & _
                                                    "&District=" & .District & "&CustGrpName=" & .CustGrp & "&CustClass=" & .CustClass & "&CustType=" & .CustType & "&CatCode=" & .SFMSCategory & "&SubCatCode=" & .SFMSSubCategory & _
                                                    "&StartDate=" & .SFMSStartDate & "&EndDate=" & .SFMSEndDate & "&CatName=" & .SFMSCategoryName & "&SubCatName=" & .SFMSSubCategoryName & _
                                                    "&MtdStart={0}&MtdEnd={1}&YtdStart={2}&YtdEnd={3}&NoSKUStart={4}&NoSKUEnd={5}&TeamCode={6}&SalesrepCode={7}&PrdCode={8}"")", _
                                                    .MtdSalesStartRange, .MtdSalesEndRange, .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU), True)
            End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub wuc_toolbar_KMLBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.KMLBtn_Click
        Dim strFilePath As String
        Try
            Dim clsXML As New rpt_Customize.clsSFMSEnquiryKML

            With wuc_toolbar
                strFilePath = clsXML.generateKMLFile(.CustName, .Address, .District, .CustGrp, .CustClass, .CustType, _
                                                      .SFMSCategory, .SFMSSubCategory, .SFMSStartDate, .SFMSEndDate, Session("NetValue"), DateTime.Now.ToString("yyyy-MM-dd"), .MtdSalesStartRange, _
                                                        .MtdSalesEndRange, .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU, .CustTypeName, .MTeamName, .MSalesrepName, .SKUName, .SFMSCategoryName, .SFMSSubCategoryName)
            End With

            If IO.File.Exists(strFilePath) Then
                downloadKML(strFilePath)
            Else
                lblErr.Text = "Failed to initial download KML file, file does not exists or not readable."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub downloadKML(ByVal strFilePath As String)
        Dim file As System.IO.FileInfo = New System.IO.FileInfo(strFilePath)
        If file.Exists Then
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End()
        End If
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustMSS As New clsSFMSEnqQuery

            With wuc_toolbar
                DT = clsCustMSS.SearchCustSFMS(.CustName, .Address, .District, .CustGrp, .CustClass, .CustType, .SFMSCategory, .SFMSSubCategory, .SFMSStartDate, .SFMSEndDate, Session("NetValue"), _
                                               DateTime.Now.ToString("yyyy-MM-dd"), .MtdSalesStartRange, .MtdSalesEndRange, .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU)
            End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListExport() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustMSS As New clsSFMSEnqQuery
            With wuc_toolbar
                DT = clsCustMSS.SearchCustSFMS(.CustName, .Address, .District, .CustGrp, .CustClass, .CustType, .SFMSCategory, .SFMSSubCategory, .SFMSStartDate, .SFMSEndDate, Session("NetValue"), _
                                               DateTime.Now.ToString("yyyy-MM-dd"), .MtdSalesStartRange, .MtdSalesEndRange, .YtdSalesStartRange, .YtdSalesEndRange, .NoSkuStartRange, .NoSkuEndRange, .MTeam, .MSalesrep, .SKU)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If isExport = True Then
                dtCurrentTable = GetRecListExport()
            Else
                dtCurrentTable = GetRecList()
            End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    Dim strPK As String = DirectCast(sender, GridView).DataKeys(e.Row.RowIndex).Values(0).ToString()
                    Dim strCustCode As String = DirectCast(sender, GridView).DataKeys(e.Row.RowIndex).Values(1).ToString()

                    Dim bfGPS As LinkButton = CType(e.Row.Cells(8).Controls(1), LinkButton)
                    bfGPS.OnClientClick = "NavigateAnswer('" & strPK & "', '" & strCustCode & "', 'Search');"
                    bfGPS.Attributes("href") = "#"
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Select Case e.CommandName.ToUpper
            'Case "PHOTO"
            '    'ShowCustPhotoPop()

            '    Dim rowIndex As Integer = Convert.ToInt32(e.CommandArgument)
            '    Dim strCustCode As String = dgList.DataKeys(rowIndex)(0).ToString

            '    'hfCustCode.Value = strCustCode
            '    'updPnlMaintenanceCustomerPhoto.Update()

            '    'BindCustPhoto(strCustCode)

            '    wuc_CustomerPhoto.CustCode = strCustCode
            '    wuc_CustomerPhoto.Show()
            'Case "GPS"
            '    wuc_CustomerGPS.Show()
            '    wuc_CustomerGPS.GetDefaultPrincipalLoc()
            'AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShowPhotoPopup", "map.checkResize();", True)
        End Select
    End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Standard Template"

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region
End Class
