Imports System.Data
Partial Class iFFMS_Customer_CustList_ContactDtl
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim strContactCode As String

    Public ReadOnly Property pageName() As String
        Get
            Return "CustomerContactDetail"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Call Header
        With wuc_lblheader
            .Title = "Customer Contact Details"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
        End If


    End Sub

    Sub OnPageload()
        Try

            Dim strSalesmanCode, strCustCode As String
            Dim obj As New txn_Customer.clsCustInfo
            Dim dt As DataTable = Nothing

            strSalesmanCode = Request.QueryString("salesrepcode")
            strCustCode = Request.QueryString("custcode")
            strContactCode = Request.QueryString("contcode")

            lblContactIDAns.Text = strContactCode
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "2", strContactCode)
            bindDetail(dt)
            bindPDS()
            bindBestToCall()
            UpdateDatagrid.Update()

        Catch ex As Exception
            ExceptionMsg(pageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub bindDetail(ByVal dt As DataTable)

        Try
            If dt.Rows.Count > 0 Then
                For intLoop As Integer = 0 To dt.Rows.Count - 1
                    'lblContactIDAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("cont_acc_code")), "", dt.Rows(intLoop)("cont_acc_code"))
                    'lblContactIDAns.Text = IIf(String.IsNullOrEmpty(lblContactIDAns.Text), "Not Available", lblContactIDAns.Text)

                    lblRepSpecialityAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("rep_speciality")), "", dt.Rows(intLoop)("rep_speciality"))
                    lblRepSpecialityAns.Text = IIf(String.IsNullOrEmpty(lblRepSpecialityAns.Text), "Not Available", lblRepSpecialityAns.Text)

                    lblPositionAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("position")), "", dt.Rows(intLoop)("position"))
                    lblPositionAns.Text = IIf(String.IsNullOrEmpty(lblPositionAns.Text), "Not Available", lblPositionAns.Text)

                    lblPacAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("pac")), "", dt.Rows(intLoop)("pac"))
                    lblPacAns.Text = IIf(String.IsNullOrEmpty(lblPacAns.Text), "Not Available", lblPacAns.Text)

                    lblDeptAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("department")), "", dt.Rows(intLoop)("department"))
                    lblDeptAns.Text = IIf(String.IsNullOrEmpty(lblDeptAns.Text), "Not Available", lblDeptAns.Text)

                    lblAssistantAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("assistant_secretary")), "", dt.Rows(intLoop)("assistant_secretary"))
                    lblAssistantAns.Text = IIf(String.IsNullOrEmpty(lblAssistantAns.Text), "Not Available", lblAssistantAns.Text)

                    lblLocationAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("location")), "", dt.Rows(intLoop)("location"))
                    lblLocationAns.Text = IIf(String.IsNullOrEmpty(lblLocationAns.Text), "Not Available", lblLocationAns.Text)

                    lblPhone1Ans.Text = IIf(IsDBNull(dt.Rows(intLoop)("phone1")), "", dt.Rows(intLoop)("phone1"))
                    lblPhone1Ans.Text = IIf(String.IsNullOrEmpty(lblPhone1Ans.Text), "Not Available", lblPhone1Ans.Text)

                    lblPhone2Ans.Text = IIf(IsDBNull(dt.Rows(intLoop)("phone2")), "", dt.Rows(intLoop)("phone2"))
                    lblPhone2Ans.Text = IIf(String.IsNullOrEmpty(lblPhone2Ans.Text), "Not Available", lblPhone2Ans.Text)

                    lblFaxNoAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("fax_no")), "", dt.Rows(intLoop)("fax_no"))
                    lblFaxNoAns.Text = IIf(String.IsNullOrEmpty(lblFaxNoAns.Text), "Not Available", lblFaxNoAns.Text)

                    lblPriorityAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("Priority")), "", dt.Rows(intLoop)("Priority"))
                    lblPriorityAns.Text = IIf(String.IsNullOrEmpty(lblPriorityAns.Text), "Not Available", lblPriorityAns.Text)

                    lblBedCountAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("bed_count")), "", dt.Rows(intLoop)("bed_count"))
                    lblBedCountAns.Text = IIf(String.IsNullOrEmpty(lblBedCountAns.Text), "Not Available", lblBedCountAns.Text)

                    lblPatientCapAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("patient_cap")), "", dt.Rows(intLoop)("patient_cap"))
                    lblPatientCapAns.Text = IIf(String.IsNullOrEmpty(lblPatientCapAns.Text), "Not Available", lblPatientCapAns.Text)

                    lblPracSizeAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("practice_size")), "", dt.Rows(intLoop)("practice_size"))
                    lblPracSizeAns.Text = IIf(String.IsNullOrEmpty(lblPracSizeAns.Text), "Not Available", lblPracSizeAns.Text)

                    lblPatientPerDayAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("patient_per_day")), "", dt.Rows(intLoop)("patient_per_day"))
                    lblPatientPerDayAns.Text = IIf(String.IsNullOrEmpty(lblPatientPerDayAns.Text), "Not Available", lblPatientPerDayAns.Text)

                    lblMktgPrefsAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("mktg_pref")), "", dt.Rows(intLoop)("mktg_pref"))
                    lblMktgPrefsAns.Text = IIf(String.IsNullOrEmpty(lblMktgPrefsAns.Text), "Not Available", lblMktgPrefsAns.Text)

                    lblPersonalIntAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("personal_int")), "", dt.Rows(intLoop)("personal_int"))
                    lblPersonalIntAns.Text = IIf(String.IsNullOrEmpty(lblPersonalIntAns.Text), "Not Available", lblPersonalIntAns.Text)

                    lblMedicalIntAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("med_int")), "", dt.Rows(intLoop)("med_int"))
                    lblMedicalIntAns.Text = IIf(String.IsNullOrEmpty(lblMedicalIntAns.Text), "Not Available", lblMedicalIntAns.Text)

                    lblVisitFreqAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("visit_frequency")), "", dt.Rows(intLoop)("visit_frequency"))
                    lblVisitFreqAns.Text = IIf(String.IsNullOrEmpty(lblVisitFreqAns.Text), "Not Available", lblVisitFreqAns.Text)

                    lblXField1Ans.Text = IIf(IsDBNull(dt.Rows(intLoop)("xfield1")), "", dt.Rows(intLoop)("xfield1"))
                    lblXField1Ans.Text = IIf(String.IsNullOrEmpty(lblXField1Ans.Text), "Not Available", lblXField1Ans.Text)

                    lblXField2Ans.Text = IIf(IsDBNull(dt.Rows(intLoop)("xfield2")), "", dt.Rows(intLoop)("xfield2"))
                    lblXField2Ans.Text = IIf(String.IsNullOrEmpty(lblXField2Ans.Text), "Not Available", lblXField2Ans.Text)

                    lblNotesAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("notes")), "", dt.Rows(intLoop)("notes"))
                    lblNotesAns.Text = IIf(String.IsNullOrEmpty(lblNotesAns.Text), "Not Available", lblNotesAns.Text)

                    lblStatusAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("status")), "", dt.Rows(intLoop)("status"))
                    lblStatusAns.Text = IIf(String.IsNullOrEmpty(lblStatusAns.Text), "Not Available", lblStatusAns.Text)

                    lblHospAbbvAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("hospital_abbv")), "", dt.Rows(intLoop)("hospital_abbv"))
                    lblHospAbbvAns.Text = IIf(String.IsNullOrEmpty(lblHospAbbvAns.Text), "Not Available", lblHospAbbvAns.Text)

                    lblHospRankAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("hospital_rank")), "", dt.Rows(intLoop)("hospital_rank"))
                    lblHospRankAns.Text = IIf(String.IsNullOrEmpty(lblHospRankAns.Text), "Not Available", lblHospRankAns.Text)

                    lblTitleAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("title")), "", dt.Rows(intLoop)("title"))
                    lblTitleAns.Text = IIf(String.IsNullOrEmpty(lblTitleAns.Text), "Not Available", lblTitleAns.Text)

                    lblContactNameAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("contact_name")), "", dt.Rows(intLoop)("contact_name"))
                    lblContactNameAns.Text = IIf(String.IsNullOrEmpty(lblContactNameAns.Text), "Not Available", lblContactNameAns.Text)

                    lblSurnameAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("surname")), "", dt.Rows(intLoop)("surname"))
                    lblSurnameAns.Text = IIf(String.IsNullOrEmpty(lblSurnameAns.Text), "Not Available", lblSurnameAns.Text)

                    lblFirstNameAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("first_name")), "", dt.Rows(intLoop)("first_name"))
                    lblFirstNameAns.Text = IIf(String.IsNullOrEmpty(lblFirstNameAns.Text), "Not Available", lblFirstNameAns.Text)

                    lblChristianNameAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("christian_name")), "", dt.Rows(intLoop)("christian_name"))
                    lblChristianNameAns.Text = IIf(String.IsNullOrEmpty(lblChristianNameAns.Text), "Not Available", lblChristianNameAns.Text)

                    lblGenderAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("gender")), "", dt.Rows(intLoop)("gender"))
                    lblGenderAns.Text = IIf(String.IsNullOrEmpty(lblGenderAns.Text), "Not Available", lblGenderAns.Text)

                    lblDOBAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("dob")), "", dt.Rows(intLoop)("dob"))
                    lblDOBAns.Text = IIf(String.IsNullOrEmpty(lblDOBAns.Text), "Not Available", lblGenderAns.Text)

                    lblMaritalStatusAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("marital_status")), "", dt.Rows(intLoop)("marital_status"))
                    lblMaritalStatusAns.Text = IIf(String.IsNullOrEmpty(lblMaritalStatusAns.Text), "Not Available", lblMaritalStatusAns.Text)

                    lblMedSchoolAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("med_school")), "", dt.Rows(intLoop)("med_school"))
                    lblMedSchoolAns.Text = IIf(String.IsNullOrEmpty(lblMedSchoolAns.Text), "Not Available", lblMedSchoolAns.Text)

                    lblGradDateAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("grad_date")), "", dt.Rows(intLoop)("grad_date"))
                    lblGradDateAns.Text = IIf(String.IsNullOrEmpty(lblGradDateAns.Text), "Not Available", lblGradDateAns.Text)

                    lblEmailAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("email")), "", dt.Rows(intLoop)("email"))
                    lblEmailAns.Text = IIf(String.IsNullOrEmpty(lblEmailAns.Text), "Not Available", lblEmailAns.Text)

                    lblPhoneAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("phone")), "", dt.Rows(intLoop)("phone"))
                    lblPhoneAns.Text = IIf(String.IsNullOrEmpty(lblPhoneAns.Text), "Not Available", lblPhoneAns.Text)

                    lblMobileAns.Text = IIf(IsDBNull(dt.Rows(intLoop)("mobile")), "", dt.Rows(intLoop)("mobile"))
                    lblMobileAns.Text = IIf(String.IsNullOrEmpty(lblMobileAns.Text), "Not Available", lblMobileAns.Text)
                    'ddlBTCall.Items.Add(dt.Rows(intLoop)("").ToString)
                    'ddlPDSPR.Items.Add(dt.Rows(intLoop)("").ToString)
                Next
            Else
                lblRepSpecialityAns.Text = "Not Available"
                lblPositionAns.Text = "Not Available"
                lblPacAns.Text = "Not Available"
                lblDeptAns.Text = "Not Available"
                lblAssistantAns.Text = "Not Available"
                lblLocationAns.Text = "Not Available"
                lblPhone1Ans.Text = "Not Available"
                lblPhone2Ans.Text = "Not Available"
                lblFaxNoAns.Text = "Not Available"
                lblPriorityAns.Text = "Not Available"
                lblBedCountAns.Text = "Not Available"
                lblPatientCapAns.Text = "Not Available"
                lblPracSizeAns.Text = "Not Available"
                lblMktgPrefsAns.Text = "Not Available"
                lblPersonalIntAns.Text = "Not Available"
                lblMedicalIntAns.Text = "Not Available"
                lblVisitFreqAns.Text = "Not Available"
                lblXField1Ans.Text = "Not Available"
                lblXField2Ans.Text = "Not Available"
                lblNotesAns.Text = "Not Available"
                lblStatusAns.Text = "Not Available"
                lblHospAbbvAns.Text = "Not Available"
                lblHospRankAns.Text = "Not Available"
                lblTitleAns.Text = "Not Available"
                lblContactNameAns.Text = "Not Available"
                lblSurnameAns.Text = "Not Available"
                lblFirstNameAns.Text = "Not Available"
                lblChristianNameAns.Text = "Not Available"
                lblGenderAns.Text = "Not Available"
                lblDOBAns.Text = "Not Available"
                lblMaritalStatusAns.Text = "Not Available"
                lblMedSchoolAns.Text = "Not Available"
                lblGradDateAns.Text = "Not Available"
                lblEmailAns.Text = "Not Available"
                lblPhoneAns.Text = "Not Available"
                lblMobileAns.Text = "Not Available"
            End If


        Catch ex As Exception
            ExceptionMsg(pageName & ".bindDetail : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub bindPDS()
        Dim dt As DataTable
        Dim strSalesmanCode, strCustCode As String
        Dim obj As New txn_Customer.clsCustInfo
        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getPdsDT(strSalesmanCode, strCustCode, strContactCode)
            If dt.Rows.Count > 0 Then
                ddlPDSPR.DataSource = dt
                ddlPDSPR.DataTextField = "pr"
                ddlPDSPR.DataBind()
            Else
                ddlPDSPR.Items.Add("None")
            End If
        Catch ex As Exception
            ExceptionMsg(pagename & ".bindPDS : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub bindBestToCall()
        Dim dt As DataTable
        Dim strSalesmanCode, strCustCode As String
        Dim obj As New txn_Customer.clsCustInfo
        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getBestTocallDT(strSalesmanCode, strCustCode, strContactCode)
            If dt.Rows.Count > 0 Then
                ddlBTCall.DataSource = dt
                ddlBTCall.DataTextField = "call"
                ddlBTCall.DataBind()
            Else
                ddlBTCall.Items.Add("None")
            End If
        Catch ex As Exception
            ExceptionMsg(pagename & ".bindBestToCall : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageload()
            TimerControl1.Enabled = False
        End If

    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        'Response.Redirect("../Customer/CustomerContactGeneralInfo.aspx", False)
    End Sub
End Class




