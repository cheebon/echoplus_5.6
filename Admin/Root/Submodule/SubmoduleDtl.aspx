﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SubmoduleDtl.aspx.vb" Inherits="Admin_Root_Submodule_SubmoduleDtl" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Submodule Detail</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmSubmoduleDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /><br />
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <!--Begin Modify Here -->
                <div id="MainFrame" class="MainFrame">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                        <tr>
                            <td align="left">
                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 0px 15px 0px;">
                                            <tr>
                                                <td>
                                                    <div class="border_std">
                                                    <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="False"
                                                        UseSubmitBehavior="false" Width="60px" />
                                                    </div>
                                                </td>
                                            </tr>
                                           
                                            <tr class="ajax__tab_portal">
                                                <td class="ajax__tab_PNL">
                                                <div class="border_std">
                                                    <customToolkit:wuc_lblInfo ID="lblErrDtl" runat="server" />
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnEditDtl" runat="server" Text="Edit" CausesValidation="False"
                                                                    Width="60px" />&nbsp;
                                                                <asp:Button ID="btnSaveDtl" runat="server" Text="Save" CausesValidation="true"
                                                                    ValidationGroup="ValidateDtl" Width="60px" />&nbsp;
                                                                <asp:Button ID="btnCancelDtl" runat="server" Text="Cancel"
                                                                    CausesValidation="false" Width="60px" />&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr valign="top">
                                                            <td style="width:70%">
                                                                <asp:DetailsView ID="dvHdr_Left" runat="server" AutoGenerateRows="False" Width="100%"
                                                                    DataKeyNames="SUBMODULE_ID">                                                                    
                                                                    <Fields>
                                                                        <asp:TemplateField HeaderText="Submodule ID">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleID" runat="server" Enabled="false" Text='<%# Bind("SUBMODULE_ID") %>' MaxLength="50" />
                                                                            
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleID" runat="server" Enabled="false" Text='[Auto Generate]' MaxLength="50" />
                                                                                
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubmoduleID" runat="server" Text='<%# Bind("SUBMODULE_ID") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Module Code">
                                                                            <EditItemTemplate>
                                                                                <asp:DropDownList ID="ddlModuleCode" runat="server" />
                                                                                <asp:RequiredFieldValidator ID="rfvModuleCode" runat="server" ControlToValidate="ddlModuleCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:DropDownList ID="ddlModuleCode" runat="server" />
                                                                                <asp:RequiredFieldValidator ID="rfvModuleCode" runat="server" ControlToValidate="ddlModuleCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblModuleCode" runat="server" Text='<%# Bind("MODULE_CODE") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Submodule Code">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleCode" runat="server" Text='<%# Bind("SUBMODULE_CODE") %>' MaxLength="50" Enabled="false" />
                                                                                <asp:RequiredFieldValidator ID="rfvSubmoduleCode" runat="server" ControlToValidate="txtSubmoduleCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleCode" runat="server" Text="" MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvSubmoduleCode" runat="server" ControlToValidate="txtSubmoduleCode"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubmoduleCode" runat="server" Text='<%# Bind("SUBMODULE_CODE") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Submodule Name">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleName" runat="server" Text='<%# Bind("SUBMODULE_NAME") %>' MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvSubmoduleName" runat="server" ControlToValidate="txtSubmoduleName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleName" runat="server" Text="" MaxLength="50" />
                                                                                <asp:RequiredFieldValidator ID="rfvSubmoduleName" runat="server" ControlToValidate="txtSubmoduleName"
                                                                                    ErrorMessage="*" Display="Dynamic" CssClass="cls_validator"
                                                                                    ValidationGroup="ValidateDtl" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubmoduleName" runat="server" Text='<%# Bind("SUBMODULE_NAME") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Submodule Name 1">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleName1" runat="server" Text='<%# Bind("SUBMODULE_NAME_1") %>' MaxLength="50" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleName1" runat="server" Text="" MaxLength="50" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubmoduleName1" runat="server" Text='<%# Bind("SUBMODULE_NAME_1") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Submodule Desc">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleDesc" runat="server" Text='<%# Bind("SUBMODULE_DESC") %>' MaxLength="100" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleDesc" runat="server" Text="" MaxLength="100" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubmoduleDesc" runat="server" Text='<%# Bind("SUBMODULE_DESC") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Submodule Desc 1">
                                                                            <EditItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleDesc1" runat="server" Text='<%# Bind("SUBMODULE_DESC_1") %>' MaxLength="100" />
                                                                            </EditItemTemplate>
                                                                            <InsertItemTemplate>
                                                                                <asp:TextBox ID="txtSubmoduleDesc1" runat="server" Text="" MaxLength="100" />
                                                                            </InsertItemTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSubmoduleDesc1" runat="server" Text='<%# Bind("SUBMODULE_DESC_1") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Fields>
                                                                </asp:DetailsView>
                                                                <br />
                                                                <asp:Panel ID="pnlAction" runat="server">                                                               
                                                                 <ccGV:clsGridView ID="dgActionList" runat="server" ShowFooter="false" AllowSorting="True"
                                                                    AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="100%"
                                                                    AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                                    FreezeRows="0" GridWidth="" AllowPaging="False" PagerSettings-Visible="false" Visible="true">
                                                                    <Columns>
                                                                        <asp:TemplateField headertext = "View">
                                                                            <headertemplate>
                                                                               View
                                                                            </headertemplate>
                                                                            <headerstyle horizontalalign="Left" />
                                                                            <ItemTemplate>                                               
                                                                                 <asp:CheckBox runat="server"  CssClass="cls_checkbox" ID="ViewCheckBox" checked='<%# CONTAINER.DATAITEM("VIEW_ACTION") %>' Enabled='<%# CONTAINER.DATAITEM("ENABLED") %>'/>                                      
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField> 
					                                                    <asp:TemplateField headertext = "Create">
					                                                        <headertemplate>
					                                                        Create
                                                                            </headertemplate>
                                                                            <headerstyle horizontalalign="Left" />
                                                                            <ItemTemplate>
                                                                                 <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="CreateCheckBox"  checked='<%# CONTAINER.DATAITEM("CREATE_ACTION") %>' Enabled='<%# CONTAINER.DATAITEM("ENABLED") %>'/>                                                
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField> 
                                                                        <asp:TemplateField headertext = "Edit">
                                                                            <headertemplate>
                                                                               Edit
                                                                            </headertemplate>
                                                                            <headerstyle horizontalalign="Left" />
                                                                            <ItemTemplate>
                                                                                 <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="EditCheckBox" checked='<%# CONTAINER.DATAITEM("EDIT_ACTION") %>' Enabled='<%# CONTAINER.DATAITEM("ENABLED") %>' />                                                
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField> 
                                                                        <asp:TemplateField headertext = "Delete">
                                                                            <headertemplate>
                                                                               Delete
                                                                            </headertemplate>
                                                                            <headerstyle horizontalalign="Left" />
                                                                            <ItemTemplate>                                                
                                                                                 <asp:CheckBox runat="server" CssClass="cls_checkbox" ID="DeleteCheckBox" checked='<%# CONTAINER.DATAITEM("DELETE_ACTION") %>' Enabled='<%# CONTAINER.DATAITEM("ENABLED") %>' />                                                
                                                                            </ItemTemplate>
                                                         
                                                                        </asp:TemplateField> 
                                                                    </Columns>                                                
                                                                     <FooterStyle CssClass="GridFooter" />
                                                                     <HeaderStyle CssClass="GridHeader" />
                                                                     <AlternatingRowStyle CssClass="GridAlternate" />
                                                                     <RowStyle CssClass="GridNormal" />
                                                                     <PagerSettings Visible="False" />
                                                                </ccGV:clsGridView>
                                                                 </asp:Panel>
                                                            </td>
                                                                                                                   
                                                        </tr>                                                        
                                                    </table>
                                                </div>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--End Modify Here -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
