﻿Imports System.Data
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json

Partial Class Admin_BCP_BCPTemplateList
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function GetTemplateList() As String
        Dim dtResults As DataTable = Nothing
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            dtResults = .GetBCPTemplateList
        End With

        Return JsonConvert.SerializeObject(dtResults)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function DeleteTemplateList(ByVal strBCPTemplateID As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            .DeleteBCPTemplate(strBCPTemplateID)
        End With
        Return JsonConvert.SerializeObject(True)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function GetBCPTemplateDuplicate(ByVal strBCPTemplateName As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        Dim dt As New DataTable
        With objBCP
            dt = .GetBCPTemplateDuplicate(strBCPTemplateName)
        End With
        Return JsonConvert.SerializeObject(dt)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function CreateBCPTemplate(ByVal strBCPTemplateName As String, ByVal strBCPTemplateDesc As String, ByVal strInSeq As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            .InsertBCPTemplate(strBCPTemplateName, strBCPTemplateDesc, strInSeq, HttpContext.Current.Session("UserID"))
        End With
        Return JsonConvert.SerializeObject(True)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function UpdateBCPTemplate(ByVal strBCPTemplateID As String, ByVal strBCPTemplateDesc As String, ByVal strInSeq As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            .UpdateBCPTemplate(strBCPTemplateID, strBCPTemplateDesc, strInSeq, HttpContext.Current.Session("UserID"))
        End With
        Return JsonConvert.SerializeObject(True)
    End Function




    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function GetStepList(strBCPTemplateID As String) As String
        Dim dtResults As DataTable = Nothing
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            dtResults = .GetBCPStepList(strBCPTemplateID)
        End With

        Return JsonConvert.SerializeObject(dtResults)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function GetModuleDDL() As String
        Dim objBCP As New adm_bcp.clsBCP
        Dim dt As New DataTable
        With objBCP
            dt = .GetModuleDDL()
        End With
        Return JsonConvert.SerializeObject(dt)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function GetStepDtl(strBCPTemplateID As String, strStepNo As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        Dim dt As New DataTable
        With objBCP
            dt = .GetStepDtl(strBCPTemplateID, strStepNo)
        End With
        Return JsonConvert.SerializeObject(dt)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function GetStepModuleDtl(strBCPTemplateID As String, strStepNo As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        Dim dt As New DataTable
        With objBCP
            dt = .GetStepModuleDtl(strBCPTemplateID, strStepNo)
        End With
        Return JsonConvert.SerializeObject(dt)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function DeleteBCPStepModule(ByVal strBCPTemplateID As String, ByVal strStepNo As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            .DeleteBCPStepModule(strBCPTemplateID, strStepNo, HttpContext.Current.Session("UserID"))
        End With
        Return JsonConvert.SerializeObject(True)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function InsertBCPStepModule(ByVal strBCPTemplateID As String, ByVal strStepName As String, ByVal strStepDesc As String, ByVal strMandStep As String, ByVal strModuleIdList As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            .InsertBCPStepModule(strBCPTemplateID, strStepName, strStepDesc, strMandStep, strModuleIdList, HttpContext.Current.Session("UserID"))
        End With
        Return JsonConvert.SerializeObject(True)
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function UpdateBCPStepModule(ByVal strBCPTemplateID As String, ByVal strStepNo As String, ByVal strStepName As String, ByVal strStepDesc As String, ByVal strMandStep As String, ByVal strModuleIdList As String) As String
        Dim objBCP As New adm_bcp.clsBCP
        With objBCP
            .UpdateBCPStepModule(strBCPTemplateID, strStepNo, strStepName, strStepDesc, strMandStep, strModuleIdList, HttpContext.Current.Session("UserID"))
        End With
        Return JsonConvert.SerializeObject(True)
    End Function
End Class
