'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/10/2006
'	Purpose	    :	User Create
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_User_UserCreate
    Inherits System.Web.UI.Page

    Public Property dtPassLib() As DataTable
        Get
            Return ViewState("dtPassLib")
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtPassLib") = value
        End Set
    End Property

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strUserCode As String
        Dim objUserQuery As adm_User.clsUserQuery
        Dim dtAR As DataTable
        
        Try
            If Session("UserID") = "" Then
                Dim strScript As String = ""
                strScript = "self.parent.parent.location='../../../../../../" & ConfigurationManager.AppSettings("ServerName") & "/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
                'strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
                Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>" & strScript & "</script>")
            End If

            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Creating User"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant
                strUserCode = Trim(Session("UserCode"))

                'request
                txtSearchType.Text = Trim(Request.QueryString("search_type"))
                txtSearchValue.Text = Trim(Request.QueryString("search_value"))

                'Prepare Recordset,Createobject as needed
                'Call User Type
                With Wuc_ddlUserType
                    .RequiredValidation = True
                    .AutoPostBack = True
                    .DataBind()
                    .Visible = True
                End With

                'Call Language
                With Wuc_ddlLanguage
                    .RequiredValidation = True
                    .AutoPostBack = False
                    .DataBind()
                    .Visible = True
                End With

                'Call Operation Type
                With Wuc_ddlOperationType
                    .RequiredValidation = False
                    .AutoPostBack = False
                    .DataBind()
                    .Visible = True
                End With

                'Call Country
                With Wuc_ddlCountry
                    .UserCode = strUserCode
                    .RequiredValidation = True
                    .AutoPostBack = True
                    .DataBind()
                    .Visible = True
                End With

                'Call Principal
                With Wuc_ddlPrincipal
                    .CountryCode = Wuc_ddlCountry.SelectedValue
                    .UserCode = strUserCode
                    .RequiredValidation = True
                    .AutoPostBack = True
                    .DataBind()
                    .Visible = True
                End With

                'Call Branch
                With Wuc_ddlBranch
                    .CountryCode = Wuc_ddlCountry.SelectedValue
                    .RequiredValidation = False
                    .AutoPostBack = False
                    .DataBind()
                    .Visible = True
                End With

                'Call Sales Team
                With Wuc_ddlSalesTeam
                    .PrincipalCode = Wuc_ddlPrincipal.SelectedValue
                    .RequiredValidation = False
                    .AutoPostBack = True
                    .DataBind()
                    .Visible = True
                End With

                'Call Sales Rep
                With Wuc_ddlSalesRep
                    .SalesTeamCode = Wuc_ddlSalesTeam.SelectedValue
                    .RequiredValidation = False
                    .AutoPostBack = False
                    .DataBind()
                    .Visible = True
                End With

                'Bind Access Rights
                objUserQuery = New adm_User.clsUserQuery
                With objUserQuery
                    .clsProperties.user_code = strUserCode
                    dtAR = .GetAccessRightListByUserCode
                End With

                With lstAccessRightFrom
                    .DataSource = dtAR
                    .DataTextField = "accessright_name"
                    .DataValueField = "accessright_id"
                    .DataBind()
                End With

                'Bind SalesRep
                BindSR()

            End If

        Catch ex As Exception
            ExceptionMsg("UserCreate.Page_Load : " & ex.ToString)
        Finally
            objUserQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlCountry_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlCountry.SelectedIndexChanged
        Dim ary As Array

        Try
            ary = Split(Wuc_ddlCountry.SelectedValue, "@")

            'Call Principal
            With Wuc_ddlPrincipal
                If Wuc_ddlCountry.SelectedValue <> "0" Then
                    .CountryCode = ary(1)
                Else
                    .CountryCode = ary(0) 'Wuc_ddlCountry.SelectedValue
                End If
                .UserCode = Trim(Session("UserCode"))
                .RequiredValidation = True
                .AutoPostBack = True
                .DataBind()
                .Visible = True
            End With

            'Call Branch
            With Wuc_ddlBranch
                If Wuc_ddlCountry.SelectedValue <> "0" Then
                    .CountryCode = ary(1) 'Wuc_ddlCountry.SelectedValue
                Else
                    .CountryCode = ary(0) 'Wuc_ddlCountry.SelectedValue
                End If
                .RequiredValidation = False
                .AutoPostBack = False
                .DataBind()
                .Visible = True
            End With

        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlCountry_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlPrincipal_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlPrincipal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlPrincipal.SelectedIndexChanged
        Dim ary As Array

        Try
            ary = Split(Wuc_ddlPrincipal.SelectedValue, "@")

            'Call Sales Team
            With Wuc_ddlSalesTeam
                If Wuc_ddlPrincipal.SelectedValue <> "0" Then
                    .PrincipalCode = ary(1)
                Else
                    .PrincipalCode = ary(0) 'Wuc_ddlPrincipal.SelectedValue
                End If
                .RequiredValidation = False
                .AutoPostBack = True
                .DataBind()
                .Visible = True
            End With
        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlPrincipal_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlSalesTeam_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlSalesTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlSalesTeam.SelectedIndexChanged
        Dim ary As Array

        Try
            ary = Split(Wuc_ddlSalesTeam.SelectedValue, "@")

            'Call Sales Rep
            With Wuc_ddlSalesRep
                If Wuc_ddlSalesTeam.SelectedValue <> "0" Then
                    .SalesTeamCode = ary(1)
                Else
                    .SalesTeamCode = ary(0) 'Wuc_ddlSalesTeam.SelectedValue
                End If
                .RequiredValidation = False
                .AutoPostBack = False
                .DataBind()
                .Visible = True
            End With
        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlSalesTeam_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Wuc_ddlUserType_SelectedIndexChanged
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Wuc_ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Wuc_ddlUserType.SelectedIndexChanged
        Dim aryUT, aryCountry, aryST As Array
        Dim strUserType As String

        Try
            aryUT = Split(Wuc_ddlUserType.SelectedValue, "@")
            aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            aryST = Split(Wuc_ddlSalesTeam.SelectedValue, "@")
            strUserType = IIf(aryUT.Length > 0, aryUT(1), aryUT(0))

            'Call Operation Type
            With Wuc_ddlOperationType
                .RequiredValidation = False
                .AutoPostBack = False
                .DataBind()
                .Visible = True
                .SelectedValue = IIf(Trim(strUserType) <> "SR", 0, Wuc_ddlOperationType.SelectedValue)
                .SetEnable = True 'IIf(Trim(strUserType) <> "SR", False, True)
            End With

            'Call Branch
            With Wuc_ddlBranch
                If Wuc_ddlCountry.SelectedValue = "0" Then
                    .CountryCode = Wuc_ddlCountry.SelectedValue
                Else
                    .CountryCode = aryCountry(1)
                End If
                .RequiredValidation = False
                .AutoPostBack = False
                .DataBind()
                .Visible = True
                .SelectedValue = IIf(Trim(strUserType) <> "SR", 0, Wuc_ddlBranch.SelectedValue)
                .SetEnable = True 'IIf(Trim(strUserType) <> "SR", False, True)
            End With

            'Call Sales Rep
            With Wuc_ddlSalesRep
                If Wuc_ddlSalesTeam.SelectedValue = "0" Then
                    .SalesTeamCode = Wuc_ddlSalesTeam.SelectedValue
                Else
                    .SalesTeamCode = aryST(1)
                End If
                .RequiredValidation = IIf(Trim(strUserType) <> "SR", False, True)
                .AutoPostBack = False
                .DataBind()
                .Visible = True
                .SelectedValue = IIf(Trim(strUserType) <> "SR", 0, Wuc_ddlSalesRep.SelectedValue)
                .SetEnable = True 'IIf(Trim(strUserType) <> "SR", False, True)
            End With

        Catch ex As Exception
            ExceptionMsg("UserCreate.Wuc_ddlUserType_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnReset_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Dim i As Integer

        Try
            ddlStatusID.SelectedValue = 0
            txtLogin.Text = ""
            txtPassword.Text = ""
            txtConfirmedPassword.Text = ""
            txtUserCode.Text = ""
            txtUserName.Text = ""
            txtUserName1.Text = ""
            Wuc_ddlUserType.SelectedValue = 0
            Wuc_ddlCountry.SelectedValue = 0
            Wuc_ddlPrincipal.SelectedValue = 0
            Wuc_ddlBranch.SelectedValue = 0
            Wuc_ddlSalesTeam.SelectedValue = 0
            Wuc_ddlSalesRep.SelectedValue = 0
            Wuc_ddlLanguage.SelectedValue = 0
            Wuc_ddlOperationType.SelectedValue = 0
            txtEmail.Text = ""
            txtNewCustNo.Text = ""
            txtDummyCustNo.Text = ""
            txtIDPrefix.Text = ""
            txtPwdExpiry.Text = ConfigurationManager.AppSettings("PwdExpiryDays")
            chkLoginExpiry.Checked = False
            txtMaxRetry.Text = ConfigurationManager.AppSettings("MaxRetry")
            txtPageSize.Text = 20

            For i = 0 To lstAccessRightTo.Items.Count - 1
                lstAccessRightFrom.Items.Add(New ListItem(lstAccessRightTo.Items(i).Text, lstAccessRightTo.Items(i).Value))
            Next
            lstAccessRightTo.Items().Clear()
            SortItem(lstAccessRightFrom)

            lstSRFrom.Items.Clear()
            lstSRTo.Items.Clear()

        Catch ex As Exception
            ExceptionMsg("UserCreate.btnReset_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSave_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objUser As adm_User.clsUser
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String
        Dim aryUT, aryCountry, aryPrincipal, aryST, arySR, aryLanguage, aryBranch, aryOT As Array
        Dim intCnt As Integer
        Dim dt As DataTable

        Try
            'Check login or user code exist or not
            objUserQuery = New adm_User.clsUserQuery
            With objUserQuery
                .clsProperties.login = Trim(txtLogin.Text)
                .clsProperties.user_code = Trim(txtUserCode.Text)
                .clsProperties.user_id = "0"
                dt = .GetUserDuplicate
                intCnt = IIf(dt.Rows(0)("cnt") = Nothing, 0, dt.Rows(0)("cnt"))
            End With
            objUserQuery = Nothing

            If intCnt > 0 Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Login / User Code already exists!');</script>")
                Exit Sub
            End If

            'Check Password match
            If Trim(txtPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtPassword.Text <> "") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                Exit Sub
            End If

            'Encrypt Password
            objCrypto = New cor_Crypto.clsEncryption
            With objCrypto
                strEncryptPwd = .TripleDESEncrypt(Trim(txtPassword.Text))
            End With

            'Get User Control Value
            aryUT = Split(Wuc_ddlUserType.SelectedValue, "@")
            aryCountry = Split(Wuc_ddlCountry.SelectedValue, "@")
            aryPrincipal = Split(Wuc_ddlPrincipal.SelectedValue, "@")
            aryST = Split(Wuc_ddlSalesTeam.SelectedValue, "@")
            arySR = Split(Wuc_ddlSalesRep.SelectedValue, "@")
            aryLanguage = Split(Wuc_ddlLanguage.SelectedValue, "@")
            aryBranch = Split(Wuc_ddlBranch.SelectedValue, "@")
            aryOT = Split(Wuc_ddlOperationType.SelectedValue, "@")

            'Insert into User Table
            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_type_id = Trim(aryUT(0))
                .clsProperties.principal_id = Trim(aryPrincipal(0))
                .clsProperties.country_id = Trim(aryCountry(0))
                .clsProperties.salesteam_id = Trim(aryST(0))
                If Wuc_ddlSalesTeam.SelectedValue <> "0" Then
                    .clsProperties.salesteam_code = aryST(1)
                Else
                    .clsProperties.salesteam_code = ""
                End If
                .clsProperties.salesrep_id = Trim(arySR(0))
                If Wuc_ddlSalesRep.SelectedValue <> "0" Then
                    .clsProperties.salesrep_code = arySR(1)
                Else
                    .clsProperties.salesrep_code = ""
                End If
                .clsProperties.region_id = 0
                .clsProperties.region_code = Nothing
                .clsProperties.area_id = 0
                .clsProperties.area_code = Nothing
                .clsProperties.language_id = Trim(aryLanguage(0))
                .clsProperties.branch_id = Trim(aryBranch(0))
                If Wuc_ddlBranch.SelectedValue <> "0" Then
                    .clsProperties.branch_code = aryBranch(1)
                Else
                    .clsProperties.branch_code = ""
                End If
                .clsProperties.login = Trim(txtLogin.Text)
                .clsProperties.pwd = strEncryptPwd
                .clsProperties.user_code = Trim(txtUserCode.Text)
                .clsProperties.user_name = Trim(txtUserName.Text)
                .clsProperties.user_name_1 = Trim(txtUserName1.Text)
                .clsProperties.email = Trim(txtEmail.Text)
                .clsProperties.pwd_expiry = CInt(Trim(txtPwdExpiry.Text))
                .clsProperties.login_expiry_flag = IIf(chkLoginExpiry.Checked, 1, 0)
                .clsProperties.max_retry = CInt(Trim(txtMaxRetry.Text))
                .clsProperties.page_size = CInt(txtPageSize.Text)
                .clsProperties.nv_flag = IIf(chkNVFlag.Checked, 1, 0)
                .clsProperties.default_nv = CInt(ddlDefaultNV.SelectedValue)
                .clsProperties.operation_type_id = Trim(aryOT(0))
                If Wuc_ddlOperationType.SelectedValue <> "0" Then
                    .clsProperties.operation_type_code = aryOT(1)
                Else
                    .clsProperties.operation_type_code = ""
                End If
                .clsProperties.new_cust_no = Trim(txtNewCustNo.Text)
                .clsProperties.dummy_cust_no = Trim(txtDummyCustNo.Text)
                .clsProperties.id_prefix = Trim(txtIDPrefix.Text)
                .clsProperties.pwd_changed_date = Nothing '.ToString
                .clsProperties.created_date = Now '.ToString
                .clsProperties.creator_user_id = Session("UserID")
                .clsProperties.changed_date = Nothing
                .clsProperties.changed_user_id = 0
                .clsProperties.delete_flag = 0
                .clsProperties.active_flag = ddlStatusID.SelectedValue
                .clsProperties.liAR = lstAccessRightTo.Items
                .clsProperties.liSR = lstSRTo.Items
                .Create()
            End With
            Response.Redirect("UserDtl.aspx?user_code=" & Trim(txtUserCode.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("UserCreate.btnSave_Click : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkAdd_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Dim i As Integer
        Dim li As ListItem

        Try
            If lstAccessRightFrom.SelectedIndex <> -1 Then
                Do
                    If lstAccessRightFrom.Items(i).Selected Then
                        li = lstAccessRightTo.Items.FindByValue(lstAccessRightFrom.Items(i).Value)
                        If IsNothing(li) Then
                            lstAccessRightTo.Items.Add(New ListItem(lstAccessRightFrom.Items(i).Text, lstAccessRightFrom.Items(i).Value))
                        End If
                        lstAccessRightFrom.Items.RemoveAt(i)
                    Else
                        i = i + 1
                    End If
                Loop Until i = lstAccessRightFrom.Items.Count
            End If

            SortItem(lstAccessRightTo)
            BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkAdd_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkRemove_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemove.Click
        Dim li As ListItem
        Dim i As Integer

        Try
            If lstAccessRightTo.SelectedIndex <> -1 Then
                Do
                    If lstAccessRightTo.Items(i).Selected Then
                        li = lstAccessRightFrom.Items.FindByValue(lstAccessRightTo.Items(i).Value)
                        If IsNothing(li) Then
                            lstAccessRightFrom.Items.Add(New ListItem(lstAccessRightTo.Items(i).Text, lstAccessRightTo.Items(i).Value))
                        End If
                        lstAccessRightTo.Items.RemoveAt(i)
                    Else
                        i = i + 1
                    End If
                Loop Until i = lstAccessRightTo.Items.Count
            End If

            SortItem(lstAccessRightFrom)
            BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkRemove_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkAddAll_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAll.Click
        Dim i As Integer

        Try
            For i = 0 To lstAccessRightFrom.Items.Count - 1
                lstAccessRightTo.Items.Add(New ListItem(lstAccessRightFrom.Items(i).Text, lstAccessRightFrom.Items(i).Value))
            Next
            lstAccessRightFrom.Items.Clear()
            SortItem(lstAccessRightTo)
            BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkAddAll_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkRemoveAll_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAll.Click
        Dim i As Integer

        Try
            For i = 0 To lstAccessRightTo.Items.Count - 1
                lstAccessRightFrom.Items.Add(New ListItem(lstAccessRightTo.Items(i).Text, lstAccessRightTo.Items(i).Value))
            Next
            lstAccessRightTo.Items().Clear()
            SortItem(lstAccessRightFrom)
            BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkRemoveAll_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkAdd1_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd1.Click
        Dim i As Integer = 0
        Dim li As ListItem

        Try
            If lstSRFrom.SelectedIndex <> -1 Then
                Do
                    If lstSRFrom.Items(i).Selected Then
                        li = lstSRTo.Items.FindByValue(lstSRFrom.Items(i).Value)
                        If IsNothing(li) Then
                            lstSRTo.Items.Add(New ListItem(lstSRFrom.Items(i).Text, lstSRFrom.Items(i).Value))
                        End If
                        lstSRFrom.Items.RemoveAt(i)
                    Else
                        i = i + 1
                    End If
                Loop Until i = lstSRFrom.Items.Count
            End If

            SortItem(lstSRTo)
            'BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkAdd1_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkRemove1_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkRemove1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemove1.Click
        Dim li As ListItem
        Dim i As Integer

        Try
            If lstSRTo.SelectedIndex <> -1 Then
                Do
                    If lstSRTo.Items(i).Selected Then
                        li = lstSRFrom.Items.FindByValue(lstSRTo.Items(i).Value)
                        If IsNothing(li) Then
                            lstSRFrom.Items.Add(New ListItem(lstSRTo.Items(i).Text, lstSRTo.Items(i).Value))
                        End If
                        lstSRTo.Items.RemoveAt(i)
                    Else
                        i = i + 1
                    End If
                Loop Until i = lstSRTo.Items.Count
            End If

            SortItem(lstSRFrom)
            'BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkRemove1_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkAddAll1_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkAddAll1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAll1.Click
        Dim i As Integer

        Try
            For i = 0 To lstSRFrom.Items.Count - 1
                lstSRTo.Items.Add(New ListItem(lstSRFrom.Items(i).Text, lstSRFrom.Items(i).Value))
            Next
            lstSRFrom.Items.Clear()
            SortItem(lstSRTo)
            'BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkAddAll1_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub lnkRemoveAll1_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub lnkRemoveAll1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAll1.Click
        Dim i As Integer

        Try
            For i = 0 To lstSRTo.Items.Count - 1
                lstSRFrom.Items.Add(New ListItem(lstSRTo.Items(i).Text, lstSRTo.Items(i).Value))
            Next
            lstSRTo.Items().Clear()
            SortItem(lstSRFrom)
            'BindSR()

        Catch ex As Exception
            ExceptionMsg("UserCreate.lnkRemoveAll1_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub SortItem
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Public Sub SortItem(ByVal lst As ListBox)
        Dim i As Integer
        Dim liTemp As ListItem

        Try
            Do
                If i + 1 <= lst.Items.Count - 1 Then
                    If lst.Items(i).Text > lst.Items(i + 1).Text Then
                        liTemp = lst.Items(i)
                        lst.Items.RemoveAt(i)
                        lst.Items.Insert(i + 1, liTemp)
                        i = 0
                    Else
                        i = i + 1
                    End If
                End If
            Loop While i < lst.Items.Count - 1

            'For i = 0 To lst.Items.Count - 1
            '    If i <> lst.Items.Count - 1 Then
            '        If lst.Items(i).Text > lst.Items(i + 1).Text Then
            '            liTemp = lst.Items(i)
            '            lst.Items.RemoveAt(i)
            '            lst.Items.Insert(i + 1, liTemp)
            '        End If
            '    End If
            'Next
        Catch ex As Exception
            ExceptionMsg("UserCreate.SortItem : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub BindSR
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub BindSR()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim strAccessRightID As String
        Dim dt As DataTable
        Dim i As Integer = 0
        Dim y As Integer = 0
        Dim drCurrRow As DataRow()
        Dim li As ListItem

        Try
            strAccessRightID = ""
            For i = 0 To lstAccessRightTo.Items.Count - 1
                strAccessRightID = strAccessRightID & Trim(lstAccessRightTo.Items(i).Value) & ","
            Next

            If strAccessRightID <> "" Then
                strAccessRightID = Mid(strAccessRightID, 1, Len(strAccessRightID) - 1)

                objCommonQuery = New cor_Common.clsCommonQuery
                With objCommonQuery
                    .clsProperties.accessright_id = strAccessRightID
                    dt = .GetSalesRepListByAccessRightID()
                End With

                'If lstSRTo.Items.Count = 0 Then
                With lstSRFrom
                    .DataSource = dt
                    .DataTextField = "salesrep_desc"
                    .DataValueField = "salesrep_id"
                    .DataBind()
                End With
                'Else
                If lstSRTo.Items.Count <> 0 Then
                    Do
                        li = lstSRFrom.Items.FindByValue(lstSRTo.Items(y).Value)
                        If Not IsNothing(li) Then
                            lstSRFrom.Items.RemoveAt(lstSRFrom.Items.IndexOf(li))
                        End If

                        drCurrRow = dt.Select("salesrep_id='" & Trim(lstSRTo.Items(y).Value) & "'", "")
                        If drCurrRow.Length = 0 Then
                            lstSRTo.Items().RemoveAt(y)
                        Else
                            y = y + 1
                        End If
                    Loop Until y = lstSRTo.Items.Count
                End If
            Else
                lstSRFrom.Items.Clear()
                lstSRTo.Items.Clear()
            End If
        Catch ex As Exception
            ExceptionMsg("UserCreate.BindSR : " & ex.ToString)
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

#Region "Pwd Policy"
    Sub PolicyCheck(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        e.IsValid = Portal.Policy.CheckPwd(e.Value.Trim)
    End Sub

    Private Function RepeatedPassCheck(ByVal strPassword As String) As Boolean
        Dim clsUserQuery As New adm_User.clsUserQuery
        Dim dt As DataTable = Nothing
        Dim rept As Boolean = False
        Try
            If IsNothing(dtPassLib) Then
                With clsUserQuery.clsProperties
                    .user_id = Portal.UserSession.UserID
                End With
                dt = clsUserQuery.GetUsrPassLibList
                dtPassLib = dt
            End If


            Dim objCrypto As cor_Crypto.clsEncryption
            objCrypto = New cor_Crypto.clsEncryption


            Dim dv1 As New DataView(dtPassLib, "pwd='" + objCrypto.TripleDESEncrypt(Trim(strPassword)) + "' ", "pwd", DataViewRowState.CurrentRows)
            If dv1.Count > 0 Then
                rept = True
            End If

        Catch ex As Exception
            ExceptionMsg("RepeatedPassCheck  : " & ex.ToString)
        End Try

        Return rept
    End Function
#End Region

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

   
End Class
