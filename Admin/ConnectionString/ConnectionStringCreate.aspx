<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ConnectionStringCreate.aspx.vb" Inherits="Admin_ConnectionString_ConnectionStringCreate" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Creating Connection String</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head> 
<body class="BckgroundInsideContentLayout">
    <form id="frmUserCreate" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>
							                        <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label><asp:label id="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:dropdownlist id="ddlStatusID" Runat="server" CssClass="cls_dropdownlist">
									                        <asp:ListItem Value="">Select</asp:ListItem>
									                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
									                        <asp:ListItem Value="0">In-Active</asp:ListItem>
								                        </asp:dropdownlist>
								                        <asp:RequiredFieldValidator id="rfvStatusID" runat="server" ControlToValidate="ddlStatusID" ErrorMessage="Please select Status."
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
								                    </td>
						                        </tr>
						                        <tr><td>&nbsp;</td></tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCommCode" runat="server" CssClass="cls_label_header">Setting Code</asp:label><asp:label id="lblMark5" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtCommCode" MaxLength="20" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvCommCode" runat="server" ControlToValidate="txtCommCode" ErrorMessage="Setting Code cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblCommName" runat="server" CssClass="cls_label_header">Setting Name</asp:label><asp:label id="lblMark6" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtCommName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvCommName" runat="server" ControlToValidate="txtCommName" ErrorMessage="Setting Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>					                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblServerIP" runat="server" CssClass="cls_label_header">Server IP</asp:label><asp:label id="Label2" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label3" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtServerIP" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvServerIP" runat="server" ControlToValidate="txtServerIP" ErrorMessage="Server IP cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblServerName" runat="server" CssClass="cls_label_header">Server Name</asp:label><asp:label id="Label5" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtServerName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvServerName" runat="server" ControlToValidate="txtServerName" ErrorMessage="Server Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>	
						                        <tr>
							                        <td width="20%"><asp:label id="lblDBIP" runat="server" CssClass="cls_label_header">DB IP</asp:label><asp:label id="Label8" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label9" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtDBIP" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvDBIP" runat="server" ControlToValidate="txtDBIP" ErrorMessage="DB IP cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblDBPort" runat="server" CssClass="cls_label_header">DB Port</asp:label><asp:label id="Label11" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label12" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtDBPort" MaxLength="9" Runat="server" CssClass="cls_textbox" Text="0"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvDBPort" runat="server" ControlToValidate="txtDBPort" ErrorMessage="DB Port cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>	
						                        <tr>
							                        <td width="20%"><asp:label id="lblDBName" runat="server" CssClass="cls_label_header">DB Name</asp:label><asp:label id="Label14" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label15" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtDBName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvDBName" runat="server" ControlToValidate="txtDBName" ErrorMessage="DB Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblLinkServerName" runat="server" CssClass="cls_label_header">Link Server Name</asp:label><!--<asp:label id="Label17" runat="server" CssClass="cls_mandatory">*</asp:label></td>-->
							                        <td width="2%"><asp:label id="Label18" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtLinkServerName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvLinkServerName" runat="server" ControlToValidate="txtLinkServerName" ErrorMessage="Link Server Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator" Visible="false"></asp:RequiredFieldValidator></td>
						                        </tr>	
						                        <tr>
							                        <td width="20%"><asp:label id="lblDBUserID" runat="server" CssClass="cls_label_header">DB User ID</asp:label><!--<asp:label id="Label20" runat="server" CssClass="cls_mandatory">*</asp:label></td>-->
							                        <td width="2%"><asp:label id="Label21" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtDBUserID" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvDBUserID" runat="server" ControlToValidate="txtDBUserID" ErrorMessage="DB User ID cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblProxy" runat="server" CssClass="cls_label_header">Proxy</asp:label><asp:label id="Label32" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label24" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td>
                                                        <asp:textbox id="txtProxy" MaxLength="100" Runat="server" CssClass="cls_textbox"></asp:textbox><asp:RequiredFieldValidator id="rfvProxy" runat="server" ControlToValidate="txtProxy" ErrorMessage="Proxy cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator" Visible="false"></asp:RequiredFieldValidator></td>
						                        </tr>	
						                        <tr>
							                        <td width="20%"><asp:label id="lblDBUserPwd" runat="server" CssClass="cls_label_header">DB User Password</asp:label><asp:label id="Label23" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label33" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td>
                                                        <asp:TextBox ID="txtDBUserPwd" runat="server" CssClass="cls_textbox" MaxLength="100" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator id="rfvDBUserPwd" runat="server" ControlToValidate="txtDBUserPwd" ErrorMessage="DB User Password cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblConfirmPwd" runat="server" CssClass="cls_label_header">Confirm Password</asp:label><asp:label id="Label4" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label7" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td>
                                                        <asp:TextBox ID="txtConfirmPwd" runat="server" CssClass="cls_textbox" MaxLength="100" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator id="rfvConfirmPwd" runat="server" ControlToValidate="txtConfirmPwd" ErrorMessage="DB User Password cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>

						                        </tr>	
						                        <tr>
							                        <td width="20%"><asp:label id="lblURL" runat="server" CssClass="cls_label_header">URL</asp:label><asp:label id="Label26" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label27" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:textbox id="txtURL" MaxLength="100" Runat="server" CssClass="cls_textbox"></asp:textbox>
                                                        <br />
								                        <asp:RequiredFieldValidator id="rfvURL" runat="server" ControlToValidate="txtURL" ErrorMessage="URL cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
				
						                        </tr>							                        						                        						                        						                        						                        						                        
			
					                        </table>
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
				                            <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:button id="btnReset" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="False"></asp:button>
					                        <asp:button id="btnSave" runat="server" CssClass="cls_button" Text="Save"></asp:button>
                                            <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" CausesValidation="False" />
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
