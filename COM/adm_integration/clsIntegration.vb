﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data
Imports cor_DB

Public Class clsIntegration
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = CStr(Web.HttpContext.Current.Session("echoplus_conn"))
    Public clsProperties As New clsProperties.clsFFMRIntegration

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsIntegration"
        End Get
    End Property


    Public Function LoadUser() As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_INTEGRATION_LOAD_USER"
                .addItem("USER_ID", clsProperties.dbuserid, clsDB.DataType.DBString)
                .addItem("GUID", clsProperties.strguid, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetDataFileList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function


    Public Function InsertIntegration() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_INTEGRATION_INSERT"
                .addItem("USER_ID", clsProperties.dbuserid, cor_DB.clsDB.DataType.DBString)
                .addItem("GUID", clsProperties.strguid, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
