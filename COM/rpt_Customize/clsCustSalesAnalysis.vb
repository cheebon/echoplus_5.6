﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCustSalesAnalysis
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCustSalesAnalysis"
        End Get
    End Property

    Function GetCustSalesAnalysis(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
  ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strGroupField As String, ByVal intNetValue As Integer, _
  ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CUST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupField, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetCustSalesAnalysisByDate(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
       ByVal strYear As String, ByVal strMonth As String, _
       ByVal strTeamCode As String, ByVal strRegionCode As String, ByVal strSalesrepCode As String, _
       ByVal strChainCode As String, ByVal strChannelCode As String, _
       ByVal strCustGrpCode As String, ByVal strCustCode As String, _
       ByVal strPrdGrpCode As String, ByVal strPrdCode As String, _
       ByVal strSalesAreaCode As String, ByVal strShiptoCode As String, _
       ByVal intNetValue As Integer, ByVal strTreeSalesrep As String) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CUST_BY_DATE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CHAIN_CODE", strChainCode, clsDB.DataType.DBString)
                .addItem("CHANNEL_CODE", strChannelCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                .addItem("TREE_SALESREP_LIST", strTreeSalesrep, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesInfoByDate :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

    Function GetCustSalesAnalysisSummList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSelectedDate As String, _
        ByVal strTeamCode As String, ByVal strRegionCode As String, ByVal strSalesrepCode As String, _
        ByVal strChainCode As String, ByVal strChannelCode As String, _
        ByVal strCustGrpCode As String, ByVal strCustCode As String, _
        ByVal strPrdGrpCode As String, ByVal strPrdCode As String, _
        ByVal strSalesAreaCode As String, ByVal strShiptoCode As String, _
        ByVal intNetValue As Integer, ByVal strTreeSalesrep As String) As DataTable

        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALES_BY_CUST_SUMM_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SELECTED_DATE", strSelectedDate, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("REGION_CODE", strRegionCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("CHAIN_CODE", strChainCode, clsDB.DataType.DBString)
                .addItem("CHANNEL_CODE", strChannelCode, clsDB.DataType.DBString)
                .addItem("CUST_GRP_CODE", strCustGrpCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SALES_AREA_CODE", strSalesAreaCode, clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, clsDB.DataType.DBString)
                .addItem("NET_VALUE", CStr(intNetValue), clsDB.DataType.DBInt)
                .addItem("TREE_SALESREP_CODE", strTreeSalesrep, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesInfoByDate :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
