Imports System.Data

Partial Class iFFMA_Services_StockTransferDtl
    Inherits System.Web.UI.UserControl

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10
    End Sub

#Region "PROPERTY"
    Public Property TxnNo() As String
        Get
            Return Trim(hdTxnNo.Value)
        End Get
        Set(ByVal value As String)
            hdTxnNo.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgSTDtlList.PageIndex = 0
        wuc_dgSTDtlPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsStockTransfer As New mst_Services.clsStockTransfer

            DT = clsStockTransfer.GetStockTransferDetails(TxnNo)
            dgSTDtlList.DataKeyNames = New String() {"TXN_NO"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgSTDtlList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgSTDtlList.PageIndex = 0
            '    wuc_dgSTDtlPaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgSTDtlList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With


            'Call Paging
            With wuc_dgSTDtlPaging
                .PageCount = dgSTDtlList.PageCount
                .CurrentPageIndex = dgSTDtlList.PageIndex
                .DataBind()
                .RowCount = dvCurrentView.Count
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlStockTransferDtl.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgSTDtlList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSTDtlList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgSTDtlList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_StockTransferDtl.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_StockTransferDtl.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_StockTransferDtl.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_StockTransferDtl.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_StockTransferDtl.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgSTDtlList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSTDtlList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgSTDtlList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Function GetdgSTDtlList() As GridView
        Dim dgSTDtlListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgSTDtlList.AllowSorting
            Dim blnAllowPaging As Boolean = dgSTDtlList.AllowPaging

            dgSTDtlList.AllowSorting = False
            dgSTDtlList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgSTDtlListToExport = dgSTDtlList

            dgSTDtlList.AllowPaging = blnAllowPaging
            dgSTDtlList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgSTDtlListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgSTDtlPaging.Go_Click
        Try
            dgSTDtlList.PageIndex = CInt(wuc_dgSTDtlPaging.PageNo - 1)

            dgSTDtlList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgSTDtlPaging.Previous_Click
        Try
            If dgSTDtlList.PageIndex > 0 Then
                dgSTDtlList.PageIndex = dgSTDtlList.PageIndex - 1
            End If
            wuc_dgSTDtlPaging.PageNo = dgSTDtlList.PageIndex + 1

            dgSTDtlList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgSTDtlPaging.Next_Click
        Try
            If dgSTDtlList.PageCount - 1 > dgSTDtlList.PageIndex Then
                dgSTDtlList.PageIndex = dgSTDtlList.PageIndex + 1
            End If
            wuc_dgSTDtlPaging.PageNo = dgSTDtlList.PageIndex + 1

            dgSTDtlList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_StockTransferDtl
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn. No."
            Case "SLOC_FROM"
                strFieldName = "Storage Location From"
            Case "SLOC_TO"
                strFieldName = "Storage Location To"
            Case "MV_TYPE"
                strFieldName = "Movement Type"
            Case "MAT_DOC_NO"
                strFieldName = "Material Doc. No."
            Case "OLD_MAT_NO"
                strFieldName = "Old Material No."
            Case "QTY"
                strFieldName = "Qty"
            Case "RECEIVED_DATE"
                strFieldName = "Received Date"
            Case "BATCH_NO"
                strFieldName = "Batch No."
            Case "CONFIRM"
                strFieldName = "Confirm"
            Case "REMARK"
                strFieldName = "Remarks"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
