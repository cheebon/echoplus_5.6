<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MessagingList.aspx.vb" Inherits="iFFMS_Messaging_MessagingList" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Messaging List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('tcResult_TabPanelInbox_dgListInbox');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
        function ChangeAllCheckBoxStates2(checkState)
   { var dglist = document.getElementById('tcResult_TabPanelOutbox_dgListOutbox');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
       
              function ValidateCheckBoxStatesInbox()
   { var dglist = document.getElementById('tcResult_TabPanelInbox_dgListInbox');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
                 function ValidateCheckBoxStatesOutBox()
   { var dglist = document.getElementById('tcResult_TabPanelOutbox_dgListOutbox');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
   function Reset()
   {var txtSenderId = document.getElementById('tcResult_TabPanelcompose_txtSenderId');
   var Txtsubject = document.getElementById('tcResult_TabPanelcompose_Txtsubject');
   var txtcontent = document.getElementById('tcResult_TabPanelcompose_txtcontent');
   txtSenderId.value = '';Txtsubject.value = '';txtcontent.value='';
   }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <div style="width: 100%; position: relative; padding: 0; margin: 0; float: left;">
                <div id="title">
                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    <customToolkit:wuc_lblheader ID="wuc_lblheader" runat="server"></customToolkit:wuc_lblheader>
                </div>
                <div id="details">
                    <asp:UpdatePanel runat="server" ID="UpdatePage" RenderMode="Block" UpdateMode="Conditional">
                        <ContentTemplate>
                            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                            <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                                float: left; margin: 0;">
                                <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                    ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                    <ajaxToolkit:TabPanel ID="TabPanelInbox" runat="server" HeaderText="Inbox">
                                        <ContentTemplate>
                                            <asp:UpdatePanel runat="server" ID="UpdateInbox" RenderMode="Block" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <asp:Label ID="lblinboxmsg" runat="server" CssClass="cls_label_header" Text="" Font-Italic="true"
                                                            BackColor="AliceBlue"></asp:Label>
                                                    </div>
                                                    <input type="Button" id="btncheckallinbox" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                        style="width: 80px" runat="server" visible="false" />
                                                    <input type="Button" id="btnuncheckallinbox" value="Uncheck All" class="cls_button"
                                                        onclick="ChangeAllCheckBoxStates(false);" style="width: 80px" runat="server"
                                                        visible="false" />
                                                    <asp:Button ID="btndeleteinbox" runat="server" Text="Delete" CssClass="cls_button" OnClientClick="return ValidateCheckBoxStatesInbox();"
                                                        Width="80px" Visible="false" />
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <ccGV:clsGridView ID="dgListInbox" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                            Width="98%" FreezeHeader="True" GridHeight="450" AddEmptyHeaders="0" CellPadding="2"
                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                            DataKeyNames="MSG_CODE,SENDER_ID" ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Delete">
                                                                    <itemtemplate><asp:CheckBox id="chkdeleteinbox" runat="server"></asp:CheckBox>
                                                                    </itemtemplate>
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:CommandField HeaderText="View" ShowSelectButton="True" ShowHeader="True" Selecttext="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:CommandField>
                                                                <asp:CommandField HeaderText="Reply" ShowEditButton="True" ShowHeader="True" edittext="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="CREATED_DATE" HeaderText="Date" ReadOnly="True" SortExpression="CREATED_DATE">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MSG_CODE" HeaderText="Msg code" ReadOnly="True" SortExpression="MSG_CODE">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SENDER_ID" HeaderText="Sender Id" ReadOnly="True" SortExpression="SENDER_ID">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SENDER_NAME" HeaderText="Sender Name" ReadOnly="True"
                                                                    SortExpression="SENDER_NAME">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SUBJECT" HeaderText="Subject" ReadOnly="True" SortExpression="SUBJECT">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CONTENT" HeaderText="Content" ReadOnly="True" SortExpression="CONTENT">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </ccGV:clsGridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="TabPanelOutbox" runat="server" HeaderText="OutBox">
                                        <ContentTemplate>
                                            <asp:UpdatePanel runat="server" ID="UpdateOutBox" RenderMode="Block" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <asp:Label ID="lbloutboxmsg" runat="server" CssClass="cls_label_header" Text="" Font-Italic="true"
                                                            BackColor="AliceBlue"></asp:Label>
                                                    </div>
                                                    <input type="Button" id="btncheckalloutbox" value="Check All" class="cls_button"
                                                        onclick="ChangeAllCheckBoxStates2(true);" style="width: 80px" runat="server"
                                                        visible="false" />
                                                    <input type="Button" id="btnuncheckalloutbox" value="Uncheck All" class="cls_button"
                                                        onclick="ChangeAllCheckBoxStates2(false);" style="width: 80px" runat="server"
                                                        visible="false" />
                                                    <asp:Button ID="btndeleteoutbox" runat="server" Text="Delete" CssClass="cls_button"
                                                        Width="80px" Visible="false" OnClientClick="return ValidateCheckBoxStatesOutBox();"/>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <ccGV:clsGridView ID="dgListOutbox" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                            Width="98%" FreezeHeader="True" GridHeight="450" AddEmptyHeaders="0" CellPadding="2"
                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                            DataKeyNames="MSG_CODE,USER_ID" ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Delete">
                                                                    <itemtemplate><asp:CheckBox id="chkdeleteoutbox" runat="server"></asp:CheckBox>
                                                                    </itemtemplate>
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:TemplateField>
                                                                <asp:CommandField HeaderText="View" ShowSelectButton="True" ShowHeader="True" Selecttext="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="CREATED_DATE" HeaderText="Date" ReadOnly="True" SortExpression="CREATED_DATE">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MSG_CODE" HeaderText="Msg code" ReadOnly="True" SortExpression="MSG_CODE">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="USER_ID" HeaderText="Receiver Id" ReadOnly="True" SortExpression="USER_ID">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RECEIVER_NAME" HeaderText="Receiver Name" ReadOnly="True"
                                                                    SortExpression="RECEIVER_NAME">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SUBJECT" HeaderText="Subject" ReadOnly="True" SortExpression="SUBJECT">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CONTENT" HeaderText="Content" ReadOnly="True" SortExpression="CONTENT">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </ccGV:clsGridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="TabPanelDeleted" runat="server" HeaderText="Deleted Message">
                                        <ContentTemplate>
                                            <asp:UpdatePanel runat="server" ID="UpdateDeleted" RenderMode="Block" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <ccGV:clsGridView ID="dgListDeleted" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                            Width="98%" FreezeHeader="True" GridHeight="450" AddEmptyHeaders="0" CellPadding="2"
                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                            DataKeyNames="MSG_CODE,USER_ID" ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                            <Columns>
                                                                <asp:CommandField HeaderText="View" ShowSelectButton="True" ShowHeader="True" Selecttext="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="CREATED_DATE" HeaderText="Date" ReadOnly="True" SortExpression="CREATED_DATE">
                                                                    <itemstyle horizontalalign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MSG_CODE" HeaderText="Msg code" ReadOnly="True" SortExpression="MSG_CODE">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="USER_ID" HeaderText="TO/FROM ID" ReadOnly="True" SortExpression="USER_ID">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RECEIVER_NAME" HeaderText="TO/FROM Name" ReadOnly="True"
                                                                    SortExpression="RECEIVER_NAME">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="SUBJECT" HeaderText="Subject" ReadOnly="True" SortExpression="SUBJECT">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CONTENT" HeaderText="Content" ReadOnly="True" SortExpression="CONTENT">
                                                                    <itemstyle horizontalalign="Left" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                        </ccGV:clsGridView>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="TabPanelMsg" runat="server" HeaderText="Message">
                                        <ContentTemplate>
                                            <asp:UpdatePanel runat="server" ID="UpdateView" RenderMode="Block" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <!-- Begin Customized Content -->
                                                        <div style="width: 98%;">
                                                            <asp:Panel ID="pnlViewMode" runat="server">
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Date</span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:Label ID="lblDate" runat="server" CssClass="cls_label" />
                                                                </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">
                                                                    <asp:Label ID="lblIdTxt" runat="server" CssClass="cls_label_header" /></span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:Label ID="lblId" runat="server" CssClass="cls_label" />
                                                                </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">
                                                                    <asp:Label ID="lblNameTxt" runat="server" CssClass="cls_label_header" /></span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:Label ID="lblName" runat="server" CssClass="cls_label" />
                                                                </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Subject</span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:Label ID="lblSubject" runat="server" CssClass="cls_label" />
                                                                </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Content</span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:Label ID="lblMsgContent" runat="server" CssClass="cls_label" />
                                                                </span><span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                                                                    <%-- <center>
                                                                    <asp:Button ID="btnForward" runat="server" CssClass="cls_button" Text="Forward" />
                                                                   <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" />
                                                                </center>--%>
                                                                </span>
                                                            </asp:Panel>
                                                        </div>
                                                        <!-- End Customized Content -->
                                                        <asp:HiddenField ID="hdID" runat="server" Value="" />
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="TabPanelcompose" runat="server" HeaderText="Compose Message">
                                        <ContentTemplate>
                                            <asp:UpdatePanel runat="server" ID="UpdateCompose" RenderMode="Block" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <asp:Label ID="lblcomposemsg" runat="server" CssClass="cls_label_header" Text=""
                                                            Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                                    </div>
                                                    <div style="width: 98%; padding-left: 10px; padding-top: 10px">
                                                        <!-- Begin Customized Content -->
                                                        <div style="width: 98%;">
                                                            <asp:Panel ID="pnlEditMode" runat="server" Visible="true">
                                                                <asp:Button ID="btnsend" runat="server" Text="Send" CssClass="cls_button" Width="80px" />
                                                                <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="cls_button" Width="80px" OnClientClick = "Reset();"  />
                                                                <br />
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Msg
                                                                    Code</span> <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">
                                                                        :</span> <span style="float: left; width: 70%;">
                                                                            <asp:Label ID="lblMsgCode" runat="server" CssClass="cls_label" />
                                                                        </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Date</span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:Label ID="lbldatecompose" runat="server" CssClass="cls_label" />
                                                                </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Sender
                                                                    Id</span> <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">
                                                                        :</span> <span style="float: left; width: 70%;">
                                                                            <asp:TextBox ID="txtSenderId" runat="server" CssClass="cls_textbox" Width="200px" />
                                                                        </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Subject</span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:TextBox ID="Txtsubject" runat="server" CssClass="cls_textbox" Width="200px" />
                                                                </span>
                                                                <br />
                                                                <span style="float: left; width: 25%; padding-top: 2px;" class="cls_label_header">Content</span>
                                                                <span style="float: left; width: 2%; padding-top: 2px;" class="cls_label_header">:</span>
                                                                <span style="float: left; width: 70%;">
                                                                    <asp:TextBox ID="txtcontent" runat="server" CssClass="cls_textbox" TextMode="MultiLine"
                                                                        Width="500px" Rows="10" />
                                                                </span>
                                                                <br />
                                                            </asp:Panel>
                                                        </div>
                                                        <!-- End Customized Content -->
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value="" />
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </fieldset>
    </form>
</body>
</html>
