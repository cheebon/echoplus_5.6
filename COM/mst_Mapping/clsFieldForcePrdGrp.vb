'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	16/04/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsFieldForcePrdGrp
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsFieldForcePrdGrp"
        End Get
    End Property

    Public Function GetFieldForcePrdGrpList(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALESREP_PRD_GRP_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetFieldForcePrdGrpExcludeList(ByVal strPrdGrpCode As String, ByVal strPrdGrpName As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strAgencyCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALESREP_PRD_GRP_EXCLUDE_LIST"
                .addItem("PRD_GRP_CODE", strPrdGrpCode, clsDB.DataType.DBString)
                .addItem("PRD_GRP_NAME", strPrdGrpName, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("AGENCY_CODE", strAgencyCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub CreateFieldForcePrdGrp(ByVal strCodeList As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALESREP_PRD_GRP_CREATE"
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteFieldForcePrdGrp(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strCodeList As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SALESREP_PRD_GRP_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CODE_LIST", strCodeList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class