﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsMerchandising
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMerchandising"
        End Get
    End Property

    Public Function GetMerchandising(strUserID As String, strPrincipalID As String, strPrincipalCode As String, _
                                               strYear As String, strMonth As String, strSalesrepList As String, strDateFrom As String, strDateTo As String, _
                                               strBrand As String, strActivity As String) As DataTable
        Dim clsMerchandising As clsDB
        Try
            clsMerchandising = New clsDB
            With clsMerchandising
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MERCHANDISING"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("FROM_DATE", strDateFrom, clsDB.DataType.DBString)
                .addItem("TO_DATE", strDateTo, clsDB.DataType.DBString)
                .addItem("BRAND", strBrand, clsDB.DataType.DBString)
                .addItem("ACTIVITY", strActivity, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMerchandising :" & ex.Message))
        Finally
            clsMerchandising = Nothing
        End Try
    End Function

    Public Function GetMerchandisingDetails(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strActivityCode As String, ByVal strYear As String, ByVal strMonth As String) As DataTable
        Dim clsMerchandisingDetails As clsDB
        Try
            clsMerchandisingDetails = New clsDB
            With clsMerchandisingDetails
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MERCHANDISING_DETAILS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("ACTIVITY_CODE", strActivityCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMerchandisingDetails :" & ex.Message))
        Finally
            clsMerchandisingDetails = Nothing
        End Try
    End Function

    Public Function GetMerchandisingDetailsTVS(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strActivityCode As String, ByVal strYear As String, ByVal strMonth As String) As DataTable
        Dim clsMerchandisingDetails As clsDB
        Try
            clsMerchandisingDetails = New clsDB
            With clsMerchandisingDetails
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MERCHANDISING_DETAILS_TVS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("ACTIVITY_CODE", strActivityCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMerchandisingDetails :" & ex.Message))
        Finally
            clsMerchandisingDetails = Nothing
        End Try
    End Function

    Public Function GetMerchandisingDetailsImage(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
        ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strActivityCode As String, ByVal strCustCode As String, ByVal strTxnNo As String) As DataTable
        Dim clsMerchandisingDetailsImage As clsDB
        Try
            clsMerchandisingDetailsImage = New clsDB
            With clsMerchandisingDetailsImage
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MERCHANDISING_DETAILS_IMAGE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("ACTIVITY_CODE", strActivityCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMerchandisingDetailsImage :" & ex.Message))
        Finally
            clsMerchandisingDetailsImage = Nothing
        End Try
    End Function

    Public Function GetBrandDDL() As DataTable
        Dim clsMerchandising As clsDB
        Try
            clsMerchandising = New clsDB
            With clsMerchandising
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MERCHANDISING_GET_BRAND"
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetBrandDDL :" & ex.Message))
        Finally
            clsMerchandising = Nothing
        End Try
    End Function

    Public Function GetActivityDDL(Optional ByVal strBrand As String = "") As DataTable
        Dim clsDB As clsDB
        Try
            clsDB = New clsDB
            With clsDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MERCHANDISING_GET_ACTIVITY"
                .addItem("BRAND", strBrand, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetActivityDDL :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function
#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
