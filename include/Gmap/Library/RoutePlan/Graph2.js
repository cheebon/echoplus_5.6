﻿var _graph; //store reference to Graph object
var CallBackFunction = function () { };
var FailCallBackFunction = function () { };
var gdir = []; //Store gDirection obj
var path; //multidimensional array to store paths. as loadfromwaypoints only accep max of 25 points. so need to break full path into several paths.
    var custcode;
var delay = 500;
var callbackCount = 0;
var SuccessRequest = 0; 
function Graph(_map) {
    this.RawGraph = new Array; //Customer Type; The original graph
    this.ShortestPath = new Array; //Node Type; Processed shortest path
    this.Neighbours = new Array; //Node Type; Place to place current node's neighbour
    this.Map = _map;

    _graph = this;
}

//Functions prototype
Graph.prototype.BulkAddWaypoint = bulkAddWaypoint;
Graph.prototype.PopulateNeighbours = populateNeighbours;
Graph.prototype.setMin = setMin;
Graph.prototype.ReturnError = ReturnError;
Graph.prototype.GetNextDestination = getNextDestination;
Graph.prototype.PrepPath = prepPath;
/////////////////////////////////////////////////////////////////////


//
//Function Definition
function bulkAddWaypoint(_rgArray) {
    _graph.RawGraph = _rgArray;
}

/******************************************************************
//Find all unvisited neighbours
******************************************************************/
function populateNeighbours(_CurrentCust) {
    _graph.Neighbours = [];
    var neigh

    for (var i = 0; i < _graph.RawGraph.length; ++i) {
        if (_graph.RawGraph[i].CustCode != _CurrentCust.CustCode && _graph.RawGraph[i].Visited == false && _graph.RawGraph[i].IsDestination == false) {
            neigh = new Node(_graph.RawGraph[i].CustCode, _graph.RawGraph[i].Latitude, _graph.RawGraph[i].Longitude);
            _graph.Neighbours.push(neigh);
        }
    }
}

function setWaypointsV3(path) {
    var partsOfStr = String(path).split(',');
    var waypts = [];
    var count = 0;
    for (var loop = 0; loop < partsOfStr.length / 4; loop++) {
        endPointlat = partsOfStr[count + 2];
        endPointlng = partsOfStr[count + 3];
        waypts.push({
            lat: endPointlat,
            lng: endPointlng,
        });
        count = count + 4;
    }
    return waypts;
}

function getNextDestination(_CallBackFunc, _FailCallBackFunc, _CurrentCust) {
    if (typeof _CallBackFunc == 'function') {
        CallBackFunction = _CallBackFunc;
    }
    if (typeof _FailCallBackFunc == 'function') {
        FailCallBackFunction = _FailCallBackFunc;
    }
  
    MinDistance = 0;
    MinPoint = 0;
    path = _graph.PrepPath(_CurrentCust);
   
    waypoints = setWaypointsV3(path);

    var directionsService = new google.maps.DirectionsService();
    var partsOfStr = String(path).split(',');
    var startPointlat;
    var startPointlng;
    var endPointlat;
    var endPointlng;
    var locationArray = [];
    var partsOfStr = String(path).split(',');
    startPointlat = partsOfStr[0];
    startPointlng = partsOfStr[1];
    totpoints=waypoints.length;
   
    var count = 0;
    var returnRequest = function() {  
    requestRoute(totpoints,count,new google.maps.LatLng(startPointlat, startPointlng) ,new google.maps.LatLng(waypoints[count].lat, waypoints[count].lng));
    if(count++ < waypoints.length-1) {
     /////// only request 1 time directionsService per second. so that it will not over the google api rate limit(10 requests per second)
       setTimeout(returnRequest,  delay*2);
     }
  };
  returnRequest();

}
function requestRoute(totalpoints,index, origin, destination)
{    
      var distance=0;
      var requestCount;
      var directionsService = new google.maps.DirectionsService();
      var request={
            origin: origin,
            destination: destination,
            optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
                var route = response.routes[0];
                distance=route.legs[0].distance.value;  
                setTimeout(function() {calcShortestRoute(distance, index,totalpoints); }, delay); 
            }
            else
            {
               if (status == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) 
               {
              
                setTimeout(function() {
                requestRoute(totalpoints,index, origin, destination);
                 }, delay*2);
               }
               else
                return _graph.ReturnError(status);    
            }
        });   
        
        
  return requestCount;
}
          
function setMin() {
    for (var i = 0; i < _graph.RawGraph.length; ++i) {
        if (_graph.RawGraph[i].CustCode.toString() == _graph.Neighbours[MinPoint].CustCode.toString()) {
            var node = new Node(_graph.Neighbours[MinPoint].CustCode, _graph.Neighbours[MinPoint].Latitude, _graph.Neighbours[MinPoint].Longitude);
            //_graph.SPaths.push(_graph.Waypoints[i].LatLng.toString());
            _graph.ShortestPath.push(node);
            _graph.RawGraph[i].Visited = true;
            break;
             
        }
    }
    CallBackFunction(_graph.Neighbours[MinPoint].CustCode);
}


function ReturnError(ErrorCode) {
    FailCallBackFunction(ErrorCode);
}
function calcShortestRoute(Comparedistance, Compareindex,totalpoints) {
    SuccessRequest++;
    if(MinDistance!=0)
    {
        if(MinDistance>Comparedistance)
        {
        MinDistance=Comparedistance;
        MinPoint=Compareindex;
        }
    }
    else
    {
      MinDistance=Comparedistance;
      MinPoint=0;
    }
    if(Compareindex==totalpoints-1)
    {
      _graph.setMin();
    }
 //   alert(SuccessRequest);
    return SuccessRequest;
}
function prepPath(_CurrentCust) {
    var paths = [];
    var partialpath;


    for (var i = 0; i < _graph.Neighbours.length; ++i) {
        if (i % 10 == 0) {
            if (i == 0)
            { partialpath = []; }
            else {
                paths.push(partialpath);
                partialpath = [];
            }
        }

        partialpath.push(_CurrentCust.Latitude + ", " + _CurrentCust.Longitude);
        partialpath.push(_graph.Neighbours[i].Latitude + ", " + _graph.Neighbours[i].Longitude);
       
    }
    paths.push(partialpath);
    return paths;
}