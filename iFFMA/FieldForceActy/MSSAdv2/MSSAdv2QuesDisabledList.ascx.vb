﻿Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdv2QuesDisabledList
    Inherits System.Web.UI.UserControl

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

        'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If
        '----------------------------------------------------------------------------------------------------
    End Sub

#Region "PROPERTY"
    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property

    Public Property SubQuesCode() As String
        Get
            Return Trim(hdSubQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubQuesCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgQuesDisabledList.PageIndex = 0
        wuc_dgQuesDisabledPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Public Sub UpdateHdrInfo()
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2
            DT = clsMSSAdv.MSSUpdateHdrInfo(TeamCode, TitleCode, QuesCode, SubQuesCode)

            If DT.Rows.Count > 0 Then
                lblTeamCode.Text = DT.Rows(0)("TEAM_NAME").ToString + " (" + TeamCode + ")"
                lblTitleCode.Text = DT.Rows(0)("TITLE_NAME").ToString + " (" + TitleCode + ")"
                lblQuesCode.Text = DT.Rows(0)("QUES_NAME").ToString + " (" + QuesCode + ")"
                lblSubQuesCode.Text = DT.Rows(0)("SUB_QUES_NAME").ToString + " (" + SubQuesCode + ")"
            Else
                lblTeamCode.Text = TeamCode
                lblTitleCode.Text = TitleCode
                lblQuesCode.Text = QuesCode
                lblSubQuesCode.Text = SubQuesCode
            End If
            UpdatePnlQuesDisabled.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            DT = clsMSSAdv.GetMSSQuesDisabledList(TeamCode, TitleCode, QuesCode, SubQuesCode)
            dgQuesDisabledList.DataKeyNames = New String() {"CRITERIA_VALUE", "DISABLE_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgQuesDisabledList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            UpdateHdrInfo()

            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgQuesDisabledList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgQuesDisabledPaging
                .PageCount = dgQuesDisabledList.PageCount
                .CurrentPageIndex = dgQuesDisabledList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlQuesDisabled.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgQuesDisabledList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesDisabledList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgQuesDisabledList.Columns.Clear()

            'ADD BUTTON DELETE
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                dgBtnColumn.HeaderText = "Delete"

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowDeleteButton = True
                    dgBtnColumn.DeleteText = "<img src='../../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgQuesDisabledList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_DELETE")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_MSSAdvQuesDisabledList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_MSSAdvQuesDisabledList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_MSSAdvQuesDisabledList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_MSSAdvQuesDisabledList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_MSSAdvQuesDisabledList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgQuesDisabledList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesDisabledList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgQuesDisabledList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesDisabledList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgQuesDisabledList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
                        Dim strDeleteMsg As String = Nothing

                        If btnDelete IsNot Nothing Then
                            strDeleteMsg = "Are you sure want to delete?"
                            'btnDelete.OnClientClick = "if (confirm('" + strDeleteMsg + "') == false) { window.event.returnValue = false; return false; }"
                            btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"

                            'HL: 20120702 - Due to BDF, display delete button according to accessright----------------------------
                            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV2, SubModuleAction.Delete) Then
                                btnDelete.Visible = False
                            End If
                            '----------------------------------------------------------------------------------------------------
                        End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgQuesDisabledList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgQuesDisabledList.RowDeleting
        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            Dim strCriteriaValue As String
            Dim strDisableCode As String

            strCriteriaValue = IIf(sender.datakeys(e.RowIndex).item("CRITERIA_VALUE") Is Nothing, "", sender.datakeys(e.RowIndex).item("CRITERIA_VALUE"))
            strDisableCode = IIf(sender.datakeys(e.RowIndex).item("DISABLE_CODE") Is Nothing, "", sender.datakeys(e.RowIndex).item("DISABLE_CODE"))

            clsMSSAdv.DeleteMSSQuesDisabled(TeamCode, TitleCode, QuesCode, SubQuesCode, strCriteriaValue, strDisableCode)

            RenewDataBind()
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgQuesDisabledList() As GridView
        Dim dgQuesDisabledListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgQuesDisabledList.AllowSorting
            Dim blnAllowPaging As Boolean = dgQuesDisabledList.AllowPaging

            dgQuesDisabledList.AllowSorting = False
            dgQuesDisabledList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgQuesDisabledListToExport = dgQuesDisabledList

            dgQuesDisabledList.AllowPaging = blnAllowPaging
            dgQuesDisabledList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgQuesDisabledListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgQuesDisabledPaging.Go_Click
        Try
            dgQuesDisabledList.PageIndex = CInt(wuc_dgQuesDisabledPaging.PageNo - 1)

            dgQuesDisabledList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgQuesDisabledPaging.Previous_Click
        Try
            If dgQuesDisabledList.PageIndex > 0 Then
                dgQuesDisabledList.PageIndex = dgQuesDisabledList.PageIndex - 1
            End If
            wuc_dgQuesDisabledPaging.PageNo = dgQuesDisabledList.PageIndex + 1

            dgQuesDisabledList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgQuesDisabledPaging.Next_Click
        Try
            If dgQuesDisabledList.PageCount - 1 > dgQuesDisabledList.PageIndex Then
                dgQuesDisabledList.PageIndex = dgQuesDisabledList.PageIndex + 1
            End If
            wuc_dgQuesDisabledPaging.PageNo = dgQuesDisabledList.PageIndex + 1

            dgQuesDisabledList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            With (wuc_MSSAdv2QuesDisabledPop)
                .TeamCode = TeamCode
                .TitleCode = TitleCode
                .QuesCode = QuesCode
                .SubQuesCode = SubQuesCode
                .BindDetailsView()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnMSSAdvQuesDisabledPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_MSSAdv2QuesDisabledPop.SaveButton_Click
        RenewDataBind()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_MSSAdvQuesDisabledList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "CRITERIA_VALUE"
                strFieldName = "Criteria Value"
            Case "DISABLE_CODE"
                strFieldName = "Disable Code"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

