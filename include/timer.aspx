<%@ Page Language="vb" AutoEventWireup="false" Inherits="Echoplus.Timer" CodeFile="Timer.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Timer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK rel="stylesheet" href='<%=Session("ServerName")%>/include/cdna.css' type="text/css">
	</HEAD>
	<body>
		<form id="frmTimer" method="post" runat="server">
			<table align="center" class="cls_calendar_bgcolor" border="0" cellpadding="0" cellspacing="0">
				<TR>
					<td vAlign="top" align="center"><asp:button id="PrevMonth" Width="20" Height="20" Runat="server" Text="<"></asp:button>
						&nbsp;&nbsp;<asp:dropdownlist id="tbSelMonth" Runat="server" AutoPostBack="True">
							<asp:ListItem Value="1">January</asp:ListItem>
							<asp:ListItem Value="2">February</asp:ListItem>
							<asp:ListItem Value="3">March</asp:ListItem>
							<asp:ListItem Value="4">April</asp:ListItem>
							<asp:ListItem Value="5">May</asp:ListItem>
							<asp:ListItem Value="6">June</asp:ListItem>
							<asp:ListItem Value="7">July</asp:ListItem>
							<asp:ListItem Value="8">August</asp:ListItem>
							<asp:ListItem Value="9">September</asp:ListItem>
							<asp:ListItem Value="10">October</asp:ListItem>
							<asp:ListItem Value="11">November</asp:ListItem>
							<asp:ListItem Value="12">December</asp:ListItem>
						</asp:dropdownlist>
						&nbsp;&nbsp;<asp:dropdownlist id="tbSelYear" runat="server" AutoPostBack="True"></asp:dropdownlist>
						&nbsp;&nbsp;<asp:button id="NextMonth" runat="server" Width="20" Height="20" Text=">"></asp:button>
					</td>
				</TR>
				<TR>
					<TD align="center">
						<table class="cls_calendar_grid" width="100%" border="0">
							<tr>
								<td>
									<asp:Label ID="lblTime" Runat="server">Time : </asp:Label>
									<asp:label ID="lblHour" Runat="server">Hour</asp:label>&nbsp;
									<asp:dropdownlist id="ddHour" runat="server"></asp:dropdownlist>
									<asp:label ID="lblMin" Runat="server">Minute</asp:label>
									<asp:dropdownlist id="ddMin" runat="server"></asp:dropdownlist>
								</td>
							</tr>
						</table>
					</TD>
				</TR>
				<tr>
					<td align="center">
						<table class="cls_calendar_grid" width="100%" border="0">
							<tr style="FONT-WEIGHT: bold; COLOR: navy" align="center" bgColor="#ffffff">
								<td width="26">Sun</td>
								<td width="26">Mon</td>
								<td width="26">Tue</td>
								<td width="26">Wed</td>
								<td width="26">Thu</td>
								<td width="26">Fri</td>
								<td width="26">Sat</td>
							</tr>
							<asp:repeater id="Repeater1" runat="server">
								<ItemTemplate>
									<tr bgcolor="#ffffff">
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar1" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day1")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day1")%>' ForeColor=Red >
											</asp:Button></td>
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar2" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day2")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day2")%>'>
											</asp:Button></td>
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar3" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day3")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day3")%>'>
											</asp:Button></td>
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar4" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day4")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day4")%>'>
											</asp:Button></td>
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar5" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day5")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day5")%>'>
											</asp:Button></td>
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar6" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day6")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day6")%>'>
											</asp:Button></td>
										<td width="26" align="center">
											<asp:Button CssClass="cls_Calendar_Button" ID="btnCalendar7" Runat="server" Width="100%" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "day7")%>' Text='<%# DataBinder.Eval(Container.DataItem, "day7")%>' ForeColor=Red >
											</asp:Button></td>
									</tr>
								</ItemTemplate>
							</asp:repeater></table>
					</td>
				</tr>
				<TR>
					<TD align="center"><asp:Button ID="btnCalendar" CssClass="cls_calendar_button" Runat="server"></asp:Button></TD>
				</TR>
				<TR>
					<TD align="center"><input id="control" type="hidden" name="control" runat="server"></TD>
				</TR>
			</table>
		</form>
		<script><asp:literal id="ltlAlert" runat="server" enableviewstate="false"></asp:literal></script>
		<script>window.focus();</script>
		<script language="javascript">
			function click_OK()
			{
				//temp='window.opener.'+document.frmTimer.Text3.value+'.value='+document.frmTimer.Text2.value;
				//eval(temp);
				//window.close();
			}
		</script>
	</body>
</HTML>
