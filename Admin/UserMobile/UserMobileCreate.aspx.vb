'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	10/04/2007
'	Purpose	    :	Mobile User Create
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data
Partial Public Class Admin_UserMobile_UserMobileCreate
    Inherits System.Web.UI.Page
    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strUserCode As String
        'Dim objUserQuery As adm_PDAUser.clsPDAUserQuery

        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Creating Mobile User"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant
                strUserCode = Trim(Session("UserCode"))

                'request
                txtSearchType.Text = Trim(Request.QueryString("search_type"))
                txtSearchValue.Text = Trim(Request.QueryString("search_value"))

                'populate ddl
                LoadDDL()
                'Prepare Recordset,Createobject as needed
                'Call User Type

            End If

        Catch ex As Exception
            ExceptionMsg("UserMobileCreate.Page_Load : " & ex.ToString)
        Finally
            '   objUserQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub LoadDDL
    ' Purpose	    :	Load dropdownlist
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub LoadDDL()
        Dim objUserQuery As adm_PDAUser.clsPDAUserQuery
        Dim dt As DataTable
        Dim intLoopCount As Integer = 0

        objUserQuery = New adm_PDAUser.clsPDAUserQuery
        With objUserQuery
            dt = .GetConnectionString
        End With

        If dt.Rows.Count > 0 Then
            Do While intLoopCount < dt.Rows.Count
                ddlSettingCode.Items.Insert(intLoopCount + 1, dt.Rows(intLoopCount).Item("comm_svr_setting_code").ToString)
                intLoopCount = intLoopCount + 1
            Loop
        End If
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnReset_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'ddlStatusID.SelectedValue = 0
            txtPassword.Text = ""
            txtConfirmedPassword.Text = ""
            txtUserCode.Text = ""
            txtUserName.Text = ""
            txtMobileCode.Text = ""
            txtAddress.Text = ""
            txtEmail.Text = ""
            txtContactNo.Text = ""

        Catch ex As Exception
            ExceptionMsg("UserMobileCreate.btnReset_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSave_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objUser As adm_PDAUser.clsPDAUser
        Dim objUserQuery As adm_PDAUser.clsPDAUserQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String
        Dim bUserExist As Boolean

        Try
            'Check login or user code exist or not
            objUserQuery = New adm_PDAUser.clsPDAUserQuery
            With objUserQuery
                .clsProperties.strMobileUserID = "0"
                .clsProperties.strSalesRepCode = Trim(txtUserCode.Text)
                .clsProperties.strMobileCode = trim(txtMobileCode.Text)
                bUserExist = .UserExist
            End With
            objUserQuery = Nothing

            If bUserExist = True Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Login / User Code already exists!');</script>")
                Exit Sub
            End If

            'Check Password match
            If Trim(txtPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtPassword.Text <> "") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                Exit Sub
            End If

            'Encrypt Password
            objCrypto = New cor_Crypto.clsEncryption
            With objCrypto
                strEncryptPwd = .TripleDESEncrypt(Trim(txtPassword.Text))
            End With

            'Insert into User Table
            objUser = New adm_PDAUser.clsPDAUser 'adm_User.clsUser
            With objUser
                .clsProperties.strPassword = strEncryptPwd
                .clsProperties.strSalesRepCode = Trim(txtUserCode.Text)
                .clsProperties.strSalesRepName = Trim(txtUserName.Text)
                .clsProperties.strActiveFlag = ddlStatusID.SelectedValue
                .clsProperties.strMobileCode = Trim(txtMobileCode.Text)
                .clsProperties.strContactNo = Trim(txtContactNo.Text)
                .clsProperties.strAddress = Trim(txtAddress.Text)
                .clsProperties.strEmail = Trim(txtEmail.Text)
                .clsProperties.strConnCode = ddlSettingCode.SelectedValue.Trim
                .clsProperties.strSyncOperation = "A"
                If Session("UserID") <> "" Then
                    .clsProperties.strCreatorUserID = Session("UserID")
                Else
                    .clsProperties.strCreatorUserID = ""
                End If
                .clsProperties.strCreatedDate = Format(Date.Now, "MM/dd/yyyy hh:mm:ss")
                .Create()
            End With
            Response.Redirect("UserMobileDtl.aspx?user_code=" & Trim(txtUserCode.Text) & "&search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("UserMobileCreate.btnSave_Click : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
            objUser = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("UserMobileList.aspx", False)
    End Sub
End Class