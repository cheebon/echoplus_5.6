﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustomerTgtPrdPop.ascx.vb" Inherits="iFFMA_SalesAccount_CustomerTgtPrdPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdSearch" Src="../Common/wuc_PrdSearch.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustContSearch" Src="CustSearchPop.ascx" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 450px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px; text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 92%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: right; width: 8%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; /*width: 98%*/">
                <fieldset style="padding-left: 10px; width: 100%; box-sizing: border-box;" id="fldsetTgtPrd" runat="server">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />

                    <table cellspacing="1" cellpadding="2" rules="all" border="0" style="border-width: 0px; border-style: None; width: 100%;">
                        <tbody runat="server" id="pnlTgtPrd">
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Customer
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtCust" runat="server" CssClass="cls_textbox" Enabled="False" Width="200px"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                                <td align="middle" rowspan="2" colspan="3">
                                    <asp:Button ID="btnSearchCustCont" runat="server" CssClass="cls_button" Text="Search" OnClick="btnSearchCustCont_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Contact
                                </td>
                                <td>
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtCont" runat="server" CssClass="cls_textbox" Enabled="False" Width="200px"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Product 1</td>
                                <td colspan="2">
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtPrd1" runat="server" CssClass="cls_textbox" Width="200px" MaxLength="250"></asp:TextBox>
                                    <span class="cls_label_err">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Product 2</td>
                                <td colspan="2">
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtPrd2" runat="server" CssClass="cls_textbox" Width="200px" MaxLength="250"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="cls_label_header" align="left" valign="middle" style="width: 30%; white-space: nowrap;">Product 3</td>
                                <td colspan="2">
                                    <span class="cls_label_header">:</span>
                                    <asp:TextBox ID="txtPrd3" runat="server" CssClass="cls_textbox" Width="200px" MaxLength="250"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <span style="float: left; width: 100%; padding-top: 10px; padding-bottom: 10px">
                        <center>
                            <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" />
                        </center>
                    </span>
                    <!-- End Customized Content -->

                    <asp:HiddenField ID="hdCustCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdContCode" runat="server" Value="" />

                    <asp:HiddenField ID="hdCustomer" runat="server" Value="" />
                    <asp:HiddenField ID="hdContact" runat="server" Value="" />

                    <asp:HiddenField ID="hdIsEdit" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server"
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden"
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop"
            BackgroundCssClass="modalBackground"
            DropShadow="True"
            RepositionMode="RepositionOnWindowResizeAndScroll" />

        <span style="float: left;">
            <customToolkit:wuc_PrdSearch ID="wuc_PrdSearch" Title="Product Search" runat="server" />
        </span>
        <span style="float: left;">
            <customToolkit:wuc_CustContSearch ID="wuc_CustContSearch" Title="Customer Search" runat="server" />
        </span>
    </ContentTemplate>
</asp:UpdatePanel>
