Imports System.Data

Partial Class iFFMR_SFA_USRCTRL_wuc_ctrlpnl
    Inherits System.Web.UI.UserControl
#Region "Local Variable"
    Public Event SearchBtn_Click As EventHandler
    Public Event ExportBtn_Click As EventHandler
    Private lngSubModuleID As SubModuleType
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") Is Nothing OrElse Session("UserID") = "" Then
            Dim strScript As String = ""
            strScript = "self.parent.parent.location='" & ResolveClientUrl("~/login.aspx") & "?ErrMsg=Session Time Out, Please login again !!!';"
            ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Timeout", strScript, True)
        End If
        If Not IsPostBack Then
            BuildQueryYearMonth()
            'BindDDLSupplier()
            'BindDDLChannel()
        End If
        RefreshDetails(IsPostBack)
    End Sub

    Private Sub RestoreDefaultValue(ByVal ddlTarget As DropDownList, ByVal strValue As String)
        If ddlTarget.Items.FindByValue(strValue) IsNot Nothing Then
            ddlTarget.Items.FindByValue(strValue).Selected = True
        End If
    End Sub

#Region "Action/DataBinding"
    Public Sub RefreshDetails(Optional ByVal blnIsPostback As Boolean = False)
        Dim strValuePath As String = Session("TREE_PATH")
        Dim strValues(), strName, strvalue As String
        Dim strMap As New StringBuilder
        If Not strValuePath = String.Empty Then
            strValues = strValuePath.Split("/")
            For Each strvalue In strValues
                strName = strvalue.Split("@")(0).Replace("_CODE", "_NAME")
                strvalue = Session(strName)
                If strMap.ToString <> String.Empty Then strMap.Append(" > ")
                strMap.Append(strvalue)
            Next
        End If
        lblMapPath.Text = strMap.ToString

    End Sub
    Public Overloads Sub DataBind()
        pnlMonth_Visible = True
        pnlQuarter_Visible = False

        Select Case lngSubModuleID
            Case SubModuleType.SFA_ATCHART
            Case SubModuleType.SFA_PERF_M
                pnlQuarter_Visible = False
            Case SubModuleType.SFA_PERF_Q
                pnlMonth_Visible = False
                pnlQuarter_Visible = True
            Case SubModuleType.SFA_SALESBYAGENCY
                pnlMonth_Visible = False
                pnlQuarter_Visible = True
            Case SubModuleType.SFA_SALESBYPRODUCT
                pnlSupplier_Visible = True
                pnlChannel_Visible = True
                pnlRegion_Visible = True
                pnlMonth_Visible = False
                BindDDLSupplier()
                BindDDLChannel()
            Case SubModuleType.SFA_SALESBYCHANNEL
                pnlSupplier_Visible = True
                pnlMonth_Visible = False
                BindDDLSupplier()
        End Select
    End Sub
    Private Sub BindDDLSupplier()
        Dim dtSupplier As DataTable
        Try
            Dim clsCommon As New rpt_SFA.SFA_DLL

            dtSupplier = clsCommon.GetSupplierDDL()
            With ddlSupplier
                .Items.Clear()
                .DataSource = dtSupplier.DefaultView
                .DataTextField = "AGENCY_NAME"
                .DataValueField = "AGENCY_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                Session("SUPPLIER") = .SelectedItem.Value
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub BindDDLChannel()
        Dim dtSupplier As DataTable
        Try
            Dim clsCommon As New rpt_SFA.SFA_DLL

            dtSupplier = clsCommon.GetChannelDDL()
            With ddlChannel
                .Items.Clear()
                .DataSource = dtSupplier.DefaultView
                .DataTextField = "CHANNEL_NAME"
                .DataValueField = "CHANNEL_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("--- Select All ---", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub BuildQueryYearMonth()
        Dim iYear As Integer = -1
        While iYear < 1
            ddlYear.Items.Add(New ListItem(Date.Now.AddYears(iYear).Year))
            iYear += 1
        End While

        If (String.IsNullOrEmpty(Session("QUARTER"))) Then Session("QUARTER") = DatePart(DateInterval.Quarter, Now)
        RestoreDefaultValue(ddlYear, Session("YEAR"))
        RestoreDefaultValue(ddlMonth, Session("MONTH"))
        RestoreDefaultValue(ddlQuarter, Session("QUARTER"))

    End Sub

#End Region

#Region "Properties"
#Region "   Panel Visible"
    Public Property pnlMonth_Visible() As Boolean
        Get
            Return ViewState("_pnlMonth")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_pnlMonth") = value
        End Set
    End Property
    Public Property pnlQuarter_Visible() As Boolean
        Get
            Return ViewState("_pnlQuarter")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_pnlQuarter") = value
        End Set
    End Property
    Public Property pnlSupplier_Visible() As Boolean
        Get
            Return ViewState("_pnlSupplier")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_pnlSupplier") = value
        End Set
    End Property
    Public Property pnlChannel_Visible() As Boolean
        Get
            Return ViewState("_pnlChannel")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_pnlChannel") = value
        End Set
    End Property
    Public Property pnlRegion_Visible() As Boolean
        Get
            Return ViewState("_pnlRegion")
        End Get
        Set(ByVal value As Boolean)
            ViewState("_pnlRegion") = value
        End Set
    End Property
#End Region

    Public Property SubModuleID() As SubModuleType
        Get
            Return lngSubModuleID
        End Get
        Set(ByVal Value As SubModuleType)
            lngSubModuleID = Value
        End Set
    End Property

    Public ReadOnly Property ClassName() As String
        Get
            Return "SFA_USRCTRL_wuc_ctrlpnl"
        End Get
    End Property

    Public Sub UpdateControlPanel()
        UpdateCtrlPanel.Update()
    End Sub

    Public Property Year() As Integer
        Get
            Return ddlYear.SelectedValue
        End Get
        Set(ByVal value As Integer)
            ddlYear.SelectedValue = value
        End Set
    End Property

    Public Property Month() As Integer
        Get
            Return ddlMonth.SelectedValue
        End Get
        Set(ByVal value As Integer)
            ddlMonth.SelectedValue = value
        End Set
    End Property

    Public Property Quarter() As Integer
        Get
            Return ddlQuarter.SelectedValue
        End Get
        Set(ByVal value As Integer)
            ddlQuarter.SelectedValue = value
        End Set
    End Property

    Public Property AgencyCode() As String
        Get
            Return ddlSupplier.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlSupplier.SelectedValue = value
        End Set
    End Property
    Public Property ChannelCode() As String
        Get
            Return ddlChannel.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlChannel.SelectedValue = value
        End Set
    End Property
    Public Property CustRegionCode() As String
        Get
            Return ddlRegion.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlRegion.SelectedValue = value
        End Set
    End Property

#End Region

#Region "Event Handler"
    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgExport.Click
        RaiseEvent ExportBtn_Click(Me, e)
    End Sub

    Protected Sub imgSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSearch.Click
        Session("YEAR") = ddlYear.SelectedValue
        Session("MONTH") = ddlMonth.SelectedValue
        Session("QUARTER") = ddlQuarter.SelectedValue
        RaiseEvent SearchBtn_Click(Me, e)
    End Sub

#End Region

#Region "Export"
    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "")
        Excel.ExportToFile(dgList, Response, customFileName)
    End Sub

    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "")
        Excel.ExportToFile(aryDgList, Response, customFileName)
    End Sub

    Public Sub ExportToFile(ByRef PrintPanel As Object, Optional ByVal customFileName As String = "")
        Excel.ExportToFile(PrintPanel, Response, customFileName)
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
