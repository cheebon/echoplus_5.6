'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	20/07/2006
'	Purpose	    :	Logging Properties 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On 
Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
'Imports System.ComponentModel

'<Guid("D71F4D4E-E7E3-46b2-9DC1-4BE6C5D4B2F3"), 
'<Transaction(TransactionOption.Disabled), ClassInterface(ClassInterfaceType.None)> _
Public Class clsProperties
    'Inherits ServicedComponent

    Public Class clsLog
        Public LogTypeID As Long
        Public SeverityID As Long
        Public DateLogIn As DateTime
        Public DateLogOut As DateTime
        Public LogMsg As String
        Public Remarks As String
    End Class

End Class
