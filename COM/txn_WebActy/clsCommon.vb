
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCommon
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCommon"
        End Get
    End Property

    Public Function GetSRVisitID(ByVal strSalesRepCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_VISITID"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addOutput("VISIT_ID", 2)
            Return .spRetrieveValue("VISIT_ID")
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCode(ByVal strUserId As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_SALESREPCODE"
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            .addOutput("SALESREP_CODE", 2)
            Return .spRetrieveValue("SALESREP_CODE")
        End With
        objDB = Nothing
    End Function

    Public Function GetSRKIVTxnList(ByVal strTxnType As String, ByVal strSalesRepCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_KIV_TXN_LIST"
            .addItem("TXN_TYPE", strTxnType, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function LoadSRKIVTxnList(ByVal strTxnType As String, ByVal strSessionId As String, ByVal strTxnNo As String, _
    ByVal strVisitId As String, ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_LOAD_KIV_TXN_LIST"
            .addItem("TXN_TYPE", strTxnType, clsDB.DataType.DBString)
            .addItem("SESSION_ID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetHdrSRTxnList(ByVal strTxnType As String, ByVal strTxnNo As String, ByVal strVisitId As String, _
    ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
    ByVal strStatusCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TXN_HDR_LIST"
            .addItem("TXN_TYPE", strTxnType, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatusCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetDtlSRTxnList(ByVal strTxnType As String, ByVal strTxnNo As String, ByVal strVisitId As String, _
    ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
    ByVal strStatusCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TXN_DTL_LIST"
            .addItem("TXN_TYPE", strTxnType, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatusCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function CancelHdrDtlSRTxnList(ByVal strTxnType As String, ByVal strTxnNo As String, ByVal strVisitId As String, _
       ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
       ByVal strSessionID As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TXN_HDR_DTL_CANCEL"
            .addItem("TXN_TYPE", strTxnType, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("SESSION_ID", strSessionID, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function SubmitHdrDtlSRTxnList(ByVal strTxnType As String, ByVal strTxnNo As String, ByVal strVisitId As String, _
     ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, _
     ByVal strSessionID As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TXN_HDR_DTL_SUBMIT"
            .addItem("TXN_TYPE", strTxnType, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitId, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("SESSION_ID", strSessionID, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetAdmConfig(ByVal strSettingCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_ADM_CONFIG"
            .addItem("SETTING_CODE", strSettingCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetVisitAdmConfig(ByVal strSettingCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_ADM_CONFIG_VISIT"
            .addItem("SETTING_CODE", strSettingCode, clsDB.DataType.DBString)
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetDefaultDate() As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_VISITDEFAULTBACKDATE"
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
