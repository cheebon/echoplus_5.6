<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_txtDate.ascx.vb" Inherits="include_wuc_txtDate" %>
<div style="vertical-align: text-top; float: left;">
    <asp:TextBox ID="txtDate" runat="server" CssClass="cls_textbox"></asp:TextBox>
    <asp:Image ID="imgDate" runat="server" CssClass="clsButton" ImageUrl="~/images/icoCalendar.gif" AlternateText="Click to show calendar" />
    <asp:CustomValidator ID="rfvFormatDate" runat="server" Display="Dynamic" ControlToValidate="txtDate" CssClass="cls_validator" ValidateEmptyText="false" />
<asp:RequiredFieldValidator ID="rfvDateStart" runat="server" Display="Dynamic" ControlToValidate="txtDate" ErrorMessage="<br />Date cannot be blank !" CssClass="cls_validator" />
<asp:CompareValidator ID="rfvCheckDataType" runat="server" CssClass="cls_validator" ControlToValidate="txtDate" Display="Dynamic" Operator="DataTypeCheck" Type="Date" ErrorMessage="<BR/>Invalid Date!" />
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate" PopupButtonID="imgDate" Format="yyyy-MM-dd" CssClass="CalendarExtender"></ajaxToolkit:CalendarExtender>
</div>



