<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_SRSearch.ascx.vb"
    Inherits="iFFMS_Common_wuc_SRSearch" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Assembly="cor_CustomCtrl" Namespace="cor_CustomCtrl" TagPrefix="ccGV" %>
<%@ Register TagPrefix="customControl" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel DefaultButton="btnSearch" ID="pnlMsgPop" runat="server" Style="display: none;
            width: 600px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float: left; width: 95%; padding-top: 5px; padding-bottom: 5px;">
                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float: left; width: 5%; padding-top: 2px; padding-bottom: 1px;">
                                <asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%; padding-bottom: 5px;">
                    <table>
                        <tr>
                            <td><span style="float: left; width: 100px; padding-top: 2px;" class="cls_label_header">Salesrep Code</span></td>
                            <td><span style="float: left; width: 2px; padding-top: 2px;" class="cls_label_header">:</span></td>
                            <td><asp:TextBox ID="txtSalesrepCode" runat="server" CssClass="cls_textbox" /></td>
                            <td><span style="float: left; width: 200px; padding-top: 2px;" class="cls_label_header">(eg. *1003432*)</span></td>
                        </tr>
                        <tr>
                            <td><span style="float: left; width: 100px; padding-top: 2px;" class="cls_label_header">Salesrep Name</span></td>
                            <td><span style="float: left; width: 2px; padding-top: 2px;" class="cls_label_header">:</span></td>
                            <td><asp:TextBox ID="txtSalesrepName" runat="server" CssClass="cls_textbox" /></td>
                            <td><span style="float: left; width: 200px; padding-top: 2px;" class="cls_label_header">(eg. *John*)</span></td>
                        </tr>
                        <tr>
                            <td colspan="4"><asp:Button ID="btnSearch" runat="server" CssClass="cls_button" Text="Search" /></td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset style="padding-left: 10px; width: 100%;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                    <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="250px" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="SALESREP_CODE,SALESREP_NAME,SALES_AREA_CODE,SALES_AREA_NAME">
                        <EmptyDataTemplate>
                            <customControl:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                        </EmptyDataTemplate>
                    </ccGV:clsGridView>
                    <asp:HiddenField ID="hdSalesrepCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdSalesrepName" runat="server" Value="" />
                    <asp:HiddenField ID="hdSalesAreaCode" runat="server" Value="" />
                    <asp:HiddenField ID="hdSalesAreaName" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" PopupControlID="pnlMsgPop" BackgroundCssClass="modalBackground"
            DropShadow="True" RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>
