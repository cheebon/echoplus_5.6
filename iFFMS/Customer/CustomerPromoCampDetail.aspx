<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerPromoCampDetail"
    CodeFile="CustomerPromoCampDetail.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Promo Camp Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCustPromoCampDtl" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="Bckgroundreport">
                                                            <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                                <ContentTemplate>
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                                    </asp:Timer>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td align="left" colspan="3">
                                                                                <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" Width="60px" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label11" runat="server" CssClass="cls_label_header">Cust ID :</asp:Label></td>
                                                                            <td align="left" style="width: auto; height: 21px;" colspan="2" valign="top">
                                                                                <asp:Label ID="lblCustCode" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label4" runat="server" CssClass="cls_label_header">Name : </asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblName" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label1" runat="server" CssClass="cls_label_header" Text="Campaign Code : "
                                                                                    Width="117px"></asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblCampaignCode" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label2" runat="server" CssClass="cls_label_header">Title :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblTitle" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label5" runat="server" CssClass="cls_label_header">Details :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblDetails" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label6" runat="server" CssClass="cls_label_header">Department :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblDept" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label7" runat="server" CssClass="cls_label_header">Start Date :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblStartDate" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label8" runat="server" CssClass="cls_label_header">End Date :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblEndDate" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label9" runat="server" CssClass="cls_label_header">X Field 1 :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblXField1" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label10" runat="server" CssClass="cls_label_header">X Field 2 :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblXField2" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" style="width: 150px; height: 21px;" valign="top">
                                                                                <asp:Label ID="Label3" runat="server" CssClass="cls_label_header">X Field 3 :</asp:Label></td>
                                                                            <td align="left" colspan="2" style="width: auto; height: 21px;" valign="top">
                                                                                <asp:Label ID="lblXField3" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
