Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Partial Class iFFMA_HCD_MssCont_MssContDtl
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum dgcol As Integer

        UPDATED_FLAG = 6
        MANDATORY_FLAG = 7
        QUES_NAME = 3
    End Enum

    Public ReadOnly Property PageName() As String
        Get
            Return "MssContactDtl.aspx"
        End Get
    End Property

    Public Property TxnAction() As String
        Get
            Return ViewState("TxnAction")
        End Get
        Set(ByVal value As String)
            ViewState("TxnAction") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MSS_CONT)
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

            End If

            lblErr.Text = ""
            lblmsg.Text = ""

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub GetTxnActionDesc()
        Dim clsMssCont As New mst_Contact.clsMssCont
        Dim DT As DataTable = Nothing
        Dim strTxnNo As String, strTitleCode As String, strSalesrepCode As String, strCustCode As String, strContCode As String

        strTxnNo = Request.QueryString("TXN_NO")
        strTitleCode = Request.QueryString("TITLE_CODE")
        strSalesrepCode = Request.QueryString("SALESREP_CODE")
        strCustCode = Request.QueryString("CUST_CODE")
        strContCode = Request.QueryString("CONT_CODE")
        DT = clsMssCont.GetActionDesc(strTxnNo, strSalesrepCode, strCustCode, strContCode, strTitleCode)
        TxnAction = DT.Rows(0)("ACTION")
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then
            GetTxnActionDesc()
            RefreshDatabinding()
        End If

        TimerControl1.Enabled = False
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnapprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnapprove.Click
        Try
            If dgList.Rows.Count > 0 Then
                Dim dt As DataTable

                Dim strTxnNo As String, strTitleCode As String, strSalesrepCode As String, strCustCode As String, strContCode As String

                strTxnNo = Request.QueryString("TXN_NO")
                strSalesrepCode = Request.QueryString("SALESREP_CODE")
                strCustCode = Request.QueryString("CUST_CODE")
                strContCode = Request.QueryString("CONT_CODE")
                strTitleCode = Request.QueryString("TITLE_CODE")

                Dim clsMssCont As New mst_Contact.clsMssCont
                dt = clsMssCont.ValidateMssContDtl(strTxnNo, strTitleCode, strSalesrepCode, strCustCode, strContCode)

                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)(0) = 0 Then
                        ApproveSelected()
                    ElseIf dt.Rows(0)(0) = -1 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Unable to proceed, due to incomplete KEY TXN INFORMATION');", True)
                    Else
                        ActivateAddTab()
                        dgContGenList.DataSource = clsMssCont.GetDuplicatedList
                        dgContGenList.DataBind()
                        If dgContGenList.Rows.Count > 0 Then
                            btnapproveDuplicate.Visible = True
                        End If
                        ' ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "ConfirmSave();", True)
                    End If
                End If
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        ApproveSelected()
    End Sub

    Protected Sub btnvalidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnvalidate.Click
        If dgList.Rows.Count > 0 Then
            Dim dt As DataTable

            Dim strTxnNo As String, strTitleCode As String, strSalesrepCode As String, strCustCode As String, strContCode As String

            strTxnNo = Request.QueryString("TXN_NO")
            strSalesrepCode = Request.QueryString("SALESREP_CODE")
            strCustCode = Request.QueryString("CUST_CODE")
            strContCode = Request.QueryString("CONT_CODE")
            strTitleCode = Request.QueryString("TITLE_CODE")

            Dim clsMssCont As New mst_Contact.clsMssCont
            dt = clsMssCont.ValidateMssContDtl(strTxnNo, strTitleCode, strSalesrepCode, strCustCode, strContCode)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0)(0) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Contact name does not exists in the database! No duplidation detected.');", True)
                ElseIf dt.Rows(0)(0) = -1 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Unable to proceed, due to incomplete KEY TXN INFORMATION.');", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Contact name already exists in the database! Kindly make the necessary changes.');", True)
                End If
            End If
        End If
    End Sub

    Protected Sub btnapproveDuplicate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnapproveDuplicate.Click
        Try
            If dgContGenList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim updatedrows As Integer = 0
                Dim SelectedCount As Integer = 0
                Dim strContCode As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dgContGenList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dgContGenList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                            strContCode = dgContGenList.DataKeys(i).Item("CONT_CODE")
                            If strContCode = "NEW CONTACT" Then
                                ApproveSelected()
                                ActivateFirstTab()
                            Else
                                ApproveDuplicateSelected(strContCode)
                                ApproveSelected()
                                ActivateFirstTab()
                            End If

                        End If
                        SelectedCount = SelectedCount + 1
                        Exit For
                    End If
                    i += 1

                Next

                If SelectedCount = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select either one record to continue!');", True)
                End If


            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCancelDuplicate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDuplicate.Click
        ActivateFirstTab()
    End Sub
#End Region

    Private Sub ApproveSelected()
        If dgList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0
            Dim updatedrows As Integer = 0
            Dim ApprovedCount As Integer = 0
            Dim strTxnNo As String, strTitleCode As String, strQuesCode As String, strSubQuesCode As String, _
            strSalesrepCode As String, strCustCode As String, strContCode As String

            Dim DK As DataKey
            For Each DR As GridViewRow In dgList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                    DK = dgList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        strTxnNo = Request.QueryString("TXN_NO")
                        strSalesrepCode = Request.QueryString("SALESREP_CODE")
                        strCustCode = Request.QueryString("CUST_CODE")
                        strContCode = Request.QueryString("CONT_CODE")
                        strTitleCode = Request.QueryString("TITLE_CODE")
                        strQuesCode = dgList.DataKeys(i).Item("QUES_CODE")
                        strSubQuesCode = dgList.DataKeys(i).Item("SUB_QUES_CODE")
                        Dim strEditType As String = dgList.DataKeys(i).Item("TYPE")
                        Dim strCriteriaCodeGrid As String
                        If strEditType = 5 OrElse strEditType = 6 Then
                            strCriteriaCodeGrid = dgList.DataKeys(i).Item("CRITERIA_CODE_GRID")
                        End If

                        Dim clsMssCont As New mst_Contact.clsMssCont
                        clsMssCont.ApproveMssContDtl(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSalesrepCode, strCustCode, strContCode, strCriteriaCodeGrid, strEditType)
                        updatedrows = updatedrows + 1
                    End If
                End If
                i += 1

            Next
            lblmsg.Text = CStr(updatedrows) + " row(s) updated!"

            TimerControl1.Enabled = True
        End If
    End Sub

    Private Sub ApproveDuplicateSelected(ByVal strUpdateCustCode As String)

        Dim strTxnNo As String, strTitleCode As String, _
        strSalesrepCode As String, strCustCode As String, strContCode As String

        strTxnNo = Request.QueryString("TXN_NO")
        strSalesrepCode = Request.QueryString("SALESREP_CODE")
        strCustCode = Request.QueryString("CUST_CODE")
        strContCode = Request.QueryString("CONT_CODE")
        strTitleCode = Request.QueryString("TITLE_CODE")

        Dim clsMssCont As New mst_Contact.clsMssCont
        clsMssCont.ApproveMssContDtlDuplicate(strTxnNo, strTitleCode, strSalesrepCode, strCustCode, strContCode, strUpdateCustCode)

    End Sub

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        'wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMssCont As New mst_Contact.clsMssCont
            Dim strTxnNo As String, strTitleCode As String, strSalesrepCode As String, strCustCode As String, strContCode As String

            strTxnNo = Request.QueryString("TXN_NO")
            strTitleCode = Request.QueryString("TITLE_CODE")
            strSalesrepCode = Request.QueryString("SALESREP_CODE")
            strCustCode = Request.QueryString("CUST_CODE")
            strContCode = Request.QueryString("CONT_CODE")
            DT = clsMssCont.GetMssContDtl(strTxnNo, strTitleCode, strSalesrepCode, strCustCode, strContCode)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                    btncheckall.Visible = False
                    btnuncheckall.Visible = False
                    btnapprove.Visible = False
                    btnvalidate.Visible = False
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                    btncheckall.Visible = True
                    btnuncheckall.Visible = True
                    btnapprove.Visible = True
                    btnvalidate.Visible = True
                End If
            End If


            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = IIf(Master_Row_Count > 0, True, False)
            'End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If


            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgList.RowCancelingEdit
        Try
            dgList.EditIndex = -1
            RefreshDatabinding()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try

            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    Dim btnedit As Button = CType(e.Row.FindControl("btnedit"), Button)
                    Dim btncancel As Button = CType(e.Row.FindControl("btncancel"), Button)
                    Dim btnupdate As Button = CType(e.Row.FindControl("btnupdate"), Button)
                    Dim chkselect As CheckBox = CType(e.Row.FindControl("chkselect"), CheckBox)


                    If e.Row.RowIndex = dgList.EditIndex Then 'CHANGE THE ROW TO EDIT MODE FOR EDITING
                        Dim EditIndex As Integer = dgList.EditIndex

                        Dim txtS1 As TextBox = CType(e.Row.FindControl("txtS1"), TextBox)
                        Dim ddlS1 As DropDownList = CType(e.Row.FindControl("ddlS1"), DropDownList)
                        Dim txtDateS1 As include_wuc_txtdate_Grid = CType(e.Row.FindControl("txtDateS1"), include_wuc_txtdate_Grid)

                        If EditIndex >= 0 Then
                            Dim strEditType As Integer
                            strEditType = dgList.DataKeys(EditIndex).Item("TYPE")
                            Dim strTitleCode As String, strQuesCode As String, strSubQuesCode As String
                            strTitleCode = Request.QueryString("TITLE_CODE")
                            strQuesCode = dgList.DataKeys(EditIndex).Item("QUES_CODE")
                            strSubQuesCode = dgList.DataKeys(EditIndex).Item("SUB_QUES_CODE")

                            btnedit.Visible = False
                            btncancel.Visible = True
                            btnupdate.Visible = True

                            Select Case strEditType
                                Case 1 'textbox
                                    txtS1.Visible = True
                                    ddlS1.Visible = False
                                    txtDateS1.Visible = False
                                Case 2 'dropdownlist
                                    ddlS1.Visible = True
                                    txtS1.Visible = False
                                    txtDateS1.Visible = False

                                    Dim clsMssCont As New mst_Contact.clsMssCont
                                    With ddlS1
                                        Dim strSalesrepCode As String
                                        strSalesrepCode = Request.QueryString("SALESREP_CODE")
                                        .DataSource = clsMssCont.GetMssContCriteriaList(strTitleCode, strQuesCode, strSubQuesCode, strSalesrepCode)
                                        .DataTextField = "CRITERIA_NAME"
                                        .DataValueField = "CRITERIA_CODE"
                                        .DataBind()
                                        Dim lstItem As ListItem = ddlS1.Items.FindByText(Trim(txtS1.Text))
                                        If lstItem IsNot Nothing Then
                                            ddlS1.SelectedIndex = -1
                                            lstItem.Selected = True
                                        End If
                                    End With
                                Case 3 'date
                                    txtDateS1.Visible = True
                                    txtS1.Visible = False
                                    ddlS1.Visible = False
                                    txtDateS1.Text = txtS1.Text
                                Case 4 'dropdownlist
                                    ddlS1.Visible = True
                                    txtS1.Visible = False
                                    txtDateS1.Visible = False

                                    Dim clsMssCont As New mst_Contact.clsMssCont
                                    With ddlS1
                                        Dim strSalesrepCode As String
                                        strSalesrepCode = Request.QueryString("SALESREP_CODE")
                                        .DataSource = clsMssCont.GetMssContCriteriaList(strTitleCode, strQuesCode, strSubQuesCode, strSalesrepCode)
                                        .DataTextField = "CRITERIA_NAME"
                                        .DataValueField = "CRITERIA_CODE"
                                        .DataBind()
                                        Dim lstItem As ListItem = ddlS1.Items.FindByText(Trim(txtS1.Text))
                                        If lstItem IsNot Nothing Then
                                            ddlS1.SelectedIndex = -1
                                            lstItem.Selected = True
                                        End If

                                    End With

                                Case 5 'textbox
                                    txtS1.Visible = True
                                    ddlS1.Visible = False
                                    txtDateS1.Visible = False
                                Case 6 'dropdownlist
                                    ddlS1.Visible = True
                                    txtS1.Visible = False
                                    txtDateS1.Visible = False
                            End Select
                        Else
                            btnedit.Visible = True
                            btncancel.Visible = False
                            btnupdate.Visible = False
                        End If
                    Else
                        btnedit.Visible = True
                        btncancel.Visible = False
                        btnupdate.Visible = False
                    End If

                    If Master_Row_Count > 0 Then
                        Dim strstatusCode As String, strQuesName As String, strMandatoryFlag As String
                        strstatusCode = Trim(e.Row.Cells(dgcol.UPDATED_FLAG).Text)
                        strQuesName = Trim(e.Row.Cells(dgcol.QUES_NAME).Text)
                        strMandatoryFlag = dgList.DataKeys(e.Row.RowIndex).Item("MANDATORY_FLAG")

                        If strstatusCode.ToUpper = "UPDATED" Then
                            btnedit.Visible = False
                            chkselect.Enabled = False
                        ElseIf strMandatoryFlag = 1 Then
                            chkselect.Checked = True
                            chkselect.Enabled = False
                        End If

                    End If
            End Select

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

        Try
            Dim index As Integer = e.NewEditIndex
            dgList.EditIndex = index


            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgList_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgList.RowUpdating
        Try
            Dim EditIndex As Integer = dgList.EditIndex
            Dim strTxnNo As String, strTitleCode As String, strQuesCode As String, strSubQuesCode As String, strUpdateValue As String, strUpdateText As String
            Dim strEditType As Integer

            strEditType = dgList.DataKeys(EditIndex).Item("TYPE")
            strTxnNo = Request.QueryString("TXN_NO")
            strTitleCode = Request.QueryString("TITLE_CODE")
            strQuesCode = dgList.DataKeys(EditIndex).Item("QUES_CODE")
            strSubQuesCode = dgList.DataKeys(EditIndex).Item("SUB_QUES_CODE")
            Dim strCriteriaCodeGrid As String
            If strEditType = 5 OrElse strEditType = 6 Then
                strCriteriaCodeGrid = dgList.DataKeys(EditIndex).Item("CRITERIA_CODE_GRID")
            End If


            Dim txtS1 As TextBox = CType(dgList.Rows(e.RowIndex).FindControl("txtS1"), TextBox)
            Dim ddlS1 As DropDownList = CType(dgList.Rows(e.RowIndex).FindControl("ddlS1"), DropDownList)
            Dim txtDateS1 As include_wuc_txtdate_Grid = CType(dgList.Rows(e.RowIndex).FindControl("txtDateS1"), include_wuc_txtdate_Grid)

            strUpdateValue = ""
            strUpdateText = ""
            Select Case strEditType
                Case 1 'textbox
                    strUpdateValue = txtS1.Text
                Case 2 'dropdownlist
                    strUpdateValue = ddlS1.SelectedValue
                    strUpdateText = ddlS1.SelectedItem.Text
                Case 3 'date
                    strUpdateValue = txtDateS1.Text
                Case 4 'dropdownlist
                    strUpdateValue = ddlS1.SelectedValue
                    strUpdateText = ddlS1.SelectedItem.Text
                Case 5 'textbox
                    strUpdateValue = txtS1.Text
                Case 6 'dropdownlist
                    strUpdateValue = ddlS1.SelectedValue
                    strUpdateText = ddlS1.SelectedItem.Text
            End Select

            Dim clsMssCont As New mst_Contact.clsMssCont
            clsMssCont.UpdMssContDtl(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strUpdateValue, strUpdateText, strEditType, strCriteriaCodeGrid)
            dgList.EditIndex = -1

            lblmsg.Text = "1 rows updated!"
            RefreshDatabinding()
            'MsgBox(strTxnNo + strTitleCode + strQuesCode + strSubQuesCode + strUpdateValue + strUpdateText)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region



    'Protected Sub dgContGenList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgContGenList.RowCreated

    '    Try
    '        If e.Row.RowType = DataControlRowType.Header Then
    '        ElseIf e.Row.RowType = DataControlRowType.DataRow Then

    '            Dim strContCode = dgContGenList.DataKeys(e.Row.RowIndex).Item("CONT_CODE")
    '            If strContCode <> "NEW CONTACT" Then
    '                Dim str As String
    '                str = "parent.document.getElementById('SubDetailBarIframe').src='../MssContDtl/MssContDtlList.aspx?CONT_CODE=" + strContCode + "';"
    '                e.Row.Cells(1).Attributes.Add("onclick", str)
    '            End If

    '        End If
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Mss Contact Detail List"
        pnlList.Visible = True

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""
        pnlContGenList.Visible = False

    End Sub

    Private Sub ActivateAddTab()
        TabPanel2.HeaderText = "Contact List"
        TabPanel2.Visible = True
        pnlContGenList.Visible = True
        tcResult.ActiveTabIndex = 1
        'pnlList.Visible = False
        'TabPanel1.Visible = False
        'TabPanel1.HeaderText = ""

    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub



End Class
