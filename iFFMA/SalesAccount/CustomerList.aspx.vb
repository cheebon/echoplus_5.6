'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	18/03/2008
'	Purpose	    :	Customer List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_SalesAccount_CustomerList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerList.aspx"
        End Get
    End Property
    Public Property CustCode() As String
        Get
            Return ViewState("CustCode")
        End Get
        Set(ByVal value As String)
            ViewState("CustCode") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CUSTOMER)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.CUSTOMER
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                LoadDDL()

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                End If

                'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, SubModuleAction.Edit) Then
                    btnSave.Visible = False
                    btnAddContact.Visible = False
                    btnDeleteContact.Visible = False
                End If
                '----------------------------------------------------------------------------------------------------

            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DDL"
    Private Sub LoadDDL()
        Dim dtRegion As DataTable
        Dim dtCustGrp As DataTable
        Dim dtPayTerm As DataTable

        Try
            Dim clsCommon As New mst_Common.clsDDL

            dtRegion = clsCommon.GetCustRegionDDL
            With ddlRegion
                .Items.Clear()
                .DataSource = dtRegion.DefaultView
                .DataTextField = "REGION_NAME"
                .DataValueField = "REGION_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            dtCustGrp = clsCommon.GetCustGrpDDL
            With ddlCustGrp
                .Items.Clear()
                .DataSource = dtCustGrp.DefaultView
                .DataTextField = "CUST_GRP_NAME"
                .DataValueField = "CUST_GRP_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            dtPayTerm = clsCommon.GetPayTermDDL
            With ddlPayTerm
                .Items.Clear()
                .DataSource = dtPayTerm.DefaultView
                .DataTextField = "PAY_TERM_NAME"
                .DataValueField = "PAY_TERM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
            'If Page.IsValid = False Then
            '    Exit Sub
            'Else
            '    ActivateFirstTab()
            '    RenewDataBind()
            'End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ViewState("CUST_CODE") = ""

        txtCustCode.Text = ""
        txtCustCode.Enabled = True
        txtCustName.Text = ""
        ddlStatus.SelectedIndex = 0
        txtAbbv.Text = ""
        txtAddr1.Text = ""
        txtAddr2.Text = ""
        txtAddr3.Text = ""
        txtAddr4.Text = ""
        txtPostcode.Text = ""
        txtCity.Text = ""
        ddlRegion.SelectedIndex = 0
        txtRouteCode.Text = ""
        txtTelNo1.Text = ""
        txtTelNo2.Text = ""
        txtFaxNo1.Text = ""
        txtFaxNo2.Text = ""
        txtEmail.Text = ""
        ddlRegion.Enabled = True
        ddlCustGrp.Enabled = True
        ddlPoison.Enabled = True
        ddlPayTerm.Enabled = True

        ddlCustGrp.SelectedIndex = 0
        ddlPoison.SelectedIndex = 0
        ddlPayTerm.SelectedIndex = 0
        

        btnSave.Visible = True 'HL: 20080429 (AR)
        ActivateAddTab()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = ""

            Dim clsCustomer As New mst_SalesAccount.clsCustomer
            Dim strSelectedCode As String = ViewState("CUST_CODE").ToString

            If strSelectedCode <> "" Then
                clsCustomer.UpdateCust(strSelectedCode, Trim(txtCustName.Text), ddlStatus.SelectedValue, _
                                        Trim(txtAbbv.Text), Trim(txtAddr1.Text), Trim(txtAddr2.Text), _
                                        Trim(txtAddr3.Text), Trim(txtAddr4.Text), Trim(txtPostcode.Text), _
                                        Trim(txtCity.Text), Trim(ddlRegion.SelectedValue), Trim(txtRouteCode.Text), _
                                        Trim(txtTelNo1.Text), Trim(txtTelNo2.Text), Trim(txtFaxNo1.Text), _
                                        Trim(txtFaxNo2.Text), Trim(txtEmail.Text), Trim(ddlCustGrp.SelectedValue), Trim(ddlPoison.SelectedValue), _
                                        Trim(ddlPayTerm.SelectedValue))

                ViewState("CUST_CODE") = strSelectedCode
                lblInfo.Text = "The record is successfully saved."
            Else
                Dim DT As DataTable

                DT = clsCustomer.CreateCust(Trim(txtCustCode.Text), Trim(txtCustName.Text), ddlStatus.SelectedValue, _
                                        Trim(txtAbbv.Text), Trim(txtAddr1.Text), Trim(txtAddr2.Text), _
                                        Trim(txtAddr3.Text), Trim(txtAddr4.Text), Trim(txtPostcode.Text), _
                                        Trim(txtCity.Text), Trim(ddlRegion.SelectedValue), Trim(txtRouteCode.Text), _
                                        Trim(txtTelNo1.Text), Trim(txtTelNo2.Text), Trim(txtFaxNo1.Text), _
                                        Trim(txtFaxNo2.Text), Trim(txtEmail.Text), Trim(ddlCustGrp.SelectedValue), Trim(ddlPoison.SelectedValue), _
                                        Trim(ddlPayTerm.SelectedValue))

                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Customer Code already exists!"
                Else
                    ViewState("CUST_CODE") = Trim(txtCustCode.Text)

                    'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
                    'HL: 20080429 (AR)
                    'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, SubModuleAction.Edit) Then
                    '    ActivateEditTab()
                    'Else
                    '    btnSave.Visible = False
                    'End If

                    ActivateEditTab()
                    If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, SubModuleAction.Edit) Then
                        btnSave.Visible = False
                    End If
                    '----------------------------------------------------------------------------------------------------

                    lblInfo.Text = "The record is successfully created."
                End If
                End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnAddContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddContact.Click
        With (wuc_CustomerContAddPop)
            .CustCode = ViewState("CUST_CODE")
            .ResetPage()
            .RenewDataBind()
            .Show()
        End With
    End Sub

    Protected Sub btnRefreshAddContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustomerContAddPop.SelectButton_Click
        Try
            wuc_CustomerContList.RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnDeleteContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteContact.Click
        With (wuc_CustomerContDelPop)
            .CustCode = ViewState("CUST_CODE")
            .ResetPage()
            .RenewDataBind()
            .Show()
        End With
    End Sub


    Protected Sub btnRefreshDelContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustomerContDelPop.SelectButton_Click
        Try
            wuc_CustomerContList.RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New mst_SalesAccount.clsCustomer
            With (wuc_toolbar)
                DT = clsCustomer.GetCustList(.SearchType, .SearchValue, .Status, .GPSStatus)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListExport() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New mst_SalesAccount.clsCustomer
            With (wuc_toolbar)
                DT = clsCustomer.GetCustListExport(.SearchType, .SearchValue, .Status)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            If isExport = True Then
                dtCurrentTable = GetRecListExport()

                ' Not all principal has this customized export spp. In the case of standard export, use back what is shown
                ' in grid.
                If dtCurrentTable Is Nothing Then
                    dtCurrentTable = GetRecList()
                End If
            Else
                dtCurrentTable = GetRecList()
            End If

            

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

            'ADD BUTTON EDIT
            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details-------------------------------------------------
            'If Master_Row_Count > 0 AndAlso _
            '    Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTOMER, SubModuleAction.Edit) Then 'HL: 20080429 (AR)
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")


                'ADD BUTTON VIEW MAP
                'KIWI: 20131128 
                Dim dgBtnColumnMap As New ButtonField

                dgBtnColumnMap.HeaderStyle.Width = "50"
                dgBtnColumnMap.HeaderText = "View Map"
                dgBtnColumnMap.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumnMap.ButtonType = ButtonType.Link
                    'dgBtnColumnMap.ShowEditButton = True
                    dgBtnColumnMap.Text = "<img src='../../images/ico_editProfile.gif' alt='View Map' border='0' height='18px' width='18px'/>"
                    dgBtnColumnMap.Visible = True
                  
                    dgBtnColumnMap.CommandName = "ViewMap"

                End If

                dgList.Columns.Add(dgBtnColumnMap)
                dgBtnColumnMap = Nothing
                aryDataItem.Add("BTN_MAP")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CustomerList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_CustomerList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_CustomerList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_CustomerList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_CustomerList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
     

        Try
            Select e.CommandName

                Case "ViewMap"

                    Dim index As Integer = Convert.ToInt32(e.CommandArgument)

                    CustCode = dgList.DataKeys(index).Item("CUST_CODE")
                    ActivateMapTab()
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strSelectedCode As String = sender.datakeys(e.NewEditIndex).item("CUST_CODE")
            Dim DT As DataTable
            Dim clsCustomer As New mst_SalesAccount.clsCustomer
            Dim lstSelected As ListItem
            lstSelected = Nothing

            DT = clsCustomer.GetCustDetails(strSelectedCode)

            If DT.Rows.Count > 0 AndAlso DT.Rows(0)("LOCK_FLAG") = "0" Then
                ViewState("CUST_CODE") = strSelectedCode
                txtCustCode.Text = IIf(IsDBNull(DT.Rows(0)("CUST_CODE")), "", DT.Rows(0)("CUST_CODE"))
                txtCustName.Text = IIf(IsDBNull(DT.Rows(0)("CUST_NAME")), "", DT.Rows(0)("CUST_NAME"))
                ddlStatus.SelectedValue = UCase(Trim(IIf(IsDBNull(DT.Rows(0)("STATUS")), "", DT.Rows(0)("STATUS"))))
                txtAbbv.Text = IIf(IsDBNull(DT.Rows(0)("ABBV")), "", DT.Rows(0)("ABBV"))
                txtAddr1.Text = IIf(IsDBNull(DT.Rows(0)("ADD_1")), "", DT.Rows(0)("ADD_1"))
                txtAddr2.Text = IIf(IsDBNull(DT.Rows(0)("ADD_2")), "", DT.Rows(0)("ADD_2"))
                txtAddr3.Text = IIf(IsDBNull(DT.Rows(0)("ADD_3")), "", DT.Rows(0)("ADD_3"))
                txtAddr4.Text = IIf(IsDBNull(DT.Rows(0)("ADD_4")), "", DT.Rows(0)("ADD_4"))
                txtPostcode.Text = IIf(IsDBNull(DT.Rows(0)("POSTCODE")), "", DT.Rows(0)("POSTCODE"))
                txtCity.Text = IIf(IsDBNull(DT.Rows(0)("CITY")), "", DT.Rows(0)("CITY"))

                ddlRegion.SelectedIndex = -1
                lstSelected = ddlRegion.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("REGION_CODE")), "", DT.Rows(0)("REGION_CODE")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                txtRouteCode.Text = IIf(IsDBNull(DT.Rows(0)("ROUTE_CODE")), "", DT.Rows(0)("ROUTE_CODE"))
                txtTelNo1.Text = IIf(IsDBNull(DT.Rows(0)("TEL_NO_1")), "", DT.Rows(0)("TEL_NO_1"))
                txtTelNo2.Text = IIf(IsDBNull(DT.Rows(0)("TEL_NO_2")), "", DT.Rows(0)("TEL_NO_2"))
                txtFaxNo1.Text = IIf(IsDBNull(DT.Rows(0)("FAX_NO_1")), "", DT.Rows(0)("FAX_NO_1"))
                txtFaxNo2.Text = IIf(IsDBNull(DT.Rows(0)("FAX_NO_2")), "", DT.Rows(0)("FAX_NO_2"))
                txtEmail.Text = IIf(IsDBNull(DT.Rows(0)("EMAIL")), "", DT.Rows(0)("EMAIL"))

                ddlCustGrp.SelectedIndex = -1
                lstSelected = ddlCustGrp.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("CUST_GRP_CODE")), "", DT.Rows(0)("CUST_GRP_CODE")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                ddlPoison.SelectedIndex = -1
                lstSelected = ddlPoison.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("POISON_IND")), "", DT.Rows(0)("POISON_IND")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                ddlPayTerm.SelectedIndex = -1
                lstSelected = ddlPayTerm.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("PAY_TERM_CODE")), "", DT.Rows(0)("PAY_TERM_CODE")))
                If lstSelected IsNot Nothing Then lstSelected.Selected = True

                Dim sap_ind As String 'EUJIN CHECK IF ITS COME FROM SAP
                sap_ind = IIf(IsDBNull(DT.Rows(0)("SAP_IND")), "S", DT.Rows(0)("SAP_IND"))
                If sap_ind.ToUpper = "S" Then
                    ddlRegion.Enabled = False
                    ddlCustGrp.Enabled = False
                    ddlPoison.Enabled = False
                    ddlPayTerm.Enabled = False
                Else
                    ddlRegion.Enabled = True
                    ddlCustGrp.Enabled = True
                    ddlPoison.Enabled = True
                    ddlPayTerm.Enabled = True
                End If



                ActivateEditTab()
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "<script>alert('This record is locked by other user!');</script>", False)
                RenewDataBind()
            End If
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Customer List"
        pnlList.Visible = True
        pnlDetails.Visible = False
        TabPanel1.Visible = True
        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""

        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""

        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""
        'pnlMap.Visible = False
        ifMap.Attributes.Add("src", "")
        ifMap.Visible = False
        wuc_toolbar.show()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateAddTab()
        TabPanel1.HeaderText = "Customer Details"
        pnlList.Visible = False
        pnlDetails.Visible = True
        'pnlMap.Visible = False
        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""
        ifMap.Attributes.Add("src", "")
        ifMap.Visible = False

        TabPanel2.Visible = False
        TabPanel2.HeaderText = ""

        TabPanel3.Visible = False
        TabPanel3.HeaderText = ""

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub

    Private Sub ActivateEditTab()
        TabPanel1.HeaderText = "Customer Details"
        pnlList.Visible = False
        pnlDetails.Visible = True

        TabPanel2.Visible = True
        TabPanel2.HeaderText = "Price Group"

        TabPanel3.Visible = True
        TabPanel3.HeaderText = "Contact List"

        TabPanel4.Visible = False
        TabPanel4.HeaderText = ""
        ifMap.Attributes.Add("src", "")
        ifMap.Visible = False

        txtCustCode.Enabled = False

        wuc_CustomerPriceGrpList.CustCode = Trim(txtCustCode.Text)
        wuc_CustomerContList.CustCode = Trim(txtCustCode.Text)
        RenewAllTab()

        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub
    Private Sub ActivateMapTab()
        ' pnlMap.Visible = True
        TabPanel4.Visible = True
        TabPanel4.HeaderText = ""
        TabPanel4.HeaderText = "Get address"
        pnlList.Visible = False
        pnlDetails.Visible = False
        TabPanel1.Visible = False
        ifMap.Attributes.Add("src", "CustomerGetAddress.aspx?custcode=" + CustCode)
        ifMap.Visible = True
        wuc_toolbar.hide()
        lblInfo.Text = ""
    End Sub
    Private Sub RenewAllTab()
        wuc_CustomerPriceGrpList.RenewDataBind()
        wuc_CustomerContList.RenewDataBind()
    End Sub

    Private Sub RefreshAllTab()
        wuc_CustomerPriceGrpList.RefreshDataBind()
        wuc_CustomerContList.RefreshDataBind()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList)
            If TabPanel2.Visible Then
                aryDgList.Add(wuc_CustomerPriceGrpList.GetdgPriceGrpList())
            End If
            If TabPanel3.Visible Then
                aryDgList.Add(wuc_CustomerContList.GetdgContactList())
            End If

            'If tcResult.ActiveTab.ID = "TabPanel3" Then
            '    aryDgList.Add(wuc_CustomerContList.GetdgContactList())
            'Else
            '    aryDgList.Add(dgList)
            '    If TabPanel2.Visible Then
            '        aryDgList.Add(wuc_CustomerPriceGrpList.GetdgPriceGrpList())
            '    End If
            'End If



            wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            If TabPanel2.Visible Or TabPanel3.Visible Then
                RefreshAllTab()
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class

Public Class CF_CustomerList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "ABBV"
                strFieldName = "Abbreviation"
            Case "GPS_STATUS"
                strFieldName = "GPS Status"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

