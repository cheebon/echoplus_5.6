'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	06/10/2006
'	Purpose	    :	User Create
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class iFFMS_Map_map
    Inherits System.Web.UI.Page

    Private strEchoplusConn As String = "server=KULGCA01;database=MY_iFFMS_INT_V5;User ID=sa;password=csscbs"

    Protected Sub ddlSalesrepCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesrepCode.SelectedIndexChanged
        Dim dt As DataTable
        Dim i As Integer
        Dim strScript As String = ""
        Dim strinlatitude As String = ""
        Dim strinlongitude As String = ""

        Try
            If ddlSalesrepCode.SelectedValue <> 0 Or ddlSalesrepCode.SelectedIndex > 0 Then
                dt = GetDailyTimeSummListBySR(ddlSalesrepCode.SelectedValue)
                For i = 0 To dt.Rows.Count - 1

                    If Not IsDBNull(dt.Rows(i)("in_latitude")) Then
                        If dt.Rows(i)("in_latitude") <> "" Then
                            strinlatitude = Mid(dt.Rows(i)("in_latitude"), 1, Len(dt.Rows(i)("in_latitude")) - 1)
                            strinlongitude = Mid(dt.Rows(i)("in_longitude"), 1, Len(dt.Rows(i)("in_longitude")) - 1)
                        End If
                    End If

                    'strScript = "<script type='text/javascript' language='javascript'>"
                    'strScript += "</" + "script>"
                    ' txtScript.Text = strScript
                    If (strinlatitude <> "" And IsNumeric(strinlatitude)) And (strinlongitude <> "" And IsNumeric(strinlongitude)) Then
                        strScript += "addposicon('" & strinlatitude & "','" & strinlongitude & "','" & ddlSalesrepCode.SelectedValue & "');"
                    End If
                Next
                If strScript <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "", strScript, True)
                    'ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "OnLoad", strScript, True)
                End If
            End If

        Catch ex As Exception
            lblErr.Text = "Map.ddlSalesrepCode_SelectedIndexChanged : " & ex.ToString()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            If Not Page.IsPostBack Then
                dt = GetSalesrepList()
                ddlSalesrepCode.Items.Clear()
                ddlSalesrepCode.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlSalesrepCode.Items.Add(New ListItem(Trim(drRow("salesrep_code")), Trim(drRow("salesrep_code"))))
                    Next
                End If
            End If
           
        Catch ex As Exception
            lblErr.Text = "Map.Page_Load : " & ex.ToString()
        End Try
    End Sub

    Private Function GetSalesrepList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String
        Dim dt As DataTable = Nothing

        Try
            strSQL = " SELECT DISTINCT salesrep_code" & _
                     " FROM TBL_TXN_DAILY_TIME_SUMM WITH (NOLOCK)"
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                dt = .Retrieve(strSQL)
            End With

        Catch ex As Exception
            lblErr.Text = "Map.GetSalesrepList : " & ex.ToString()
        Finally
            objDB = Nothing
        End Try
        Return dt
    End Function

    Private Function GetDailyTimeSummListBySR(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String
        Dim dt As DataTable = Nothing

        Try
            strSQL = " SELECT *" & _
                     " FROM TBL_TXN_DAILY_TIME_SUMM WITH (NOLOCK)" & _
                     " WHERE salesrep_code=@salesrep_code"
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .addItem("salesrep_code", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                dt = .Retrieve(strSQL)
            End With

        Catch ex As Exception
            lblErr.Text = "Map.GetDailyTimeSummListBySR : " & ex.ToString()
        Finally
            objDB = Nothing
        End Try
        Return dt
    End Function
End Class
