Imports System.Data

Partial Class ActualCallKPIbySR
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_StkAlloc"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

    Private _strPriceGrp As String
    Public Property PriceGroup() As String
        Get
            Return txtPricegroup.Text.Trim
        End Get
        Set(ByVal value As String)
            txtPricegroup.Text = value
        End Set
    End Property

    Private _strCustGrp As String
    Public Property CustomerGroup() As String
        Get
            Return txtCustgrp.Text.Trim
        End Get
        Set(ByVal value As String)
            txtCustgrp.Text = value
        End Set
    End Property



#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "ActualCallKPIbySR.aspx"
        End Get
    End Property
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.ActCallKPIbySR) '"Actual Call KPI by SR"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.ActCallKPIbySR
                .DataBind()
                .Visible = True
            End With

            lblErr.Text = ""

            If Not IsPostBack Then
                'If PreviousPage IsNot Nothing Then
                '    With CriteriaCollector
                '        Session.Item("Year") = .Year
                '        Session.Item("Month") = .Month
                '        session("PRINCIPAL_ID") = .PrincipalID
                '        session("PRINCIPAL_CODE") = .PrincipalCode
                '        session("TEAM_CODE") = .TeamCode
                '        session("REGION_CODE") = .RegionCode
                '        session("SALESREP_CODE") = .SalesrepCode
                '        ViewState("strSortExpression") = .SortExpression
                '        wuc_Menu.RestoreSessionState = True
                '    End With
                'Else
                '    CriteriaCollector = Nothing ' New clsSharedValue
                '    'SetButtonVisibility(ViewMode.ViewByFigure)
                'End If
                TimerControl1.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub

#Region "Event Handler"

    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            'wuc_ctrlPanel.RefreshDetails()
            'wuc_ctrlPanel.UpdateControlPanel()
            If IsPostBack Then RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            'ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            'PrepareGridViewForExport(dgList)
            'wuc_ctrlpanel.ExportToFile(dgList, "StockAllocationByReps")
            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Data Binding"
    Public Sub RenewDataBind(Optional isLocalFilter As Boolean = False)
        If isLocalFilter = False Then ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub


    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            ElseIf dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            Else
                PreRenderMode(dtCurrentTable)
            End If

            Dim strRowFilter As New StringBuilder
            If CustomerGroup.Length > 0 Then strRowFilter.Append("cg_code ='" & CustomerGroup & "'")
            If PriceGroup.Length > 0 Then
                If strRowFilter.Length > 0 Then strRowFilter.Append(" AND ")
                strRowFilter.Append("pricegrp_code ='" & PriceGroup & "'")
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable, strRowFilter.ToString, String.Empty, DataViewRowState.OriginalRows)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If


            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_ActCallKPIbySR.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_ActCallKPIbySR.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_ActCallKPIbySR.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_ActCallKPIbySR.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strYear, strMonth, strUserID, strPrincipalCode, strPrincipalID As String

            strUserID = Session.Item("UserID")
            strPrincipalID = Session("PRINCIPAL_ID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strYear = Session.Item("Year")
            strMonth = Session.Item("Month")


            Dim strSalesRep As String
            strSalesRep = Request.QueryString("SALESREP_CODE")
            'Stored Criteria into Static Value Collector
            'With CriteriaCollector
            '    .PrincipalID = strPrincipalID
            '    .PrincipalCode = strPrincipalCode
            '    .Year = strYear
            '    .Month = strMonth
            '    .TeamCode = strTeamCode
            '    .RegionCode = strRegionCode
            '    .SalesrepCode = strSalesrepCode
            'End With

            Dim clsCallDB As New rpt_Customize.clsActCallKPIbySR
            DT = clsCallDB.GetActCallKPIbySR(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRep)
            PreRenderMode(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except ACH_*
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If strColumnName Like "HIT_QTY" Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        Try
            If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
                dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        End Try
        Return dblValue
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0.0

            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                Case DataControlRowType.Footer

                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_ActCallKPIbySR.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
                    Dim liHit As ListItem
                    liHit = licItemFigureCollector.FindByText("HIT_QTY")
                    If liHit IsNot Nothing Then
                        e.Row.Cells(aryDataItem.IndexOf("HIT_QTY")).Text = String.Format(CF_ActCallKPIbySR.GetOutputFormatString("HIT_QTY"), liHit.Value)
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try

    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    TimerControl1.Enabled = False
    '    RenewDataBind()
    'End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click

    End Sub

    Protected Sub btnFilter_Click(sender As Object, e As EventArgs) Handles btnFilter.Click
        RenewDataBind(True)
    End Sub
End Class

Public Class CF_ActCallKPIbySR
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "KPI"
                strFieldName = "KPI %"
            Case "CG_CODE"
                strFieldName = "CG Code"
            Case "PRICEGRP_CODE"
                strFieldName = "Price Group Code"
            Case "FREQ"
                strFieldName = "Freq"
            Case "ACT_QTY"
                strFieldName = "Actual Call"
            Case "HIT_QTY"
                strFieldName = "Hit"
            Case "CG_NAME"
                strFieldName = "CG Name"
            Case "PRICEGRP_Name"
                strFieldName = "Price Grp Name"
            Case "CUST GROUP"
                strFieldName = "Cust Group"
            Case "PRICE GROUP"
                strFieldName = "Price Group"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Select Case strColumnName.ToUpper
                Case "KPI"
                    FCT = FieldColumntype.HyperlinkColumn
                Case "CG_CODE", "PRICEGRP_CODE", "DATE"
                    FCT = FieldColumntype.InvisibleColumn
            End Select

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE", "START_DATE", "END_DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "FREQ"
                    strFormatString = "{0:0.0}"
                Case "ACT_QTY", "HIT_QTY"
                    strFormatString = "{0:#,0}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "CLASS" _
                 Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class