<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_toolbar.ascx.vb" Inherits="include_wuc_toolbar" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtCalendarRange" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_txtIntFieldRange.ascx" TagName="wuc_txtIntFieldRange" TagPrefix="customToolkit" %>
<link href="~/include/DKSH.css" rel="stylesheet" />

<%--<asp:UpdatePanel ID="UpdPnlToolbar" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>--%>
        
        <asp:Panel ID="pnlToolbar" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr align="left">
                    <td align="left">
                        
                        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0" width="100%" border="0" style="height:30px; padding:2px 0px 2px 2px " >
                            <tr valign="bottom">
                                <td>
                                    <asp:Image ID="imgEnquiry" ImageUrl="~/images/ico_field.gif" runat="server" CssClass="cls_button" ToolTip="Search"></asp:Image>
                                </td>
                                <td style="padding-left: 5px; padding-right: 5px">
                                    <asp:Image ID="imgEnquirySeparator" runat="server" ImageUrl="~/images/toolbarseparator.gif" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server" CssClass="cls_button" ToolTip="Export"></asp:ImageButton>
                                </td>
                                <td style="width: 95%;"></td>
                            </tr>
                        </table>
                            
                    </td>
                </tr>
                <tr align="left">
                    <td align="left">
                        <asp:UpdatePanel ID="UpdateCtrlPanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="pnlEnquiry" runat="server" Visible="true" CssClass="cls_ctrl_panel" DefaultButton="btnSearch">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr id="All Control Panel ">
                                            <td>
                                                <table cellspacing="0" cellpadding="2" width="900px" border="0" align="left">
                                                    <!-- BEGIN ADD HERE -->
                                                    <tr valign="bottom">
                                                        <td style="width: 150px"></td>
                                                        <td style="width: 2px"></td>
                                                        <td style="width: 648px"></td> 
                                                    </tr>
                                                    
                                                    <asp:Panel ID="pnlTeam" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Team</span> <span id="lblTeamastr" runat="server" class="cls_label_err" >*</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlSalesrep" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Field Force</span> <span class="cls_label_err" >*</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSalesrep" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlTechnician" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Technician</span> <span class="cls_label_err" >*</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTechnician" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlRoute" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Route</span> <span class="cls_label_err" >*</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlRoute" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlYear" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Year</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlMonth" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Month</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" >
                                                                <asp:ListItem Text="Jan" Value="1" ></asp:ListItem>
                                                                <asp:ListItem Text="Feb" Value="2" ></asp:ListItem>
                                                                <asp:ListItem Text="Mar" Value="3" ></asp:ListItem>
                                                                <asp:ListItem Text="Apr" Value="4" ></asp:ListItem>
                                                                <asp:ListItem Text="May" Value="5" ></asp:ListItem>
                                                                <asp:ListItem Text="Jun" Value="6" ></asp:ListItem>
                                                                <asp:ListItem Text="Jul" Value="7" ></asp:ListItem>
                                                                <asp:ListItem Text="Aug" Value="8" ></asp:ListItem>
                                                                <asp:ListItem Text="Sep" Value="9" ></asp:ListItem>
                                                                <asp:ListItem Text="Oct" Value="10" ></asp:ListItem>
                                                                <asp:ListItem Text="Nov" Value="11" ></asp:ListItem>
                                                                <asp:ListItem Text="Dec" Value="12" ></asp:ListItem>
                                                            </asp:DropDownList> 
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <asp:Panel ID="pnlSearch" runat="server" Visible="true" CssClass="cls_ctrl_panel">
                                                        <tr>
                                                            <td><span class="cls_label_header">Search By</span></td>
                                                            <td><span class="cls_label_header">:</span></td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" Width="115px" />
                                                            </td>
                                                        </tr>
                                                        <asp:Panel ID="pnlCtrl" runat="server" Visible="true" CssClass="cls_ctrl_panel">
                                                        <tr>
                                                            <td><span class="cls_label_header">Search Value</span></td>
                                                            <td><span class="cls_label_header">:</span></td>
                                                            <td>
                                                                <asp:TextBox ID="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                                                                        
                                                    <asp:Panel ID="pnlStatus" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Status</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" >
                                                                <asp:ListItem Value="">All</asp:ListItem>
                                                                <asp:ListItem Value="A">Active</asp:ListItem>
                                                                <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    
                                                    <%--kiwi 2014/10/20 added gps status for customer list--%>
                                                     <asp:Panel ID="pnlGPSStatus" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">GPS Status</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlGPSStatus" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" >
                                                                <asp:ListItem Value="">All</asp:ListItem>
                                                                <asp:ListItem Value="C">Confirmed</asp:ListItem>
                                                                <asp:ListItem Value="N">Unconfirmed</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlReasonType" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Reason Type</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlReasonType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                          
                                                    <asp:Panel ID="pnlViewType" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">View</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlViewType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" >
                                                                <asp:ListItem Value="0">New</asp:ListItem>
                                                                <asp:ListItem Value="1">Archive</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>
                                                     
                                                    <asp:Panel ID="pnlTxnDate" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td><span class="cls_label_header">Transaction Date</span></td>
                                                        <td><span class="cls_label_header">:</span></td>
                                                        <td>
                                                            <customToolkit:wuc_txtCalendarRange 
                                                                ID="txtTxnDate" 
                                                                runat="server"  
                                                                RequiredValidation="false"
                                                                RequiredValidationGroup="Search" 
                                                                DateFormatString="yyyy-MM-dd" 
                                                                CompareDateRangeValidation="true"
                                                           />
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>              
                                                    
                                                        <!-- For Customer Profile Maintenance or Customer Profile Enquiry or MSS Enquiry -->
                                                    <asp:Panel ID="pnlCustProfile" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                    <tr>
                                                        <td colspan="3">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                             <tr>
                                                                <td><span class="cls_label_header">Customer Name</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCustName" runat="server" CssClass="cls_textbox" MaxLength="100"></asp:TextBox>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">Customer Group</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <%--<asp:TextBox ID="txtCustGrp" runat="server" CssClass="cls_textbox" MaxLength="100"></asp:TextBox>--%>
                                                                    <asp:DropDownList ID="ddlCustGrp" runat="server" CssClass="cls_dropdownlist" Width="150px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="cls_label_header">Address</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="cls_textbox" MaxLength="500"></asp:TextBox>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">Customer Class</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlCustClass" runat="server" CssClass="cls_dropdownlist" AutoPostBack="false" Width="120px" >
                                                                        <asp:ListItem Value="">All</asp:ListItem>
                                                                        <asp:ListItem Value="A">A</asp:ListItem>
                                                                        <asp:ListItem Value="B">B</asp:ListItem>
                                                                        <asp:ListItem Value="C">C</asp:ListItem>
                                                                        <asp:ListItem Value="OTH">Other</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="cls_label_header">District</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDistrict" runat="server" CssClass="cls_textbox" MaxLength="190"></asp:TextBox>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">Customer Type</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlCustType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="false" Width="120px" >
                                                                        <asp:ListItem Value="">All</asp:ListItem>
                                                                        <asp:ListItem Value="0">Buying</asp:ListItem>
                                                                        <asp:ListItem Value="1">Non Buying</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="cls_label_header">Team</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlMapTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" Width="250px" >
                                                                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">MTD Sales Range</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <customToolkit:wuc_txtIntFieldRange 
                                                                            ID="MtdSalesRange" 
                                                                            runat="server"  
                                                                            RequiredValidation="true"
                                                                            RequiredValidationGroup="Search" 
                                                                         />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="cls_label_header">Salesrep</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlMapSalesrep" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" Width="250px" >
                                                                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">YTD Sales Range</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <customToolkit:wuc_txtIntFieldRange 
                                                                            ID="YtdSalesRange" 
                                                                            runat="server"  
                                                                            RequiredValidation="true"
                                                                            RequiredValidationGroup="Search" 
                                                                         />
                                                                </td>
                                                            </tr>
                                                            <asp:Panel ID="pnlSku" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                            <tr>
                                                                <td><span class="cls_label_header">SKU</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSKU" runat="server" CssClass="cls_dropdownlist" AutoPostBack="false" Width="250px" >
                                                                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">No of SKU</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                    <customToolkit:wuc_txtIntFieldRange 
                                                                            ID="NoSkuRange" 
                                                                            runat="server"  
                                                                            RequiredValidation="true"
                                                                            RequiredValidationGroup="Search" 
                                                                         />
                                                                </td>
                                                            </tr>
                                                             </asp:Panel>
                                                            <asp:Panel ID="pnlMSSEnquiry" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                            <tr>
                                                                <td><span class="cls_label_header">MSS Title</span></td>
                                                                <td><span class="cls_label_header">: </span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlMssTitle" CssClass="cls_dropdownlist" Width="250px" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">MSS Txn Date Range</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                     <customToolkit:wuc_txtCalendarRange 
                                                                            ID="txnMssTitleDate" 
                                                                            runat="server"  
                                                                            RequiredValidation="true"
                                                                            RequiredValidationGroup="Search" 
                                                                            DateFormatString="yyyy-MM-dd" 
                                                                            CompareDateRangeValidation="true"
                                                                       />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="cls_label_header">MSS Question</span></td>
                                                                <td><span class="cls_label_header">: </span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlMSSQuestion" CssClass="cls_dropdownlist" Width="250px" runat="server" AutoPostBack="true">
                                                                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td></td>
                                                                <td></td>
                                                                <td>                                                                     
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><span class="cls_label_header">MSS Sub Question</span></td>
                                                                <td><span class="cls_label_header">: </span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlMSSSubQuestion" CssClass="cls_dropdownlist" Width="250px" runat="server" AutoPostBack="true">
                                                                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <asp:Panel ID="pnlAnswerOn" runat="server" Visible="false">
                                                                    <td><span class="cls_label_header">Answer</span></td>
                                                                    <td><span class="cls_label_header">: </span></td>
                                                                    <td>
                                                                        <asp:Panel ID="pnlMssFreetext" runat="server" Visible="false">
                                                                            <asp:TextBox ID="txtMssTextAnswer" runat="server" CssClass="cls_textbox" Width="250px" MaxLength="100"></asp:TextBox>
                                                                        </asp:Panel>
                                                                        <asp:Panel ID="pnlMssRadio" runat="server" Visible="false">
                                                                            <asp:RadioButtonList ID="radMssRadioAnswer" runat="server" CssClass="cls_radiobuttonlist">
                                                                                <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                                                <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </asp:Panel>
                                                                        <asp:Panel ID="pnlMssDropdownlist" runat="server" Visible="false">
                                                                            <asp:DropDownList ID="ddlMssDDLAnswer" runat="server" CssClass="cls_dropdownlist" Width="250px">
                                                                                <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlAnswerOff" runat="server" Visible="true">
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td>
                                                                    </td>
                                                                </asp:Panel>
                                                            </tr>
                                                            </asp:Panel>
                                                            <asp:Panel ID="pnlSFMSEnquiry" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                                            <tr>
                                                                <td><span class="cls_label_header">Field Activity Category</span></td>
                                                                <td><span class="cls_label_header">: </span></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSFMSCategory" CssClass="cls_dropdownlist" Width="250px" runat="server" AutoPostBack="true">
                                                                        <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td><span class="cls_label_header">Txn Date Range</span></td>
                                                                <td><span class="cls_label_header">:</span></td>
                                                                <td>
                                                                     <customToolkit:wuc_txtCalendarRange 
                                                                            ID="txnSFMSTitleDate" 
                                                                            runat="server"  
                                                                            RequiredValidation="true"
                                                                            RequiredValidationGroup="Search" 
                                                                            DateFormatString="yyyy-MM-dd" 
                                                                            CompareDateRangeValidation="true"
                                                                       />
                                                                </td>
                                                                
                                                                
                                                            </tr>
                                                            <tr>    
                                                                <td><span class="cls_label_header">Field Activity Sub Category</span></td>
                                                                <td><span class="cls_label_header">: </span></td>
                                                                <td><asp:DropDownList ID="ddlSFMSSubCategory" CssClass="cls_dropdownlist" Width="250px" runat="server">
                                                                    <asp:ListItem Text="-- SELECT --" Value=""></asp:ListItem>
                                                                    </asp:DropDownList></td>
                                                                
                                                                <td style="width:20px"></td>
                                                                
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>    
                                                            </asp:Panel>
                                                            </table>                                                          
                                                        </td>
                                                    </tr>
                                                    </asp:Panel>  
                                                    
                                                        
                                                                            
                                                    <!-- END ADD HERE -->
                                                             
                                                    <tr><td style="height: 10px;" colspan="3"></td></tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="cls_button" ValidationGroup="Search"></asp:Button>
                                                            <asp:Button ID="btnGPS" runat="server" Text="View Map" CssClass="cls_button" ValidationGroup="Search" Visible="False" style="margin-left:10px"></asp:Button>
                                                            <asp:Button ID="btnKML" runat="server" Text="Download KML" CssClass="cls_button" ValidationGroup="Search" Visible="False" style="margin-left:10px"></asp:Button>
                                                          
                                                            <asp:Label ID="lblInfo" runat="server" CssClass="cls_validator"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                
                                <asp:Panel ID="pnlBack" runat="server" Visible="false" >
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0" style="height:30px; padding:2px 2px 2px 10px " >
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                
                                <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlEnquiry" runat="server"
                                    CollapseControlID="imgEnquiry"
                                    ExpandControlID="imgEnquiry" 
                                    TargetControlID="pnlEnquiry" 
                                    CollapsedSize="0"
                                    Collapsed="false" 
                                    ExpandDirection="Vertical" 
                                    SuppressPostBack="true">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                            <Triggers >
                            <asp:PostBackTrigger ControlId="btnKML"/>
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            
        </asp:Panel>
        
        
    <%--</ContentTemplate>
</asp:UpdatePanel>--%>

<table cellspacing="0" cellpadding="0" width="100%" border="0" class="cls_panel_header">
    <asp:Panel ID="pnlExport" runat="server" Visible="false">
        <tr>
            <td>
                <table class="cls_panel" id="Table1" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlExport" CssClass="cls_dropdownlist" runat="server">
                                            <asp:ListItem Value="0" Selected="True">Excel</asp:ListItem>
                                            <asp:ListItem Value="1">Text</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td>
                                        <asp:Button ID="btnExport" CssClass="cls_button" runat="server" Text="Export"></asp:Button></td>
                                    <td align="right" width="90%">
                                        <asp:ImageButton ID="imgClose_Export" runat="server" ImageUrl="~/images/ico_close.gif">
                                        </asp:ImageButton></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </asp:Panel>
</table>