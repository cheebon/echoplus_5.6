<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesOrderPrdMatrix.aspx.vb" Inherits="iFFMR_Customize_SalesOrderPrdMatrixMain" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sales Order with Product Matrix</title>
</head>
<body style="margin: 0; border: 0; padding: 0; background-color: #DDDDDD">
    <span id="TopBar" style="display: inline; overflow: hidden; margin: 0; border: 0;
        padding: 0;">
        <iframe id="TopBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMR/Customize/SalesOrderPrdMatrix/SalesOrderPrdMatrixSearch.aspx"
            width="100%" height="250" scrolling="no" style="border: 0; position: relative;
            top: 0px;"></iframe>
    </span><span id="ContentBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="ContentBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" height="0" scrolling="no" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span><%--<span id="DetailBar" style="display: inline; overflow: hidden; margin: 0;
        border: 0; padding: 0;">
        <iframe id="DetailBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx"
            width="100%" scrolling="no" height="0" style="border: 0; position: relative; top: 0px;">
        </iframe>
    </span>--%>
</body>

<%--<frameset rows="37%,0,*" frameborder="no" border="0" framespacing="0" id="fraDefaultMainHoriz">
        <frame src="../../iFFMR/Customize/SalesOrderPrdMatrix/SalesOrderPrdMatrixSearch.aspx" name="TopBar" >
        <frame name="fraSpliterHoriz"  src="../../iFFMR/Customize/SalesOrderPrdMatrix/SpliterHoriz.aspx" scrolling="no">
        <frame src="../../iFFMR/Customize/SalesOrderPrdMatrix/NoRecordFoundPage.aspx" name="ContentBar" >
</frameset>--%>
</html>
