﻿Imports System.Data
Imports System.Collections.Generic
Imports adm_Config

Partial Class Admin_Device_Config_Config
    Inherits System.Web.UI.Page
    Private dtAR As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            With wuc_lblheader
                .Title = "Configuration"
                .DataBind()
                .Visible = True
            End With

            If Not Page.IsPostBack Then
                GetCategoryList()

                If Not GetAccessRight(1, SubModuleType.CFGDEVICE, "'1'") Then
                    btnSave.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "dgCategory"
    Private Sub GetCategoryList()
        Dim dt As DataTable
        Dim objCat As clsConfig
        Try
            objCat = New clsConfig
            dt = objCat.GetCategoryList(Portal.UserSession.UserID)

            With dgCategoryList
                .DataSource = dt
                .DataBind()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateConfig_Update()
        End Try
    End Sub
    Protected Sub dgCategoryList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgCategoryList.RowCommand
        Dim strType As String
        Try
            Select Case e.CommandName
                Case "Select"
                    strType = dgCategoryList.DataKeys(Convert.ToInt32(e.CommandArgument)).Value
                    hfCurrentCat.Value = strType 'store currently selected category

                    RenewDataBind(strType)
                    OnOffGrid(True)



                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ValidateCheckbox", "ValidateAllChecked();", True)
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "DATA BIND"
    Public Sub RenewDataBind(ByVal strType As String)
        RefreshDataBind(strType)
    End Sub
    Public Sub RefreshDataBind(ByVal strType As String)
        RefreshDatabinding(strType)
    End Sub
    Private Function GetRecList(ByVal strType As String) As DataTable
        Dim dt As DataTable
        Dim objConfig As clsConfig
        Try
            objConfig = New clsConfig
            dt = objConfig.GetConfiguration(strType, Portal.UserSession.UserID)

            Return dt
            'With dgList
            '    .DataSource = dt
            '    .DataBind()
            'End With

            'OnOffGrid(True)
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region
#Region "DGLIST"
    Public Sub RefreshDatabinding(ByVal strType As String)
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try

            dtCurrentTable = GetRecList(strType)


            Dim dvCurrentView As New DataView(dtCurrentTable)


            With dgList
                .DataSource = dvCurrentView
                .DataBind()
            End With



        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateConfig_Update()
        End Try
    End Sub
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header
                    Dim chkbox As CheckBox = CType(e.Row.FindControl("chkbValueAll"), CheckBox)
                    chkbox.Attributes("onclick") = "ToggleAllCheckbox(this);"
                    chkbox.Enabled = CanEdit()
                Case DataControlRowType.DataRow
                    Dim chkbox As CheckBox = CType(e.Row.FindControl("chkbValue"), CheckBox)
                    chkbox.Attributes("onclick") = "ValidateAllChecked();"
                    chkbox.Enabled = CanEdit()

                    Dim txtValue2 As TextBox = CType(e.Row.FindControl("txtValue2"), TextBox)
                    Dim txtValue3 As TextBox = CType(e.Row.FindControl("txtValue3"), TextBox)

                    txtValue2.Enabled = CanEdit()
                    txtValue3.Enabled = CanEdit()


            End Select
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    Dim hlTeam As HyperLink = CType(e.Row.FindControl("hlTeamCount"), HyperLink)

                    Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)

                    If drv("TEAM_COUNT") > 0 Then
                        hlTeam.Attributes("onclick") = "OpenPopup('" & drv("SETTING_CODE") & "','" & clsCommon.DtlTypeTeam & "');"
                    Else
                        hlTeam.Attributes("onclick") = "redirect('','','" & hfCurrentCat.Value & "');"
                    End If

                    Dim hlSR As HyperLink = CType(e.Row.FindControl("hlSRCount"), HyperLink)

                    Dim drvSR As DataRowView = CType(e.Row.DataItem, DataRowView)

                    If drvSR("SR_COUNT") > 0 Then
                        hlSR.Attributes("onclick") = "OpenPopup('" & drv("SETTING_CODE") & "','" & clsCommon.DtlTypeSR & "');"
                    Else
                        hlSR.Attributes("onclick") = "redirect('','','" & hfCurrentCat.Value & "');"
                    End If

            End Select
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Function"
    Private Sub UpdateConfig_Update()
        updPnlConfig.Update()
    End Sub
    Private Function CanEdit() As Boolean
        Return GetAccessRight(1, SubModuleType.CFGDEVICE, "'3'")
    End Function
    Private Sub OnOffGrid(ByVal blnFlag As Boolean)
        dgList.Visible = blnFlag
        lblMesg.Visible = Not blnFlag

        If GetAccessRight(1, SubModuleType.CFGDEVICE, "'3'") Then
            btnSave.Visible = blnFlag
        Else
            btnSave.Visible = False
        End If

    End Sub
    Private Sub RunJob()
        Dim objConfig As New clsConfig
        Try
            objConfig.RunJob(Portal.UserSession.UserID)
        Catch ex As Exception
            lblMesg.Text = "Records saved successfully but invoke job failed."
        End Try
    End Sub
    Private Sub SaveSetting()
        Dim chkValue As CheckBox
        Dim txtValue2 As TextBox
        Dim txtValue3 As TextBox
        Dim txtValue4 As TextBox
        Dim strSettingCode As String
        Dim objConfig As New clsConfig
        Try
            For Each row As GridViewRow In dgList.Rows
                chkValue = CType(row.FindControl("chkbValue"), CheckBox)
                txtValue2 = CType(row.FindControl("txtValue2"), TextBox)
                txtValue3 = CType(row.FindControl("txtValue3"), TextBox)
                txtValue4 = CType(row.FindControl("txtValue4"), TextBox)
                strSettingCode = dgList.DataKeys(row.RowIndex).Value

                If txtValue2.Text.Trim = "" Then txtValue2.Text = "0"
                If txtValue3.Text.Trim = "" Then txtValue3.Text = "0"
                If txtValue4.Text.Trim = "" Then txtValue4.Text = "0"

                objConfig.SaveSetting(strSettingCode, Convert.ToInt32(chkValue.Checked), txtValue2.Text.Trim, txtValue3.Text.Trim, txtValue4.Text.Trim, Portal.UserSession.UserID)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function
#End Region
#Region "Event"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveSetting()
            OnOffGrid(False)
            lblMesg.Text = "Records saved!"

            RunJob()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateConfig_Update()
        End Try
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    
End Class
