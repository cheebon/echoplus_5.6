﻿Imports System.Data

Partial Class iFFMR_Customize_CallByMonthAdv_CallByMonthAdv
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_CallAnalysisListByMonthAdv")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_CallAnalysisListByMonthAdv") = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CallAnalysisListByMonthAdv"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "CallAnalysisListByMonthAdv"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        ' Try
        If Not IsPostBack Then

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CALLBYMONTHADV) '"Call Analysis List By Month"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CALLBYMONTHADV
                .DataBind()
                .Visible = True
            End With

            LoadTypeDDL()

            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    Session("TREE_PATH") = .Tree_Path
                    If .ReportType < ddlReportType.Items.Count Then ddlReportType.SelectedIndex = .ReportType
                    If Not IsNothing(.Type) Then ddlType.SelectedValue = .Type
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                CriteriaCollector = Nothing
                ChangeReportType()
                'Dim intMonth As Integer = CInt(Session.Item("Month"))
                'If intMonth > 6 Then ddlMonthType.SelectedIndex = 1
            End If
            licHeaderCollector = Nothing
            TimerControl1.Enabled = True
        End If
        lblErr.Text = ""

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try
    End Sub


#Region "EVENT HANDLER"


    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        '    Try
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        ' Try
        ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ChangeReportType()
        ' Try
        Dim intMonth As Integer = CInt(Session.Item("Month"))
        If intMonth <= 6 Then
            ddlReportType.SelectedIndex = 0
        ElseIf intMonth > 6 Then
            ddlReportType.SelectedIndex = 1
        Else
            ddlReportType.SelectedIndex = 2
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        '  Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        ' Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        '  Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'wuc_ctrlpanel.ExportToFile(dgList, "CallAnalysisByMonth_" & ddlReportType.SelectedItem.Text)
        'wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))
        wuc_ctrlpanel.ExportToFile(dgList, PageName)


        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ' ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        'Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                'dgList.ShowFooter = False
            End If
        End If


        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        'End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        'Try
        If dgList.Rows.Count < 15 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()
        aryDataItem.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
            Select Case CF_CallByMonthAdv.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CallByMonthAdv.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_CallByMonthAdv.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallByMonthAdv.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallByMonthAdv.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    Dim intmonth As Integer = CF_CallByMonthAdv.GetMonthValue(ColumnName)

                    'HL-----------------------------
                    'Dim strUrlFields(2) As String
                    'strUrlFields(0) = "TEAM_CODE"
                    'strUrlFields(1) = "REGION_CODE"
                    'strUrlFields(2) = "SALESREP_CODE"
                    'Dim strUrlFormatString As String = "CallAnalysisListByDay.aspx?MONTH=" & intmonth & "&TEAM_CODE={0}&REGION_CODE={1}&SALESREP_CODE={2}"

                    Dim strUrlFields(0) As String
                    strUrlFields(0) = "SALESREP_CODE"
                    Dim strUrlFormatString As String = "CallByDayAdv.aspx?MONTH=" & intmonth & "&SALESREP_CODE={0}&TYPE=" & ddlType.SelectedValue


                    'Dim strUrlFormatString As String
                    'Dim strUrlFields() As String = Nothing
                    'strUrlFormatString = FormUrlFormatString(intmonth, dtToBind, strUrlFields)
                    '-------------------------------

                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_CallByMonthAdv.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_SalesList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallByMonthAdv.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallByMonthAdv.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        aryDataItem = _aryDataItem
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        ' Try
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '  Try
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow
                'Dim iIndex As Integer

                'For Each li As ListItem In licItemFigureCollector
                '    iIndex = aryDataItem.IndexOf(li.Text)
                '    If iIndex > 0 Then
                '        li.Value = SUM(li.Value, DataBinder.Eval(e.Row.DataItem, li.Text))
                '    End If
                'Next

            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_CallByMonthAdv.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next


        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        '    Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        'Try
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim strYear, strMonth, strUserID, strPrincipalID, strPrincipalCode, strSalesRepList As String
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        strUserID = Session.Item("UserID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strPrincipalID = Session("PRINCIPAL_ID")
        strSalesRepList = Session("SALESREP_LIST")


        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .ReportType = ddlReportType.SelectedIndex
            .Type = ddlType.SelectedValue
            .Year = strYear
            .Month = strMonth
            .PrincipalCode = strPrincipalCode
            .PrincipalID = strPrincipalID
            .Tree_Path = Session("TREE_PATH")
        End With


        Dim clsCallByMonthAndDayAdv As New rpt_Customize.clsCallByMonthAndDayAdv
        DT = clsCallByMonthAndDayAdv.GetCallByMonthAdv(strUserID, strPrincipalID, strPrincipalCode, strYear, strSalesRepList, ddlReportType.SelectedValue, ddlType.SelectedValue)
        'dgList_Init(DT)
        PreRenderMode(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        '    Try
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)


        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        ' Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        'Cut the string with char '_CALLS' and '_WD'
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper.Replace("CALL", "").Replace("WD", "")
            strColumnName = strColumnName.Replace("_", "")
            strColumnName = strColumnName.Replace("SALESREP_NAME", "&nbsp;")
            strColumnName = strColumnName.Replace("Q", "Quarter ") '.Replace("Q1", "Quarter 1").Replace("Q1", "Quarter 1").Replace("Q1", "Quarter 1")
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                'liColumnField.Text = strColumnName
                'liColumnField.Value = 1
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "*_CALL" OrElse _
             strColumnName Like "*_WD" OrElse _
             strColumnName Like "Q*CALL" OrElse _
             strColumnName Like "Q*WD" Then

                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        'Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        CriteriaCollector.SortExpression = strSortExpression
        SortingExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub

#End Region

    Protected Sub LoadTypeDDL()
        Dim DT As DataTable
        Dim clsCallByMonthAndDayAdv As New rpt_Customize.clsCallByMonthAndDayAdv
        Dim struserid As String
        struserid = Trim(Session("UserID"))

        DT = clsCallByMonthAndDayAdv.GetCallByDayMonthAdvType(struserid)
        With ddlType
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "TYPE"
            .DataValueField = "TYPE"
            .DataBind()
            '.Items.Insert(0, New ListItem("-- SELECT --", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        'Try
        SortingExpression = Nothing
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        SortingExpression = Nothing
        RenewDataBind()
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '   Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub

 
End Class


Public Class CF_CallByMonthAdv
    'Jan_Calls,Jan_WD,Feb_Calls,Feb_WD,Mar_Calls,Mar_WD,Apr_Calls,Apr_WD,May_Calls,May_WD,June_Calls,June_WD,
    'July_Calls,July_WD,Aug_Calls,Aug_WD,Sept_Calls,Sept_WD,Oct_Calls,Oct_WD,Nov_Calls,Nov_WD,Dec_Calls,Dec_WD,
    'YTD_Calls,YTD_WD

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strTblName As String = ""
        ColumnName = ColumnName.ToUpper

        If ColumnName Like "*_CALL" OrElse ColumnName Like "Q*CALL" Then
            strTblName = "AC"
        ElseIf ColumnName Like "*_WD" OrElse ColumnName Like "Q*WD" Then
            strTblName = "TC"
        Else
            strTblName = Report.GetDisplayColumnName(ColumnName)
        End If

        Return strTblName

    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '  Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        If (strColumnName = "REGION_CODE") OrElse (strColumnName = "TEAM_CODE") Then
            FCT = FieldColumntype.InvisibleColumn
        ElseIf (strColumnName Like "*_CALL" OrElse strColumnName Like "*_WD") And Not (strColumnName Like "YTD_*") _
             And Not (strColumnName Like "Q*_CALL" OrElse strColumnName Like "Q*_WD") Then

            If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CABYDAY, "'1','8'") Then
                FCT = FieldColumntype.HyperlinkColumn
            End If
        End If

        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetMonthValue(ByVal ColumnName As String) As Integer
        ' Try
        Dim strColumnName As String = ColumnName.ToUpper
        strColumnName = strColumnName.Replace("_CALL", "")
        strColumnName = strColumnName.Replace("_WD", "")
        Select Case strColumnName
            Case "JAN"
                Return 1
            Case "FEB"
                Return 2
            Case "MAR"
                Return 3
            Case "APR"
                Return 4
            Case "MAY"
                Return 5
            Case "JUNE"
                Return 6
            Case "JULY"
                Return 7
            Case "AUG"
                Return 8
            Case "SEPT"
                Return 9
            Case "OCT"
                Return 10
            Case "NOV"
                Return 11
            Case "DEC"
                Return 12
            Case Else
                Return 1
        End Select
        'Catch ex As Exception
        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        '  Try
        If strColumnName Like "*_WD" OrElse strColumnName Like "*_CALL" Then
            strStringFormat = "{0:0.##}"
        Else
            strStringFormat = ""
        End If
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        '    Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function
End Class