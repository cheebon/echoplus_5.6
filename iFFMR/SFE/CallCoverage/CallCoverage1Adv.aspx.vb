Imports System.Data

Partial Class CallCoverage1Adv
    Inherits System.Web.UI.Page

#Region "Local Variable"
    'Protected Property IsViewByFigure() As Boolean
    '    Get
    '        Return CBool(ViewState("VIEWBYFIGURE"))
    '    End Get
    '    Set(ByVal value As Boolean)
    '        ViewState("VIEWBYFIGURE") = value
    '    End Set
    'End Property

    Dim _viewMode As ViewMode

    Protected Property SelectedViewMode() As ViewMode
        Get
            If ViewState("VIEWMODE") IsNot Nothing Then _viewMode = ViewState("VIEWMODE")
            Return _viewMode
        End Get
        Set(ByVal value As ViewMode)
            _viewMode = value
            ViewState("VIEWMODE") = value
        End Set
    End Property

    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_CallCoverage1")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_CallCoverage1") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CallCoverage1"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property


    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If Not IsPostBack Then
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CALLCOV1ADV) '"Call Coverage 1"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CALLCOV1ADV
                .DataBind()
                .Visible = True
            End With

            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    If .ReportType < ddlReportType.Items.Count Then ddlReportType.SelectedIndex = .ReportType
                    SortingExpression = .SortExpression
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                CriteriaCollector = Nothing ' New clsSharedValue
            End If
            TimerControl1.Enabled = True
        End If
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "CallCoverage1Adv"
        End Get
    End Property

#Region "Event Handler"

    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        '  Try
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        '   Try
        'ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    'Private Sub ChangeReportType()
    '    Try
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        ' Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        '   Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        ' Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        'Try
        'If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        '  End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        '     Try
        If dgList.Rows.Count < 18 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "Custom DGList"

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_CALLCOV1ADV.GetFieldColumnType(ColumnName, SelectedViewMode)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CALLCOV1ADV.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_CALLCOV1ADV.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CALLCOV1ADV.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CALLCOV1ADV.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields() As String = Nothing

                    strUrlFormatString = FormUrlFormatString(ColumnName, dtToBind, strUrlFields)
                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing
                    aryDataItem.Add(ColumnName)
                Case FieldColumntype.InvisibleColumn
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CALLCOV1ADV.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataFormatString = CF_CALLCOV1ADV.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CALLCOV1ADV.ColumnStyle(ColumnName).HorizontalAlign


                    dgColumn.HeaderText = CF_CALLCOV1ADV.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub


    Private Function FormUrlFormatString(ByVal strColumnName As String, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty

        Dim strSalesrepCode As String
        Dim strClass As String = String.Empty

        ReDim strUrlFields(0)
        strUrlFields(0) = IIf(dtToBind.Columns.Item("SALESREP_CODE") IsNot Nothing, "SALESREP_CODE", "")

        strSalesrepCode = Session("SALESREP_CODE")

        If strColumnName Like "CUST_*" Then 'Click Customer
            strColumnName = strColumnName.Replace("CUST_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/COMMON/CommonCustContInfo.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "REACH_*" Then 'Click Call
            strColumnName = strColumnName.Replace("REACH_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/CommonCustContCallInfo.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "COVERAGE_*" Then 'Click Coverage
            strColumnName = strColumnName.Replace("COVERAGE_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/CommonCustContCoverageInfo.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "NONCOVERAGE_*" Then 'Click Non Coverage
            strColumnName = strColumnName.Replace("NONCOVERAGE_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/CommonCustContNonCoverageInfo.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "EFFECT_*" Then 'Click Effective
            strColumnName = strColumnName.Replace("EFFECT_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/CommonCustContEffectiveCallInfo.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "NONEFFECT_*" Then 'Click Non Effective
            strColumnName = strColumnName.Replace("NONEFFECT_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/CommonCustContNonEffectiveCallInfo.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "ACTIVE_*" Then 'Click Active
            strColumnName = strColumnName.Replace("ACTIVE_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/ActiveCustomer.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        ElseIf strColumnName Like "NONACTIVE_*" Then
            strColumnName = strColumnName.Replace("NONACTIVE_", "")
            Select Case strColumnName
                Case "A", "B", "C", "OTH"
                    strClass = strColumnName
                Case Else
                    strClass = String.Empty
            End Select
            strUrlFormatString = "~/iFFMR/SFE/CallCoverage/NonActiveCustomer.aspx?SALESREP_CODE={0}" & IIf(String.IsNullOrEmpty(strUrlFields(0)), strSalesrepCode, "") & "&CLASS=" & strClass & "&PAGE_IND=CONV1ADV"
        End If
        Return strUrlFormatString
    End Function

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
    End Sub
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next

                'SALESREP_PERCENTAGE
                Dim liWD, liCD As ListItem
                liWD = licItemFigureCollector.FindByText("FTE_WD")

                If licItemFigureCollector.FindByText("FTE_CD") Is Nothing Then
                    liCD = licItemFigureCollector.FindByText("FTE_CD_HALF")
                Else
                    liCD = licItemFigureCollector.FindByText("FTE_CD")
                End If

                If liWD IsNot Nothing AndAlso liCD IsNot Nothing Then
                    e.Row.Cells(aryDataItem.IndexOf("FTE_ACH")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("FTE_ACH"), (DIVISION(liCD, liWD) * 100))
                End If

                'For All "ACH_"
                If SelectedViewMode = ViewMode.ViewByPercentages Then
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_A")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_COVERAGE_A"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_A"), licItemFigureCollector.FindByText("CUST_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_B")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_COVERAGE_B"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_B"), licItemFigureCollector.FindByText("CUST_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_C")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_COVERAGE_C"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_C"), licItemFigureCollector.FindByText("CUST_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_OTH")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_COVERAGE_OTH"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_OTH"), licItemFigureCollector.FindByText("CUST_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_COVERAGE_TTL")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_COVERAGE_TTL"), (DIVISION(licItemFigureCollector.FindByText("COVERAGE_TTL"), licItemFigureCollector.FindByText("CUST_TTL")) * 100))

                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONCOVERAGE_A")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONCOVERAGE_A"), (DIVISION(licItemFigureCollector.FindByText("NONCOVERAGE_A"), licItemFigureCollector.FindByText("CUST_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONCOVERAGE_B")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONCOVERAGE_B"), (DIVISION(licItemFigureCollector.FindByText("NONCOVERAGE_B"), licItemFigureCollector.FindByText("CUST_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONCOVERAGE_C")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONCOVERAGE_C"), (DIVISION(licItemFigureCollector.FindByText("NONCOVERAGE_C"), licItemFigureCollector.FindByText("CUST_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONCOVERAGE_OTH")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONCOVERAGE_OTH"), (DIVISION(licItemFigureCollector.FindByText("NONCOVERAGE_OTH"), licItemFigureCollector.FindByText("CUST_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONCOVERAGE_TTL")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONCOVERAGEE_TTL"), (DIVISION(licItemFigureCollector.FindByText("NONCOVERAGE_TTL"), licItemFigureCollector.FindByText("CUST_TTL")) * 100))

                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_A")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_CALL_A"), (DIVISION(licItemFigureCollector.FindByText("REACH_A"), licItemFigureCollector.FindByText("COVERAGE_A"))))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_B")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_CALL_B"), (DIVISION(licItemFigureCollector.FindByText("REACH_B"), licItemFigureCollector.FindByText("COVERAGE_B"))))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_C")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_CALL_C"), (DIVISION(licItemFigureCollector.FindByText("REACH_C"), licItemFigureCollector.FindByText("COVERAGE_C"))))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_OTH")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_CALL_OTH"), (DIVISION(licItemFigureCollector.FindByText("REACH_OTH"), licItemFigureCollector.FindByText("COVERAGE_OTH"))))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_CALL_TTL")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_CALL_TTL"), (DIVISION(licItemFigureCollector.FindByText("REACH_TTL"), licItemFigureCollector.FindByText("COVERAGE_TTL"))))

                    e.Row.Cells(aryDataItem.IndexOf("ACH_EFFECTIVE_A")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_EFFECTIVE_A"), (DIVISION(licItemFigureCollector.FindByText("EFFECT_A"), licItemFigureCollector.FindByText("REACH_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_EFFECTIVE_B")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_EFFECTIVE_B"), (DIVISION(licItemFigureCollector.FindByText("EFFECT_B"), licItemFigureCollector.FindByText("REACH_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_EFFECTIVE_C")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_EFFECTIVE_C"), (DIVISION(licItemFigureCollector.FindByText("EFFECT_C"), licItemFigureCollector.FindByText("REACH_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_EFFECTIVE_OTH")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_EFFECTIVE_OTH"), (DIVISION(licItemFigureCollector.FindByText("EFFECT_OTH"), licItemFigureCollector.FindByText("REACH_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_EFFECTIVE_TTL")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_EFFECTIVE_TTL"), (DIVISION(licItemFigureCollector.FindByText("EFFECT_TTL"), licItemFigureCollector.FindByText("REACH_TTL")) * 100))

                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONEFFECTIVE_A")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONEFFECTIVE_A"), (DIVISION(licItemFigureCollector.FindByText("NONEFFECT_A"), licItemFigureCollector.FindByText("REACH_A")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONEFFECTIVE_B")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONEFFECTIVE_B"), (DIVISION(licItemFigureCollector.FindByText("NONEFFECT_B"), licItemFigureCollector.FindByText("REACH_B")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONEFFECTIVE_C")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONEFFECTIVE_C"), (DIVISION(licItemFigureCollector.FindByText("NONEFFECT_C"), licItemFigureCollector.FindByText("REACH_C")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONEFFECTIVE_OTH")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONEFFECTIVE_OTH"), (DIVISION(licItemFigureCollector.FindByText("NONEFFECT_OTH"), licItemFigureCollector.FindByText("REACH_OTH")) * 100))
                    e.Row.Cells(aryDataItem.IndexOf("ACH_NONEFFECTIVE_TTL")).Text = String.Format(CF_CALLCOV1ADV.GetOutputFormatString("ACH_NONEFFECTIVE_TTL"), (DIVISION(licItemFigureCollector.FindByText("NONEFFECT_TTL"), licItemFigureCollector.FindByText("REACH_TTL")) * 100))
                End If
        End Select

    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double

        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2

    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        '    Try
        Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth As String
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")

        Dim strSalesRepList As String
        strSalesRepList = Session("SALESREP_LIST")

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .GroupField = ""
            .PrincipalID = strPrincipalID
            .PrincipalCode = strPrincipalCode
            .Year = strYear
            .Month = strMonth

            .ReportType = ddlReportType.SelectedIndex
        End With

        Dim clsCallDB As New rpt_CALL.clsCallQuery
        DT = clsCallDB.GetCallCoverage1Adv(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        PreRenderMode(DT)
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)

    End Sub

    Private Sub Cal_CustomerHeader()
        '      Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        'SALESREP_*, CUST_*, REACH_*, COVERAGE_*, EFFECT_*
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "SALESREP_*" OrElse strColumnName Like "TEAM_CODE" OrElse strColumnName Like "REGION_*" Then
            ElseIf strColumnName Like "FTE_*" Then
                strColumnName = "FTE"
            ElseIf strColumnName Like "CUST_*" Then
                strColumnName = "Customer"
            ElseIf strColumnName Like "REACH_*" Then
                strColumnName = "Call"
            ElseIf strColumnName Like "COVERAGE_*" Then
                strColumnName = "Coverage"
            ElseIf strColumnName Like "EFFECT_*" Then
                strColumnName = "Effective Call"
            ElseIf strColumnName Like "ACH_COVERAGE_*" Then
                strColumnName = "Coverage"
            ElseIf strColumnName Like "ACH_NONCOVERAGE_*" Then
                strColumnName = "Non Coverage"
            ElseIf strColumnName Like "ACH_CALL_*" Then
                strColumnName = "Call per Customer"
            ElseIf strColumnName Like "ACH_EFFECTIVE_*" Then
                strColumnName = "Effectiveness"
            ElseIf strColumnName Like "ACH_NONEFFECTIVE_*" Then
                strColumnName = "Non effectiveness"
            ElseIf strColumnName Like "NONCOVERAGE_*" Then
                strColumnName = "Non Coverage"
            ElseIf strColumnName Like "NONEFFECT_*" Then
                strColumnName = "Non Effective Call"
            ElseIf strColumnName Like "ACTIVE_*" Then
                strColumnName = "Active Customer"
            ElseIf strColumnName Like "NONACTIVE_*" Then
                strColumnName = "Non Active Customer"
            Else
                strColumnName = "UKNOWN"
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            If (strColumnName Like "*_WD" OrElse _
             strColumnName Like "*_CD*" OrElse _
             strColumnName Like "*_A" OrElse strColumnName Like "*_B" OrElse _
             strColumnName Like "*_C" OrElse strColumnName Like "*_OTH" OrElse _
             strColumnName Like "*_TTL") AndAlso Not strColumnName Like "ACH_*" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        '   Try
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()

    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub


    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SelectedViewMode = ddlReportType.SelectedIndex
        SortingExpression = Nothing
        RefreshDatabinding()
    End Sub

    Public Enum ViewMode
        ViewByFigure = 0
        ViewByPercentages = 1
    End Enum

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '      Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

End Class

Public Class CF_CALLCOV1ADV
    Public Shared Function FilterPrefixName(ByVal columnName As String) As String
        'SALESREP_CODE ,SALESREP_NAME , FTE_WD , FTE_CD , FTE_ACH , 
        'CUST_A , CUST_B , CUST_C , CUST_OTH , CUST_TTL , 
        'REACH_A , REACH_B , REACH_C , REACH_OTH , REACH_TTL , 
        'COVERAGE_A , COVERAGE_B , COVERAGE_C , COVERAGE_OTH , COVERAGE_TTL , 
        'EFFECT_A , EFFECT_B , EFFECT_C , EFFECT_OTH , EFFECT_TTL
        'ACH_COVERAGE_A , ACH_COVERAGE_B , ACH_COVERAGE_C , ACH_COVERAGE_OTH , ACH_COVERAGE_TTL , 
        'ACH_CALL_A , ACH_CALL_B , ACH_CALL_C , ACH_CALL_OTH , ACH_CALL_TTL , 
        'ACH_EFFECTIVE_A , ACH_EFFECTIVE_B , ACH_EFFECTIVE_C , ACH_EFFECTIVE_OTH ,ACH_EFFECTIVE_TTL

        Dim strName As String = columnName
        If Not String.IsNullOrEmpty(columnName) AndAlso Not columnName Like "SALESREP_CODE" Then
            strName = columnName.ToUpper.Replace("SALESREP_", "").Replace("FTE_", "").Replace("CUST_", "").Replace("REACH_", "").Replace("COVERAGE_", "").Replace("NON_COVERAGE_", "").Replace("EFFECT_", "").Replace("NON_EFFECT_", "").Replace("ACTIVE_", "").Replace("NONACTIVE_", "")
            strName = strName.ToUpper.Replace("ACH_", "").Replace("CALL_", "").Replace("EFFECTIVE_", "").Replace("NON", "")
        End If
        Return strName
    End Function

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strNewName As String = FilterPrefixName(ColumnName.ToUpper)
        Dim strSign As String = IIf(ColumnName.StartsWith("ACH_") AndAlso Not ColumnName Like ("ACH_CALL_*"), "%", "")

        Select Case strNewName
            Case "WD"
                strFieldName = "WD"
            Case "CD"
                strFieldName = "CD"
            Case "CD_HALF"
                strFieldName = "CD"
            Case "ACH"
                strFieldName = "%"
            Case "A"
                strFieldName = "A " & strSign
            Case "B"
                strFieldName = "B " & strSign
            Case "C"
                strFieldName = "C " & strSign
            Case "OTH"
                strFieldName = "OTH " & strSign
            Case "TTL"
                strFieldName = "Total"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function
    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, ByVal Mode As CallCoverage1Adv.ViewMode) As FieldColumntype
        '    Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper

        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        If Not (strColumnName Like "*_CODE" OrElse strColumnName Like "*_NAME") Then
            If strColumnName Like "FTE_*" Then
                FCT = FieldColumntype.BoundColumn
            Else
                If Mode = CallCoverage1Adv.ViewMode.ViewByPercentages Then
                    FCT = IIf(strColumnName Like "ACH_*", FieldColumntype.BoundColumn, FieldColumntype.InvisibleColumn)
                Else
                    If strColumnName Like "ACH_*" Then
                        FCT = FieldColumntype.InvisibleColumn
                    ElseIf strColumnName Like "REACH_*" Then
                        FCT = IIf(strColumnName Like "REACH_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTCALLINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    ElseIf strColumnName Like "COVERAGE_*" Then
                        FCT = IIf(strColumnName Like "COVERAGE_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTCOVERAGEINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    ElseIf strColumnName Like "NONCOVERAGE_*" Then
                        FCT = IIf(strColumnName Like "NONCOVERAGE_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTNONCOVERAGEINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    ElseIf strColumnName Like "EFFECT_*" Then
                        FCT = IIf(strColumnName Like "EFFECT_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTEFFECTIVECALLINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    ElseIf strColumnName Like "NONEFFECT_*" Then
                        FCT = IIf(strColumnName Like "NONEFFECT_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTNONEFFECTIVEINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    ElseIf strColumnName Like "ACTIVE_*" Then
                        FCT = IIf(strColumnName Like "ACTIVE_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTEFFECTIVECALLINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    ElseIf strColumnName Like "NONACTIVE_*" Then
                        FCT = IIf(strColumnName Like "NONACTIVE_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTEFFECTIVECALLINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    Else
                        FCT = IIf(strColumnName Like "CUST_*" AndAlso Report.GetAccessRight(ModuleID.FFMR, SubModuleType.CUSTCONTINFO, "'1','8'"), FieldColumntype.HyperlinkColumn, FieldColumntype.BoundColumn)
                    End If
                End If
            End If
        End If

        Return FCT

    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)

        Select Case strNewName
            Case "ACH", "CD_HALF"
                strStringFormat = "{0:0.0}"
            Case "A", "B", "C", "OTH", "TTL", "WD", "CD"
                strStringFormat = "{0:0.#}"
            Case Else
                strStringFormat = ""
        End Select

        If strColumnName Like "ACH*" Then strStringFormat = "{0:0.0}"

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        '  Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With
        Return CS
    End Function

End Class