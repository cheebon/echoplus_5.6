<%@ Control Language="VB" AutoEventWireup="false" EnableViewState="false" CodeFile="wuc_UpdateProgress.ascx.vb"
    Inherits="include_wuc_UpdateProgress" %>
<%@ Register Assembly="AjaxControls" Namespace="AjaxControls" TagPrefix="asp" %>
<asp:ModalUpdateProgress ID="UpdateProgress_General" runat="server" DisplayAfter="300" BackgroundCssClass="modalBackground">
    <ProgressTemplate>
        <fieldset id="loading-area" >
            <span style="width:125px; height:25px; font-size: small;font-weight: bold;font: italic small-caps bold small/normal Verdana;padding: 5px 10px 5px 10px;margin: 0;color: #800000;font-family: Tahoma, Verdana, Arial;text-align:center;vertical-align:baseline;">
                <asp:Image ID="imgIndicator" runat="server" ImageUrl="~/images/loading.gif" />
                Loading... &nbsp;&nbsp;
           </span>
        </fieldset>
    </ProgressTemplate>
</asp:ModalUpdateProgress>


<%--
<asp:UpdateProgress ID="UpdateProgress_General" runat="server" DisplayAfter="300">
    <ProgressTemplate>
    <fieldset id="loading-area">
        <div >
            <p>
                Loading...&nbsp;&nbsp;<asp:Image ID="imgIndicator" runat="server" ImageUrl="~/images/indicator.gif" /></p>
        </div>
     </fieldset>
    </ProgressTemplate>
</asp:UpdateProgress>
--%>
