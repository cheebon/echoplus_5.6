<%@ Page Language="vb" AutoEventWireup="false" Inherits="CallAnalysisListByContact" CodeFile="CallAnalysisListByContact.aspx.vb" Buffer="true" EnableEventValidation="false" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sales List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmSalesRepList" method="post" runat="server">
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" align="left">
                    <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional"  RenderMode="Inline" >
                        <ContentTemplate>
                            <asp:Panel ID="pnlTreeMenuControl" runat="server"  >
                                <customToolkit:wuc_Menu ID="wuc_Menu" runat="server" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left">
                                                                    <asp:Timer  id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                        AllowSorting="true" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                        GridHeight="435" RowSelectionEnabled="true">
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
<%--<asp:GridView ID="dgList" runat="server" Width="100%" ShowFooter="true" CssClass="cls_table"
                                            AllowSorting="true" AutoGenerateColumns="false"  >
                                            <AlternatingRowStyle CssClass="cls_table_alternating_item" />
                                            <RowStyle CssClass="cls_table_item" />
                                            <HeaderStyle CssClass="cls_table_header" ></HeaderStyle>
                                            <FooterStyle CssClass="cls_table_footer" HorizontalAlign="Right" Font-Bold="True"></FooterStyle>
                                            <PagerStyle VerticalAlign="Top" CssClass="cls_table_header" />
                                        </asp:GridView>
                                                                <%--<asp:DataGrid CssClass="cls_table" ID="dgList" runat="server" Width="100%" DataKeyField="ID"
                                                                    AutoGenerateColumns="False" BorderColor="SlateGray" AllowPaging="false" AllowSorting="True">
                                                                    <AlternatingItemStyle CssClass="cls_table_odd"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="cls_table_item"></ItemStyle>
                                                                    <HeaderStyle CssClass="cls_table_header"></HeaderStyle>
                                                                    <FooterStyle CssClass="cls_tableheader"></FooterStyle>
                                                                    <PagerStyle Position="Top" CssClass="cls_table_header"></PagerStyle>
                                                                    <Columns>
                                                                        <asp:TemplateColumn SortExpression="AgencyName" HeaderText="Agency Name">
                                                                            <HeaderStyle Width="22%" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"AgencyName")%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="MTDQty" SortExpression="MTDQty" HeaderText="Qty">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="MTDFOC" SortExpression="MTDFOC" HeaderText="FOC">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:TemplateColumn SortExpression="MTDSales" HeaderText="Sales">
                                                                            <HeaderStyle Width="22%" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="HyperLink1" NavigateUrl="SalesInfoByDate.aspx" runat="server"
                                                                                    CssClass="cls_link">
											<%# DataBinder.Eval(Container.DataItem,"MTDSales")%>
                                                                                </asp:HyperLink>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="QTDQty" SortExpression="QTDQty" HeaderText="Qty">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="QTDFOC" SortExpression="QTDFOC" HeaderText="FOC">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="QMTDSales" SortExpression="QTDSales" HeaderText="Sales">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="YTDQty" SortExpression="YTDQty" HeaderText="Qty">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="YTDFOC" SortExpression="YTDFOC" HeaderText="FOC">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="YTDSales" SortExpression="YTDSales" HeaderText="Sales">
                                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>--%>
