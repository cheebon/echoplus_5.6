Imports System.Data

Partial Class iFFMA_Mapping_FieldForceCustBTCList
    Inherits System.Web.UI.UserControl

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum TypeName As Long
        Customer = 0
        Contact = 1
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = 7 'CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10


    End Sub

#Region "PROPERTY"
    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(hdSalesrepCode.value)
        End Get
        Set(ByVal value As String)
            hdSalesrepCode.value = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(hdContCode.Value)
        End Get
        Set(ByVal value As String)
            hdContCode.Value = value
        End Set

    End Property

    Public Property Type() As Long
        Get
            Return Trim(hdType.Value)
        End Get
        Set(ByVal value As Long)
            hdType.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsFieldForceCust As New mst_Mapping.clsFieldForceCust

            If Type = TypeName.Customer Then
                DT = clsFieldForceCust.GetCustBTCList(SalesrepCode, CustCode)
                dgCustBTCList.DataKeyNames = New String() {"CALL_DAY"}
            ElseIf Type = TypeName.Contact Then
                DT = clsFieldForceCust.GetContBTCList(SalesrepCode, CustCode, ContCode)
                dgCustBTCList.DataKeyNames = New String() {"CALL_DAY"}
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgCustBTCList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    dgCustBTCList.PageIndex = 0
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)

            With dgCustBTCList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .DataBind()
            End With
            
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlFieldForceCustBTC.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgCustBTCList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgCustBTCList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgCustBTCList.Columns.Clear()

            'ADD BUTTON EDIT
            If Master_Row_Count > 0 AndAlso _
                Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDFORCECUST, SubModuleAction.Edit) Then 'HL: 20080429 (AR)
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgCustBTCList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_FieldForceCustBTCList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_FieldForceCustBTCList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_FieldForceCustBTCList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_FieldForceCustBTCList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_FieldForceCustBTCList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgCustBTCList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgCustBTCList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgCustBTCList.RowEditing
        Try
            Dim strCallDay As String = sender.datakeys(e.NewEditIndex).item("CALL_DAY")

            With (wuc_FieldForceCustBTCPop)
                .Type = Type
                .SalesrepCode = SalesrepCode
                .CustCode = CustCode
                .ContCode = ContCode
                .CallDay = strCallDay
                .LoadDvEditMode()
                .ResetPage()
                .BindDetailsView()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgCustBTCList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgCustBTCList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header

                Case DataControlRowType.DataRow
                    Dim TC As TableCell

                    For i As Integer = 0 To aryDataItem.Count - 1
                        If aryDataItem.Item(i).ToString Like "CALL_TIME_*" Then
                            TC = e.Row.Cells(i)
                            If TC.Text = "0" Then
                                TC.Text = "" '"<img src='../../images/ico_checkbox.gif'>"
                                Server.HtmlEncode(TC.Text)
                            Else
                                TC.Text = "<img src='../../images/ico_tick.gif'>"
                                Server.HtmlEncode(TC.Text)
                            End If
                        End If
                    Next

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Function GetdgCustBTCList() As GridView
        Dim dgCustBTCListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgCustBTCList.AllowSorting
            Dim blnAllowPaging As Boolean = dgCustBTCList.AllowPaging

            dgCustBTCList.AllowSorting = False
            dgCustBTCList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            dgCustBTCListToExport = dgCustBTCList

            dgCustBTCList.AllowPaging = blnAllowPaging
            dgCustBTCList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgCustBTCListToExport
    End Function
#End Region

#Region "EVENTS"
    Protected Sub btnCustBTCPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_FieldForceCustBTCPop.SaveButton_Click
        RenewDataBind()
    End Sub

#End Region

#Region "COMMON FUNCTION"

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_FieldForceCustBTCList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "CALL_DAY"
                strFieldName = "Call Day \ Call Time"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Dim strCallTime As String = ""
        If ColumnName Like "CALL_TIME_*" Then
            strCallTime = ColumnName.Replace("CALL_TIME_", "")

            If strCallTime = "AM" OrElse strCallTime = "PM" OrElse strCallTime = "W" Then
                strFieldName = strCallTime
            Else
                strFieldName = strCallTime + ":00"
            End If
        End If

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "*TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

