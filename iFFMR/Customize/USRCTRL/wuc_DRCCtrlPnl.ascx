<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_DRCCtrlPnl.ascx.vb" Inherits="iFFMR_Customize_USRCTRL_wuc_DRCCtrlPnl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx"  %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_lblDate.ascx" TagName="wuc_lblDate" TagPrefix="customToolkit" %>

<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td>
            <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                width="100%" border="0" style="height: 30px">
                <tr align="center" valign="bottom">                    
                    <td>
                        <asp:Image ID="imgEnquiryDetails" ImageUrl="~/images/ico_Field.gif" runat="server"
                            CssClass="cls_button" ToolTip="Search" EnableViewState="false" />
                    </td>
                    <%If imgEnquiryDetails.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image6" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgNetValue" runat="server" CssClass="cls_button" ToolTip="Net Value Selection"
                            ImageUrl="~/images/ico_money.gif" EnableViewState="false" />
                    </td>
                    <%If imgNetValue.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image9" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server"
                            CssClass="cls_button" ToolTip="Export" EnableViewState="false" />
                    </td>
                    <%If imgExport.Visible Then%>
                    <td style="padding-left: 5px; padding-right: 5px">
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/images/toolbarseparator.gif" EnableViewState="false" />
                    </td>
                    <%End If%>
                    <td>
                        <asp:Image ID="imgPrint" ImageUrl="~/images/ico_printer.gif" runat="server" CssClass="cls_button"
                            ToolTip="Print" onclick="window.print();" EnableViewState="false" />
                    </td>
                    <td style="width: 95%">
                    </td>
                </tr>                 
            
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="UpdateCtrlPanel" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr id="All Control Panel ">
                            <td>
                               
                                <asp:Panel ID="pnlNetValue" runat="server" Visible="False" CssClass="cls_ctrl_panel">
                                    <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="radlNetValue" runat="server" RepeatDirection="Horizontal"
                                                                CssClass="cls_radiobuttonlist">
                                                                <asp:ListItem Selected="True" Value="1">Net Value 1</asp:ListItem>
                                                                <asp:ListItem Value="2">Net Value 2</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnNetValue_refresh" CssClass="cls_button" runat="server" Text="Refresh"
                                                                EnableViewState="false" OnClick="btnNetValue_refresh_Click" /></td>
                                                        <td align="right" style="width: 70%;">
                                                            <asp:Image ID="imgNetValue_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_DRCCtrlPnl1_pnlNetValue')" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlNetValue" runat="server" CollapseControlID="imgNetValue"
                                        ExpandControlID="imgNetValue" TargetControlID="pnlNetValue" CollapsedSize="0"
                                        Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                                
                                <asp:Panel ID="pnlDRCSearch" runat="server" Visible="false" CssClass="cls_ctrl_panel">
                                    <table border="0" cellpadding="0" cellspacing="2" class="cls_panel" width="100%">
                                        <tr>
                                            <td><div class="cls_label_header">Field Force</div></td>
                                            <td><div class="cls_label_header">:</div></td>
                                            <td>
                                                <asp:DropDownList ID="ddlSalesRep" runat="server" Width="350px" CssClass="cls_dropdownlist" AutoPostBack="true">
                                                </asp:DropDownList></td>
                                            <td rowspan="4" valign="top" align="right"><asp:Image ID="imgDRCSearch_Close" runat="server" ImageUrl="~/images/ico_close.gif"
                                                                CssClass="cls_image_Close" onclick="CtrlPnl_ChangeCollapsibleState('wuc_DRCCtrlPnl1_pnlDRCSearch')" /></td>
                                        </tr>
                                        <tr valign="top">
                                            <td><div class="cls_label_header">Date</div></td>
                                            <td><div class="cls_label_header">:</div></td>
                                            <td><uc1:wuc_txtCalendarRange ID="wuc_txtCalendarRange" runat="server" EnableViewState="true" /></td>
                                        </tr>
                                        <tr valign="top">
                                            <td><div class="cls_label_header">Product</div></td>
                                            <td><div class="cls_label_header">:</div></td>
                                            <td>
                                                <%--<asp:ListBox ID="ddlPrd" runat="server" Height="150px" Width="350px" CssClass="cls_listbox"></asp:ListBox>--%>
                                                <asp:DropDownList ID="ddlPrd" runat="server" Width="350px" CssClass="cls_dropdownlist" AutoPostBack="false"></asp:DropDownList>
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td colspan="3" align="left">
                                                <asp:Button ID="btnGenerate" runat="server" Text="Refresh" cssclass="cls_button" OnClick="btnGenerate_Click"/></td>
                                        </tr>                                                                             
                                    </table>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlDRCSearch" runat="server" CollapseControlID="imgEnquiryDetails"
                                        ExpandControlID="imgEnquiryDetails" TargetControlID="pnlDRCSearch" CollapsedSize="0"
                                        Collapsed="true" ExpandDirection="Vertical" SuppressPostBack="true">
                                    </ajaxToolkit:CollapsiblePanelExtender>
                                </asp:Panel>
                                
                                
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
 </table>