Imports System.Data

Partial Class SFMSActyList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Dim aryDataItemSub As New ArrayList

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SFMSActyList")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SFMSActyList") = value
        End Set
    End Property

    Private _licCustomHeaderCollectorSub As ListItemCollection
    Protected Property licHeaderCollectorSub() As ListItemCollection
        Get
            If _licCustomHeaderCollectorSub Is Nothing Then _licCustomHeaderCollectorSub = Session("HeaderCollectorSub_SFMSActyList")
            If _licCustomHeaderCollectorSub Is Nothing Then _licCustomHeaderCollectorSub = New ListItemCollection
            Return _licCustomHeaderCollectorSub
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollectorSub = value
            Session("HeaderCollectorSub_SFMSActyList") = value
        End Set
    End Property
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "SFMSActyList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblheader
                .Title = Report.GetName(SubModuleType.SFMSACTY) '"SFMS Activity List"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.SFMSACTY
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                TimerControl1.Enabled = True
            End If
            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 1 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(1)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    Try
    ''        Wuc_ctrlpanel.RefreshDetails()
    ''        Wuc_ctrlpanel.UpdateControlPanel()
    '        RenewDataBind()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabindingMaster()

            'wuc_ctrlpanel.ExportToFile(dgList, "Sales_Force_Activity_List")
            wuc_ctrlPanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabindingMaster()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDatabindingMaster()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabindingMaster()
    End Sub

    Public Sub RefreshDatabindingMaster()
        Dim dtMasterTable, dtCompleteTable As DataTable
        dtMasterTable = CType(ViewState("dtMaster"), DataTable)
        Dim strSortExpMaster As String = CType(ViewState("strSortExpression"), String)

        Try
            If dtMasterTable Is Nothing Then
                dtMasterTable = GetRecList(True)
                dtCompleteTable = GetRecList(False)

                ViewState("dtMaster") = dtMasterTable
                ViewState("dtComplete") = dtCompleteTable

                ViewState("strSortExpression") = Nothing
            End If

            If dtMasterTable Is Nothing Then
                dtMasterTable = New DataTable
            Else
                PreRenderMode(dtMasterTable, True)
                If dtMasterTable.Rows.Count = 0 Then
                    dtMasterTable.Rows.Add(dtMasterTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtMasterTable.Rows.Count
                End If
            End If


            Dim dvMaster As DataView
            dvMaster = New DataView(dtMasterTable)
            If Not String.IsNullOrEmpty(strSortExpMaster) Then
                Dim strSortExpressionName As String = strSortExpMaster.Replace(" DESC", "")
                dvMaster.Sort = IIf(dtMasterTable.Columns.Contains(strSortExpressionName), strSortExpMaster, "")
            End If

            dgList.DataSource = dvMaster
            dgList.DataBind()
            'If Not IsPostBack And dgList.Rows.Count > 0 Then
            If TimerControl1.Enabled And dgList.Rows.Count > 0 Then
                dgList.SelectedIndex = 0
                dgList_SelectedIndexChanged(dgList, Nothing)
            End If
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            RefreshDatabindingSub()
            wuc_ctrlpanel.RefreshDetails()
            wuc_ctrlpanel.UpdateControlPanel()
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub RefreshDatabindingSub()
        Dim dtDetailTable As DataTable
        Dim dvDetailView As DataView
        dtDetailTable = CType(ViewState("dtComplete"), DataTable)
        dvDetailView = CType(ViewState("dvDetailView"), DataView)

        Dim strTeamCode, strCatCode, strFilter As String
        strTeamCode = Trim(ViewState("TEAM_CODE"))
        strCatCode = Trim(ViewState("CAT_CODE"))
        
        Dim strSortExpSub As String
        strSortExpSub = CType(ViewState("strSortExpression_Sub"), String)

        Try
            If dvDetailView Is Nothing Then
                If dtDetailTable Is Nothing Then
                    dvDetailView = New DataView
                Else
                    PreRenderMode(dtDetailTable, False)

                    strFilter = "TEAM_CODE='" & strTeamCode & "' AND CAT_CODE='" & strCatCode & "'"
                    dvDetailView = New DataView(dtDetailTable, strFilter, "", DataViewRowState.Unchanged)
                    If dvDetailView.Count = 0 Then dvDetailView.AddNew()
                End If
            End If

            'Dim dvSub As DataView
            If Not String.IsNullOrEmpty(strSortExpSub) Then
                Dim strSortExpressionName As String = strSortExpSub.Replace(" DESC", "")
                dvDetailView.Sort = IIf(dtDetailTable.Columns.Contains(strSortExpressionName), strSortExpSub, "")
            End If

            'Bind To dgSubList
            dgSubList.DataSource = dvDetailView
            dgSubList.DataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If Master_Row_Count > 0 AndAlso dgList.Rows.Count < 15 Then dgList.GridHeight = Nothing
            If dgSubList.Rows.Count < 15 Then dgSubList.GridHeight = Nothing
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable, ByRef dgToBind As GridView, ByVal IsMaster As Boolean)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            dgToBind.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Dim MyType As FieldColumntype = IIf(IsMaster, DBSFMSAct_ColField.GetFieldColumnType_Master(ColumnName), DBSFMSAct_ColField.GetFieldColumnType_SUB(ColumnName))
                Select Case MyType
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                        If ColumnName Like ("*_NAME") Then dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Left

                        dgColumn.HeaderText = DBSFMSAct_ColField.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgToBind.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        If IsMaster Then
                            aryDataItem.Add(ColumnName)
                        Else
                            aryDataItemSub.Add(ColumnName)
                        End If
                End Select
            Next

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgToBind_Sorting : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList(ByVal IsMaster As Boolean) As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalID As String
            strPrincipalID = Session("PRINCIPAL_ID")
            strUserID = Session.Item("UserID")

            Dim clsSFMSAct As New rpt_SFMS.clsActyQuery
            If IsMaster Then
                DT = clsSFMSAct.GetActivity_MasterList(strUserID, strPrincipalID)
                dgList_Init(DT, dgList, True)
            Else
                DT = clsSFMSAct.GetActivityList(strUserID, strPrincipalID)
                dgList_Init(DT, dgSubList, False)
            End If

            RefreshDatabindingSub()

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable, ByVal IsMaster As Boolean)
        Try
            If IsMaster Then
                aryDataItem.Clear()
                dgList_Init(DT, dgList, True)
                'Cal_CustomerHeader()
            Else
                aryDataItemSub.Clear()
                dgList_Init(DT, dgSubList, False)
                'Cal_CustomerHeaderSub()
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "MASTER DGLIST"
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                'Dim oGridView As GridView = dgList
                'Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim TC As TableHeaderCell

                'TC = New TableHeaderCell
                'TC.Text = "Category"
                'TC.ColumnSpan = 3
                'GVR.Cells.Add(TC)

                'oGridView.Controls(0).Controls.AddAt(0, GVR)

                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)

            ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                e.Row.Attributes.Add("onclick", "javascript:__doPostBack('dgList','Select$" & e.Row.RowIndex & "')")
                e.Row.Attributes.Add("style", "cursor:pointer;")
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgList.SelectedIndexChanged
        Try
            If dgList.SelectedIndex < 0 Then Exit Sub
            Dim strTeamCode, strCatCode As String

            Dim dtMasterTable As DataTable = CType(ViewState("dtMaster"), DataTable)
            Dim strSortExpMaster As String = CType(ViewState("strSortExpression"), String)
            Dim dvMaster As DataView
            dvMaster = New DataView(dtMasterTable)
            If Not String.IsNullOrEmpty(strSortExpMaster) Then
                Dim strSortExpressionName As String = strSortExpMaster.Replace(" DESC", "")
                dvMaster.Sort = IIf(dtMasterTable.Columns.Contains(strSortExpressionName), strSortExpMaster, "")
            End If
            strTeamCode = IIf(IsDBNull(dvMaster.Item(dgList.SelectedIndex).Item(0)), "", dvMaster.Item(dgList.SelectedIndex).Item(0))
            strCatCode = IIf(IsDBNull(dvMaster.Item(dgList.SelectedIndex).Item(2)), "", dvMaster.Item(dgList.SelectedIndex).Item(2))

            'Dim dgRow As GridViewRow = dgList.SelectedRow
            'strTeamCode = Trim(IIf(dgRow.Cells(0) IsNot Nothing, dgRow.Cells(0).Text, ""))
            'strCatCode = Trim(IIf(dgRow.Cells(2) IsNot Nothing, dgRow.Cells(2).Text, ""))
            'strCatDesc = Trim(IIf(dgRow.Cells(3) IsNot Nothing, dgRow.Cells(3).Text, ""))
            ViewState("TEAM_CODE") = Trim(strTeamCode)
            ViewState("CAT_CODE") = Trim(strCatCode)
            'ViewState("CAT_NAME") = Trim(strCatDesc)
            RefreshDatabindingSub()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_SelectedIndexChanged : " & ex.ToString)
        End Try

    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            'collect the column name that ready to show on the screen
            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "CAT_*" Then
                    strColumnName = "Category"
                Else
                    strColumnName = DBSFMSAct_ColField.GetDisplayColumnName(strColumnName)
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabindingMaster()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SUB DGLIST"
    Protected Sub dgSubList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgSubList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                'Dim oGridView As GridView = dgSubList
                'Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                'Dim TC As TableHeaderCell

                'TC = New TableHeaderCell
                'TC.Text = "Sub Category"
                'TC.ColumnSpan = 2
                'GVR.Cells.Add(TC)

                'oGridView.Controls(0).Controls.AddAt(0, GVR)

                Dim oGridView As GridView = dgSubList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollectorSub
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollectorSub.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Private Sub Cal_CustomerHeaderSub()
        Try
            licHeaderCollectorSub = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList

            'collect the column name that ready to show on the screen
            For Each strColumnName In aryDataItemSub
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "SUB_CAT_*" Then
                    strColumnName = "Sub Category"
                Else
                    strColumnName = DBSFMSAct_ColField.GetDisplayColumnName(strColumnName)
                End If
                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollectorSub.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    licHeaderCollectorSub.Add(liColumnField)
                End If

            Next
            licHeaderCollectorSub = _licCustomHeaderCollectorSub
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeaderSub : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgSubList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression_Sub")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression_Sub") = strSortExpression
            RefreshDatabindingSub()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgSubList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub
End Class

Public Class DBSFMSAct_ColField

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strColumnName As String = ColumnName.ToUpper

        Select Case strColumnName
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType_Master(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetFieldColumnType_SUB(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            If strColumnName Like "TEAM_*" OrElse strColumnName Like "CAT_*" Then
                FCT = FieldColumntype.InvisibleColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

End Class

