'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	08/01/2007
'	Purpose	    :	Class to build datatables for getting trade return transaction details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsTradeReturn
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTradeReturn"
        End Get
    End Property

    Function getReasonDT(ByVal strSalesmanCode As String, ByVal strInd As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADERETURNREASON"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("ind", strind, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getReasonDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getLineTypeDT(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADELINETYPE"
                .addItem("salesmancode", strSalesmanCode, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getLineTypeDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Sub insertHeader(ByVal strTxnNo As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitID As String, ByVal dblRev As Double, ByVal strRemarks As String, ByVal strReason As String, ByVal strRefNo As String, ByVal strType As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADERETURNINSERTHEADER"
                .addItem("txnno", strTxnNo, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContCode, 2)
                .addItem("branchcode", strBranchCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("visitid", strVisitID, 2)
                .addItem("tolretval", dblRev, 5)
                .addItem("remarks", strRemarks, 2)
                .addItem("reasoncode", strReason, 2)
                .addItem("refno", strRefNo, 2)
                .addItem("rettype", strType, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertHeader :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub insertDetail(ByVal strTxnNo As String, ByVal strItemCode As String, ByVal strBatchNo As String, ByVal intQty As Integer, ByVal dblPrice As Double, ByVal strReasonCode As String, ByVal dtmExp As DateTime, ByVal strLT As String, ByVal strUOM As String, ByVal strRemark As String, ByVal dtmMnf As DateTime)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADERETURNINSERTDETAIL"
                .addItem("txnno", strTxnNo, 2)
                .addItem("itemcode", strItemCode, 2)
                .addItem("batchno", strBatchNo, 2)
                .addItem("retqty", intQty, 1)
                .addItem("listprice", dblPrice, 5)
                .addItem("reasoncode", strReasonCode, 2)
                .addItem("expdate", dtmExp, 3)
                .addItem("linetype", strLT, 2)
                .addItem("uomcd", strUOM, 2)
                .addItem("remarks", strRemark, 2)
                .addItem("mnfdate", dtmMnf, 3)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertDetail :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub deleteDetail(ByVal strTxnNo As String)
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADERETURNDELETEDETAIL"
                .addItem("txnno", strTxnNo, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteDetail :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub updateHeader(ByVal strTxnNo As String, ByVal dblTotal As Double, ByVal strRemarks As String, ByVal strReasonCode As String, ByVal strRefNo As String, ByVal strRetType As String)
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADERETURNUPDATEHEADER"
                .addItem("txnno", strTxnNo, 2)
                .addItem("total", dblTotal, 5)
                .addItem("remarks", strRemarks, 2)
                .addItem("reasoncode", strReasonCode, 2)
                .addItem("refno", strRefNo, 2)
                .addItem("rettype", strRetType, 2)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateHeader :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Function getEditInfo(ByVal strTxnNo As String)
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_TRADERETURNEDIT"
                .addItem("txnno", strTxnNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteDetail :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
