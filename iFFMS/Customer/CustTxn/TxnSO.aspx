<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnSO.aspx.vb" Inherits="iFFMS_Customer_CustTxn_TxnSO"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Prd_PrgGrpSearch" Src="~/iFFMS/COMMON/wuc_Prd_PrgGrpSearch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Transaction SO</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script src="../../../include/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        //GetShipToAddress
        var GetShipToAddressMaxRetry = 0;
        function GetShipToAddress() {
            $("#message").css('display', 'none'); $("#txtshiptoadd").val('Loading');
            if ($("#ddlshiptocode").val() && getQueryVariable('custcode')) {
                ws_CustTxn.GetShipToAddress(getQueryVariable('custcode'), $("#ddlshiptocode").val(), onGetShipToAddressLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
            }
            else {
                if (GetShipToAddressMaxRetry <= 5) { setTimeout('GetShipToAddress()', 2000); GetShipToAddressMaxRetry + GetShipToAddressMaxRetry + 1; }
                else { $("#txtshiptoadd").val('Kindly reselect ship to'); };
            };
        }

        function onGetShipToAddressLoadSuccess(result) {
            if (result.Total > 0) {
                $("#txtshiptoadd").val(result.Rows[0]['ADDRESS']);
            }
            else { $("#lblshiptoadd").html('Ship to Address information not found!'); };
        }
        //GetShipToAddress

        //ValidateProduct
        function ValidateProduct() {
            $("#message").css('display', 'none'); $("#txtPrdName").val('Loading'); $("#lbluomload").html('Loading');
            if ($("#txtPrdCode").val()) {
                ws_CustTxn.ValidateProduct($("#txtPrdCode").val(), getQueryVariable('salesrepcode'), onValidateProductLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
            } else { $("#txtPrdName").val(''); $("#lbluomload").html(''); $("#lbluomload").html(''); bindDDLUOM(); }
        }

        function onValidateProductLoadSuccess(result) {
            if (result.Total > 0) {
                $("#txtPrdName").val(result.Rows[0]['PRD_NAME']); bindDDLUOM();
            } else { PrdSearchPopup_Show(); $("#txtPrdCode").val(''); $("#txtPrdName").val('Product information not found!'); }
        }

        function bindDDLUOM() {
            $("#message").css('display', 'none');
            ws_CustTxn.GetUOMCode($("#txtPrdCode").val(), onUOMLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
        }
        //ValidateProduct

        //SearchProduct
        function SearchProduct() {
            $("#message").css('display', 'none');
            if ($("#txtPrdCode").val()) {
                ws_CustTxn.ValidateProduct($("#txtPrdCode").val(), getQueryVariable('salesrepcode'), onSearchProductLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
            } else { PrdSearchPopup_Show(); }
        }

        function onSearchProductLoadSuccess(result) {
            if (result.Total > 0) {
                alert('Product found!');
            } else { alert('Product not found!'); PrdSearchPopup_Show(); }
        }
        //SearchProduct

        //ddluom
        function onUOMLoadSuccess(result) {
            $("#ddluom").find('option').remove().end();
            for (var idx = 0; idx < result.Total; idx++) {
                addDropDownValue($("#ddluom"), result.Rows[idx]["UOM_CODE"], result.Rows[idx]["UOM_PRICE"]);
            }
            if (result.Total > 0) { $("select#ddluom").get(0).selectedIndex = 0; UOMPrice(); };
            $("#lbluomload").html('');
        }

        function addDropDownValue(ddlCtrl, text, value) {
            ddlCtrl.append($(document.createElement("option")).attr("value", value).text(text));
        }
        //ddluom

        //UOMPrice
        function UOMPrice() {
            $("#lbluompricedisplay").html('Loading');
            if ($("select#ddluom").get(0).selectedIndex == -1) { $("#txtselecteduom").val('0'); } else { $("#txtselecteduom").val($("#ddluom option:selected").text()); }
            ws_CustTxn.GetPrdUOMPrice(getQueryVariable('salesrepcode'), getQueryVariable('custcode'), getQueryVariable('contcode'), $("#txtPrdCode").val(), $("#txtselecteduom").val(), '1', onUOMPriceLoadSuccess, function(exception) { $("#message").html(exception.get_message()); $("#message").css('display', ''); });
        }

        function onUOMPriceLoadSuccess(result) {
            $("#lbluomprice").val(result.Rows[0]["UOM_PRICE"]); CalculateAmt(); $("#lbluompricedisplay").html('');
        }
        //UOMPrice
        
        function CalculateAmt() {
            var amt = ($("#txtqty").val() * $("#lbluomprice").val()).toFixed(2);
            if ($("#ddllinetype").val() == '.' || $("#ddllinetype").val() == ',') { $("#lblamount").val(amt); } else { $("#lblamount").val(0); }
        }

        function EnterToTab() { var keycode = window.event.keyCode; if (keycode == 13) { window.event.keyCode = 9; } }

        function getQueryVariable(variable) {
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) { return pair[1]; }
            }
        }

        function DisableValidator(validator) {
            var Validator = document.getElementById(validator);
            ValidatorEnable(Validator, false);
        }


        function ValidateCheckBoxStates() {
            var flag = false;
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() {
                if (this.checked == true) flag = true;
            });
            if (flag) { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function ChangeAllCheckBoxStates(element) {
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() { if (this != element) { this.checked = element.checked; } });
        }

        function ChangeHeaderCheckBoxStates(element) {
            var flag = true;
            $("#dgList").find("input:checkbox[Id*=chkdelete]").each(function() { if (this.checked == false) flag = false; });
            $("#dgList").find("input:checkbox[Id*=DeleteAllCheckBox]").attr('checked', flag);
        }

        function ReselectDDLUOM(strSelectedUOM) {
            $("#ddluom").val(strSelectedUOM);
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe')"
    style="background-color: #DDDDDD;">
    <form id="frmtxnSo" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release">
        <Services>
            <asp:ServiceReference Path="~/DataServices/ws_CustTxn.asmx" />
        </Services>
    </ajaxToolkit:ToolkitScriptManager>
    <fieldset style="width: 98%;">
        <div id="title">
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
            <span id="message" style="display: none; float: left;" class="cls_label_err"></span>
        </div>
        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
            width="100%" border="0" style="height: 30px">
            <tr align="left" valign="bottom">
                <td>
                    <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                        CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
            <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Field Force Name</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 300px">
                                        <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Customer Name</span>
                                        <td style="width: 5px">
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td style="width: 300px">
                                            <asp:Label ID="lblCustName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                        </td>
                                        <td style="width: 100px">
                                            <span class="cls_label_header">Contact Name</span>
                                            <td style="width: 5px">
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:Label ID="lblContName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                            </td>
                            </tr>
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Visit ID</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblVisitId" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Map</span>
                                    </td>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </asp:Panel>
        <div id="txnSo">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:Button runat="server" ID="btnHidden" Style="display: none;" Text="" Visible="false" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                    <div id="Proce" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                        background-color: Gray">
                        <span style="float: right;">
                            <asp:Button ID="btnsubmit" runat="server" Text="Proceed >>" CssClass="cls_button"
                                TabIndex="17" ValidationGroup="SoHdr" Width="80px" />
                        </span>
                    </div>
                    <asp:UpdatePanel ID="UPHdr" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <div id="dheader" class="S_DivHeader">
                                General Information
                            </div>
                            <asp:Panel ID="pSoHdr" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                                    <table class="cls_form_table">
                                        <tr>
                                            <td width="10%">
                                                <span class="cls_label">Txn No:</span>
                                            </td>
                                            <td width="40%">
                                                <asp:Label ID="lbltxnno" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td width="10%">
                                                <span class="cls_label">PO No: </span>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="txtpono" runat="server" CssClass="cls_textbox" ValidationGroup="SOHdr"
                                                    MaxLength="15" TabIndex="4" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <span class="cls_label">Ship To:</span>
                                            </td>
                                            <td width="40%">
                                                <asp:DropDownList ID="ddlshiptocode" runat="server" CssClass="cls_dropdownlist" Width=""
                                                    ValidationGroup="SoDtl" onChange="GetShipToAddress();" Onblur="GetShipToAddress();"
                                                    TabIndex="0">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvddlshiptocode" runat="server" ControlToValidate="ddlshiptocode"
                                                    CssClass="cls_label_err" ErrorMessage="<br />Select a ship to" ValidationGroup="SoDtl"
                                                    Display="Dynamic" />
                                            </td>
                                            <td width="10%">
                                                <span class="cls_label">Delivery Date: </span>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="txtdeldate" runat="server" CssClass="cls_textbox" ValidationGroup="SoDtl"
                                                    MaxLength="10" TabIndex="5"></asp:TextBox>
                                                <asp:ImageButton ID="imgdeldate" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                                    CssClass="cls_button" CausesValidation="false" />
                                                <ajaxToolkit:CalendarExtender ID="ceexpdate" TargetControlID="txtdeldate" runat="server"
                                                    PopupButtonID="imgdeldate" Animated="false" Format="yyyy-MM-dd" PopupPosition="TopLeft">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="rfvFormatDate" runat="server" Display="Dynamic" ControlToValidate="txtdeldate"
                                                    CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="SoDtl" />
                                                <asp:CompareValidator ID="rfvCheckDataType" runat="server" CssClass="cls_validator"
                                                    ControlToValidate="txtdeldate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                    ErrorMessage=" Invalid Date!" ValidationGroup="SoDtl" />
                                                <asp:CompareValidator runat="server" ID="CVtxtdeldatedate" ControlToValidate="txtdeldate"
                                                    CssClass="cls_validator" Display="Dynamic" ValueToCompare='<%# DateTime.Now.ToString("d") %>'
                                                    Type="date" Operator="GreaterThanEqual" ErrorMessage="Date less than today" ValidationGroup="SoDtl" />
                                                <asp:RequiredFieldValidator ID="rfvtxtdeldate" runat="server" ControlToValidate="txtdeldate"
                                                    CssClass="cls_label_err" ErrorMessage="Enter delivery date" ValidationGroup="SoDtl"
                                                    Display="Dynamic" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%" valign="top">
                                                <span class="cls_label">Ship To Address:</span>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="txtshiptoadd" runat="server" CssClass="cls_textbox" ValidationGroup="SoDtl"
                                                    Width="350px" Enabled="false" TextMode="MultiLine" Height="50px" TabIndex="1" />
                                            </td>
                                            <td width="10%" valign="top">
                                                <span class="cls_label">Partial Delivery: </span>
                                            </td>
                                            <td width="40%" valign="top">
                                                <asp:CheckBox ID="chkpartdel" runat="server" CssClass="cls_checkbox" ValidationGroup="SoDtl"
                                                    Checked="true" TabIndex="6" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <span class="cls_label">Payment By:</span>
                                            </td>
                                            <td width="40%">
                                                <asp:RadioButtonList ID="rdopayby" runat="server" CssClass="cls_radiobuttonlist"
                                                    ValidationGroup="SoDtl" AutoPostBack="true" RepeatDirection="Horizontal" TabIndex="2">
                                                    <asp:ListItem Text="TERM" Value="TERM" Selected="True" />
                                                    <asp:ListItem Text="DATE" Value="DATE" />
                                                </asp:RadioButtonList>
                                            </td>
                                            <td width="10%">
                                                <asp:Label ID="lblpaydate" runat="server" Text="Payment Date:" CssClass="cls_label"></asp:Label>
                                                <asp:Label ID="lblpayterm" runat="server" Text="Payment Term:" CssClass="cls_label"></asp:Label>
                                            </td>
                                            <td width="40%" colspan="2">
                                                <asp:TextBox ID="txtpaydate" runat="server" CssClass="cls_textbox" ValidationGroup="SoDtl"
                                                    MaxLength="10" TabIndex="7"></asp:TextBox>
                                                <asp:ImageButton ID="imgpaydate" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                                    CssClass="cls_button" CausesValidation="false" />
                                                <ajaxToolkit:CalendarExtender ID="cetxtpaydate" TargetControlID="txtpaydate" runat="server"
                                                    PopupButtonID="imgpaydate" Animated="false" Format="yyyy-MM-dd" PopupPosition="TopLeft">
                                                </ajaxToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="cvtxtpaydate" runat="server" Display="Dynamic" ControlToValidate="txtpaydate"
                                                    CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="SoDtl" />
                                                <asp:CompareValidator ID="cvtxtpaydate2" runat="server" CssClass="cls_validator"
                                                    ControlToValidate="txtpaydate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                    ErrorMessage=" Invalid Date!" ValidationGroup="SoDtl" />
                                                <asp:DropDownList ID="ddltermcode" runat="server" CssClass="cls_dropdownlist" ValidationGroup="SOHdr" />
                                                <asp:RequiredFieldValidator ID="rfvddltermcode" runat="server" ControlToValidate="ddltermcode"
                                                    CssClass="cls_label_err" ErrorMessage="Enter pay date" ValidationGroup="SoDtl"
                                                    Display="Dynamic" />
                                                <asp:CompareValidator runat="server" ID="cvtxtpaydatedate" ControlToValidate="txtpaydate"
                                                    CssClass="cls_validator" Display="Dynamic" ValueToCompare='<%# DateTime.Now.ToString("d") %>'
                                                    Type="date" Operator="GreaterThan" ErrorMessage="Date less than today" ValidationGroup="SoDtl" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10%">
                                                <span class="cls_label">Remarks:</span>
                                            </td>
                                            <td width="40%">
                                                <asp:TextBox ID="txtrmks" runat="server" CssClass="cls_textbox" ValidationGroup="SoDtl"
                                                    Width="300px" MaxLength="100" TabIndex="3" />
                                            </td>
                                            <td width="10%">
                                                <span class="cls_label"></span>
                                            </td>
                                            <td width="40%">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpepsoHdr" runat="server" TargetControlID="psoHdr"
                                ExpandControlID="dheader" CollapseControlID="dheader">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdPnlDtl" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <div id="ddtl" class="S_DivHeader">
                                Detail Information - Add New/Edit
                            </div>
                            <div style="width: 100%;">
                                <asp:Panel ID="pSoDtl" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                                        <table class="cls_form_table">
                                            <tr>
                                                <td align="left" colspan="4">
                                                    <asp:Button ID="btnresetdtl" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="false"
                                                        TabIndex="15" Width="80px" ValidationGroup="SoDtl" OnClientClick="var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;" />
                                                    <asp:Button ID="btnsavedtl" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="SoDtl"
                                                        TabIndex="16" Width="80px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Product Code: </span>
                                                    <asp:Label ID="lbllineno" runat="server" Text="0" Visible="false" CssClass="cls_label"></asp:Label>
                                                </td>
                                                <td width="40%">
                                                    <asp:TextBox ID="txtPrdCode" runat="server" CssClass="cls_textbox" ValidationGroup="SoDtl"
                                                        MaxLength="49" onBlur="ValidateProduct();" onChange="ValidateProduct();" TabIndex="8"></asp:TextBox>
                                                    <input id="btnSearchPrd" type="button" class="cls_button" value="Search" onclick="SearchProduct();"
                                                        style="width: 80px;" />
                                                    <asp:RequiredFieldValidator ID="rfvprdcode" ControlToValidate="txtPrdCode" runat="server"
                                                        Display="Dynamic" ErrorMessage="Select a product" CssClass="cls_label_err" ValidationGroup="SoDtl"></asp:RequiredFieldValidator>
                                                </td>
                                                <td width="10%">
                                                    <span class="cls_label">Line Type:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:DropDownList ID="ddllinetype" runat="server" CssClass="cls_dropdownlist" ValidationGroup="SoDtl"
                                                        onChange="CalculateAmt()" TabIndex="12">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvddllinetype" runat="server" ControlToValidate="ddllinetype"
                                                        Display="Dynamic" CssClass="cls_label_err" ValidationGroup="SoDtl" ErrorMessage="Select a line type"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="cls_label">Product Name: </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPrdName" runat="server" CssClass="cls_textbox" Width="300px"
                                                        Enabled="false" ValidationGroup="SoDtl" TabIndex="9"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <span class="cls_label">Quantity: </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtqty" runat="server" CssClass="cls_textbox" onchange='CalculateAmt();'
                                                        Text="" MaxLength="8" onblur='CalculateAmt();' ValidationGroup="SoDtl" TabIndex="13"></asp:TextBox>
                                                    <asp:CompareValidator ID="cvtxtqty" runat="server" ControlToValidate="txtqty" ValidationGroup="SoDtl"
                                                        Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ErrorMessage="Enter number only with no decimal"
                                                        CssClass="cls_label_err">
                                                    </asp:CompareValidator>
                                                    <asp:CompareValidator ID="cvtxtqty2" ControlToValidate="txtqty" Operator="GreaterThan"
                                                        Display="Dynamic" ValueToCompare="0" Type="Double" runat="server" ErrorMessage=" Value cannot be 0"
                                                        ValidationGroup="SoDtl" CssClass="cls_label_err"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="rfvtxtqty" ControlToValidate="txtqty" runat="server"
                                                        Display="Dynamic" ErrorMessage="Enter quantity" CssClass="cls_label_err" ValidationGroup="SoDtl"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="cls_label">UOM: </span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddluom" runat="server" CssClass="cls_dropdownlist" onchange='UOMPrice();CalculateAmt();'
                                                        onblur='UOMPrice();CalculateAmt();' ValidationGroup="SoDtl" TabIndex="10">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lbluomload" runat="server" Text="" CssClass="cls_label" />
                                                    <div style="display: none;">
                                                        <asp:TextBox ID="txtselecteduom" runat="server"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="rfvddluom" runat="server" ControlToValidate="txtselecteduom"
                                                        Display="Dynamic" CssClass="cls_label_err" ValidationGroup="SoDtl" ErrorMessage="Select UOM"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    <span class="cls_label">Amount: </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="lblamount" runat="server" Text="0" CssClass="cls_textbox" ValidationGroup="SoDtl"
                                                        Enabled="false" MaxLength="8" TabIndex="14"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="cls_label">UOM.PRC: </span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="lbluomprice" runat="server" Text="" CssClass="cls_textbox" Enabled="false"
                                                        onchange='CalculateAmt();' onblur='CalculateAmt();' ValidationGroup="SoDtl" TabIndex="11"></asp:TextBox>
                                                    <asp:Label ID="lbluompricedisplay" runat="server" Text="" CssClass=" cls_label"></asp:Label>
                                                </td>
                                                <td>
                                                    <span class="cls_label"></span>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpesoDtl" runat="server" TargetControlID="psoDtl"
                                    ExpandControlID="ddtl" CollapseControlID="ddtl">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpPnlDtlSum" runat="server" UpdateMode="Conditional" RenderMode="block">
                        <ContentTemplate>
                            <div id="Div1" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                                background-color: Gray">
                                <span style="float: right;">
                                    <asp:Button ID="btnProceeed" runat="server" Text="Proceed >>" CssClass="cls_button"
                                        ValidationGroup="SoHdr" Width="80px" TabIndex="17" />
                                </span>
                            </div>
                            <div id="divsumm" class="S_DivHeader">
                                Detail Information - View/Delete
                            </div>
                            <asp:Panel ID="pgridview" runat="server" CssClass="cls_panel_header" Width="99.8%">
                                <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                    <asp:Button ID="btndeletesum" runat="server" Text="Delete" CssClass="cls_button"
                                        Visible="false" Width="80px" CausesValidation="false" ValidationGroup="SOSum"
                                        OnClientClick="return ValidateCheckBoxStates();" TabIndex="19" />
                                </div>
                                <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                    padding-bottom: 10px;">
                                    <ccGV:clsGridView ID="dgList" runat="server" AutoGenerateColumns="False" Width="100%"
                                        FreezeHeader="false" GridHeight="" AddEmptyHeaders="0" CellPadding="2" CssClass="Grid"
                                        EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="99.8%" PagerSettings-Visible="false"
                                        DataKeyNames="TXN_NO,PRD_CODE,LINE_TYPE,LINE_NO,FOC_IND" BorderColor="Black"
                                        BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="DeleteAllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkdelete" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:CommandField HeaderText="Edit" ShowEditButton="True" ShowHeader="True" EditText="<img src='../../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:CommandField>
                                            <asp:BoundField DataField="LINE_NO" HeaderText="Line No" ReadOnly="True" SortExpression="LINE_NO">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TXN_NO" HeaderText="Txn No" ReadOnly="True" SortExpression="TXN_NO">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRD_CODE" HeaderText="Product Code" ReadOnly="True" SortExpression="PRD_CODE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRD_NAME" HeaderText="Product Name" ReadOnly="True" SortExpression="PRD_NAME">
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LINE_TYPE" HeaderText="Line Type" ReadOnly="True" SortExpression="LINE_TYPE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LINE_TYPE_NAME" HeaderText="Line Type" ReadOnly="True"
                                                SortExpression="LINE_TYPE_NAME">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UOM_CODE" HeaderText="UOM Code" ReadOnly="True" SortExpression="UOM_CODE">
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="QTY" HeaderText="Quantity" ReadOnly="True" SortExpression="QTY">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LIST_PRICE" HeaderText="List Price" ReadOnly="True" SortExpression="LIST_PRICE">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AMT" HeaderText="Total Amount" ReadOnly="True" SortExpression="AMT">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                        </Columns>
                                        <FooterStyle CssClass="GridFooter" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAlternate" />
                                        <RowStyle CssClass="GridNormal" />
                                        <PagerSettings Visible="False" />
                                    </ccGV:clsGridView>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:CollapsiblePanelExtender ID="cpepgridview" runat="server" TargetControlID="pgridview"
                                ExpandControlID="divsumm" CollapseControlID="divsumm">
                            </ajaxToolkit:CollapsiblePanelExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    <customToolkit:wuc_Prd_PrgGrpSearch ID="wuc_Prd_PrgGrpSearch" Title="Product Search"
        runat="server" />
    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    </form>
</body>
</html>
