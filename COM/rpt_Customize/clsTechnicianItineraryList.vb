'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	20/08/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsTechnicianItineraryList
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTechinicianItineraryList"
        End Get
    End Property

    'Public Function GetItineraryAct(ByVal strTechnicianCode As String, ByVal strCustCode As String, _
    '                                ByVal strStartDateFrom As String, ByVal strStartDateTo As String, _
    '                                ByVal strEndDateFrom As String, ByVal strEndDateTo As String, _
    '                                ByVal strUserID As String) As DataTable
    '    Dim objDB As clsDB

    '    Try
    '        objDB = New clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_RPT_ITINERARY_ACT_LIST"
    '            .addItem("TECHNICIAN_CODE", strTechnicianCode, clsDB.DataType.DBString)
    '            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
    '            .addItem("START_DATE_FROM", strStartDateFrom, clsDB.DataType.DBString)
    '            .addItem("START_DATE_TO", strStartDateTo, clsDB.DataType.DBString)
    '            .addItem("END_DATE_FROM", strEndDateFrom, clsDB.DataType.DBString)
    '            .addItem("END_DATE_TO", strEndDateTo, clsDB.DataType.DBString)
    '            .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
    '            Return .spRetrieve()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    'Public Function GetItineraryNtf(ByVal strTechnicianCode As String, ByVal strCustCode As String, _
    '                                ByVal strStartDateFrom As String, ByVal strStartDateTo As String, _
    '                                ByVal strEndDateFrom As String, ByVal strEndDateTo As String, _
    '                                ByVal strUserID As String) As DataTable
    '    Dim objDB As clsDB

    '    Try
    '        objDB = New clsDB
    '        With objDB
    '            .ConnectionString = strConn
    '            .CmdText = "SPP_RPT_ITINERARY_NTF_LIST"
    '            .addItem("TECHNICIAN_CODE", strTechnicianCode, clsDB.DataType.DBString)
    '            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
    '            .addItem("START_DATE_FROM", strStartDateFrom, clsDB.DataType.DBString)
    '            .addItem("START_DATE_TO", strStartDateTo, clsDB.DataType.DBString)
    '            .addItem("END_DATE_FROM", strEndDateFrom, clsDB.DataType.DBString)
    '            .addItem("END_DATE_TO", strEndDateTo, clsDB.DataType.DBString)
    '            .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
    '            Return .spRetrieve()
    '        End With
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
    '    Finally
    '        objDB = Nothing
    '    End Try
    'End Function

    Public Function GetItineraryList(ByVal strTechnicianCode As String, ByVal strCustCode As String, _
                                    ByVal strStartDateFrom As String, ByVal strStartDateTo As String, _
                                    ByVal strEndDateFrom As String, ByVal strEndDateTo As String, _
                                    ByVal strReportType As String, ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ITINERARY_LIST"
                .addItem("TECHNICIAN_CODE", strTechnicianCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("START_DATE_FROM", strStartDateFrom, clsDB.DataType.DBString)
                .addItem("START_DATE_TO", strStartDateTo, clsDB.DataType.DBString)
                .addItem("END_DATE_FROM", strEndDateFrom, clsDB.DataType.DBString)
                .addItem("END_DATE_TO", strEndDateTo, clsDB.DataType.DBString)
                .addItem("REPORT_TYPE", strReportType, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetItineraryNtfHdr(ByVal strTxnNo As String, ByVal strInd As String, ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ITINERARY_NTF_HDR"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("IND", strInd, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetItineraryNtfDtl(ByVal strTxnNo As String, ByVal strInd As String, ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ITINERARY_NTF_DTL"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("IND", strInd, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
