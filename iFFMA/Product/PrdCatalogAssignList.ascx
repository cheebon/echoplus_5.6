﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PrdCatalogAssignList.ascx.vb"
    Inherits="iFFMA_Product_PrdCatalogAssignList" %>
<%@ Register TagPrefix="wuctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<%@ Register TagPrefix="wucPrdCatalogPop" TagName="wuc_PrdCatalogPop" Src="PrdCatalogPop.ascx" %>
<%@ Register TagPrefix="wucPrdCatalogRatingPop" TagName="wuc_PrdCatalogRatingPop" Src="PrdCatalogRatingPop.ascx" %>

<%--<%@ Register TagPrefix="wucSalesteamDashboardXFieldPop" TagName="wuc_SalesteamDashboardXFieldPop" Src="SalesteamDashboardXFieldPop.ascx" %>
<%@ Register TagPrefix="wucSalesteamSfeKpiPop" TagName="wuc_SalesteamSfeKpiPop" Src="SalesteamSfeKpiPop.ascx" %>
<%@ Register TagPrefix="wucSalesteamDetailingPop" TagName="wuc_SalesteamDetailingPop" Src="SalesteamDetailingPop.ascx" %>--%>
<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />

<asp:UpdatePanel ID="UpdatePnlSalesteamAssign" runat="server" RenderMode="Inline" UpdateMode="Conditional">

    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%">
            <tr>
                <td style="width: 8%">
                </td>
                <td style="width: 2%">
                </td>
                <td style="width: 90%">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="left">
                    <asp:UpdatePanel ID="UpdatepnlCtrlSearch" runat="server" RenderMode="block" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlCtrlSearch" runat="server" Visible="false" class="cls_panel_header">
                                <table width="500px" >
                                    <tr>
                                        <td>
                                            <span class="cls_label_header">Search By</span>
                                        </td>
                                        <td>
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True"
                                                Width="115px" />
                                        </td>
                                    </tr>
                                    <tr id="SearchValueRow" runat="server">
                                        <td>
                                            <span class="cls_label_header">Search Value</span>
                                        </td>
                                        <td>
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSearch" CssClass="cls_button" runat="server" Text="Search" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr style="height: 15px" align="left" valign="middle">
                <td colspan="3">
                    <asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Font-Underline="true"></asp:Label>
                </td>
            </tr>
            <tr style="height: 15px" align="left">
                <%--<td>
                    <asp:Label ID="lblTotalHdr" runat="server" CssClass="cls_label"></asp:Label>
                </td>
                <td>
                    <span class="cls_label">:</span>
                </td>
                <td>
                    <asp:Label ID="lblTotal" runat="server" CssClass="cls_label">0</asp:Label>
                </td>--%>
                <asp:HiddenField ID="hdAssignType" runat="server" Value="0" />
                <asp:HiddenField ID="hdCode" runat="server" Value="" />
                <asp:HiddenField ID="hdCatCode" runat="server" Value="" />
            </tr>
            <%--<tr>
                <td>
                    &nbsp;
                </td>
            </tr>--%>
            <asp:Panel ID="pnlCtrlAction" runat="server">
                <tr>
                    <td colspan="3" align="left" style="padding-left: 15px">
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add"/>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td colspan="3" align="center" style="width: 95%;">
                    <customToolkit:wuc_dgpaging ID="wuc_dgAssignPaging" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center" style="width: 95%;">
                    <ccGV:clsGridView ID="dgAssignList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                    </ccGV:clsGridView>
                </td>
            </tr>
        </table>

        <wucPrdCatalogPop:wuc_PrdCatalogPop ID="wuc_PrdCatalogPop" Title="Product Catalog Assignment" runat="server" />
        <wucPrdCatalogRatingPop:wuc_PrdCatalogRatingPop ID="wuc_PrdCatalogRatingPop" Title="Product Catalog Rating Assignment" runat="server" />

        <%--<wucSalesteamDashboardXFieldPop:wuc_SalesteamDashboardXFieldPop ID="wuc_SalesteamDashboardXFieldPop" Title="Salesteam Dashboard XField Assignment" runat="server" />
        <wucSalesteamSfeKpiPop:wuc_SalesteamSfeKpiPop ID="wuc_SalesteamSfeKpiPop" Title="Salesteam SFE KPI Assignment" runat="server" />
        <wucSalesteamDetailingPop:wuc_SalesteamDetailingPop ID="wuc_SalesteamDetailingPop" Title="Salesteam Detailing Assignment" runat="server" />
        --%>
    </ContentTemplate>
</asp:UpdatePanel>

