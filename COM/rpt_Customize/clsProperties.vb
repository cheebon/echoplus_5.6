Public Class clsProperties
    Public UserID, UserCode As String
    Public PrinID, PrinCode As String
    Public Year, Month, SelectedDate As String
    Public TeamCode, SalesrepCode, ContCode, CustCode, PTLCode As String, ProductCode As String
    Public SubCatCode, strClass As String
    Public StartYear, StartMonth, EndYear, EndMonth As String
    Public strPrincipalId As String, strPrincipalCode As String
    Public strSalesrepList As String
    Public intNetValue As String
    Public strChainCode As String
End Class
