﻿Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPrdCatalog
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPrdCatalog"
        End Get
    End Property

    Public Function GetPrdCatalogCategoryList(strTeamCode As String, strSearchType As String, strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_CAT_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function PrdCatalogCategoryOperation(strTeamCode As String, strCatCode As String, strOperationType As String, Optional strCatName As String = "") As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_CAT_OPERATION"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("CAT_NAME", strCatName, clsDB.DataType.DBString)
                .addItem("OPERATION_TYPE", strOperationType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdCatalogCategoryDetail(strTeamCode As String, strCatCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_CAT_DTL"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdCatalogTeamList() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_TEAM_LIST"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdCatalogList(strTeamCode As String, strCatCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_LIST"
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetPrdCatalogRatingList(strCatCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_RATING_LIST"
                '.addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function PrdCatalogOperation(strCatCode As String, strPrdCode As String, strSeqId As String, strOperationType As String, Optional strFilePath As String = "") As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_OPERATION"
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("SEQ_ID", strSeqId, clsDB.DataType.DBString)
                .addItem("FILE_PATH", strFilePath, clsDB.DataType.DBString)
                .addItem("OPERATION_TYPE", strOperationType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function PrdCatalogRatingOperation(strCatCode As String, strPrdCode As String, strRateCode As String, strRateValue As String, strOperationType As String, Optional strFilePath As String = "") As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PRD_CATALOG_RATING_OPERATION"
                .addItem("CAT_CODE", strCatCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("RATE_CODE", strRateCode, clsDB.DataType.DBString)
                .addItem("RATE_VALUE", strRateValue, clsDB.DataType.DBString)
                .addItem("FILE_PATH", strFilePath, clsDB.DataType.DBString)
                .addItem("OPERATION_TYPE", strOperationType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
