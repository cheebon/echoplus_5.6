<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PerformanceOverview.aspx.vb" Inherits="iFFMS_Performance_PerformanceOverview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Performance Overview</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body style="margin: 0; border: 0; padding: 0; background-color: #DDDDDD; overflow: hidden;">
    <div id="Top" style="display: block; margin: 0; border: 0; padding: 0; float: left;
        width: 100%; overflow: hidden;">
        <div id="TopBar" style="display: block; margin: 0; border: 0; width: 100%; overflow: hidden;
            padding: 0; float: left">
            <iframe id="TopBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMS/Performance/Perf.aspx"
                width="100%" height="" scrolling="auto" style="border: 0; position: relative; display: inline;
                top: 0px;"></iframe>
        </div>
        <div id="ContentBar" style="display: none; margin: 0; overflow: hidden; border: 0;width: 100%; 
            padding: 0; float: left">
            <iframe id="ContentBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMS/Common/NoRecordFoundPage.aspx"
                width="100%" height="" scrolling="auto" style="border: 0; position: relative; display: inline;
                top: 0px;"></iframe>
        </div>
    </div>
    <div id="DetailBar" style="display: none; margin: 0; border: 0; overflow: hidden;
        width: 100%; padding: 0;">
        <iframe id="DetailBarIframe" frameborder="0" marginwidth="0" marginheight="0" src="../../iFFMS/Common/NoRecordFoundPage.aspx"
            width="100%" scrolling="auto" height="" style="border: 0; position: relative; display: inline;
            top: 0px;"></iframe>
    </div>
  
</body>
</html>
