'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	24/11/2006
'	Purpose	    :	Editing User Password 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_User_UserPwdEdit
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        cvPoorPass.ErrorMessage = Portal.Policy.GetPwdErrorMsg()
    End Sub


    Dim dtPassLib As DataTable 

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCrypto As cor_Crypto.clsDecryption
        Dim strUserCode As String
        Dim dt As DataTable

        Try
            lblErr.Text = ""

            'Call Header
            With wuc_lblheader
                .Title = "Change User Passsword"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request              
                strUserCode = Trim(Request.QueryString("user_code"))

                'Prepare Recordset,Createobject as needed

                objUserQuery = New adm_User.clsUserQuery
                With objUserQuery
                    .clsProperties.user_code = strUserCode
                    dt = .GetUserDtl()
                End With

                If dt.Rows.Count > 0 Then
                    'Decrypt Password
                    objCrypto = New cor_Crypto.clsDecryption
                    With objCrypto
                        txtOrigPassword.Text = .TripleDESDecrypt(Trim(dt.Rows(0)("pwd")))
                    End With

                    txtUserID.Text = Trim(dt.Rows(0)("user_id"))
                    lblLoginValue.Text = Trim(dt.Rows(0)("login"))
                    lblUserCodeValue.Text = Trim(dt.Rows(0)("user_code"))
                    lblUserNameValue.Text = Trim(dt.Rows(0)("user_name"))
                    'txtOrigPassword.Text = Trim(dt.Rows(0)("pwd"))
                End If
            End If

        Catch ex As Exception
            ExceptionMsg("UserPwdEdit.Page_Load : " & ex.ToString)
        Finally
            objCrypto = Nothing
            objUserQuery = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnSubmit_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim objUser As adm_User.clsUser
        Dim objUserQuery As adm_User.clsUserQuery
        Dim objCrypto As cor_Crypto.clsEncryption
        Dim strEncryptPwd As String = ""
        'Dim dt As DataTable
        'Dim strPwd As String = ""
        Dim strScript As String = ""

        Try
            'Check New Password match
            If Trim(txtNewPassword.Text) <> Trim(txtConfirmedPassword.Text) And Trim(txtNewPassword.Text <> "") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('New Password not match. \nPlease check your New Password.');</script>")
                Exit Sub
            End If

            'Check Old Password Validity
            'objUserQuery = New adm_User.clsUserQuery
            'With objUserQuery
            '    .clsProperties.login = lblLoginValue.Text
            '    dt = .GetUserLogin
            '    If dt.Rows.Count > 1 Then
            '        strPwd = dt.Rows(0)("pwd")
            '    End If
            'End With
            If Trim(txtPassword.Text) <> Trim(txtOrigPassword.Text) And Trim(txtPassword.Text <> "") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Password not match. \nPlease check your Password.');</script>")
                Exit Sub
            End If

            'New Password <> Old Password
            'If Trim(txtNewPassword.Text) = Trim(txtOrigPassword.Text) Then
            '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('New Password must be different !');</script>")
            '    Exit Sub
            'End If

            If (Trim(txtNewPassword.Text) = Trim(txtOrigPassword.Text)) Or (RepeatedPassCheck(txtNewPassword.Text.Trim) And ConfigurationManager.AppSettings("DiffLast") = "1") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('New password must be different from last " & ConfigurationManager.AppSettings("PwdHistLimit") & " old password.!');</script>")
                Exit Sub
            End If

            'Encrypt Password
            objCrypto = New cor_Crypto.clsEncryption
            With objCrypto
                strEncryptPwd = .TripleDESEncrypt(Trim(txtNewPassword.Text))
            End With

            'Update User Table
            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_code = Trim(lblUserCodeValue.Text)
                .clsProperties.pwd = strEncryptPwd
                .PwdUpdate()
            End With

            strScript = "window.opener.location=window.opener.location;"
            strScript += "window.close();"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "anything", "<script language=javascript>" & strScript & "</script>")

        Catch ex As Exception
            ExceptionMsg("UserPwdEdit.btnSubmit_Click : " & ex.ToString)
        Finally
            objUserQuery = Nothing
            objUser = Nothing
            objCrypto = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub btnClose_Click
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim strScript As String = ""

        Try
            strScript = "window.close();"
            Page.ClientScript.RegisterStartupScript(Me.GetType, "anything", "<script language=javascript>" & strScript & "</script>")

        Catch ex As Exception
            ExceptionMsg("UserPwdEdit.btnClose_Click : " & ex.ToString)
        End Try
    End Sub


#Region "Pwd Policy"
    Sub PolicyCheck(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        e.IsValid = Portal.Policy.CheckPwd(e.Value.Trim)
    End Sub

    Private Function RepeatedPassCheck(ByVal strPassword As String) As Boolean
        Dim clsUserQuery As New adm_User.clsUserQuery
        Dim dt As DataTable = Nothing
        Dim rept As Boolean = False
        Try
            If IsNothing(dtPassLib) Then
                With clsUserQuery.clsProperties
                    .user_id = Portal.UserSession.UserID
                End With
                dt = clsUserQuery.GetUsrPassLibList
                dtPassLib = dt
            End If


            Dim objCrypto As cor_Crypto.clsEncryption
            objCrypto = New cor_Crypto.clsEncryption


            Dim dv1 As New DataView(dtPassLib, "pwd='" + objCrypto.TripleDESEncrypt(Trim(strPassword)) + "' ", "pwd", DataViewRowState.CurrentRows)
            If dv1.Count > 0 Then
                rept = True
            End If

        Catch ex As Exception
            ExceptionMsg("RepeatedPassCheck  : " & ex.ToString)
        End Try

        Return rept
    End Function
#End Region


    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
