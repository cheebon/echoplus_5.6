'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	28/04/2008
'	Purpose	    :	User Setting for FFMA
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_User_UserFFMA
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objUserQuery As adm_User.clsUserQuery
        Dim dt As DataTable
        Dim strTitle As String

        Try
            lblErr.Text = ""

            If Not IsPostBack Then
                txtSearchType.Text = Trim(Request.QueryString("search_type"))
                txtSearchValue.Text = Trim(Request.QueryString("search_value"))
                txtType.Text = Trim(Request.QueryString("type"))
                lblUserCode.Text = Trim(Request.QueryString("user_code"))

                strTitle = "FFMA Setting"

                'Call Header
                With wuc_lblheader
                    .Title = strTitle
                    .DataBind()
                    .Visible = True
                End With

                BindLsbTeam()

                'Get Details
                objUserQuery = New adm_User.clsUserQuery
                With objUserQuery
                    .clsProperties.user_code = lblUserCode.Text
                    dt = .GetUserDtl
                End With

                If dt.Rows.Count > 0 Then
                    lblLogin.Text = Trim(dt.Rows(0)("login"))
                    txtUserID.Text = Trim(dt.Rows(0)("user_id"))
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "LISTBOX"
    Private Sub BindLsbTeam()
        Dim dtTeam As DataTable = Nothing
        Dim dtSelectedTeam As DataTable = Nothing
        Dim dvTeam, dvSelectedTeam As DataView
        Dim strFilter As String = ""

        Try
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsUserFFMA As New adm_User.clsUserFFMA

            dtSelectedTeam = clsUserFFMA.GetUserTeamList(lblUserCode.Text)
            dvSelectedTeam = dtSelectedTeam.DefaultView
            lsbSelectedTeam.DataSource = dvSelectedTeam
            lsbSelectedTeam.DataTextField = "TEAM_NAME"
            lsbSelectedTeam.DataValueField = "TEAM_CODE"
            lsbSelectedTeam.DataBind()

            Dim clsCommon As New mst_Common.clsCommon
            dvTeam = clsCommon.GetTeamList.DefaultView

            If dtSelectedTeam.Rows.Count > 0 Then
                dtTeam = dvTeam.ToTable

                Dim drTeam As DataRow() = dtSelectedTeam.Select()
                strFilter = "TEAM_CODE NOT IN (" & GetFilterString(drTeam, "TEAM_CODE") & ")"

                dvTeam = New DataView(dtTeam, strFilter, "", DataViewRowState.CurrentRows)
            End If

            lsbTeam.DataSource = dvTeam
            lsbTeam.DataTextField = "TEAM_NAME"
            lsbTeam.DataValueField = "TEAM_CODE"
            lsbTeam.DataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetFilterString(ByRef drRows() As DataRow, ByVal strColumnName As String) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList

        For Each ROW As Data.DataRow In drRows
            If aryList.IndexOf(Trim(ROW(strColumnName))) < 0 Then
                sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(ROW(strColumnName)) & "'")
                aryList.Add(Trim(ROW(strColumnName)))
            End If
        Next
        Return sbString.ToString
    End Function

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next

                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function
#End Region
    
#Region "EVENT HANDLER"
    Protected Sub btnViewTray_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTray.Click
        Try
            Response.Redirect("UserList.aspx?search_type=" & Trim(txtSearchType.Text) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim clsUserFFMA As New adm_User.clsUserFFMA
            
            Dim strTeamList As String
            strTeamList = GetItemsInString(lsbSelectedTeam)

            If lblUserCode.Text <> "" Then
                clsUserFFMA.UpdateUserTeam(Trim(txtUserID.Text), Trim(lblUserCode.Text), strTeamList)
            End If

            Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('FFMA Setting is successfully updated.');</script>")
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing
        Catch ex As Exception

        End Try
    End Sub
End Class

