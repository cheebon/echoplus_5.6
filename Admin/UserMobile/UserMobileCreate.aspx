<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UserMobileCreate.aspx.vb" Inherits="Admin_UserMobile_UserMobileCreate" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Creating Mobile User</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head> 
<body class="BckgroundInsideContentLayout">
    <form id="frmUserCreate" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td>
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">
					                            <tr>
							                        <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label><asp:label id="lblMark1" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:dropdownlist id="ddlStatusID" Runat="server" CssClass="cls_dropdownlist">
									                        <asp:ListItem Value="">Select</asp:ListItem>
									                        <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
									                        <asp:ListItem Value="0">In-Active</asp:ListItem>
								                        </asp:dropdownlist>
								                        <asp:RequiredFieldValidator id="rfvStatusID" runat="server" ControlToValidate="ddlStatusID" ErrorMessage="Please select Status."
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator>
								                    </td>
						                        </tr>
						                        <tr><td>&nbsp;</td></tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblUserCode" runat="server" CssClass="cls_label_header">Sales Rep Code</asp:label><asp:label id="lblMark5" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtUserCode" MaxLength="20" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvUserCode" runat="server" ControlToValidate="txtUserCode" ErrorMessage="User Code cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
							                        <td width="20%"><asp:label id="lblUserName" runat="server" CssClass="cls_label_header">Sales Rep Name</asp:label><asp:label id="lblMark6" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtUserName" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvUserName" runat="server" ControlToValidate="txtUserName" ErrorMessage=" User Name cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>					                        
						                        <tr>
							                        <td width="20%"><asp:label id="lblPassword" runat="server" CssClass="cls_label_header">Password</asp:label><asp:label id="lblMark3" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot3" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtPassword" MaxLength="50" Runat="server" Width="110px" TextMode="Password" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                            <td width="20%"><asp:label id="lblConfirmedPassword" runat="server" CssClass="cls_label_header">Confirmed Password</asp:label><asp:label id="lblMark4" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot4" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtConfirmedPassword" MaxLength="50" Runat="server" Width="110px" TextMode="Password" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="rfvConfirmedPassword" runat="server" ControlToValidate="txtConfirmedPassword" ErrorMessage="Password cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblMobileCode" runat="server" CssClass="cls_label_header">Mobile Code</asp:label><asp:label id="Label2" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%"><asp:label id="Label3" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtMobileCode" MaxLength="50" Runat="server" Width="110px" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtMobileCode" ErrorMessage="Mobile Code cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator"></asp:RequiredFieldValidator></td>
						                            <td width="20%"><asp:label id="lblContactNo" runat="server" CssClass="cls_label_header">Contact No</asp:label><!--<asp:label id="Label10" runat="server" CssClass="cls_mandatory">*</asp:label></td>-->
							                        <td width="2%"><asp:label id="Label11" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtContactNo" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" ControlToValidate="txtContactNo" ErrorMessage="Contact no cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator" Visible="false"></asp:RequiredFieldValidator></td>
						                        </tr>
						                        <tr>
							                        <td width="20%" valign="top"><asp:label id="lblAddress" runat="server" CssClass="cls_label_header">Address</asp:label><!--<asp:label id="Label4" runat="server" CssClass="cls_mandatory">*</asp:label></td>-->
							                        <td width="2%" valign="top"><asp:label id="Label5" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtAddress" MaxLength="50" Runat="server" TextMode="MultiLine" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddress" ErrorMessage="Address cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator" Visible="false"></asp:RequiredFieldValidator></td>
						                            <td width="20%"><asp:label id="lblEmail" runat="server" CssClass="cls_label_header">Email</asp:label><!--<asp:label id="Label7" runat="server" CssClass="cls_mandatory">*</asp:label></td>-->
							                        <td width="2%"><asp:label id="Label8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td><asp:textbox id="txtEmail" MaxLength="50" Runat="server" CssClass="cls_textbox"></asp:textbox>
								                        <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email cannot be Blank !"
									                        Display="Dynamic" CssClass="cls_validator" Visible="false"></asp:RequiredFieldValidator></td>
						                        </tr>
						                        <tr>
							                        <td width="20%" style="height: 38px"><asp:label id="lblSettingCode" runat="server" CssClass="cls_label_header">Setting Code</asp:label><asp:label id="Label6" runat="server" CssClass="cls_mandatory">*</asp:label></td>
							                        <td width="2%" style="height: 38px"><asp:label id="Label9" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 38px"><asp:dropdownlist id="ddlSettingCode" Runat="server" CssClass="cls_dropdownlist">
                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                    </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvSettingCode" runat="server" ControlToValidate="ddlSettingCode"
                                                            CssClass="cls_validator" Display="Dynamic" ErrorMessage="Please select Setting."></asp:RequiredFieldValidator></td>
						                        </tr>						                        						                        			
					                        </table>
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
				                            <asp:TextBox ID="txtSearchType" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:TextBox ID="txtSearchValue" runat="server" Visible="false" CssClass="cls_textbox"></asp:TextBox>
					                        <asp:button id="btnReset" runat="server" Text="Reset" CssClass="cls_button" CausesValidation="False"></asp:button>
					                        <asp:button id="btnSave" runat="server" CssClass="cls_button" Text="Save"></asp:button>
                                            <asp:Button ID="btnClose" runat="server" CssClass="cls_button" Text="Close" CausesValidation="False"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
