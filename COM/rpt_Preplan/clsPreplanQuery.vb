'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	17/1011/2006
'	Purpose	    :	Class to build Preplan Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPreplanQuery
    Implements IDisposable
#Region "Default Variable"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.

#End Region

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPreplanQuery"
        End Get
    End Property


    ''' <summary>
    ''' </summary>
    ''' <param name="strYear"></param>
    ''' <param name="strMonth"></param>
    ''' <param name="strUserID"></param>
    ''' <param name="strGroupFields">SALESREP + SMNAME | DATE</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPreplanList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesrepList As String, ByVal strGroupFields As String) As DataTable
        'execute SPP_RPT_PREPLAN_BYDATE @USER_ID, @YEAR, @MONTH, @TEAM_CODE, @SALESREP_CODE, @GRP_FIELD
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_PREPLAN_BY_DATE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesrepList, clsDB.DataType.DBDouble, True)
                .addItem("GRP_FIELD", strGroupFields, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPreplanList :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function



    ''' <summary>
    ''' Preplan List by Customer
    ''' 
    ''' </summary>
    ''' <param name="strUserID"></param>
    ''' <param name="strYear"></param>
    ''' <param name="strMonth"></param>
    ''' <param name="strStatus"></param>
    ''' <param name="strSelectedDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetPreplanByCust(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String, ByVal strStatus As String, ByVal strSelectedDate As String) As DataTable
        '--execute SPP_RPT_PREPLAN_BYCUST @USER_ID, @YEAR, @MONTH, @TEAM_CODE, @SALESREP_CODE, @STATUS , @SELECTED_DATE
        Dim clsSalesDB As clsDB
        Try
            clsSalesDB = New clsDB
            With clsSalesDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_PREPLAN_BY_CUST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("SELECTED_DATE", strSelectedDate, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPreplanByCust :" & ex.Message))
        Finally
            clsSalesDB = Nothing
        End Try
    End Function

#Region "Default Method"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region



End Class
