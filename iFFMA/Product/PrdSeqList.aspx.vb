﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_Others_PrdSeqList
    Inherits System.Web.UI.Page

    Private dtAR As DataTable

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Index() As Integer

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.PRDSEQ)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.PRDSEQ
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                ActivateFirstTab()
                TimerControl1.Enabled = True

                'HL: 20080429 (AR)
                If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, SubModuleAction.Create) Then
                    btnAdd.Visible = False
                Else
                    btnAdd.Visible = True
                End If
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            ActivateFirstTab()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            Dim sbSelectedStr As New Text.StringBuilder

            If Not String.IsNullOrEmpty(Trim(hdDgListIndex.Value)) Then
                If dgList.Rows.Count > 0 Then
                    Dim indexList As String() = hdDgListIndex.Value.Split(New Char() {","c})
                    Dim prdList = New List(Of String)

                    For Each item In indexList
                        Dim row As Object = dgList.DataKeys(item)
                        prdList.Add(row.Values("PRD_CODE").ToString())
                    Next

                    Dim clsPrdSeq As New mst_others.clsPrdSeq
                    Dim strPrdCode As String
                    strPrdCode = String.Join(",", prdList.ToArray())

                    clsPrdSeq.DeletePrdSeq(strPrdCode)

                End If
            End If

            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        With (wuc_PrdSeqPop)
            .IsEdit = False
            .LoadInsertPanel()
            .Show()
        End With
    End Sub

    Protected Sub btnCustomerTargetProductPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSeqPop.SaveButton_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsPrdSeq As New mst_others.clsPrdSeq
            With (wuc_toolbar)
                DT = clsPrdSeq.GetPrdSeqList(.SearchType, .SearchValue)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With


            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function CreateBtnColumn(ByVal action As String, ByVal dtToBind As DataTable) As CommandField
        Dim dgBtnColumn As New CommandField

        Select Case action
            Case SubModuleAction.Edit
                With dgBtnColumn
                    .HeaderStyle.Width = "50"
                    .ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    .HeaderText = "Edit"
                End With

                If dtToBind.Rows.Count > 0 Then
                    With dgBtnColumn
                        .ButtonType = ButtonType.Link
                        .ShowEditButton = True
                        .EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                        .Visible = True
                    End With
                End If
            Case SubModuleAction.Delete
                With dgBtnColumn
                    .HeaderStyle.Width = "50"
                    .ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    .HeaderText = "Delete"
                End With

                If dtToBind.Rows.Count > 0 Then
                    With dgBtnColumn
                        .ButtonType = ButtonType.Link
                        .ShowDeleteButton = True
                        .DeleteText = "<img src='../../images/ico_delete.gif' alt='Delete' border='0' height='18px' width='18px'/>"
                        .Visible = True
                    End With
                End If
        End Select

        Return IIf(dgBtnColumn IsNot Nothing, dgBtnColumn, Nothing)
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgList.Columns.Clear()

            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, SubModuleAction.Delete) Then
                    aryDataItem.Add("chkSelect")
                    Index = 0
                    dgBtnColumn.HeaderStyle.Width = "25"
                    dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    dgList.Columns.Add(dgBtnColumn)

                    'dgList.Columns.Add(CreateBtnColumn(SubModuleAction.Delete, dtToBind))
                    'aryDataItem.Add("BTN_DELETE")
                End If
                If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRDSEQ, SubModuleAction.Edit) Then
                    dgList.Columns.Add(CreateBtnColumn(SubModuleAction.Edit, dtToBind))
                    aryDataItem.Add("BTN_EDIT")
                End If

            End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_CustomerTgtPrdList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        With dgColumn
                            .ReadOnly = True

                            .HeaderStyle.VerticalAlign = VerticalAlign.Middle
                            .ItemStyle.VerticalAlign = VerticalAlign.Middle
                            .HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                            Dim strFormatString As String = CF_CustomerTgtPrdList.GetOutputFormatString(ColumnName)
                            If String.IsNullOrEmpty(strFormatString) = False Then
                                .DataFormatString = strFormatString
                                .HtmlEncode = False
                            End If
                            .ItemStyle.HorizontalAlign = CF_CustomerTgtPrdList.ColumnStyle(ColumnName).HorizontalAlign
                            .ItemStyle.Wrap = CF_CustomerTgtPrdList.ColumnStyle(ColumnName).Wrap

                            .HeaderText = CF_CustomerTgtPrdList.GetDisplayColumnName(ColumnName)
                            .DataField = ColumnName
                            .SortExpression = ColumnName
                        End With
                        
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUST_TEAM_CLASS, SubModuleAction.Delete) Then
                If e.Row.RowType = DataControlRowType.Header AndAlso Master_Row_Count > 0 Then
                    Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                    If chkAll Is Nothing Then
                        chkAll = New CheckBox
                        chkAll.ID = "chkSelectAll"
                        e.Row.Cells(0).Controls.Add(chkAll)
                    End If
                    chkAll.Attributes.Add("onClick", "SelectAllRows(this," + Master_Row_Count.ToString + ")")
                    chkAll.ToolTip = "Click to toggle the selection of ALL rows"
                ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                    Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                    If chk Is Nothing Then
                        chk = New CheckBox
                        chk.ID = "chkSelect"
                        e.Row.Cells(0).Controls.Add(chk)
                    End If
                    chk.Attributes.Add("onClick", "SelectRow(this, " + Index.ToString + ")")
                    Index += 1
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub dgAssignList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
    '    Try
    '        Select Case e.Row.RowType
    '            Case DataControlRowType.DataRow
    '                If Master_Row_Count > 0 Then
    '                    Dim btnDelete As LinkButton = CType(e.Row.Cells(aryDataItem.IndexOf("BTN_DELETE")).Controls(0), LinkButton)
    '                    Dim strDeleteMsg As String = Nothing

    '                    If btnDelete IsNot Nothing Then
    '                        strDeleteMsg = "The selected item will be removed from the Product Sequence list. \nAre you sure you want to delete?"
    '                    End If
    '                    btnDelete.OnClientClick = "if (!confirm('" + strDeleteMsg + "')) { return false; }"
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub dgAssignList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        Try
            Dim clsPrdSeq As New mst_others.clsPrdSeq

            Dim strPrdCode As String
            strPrdCode = Report.GetRowData(sender.datakeys(e.RowIndex).item("PRD_CODE"))  'IIf( Is Nothing, "", sender.datakeys(e.RowIndex).item("PRD_CODE"))
            clsPrdSeq.DeletePrdSeq(strPrdCode)

            RenewDataBind()

            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            Dim strPrdCode, strPrduct, strSeq As String

            Dim row As Object = sender.datakeys(e.NewEditIndex)
            strPrdCode = Report.GetRowData(row.item("PRD_CODE"))
            strPrduct = Report.GetRowData(row.item("PRODUCT"))
            strSeq = Report.GetRowData(row.item("SEQ"))
            With (wuc_PrdSeqPop)
                .IsEdit = True
                .PrdCode = strPrdCode
                .Seq = strSeq
                .Product = strPrduct
                .LoadEditPanel()
                .Show()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Sub ActivateFirstTab()
        tcResult.ActiveTabIndex = 0
        TabPanel1.HeaderText = "Product Sequence List"
        pnlList.Visible = True
        'wuc_toolbar.show()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True)

            Dim aryDgList As New ArrayList
            aryDgList.Add(dgList)
            'If TabPanel2.Visible Then
            '    aryDgList.Add(wuc_AssignFieldForce.GetdgAssignList())
            '    aryDgList.Add(wuc_AssignSupplier.GetdgAssignList())
            '    aryDgList.Add(wuc_AssignPrdGrp.GetdgAssignList())
            '    If GetAccessRight(ModuleID.FFMA, SubModuleType.SALESTEAM_SUB, "'1'") Then
            '        aryDgList.Add(wuc_AssignPrdV2.GetdgAssignList())
            '    Else
            '        aryDgList.Add(wuc_AssignPrd.GetdgAssignList())
            '    End If
            'End If

            wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
            'If TabPanel2.Visible Then
            '    RefreshAllTab()
            'End If

        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Class CF_CustomerTgtPrdList
        Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
            Dim strFieldName As String = ""
            Select Case ColumnName.ToUpper
                Case "CREATED_DATE"
                    strFieldName = "Created Date"
                Case "CREATED_BY"
                    strFieldName = "Created By"
                Case Else
                    strFieldName = Report.GetDisplayColumnName(ColumnName)
            End Select

            Return strFieldName
        End Function

        Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
            Try
                Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
                strColumnName = strColumnName.ToUpper

                If strColumnName = "ID" Or strColumnName = "PRD_CODE" Then
                    FCT = FieldColumntype.InvisibleColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If
                Return FCT
            Catch ex As Exception

            End Try
        End Function

        Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
            Dim strFormatString As String = ""
            Try
                strColumnName = strColumnName.ToUpper

                If strColumnName Like "*DATE" Then
                    strFormatString = "{0:yyyy-MM-dd}"
                ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                    strFormatString = "{0:#,0}"
                ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                    strFormatString = "{0:#,0.00}"
                Else
                    strFormatString = ""
                End If

            Catch ex As Exception
            End Try

            Return strFormatString
        End Function

        Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
            Dim CS As New ColumnStyle
            Try
                With CS
                    Dim strColumnName As String = ColumnName.ToUpper
                    .FormatString = GetOutputFormatString(ColumnName)

                    If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                    OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                    OrElse strColumnName Like "TIME_*" Then
                        .HorizontalAlign = HorizontalAlign.Center
                        .Wrap = False
                    ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                        .HorizontalAlign = HorizontalAlign.Right
                    Else
                        .HorizontalAlign = HorizontalAlign.Left
                    End If

                End With

            Catch ex As Exception

            End Try
            Return CS
        End Function
    End Class
End Class
