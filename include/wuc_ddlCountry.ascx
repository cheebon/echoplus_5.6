<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlCountry.ascx.vb" Inherits="include_wuc_ddlCountry" %>
<asp:DropDownList id="ddlCountryID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvCountryID" ControlToValidate="ddlCountryID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select Country."></asp:CompareValidator>
