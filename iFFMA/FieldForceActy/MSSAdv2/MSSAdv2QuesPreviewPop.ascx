<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MSSAdv2QuesPreviewPop.ascx.vb" Inherits="iFFMA_FieldForceActy_MSSAdv2QuesPreviewPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtDate" Src="~/include/wuc_txtDate.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:95%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:left; width:5%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                
                <fieldset style="padding-left: 10px; width: 100%; padding-bottom: 5px;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                </fieldset>
                
                <fieldset runat="server" id="S1" style="padding-left:10px; width:100%; height:50px; padding-bottom:5px;">
                    <span style="float:left; width:100%; padding-top: 2px; padding-bottom: 2px;">
                        <asp:Label ID="lblS1" runat="server" CssClass="cls_label_header" />
                    </span>
                    
                    <asp:TextBox ID="txtS1" runat="server" CssClass="cls_textbox" visible="false" />
                    <asp:Dropdownlist ID="ddlS1" runat="server" CssClass="cls_dropdownlist" visible="false" />
                    <customToolkit:wuc_txtDate ID="txtDateS1" runat="server" RequiredValidation="false" RequiredValidationGroup="Search" DateTimeFormatString="yyyy-MM-dd" visible="false" />
                    <asp:RadioButtonList ID="rdbListS1" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal" Visible="false" >
                        <asp:ListItem Value="1" Text="True" Selected="True"/>
                        <asp:ListItem Value="0" Text="False"/>
                    </asp:RadioButtonList>
                    <ccGV:clsGridView ID="dgTextS1" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                        <Columns>
                            <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                <itemstyle horizontalalign="Left" />
                            </asp:BoundField>  
                            <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center">
                                <itemtemplate>
                                    <asp:TextBox ID="txtColumnS1" runat="server" class="cls_textbox"/>
                                </itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ccGV:clsGridView>
                    <ccGV:clsGridView ID="dgSelectS1" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                                <itemtemplate>
                                    <asp:checkbox ID="chkSelectS1" runat="server" class="cls_checkbox"/>
                                </itemtemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                <itemstyle horizontalalign="Left" />
                            </asp:BoundField>  
                        </Columns>
                    </ccGV:clsGridView>
                </fieldset>
                
                <fieldset runat="server" id="S2" style="padding-left:10px; width:100%; height:50px; padding-bottom:5px;">
                    <span style="float:left; width:100%; padding-top: 2px; padding-bottom: 2px;">
                        <asp:Label ID="lblS2" runat="server" CssClass="cls_label_header" />
                    </span>
                    
                    <asp:TextBox ID="txtS2" runat="server" CssClass="cls_textbox" visible="false" />
                    <asp:Dropdownlist ID="ddlS2" runat="server" CssClass="cls_dropdownlist" visible="false" />
                    <customToolkit:wuc_txtDate ID="txtDateS2" runat="server" RequiredValidation="false" RequiredValidationGroup="Search" DateTimeFormatString="yyyy-MM-dd" visible="false" />
                    <asp:RadioButtonList ID="rdbListS2" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal" Visible="false" >
                        <asp:ListItem Value="1" Text="True" Selected="True"/>
                        <asp:ListItem Value="0" Text="False"/>
                    </asp:RadioButtonList>
                    <ccGV:clsGridView ID="dgTextS2" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                        <Columns>
                            <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                <itemstyle horizontalalign="Left" />
                            </asp:BoundField>  
                            <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center">
                                <itemtemplate>
                                    <asp:TextBox ID="txtColumnS2" runat="server" class="cls_textbox"/>
                                </itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ccGV:clsGridView>
                    <ccGV:clsGridView ID="dgSelectS2" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                                <itemtemplate>
                                    <asp:checkbox ID="chkSelectS2" runat="server" class="cls_checkbox"/>
                                </itemtemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                <itemstyle horizontalalign="Left" />
                            </asp:BoundField>  
                        </Columns>
                    </ccGV:clsGridView>
                </fieldset>  
                
                <fieldset runat="server" id="S3" style="padding-left:10px; width:100%; height:50px; padding-bottom:5px;">
                    <span style="float:left; width:100%; padding-top: 2px; padding-bottom: 2px;">
                        <asp:Label ID="lblS3" runat="server" CssClass="cls_label_header" />
                    </span>
                    
                    <asp:TextBox ID="txtS3" runat="server" CssClass="cls_textbox" visible="false" />
                    <asp:Dropdownlist ID="ddlS3" runat="server" CssClass="cls_dropdownlist" visible="false" />
                    <customToolkit:wuc_txtDate ID="txtDateS3" runat="server" RequiredValidation="false" RequiredValidationGroup="Search" DateTimeFormatString="yyyy-MM-dd" visible="false" />
                    <asp:RadioButtonList ID="rdbListS3" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal" Visible="false" >
                        <asp:ListItem Value="1" Text="True" Selected="True"/>
                        <asp:ListItem Value="0" Text="False"/>
                    </asp:RadioButtonList>
                    <ccGV:clsGridView ID="dgTextS3" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                        <Columns>
                            <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                <itemstyle horizontalalign="Left" />
                            </asp:BoundField>  
                            <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center">
                                <itemtemplate>
                                    <asp:TextBox ID="txtColumnS3" runat="server" class="cls_textbox"/>
                                </itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ccGV:clsGridView>
                    <ccGV:clsGridView ID="dgSelectS3" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                        Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                                <itemtemplate>
                                    <asp:checkbox ID="chkSelectS3" runat="server" class="cls_checkbox"/>
                                </itemtemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                <itemstyle horizontalalign="Left" />
                            </asp:BoundField> 
                        </Columns>
                    </ccGV:clsGridView>
                </fieldset> 
                
                <fieldset runat="server" id="S4" style="padding-left:10px; width:100%; height:50px; padding-bottom:5px;">
                    <span style="float:left; width:100%; padding-top: 2px; padding-bottom: 2px;">
                        <asp:Label ID="lblS4" runat="server" CssClass="cls_label_header" />
                    </span>
                    
                    <asp:TextBox ID="txtS4" runat="server" CssClass="cls_textbox" visible="false" />
                    <asp:Dropdownlist ID="ddlS4" runat="server" CssClass="cls_dropdownlist" visible="false" />
                    <customToolkit:wuc_txtDate ID="txtDateS4" runat="server" RequiredValidation="false" RequiredValidationGroup="Search" DateTimeFormatString="yyyy-MM-dd" visible="false" />
                    <asp:RadioButtonList ID="rdbListS4" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal" Visible="false" >
                        <asp:ListItem Value="1" Text="True" Selected="True"/>
                        <asp:ListItem Value="0" Text="False"/>
                    </asp:RadioButtonList>
                </fieldset>                 
                
                <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
                <asp:HiddenField ID="hdQuesCode" runat="server" Value="" />
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>