﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SFMSActivityList.aspx.vb" Inherits="iFFMR_Customize_SFMSEnquiry_SFMSActivityList" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../../../include/layout.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MessagerRelative.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/ColorPicker.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/MapIconMaker/mapiconmaker2.js" type="text/javascript"></script>
    <script src="../../../../include/jQuery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../../../include/jQuery/Jquery_Query.js" type="text/javascript"></script>
    <script src="../../../../include/Gmap/Library/ModalPopup/jModalPopup.js" type="text/javascript"></script>      
    <script src="../../../../include/layout.js" type="text/javascript"></script>
      <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeQ25EsNUuyrH2MjtiYw17rxGPpAdIwOo&sensor=true" type="text/javascript"> </script>
    <script type='text/javascript'>
        /// Parameters to play with
        /// Currently hard code color first. 
        var ClassA = "#bc0000";
        var ClassB = "#345bec";
        var ClassC = "#c88c18";
        var ClassD = "#3bba04";
        var Unclassified = "#999a9d";

        //Global variables
        var map;
        var Guid;
        var root; //store app root path
        var marker;
        var markers = [];
        var ValidCoordinates = []; //Store valid coordinate

        /***********************************************************
        / Initialiazer
        /
        ************************************************************/
        function Initialize() {

           // if (GBrowserIsCompatible()) {
              //  map = new GMap2(document.getElementById('map_canvas'));
            var mapOptions = {
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                panControl: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE

                },
                mapTypeControl: true
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
                //QueryLocation();

              //  AddControl();


          //  }
        }
        google.maps.event.addDomListener(window, 'load', Initialize);
        // Add a marker to the map and push to the array.
        function addMarker(Latitude, Longitude) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(Latitude, Longitude),
                map: map,
                draggable: true
            });
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the overlays from the map, but keeps them in the array.
        function clearOverlays() {
            setAllMap(null);
        }

        // Shows any overlays currently in the array.
        function showOverlays() {
            setAllMap(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteOverlays() {
            clearOverlays();
        }
        function AddControl() {
            var mapTypeControl = new GMapTypeControl();
            var LargeMapControl3D = new GLargeMapControl3D();
            var bottomLeft = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(10, 10));
            var bottomRight = new GControlPosition(G_ANCHOR_BOTTOM_RIGHT, new GSize(10, 10));

            map.addControl(mapTypeControl, bottomRight);
            map.addControl(LargeMapControl3D, bottomLeft);
            map.enableScrollWheelZoom();
        }

        function QueryLocation() {
            //map.setCenter(new google.maps.LatLng(25.324167, 109.160156), 3);
            ws_PrincipalLoc.ReturnCustomerGPS(onSuccessPrincipalLocList);
        }
        function onSuccessPrincipalLocList(result) {
            if (result) {
                if (result.Total > 0) {


                    Default_Latitude = result.Rows[0].LATITUDE;
                    Default_Longitude = result.Rows[0].LONGITUDE;
                    Default_Zoomlevel = result.Rows[0].ZOOM_LEVEL;


                    SetMapDefaultPoint();

                }
            }
        }
        function SetMapDefaultPoint() {
            if (map) {

                var point = new google.maps.LatLng(Default_Latitude, Default_Longitude);
                map.setCenter(point, parseInt(Default_Zoomlevel));

            }
        }

        /***********************************************************
        / Method to plot customer.
        /
        ************************************************************/
        function PlotCustomer(CustCode) {

            ws_CustLoc.ReturnCustomerProfileByCode(CustCode,
                                            onSuccessLocateCustomer, onFailLocateCustomer);

            ShowMessage("Loading map data.");
        }

        function onSuccessLocateCustomer(result) {
            if (result) {
                if (result.Total > 0) {
                    HideMessage();
                    PlotMap(result);
                    SetCenterMap();

                }
                else //Shit happens
                {

                    ShowMessage("No Record Returned.");

                    QueryLocation();
                }
            }
        }

        function onFailLocateCustomer(ex) {
            //If webservice call fail
            ShowMessage("Error occurred when retrieving data." + ex.get_message());
        }

        function PlotMap(result) {
            var count = 0;
           deleteOverlays();
            WriteRecordCount(count);

            for (var i = 0; i < result.Total; ++i) {
                //Check coordinate contains value
                if ((result.Rows[i].LATITUDE != undefined && result.Rows[i].LATITUDE != "") && (result.Rows[i].LONGITUDE != undefined && result.Rows[i].LONGITUDE != "")) {
                    //check is numeric
                    if (!isNaN(result.Rows[i].LATITUDE) && !isNaN(result.Rows[i].LONGITUDE)) {
                        var icon = CreateIcon(result.Rows[i].CUST_CLASS);

                         marker = CreateMarker(result.Rows[i].CUST_CODE, result.Rows[i].CUST_NAME,
                                                  result.Rows[i].CONT_CODE, result.Rows[i].CONT_NAME,
                                                  result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE,
                                                  result.Rows[i].ADDRESS, result.Rows[i].DISTRICT,
                                                  result.Rows[i].CUST_GRP_NAME, result.Rows[i].CUST_TYPE,
                                                  result.Rows[i].MTD_SALES, result.Rows[i].YTD_SALES, result.Rows[i].NO_SKU,
                                                  result.Rows[i].CUST_CLASS, result.Rows[i].TXN_NO,
                                                  result.Rows[i].TXN_DATE, result.Rows[i].TITLE_CODE,
                                                  result.Rows[i].TITLE_NAME, result.Rows[i].IMG_LOC, result.Rows[i].CREDITLIMIT, result.Rows[i].OUTBAL, icon);

                        marker.setMap(map);
                        WriteRecordCount(++count); //Display records plot on map

                        ValidCoordinates.push(new google.maps.LatLng(result.Rows[i].LATITUDE, result.Rows[i].LONGITUDE));
                    }
                    else
                    { ShowMessage("Certain customers have invalid coordinates."); }
                }
                else
                { ShowMessage("Certain customers does not have coordinates information."); }
            }
        }
        function WriteRecordCount(count) {
            $("#spnCount").text(count);
        }
        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateIcon(Class) {
            var Icon;

            switch (Class) {
                case "A":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassA, label: 'A' });
                    break;
                case "B":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassB, label: 'B' });
                    break;
                case "C":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassC, label: 'C' });
                    break;
                case "D":
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: ClassD, label: 'D' });
                    break;
                default:
                    Icon = MapIconMaker.createLabeledMarkerIcon({ width: 32, height: 32, primaryColor: Unclassified, label: 'O' });
                    break;
            }

            return Icon;
        }

        /*****************************************************************
        / Create Marker Helper Class
        ******************************************************************/
        function CreateMarker(CustCode, CustName, ContCode, ContName, Latitude, Longitude,
                            Address, District, CustGrpName, CustType,
                            MtdSales, YtdSales, NoSKU,
                            Class, TxnNo, TxnDate, TitleCode, TitleName, ImgLoc, CreditLimit, OutBal, Icon) {
            // marker = new GMarker(new google.maps.LatLng(Latitude, Longitude), Icon);
             marker = new google.maps.Marker({
                 position: new google.maps.LatLng(Latitude, Longitude),
                 map: map,
                 icon: Icon
             });
            google.maps.event.addListener(marker, "click", function() {
                var str = "<div class=\"cls_label\">";

                str += "<span class='map_header_text'>Customer:</span> " + CustName + "<br />";
                str += "<span class='map_header_text'>Contact:</span> " + ContName + "<br />";
                str += "<span class='map_header_text'>Address:</span> " + Address + "<br />";
                str += "<span class='map_header_text'>District:</span> " + District + "<br />";
                str += "<span class='map_header_text'>Customer Group:</span> " + CustGrpName + "<br />";
                str += "<span class='map_header_text'>Customer Class:</span> " + GetClassName(Class) + "<br />";
                str += "<span class='map_header_text'>Customer Type:</span> " + CustType + "<br />";
                str += "<span class='map_header_text'>Customer MTD Sales:</span> " + MtdSales + "<br />";
                str += "<span class='map_header_text'>Customer YTD Sales:</span> " + YtdSales + "<br />";
                str += "<span class='map_header_text'>Customer No of SKU:</span> " + NoSKU + "<br />";
                str += "<span class='map_header_text'>Credit Limit:</span> " + CreditLimit + "<br />";
                str += "<span class='map_header_text'>Outstanding Balance:</span> " + OutBal + "<br />";
                //                str += "<span class='map_header_text'>Last MSS Date:</span> " + TxnDate + "<br />";
                //                str += "<span class='map_header_text'>Last Title Name:</span> " + TitleName + "<br />";
                //str += "<a href='#' onclick='PopImage(\"" + root + "" + ImgLoc + "\", \"" + CustName + "\", \"\");'>Outlet Photo</a><br />";
                str += "<a href='#' onclick='PopImage(\"" + root + "" + ImgLoc + "\", \"" + CustName + "\", \"" + Address + "\",  \"" + CustCode + "\" );'>Outlet Photo</a><br />";

                str += "</div>";

                /// Eye Candy Code. Pan marker to center then only show InfoWindow
                /// Just calling panning and InfoWindow sequentially cannot achieve such effects
//                var moveEnd = google.maps.event.addListener(map, "moveend", function() {
                    marker.openInfoWindowHtml(str);
//                    google.maps.event.removeListener(moveEnd);
//                });

//                map.panTo(marker.getLatLng());
                //////////////////////////////////////////////////////////////////////////////////
            }
            );

            return marker;
        }
        function GetClassName(Class) {
            switch (Class) {
                case "A":
                    return "A";
                    break;
                case "B":
                    return "B";
                    break;
                case "C":
                    return "C";
                    break;
                case "D":
                    return "D";
                    break;
                default:
                    return "OTH";
                    break;
            }
        }
        function SetCenterMap() {
            if (ValidCoordinates != undefined && ValidCoordinates.length > 0) {
                var iCenter = parseInt(ValidCoordinates.length / 2);

                map.setCenter(ValidCoordinates[iCenter], 10);
                map.setZoom(10);
            }
        }

        /***********************************************
        / Show cust image
        ***********************************************/
        function PopImage(ImgLoc, CustName, Address, CustCode) {
          // mp_ToggleModalPopup("pnlPictureDiv", "divModalMask2", "on");

           BindCust(CustName, Address, CustCode);
           LoadImage(ImgLoc); 
        }
        function BindCust(CustName, Address, CustCode) {
//            $("#PicDiv_CustName").text(CustName);
//            $("#PicDiv_Address").text(Address);

            $("#hfPicCustCode").val(CustCode);
            $("#hfPicCustName").val(CustName);
            $("#hfPicCustAdd").val(CustName);

        }
        function LoadImage(ImgLoc) {
            //$("#CustImg").error(Image_OnErrorHide);
//            resetCustImg();

//            $("#CustImg").attr({
//                src: ImgLoc,
//                title: "",
//                alt: "Customer Image"
//            });
             document.all('btnRefreshPic').click();

        }
        /***********************************************
        / Hide broken image
        ***********************************************/
        function Image_OnErrorHide() {
            $("#CustImg").hide();
            $("#ImgLoadFailMsg").show();
        }
        function resetCustImg() {
            $("#CustImg").show();
            $("#ImgLoadFailMsg").hide();

            $("#CustImg").attr({
                src: "../../../../images/indicator.gif",
                title: "",
                alt: "Loading"
            });

            $("#PicDiv_CustName").text("");
            $("#PicDiv_Address").text("");
        }

//        $(document).ready(function() {
//            if ($.query.get('custcode')) {
//                if ($.query.get('custcode') != '') {
//                    PlotCustomer($.query.get('custcode'));
//                }
//            }

//        });

         $(document).ready(function () {
            ShowVisit();

            if ($.query.get('custcode')) {
                if ($.query.get('custcode') != '') {
                    PlotCustomer($.query.get('custcode'));
                }
            }
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {

                ShowVisit();
                if ($.query.get('custcode')) {
                    if ($.query.get('custcode') != '') {
                        PlotCustomer($.query.get('custcode'));
                    }
                }

            }
        });

        function ShowVisit() {
            var value = $('#hfVisitStatus').val()
            if (value == "") { $('#Visit').hide(); }
            else {
                if (value == "Show") { $('#Visit').show(); }
                else { $('#Visit').hide(); };
            };
        }

        function SetVisit(SetVis) {
            if (SetVis == 'ForceHide') { $('#hfVisitStatus').val('Hide'); }
            else {
                var value = $('#hfVisitStatus').val();
                if (value == 'Hide') { $('#hfVisitStatus').val('Show'); }
                else { $('#hfVisitStatus').val('Hide'); }
            }
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
            this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
            return this;
        }
    </script>
    
    <style type="text/css">
        .answerPane
        {
            FONT-WEIGHT: normal;
            FONT-SIZE:8pt;
            COLOR: Black;   
            FONT-FAMILY:  "Frutiger LT 45 Light", Tahoma,Verdana, Arial;
        }
        .Header
        {
            background-color: #5d7b9d;
            COLOR: White;
             border-bottom-color:Black; 
            border-top-color:Black; 
            border-left-color:black; 
            border-right-color:black;
            text-align: center; 
            font-weight: bold; 
            font-size: larger;
            }    
        
        .Row
        {
            border-bottom-color:Black; 
            border-top-color:Black; 
            border-left-color:black; 
            border-right-color:black;
            vertical-align: top
            }
       
        .ctrlPanel {border: solid 1px #0099ff; width:99%; padding: 5px 0px 5px 5px}
    </style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('ContentBar');HideElement('MapBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe');Initialize();" >
    <form id="form1" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release">
            <Services>
           <asp:ServiceReference
           path="~/DataServices/ws_PrincipalLoc.asmx" />
            <asp:ServiceReference
           path="~/DataServices/ws_CustLoc.asmx" />
           </Services>
            </ajaxToolkit:ToolkitScriptManager>
    
    
    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
    
    <div style="width:90%;border: solid 1px #ffffff; padding: 1px; margin-left: 10px; margin-right: 10px; margin-top:10px">
        <div class="bckgroundTitleBar" style="width:99%">
            <span class="cls_label" style="float:left"><b>Customer Field Activity Enquiry Details</b></span>
            <input runat="server" id="btnBack" type="button" class="cls_button" style="float:right; margin-right:10px"  value="Back" onclick="ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');" />
        </div>    
        <div class="ctrlPanel">
            <table border="0">
                <tr>
                    <td class="cls_label_header" style="width:100px">Txn No:</td><td class="cls_label" style="width:300px"><asp:Label ID="lblTxnNo" runat="server" Text=""></asp:Label></td>
                    <td class="cls_label_header" style="width:100px">Txn Date:</td><td class="cls_label"><asp:Label ID="lblTxnDate" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="cls_label_header">Customer:</td><td class="cls_label"><asp:Label ID="lblCustomer" runat="server" Text=""></asp:Label></td>
                    <td class="cls_label_header">Salesrep:</td><td class="cls_label"><asp:Label ID="lblSalesrep" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    
                    
                </tr>
            </table>
        </div>
    </div>
    <br />

    
    <div style="width:98%; margin-left: 10px;" class="answerPane">
        <asp:Repeater ID="rptrAnswer" runat="server">
            <HeaderTemplate>
                <table border="1" cellpadding="2" cellspacing="0" width="800px" rules="all" style="border: solid 1px black; border-collapse:collapse">
                <tr class="Header">
                    <td class="Row">Category</td>
                    <td class="Row">Sub Category</td>
                    <td class="Row">Remarks</td>
                    <td class="Row">Quantity</td>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <%#GetGroup(Container.DataItem("CAT_CODE").ToString, Container.DataItem("CAT_NAME").ToString, Container.DataItem("GROUP_COUNT").ToString)%>
                               
                    <td class="Row">
                        <%#Container.DataItem("SUB_CAT_CODE").ToString%>. <%#Container.DataItem("SUB_CAT_NAME").ToString%>
                    </td>
                    <td class="Row">
                         <%#Server.HtmlDecode(Container.DataItem("REMARKS").ToString)%>
                    </td>
                     <td class="Row">
                         <%#Server.HtmlDecode(Container.DataItem("QTY").ToString)%>
                    </td>
                   </tr>
            </ItemTemplate>  
            <FooterTemplate>
                </table>
            </FooterTemplate>          
        </asp:Repeater>
    </div>
    <br />
    <div style="width:98%; margin-left: 10px;" class="answerPane">
        <span class="cls_label_header" style="text-decoration:underline">Customer Map Preview</span><br /><br />
        <div style="float:left; position:relative">
        <div style="width:593px; background-color:#e3e3e3;padding-left:5px;border-left:solid 1px #989898;border-right:solid 1px #989898;border-top:solid 1px #989898"><span class="cls_label" id="spnCount" style="font-weight:bold">0</span><span class="cls_label" style="font-weight:bold"> records plot on map.</span></div>
        <div id="map_canvas" style="width: 600px; height: 450px; padding: 0px 0px 0px 0px;z-index:20">
        <%--<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=ABQIAAAAviZX_5ANQLbn5c6dZmkdkRSSF4RoMyN7orcJSVRUD19sX6pe3hQRouH06-TjWCldY1M8qE45jNoHDA" type="text/javascript"></script>--%>
     <%--   <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&amp;key=<%= GoogleMap.Key %>" type="text/javascript"></script>--%>
        </div>
        <div id="Message" class="cls_label" style="width:594px; display:none; z-index:30; position:absolute; padding:3px; background:#3C3C3C; color: #ffffff"></div>
    </div>
    </div>
    <asp:HiddenField ID="hfPicCustCode" runat="server" />
    <asp:HiddenField ID="hfPicCustName" runat="server" />
    <asp:HiddenField ID="hfPicCustAdd" runat="server" />
    <asp:UpdatePanel ID="UpdateVisit" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="hfVisitStatus" runat="server" Value="Hide" />
            <div id="Visit" style="width: 50%; float: right; position: absolute; display: none; z-index: 9100"
                class="modalPopup">
                <div class="shadow">
                    <div class="content" style="width: 100%; height: 100%;">
                        <fieldset>
                            <div id="VisitPnl" style="width: 100%;">
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <img src="../../../../images/ico_down.jpg" alt='' id='img1' />
                                            <asp:Label ID="lblVistCustDesc" runat="server" Text="Photo" CssClass="cls_label_header"
                                                Style="color: Black"></asp:Label>
                                        </td>
                                        <td align="right">
                                            <img alt="Close" src="../../../../images/ico_close.gif" onclick="SetVisit('ForceHide');ShowVisit();"
                                                height="25px" width="25px" style="cursor: pointer;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="ContainerDivBlue">
                                <div id="VisitContent" style="width: 100%;">
                                    <asp:HiddenField ID="hfVisitFilter" runat="server" />
                                    <div id="dVisitFilter" style="width: 98%;">
                                        <span class="cls_label_header">Customer:
                                            <asp:Label ID="lblCustName" runat="server" Text="" CssClass="cls_label"></asp:Label><br />
                                            <br />
                                          <asp:Label ID="lblImgLoadFailMsg" runat="server" Text=" Unable to load customer picture. This can be due to no image has been uploaded for
                                                this customer, or the image file has been removed." CssClass="cls_label" Visible="false" /> 
                                            <asp:Button ID="btnRefreshPic" runat="server" Text="Refresh Pic" Style="display: none" />
                                            <asp:Repeater ID="rpCust" runat="server">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hfCustCode" runat="server" Value='<%# Eval("CUST_CODE") %>' />
                                                    <asp:HiddenField ID="hfCustName" runat="server" Value='<%# Eval("CUST_NAME") %>' />
                                                    <asp:HiddenField ID="hfImgLoc" runat="server" Value='<%# Eval("IMG_LOC") %>' />
                                                    <asp:Image ID="imgCust" runat="server" ImageUrl="../../../../images/indicator.gif"  
                                                        Height="150px" />
                                                    </td> </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            
                                    </div>
                                    <asp:HiddenField ID="hfVisit" runat="server" />
                                </div>
                            </div>
                            </span>
                        </fieldset>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<div id="pnlPictureDiv" class="modalPopup" style="z-index:9001;display: none; padding:10px; width:700px; height:450px; overflow:auto; position:absolute;"> 
        <img alt="Close" src="../../../../images/ico_close.gif" OnClick="mp_ToggleModalPopup('pnlPictureDiv','divModalMask2', 'off');" style="float:right;margin-right:5px"/>
        <br />
        <span class="cls_label_header">Customer: </span><span id="PicDiv_CustName" class="cls_label"></span><br />
        <br />
        
        <div id="ImgLoadFailMsg" class="cls_label" style="display:none">Unable to load customer picture. This can be due to no image has been uploaded for this customer, or the image file has been removed. </div>
        <img id="CustImg" src="../../../../images/indicator.gif" alt='Loading' onerror='Image_OnErrorHide();' />
    </div>--%>
    <div id="divModalMask2" style="position:absolute; left:0; top:0; z-index: 8000; display:none; background-color:#fff"></div>
    </form>
</body>
</html>
