﻿Option Explicit On

Imports System.Data
Imports adm_SuperAdmin
Partial Class Admin_Root_DFPrincipal_dfPrincipalDtl
    Inherits System.Web.UI.Page
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property TeamCode() As String
        Get
            Return CStr(ViewState("TeamCode"))
        End Get
        Set(ByVal value As String)
            ViewState("TeamCode") = value
        End Set
    End Property

    Public ReadOnly Property PageName() As String
        Get
            Return "Data Files Principal List Dtl"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            wuc_lblHeader.Title = "Data Files Principal Dtl"
            If Not IsPostBack Then
                LoadSalesTeamDDL()
                LoadCountryDDL()
                LoadDFDDL()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


#Region "DGLIST"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDatabinding()
    End Sub

    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False, Optional ByVal isReference As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            If isReference = True Then
                dtCurrentTable = GetRecListRef()
            Else
                dtCurrentTable = GetRecList()
            End If


            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    Master_Row_Count = 0
                    btnSave.Visible = False
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                    TeamCode() = ddlTeam.SelectedValue
                    btnSave.Visible = True
                End If
            End If

            dgList_Init(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                '.PageSize = intPageSize
                .DataBind()
                .AllowSorting = IIf(isExport, False, IIf(dgList.Rows.Count > 0, True, False))
            End With

            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    .CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    '.RowCount = dvCurrentView.Count
            '    .Visible = IIf(dgList.Rows.Count > 0, True, False)
            'End With

            If dtCurrentTable.Rows.Count = 0 Then
                'btnSave.Visible = Portal.GetAccessRight(ModuleID.DATAENTRY, SubModuleID.StatisticalDataSupplier, SubModuleAction.Create)
            Else
                'btnSave.Visible = True
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsDfPrincipal As New adm_SuperAdmin.clsDfPrincipal
            Dim strPrincipalId As String = Request.QueryString("PRINCIPAL_ID")
            Dim strTeamCode As String = ddlTeam.SelectedValue
            Dim strPivotCode As String = ddlPivotCode.SelectedValue
            Dim strUSerId As String = Session("UserID")

            DT = clsDfPrincipal.DFPrincipalDtlList(strPivotCode, strPrincipalId, strTeamCode, strUSerId)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListRef() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsDfPrincipal As New adm_SuperAdmin.clsDfPrincipal
            Dim strPrincipalId As String = Request.QueryString("PRINCIPAL_ID")
            Dim strTeamCode As String = ddlTeam.SelectedValue
            Dim strPivotCode As String = ddlPivotCode.SelectedValue
            Dim strUSerId As String = Session("UserID")
            Dim strRefCountryId As String = ddlCountry.SelectedValue
            Dim strRefPrincipalId As String = ddlPrincipal.SelectedValue

            DT = clsDfPrincipal.RefDFPrincipalDtlList(strPivotCode, strPrincipalId, strTeamCode, strUSerId, strRefCountryId, strRefPrincipalId)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            dgList.Columns.Clear()
            aryDataItem.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_DFPrincipalList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_DFPrincipalList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_DFPrincipalList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_DFPrincipalList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_DFPrincipalList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try

            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        LoadChkBox(e)
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try

            Select Case e.Row.RowType
                Case DataControlRowType.Header
                    If Master_Row_Count > 0 Then
                        LoadHeaderChkBox(e)
                    End If
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then
                        LoadChkBox(e)
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub LoadHeaderChkBox(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim tc As TableCell
            Dim ChkBox As New CheckBox

            tc = e.Row.Cells(aryDataItem.IndexOf("SELECT_IND"))

            With ChkBox
                .ID = "chkAllSelectInd"
                .CssClass = "cls_checkbox"
                .Attributes.Add("onclick", "ChangeAllCheckBoxStates(this);")
                
            End With

            tc.Controls.Add(ChkBox)


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadChkBox(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim tc As TableCell
            Dim strValue As String
            Dim ChkBox As New CheckBox

            tc = e.Row.Cells(aryDataItem.IndexOf("SELECT_IND"))
            strValue = Trim(e.Row.Cells(aryDataItem.IndexOf("SELECT_IND")).Text)

            With ChkBox
                .ID = "chkSelectInd"
                .CssClass = "cls_checkbox"
                .Attributes.Add("onclick", "ChangeHeaderCheckBoxStates(this);")
                If strValue = "1" Then
                    .Checked = True
                ElseIf strValue = "0" Then
                    .Checked = False
                Else
                    .Checked = False
                End If
            End With

            tc.Controls.Add(ChkBox)


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "DDL"
    Private Sub LoadSalesTeamDDL()
        Dim dt As DataTable
        Dim clsDfPrincipal As adm_SuperAdmin.clsDfPrincipal
        Try
            clsDfPrincipal = New adm_SuperAdmin.clsDfPrincipal

            Dim strPrincipalId As String = Request.QueryString("PRINCIPAL_ID")
            dt = clsDfPrincipal.LoadSalesTeamDDL(strPrincipalId, Session("UserID"))

            With ddlTeam
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "SALESTEAM_NAME"
                .DataValueField = "SALESTEAM_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("ALL", ""))
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDFDDL()
        Dim dt As DataTable
        Dim clsDfPrincipal As adm_SuperAdmin.clsDfPrincipal
        Try
            clsDfPrincipal = New adm_SuperAdmin.clsDfPrincipal

            dt = clsDfPrincipal.LoadDFDDL(Session("UserID"))

            With ddlPivotCode
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "PIVOT_NAME"
                .DataValueField = "PIVOT_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("ALL", ""))
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadPrincipalDDL()
        Dim dt As DataTable
        Dim clsDfPrincipal As adm_SuperAdmin.clsDfPrincipal
        Try
            If ddlCountry.SelectedIndex = 0 Then
                ddlPrincipal.Items.Clear()
                ddlPrincipal.Items.Add(New ListItem("-- Select --", ""))
            Else
                clsDfPrincipal = New adm_SuperAdmin.clsDfPrincipal
                dt = clsDfPrincipal.LoadPrincipalDDL(ddlCountry.SelectedValue, Session("UserID"))

                With ddlPrincipal
                    .Items.Clear()
                    .DataSource = dt
                    .DataValueField = "PRINCIPAL_ID"
                    .DataTextField = "PRINCIPAL_NAME"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- Select --", ""))
                    .SelectedIndex = 0
                End With
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCountryDDL()
        Dim dt As DataTable
        Dim clsDfPrincipal As adm_SuperAdmin.clsDfPrincipal
        Try
            clsDfPrincipal = New adm_SuperAdmin.clsDfPrincipal

            dt = clsDfPrincipal.LoadCountryDDL(Session("UserID"))

            With ddlCountry
                .Items.Clear()
                .DataSource = dt
                .DataTextField = "COUNTRY_NAME"
                .DataValueField = "COUNTRY_ID"
                .DataBind()
                .Items.Insert(0, New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With

            With ddlPrincipal
                .Items.Clear()
                .Items.Add(New ListItem("-- Select --", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Event Handler"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RefreshDatabinding()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("dfPrincipalList.aspx")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save()
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        LoadPrincipalDDL()
    End Sub

    Protected Sub btnreference_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreference.Click
        RefreshDatabinding(, True)
    End Sub

#End Region

    Private sub Save()

        Try
            If dgList.Rows.Count > 0 Then

                Dim i As Integer = 0
                Dim DK As DataKey

                For Each DR As GridViewRow In dgList.Rows

                    DK = dgList.DataKeys(i)

                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then


                        '=============================================================================================

                        Dim chkBox As CheckBox = CType(dgList.Rows(i).FindControl("chkSelectInd"), CheckBox)
                        Dim strSelectInd As String = Trim(dgList.DataKeys(i)("SELECT_IND"))
                        Dim strDFID As String = Trim(dgList.Rows(i).Cells(aryDataItem.IndexOf("DF_ID")).Text)
                        Dim strPrincipalID As String = Request.QueryString("PRINCIPAL_ID")
                        Dim strTeamCode As String = TeamCode
                        Dim strAltPivotCode As String = Trim(dgList.Rows(i).Cells(aryDataItem.IndexOf("ALT_PIVOT_CODE")).Text)
                        Dim strAltDfCode As String = Trim(dgList.Rows(i).Cells(aryDataItem.IndexOf("ALT_DF_CODE")).Text)
                        Dim strPivotName As String = Trim(dgList.Rows(i).Cells(aryDataItem.IndexOf("ALT_PIVOT_NAME")).Text)
                        Dim strPivotDesc As String = Trim(dgList.Rows(i).Cells(aryDataItem.IndexOf("ALT_PIVOT_DESC")).Text)
                        Dim strStatus As String = IIf(chkBox.Checked = True, 1, 0)
                        Dim strUserID As String = Portal.UserSession.UserID



                        Dim clsDfPrincipal As New adm_SuperAdmin.clsDfPrincipal
                        Dim dt As DataTable
                        dt = clsDfPrincipal.DFPrincipalDtlSave(strDFID, strPrincipalID, strTeamCode, _
                        strAltPivotCode, strAltDfCode, strPivotName, strPivotDesc, strStatus, strUserID)


                        '=============================================================================================


                    End If
            i += 1

                Next
                'RefreshDatabinding()
                UpdateDatagrid.Update()
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

 

End Class

Public Class CF_DFPrincipalList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "TEAM_CODE"
                strFieldName = "Team"
            Case "DF_CODE"
                strFieldName = "DF Code"
            Case "PIVOT_CODE"
                strFieldName = "PIVOT Code"
            Case "PIVOT_NAME"
                strFieldName = "Pivot Name"
            Case "PIVOT_DESC"
                strFieldName = "Pivot Desc."
            Case "ALT_DF_CODE"
                strFieldName = "Alt. DF Code"
            Case "ALT_PIVOT_CODE"
                strFieldName = "Alt. Pivot Code"
            Case "ALT_PIVOT_NAME"
                strFieldName = "Alt. Pivot Name"
            Case "ALT_PIVOT_DESC"
                strFieldName = "Alt. Pivot Desc."
            Case "SELECT_IND"
                strFieldName = "Select"
            Case "DF_ID"
                strFieldName = "DF Id"
            Case "SALESTEAM_CODE"
                strFieldName = "Sales Team Code"
            Case "CAT_TYPE"
                strFieldName = "Category"
            Case Else
                strFieldName = ColumnName.ToUpper
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype

        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        strColumnName = strColumnName.ToUpper

        'If strColumnName = "" Then
        'FCT = FieldColumntype.InvisibleColumn
        ' Else
        FCT = FieldColumntype.BoundColumn
        ' End If

        Return FCT

    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd h:mm tt}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class
