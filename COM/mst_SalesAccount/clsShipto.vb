'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	18/03/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsShipto
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsShipto"
        End Get
    End Property

    Public Function GetShiptoList(ByVal strSearchType As String, ByVal strSearchValue As String, ByVal strStatus As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SHIPTO_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetShiptoDetails(ByVal strCustCode As String, ByVal strShiptoCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SHIPTO_DTL"
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub UpdateShipto(ByVal strCustCode As String, ByVal strShiptoCode As String, ByVal strShiptoName As String, _
                            ByVal strStatus As String, ByVal strContName As String, ByVal strAdd1 As String, _
                            ByVal strAdd2 As String, ByVal strAdd3 As String, ByVal strAdd4 As String, ByVal strPostcode As String, _
                            ByVal strCity As String, ByVal strPOBox As String, ByVal strTelNo As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SHIPTO_UPDATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SHIPTO_NAME", strShiptoName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_1", strAdd1, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_2", strAdd2, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_3", strAdd3, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_4", strAdd4, cor_DB.clsDB.DataType.DBString)
                .addItem("POSTCODE", strPostcode, cor_DB.clsDB.DataType.DBString)
                .addItem("CITY", strCity, cor_DB.clsDB.DataType.DBString)
                .addItem("POBOX", strPOBox, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO", strTelNo, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function CreateShipto(ByVal strCustCode As String, ByVal strShiptoCode As String, ByVal strShiptoName As String, _
                            ByVal strStatus As String, ByVal strContName As String, ByVal strAdd1 As String, _
                            ByVal strAdd2 As String, ByVal strAdd3 As String, ByVal strAdd4 As String, ByVal strPostcode As String, _
                            ByVal strCity As String, ByVal strPOBox As String, ByVal strTelNo As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_SHIPTO_CREATE"
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SHIPTO_CODE", strShiptoCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SHIPTO_NAME", strShiptoName, cor_DB.clsDB.DataType.DBString)
                .addItem("STATUS", strStatus, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_NAME", strContName, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_1", strAdd1, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_2", strAdd2, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_3", strAdd3, cor_DB.clsDB.DataType.DBString)
                .addItem("ADD_4", strAdd4, cor_DB.clsDB.DataType.DBString)
                .addItem("POSTCODE", strPostcode, cor_DB.clsDB.DataType.DBString)
                .addItem("CITY", strCity, cor_DB.clsDB.DataType.DBString)
                .addItem("POBOX", strPOBox, cor_DB.clsDB.DataType.DBString)
                .addItem("TEL_NO", strTelNo, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class



