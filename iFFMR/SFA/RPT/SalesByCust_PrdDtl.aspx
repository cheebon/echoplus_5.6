<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesByCust_PrdDtl.aspx.vb" Inherits="iFFMR_SFA_RPT_SalesByCust_PrdDtl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Product Detail</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<body style="background-color:#EEEEEE;">
    <form id="frmPrdDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
    <div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="" align="center">
                    <%--<fieldset class="" style="width: 98%;">--%>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr>
                            <td class="" align="left">
                            <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                <ContentTemplate>
                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                       <div style="height: 400px; overflow: auto;">
                                        <asp:Repeater ID="RptOuter" runat="server">
                                        <HeaderTemplate>
                                            <table border="1" cellpadding="0" cellspacing="0" width="99%" class="RepeaterBorder">
                                            <tr class="GridHeader">
                                                <td align="center">Product</td><td align="right">Qty</td>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="GridNormal" style="background-color:#C9D1F9">
                                                <td align="left">
                                                <div style="font-weight:bold"><%# Eval("AGENCY_NAME") %></div></td>
                                                <td align="right">
                                                <div style="font-weight:bold"><%# Eval("SALES_AMT1") %></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Repeater ID="RptInner" runat="server">
                                                    <HeaderTemplate>
                                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>                                                        
                                                            <tr class="GridNormal" valign="top">
                                                                <td align="left">
                                                                <%# Eval("PRD_CODE") %></td>
                                                                <td align="left">
                                                                <%# Eval("PRD_NAME") %></td>
                                                                <td align="right">
                                                                <%# Eval("SALES_QTY") %>
                                                                </td>
                                                            </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        </asp:Repeater>
                                        </div>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                             </td></tr>
                        </table>
             <%--</fieldset>--%></td></tr></table>
       </div>
    </form>
</body>
</html>
