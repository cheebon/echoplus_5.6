<%@ Page Language="vb" AutoEventWireup="false" Inherits="CallEnquiryList" CodeFile="CallEnquiryList.aspx.vb" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="AjaxToolkit" %>--%>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblDate" Src="~/include/wuc_lblDate.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CallEnquiry</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallEnquiry" method="post" runat="server">
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: auto">
                                                            <tr>
                                                                <td></td>
                                                                <td valign="top" class="Bckgroundreport">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td class="Bckgroundreport">
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                    <tr style="width: 98%">
                                                                                        <td align="left">
                                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                <tr>
                                                                                                    <td></td>
                                                                                                    <td>
                                                                                                        <asp:Timer id="TimerControl1" runat="server" interval="100" enabled="false" OnTick="TimerControl1_Tick"></asp:Timer>
                                                                                                        <%--<cc1:stoppabletimer id="StoppableTimer1" runat="server" interval="50" enabled="false">
                                                                                                        </cc1:stoppabletimer>--%>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <customToolkit:wuc_dgpaging ID="Wuc_dgpaging" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                                Width="98%" FreezeHeader="True" GridHeight="455" AddEmptyHeaders="0" CellPadding="2"
                                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                                PagerSettings-Visible="false" AllowPaging="True">
                                                                                                <EmptyDataTemplate>
                                                                                                    <customToolkit:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                                                </EmptyDataTemplate>
                                                                                                <%--<Columns>
                                                                                                    <asp:BoundField DataField="CUST_CODE" HeaderText="Customer A/C" ReadOnly="True" SortExpression="CUST_CODE">
                                                                                                        <itemstyle horizontalalign="Center" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:TemplateField HeaderText="Customer Name" SortExpression="CUST_NAME">
                                                                                                        <itemtemplate>
                                                                            <asp:Label runat="server" Text='<%# Bind("CUST_NAME") %>' id="lblCompany"></asp:Label>
                                                                            </itemtemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Address" SortExpression="ADDRESS">
                                                                                                        <itemtemplate>
                                                                            <asp:Label runat="server" Text='<%# EVAL("ADD1") & "<BR/>" & EVAL("ADD2") %>' id="lblAddress"></asp:Label>
                                                                            </itemtemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:BoundField DataField="CONT_NAME" HeaderText="Contact Name" ReadOnly="True"
                                                                                                        SortExpression="CONT_NAME">
                                                                                                        <itemstyle horizontalalign="Left" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:BoundField DataField="TIME_IN" HeaderText="Time In" ReadOnly="True" SortExpression="TIME_IN">
                                                                                                        <itemstyle horizontalalign="Center" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:BoundField DataField="TIME_OUT" HeaderText="Time Out" ReadOnly="True" SortExpression="TIME_OUT">
                                                                                                        <itemstyle horizontalalign="Center" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:BoundField DataField="TIME_SPEND" HeaderText="Time Spend" ReadOnly="True" SortExpression="TIME_SPEND">
                                                                                                        <itemstyle horizontalalign="Center" />
                                                                                                    </asp:BoundField>
                                                                                                    <asp:BoundField DataField="REASON_NAME" HeaderText="Reason" ReadOnly="True" SortExpression="REASON_NAME" />
                                                                                                    <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS" />
                                                                                                    <asp:HyperLinkField DataTextField="SFMS_INDICATOR" HeaderText="A" InsertVisible="False"
                                                                                                        SortExpression="SFMS_INDICATOR" />
                                                                                                    <asp:HyperLinkField DataNavigateUrlFields="SALES_INDICATOR" DataTextField="SALES_INDICATOR"
                                                                                                        HeaderText="$" InsertVisible="False" SortExpression="SALES_INDICATOR" />
                                                                                                </Columns>--%>
                                                                                            </ccGV:clsGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>        
    </form>
</body>
</html>
