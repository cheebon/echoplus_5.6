Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Configuration
Imports System.Data
'Imports System.EnterpriseServices
'Imports System.Reflection
'Imports System.Runtime.InteropServices
'Imports System.Runtime.CompilerServices

'<Guid("368F4EC0-D9A0-4399-B793-494F9D165904"), Transaction(TransactionOption.Required)> _
'<Transaction(TransactionOption.Required)> _
Public Class clsConnection
    'Inherits ServicedComponent

    Private objSession As System.Web.HttpContext
    'Private cnnString As String = "server=" & ConfigurationSettings.AppSettings("server") & ";database=" & ConfigurationSettings.AppSettings("database") & ";User ID=" & ConfigurationSettings.AppSettings("userid") & ";password=" & ConfigurationSettings.AppSettings("password")
    'Private cnnString As String = "server=" & Web.HttpContext.Current.Session("setting_serverip") & ";database=" & Web.HttpContext.Current.Session("setting_database") & ";User ID=" & Web.HttpContext.Current.Session("setting_userid") & ";password=" & Web.HttpContext.Current.Session("setting_pwd")
    Private cnnString As String

    Private cnn As New SqlConnection(cnnString)
    Private cmd As New SqlCommand
    Private prm As SqlParameter
    Private trns As SqlTransaction
    Private mCmdType As String
    Private mCmdText As String
    Private mTimeout As Integer = ConfigurationSettings.AppSettings("DBTimeout")

    Const DBInt As Integer = 1
    Const DBString As Integer = 2
    Const DBDatetime As Integer = 3
    Const DBLong As Integer = 4
    Const DBDouble As Integer = 5
    Const DBByte As Integer = 6

    Public Property ConnectionString() As String
        Get
            Return cnnString
        End Get
        Set(ByVal Value As String)
            cnnString = Value
            cnn.ConnectionString = cnnString
        End Set
    End Property

    Public Property CmdType() As String
        Get
            Return mCmdType
        End Get
        Set(ByVal Value As String)
            mCmdType = Value
        End Set
    End Property

    Public Property CmdText() As String
        Get
            Return mCmdText
        End Get
        Set(ByVal Value As String)
            mCmdText = Value
        End Set
    End Property

    Private Function GetFieldIndex(ByVal mFieldType As String) As Integer
        Select Case mFieldType
            Case 1
                GetFieldIndex = SqlDbType.Int
            Case 2
                GetFieldIndex = SqlDbType.VarChar
            Case 3
                GetFieldIndex = SqlDbType.DateTime
            Case 4
                GetFieldIndex = SqlDbType.BigInt
            Case 5
                GetFieldIndex = SqlDbType.Float
        End Select
        Exit Function
    End Function

    Public Sub addInputParam(ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As Integer)
        Try
            mFieldType = GetFieldIndex(mFieldType)
            prm = New SqlParameter("@" & mFieldName, mFieldType)
            cmd.Parameters.Add(prm)
            'assign parameter value
            If mFieldType = SqlDbType.DateTime And (mFieldValue = "" Or mFieldValue = "12:00:00 AM") Then
                cmd.Parameters("@" & mFieldName).Value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                cmd.Parameters("@" & mFieldName).Value = IIf(IsNothing(mFieldValue), "", mFieldValue)
            End If

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsConnection.addInputParam : " + ex.ToString))
        End Try
    End Sub

    Public Sub addInputParamByte(ByVal mFieldName As String, ByVal mFieldValue As Byte(), ByVal mFieldType As Integer)
        Try
            mFieldType = GetFieldIndex(mFieldType)
            prm = New SqlParameter("@" & mFieldName, mFieldType)
            cmd.Parameters.Add(prm)
            'assign parameter value 
            cmd.Parameters("@" & mFieldName).Value = IIf(IsNothing(mFieldValue), "", mFieldValue)
             
        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsConnection.addInputParam : " + ex.ToString))
        End Try
    End Sub

    Public Sub addOutputParam(ByVal mFieldName As String, ByVal mFieldType As Integer, Optional ByVal mSize As Integer = 100)
        Try
            mFieldType = GetFieldIndex(mFieldType)
            prm = New SqlParameter("@" & mFieldName, mFieldType, mSize)
            prm.Direction = ParameterDirection.Output
            cmd.Parameters.Add(prm)
        Catch ex As Exception
            Throw (New ExceptionMsg("cor_DB.clsConnection.addOutputParam : " + ex.ToString))
        End Try
    End Sub

    '<AutoComplete()> _
    Public Function ExecuteReturnDT() As System.Data.DataTable
        'Dim cn As New SqlConnection(cnnString)
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable

        Try
            cnn.Open()
            With cmd
                .CommandTimeout = mTimeout
                .Connection = cnn
                trns = cnn.BeginTransaction()
                .Transaction = trns
                .CommandText = mCmdText
                mCmdType = IIf(IsDBNull(mCmdType), "", mCmdType)
                If UCase(mCmdType) = "STOREDPROCEDURE" Then
                    .CommandType = CommandType.StoredProcedure
                Else
                    .CommandType = CommandType.Text
                End If
            End With

            trns.Commit()
            da.SelectCommand = cmd
            da.Fill(dt)

            ExecuteReturnDT = dt

            'ContextUtil.SetComplete()

            da = Nothing
            cmd = Nothing
            cnn.Close()
            Exit Function

        Catch ex As Exception
            ExceptionMsg.WriteLog("DBError:" & ex.ToString)
            trns.Rollback()
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_DB.clsConnection.ExecuteReturnDT : " + ex.ToString))
        Finally
            da = Nothing
            cmd = Nothing
            cnn.Close()
        End Try
    End Function

    Public Function ExecuteReturnDS() As System.Data.DataSet
        'Dim cn As New SqlConnection(cnnString)
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        Try
            cnn.Open()
            With cmd
                .CommandTimeout = mTimeout
                .Connection = cnn
                trns = cnn.BeginTransaction()
                .Transaction = trns
                .CommandText = mCmdText
                mCmdType = IIf(IsDBNull(mCmdType), "", mCmdType)
                If UCase(mCmdType) = "STOREDPROCEDURE" Then
                    .CommandType = CommandType.StoredProcedure
                Else
                    .CommandType = CommandType.Text
                End If
            End With

            trns.Commit()
            da.SelectCommand = cmd
            da.Fill(ds)

            ExecuteReturnDS = ds

            'ContextUtil.SetComplete()

            da = Nothing
            cmd = Nothing
            cnn.Close()
            Exit Function

        Catch ex As Exception
            ExceptionMsg.WriteLog("DBError:" & ex.ToString)
            trns.Rollback()
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_DB.clsConnection.ExecuteReturnDS : " + ex.ToString))
        Finally
            da = Nothing
            cmd = Nothing
            cnn.Close()
        End Try
    End Function

    Public Function ExecuteReturnValue(ByVal strValueName As String) As String
        'Dim cn As New SqlConnection(cnnString)
        Dim da As New SqlDataAdapter
        Dim Value As String = String.Empty

        Try
            cnn.Open()
            With cmd
                .CommandTimeout = mTimeout
                .Connection = cnn
                trns = cnn.BeginTransaction()
                .Transaction = trns
                .CommandText = mCmdText

                mCmdType = IIf(IsDBNull(mCmdType), "", mCmdType)
                If UCase(mCmdType) = "STOREDPROCEDURE" Then
                    .CommandType = CommandType.StoredProcedure
                Else
                    .CommandType = CommandType.Text
                End If
            End With

            cmd.ExecuteNonQuery()
            If cmd.Parameters(strValueName) IsNot Nothing Then Value = cmd.Parameters(strValueName).Value
            cmd = Nothing
            cnn.Close()
        Catch ex As Exception
            ExceptionMsg.WriteLog("DBError:" & ex.ToString)
            trns.Rollback()
            Throw (New ExceptionMsg("cor_DB.clsConnection.ExecuteReturnValue : " + ex.ToString))
        Finally
            da = Nothing
            cmd = Nothing
        End Try
        Return Value
    End Function
    Public Function ExecuteReturnValue() As String
        ExecuteReturnValue = String.Empty
        Dim dr As SqlDataReader
        Try
            cnn.Open()
            With cmd
                .CommandTimeout = mTimeout
                .Connection = cnn
                trns = cnn.BeginTransaction()
                .Transaction = trns
                .CommandText = mCmdText
                mCmdType = IIf(IsDBNull(mCmdType), "", mCmdType)
                If UCase(mCmdType) = "STOREDPROCEDURE" Then
                    .CommandType = CommandType.StoredProcedure
                Else
                    .CommandType = CommandType.Text
                End If
            End With
            trns.Commit()
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection) 'SQLCmd.ExecuteReader(CommandBehavior.CloseConnection)
            If dr.Read Then ExecuteReturnValue = dr(0)
            dr = Nothing
            cmd = Nothing
            cnn = Nothing

            Exit Function
        Catch ex As Exception
            ExceptionMsg.WriteLog(ex.Message)
            trns.Rollback()
            Throw (New ExceptionMsg("cor_DB.clsConnection.ExecuteReader : " + ex.Message))
        Finally
            cmd = Nothing
            cnn = Nothing
        End Try
    End Function


    Public Sub ExecuteNonQuery()
        'Dim cn As New SqlConnection(cnnString)
        'Dim cmd As New SqlCommand(pSQL, cn)

        Try
            cnn.Open()
            With cmd
                .CommandTimeout = mTimeout
                .Connection = cnn
                trns = cnn.BeginTransaction()
                .Transaction = trns
                .CommandText = mCmdText
                mCmdType = IIf(IsDBNull(mCmdType), "", mCmdType)
                If UCase(mCmdType) = "STOREDPROCEDURE" Then
                    .CommandType = CommandType.StoredProcedure
                Else
                    .CommandType = CommandType.Text
                End If
            End With

            trns.Commit()
            cmd.ExecuteNonQuery()

            'ContextUtil.SetComplete()

            cmd = Nothing
            cnn.Close()
            Exit Sub
        Catch ex As Exception
            ExceptionMsg.WriteLog(ex.Message)
            trns.Rollback()
            Throw (New ExceptionMsg("cor_DB.clsConnection.ExecuteSQL : " + ex.ToString))
            'ContextUtil.SetAbort()
        Finally
            cmd = Nothing
            cnn.Close()
        End Try
    End Sub

    Public Function ExecuteReader() As SqlDataReader
        'Dim cn As New SqlConnection(cnnString)
        Dim dr As SqlDataReader

        Try
            cnn.Open()
            With cmd
                .CommandTimeout = mTimeout
                .Connection = cnn
                trns = cnn.BeginTransaction()
                .Transaction = trns
                .CommandText = mCmdText
                mCmdType = IIf(IsDBNull(mCmdType), "", mCmdType)
                If UCase(mCmdType) = "STOREDPROCEDURE" Then
                    .CommandType = CommandType.StoredProcedure
                Else
                    .CommandType = CommandType.Text
                End If
            End With

            'open connection and execution
            trns.Commit()
            'dr.Close()
            ExecuteReader = cmd.ExecuteReader(CommandBehavior.CloseConnection) 'SQLCmd.ExecuteReader(CommandBehavior.CloseConnection)

            'ContextUtil.SetComplete()
            dr = Nothing
            cmd = Nothing
            cnn = Nothing

            Exit Function
        Catch ex As Exception
            ExceptionMsg.WriteLog(ex.Message)
            trns.Rollback()
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_DB.clsConnection.ExecuteReader : " + ex.ToString))
        Finally
            cmd = Nothing
            cnn = Nothing
        End Try
    End Function

    Public Sub Connect()
        Try
            cnn.Open()
            'ContextUtil.SetComplete()

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_DB.clsConnection.Connect : " + ex.ToString))
        Finally
            cnn = Nothing
        End Try
    End Sub

    Public Sub Disconnect()
        Try
            If cnn.State <> ConnectionState.Closed Then
                cnn.Close()
            End If
            'ContextUtil.SetComplete()

        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw (New ExceptionMsg("cor_DB.clsConnection.Disconnect : " + ex.ToString))
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Private objEventLog As New EventLog("Application", ".", ".Net Version 2005")

        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'objEventLog.WriteEntry(msg)
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = msg
                .Log()
            End With
            objLog = Nothing
        End Sub

        Public Shared Sub WriteLog(ByVal msg As String)
            Dim objLog As New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = msg
                .Log()
            End With
            objLog = Nothing
        End Sub
    End Class
End Class

