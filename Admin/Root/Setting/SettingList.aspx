﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SettingList.aspx.vb" Inherits="Admin_Root_Setting_SettingList" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Setting List</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmSetting" runat="server">
    
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />           
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>      
        
       
        
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>                    
                    <!--Begin Modify Here -->
                    <div class="border_std">
                        <asp:Button ID="btnCreate" runat="server" Text="Create" CausesValidation="False"
                                                        UseSubmitBehavior="false" Visible="true" />
                    </div>
                    <div class="border_std">
                    <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                    <td style="width:100px">
                        <asp:Label ID="lblSearchType" runat="server" Text="Search by "></asp:Label></td><td>
                            <asp:DropDownList ID="ddlSearchType" runat="server" AutoPostBack="true" Width="200px">
                                <asp:ListItem Value="ALL" Text="All"></asp:ListItem>
                                <asp:ListItem Value="SETTING_ID" Text="Setting ID"></asp:ListItem>
                                <asp:ListItem Value="FFMA_SERVER_IP" Text="FFMA Server IP"></asp:ListItem>
                                <asp:ListItem Value="FFMA_DB_NAME" Text="FFMA DB Name"></asp:ListItem>
                                <asp:ListItem Value="FFMR_SERVER_IP" Text="FFMR Server IP"></asp:ListItem>
                                <asp:ListItem Value="FFMR_DB_NAME" Text="FFMR DB Name"></asp:ListItem>
                                <asp:ListItem Value="FFMS_SERVER_IP" Text="FFMS Server IP"></asp:ListItem>
                                <asp:ListItem Value="FFMS_DB_NAME" Text="FFMS DB Name"></asp:ListItem>
                                <asp:ListItem Value="ECHOPLUS_SERVER_IP" Text="ECHOPLUS Server IP"></asp:ListItem>
                                <asp:ListItem Value="ECHOPLUS_DB_NAME" Text="ECHOPLUS DB Name"></asp:ListItem>
                            </asp:DropDownList></td>
                    </tr> 
                    <asp:Panel runat="server" ID="pnlSeachValue" Visible="false">
                    <tr>
                        <td>
                            <asp:Label ID="lblSearchValue" runat="server" Text="Search value "></asp:Label></td><td>
                                <asp:TextBox ID="txtSearchValue" runat="server" MaxLength="100" Width="200px"></asp:TextBox></td>
                    </tr> 
                    </asp:Panel>
                    <%--
                    <tr>
                        <td style="width:50%">
                            <asp:Label ID="lblFFMAdbname" runat="server" Text="FFMA DB Name  "></asp:Label></td><td>
                                <asp:TextBox ID="txtFFMAdbname" runat="server" MaxLength="50"></asp:TextBox></td>
                    </tr> 
                    <tr>
                        <td>
                            <asp:Label ID="lblFFMRdbname" runat="server" Text="FFMR DB Name  "></asp:Label></td><td>
                                <asp:TextBox ID="txtFFMRdbname" runat="server" MaxLength="50"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFFMSdbname" runat="server" Text="FFMS DB Name  "></asp:Label></td><td>
                                <asp:TextBox ID="txtFFMSdbname" runat="server" MaxLength="50"></asp:TextBox></td>
                    </tr> --%>
                    </table>
                   
                    <asp:Button ID="btnSearch" runat="server" Text="Search" />
                </div>
                <br />
                <div class="border_std"> 
                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="330px" AddEmptyHeaders="0" CellPadding="2"
                        EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="SETTING_ID">
                        <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                        <EmptyDataTemplate>There is no data added.</EmptyDataTemplate>
                        <Columns>
                            <asp:ButtonField CommandName="Delete" AccessibleHeaderText="Delete" ImageUrl="~/images/ico_Delete.gif" ButtonType="Image" HeaderText="Delete">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="Modify" AccessibleHeaderText="Edit" ImageUrl="~/images/ico_Edit.gif" ButtonType="Image" HeaderText="Edit">
                                <itemstyle horizontalalign="Center" width="5%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="SETTING_ID" HeaderText="Setting ID" SortExpression="SETTING_ID" />
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="FFMA_SERVER_IP" HeaderText="FFMA Server IP" SortExpression="FFMA_SERVER_IP" />
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="FFMA_DB_NAME" HeaderText="FFMA DB Name" SortExpression="FFMA_DB_NAME" />
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="FFMR_SERVER_IP" HeaderText="FFMR Server IP" SortExpression="FFMR_SERVER_IP" />                            
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="FFMR_DB_NAME" HeaderText="FFMR DB Name" SortExpression="FFMR_DB_NAME" />  
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="FFMS_SERVER_IP" HeaderText="FFMS Server IP" SortExpression="FFMS_SERVER_IP" /> 
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="FFMS_DB_NAME" HeaderText="FFMS DB Name" SortExpression="FFMS_DB_NAME" />  
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="ECHOPLUS_SERVER_IP" HeaderText="Echoplus Server IP" SortExpression="ECHOPLUS_SERVER_IP" /> 
                            <asp:ButtonField ButtonType="Link" CommandName="Details" DataTextField="ECHOPLUS_DB_NAME" HeaderText="Echoplus DB Name" SortExpression="ECHOPLUS_DB_NAME" />                         
                            
                        </Columns>
                    </ccGV:clsGridView>
                </div>           
                    <!--End Modify Here -->
                </ContentTemplate>
            </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
