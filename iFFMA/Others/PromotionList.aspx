<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PromotionList.aspx.vb" Inherits="iFFMA_Others_Promotion" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_PrdSearch" Src="../Common/wuc_PrdSearch.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Shipto List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script type="text/javascript" language="Javascript">
        function SelectRow(chkSelected) {
            if (chkSelected.checked == false) {

                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    var e = document.forms[0].elements[i];

                    if (e.type == 'checkbox') {
                        e.checked = chkSelected.checked;
                        break;
                    }
                }
            }
        }

        function SelectAllRows(chkAll) {

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox') {
                    e.checked = chkAll.checked;
                }
            }
        }

        function checkDelete() {
            var isChecked = false;

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox' && e.checked) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                if (!confirm('Are you sure want to delete?')) {
                    return false;
                }
            }
            else {
                alert('Please select at least one of the record to delete!');
                return false;
            }

            return true;
        }
    </script>
</head>

<body class="BckgroundInsideContentLayout">
    <form id="frmPromotion" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                               
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                        <asp:Button ID="btnDelete" CssClass="cls_button" runat="server" Text="Delete" OnClientClick="if (!checkDelete()){return false; }"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="TEAM_CODE, CGCODE, PRD_CODE, STATUS">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <itemtemplate>
                                                                                            <%--<asp:checkbox ID="chkSelect" runat="server" class="cls_checkbox"/>--%>
                                                                                        </itemtemplate> 
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Team Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:Label ID="lblTeamCode" runat="server" CssClass="cls_label" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Customer group code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                   <asp:DropDownList ID="ddlCustGrpCode" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                                                </td>
                                                                                <asp:RequiredFieldValidator ID="rfvCGCode" runat="server" ControlToValidate="ddlCustGrpCode"
                                                                                    ErrorMessage="Customer group code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator" />
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Product code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                   <asp:DropDownList ID="ddlProdCode" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" />
                                                                                </td>
                                                                                <asp:RequiredFieldValidator ID="rfvProdCode" runat="server" ControlToValidate="ddlProdCode"
                                                                                    ErrorMessage="Product code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator" />
                                                                            </tr>
<%--                                                                             <tr>
                                                                                <td><span class="cls_label_header">Status</span> <span class="cls_label_mark"></span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                    <asp:ListItem Value="A">Active</asp:ListItem>
                                                                                    <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                </td>
                                                                            </tr>--%>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel> 
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
    
                                            </ajaxToolkit:TabContainer>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
      <%--  <customToolkit:wuc_PrdSearch ID="wuc_PrdSearch" Title="Product Search" runat="server" />--%>
    </form>
</body>
</html>
