<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FieldForceList.aspx.vb" Inherits="iFFMA_FieldForce_FieldForceList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Field Force List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmFieldForceList" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                        
                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <asp:Timer id="TimerControl1" runat="server" enabled="False" interval="100" ontick="TimerControl1_Tick" />
                                                      
                                            <%--<span style="float:left; width:100%;">--%>            
                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="SALESREP_CODE">
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel> 
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left:15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%" >
                                                                            <tr>
                                                                                <td style="width:17%"></td>
                                                                                <td style="width:3%"></td>
                                                                                <td style="width:80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Team Code</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:Label ID="lblTeamCode" runat="server" CssClass="cls_label" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Field Force Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtSalesrepCode" runat="server" CssClass="cls_textbox" MaxLength="50" /></td>
                                                                                <asp:RequiredFieldValidator ID="rfvSalesrepCode" runat="server" ControlToValidate="txtSalesrepCode"
                                                                                    ErrorMessage="Salesrep Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator" />
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Field Force Name</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:TextBox ID="txtSalesrepName" runat="server" CssClass="cls_textbox" MaxLength="100" Width="200px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Status</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td> 
                                                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist">
                                                                                        <asp:ListItem Value="A">Active</asp:ListItem>
                                                                                        <asp:ListItem Value="D">Inactive</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Sales Area</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlSalesArea" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Default Warehouse</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlDefaultWarehouse" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top"><span class="cls_label_header">Warehouse</span></td>
                                                                                <td valign="top"><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <span style="float:left; width:200px; padding-top:2px; padding-bottom:2px;">
                                                                                        <span class="cls_label_header"></span><br />
                                                                                        <asp:ListBox ID="lsbWarehouse" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                                    </span>
                                                                                    
                                                                                    <span style="float:left; width:55px; padding-top: 2px; padding-bottom:2px;">
                                                                                        <span style="float:left; padding-top:30px;"><asp:Button ID="lnkAddWarehouse" runat="server" CssClass="cls_button" Width="35px" Text=">" /></span>
                                                                                        <span style="float:left; padding-top:5px;"><asp:Button ID="lnkRemoveWarehouse" runat="server" CssClass="cls_button" Width="35px" Text="<" /></span>
                                                                                        <span style="float:left; padding-top:5px;"><asp:Button ID="lnkAddAllWarehouse" runat="server" CssClass="cls_button" Width="35px" Text=">>" /></span>
                                                                                        <span style="float:left; padding-top:5px;"><asp:Button ID="lnkRemoveAllWarehouse" runat="server" CssClass="cls_button" Width="35px" Text="<<" /></span>
                                                                                    </span>
                                                                                    
                                                                                    <span style="float:left; width:180px; padding-top:2px; padding-bottom:2px;">
                                                                                        <span class="cls_label_header">Selected Warehouse</span><br>
                                                                                        <asp:ListBox ID="lsbSelectedWarehouse" CssClass="cls_listbox" runat="server" SelectionMode="Multiple" Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                                    </span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Region</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlRegion" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Company</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td><asp:DropDownList ID="ddlCompany" runat="server" CssClass="cls_dropdownlist" /></td>
                                                                            </tr>
                                                                            <tr><td colspan="3">&nbsp;</td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>  
                                                            </table>
                                                        </asp:Panel> 
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel> 
                                                               
                                            </ajaxToolkit:TabContainer>
                                            <%--</span>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                            <tr class="Bckgroundreport"><td style="height:5px"></td></tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        
    </form>
</body>
</html>
