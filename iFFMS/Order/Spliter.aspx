<%@ Page Language="VB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
    .SplitterBar {
        background-color:#BBBBBB;
        background-position:center;
        background-repeat:no-repeat;       
        cursor:pointer;
        margin:0px;
        padding:0px;
        vertical-align:middle;
        }
    </style>
    <script type="text/javascript">
         function resizeHandler() 
         {
            if(top.clientHeight>100)
            {
                  window.document.body.style.height =top.clientHeight-100;
            }
           else if (screen.availHeight>300)
	       {
	          window.document.body.style.height = screen.availHeight-300;
	       }
	       else 
	       {
              window.document.body.style.height=top.screen.height-300;  
	       }
         }    
        window.onresize=resizeHandler; 
    </script>
</head>
<body class="SplitterBar" style="background-image:url(../../images/lib_grippy.gif)" onclick="self.parent.CollapseExpand()" onload="javascript:resizeHandler()">
</body>

</html>
