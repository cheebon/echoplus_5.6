<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanMSS.aspx.vb" Inherits="iFFMS_Plan_PlanMSS" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plan Addon MSS</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dglistNewMSS');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
        function ChangeAllCheckBoxStates2(checkState)
   { var dglist = document.getElementById('dglistSelectedMSS');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
       
                 function ValidatedglistNewMSSCheckBoxStates()
   { var dglist = document.getElementById('dglistNewMSS');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
        function ValidatedglistSelectedMSSCheckBoxStates()
   { var dglist = document.getElementById('dglistSelectedMSS');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('TopBar');HideElement('DetailBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe')"
    class="BckgroundInsideContentLayout">
    <form id="frmplanaddoncont" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" Visible="true"
                            Width="80px" OnClientClick="HideElement('ContentBar');ShowElement('TopBar'); MaximiseFrameHeight('TopBarIframe')" />
                    </div>
                    <div id="txnContent">
                        <asp:UpdatePanel ID="UpdateSelectedMSS" runat="server" UpdateMode="Conditional"
                            RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlSelectedMSS" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlSelectedMSS_Tick" />
                                <div id="SelectedMSS" class="S_DivHeader">
                                    Assigned Market Survey
                                </div>
                                <asp:Panel ID="pSelectedMSS" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblSmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckalldelete" value="Check All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(true);" visible="false" style="width: 80px"
                                                validationgroup="deleteMSS" runat="server" />
                                            <input type="Button" id="btnuncheckalldelete" value="Uncheck All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(false);" visible="false" style="width: 80px"
                                                validationgroup="deleteMSS" runat="server" />
                                            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="cls_button" ValidationGroup="deleteMSS"
                                                Visible="false" Width="80px"  OnClientClick="return ValidatedglistSelectedMSSCheckBoxStates();"/>
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistSelectedMSS" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TITLE_CODE">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                 There is no data added.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkdelete" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TITLE_CODE" HeaderText="Title code" ReadOnly="True" SortExpression="TITLE_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TITLE_NAME" HeaderText="Title Name" ReadOnly="True" SortExpression="TITLE_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                         
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeSelectedMSS" runat="server" TargetControlID="pSelectedMSS"
                                    ExpandControlID="SelectedMSS" CollapseControlID="SelectedMSS">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateNewMSS" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlNewMSS" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlNewMSS_Tick" />
                                <div id="NewMSS" class="S_DivHeader">
                                    Add New Market Survey
                                </div>
                                <asp:Panel ID="PNewMSS" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                 <asp:Label ID="lblNmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                visible="false" style="width: 80px" validationgroup="newMSS" runat="server" />
                                            <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                visible="false" style="width: 80px" validationgroup="newMSS" runat="server" />
                                            <asp:Button ID="btnadd" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="newMSS"
                                                Visible="false" Width="80px" OnClientClick= "return ValidatedglistNewMSSCheckBoxStates();" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistNewMSS" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="TITLE_CODE">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkselect" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TITLE_CODE" HeaderText="Title code" ReadOnly="True" SortExpression="TITLE_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TITLE_NAME" HeaderText="Title Name" ReadOnly="True" SortExpression="TITLE_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                         
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeNewMSS" runat="server" TargetControlID="PNewMSS"
                                    ExpandControlID="NewMSS" CollapseControlID="NewMSS">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
