﻿/***************************************************************
/ Based on the old Messager.js
/ This one pass in which div to show as message container, and the map div
/ You can customise where you want the message div to appear inside the map
***************************************************************/
var timer;
var mapOffset; // = $("#map_canvas").offset();

function ShowMessage(mapDiv, msgDiv, msg, counterFlag) {
    var mapOffset = $("#" + mapDiv).offset();
    if (mapOffset) {
        $("#" + msgDiv).css({ top: mapOffset.top, left: mapOffset.left });
    }
    
    //set text first
    $("#" + msgDiv).text(msg);    
    
    //If div is hidden then slide out
    if ($("#" + msgDiv).is(":hidden")) {
        $("#" + msgDiv).slideDown();
    }

    if (timer) {
        clearTimeout(timer);
        timer = null;
    }
    
    switch (counterFlag) {
        case "on":
            timer = setTimeout("HideMessage()", 10000);
            break;
        case "off":
            //nothing
            break;
    }
    
}

function HideMessage(msgDiv) {
    $("#" + msgDiv).slideUp();

    if (timer) {
        clearTimeout(timer);
        timer = null;
    }
}