Imports System.Data

Partial Class iFFMA_TradeDeal_Bonus_PriceGrpBonusPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property PrdCode() As String
        Get
            Return Trim(hdPrdCode.Value)
        End Get
        Set(ByVal value As String)
            hdPrdCode.Value = value
        End Set
    End Property

    Public Property PriceGrpCode() As String
        Get
            Return Trim(hdPriceGrpCode.Value)
        End Get
        Set(ByVal value As String)
            hdPriceGrpCode.Value = value
        End Set
    End Property

    Public Property Qty() As String
        Get
            Return Trim(hdQty.Value)
        End Get
        Set(ByVal value As String)
            hdQty.Value = value
        End Set
    End Property

    Public Property UomCode() As String
        Get
            Return Trim(hdUomCode.Value)
        End Get
        Set(ByVal value As String)
            hdUomCode.Value = value
        End Set
    End Property

    Public Property StartDate() As String
        Get
            Return Trim(hdStartDate.Value)
        End Get
        Set(ByVal value As String)
            hdStartDate.Value = value
        End Set
    End Property

    Public Property EndDate() As String
        Get
            Return Trim(hdEndDate.Value)
        End Get
        Set(ByVal value As String)
            hdEndDate.Value = value
        End Set
    End Property

    Public Property RegionCode() As String
        Get
            Return Trim(hdRegionCode.Value)
        End Get
        Set(ByVal value As String)
            hdRegionCode.Value = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtPriceGrp As DataTable
        Dim dtRegion As DataTable
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlFOCType As DropDownList = CType(DetailsView1.FindControl("ddlFOCType"), DropDownList)
            If ddlFOCType IsNot Nothing Then
                ddlFOCType.SelectedValue = IIf(IsDBNull(DT.Rows(0)("FOC_TYPE")), "", DT.Rows(0)("FOC_TYPE"))
            End If

            Dim ddlPriceGrp As DropDownList = CType(DetailsView1.FindControl("ddlPriceGrp"), DropDownList)
            If ddlPriceGrp IsNot Nothing Then
                dtPriceGrp = clsCommon.GetPriceGrpDDL
                If ddlPriceGrp IsNot Nothing Then
                    With ddlPriceGrp
                        .Items.Clear()
                        .DataSource = dtPriceGrp.DefaultView
                        .DataTextField = "PRICE_GRP_CODE"
                        .DataValueField = "PRICE_GRP_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                        .SelectedValue = IIf(IsDBNull(DT.Rows(0)("PRICE_GRP_CODE")), "", DT.Rows(0)("PRICE_GRP_CODE"))
                    End With
                End If
            End If

            If Session("PRINCIPAL_ID") = 5 Then
                Dim ddlRegion As DropDownList = CType(DetailsView1.FindControl("ddlRegion"), DropDownList)
                If ddlRegion IsNot Nothing Then
                    dtRegion = clsCommon.GetRegionDDL
                    If ddlRegion IsNot Nothing Then
                        With ddlRegion
                            .Items.Clear()
                            .DataSource = dtRegion.DefaultView
                            .DataTextField = "REGION_NAME"
                            .DataValueField = "REGION_CODE"
                            .DataBind()
                            .Items.Insert(0, New ListItem("-- SELECT --", ""))
                            .SelectedIndex = 0
                            .SelectedValue = IIf(IsDBNull(DT.Rows(0)("REGION_CODE")), "", DT.Rows(0)("REGION_CODE"))
                        End With
                    End If
                End If
            End If

            dtUOM = clsCommon.GetUOMDDLByProduct(PrdCode)

            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            If ddlUOM IsNot Nothing Then
                With ddlUOM
                    .Items.Clear()
                    .DataSource = dtUOM.DefaultView
                    .DataTextField = "UOM_NAME"
                    .DataValueField = "UOM_CODE"
                    .DataBind()
                    '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("UOM_CODE")), "", DT.Rows(0)("UOM_CODE"))
                End With
            End If

            Dim ddlFOCUOM As DropDownList = CType(DetailsView1.FindControl("ddlFOCUOM"), DropDownList)
            If ddlFOCUOM IsNot Nothing Then
                With ddlFOCUOM
                    .Items.Clear()
                    .DataSource = dtUOM.DefaultView
                    .DataTextField = "UOM_NAME"
                    .DataValueField = "UOM_CODE"
                    .DataBind()
                    '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("FOC_UOM_CODE")), "", DT.Rows(0)("FOC_UOM_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ResetPage()
        'PriceGrpBonusID = ""
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
    End Sub

    Private Sub LoadAddNewUOMDdl(ByVal strPrdCode As String)
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL
        dtUOM = clsCommon.GetUOMDDLByProduct(strPrdCode)

        Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
        If ddlUOM IsNot Nothing Then
            With ddlUOM
                .Items.Clear()
                .DataSource = dtUOM.DefaultView
                .DataTextField = "UOM_NAME"
                .DataValueField = "UOM_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                '.SelectedValue = IIf(IsDBNull(DT.Rows(0)("UOM_CODE")), "", DT.Rows(0)("UOM_CODE"))
            End With
        End If


    End Sub


    Private Sub LoadAddNewFOCUOMDdl(ByVal strPrdCode As String)
        Dim dtUOM As DataTable
        Dim clsCommon As New mst_Common.clsDDL
        dtUOM = clsCommon.GetUOMDDLByProduct(strPrdCode)


        Dim ddlFOCUOM As DropDownList = CType(DetailsView1.FindControl("ddlFOCUOM"), DropDownList)
        If ddlFOCUOM IsNot Nothing Then
            With ddlFOCUOM
                .Items.Clear()
                .DataSource = dtUOM.DefaultView
                .DataTextField = "UOM_NAME"
                .DataValueField = "UOM_CODE"
                .DataBind()
                '.Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
                '.SelectedValue = IIf(IsDBNull(DT.Rows(0)("FOC_UOM_CODE")), "", DT.Rows(0)("FOC_UOM_CODE"))
            End With
        End If
    End Sub

#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsBonus As New mst_TradeDeal.clsBonus

            DT = clsBonus.GetPriceGrpBonusDetails(PrdCode, PriceGrpCode, Qty, UomCode, StartDate, EndDate, RegionCode)
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)("LOCK_FLAG") <> "0" Then
                    RaiseEvent PopLockAlert()
                    Exit Sub
                End If

                txtDate.DateStart = DT.Rows(0)("START_DATE")
                txtDate.DateEnd = DT.Rows(0)("END_DATE")

                lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("START_DATE")), "", DT.Rows(0)("START_DATE")))
                lblEndDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("END_DATE")), "", DT.Rows(0)("END_DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
            If Session("PRINCIPAL_ID") = 5 Then
                Dim lblRegion As Label = CType(DetailsView1.FindControl("lblRegion"), Label)
                If lblRegion IsNot Nothing Then
                    lblRegion.Text = DT.Rows(0).Item("REGION_NAME")
                End If
            Else
                DetailsView1.Rows(4).Visible = False
            End If

            Show()
            RaiseEvent CloseButton_Click(Me, Nothing)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.PRICEGRPBONUS, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_PrdSearch)
                .ResetPage()
                .TeamCode = TeamCode
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SelectButton_Click
        Try
            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            txtPrdCode.Text = wuc_PrdSearch.PrdCode
            LoadAddNewUOMDdl(wuc_PrdSearch.PrdCode)
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchFOCPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_FOCPrdSearch)
                .ResetPage()
                .TeamCode = TeamCode
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectFOCPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_FOCPrdSearch.SelectButton_Click
        Try
            Dim txtFOCPrdCode As TextBox = CType(DetailsView1.FindControl("txtFOCPrdCode"), TextBox)
            txtFOCPrdCode.Text = wuc_FOCPrdSearch.PrdCode
            LoadAddNewFOCUOMDdl(wuc_FOCPrdSearch.PrdCode)
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchPrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.SearchButton_Click, wuc_FOCPrdSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClosePrdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_PrdSearch.CloseButton_Click, wuc_FOCPrdSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Page.Validate()
            If Page.IsValid = False Then
                Show()
                Exit Sub
            End If

            Dim txtPrdCode As TextBox = CType(DetailsView1.FindControl("txtPrdCode"), TextBox)
            Dim ddlPriceGrp As DropDownList = CType(DetailsView1.FindControl("ddlPriceGrp"), DropDownList)
            Dim txtQty As TextBox = CType(DetailsView1.FindControl("txtQty"), TextBox)
            Dim ddlUOM As DropDownList = CType(DetailsView1.FindControl("ddlUOM"), DropDownList)
            Dim txtFOCPrdCode As TextBox = CType(DetailsView1.FindControl("txtFOCPrdCode"), TextBox)
            Dim txtFOCQty As TextBox = CType(DetailsView1.FindControl("txtFOCQty"), TextBox)
            Dim ddlFOCUOM As DropDownList = CType(DetailsView1.FindControl("ddlFOCUOM"), DropDownList)
            Dim ddlFOCType As DropDownList = CType(DetailsView1.FindControl("ddlFOCType"), DropDownList)
            Dim txtMaxFOCQty As TextBox = CType(DetailsView1.FindControl("txtMaxFOCQty"), TextBox)
            Dim isDuplicate As Integer = 0

            txtQty.Text = IIf(Trim(txtQty.Text) = "", 0, Trim(txtQty.Text))
            txtFOCQty.Text = IIf(Trim(txtFOCQty.Text) = "", 0, Trim(txtFOCQty.Text))
            txtMaxFOCQty.Text = IIf(Trim(txtMaxFOCQty.Text) = "", 0, Trim(txtMaxFOCQty.Text))

            Dim clsBonus As New mst_TradeDeal.clsBonus
            Dim DT As DataTable

            Dim strRegionCode As String = ""

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then

                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Session("PRINCIPAL_ID") = 5 Then
                    Dim ddlRegion As DropDownList = CType(DetailsView1.FindControl("ddlRegion"), DropDownList)
                    strRegionCode = Trim(ddlRegion.SelectedValue)
                End If

                DT = clsBonus.UpdatePriceGrpBonus(PrdCode, PriceGrpCode, Qty, UomCode, StartDate, EndDate, RegionCode, Trim(txtQty.Text), Trim(ddlUOM.SelectedValue), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strRegionCode, _
                        Trim(txtFOCPrdCode.Text), Trim(txtFOCQty.Text), Trim(ddlFOCUOM.SelectedValue), Trim(ddlFOCType.SelectedValue), Trim(txtMaxFOCQty.Text))

                'If DT.Rows.Count > 0 Then
                '    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                'End If
                If DT Is Nothing Then
                    isDuplicate = 1
                Else
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Bonus for this product in this price group with the same date range and quantity already exists!"
                    LoadDvEditMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully saved."
                    LoadDvViewMode()
                End If
            Else
                'HL: 20120217: For CR-MY01-20110628-000056 - MY OTC migration
                If Session("PRINCIPAL_ID") = 5 Then
                    Dim ddlRegion As DropDownList = CType(DetailsView1.FindControl("ddlRegion"), DropDownList)
                    strRegionCode = Trim(ddlRegion.SelectedValue)
                End If

                DT = clsBonus.CreatePriceGrpBonus(Trim(txtPrdCode.Text), Trim(ddlPriceGrp.SelectedValue), Trim(txtQty.Text), Trim(ddlUOM.SelectedValue), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strRegionCode, _
                        Trim(txtFOCPrdCode.Text), Trim(txtFOCQty.Text), Trim(ddlFOCUOM.SelectedValue), Trim(ddlFOCType.SelectedValue), Trim(txtMaxFOCQty.Text))

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "Bonus for this product in this price group with the same date range and quantity already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    lblInfo.Text = "The record is successfully created."
                    PrdCode = txtPrdCode.Text.ToString
                    PriceGrpCode = ddlPriceGrp.SelectedValue.ToString
                    LoadDvViewMode()
                End If
            End If

            Qty = txtQty.Text.ToString
            UomCode = ddlUOM.SelectedValue.ToString
            StartDate = txtDate.DateStart.ToString
            EndDate = txtDate.DateEnd.ToString
            RegionCode = strRegionCode
            BindDetailsView()                       'Rebind the details view

            'ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        ResetPage()
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub txtDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.Load
    '    If PriceGrpBonusID <> "" Then
    '        Show()
    '    End If
    'End Sub

    Protected Sub imgCal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.imgCal_Click
        Show()
    End Sub
End Class

