<%@ Page Language="vb" AutoEventWireup="false" Inherits="SFMSActyList" CodeFile="PlanActivity.aspx.vb" %>


<%@ Register Src="~/include/wuc_pnlRecordNotFound.ascx" TagName="wuc_pnlRecordNotFound" TagPrefix="uc2" %>    
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="uc1" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300"></asp:ScriptManager>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <uc1:wuc_ctrlpanel id="Wuc_ctrlpanel" runat="server">
                                </uc1:wuc_ctrlpanel></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr">
                                    <ContentTemplate>
                                &nbsp;
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                                        </td>
                                    </tr>
                                    <%--<tr class="Bckgroundreport">
                                        <td colspan="3">
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <%--<tr class="Bckgroundreport">
                                                    <td >
                                                        &nbsp;</td>
                                                        <td>
                                                        
                                                        </td>
                                                </tr>--%>
                                                 <tr class="Bckgroundreport">
                                        <td colspan="3" style="height=10px">
                                            &nbsp;</td>
                                    </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td class="Bckgroundreport">
                                                      
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="left" colspan="2" style="width:100%; height: 170px;">
                                                                        <asp:UpdatePanel runat="server" ID="update">
                                                                        <ContentTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td colspan="5">
                                                                                    &nbsp;<%--<uc1:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />--%>
                                                                                </td>
                                                                            </tr>
                                                                        <tr>
                                                                        <td style="width:180px" valign="top">
                                                                            <asp:ListBox ID="lstActivity" AutoPostBack="true" runat="server" CssClass="cls_listbox" Height="200px" Width="100%"></asp:ListBox></td>
                                                                        <td style="width:20px">
                                                                            <asp:Panel ID="Panel1" runat="server" Height="20px" Width="100%">
                                                                            </asp:Panel>
                                                                        </td>
                                                                        <td style="width:280px" valign="top">
                                                                        <ccGV:clsGridView id="dgUnselected" runat="server" CssClass="Grid" RowHighlightColor="AntiqueWhite" Width="100%" PagerSettings-Visible="false" GridWidth="100%" FreezeRows="0" FreezeHeader="True" FreezeColumns="0" EmptyHeaderClass="" CellPadding="2" AllowSorting="True" AllowPaging="false" AddEmptyHeaders="0" AutoGenerateColumns="False" GridHeight="250px">
                                                                       <EmptyDataTemplate>
                                                                           <uc2:wuc_pnlrecordnotfound id="Wuc_pnlRecordNotFound" runat="server" showpanel="true">
</uc2:wuc_pnlrecordnotfound>
                                                                       </EmptyDataTemplate>
                                                                       <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black"  />
                                                                       <FooterStyle CssClass="GridFooter"  />
                                                                       <HeaderStyle CssClass="GridHeader"  />
                                                                       <PagerSettings Visible="False"  />
                                                                       <AlternatingRowStyle CssClass="GridAlternate"  />
                                                                       <RowStyle CssClass="GridNormal"  />
                                                                   </ccGV:clsGridView>
                                                                   <asp:Label id="lblTitle" runat="server" CssClass="cls_label_header" Visible="False">Free Text :</asp:Label>
                                                                            <asp:TextBox ID="txtFreeText" runat="server" CssClass="cls_textbox" TextMode="MultiLine"
                                                                                Visible="False" Width="100%" Height="60px"></asp:TextBox></td>
                                                                        <td valign="middle" style="width:80px">
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                                <td align="center" style="width: 27px" valign="middle">
                                                                                    <asp:Button ID="btnAddAll" runat="server" Text=">>" Width="60px" CssClass="cls_button" OnClick="btnAddAll_Click" /></td>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                                <td align="center" style="width: 27px" valign="middle">
                                                                                </td>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                                <td align="center" style="width: 27px" valign="middle">
                                                                                    <asp:Button ID="btnRemoveAll" runat="server" Text="<<" Width="60px" CssClass="cls_button" OnClick="btnRemoveAll_Click" /></td>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                                <td align="center" style="width: 27px" valign="middle">
                                                                                </td>
                                                                                <td style="width: 10%">
                                                                                </td>
                                                                            </tr>
                                                                        <tr>
                                                                        <td style="width:10%"></td>
                                                                        <td align="center" valign="middle" style="width: 27px">
                                                                            <asp:Button ID="btnAdd" runat="server" Text=">" Width="60px" CssClass="cls_button" OnClick="btnAdd_Click" />
                                                                        </td>
                                                                            
                                                                        <td style="width:10%"></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td style="width:10%"></td>
                                                                        <td style="width:27px"></td>
                                                                        <td style="width:10%"></td>
                                                                        </tr>
                                                                            <tr><td style="width:10%"></td><td align="center" valign="middle" style="width: 27px">
                                                                                <asp:Button ID="btnRemove" runat="server" Text="<" Width="60px" CssClass="cls_button" OnClick="btnRemove_Click" /></td><td style="width:10%"></td></tr>
                                                                        </table>
                                                                        </td>
                                                                        <td style="width:280px"><ccGV:clsGridView id="dgSelected" runat="server" CssClass="Grid" RowHighlightColor="AntiqueWhite" Width="100%" PagerSettings-Visible="false" GridWidth="100%" FreezeRows="0" FreezeHeader="True" FreezeColumns="0" EmptyHeaderClass="" CellPadding="2" AllowSorting="True" AllowPaging="false" AddEmptyHeaders="0" AutoGenerateColumns="False" GridHeight="250px">
                                                                            <FooterStyle CssClass="GridFooter"  />
                                                                            <HeaderStyle CssClass="GridHeader"  />
                                                                            <EmptyDataTemplate>
                                                                                <uc2:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                            </EmptyDataTemplate>
                                                                            <PagerSettings Visible="False"  />
                                                                            <SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black"  />
                                                                            <AlternatingRowStyle CssClass="GridAlternate"  />
                                                                            <RowStyle CssClass="GridNormal"  />
                                                                        </ccGV:clsGridView>
                                                                        </td>
                                                                        </tr>
                                                                            <tr>
                                                                                <td style="width: 120px" valign="top">
                                                                                </td>
                                                                                <td style="width: 40px">
                                                                                </td>
                                                                                <td style="width: 280px">
                                                                                    <asp:Panel ID="Panel2" runat="server" Height="30px" Width="125px">
                                                                                    </asp:Panel>
                                                                                </td>
                                                                                <td style="width: 80px" valign="middle">
                                                                                </td>
                                                                                <td style="width: 280px">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 20%" valign="top">
                                                                                </td>
                                                                                <td>
                                                                                </td>
                                                                                <td align="center" colspan="3">
                                                                                <table>
                                                                                <tr>
                                                                                <td>
                                                                                    <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="cls_button" Width="60px" OnClick="btnOK_Click" /></td>
                                                                                <td>
                                                                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="cls_button" Width="60px" OnClick="btnReset_Click" OnClientClick='return confirm("Are you sure you want to reset this category?");' /></td>
                                                                                <td>
                                                                                    <asp:Button ID="btnSave" runat="server" Text="Save All" CssClass="cls_button" Width="60px" OnClick="btnSave_Click" /></td>
                                                                                </tr>
                                                                                </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                        </td>
                                                                        <%--<td align="right">
                                                                            &nbsp;</td>--%>
                                                                    </tr>
                                                                    <tr>
                                                                    <td align="left" style="width: 123px; height: 10px;">
                                                                            </td>
                                                                    </tr>
                                                                </table>
                                                              
                                                         
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3" style="height: 4px">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
