<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DRCByDistrict.aspx.vb" Inherits="iFFMR_Customize_DRCByDistrict" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_DRCCtrlPnl" Src="./USRCTRL/wuc_DRCCtrlPnl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>DRC By District</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmDRCDistrict" runat="server">
    <div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td>
        <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
         <asp:Panel ID="PnlExport" runat="server" Height="" Width="99%" Visible="true">
                    <customToolkit:wuc_DRCCtrlPnl ID="wuc_DRCCtrlPnl1" runat="server" />
                    </asp:Panel>
     </td></tr> 
     <tr><td>              
        <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="5" width="100%">
        <tr>
            <td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td>
        </tr>
        <tr><td class="BckgroundInsideContentLayout" align="left">
            <table class="Bckgroundreport" border="0" cellpadding="0" cellspacing="0" width="100%"
                                        align="center">
                <tr><td>
                   
                </td></tr>
                <tr>
                    <td><customToolkit:wuc_lblheader id="wuc_lblheader" runat="server" Title="Inventory Forecasting By District"/></td>
                </tr>
                <tr>
                    <td>
                    <table  id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="0" width="100%">
                    
                
                <tr><td>
                    <asp:Label ID="lblMesg" runat="server" Text="" CssClass="cls_label"></asp:Label></td></tr>
                <tr><td valign="top" class="Bckgroundreport" style="padding-top: 10px">
                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                        AllowSorting="true" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                        GridHeight="480" RowSelectionEnabled="true">
                    </ccGV:clsGridView>
                </table>
                
                </td></tr>
            </table>
        </td></tr>
        </table> 
        
        </ContentTemplate></asp:UpdatePanel>
     </td></tr>
    </table>
    </div>
    </form>
</body>
</html>
