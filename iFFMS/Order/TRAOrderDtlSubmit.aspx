<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAOrderDtlSubmit.aspx.vb" Inherits="iFFMS_Order_TRAOrderDtlSubmit" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>TRA Order Detail Confirmation</title>
    <link href="~/include/DKSH.css" rel="stylesheet"/> <%--onload="self.parent.CollapseExpand()"--%>
    <script language="javascript" type="text/javascript">
        var valueChanged = false;
        function changeValue(value)
        { valueChanged = value; }

      
        </script>
</head>
<body class="BckgroundInsideContentLayout" onload="changeValue(true);" >
<form id="frmTRAConfirm" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td style="width: 100%">
                                                <uc1:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uc1:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                             <asp:Label ID="lblmsg" runat="server" CssClass="cls_label_err"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>  <input type="button" runat="server" id="btnBackTolist" value="Back" onclick="self.parent.CollapseExpand();self.parent.RefreshSideBar();changeValue(false);"
                                                                class="cls_button" style="width: 80px"  visible="false"/>
                                                <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" Width="80px" OnClientClick="changeValue(false);" />
                                                 <asp:Button ID="btnAddOrder" runat="server" Text="Add New Order" CssClass="cls_button" Width="80px" Visible="false"  OnClientClick="changeValue(false);"/>
                                                <asp:Button ID="btnedit" runat="server" Text="Edit/Submit Txn"  cssclass="cls_button" width="80px" Visible="false"  OnClientClick="changeValue(false);"/>
                                                 <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="cls_button" Width="80px" 
                                                 OnClientClick="var agree=confirm('System will submit the current transaction! After submit the transaction will not be editable. Are you sure you wish to continue?');if(agree){changeValue(false);return true;}else {return false;}" />
                                                  <asp:Button ID="btnkiv" runat="server" Text="KIV" CssClass="cls_button" Width="80px" 
                                                 OnClientClick="var agree=confirm('System will KIV the current transaction! Are you sure you wish to continue?');if(agree){changeValue(false);return true;}else {return false;}"/>
                                          </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 95%;">
                                                                    <div style="width:800px">
                                                                        <span style="float: left; width: 400px">
                                                                            <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                    Width="35%" Wrap="False" />
                                                                            </asp:DetailsView>
                                                                        </span><span style="float: left; width: 400px">
                                                                            <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                    Width="35%" Wrap="False" />
                                                                            </asp:DetailsView>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="width: 95%;">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                        AllowSorting="true" AutoGenerateColumns="false" Width="98%" FreezeHeader="false"
                                                                        GridHeight="455" RowSelectionEnabled="true">
                                                                    </ccGV:clsGridView>
                                                                    <%--<ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" AddEmptyHeaders="0" CellPadding="2" GridHeight="440"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <uc1:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td >
                                             <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
<script type="text/javascript" language="javascript">
    window.document.body.onbeforeunload = function() { if (valueChanged == true) { return "All modification done will not be save!"; } }
   
</script>
</html>
