'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	23/10/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsServiceStatistic
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsServiceStatistic"
        End Get
    End Property

    Public Function GetServiceStatisticHdr(ByVal strSalesOffCode As String, ByVal strSvcStatus As String, _
                                    ByVal strReqStartDateFrom As String, ByVal strReqStartDateTo As String, _
                                    ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SVC_STAT_HDR"
                .addItem("SALESOFF_CODE", strSalesOffCode, clsDB.DataType.DBString)
                .addItem("SVC_STATUS", strSvcStatus, clsDB.DataType.DBString)
                .addItem("REQ_START_DATE_FROM", strReqStartDateFrom, clsDB.DataType.DBString)
                .addItem("REQ_START_DATE_TO", strReqStartDateTo, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetServiceStatisticDtl(ByVal strSalesOffCode As String, ByVal strSvcStatus As String, _
                                    ByVal strReqStartDateFrom As String, ByVal strReqStartDateTo As String, _
                                    ByVal strUserID As String) As DataTable
        Dim objDB As clsDB

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SVC_STAT_DTL"
                .addItem("SALESOFF_CODE", strSalesOffCode, clsDB.DataType.DBString)
                .addItem("SVC_STATUS", strSvcStatus, clsDB.DataType.DBString)
                .addItem("REQ_START_DATE_FROM", strReqStartDateFrom, clsDB.DataType.DBString)
                .addItem("REQ_START_DATE_TO", strReqStartDateTo, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class

