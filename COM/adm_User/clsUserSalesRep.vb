'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	16/11/2006
'	Purpose	    :	Class User SalesRep
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel

'Interface Icor_UserSalesRep
'    Function GetTemplateList(ByVal UserSalesRepID As Long) As DataTable
'End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Required), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsUserSalesRep
    'Inherits ServicedComponent
    'Implements Icor_UserSalesRep
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New clsProperties.clsUserSalesRep

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Create
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Create()
        Dim objDB As cor_DB.clsDB
        'Dim lngUserSalesRepID As Long

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERSALESREPCREATE"
                '.addItem("usersalesrep_id", clsProperties.usersalesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_code", clsProperties.salesrep_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_id", clsProperties.salesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_id", clsProperties.salesteam_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                'lngUserSalesRepID = .spInsert()
                .spInsert()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserSalesRep.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Update()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERSALESREPUPDATE"
                .addItem("usersalesrep_id", clsProperties.usersalesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_id", clsProperties.user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_code", clsProperties.salesrep_code, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_id", clsProperties.salesrep_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("salesteam_id", clsProperties.salesteam_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("created_date", clsProperties.created_date, cor_DB.clsDB.DataType.DBDatetime)
                '.addItem("creator_user_id", clsProperties.creator_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("changed_date", clsProperties.changed_date, cor_DB.clsDB.DataType.DBDatetime)
                .addItem("changed_user_id", clsProperties.changed_user_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("delete_flag", clsProperties.delete_flag, cor_DB.clsDB.DataType.DBInt)
                .spUpdate()
            End With
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserSalesRep.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Update
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Sub Delete()
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USERSALESREPDELETE"
                .addItem("user_code", clsProperties.user_code, cor_DB.clsDB.DataType.DBString)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_User.clsUserSalesRep.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserSalesRepID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
