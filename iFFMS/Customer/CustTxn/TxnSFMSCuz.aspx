﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnSFMSCuz.aspx.vb" Inherits="iFFMS_Customer_CustTxn_TxnSFMSCuz" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Transaction SFMS Cuz</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">


        function ValidateCheckBoxStates() {
            var dglist = document.getElementById('dgList');
            var selected; selected = 0;
            for (var i = 1; i < dglist.rows.length; i++) {
                if (dglist.rows[i].cells[0].childNodes[0].childNodes[0]) {
                    if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].type == "checkbox") {
                        if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].checked) selected = selected + 1;
                    }
                }
            }
            if (selected > 0)
            { var agree = confirm('Are you sure you want to continue?'); if (agree) return true; else return false; }
            else { alert('Kindly select by checking the checkbox in the list!'); return false; }
        }

        function getDglist(element, idx) { var dglist; idx = idx + 1; if (element) { if (element.tagName == 'TABLE') { dglist = element; } else if (idx < 10) { dglist = getDglist(element.parentElement, idx); } } return dglist; }
        function ChangeAllCheckBoxStates(element) { var dglist = getDglist(element, 1); var checkState = element.status; for (var i = 1; i < dglist.rows.length; i++) { if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].childNodes[0].checked = checkState; } }
        function ChangeHeaderCheckBoxStates(element) { var dglist = getDglist(element, 1); var checkTrueStateCount = 0; for (var i = 1; i < dglist.rows.length; i++) { if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].type == "checkbox") if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].checked == true) { checkTrueStateCount = checkTrueStateCount + 1; } } if (checkTrueStateCount == dglist.rows.length - 1) { dglist.rows[0].cells[0].childNodes[0].childNodes[0].checked = true; } else { dglist.rows[0].cells[0].childNodes[0].childNodes[0].checked = false; } }

    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe')"
    style="background-color: #DDDDDD;">
    <form id="frmtxnsfms" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <fieldset class="" style="width: 98%;">
        <div id="title">
            <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" Visible="false"></customToolkit:wuc_lblheader>
            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
        </div>
        <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
            width="100%" border="0" style="height: 30px">
            <tr align="left" valign="bottom">
                <td>
                    <asp:Image ID="imgGeneralInfo" runat="server" ImageUrl="~/images/ico_general_info.gif"
                        CssClass="cls_button" ToolTip="Info" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="pnlGeneralInfo" runat="server" CssClass="cls_ctrl_panel">
            <table class="cls_panel" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" style="text-align: left">
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Field Force Name</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 300px">
                                        <asp:Label ID="lblFieldForceName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Customer Name</span>
                                        <td style="width: 5px">
                                            <span class="cls_label_header">:</span>
                                        </td>
                                        <td style="width: 300px">
                                            <asp:Label ID="lblCustName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                        </td>
                                        <td style="width: 100px">
                                            <span class="cls_label_header">Contact Name</span>
                                            <td style="width: 5px">
                                                <span class="cls_label_header">:</span>
                                            </td>
                                            <td style="width: 300px">
                                                <asp:Label ID="lblContName" CssClass="cls_label" runat="server" EnableViewState="false" />
                                            </td>
                            </tr>
                            <tr align="left">
                                <td style="width: 100px">
                                    <span class="cls_label_header">Visit ID</span>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblVisitId" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                                    <td style="width: 100px">
                                        <span class="cls_label_header">Map</span>
                                    </td>
                                    <td style="width: 5px">
                                        <span class="cls_label_header">:</span>
                                    </td>
                                    <td align="left" colspan="4">
                                        <asp:Label ID="lblMapPath" CssClass="cls_label" runat="server" EnableViewState="false" />
                                    </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlGeneralInfo" runat="server" CollapseControlID="imgGeneralInfo"
                ExpandControlID="imgGeneralInfo" TargetControlID="pnlGeneralInfo" CollapsedSize="0"
                Collapsed="false" ExpandDirection="Vertical" SuppressPostBack="true">
            </ajaxToolkit:CollapsiblePanelExtender>
        </asp:Panel>
        <div id="txnsfms">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:Button runat="server" ID="btnHidden" Style="display: none;" Text="" Visible="false" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                    <div id="Proce" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                        background-color: Gray">
                        <span style="float: right;">
                            <asp:Button ID="btnsubmit" runat="server" Text="Proceed >>" CssClass="cls_button"
                                Width="80px" />
                        </span>
                    </div>
                    <div style="width: 100%; position: relative; padding: 0; margin: 0;">
                        <asp:UpdatePanel ID="UpdateHdr" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <div id="dheader" class="S_DivHeader">
                                    General Information
                                </div>
                                <asp:Panel ID="pSFMSHdr" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div style="width: 98%; position: relative; padding-left: 5px; margin: 0;">
                                        <table class="cls_form_table">
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Txn No:</span>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lbltxnno" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label">Remarks: </span>
                                                </td>
                                                <td width="40%">
                                                    <asp:TextBox ID="txtrmks" runat="server" CssClass="cls_textbox" ValidationGroup="SFMSHdr"
                                                        Width="400px" MaxLength="249" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpepSFMSHdr" runat="server" TargetControlID="pSFMSHdr"
                                    ExpandControlID="dheader" CollapseControlID="dheader">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateDtl" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlDtl" runat="server" Enabled="False" Interval="100" OnTick="TimerControlDtl_Tick" />
                                <div id="ddtl" class="S_DivHeader">
                                    Detail Information - Add New
                                </div>
                                <asp:Panel ID="pSFMSDtl" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <div style="width: 98%; padding-bottom: 5px; padding-left: 5px;">
                                        <asp:Button ID="btnsavedtl" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="SFMSDtl"
                                            Width="80px" />
                                    </div>
                                    <div>
                                        <div style="width: 98%; padding-left: 5px;">
                                            <span class="cls_label_header">Category:
                                                <asp:DropDownList ID="ddlcatcode" runat="server" CssClass="cls_dropdownlist" Width="150px"
                                                    ValidationGroup="SFMSHdr" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </span>
                                        </div>
                                        <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                            padding-bottom: 10px;">
                                            <ccGV:clsGridView ID="dglistdtl" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                Width="80%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="SUB_CAT_CODE,CAT_CODE">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="AllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelection" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CAT_CODE" HeaderText="Category Code" ReadOnly="True" SortExpression="CAT_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SUB_CAT_CODE" HeaderText="Field Act. Code" ReadOnly="True"
                                                        SortExpression="SUB_CAT_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SUB_CAT_NAME" HeaderText="Field Act. Desc." ReadOnly="True"
                                                        SortExpression="SUB_CAT_NAME">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Quantity">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtqty" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                                Style="text-align: right; border-style: ridge;" Text=""> </asp:TextBox>
                                                            <asp:CompareValidator ID="cvtxtqty" runat="server" ControlToValidate="txtqty" ValidationGroup="SFMSDtl"
                                                                Display="Dynamic" Operator="DataTypeCheck" Type="Double" ErrorMessage="Enter numeric only"
                                                                CssClass="cls_label_header">
                                                            </asp:CompareValidator>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="right" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtdtlrmks" runat="server" MaxLength="249" Width="350px" CssClass="cls_textbox"
                                                                Style="text-align: left; border-style: ridge;" Text=""> </asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </ccGV:clsGridView>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeSFMSDtl" runat="server" TargetControlID="pSFMSDtl"
                                    ExpandControlID="ddtl" CollapseControlID="ddtl">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                <div id="Div1" style="width: 99.8%; height: 20px; position: relative; border: solid 1px black;
                                    background-color: Gray">
                                    <span style="float: right;">
                                        <asp:Button ID="btnProceeed" runat="server" Text="Proceed >>" CssClass="cls_button"
                                            Width="80px" />
                                    </span>
                                </div>
                                <div id="divsumm" class="S_DivHeader">
                                    <span style="float: left;">Detail Information - View/Delete/Edit</span>
                                </div>
                                <asp:Panel ID="pgridview" runat="server" CssClass="cls_panel_header" Width="99.8%">
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 10px; padding-top: 5px;">
                                        <div>
                                            <asp:Button ID="btndeletesum" runat="server" Text="Delete" CssClass="cls_button"
                                                Width="80px" Visible="false" CausesValidation="false" ValidationGroup="SFMSSum"
                                                OnClientClick="return  ValidateCheckBoxStates();" />
                                            <asp:Button ID="btnsavesum" runat="server" Text="Save" CssClass="cls_button" Width="80px"
                                                ValidationGroup="SFMSSum" Visible="false" />
                                        </div>
                                        <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                            padding-bottom: 10px;">
                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                                Width="100%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="CAT_CODE,SUB_CAT_CODE,QTY">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="DeleteAllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkdelete" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TXN_NO" HeaderText="Txn No." ReadOnly="True" SortExpression="TXN_NO">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="TXN_STATUS" HeaderText="Status" ReadOnly="True" SortExpression="TXN_STATUS">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CAT_CODE" HeaderText="Category Code" ReadOnly="True" SortExpression="CAT_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CAT_NAME" HeaderText="Category" ReadOnly="True" SortExpression="CAT_NAME">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SUB_CAT_CODE" HeaderText="Field Act. Code" ReadOnly="True"
                                                        SortExpression="SUB_CAT_CODE">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="SUB_CAT_NAME" HeaderText="Field Act. Desc." ReadOnly="True"
                                                        SortExpression="SUB_CAT_NAME">
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Quantity">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtqtysum" runat="server" MaxLength="7" Width="50px" CssClass="cls_textbox"
                                                                Style="text-align: right; border-style: ridge;" Text='<%#Bind("QTY") %>'> </asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvtxtqtysum" runat="server" ErrorMessage="Enter quantity"
                                                                ControlToValidate="txtqtysum" Display="Dynamic" CssClass="cls_label_header" ValidationGroup="SFMSSum">
                                                            </asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="cvtxtqtysum" runat="server" ControlToValidate="txtqtysum"
                                                                ValidationGroup="SFMSSum" Display="Dynamic" Operator="DataTypeCheck" Type="Double"
                                                                ErrorMessage="Enter numeric only" CssClass="cls_label_header">
                                                            </asp:CompareValidator>
                                                            <asp:DropDownList ID="ddlqty" runat="server" CssClass="cls_dropdownlist" Width="50px"
                                                                Style="text-align: right;">
                                                                <asp:ListItem Text="0.0" Value="0.0" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="0.1" Value="0.1"></asp:ListItem>
                                                                <asp:ListItem Text="0.2" Value="0.2"></asp:ListItem>
                                                                <asp:ListItem Text="0.3" Value="0.3"></asp:ListItem>
                                                                <asp:ListItem Text="0.4" Value="0.4"></asp:ListItem>
                                                                <asp:ListItem Text="0.5" Value="0.5"></asp:ListItem>
                                                                <asp:ListItem Text="0.6" Value="0.6"></asp:ListItem>
                                                                <asp:ListItem Text="0.7" Value="0.7"></asp:ListItem>
                                                                <asp:ListItem Text="0.8" Value="0.8"></asp:ListItem>
                                                                <asp:ListItem Text="0.9" Value="0.9"></asp:ListItem>
                                                                <asp:ListItem Text="1.0" Value="1.0"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="right" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtdtlrmksSum" runat="server" MaxLength="249" Width="350px" CssClass="cls_textbox"
                                                                ValidationGroup="SFMSSum" Style="text-align: left; border-style: ridge;" Text='<%#Bind("REMARKS") %>'> </asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </ccGV:clsGridView>
                                        </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpepgridview" runat="server" TargetControlID="pgridview"
                                    ExpandControlID="divsumm" CollapseControlID="divsumm">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </fieldset>
    <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    </form>
</body>
</html>
