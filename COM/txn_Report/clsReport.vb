'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	8/12/2006
'	Purpose	    :	Class to build datatables for getting report details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsReport
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsReport"
        End Get
    End Property


    Function GetRptList(ByVal strSalesrepCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_REPORTLIST"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdDetails :" & ex.Message))
        End Try
    End Function

    Function GetRptHdr(ByVal strSalesrepCode As String, ByVal strReportCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_REPORTHDR"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("REPORT_CODE", strReportCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdDetails :" & ex.Message))
        End Try
    End Function

    Function GetRptDtl(ByVal strSalesrepCode As String, ByVal strReportCode As String, ByVal strUserId As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_REPORTDTL"
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("REPORT_CODE", strReportCode, clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPrdDetails :" & ex.Message))
        End Try
    End Function

#Region "OLD SPP"

    Function getReportHeader(ByVal strSalesmanCode As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_REPORTLISTHDR"
                .addItem("salesmancode", strSalesmanCode, 2)

                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getReportHeader :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getReportDetail(ByVal strSalesmanCode As String, ByVal strReportNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_REPORTLISTDTL"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("reportcode", strReportNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getReportDetail :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Function getReportTitle(ByVal strSalesmanCode As String, ByVal strReportNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_REPORTTITLE"
                .addItem("salesrepcode", strSalesmanCode, 2)
                .addItem("reportcode", strReportNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getReportTitle :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function
#End Region

#Region "Exception"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region


End Class
