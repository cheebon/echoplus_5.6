'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	UserType User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlUserType
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlUserTypeID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlUserTypeID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlUserTypeID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlUserTypeID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlUserTypeID.SelectedValue = "0"
    '        Else
    '            ddlUserTypeID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlUserTypeID.SelectedValue = String.Empty Then
                Return 0
            End If
            'Return Long.Parse(ddlUserTypeID.SelectedValue)
            Return ddlUserTypeID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlUserTypeID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlUserTypeID.SelectedValue = "0"
            Else
                ddlUserTypeID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlUserTypeID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlUserTypeID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlUserTypeID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlUserTypeID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlUserTypeID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlUserTypeID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlUserTypeID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlUserTypeID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlUserTypeID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvUserTypeID.Visible 'cfvUserTypeID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvUserTypeID.Enabled = Value
            cfvUserTypeID.Visible = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objUserQuery As adm_User.clsUserQuery
        Dim drRow As DataRow

        Try
            If ddlUserTypeID.Items.Count <= 0 Then
                'Get UserType List
                objUserQuery = New adm_User.clsUserQuery
                With objUserQuery
                    dt = .GetUserTypeList
                End With
                objUserQuery = Nothing

                ddlUserTypeID.Items.Clear()
                ddlUserTypeID.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlUserTypeID.Items.Add(New ListItem(Trim(drRow("user_type_name")), Trim(drRow("user_type_id")) & "@" & Trim(drRow("user_type_code"))))
                    Next
                End If
            End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlUserType.DataBind : " & ex.ToString))
        Finally
            objUserQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


