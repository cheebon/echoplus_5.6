<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlRegion.ascx.vb" Inherits="include_wuc_ddlRegion" %>
<asp:DropDownList id="ddlRegionID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator id="cfvRegionID" runat="server" ControlToValidate="ddlRegionID" Operator="GreaterThan"
	ValueToCompare="0" Display="Dynamic" CssClass="cls_validator"></asp:CompareValidator>