﻿Imports System.Data
Partial Class iFFMR_Customize_CallRemainingReport
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

#Region "Local Variable"
    Dim aryDataItem As New ArrayList

    Dim licItemFigureCollector As ListItemCollection

    'Dim _licItemFigureCollector As ListItemCollection
    'Protected Property licItemFigureCollector() As ListItemCollection
    '    Get
    '        If _licItemFigureCollector Is Nothing Then _licItemFigureCollector = Session("ItemFigureCollector_CallAnalysisListByDay")
    '        If _licItemFigureCollector Is Nothing Then _licItemFigureCollector = New ListItemCollection
    '        Return _licItemFigureCollector
    '    End Get
    '    Set(ByVal value As ListItemCollection)
    '        _licItemFigureCollector = value
    '        Session("ItemFigureCollector_CallAnalysisListByDay") = value
    '    End Set
    'End Property

    Public Property blnHasPreviousPage() As Boolean
        Get
            Return CBool(ViewState("HasPreviousPage"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("HasPreviousPage") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CallAnalysisListByDay"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
        End Set
    End Property
    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
#End Region


    Public ReadOnly Property PageName() As String
        Get
            Return "CallRemainingReport"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Try

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        Dim intYear As Integer '= Session("Year")
        Dim intMonth As Integer

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.REMAIN_CALL) '"Call Analysis List By Day"
            .DataBind()
            .Visible = True
        End With

        'Call Panel
        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.REMAIN_CALL
            .DataBind()
            .Visible = True
        End With

        'Call Paging
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

        
            ' ViewState("dtCurrentView") = Nothing
            ViewState("SessionMonth") = Session("MONTH")
            ViewState("SessionYear") = Session("YEAR")

            TimerControl1.Enabled = True 

        End If
        ViewState("MonthInThisPage") = intMonth
        ViewState("YearInThisPage") = intYear

        lblErr.Text = ""

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'Finally
        'End Try

    End Sub

#Region "EVENT HANDLER"
    

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        '   Try
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        '   Try
        'ChangeReportType()
        ViewState("MonthInThisPage") = Session("MONTH")
        ViewState("YearInThisPage") = Session("YEAR")
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

 

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        '  Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        '   Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        'UpdateDatagrid_Update
        RefreshDatabinding()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        'Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False

        ViewState("EXPORT") = True
        RefreshDatabinding()

        'wuc_ctrlPanel.ExportToFile(dgList, "CallAnalysisByDay")
        wuc_ctrlpanel.ExportToFile(dgList, PageName)
        'wuc_ctrlpanel.ExportToFile(PrntPanel, PageName)
        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'Finally
        '    ViewState("EXPORT") = False
        'End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ' ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlPanel.RefreshDetails()
        'wuc_ctrlPanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        'Try
        ' If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        ' ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
        End If



        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If


        dgList.DataSource = dvCurrentView
        dgList.PageSize = intPageSize
        dgList.DataBind()

        'Call Paging
        With wuc_dgpaging
            .PageCount = dgList.PageCount
            .CurrentPageIndex = dgList.PageIndex
            .Visible = IIf(dgList.Rows.Count = 0, False, True)
        End With
 
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        ' Try
        If dgList.Rows.Count < 12 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '  Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Try
        dgList.Columns.Clear()
        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
            Select Case CF_CallRemainingReport.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CallRemainingReport.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_CallRemainingReport.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallRemainingReport.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallRemainingReport.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    'dgColumn.DataTextFormatString = "{0}" ' & Eval(ColumnName)
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFields(0) As String

                    'FORM WHEN ROW CREATED, HERE IS NOT USED
                    Dim strUrlFormatString As String = Nothing
                    dgColumn.DataNavigateUrlFields = strUrlFields
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing



                    'Add the field name
                    aryDataItem.Add(ColumnName)

                Case FieldColumntype.InvisibleColumn

                Case FieldColumntype.BoundColumn
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_CallRemainingReport.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataFormatString = CF_CallRemainingReport.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_CallRemainingReport.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_CallRemainingReport.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    ''Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        'Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow

        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'Finally
        'End Try

    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        '   Try
        Select Case e.Row.RowType
            Case DataControlRowType.Header
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex > 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_CallRemainingReport.GetOutputFormatString(li.Text), CDbl(li.Value))
                    End If
                Next

        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        '    Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        '      Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".DIVISION : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        '  Try
        Dim dblValue As Double = 0
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToInt : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        '   Try
        Dim strYear, strMonth, strUserID, strPrincipalCode, strPrincipalID As String



        If ViewState("SessionMonth") <> Session("MONTH") Then
            ViewState("SessionMonth") = Session("MONTH")
            ViewState("MonthInThisPage") = Session("MONTH")
        End If
        If ViewState("SessionYear") <> Session("YEAR") Then
            ViewState("SessionYear") = Session("YEAR")
            ViewState("YearInThisPage") = Session("YEAR")
        End If

        'strYear = Session.Item("Year")
        strYear = Session("YEAR")
        strMonth = Session("MONTH")
        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")

    

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .Year = strYear
            .Month = strMonth
            .PrincipalID = strPrincipalID
            .PrincipalCode = strPrincipalCode
            .Tree_Path = Session("TREE_PATH")
        End With

        Dim strSalesRepList As String
        strSalesRepList = Session("SALESREP_LIST")

        Dim clsCallRemaining As New rpt_Customize.clsCallRemaining
        DT = clsCallRemaining.GetCallRemaining(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList)
        'dgList_Init(DT)
        PreRenderMode(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    'Run Each time reNew the databinding
    'To generate information inorder to build the custom header
    Private Sub PreRenderMode(ByRef DT As DataTable)
        '     Try
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_ItemFigureCollector(DT)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub

   

    Private Sub Cal_ItemFigureCollector(ByVal DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String 'OrElse strColumnName Like "*DAY" _
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If (strColumnName = "VISIT_FREQ" OrElse _
             strColumnName = "ACT_CALL" OrElse strColumnName = "REMAIN_CALL") Then

                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'licItemFigureCollector = _licItemFigureCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        ' Try
        If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        CriteriaCollector.SortExpression = strSortExpression
        SortingExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region


End Class

Public Class CF_CallRemainingReport

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "VISIT_FREQ"
                strFieldName = "Visit Freq."
            Case "ACT_CALL"
                strFieldName = "Act. Call"
            Case "REMAIN_CALL"
                strFieldName = "Remaining Call"
            Case "STATE"
                strFieldName = "State"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)

        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        '   Try
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        If (strColumnName Like "DAY_*_DEF") OrElse _
            (strColumnName Like "REGION*") OrElse _
            (strColumnName = "YEAR") OrElse (strColumnName = "MONTH") Then
            FCT = FieldColumntype.InvisibleColumn


        End If

        Return FCT
        'Catch ex As Exception

        'End Try
    End Function


    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        'Try

        Select Case strNewName
            Case "CALL_ACH", "ACT_FIELD_DAY_HALF"
                strStringFormat = "{0:0.0}"
            Case "VISIT_FREQ", "ACT_CALL", "REMAIN_CALL"
                strStringFormat = "{0:0.##}"
            Case Else
                strStringFormat = ""
        End Select

        
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        ' Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
            OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
            OrElse strColumnName Like "TIME_*" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function
End Class

