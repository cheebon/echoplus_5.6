<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ConnectionStringDtl.aspx.vb" Inherits="Admin_ConnectionString_ConnectionStringDtl" %>
<%@ Register Src="../../include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmUserDtl" method="post" runat="server">
        <TABLE id="tbl1" cellSpacing="0" cellPadding="0" width="98%" border="0">
		   <%-- <tr><td colspan="3"><uc1:wuc_lblHeader ID="Wuc_lblHeader" runat="server" /></td></tr>--%>
			<tr><td>&nbsp;</td></tr>
			<tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
			<tr>			    
                <td class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                        <tr>
                            <%--<td width="16"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopindicator.gif" border="0"/></td>--%>
                            <td colspan="3">
                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                            </td>
                            <%--<td width="9"><img src="<%=ConfigurationManager.AppSettings("ServerName")%>/images/RptTopEnd.gif" border="0"/></td>--%>
                        </tr>
                        <tr><td class="BckgroundBenealthTitle" colspan="3" height="5"></td></tr>
                        <tr class="Bckgroundreport"><td colspan="3"></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td valign="top" class="Bckgroundreport">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class="Bckgroundreport">
                                        <td>&nbsp;</td>                                                   
                                    </tr>
                                    <tr class="Bckgroundreport">
				                        <td style="height: 95px">
					                        <table width="100%" cellpadding="0" cellspacing="0" align="left" border="0">					                            
					                            <tr>
							                        <td width="20%"><asp:label id="lblStatus" runat="server" CssClass="cls_label_header">Status</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot1" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblStatusValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr><td>&nbsp;</td></tr>						                        
						                        <tr>
							                        <td width="20%" style="height: 19px"><asp:label id="lblCommID" runat="server" CssClass="cls_label_header">Setting ID</asp:label></td>
							                        <td width="2%" style="height: 19px"><asp:label id="lblDot2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 19px"><asp:label id="lblCommIDValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCommCode" runat="server" CssClass="cls_label_header">Setting Code</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblCommCodeValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblCommName" runat="server" CssClass="cls_label_header">Setting Name</asp:label></td>
							                        <td width="2%"><asp:label id="lblDot6" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblCommNameValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%" style="height: 19px"><asp:label id="lblServerIP" runat="server" CssClass="cls_label_header">Server IP</asp:label></td>
							                        <td width="2%" style="height: 19px"><asp:label id="Label2" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 19px"><asp:label id="lblServerIPValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblServerName" runat="server" CssClass="cls_label_header">Server Name</asp:label></td>
							                        <td width="2%"><asp:label id="Label5" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblServerNameValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblDBIP" runat="server" CssClass="cls_label_header">DB IP</asp:label></td>
							                        <td width="2%"><asp:label id="Label8" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblDBIPValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%" style="height: 19px"><asp:label id="lblDBPort" runat="server" CssClass="cls_label_header">DB Port</asp:label></td>
							                        <td width="2%" style="height: 19px"><asp:label id="Label11" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 19px"><asp:label id="lblDBPortValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%" style="height: 19px"><asp:label id="lblDBName" runat="server" CssClass="cls_label_header">DB Name</asp:label></td>
							                        <td width="2%" style="height: 19px"><asp:label id="Label14" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 19px"><asp:label id="lblDBNameValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblLinkServerName" runat="server" CssClass="cls_label_header">Link Server Name</asp:label></td>
							                        <td width="2%"><asp:label id="Label17" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblLinkServerNameValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%" style="height: 19px"><asp:label id="lblDBUserID" runat="server" CssClass="cls_label_header">DB User ID</asp:label></td>
							                        <td width="2%" style="height: 19px"><asp:label id="Label20" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4" style="height: 19px"><asp:label id="lblDBUserIDValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblURL" runat="server" CssClass="cls_label_header">URL</asp:label></td>
							                        <td width="2%"><asp:label id="Label23" runat="server"  CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan="4"><asp:label id="lblURLValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>
						                        <tr>
							                        <td width="20%"><asp:label id="lblProxy" runat="server" CssClass="cls_label_header">Proxy</asp:label></td>
							                        <td width="2%"><asp:label id="Label26" runat="server" CssClass="cls_label_header">:</asp:label></td>
							                        <td colspan=4><asp:label id="lblProxyValue" runat="server" CssClass="cls_label"></asp:label></td>
						                        </tr>						                        						                        
					                        </table>
				                        </td>
			                        </tr>
			                        <tr class="Bckgroundreport">
				                        <td align="right">
                                            &nbsp; &nbsp; &nbsp;&nbsp;
					                        <asp:button id="btnDelete" runat="server" Text="Delete" CssClass="cls_button"></asp:button>
					                        <asp:button id="btnEdit" runat="server" CssClass="cls_button" Text="Edit"></asp:button>
                                            <asp:button id="btnClose" runat="server" CssClass="cls_button" Text="Close"></asp:button>
				                        </td>
			                        </tr>
                                    <%--<tr>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td class="Bckgroundreport"></td>
                                    </tr>--%>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport"><td colspan="3">&nbsp;</td></tr>
                    </table> 
			    </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</TABLE> 	
	</form>
</body>
</html>
<%'List function called by in-line scripts%>		
