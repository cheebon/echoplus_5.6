﻿Imports System.ComponentModel

Public Class clsPushNotification
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public Function GetMessageList(searchtype As String, searchvalue As String, usercode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_MSG_LIST"
                .addItem("SEARCH_TYPE", searchtype, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", searchvalue, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_CODE", usercode, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSalesrepList(usercode As String, teamCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_SALESREP_LIST"
                .addItem("USER_CODE", usercode, cor_DB.clsDB.DataType.DBString)
                .addItem("TEAM_CODE", teamCode, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetHistoryList(strMsgId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_HISTORY_LIST"
                .addItem("MSG_ID", strMsgId, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub CreateMessage(ByVal strTitle As String, ByVal strMsg As String, ByVal strAction As String,
                             ByVal strUrl As String, ByVal expiryDays As Integer, ByVal strSalesrepCode As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_MSG_CREATE"
                .addItem("TITLE", strTitle, cor_DB.clsDB.DataType.DBString)
                .addItem("MSG_TEXT", strMsg, cor_DB.clsDB.DataType.DBString)
                .addItem("ACTION", strAction, cor_DB.clsDB.DataType.DBString)
                .addItem("URL", strUrl, cor_DB.clsDB.DataType.DBString)
                .addItem("EXPIRY_DAYS", expiryDays, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetToken(ByVal strID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_GET_TOKEN"
                .addItem("ID_LIST", strID, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteMessage(ByVal strMsg As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_MSG_DELETE"
                .addItem("MSG_ID", strMsg, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub SendMessage(strMsgId As String, salesrepList As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_PUSH_NOTIFICATION_MSG_SEND"
                .addItem("MSG_ID", strMsgId, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_LIST", salesrepList, cor_DB.clsDB.DataType.DBString)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception

        Public Sub New(ByVal msg As Object)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region


End Class
