Imports System.Data

Partial Class iFFMA_FieldForceActy_FieldActySubCatPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property CatCode() As String
        Get
            Return Trim(hdCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdCatCode.Value = value
        End Set
    End Property

    Public Property SubCatCode() As String
        Get
            Return Trim(hdSubCatCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubCatCode.Value = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        'SubCatID = ""
        txtDate.DateStart = ""
        txtDate.DateEnd = ""
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False
        tcResult.Tabs(1).Enabled = True
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True
        tcResult.ActiveTabIndex = 0
        tcResult.Tabs(1).Enabled = False

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True
        tcResult.Tabs(1).Enabled = True
        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy

            DT = clsFieldActy.GetSubCatDetails(TeamCode, CatCode, SubCatCode)
            If DT.Rows.Count > 0 Then
                txtDate.DateStart = DT.Rows(0)("START_DATE")
                txtDate.DateEnd = DT.Rows(0)("END_DATE")

                lblStartDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("START_DATE")), "", DT.Rows(0)("START_DATE")))
                lblEndDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("END_DATE")), "", DT.Rows(0)("END_DATE")))
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Show()


            FieldActyExtraSubCatList1.TeamCode = TeamCode
            FieldActyExtraSubCatList1.CatCode = CatCode
            FieldActyExtraSubCatList1.SubCatCode = SubCatCode

            FieldActyExtraSubCatList1.BindExtrCatCodeList()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

           

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.FIELDACTY, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Page.Validate("SaveSubCat")
            If Page.IsValid = False Then
                Show()
                Exit Sub
            End If

            Dim txtSubCatCode As TextBox = CType(DetailsView1.FindControl("txtSubCatCode"), TextBox)
            Dim lblSubCatCode As Label = CType(DetailsView1.FindControl("lblSubCatCode"), Label)
            Dim txtSubCatName As TextBox = CType(DetailsView1.FindControl("txtSubCatName"), TextBox)
            Dim ddlCameraMandatory As DropDownList = CType(DetailsView1.FindControl("ddlCameraMandatory"), DropDownList)
            Dim chkImplemented As CheckBox = CType(DetailsView1.FindControl("chkImplemented"), CheckBox)
            Dim clsFieldActy As New mst_FieldForceActy.clsFieldActy
            Dim strCameraInd As String = ddlCameraMandatory.SelectedItem.Value
            Dim strImplemented As String = If(chkImplemented.Checked, "YES", "NO")
            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsFieldActy.UpdateSubCat(TeamCode, CatCode, Trim(lblSubCatCode.Text), Trim(txtSubCatName.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strCameraInd, strImplemented)
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim strIsDeleted As String = ""

                DT = clsFieldActy.CreateSubCat(TeamCode, CatCode, Trim(txtSubCatCode.Text), Trim(txtSubCatName.Text), Trim(txtDate.DateStart), Trim(txtDate.DateEnd), strCameraInd, strImplemented)
                Dim isDuplicate As Integer

                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                    strIsDeleted = DT.Rows(0)("IS_DELETED")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Sub Category Code already exists!"
                    SubCatCode = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                ElseIf strIsDeleted = "Y" Then
                    lblInfo.Text = "This Sub Category Code is used before, please create a new one!"
                    SubCatCode = ""
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    SubCatCode = Trim(txtSubCatCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    SubCatCode = Trim(txtSubCatCode.Text)
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view

            'ResetPage()
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
    '    Try
    '        ResetPage()
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    'Protected Sub txtDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.Load
    '    If SubCatID <> "" Then
    '        Show()
    '    End If
    'End Sub

    Protected Sub imgCal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDate.imgCal_Click
        Show()
    End Sub

    Protected Sub DetailsView1_DataBound(sender As Object, e As EventArgs) Handles DetailsView1.DataBound
        Dim ddlCameraMandatory As DropDownList = TryCast(DetailsView1.FindControl("ddlCameraMandatory"), DropDownList)
        If DetailsView1.CurrentMode = DetailsViewMode.Edit Or DetailsView1.CurrentMode = DetailsViewMode.ReadOnly Then
            Dim drv As DataRowView = DetailsView1.DataItem
            Dim ddlItem = ddlCameraMandatory.Items.FindByValue(drv("camera_ind").ToString())
            If ddlItem IsNot Nothing Then
                ddlCameraMandatory.ClearSelection()
                ddlCameraMandatory.SelectedIndex = -1
                ddlItem.Selected = True
            End If
        End If
    End Sub
End Class



