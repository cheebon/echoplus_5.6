Option Explicit On

Imports System.Data

Partial Class iFFMR_Customize_USRCTRL_wuc_paneldrc
    Inherits System.Web.UI.UserControl
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
    Public Property SRSelectedValue() As String
        Get
            
            Return ddlSalesRep.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlSalesRep.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlSalesRep.SelectedValue = "0"
            Else
                ddlSalesRep.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SRSelectedItem() As ListItem
        Get
            Return ddlSalesRep.SelectedItem
        End Get
    End Property

    Public Property PRDSelectedValue() As String
        Get
            'If lbProduct.SelectedValue = String.Empty Then
            '    Return 0
            'End If
            Return ddlPrd.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlPrd.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlPrd.SelectedValue = "0"
            Else
                ddlPrd.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property PRDSelectedItem() As ListItem
        Get
            Return ddlPrd.SelectedItem
        End Get
    End Property

    Public ReadOnly Property DateStart() As String
        Get
            Return wuc_txtCalendarRange.DateStart
        End Get
    End Property

    Public ReadOnly Property DateEnd() As String
        Get
            Return wuc_txtCalendarRange.DateEnd
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Overrides Sub DataBind()
        BindSalesrep()
        BindProduct()
        PanelUpdate()
        'BindProduct()


        'Dim objDRC As rpt_Customize.clsDRC
        'Dim drRow As DataRow
        'Dim dtSR, dtPRD As DataTable
        'Try
        '    'Get SalesRep List
        '    objDRC = New rpt_Customize.clsDRC
        '    With objDRC
        '        dtSR = .DLLGetSR(Session("UserID"), Session("PRINCIPAL_ID"))
        '        dtPRD = .DLLGetPRD(Session("UserID"), Session("PRINCIPAL_ID"), ddlSalesRep.SelectedValue)
        '    End With

        '    ddlSalesRep.Items.Clear()
        '    ddlSalesRep.Items.Add(New ListItem("Select All", ""))
        '    If dtSR.Rows.Count > 0 Then
        '        For Each drRow In dtSR.Rows
        '            ddlSalesRep.Items.Add(New ListItem(Trim(drRow("salesrep_name")), Trim(drRow("salesrep_code"))))
        '        Next
        '    End If

        '    ddlPrd.Items.Clear()
        '    ddlPrd.Items.Add(New ListItem("Select All", ""))
        '    If dtPRD.Rows.Count > 0 Then
        '        For Each drRow In dtPRD.Rows
        '            ddlPrd.Items.Add(New ListItem(Trim(drRow("prd_name")), Trim(drRow("prd_code"))))
        '        Next
        '    End If
        '    ddlPrd.SelectedIndex = 0
        '    'End If
        '    objDRC = Nothing
        '    Exit Sub

        'Catch ex As Exception
        '    Throw (New ExceptionMsg("wuc_paneldrc.DataBind : " & ex.ToString))
        'Finally
        '    'objDRC = Nothing
        'End Try
    End Sub

    Private Sub BindSalesrep()
        Dim objDRC As New rpt_Customize.clsDRC
        Dim dtSR As DataTable
        Try
            'Get SalesRep List
            dtSR = objDRC.DLLGetSR(Session("UserID"), Session("PRINCIPAL_ID"))

            With ddlSalesRep
                .Items.Clear()
                .DataSource = dtSR
                .DataTextField = "salesrep_name"
                .DataValueField = "salesrep_code"
                .DataBind()
                .Items.Insert(0, New ListItem("Select All", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("BindSalesrep : " & ex.ToString))
        Finally
            If objDRC IsNot Nothing Then objDRC = Nothing
        End Try
    End Sub
    Private Sub BindProduct()
        Dim objDRC As New rpt_Customize.clsDRC
        Dim dtPrd As DataTable
        Try
            'Get SalesRep List
            dtPrd = objDRC.DLLGetPRD(Session("UserID"), Session("PRINCIPAL_ID"), ddlSalesRep.SelectedValue)

            With ddlPrd
                .Items.Clear()
                .DataSource = dtPrd
                .DataTextField = "prd_name"
                .DataValueField = "prd_code"
                .DataBind()
                .Items.Insert(0, New ListItem("Select All", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("BindProduct : " & ex.ToString))
        Finally
            If objDRC IsNot Nothing Then objDRC = Nothing
        End Try
    End Sub

    Protected Sub ddlSalesRep_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesRep.SelectedIndexChanged
        BindProduct()
        PanelUpdate()
    End Sub

    Public Sub PanelUpdate()
        DRCUpdatePanel.Update()
    End Sub
End Class
