﻿'************************************************************************
'	Author	    :	Cheong Boo Lim
'	Date	    :	08/09/2010
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsMssEnqQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMR As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Private strFFMA As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))


    Public Function MSSTitleList() As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMA
                .CmdText = "SPP_MST_DDL_MSS_TITLE"
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                MSSTitleList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function MSSQuesList(ByVal strTitleCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMA
                .CmdText = "SPP_MST_DDL_MSS_QUESTION"
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                MSSQuesList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function MSSSubQuesList(ByVal strTitleCode As String, ByVal strQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMA
                .CmdText = "SPP_MST_DDL_MSS_SUB_QUESTION"
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                MSSSubQuesList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function SearchCustTitle(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, ByVal intNetValue As Integer, _
                                       ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strAnswer As String, ByVal strSubQuesType As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMR
                .CmdText = "SPP_RPT_MSS_CUST_TITLE_SEARCH"
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("ADDRESS", strAddress, clsDB.DataType.DBString)
                .addItem("DISTRICT", strDistrict, clsDB.DataType.DBString)
                .addItem("CUST_GRP_NAME", strCustGrp, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("CUST_TYPE", strCustType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("MSS_START_DATE", strMssStartDate, clsDB.DataType.DBDatetime)
                .addItem("MSS_END_DATE", strMssEndDate, clsDB.DataType.DBDatetime)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBDatetime)
                .addItem("MTDSTART", strMtdStart, clsDB.DataType.DBString)
                .addItem("MTDEND", strMtdEnd, clsDB.DataType.DBString)
                .addItem("YTDSTART", strYtdStart, clsDB.DataType.DBString)
                .addItem("YTDEND", strYtdEnd, clsDB.DataType.DBString)
                .addItem("NOSKUSTART", strNoSkuStart, clsDB.DataType.DBString)
                .addItem("NOSKUEND", strNoSkuEnd, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("ANSWER", strAnswer, clsDB.DataType.DBString)
                .addItem("SUB_QUES_TYPE", strSubQuesType, clsDB.DataType.DBString)
                SearchCustTitle = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function SearchCustTitleStats(ByVal strGUID As String, ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, _
                                       ByVal intNetValue As Integer, ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMR
                .CmdText = "SPP_RPT_MSS_CUST_TITLE_SEARCH_STATS"
                .addItem("GUID", strGUID, clsDB.DataType.DBString)
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("ADDRESS", strAddress, clsDB.DataType.DBString)
                .addItem("DISTRICT", strDistrict, clsDB.DataType.DBString)
                .addItem("CUST_GRP_NAME", strCustGrp, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("CUST_TYPE", strCustType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("MSS_START_DATE", strMssStartDate, clsDB.DataType.DBDatetime)
                .addItem("MSS_END_DATE", strMssEndDate, clsDB.DataType.DBDatetime)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBDatetime)
                .addItem("MTDSTART", strMtdStart, clsDB.DataType.DBString)
                .addItem("MTDEND", strMtdEnd, clsDB.DataType.DBString)
                .addItem("YTDSTART", strYtdStart, clsDB.DataType.DBString)
                .addItem("YTDEND", strYtdEnd, clsDB.DataType.DBString)
                .addItem("NOSKUSTART", strNoSkuStart, clsDB.DataType.DBString)
                .addItem("NOSKUEND", strNoSkuEnd, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                SearchCustTitleStats = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function ExportCustTitle(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, ByVal intNetValue As Integer, _
                                       ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String, ByVal strAnswer As String, ByVal strSubQuesType As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMR
                .CmdText = "SPP_RPT_MSS_CUST_TITLE_SEARCH_EXPORT"
                .addItem("CUST_NAME", strCustName, clsDB.DataType.DBString)
                .addItem("ADDRESS", strAddress, clsDB.DataType.DBString)
                .addItem("DISTRICT", strDistrict, clsDB.DataType.DBString)
                .addItem("CUST_GRP_NAME", strCustGrp, clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, clsDB.DataType.DBString)
                .addItem("CUST_TYPE", strCustType, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("MSS_START_DATE", strMssStartDate, clsDB.DataType.DBDatetime)
                .addItem("MSS_END_DATE", strMssEndDate, clsDB.DataType.DBDatetime)
                .addItem("NET_VALUE", intNetValue, clsDB.DataType.DBInt)
                .addItem("DATE", strDate, clsDB.DataType.DBDatetime)
                .addItem("MTDSTART", strMtdStart, clsDB.DataType.DBString)
                .addItem("MTDEND", strMtdEnd, clsDB.DataType.DBString)
                .addItem("YTDSTART", strYtdStart, clsDB.DataType.DBString)
                .addItem("YTDEND", strYtdEnd, clsDB.DataType.DBString)
                .addItem("NOSKUSTART", strNoSkuStart, clsDB.DataType.DBString)
                .addItem("NOSKUEND", strNoSkuEnd, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
                .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
                .addItem("ANSWER", strAnswer, clsDB.DataType.DBString)
                .addItem("SUB_QUES_TYPE", strSubQuesType, clsDB.DataType.DBString)
                ExportCustTitle = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetMSSAnswer(ByVal strTxnNo As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMR
                .CmdText = "SPP_RPT_MSS_CUST_TITLE_GET_ANSWER"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                GetMSSAnswer = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetMSSHeader(ByVal strTxnNo As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMR
                .CmdText = "SPP_RPT_MSS_CUST_TITLE_GET_HEADER"
                .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                GetMSSHeader = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetMSSCriteria(ByVal strTitleCode As String, ByVal strQuesCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strFFMA
                .CmdText = "SPP_MST_DDL_MSS_CRITERIA"
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                GetMSSCriteria = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
