Option Explicit On

Imports System.Data

Partial Class index
    Inherits System.Web.UI.Page
    Private dtAR As DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Call Header
        With wuc_lblHeader
            .Title = "Quick Link"
            .DataBind()
            .Visible = True
        End With
    End Sub

    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function

    Private Sub GetRedirectMenu(ByVal dblSubModuleID As Double)
        Dim strURL As String

        Select Case dblSubModuleID
            'Admin
            Case 2 'Access Right
                strURL = "Admin/AccessRight/AccessRightList.aspx"
            Case 1 'User
                strURL = "Admin/User/UserList.aspx"
            Case 91 ' User Mobile
                strURL = "Admin/UserMobile/UserMobileList.aspx"
            Case 92 ' Connection
                strURL = "Admin/ConnectionString/ConnectionStringList.aspx"
            Case 226
                strURL = "Admin/SalesrepGrp/SalesrepGrpList.aspx"
            Case 227
                strURL = "iFFMA/SAP/SAP_v3.aspx"
            Case 242
                strURL = "iFFMA/SAP/SAPList_v4.aspx"
            Case 264
                strURL = "Admin/Device Config/Config.aspx"
            Case 265
                strURL = "Admin/Device Config/ConfigDtl.aspx"
                'iFFMS
            Case 15 'Customer
                strURL = "iFFMS/Customer/CustomerListing.aspx"
            Case 24 'Product
                strURL = "iFFMS/Product/ProductList.aspx"
            Case 19, 20, 21, 22, 23 'Transaction
                strURL = "iFFMS/Transaction/TransactionSelection.aspx"
            Case 102 ' Data Files
                strURL = "IFFMS/DataFiles/DataFilesList.aspx"
                'iFFMR
            Case 5 'Inventory Forecasting Analysis
                strURL = "iFFMR/DRC/DRCList.aspx"
            Case 4 'PDA Comm
                strURL = "iFFMR/Comm/CommList.aspx"
            Case 7 'Sales List
                strURL = "iFFMR/Sales/SalesList.aspx"
            Case 8 'Sales Enq
                strURL = "iFFMR/Sales/SalesEnquiryList.aspx"
            Case 10 'Call Analysis By Month
                strURL = "iFFMR/SFE/CallAnalysis/CallAnalysisListByMonth.aspx"
            Case 9 'Call Analysis By Day
                strURL = "iFFMR/SFE/CallAnalysis/CallAnalysisListByDay.aspx"
            Case 25 'Call Enquiry
                strURL = "iFFMR/SFE/CallAnalysis/CallEnquiryList.aspx"
            Case 12 'Call Productivity
                strURL = "iFFMR/SFE/CallProd/CallProdList.aspx"
            Case 13 'Preplan Call
                strURL = "iFFMR/SFE/Preplan/PreplanList.aspx"
            Case 14 'SFMS Acty List
                strURL = "iFFMR/SFE/SFMSActy/SFMSActyList.aspx"
            Case 6 'SFMS Extract
                strURL = "iFFMR/RptExtract/SFMSExtract/SFMSExtractList.aspx"
            Case 30 'Data Files
                strURL = "iFFMR/DataFiles/DataFilesList.aspx"
            Case 31 'Customer Extract
                strURL = "blank.aspx"
            Case 32 'Inventory Forecasting  Extract
                strURL = "blank.aspx"
            Case 33 'MSS Extract
                strURL = "blank.aspx"
            Case 34 'Sales Order Extract
                strURL = "blank.aspx"
            Case 35 'Call Coverage 1
                strURL = "iFFMR/SFE/CallCoverage/CallCoverage1.aspx"
            Case 36 'Call Coverage 2
                strURL = "iFFMR/SFE/CallCoverage/CallCoverage2.aspx"
            Case 37 'Relative Performance
                strURL = "iFFMR/KPI/RelPerformance.aspx"
            Case 38 ' Monthly Overview
                strURL = "iFFMR/KPI/MthOverview.aspx"
            Case SubModuleType.ACTYSALESBYINDSR
                strURL = "iFFMR/KPI/ActSalesBySalesrep.aspx"
            Case 43 'Messaging
                strURL = "iFFMS/Messaging/MessagingList.aspx"
            Case 45 'Performance
                strURL = "iFFMS/Performance/PerformanceOverview.aspx"
            Case 44 'Reporting
                strURL = "iFFMS/Report/ReportHeader.aspx"
            Case 46 'Summary
                strURL = "iFFMS/Summary/SummaryListing.aspx"
            Case 47
                strURL = "iFFMS/Plan/PlanMenu.aspx"
          
            Case SubModuleType.TRAORDERLIST
                strURL = "iFFMS/Order/TRAOrderList.aspx"

            Case SubModuleType.TRAORDERLISTV2
                strURL = "iFFMS/Order/TRAOrderListV2.aspx"

            Case 58 ' call by contact
                strURL = "IFFMR/SFE/CallCoverage/CallAnalysisListByContact.aspx"
            Case 59 ' call by speciality
                strURL = "IFFMR/SFE/CallAnalysis/CallAnalysisListBySpeciality.aspx"
            Case 60 ' comm upload information
                strURL = "IFFMR/Comm/CommUploadInfo.aspx"
            Case 61 ' call by strike
                strURL = "IFFMR/SFE/CallAnalysis/CallAnalysisListByStrike.aspx"
            Case 62 ' smfs performance
                strURL = "IFFMR/SFE/SFMSPRFM/SFMSPerformance.aspx"
            Case 63 ' Inventory Forecasting Enquiry
                strURL = "IFFMR/DRC/DRCEnquiryList.aspx"
            Case 64 ' Payment Collection Cheque Enquiry
                strURL = "IFFMR/Customize/CollCheqEnquiryList.aspx"
            Case 68 ' Promotion Acceptance Enquiry
                strURL = "IFFMR/Customize/PAFEnquiryList.aspx"
            Case 69 ' Goods Return By Month End Closing
                strURL = "IFFMR/Customize/TRARetByMth.aspx"
            Case 73 ' Sales By Key Product
                strURL = "IFFMR/Sales/SalesInfoByKeyPrd.aspx"
            Case 70 ' Sales Rep Stock Allocation
                strURL = "IFFMR/Customize/SalesrepStockAllocation.aspx"
            Case 71 ' Customer Status
                strURL = "IFFMR/KPI/CustStatusList.aspx"
            Case 72 ' Sales Top 80% Customer By Reps
                strURL = "IFFMR/Sales/SalesTop80CustByRep.aspx"
            Case 103 ' Customization: Call Rate
                strURL = "IFFMR/Customize/CallRate.aspx"
            Case 104 ' Customization: Sales Target Vs Actual
                strURL = "IFFMR/Customize/SalesTgtVsActual.aspx"
            Case 105 ' Customization: Sales Estimation Vs Actual
                strURL = "IFFMR/Customize/SalesEstVsActual.aspx"
            Case 118 ' Audit Log
                strURL = "IFFMR/Comm/AuditLog.aspx"
            Case 120 ' Sales By Customer Count
                strURL = "IFFMR/Sales/SalesByCustCount.aspx"
            Case 122 ' SAP List
                strURL = "iFFMA/SAP/SAPList.aspx"
            Case SubModuleType.SAPLISTV2
                strURL = "iFFMA/SAP/SAPList_v2.aspx"
            Case 123 ' SFMS Extraction
                strURL = "IFFMR/Customize/SFMSExtraction.aspx"
            Case 124 ' Call Info By Month
                strURL = "IFFMR/Customize/CallInfoByMonth.aspx"
            Case 125 ' Salesforce Capacity By Month
                strURL = "IFFMR/Customize/SalesForceCapacity.aspx"

            Case SubModuleType.COMPANY
                strURL = "iFFMA/General/CompanyList.aspx"
            Case SubModuleType.SALESAREA
                strURL = "iFFMA/General/SalesAreaList.aspx"
            Case SubModuleType.WAREHOUSE
                strURL = "iFFMA/General/WarehouseList.aspx"
            Case SubModuleType.REGION
                strURL = "iFFMA/General/RegionList.aspx"
            Case SubModuleType.SALESTEAM
                strURL = "iFFMA/Salesteam/SalesteamList.aspx"
            Case SubModuleType.CUSTOMER
                strURL = "iFFMA/SalesAccount/CustomerList.aspx"
            Case SubModuleType.SHIPTO
                strURL = "iFFMA/SalesAccount/ShipToList.aspx"
            Case SubModuleType.CONTACT
                strURL = "iFFMA/Contact/ContactList.aspx"
            Case SubModuleType.PRODUCT
                strURL = "iFFMA/Product/ProductList.aspx"
            Case SubModuleType.PRODUCTGROUP
                strURL = "iFFMA/Product/ProductGroupList.aspx"
            Case SubModuleType.SUPPLIER
                strURL = "iFFMA/Product/SupplierList.aspx"
            Case SubModuleType.STOCKALLOC
                strURL = "iFFMA/Product/StockAllocList.aspx"
            Case SubModuleType.PRODUCT_MATRIX
                strURL = "iFFMA/Product/ProductMatrixList.aspx"
            Case SubModuleType.STANDARDPRICE
                strURL = "iFFMA/TradeDeal/Discount/StandardPriceList.aspx"
            Case SubModuleType.SPECIALPRICE
                strURL = "iFFMA/TradeDeal/Discount/SpecialPriceList.aspx"
            Case SubModuleType.CUSTGRPPRICE
                strURL = "iFFMA/TradeDeal/Discount/CustGrpPriceList.aspx"
            Case SubModuleType.PRICEGRPPRICE
                strURL = "iFFMA/TradeDeal/Discount/PriceGrpPriceList.aspx"
            Case SubModuleType.STANDARDBONUS
                strURL = "iFFMA/TradeDeal/Bonus/StandardBonusList.aspx"
            Case SubModuleType.SPECIALBONUS
                strURL = "iFFMA/TradeDeal/Bonus/SpecialBonusList.aspx"
            Case SubModuleType.CUSTGRPBONUS
                strURL = "iFFMA/TradeDeal/Bonus/CustGrpBonusList.aspx"
            Case SubModuleType.PRICEGRPBONUS
                strURL = "iFFMA/TradeDeal/Bonus/PriceGrpBonusList.aspx"
            Case SubModuleType.PACKAGE
                strURL = "iFFMA/TradeDeal/PackageList.aspx"
            Case SubModuleType.FIELDACTY
                strURL = "iFFMA/FieldForceActy/FieldActyCatList.aspx"
            Case SubModuleType.MSSADV
                strURL = "iFFMA/FieldForceActy/MSSAdvList.aspx"
            Case SubModuleType.FIELDFORCE
                strURL = "iFFMA/FieldForce/FieldForceList.aspx"
            Case SubModuleType.SALESTGT
                strURL = "iFFMA/FieldForce/SalesTgtList.aspx"
            Case SubModuleType.REASON
                strURL = "iFFMA/Others/ReasonList.aspx"
            Case SubModuleType.ROUTE
                strURL = "iFFMA/Others/RouteList.aspx"
            Case SubModuleType.PRICEGROUP
                strURL = "iFFMA/Others/PriceGroupList.aspx"
            Case SubModuleType.LEAD
                strURL = "iFFMA/Others/LeadList.aspx"
            Case SubModuleType.INBOX
                strURL = "iFFMA/Utility/Messaging/InboxMsgList.aspx"
            Case SubModuleType.OUTBOX
                strURL = "iFFMA/Utility/Messaging/OutboxMsgList.aspx"
            Case SubModuleType.EASYLOADER
                strURL = "iFFMA/Utility/EasyLoader/EasyLoader.aspx"
            Case SubModuleType.FIELDFORCECUST
                strURL = "iFFMA/Mapping/FieldForceCustList.aspx"
            Case SubModuleType.FIELDFORCEROUTE
                strURL = "iFFMA/Mapping/FieldForceRouteList.aspx"
            Case SubModuleType.FIELDFORCEPRDGRP
                strURL = "iFFMA/Mapping/FieldForcePrdGrpList.aspx"
            Case SubModuleType.FIELDFORCEACTY
                strURL = "iFFMA/Mapping/FieldForceActyList.aspx"
            Case SubModuleType.FIELDFORCEMSS
                strURL = "iFFMA/Mapping/FieldForceMSSList.aspx"
            Case SubModuleType.FIELDFORCEPACKAGE
                strURL = "iFFMA/Mapping/FieldForcePackageList.aspx"
            Case SubModuleType.FIELDFORCEITINERARYTRANSFER
                strURL = "iFFMA/Mapping/FieldForceItineraryTransfer.aspx"
            Case SubModuleType.CALLBYDKSHCLASS
                strURL = "iFFMR/Customize/CallByDKSHClass.aspx"
            Case SubModuleType.CALLBYSUPPLIERCLASS
                strURL = "iFFMR/Customize/CallBySupplierClass.aspx"
            Case SubModuleType.CALLACTYENQ
                strURL = "iFFMR/Customize/CallActyEnquiryList.aspx"
            Case SubModuleType.FIELDFORCEMTHACTY
                strURL = "iFFMR/Customize/SalesrepMthActy.aspx"
            Case SubModuleType.FIELDFORCEPRDFREQ
                strURL = "iFFMR/Customize/SalesrepPrdFreq.aspx"
            Case SubModuleType.STOCKADJUST
                strURL = "iFFMA/GCIM/StockAdjustList.aspx"
            Case SubModuleType.CALLANALYENQ
                strURL = "iFFMR/Customize/CallAnalysisEnquiry.aspx"
            Case SubModuleType.CALLANALYBYCONTCLASS
                strURL = "iFFMR/Customize/CallAnalysisByContClass.aspx"
            Case SubModuleType.CPRODACTYSUP
                strURL = "iFFMR/Customize/CallProdSupList.aspx"
            Case SubModuleType.STOCKTRANSFERLIST
                strURL = "iFFMA/Services/StockTransferList.aspx"
            Case SubModuleType.NEWEQPLIST
                strURL = "iFFMA/Services/NewEqpList.aspx"
            Case SubModuleType.APPLYSTOCKLIST
                strURL = "iFFMA/Services/ApplyStockList.aspx"
            Case SubModuleType.STOCKHIST
                strURL = "iFFMA/GCIM/StockHistList.aspx"
            Case SubModuleType.STOCKSTATUS
                strURL = "iFFMA/GCIM/StockStatusList.aspx"


            Case SubModuleType.COMPANYCODE_BUPROFILE
                strURL = "iFFMA/AR/CompBUList.aspx"
            Case SubModuleType.COMPANYCODE_BANKPROFILE
                strURL = "iFFMA/AR/CompBankList.aspx"
            Case SubModuleType.COLLECTORPROFILE
                strURL = "iFFMA/AR/CollectorProfileList.aspx"
            Case SubModuleType.COLLECTORCUSTOMERMAPPING
                strURL = "iFFMA/AR/CollectorCustMappingList.aspx"
            Case SubModuleType.PRODUCTMUSTSELL
                strURL = "iFFMA/Product/PrdMustSellList.aspx"

            Case SubModuleType.PRODUCTMUSTSELLADV
                strURL = "iFFMA/Product/PrdMustSellListAdv.aspx"

            Case SubModuleType.DRCEXTRACTION
                strURL = "iFFMR/DRC/DRCExtractList.aspx"
            Case SubModuleType.TECHNICIANSALESGRP
                strURL = "iFFMA/Mapping/TechnicianSalesGrpList.aspx"
            Case SubModuleType.TECHNICIANITINERARY
                strURL = "iFFMR/Customize/TechnicianItineraryList.aspx"
            Case SubModuleType.TECHNICIANFUNCLOC
                strURL = "iFFMA/Mapping/TechnicianFuncLocList.aspx"
            Case SubModuleType.TECHNICIANSALESOFF
                strURL = "iFFMA/Mapping/TechnicianSalesOffList.aspx"
            Case SubModuleType.ORDPRDMATRIX
                strURL = "iFFMR/Customize/SalesOrderPrdMatrix.aspx"
            Case SubModuleType.SRCALLRATE
                strURL = "iFFMR/Customize/SalesrepCallRate.aspx"
            Case SubModuleType.MANUFACTURER
                strURL = "iFFMA/Services/ManufacturerList.aspx"
            Case SubModuleType.SVCSTAT
                strURL = "iFFMR/Customize/ServiceStatisticHdr.aspx"
            Case SubModuleType.EQPHISTLIST
                strURL = "iFFMA/Services/EqpHistList.aspx"
            Case SubModuleType.COLLENQ
                strURL = "iFFMR/Customize/CollEnq.aspx"
            Case SubModuleType.UPLOADITINERARY
                strURL = "iFFMA/Utility/Upload/Itinerary.aspx"
            Case SubModuleType.MTHPRDFREQ
                strURL = "iFFMR/Customize/PrdFreqMthly.aspx"
            Case SubModuleType.MTHSPECIALITY
                strURL = "iFFMR/Customize/SpecialityMthly.aspx"
            Case SubModuleType.MTHTERRITORY
                strURL = "iFFMR/Customize/TerritoryMthly.aspx"
            Case SubModuleType.SRSCORECARD
                strURL = "iFFMR/Customize/ScoreCardMthly.aspx"
            Case SubModuleType.MSS_CONT
                strURL = "iFFMA/HCD/MssCont.aspx"
            Case SubModuleType.MSS_CONT_DTL
                strURL = "iFFMA/HCD/MssContDtl.aspx"
            Case SubModuleType.CUZDRCCUST
                strURL = "iFFMR/Customize/DRCByCUST.aspx"
            Case SubModuleType.CUZDRCCUSTCLASS
                strURL = "iFFMR/Customize/DRCByCustClass.aspx"
            Case SubModuleType.CUZDRCDISTRICT
                strURL = "iFFMR/Customize/DRCByDistrict.aspx"

            Case SubModuleType.SFMSEXTRACTION_2  ' SFMS Extraction
                strURL = "IFFMR/Customize/SFMSExtraction2.aspx"
            Case SubModuleType.CUZPACKAGE  'PACKAGE ANALYSIS
                strURL = "IFFMR/Customize/PckgAnalysis.aspx"
            Case SubModuleType.LEAVE   ' Leave
                strURL = "IFFMS/Leave/LeaveList.aspx"
            Case SubModuleType.CALLDTLBYGROUP
                strURL = "iFFMR/Customize/CallDtlByGroup.aspx"
            Case SubModuleType.DATA_V2
                strURL = "iFFMR/DataFiles/DataFileList_V2.aspx"
            Case SubModuleType.SALES_CUST_PER_PRD
                strURL = "iFFMR/Customize/SalesCustPerProduct.aspx"
            Case SubModuleType.SALES_CUZ
                strURL = "iFFMR/Customize/SalesListCuz.aspx"
            Case SubModuleType.CUSTINFO
                strURL = "iFFMA/SalesAccount/CustomerInfoList.aspx"
            Case SubModuleType.CUSTCLASS
                strURL = "iFFMA/SalesAccount/CustomerClassList.aspx"
            Case SubModuleType.CUSTSALESTGT
                strURL = "iFFMA/SalesAccount/CustomerSalesTgtList.aspx"
            Case SubModuleType.CUSTPROFILEMAINTAIN
                strURL = "iFFMA/SalesAccount/CustProfileMaintenance.aspx"
            Case SubModuleType.CUSTSALESANALYSIS
                strURL = "iFFMR/Customize/CustSalesAnalysis.aspx"
            Case SubModuleType.CUSTPRDSALESANALYSIS
                strURL = "iFFMR/Customize/CustPrdSalesAnalysis.aspx"
            Case SubModuleType.SOVARIANCE
                strURL = "iFFMR/Customize/SOVariance.aspx"
            Case SubModuleType.PLAN_ACTUAL
                strURL = "iFFMR/Customize/PlanVsActual.aspx"
            Case SubModuleType.CALLRATE_BYBRAND
                strURL = "iFFMR/Customize/CallRateByBrand.aspx"
            Case SubModuleType.TRAList
                strURL = "Admin/TRA/TRAList.aspx"
            Case SubModuleType.TRAReport
                strURL = "iFFMR/Customize/TRAReport.aspx"
            Case SubModuleType.MERCHSFMS
                strURL = "iFFMR/Customize/MerchSfms/MerchSfms.aspx"
            Case SubModuleType.SALESREP_KPI
                strURL = "iFFMR/Customize/SalesrepKPI/SalesrepKPI.aspx"
            Case SubModuleType.DAYCALLDISTRIBUTION
                strURL = "iFFMR/Customize/DailyCallDistribution/DailyCallDistribution.aspx"
            Case SubModuleType.DRCCuzList
                strURL = "iFFMR/Customize/DRCCuzList/DRCCuzList.aspx"
            Case SubModuleType.PREPLANEXTRACT
                strURL = "iFFMR/Customize/PrePlanExtraction.aspx"
            Case SubModuleType.PREPLANCUZ
                strURL = "iFFMS/PlanCuz/PlanCuzMenu.aspx"
            Case SubModuleType.CALLACHIEVEMENT
                strURL = "iFFMR/Customize/CallAchievement/CallAchievement.aspx"
            Case SubModuleType.CALLBYDAYADV
                strURL = "iFFMR/Customize/CallByMonthAdv/CallByDayAdv.aspx"
            Case SubModuleType.CALLBYMONTHADV
                strURL = "iFFMR/Customize/CallByMonthAdv/CallByMonthAdv.aspx"

            Case SubModuleType.CUZMSSENQUIRY
                strURL = "iFFMR/Customize/Geomap/MSSEnquiry.aspx"
            Case SubModuleType.CUZSFMSENQUIRY
                strURL = "iFFMR/Customize/Geomap/SFMSEnquiry.aspx"
            Case SubModuleType.CUSTPROFILEENQ
                strURL = "iFFMR/Customize/Geomap/CustProfileEnq.aspx"

            Case SubModuleType.MSSEXTRACTION
                strURL = "iFFMR/Customize/MSSExtraction.aspx"

            Case SubModuleType.MSS_TEAM_MAPPING
                strURL = "iFFMA/FieldForceActy/MSSTeamMap.aspx"
            Case SubModuleType.MISSCALLANALYSIS
                strURL = "iFFMR/Customize/MissCallAnalysis/MissCallAnalysis.aspx"
            Case SubModuleType.SALES_BY_CHAIN
                strURL = "iFFMR/Customize/SalesByChainCustMain.aspx"
            Case SubModuleType.REMAIN_CALL
                strURL = "iFFMR/Customize/CallRemainingReport.aspx"
            Case SubModuleType.STK_CONSUMP_BY_BIMONTH
                strURL = "iFFMR/Customize/StkConsump/StkConsumpByBiMthMain.aspx"
            Case SubModuleType.STK_CONSUMP_BY_QUARTER
                strURL = "iFFMR/Customize/StkConsump/StkConsumpByQtrMain.aspx"
            Case SubModuleType.EASYLOADERV2
                strURL = "iFFMA/Utility/EasyLoader/EasyLoaderV2.aspx"
            Case SubModuleType.MSSADV2
                strURL = "iFFMA/FieldForceActy/MSSAdv2/MSSAdv2List.aspx"
            Case SubModuleType.ELACCESSRIGHT
                strURL = "Admin/ELAccessright/ELAccessRightList.aspx"
            Case SubModuleType.FIELDFORCEMTHACTYPrd
                strURL = "iFFMR/Customize/SalesrepMthActyPrd.aspx"
            Case SubModuleType.LINKEPONDEMAND6
                strURL = "Admin/Integration/ffms6.aspx"
            Case SubModuleType.STK_CONSUMP_BY_QUADMONTH
                strURL = "iFFMR/Customize/StkConsump/StkConsumpByQuadMthMain.aspx"
            Case SubModuleType.ActCallKPI
                strURL = "iFFMR/Customize/ActCallKPI/ActualCallKPI.aspx"
            Case SubModuleType.SFMSEXTRACTION_EXTRACAT
                strURL = "iFFMR/Customize/SFMSExtractionExtraCat.aspx"
            Case SubModuleType.TRAREPORT_EDI
                strURL = "iFFMR/Customize/TRAReport/TRAReportMain.aspx"
            Case SubModuleType.SFMS_EXTRACT
                strURL = "iFFMR/Customize/SFMSExtract.aspx"
            Case SubModuleType.VISIT_EXTRACT
                strURL = "iFFMR/Customize/VisitExtract.aspx"
            Case SubModuleType.CUST_CONT_EXTRACT
                strURL = "iFFMR/Customize/CustContExtract.aspx"
            Case SubModuleType.CUST_CONT_PRD_EXTRACT
                strURL = "iFFMR/Customize/CustContPrdExtract.aspx"


            Case SubModuleType.CUSTGENERATELOC
                strURL = "iFFMA/SalesAccount/Custgenerateloc.aspx"

            Case SubModuleType.DASHBOARDXFIELD
                strURL = "iFFMA/Dashboard/DashboardXFieldList.aspx"
            Case SubModuleType.CUSTTGTPRD
                strURL = "iFFMA/SalesAccount/CustomerTgtPrdList.aspx"
            Case SubModuleType.CUST_TEAM_CLASS
                strURL = "iFFMA/SalesAccount/CustomerTeamClassList.aspx"
            Case SubModuleType.PRDSEQ
                strURL = "iFFMA/Product/PrdSeqList.aspx"
            Case SubModuleType.PROMO_SO
                strURL = "iFFMA/Others/PromotionList.aspx"
            Case SubModuleType.DETAIL_BRAND
                strURL = "iFFMA/Detailing/DetailingCatList.aspx"
            Case SubModuleType.CONTKPI
                strURL = "iFFMR/KPI/ContStatusList.aspx"
            Case SubModuleType.SFEKPIREPORT
                strURL = "iFFMR/KPI/SFEKPIList.aspx"
            Case SubModuleType.DET_COVERAGE_BY_SPECIALTY
                strURL = "iFFMR/Detailing/SpecialtyCoverageByProd.aspx"
            Case SubModuleType.DET_CALL_BY_AREA
                strURL = "iFFMR/Detailing/CallByAreaByProduct.aspx"
            Case SubModuleType.CALLCOV1ADV
                strURL = "iFFMR/SFE/CallCoverage/CallCoverage1Adv.aspx"
            Case SubModuleType.MUSTSELL
                strURL = "iFFMR/Customize/MustSell.aspx"
            Case SubModuleType.DET_COVERAGE_CALL_BY_CLASS_BY_PRODUCT
                strURL = "iFFMR/Detailing/CallByClassByProduct.aspx"
            Case SubModuleType.ONLINEGALLERY
                strURL = "iFFMR/OnlineGallery.aspx"
            Case SubModuleType.SYNCIMAGE
                strURL = "iFFMA/SyncImage/SyncImageToDB.aspx"
            Case SubModuleType.PRDCATALOG
                strURL = "iFFMA/Product/PrdCatalogList.aspx"
            Case SubModuleType.MERCHANDISING
                strURL = "iFFMR/Customize/Merchandising.aspx"
            Case SubModuleType.COMPETITOR
                strURL = "iFFMR/Customize/Competitor.aspx"
            Case SubModuleType.PUSHNOTIFICATION
                strURL = "iFFMA/Utility/PushNotification/PushNotificationList.aspx"
            Case SubModuleType.DAILYTRACKING
                strURL = "iFFMR/Customize/Geomap/DailyTracking.aspx"
            Case SubModuleType.CUSTOMERCOVERAGE
                strURL = "iFFMR/Customize/Geomap/CustomerCoverage.aspx"
            Case SubModuleType.HISTORICALTRACKING
                strURL = "iFFMR/Customize/Geomap/HistoricalTracking.aspx"
            Case SubModuleType.BCP
                strURL = "Admin/BCP/BCPTemplateList.aspx"
            Case SubModuleType.SFMS_CFG
                strURL = "Admin/BCP/SFMSConfig.aspx"
            Case SubModuleType.MSS_CFG
                strURL = "Admin/BCP/MSSConfig.aspx"
            Case SubModuleType.VIEW_SALESMAN_ACTIVITY
                strURL = "iFFMR/Customize/ViewSalesmanActivity/ViewSalesmanActivity.aspx"
            Case Else
                strURL = "blank.aspx"
        End Select

        Response.Redirect(strURL, False)
        'Try

        '    'Response.Redirect(strURL, False)

        'Catch ex As Exception
        '    ExceptionMsg("index.GetRedirectMenu : " & ex.ToString)
        'End Try
    End Sub

#Region "FFMR"

    Protected Sub lnkCallInfoByMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallInfoByMonth.Click
        GetRedirectMenu(124)
    End Sub

    Protected Sub lnkSFCAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFCAP.Click
        GetRedirectMenu(125)
    End Sub

    Protected Sub lnkCallByDKSHClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallByDKSHClass.Click
        GetRedirectMenu(165)
    End Sub

    Protected Sub lnkCallBySupplierClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallBySupplierClass.Click
        GetRedirectMenu(166)
    End Sub

    Protected Sub lnkCallActyEnq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallActyEnq.Click
        GetRedirectMenu(168)
    End Sub

    Protected Sub lnkDRC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDRC.Click
        GetRedirectMenu(5)
    End Sub

    Protected Sub lnkDRCEnq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDRCEnq.Click
        GetRedirectMenu(63)
    End Sub

    Protected Sub lnkPDAComm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPDAComm.Click
        GetRedirectMenu(4)
    End Sub

    Protected Sub lnkUplInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUplInfo.Click
        GetRedirectMenu(60)
    End Sub

    Protected Sub lnkAuditLog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAuditLog.Click
        GetRedirectMenu(118)
    End Sub

    Protected Sub lnkSalesList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesList.Click
        GetRedirectMenu(7)
    End Sub

    Protected Sub lnkSalesEnq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesEnq.Click
        GetRedirectMenu(8)
    End Sub

    Protected Sub lnkSalesByKeyPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesByKeyPrd.Click
        GetRedirectMenu(73)
    End Sub

    Protected Sub lnkSalesTop80CustByRep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesTop80CustByRep.Click
        GetRedirectMenu(72)
    End Sub

    Protected Sub lnkSalesByCustCount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesByCustCount.Click
        GetRedirectMenu(120)
    End Sub

    Protected Sub lnkCAByMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCAByMonth.Click
        GetRedirectMenu(10)
    End Sub

    Protected Sub lnkCAByDay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCAByDay.Click
        GetRedirectMenu(9)
    End Sub

    Protected Sub lnkCallEnq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallEnq.Click
        GetRedirectMenu(25)
    End Sub

    Protected Sub lnkCallCoverage1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallCoverage1.Click
        GetRedirectMenu(35)
    End Sub

    Protected Sub lnkCallCoverage2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallCoverage2.Click
        GetRedirectMenu(36)
    End Sub
    Protected Sub lnkCallCoverage1Adv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallCoverage1Adv.Click
        GetRedirectMenu(313)
    End Sub

    Protected Sub lnkCallProductivity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallProductivity.Click
        GetRedirectMenu(12)
    End Sub

    Protected Sub lnkPreplan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreplan.Click
        GetRedirectMenu(13)
    End Sub

    Protected Sub lnkSFMSActy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSActy.Click
        GetRedirectMenu(14)
    End Sub

    Protected Sub lnkCustStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustStatus.Click
        GetRedirectMenu(71)
    End Sub
    Protected Sub lnkContStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkContStatus.Click
        GetRedirectMenu(315)
    End Sub
    Protected Sub lnkSFEKPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFEKPI.Click
        GetRedirectMenu(312)
    End Sub
    Protected Sub lnkRelativePerformance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRelativePerformance.Click
        GetRedirectMenu(37)
    End Sub

    Protected Sub lnkMonthlyOverview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMonthlyOverview.Click
        GetRedirectMenu(38)
    End Sub

    Protected Sub lnkCustomerExt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustomerExt.Click
        GetRedirectMenu(31)
    End Sub

    Protected Sub lnkDRCExt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDRCExt.Click
        GetRedirectMenu(32)
    End Sub

    Protected Sub lnkMSSExt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMSSExt.Click
        GetRedirectMenu(33)
    End Sub

    Protected Sub lnkSOExt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSOExt.Click
        GetRedirectMenu(34)
    End Sub

    Protected Sub lnkSFMSExt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSExt.Click
        GetRedirectMenu(6)
    End Sub

    Protected Sub lnkCollCheqEnq_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCollCheqEnq.Click
        GetRedirectMenu(64)
    End Sub

    Protected Sub lnkPAFEnq_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPAFEnq.Click
        GetRedirectMenu(68)
    End Sub

    Protected Sub lnkStkRetByMth_click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStkRetByMth.Click
        GetRedirectMenu(69)
    End Sub

    Protected Sub lnkDataFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDataFiles.Click
        GetRedirectMenu(30)
    End Sub

    Protected Sub lnkCustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustomer.Click
        GetRedirectMenu(15)
    End Sub
   

    Protected Sub lnkProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkProduct.Click
        GetRedirectMenu(24)
    End Sub

    Protected Sub lnkTransaction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTransaction.Click
        GetRedirectMenu(19)
    End Sub

    Protected Sub lnkAR_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAR.Click
        GetRedirectMenu(2)
    End Sub

    Protected Sub lnkUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUser.Click
        GetRedirectMenu(1)
    End Sub
    Protected Sub lnkActSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkActSales.Click
        GetRedirectMenu(SubModuleType.ACTYSALESBYINDSR)
    End Sub

    Protected Sub lnkMessaging_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMessaging.Click
        GetRedirectMenu(43)
    End Sub

    Protected Sub lnkPerformance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPerformance.Click
        GetRedirectMenu(45)
    End Sub

    Protected Sub lnkReporting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReporting.Click
        GetRedirectMenu(44)
    End Sub

    Protected Sub lnkSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSummary.Click
        GetRedirectMenu(46)
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        GetRedirectMenu(47)
    End Sub
    Protected Sub lnkCAByCont_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCAByCont.Click
        GetRedirectMenu(58)
    End Sub

    Protected Sub lnkCABySpec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCABySpec.Click
        GetRedirectMenu(59)
    End Sub
    Protected Sub lnkCAByStrike_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCAByStrike.Click
        GetRedirectMenu(61)
    End Sub

    Protected Sub lnkSFMSPRFM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSPRFM.Click
        GetRedirectMenu(62)
    End Sub

    Protected Sub lnkSalesrepStkAlloc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesrepStkAlloc.Click
        GetRedirectMenu(70)
    End Sub

    Protected Sub lnkCallRate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallRate.Click
        GetRedirectMenu(103)
    End Sub

    Protected Sub lnkSalesTgtVsAct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesTgtVsAct.Click
        GetRedirectMenu(104)
    End Sub

    Protected Sub lnkSalesEstVsAct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesEstVsAct.Click
        GetRedirectMenu(105)
    End Sub

    Protected Sub lnkSFMSExtraction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSExtraction.Click
        GetRedirectMenu(123)
    End Sub

    Protected Sub lnkConn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkConn.Click
        GetRedirectMenu(92)
    End Sub

    Protected Sub lnkUserMobile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUserMobile.Click
        GetRedirectMenu(91)
    End Sub

    Protected Sub lnkFFMSDataFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMSDataFiles.Click
        GetRedirectMenu(102)
    End Sub

    Protected Sub lnkCallAnalyEnq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallAnalyEnq.Click
        GetRedirectMenu(175)
    End Sub

    Protected Sub lnkSalesOrderPrdMatrix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesOrderPrdMatrix.Click
        GetRedirectMenu(192)
    End Sub
    Protected Sub lnkSalesrepCallRate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesrepCallRate.Click
        GetRedirectMenu(193)
    End Sub

    Protected Sub lnkSalesrepMthActy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesrepMthActy.Click
        GetRedirectMenu(169)
    End Sub

    Protected Sub lnkSalesrepPrdFreq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesrepPrdFreq.Click
        GetRedirectMenu(170)
    End Sub

    Protected Sub lnkCProcActivSup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCProcActivSup.Click
        GetRedirectMenu(182)
    End Sub

    Protected Sub lnkClnkDRCExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDRCExtract.Click
        GetRedirectMenu(184)
    End Sub

    Protected Sub lnkTechnicianItinerary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTechnicianItinerary.Click
        GetRedirectMenu(186)
    End Sub

    Protected Sub lnkTRAOrderList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTRAOrderList.Click
        GetRedirectMenu(190)
    End Sub
    Protected Sub lnkTRAOrderListV2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTRAOrderListV2.Click
        GetRedirectMenu(293)
    End Sub

    Protected Sub lnkServiceStatistic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkServiceStatistic.Click
        GetRedirectMenu(196)
    End Sub

    Protected Sub lnkCollEnq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCollEnq.Click
        GetRedirectMenu(199)
    End Sub

    Protected Sub lnkMTHPRDFREQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMTHPRDFREQ.Click
        GetRedirectMenu(214)
    End Sub

    Protected Sub lnkMTHTERRITORY_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMTHTERRITORY.Click
        GetRedirectMenu(215)
    End Sub

    Protected Sub lnkMTHSPECIALITY_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMTHSPECIALITY.Click
        GetRedirectMenu(216)
    End Sub

    Protected Sub lnkSRSCORECARD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRSCORECARD.Click
        GetRedirectMenu(217)
    End Sub

    Protected Sub lnkInvForecastCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInvForecastCust.Click
        GetRedirectMenu(200)
    End Sub

    Protected Sub lnkInvForecastCustClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInvForecastCustClass.Click
        GetRedirectMenu(201)
    End Sub

    Protected Sub lnkInvForecastDistrict_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInvForecastDistrict.Click
        GetRedirectMenu(202)
    End Sub

    Protected Sub lnkSFMSExtraction2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSExtraction2.Click
        GetRedirectMenu(220)
    End Sub


    Protected Sub lnkPACKAGE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPACKAGE.Click
        GetRedirectMenu(223)
    End Sub

    Protected Sub lnkCallDtlByGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallDtlByGroup.Click
        GetRedirectMenu(224)
    End Sub


    Protected Sub lnkDataFiles2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDataFiles2.Click
        GetRedirectMenu(228)
    End Sub

    Protected Sub lnkSalesCustPerProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesCustPerProduct.Click
        GetRedirectMenu(229)
    End Sub

    Protected Sub lnkSalesCuz_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesCuz.Click
        GetRedirectMenu(230)
    End Sub

    Protected Sub lnkPlanVsActual_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPlanVsActual.Click
        GetRedirectMenu(237)
    End Sub

    Protected Sub lnkCallRateByBrand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallRateByBrand.Click
        GetRedirectMenu(238)
    End Sub

    Protected Sub lnkTRAReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTRAReport.Click
        GetRedirectMenu(241)
    End Sub
    Protected Sub lnkMERCHSFMS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMERCHSFMS.Click
        GetRedirectMenu(243)
    End Sub

    Protected Sub lnkSRKPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRKPI.Click
        GetRedirectMenu(244)
    End Sub

    Protected Sub lnkDailyCallDistribution_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDailyCallDistribution.Click
        GetRedirectMenu(245)
    End Sub

    Protected Sub lnkDRCCuZList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDRCCuZList.Click
        GetRedirectMenu(246)
    End Sub

    Protected Sub lnkPrePlanExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrePlanExtract.Click
        GetRedirectMenu(SubModuleType.PREPLANEXTRACT)
    End Sub

    Protected Sub LnkPrePlanCuz_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkPrePlanCuz.Click
        GetRedirectMenu(SubModuleType.PREPLANCUZ)
    End Sub

    Protected Sub lnkCallAchievement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallAchievement.Click
        GetRedirectMenu(SubModuleType.CALLACHIEVEMENT)
    End Sub

    Protected Sub lnkCallByMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallByMonth.Click
        GetRedirectMenu(SubModuleType.CALLBYMONTHADV)
    End Sub
    Protected Sub lnkMSSEnquiry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMSSEnquiry.Click
        GetRedirectMenu(SubModuleType.CUZMSSENQUIRY)
    End Sub
    Protected Sub lnkSFMSEnquiry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSEnquiry.Click
        GetRedirectMenu(SubModuleType.CUZSFMSENQUIRY)
    End Sub
    Protected Sub lnkCustProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustProfile.Click
        GetRedirectMenu(SubModuleType.CUSTPROFILEENQ)
    End Sub
    Protected Sub lnkCallByDay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallByDay.Click
        GetRedirectMenu(SubModuleType.CALLBYDAYADV)
    End Sub

    Protected Sub lnkMSSExtraction_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMSSExtraction.Click
        GetRedirectMenu(SubModuleType.MSSEXTRACTION)
    End Sub


    Protected Sub lnkMissCallAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMissCallAnalysis.Click
        GetRedirectMenu(SubModuleType.MISSCALLANALYSIS)
    End Sub

    Protected Sub lnkSalesByChain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalesByChain.Click
        GetRedirectMenu(SubModuleType.SALES_BY_CHAIN)
    End Sub

    Protected Sub lnkRemainCall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemainCall.Click
        GetRedirectMenu(SubModuleType.REMAIN_CALL)
    End Sub


    Protected Sub lnkStkConsumpByBiMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStkConsumpByBiMonth.Click
        GetRedirectMenu(SubModuleType.STK_CONSUMP_BY_BIMONTH)
    End Sub

    Protected Sub lnkStkConsumpByQtr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStkConsumpByQtr.Click
        GetRedirectMenu(SubModuleType.STK_CONSUMP_BY_QUARTER)
    End Sub

    Protected Sub lnkSrMthActByPrd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSrMthActByPrd.Click
        GetRedirectMenu(SubModuleType.FIELDFORCEMTHACTYPrd)
    End Sub

    Protected Sub lnkStkConsumpByQuadMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStkConsumpByQuadMonth.Click
        GetRedirectMenu(SubModuleType.STK_CONSUMP_BY_QUADMONTH)
    End Sub
    Protected Sub lnkActCallKPI_Click(sender As Object, e As EventArgs) Handles lnkActCallKPI.Click
        GetRedirectMenu(SubModuleType.ActCallKPI)
    End Sub

    Protected Sub lnkSFMSExtractionExtraCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSExtractionExtraCat.Click
        GetRedirectMenu(SubModuleType.SFMSEXTRACTION_EXTRACAT)
    End Sub
    Protected Sub lnkSFMSExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSExtract.Click
        GetRedirectMenu(SubModuleType.SFMS_EXTRACT)
    End Sub
    Protected Sub lnkVisitExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkVisitExtract.Click
        GetRedirectMenu(SubModuleType.VISIT_EXTRACT)
    End Sub
    Protected Sub lnkCustContExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustContExtract.Click
        GetRedirectMenu(SubModuleType.CUST_CONT_EXTRACT)
    End Sub
    Protected Sub lnkCustContPrdExtract_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustContPrdExtract.Click
        GetRedirectMenu(SubModuleType.CUST_CONT_PRD_EXTRACT)
    End Sub

    Protected Sub lnkTraReportEDI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTraReportEDI.Click
        GetRedirectMenu(SubModuleType.TRAREPORT_EDI)
    End Sub

    Protected Sub lnkDetCoverageByProd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDetCoverageByProd.Click
        GetRedirectMenu(SubModuleType.DET_COVERAGE_BY_SPECIALTY)
    End Sub

    Protected Sub lnkDetCallByArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDetCallByArea.Click
        GetRedirectMenu(SubModuleType.DET_CALL_BY_AREA)
    End Sub

    Protected Sub lnkMustSell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMustSell.Click
        GetRedirectMenu(SubModuleType.MUSTSELL)
    End Sub

    Protected Sub lnkCallByClassByProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCallByClassByProduct.Click
        GetRedirectMenu(SubModuleType.DET_COVERAGE_CALL_BY_CLASS_BY_PRODUCT)
    End Sub

    Protected Sub lnkOnlineGallery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOnlineGallery.Click
        GetRedirectMenu(SubModuleType.ONLINEGALLERY)
    End Sub

    Protected Sub lnkMerchandising_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMerchandising.Click
        GetRedirectMenu(SubModuleType.MERCHANDISING)
    End Sub
    Protected Sub lnkCompetitor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompetitor.Click
        GetRedirectMenu(SubModuleType.COMPETITOR)
    End Sub
    Protected Sub lnkFFMAPushNotification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPushNotification.Click
        GetRedirectMenu(SubModuleType.PUSHNOTIFICATION)
    End Sub
    Protected Sub lnkViewSalesmanActivity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewSalesmanActivity.Click
        GetRedirectMenu(SubModuleType.VIEW_SALESMAN_ACTIVITY)
    End Sub
#End Region

#Region "ADMIN"
    Protected Sub lnlSAPList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlSAPList.Click
        GetRedirectMenu(122)
    End Sub
    Protected Sub lnlSAPListV2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlSAPListV2.Click
        GetRedirectMenu(225)
    End Sub
    Protected Sub lnkSRGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRGrp.Click
        GetRedirectMenu(226)
    End Sub
    Protected Sub lnlSAPListV3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnlSAPListV3.Click
        GetRedirectMenu(227)
    End Sub
    Protected Sub lnkSapListV4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSapListV4.Click
        GetRedirectMenu(242)
    End Sub
    Protected Sub lnkCfgDevice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCfgDevice.Click
        GetRedirectMenu(264)
    End Sub
    Protected Sub lnkCfgDeviceDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCfgDeviceDtl.Click
        GetRedirectMenu(265)
    End Sub
    Protected Sub lnkBCP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBCP.Click
        GetRedirectMenu(SubModuleType.BCP)
    End Sub
    Protected Sub lnkSFMSCfg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSFMSCfg.Click
        GetRedirectMenu(SubModuleType.SFMS_CFG)
    End Sub
    Protected Sub lnkMSSCfg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMSSCfg.Click
        GetRedirectMenu(SubModuleType.MSS_CFG)
    End Sub
#End Region

    'Protected Sub lnkCustom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustom.Click
    '    GetRedirectMenu(0)
    'End Sub


#Region "FFMA"
    Protected Sub lnkFFMAPrdCatalog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPrdCatalog.Click
        GetRedirectMenu(SubModuleType.PRDCATALOG)
    End Sub

    Protected Sub lnkFFMASyncImg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASyncImg.Click
        GetRedirectMenu(SubModuleType.SYNCIMAGE)
    End Sub

    Protected Sub lnkFFMACompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACompany.Click
        GetRedirectMenu(SubModuleType.COMPANY)
    End Sub

    Protected Sub lnkFFMASalesArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASalesArea.Click
        GetRedirectMenu(SubModuleType.SALESAREA)
    End Sub

    Protected Sub lnkFFMAWarehouse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAWarehouse.Click
        GetRedirectMenu(SubModuleType.WAREHOUSE)
    End Sub

    Protected Sub lnkFFMARegion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMARegion.Click
        GetRedirectMenu(SubModuleType.REGION)
    End Sub

    Protected Sub lnkFFMASalesTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASalesTeam.Click
        GetRedirectMenu(SubModuleType.SALESTEAM)
    End Sub

    Protected Sub lnkFFMACustomer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustomer.Click
        GetRedirectMenu(SubModuleType.CUSTOMER)
    End Sub
    Protected Sub lnkFFMACustGenerateLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustGenerateLoc.Click
        GetRedirectMenu(SubModuleType.CUSTGENERATELOC)
    End Sub
    Protected Sub lnkFFMAShipto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAShipto.Click
        GetRedirectMenu(SubModuleType.SHIPTO)
    End Sub

    Protected Sub lnkFFMAContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAContact.Click
        GetRedirectMenu(SubModuleType.CONTACT)
    End Sub

    Protected Sub lnkFFMAProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAProduct.Click
        GetRedirectMenu(SubModuleType.PRODUCT)
    End Sub

    Protected Sub lnkFFMAProductGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAProductGroup.Click
        GetRedirectMenu(SubModuleType.PRODUCTGROUP)
    End Sub

    Protected Sub lnkFFMASupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASupplier.Click
        GetRedirectMenu(SubModuleType.SUPPLIER)
    End Sub

    Protected Sub lnkFFMAStockAlloc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAStockAlloc.Click
        GetRedirectMenu(SubModuleType.STOCKALLOC)
    End Sub

    Protected Sub lnkFFMAProductMatrix_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAProductMatrix.Click
        GetRedirectMenu(SubModuleType.PRODUCT_MATRIX)
    End Sub

    Protected Sub lnkFFMAStandardPrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAStandardPrice.Click
        GetRedirectMenu(SubModuleType.STANDARDPRICE)
    End Sub

    Protected Sub lnkFFMASpecialPrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASpecialPrice.Click
        GetRedirectMenu(SubModuleType.SPECIALPRICE)
    End Sub

    Protected Sub lnkFFMACustGrpPrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustGrpPrice.Click
        GetRedirectMenu(SubModuleType.CUSTGRPPRICE)
    End Sub

    Protected Sub lnkFFMAPriceGrpPrice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPriceGrpPrice.Click
        GetRedirectMenu(SubModuleType.PRICEGRPPRICE)
    End Sub

    Protected Sub lnkFFMAStandardBonus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAStandardBonus.Click
        GetRedirectMenu(SubModuleType.STANDARDBONUS)
    End Sub

    Protected Sub llnkFFMASpecialBonus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASpecialBonus.Click
        GetRedirectMenu(SubModuleType.SPECIALBONUS)
    End Sub

    Protected Sub lnkFFMACustGrpBonus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustGrpBonus.Click
        GetRedirectMenu(SubModuleType.CUSTGRPBONUS)
    End Sub

    Protected Sub lnkFFMAPriceGrpBonus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPriceGrpBonus.Click
        GetRedirectMenu(SubModuleType.PRICEGRPBONUS)
    End Sub

    Protected Sub lnkFFMAPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPackage.Click
        GetRedirectMenu(SubModuleType.PACKAGE)
    End Sub

    Protected Sub lnkFFMAFieldActy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldActy.Click
        GetRedirectMenu(SubModuleType.FIELDACTY)
    End Sub

    Protected Sub lnkFFMAMSSAdv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAMSSAdv.Click
        GetRedirectMenu(SubModuleType.MSSADV)
    End Sub

    Protected Sub lnkFFMAFieldForce_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForce.Click
        GetRedirectMenu(SubModuleType.FIELDFORCE)
    End Sub

    Protected Sub lnkFFMASalesTgt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMASalesTgt.Click
        GetRedirectMenu(SubModuleType.SALESTGT)
    End Sub

    Protected Sub lnkFFMAReason_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAReason.Click
        GetRedirectMenu(SubModuleType.REASON)
    End Sub

    Protected Sub lnkFFMARoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMARoute.Click
        GetRedirectMenu(SubModuleType.ROUTE)
    End Sub

    Protected Sub lnkFFMAPriceGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPriceGroup.Click
        GetRedirectMenu(SubModuleType.PRICEGROUP)
    End Sub

    Protected Sub lnkFFMAInbox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAInbox.Click
        GetRedirectMenu(SubModuleType.INBOX)
    End Sub

    Protected Sub lnkFFMAOutbox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAOutbox.Click
        GetRedirectMenu(SubModuleType.OUTBOX)
    End Sub

    Protected Sub lnkFFMAEasyLoader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAEasyLoader.Click
        GetRedirectMenu(SubModuleType.EASYLOADER)
    End Sub

    Protected Sub lnkFFMAFieldForceCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForceCust.Click
        GetRedirectMenu(SubModuleType.FIELDFORCECUST)
    End Sub

    Protected Sub lnkFFMAFieldForceRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForceRoute.Click
        GetRedirectMenu(SubModuleType.FIELDFORCEROUTE)
    End Sub

    Protected Sub lnkFFMAFieldForcePrdGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForcePrdGrp.Click
        GetRedirectMenu(SubModuleType.FIELDFORCEPRDGRP)
    End Sub

    Protected Sub lnkFFMAFieldForceActy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForceActy.Click
        GetRedirectMenu(SubModuleType.FIELDFORCEACTY)
    End Sub

    Protected Sub lnkFFMAFieldForceMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForceMSS.Click
        GetRedirectMenu(SubModuleType.FIELDFORCEMSS)
    End Sub

    Protected Sub lnkFFMAFieldForcePackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAFieldForcePackage.Click
        GetRedirectMenu(SubModuleType.FIELDFORCEPACKAGE)
    End Sub

    Protected Sub lnkFFMAStockTransferList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAStockTransferList.Click
        GetRedirectMenu(SubModuleType.STOCKTRANSFERLIST)
    End Sub

    Protected Sub lnkFFMANewEqpList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMANewEqpList.Click
        GetRedirectMenu(SubModuleType.NEWEQPLIST)
    End Sub

    Protected Sub lnkFFMAApplyStockList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAApplyStockList.Click
        GetRedirectMenu(SubModuleType.APPLYSTOCKLIST)
    End Sub
    Protected Sub lnkFFMATechnicianSalesGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMATechnicianSalesGrp.Click
        GetRedirectMenu(SubModuleType.TECHNICIANSALESGRP)
    End Sub

    Protected Sub lnkFFMATechnicianFuncLoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMATechnicianFuncLoc.Click
        GetRedirectMenu(SubModuleType.TECHNICIANFUNCLOC)
    End Sub

    Protected Sub lnkFFMATechnicianSalesOff_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMATechnicianSalesOff.Click
        GetRedirectMenu(SubModuleType.TECHNICIANSALESOFF)
    End Sub

    Protected Sub lnkFFMAManufacturerList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAManufacturerList.Click
        GetRedirectMenu(SubModuleType.MANUFACTURER)
    End Sub

    Protected Sub lnkFFMAEqpHistList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAEqpHistList.Click
        GetRedirectMenu(SubModuleType.EQPHISTLIST)
    End Sub

    Protected Sub lnkFFMAUPLOADITINERARY_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAUPLOADITINERARY.Click
        GetRedirectMenu(213)
    End Sub


    Protected Sub lnkFFMAMssContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAMssContact.Click
        GetRedirectMenu(SubModuleType.MSS_CONT)
    End Sub


    Protected Sub lnkFFMAMssContactDtl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAMssContactDtl.Click
        GetRedirectMenu(SubModuleType.MSS_CONT_DTL)
    End Sub

    Protected Sub lnkFFMACustInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustInfo.Click
        GetRedirectMenu(SubModuleType.CUSTINFO)
    End Sub

    Protected Sub lnkFFMACustClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustClass.Click
        GetRedirectMenu(SubModuleType.CUSTCLASS)
    End Sub

    Protected Sub lnkFFMACustSalesTgt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustSalesTgt.Click
        GetRedirectMenu(SubModuleType.CUSTSALESTGT)
    End Sub

    Protected Sub lnkCustSalesAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustSalesAnalysis.Click
        GetRedirectMenu(SubModuleType.CUSTSALESANALYSIS)
    End Sub
    Protected Sub lnkFFMACustProfileMaintain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACustProfileMaintain.Click
        GetRedirectMenu(SubModuleType.CUSTPROFILEMAINTAIN)
    End Sub

    Protected Sub lnkCustPrdSalesAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustPrdSalesAnalysis.Click
        GetRedirectMenu(SubModuleType.CUSTPRDSALESANALYSIS)
    End Sub

    Protected Sub lnkSOVariance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSOVariance.Click
        GetRedirectMenu(SubModuleType.SOVARIANCE)
    End Sub

    Protected Sub lnkFFMSTeamMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMSTeamMap.Click
        GetRedirectMenu(SubModuleType.MSS_TEAM_MAPPING)
    End Sub

    Protected Sub lnkFFMAEasyLoaderV2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAEasyLoaderV2.Click
        GetRedirectMenu(SubModuleType.EASYLOADERV2)
    End Sub
    Protected Sub lnkELAccessRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkELAccessRight.Click
        GetRedirectMenu(SubModuleType.ELACCESSRIGHT)
    End Sub

    Protected Sub lnkFFMAMSSAdv2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAMSSAdv2.Click
        GetRedirectMenu(SubModuleType.MSSADV2)
    End Sub

    Protected Sub lnkFFMATargetProductList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMATargetProductList.Click
        GetRedirectMenu(SubModuleType.CUSTTGTPRD)
    End Sub

    Protected Sub lnkFFMATeamClassList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMATeamClassList.Click
        GetRedirectMenu(SubModuleType.CUST_TEAM_CLASS)
    End Sub

    Protected Sub lnkFFMAPrdSeqList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPrdSeqList.Click
        GetRedirectMenu(SubModuleType.PRDSEQ)
    End Sub
    Protected Sub lnkFFMAPromoSO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPromoSO.Click
        GetRedirectMenu(SubModuleType.PROMO_SO)
    End Sub
    Protected Sub lnkFFMADetailingBrand_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMADetailingBrand.Click
        GetRedirectMenu(SubModuleType.DETAIL_BRAND)
    End Sub

    Protected Sub lnkFFMACompBU_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACompBU.Click
        GetRedirectMenu(SubModuleType.COMPANYCODE_BUPROFILE)
    End Sub
    Protected Sub lnkFFMACompBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACompBank.Click
        GetRedirectMenu(SubModuleType.COMPANYCODE_BANKPROFILE)
    End Sub
    Protected Sub lnkFFMACollectorProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACollectorProfile.Click
        GetRedirectMenu(SubModuleType.COLLECTORPROFILE)
    End Sub
    Protected Sub lnkFFMACollectorCustMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMACollectorCustMapping.Click
        GetRedirectMenu(SubModuleType.COLLECTORCUSTOMERMAPPING)
    End Sub
    Protected Sub lnkFFMAPrdMustSell_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPrdMustSell.Click
        GetRedirectMenu(SubModuleType.PRODUCTMUSTSELL)
    End Sub
    Protected Sub lnkFFMALead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMALead.Click
        GetRedirectMenu(SubModuleType.LEAD)
    End Sub
    Protected Sub lnkFFMAPrdMustSellAdv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFFMAPrdMustSellAdv.Click
        GetRedirectMenu(SubModuleType.PRODUCTMUSTSELLADV)
    End Sub

#End Region

    Protected Sub lnkTRAlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTRAlist.Click
        GetRedirectMenu(SubModuleType.TRAList)
    End Sub

#Region "FFMS"

    Protected Sub lnkLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeave.Click
        GetRedirectMenu(SubModuleType.LEAVE)
    End Sub
#End Region

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub












End Class
