'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Class Common for Query
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

'Imports System.EnterpriseServices
'Imports System.Runtime.InteropServices
Imports System.ComponentModel

'Interface Icor_CommonQuery
'    Function GetTemplateList(ByVal UserID As Long) As DataTable
'End Interface

'<Guid("4B8B0D32-81AC-45df-ADF8-769D8635FBC6"), 
'<Transaction(TransactionOption.Supported), ClassInterface(ClassInterfaceType.AutoDual)> _
Public Class clsCommonQuery
    'Inherits ServicedComponent
    'Implements Icor_CommonQuery
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New clsProperties.clsCommonQuery

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetCountryList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetCountryList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = " SELECT *" & _
                     " FROM TBL_MST_COUNTRY WITH (NOLOCK)" & _
                     " WHERE delete_flag <> 1" & _
                     " AND country_code IN (" & _
                     " SELECT DISTINCT country_code" & _
                     " FROM TBL_ADM_USERACCESSRIGHT UAR WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_COUNTRY C WITH (NOLOCK) ON UAR.country_id = C.country_id" & _
                     " WHERE UAR.user_code=@user_code" & _
                     " AND UAR.delete_flag <> 1" & _
                     " )"
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .addItem("user_code", clsProperties.UserCode, cor_DB.clsDB.DataType.DBString)
                GetCountryList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetCountryList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetLanguageList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetLanguageList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = "SELECT * FROM TBL_ADM_LANGUAGE WITH (NOLOCK) WHERE delete_flag <> 1"
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                GetLanguageList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetLanguageList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetOperationTypeList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetOperationTypeList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = "SELECT * FROM TBL_MST_OPERATION_TYPE WITH (NOLOCK) WHERE delete_flag <> 1"
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                GetOperationTypeList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetOperationTypeList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetPrincipalList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetPrincipalList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            ' " AND C.country_code=@country_code" & _
            strSQL = " SELECT P.*,C.country_code,C.country_name" & _
                     " FROM TBL_MST_PRINCIPAL P WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_COUNTRY C WITH (NOLOCK) ON P.country_id = C.country_id" & _
                     " WHERE P.delete_flag <> 1" & _
                     " AND P.principal_code IN (" & _
                     " SELECT DISTINCT principal_code" & _
                     " FROM TBL_ADM_USERACCESSRIGHT UAR WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_PRINCIPAL P WITH (NOLOCK) ON UAR.principal_id = P.principal_id" & _
                     " WHERE UAR.user_code=@user_code" & _
                     " AND UAR.country_id=C.country_id" & _
                     " AND UAR.delete_flag <> 1" & _
                     " )"

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .addItem("country_code", clsProperties.CountryCode, cor_DB.clsDB.DataType.DBString)
                .addItem("user_code", clsProperties.UserCode, cor_DB.clsDB.DataType.DBString)
                GetPrincipalList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetPrincipalList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetBranchList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetBranchList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = " SELECT B.*,C.country_code,C.country_name" & _
                     " FROM TBL_MST_BRANCH B WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_COUNTRY C WITH (NOLOCK) ON B.country_id = C.country_id" & _
                     " WHERE C.country_code=@country_code" & _
                     " AND B.delete_flag <> 1"

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .addItem("country_code", clsProperties.CountryCode, cor_DB.clsDB.DataType.DBString)
                GetBranchList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetBranchList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSalesTeamList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSalesTeamList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = " SELECT ST.*,P.principal_code,P.principal_name,C.country_code,C.country_name" & _
                     " FROM TBL_MST_SALESTEAM ST WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_PRINCIPAL P WITH (NOLOCK) ON P.principal_id = ST.principal_id" & _
                     " INNER JOIN TBL_MST_COUNTRY C WITH (NOLOCK) ON P.country_id = C.country_id" & _
                     " WHERE P.principal_code=@principal_code" & _
                     " AND ST.delete_flag <> 1"

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .addItem("principal_code", clsProperties.PrincipalCode, cor_DB.clsDB.DataType.DBString)
                GetSalesTeamList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetSalesTeamList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSalesRepList
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSalesRepList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = " SELECT SR.*,ST.salesteam_code,ST.salesteam_name,P.principal_code,P.principal_name,C.country_code,C.country_name" & _
                     " FROM TBL_MST_SALESREP SR WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_SALESTEAM ST WITH (NOLOCK) ON ST.salesteam_id = SR.salesteam_id" & _
                     " INNER JOIN TBL_MST_PRINCIPAL P WITH (NOLOCK) ON P.principal_id = ST.principal_id" & _
                     " INNER JOIN TBL_MST_COUNTRY C WITH (NOLOCK) ON P.country_id = C.country_id" & _
                     " WHERE ST.salesteam_code=@salesteam_code  and ST.principal_id=@principal_id " & _
                     " AND SR.delete_flag <> 1" & _
                     " ORDER BY SR.SALESREP_NAME"

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .addItem("salesteam_code", clsProperties.SalesTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("principal_id", clsProperties.PrincipalID, cor_DB.clsDB.DataType.DBInt)
                GetSalesRepList = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetSalesRepList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    '---------------------------------------------------------------------
    ' Procedure 	: 	Function GetSalesRepListByAccessRightID
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : Datatable
    '----------------------------------------------------------------------
    '<AutoComplete()> _
    Public Function GetSalesRepListByAccessRightID() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim strSQL As String

        Try
            strSQL = " SELECT DISTINCT SR.salesrep_id,SR.salesteam_id,SR.region_id,SR.area_id,RTRIM(LTRIM(SR.salesrep_code)) AS salesrep_code,SR.salesrep_name,SR.salesrep_name_1,SR.created_date,SR.creator_user_id,SR.changed_date,SR.changed_user_id,SR.delete_flag," & _
                     " ST.SALESTEAM_NAME + '-' +SR.salesrep_code + ' - ' + SR.salesrep_name as salesrep_desc," & _
                     " ST.salesteam_code,ST.salesteam_name,P.principal_code,P.principal_name,C.country_code,C.country_name" & _
                     " FROM TBL_MST_SALESREP SR WITH (NOLOCK)" & _
                     " INNER JOIN TBL_MST_SALESTEAM ST WITH (NOLOCK) ON ST.salesteam_id = SR.salesteam_id and st.delete_flag <> 1" & _
                     " INNER JOIN TBL_MST_PRINCIPAL P WITH (NOLOCK) ON P.principal_id = ST.principal_id and P.delete_flag <> 1" & _
                     " INNER JOIN TBL_MST_COUNTRY C WITH (NOLOCK) ON P.country_id = C.country_id and C.delete_flag <> 1" & _
                     " INNER JOIN TBL_ADM_ACCESSRIGHT AR WITH (NOLOCK) ON AR.principal_id = P.principal_id AND AR.delete_flag <> 1" & _
                     " WHERE AR.accessright_id IN (" & Trim(clsProperties.accessright_id) & ")" & _
                     " AND SR.delete_flag <> 1" & _
                     " ORDER BY ST.SALESTEAM_NAME,SR.salesrep_name,salesrep_code"

            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                GetSalesRepListByAccessRightID = .Retrieve(strSQL)
            End With
            Exit Function

        Catch ex As Exception
            Throw (New ExceptionMsg("cor_CommonQuery.clsCommonQuery.GetSalesRepListByAccessRightID" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
