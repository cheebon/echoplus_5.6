<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanCust.aspx.vb" Inherits="iFFMS_Plan_PlanCust"  EnableEventValidation="false"%>
<%@ Register TagPrefix="customToolkit" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pre Plan Cust Addon</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript">
     function ChangeAllCheckBoxStates(checkState)
   { var dglist = document.getElementById('dglistNewCust');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
        function ChangeAllCheckBoxStates2(checkState)
   { var dglist = document.getElementById('dglistSelectedCust');
       for (var i = 1; i < dglist.rows.length; i++)
       { if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;}}
       
   function ValidateCheckBoxStates()
   { var dglist = document.getElementById('dglistSelectedCust');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {var agree=confirm('Are you sure you want to continue?');if(agree)return true;else return false;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}     
   }
   
    function ValidateCheckBoxStatesNew()
   { var dglist = document.getElementById('dglistNewCust');
        var selected; selected = 0;
       for (var i = 1; i < dglist.rows.length; i++)
       {if(dglist.rows[i].cells[0].childNodes[0]){
       if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") {
       if (dglist.rows[i].cells[0].childNodes[0].checked) selected = selected + 1;}}}
       if (selected > 0)
       {return true;} 
       else  {alert('Kindly select by checking the checkbox in the list!'); return false;}
   }
    
        function ClickBtn() {
            __doPostBack('btnrefresh');
        }
 
    </script>


</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('ContentBar');HideElement('DetailBar');ShowElement('TopBar'); MaximiseFrameHeight('TopBarIframe')"
    class="BckgroundInsideContentLayout">
    <form id="frmpreplancust" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <asp:Timer ID="TimerControl2" runat="server" Enabled="False" Interval="100" OnTick="TimerControl2_Tick" />
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    </div>
                    <div id="txnContent">
                        <asp:UpdatePanel ID="UpdateSelectedCust" runat="server" UpdateMode="Conditional"
                            RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlSelectedCust" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlSelectedCust_Tick" />
                                <div id="SelectedCust" class="S_DivHeader">
                                    Plan Addon By Customer
                                </div>
                                <asp:Panel ID="pSelectedCust" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <asp:Label ID="lblSmsg" runat="server" CssClass="cls_label_header" Text="" Font-Italic="true"
                                        BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckalldelete" value="Check All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(true);" visible="false" style="width: 80px"
                                                validationgroup="deletecust" runat="server" />
                                            <input type="Button" id="btnuncheckalldelete" value="Uncheck All" class="cls_button"
                                                onclick="ChangeAllCheckBoxStates2(false);" visible="false" style="width: 80px"
                                                validationgroup="deletecust" runat="server" />
                                            <asp:Button ID="btndelete" runat="server" Text="Delete" CssClass="cls_button" ValidationGroup="deletecust"
                                                Visible="false" Width="80px" OnClientClick="return ValidateCheckBoxStates();" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 5px;">
                                        <table width="98%">
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label_header">Year :</span></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlyear" runat="server" CssClass="cls_dropdownlist" Width="100px"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label_header">Month :</span></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlmonth" runat="server" CssClass="cls_dropdownlist" Width="100px"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="cls_label_header">Route Date :</span></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlrouteDate" runat="server" CssClass="cls_dropdownlist" Width="200px"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="display: none;">
                                        <asp:Button ID="btnrefresh" runat="server" CssClass="cls_button" Text="Refresh" />
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistSelectedCust" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="ROUTE_DATE,CUST_CODE">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>
                                                There is no data added.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkdelete" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <%--      <asp:BoundField DataField="LINE_NO" HeaderText="Line No" ReadOnly="True" SortExpression="LINE_NO">
                                                    <itemstyle horizontalalign="CENTER" />
                                                </asp:BoundField>--%>
                                                 <asp:BoundField DataField="VISIT_IND" HeaderText="Visit Ind" ReadOnly="True" SortExpression="VISIT_IND">
                                                    <itemstyle horizontalalign="CENTER" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ROUTE_DATE" HeaderText="Route Date" ReadOnly="True" SortExpression="ROUTE_DATE">
                                                    <itemstyle horizontalalign="CENTER" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CUST_CODE" HeaderText="Customer code" ReadOnly="True"
                                                    SortExpression="CUST_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True"
                                                    SortExpression="CUST_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                                <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="CONT_DEPT" Headertext="Cont. Dept." >
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                                <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="CONT" Headertext="Contact">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                                <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="PRD_GRP" Headertext="Prd. Grp.">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                                <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="FACT" Headertext="Field Acty.">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                                <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="MSS" Headertext="Market Survey">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                                <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="RMKS" Headertext="Remarks">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                                 <asp:ButtonField Text="<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                                                    CommandName="VISIT" Headertext="Visit">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:ButtonField>
                                               
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeSelectedCust" runat="server" TargetControlID="pSelectedCust"
                                    ExpandControlID="SelectedCust" CollapseControlID="SelectedCust">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdateNewCust" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>
                                <asp:Timer ID="TimerControlNewCust" runat="server" Enabled="False" Interval="100"
                                    OnTick="TimerControlNewCust_Tick" />
                                <div id="NewCust" class="S_DivHeader">
                                    Add New Plan Customer By Date
                                </div>
                                <asp:Panel ID="PNewCust" runat="server" Width="99.8%" CssClass="cls_panel_header">
                                    <asp:Label ID="lblNmsg" runat="server" CssClass="cls_label_header" Text="" Font-Italic="true"
                                        BackColor="AliceBlue"></asp:Label>
                                    <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                                        <div>
                                            <input type="Button" id="btncheckall" value="Check All" class="cls_button" onclick="ChangeAllCheckBoxStates(true);"
                                                visible="false" style="width: 80px" validationgroup="newcust" runat="server" />
                                            <input type="Button" id="btnuncheckall" value="Uncheck All" class="cls_button" onclick="ChangeAllCheckBoxStates(false);"
                                                visible="false" style="width: 80px" validationgroup="newcust" runat="server" />
                                            <asp:Button ID="btnadd" runat="server" Text="Add" CssClass="cls_button" ValidationGroup="newcust"
                                                OnClientClick="return ValidateCheckBoxStatesNew();" Visible="false" Width="80px" />
                                        </div>
                                    </div>
                                    <div style="width: 98%; padding-left: 5px;">
                                        <table width="98%">
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label_header">Planned Date :</span></td>
                                                <td>
                                                    <asp:TextBox ID="txtpdate" runat="server" CssClass="cls_textbox" ValidationGroup="newcust"></asp:TextBox>
                                                    <asp:ImageButton ID="imgpdate" ImageUrl="~/images/icoCalendar.gif" runat="server"
                                                        CssClass="cls_button" CausesValidation="false" />
                                                    <ajaxToolkit:CalendarExtender ID="ceexpdate" TargetControlID="txtpdate" runat="server"
                                                        PopupButtonID="imgpdate" Animated="false" Format="yyyy-MM-dd" PopupPosition="Topleft">
                                                    </ajaxToolkit:CalendarExtender>
                                                    <asp:CustomValidator ID="rfvFormatDate" runat="server" Display="Dynamic" ControlToValidate="txtpdate"
                                                        CssClass="cls_validator" ValidateEmptyText="false" ValidationGroup="newcust" />
                                                    <asp:CompareValidator ID="rfvCheckDataType" runat="server" CssClass="cls_validator"
                                                        ControlToValidate="txtpdate" Display="Dynamic" Operator="DataTypeCheck" Type="Date"
                                                        ErrorMessage=" Invalid Date!" ValidationGroup="newcust" />
                                                    <asp:RequiredFieldValidator ID="rfetxtpdate" runat="server" ErrorMessage="Select a date!"
                                                        Display="Dynamic" ControlToValidate="txtpdate" ValidationGroup="newcust" CssClass="cls_validator">
                                                    </asp:RequiredFieldValidator>
                                                   <asp:CompareValidator runat="server" ID="CVtxtpdate" ControlToValidate="txtpdate"
                                                        CssClass="cls_validator" Display="Dynamic" ValueToCompare="<%# DateTime.Today.ToShortDateString() %>"
                                                        Type="date" Operator="GreaterThanEqual" ErrorMessage="Date selected less than today." ValidationGroup="newcust" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <span class="cls_label_header">Search By :</span></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlsearchby" runat="server" AutoPostBack="True" CssClass="cls_dropdownlist"
                                                        Width="144px">
                                                            <asp:ListItem Selected="True" Text="Route"  Value="ALL" ></asp:ListItem>
                                                            <asp:ListItem Text="Customer Code"  Value="CUST_CODE"  ></asp:ListItem>
                                                            <asp:ListItem Text="Customer Name"  Value="CUST_NAME"  ></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <asp:Label ID="lblroutename" runat="server" Text="Route :" cssclass="cls_label_header" Visible="true"></asp:Label>
                                                    <asp:Label ID="lblcustcode" runat="server" Text="Customer Code :" cssclass="cls_label_header"  Visible="false"></asp:Label>
                                                    <asp:Label ID="lblcustname" runat="server" Text="Customer Name :" cssclass="cls_label_header"  Visible="false"></asp:Label>
                                                <td>
                                                    <asp:DropDownList ID="ddlRoute" runat="server" AutoPostBack="FALSE" CssClass="cls_dropdownlist"  Visible="true"
                                                        Width="144px">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtcustcode" runat="server" CssClass="cls_textbox"  Visible="false" Width="100px" MaxLength="50"></asp:TextBox>
                                                    <asp:TextBox ID="txtcustname" runat="server" CssClass="cls_textbox"  Visible="false" Width="200px" MaxLength="50"></asp:TextBox>
                                                    <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="cls_button"   ValidationGroup="newcust"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="width: 98%; padding-left: 10px; padding-right: 10px; padding-top: 10px;
                                        padding-bottom: 10px;">
                                        <ccGV:clsGridView ID="dglistNewCust" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="CUST_CODE">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <itemtemplate>
                                                        <asp:CheckBox id="chkselect" runat="server"></asp:CheckBox>
                                                        </itemtemplate>
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code" ReadOnly="True"
                                                    SortExpression="CUST_CODE">
                                                    <itemstyle horizontalalign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True"
                                                    SortExpression="CUST_NAME">
                                                    <itemstyle horizontalalign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <customToolkit:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                            </EmptyDataTemplate>
                                        </ccGV:clsGridView>
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:CollapsiblePanelExtender ID="cpeNewCust" runat="server" TargetControlID="PNewCust"
                                    ExpandControlID="NewCust" CollapseControlID="NewCust">
                                </ajaxToolkit:CollapsiblePanelExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </form>
</body>
</html>
