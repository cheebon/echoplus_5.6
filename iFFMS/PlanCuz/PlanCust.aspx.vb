﻿Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_PlanCuz_PlanCust
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Public Enum dgcolSelectedCust As Integer
        ROUTE_DATE = 2
        CUST_CODE = 3
        CUST_NAME = 4
        CONT_CODE = 5
        CONT_NAME = 6
    End Enum

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountSelectedCust() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSelectedCust"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSelectedCust") = value
        End Set
    End Property

    Private Property Master_Row_CountNewCust() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountNewCust"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountNewCust") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()
   

        'Call Header
        With wuc_lblHeader
            .Title = "Plan Addon By Customer Advance"
            .DataBind()
        End With


        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            ScriptManager.RegisterClientScriptInclude(Me, Me.GetType, "jswuc_txtCalendarRange", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/datefc.js")
            txtpdate.Attributes.Add("onblur", "formatDate('" & txtpdate.ClientID & "', '" & "yyyy-MM-dd" & "');")
            rfvCheckDataType.ErrorMessage = " Invalid Date!(" & "yyyy-MM-dd" & ")"
            TimerControl2.Enabled = True

        End If

        CVtxtpdate.DataBind()
    End Sub

    Protected Sub TimerControl2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl2.Tick
        If TimerControl2.Enabled Then
            If Trim(Session("SALESREP_CODE")) IsNot Nothing Then
                LoadRouteYearDDL()
                LoadRouteMonthDDL()
                LoadRouteDateDDL()
                LoadRoute()
            End If

            TimerControl2.Enabled = False
        End If
    End Sub

#Region "DGLISTSelectedCust"
    Protected Sub TimerControlSelectedCust_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlSelectedCust.Tick
        If TimerControlSelectedCust.Enabled Then RefreshDatabindingSelectedCust()
        TimerControlSelectedCust.Enabled = False
    End Sub

    Public Sub RenewDataBindSelectedCust()
        RefreshDataBindSelectedCust()
    End Sub

    Public Sub RefreshDataBindSelectedCust()
        RefreshDatabindingSelectedCust()
    End Sub

    Public Sub RefreshDatabindingSelectedCust(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableSelectedCust As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionSelectedCust As String = CType(ViewState("strSortExpressionSelectedCust"), String)


        dtCurrentTableSelectedCust = GetRecListSelectedCust()

        If dtCurrentTableSelectedCust Is Nothing Then
            dtCurrentTableSelectedCust = New DataTable
        Else
            If dtCurrentTableSelectedCust.Rows.Count = 0 Then
                Master_Row_CountSelectedCust = 0
                btncheckalldelete.Visible = False
                btnuncheckalldelete.Visible = False
                btndelete.Visible = False
                'dtCurrentTableSelectedCust.Rows.Add(dtCurrentTableSelectedCust.NewRow())
            Else

                Master_Row_CountSelectedCust = dtCurrentTableSelectedCust.Rows.Count
                btncheckalldelete.Visible = True
                btnuncheckalldelete.Visible = True
                btndelete.Visible = True
            End If
        End If

        Dim dvCurrentViewSelectedCust As New DataView(dtCurrentTableSelectedCust)
        If Not String.IsNullOrEmpty(strSortExpressionSelectedCust) Then
            Dim strSortExpressionNameSelectedCust As String = strSortExpressionSelectedCust.Replace(" DESC", "")
            dvCurrentViewSelectedCust.Sort = IIf(dtCurrentTableSelectedCust.Columns.Contains(strSortExpressionSelectedCust), strSortExpressionSelectedCust, "")
        End If

        With dglistSelectedCust
            .DataSource = dvCurrentViewSelectedCust
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountSelectedCust > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateSelectedCust()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListSelectedCust() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strUserId As String
            strSalesrepCode = Trim(Session("SALESREP_CODE"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = ddlrouteDate.SelectedValue.ToString

            Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
            DT = clsPrePlanCuz.GetPlanAddonCustCont(strSalesrepCode, strRouteDate, strUserId)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateSelectedCust()

        UpdateSelectedCust.Update()

    End Sub

    Protected Sub dglistSelectedCust_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedCust.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedCust > 0 Then
                        'If Session("PRINCIPAL_ID") = "48" Then
                        '    e.Row.Cells(5).Text = ""
                        '    e.Row.Cells(7).Text = ""
                        '    e.Row.Cells(9).Text = ""
                        '    e.Row.Cells(10).Text = ""
                        'End If


                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dglistSelectedCust_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedCust.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedCust > 0 Then
                        'If Session("PRINCIPAL_ID") = "48" Then
                        '    e.Row.Cells(5).Text = ""
                        '    e.Row.Cells(7).Text = ""
                        '    e.Row.Cells(9).Text = ""
                        '    e.Row.Cells(10).Text = ""
                        'End If
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgListSelectedCust_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistSelectedCust.Sorting
        Dim strSortExpressionSelectedCust As String = ViewState("strSortExpressionSelectedCust")

        If strSortExpressionSelectedCust IsNot Nothing AndAlso strSortExpressionSelectedCust.Length > 0 Then
            If strSortExpressionSelectedCust Like (e.SortExpression & "*") Then
                If strSortExpressionSelectedCust.IndexOf(" DESC") > 0 Then
                    strSortExpressionSelectedCust = e.SortExpression
                Else
                    strSortExpressionSelectedCust = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionSelectedCust = e.SortExpression
            End If
        Else
            strSortExpressionSelectedCust = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionSelectedCust") = strSortExpressionSelectedCust

        RefreshDatabindingSelectedCust()

    End Sub

    Protected Sub dglistSelectedCust_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dglistSelectedCust.RowCommand
        Dim strCustCode As String, strContCode As String, strRouteDate As String, strsalesrepcode As String
        If IsNumeric(e.CommandArgument) Then
            strCustCode = dglistSelectedCust.Rows(e.CommandArgument).Cells(dgcolSelectedCust.CUST_CODE).Text
            strContCode = dglistSelectedCust.Rows(e.CommandArgument).Cells(dgcolSelectedCust.CONT_CODE).Text
            strRouteDate = dglistSelectedCust.Rows(e.CommandArgument).Cells(dgcolSelectedCust.ROUTE_DATE).Text
            strsalesrepcode = Trim(Session("SALESREP_CODE"))

            Select Case e.CommandName

                Case "FACT"
                    Dim StrScript As String
                    If Session("PRINCIPAL_ID") = "48" Or Session("PRINCIPAL_ID") = "26" Then
                        StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/PlanCuz/PlanSFMSCuz.aspx?custcode=" + strCustCode + "&ContCode=" + strContCode + "&RouteDate=" + strRouteDate + "&SalesrepCode=" + strsalesrepcode + "';HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');ResizeFrameWidth('ContentBar','100%');MaximiseFrameHeight('TopBarIframe');"
                    Else
                        StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/PlanCuz/PlanSFMS.aspx?custcode=" + strCustCode + "&ContCode=" + strContCode + "&RouteDate=" + strRouteDate + "&SalesrepCode=" + strsalesrepcode + "';HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');ResizeFrameWidth('ContentBar','100%');MaximiseFrameHeight('TopBarIframe');"
                    End If
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
                Case "MSS"
                    Dim StrScript As String
                    StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/PlanCuz/PlanMSS.aspx?custcode=" + strCustCode + "&ContCode=" + strContCode + "&RouteDate=" + strRouteDate + "&SalesrepCode=" + strsalesrepcode + "';HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');ResizeFrameWidth('ContentBar','100%');MaximiseFrameHeight('TopBarIframe');"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
                Case "RMKS"
                    Dim StrScript As String
                    StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/PlanCuz/PlanRmks.aspx?custcode=" + strCustCode + "&ContCode=" + strContCode + "&RouteDate=" + strRouteDate + "&SalesrepCode=" + strsalesrepcode + "';HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');ResizeFrameWidth('ContentBar','100%');MaximiseFrameHeight('TopBarIframe');"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
                Case "VISIT"
                    Dim StrScript As String
                    StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/PlanCuz/PlanTxnMenu.aspx?custcode=" + strCustCode + "&ContCode=" + strContCode + "&SalesrepCode=" + strsalesrepcode + "&RouteDate=" + strRouteDate + "&visitdate=" + Now().ToString("yyyy-MM-dd") + "&visittime=" + Now().ToString("HH:mm") + "';"
                    Dim strAction As String
                    strAction = "ResizeFrameWidth('TopBar','100%');HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar'); ResizeFrameHeight('DetailBarIframe','45px');"
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", strAction + StrScript, True)
            End Select
        End If
    End Sub

#End Region

#Region "DGLISTNewCust"
    Protected Sub TimerControlNewCust_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlNewCust.Tick
        If TimerControlNewCust.Enabled Then RefreshDatabindingNewCust()
        TimerControlNewCust.Enabled = False
    End Sub

    Public Sub RenewDataBindNewCust()
        RefreshDataBindNewCust()
    End Sub

    Public Sub RefreshDataBindNewCust()
        RefreshDatabindingNewCust()
    End Sub

    Public Sub RefreshDatabindingNewCust(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableNewCust As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionNewCust As String = CType(ViewState("strSortExpressionNewCust"), String)


        dtCurrentTableNewCust = GetRecListNewCust()

        If dtCurrentTableNewCust Is Nothing Then
            dtCurrentTableNewCust = New DataTable
        Else
            If dtCurrentTableNewCust.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountSelectedCust = 0
                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btnadd.Visible = False
            Else
                Master_Row_CountSelectedCust = dtCurrentTableNewCust.Rows.Count
                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btnadd.Visible = True
            End If
        End If

        Dim dvCurrentViewNewCust As New DataView(dtCurrentTableNewCust)
        If Not String.IsNullOrEmpty(strSortExpressionNewCust) Then
            Dim strSortExpressionNameSelectedCust As String = strSortExpressionNewCust.Replace(" DESC", "")
            dvCurrentViewNewCust.Sort = IIf(dtCurrentTableNewCust.Columns.Contains(strSortExpressionNewCust), strSortExpressionNewCust, "")
        End If

        With dglistNewCust
            .DataSource = dvCurrentViewNewCust
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountNewCust > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateNewCust()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListNewCust() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strUserId As String, strSearchBy As String, strSearchValue As String, strPlanDate As String
            strSalesrepCode = Trim(Session("SALESREP_CODE"))
            strUserId = Trim(Session("UserID"))
            strSearchBy = ddlsearchby.SelectedValue.ToString

            If ddlsearchby.SelectedValue.ToString = "ROUTE_CODE" Then
                strSearchValue = ddlRoute.SelectedValue
            ElseIf ddlsearchby.SelectedValue.ToString = "CUST_CODE" Then
                strSearchValue = txtcustcode.Text
            ElseIf ddlsearchby.SelectedValue.ToString = "CUST_NAME" Then
                strSearchValue = txtcustname.Text
            ElseIf ddlsearchby.SelectedValue.ToString = "CONT_CODE" Then
                strSearchValue = txtcontcode.Text
            ElseIf ddlsearchby.SelectedValue.ToString = "CONT_NAME" Then
                strSearchValue = txtcontname.Text
            Else
                strSearchValue = ""
            End If

            strPlanDate = txtpdate.Text

            Dim clsCustomerListing As New txn_Customer.clsCustomerListing
            DT = clsCustomerListing.getPlan_CustContListDT(strSalesrepCode, strSearchValue, strSearchBy, strPlanDate)

        End If

            Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateNewCust()

        UpdateNewCust.Update()

    End Sub

    Protected Sub dglistNewCust_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistNewCust.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountNewCust > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListNewCust_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistNewCust.Sorting
        Dim strSortExpressionNewCust As String = ViewState("strSortExpressionNewCust")

        If strSortExpressionNewCust IsNot Nothing AndAlso strSortExpressionNewCust.Length > 0 Then
            If strSortExpressionNewCust Like (e.SortExpression & "*") Then
                If strSortExpressionNewCust.IndexOf(" DESC") > 0 Then
                    strSortExpressionNewCust = e.SortExpression
                Else
                    strSortExpressionNewCust = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionNewCust = e.SortExpression
            End If
        Else
            strSortExpressionNewCust = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionNewCust") = strSortExpressionNewCust

        RefreshDatabindingNewCust()

    End Sub

#End Region

#Region "LoadDDL"
    Protected Sub LoadRouteDateDDL()
        Dim DT As DataTable
        Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
        Dim stryear As String, strmonth As String, strsalesrepcode As String, struserid As String
        stryear = ddlyear.SelectedValue
        strmonth = ddlmonth.SelectedValue
        strsalesrepcode = Trim(Session("SALESREP_CODE"))
        struserid = Trim(Session("UserID"))

        DT = clsPrePlanCuz.GetPlanAddonDateCustCont(stryear, strmonth, strsalesrepcode, struserid)
        With ddlrouteDate
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "ROUTE_DATE"
            .DataValueField = "ROUTE_DATE"
            .DataBind()
            .Items.Insert(0, New ListItem("-- SELECT --", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub LoadRouteMonthDDL()
        Dim DT As DataTable
        Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
        Dim strsalesrepcode As String, struserid As String
        strsalesrepcode = Trim(Session("SALESREP_CODE"))
        struserid = Trim(Session("UserID"))

        DT = clsPrePlanCuz.GetPlanAddonMonthCustCont(strsalesrepcode, struserid)
        With ddlmonth
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "MONTH_NAME"
            .DataValueField = "MONTH_VALUE"
            .DataBind()
            .Items.Insert(0, New ListItem("-- SELECT --", ""))
            If DT.Rows.Count = 0 Then

                .SelectedIndex = 0
            Else
                '.SelectedValue = CInt(Format(Now(), "MM"))
                ' .SelectedIndex = .Items.IndexOf(.Items.FindByValue(CInt(Format(Now(), "MM"))))
            End If

        End With
    End Sub

    Protected Sub LoadRouteYearDDL()
        Dim DT As DataTable
        Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
        Dim strsalesrepcode As String, struserid As String
        strsalesrepcode = Trim(Session("SALESREP_CODE"))
        struserid = Trim(Session("UserID"))

        DT = clsPrePlanCuz.GetPlanAddonYearCustCont(strsalesrepcode, struserid)
        With ddlyear
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "YEAR_NAME"
            .DataValueField = "YEAR_VALUE"
            .DataBind()
            .Items.Insert(0, New ListItem("-- SELECT --", ""))
            If DT.Rows.Count = 0 Then

                .SelectedIndex = 0
            Else
                '.SelectedValue = CInt(Format(Now(), "yyyy").ToString)
                '.SelectedIndex = ddlyear.Items.IndexOf(ddlyear.Items.FindByValue(Format(Now(), "yyyy").ToString))
            End If

        End With

    End Sub

    Protected Sub LoadRoute()
        Dim obj As New txn_Customer.clsCustomerListing
        Dim dt As DataTable = Nothing
        Dim strSalesmanCOde As String
        strSalesmanCOde = Trim(Session("SALESREP_CODE"))
        dt = obj.getRouteDT(strSalesmanCOde)
        If dt.Rows.Count > 0 Then
            ddlRoute.DataSource = dt
            ddlRoute.DataValueField = "route_code"
            ddlRoute.DataTextField = "route_desc"
            ddlRoute.DataBind()
            ddlRoute.Items.Insert(0, "--SELECT--")
            ddlRoute.Items.Insert(1, "All")
        Else
            ddlRoute.Items.Add("None")
        End If

    End Sub
#End Region

#Region "Event Handler"
    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlyear.SelectedIndexChanged
        LoadRouteMonthDDL()
        LoadRouteDateDDL()
    End Sub

    Protected Sub ddlmonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlmonth.SelectedIndexChanged
        LoadRouteDateDDL()
    End Sub

    Protected Sub ddlrouteDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlrouteDate.SelectedIndexChanged
        TimerControlSelectedCust.Enabled = True
    End Sub

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        If Page.IsValid Then
            Dim pdate As DateTime
            pdate = txtpdate.Text
            If pdate > Now() Then
                AddNewCust()
                LoadRouteYearDDL()
                LoadRouteMonthDDL()
                LoadRouteDateDDL()
                RefreshDatabindingSelectedCust()
            ElseIf pdate < Now() Then
                If pdate > DateAdd(DateInterval.Day, -7, Now()) Then
                    AddNewCust()
                    LoadRouteDateDDL()
                    RefreshDatabindingSelectedCust()
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have entered an invalid date! System only accept last 7 days and future dates.')", True)
                End If
            End If
        End If

    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        DelNewCust()
        TimerControlSelectedCust.Enabled = True
    End Sub

    Protected Sub ddlsearchby_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlsearchby.SelectedIndexChanged

        If ddlsearchby.SelectedValue = "ROUTE_CODE" Then 'Route

            lblroutename.Visible = True
            lblcustcode.Visible = False
            lblcustname.Visible = False
            lblcontcode.Visible = False
            lblcontName.Visible = False

            ddlRoute.Visible = True
            txtcustcode.Visible = False
            txtcustname.Visible = False
            txtcontcode.Visible = False
            txtcontname.Visible = False

        ElseIf ddlsearchby.SelectedValue = "CUST_CODE" Then

            lblroutename.Visible = False
            lblcustcode.Visible = True
            lblcustname.Visible = False
            lblcontcode.Visible = False
            lblcontName.Visible = False

            ddlRoute.Visible = False
            txtcustcode.Visible = True
            txtcustname.Visible = False
            txtcontcode.Visible = False
            txtcontname.Visible = False
        ElseIf ddlsearchby.SelectedValue = "CUST_NAME" Then

            lblroutename.Visible = False
            lblcustcode.Visible = False
            lblcustname.Visible = True
            lblcontcode.Visible = False
            lblcontName.Visible = False

            ddlRoute.Visible = False
            txtcustcode.Visible = False
            txtcustname.Visible = True
            txtcontcode.Visible = False
            txtcontname.Visible = False
        ElseIf ddlsearchby.SelectedValue = "CONT_CODE" Then

            lblroutename.Visible = False
            lblcustcode.Visible = False
            lblcustname.Visible = False
            lblcontcode.Visible = True
            lblcontName.Visible = False

            ddlRoute.Visible = False
            txtcustcode.Visible = False
            txtcustname.Visible = False
            txtcontcode.Visible = True
            txtcontname.Visible = False
        ElseIf ddlsearchby.SelectedValue = "CONT_NAME" Then

            lblroutename.Visible = False
            lblcustcode.Visible = False
            lblcustname.Visible = False
            lblcontcode.Visible = False
            lblcontName.Visible = True

            ddlRoute.Visible = False
            txtcustcode.Visible = False
            txtcustname.Visible = False
            txtcontcode.Visible = False
            txtcontname.Visible = True
        Else

            lblroutename.Visible = False
            lblcustcode.Visible = False
            lblcustname.Visible = False
            lblcontcode.Visible = False
            lblcontName.Visible = False

            ddlRoute.Visible = False
            txtcustcode.Visible = False
            txtcustname.Visible = False
            txtcontcode.Visible = False
            txtcontname.Visible = False

            btnsearch.Visible = False
        End If

    End Sub

    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        RefreshDatabindingNewCust()
    End Sub

    Protected Sub btnrefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnrefresh.Click
        RefreshDatabindingSelectedCust()
    End Sub

#End Region

#Region "Function"
    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Private Sub AddNewCust()
        Try
            If dglistNewCust.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strContCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistNewCust.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistNewCust.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Session("SALESREP_CODE"))
                            strCustCode = dglistNewCust.DataKeys(i).Item("CUST_CODE")
                            strContCode = dglistNewCust.DataKeys(i).Item("CONT_CODE")
                            strRouteDate = txtpdate.Text
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlanCuz.InstPlanAddonCustCont(strSalesrepCode, strRouteDate, strCustCode, strContCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblNmsg.Text = "Record(s) added !"
            lblSmsg.Text = ""

            ResetNewCustQuery()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelNewCust()
        Try
            If dglistSelectedCust.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlanCuz As New txn_PrePlan.clsPrePlanCuz
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strContCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistSelectedCust.Rows
                    chkSelected = CType(DR.FindControl("chkDelete"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistSelectedCust.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Session("SALESREP_CODE"))
                            strRouteDate = dglistSelectedCust.DataKeys(i).Item("ROUTE_DATE")
                            strCustCode = dglistSelectedCust.DataKeys(i).Item("CUST_CODE")
                            strContCode = dglistSelectedCust.DataKeys(i).Item("CONT_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlanCuz.DelPlanAddonCustCont(strSalesrepCode, strRouteDate, strCustCode, strContCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblSmsg.Text = "Record(s) deleted !"
            lblNmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ResetNewCustQuery()
        txtcustcode.Text = ""
        txtcustname.Text = ""
        txtcontcode.Text = ""
        txtcontname.Text = ""
        'txtpdate.Text = ""
        'RefreshDatabindingNewCust()
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class
