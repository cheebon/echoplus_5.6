﻿Imports System.Data

Partial Class iFFMA_AR_CollectorCustMappingPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property CollectorSAPCode() As String
        Get
            Return Trim(hdCollectorSAPCode.Value)
        End Get
        Set(ByVal value As String)
            hdCollectorSAPCode.Value = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadCollectorSAPCodeDDL(ByVal DT As DataTable)
        Dim dtCollectorSAPCode As DataTable
        Dim clsCollectorCust As New mst_AR.clsCollectorCust

        Try
            Dim ddlCollectorSAPCode As DropDownList = CType(DetailsView1.FindControl("ddlCollectorSAPCode"), DropDownList)
            If ddlCollectorSAPCode IsNot Nothing Then
                dtCollectorSAPCode = clsCollectorCust.GetCollectorSAPCodeDDL
                With ddlCollectorSAPCode
                    .Items.Clear()
                    .DataSource = dtCollectorSAPCode.DefaultView
                    .DataTextField = "COLLECTOR_SAP_NAME"
                    .DataValueField = "COLLECTOR_SAP_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("COLLECTOR_SAP_CODE")), "", DT.Rows(0)("COLLECTOR_SAP_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Private Sub LoadCustDDL(ByVal DT As DataTable)
    '    Dim dtCust As DataTable
    '    Dim clsCommon As New mst_Common.clsDDL

    '    Try
    '        Dim ddlCust As DropDownList = CType(DetailsView1.FindControl("ddlCust"), DropDownList)
    '        If ddlCust IsNot Nothing Then
    '            dtCust = clsCommon.GetCustomerDDL
    '            With ddlCust
    '                .Items.Clear()
    '                .DataSource = dtCust.DefaultView
    '                .DataTextField = "CUST_NAME"
    '                .DataValueField = "CUST_CODE"
    '                .DataBind()
    '                .Items.Insert(0, New ListItem("-- SELECT --", ""))
    '                .SelectedIndex = 0
    '                .SelectedValue = IIf(IsDBNull(DT.Rows(0)("CUST_CODE")), "", DT.Rows(0)("CUST_CODE"))
    '            End With
    '        End If

    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsCollectorCust As New mst_AR.clsCollectorCust

            DT = clsCollectorCust.GetCollectorCustDetails(CollectorSAPCode, CustCode)
            'If DT.Rows.Count > 0 Then
            '    If DT.Rows(0)("LOCK_FLAG") <> "0" Then
            '        RaiseEvent PopLockAlert()
            '        Exit Sub
            '    End If
            'Else
            '    DT.Rows.Add(DT.NewRow())
            'End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            If DT IsNot Nothing Then
                LoadCollectorSAPCodeDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_CustSearch)
                .ResetPage()
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SelectButton_Click
        Try
            Dim txtCustCode As TextBox = CType(DetailsView1.FindControl("txtCustCode"), TextBox)
            txtCustCode.Text = wuc_CustSearch.CustCode

            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SearchButton_Click
        Try
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ddlCollectorSAPCode As DropDownList = CType(DetailsView1.FindControl("ddlCollectorSAPCode"), DropDownList)
            Dim txtCustCode As TextBox = CType(DetailsView1.FindControl("txtCustCode"), TextBox)

            Dim clsCollectorCust As New mst_AR.clsCollectorCust

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsCollectorCust.UpdateCollectorCust(CollectorSAPCode, CustCode)
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                DT = clsCollectorCust.CreateCollectorCust(ddlCollectorSAPCode.SelectedValue, txtCustCode.Text)

                'Dim isDuplicate As Integer
                'If DT.Rows.Count > 0 Then
                '    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                'End If

                'If isDuplicate = 1 Then
                '    lblInfo.Text = "Record already exists!"
                '    CompCode = ""
                '    LoadDvInsertMode()
                '    Show()
                '    Exit Sub
                'Else

                lblInfo.Text = "The record is successfully created."
                'BindDetailsView()
                CollectorSAPCode = ddlCollectorSAPCode.SelectedValue.ToString
                CustCode = txtCustCode.Text.ToString
                'BUCode = ddlBUCode.SelectedValue.ToString
                LoadDvViewMode()
            End If
            'End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnClose.Click
    '    Try
    '        RaiseEvent CloseButton_Click(sender, e)
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class