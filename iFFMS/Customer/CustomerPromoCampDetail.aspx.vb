Imports System.Data

Partial Class CustomerPromoCampDetail
    Inherits System.Web.UI.Page



    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerPromoCampDetail"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
        'Call Header
        With wuc_lblheader
            .Title = "Promo. Camp. Detail"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If

      
    End Sub
    Sub OnPageLoad()
        Dim strCampNo, strSalesmanCode, strCustCode As String
        Try
            strCampNo = Request.QueryString("campno")
            strSalesmanCode = Session("SALESREP_CODE")
            strCustCode = Session("customercode")
            bindGridView(strCustCode, strCampNo)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub
    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strCustCode As String, ByVal strCampNo As String)
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getPromoCampDT(strCustCode, "2", strCampNo)

            If dt.Rows.Count > 0 Then

                lblCustCode.Text = IIf(IsDBNull(dt.Rows(0)("cust_cd")), "Not Available", dt.Rows(0)("cust_cd"))
                lblCustCode.Text = IIf(String.IsNullOrEmpty(lblCustCode.Text), "Not Available", lblCustCode.Text)

                lblName.Text = IIf(IsDBNull(dt.Rows(0)("cust_name")), "Not Available", dt.Rows(0)("cust_name"))
                lblName.Text = IIf(String.IsNullOrEmpty(lblName.Text), "Not Available", lblName.Text)

                lblCampaignCode.Text = IIf(IsDBNull(dt.Rows(0)("campaign_cd")), "Not Available", dt.Rows(0)("campaign_cd"))
                lblCampaignCode.Text = IIf(String.IsNullOrEmpty(lblCampaignCode.Text), "Not Available", lblCampaignCode.Text)

                lblTitle.Text = IIf(IsDBNull(dt.Rows(0)("campaign_title")), "Not Available", dt.Rows(0)("campaign_title"))
                lblTitle.Text = IIf(String.IsNullOrEmpty(lblTitle.Text), "Not Available", lblTitle.Text)

                lblDetails.Text = IIf(IsDBNull(dt.Rows(0)("campaign_detail")), "Not Available", dt.Rows(0)("campaign_detail"))
                lblDetails.Text = IIf(String.IsNullOrEmpty(lblDetails.Text), "Not Available", lblDetails.Text)

                lblDept.Text = IIf(IsDBNull(dt.Rows(0)("depart_desc")), "Not Available", dt.Rows(0)("depart_desc"))
                lblDept.Text = IIf(String.IsNullOrEmpty(lblDept.Text), "Not Available", lblDept.Text)

                lblStartDate.Text = IIf(IsDBNull(dt.Rows(0)("start_date")), "Not Available", dt.Rows(0)("start_date"))
                lblStartDate.Text = IIf(String.IsNullOrEmpty(lblStartDate.Text), "Not Available", lblStartDate.Text)

                lblEndDate.Text = IIf(IsDBNull(dt.Rows(0)("end_date")), "Not Available", dt.Rows(0)("end_date"))
                lblEndDate.Text = IIf(String.IsNullOrEmpty(lblEndDate.Text), "Not Available", lblEndDate.Text)

                lblXField1.Text = IIf(IsDBNull(dt.Rows(0)("xfielda")), "Not Available", dt.Rows(0)("xfielda"))
                lblXField1.Text = IIf(String.IsNullOrEmpty(lblXField1.Text), "Not Available", lblXField1.Text)

                lblXField2.Text = IIf(IsDBNull(dt.Rows(0)("xfieldb")), "Not Available", dt.Rows(0)("xfieldb"))
                lblXField2.Text = IIf(String.IsNullOrEmpty(lblXField2.Text), "Not Available", lblXField2.Text)

                lblXField3.Text = IIf(IsDBNull(dt.Rows(0)("xfieldc")), "Not Available", dt.Rows(0)("xfieldc"))
                lblXField3.Text = IIf(String.IsNullOrEmpty(lblXField3.Text), "Not Available", lblXField3.Text)
            Else
                lblCustCode.Text = "Not Available"
                lblName.Text = "Not Available"
                lblCampaignCode.Text = "Not Available"
                lblTitle.Text = "Not Available"
                lblDetails.Text = "Not Available"
                lblDept.Text = "Not Available"
                lblStartDate.Text = "Not Available"
                lblEndDate.Text = "Not Available"
                lblXField1.Text = "Not Available"
                lblXField2.Text = "Not Available"
                lblXField3.Text = "Not Available"
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../Customer/CustomerPromoCamp.aspx", False)
    End Sub
End Class



