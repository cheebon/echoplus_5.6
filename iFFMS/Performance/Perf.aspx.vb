Imports System.Data
Partial Class iFFMS_Performance_Perf
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemPerf")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemPerf") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMS_Perf"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

#End Region

    Private intPageSize As Integer
    Public ReadOnly Property PageName() As String
        Get
            Return "Performance"
        End Get
    End Property
    Public ReadOnly Property ClassName() As String
        Get
            Return "Performance_Perf"
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

        'Call Header
        With wuc_lblheader
            .Title = "Performance"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            BindTgtDurationDLL()
            TimerControl1.Enabled = True
        End If

    End Sub
    Sub BindTgtDurationDLL()

        Dim obj As New txn_Performance.clsPerformance
        Dim dt As DataTable = Nothing

        Try
            dt = obj.GetTgtDurationDDl(Trim(Session("SALESREP_CODE")), Trim(Session("UserID")))
            If dt.Rows.Count > 0 Then
                ddltgtduration.DataSource = dt
                ddltgtduration.DataValueField = "TGT_DURATION"
                ddltgtduration.DataTextField = "TGT_DURATION"
                ddltgtduration.DataBind()
                Dim lstItem As ListItem = ddltgtduration.Items.FindByText("default")
                If lstItem IsNot Nothing Then
                    ddltgtduration.SelectedIndex = -1
                    lstItem.Selected = True
                Else
                    ddltgtduration.Items.Insert(0, New ListItem("default", "default"))
                    ddltgtduration.SelectedIndex = 0
                End If
            Else
                ddltgtduration.Items.Insert(0, "None")
            End If

            UpdatePage.Update()
        Catch ex As Exception

        End Try



        'Session("Prdgrp") = dt
    End Sub

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        dtCurrenttable = GetRecList()

        PreRenderMode(dtCurrenttable)

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgList.DataSource = dvCurrentView
        dgList.DataBind()

        UpdateDatagrid_Update()
        UpdatePage.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_Perf.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_Perf.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_Perf.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_Perf.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select

            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim clsPerformance As txn_Performance.clsPerformance
        Dim DT As DataTable = Nothing
        Try

            clsPerformance = New txn_Performance.clsPerformance
            Dim strSalesrepCode As String, strUserId As String, strTgtDuration As String

            strTgtDuration = Trim(ddltgtduration.SelectedValue.ToString)
            strSalesrepCode = Trim(Session("SALESREP_CODE"))
            strUserId = Trim(Session("UserID"))

            With clsPerformance
                DT = .GetPerfDtl(strSalesrepCode, strTgtDuration, strUserId)
            End With

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)
            Cal_ItemFigureCollector(DT)


        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper

            If strColumnName = "RET_QTY" Or strColumnName = "LIST_PRICE" Or strColumnName = "RET_AMT" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        'Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0
        'Try
        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
        Return dblValue
    End Function

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound

        Select Case e.Row.RowType
            Case DataControlRowType.DataRow

            Case DataControlRowType.Footer
                Dim iIndex As Integer

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_Perf.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                    End If
                Next


        End Select

    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()

            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        UpdatePage.Update()
    End Sub

#End Region

#Region "DETAILVIEW"

    Private Function GetRecList2() As Data.DataTable
        Dim clsPerformance As txn_Performance.clsPerformance
        Dim DT As DataTable = Nothing
        Try

            clsPerformance = New txn_Performance.clsPerformance
            Dim strSalesrepCode As String, strUserId As String, strTgtDuration As String

            strTgtDuration = Trim(ddltgtduration.SelectedValue.ToString)
            strSalesrepCode = Trim(Session("SALESREP_CODE"))
            strUserId = Trim(Session("UserID"))

            With clsPerformance
                DT = .GetPerfHdr(strSalesrepCode, strTgtDuration, strUserId)
            End With

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub RefreshDataBinding2()
        Dim dtCurrenttable2 As Data.DataTable = Nothing
        dtCurrenttable2 = GetRecList2()

        Dim dvCurrentView2 As New Data.DataView(dtCurrenttable2)
        If dtCurrenttable2 IsNot Nothing Then dv_Init(dtCurrenttable2)
        dtviewleft.DataSource = dvCurrentView2
        dtviewleft.DataBind()
        dtviewright.DataSource = dvCurrentView2
        dtviewright.DataBind()


    End Sub

    Protected Sub dv_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim j As Integer = 0
        Dim ColumnNamedv As String = ""

        Try
            dtviewleft.Fields.Clear()
            dtviewright.Fields.Clear()

            For i = 0 To dtToBind.Columns.Count - 1
                ColumnNamedv = dtToBind.Columns(i).ColumnName

                Select Case CF_Perf.GetFieldColumnType(ColumnNamedv, True)
                    Case FieldColumntype.InvisibleColumn

                    Case Else
                        j = j + 1
                        Dim dvcolumn As New BoundField

                        dvcolumn = New BoundField

                        Dim strFormatStringdv As String = CF_Perf.GetOutputFormatString(ColumnNamedv)
                        If String.IsNullOrEmpty(strFormatStringdv) = False Then
                            dvcolumn.DataFormatString = strFormatStringdv
                            dvcolumn.HtmlEncode = False
                        End If

                        dvcolumn.DataField = ColumnNamedv
                        dvcolumn.HeaderText = CF_Perf.GetDisplayColumnName(ColumnNamedv)
                        dvcolumn.ReadOnly = True
                        dvcolumn.SortExpression = ColumnNamedv

                        If j Mod 2 = 0 Then
                            dtviewright.Fields.Add(dvcolumn)
                        Else
                            dtviewleft.Fields.Add(dvcolumn)
                        End If

                        dvcolumn = Nothing
                End Select
            Next

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDatabinding()
            RefreshDataBinding2()
         

        End If
        TimerControl1.Enabled = False
        UpdatePage.Update()
    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddltgtduration_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltgtduration.SelectedIndexChanged
        TimerControl1.Enabled = True
    End Sub

#Region "Export"
    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
        ActivateExportBtnClick(Me, e)
        'Try
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(ClassName & ".btnExport_Click : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            ExportToFile(pnldglist, wuc_lblheader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#Region "Export"
    Public Enum ExportType
        EXCEL = 0
        TEXT = 1
    End Enum

    Public ReadOnly Property ExportFileType() As ExportType
        Get
            Return ddlExport.SelectedIndex
        End Get
    End Property

    Public ReadOnly Property ExportContentType() As String
        Get
            Dim iValue As Integer = ddlExport.SelectedIndex
            Dim strContentType As String = "application/vnd.xls"
            Select Case iValue
                Case 0
                    'Excel Format
                    strContentType = "application/vnd.xls"
                Case 1
                    'Text Format
                    strContentType = "application/vnd.text"
                Case Else
                    'strContentType = ""
            End Select
            Return strContentType
        End Get
    End Property

    Public ReadOnly Property ExportFileExtenstion() As String
        Get
            Dim iValue As Integer = ddlExport.SelectedIndex
            Dim strExt As String = ".xls"
            Select Case iValue
                Case 0
                    'Excel Format
                    strExt = ".xls"
                Case 1
                    'Text Format
                    strExt = ".txt"
                Case Else
                    'strExt = ""
            End Select
            Return strExt
        End Get
    End Property

    Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
        Dim ltrField As Literal = New Literal()
        Dim intIdx As Integer
        Dim ctrl As Control = Nothing
        Try
            For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
                ctrl = ctrlHTML.Controls(intIdx)
                If TypeOf ctrl Is LinkButton Then
                    'ltrField.Text = (CType(ctrl, LinkButton)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is HyperLink Then
                    ltrField.Text = (CType(ctrl, HyperLink)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is DropDownList Then
                    ltrField.Text = (CType(ctrl, DropDownList)).SelectedItem.Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is TextBox Then
                    ltrField.Text = (CType(ctrl, TextBox)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is CheckBox Then
                    'ltrField.Text = IIf(CType(ctrl, CheckBox).Checked, "True", "False")
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is ImageButton OrElse TypeOf ctrl Is Image OrElse TypeOf ctrl Is Button Then
                    ctrlHTML.Controls.Remove(ctrl)
                End If
                If ctrl IsNot Nothing AndAlso ctrl.HasControls Then PrepareControlForExport(ctrl)
            Next
        Catch ex As Exception
            ExceptionMsg(ClassName & ".PrepareGridViewForExport : " & ex.ToString)
        End Try

    End Sub


    'Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
    '    Dim ltrField As Literal = New Literal()
    '    Dim intIdx As Integer

    '    Try
    '        For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
    '            If ctrlHTML.Controls(intIdx).GetType().Equals((New LinkButton).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), LinkButton)).Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New HyperLink).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), HyperLink)).Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New DropDownList).GetType()) Then
    '                ltrField.Text = (CType(ctrlHTML.Controls(intIdx), DropDownList)).SelectedItem.Text
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New CheckBox).GetType()) Then
    '                ltrField.Text = IIf(CType(ctrlHTML.Controls(intIdx), CheckBox).Checked, "True", "False")
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '                ctrlHTML.Controls.AddAt(intIdx, ltrField)
    '            ElseIf ctrlHTML.Controls(intIdx).GetType().Equals((New ImageButton).GetType()) OrElse _
    '            ctrlHTML.Controls(intIdx).GetType().Equals((New Image).GetType()) Then
    '                ctrlHTML.Controls.Remove(ctrlHTML.Controls(intIdx))
    '            End If
    '            If ctrlHTML.Controls(intIdx).HasControls Then PrepareControlForExport(ctrlHTML.Controls(intIdx))
    '        Next
    '    Catch ex As Exception
    '        ExceptionMsg(ClassName & ".PrepareGridViewForExport : " & ex.ToString)
    '    End Try

    'End Sub

    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "")
        Try
            customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            Dim strFileName As String = customFileName & ExportFileExtenstion
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            Response.Charset = ""
            Response.ContentType = ExportContentType '"application/vnd.xls"
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

            'create a emptry row table, act as empty row for insert
            Dim newRow As New HtmlTable()
            newRow.Rows.Add(New HtmlTableRow())

            'Dim pnlInfo As Control
            'pnlInfo = pnlGeneralInfo
            'PrepareControlForExport(pnlInfo)
            'pnlInfo.RenderControl(htmlWrite)

            newRow.RenderControl(htmlWrite) 'insert a empty row

            PrepareControlForExport(dgList)
            dgList.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString)
            Response.End()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "")
        Try
            customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            Dim strFileName As String = customFileName & ExportFileExtenstion
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            Response.Charset = ""
            Response.ContentType = ExportContentType '"application/vnd.xls"
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)


            'create a emptry row table, act as empty row for insert
            Dim newRow As New HtmlTable()
            newRow.Rows.Add(New HtmlTableRow())

            'newRow.RenderControl(htmlWrite) 'insert a empty row
            'Dim pnlInfo As Control
            'pnlInfo = pnlGeneralInfo
            'PrepareControlForExport(pnlInfo)
            'pnlInfo.RenderControl(htmlWrite)

            For Each dgList As Control In aryDgList
                newRow.RenderControl(htmlWrite) 'insert a empty row
                PrepareControlForExport(dgList)
                dgList.RenderControl(htmlWrite)
            Next
            Response.Write(stringWrite.ToString)
            Response.End()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#End Region
End Class


Public Class CF_Perf
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""



        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn No."
            Case "TXN_STATUS"
                strFieldName = "Status"
            Case "LINE_NO"
                strFieldName = "Line No."
            Case "UOM_CODE"
                strFieldName = "UOM Code"
            Case "COLL_CODE"
                strFieldName = "Collector Code"
            Case "BATCH_NO"
                strFieldName = "Batch No."
            Case "REASON_CODE"
                strFieldName = "Reason Code"
            Case "RET_QTY"
                strFieldName = "Trade Rtn."
            Case "COLL_TGT"
                strFieldName = "Coll. Tgt"
            Case "COLL_ACH"
                strFieldName = "Coll. Achieved"
            Case "MTD_ACH"
                strFieldName = "MTD Sales Achieved"
            Case "MTD_TGT"
                strFieldName = "MTD Sales Tgt."
            Case "WGHT"
                strFieldName = "Weight"
            Case "QTY"
                strFieldName = "Qty."
            Case "TGT_ACH"
                strFieldName = "Tgt Achieved"
            Case "TGT_ASSIGN"
                strFieldName = "Tgt. Assign"
            Case "PERIOD"
                strFieldName = "Period"
            Case "TGT_DURATION"
                strFieldName = "Tgt. Duration"
            Case "QTD_TGT"
                strFieldName = "QTD Sales Tgt."
            Case "QTD_ACH"
                strFieldName = "QTD Sales Achieved"
            Case "YTD_TGT"
                strFieldName = "YTD Sales Tgt."
            Case "YTD_ACH"
                strFieldName = "YTD Sales Achieved"
            Case "YTD_QTY"
                strFieldName = "YTD Sales Qty."
            Case "&NBSP;"
                strFieldName = " "
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "" Then

                FCT = FieldColumntype.HyperlinkColumn

            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "*QTY"
                    strFormatString = "{0:#,0}"
                Case "*ACH"
                    strFormatString = "{0:#,0}"
                Case "RET_AMT"
                    strFormatString = "{0:#,0.00}"
                Case "LIST_PRICE"
                    strFormatString = "{0:#,0.00}"
                Case "RET_QTY"
                    strFormatString = "{0:#,0.00}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "DESC_CODE" Then
                    .HorizontalAlign = HorizontalAlign.Left
                ElseIf strColumnName Like "*TGT" Or strColumnName Like "*ACH" Or strColumnName Like "*AMT" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class