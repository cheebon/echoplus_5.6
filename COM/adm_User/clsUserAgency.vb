﻿'************************************************************************
'	Author	    :	Lai Eu Jin
'	Date	    :	22/07/2011
'	Purpose	    :	Class to query all the data related to master user
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsUserAgency
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public clsProperties As New adm_User.clsProperties.clsUserAgency

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsUserAgency"
        End Get
    End Property

    Public Function GetUserAgencyOptionIndList() As DataTable
        Dim clsDB As clsDB
        clsDB = New clsDB
        Try
            With clsDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_TAGGING_OPTION_IND"
                .addItem("PRINCIPAL_ID", clsProperties.strPrincipalId, 1)
                .addItem("USER_ID", clsProperties.strUserId, 1)
                .addItem("SESSION_USER_ID", clsProperties.strSessionUserId, 1)
                .addItem("OPTION", clsProperties.strOption, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetUserAgencyOptionIndList :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function


    Public Function GetUserAgencyList() As DataTable
        Dim clsDB As clsDB
        clsDB = New clsDB
        Try
            With clsDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_AGENCY_LIST"
                .addItem("PRINCIPAL_ID", clsProperties.strPrincipalId, 1)
                .addItem("USER_ID", clsProperties.strUserId, 1)
                .addItem("SESSION_USER_ID", clsProperties.strSessionUserId, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetUserAgencyList :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function

    Public Function GetUserAgencyListAll() As DataTable
        Dim clsDB As clsDB
        clsDB = New clsDB
        Try
            With clsDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_AGENCY_LIST_ALL"
                .addItem("PRINCIPAL_ID", clsProperties.strPrincipalId, 1)
                .addItem("USER_ID", clsProperties.strUserId, 1)
                .addItem("SESSION_USER_ID", clsProperties.strSessionUserId, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetUserAgencyListAll :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function

   
    Public Function UpdateUserAgencyList() As DataTable
        Dim clsDB As clsDB
        clsDB = New clsDB
        Try
            With clsDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_USER_AGENCY_UPDATE"
                .addItem("PRINCIPAL_ID", clsProperties.strPrincipalId, 1)
                .addItem("USER_ID", clsProperties.strUserId, 1)
                .addItem("SESSION_USER_ID", clsProperties.strSessionUserId, 1)
                .addItem("AGENCY_LIST", clsProperties.strAgencyList, 1)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".UpdateUserAgencyList :" & ex.Message))
        Finally
            clsDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserSalesRepID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
