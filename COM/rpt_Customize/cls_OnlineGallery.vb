﻿Option Explicit On
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data
''' <summary>
''' '''  ************************************************************************
'''	Author	    :	Eujin
'''	Date	    :	2014/4/15
'''	Purpose	    :	 
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************ 
'''  
''' </summary>
''' <remarks></remarks>
Public Class cls_OnlineGallery
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties



    Public Function GetOnlineGalleryList(ByVal strSortCol As String, ByVal strSortDir As String, ByVal intPageIndex As Integer, ByVal intPageSize As Integer, _
        ByVal intIsExport As Integer, ByVal strUserId As String, ByVal strProduct As String, ByVal strCustomer As String, ByVal strActivityCode As String,
        ByVal strSalesrepCode As String, ByVal strStartDate As String, ByVal strEndDate As String,
        intFilterType As Integer, Optional strCode01 As String = "", Optional strCode02 As String = "", Optional strCode03 As String = "") As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_LIST"
                .addItem("product", strProduct, cor_DB.clsDB.DataType.DBString)
                .addItem("customer", strCustomer, cor_DB.clsDB.DataType.DBString)
                .addItem("activity_code", strActivityCode, cor_DB.clsDB.DataType.DBString)
                .addItem("salesrep_code", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("start_date", strStartDate, cor_DB.clsDB.DataType.DBString)
                .addItem("end_date", strEndDate, cor_DB.clsDB.DataType.DBString)
                .addItem("sortcol", strSortCol, cor_DB.clsDB.DataType.DBString)
                .addItem("sortdir", strSortDir, cor_DB.clsDB.DataType.DBString)
                .addItem("pageindex", intPageIndex, cor_DB.clsDB.DataType.DBInt)
                .addItem("pagesize", intPageSize, cor_DB.clsDB.DataType.DBInt)
                .addItem("isexport", intIsExport, cor_DB.clsDB.DataType.DBInt)
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                .addItem("filter_type", intFilterType, cor_DB.clsDB.DataType.DBInt)
                If intFilterType = 0 Then
                    .addItem("ques_code", strCode01, cor_DB.clsDB.DataType.DBString)
                    .addItem("sub_ques_code", strCode02, cor_DB.clsDB.DataType.DBString)
                ElseIf intFilterType = 1 Then
                    .addItem("cat_code", strCode01, cor_DB.clsDB.DataType.DBString)
                    .addItem("sub_cat_code", strCode02, cor_DB.clsDB.DataType.DBString)
                ElseIf intFilterType = 2 Then
                    .addItem("po_no", strCode01, cor_DB.clsDB.DataType.DBString)
                    .addItem("so_no", strCode02, cor_DB.clsDB.DataType.DBString)
                    .addItem("channel_code", strCode03, cor_DB.clsDB.DataType.DBString)
                ElseIf intFilterType = 4 Then
                    .addItem("po_no", strCode01, cor_DB.clsDB.DataType.DBString)
                    .addItem("so_no", strCode02, cor_DB.clsDB.DataType.DBString)
                End If
                dt = .spRetrieve
            End With

            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function


    Public Function GetOnlineGallerySalesrepList(ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_SALESREP"
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With

            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGalleryActivityList(ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_ACTIVITY"
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With

            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGallerySFMSCategoryList(ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_SFMS_CAT_LIST"
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With

            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGallerySFMSSubCategoryList(strCatCode As String, ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_SFMS_SUBCAT_LIST"
                .addItem("cat_code", strCatCode, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With

            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGalleryMSSQuesList(ByVal strMSSTitleCode As String, ByVal strUserId As String)
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_MSS_QUES_LIST"
                .addItem("title_code", strMSSTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGalleryMSSSubQuesList(ByVal strMSSQuesCode As String, ByVal strUserId As String)
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_MSS_SUB_QUES_LIST"
                .addItem("ques_code", strMSSQuesCode, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGalleryChannelList(ByVal strUserId As String)
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_CHANNEL_LIST"
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGalleryCollectorCustList(ByVal strUserId As String, ByVal strCollectorCode As String, ByVal strSearch As String)
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_COLLECTOR_CUST_LIST"
                .addItem("Search", strSearch, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                .addItem("collector_code", strCollectorCode, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetOnlineGalleryCollectorList(ByVal strSearch As String, ByVal strUserId As String)
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_ONLINE_GALLERY_COLLECTOR_LIST"
                .addItem("Search", strSearch, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", strUserId, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
