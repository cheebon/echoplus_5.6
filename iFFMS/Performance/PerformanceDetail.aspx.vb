Imports System.Data

Partial Class ReportDetail
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "ReportDetail"
        End Get
    End Property
    Dim strSalesmanCode, strDuration, strProdCode As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Try
          
            'Call Header
            With wuc_lblheader
                .Title = "Report Detail"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.PERFORMANCE
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                strSalesmanCode = Trim(Session("dflSalesRepCode"))
                strDuration = Request.QueryString("duration")
                strProdCode = Request.QueryString("prodcode")
                bindData()
                lblSalesRepName.Text = Trim(Session("dflsalesrepname"))
                lblSalesRepCode.Text = Trim(Session("dflsalesrepcode"))
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub bindData()
        Dim dt As DataTable = Nothing
        Dim obj As New txn_Performance.clsPerformance
        Try
            dt = obj.getDetailDT(strSalesmanCode, strDuration, strProdCode)
            If dt.Rows.Count > 0 Then
                lblColTgt.Text = dt.Rows(0)("col_tgt")
                lblMTDTgt.Text = dt.Rows(0)("ctd_col")
                If Double.Parse(dt.Rows(0)("col_tgt")) > 0 Then
                    lblRatio.Text = Double.Parse(dt.Rows(0)("ctd_col")) * 100 / Double.Parse(dt.Rows(0)("col_tgt")) & "%"
                Else
                    lblRatio.Text = 0
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".bindData : " & ex.ToString)
        Finally
        End Try
    End Sub
  
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



