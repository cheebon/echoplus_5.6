<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SalesteamAssignPop.ascx.vb" Inherits="iFFMA_Salesteam_SalesteamAssignPop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register Assembly="cor_CustomCtrl" Namespace="cor_CustomCtrl" TagPrefix="ccGV" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel DefaultButton="btnSearch" ID="pnlMsgPop" runat="server" Style="display: none; width: 600px; padding: 15px" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:95%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:left; width:5%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                <fieldset style="padding-left: 10px; width: 100%; padding-bottom: 5px;">
                  <asp:Panel ID="pnlSupplierCode" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Supplier Code</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                        <asp:TextBox ID="txtSupplierCode" runat="server" CssClass="cls_textbox" />
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="pnlSupplierName" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Supplier Name</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                        <asp:TextBox ID="txtSupplierName" runat="server" CssClass="cls_textbox" />
                    </asp:Panel>
                     <asp:Panel ID="pnlddlSupplierCode" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Supplier</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                         <asp:DropDownList ID="ddlSupplierCode" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
                    </asp:Panel>
                    <asp:Panel ID="pnlPrdGrpCode" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Product Group Code</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                        <asp:TextBox ID="txtPrdGrpCode" runat="server" CssClass="cls_textbox" />
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="pnlPrdGrpName" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Product Group Name</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                        <asp:TextBox ID="txtPrdGrpName" runat="server" CssClass="cls_textbox" />
                    </asp:Panel>
                    <asp:Panel ID="pnlPrdCode" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Product Code</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                        <asp:TextBox ID="txtPrdCode" runat="server" CssClass="cls_textbox" />
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="pnlPrdName" runat="server">
                        <span style="float:left; width:30%; padding-top: 2px;" class="cls_label_header">Product Name</span>
                        <span style="float:left; width:2%; padding-top: 2px;" class="cls_label_header">:</span>
                        <asp:TextBox ID="txtPrdName" runat="server" CssClass="cls_textbox" />
                    </asp:Panel>
                   
                    
                    <span style="float:left; padding-top: 2px; padding-bottom: 2px;"><asp:Button ID="btnSearch" runat="server" CssClass="cls_button" Text="Search" /></span> 
                </fieldset>
                
                <fieldset style="padding-left: 10px; width: 100%;">
                    <span ID="pnlRegion" runat="server"> 
                        <span style="  width:30%; padding-top: 2px;" class="cls_label_header">Region</span>
                        <span style=" width:2%; padding-top: 2px;" class="cls_label_header">:</span> 
                        <asp:DropDownList ID="ddlRegion" runat="server" CssClass="cls_dropdownlist"  >
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvRegion" runat="server" ErrorMessage="Please select a region!" ValidationGroup="Select" 
                            ControlToValidate="ddlRegion" Display="Dynamic" CssClass="cls_validator" >
                        </asp:RequiredFieldValidator>
                    </span>
                    <asp:Button ID="btnSelect" runat="server" CssClass="cls_button" Text="Select" ValidationGroup="Select"  /> 
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                    <customToolkit:wuc_dgpaging ID="wuc_dgAssignPopPaging" runat="server" />
                    <ccGV:clsGridView ID="dgAssignPopList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        Width="98%" FreezeHeader="True" GridHeight="300px" AddEmptyHeaders="0" CellPadding="2"
                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                        ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                        <EmptyDataTemplate>
                            <customToolkit:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <itemtemplate>
                                    <asp:CheckBox id="chkSelect" runat="server" CssClass="cls_checkbox" /> 
                                </itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                    </ccGV:clsGridView>
                                     
                    <asp:HiddenField ID="hdAssignType" runat="server" Value="0" />
                    <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                </fieldset>
            </div>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            PopupControlID="pnlMsgPop" 
            CancelControlID="imgClose"
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>