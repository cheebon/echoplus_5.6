'clsSalesOrder
'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	22/12/2005
'	Purpose	    :	Class to build Sales Order Report from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSalesOrder
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSalesOrder"
        End Get
    End Property

#Region "Sales Order"
    Public Function GET_ORD_HDR_TXNNO(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisitID As String, ByVal strCustCode As String, ByVal strTxnDate As String) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ORD_HDR_TXN_NO"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("TXN_DATE", strTxnDate, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CUST_CODE", strCustCode, 2)
                .addItem("VISIT_ID", strVisitID, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".Get_ORD_HDR_TXNNO :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    Public Function GET_ORD_HDR(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strPrincipalCode As String, _
    ByVal strSalesrepCode As String, ByVal strVisit As String, ByVal strCustCode As String, ByVal strTxnDate As String, ByVal strTXNNO As String) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ORD_HDR"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, clsDB.DataType.DBString)
                .addItem("TXN_NO", strTXNNO, 2)
                .addItem("TXN_DATE", strTxnDate, 2)
                .addItem("SALESREP_CODE", strSalesrepCode, 2)
                .addItem("CUST_CODE", strCustCode, 2)
                .addItem("VISIT_ID", strVisit, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".Get_ORD_HDR :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

    'EXEC SPP_RPT_ORD_DET @TXN_NO
    Public Function GET_ORD_DET(ByVal strTXNNO As String) As DataTable
        Dim clsSODB As clsDB
        Try
            clsSODB = New clsDB
            With clsSODB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_ORD_DET"
                .addItem("TXN_NO", strTXNNO, 2)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".Get_ORD_DET :" & ex.Message))
        Finally
            clsSODB = Nothing
        End Try
    End Function

#End Region

#Region "PreBuild"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class



