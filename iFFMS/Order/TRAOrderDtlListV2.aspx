<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TRAOrderDtlListV2.aspx.vb"
    Inherits="iFFMS_Order_TRAOrderDtlList" EnableEventValidation="false" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="../../styles/jquery-ui-1.8.23.custom.css" rel="stylesheet" />
    <%--<script src="../../scripts/jquery-1.10.2.min.js" type="text/javascript"></script>--%>
    <link href="../../styles/bootstrap.css" rel="stylesheet" />
    <script type="text/javascript" src="../../scripts/jquery-2.1.4.min.js"></script>
    <script src="../../scripts/Popup.js" type="text/javascript"></script>
    <script src="../../scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../scripts/bootbox.min.js" type="text/javascript"> </script>
    <link href="../../scripts/jQuery-File-Upload/uploadfile.css" rel="stylesheet" />
    <script src="../../scripts/jQuery-File-Upload/jquery.uploadfile.min.js" type="text/javascript"></script>
    <%--<script src="../../scripts/attachment.js" type="text/javascript"> </script>--%>

    <script src="../../scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function CollapseParent() {
            if (self.parent.CollapseMain) {
                self.parent.CollapseMain();
            }
        }
        function refreshParent() {
            if (parent) {
                parent.frames['SideBar'].location.href = parent.frames['SideBar'].location.href;
                if (self.parent.CollapseMain) { self.parent.CollapseExpand(); }
            }
        }

        function printdiv(printpage) {
            var lblviewdatetime = document.getElementById('lblviewdatetime');
            lblviewdatetime.innerHTML = Date();
            window.print();
        }

        function HideNavigation() {
            var height = screen.height;
            if (height < 700) {
                self.parent.parent.document.getElementById('fraDefault').rows = "0,*";
            }
            else { self.parent.parent.parent.document.getElementById('fraDefault').rows = "0,*"; }
        }

        function ShowNavigation() {
            var height = screen.height;
            if (height < 700) {
                var fraDefault = self.parent.parent.parent.document.getElementById('fraDefault').rows = "65,*";
            }
            else { self.parent.parent.parent.document.getElementById('fraDefault').rows = "132,*"; }
        }

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        var txn_no = getParameterByName('TXN_NO');

        //Attachment
        function LoadAttachment() {
            LoadUploaded();
            bootbox.dialog({
                title: 'Attachment',
                message: $('#AttachmentForm1'),
                show: false
            })
        .on('shown.bs.modal', function () {
            $('#AttachmentForm1').show()
        })
        .on('hide.bs.modal', function (e) {
            $('#AttachmentForm1').hide().appendTo('body');
        })
        .modal('show');

        }

        function LoadUploaded() {
            $.get('../../DataServices/attachment.ashx?strTxnNo=' + txn_no + '&Mode=GetAttachment', function (data) {
                var objs = JSON.parse(data).files;
                if (objs == null) { } else {
                    $("#lstAttachmentDtl tr").empty();
                    if (objs.length > 0) {
                        var tr = '';
                        $.each(objs, function (i, value) {
                            tr += '<tr style="color:black">';
                            tr += '<td width="5%" ><span>' + parseFloat(i + 1) + '.</span></td>';
                            tr += '<td><a style="color:black" href="#" onclick="doViewFile(\'' + value + '\');return false;">' + value.split('_')[1] + '</a></td>';
                            tr += '</tr>';
                        })
                        $('#lstAttachmentDtl').append(tr);
                    }
                }
            })
        }

        function doViewFile(file) {
            var encodedInputString = encodeURIComponent(file);
            window.location.href = "ExportImg.aspx?strFileName=" + encodedInputString;
        }
    </script>
</head>

<body class="BckgroundInsideContentLayout" onload="CollapseParent()">
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td style="width: 100%">
                                                <uc1:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uc1:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="button" runat="server" id="btnback" value="Back" onclick="self.parent.CollapseExpand()"
                                                    class="cls_button" style="width: 80px" />
                                                <asp:Button ID="btncancel" runat="server" Text="Cancel Txn" CssClass="cls_button" Width="80px" Visible="false"
                                                    OnClientClick="var agree=confirm('System will cancel the current transaction! After cancel the transaction will not be editable. Are you sure you wish to continue?');if(agree)return true;else return false;" />
                                                <asp:Button ID="btnedit" runat="server" Text="Edit/Submit Txn" CssClass="cls_button" Width="80px" />
                                                <%--  <input type="button"  runat="server"  id="Button1" value="Goto TRA List" onclick="refreshParent()" class="cls_button"  />--%>
                                                <input name="b_print" type="button" class="cls_button" onclick="printdiv('div_print');" value=" Print" style="width: 80px;" />
                                                <asp:Button ID="btnAttachment" runat="server" CssClass="cls_button" Text="View File" TabIndex="11" Visible="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <div id="div_print" style="margin: 0px 0px 0px 0px;">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="650px">
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                        <div>
                                                                            <span class="cls_label_header">TRADE RETURN ADVICE (WEB TRA)</span>
                                                                            <asp:Label ID="lblviewdatetime" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                                            <asp:Label ID="lblviewusername" runat="server" Text="" CssClass="cls_label"></asp:Label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                            Width="325px" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                            <Fields>
                                                                                <asp:BoundField DataField="TXN_NO" HeaderText="Txn No." ReadOnly="True" SortExpression="TXN_NO" HtmlEncode="false">
                                                                                    <ItemStyle CssClass="hiddencol data1" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SALESREP" HeaderText="Salesman" ReadOnly="True" SortExpression="SALESREP" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="SALES_TEAM" HeaderText="Sales Team" ReadOnly="True" SortExpression="SALES_TEAM" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="SHIPTO" HeaderText="Ship To" ReadOnly="True" SortExpression="SHIPTO" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="PAYER" HeaderText="Payer" ReadOnly="True" SortExpression="PAYER" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="REFER_NO" HeaderText="Refer No." ReadOnly="True" SortExpression="REFER_NO" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="VOUCHER_NO" HeaderText="Voucher No." ReadOnly="True" SortExpression="VOUCHER_NO" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="CREATED_BY" HeaderText="Created By" ReadOnly="True" SortExpression="CREATED_BY" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="CREATED_DATE" HeaderText="Created Date" ReadOnly="True" SortExpression="CREATED_DATE" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="REMARKS" HeaderText="Remarks" ReadOnly="True" SortExpression="REMARKS" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="SAP_BILLING" HeaderText="SAP Billing" ReadOnly="True" SortExpression="SAP_BILLING" HtmlEncode="false" />
                                                                            </Fields>
                                                                            <RowStyle CssClass="cls_DV_Row_MST" />
                                                                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                Width="35%" Wrap="False" />
                                                                        </asp:DetailsView>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                            Width="325px" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                            <Fields>
                                                                                <asp:BoundField DataField="STATUS" HeaderText="Status" ReadOnly="True" SortExpression="STATUS" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="WF_REMARKS" HeaderText ="WF Remarks" ReadOnly="true" SortExpression="WF_REMARKS" HtmlEncode="false" Visible="false"/>
                                                                                <asp:BoundField DataField="SALES_OFFICE" HeaderText="Salesrep Sales Office" ReadOnly="True" SortExpression="SALESREP_SALES_OFFICE" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="SHIPTO_SALES_OFFICE" HeaderText="Shipto Sales Office" ReadOnly="True" SortExpression="SHIPTO_SALES_OFFICE" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="STORAGE_LOCATION" HeaderText="Storage Location" ReadOnly="True" SortExpression="STORAGE_LOCATION" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="TTL_NO_CARTON" HeaderText="Total No. Of Carton" ReadOnly="True" SortExpression="TTL_NO_CARTON" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="HEADER_REASON" HeaderText="Header Reason" ReadOnly="True" SortExpression="HEADER_REASON" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="COLLECTED_BY" HeaderText="Collected By" ReadOnly="True" SortExpression="COLLECTED_BY" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="VENDOR_NO" HeaderText="Vendor No" ReadOnly="True" SortExpression="VENDOR_NO" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="CONTRACT_NO" HeaderText="Contract No" ReadOnly="True" SortExpression="CONTRACT_NO" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="DEPARTMENT_CODE" HeaderText="Department Code" ReadOnly="True" SortExpression="DEPARTMENT_CODE" HtmlEncode="false" />
                                                                                <asp:BoundField DataField="DISPOSAL_DATE" HeaderText="Disposal Date" ReadOnly="True" SortExpression="DISPOSAL_DATE" HtmlEncode="false" DataFormatString="{0:yyyy-MM-dd}" />

                                                                                <asp:BoundField DataField="GRAND_TOTAL" HeaderText="Grand Total" ReadOnly="True" SortExpression="GRAND_TOTAL" HtmlEncode="false" DataFormatString="{0:#,0.00}" />
                                                                            </Fields>
                                                                            <RowStyle CssClass="cls_DV_Row_MST" />
                                                                            <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                            <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                Width="35%" Wrap="False" />
                                                                        </asp:DetailsView>
                                                                    </td>
                                                                    <%--        <td style="width: 95%;">
                                                                   <div style="width: 650px; ">
                                                                        <span style="float: left; width: 325px;">
                                                                            <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                    Width="35%" Wrap="False" />
                                                                            </asp:DetailsView>
                                                                        </span><span style="float: left; width: 325px;">
                                                                            <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                    Width="35%" Wrap="False" />
                                                                            </asp:DetailsView>
                                                                        </span>
                                                                    </div>
                                                                </td>--%>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 95%;" colspan="2">
                                                                        <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                            AllowSorting="true" AutoGenerateColumns="false" Width="650px" FreezeHeader="false"
                                                                            GridHeight="455" RowSelectionEnabled="true">
                                                                        </ccGV:clsGridView>
                                                                        <%--<ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" AddEmptyHeaders="0" CellPadding="2" GridHeight="440"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                                                                        <EmptyDataTemplate>
                                                                            <uc1:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                        </EmptyDataTemplate>
                                                                    </ccGV:clsGridView>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td style="height: 5px" colspan="2">
                                                <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>

        <table id="AttachmentForm1" cellpadding="0" cellspacing="0" style="height: 20px; width: 99.5%; padding-left: 5px; border: solid 2px black; cursor: pointer; display:none;" class="GridHeader" >
            <tr>
                <th>
                    <asp:Label ID="lblUploadedFile" runat="server" Text="Uploaded Files" />
                </th>
            </tr>
            <tr>
                <td>
                    <div>
                        <table id="lstAttachmentDtl" width="99%;" style="background-color: white;">
                            <tr></tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </form>

    
</body>
</html>
