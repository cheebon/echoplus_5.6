'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	30/10/2006
'	Purpose	    :	Class to build datatables for getting SFMS transaction details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsSFMS
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFMS"
        End Get
    End Property

    Public ReadOnly Property Color(ByVal intCode As Integer) As Drawing.Color
        Get
            If intCode = 1 Then
                Return Drawing.Color.LightGray
            ElseIf intCode = 2 Then
                Return Drawing.Color.White
            End If
        End Get
       
    End Property

    Function getSFMSCategoryListing(ByVal strSalesmanCode As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Dim dt As Data.DataTable = Nothing

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSCATEGORYLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                dt = .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSFMSCategoryListing :" & ex.Message))
        End Try
        Return dt
    End Function

    Function getActivityDT(ByVal strSalesmanCode As String, ByVal strCatCode As String) As DataTable
        Dim dt As Data.DataTable = Nothing
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSACTIVITYLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("catcode", strCatCode, 2)
                dt = .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getActivityDT :" & ex.Message))
        End Try
        Return dt
    End Function

    Sub insertSFMSHeader(ByVal strSfmsNo As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitNo As String, ByVal strTime As String, ByVal strRemark As String)
        Dim objdb As New cor_DB.clsDB
        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSINSERTHDR"
                .addItem("sfmsno", strSfmsNo, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)
                .addItem("branchcode", strBranchCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("visitid", strVisitNo, 2)
                ' .addItem("txndate", DateTime.Now, 3)
                .addItem("txntime", strTime, 2)
                .addItem("remarks", strRemark, 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertSFMSHeader :" & ex.Message))
        End Try
    End Sub

    Function getSFMSDetailDT(ByVal strSalesmanCode As String, ByVal strDesc As String) As DataTable
        Dim objdb As New cor_DB.clsDB
        Dim dt As Data.DataTable = Nothing

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_GETSFMSDET"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("sfmsdesc", strDesc, 2)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSFMSDetailDT :" & ex.Message))
        End Try
    End Function

    Sub insertSFMSDetail(ByVal strSFMSNo As String, ByVal strCatCode As String, ByVal strSubcatcode As String, ByVal strSfmsDesc As String, ByVal strRmk As String, ByVal strQty As String)
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSINSERTDET"
                .addItem("sfmsno", strSFMSNo, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubcatcode, 2)
                .addItem("sfmsdesc", strSfmsDesc, 2)
                .addItem("remark", strRmk, 2)
                .addItem("qty", CStr(Integer.Parse(strQty)), 1)
                '.addItem("txntime", DateTime.Now, 3)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertSFMSDetail :" & ex.Message))
        End Try
    End Sub

    Function getSFMSEditDetailsDT(ByVal strSalesmanCode As String, ByVal strSFMSNo As String) As DataTable
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSEDIT"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("sfmsno", strSFMSNo, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSFMSEditDetailsDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Function getSubCatCode(ByVal strSalesmanCode As String, ByVal strDesc As String) As String
        Dim obj As New cor_DB.clsDB
        Dim dt As DataTable = Nothing
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSGETSUBCATCODE"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("sfmsdesc", strDesc, 2)
                dt = .spRetrieve

                If dt.Rows.Count > 0 Then
                    Return dt.Rows(0)("sub_cat_code").ToString
                Else
                    Return ""
                End If
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getSubCatCode :" & ex.Message))
        Finally
            obj = Nothing
        End Try

    End Function

    Sub updateSFMS(ByVal strTxnno As String, ByVal strCatCode As String, ByVal strSubCatCode As String, ByVal intQty As Integer, ByVal strRemark As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSUPDATE"
                .addItem("txnno", strTxnno, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .addItem("qty", intQty, 1)
                .addItem("remark", strRemark, 2)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateSFMS :" & ex.Message))
        End Try

    End Sub

    Sub deleteSFMS(ByVal strTxnno As String, ByVal strCatCode As String, ByVal strSubCatCode As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSDELETE"
                .addItem("txnno", strTxnno, 2)
                .addItem("catcode", strCatCode, 2)
                .addItem("subcatcode", strSubCatCode, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteSFMS :" & ex.Message))
        End Try

    End Sub

    Sub deleteEdit(ByVal strTxnno As String, ByVal strInd As String)

        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSEDITDELETE"
                .addItem("sfmsno", strTxnno, 2)
                .addItem("type", strInd, 2)

                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteEdit :" & ex.Message))
        End Try
    End Sub

    Sub updateHeader(ByVal strSFMSNo As String, ByVal strRemark As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_SFMSUPDATEHDR"
                .addItem("sfmsno", strSFMSNo, 2)
                .addItem("remark", strRemark, 2)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".updateHeader :" & ex.Message))
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
