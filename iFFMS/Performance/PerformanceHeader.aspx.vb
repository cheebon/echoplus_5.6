Imports System.Data

Partial Class ReportDetail
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Public ReadOnly Property PageName() As String
        Get
            Return "PerformanceHeader"
        End Get
    End Property
    Dim strSalesmanCode, strDuration, strName As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Session("dflsalesrepname") = "Wee Swee Wah"

        'Put user code to initialize the page here
        Try
            
            'Call Header
            With wuc_lblheader
                .Title = "Performance Header"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With Wuc_ctrlpanel
                .SubModuleID = SubModuleType.PERFORMANCE
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                strName = Trim(Session("dflsalesrepname"))
                strSalesmanCode = Trim(Session("dflSalesRepCode"))
                strDuration = Request.QueryString("duration")
                ViewState("duration") = strDuration
                bindGridView()
                lblSalesRepName.Text = strName
                lblSalesRepCode.Text = strSalesmanCode
                lblTarget.Text = strDuration
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Sub bindGridView()
        Dim obj As New txn_Performance.clsPerformance
        Dim dt As DataTable = Nothing
        Try

            dt = obj.getHeaderDT(strSalesmanCode, strDuration)
            'dt.Rows.Clear()
            'dt.Rows.Add(dt.NewRow)
            If dt.Rows.Count > 0 Then
                dgPerformance.DataSource = dt
                dgPerformance.DataKeyNames = New String() {"prod_grp_cd"}
                dgPerformance.DataBind()
            Else
                dt.Rows.Add(dt.NewRow)
                dgPerformance.DataSource = dt
                dgPerformance.DataBind()
                dgPerformance.Columns(4).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub addClickEvent(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles dgPerformance.RowCreated
        Dim obj As New txn_Performance.clsPerformance
        Dim dt As DataTable = Nothing
        Try
            dt = obj.getHeaderDT(strSalesmanCode, strDuration)
            If dt.Rows.Count > 0 Then
                If e.Row.RowIndex <> -1 Then
                    Dim PostBack As String = Page.ClientScript.GetPostBackEventReference(dgPerformance, "Select$" & e.Row.RowIndex)
                    e.Row.Attributes.Add("OnClick", PostBack)
                    e.Row.Attributes.Add("Style", "cursor:hand")
                End If
            End If
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub dg_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgPerformance.SelectedIndexChanged
        Dim strkey As String
        Try
            strkey = dgPerformance.SelectedDataKey.Value
            strDuration = CType(ViewState("duration"), String)
            Response.Redirect("../performance/performancedetail.aspx?duration=" & strDuration & "&prodcode=" & dgPerformance.SelectedDataKey.Value)
        Catch ex As Exception

        End Try
    End Sub

    Sub gridview_Selected(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles dgPerformance.RowCommand

        Try

            Dim strkey As String = dgPerformance.DataKeys(e.CommandArgument).Value
            strDuration = CType(ViewState("duration"), String)
            Response.Redirect("../performance/performancedetail.aspx?duration=" & strDuration & "&prodcode=" & dgPerformance.DataKeys(e.CommandArgument).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../performance/performanceoverview.aspx", False)
    End Sub
End Class



