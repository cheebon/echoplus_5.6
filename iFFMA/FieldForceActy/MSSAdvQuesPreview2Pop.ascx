﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MSSAdvQuesPreview2Pop.ascx.vb" Inherits="iFFMA_FieldForceActy_MSSAdvQuesPreview2Pop" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtDate" Src="~/include/wuc_txtDate.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<asp:UpdatePanel ID="updPnlMaintenance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <asp:Button runat="server" ID="btnHidden" Style="display: none" />
        <asp:Panel ID="pnlMsgPop" runat="server" Style="display: none; width: 400px; padding: 15px; height:500px; overflow:scroll;" CssClass="modalPopup">
            <asp:Panel ID="pnlMsgPopDragZone" runat="server" Style="background-color: #DDDDDD;
                border: solid 1px Gray; color: Black; width: 98%; padding-left: 10px; padding-bottom: 5px;
                text-align: center">
                <table width="100%">
                    <tr>
                        <td>
                            <span style="float:left; width:95%; padding-top: 5px; padding-bottom: 5px;"><asp:Label ID="lblTitle" runat="server" CssClass="cls_label_header" Text="" /></span>
                            <span style="float:left; width:5%; padding-top: 2px; padding-bottom: 1px;"><asp:ImageButton ID="imgClose" ImageUrl="~/images/ico_close.gif" runat="server" CssClass="cls_button" /></span>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
            <div id="lblPopNotify_pnlMsgPopContentZone" style="padding-top: 5px; width: 98%">
                
                <fieldset style="padding-left: 10px; width: 100%; padding-bottom: 5px;">
                    <asp:Label ID="lblContent" runat="server" CssClass="cls_label_header" Text="" />
                </fieldset>

                <asp:DataList ID="dListPreview" runat="server" DataKeyField="SUB_QUES_CODE" RepeatColumns="1" RepeatDirection="Horizontal" Width="100%" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="0" CellPadding="8">
                    <ItemTemplate>
                        <fieldset runat="server" id="pnlSubQues" style="padding-left:10px; width:100%; height:50px; padding-bottom:5px;">
                            <span style="float:left; width:100%; padding-top: 2px; padding-bottom: 2px;">
                                <asp:Label ID="lblSubQues" runat="server" Text='<%# Bind("SUB_QUES_NAME") %>' CssClass="cls_label_header" />
                            </span>
                    
                            <asp:TextBox ID="txtSubQues" runat="server" CssClass="cls_textbox" visible="false" />
                            <asp:Dropdownlist ID="ddlSubQues" runat="server" CssClass="cls_dropdownlist" visible="false" />
                            <customToolkit:wuc_txtDate ID="txtDateSubQues" runat="server" RequiredValidation="false" RequiredValidationGroup="Search" DateTimeFormatString="yyyy-MM-dd" visible="false" />
                            <asp:RadioButtonList ID="rdbListSubQues" runat="Server" CssClass="cls_radiobutton" RepeatDirection="Horizontal" Visible="false" >
                                <asp:ListItem Value="1" Text="True" Selected="True"/>
                                <asp:ListItem Value="0" Text="False"/>
                            </asp:RadioButtonList>
                            <ccGV:clsGridView ID="dgTextSubQues" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                                <Columns>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                        <itemstyle horizontalalign="Left" />
                                    </asp:BoundField>  
                                    <asp:TemplateField ItemStyle-Width="80" ItemStyle-HorizontalAlign="Center">
                                        <itemtemplate>
                                            <asp:TextBox ID="txtColumnSubQues" runat="server" class="cls_textbox"/>
                                        </itemtemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </ccGV:clsGridView>
                            <ccGV:clsGridView ID="dgSelectSubQues" runat="server" AllowSorting="False" AutoGenerateColumns="False"
                                Width="90%" FreezeHeader="True" GridHeight="80" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="False" PagerSettings-Visible="false" Visible="false">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="25" ItemStyle-HorizontalAlign="Center">
                                        <itemtemplate>
                                            <asp:checkbox ID="chkSelectSubQues" runat="server" class="cls_checkbox"/>
                                        </itemtemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CRITERIA_NAME" HeaderText="" ReadOnly="True">
                                        <itemstyle horizontalalign="Left" />
                                    </asp:BoundField>  
                                </Columns>
                            </ccGV:clsGridView>
                        </fieldset>
                    </ItemTemplate>
                </asp:DataList>

                <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
                <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
                <asp:HiddenField ID="hdQuesCode" runat="server" Value="" />
            </div>
        </asp:Panel>
        
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupMaintenance" runat="server" 
            BehaviorID="ModalPopupMaintenanceBehavior"
            TargetControlID="btnHidden" 
            CancelControlID="imgClose"
            PopupControlID="pnlMsgPop" 
            BackgroundCssClass="modalBackground"
            DropShadow="True" 
            RepositionMode="RepositionOnWindowResizeAndScroll" />
    </ContentTemplate>
</asp:UpdatePanel>