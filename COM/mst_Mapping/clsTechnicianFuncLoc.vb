'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	11/09/2008
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsTechnicianFuncLoc
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTechnicianFuncLoc"
        End Get
    End Property

    Public Function GetTechnicianFuncLocList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_TECHNICIAN_FUNC_LOC_LIST"
                .addItem("SEARCH_TYPE", strSearchType, clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTechnicianFuncLocDetails(ByVal strTechnicianCode As String, ByVal strFuncLocCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_TECHNICIAN_FUNC_LOC_DTL"
                .addItem("TECHNICIAN_CODE", strTechnicianCode, clsDB.DataType.DBString)
                .addItem("FUNC_LOC_CODE", strFuncLocCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetTechnicianFuncLocExcludeList(ByVal strTechnicianCode As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_TECHNICIAN_FUNC_LOC_EXCLUDE_LIST"
                .addItem("TECHNICIAN_CODE", strTechnicianCode, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateTechnicianFuncLoc(ByVal strTechnicianCode As String, ByVal strFuncLocCode As String, ByVal strSupervisorInd As String) As DataTable
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_TECHNICIAN_FUNC_LOC_CREATE"
                .addItem("TECHNICIAN_CODE", strTechnicianCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FUNC_LOC_CODE", strFuncLocCode, clsDB.DataType.DBString)
                .addItem("SUPERVISOR_IND", strSupervisorInd, clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function UpdateTechnicianFuncLoc(ByVal strTechnicianCode As String, ByVal strPKFuncLocCode As String, _
                                    ByVal strFuncLocCode As String, ByVal strSupervisorInd As String) As DataTable
        Dim objDB As clsDB
        Dim DT As DataTable = Nothing

        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_TECHNICIAN_FUNC_LOC_UPDATE"
                .addItem("TECHNICIAN_CODE", strTechnicianCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PK_FUNC_LOC_CODE", strPKFuncLocCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FUNC_LOC_CODE", strFuncLocCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SUPERVISOR_IND", strSupervisorInd, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                DT = .spRetrieve()
            End With
        Catch ex As Exception
            'Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try

        Return DT
    End Function

    Public Sub DeleteTechnicianFuncLoc(ByVal strTechnicianCode As String, ByVal strFuncLocCode As String)
        Dim objDB As clsDB
        Try
            objDB = New clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_TECHNICIAN_FUNC_LOC_DELETE"
                .addItem("TECHNICIAN_CODE", strTechnicianCode, cor_DB.clsDB.DataType.DBString)
                .addItem("FUNC_LOC_CODE", strFuncLocCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Class"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
