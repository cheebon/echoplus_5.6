﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SubmodulePrincipalDtl.aspx.vb" Inherits="Admin_Root_SubmodulePrincipal_SubmodulePrincipalDtl" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Submodule Principal Detail</title>
    <script type="text/javascript">
   
    function ChangeAllCheckBoxState(checkState,elementId)
    {
        var dglist = document.getElementById(elementId);
        if (dglist) 
        {
            for (var i = 1; i < dglist.rows.length; i++) 
            {
                if(dglist.rows[i].cells[0].childNodes[0]) 
                {
                    if (dglist.rows[i].cells[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].checked = checkState;
                }
            }
        }
    } </script>
</head>
<body>
    <form id="frmSubmdlPrincpDtl" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
         <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>                             
                
                <%--<table border="0" cellspacing="0" cellpadding="0" width="98%">
                <tr>
                    <td width="20%">
                        <asp:Label ID="lblSelectPrincipal" runat="server" Text="Principal : "></asp:Label></td><td>
                            <asp:DropDownList ID="ddlSelectPrincipal" runat="server">
                            </asp:DropDownList> </td>
                </tr>     
                </table>--%>
               
                
                <table border="0" cellspacing="0" cellpadding="0" width="98%">
                    <tr>
                        <td valign="top" width="20%">
                            <ccGV:clsGridView ID="dgModuleList" runat="server" ShowFooter="false" AllowSorting="True"
                                        AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="" 
                                        AddEmptyHeaders="0" CellPadding="2" EmptyHeaderClass="" FreezeColumns="0"
                                        FreezeRows="0" GridWidth="" AllowPaging="false" PagerSettings-Visible="false" DataKeyNames="module_id" OnRowCommand="dgModuleList_RowCommand">
                                        <Columns>                                                 
				                            <asp:ButtonField DataTextField="module_name" HeaderText="Module" CommandName="Select" AccessibleHeaderText="Select" ButtonType="Link" />                                                                                                                                                      
                                        </Columns>                                                
                                    </ccGV:clsGridView>
                             <br />
                             <!-- Reference part -->
                            <asp:Panel ID="pnlRef" runat="server">
                            
                            <div class="border_std" style="width:85%">
                            <asp:Label ID="lblRef" runat="server" Text="Pick any principal as a reference:"></asp:Label><br /><br />
                            <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblCountry" runat="server" Text="Select Country : "></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" width="150px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPrincipal" runat="server" Text="Select Principal : "></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlPrincipal" runat="server" width="150px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            </table>
                            <asp:Button ID="btnRef" runat="server" Text="Reference" />
                            </div>
                            
                            </asp:Panel>
                             <!-- Reference part end -->
                        </td>
                        <td valign="top" width="80%">
                           
                            <asp:Button ID="btnSave" runat="server" Text="Save" visible="false" /><br />       
                            <asp:Label ID="lblSaveMsg" runat="server" Text="Module saved." Visible="false"></asp:Label>
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="330px" AddEmptyHeaders="0" CellPadding="2"
                                            EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            AllowPaging="FALSE" PagerSettings-Visible="false" DataKeyNames="SUBMODULE_ID">
                                            <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                            <EmptyDataTemplate>Please select a module.</EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                <headertemplate>
                                                  <asp:checkbox id="SelectAllCheckBox"                                                   
                                                    runat="server" /> Select All
                                                </headertemplate>
                                                <itemtemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Container.DataItem("SELECT") %>' ENABLED='<%# Container.DataItem("ENABLE") %>'></asp:CheckBox>
                                                </itemtemplate>
                                                </asp:TemplateField> 
                                                <asp:BoundField DataField="SUBMODULE_CODE" HeaderText="Submodule Code" SortExpression="SUBMODULE_CODE" />
                                                <asp:BoundField DataField="SUBMODULE_NAME" HeaderText="Submodule Name" SortExpression="SUBMODULE_NAME" />
                                                                            
                                               
                                </Columns>
                            </ccGV:clsGridView>
                        </td>
                    </tr>
                </table>
                
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
