<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_page.ascx.vb" Inherits="iFFMR_SFA_USRCTRL_wuc_page" %>
<asp:UpdatePanel ID="UpdatePager" runat="server" RenderMode="Inline" UpdateMode="Conditional">
<ContentTemplate>
    <table id="tblctrlpanel" class="" cellspacing="0" cellpadding="0" width="100%" border="0" style="">
    <tr class="cls_label">
        <td>
            Showing 
            <asp:Label ID="lblStart" runat="server" Text="" CssClass="cls_label"></asp:Label> to 
            <asp:Label ID="lblEnd" runat="server" Text="" CssClass="cls_label"></asp:Label>&nbsp;
            <asp:ImageButton ID="IBPrev" runat="server" ImageUrl="~/images/ico_previous.gif" OnClick="IBPrev_Click" />&nbsp;
            <asp:ImageButton ID="IBNext" runat="server" ImageUrl="~/images/ico_next.gif" OnClick="IBNext_Click" /> &nbsp: &nbsp;&nbsp;&nbsp;|
            Page 
            <asp:TextBox ID="txtPage" runat="server" CssClass="cls_textbox" Width="30px" MaxLength="4"></asp:TextBox>&nbsp;/&nbsp;<asp:Label
                ID="lblPage" runat="server" Text="" CssClass="cls_label"></asp:Label>
            <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="cls_button" OnClick="btnGo_Click" />
        </td>
    </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
