<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPList_v2.aspx.vb" Inherits="iFFMA_SAP_SAPList_v2" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SAP List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />  
</head>
 <!--#include File="~/include/commonutil.js"--> 
<body class="BckgroundInsideContentLayout">
    <form id="frmSAPList" runat="server">
    <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
    <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="UpdateSearch" runat="server" UpdateMode="Always" RenderMode="Inline" >
        <ContentTemplate>
            <div style="width:98%" class="PagePadder">
            <fieldset class="" style="width: 98%;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td>
                </tr>
                <tr class="BckgroundInsideContentLayout">
                    <td><uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader></td>
                </tr>
                <tr style="height:5px">
                    <td></td>
                </tr>
                <tr class="BckgroundBenealthTitle">
                    <td>
                    
                    <table cellpadding="1" cellspacing="1" width="60%" class="cls_table">
                    <tr>
                        <TD width="10%" valign="top"><asp:label ID="lblDocType" Runat="server" CssClass="cls_label_header">Document Type</asp:label></TD>
                        <TD width="10%" valign="top"><asp:label ID="lblTeam" Runat="server" CssClass="cls_label_header">Team</asp:label></TD>
                        <TD width="10%" valign="top"><asp:label ID="lblSearch" Runat="server" CssClass="cls_label_header">Search By</asp:label></TD>
                        <TD valign="top"><asp:label id="lblSearchValue" runat="server" CssClass="cls_label_header"></asp:label></TD>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:dropdownlist id="ddlDocType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="False">
                                <asp:ListItem Value="SO">SALES ORDER</asp:ListItem>
                                <%--<asp:ListItem Value="TRA">TRADE RETURN</asp:ListItem>--%>
                            </asp:dropdownlist>
                        </td>
                        <td valign="top">
                            <asp:dropdownlist id="ddlTeam" runat="server" CssClass="cls_dropdownlist" AutoPostBack="False">
                                <asp:ListItem Value="ALL">All</asp:ListItem>
                            </asp:dropdownlist>
                        </td>
                        <td valign="top">						                                    
                            <asp:dropdownlist id="ddlSearchType" runat="server" CssClass="cls_dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlSearchType_SelectedIndexChanged">
                                <asp:ListItem Value="ALL">All</asp:ListItem>                   
                                <asp:ListItem Value="TXN_NO">Txn No</asp:ListItem>							                                    
                                <asp:ListItem Value="TXN_TIMESTAMP">Txn Timestamp</asp:ListItem>
                                <asp:ListItem Value="TXN_STATUS">Txn Status</asp:ListItem>							                                    
                                <asp:ListItem Value="CUST_CODE">Cust Code</asp:ListItem>
                                <asp:ListItem Value="SALESREP_CODE">Salesrep Code</asp:ListItem>
                                <asp:ListItem Value="CONT_CODE">Cont Code</asp:ListItem>
                                <asp:ListItem Value="VISIT_ID">Visit ID</asp:ListItem>
                            </asp:dropdownlist>					                                    
                           
                         </td>
                         <td valign="top" nowrap>		
                            <table border="0" cellpadding="0" cellspacing="0" width="350">
                                <tr>
                                    <td nowrap valign="top">
                                        <asp:dropdownlist id="ddlList" runat="server" CssClass="cls_dropdownlist"></asp:dropdownlist>
                                        <asp:textbox id="txtSearchValue" runat="server" CssClass="cls_textbox"></asp:textbox>
                                        <customToolkit:wuc_txtDate ID="Wuc_txtDate1" runat="server" ControlType="DateOnly" />
                                    </td>
                                    <td width="1%" valign="top"><asp:label id="lblTo" runat="server" CssClass="cls_label_header">To</asp:label></td>
                                    <td nowrap valign="top">
                                        <asp:textbox id="txtSearchValue1" runat="server" CssClass="cls_textbox"></asp:textbox>
                                        <customToolkit:wuc_txtDate ID="Wuc_txtDate2" runat="server" ControlType="DateOnly" />
                                    </td>
                                    <td valign="top"><asp:button id="btnSearch" runat="server" Text="Search" CssClass="cls_button" OnClick="btnSearch_Click"></asp:button></td>
                                </tr>
                            </table>	
                        </td>
                    </tr>
                </table>
                    <br />
                <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server"></uc1:wuc_dgpaging>
                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="true"
                    AllowSorting="true" AutoGenerateColumns="false" GridWidth="" FreezeHeader="true" Width="98%" PagerSettings-Visible="false"
                    GridHeight="300px" RowSelectionEnabled="true" OnRowDataBound="dgList_RowDataBound" EnableViewState="true" OnRowCommand="dgList_RowCommand" OnRowCreated="dgList_RowCreated" OnSorting="dgList_Sorting">
                    <%--<Columns>
                    <asp:TemplateField headertext="Send SAP">
                    <ItemTemplate>
                        <asp:LinkButton id="lnkUnBlock_SAP" runat="server" CommandArgument='<%# Eval("txn_no") & "@" & Eval("txn_timestamp") %>' CommandName="UnBlockSAP" text="<img src='../../images/ico_Unblock.gif' alt='Unblock' border='0'/>" CausesValidation="False" visible="False"></asp:LinkButton>
                    </ItemTemplate>
                    </asp:TemplateField>
                </Columns>--%>
                    <FooterStyle CssClass="GridFooter" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAlternate" />
                    <RowStyle CssClass="GridNormal" />
                </ccGV:clsGridView>        
                 
                    </td>
                </tr>
            </table>
            </fieldset>    
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
