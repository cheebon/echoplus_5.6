<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerContactDetail" CodeFile="CustomerContactDetail.aspx.vb" %>

<%@ Register TagPrefix="uc1" Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"  %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Contact Detail</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                        </asp:Timer>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td class="Bckgroundreport">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td align="left" style="height: 115px;" colspan="3">
                                                                                <table id="Table4" border="0" cellpadding="0" cellspacing="0" width="800">
                                                                                    <tr>
                                                                                        <td colspan="4" style="height: 24px">
                                                                                            <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" Width="60px" /></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblContactID" runat="server" CssClass="cls_label_header" Text="Contact ID"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblContactIDAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="cls_label_header"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblTitleAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblContactName" runat="server" CssClass="cls_label_header" Text="Contact Name"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblContactNameAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblSurname" runat="server" CssClass="cls_label_header" Text="Surname"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblSurnameAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblFirstName" runat="server" CssClass="cls_label_header" Text="First Name"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblFirstNameAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblChristianName" runat="server" CssClass="cls_label_header" Text="Christian Name"
                                                                                                Width="98px"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblChristianNameAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="Label1" runat="server" CssClass="cls_label_header" Text="Rep Specialty"
                                                                                                Width="93px"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblRepSpecialityAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblGender" runat="server" CssClass="cls_label_header" Text="Gender"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblGenderAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblDOB" runat="server" CssClass="cls_label_header" Text="DOB"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblDOBAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblMaritalStatus" runat="server" CssClass="cls_label_header" Text="Marital Status"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblMaritalStatusAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblMedSchool" runat="server" CssClass="cls_label_header" Text="Med School"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblMedSchoolAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblGradDate" runat="server" CssClass="cls_label_header" Text="Grad Date"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblGradDateAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblEmail" runat="server" CssClass="cls_label_header" Text="Email"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblEmailAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPhone" runat="server" CssClass="cls_label_header" Text="Phone"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPhoneAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblMobile" runat="server" CssClass="cls_label_header" Text="Mobile"></asp:Label></td>
                                                                                        <td style="width: 376px; height: 21px">
                                                                                            <asp:Label ID="lblMobileAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPosition" runat="server" CssClass="cls_label_header" Text="Position"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPositionAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPac" runat="server" CssClass="cls_label_header" Text="Pac"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblPacAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblDept" runat="server" CssClass="cls_label_header" Text="Dept"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblDeptAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblAssistant" runat="server" CssClass="cls_label_header" Text="Assistant"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblAssistantAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblLocation" runat="server" CssClass="cls_label_header" Text="Location"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblLocationAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPhone1" runat="server" CssClass="cls_label_header" Text="Phone 1"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblPhone1Ans" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPhone2" runat="server" CssClass="cls_label_header" Text="Phone 2"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPhone2Ans" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblFaxNo" runat="server" CssClass="cls_label_header" Text="Fax No"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblFaxNoAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPriority" runat="server" CssClass="cls_label_header" Text="Priority"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPriorityAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblBedCount" runat="server" CssClass="cls_label_header" Text="Bed Count"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblBedCountAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPatientCap" runat="server" CssClass="cls_label_header" Text="Patient Cap"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPatientCapAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPracSize2" runat="server" CssClass="cls_label_header" Text="Prac Size"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblPracSizeAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPatientPerDay" runat="server" CssClass="cls_label_header" Text="Patient Per Day"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPatientPerDayAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblMktgPrefs" runat="server" CssClass="cls_label_header" Text="Mktg Prefs"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblMktgPrefsAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPersonalInt" runat="server" CssClass="cls_label_header" Text="Personal Int"
                                                                                                Width="105px"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblPersonalIntAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblMedicalInt" runat="server" CssClass="cls_label_header" Text="Medical Int"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblMedicalIntAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblBTCall" runat="server" Text="BT Call" CssClass="cls_label_header"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:DropDownList ID="ddlBTCall" runat="server" CssClass="cls_dropdownlist">
                                                                                            </asp:DropDownList></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblVisitFreq" runat="server" Text="Visit Freq" CssClass="cls_label_header"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblVisitFreqAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblPSDPR" runat="server" CssClass="cls_label_header" Text="PDS/PR"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:DropDownList ID="ddlPDSPR" runat="server" CssClass="cls_dropdownlist">
                                                                                            </asp:DropDownList></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblXField1" runat="server" CssClass="cls_label_header" Text="XField 1"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblXField1Ans" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblXField2" runat="server" CssClass="cls_label_header" Text="XField 2"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblXField2Ans" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblNotes" runat="server" CssClass="cls_label_header" Text="Notes"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblNotesAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblStatus" runat="server" CssClass="cls_label_header" Text="Status"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblStatusAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblHospAbbv" runat="server" CssClass="cls_label_header" Text="Hosp Abbv"></asp:Label></td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                            <asp:Label ID="lblHospAbbvAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                            <asp:Label ID="lblHospRank" runat="server" CssClass="cls_label_header" Text="Hosp Rank"></asp:Label></td>
                                                                                        <td style="width: 350px; height: 21px">
                                                                                            <asp:Label ID="lblHospRankAns" runat="server" CssClass="cls_label"></asp:Label></td>
                                                                                        <td style="width: 200px; height: 21px">
                                                                                        </td>
                                                                                        <td style="height: 21px; width: 376px;">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
