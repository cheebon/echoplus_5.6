﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesrepGrpDtl.aspx.vb" Inherits="Admin_SalesrepGrp_SalesrepGrpDtl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register Src="~/include/wuc_ddlCountry.ascx" TagName="wuc_ddlCountry" TagPrefix="uc2" %>
<%@ Register Src="~/include/wuc_ddlPrincipal.ascx" TagName="wuc_ddlPrincipal" TagPrefix="uc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Salesrep Group</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <script type="text/javascript">
    
      function pageLoad() {
      }
    
    </script>
    <style type="text/css">
        
        .header_col {
            width: 150px;
            background-color: #C9D1F9;  
            vertical-align: top; 
            padding: 1px 2px 1px 7px;                            
        }
        .content_col {
            width: 80%;
            background-color: #EEEEEE;
            vertical-align: top;
            padding: 1px 2px 1px 7px;  
        }
       
        .cls_button
        {
            height: 26px;
        }
       
    </style>
</head>

<body class="BckgroundInsideContentLayout">
    <form id="frmSalesrepGrpDtl" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
        
        <asp:UpdatePanel ID="UpdatePanelDtl" runat="server">
            <ContentTemplate>
            <fieldset class="" style="width: 98%;">
            <table id="tbl1" cellspacing="0" cellpadding="2" width="100%" border="0">
                <tr><td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td></tr>
                <tr>			    
                    <td class="BckgroundInsideContentLayout">
                        <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                    </td>
                </tr>
                <tr><td class="BckgroundBenealthTitle" height="5"></td></tr>
                <tr class="Bckgroundreport" style="padding:3px;">
                    <td>
                        <div style="padding:1px 0px 1px 3px">
                            <asp:Button ID="cmdSave" runat="server" Text="Save" cssclass="cls_button" Width="80px"/>
                            <asp:Button ID="cmdEdit" runat="server" Text="Edit" cssclass="cls_button" Width="80px"/>
                            <asp:Button ID="cmdReturn" runat="server" Text="Back" cssclass="cls_button" Width="80px" CausesValidation="false"/>
                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" cssclass="cls_button" Width="80px" CausesValidation="false"/>
                        </div>
                        <div style="border:solid 1px #C9D1F9; width:auto">
                        <table border="0" cellpadding="2" cellspacing="1">
                            <tr>
                                <td class="header_col">
                                   <asp:Label ID="lblCountry" runat="server" Text="Country" CssClass="cls_label"></asp:Label> <asp:label id="Label1" runat="server" CssClass="cls_mandatory">*</asp:label>
                                </td>
                                <td class="content_col">
                                    <uc2:wuc_ddlCountry ID="Wuc_ddlCountry" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="header_col">
                                   <asp:Label ID="lblPrincipal" runat="server" Text="Principal" CssClass="cls_label"></asp:Label><asp:label id="Label4" runat="server" CssClass="cls_mandatory">*</asp:label> 
                                </td>
                                <td class="content_col">
                                   <uc5:wuc_ddlPrincipal id="Wuc_ddlPrincipal" runat="server">
                                                        </uc5:wuc_ddlPrincipal>
                                </td>
                            </tr>
                            <tr>
                                <td class="header_col">
                                    <asp:Label ID="lblSalesrepGrpName" runat="server" Text="Salesrep Group Name" CssClass="cls_label"></asp:Label><asp:label id="Label2" runat="server" CssClass="cls_mandatory">*</asp:label>
                                </td>                            
                                <td class="content_col">
                                    <asp:TextBox ID="txtSalesrepGrpName" runat="server" CssClass="cls_textbox" 
                                        Width="200px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvSalesrepGrp" runat="server" 
                                        ErrorMessage="Cannot be blank !" ControlToValidate="txtSalesrepGrpName" 
                                        Display="Dynamic" cssclass="cls_validator"></asp:RequiredFieldValidator><asp:Label
                                            ID="lblExists" runat="server" Text="Name Exists !" CssClass="cls_validator"></asp:Label>
                                </td>
                             </tr>
                             <tr>
                                <td class="header_col">
                                    <asp:Label ID="lblSalesrepGrpDesc" runat="server" Text="Salesrep Group Description" CssClass="cls_label"></asp:Label>
                                </td>                            
                                <td class="content_col">
                                    <asp:TextBox ID="txtSalesrepGrpDesc" runat="server" CssClass="cls_textbox" 
                                        Width="200px" MaxLength="250"></asp:TextBox>
                                </td>
                             </tr> 
                             <tr>  
                                <td class="header_col">
                                    <asp:Label ID="lblCriteria" runat="server" Text="Criteria Value" CssClass="cls_label"></asp:Label><asp:label id="Label3" runat="server" CssClass="cls_mandatory">*</asp:label>
                                </td>                            
                                <td class="content_col">
                                    <asp:TextBox ID="txtCriteria" runat="server" CssClass="cls_textbox" 
                                        Height="250px" TextMode="MultiLine" Width="500px" MaxLength="250" style=""></asp:TextBox><asp:RequiredFieldValidator
                                        ID="rfvCriteria" runat="server" 
                                        ErrorMessage="Cannot be blank !" ControlToValidate="txtCriteria" 
                                        Display="Dynamic" cssclass="cls_validator"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                        </div>
                    </td>
                </tr>
                <tr class="Bckgroundreport">
                    <td>
                        
                    </td>
                </tr>
                <tr class="Bckgroundreport">
                    <td></td>
                </tr>
                </table>
            </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
