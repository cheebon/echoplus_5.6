'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	OperationType User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlOperationType
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlOperationTypeID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlOperationTypeID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlOperationTypeID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlOperationTypeID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlOperationTypeID.SelectedValue = "0"
    '        Else
    '            ddlOperationTypeID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlOperationTypeID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return ddlOperationTypeID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlOperationTypeID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlOperationTypeID.SelectedValue = "0"
            Else
                ddlOperationTypeID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlOperationTypeID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlOperationTypeID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlOperationTypeID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlOperationTypeID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlOperationTypeID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlOperationTypeID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlOperationTypeID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlOperationTypeID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlOperationTypeID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvOperationTypeID.Visible 'cfvOperationTypeID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvOperationTypeID.Enabled = Value
            cfvOperationTypeID.Visible = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            If ddlOperationTypeID.Items.Count <= 0 Then
                'Get OperationType List
                objCommonQuery = New cor_Common.clsCommonQuery
                With objCommonQuery
                    '    .clsProperties.OperationTypeID = lngOperationTypeID
                    '    .clsProperties.DocTypeID = lngDocTypeID
                    dt = .GetOperationTypeList
                End With
                objCommonQuery = Nothing

                ddlOperationTypeID.Items.Clear()
                ddlOperationTypeID.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlOperationTypeID.Items.Add(New ListItem(Trim(drRow("operation_type_name")), Trim(drRow("operation_type_id")) & "@" & Trim(drRow("operation_type_code"))))
                    Next
                End If
            End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlOperationType.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


