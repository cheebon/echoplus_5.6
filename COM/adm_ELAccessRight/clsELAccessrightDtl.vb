﻿Option Explicit On
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data

''' <summary>
'''************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-11-15
'''	Purpose	    :	Class EAsy Loader Accessright Dtl
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''*************************************************************************
'''  
''' </summary>
''' <remarks></remarks>

Public Class clsELAccessrightDtl
    Implements IDisposable
#Region "Internal Properties"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn")) 'cor_DB.clsConStr.FFMAConnectionString
    Public clsProperties As New clsProperties.clsARPpt
#End Region
 
    Public Sub Alter(ByVal _AddNewUpdate_flag As String, ByVal _Delete_flag As String, ByVal _FullRefresh_flag As String, ByVal _strUserID As String)
        Dim objDB As cor_DB.clsDB
        'Dim lngAccessRightDtlID As Long

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHTDTL_ALTER"
                .addItem("accessright_id", clsProperties.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("cat_code", clsProperties.cat_code, cor_DB.clsDB.DataType.DBString)
                .addItem("el_code", clsProperties.el_code, cor_DB.clsDB.DataType.DBString)
                .addItem("AddNewUpdate_flag", _AddNewUpdate_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("Delete_flag", _Delete_flag, cor_DB.clsDB.DataType.DBInt)
                .addItem("FullRefresh_flag", _FullRefresh_flag, cor_DB.clsDB.DataType.DBInt)
                '.addItem("delete_flag", _delete, cor_DB.clsDB.DataType.DBInt)
                .addItem("user_id", _strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spInsert()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.SeverityID = 4
                '.clsProperties.Exception = msg
                .Log()
            End With
            objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
