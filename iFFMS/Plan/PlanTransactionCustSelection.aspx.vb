﻿Imports System.Data
Partial Class iFFMS_Plan_PlanTransactionCustSelection
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "TransactionCustSelection"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'Call Header
        With wuc_lblheader
            .Title = "Contact List By Customer"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

            TimerControl1.Enabled = True
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
        End If



    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Sub onpageload()
        Dim strSalesrepCode, strCustCode, strUserId As String, strRouteDate As String
        'Put user code to initialize the page here
        Try
            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            Dim dtind As DataTable

            strCustCode = Request.QueryString("custcode")
            Session("customercode") = strCustCode
            strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strUserId = Trim(Session("UserID"))

            dtind = clsPrePlan.getContactInd(strSalesrepCode, strCustCode, strUserId, strRouteDate)

            If dtind.Rows(0)(0) = 0 Then
                ddldeptname.Enabled = False
                bindGridView(strSalesrepCode, strCustCode, "", 0, strRouteDate)
            Else
                ddldeptname.Enabled = True
                Loadddldeptname()
                bindGridView(strSalesrepCode, strCustCode, ddldeptname.SelectedValue, 1, strRouteDate)
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub Loadddldeptname()
        Dim DT As DataTable
        Dim clsPrePlan As New txn_PrePlan.clsPrePlan
        Dim strSalesrepCode, strCustCode, strUserId As String

        strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
        strCustCode = Request.QueryString("custcode")
        strUserId = Trim(Session("UserID"))

        DT = clsPrePlan.getDeptName(strSalesrepCode, strCustCode, strUserId)
        With ddldeptname
            .Items.Clear()
            .DataSource = DT.DefaultView
            .DataTextField = "DEPT_NAME"
            .DataValueField = "DEPT_NAME"
            .DataBind()
            .Items.Insert(0, New ListItem("ALL", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strDeptName As String, ByVal strTypeId As String, ByVal strRouteDate As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_PrePlan.clsPrePlan

        Try
            Dim strUserId As String = Trim(Session("UserID"))
            dt = obj.getContactGeneralInfoDT(strSalesrepCode, strCustCode, strTypeId, strDeptName, strUserId, strRouteDate)
            If dt.Rows.Count > 0 Then
                GridView1.DataKeyNames = New String() {"cont_acc_code"}
                GridView1.DataSource = dt
                GridView1.DataBind()
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                GridView1.DataSource = dtnew
                GridView1.DataBind()
                GridView1.Columns(6).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub gridview_Selected(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand

        Try

            If e.CommandName = "funny" Then

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".redirect : " & ex.ToString)
        End Try
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated

        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim strSalesmanCode, strCustCOde, strContactCode, strRouteDate As String

                strSalesmanCode = Trim(Session("dflsalesrepcode"))
                strCustCOde = Trim(Session("customercode"))
                strContactCode = GridView1.DataKeys(e.Row.RowIndex).Item("cont_acc_code")
                Session("contactcode") = strContactCode
                strRouteDate = Trim(Request.QueryString("RouteDate"))


                Dim StrScript As String
                StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/Plan/PlanTxnMenu.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "&RouteDate=" + strRouteDate + "&visitdate=" + Now().ToString("yyyy-MM-dd") + "&visittime=" + Now().ToString("HH:mm") + "';"
                Dim strAction As String
                strAction = "HideElement('ContentBar');ResizeFrameWidth('TopBar','100%');HideElement('TopBar');ShowElement('DetailBar'); ResizeFrameHeight('DetailBarIframe','45px');"
                e.Row.Cells(7).Attributes.Add("onclick", strAction + StrScript)
                e.Row.Cells(7).Attributes.Add("style", "cursor:pointer;")
                e.Row.Attributes.Add("onclick", "javascript:__doPostBack('gridview1','Select$" & e.Row.RowIndex & "')")


                '---------------
                Dim str0 As String '5 is the column to add attributes the link
                str0 = "parent.document.getElementById('ContentBarIframe').src='../../iFFMS/Customer/CustList/ContactDtl.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "';"
                Dim strAction2 As String
                strAction2 = " HideElement('TopBar'); ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe'); ResizeFrameWidth('ContentBar', '100%');"
                e.Row.Cells(0).Attributes.Add("onclick", str0 + strAction2)
                e.Row.Cells(0).Attributes.Add("style", "cursor:pointer;")

            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.SelectedIndexChanged
        'Dim strContactCode As String
        'Dim strSalesmanCode, strCustCOde As String

        'strSalesmanCode = Trim(Session("dflsalesrepcode"))
        'strCustCOde = Trim(Session("customercode"))
        'strContactCode = GridView1.SelectedDataKey.Value
        'Session("contactcode") = strContactCode
        'Dim StrScript As String
        'StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMenu.aspx?custcode=" + strCustCOde + "&contcode=" + strContactCode + "&salesrepcode=" + strSalesmanCode + "&visitdate=" + Now().ToString("yyyy-MM-dd") + "&visittime=" + Now().ToString("HH:mm") + "';"
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
        'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "HideTopShowDetail", "HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar'); ResizeFrameHeight('DetailBarIframe','42px')", True)
    End Sub

    'Sub transactionsInserted()
    '    Try
    '        Session("DRCInserted") = "No"
    '        Session("SalesOrderInserted") = "No"
    '        Session("SFMSInserted") = "No"
    '        Session("MSSInserted") = Nothing
    '        Session("TradeReturnInserted") = "No"
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Sub transactionsSaved()
    '    Try
    '        Session("DRCSaved") = "No"
    '        Session("SalesOrderSaved") = "No"
    '        Session("SFMSSaved") = "No"
    '        Session("MSSSaved") = Nothing
    '        Session("TradeReturnSaved") = "No"
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Sub transactionNothing()
    '    Session("DRCList") = Nothing
    '    Session("DRCHdr") = Nothing
    '    Session("cartdt") = Nothing
    '    Session("SalesHdr") = Nothing
    '    Session("SFMSSession") = Nothing
    '    Session("SFMSHdr") = Nothing
    '    Session("TradeReturn") = Nothing
    '    Session("TradeRetHdr") = Nothing
    '    Session("MSSList") = Nothing
    '    Session("MSSHdr") = Nothing
    'End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddldeptname_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddldeptname.SelectedIndexChanged

        Dim strSalesrepCode, strCustCode, strUserId, strRouteDate As String

        strSalesrepCode = Trim(Request.QueryString("SALESREPCODE"))
        strCustCode = Request.QueryString("custcode")
        strUserId = Trim(Session("UserID"))
        strRouteDate = Trim(Request.QueryString("RouteDate"))
        bindGridView(strSalesrepCode, strCustCode, ddldeptname.SelectedValue, 1, strRouteDate)
    End Sub

End Class
