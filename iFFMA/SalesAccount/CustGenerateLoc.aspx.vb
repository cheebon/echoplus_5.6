'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	18/03/2008
'	Purpose	    :	Customer List
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_SalesAccount_CustGenerateLoc
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "CustGenerateLoc.aspx"
        End Get
    End Property
    Public Property CustCode() As String
        Get
            Return ViewState("CustCode")
        End Get
        Set(ByVal value As String)
            ViewState("CustCode") = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CUSTLIST)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_toolbar
                .SubModuleID = SubModuleType.CUSTGENERATELOC
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            If Not IsPostBack Then
                TimerControl1.Enabled = True
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub



#Region "EVENT HANDLER"

  
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.EnqSearchBtn_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.BackBtn_Click
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally

        End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New mst_SalesAccount.clsCustomer
            With (wuc_toolbar)
                DT = clsCustomer.GetCustGPSList(.SearchType, .SearchValue, .Status)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function GetRecListExport() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New mst_SalesAccount.clsCustomer
            With (wuc_toolbar)
                DT = clsCustomer.GetCustGPSList(.SearchType, .SearchValue, .Status)
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            If isExport = True Then
                dtCurrentTable = GetRecListExport()

                If dtCurrentTable Is Nothing Then
                    dtCurrentTable = GetRecList()
                End If
            Else
                dtCurrentTable = GetRecList()
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
                '.Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If

            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Try
            Select Case e.CommandName

                Case "ViewMap"
                    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                    CustCode = dgList.DataKeys(index).Item("CUST_CODE")
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

  
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region



#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            'Dim blnAllowSorting As Boolean = dgList.AllowSorting
            'Dim blnAllowPaging As Boolean = dgList.AllowPaging

            'dgList.AllowSorting = False
            'dgList.AllowPaging = False
            'RefreshDatabinding(True) 'HL:20080424

            'Dim aryDgList As New ArrayList
            'aryDgList.Add(dgList)
            'If TabPanel2.Visible Then
            '    aryDgList.Add(wuc_CustomerPriceGrpList.GetdgPriceGrpList())
            'End If
            'If TabPanel3.Visible Then
            '    aryDgList.Add(wuc_CustomerContList.GetdgContactList())
            'End If

            ''If tcResult.ActiveTab.ID = "TabPanel3" Then
            ''    aryDgList.Add(wuc_CustomerContList.GetdgContactList())
            ''Else
            ''    aryDgList.Add(dgList)
            ''    If TabPanel2.Visible Then
            ''        aryDgList.Add(wuc_CustomerPriceGrpList.GetdgPriceGrpList())
            ''    End If
            ''End If



            'wuc_toolbar.ExportToFile(aryDgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            'dgList.AllowPaging = blnAllowPaging
            'dgList.AllowSorting = blnAllowSorting
            'RefreshDatabinding()
            'If TabPanel2.Visible Or TabPanel3.Visible Then
            '    RefreshAllTab()
            'End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadChk(ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            Dim TC As TableCell

            TC = e.Row.Cells(aryDataItem.IndexOf("chkSelect"))
            Dim chkSelected As New CheckBox
            With chkSelected
                .ID = "chkSelected"
                .CssClass = "cls_checkbox"
            End With

            TC.Controls.Add(chkSelected)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub dgList_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    'If Master_Row_Count > 0 Then
                    '    LoadChk(e)
                    'End If

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                'Case DataControlRowType.Header
                '    If Master_Row_Count > 0 Then


                '        Dim chkAll As CheckBox = CType(e.Row.FindControl("chkAll"), CheckBox)
                '        If chkAll Is Nothing Then
                '            chkAll = New CheckBox
                '            chkAll.ID = "chkAll"
                '            e.Row.Cells(e.Row.Cells.Count - 1).Controls.Add(chkAll)
                '        End If
                '        chkAll.Attributes.Add("onClick", "toggle(this)")
                '        chkAll.ToolTip = "Click to toggle the selection of ALL rows"
                '    End If
                'Case DataControlRowType.DataRow
                '    If Master_Row_Count > 0 Then
                '        LoadChk(e)
                '    End If

            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
End Class

'Public Class CF_CustomerList
'    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
'        Dim strFieldName As String = ""
'        Select Case ColumnName.ToUpper
'            Case "ABBV"
'                strFieldName = "Abbreviation"
'            Case "FULL_ADD"
'                strFieldName = "Address"
'            Case "LATITUDE"
'                strFieldName = "Latitude"
'            Case "LONGITUDE"
'                strFieldName = "Longitude"
'            Case Else
'                strFieldName = Report.GetDisplayColumnName(ColumnName)
'        End Select

'        Return strFieldName
'    End Function

'    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
'        Try
'            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
'            strColumnName = strColumnName.ToUpper

'            If strColumnName = "ID" Then
'                FCT = FieldColumntype.InvisibleColumn
'            Else
'                FCT = FieldColumntype.BoundColumn
'            End If

'            Return FCT
'        Catch ex As Exception

'        End Try
'    End Function

'    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
'        Dim strFormatString As String = ""
'        Try
'            strColumnName = strColumnName.ToUpper

'            If strColumnName Like "*DATE" Then
'                strFormatString = "{0:yyyy-MM-dd}"
'            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
'                strFormatString = "{0:#,0}"
'            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
'                strFormatString = "{0:#,0.00}"
'            Else
'                strFormatString = ""
'            End If

'        Catch ex As Exception
'        End Try

'        Return strFormatString
'    End Function

'    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
'        Dim CS As New ColumnStyle
'        Try
'            With CS
'                Dim strColumnName As String = ColumnName.ToUpper
'                .FormatString = GetOutputFormatString(ColumnName)

'                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
'                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
'                OrElse strColumnName Like "TIME_*" Then
'                    .HorizontalAlign = HorizontalAlign.Center
'                    .Wrap = False   'HL:20070711
'                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
'                    .HorizontalAlign = HorizontalAlign.Right
'                Else
'                    .HorizontalAlign = HorizontalAlign.Left
'                End If

'            End With

'        Catch ex As Exception

'        End Try
'        Return CS
'    End Function
'End Class

