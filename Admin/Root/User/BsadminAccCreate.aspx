﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BsadminAccCreate.aspx.vb" Inherits="Admin_Root_User_BsadminAccCreate" StylesheetTheme="Default" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bsadmin Creation</title>
</head>
<!--#include File="~/include/commonutil.js"-->
<body>
    <form id="frmBsadminCreation" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" ScriptMode="release" />
    
    <div>
        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" /> <br />
        <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
        
        <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblErr" runat="server" SkinID="label_err"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
            <ContentTemplate>
                <div class="border_std">
                    <asp:Button ID="btnCreate" runat="server" Text="Create" CausesValidation="true"
                                                    Visible="true" />
                </div>
                
                <div class="border_std">
                    <asp:Label ID="lblMesg" runat="server" Text="" CssClass="cls_validator"></asp:Label>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width:100px">
                                <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                              </td>
                            <td>
                                <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="true" Width="150px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ErrorMessage="*" ControlToValidate="ddlCountry" CssClass="cls_validator" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr> 
                        
                        <tr>
                            <td><asp:Label ID="lblPrincipal" runat="server" Text="Principal"></asp:Label></td>
                            <td><asp:DropDownList ID="ddlPrincipal" runat="server" AutoPostBack="true" Width="150px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvPrincipal" runat="server" ErrorMessage="*" ControlToValidate="ddlPrincipal" CssClass="cls_validator" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="Label1" runat="server" Text="Login/User Code"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtLogin" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ErrorMessage="*" ControlToValidate="txtLogin" CssClass="cls_validator" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblPassword2" runat="server" Text="Retype Password"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="txtPassword2" CssClass="cls_validator" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvPassword" runat="server" 
                                    ErrorMessage="Password not same!" ControlToCompare="txtPassword1" 
                                    ControlToValidate="txtPassword2" CssClass="cls_validator" Display="Dynamic"></asp:CompareValidator>
                                </td>
                        </tr>
                    </table>                
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
