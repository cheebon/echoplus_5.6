
Partial Class include_wuc_pnlprdfreq
    Inherits System.Web.UI.UserControl
    Public Event ResetBtn_Click As EventHandler
    Public Event RefreshBtn_Click As EventHandler
    Private strTeamCodeList, strSalesrepCodeList, strStartYear, strStartMonth, strEndYear, strEndMonth, strPTLCode, str_Class As String

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_pnlprdfreq"
        End Get
    End Property


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                LoadLsbTeam()
                Loadddlyear()
                Loadddlmonth()
                Loadddlclass()
                Loadddlptlcode()
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub

#Region "PROPERTY"
    Public Property TeamCode() As String
        Get
            Return strTeamCodeList
        End Get
        Set(ByVal value As String)
            strTeamCodeList = value
        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return strSalesrepCodeList
        End Get
        Set(ByVal value As String)
            strSalesrepCodeList = value
        End Set
    End Property

    Public Property StartYear() As String
        Get
            Return strStartYear
        End Get
        Set(ByVal value As String)
            strStartYear = value
        End Set
    End Property

    Public Property EndYear() As String
        Get
            Return strEndYear
        End Get
        Set(ByVal value As String)
            strEndYear = value
        End Set
    End Property

    Public Property StartMonth() As String
        Get
            Return strStartMonth
        End Get
        Set(ByVal value As String)
            strStartMonth = value
        End Set
    End Property

    Public Property EndMonth() As String
        Get
            Return strEndMonth
        End Get
        Set(ByVal value As String)
            strEndMonth = value
        End Set
    End Property

    Public Property PtlCode() As String
        Get
            Return strPTLCode
        End Get
        Set(ByVal value As String)
            strPTLCode = value
        End Set
    End Property

    Public Property strClass() As String
        Get
            Return str_Class
        End Get
        Set(ByVal value As String)
            str_Class = value
        End Set
    End Property

    Public WriteOnly Property PanelCollapese() As Boolean
        Set(ByVal value As Boolean)
            CPE_PnlMultiAuthen.ClientState = value.ToString
            CPE_PnlMultiAuthen.Collapsed = value
        End Set
    End Property

#End Region

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamAdd.Click
        Try
            If AddToListBox(lsbTeam, lsbSelectedTeam) > 0 Then LoadLsbSalesrep()
            Loadddlptlcode()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamAdd_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamRemove.Click
        Try
            If AddToListBox(lsbSelectedTeam, lsbTeam) > 0 Then LoadLsbSalesrep()
            Loadddlptlcode()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamRemove_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamAddAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            If AddToListBox(lsbTeam, lsbSelectedTeam) > 0 Then LoadLsbSalesrep()
            Loadddlptlcode()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamAddAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkTeamRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTeamRemoveAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            If AddToListBox(lsbSelectedTeam, lsbTeam) > 0 Then LoadLsbSalesrep()
            Loadddlptlcode()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkTeamRemoveAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRAdd.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRAdd_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRRemove.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRRemove_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRAddAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRAddAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkSRRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSRRemoveAll.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkSRRemoveAll_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "DATES"
    Protected Sub Loadddlyear()

        Dim StrCurrentYear As String
        Dim StrPreviousYear As String

        StrCurrentYear = DatePart(DateInterval.Year, Now())
        StrPreviousYear = DatePart(DateInterval.Year, DateAdd(DateInterval.Year, -1, Now()))

        With ddlstartdateyear
            .Items.Insert(0, New ListItem(StrCurrentYear, StrCurrentYear))
            .Items.Insert(1, New ListItem(StrPreviousYear, StrPreviousYear))
        End With

        With ddlenddateyear
            .Items.Insert(0, New ListItem(StrCurrentYear, StrCurrentYear))
            .Items.Insert(1, New ListItem(StrPreviousYear, StrPreviousYear))
        End With

    End Sub

    Protected Sub Loadddlmonth()

        Dim StrCurrentMonth As String
        StrCurrentMonth = DatePart(DateInterval.Month, Now())

        With ddlstartdatemonth
            .Items.Insert(0, New ListItem("Jan", "1"))
            .Items.Insert(1, New ListItem("Feb", "2"))
            .Items.Insert(2, New ListItem("Mar", "3"))
            .Items.Insert(3, New ListItem("Apr", "4"))
            .Items.Insert(4, New ListItem("May", "5"))
            .Items.Insert(5, New ListItem("Jun", "6"))
            .Items.Insert(6, New ListItem("Jul", "7"))
            .Items.Insert(7, New ListItem("Aug", "8"))
            .Items.Insert(8, New ListItem("Sep", "9"))
            .Items.Insert(9, New ListItem("Oct", "10"))
            .Items.Insert(10, New ListItem("Nov", "11"))
            .Items.Insert(11, New ListItem("Dec", "12"))
            .SelectedIndex = StrCurrentMonth - 1
        End With

        With ddlenddatemonth
            .Items.Insert(0, New ListItem("Jan", "1"))
            .Items.Insert(1, New ListItem("Feb", "2"))
            .Items.Insert(2, New ListItem("Mar", "3"))
            .Items.Insert(3, New ListItem("Apr", "4"))
            .Items.Insert(4, New ListItem("May", "5"))
            .Items.Insert(5, New ListItem("Jun", "6"))
            .Items.Insert(6, New ListItem("Jul", "7"))
            .Items.Insert(7, New ListItem("Aug", "8"))
            .Items.Insert(8, New ListItem("Sep", "9"))
            .Items.Insert(9, New ListItem("Oct", "10"))
            .Items.Insert(10, New ListItem("Nov", "11"))
            .Items.Insert(11, New ListItem("Dec", "12"))
            .SelectedIndex = StrCurrentMonth - 1
        End With

    End Sub

#End Region

#Region "PTL_CODE"
    Private Sub Loadddlptlcode()

        Dim clsCatName As New rpt_Customize.clsSFActy

        Dim strUserID As String = Session.Item("UserID")
        Dim strPrincipalID As String = Session("PRINCIPAL_ID")

        Dim strTeamList As String
        strTeamList = GetItemsInString(lsbSelectedTeam)

        With ddlptlcode
            .Items.Clear()
            .DataSource = clsCatName.GetCatName(strUserID, strPrincipalID, strTeamList)
            .DataTextField = "CAT_NAME"
            .DataValueField = "CAT_NAME"
            .DataBind()
            .Items.Insert(0, New ListItem("Unspecified", ""))
            .SelectedIndex = 0
        End With
    End Sub


#End Region

#Region "Class"
    Protected Sub Loadddlclass()
        With ddlclass
            .Items.Insert(0, New ListItem("Unspecified", ""))
            .Items.Insert(1, New ListItem("A", "A"))
            .Items.Insert(2, New ListItem("B", "B"))
            .Items.Insert(3, New ListItem("C", "C"))
            .Items.Insert(4, New ListItem("OTH", "OTH"))
            .SelectedIndex = 0
        End With
    End Sub


#End Region

#Region "EVENT HANDLER"
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try

            Dim StrCurrentMonth As String
            StrCurrentMonth = DatePart(DateInterval.Month, Now())

            lnkTeamRemoveAll_Click(sender, e)
            ddlstartdatemonth.SelectedIndex = StrCurrentMonth - 1
            ddlenddatemonth.SelectedIndex = StrCurrentMonth - 1
            ddlstartdateyear.ClearSelection()
            ddlstartdateyear.ClearSelection()
            ddlptlcode.ClearSelection()
            'RaiseEvent ResetBtn_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnReset_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try

            If lsbSelectedSalesrep.Items.Count = 0 Then
                lblInfo.Text = "<BR/>Please select atleast 1 salesrep to cotinue."
                Exit Sub
            Else
                lblInfo.Text = String.Empty
            End If

            TeamCode = Trim(GetItemsInString(lsbSelectedTeam))
            SalesrepCode = Trim(GetItemsInString(lsbSelectedSalesrep))
            StartYear = Trim(ddlstartdateyear.SelectedValue)
            EndYear = ddlenddateyear.SelectedValue
            StartMonth = Trim(ddlstartdatemonth.SelectedValue)
            EndMonth = Trim(ddlenddatemonth.SelectedValue)
            PtlCode = Trim(ddlptlcode.SelectedValue)
            strClass = Trim(ddlclass.SelectedValue)

            RaiseEvent RefreshBtn_Click(sender, e)

            PanelCollapese = True

            'CPE_PnlMultiAuthen.ClientState = "true"
            'CPE_PnlMultiAuthen.Collapsed = True

            UpdatePnlMultiAuthen.Update()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "ListBox Action"
    Private Function AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox) As Integer
        Dim iTotal As Integer = lsbFrom.GetSelectedIndices().Length
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToListBox : " & ex.ToString)
        End Try
        Return iTotal
    End Function

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
