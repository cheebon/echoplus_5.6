﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PlanRmks.aspx.vb" Inherits="iFFMS_PlanCuz_PlanRmks" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Plan Addon Remarks</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('TopBar');HideElement('DetailBar');ShowElement('ContentBar'); MaximiseFrameHeight('ContentBarIframe')"
    class="BckgroundInsideContentLayout">
   <form id="frmplanaddonRmks" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset class="" style="width: 98%;">
            <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
            <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="block">
                <ContentTemplate>
                    <div id="title">
                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="cls_button" Visible="true"
                            Width="80px" OnClientClick="HideElement('ContentBar');ShowElement('TopBar'); MaximiseFrameHeight('TopBarIframe')" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="txnContent">
                <asp:UpdatePanel ID="UpdateRmks" runat="server" UpdateMode="Conditional" RenderMode="block">
                    <ContentTemplate>
                        <asp:Timer ID="TimerControlRmks" runat="server" Enabled="False" Interval="100" OnTick="TimerControlRmks_Tick" />
                        <div id="Rmks" class="S_DivHeader">
                            Remarks
                        </div>
                        <asp:Panel ID="pRmks" runat="server" Width="99.8%" CssClass="cls_panel_header">
                        <asp:Label ID="lblmsg" runat="server"  CssClass="cls_label_header" Text="" Font-Italic="true" BackColor="AliceBlue"></asp:Label>
                        <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                        <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="cls_button" ValidationGroup="newRmks"
                                                 Width="80px" />
                        </div>
                        <div style="width: 98%; padding-left: 5px; padding-bottom: 5px; padding-top: 5px;">
                            <span class="cls_label_header" style=" vertical-align:top ">Remarks :</span>
                            <asp:TextBox ID="txtRmks" CssClass="cls_textbox" TextMode= "MultiLine" Width="400px" Rows="20" runat="server" 
                            ValidationGroup="newRmks"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtRmks" runat="server" ErrorMessage="Enter remarks"  ControlToValidate="txtRmks" 
                            ValidationGroup="newRmks"  CssClass="cls_label_header" ></asp:RequiredFieldValidator>
                        </div>
                        </asp:Panel>
                         <ajaxToolkit:CollapsiblePanelExtender ID="cpeRmks" runat="server" TargetControlID="pRmks"
                                    ExpandControlID="Rmks" CollapseControlID="Rmks">
                                </ajaxToolkit:CollapsiblePanelExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </form>
</body>
</html>
