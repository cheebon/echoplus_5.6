﻿Imports System.Data

Partial Class iFFMA_SalesAccount_CustomerTeamClassPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    ''' 
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(hdContCode.Value)
        End Get
        Set(ByVal value As String)
            hdContCode.Value = value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return Trim(hdType.Value)
        End Get
        Set(ByVal value As String)
            hdType.Value = value
        End Set
    End Property

    Public Property TeamClass() As String
        Get
            Return Trim(hdTeamClass.Value)
        End Get
        Set(ByVal value As String)
            hdTeamClass.Value = value
        End Set
    End Property

    Public Property Customer() As String
        Get
            Return Trim(hdCustomer.Value)
        End Get
        Set(ByVal value As String)
            hdCustomer.Value = value
        End Set
    End Property

    Public Property Contact() As String
        Get
            Return Trim(hdContact.Value)
        End Get
        Set(ByVal value As String)
            hdContact.Value = value
        End Set
    End Property

    Public Property IsEdit() As Boolean
        Get
            Return Trim(hdIsEdit.Value)
        End Get
        Set(ByVal value As Boolean)
            hdIsEdit.Value = value
        End Set
    End Property
#End Region
#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()

        Dim txtBoxList = (From controls In pnlInsert.Controls.OfType(Of TextBox)()
                          Where Not String.IsNullOrEmpty(controls.Text)
                          Select controls).ToList()

        If (txtBoxList.Any()) Then
            For Each item In txtBoxList
                item.Text = String.Empty
            Next
        End If

        'Reset to --SELECT--
        ddlTeamClass.SelectedValue = "0"
        searchCustContBtn.Visible = True
        'txtCust.Text = String.Empty
        'txtCont.Text = String.Empty
        'txtPrd1.Text = String.Empty
        'txtPrd2.Text = String.Empty
        'txtPrd3.Text = String.Empty
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCustCont_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_CustContSearch)
                .ResetPage()
                .BindDefault()
                .Show()
            End With
            Hide()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustContSearch.SelectButton_Click
        Try
            txtCust.Text = wuc_CustContSearch.CustCode + " - " + wuc_CustContSearch.CustName
            txtCont.Text = wuc_CustContSearch.ContCode + " - " + wuc_CustContSearch.ContName
            CustCode = wuc_CustContSearch.CustCode
            ContCode = wuc_CustContSearch.ContCode
            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustContSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = String.Empty

            If String.IsNullOrEmpty(CustCode) Or _
               String.IsNullOrEmpty(ContCode) Or _
               String.IsNullOrEmpty(Trim(txtType.Text)) Then
                lblInfo.Text = "One or more fields are required!"
                Show()
                Exit Sub
            End If

            If ddlTeamClass.SelectedValue = "0" Then
                lblInfo.Text = "Please select a Team Class!"
                Show()
                Exit Sub
            End If

            Dim DT As DataTable
            Dim clsCustomer As New mst_SalesAccount.clsCustomer

            If (IsEdit) Then
                DT = clsCustomer.UpdateCustomerTeamClass(CustCode, ContCode, Type, Trim(txtType.Text), ddlTeamClass.SelectedValue)
            Else
                DT = clsCustomer.CreateCustomerTeamClass(CustCode, ContCode, Trim(txtType.Text), ddlTeamClass.SelectedValue)
            End If

            Dim isDuplicate As Integer = 0

            If DT.Rows.Count > 0 Then
                isDuplicate = DT.Rows(0)("IS_DUPLICATE")
            End If

            If isDuplicate = 1 Then
                lblInfo.Text = "The record already exists!"
                Show()
                Exit Sub
            Else
                'Update HiddenField After Update Success
                TeamClass = ddlTeamClass.SelectedValue
                lblInfo.Text = "The record is saved successfully."

                'Update Type with new Text
                Type = Trim(txtType.Text)
            End If

            If Not (IsEdit) Then
                ResetPage()
            End If
            Show()

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub LoadTeamClassDetails()
        Try
            searchCustContBtn.Visible = False
            
            Dim DT As DataTable
            Dim clsCustomer As New mst_SalesAccount.clsCustomer

            DT = clsCustomer.GetCustomerTeamClassDetail(CustCode, ContCode, Type)
            If DT.Rows.Count > 0 Then
                txtCust.Text = DT.Rows(0)("CUSTOMER")
                txtCont.Text = DT.Rows(0)("CONTACT")
                txtType.Text = DT.Rows(0)("TYPE")
                ddlTeamClass.SelectedValue = DT.Rows(0)("TEAM_CLASS")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class