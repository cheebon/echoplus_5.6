'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	User List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Class Admin_User_UserList
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSearchType, strSearchValue As String

        Try
            If Session("UserID") = "" Then
                Dim strScript As String = ""
                strScript = "self.parent.parent.location='../../../../../../" & ConfigurationManager.AppSettings("ServerName") & "/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
                'strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
                Page.ClientScript.RegisterStartupScript(Me.GetType, "PopupScript", "<script language=javascript>" & strScript & "</script>")
            End If

            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "User List"
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            btnCreate.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Create)

            If Not IsPostBack Then
                'constant

                'request
                strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "user_name")
                strSearchValue = Trim(Request.QueryString("search_value"))
                If strSearchValue <> "" Then
                    txtSearchValue.Text = strSearchValue
                End If

                'Prepare Recordset,Createobject as needed
                ddlSearchType.SelectedValue = strSearchType
                ddlSearchType_onChanged()
                BindGrid(ViewState("SortCol"))
            End If
        Catch ex As Exception
            ExceptionMsg("UserList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim objUserQuery As adm_User.clsUserQuery
        Dim dt As DataTable

        Try
            If intPassFlag <> 1 Or IsNothing(ViewState("UserList")) Then
                objUserQuery = New adm_User.clsUserQuery
                With objUserQuery
                    .clsProperties.search_type = Trim(ddlSearchType.SelectedValue)
                    Select Case ddlSearchType.SelectedValue
                        Case "Date"
                            .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                            .clsProperties.search_value_1 = Trim(Replace(txtSearchValue1.Text, "*", "%"))
                        Case "status" '"StatusID"   'HL:20070622
                            .clsProperties.search_value = Trim(ddlList.SelectedValue)
                        Case Else
                            .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                    End Select
                    .clsProperties.delete_flag = 1
                    .clsProperties.user_code = Trim(Session("UserCode"))   'HL:20070622
                    dt = .GetUserList
                End With
                ViewState("UserList") = dt
            Else
                dt = ViewState("UserList")
            End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With

            For Each COL As DataControlField In dgList.Columns
                Select Case COL.HeaderText.Trim.ToUpper
                    Case "DELETE"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Delete)
                    Case "EDIT"
                        COL.Visible = Report.GetAccessRight(ModuleID.ADM, SubModuleType.USR, SubModuleAction.Edit)
                End Select

            Next

        Catch ex As Exception
            ExceptionMsg("UserList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            objUserQuery = Nothing
        End Try
    End Sub

#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("UserList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("UserList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("UserList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "dgList"
    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand

        Try
            'Dim objUser As adm_User.clsUser
            Dim index As Integer = 0
            Dim row As GridViewRow

            If Integer.TryParse(e.CommandArgument, index) = True Then
                row = IIf(index >= 0 AndAlso index <= dgList.Rows.Count, dgList.Rows(index), Nothing)

                Select Case e.CommandName
                    Case "Delete"
                        'If Trim(lblStatusValue.Text) = "Active" Then
                        '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Please set status to In-Active before proceed.');</script>")
                        '    Exit Sub
                        'End If

                        'objUser = New adm_User.clsUser
                        'With objUser
                        '    .clsProperties.user_id = "0" 'drv("user_id")
                        '    .clsProperties.user_code = Trim(dgList.DataKeys(row.RowIndex).Value)
                        '    .Delete()
                        'End With

                        'Response.Redirect("UserList.aspx", False)

                    Case "Details"
                        Response.Redirect("UserDtl.aspx?action=EDIT&user_code=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                    Case "Edit"
                        Response.Redirect("UserDtl.aspx?action=EDIT&user_code=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                End Select

            End If

        Catch ex As Exception
            ExceptionMsg("UserList.dgList_RowCommand : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_RowDataBound
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim lblStatus As Label
        Dim imgEdit, imgDelete As ImageButton

        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                drv = CType(e.Row.DataItem, DataRowView)

                lblStatus = New Label
                lblStatus = CType(e.Row.FindControl("lblStatus"), Label)

                If IsDBNull(drv("active_flag")) Then
                    lblStatus.Text = ""
                    imgDelete = CType(e.Row.Cells(9).Controls(0), ImageButton)
                    imgEdit = CType(e.Row.Cells(10).Controls(0), ImageButton)
                    imgDelete.Visible = False
                    imgEdit.Visible = False
                Else
                    lblStatus.Text = IIf(drv("active_flag") = "1", "Active", "In-Active")
                End If
            End If

        Catch ex As Exception
            ExceptionMsg("UserList.dgList_RowDataBound : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("UserList.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
        Dim objUser As adm_User.clsUser
        Dim intCurrentPage As Integer

        Try
            dgList.EditIndex = -1

            objUser = New adm_User.clsUser
            With objUser
                .clsProperties.user_id = "0" 'drv("user_id")
                .clsProperties.user_code = Trim(dgList.DataKeys(e.RowIndex).Value)
                .Delete()
            End With

            'Response.Redirect("UserList.aspx", False)

            intCurrentPage = wuc_dgpaging.CurrentPageIndex
            dgList.PageIndex = 0

            BindGrid(ViewState("SortCol"))

            If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
            Else
                wuc_dgpaging.CurrentPageIndex = intCurrentPage
            End If
            wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
            dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

            BindGrid(ViewState("SortCol"), 1)

        Catch ex As Exception
            ExceptionMsg("UserList.dgList_RowDeleting : " + ex.ToString)
        Finally

        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing

    End Sub
#End Region

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
        Catch ex As Exception
            lblErr.Text = "UserList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnCreate_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            'Response.Redirect("UserCreate.aspx?search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
            Response.Redirect("UserDtl.aspx?action=CREATE&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

        Catch ex As Exception
            ExceptionMsg("UserList.btnCreate_Click : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            ddlSearchType_onChanged()
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "UserList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_onChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ddlSearchType_onChanged()
        Try
            'txtSearchValue.Text = ""
            'txtSearchValue1.Text = ""
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"

            lblSearchValue.Visible = False
            lblDot.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            'lblDate1.Visible = False
            'lblDate2.Visible = False

            Select Case ddlSearchType.SelectedValue
                Case "all"

                Case "Date"
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    lblTo.Visible = True
                    txtSearchValue.Visible = True
                    txtSearchValue.Text = ""
                    txtSearchValue.ReadOnly = True
                    txtSearchValue1.Visible = True
                    txtSearchValue1.ReadOnly = True
                    txtSearchValue1.Text = ""
                    btnSearch.Visible = True
                    'lblDate1.Visible = True
                    'lblDate2.Visible = True

                Case "status"
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True
                    txtSearchValue.Text = ""   'HL:20070622

                    'Get Status Record
                    ddlList.Items.Clear()
                    ddlList.Items.Add(New ListItem("Select", ""))
                    ddlList.Items.Add(New ListItem("Active", "1"))
                    ddlList.Items.Add(New ListItem("In-Active", "0"))

                Case Else
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "UserList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
