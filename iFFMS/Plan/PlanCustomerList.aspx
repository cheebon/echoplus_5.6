<%@ Page Language="vb" AutoEventWireup="false" Inherits="PlanCustomerList" CodeFile="PlanCustomerList.aspx.vb" %>
    
    
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="uc1" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->   
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300"></asp:ScriptManager>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <uc1:wuc_ctrlpanel id="Wuc_ctrlpanel" runat="server">
                                </uc1:wuc_ctrlpanel></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr">
                                    <ContentTemplate>
                                &nbsp;
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundBenealthTitle" colspan="3" height="5">
                                        </td>
                                    </tr>
                                    <%--<tr class="Bckgroundreport">
                                        <td colspan="3">
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <%--<tr class="Bckgroundreport">
                                                    <td >
                                                        &nbsp;</td>
                                                        <td>
                                                        
                                                        </td>
                                                </tr>--%>
                                                 <tr class="Bckgroundreport">
                                        <td colspan="3" style="height=10px">
                                            &nbsp;</td>
                                    </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td class="Bckgroundreport">
                                                      
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td align="left" colspan="2">
                                                                        <table>
                                                                        <tr>
                                                                        <td> <asp:Label ID="lblRoute" runat="server" CssClass="cls_label_header">Date :</asp:Label></td>
                                                                        <td> 
                                                                            <asp:Label ID="lblSelectedDate" runat="server" CssClass="cls_label_header"></asp:Label></td>
                                                                        </tr>
                                                                        </table>
                                                                           
                                                                            
                                                                          
                                                                            &nbsp;</td>
                                                                        <%--<td align="right">
                                                                            &nbsp;</td>--%>
                                                                    </tr>
                                                                    <tr>
                                                                    <td align="left" style="width: 123px; height: 10px;">
                                                                        <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" Width="60px" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 123px; height: 10px">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width: 123px; height: 10px">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                             <asp:UpdatePanel runat="server" ID="updateDG">
                                                             <ContentTemplate>
                                                              <ccGV:clsGridView ID="dgList" runat="server"
                                                                    AllowSorting="True" AutoGenerateColumns="False" Width="100%" FreezeHeader="True"
                                                                    GridHeight="250px" AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth="" RowHighlightColor="AntiqueWhite">
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="field1" HeaderText="Customer Code"/>
                                                                        <asp:BoundField DataField="field2" HeaderText="Customer Name"/>
                                                                        <asp:ButtonField Text="Add On" CommandName="AddOn" />
                                                                        <asp:ButtonField Text="Delete" commandname="DeleteSession" />
                                                                    </Columns>
                                                                    <FooterStyle CssClass="GridFooter" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                                                    <RowStyle CssClass="GridNormal" />
                                                                </ccGV:clsGridView>
                                                             </ContentTemplate>
                                                             </asp:UpdatePanel>
                                                                 
                                                               
                                                              
                                                         
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
