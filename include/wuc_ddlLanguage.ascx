<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlLanguage.ascx.vb" Inherits="include_wuc_ddlLanguage" %>
<asp:DropDownList id="ddlLanguageID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvLanguageID" ControlToValidate="ddlLanguageID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select Language."></asp:CompareValidator>
