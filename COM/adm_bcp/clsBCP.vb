
Option Explicit On
Imports System.ComponentModel
Imports System.Data

Public Class clsBCP
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMAConn As String = Web.HttpContext.Current.Session("ffma_conn")
    Public clsProperties As New clsProperties.clsBCP

#Region "BCP Template"
    Public Function GetBCPTemplateList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_TEMPLATE_LIST"
                '.addItem("search_type", clsProperties.search_type, cor_DB.clsDB.DataType.DBString)
                '.addItem("search_value", clsProperties.search_value, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetBCPTemplateList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteBCPTemplate(ByVal bcp_template_id As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_TEMPLATE_DELETE"
                .addItem("bcp_template_id", bcp_template_id, cor_DB.clsDB.DataType.DBDouble)
                .spDelete()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.Delete" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Function GetBCPTemplateDuplicate(strBCPTemplateName As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_TEMPLATE_DUPLICATE"
                .addItem("bcp_template_name", strBCPTemplateName, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetBCPTemplateDuplicate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub InsertBCPTemplate(strBCPTemplateName As String, strBCPTemplateDesc As String, strInSeq As String, strUserID As String)
        Dim objDB As cor_DB.clsDB
        Dim drRow As DataRow = Nothing

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_TEMPLATE_CREATE"
                .addItem("bcp_template_name", strBCPTemplateName, cor_DB.clsDB.DataType.DBString)
                .addItem("bcp_template_desc", strBCPTemplateDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("in_seq", strInSeq, cor_DB.clsDB.DataType.DBString)
                .addItem("creator_user_id", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spInsert()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.Create" & ex.ToString))
        Finally
            'objAccessRightDtl = Nothing
            objDB = Nothing
        End Try
    End Sub

    Public Sub UpdateBCPTemplate(strBCPTemplateID As String, strBCPTemplateDesc As String, strInSeq As String, strUserID As String)
        Dim objDB As cor_DB.clsDB

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_TEMPLATE_UPDATE"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("bcp_template_desc", strBCPTemplateDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("in_seq", strInSeq, cor_DB.clsDB.DataType.DBString)
                .addItem("changed_user_id", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With

            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.UpdateBCPTemplate" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "BCP Steps"
    Public Function GetBCPStepList(ByVal strBCPTemplateID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_STEP_LIST"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetBCPStepList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetModuleDDL() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_MODULE"
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetModuleDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetStepDtl(ByVal strBCPTemplateID As String, ByVal strStepNo As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_STEP_DTL"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBString)
                .addItem("step_no", strStepNo, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetStepDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetStepModuleDtl(ByVal strBCPTemplateID As String, ByVal strStepNo As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_STEP_MODULE_DTL"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBString)
                .addItem("step_no", strStepNo, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetStepModuleDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub InsertBCPStepModule(ByVal strBCPTemplateID As String, ByVal strStepName As String, ByVal strStepDesc As String, ByVal strMandStep As String, ByVal strModuleIdList As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Dim drRow As DataRow = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_STEP_CREATE"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBInt)
                .addItem("step_name", strStepName, cor_DB.clsDB.DataType.DBString)
                .addItem("step_desc", strStepDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("mand_step", strMandStep, cor_DB.clsDB.DataType.DBString)
                .addItem("module_list", strModuleIdList, cor_DB.clsDB.DataType.DBString)
                .addItem("creator_user_id", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spInsert()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.InsertBCPStepModule" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub UpdateBCPStepModule(ByVal strBCPTemplateID As String, ByVal strStepNo As String, ByVal strStepName As String, ByVal strStepDesc As String, ByVal strMandStep As String, ByVal strModuleIdList As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Dim drRow As DataRow = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_STEP_UPDATE"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBInt)
                .addItem("step_no", strStepNo, cor_DB.clsDB.DataType.DBInt)
                .addItem("step_name", strStepName, cor_DB.clsDB.DataType.DBString)
                .addItem("step_desc", strStepDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("mand_step", strMandStep, cor_DB.clsDB.DataType.DBString)
                .addItem("module_list", strModuleIdList, cor_DB.clsDB.DataType.DBString)
                .addItem("changed_user_id", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.UpdateBCPStepModule" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub DeleteBCPStepModule(ByVal strBCPTemplateID As String, ByVal strStepNo As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Dim drRow As DataRow = Nothing
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_STEP_DELETE"
                .addItem("bcp_template_id", strBCPTemplateID, cor_DB.clsDB.DataType.DBInt)
                .addItem("step_no", strStepNo, cor_DB.clsDB.DataType.DBInt)
                .addItem("changed_user_id", strUserID, cor_DB.clsDB.DataType.DBDouble)
                .spUpdate()
            End With

        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.DeleteBCPStepModule" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "FieldForce Cust Mapping"
    Public Function GetBCPTemplateDDL() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_BCP_TEMPLATE_DDL"
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_bcp.clsBCP.GetBCPTemplateDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetFieldForceBCPTemplate(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_SALESREP_CUST_BCP_TEMPLATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CONT_CODE", strContCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
#End Region

#Region "SFMS Configuration"
    Public Function GetSFMSCatCodeDDL(ByVal strTeamCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_SFMS_CONFIG_CAT_CODE_DDL"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSFMSConfigList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_SFMS_CONFIG_LIST"
                .addItem("SEARCH_TYPE", strSearchType, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateSFMSConfig(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strSFMSCatCode As String, _
                                         ByVal strCustCode As String, ByVal strCustClass As String, ByVal strRegion As String, ByVal strDistchannel As String, _
                                         ByVal strCGCode As String, ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String _
                                         , ByVal strCGCode4 As String, ByVal strCGCode5 As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_SFMS_CONFIG_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SFMS_CAT_CODE", strSFMSCatCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION", strRegion, cor_DB.clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteSFMSConfig(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strSFMSCatCode As String, ByVal strCustCode As String, ByVal strCustClass As String, _
                                    ByVal strRegion As String, ByVal strDistchannel As String, ByVal strCGCode As String, ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String, _
                                    ByVal strCGCode4 As String, ByVal strCGCode5 As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_SFMS_CONFIG_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SFMS_CAT_CODE", strSFMSCatCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION", strRegion, cor_DB.clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

#Region "MSS Configuration"
    Public Function GetMSSTitleCodeDDL(ByVal strTeamCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_MSS_CONFIG_TITLE_CODE_DDL"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetMSSConfigList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_MSS_CONFIG_LIST"
                .addItem("SEARCH_TYPE", strSearchType, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function CreateMSSConfig(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strTitleCode As String, _
                                         ByVal strCustCode As String, ByVal strCustClass As String, ByVal strRegion As String, ByVal strDistchannel As String, _
                                         ByVal strCGCode As String, ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String _
                                         , ByVal strCGCode4 As String, ByVal strCGCode5 As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_MSS_CONFIG_CREATE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION", strRegion, cor_DB.clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub DeleteMSSConfig(ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strTitleCode As String, ByVal strCustCode As String, ByVal strCustClass As String, _
                                    ByVal strRegion As String, ByVal strDistchannel As String, ByVal strCGCode As String, ByVal strCGCode1 As String, ByVal strCGCode2 As String, ByVal strCGCode3 As String, _
                                    ByVal strCGCode4 As String, ByVal strCGCode5 As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strFFMAConn
                .CmdText = "SPP_MST_MSS_CONFIG_DELETE"
                .addItem("TEAM_CODE", strTeamCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, cor_DB.clsDB.DataType.DBString)
                .addItem("TITLE_CODE", strTitleCode, cor_DB.clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CUST_CLASS", strCustClass, cor_DB.clsDB.DataType.DBString)
                .addItem("REGION", strRegion, cor_DB.clsDB.DataType.DBString)
                .addItem("DISTCHANNEL", strDistchannel, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE", strCGCode, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE1", strCGCode1, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE2", strCGCode2, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE3", strCGCode3, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE4", strCGCode4, cor_DB.clsDB.DataType.DBString)
                .addItem("CGCODE5", strCGCode5, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", Web.HttpContext.Current.Session("UserID").ToString, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
#End Region

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.AccessRightQueryID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
