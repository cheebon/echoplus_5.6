'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	15/05/2007
'	Purpose	    :	Class to build Glossary Query from database.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB


Public Class clsGlossary
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("echoplus_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsGlossary"
        End Get
    End Property

    Public Function GetGlossaryList(ByVal strSubModuleID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsGlossaryDB As clsDB
        Try
            clsGlossaryDB = New clsDB
            With clsGlossaryDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_GETGLOSSARYLISTBYSUBMODULEID"
                .addItem("SUBMODULE_ID", strSubModuleID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetGlossaryList :" & ex.Message))
        Finally
            clsGlossaryDB = Nothing
        End Try
    End Function

    Public Function GetReportDefinition(ByVal strSubModuleID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsRDDB As clsDB
        Try
            clsRDDB = New clsDB
            With clsRDDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_GETREPORTDEFINISIONBYSUBMODULEID"
                .addItem("SUBMODULE_ID", strSubModuleID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetReportDefinition :" & ex.Message))
        Finally
            clsRDDB = Nothing
        End Try
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

End Class
