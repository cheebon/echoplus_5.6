Imports System.Data

Partial Class iFFMA_Contact_ContactAcctPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property ContCode() As String
        Get
            Return Trim(hdContCode.Value)
        End Get
        Set(ByVal value As String)
            hdContCode.Value = value
        End Set
    End Property

    Public Property CustCode() As String
        Get
            Return Trim(hdCustCode.Value)
        End Get
        Set(ByVal value As String)
            hdCustCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Dim dtPosition As DataTable
        Dim dtHospRank As DataTable
        Dim dtDepartment As DataTable
        Dim clsCommon As New mst_Common.clsDDL

        Try
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)
            If ddlStatus IsNot Nothing Then
                ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS_CODE")), "", DT.Rows(0)("STATUS_CODE"))
            End If

            Dim ddlPatientCap As DropDownList = CType(DetailsView1.FindControl("ddlPatientCap"), DropDownList)
            If ddlPatientCap IsNot Nothing Then
                ddlPatientCap.SelectedValue = IIf(IsDBNull(DT.Rows(0)("PATIENT_CAP")), "", DT.Rows(0)("PATIENT_CAP"))
            End If

            Dim ddlPosition As DropDownList = CType(DetailsView1.FindControl("ddlPosition"), DropDownList)
            If ddlPosition IsNot Nothing Then
                dtPosition = clsCommon.GetPositionDDL
                With ddlPosition
                    .Items.Clear()
                    .DataSource = dtPosition.DefaultView
                    .DataTextField = "POSITION_NAME"
                    .DataValueField = "POSITION_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("POSITION_CODE")), "", DT.Rows(0)("POSITION_CODE"))
                End With
            End If

            Dim ddlHospRank As DropDownList = CType(DetailsView1.FindControl("ddlHospRank"), DropDownList)
            If ddlHospRank IsNot Nothing Then
                dtHospRank = clsCommon.GetHospRankDDL
                With ddlHospRank
                    .Items.Clear()
                    .DataSource = dtHospRank.DefaultView
                    .DataTextField = "HOSP_RANK_NAME"
                    .DataValueField = "HOSP_RANK_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("HOSP_RANK_CODE")), "", DT.Rows(0)("HOSP_RANK_CODE"))
                End With
            End If
            
            Dim ddlDepartment As DropDownList = CType(DetailsView1.FindControl("ddlDepartment"), DropDownList)
            If ddlDepartment IsNot Nothing Then
                dtDepartment = clsCommon.GetDepartmentDDL
                With ddlDepartment
                    .Items.Clear()
                    .DataSource = dtDepartment.DefaultView
                    .DataTextField = "DEPARTMENT_NAME"
                    .DataValueField = "DEPARTMENT_CODE"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", ""))
                    .SelectedIndex = 0
                    .SelectedValue = IIf(IsDBNull(DT.Rows(0)("DEPARTMENT_CODE")), "", DT.Rows(0)("DEPARTMENT_CODE"))
                End With
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsContact As New mst_Contact.clsContact

            DT = clsContact.GetContAcctDetails(ContCode, CustCode)
            If DT.Rows.Count > 0 Then
                If DT.Rows(0)("LOCK_FLAG") <> "0" Then
                    RaiseEvent PopLockAlert()
                    Exit Sub
                End If
            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Dim lblContCode As Label = CType(DetailsView1.FindControl("lblContCode"), Label)
            lblContCode.Text = ContCode

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            With (wuc_CustSearch)
                .ResetPage()
                .BindDefault()
                .Show()
            End With
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSelectCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.SelectButton_Click
        Try
            Dim txtCustCode As TextBox = CType(DetailsView1.FindControl("txtCustCode"), TextBox)
            txtCustCode.Text = wuc_CustSearch.CustCode

            updPnlMaintenance.Update()
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnCloseCustSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_CustSearch.CloseButton_Click
        Try
            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtCustCode As TextBox = CType(DetailsView1.FindControl("txtCustCode"), TextBox)
            Dim lblCustCode As Label = CType(DetailsView1.FindControl("lblCustCode"), Label)
            Dim ddlStatus As DropDownList = CType(DetailsView1.FindControl("ddlStatus"), DropDownList)
            Dim ddlPosition As DropDownList = CType(DetailsView1.FindControl("ddlPosition"), DropDownList)
            Dim txtPacCode As TextBox = CType(DetailsView1.FindControl("txtPacCode"), TextBox)
            Dim txtHospAbbv As TextBox = CType(DetailsView1.FindControl("txtHospAbbv"), TextBox)
            Dim ddlHospRank As DropDownList = CType(DetailsView1.FindControl("ddlHospRank"), DropDownList)
            Dim ddlDepartment As DropDownList = CType(DetailsView1.FindControl("ddlDepartment"), DropDownList)
            Dim txtLoc As TextBox = CType(DetailsView1.FindControl("txtLoc"), TextBox)
            Dim txtTelNo1 As TextBox = CType(DetailsView1.FindControl("txtTelNo1"), TextBox)
            Dim txtTelNo2 As TextBox = CType(DetailsView1.FindControl("txtTelNo2"), TextBox)
            Dim txtFaxNo As TextBox = CType(DetailsView1.FindControl("txtFaxNo"), TextBox)
            Dim txtPatientCount As TextBox = CType(DetailsView1.FindControl("txtPatientCount"), TextBox)
            Dim txtBedCount As TextBox = CType(DetailsView1.FindControl("txtBedCount"), TextBox)
            Dim ddlPatientCap As DropDownList = CType(DetailsView1.FindControl("ddlPatientCap"), DropDownList)
            Dim txtPracticeSize As TextBox = CType(DetailsView1.FindControl("txtPracticeSize"), TextBox)

            txtPatientCount.Text = IIf(Trim(txtPatientCount.Text) = "", 0, Trim(txtPatientCount.Text))
            txtBedCount.Text = IIf(Trim(txtBedCount.Text) = "", 0, Trim(txtBedCount.Text))

            Dim clsContact As New mst_Contact.clsContact

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                
                clsContact.UpdateContAcct(ContCode, Trim(lblCustCode.Text), Trim(ddlStatus.SelectedValue), _
                                        Trim(ddlPosition.SelectedValue), Trim(txtPacCode.Text), Trim(txtHospAbbv.Text), _
                                        Trim(ddlHospRank.SelectedValue), Trim(ddlDepartment.SelectedValue), Trim(txtLoc.Text), _
                                        Trim(txtTelNo1.Text), Trim(txtTelNo2.Text), Trim(txtFaxNo.Text), Trim(txtPatientCount.Text), _
                                        Trim(txtBedCount.Text), Trim(ddlPatientCap.SelectedValue), Trim(txtPracticeSize.Text))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim strCustCode As String = ""

                If txtCustCode.Text = "" Then
                    lblInfo.Text = "Customer Code is required!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                End If

                DT = clsContact.CreateContAcct(ContCode, Trim(txtCustCode.Text), Trim(ddlStatus.SelectedValue), _
                                        Trim(ddlPosition.SelectedValue), Trim(txtPacCode.Text), Trim(txtHospAbbv.Text), _
                                        Trim(ddlHospRank.SelectedValue), Trim(ddlDepartment.SelectedValue), Trim(txtLoc.Text), _
                                        Trim(txtTelNo1.Text), Trim(txtTelNo2.Text), Trim(txtFaxNo.Text), Trim(txtPatientCount.Text), _
                                        Trim(txtBedCount.Text), Trim(ddlPatientCap.SelectedValue), Trim(txtPracticeSize.Text))

                If DT.Rows.Count > 0 Then
                    strCustCode = DT.Rows(0)("CUST_CODE")
                End If

                If strCustCode = "" Then
                    lblInfo.Text = "Customer Code already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    CustCode = Trim(txtCustCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    strCustCode = txtCustCode.Text.ToString
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

