'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Country User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlCountry
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Private strUserCode As String
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlCountryID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    'Public Property SelectedValue() As Long
    '    Get
    '        If ddlCountryID.SelectedValue = String.Empty Then
    '            Return 0
    '        End If
    '        Return Long.Parse(ddlCountryID.SelectedValue)
    '    End Get
    '    Set(ByVal Value As Long)
    '        Dim checkExist As ListItem
    '        checkExist = ddlCountryID.Items.FindByValue(Value.ToString())
    '        If checkExist Is Nothing Then
    '            ddlCountryID.SelectedValue = "0"
    '        Else
    '            ddlCountryID.SelectedValue = Value.ToString()
    '        End If
    '    End Set
    'End Property

    Public Property SelectedValue() As String
        Get
            If ddlCountryID.SelectedValue = String.Empty Then
                Return 0
            End If
            'Return Long.Parse(ddlCountryID.SelectedValue)
            Return ddlCountryID.SelectedValue
        End Get
        Set(ByVal Value As String)
            Dim checkExist As ListItem
            checkExist = ddlCountryID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlCountryID.SelectedValue = "0"
            Else
                ddlCountryID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlCountryID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlCountryID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlCountryID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlCountryID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlCountryID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlCountryID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlCountryID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlCountryID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlCountryID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvCountryID.Visible 'cfvCountryID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvCountryID.Enabled = Value
            cfvCountryID.Visible = Value
        End Set
    End Property

    Public Property UserCode() As String
        Get
            Return strUserCode
        End Get
        Set(ByVal Value As String)
            strUserCode = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            If ddlCountryID.Items.Count <= 0 Then
                'Get Country List
                objCommonQuery = New cor_Common.clsCommonQuery
                With objCommonQuery
                    .clsProperties.UserCode = strUserCode
                    dt = .GetCountryList
                End With
                objCommonQuery = Nothing

                ddlCountryID.Items.Clear()
                ddlCountryID.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlCountryID.Items.Add(New ListItem(Trim(drRow("country_name")), Trim(drRow("country_id")) & "@" & Trim(drRow("country_code"))))
                    Next
                    ddlCountryID.SelectedIndex = 1
                End If
            End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlCountry.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


