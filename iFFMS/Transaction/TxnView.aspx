<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnView.aspx.vb" Inherits="iFFMS_Transaction_TxnView" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transaction View</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmtxnview" runat="server" >
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <asp:UpdatePanel runat="server" ID="UpdatePage" RenderMode="Block" UpdateMode="Conditional">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
                    <tr align="center">
                        <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                            <fieldset class="" style="width: 98%;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="BckgroundInsideContentLayout" align="left">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                                <tr>
                                                    <td style="width: 100%">
                                                        <uc1:wuc_lblHeader ID="wuc_lblHeader" runat="server"></uc1:wuc_lblHeader>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="BckgroundBenealthTitle">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border: solid 1px black; background-color: Gray">
                                                        <input type="button" runat="server" id="btnback" value="<< Back" onclick="HideElement('DetailBar');ReloadIframe('ContentBarIframe');ShowElement('ContentBar');MaximiseFrameHeight('ContentBarIframe');"
                                                            class="cls_button" style="width:80px" />
                                                       <asp:Button ID="btncancel" runat="server" Text="Cancel Txn." CssClass="cls_button" OnClientClick="var agree=confirm('Are you sure you wish to continue?');if(agree)return true;else return false;"
                                                            Width="80px"  Visible=false /><%-- 
                                                        <asp:Button ID="btnkiv" runat="server" Text="KIV" CssClass="cls_button" OnClientClick="var agree=confirm('Are you sure you wish to continue?');if(agree)return true;else return false;"
                                                            Width="80px" />--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" class="Bckgroundreport">
                                                        <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 95%;">
                                                                            <div style="width: 800px; padding-left: 10px; padding-top: 10px">
                                                                                <span style="float: left; width: 400px">
                                                                                    <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                        Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                        <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                        <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                        <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                            Width="35%" Wrap="False" />
                                                                                    </asp:DetailsView>
                                                                                </span><span style="float: left; width: 400px">
                                                                                    <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                                                                        Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                                                                        <RowStyle CssClass="cls_DV_Row_MST" />
                                                                                        <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                                                                        <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                                                                            Width="35%" Wrap="False" />
                                                                                    </asp:DetailsView>
                                                                                </span>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" style="width: 95%; float: left;">
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                                                        Width="98%" FreezeHeader="True" GridHeight="360" AddEmptyHeaders="0" CellPadding="2"
                                                                        CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                        ShowFooter="True" AllowPaging="false" PagerSettings-Visible="false"> 
                                                                            </ccGV:clsGridView>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr class="Bckgroundreport">
                                                    <td style="height: 5px">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
