﻿Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdvQuesDisabledPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property

    Public Property SubQuesCode() As String
        Get
            Return Trim(hdSubQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdSubQuesCode.Value = value
        End Set
    End Property

#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub BindDetailsView()
        Try
            LoadDDL()

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadDDL()
        Dim dtSubQuesCode As DataTable

        Try
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv

            dtSubQuesCode = clsMSSAdv.GetMssQuesDisabledExcludeList(TeamCode, TitleCode, QuesCode, SubQuesCode, ddlCriteriaValue.SelectedValue)

            With ddlDisableCode
                .Items.Clear()
                .DataSource = dtSubQuesCode.DefaultView
                .DataTextField = "SUB_QUES_CODE"
                .DataValueField = "SUB_QUES_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            ddlCriteriaValue.SelectedIndex = 0
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strCriteriaValue As String = ddlCriteriaValue.SelectedValue
            Dim strDisableCode As String = ddlDisableCode.SelectedValue
            
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv

            Dim DT As DataTable
            Dim isDuplicate As Integer = 0

            DT = clsMSSAdv.CreateMSSQuesDisabled(TeamCode, TitleCode, QuesCode, SubQuesCode, Trim(ddlCriteriaValue.SelectedValue), Trim(ddlDisableCode.SelectedValue))
            If DT.Rows.Count > 0 Then
                isDuplicate = DT.Rows(0)("IS_DUPLICATE")
            End If

            If isDuplicate = 1 Then
                lblInfo.Text = "The mapping already exists!"
                Show()
                Exit Sub
            Else
                lblInfo.Text = "The record is successfully created."
            End If

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub ddlCriteriaValue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCriteriaValue.SelectedIndexChanged
    '    Try
    '        BindDetailsView()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class





