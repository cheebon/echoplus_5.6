'************************************************************************
'	Author	    :	Lim Xinyao
'	Date	    :	14/04/2007
'	Purpose	    :	Connection String List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data

Partial Public Class Admin_ConnectionString_ConnectionStringList
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSearchType, strSearchValue As String

        Try
            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblheader
                .Title = "Connection List"
                .DataBind()
                .Visible = True
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'constant

                'request
                strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "user_name")
                strSearchValue = Trim(Request.QueryString("search_value"))
                If strSearchValue <> "" Then
                    txtSearchValue.Text = strSearchValue
                End If

                'Prepare Recordset,Createobject as needed
                ddlSearchType.SelectedValue = strSearchType
                ddlSearchType_onChanged()
                BindGrid(ViewState("SortCol"))
            End If
        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub BindGrid(ByVal SortExpression As String, Optional ByVal intPassFlag As Integer = 0)
        Dim objUserQuery As adm_Comm_Svr_Setting.clsCommSvrQuery
        Dim dt As DataTable

        Try
            If intPassFlag <> 1 Or IsNothing(ViewState("ConnectionStringList")) Then
                objUserQuery = New adm_Comm_Svr_Setting.clsCommSvrQuery
                With objUserQuery
                    Select Case ddlSearchType.SelectedValue
                        Case "comm_svr_setting_code"
                            .clsProperties.strCommCode = txtSearchValue.Text.Trim & "%"
                        Case "comm_svr_setting_name"
                            .clsProperties.strCommName = txtSearchValue.Text.Trim & "%"
                        Case "status"
                            .clsProperties.strActiveFlag = ddlList.SelectedValue
                        Case Else
                            .clsProperties.strCommID = "%"
                    End Select
                    dt = .GetCommSvrList
                End With
                ViewState("ConnectionStringList") = dt
            Else
                dt = ViewState("ConnectionStringList")
            End If
            dt.DefaultView.Sort = SortExpression
            If dt.Rows.Count < 1 Then
                dt.Rows.Add(dt.NewRow)
            End If

            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.DataSource = Nothing
            dgList.DataBind()
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()
            ChangeActive()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With

        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
            objUserQuery = Nothing
        End Try
    End Sub

#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            Wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"), 1)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

#Region "dgList"
    '---------------------------------------------------------------------
    ' Procedure 	    : 	dgList_RowCommand
    ' Purpose	        :	
    ' Calling Methods   :   
    ' Page              :	
    '----------------------------------------------------------------------
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim objUser As adm_Comm_Svr_Setting.clsCommSvr
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgList.Rows(index)

        Try
            Select Case e.CommandName
                Case "Delete"
                    'If Trim(lblStatusValue.Text) = "Active" Then
                    '    Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Please set status to In-Active before proceed.');</script>")
                    '    Exit Sub
                    'End If

                    objUser = New adm_Comm_Svr_Setting.clsCommSvr
                    With objUser
                        .clsProperties.strCommID = Trim(dgList.DataKeys(row.RowIndex).Value)
                        .clsProperties.strSyncOperation = "D"
                        .clsProperties.strActiveFlag = "0"
                        .Suspend()
                    End With

                    Response.Redirect("ConnectionStringList.aspx", False)

                Case "Details"
                    Response.Redirect("ConnectionStringDtl.aspx?comm_svr_setting_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

                Case "Edit"
                    Response.Redirect("ConnectionStringEdit.aspx?comm_svr_setting_id=" & Trim(dgList.DataKeys(row.RowIndex).Value) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)

            End Select

        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.dgList_RowCommand : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_RowDataBound
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Dim drv As DataRowView
        Dim lblStatus As Label
        Dim imgEdit, imgDelete As ImageButton

        Try
            If dgList.Rows.Count > 0 Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    drv = CType(e.Row.DataItem, DataRowView)

                    lblStatus = New Label
                    lblStatus = CType(e.Row.FindControl("lblStatus"), Label)

                    If IsDBNull(drv("active_flag")) Then
                        lblStatus.Text = ""
                        imgDelete = CType(e.Row.Cells(5).Controls(0), ImageButton)
                        imgEdit = CType(e.Row.Cells(6).Controls(0), ImageButton)
                        imgDelete.Visible = False
                        imgEdit.Visible = False
                    Else
                        lblStatus.Text = IIf(drv("active_flag") = "1", "Active", "In-Active")
                    End If
                End If
                'dgList.Rows(e.Row.RowIndex).Cells(1).Text = IIf(e.Row.Cells(1).Text = "1", "Active", "In-Active")
            End If
        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.dgList_RowDataBound : " + ex.ToString)
        End Try
    End Sub

    Private Sub ChangeActive()
        Dim intloop As Integer = 0
        Do While intloop < dgList.Rows.Count
            If dgList.Rows(intloop).Cells(1).Text = "1" Then
                dgList.Rows(intloop).Cells(1).Text = "Active"
            Else
                dgList.Rows(intloop).Cells(1).Text = "In-Active"
            End If
            'dgList.Rows(intloop).Cells(1).Text = IIf(dgList.Rows(intloop).Cells(1).Text = "1", "Active", "In-Active")
            intloop = intloop + 1
        Loop
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub dgList_SortCommand
    ' Purpose	        :	This Sub manipulate the record sorting event
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub dgList_SortCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.dgList_SortCommand : " + ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles dgList.RowDeleting
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
    End Sub
#End Region

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSearchType.SelectedIndexChanged
        Try
            ddlSearchType_onChanged()
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "ConnectionStringList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_onChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub ddlSearchType_onChanged()
        Try
            'txtSearchValue.Text = ""
            'txtSearchValue1.Text = ""
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"

            lblSearchValue.Visible = False
            lblDot.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            'lblDate1.Visible = False
            'lblDate2.Visible = False

            Select Case ddlSearchType.SelectedValue
                Case "all"

                Case "Date"
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    lblTo.Visible = True
                    txtSearchValue.Visible = True
                    txtSearchValue.Text = ""
                    txtSearchValue.ReadOnly = True
                    txtSearchValue1.Visible = True
                    txtSearchValue1.ReadOnly = True
                    txtSearchValue1.Text = ""
                    btnSearch.Visible = True
                    'lblDate1.Visible = True
                    'lblDate2.Visible = True

                Case "status"
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True

                    'Get Status Record
                    ddlList.Items.Clear()
                    ddlList.Items.Add(New ListItem("Select", ""))
                    ddlList.Items.Add(New ListItem("Active", "1"))
                    ddlList.Items.Add(New ListItem("In-Active", "0"))

                Case Else
                    lblSearchValue.Visible = True
                    lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "ConnectionStringList.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#Region "Button operation"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
        Catch ex As Exception
            lblErr.Text = "ConnectionStringList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnCreate_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            Response.Redirect("ConnectionStringCreate.aspx?search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text), False)
        Catch ex As Exception
            ExceptionMsg("ConnectionStringList.btnCreate_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("../Menu.aspx", False)
    End Sub
#End Region
End Class