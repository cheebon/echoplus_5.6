﻿Imports System.Data
Partial Class iFFMR_Customize_StkConsump_StkConsumpByQuadMth 
    Inherits System.Web.UI.Page
#Region "Local Variable"

    Private intPageSize As Integer
    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollector2 As ListItemCollection

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMR_Customize_StkConsump_StkConsumpByBiMth"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub
#End Region

#End Region

#Region "Standard Template"
    Public ReadOnly Property PageName() As String
        Get
            Return "SalesrepPrdFreq.aspx"
        End Get
    End Property

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.STK_CONSUMP_BY_QUADMONTH)
            .DataBind()
            .Visible = True
        End With

        'Call Panel
        With wuc_ctrlpanel
            .SubModuleID = SubModuleType.STK_CONSUMP_BY_QUADMONTH
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then

            TimerControl1.Enabled = True
        End If



    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            LoadLsbTeam()
            LoadddlYear()
            RefreshDatabinding()
        End If

        TimerControl1.Enabled = False
    End Sub

#Region "DGLIST"
    Private Sub RefreshDatabinding()
        Dim dtCurrenttable As Data.DataTable = Nothing 'CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrenttable = GetRecList()
            ViewState("strSortExpression") = Nothing


            If dtCurrenttable Is Nothing Then
                dtCurrenttable = New DataTable
            Else
                PreRenderMode(dtCurrenttable)
                If dtCurrenttable.Rows.Count = 0 Then
                    dtCurrenttable.Rows.Add(dtCurrenttable.NewRow())
                End If
            End If

            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
            dgList.PageIndex = 0


        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then dgList_Init(DT)
            Cal_ItemFigureCollector(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_SFStkConsumpByQuadMth.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_SFStkConsumpByQuadMth.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_SFStkConsumpByQuadMth.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFStkConsumpByQuadMth.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFStkConsumpByQuadMth.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName


                        Dim strUrlFields(0) As String
                        strUrlFields(0) = ""
                        'strUrlFields(0) = "ACTUAL_CALL"
                        'strUrlFields(1) = "SALESREP_CODE"
                        'strUrlFields(2) = "CUST_CODE"
                        'strUrlFields(3) = "CONT_CODE" 

                        Dim strYear As String = ddlYear.SelectedValue
                        Dim strQuadMonth As String = ColumnName

                        Dim strUrlFormatString As String
                        strUrlFormatString = (" parent.document.getElementById('DetailBarIframe').src='StkConsumpByQuadMthDtl.aspx?" & _
                                            "year=" & strYear & "&Quadmonth=" & strQuadMonth & " '")

                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        dgColumn.Target = "_self"
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_SFStkConsumpByQuadMth.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_SFStkConsumpByQuadMth.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_SFStkConsumpByQuadMth.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    For Each TC As TableCell In e.Row.Cells
                        If TC.Controls.Count > 0 Then
                            Dim HYL As HyperLink = CType(TC.Controls(0), HyperLink)
                            If HYL IsNot Nothing Then
                                Dim strlink As String = HYL.NavigateUrl
                                HYL.NavigateUrl = "#"
                                HYL.Target = String.Empty
                                HYL.Attributes.Add("onclick", strlink + ";resizeLayout3();")
                            End If
                        End If
                    Next
                Case DataControlRowType.Footer

                    Dim iIndex As Integer
                    For Each li As ListItem In licItemFigureCollector2
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex >= 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_SFStkConsumpByQuadMth.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
            End Select

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try

    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        licItemFigureCollector2 = New ListItemCollection

        Dim strColumnName As String
        Dim liColumnField As ListItem

        If DT.Columns.Contains("ACHIEVED_IND") Then
            licItemFigureCollector.Add(New ListItem("TOTAL", 0))
            licItemFigureCollector.Add(New ListItem("OVER", 0))
            licItemFigureCollector.Add(New ListItem("NP", 0))
            licItemFigureCollector.Add(New ListItem("ROWCOUNT", 0))

            For Each DR As DataRow In DT.Rows
                Dim strValue As String = CStr(DR("ACHIEVED_IND"))
                Select Case strValue.ToUpper
                    Case "Y"
                        licItemFigureCollector.FindByText("TOTAL").Value = licItemFigureCollector.FindByText("TOTAL").Value + 1
                    Case "Y-OVER"
                        licItemFigureCollector.FindByText("TOTAL").Value = licItemFigureCollector.FindByText("TOTAL").Value + 1
                        licItemFigureCollector.FindByText("OVER").Value = licItemFigureCollector.FindByText("OVER").Value + 1
                    Case "NP"
                        licItemFigureCollector.FindByText("NP").Value = licItemFigureCollector.FindByText("NP").Value + 1
                End Select

                Dim strValue2 As String = CStr(DR("PLANNED_CALL"))
                If strValue2 > 0 Then
                    licItemFigureCollector.FindByText("ROWCOUNT").Value = licItemFigureCollector.FindByText("ROWCOUNT").Value + 1
                End If

            Next


            For Each DC As DataColumn In DT.Columns
                strColumnName = DC.ColumnName.ToUpper
                If (strColumnName = "PLANNED_CALL" OrElse strColumnName = "ACTUAL_CALL") Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector2.Add(liColumnField)
                End If
            Next

            licItemFigureCollector2.Add(New ListItem("ACHIEVED_IND", 0))

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector2
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next

            licItemFigureCollector2.FindByText("ACHIEVED_IND").Value = Format(ConvertToDouble(DIVISION(licItemFigureCollector.FindByText("TOTAL"), licItemFigureCollector.FindByText("ROWCOUNT")) * 100), "0.00")
        End If

    End Sub

    Private Function GetRecList() As Data.DataTable
        Dim DT As DataTable = Nothing
        Try

            Dim clsStkConsump As New rpt_Customize.clsStkConsump
            Dim strSalesrepCode As String = GetItemsInString(lsbSelectedSalesrep)
            Dim strYear As String = ddlYear.SelectedValue
            Dim strPrdList As String = GetItemsInString(lsbSelectedProduct)
            Dim strUserId As String = Portal.UserSession.UserID

            Portal.UserSession.CuzRptSrList = strSalesrepCode
            Portal.UserSession.CuzRptTeamList = GetItemsInString(lsbSelectedTeam)
            Portal.UserSession.CuzRptPrdList = strPrdList

            DT = clsStkConsump.GetStkConsumpByQuadMonthly(strYear, strSalesrepCode, strPrdList, strUserId)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT
            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        ViewState("strSortExpression") = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
    End Sub

    Private Sub LoadddlYear()
        Try
            With ddlYear
                .Items.Clear()
                .Items.Insert(0, New ListItem(Year(Now()) - 1, Year(Now()) - 1))
                .Items.Insert(1, New ListItem(Year(Now()), Year(Now())))
                ' .Items.Insert(2, New ListItem(Year(Now()) + 1, Year(Now()) + 1))
                .SelectedIndex = 1
                .DataBind()
            End With
        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadddlYear : " & ex.ToString)
        End Try
    End Sub

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            ClearLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            ClearLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            ClearLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            ClearLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            LoadLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            LoadLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
            LoadLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
            LoadLsbProduct()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Product"

    Private Sub LoadLsbProduct()
        Try
            lsbProduct.Items.Clear()
            lsbSelectedProduct.Items.Clear()

            Dim clsStkConsump As New rpt_Customize.clsStkConsump

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strSalesrepList As String

            strSalesrepList = GetItemsInString(lsbSelectedSalesrep)

            With lsbProduct
                .DataSource = clsStkConsump.GetStkConsumpPrdList(strSalesrepList, strPrincipalID, strUserID)
                .DataTextField = "PRD_NAME"
                .DataValueField = "PRD_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbProduct : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddProduct.Click
        Try
            AddToListBox(lsbProduct, lsbSelectedProduct)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddProduct_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveProduct.Click
        Try
            AddToListBox(lsbSelectedProduct, lsbProduct)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveProduct_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllProduct.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbProduct.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbProduct, lsbSelectedProduct)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllProduct_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllProduct.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedProduct.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedProduct, lsbProduct)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllProduct_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ClearLsbProduct()
        lsbProduct.Items.Clear()
        lsbSelectedProduct.Items.Clear()
    End Sub
#End Region

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String

        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer
        Try

            For intIndex = 0 To lsbFrom.Items.Count - 1
                liToAdd = lsbFrom.Items(intIndex).Value
                If liToAdd IsNot Nothing Then
                    If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                        sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                        aryList.Add(Trim(liToAdd))
                    End If
                End If
            Next

        Catch ex As Exception
            ExceptionMsg(PageName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
        Return sbString.ToString
    End Function
#End Region

#Region "EVENT HANDLER"
    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            Dim strPrdList As String = GetItemsInString(lsbSelectedProduct)
            If Len(strPrdList) > 8000 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "", "alert('Selected Product list too large. Please reduce the number of product');", True)
            Else
                RefreshDataBind()
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".btnSearch_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()
            dt.Rows.Add(dt.NewRow)
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try

            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()


        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub


#End Region

End Class

Public Class CF_SFStkConsumpByQuadMth
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""

        Select Case ColumnName.ToUpper
            Case "ITEM_NAME"
                strFieldName = " "
            Case "JAN-FEB-MAR-APR"
                strFieldName = "Jan-Feb-Mar-Apr"
            Case "MAY-JUN-JUL-AUG"
                strFieldName = "May-Jun-Jul-Aug" 
            Case "SEP-OCT-NOV-DEC"
                strFieldName = "Sep-Oct-Nov-Dec"
            
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "ID" OrElse strColumnName Like "ITEM_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "JAN-FEB-MAR-APR" OrElse strColumnName = "MAY-JUN-JUL-AUG" OrElse strColumnName = "SEP-OCT-NOV-DEC" Then
                If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.STK_CONSUMP_BY_QUADMONTH_DTL, SubModuleAction.View) Then
                    FCT = FieldColumntype.HyperlinkColumn
                Else
                    FCT = FieldColumntype.BoundColumn
                End If
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "DATE"
                    strFormatString = "{0:yyyy-MM-dd}"
                Case "JAN-FEB", "MAR-APR", "MAY-JUN", "JUL-AUG", "SEP-OCT", "NOV-DEC"
                    strFormatString = "{0:#,0.########}"
                Case Else
                    strFormatString = ""
            End Select
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName = "JAN-FEB" Or strColumnName = "MAR-APR" Or strColumnName = "MAY-JUN" Or strColumnName = "JUL-AUG" Or strColumnName = "SEP-OCT" Or strColumnName = "NOV-DEC" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class