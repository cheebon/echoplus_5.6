<%@ Page Language="vb" AutoEventWireup="false" Inherits="CustomerMthSalesHistDetail"
    CodeFile="CustomerMthSalesHistDetail.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Sales History Details</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCuctomerSalesHistDtl" method="post" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout">
                                    <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3">
                                                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="Bckgroundreport">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="Bckgroundreport">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                <tr>
                                                                    <td align="left" colspan="3">
                                                                        <asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" Width="60px" /></td>
                                                                </tr>
                                                            </table>
                                                            <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                                <ContentTemplate>
                                                                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick">
                                                                    </asp:Timer>
                                                                    <ccGV:clsGridView ID="gridview1" runat="server" ShowFooter="false" AllowPaging="false"
                                                                        AllowSorting="True" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                        GridHeight="420" RowSelectionEnabled="true">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="true" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.</EmptyDataTemplate>
                                                                        <Columns>
                                                                            <asp:BoundField DataField="item_code" HeaderText="Product Code" />
                                                                            <asp:BoundField DataField="item_desc" HeaderText="Product Name" />
                                                                            <asp:BoundField DataField="sales_qty" HeaderText="Qty" />
                                                                            <asp:BoundField DataField="sls_passqty" HeaderText="Pre Year Qty" />
                                                                            <asp:BoundField DataField="foc_qty" HeaderText="FOC" />
                                                                            <asp:BoundField DataField="foc_passqty" HeaderText="Pre Year FOC" />
                                                                            <asp:BoundField DataField="sls_value" HeaderText="Amount" />
                                                                            <asp:BoundField DataField="sls_passvalue" HeaderText="Pre Year Amt" />
                                                                            <asp:BoundField DataField="item_measure" HeaderText="Measure" />
                                                                            <asp:BoundField DataField="item_passmeasure" HeaderText="Pre Year Measure" />
                                                                        </Columns>
                                                                    </ccGV:clsGridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport">
                                            <td colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</body>
</html>
