﻿Imports System.Data

Partial Class iFFMA_Salesteam_SalesteamAssignDashboardXFieldPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event CloseButton_Click As EventHandler

    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    ''' 
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property Total() As Integer
        Get
            Return Trim(hdTotal.Value)
        End Get
        Set(ByVal value As Integer)
            hdTotal.Value = value
        End Set
    End Property

    Public Property IsEdit() As Boolean
        Get
            Return Trim(hdIsEdit.Value)
        End Get
        Set(ByVal value As Boolean)
            hdIsEdit.Value = value
        End Set
    End Property

    Public Property PerTeamLimit() As Integer
        Get
            Return Trim(hdPerTeamLimit.Value)
        End Get
        Set(ByVal value As Integer)
            hdPerTeamLimit.Value = value
        End Set
    End Property
#End Region
#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub ResetPage()
        LoadDDLXFields()
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
                If Total() >= PerTeamLimit Then
                    btnSave.Visible = False
                Else
                    btnSave.Visible = True
                End If
            Else
                lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function ValidateDDLFields(ByRef xFieldIdList As IList(Of String), ByRef seqList As IList(Of String)) As Boolean
        Dim containValue As Boolean = False
        Dim ddl As DropDownList

        xFieldIdList = New List(Of String)()
        seqList = New List(Of String)()

        For i As Integer = 1 To PerTeamLimit
            ddl = CType(updPnlMaintenance.FindControl("ddlXFieldList" + i.ToString), DropDownList)
            If ddl.SelectedValue <> "0" Then
                xFieldIdList.Add(ddl.SelectedValue)
                seqList.Add(i.ToString)
                containValue = True
            End If
        Next

        If Not (containValue) Then
            lblInfo.Text = "Please select at least one XField!"
            Show()
            Return True
        End If

        Dim checkDuplicate = xFieldIdList _
                                 .GroupBy(Function(x) x) _
                                 .Any(Function(g) g.Count() > 1)

        If (checkDuplicate) Then
            lblInfo.Text = "Cannot have duplicate XFields!"
            Show()
            Return True
        End If

        Return False
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            lblInfo.Text = String.Empty
            
            Dim ddlxFieldIdList, ddlSeqList As IList(Of String)

            If (ValidateDDLFields(ddlxFieldIdList, ddlSeqList)) Then
                Exit Sub
            End If

            Dim xFieldIdList = String.Join(",", ddlxFieldIdList.ToArray())
            Dim seqList = String.Join(",", ddlSeqList.ToArray())

            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            clsSalesteam.UpdateSalesteamDashboardXField(xFieldIdList, seqList, TeamCode)
            lblInfo.Text = "The record is saved successfully."
            Show()

            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub LoadDDLXFields()
        Try
            Dim DT As DataTable
            Dim clsSalesteam As New mst_Salesteam.clsSalesteam
            Dim dtXFieldDtl As Dictionary(Of Integer, Integer) = New Dictionary(Of Integer, Integer)

            If (IsEdit) Then
                DT = clsSalesteam.GetSalesteamDashboardXFieldListByTeam(TeamCode)
                If (DT.Rows.Count > 0) Then
                    For Each row In DT.Rows
                        'KL - Set Seq as Key, ID as Value
                        dtXFieldDtl.Add(row("SEQ"), row("DASHBOARD_XFIELD_ID"))
                    Next
                Else
                    Exit Sub
                End If
            End If

            DT = clsSalesteam.GetSalesteamDashboardXFieldList()
            For i As Integer = 1 To PerTeamLimit
                Dim ddl As DropDownList = CType(updPnlMaintenance.FindControl("ddlXFieldList" + i.ToString), DropDownList)
                With ddl
                    .DataSource = DT
                    .DataTextField = "DASHBOARD_XFIELD_DESC"
                    .DataValueField = "DASHBOARD_XFIELD_ID"
                    .DataBind()
                    .Items.Insert(0, New ListItem("-- SELECT --", "0"))
                    .SelectedIndex = 0
                    If (IsEdit) Then
                        Dim xFieldId As Integer
                        'KL - Use TryGetValue instead of ContainsValue
                        If (dtXFieldDtl.TryGetValue(i, xFieldId)) Then
                            .SelectedValue = xFieldId
                        End If
                    End If
                End With
            Next
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
