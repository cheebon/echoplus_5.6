'************************************************************************
'	Author	    :	
'	Date	    :	01/10/2007
'	Purpose	    :	SAP List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data
Partial Class iFFMS_Order_TRAOrderHdrList
    Inherits System.Web.UI.Page


#Region "Local Variable"
    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection
    Private _showMessage As String

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMS_TRAOrderLsit"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Private Property ShowMessage As String
        Get
            Return _showMessage
        End Get
        Set(value As String)
            _showMessage = value
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property


    Public Enum dgcol As Integer

        STATUS = 3
        WF_STATUS = 4
        SAP_STATUS = 15
    End Enum
#End Region
    Private intPageSize As Integer
    Public ReadOnly Property PageName() As String
        Get
            Return "TRA Order List"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        System.Web.UI.ScriptManager.GetCurrent(Me).RegisterPostBackControl(imgExport)

        If Session("UserID") = "" Then
            Dim strScript As String = ""
            strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        End If

        If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        'Call Header
        With wuc_lblheader
            .Title = "TRA List"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            'PageLoad()
            TimerControl1.Enabled = True

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = True
            End With

        End If
        lblErr.Text = ""
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            wuc_txtDate.DateStart = Now
            wuc_txtDate.DateEnd = Now
            'LoadStatusDDL()
            'RefreshDatabinding()
            dgList.DataSource = Nothing
            dgList.PageSize = intPageSize
            dgList.DataBind()

            LoadReasonCode()
            UpdatePage.Update()
        End If
        TimerControl1.Enabled = False
    End Sub

#Region "Event Handler"

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnSearch_Click
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    'Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Try
    '        dgList.PageIndex = 0
    '        RefreshDatabinding()
    '        UpdatePage.Update()
    '        UpdateDatagrid.Update()

    '    Catch ex As Exception
    '        lblErr.Text = "SAPList.btnSearch_Click : " + ex.ToString()
    '    End Try
    'End Sub

    'Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgExport.Click
    '    ExportToExcel()
    'End Sub

    Protected Sub btnSearchCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCriteria.Click
        dgList.PageIndex = 0
        RefreshDatabinding()

        CPESearchCriteria.Collapsed = True
        UpdatePage.Update()
        UpdateDatagrid.Update()
    End Sub


    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

        Dim strReasonCode As String
        strReasonCode = ddlreasoncode.SelectedValue
        'MsgBox( strReasonCode)
        Try
            If dgList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim dterror As DataTable
                Dim clsTRAOrder As New txn_order.clsTRAOrder
                Dim strTxnNo As String, strSalesrepCode As String, strCustCode As String, strVisitId As String, strVoucherNo As String, strUserName As String, strUserId As String, strStatus As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                            strTxnNo = dgList.DataKeys(i).Item("TXN_NO")
                            strSalesrepCode = dgList.DataKeys(i).Item("SALESREP_CODE")
                            strCustCode = dgList.DataKeys(i).Item("CUST_CODE")
                            strVisitId = dgList.DataKeys(i).Item("VISIT_ID")
                            strVoucherNo = dgList.DataKeys(i).Item("VOUCHER_NO")
                            strUserId = Trim(Session("UserID"))
                            strUserName = Trim(Session("UserName"))
                            strStatus = "C"
                            dterror = clsTRAOrder.InstTRAKIVDetailsV2(strTxnNo, strVoucherNo, strVisitId, strSalesrepCode, strStatus, strReasonCode, strUserName, strUserId)
                        End If


                    End If
                    i += 1

                Next
                RefreshDatabinding()
                UpdateDatagrid.Update()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    'Protected Sub btnCancel_clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblDdlMsgPop.CloseButton_Click
    '    MsgBox(lblDdlMsgPop.ReasonCode)
    'End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

    '    Dim strReasonCode As String

    '    'lblDdlMsgPop.Message = "Please select Cancellation Reason Code"
    '    'lblDdlMsgPop.Show()
    '    'strReasonCode = lblDdlMsgPop.ReasonCode

    '    'Try
    '    '    If dgList.Rows.Count > 0 Then
    '    '        Dim chkSelected As CheckBox
    '    '        Dim i As Integer = 0
    '    '        Dim dterror As DataTable
    '    '        Dim clsTRAOrder As New txn_order.clsTRAOrder
    '    '        Dim strTxnNo As String, strSalesrepCode As String, strCustCode As String, strVisitId As String, strVoucherNo As String, strUserName As String, strUserId As String, strStatus As String

    '    '        Dim DK As DataKey
    '    '        For Each DR As GridViewRow In dgList.Rows
    '    '            chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
    '    '            If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
    '    '                DK = dgList.DataKeys(i)
    '    '                If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

    '    '                    strTxnNo = dgList.DataKeys(i).Item("TXN_NO")
    '    '                    strSalesrepCode = dgList.DataKeys(i).Item("SALESREP_CODE")
    '    '                    strCustCode = dgList.DataKeys(i).Item("CUST_CODE")
    '    '                    strVisitId = dgList.DataKeys(i).Item("VISIT_ID")
    '    '                    strVoucherNo = dgList.DataKeys(i).Item("VOUCHER_NO")
    '    '                    strUserId = Trim(Session("UserID"))
    '    '                    strUserName = Trim(Session("UserName"))
    '    '                    strStatus = "C"
    '    '                    dterror = clsTRAOrder.InstTRAKIVDetails(strTxnNo, strVoucherNo, strVisitId, strSalesrepCode, strStatus, strUserName, strUserId)
    '    '                End If


    '    '            End If
    '    '            i += 1

    '    '        Next
    '    '        RefreshDatabinding()
    '    '        UpdateDatagrid.Update()
    '    '    End If
    '    'Catch ex As Exception
    '    '    ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    'End Try
    'End Sub

    Protected Sub btnSumbilt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSumbilt.Click
        Dim strReasonCode As String
        strReasonCode = ""

        Try
            If dgList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim dterror As DataTable
                Dim clsTRAOrder As New txn_order.clsTRAOrder
                Dim strTxnNo As String, strSalesrepCode As String, strCustCode As String, strVisitId As String, strVoucherNo As String, strUserName As String, strUserId As String, strStatus As String, strCurrentStatus As String

                lblMsgPop.Message = "You request is successful!!"

                Dim DK As DataKey
                For Each DR As GridViewRow In dgList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                            strTxnNo = dgList.DataKeys(i).Item("TXN_NO")
                            strSalesrepCode = dgList.DataKeys(i).Item("SALESREP_CODE")
                            strCustCode = dgList.DataKeys(i).Item("CUST_CODE")
                            strVisitId = dgList.DataKeys(i).Item("VISIT_ID")
                            strVoucherNo = dgList.DataKeys(i).Item("VOUCHER_NO")
                            strUserId = Trim(Session("UserID"))
                            strUserName = Trim(Session("UserName"))

                            strCurrentStatus = dgList.DataKeys(i).Item("TXN_STATUS")
                            If strCurrentStatus = "V" Then
                                strStatus = "PP"
                            Else
                                strStatus = "P"
                            End If

                            dterror = clsTRAOrder.InstTRAKIVDetailsV2(strTxnNo, strVoucherNo, strVisitId, strSalesrepCode, strStatus, strReasonCode, strUserName, strUserId)

                            'Dim dtWFAR = clsTRAOrder.GetAttachmentAR(Web.HttpContext.Current.Session("PRINCIPAL_ID").ToString)

                            'If dtWFAR.Rows(0)("ind").ToString = "1" Then
                            If dterror.Columns.Contains("output_value") Then
                                Dim strOutputValue As String = dterror.Rows(0)("output_value")
                                If strOutputValue = "V" Then
                                    ShowMessage() = "yes"
                                Else
                                    lblMsgPop.Message = "You TRA request (" + strTxnNo + " is not successful!!." + strOutputValue
                                    lblMsgPop.Show()
                                End If
                            Else
                                Dim strerror As String = dterror.Rows(0)("ERROR_TXN")
                                If Not strerror = "0" Then
                                    'lblMsgPop.Message = "The transaction " + strerror + " do not have the complete information to submit into SAP!"
                                    lblMsgPop.Message = "One or more of the selected transaction do not have complete information to submit into SAP, please check 'Ctn no.' and 'Collect by'!"
                                End If
                            End If

                            ' Else
                            'Dim strerror As String = dterror.Rows(0)("ERROR_TXN")

                            'If Not strerror = "0" Then
                            '    'lblMsgPop.Message = "The transaction " + strerror + " do not have the complete information to submit into SAP!"
                            '    lblMsgPop.Message = "One or more of the selected transaction do not have complete information to submit into SAP, please check 'Ctn no.' and 'Collect by'!"
                            'End If
                            'End If
                        End If


                    End If
                    i += 1

                Next

                lblMsgPop.Show()
                RefreshDatabinding()
                UpdateDatagrid.Update()
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region



#Region "DGLIST"
    Private Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentView"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrenttable = GetRecList()
            PreRenderMode(dtCurrenttable)

            If dtCurrenttable Is Nothing Then
                dtCurrenttable = New DataTable
            Else
                If dtCurrenttable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrenttable.Rows.Count
                End If
            End If



            Dim dvCurrentView As New Data.DataView(dtCurrenttable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
            End With

            wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)

        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()

        End Try
    End Sub


    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()

            'Add Data Grid Columns
            dgList.Columns.Clear()

            aryDataItem.Add("chkSelect")
            Dim chkbox As New CheckBoxField
            dgList.Columns.Add(chkbox)
            dgList.Columns(0).HeaderStyle.Width = "25"
            dgList.Columns(0).HeaderText = "Select"
            dgList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center



            licItemFigureCollector = New ListItemCollection

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_TRAOrderList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                        Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        If String.IsNullOrEmpty(CF_TRAOrderList.GetOutputFormatString(ColumnName)) = False Then
                            dgColumn.DataTextFormatString = CF_TRAOrderList.GetOutputFormatString(ColumnName)
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TRAOrderList.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_TRAOrderList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataTextField = ColumnName
                        dgColumn.SortExpression = ColumnName


                        Dim strUrlFields(5) As String
                        strUrlFields(0) = "TXN_NO"
                        strUrlFields(1) = "TXN_DATE"
                        strUrlFields(2) = "TXN_STATUS"
                        strUrlFields(3) = "SALESREP_CODE"
                        strUrlFields(4) = "VISIT_ID"
                        strUrlFields(5) = "VOUCHER_NO"

                        Dim strUrlFormatString As String
                        'strUrlFormatString = "iFFMS/ORDER/TRAORDERDTLLIST.aspx"
                        strUrlFormatString = "~/iFFMS/ORDER/TRAORDERDTLLISTV2.aspx?" & _
                         "TXN_NO={0}&TXN_STATUS={2}&SALESREP_CODE={3}&VISIT_ID={4}&VOUCHER_NO={5}&SESSION_ID=" + GetGuid()

                        dgColumn.DataNavigateUrlFields = strUrlFields
                        dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                        'dgColumn.NavigateUrl = strUrlFormatString
                        dgColumn.Target = "ContentBar" '"_self"

                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                        licItemFigureCollector.Add(ColumnName)

                    Case FieldColumntype.InvisibleColumn
                    Case Else
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_TRAOrderList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString 'CF_SFCAP.GetOutputFormatString(ColumnName)
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_TRAOrderList.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_TRAOrderList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        'Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        Finally
        End Try
    End Sub

    'Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    '    Try
    '        Select Case e.Row.RowType
    '            Case DataControlRowType.DataRow

    '                If licItemFigureCollector Is Nothing Then Exit Select

    '                For Each li As ListItem In licItemFigureCollector
    '                    Dim strTxnNo As String = DataBinder.Eval(e.Row.DataItem, li.Text)
    '                    e.Row.Cells(0).Text = "<a href=""#"" onclick=""var w=window.open('TRAOrderDtlList.aspx?TXN_NO=" + DataBinder.Eval(e.Row.DataItem, "TXN_NO") + "&TXN_DATE=" + DataBinder.Eval(e.Row.DataItem, "TXN_DATE") + "','g','width=1000,height=550,resizable=yes,left=10,screenX=330,top=10,screenY=150'); return false;"">" + strTxnNo + "</a>"

    '                Next
    '        End Select
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
    '    Finally
    '    End Try

    'End Sub


    Private Function GetRecList() As Data.DataTable
        Dim objTRAHdrQuery As txn_order.clsTRAOrderList
        Dim DT As DataTable = Nothing
        Try

            objTRAHdrQuery = New txn_order.clsTRAOrderList
            Dim strUserCode As String, strUserID As String, strTxnNo As String, strTxnDateStart As String, strTxnDateEnd As String, strTxnStatus As String, _
             strRefNo As String, strSalesrepCode As String, strSalesrepName As String, strPayerCode As String, strPayerName As String, strSearchUserId As String, strSalesAreaCode As String, strSalesTeamCode As String, strSAPNo As String

            strUserCode = Trim(Session("UserCode"))
            strUserID = Trim(Session("UserID"))
            strSalesrepCode = Trim(txtsalesrepcode.Text)
            strSalesrepName = Trim(txtsalesrepname.Text)
            strTxnNo = Trim(txttxnno.Text)
            strTxnDateStart = Trim(wuc_txtDate.DateStart)
            strTxnDateEnd = Trim(wuc_txtDate.DateEnd)
            strTxnStatus = ddltxnstatus.SelectedValue
            strRefNo = Trim(txtrefno.Text)
            strPayerCode = Trim(txtPayerCode.Text)
            strPayerName = Trim(txtpayername.Text)
            strSearchUserId = Trim(txtUserName.Text)
            strSalesAreaCode = Trim(txtSalesAreaCode.Text)
            strSalesTeamCode = Trim(txtSalesTeamCode.Text)
            strSAPNo = Trim(txtsapno.Text)

            DT = objTRAHdrQuery.GetTRAHeaderCriteriaV2(strUserCode, strSalesrepCode, strSalesrepName, strTxnNo, strTxnDateStart, strTxnDateEnd, strTxnStatus, strRefNo, strPayerCode, strPayerName, strSearchUserId, strSalesAreaCode, strSalesTeamCode, strSAPNo, strUserID)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function


    'Private Function GetRecList() As Data.DataTable
    '    Dim objTRAHdrQuery As txn_order.clsTRAOrderList
    '    Dim DT As DataTable = Nothing
    '    Try

    '        objTRAHdrQuery = New txn_order.clsTRAOrderList
    '        Dim strSalesrepCode As String, strUserCode As String, strTeamCode As String, strSearchType As String, strSearchValue As String, strSearchValue1 As String
    '        With objTRAHdrQuery
    '            strSearchType = Trim(ddlSearchType.SelectedValue)
    '            strTeamCode = Trim(ddlTeam.SelectedValue)
    '            strUserCode = Trim(Session("UserCode"))
    '            strSalesrepCode = Trim(Session("Salesrep_Code"))
    '            Select Case strSearchType
    '                Case "LASTUPDATE", "txn_timestamp", "TXN_DATE"
    '                    strSearchValue = Trim(Replace(Wuc_txtDate1.Text, "*", "%"))
    '                    strSearchValue1 = Trim(Replace(Wuc_txtDate2.Text, "*", "%"))
    '                Case "STATUS", "txn_status" '"StatusID"   'HL:20070622
    '                    strSearchValue = Trim(ddlList.SelectedValue)
    '                    strSearchValue1 = ""
    '                Case Else
    '                    strSearchValue = Trim(Replace(txtSearchValue.Text, "*", "%"))
    '                    strSearchValue1 = ""
    '            End Select
    '            If String.IsNullOrEmpty(strSearchType) Then
    '                DT = Nothing
    '            Else
    '                DT = .GetTRAHeader(strSalesrepCode, strUserCode, strTeamCode, strSearchType, strSearchValue, strSearchValue1)
    '            End If
    '        End With

    '        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
    '    Finally
    '    End Try
    '    Return DT
    'End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            If DT IsNot Nothing Then dgList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_Count > 0 Then

                        Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                        If chk Is Nothing Then
                            chk = New CheckBox
                            chk.ID = "chkSelect"
                            e.Row.Cells(0).Controls.Add(chk)

                        End If

                        'Dim strTxnNo As String = dgList.DataKeys(e.Row.RowIndex).Item("TXN_NO")
                        'Dim strTxnDate As String = dgList.DataKeys(e.Row.RowIndex).Item("TXN_DATE")
                        'Dim strTxnStatus As String = dgList.DataKeys(e.Row.RowIndex).Item("TXN_STATUS")
                        'Dim strSalesrepCode As String = dgList.DataKeys(e.Row.RowIndex).Item("SALESREP_CODE")
                        'Dim strVisitId As String = dgList.DataKeys(e.Row.RowIndex).Item("VISIT_ID")
                        'Dim strVoucherNo As String = dgList.DataKeys(e.Row.RowIndex).Item("VOUCHER_NO")
                        'Dim str As String '3 is the column to add attributes the link
                        'str = "parent.ContentBar.location.href ='../../iFFMS/ORDER/TRAORDERDTLLIST.aspx?" & _
                        ' "TXN_NO=" + strTxnNo + "&TXN_DATE=" + strTxnDate + "&TXN_STATUS=" + strTxnStatus + "&SALESREP_CODE=" + strSalesrepCode + "&VISIT_ID=" + strVisitId + "&VOUCHER_NO=" + strVoucherNo + "&SESSION_ID=" + GetGuid() + "';"

                        'e.Row.Cells(1).Attributes.Add("onclick", str)
                        'e.Row.Cells(1).Attributes.Add("style", "cursor:pointer;")
                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

                    Dim chkselect As CheckBox = CType(e.Row.FindControl("chkselect"), CheckBox)

                    If Master_Row_Count > 0 Then
                        Dim strTxnStatusCode As String, strWFStatusCode As String, strSAPStatusCode As String
                        strTxnStatusCode = Trim(e.Row.Cells(dgcol.STATUS).Text)
                        strWFStatusCode = Trim(e.Row.Cells(dgcol.WF_STATUS).Text)
                        strSAPStatusCode = e.Row.Cells(dgcol.SAP_STATUS).Text

                        'enabled/disabled checkbox
                        If Not IsNothing(chkselect) Then
                            If strTxnStatusCode.ToUpper = "K" Or strTxnStatusCode.ToUpper = "V" Then
                                chkselect.Enabled = True
                            Else
                                chkselect.Enabled = False
                            End If
                        End If

                        'rename the txn status text
                        If strTxnStatusCode.ToUpper = "K" Then
                            e.Row.Cells(dgcol.STATUS).Text = "K - KIV"
                        ElseIf strTxnStatusCode.ToUpper = "V" Then
                            e.Row.Cells(dgcol.STATUS).Text = "V - Pending revision"
                        ElseIf strTxnStatusCode.ToUpper = "P" Then
                            e.Row.Cells(dgcol.STATUS).Text = "P - Pending approval"
                        ElseIf strTxnStatusCode.ToUpper = "S" Then
                            e.Row.Cells(dgcol.STATUS).Text = "S - Submitted to SAP"
                        End If

                        'rename the wf status text
                        If strWFStatusCode.ToUpper = "P" Then
                            e.Row.Cells(dgcol.WF_STATUS).Text = "P - Pending"
                        ElseIf strWFStatusCode.ToUpper = "W" Then
                            e.Row.Cells(dgcol.WF_STATUS).Text = "W - Pending approval"
                        ElseIf strWFStatusCode.ToUpper = "A" Then
                            e.Row.Cells(dgcol.WF_STATUS).Text = "A - Approved"
                        ElseIf strWFStatusCode.ToUpper = "R" Then
                            e.Row.Cells(dgcol.WF_STATUS).Text = "R - Rejected"
                        ElseIf strWFStatusCode.ToUpper = "V" Then
                            e.Row.Cells(dgcol.WF_STATUS).Text = "V - Request revision"
                        End If

                        'add color to the text
                        If (strTxnStatusCode = "K" And strWFStatusCode = "P") Or (strTxnStatusCode = "P" And strWFStatusCode = "W") Then
                            e.Row.Cells(dgcol.STATUS).ForeColor = System.Drawing.Color.Orange
                            e.Row.Cells(dgcol.WF_STATUS).ForeColor = System.Drawing.Color.Orange
                            e.Row.Cells(dgcol.STATUS).Font.Bold = True
                            e.Row.Cells(dgcol.WF_STATUS).Font.Bold = True
                        ElseIf strTxnStatusCode = "S" And strWFStatusCode = "A" And strSAPStatusCode = "R - Success" Then
                            e.Row.Cells(dgcol.STATUS).ForeColor = System.Drawing.Color.Green
                            e.Row.Cells(dgcol.WF_STATUS).ForeColor = System.Drawing.Color.Green
                            e.Row.Cells(dgcol.SAP_STATUS).ForeColor = System.Drawing.Color.Green
                            e.Row.Cells(dgcol.STATUS).Font.Bold = True
                            e.Row.Cells(dgcol.WF_STATUS).Font.Bold = True
                            e.Row.Cells(dgcol.SAP_STATUS).Font.Bold = True
                        ElseIf (strTxnStatusCode = "S" And strWFStatusCode = "A" And strSAPStatusCode = "E - Error") Then
                            e.Row.Cells(dgcol.STATUS).ForeColor = System.Drawing.Color.Red
                            e.Row.Cells(dgcol.WF_STATUS).ForeColor = System.Drawing.Color.Red
                            e.Row.Cells(dgcol.SAP_STATUS).ForeColor = System.Drawing.Color.Red
                            e.Row.Cells(dgcol.STATUS).Font.Bold = True
                            e.Row.Cells(dgcol.WF_STATUS).Font.Bold = True
                            e.Row.Cells(dgcol.SAP_STATUS).Font.Bold = True
                        ElseIf (strTxnStatusCode = "P" And strWFStatusCode = "R") Or (strTxnStatusCode = "V" And strWFStatusCode = "V") Then
                            e.Row.Cells(dgcol.STATUS).ForeColor = System.Drawing.Color.Red
                            e.Row.Cells(dgcol.WF_STATUS).ForeColor = System.Drawing.Color.Red
                            e.Row.Cells(dgcol.STATUS).Font.Bold = True
                            e.Row.Cells(dgcol.WF_STATUS).Font.Bold = True
                        End If


                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpression") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DATA BIND"
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Sub BindDefault()
        Try
            Dim dt As Data.DataTable = GetRecList()

            dt.Rows.Add(dt.NewRow)
            ViewState("dtCurrentView") = dt
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".BindDefault : " & ex.ToString)
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadReasonCode()
        Try
            'Clear list box before fill
            ddlreasoncode.Items.Clear()

            Dim clsReasonCode As New mst_Common.clsDDL

            With ddlreasoncode
                .DataSource = clsReasonCode.GetReasonCode()
                .DataTextField = "REASON_NAME"
                .DataValueField = "REASON_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            'ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("FreezePanel.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgExport.Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            ExportToFile(dgList, wuc_lblheader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub PrepareControlForExport(ByRef ctrlHTML As Control)
        Dim ltrField As Literal = New Literal()
        Dim intIdx As Integer
        Dim ctrl As Control = Nothing
        Try
            For intIdx = 0 To ctrlHTML.Controls.Count - 1 Step intIdx + 1
                ctrl = ctrlHTML.Controls(intIdx)
                If TypeOf ctrl Is LinkButton Then
                    'ltrField.Text = (CType(ctrl, LinkButton)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is HyperLink Then
                    ltrField.Text = (CType(ctrl, HyperLink)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is DropDownList Then
                    ltrField.Text = (CType(ctrl, DropDownList)).SelectedItem.Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is TextBox Then
                    ltrField.Text = (CType(ctrl, TextBox)).Text
                    ctrlHTML.Controls.Remove(ctrl)
                    ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is CheckBox Then
                    'ltrField.Text = IIf(CType(ctrl, CheckBox).Checked, "True", "False")
                    ctrlHTML.Controls.Remove(ctrl)
                    'ctrlHTML.Controls.AddAt(intIdx, ltrField)
                ElseIf TypeOf ctrl Is ImageButton OrElse TypeOf ctrl Is Image OrElse TypeOf ctrl Is Button Then
                    ctrlHTML.Controls.Remove(ctrl)
                End If
                If ctrl IsNot Nothing AndAlso ctrl.HasControls Then PrepareControlForExport(ctrl)
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".PrepareGridViewForExport : " & ex.ToString)
        End Try

    End Sub

    Public Sub ExportToFile(ByRef dgList As Control, Optional ByVal customFileName As String = "", Optional ByVal SheetStyle As String = "")
        Try
            Excel.ExportToFile(dgList, Response, customFileName, SheetStyle)
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Sub ExportToFile(ByRef aryDgList As ArrayList, Optional ByVal customFileName As String = "")
        Try
            customFileName = customFileName & IIf(String.IsNullOrEmpty(customFileName), "Report", IIf(customFileName.ToUpper.Contains("REPORT"), "", "_Report"))
            Dim strFileName As String = customFileName & ".xls"
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & strFileName)
            Response.Charset = "utf-8"
            Response.ContentType = "application/vnd.ms-excel"
            Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

            'create a emptry row table, act as empty row for insert
            Dim newRow As New HtmlTable()
            newRow.Rows.Add(New HtmlTableRow())

            For Each dgList As Control In aryDgList
                newRow.RenderControl(htmlWrite) 'insert a empty row
                Response.Write("<meta http-equiv=Content-Type content=""text/html; charset=utf-8"">")
                PrepareControlForExport(dgList)
                dgList.RenderControl(htmlWrite)
            Next
            Response.Write(stringWrite.ToString)
            Response.Flush()
            Response.End()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    '#Region "Export Extender"

    '    Private Sub ExportToExcel()
    '        Try
    '            Response.Clear()
    '            Response.Buffer = True
    '            Response.ClearContent()
    '            Response.ClearHeaders()
    '            Response.Charset = ""
    '            Dim FileName As String = "TRA" + DateTime.Now + ".xls"
    '            Dim strwritter As New IO.StringWriter()
    '            Dim htmltextwrtter As New HtmlTextWriter(strwritter)
    '            Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '            Response.ContentType = "application/vnd.ms-excel"
    '            Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & FileName)
    '            dgList.GridLines = GridLines.Both
    '            dgList.HeaderStyle.Font.Bold = True
    '            dgList.RenderControl(htmltextwrtter)
    '            Response.Write(strwritter.ToString())
    '            Response.[End]()
    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".dgList_Export : " & ex.ToString)
    '        End Try

    '    End Sub

    '    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
    '        ''Hide this function to prevent internal asp.net error
    '        ''To enable export function
    '        'MyBase.VerifyRenderingInServerForm(control)
    '    End Sub
    '#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

#Region "Common"
    'Private Sub GetTeam()
    '    Dim objSAPQuery As mst_SAP.clsSAPQuery
    '    Dim dt As DataTable
    '    Dim drRow As DataRow

    '    Try
    '        ddlTeam.Items.Clear()
    '        objSAPQuery = New mst_SAP.clsSAPQuery
    '        With objSAPQuery
    '            .clsProperties.user_code = Trim(Session("UserCode"))
    '            dt = .GetSAPTeamList
    '        End With
    '        If dt.Rows.Count > 0 Then
    '            ddlTeam.Items.Add(New ListItem("All", "ALL"))
    '            For Each drRow In dt.Rows
    '                ddlTeam.Items.Add(New ListItem(drRow("team"), drRow("team")))
    '            Next
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clsTRAOrder As New txn_order.clsTRAOrder
            SalesrepCode = clsTRAOrder.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Public Property GetGuid() As String
        Get
            If String.IsNullOrEmpty(Session("GUID")) Then Session("GUID") = Session("USER_ID") & Guid.NewGuid.ToString.ToUpper.Substring(24, 8)
            Return Session("GUID")
        End Get
        Set(ByVal value As String)
            Session("GUID") = value
        End Set
    End Property

    'Private Sub PageLoad()
    '    Dim strDocType, strSearchType, strSearchValue, strSearchValue1 As String

    '    'constant

    '    'request
    '    strDocType = IIf(Trim(Request.QueryString("doc_type")) <> "", Trim(Request.QueryString("doc_type")), "SO")
    '    strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "LASTUPDATE")
    '    strSearchValue = Trim(Request.QueryString("search_value"))
    '    strSearchValue1 = Trim(Request.QueryString("search_value_1"))

    '    'Prepare Recordset,Createobject as needed
    '    ddlDocType.SelectedValue = strDocType
    '    ddlSearchType.SelectedValue = strSearchType
    '    ddlSearchType_onChanged()

    '    ' GetTeam()


    '    If Trim(strSearchType) = "LASTUPDATE" Or Trim(strSearchType) = "txn_timestamp" Then
    '        Wuc_txtDate1.Text = IIf(strSearchValue <> "", strSearchValue, Now.ToString("yyyy-MM-dd"))
    '        Wuc_txtDate2.Text = IIf(strSearchValue1 <> "", strSearchValue1, Now.ToString("yyyy-MM-dd"))
    '    ElseIf Trim(strSearchType) = "STATUS" Then
    '        ddlList.SelectedValue = IIf(strSearchValue <> "", strSearchValue, "")
    '    Else
    '        txtSearchValue.Text = IIf(strSearchValue <> "", strSearchValue, "")
    '        txtSearchValue1.Text = IIf(strSearchValue1 <> "", strSearchValue1, "")
    '    End If

    '    UpdatePage.Update()
    'End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlDocType_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    'Protected Sub ddlDocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocType.SelectedIndexChanged
    '    Try
    '        BindGrid(ViewState("SortCol"))

    '    Catch ex As Exception
    '        lblErr.Text = "SAPList.ddlDocType_SelectedIndexChanged : " + ex.ToString()
    '    Finally

    '    End Try
    'End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlTeam_SelectedIndexChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    'Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTeam.SelectedIndexChanged
    '    Try
    '        BindGrid(ViewState("SortCol"))

    '    Catch ex As Exception
    '        lblErr.Text = "SAPList.ddlTeam_SelectedIndexChanged : " + ex.ToString()
    '    Finally

    '    End Try
    'End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub ddlSearchType_onChanged
    ' Purpose	        :	
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------
    'Private Sub ddlSearchType_onChanged()
    '    Dim dt As DataTable
    '    Dim drRow As DataRow

    '    Try
    '        'txtSearchValue.Text = ""
    '        'txtSearchValue1.Text = ""
    '        lblSearchValue.Text = ""
    '        ddlList.SelectedValue = "0"
    '        Wuc_txtDate1.Text = ""
    '        Wuc_txtDate2.Text = ""

    '        lblSearchValue.Visible = False
    '        'lblDot.Visible = False
    '        lblTo.Visible = False
    '        txtSearchValue.Visible = False
    '        txtSearchValue.ReadOnly = False
    '        txtSearchValue1.Visible = False
    '        txtSearchValue1.ReadOnly = False
    '        btnSearch.Visible = False
    '        ddlList.Visible = False
    '        Wuc_txtDate1.Visible = False
    '        Wuc_txtDate2.Visible = False

    '        Select Case ddlSearchType.SelectedValue
    '            Case "all"
    '                btnSearch.Visible = True

    '            Case "LASTUPDATE", "txn_timestamp"
    '                lblSearchValue.Visible = True
    '                'lblDot.Visible = True
    '                lblSearchValue.Text = ddlSearchType.SelectedItem.Text
    '                lblTo.Visible = True
    '                'txtSearchValue.Visible = True
    '                'txtSearchValue.Text = ""
    '                'txtSearchValue.ReadOnly = True
    '                'txtSearchValue1.Visible = True
    '                'txtSearchValue1.ReadOnly = True
    '                'txtSearchValue1.Text = ""
    '                btnSearch.Visible = True
    '                'Wuc_txtDate1.InsertFunctionScript()
    '                Wuc_txtDate1.Visible = True
    '                Wuc_txtDate2.Visible = True

    '            Case "STATUS"
    '                lblSearchValue.Visible = True
    '                'lblDot.Visible = True
    '                lblSearchValue.Text = ddlSearchType.SelectedItem.Text
    '                btnSearch.Visible = True
    '                ddlList.Visible = True
    '                txtSearchValue.Text = ""   'HL:20070622

    '                'Get Status Record
    '                ddlList.Items.Clear()
    '                Dim objSAPQuery As mst_SAP.clsSAPQuery
    '                objSAPQuery = New mst_SAP.clsSAPQuery
    '                With objSAPQuery
    '                    dt = .GetSAPStatusList
    '                End With
    '                objSAPQuery = Nothing
    '                If dt.Rows.Count > 0 Then
    '                    ddlList.Items.Add(New ListItem("Select", 0))
    '                    For Each drRow In dt.Rows
    '                        ddlList.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
    '                    Next
    '                End If

    '            Case "txn_status"
    '                lblSearchValue.Visible = True
    '                'lblDot.Visible = True
    '                lblSearchValue.Text = ddlSearchType.SelectedItem.Text
    '                btnSearch.Visible = True
    '                ddlList.Visible = True
    '                txtSearchValue.Text = ""   'HL:20070622

    '                'Get Status Record
    '                ddlList.Items.Clear()
    '                Dim objSAPQuery As mst_SAP.clsSAPQuery
    '                objSAPQuery = New mst_SAP.clsSAPQuery
    '                With objSAPQuery
    '                    dt = .GetSAPTxnStatusList
    '                End With
    '                objSAPQuery = Nothing
    '                If dt.Rows.Count > 0 Then
    '                    ddlList.Items.Add(New ListItem("Select", 0))
    '                    For Each drRow In dt.Rows
    '                        ddlList.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
    '                    Next
    '                    ddlList.Items.Add(New ListItem("K", "K"))
    '                End If
    '            Case ""
    '                lblSearchValue.Visible = False
    '                'lblDot.Visible = True
    '                lblSearchValue.Text = ddlSearchType.SelectedItem.Text
    '                txtSearchValue.Visible = False
    '                'txtSearchValue.Text = ""
    '                btnSearch.Visible = False
    '            Case Else
    '                lblSearchValue.Visible = True
    '                'lblDot.Visible = True
    '                lblSearchValue.Text = ddlSearchType.SelectedItem.Text
    '                txtSearchValue.Visible = True
    '                'txtSearchValue.Text = ""
    '                btnSearch.Visible = True
    '        End Select

    '    Catch ex As Exception
    '        lblErr.Text = "SAPList.ddlSearchType_onChanged : " + ex.ToString()
    '    Finally

    '    End Try
    'End Sub
#End Region

    Private Sub LoadStatusDDL()
        Dim dt As DataTable
        Dim drRow As DataRow
        'Get Status Record
        ddltxnstatus.Items.Clear()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        objSAPQuery = New mst_SAP.clsSAPQuery
        With objSAPQuery
            dt = .GetSAPStatusList
        End With
        objSAPQuery = Nothing
        If dt.Rows.Count > 0 Then
            ddltxnstatus.Items.Add(New ListItem("Select", 0))
            For Each drRow In dt.Rows
                ddltxnstatus.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
            Next
        End If
    End Sub


End Class

Public Class CF_TRAOrderList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""


        Select Case ColumnName.ToUpper
            Case "TXN_NO"
                strFieldName = "Txn No."
            Case "TXN_STATUS"
                strFieldName = "Status"
            Case "VISIT_ID"
                strFieldName = "Visit ID"
            Case "LN_STATUS"
                strFieldName = "LN Status"
            Case "WF_STATUS"
                strFieldName = "WF Status"
            Case "WF_REMARKS"
                strFieldName = "WF Remarks"
            Case "VOUCHER_NO"
                strFieldName = "Voucher No."
            Case "COLL_CODE"
                strFieldName = "Collector Code"
            Case "COLL_NAME"
                strFieldName = "Collector Name"
            Case "REASON_CODE"
                strFieldName = "Reason Code"
            Case "SLOC_CODE"
                strFieldName = "Store Local Code"
            Case "SLOC_NAME"
                strFieldName = "Store Local"
            Case "CTN_NO"
                strFieldName = "Carton"
            Case "REF_NO"
                strFieldName = "Ref No."
            Case "TXN_DATE"
                strFieldName = "Txn Created Date"
            Case "&NBSP;"
                strFieldName = " "
            Case "PAYER_CODE"
                strFieldName = "Payer Code"
            Case "PAYER_NAME"
                strFieldName = "Payer"
            Case "TRA_NO"
                strFieldName = "SAP No."
            Case "LASTUPDATE"
                strFieldName = "SAP Date"
            Case "SAP_STATUS"
                strFieldName = "SAP Status"
            Case "SAP_REMARKS"
                strFieldName = "SAP Remarks"
            Case "SAP_BILLING"
                strFieldName = "SAP Billing"
            Case "CREATED_DATE"
                strFieldName = "Created Date"
            Case "CREATOR_USER_NAME"
                strFieldName = "Creator User"
            Case "CHANGED_DATE"
                strFieldName = "Changed Date"
            Case "CHANGED_USER_NAME"
                strFieldName = "Changed  User"
            Case "SUBMITTED_DATE"
                strFieldName = "Submitted Date"
            Case "SUBMITTED_USER_NAME"
                strFieldName = "Submitted User"
            Case "CANCELED_DATE"
                strFieldName = "Cancelled Date"
            Case "CANCELED_USER_NAME"
                strFieldName = "Cancelled User"
            Case "CUST_CODE"
                strFieldName = "Ship To Code"
            Case "CUST_NAME"
                strFieldName = "Ship To Name"
            Case "IMG"
                strFieldName = "Image"
            Case "DIVISION"
                strFieldName = "Division"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            strColumnName = strColumnName.ToUpper
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn

            If strColumnName Like "VISIT_ID" Then
                FCT = FieldColumntype.InvisibleColumn
            ElseIf strColumnName = "TXN_NO" Then
                FCT = FieldColumntype.HyperlinkColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            Select Case strColumnName.ToUpper
                Case "TXN_DATE"
                    strFormatString = "{0:yyyy-MM-dd hh:mm tt}"
                Case "QTY"
                    strFormatString = "{0:#,0}"
                Case "VISIT_FREQ"
                    strFormatString = "{0:0.#}"
                Case "AVGTOT"
                    strFormatString = "{0:#,0.00}"
                Case Else
                    strFormatString = ""
            End Select

            If strColumnName.ToUpper Like "*DATE" And strColumnName.ToUpper <> "TXN_DATE" Then
                strFormatString = "{0:yyyy-MM-dd hh:mm tt}"
            End If
        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" Or strColumnName Like "*_DATE" Or strColumnName Like "*_STATUS" Or strColumnName = "REF_NO" Or strColumnName = "VOUCHER_NO" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf strColumnName Like "*_AMT" Or strColumnName Like "*_TOTAL" Or strColumnName = "CTN_NO" Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class