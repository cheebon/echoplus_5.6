Imports System.Data
Imports System.IO

Partial Class DataFilesList
    Inherits System.Web.UI.Page
#Region "Global Variable"
    Private intPageSize As Integer
    Private Property dtTable() As DataTable
        Get
            Return CType(ViewState("dtCurrentView"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtCurrentView") = value
        End Set
    End Property
    Private ReadOnly Property strFilePath() As String
        Get
            Return ConfigurationManager.AppSettings("DataFilePath")
        End Get
    End Property
    Public ReadOnly Property PageName() As String
        Get
            Return "DataFileList.aspx"
        End Get
    End Property
    Private Property SortExpression() As String
        Get
            Return CType(ViewState("strSortExpression"), String)
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Public Property FilterString() As String
        Get
            Return CType(ViewState("FilterString"), String)
        End Get
        Set(ByVal value As String)
            ViewState("FilterString") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))
            lblErr.Text = String.Empty
            If Not IsPostBack Then
                With wuc_lblheader
                    .Title = Report.GetName(SubModuleType.DATA) '"Data Files List"
                    .DataBind()
                    .Visible = True
                End With
                'Call Paging
                With wuc_dgpaging
                    .PageCount = dgList.PageCount
                    .CurrentPageIndex = dgList.PageIndex
                    .DataBind()
                    .Visible = True
                End With
                'dtTable = Nothing
                'BindGrid(ViewState("SortCol"))
                RenewDataBind()
                'TimerControl1.Enabled = True

            End If

        Catch ex As Exception
            ExceptionMsg("DataFilesList.Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "Event Handle"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strFiltering As String = Trim(txtDataFileName.Text)
        FilterString = "'" & IIf(strFiltering.Contains("*"), strFiltering, "*" & strFiltering & "*") & "'"
        RefreshDatabinding()
    End Sub

#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub
#End Region

#Region "Data Binding"
    Public Sub RenewDataBind()
        dtTable = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Public Sub RefreshDatabinding()
        Dim dtCurrentTable As DataTable = dtTable

        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                SortExpression = Nothing
                dtTable = dtCurrentTable
                dgList.PageIndex = 0
            End If
            'PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New Data.DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(SortExpression) Then
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(SortExpression.Replace(" DESC", "")), SortExpression, "")
            End If
            If Not String.IsNullOrEmpty(FilterString) Then
                dvCurrentView.RowFilter = "REPORT_NAME LIKE " & FilterString
            End If

            dgList.DataSource = dvCurrentView
            dgList.PageSize = intPageSize
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
            End With

            wuc_dgpaging.Visible = IIf(dgList.Rows.Count = 0, False, True)
        Catch exv As EvaluateException
            lblErr.Text = "Invalid Search, the character '*' or '%' does not allow to specific in the middle of search pattern.<br/>Please specific it infront or behind the word.<br/>Eg:*DataFileName or DataFilename* or *DataFileName*"
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            'UpdateDatagrid_Update()
            dtCurrentTable = Nothing
        End Try
    End Sub
#End Region

#Region "Custom dgList"
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim imgBtn As ImageButton = CType(e.Row.Cells(0).FindControl("imgDataFile"), ImageButton)
            Dim imgBtnPivot As ImageButton = CType(e.Row.Cells(1).FindControl("imgPivotFile"), ImageButton)
            Dim iDataKey As Integer
            iDataKey = Convert.ToInt32(Trim(dgList.DataKeys(e.Row.RowIndex).Value))

            'Dim dtTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
            Dim drRow As DataRow

            If imgBtn IsNot Nothing Then
                'imgBtn.OnClientClick = "javascript:__doPostBack('dgList','DATA$" & e.Row.RowIndex & "')"
                'imgBtn.CommandArgument = e.Row.RowIndex

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    drRow = IIf(dtTable.Select("ID=" & iDataKey) IsNot Nothing, dtTable.Select("ID=" & iDataKey)(0), Nothing)
                    If drRow IsNot Nothing Then
                        If (drRow("DATA_FILE_DATE").Equals("N/A")) Then
                            imgBtn.ImageUrl = "~/images/Ico_Excel_Data_X.gif"
                            imgBtn.Enabled = False
                        Else
                            'imgBtn.Attributes.Add("Onclick", "javascript:__doPostBack('dgRpt','DATA$" & e.Row.RowIndex & "')")
                            imgBtn.CssClass = "g_img g_link"
                            If (Portal.Util.GetValue(Of String)(drRow("DATA_FILE_NAME")) Like "*.csv") Then
                                imgBtn.ImageUrl = "~/images/ico_CSV_Data.png"
                            ElseIf (Portal.Util.GetValue(Of String)(drRow("DATA_FILE_NAME")) Like "*.xls") Then
                                imgBtn.ImageUrl = "~/images/ico_EXCEL_Data.gif"
                            ElseIf (Portal.Util.GetValue(Of String)(drRow("DATA_FILE_NAME")) Like "*.zip") Then
                                imgBtn.ImageUrl = "~/images/ico_zip_Data.png"
                            End If
                        End If
                    End If
                End If
            End If

            If imgBtnPivot IsNot Nothing Then
                'imgBtnPivot.OnClientClick = "javascript:__doPostBack('dgList','PIVOT$" & e.Row.RowIndex & "')"
                imgBtnPivot.CommandArgument = e.Row.RowIndex

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    drRow = IIf(dtTable.Select("ID=" & iDataKey) IsNot Nothing, dtTable.Select("ID=" & iDataKey)(0), Nothing)
                    If drRow IsNot Nothing Then
                        If (drRow("PIVOT_FILE_DATE").Equals("N/A")) Then
                            imgBtnPivot.ImageUrl = "~/images/Ico_Excel_Data_X.gif"
                            imgBtnPivot.Enabled = False
                        Else
                            imgBtnPivot.ImageUrl = "~/images/Ico_Excel_Pivot.gif"
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
        Dim index As Integer
        Dim iDataKey As Integer
        Dim strPath, strFileName As String
        strPath = strFilePath 'ConfigurationManager.AppSettings("DataFilePath") '"Documents\iFFMR\DataFiles\"
        strFileName = ""

        Try
            Select Case e.CommandName
                Case "DATA"
                    index = Convert.ToInt32(e.CommandArgument)
                    index = index - (intPageSize * wuc_dgpaging.CurrentPageIndex)

                    iDataKey = Convert.ToInt32(Trim(dgList.DataKeys(index).Value))

                    Dim drRow As DataRow
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        drRow = IIf(dtTable.Select("ID=" & iDataKey) IsNot Nothing, dtTable.Select("ID=" & iDataKey)(0), Nothing)
                        If drRow IsNot Nothing Then
                            strFileName = drRow("DATA_FILE_NAME")
                            strPath = Trim(drRow("DATA_FILE_PATH"))
                            downloadFile(strPath, strFileName)
                        End If
                    End If

                Case "PIVOT"
                    index = Convert.ToInt32(e.CommandArgument)
                    'index = index - (intPageSize * wuc_dgpaging.CurrentPageIndex)

                    iDataKey = Convert.ToInt32(Trim(dgList.DataKeys(index).Value))

                    Dim dt As DataTable = CType(dtTable, DataTable)
                    Dim drRow As DataRow
                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                        drRow = IIf(dt.Select("ID=" & iDataKey) IsNot Nothing, dt.Select("ID=" & iDataKey)(0), Nothing)
                        If drRow IsNot Nothing Then
                            strFileName = drRow("PIVOT_FILE_NAME")
                            strPath = Trim(drRow("PIVOT_FILE_PATH"))
                            downloadFile(strPath, strFileName)
                        End If
                    End If
            End Select
        Catch tae As System.Threading.ThreadAbortException

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCommand : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub downloadFile(ByVal strPath As String, ByVal strFileName As String)
        Try
            Dim fs As FileStream
            fs = File.Open(strPath, FileMode.Open)
            Dim bytBytes(fs.Length) As Byte
            fs.Read(bytBytes, 0, fs.Length)

            ' Close the file stream to release the resource, if not close the resource the next person cnt download our file 
            fs.Close()
            With Response
                .Clear()
                .AddHeader("content-disposition", "attachment;filename=" & strFileName)
                .Charset = ""
                .ContentType = "application/vnd.xls"
                .BinaryWrite(bytBytes)
            End With
            Response.End()
        Catch ex As Exception
            ExceptionMsg(PageName & ".downloadFile : " & ex.ToString)
        End Try
    End Sub
    'Private Function GetContentType()
    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strCountryId, strPrincipalId, strUserCode As String
            strCountryId = Session("COUNTRY_ID")
            strPrincipalId = Session("PRINCIPAL_ID")
            strUserCode = Trim(Session("UserCode"))   'HL:20070801

            Dim clsDataFileDB As New rpt_DataFile.clsDataFileQuery()
            DT = clsDataFileDB.GetDataFileList(strCountryId, strPrincipalId, strUserCode)

            If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            FillFilePath(DT)
            UpdateDT(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub FillFilePath(ByRef dt As DataTable)
        Try
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each DR As DataRow In dt.Rows
                    'DR("DATA_FILE_PATH") = SetFilePath(Request.PhysicalApplicationPath & "\" & strFilePath & Trim(DR("DATA_FILE_PATH")) & "\" & DR("DATA_FILE_NAME"), DR)
                    SetFilePath(DR)
                    DR("PIVOT_FILE_PATH") = Request.PhysicalApplicationPath & "\" & strFilePath & Trim(DR("PIVOT_FILE_PATH")) & "\" & DR("PIVOT_FILE_NAME")
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".FillFilePath : " & ex.ToString)
        End Try
    End Sub
    Private Function SetFilePath(ByRef dr As DataRow) As String
        Dim strTmpFilePath As String = Request.PhysicalApplicationPath & "\" & strFilePath & Trim(dr("DATA_FILE_PATH")) & "\" & dr("DATA_FILE_NAME")
        Try
            If (My.Computer.FileSystem.FileExists(strTmpFilePath.Replace(".xls", ".zip"))) Then
                strTmpFilePath = strTmpFilePath.Replace(".xls", ".zip")
                dr("DATA_FILE_NAME") = dr("DATA_FILE_NAME").ToString.Replace(".xls", ".zip")
                dr("DATA_FILE_PATH") = strTmpFilePath.Replace(".xls", ".zip")
            ElseIf (My.Computer.FileSystem.FileExists(strTmpFilePath.Replace(".xls", ".csv"))) Then
                dr("DATA_FILE_NAME") = dr("DATA_FILE_NAME").ToString.Replace(".xls", ".csv")
                dr("DATA_FILE_PATH") = strTmpFilePath.Replace(".xls", ".csv")
            ElseIf (My.Computer.FileSystem.FileExists(strTmpFilePath)) Then
                dr("DATA_FILE_PATH") = strTmpFilePath
            End If
        Catch ex As Exception
        End Try
        Return strTmpFilePath
    End Function

    Private Sub UpdateDT(ByRef DT As DataTable)
        Try
            Dim strDataPath As String
            Dim strPivotPath As String
            Dim strDateFormat As String = "{0:yyyy-MM-dd}"

            If DT IsNot Nothing AndAlso DT.Rows.Count > 0 Then
                For Each DR As DataRow In DT.Rows
                    'strDataPath = Request.PhysicalApplicationPath & "\" & (DR("DATA_FILE_PATH"))
                    'strPivotPath = Request.PhysicalApplicationPath & "\" & (DR("PIVOT_FILE_PATH"))
                    strDataPath = (DR("DATA_FILE_PATH"))
                    strPivotPath = (DR("PIVOT_FILE_PATH"))

                    If (My.Computer.FileSystem.FileExists(strDataPath)) Then
                        DR("DATA_FILE_DATE") = String.Format(strDateFormat, My.Computer.FileSystem.GetFileInfo(strDataPath).LastWriteTime)
                    ElseIf (My.Computer.FileSystem.FileExists(strDataPath.Replace(".xls", ".csv"))) Then
                        DR("DATA_FILE_PATH") = strDataPath.Replace(".xls", ".csv")
                        DR("DATA_FILE_DATE") = String.Format(strDateFormat, My.Computer.FileSystem.GetFileInfo(strDataPath).LastWriteTime)
                    ElseIf (My.Computer.FileSystem.FileExists(strDataPath.Replace(".xls", ".zip"))) Then
                        DR("DATA_FILE_PATH") = strDataPath.Replace(".xls", ".zip")
                        DR("DATA_FILE_DATE") = String.Format(strDateFormat, My.Computer.FileSystem.GetFileInfo(strDataPath).LastWriteTime)
                    Else
                        DR("DATA_FILE_DATE") = "N/A"
                    End If

                    If (My.Computer.FileSystem.FileExists(strPivotPath)) Then
                        DR("PIVOT_FILE_DATE") = String.Format(strDateFormat, My.Computer.FileSystem.GetFileInfo(strPivotPath).LastWriteTime)
                    Else
                        DR("PIVOT_FILE_DATE") = "N/A"
                    End If
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateFileDate : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortExpression
        Try
            If Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression Like ("* DESC") Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            SortExpression = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    'Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If TimerControl1.Enabled Then
    '        TimerControl1.Enabled = False
    '        TimerControl1.Dispose()
    '        RenewDataBind()
    '    End If
    '    TimerControl1.Enabled = False
    'End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class