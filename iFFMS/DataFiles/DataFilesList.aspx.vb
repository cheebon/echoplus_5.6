'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	User List
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On

Imports System.Data
Imports System.IO

Partial Class iFFMS_DataFiles_DataFilesList
    Inherits System.Web.UI.Page

    Private strFilePath As String = System.AppDomain.CurrentDomain.BaseDirectory & "\Documents\iFFMS\DataFiles\"

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub Page_Load
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lnkFolder As LinkButton

        Try
            'Put user code to initialize the page here
            'Call Header
            With wuc_lblheader
                .Title = "Data Files List"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                'Populate Folder
                PopulateFolder()

                If dgFolderList.SelectedIndex = -1 Then dgFolderList.SelectedIndex = 0
                lnkFolder = CType(dgFolderList.Rows(dgFolderList.SelectedIndex).Cells(1).Controls(0), LinkButton)

                'Populate File
                PopulateFile(strFilePath & lnkFolder.Text, 1)
            End If

        Catch ex As Exception
            ExceptionMsg("DataFilesList.Page_Load : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub PopulateFolder
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub PopulateFolder()
        Dim dtFolder As New DataTable("dtFolder")
        Dim drRow As DataRow = Nothing

        Try
            If dgFolderList.Rows.Count = 0 Then
                Dim dcFolderID As New DataColumn
                dcFolderID.ColumnName = "folder_id"
                dcFolderID.DataType = GetType(Long)
                dtFolder.Columns.Add(dcFolderID)
                dcFolderID.Dispose()

                Dim dcFolderName As New DataColumn
                dcFolderName.ColumnName = "folder_name"
                dcFolderName.DataType = GetType(String)
                dtFolder.Columns.Add(dcFolderName)
                dcFolderName.Dispose()

                drRow = dtFolder.NewRow
                drRow("folder_id") = 1
                drRow("folder_name") = "Download"
                dtFolder.Rows.Add(drRow)
              
            End If

            With dgFolderList
                .DataSource = dtFolder
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg("DataFilesList.PopulateFolder : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub PopulateFile
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub PopulateFile(Optional ByVal strPath As String = "", Optional ByVal dblFolderID As Double = 0)
        Dim dtFile As New DataTable("dtFile")
        Dim drRow As DataRow = Nothing
        Dim i As Integer = 0

        Try
            'Populate Header
            'If dgFileList.Rows.Count = 0 Then
            Dim dcFolderID As New DataColumn
            dcFolderID.ColumnName = "folder_id"
            dcFolderID.DataType = GetType(Long)
            dtFile.Columns.Add(dcFolderID)
            dcFolderID.Dispose()

            Dim dcFileID As New DataColumn
            dcFileID.ColumnName = "file_id"
            dcFileID.DataType = GetType(Long)
            dtFile.Columns.Add(dcFileID)
            dcFileID.Dispose()

            Dim dcFileName As New DataColumn
            dcFileName.ColumnName = "file_name"
            dcFileName.DataType = GetType(String)
            dtFile.Columns.Add(dcFileName)
            dcFileName.Dispose()

            Dim dcFilePath As New DataColumn
            dcFilePath.ColumnName = "file_path"
            dcFilePath.DataType = GetType(String)
            dtFile.Columns.Add(dcFilePath)
            dcFilePath.Dispose()

            Dim dcDateChanged As New DataColumn
            dcDateChanged.ColumnName = "date_changed"
            dcDateChanged.DataType = GetType(String)
            dtFile.Columns.Add(dcDateChanged)
            dcDateChanged.Dispose()
            'End If

            'Get Files
            If strPath <> "" Then
                Dim objDirInfo As New System.IO.DirectoryInfo(strPath)
                Dim objFileInfo As System.IO.FileInfo()
                Dim objFileInfoTemp As System.IO.FileInfo
                Dim strFileName As String

                If objDirInfo.Exists Then
                    objFileInfo = objDirInfo.GetFiles("*")
                    For Each objFileInfoTemp In objFileInfo
                        strFileName = objFileInfoTemp.Name

                        drRow = dtFile.NewRow
                        drRow("folder_id") = dblFolderID
                        drRow("file_id") = i
                        drRow("file_name") = strFileName
                        drRow("file_path") = strPath '& "\" & strFileName
                        drRow("date_changed") = objFileInfoTemp.LastWriteTime.ToString("dd/MM/yyyy hh:mm:ss")
                        dtFile.Rows.Add(drRow)
                        i = i + 1
                    Next objFileInfoTemp
                End If

                objDirInfo = Nothing
                objFileInfo = Nothing
                objFileInfoTemp = Nothing
            End If

            'Bind Grid
            If dtFile.Rows.Count < 1 Then
                dtFile.Rows.Add(dtFile.NewRow)
            End If

            With dgFileList
                .DataSource = dtFile
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg("DataFilesList.PopulateFolder : " & ex.ToString)
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub dgFolderList_RowCommand
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub dgFolderList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgFolderList.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgFolderList.Rows(index)
        Dim lnk As LinkButton
        Dim strPath As String = ""

        Try
            Select Case e.CommandName
                Case "Select"
                    lnk = CType(row.Cells(1).Controls(0), LinkButton)

                    strPath = strFilePath & Trim(lnk.Text)
                    PopulateFile(strPath, dgFolderList.DataKeys(row.RowIndex).Value)
            End Select


        Catch ex As Exception
            ExceptionMsg("DataFilesList.dgFolderList_RowCommand : " & ex.ToString)
        Finally

        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub dgFileList_RowCommand
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Protected Sub dgFileList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgFileList.RowCommand
        Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        Dim row As GridViewRow = dgFileList.Rows(index)
        Dim lnk, lnkFolder As LinkButton
        Dim fs As FileStream
        Dim strPath As String
        Dim strFileName As String

        Try
            Select Case e.CommandName
                Case "Select"
                    If dgFolderList.SelectedIndex = -1 Then dgFolderList.SelectedIndex = 0
                    lnkFolder = CType(dgFolderList.Rows(dgFolderList.SelectedIndex).Cells(1).Controls(0), LinkButton)
                    lnk = CType(row.Cells(2).Controls(0), LinkButton)

                    strFileName = Trim(lnk.Text)
                    strPath = strFilePath & Trim(lnkFolder.Text) & "\" & strFileName 'row.Cells(3).Text

                    fs = File.Open(strPath, FileMode.Open)
                    Dim bytBytes(fs.Length) As Byte
                    fs.Read(bytBytes, 0, fs.Length)

                    ' Close the file stream to release the resource, if not close the resource the next person cnt download our file 
                    fs.Close()
                    With Response
                        .Clear()
                        .AddHeader("content-disposition", "attachment;filename=" & strFileName)
                        .Charset = ""
                        .ContentType = "application/octet-stream"
                        .BinaryWrite(bytBytes)
                        If Not IsNothing(Response) Then
                            .End()
                        End If
                    End With

            End Select

        Catch ex As Exception
            ExceptionMsg("DataFilesList.dgFileList_RowCommand : " & ex.ToString)
        Finally
            fs = Nothing
        End Try
    End Sub

    '---------------------------------------------------------------------
    ' Procedure 	: 	Sub ExceptionMsg
    ' Purpose	    :	
    ' Parameters    :	[in] sender: 
    '   		        [in]      e: -
    '		            [out]      : 
    '----------------------------------------------------------------------
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
