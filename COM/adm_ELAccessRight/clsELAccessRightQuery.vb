﻿
Option Explicit On
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Data

''' <summary>
'''************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-11-15
'''	Purpose	    :	Class EAsy Loader Accessright Query
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''*************************************************************************
'''  
''' </summary>
''' <remarks></remarks>
''' 
Public Class clsELAccessRightQuery
    Implements IDisposable
#Region "Internal Properties"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffma_conn")) 'cor_DB.clsConStr.FFMAConnectionString
    Public clsQuery As New clsProperties.clsQueryPpt
#End Region

    Public Function GetAccessrightList(ByVal strSortCol As String, ByVal strSortDir As String, ByVal intPageIndex As Integer, ByVal intPageSize As Integer, ByVal intIsExport As Integer) As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_ENQ"
                .addItem("search_type", clsQuery.search_type, cor_DB.clsDB.DataType.DBString)
                .addItem("search_value", clsQuery.search_value, cor_DB.clsDB.DataType.DBString)
                .addItem("user_id", clsQuery.usr_id, cor_DB.clsDB.DataType.DBString)
                .addItem("sortcol", strSortCol, cor_DB.clsDB.DataType.DBString)
                .addItem("sortdir", strSortDir, cor_DB.clsDB.DataType.DBString)
                .addItem("pageindex", intPageIndex, cor_DB.clsDB.DataType.DBInt)
                .addItem("pagesize", intPageSize, cor_DB.clsDB.DataType.DBInt)
                .addItem("isexport", intIsExport, cor_DB.clsDB.DataType.DBInt)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetModuleList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_CAT_LIST"
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetSubModuleListAction() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_MST_EL_HDR_GETACTION"
                .addItem("accessright_id", clsQuery.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                '.addItem("principal_id", clsQuery.principal_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("cat_code", clsQuery.cat_code, cor_DB.clsDB.DataType.DBString)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetAccessRightDtl() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_GETDTL"
                .addItem("accessright_id", clsQuery.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function GetAccessRightDtlList() As DataTable
        Dim objDB As cor_DB.clsDB
        Dim dt As DataTable

        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strConn
                .CmdText = "SPP_ADM_ELACCESSRIGHT_GETDTLLIST"
                .addItem("accessright_id", clsQuery.accessright_id, cor_DB.clsDB.DataType.DBDouble)
                .addItem("type", clsQuery.type, cor_DB.clsDB.DataType.DBInt)
                dt = .spRetrieve
            End With
            Return dt

        Catch ex As Exception
            Throw (New ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

#Region "Internal Functions"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.SeverityID = 4
                ' .clsProperties.Exception = msg
                .Log()
            End With
            objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region
End Class
