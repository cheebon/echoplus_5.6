<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlSalesTeam.ascx.vb" Inherits="include_wuc_ddlSalesTeam" %>
<asp:DropDownList id="ddlSalesTeamID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvSalesTeamID" ControlToValidate="ddlSalesTeamID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select Sales Team."></asp:CompareValidator>
