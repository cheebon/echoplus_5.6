Imports System.Data

Partial Class CustomerDealerRec
    Inherits System.Web.UI.Page


    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerDRCList"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Call Header
        With wuc_lblheader
            .Title = "Inventory Forecasting"
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            TimerControl1.Enabled = True
        End If
   
    End Sub
    Sub OnPageLoad()
        Dim strCustCode, strSalesmanCode As String
        'Put user code to initialize the page here
        Try
            strCustCode = Trim(Session("CustomerCode"))
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            bindGridView(strSalesmanCode, strCustCode)
        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            OnPageLoad()
            TimerControl1.Enabled = False
        End If

    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Try
            dt = obj.getDealerRecordDT(strSalesmanCode, strCustCode)
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                gridview1.DataSource = dtnew
                gridview1.DataBind()
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    'Sub gridview_Click(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand
    '    Dim strDocNo As String

    '    Try
    '        strDocNo = gridview1.Rows(e.CommandArgument).Cells(0).Text
    '        Response.Redirect("CustomerOpenItemDetail.aspx?docno=" + strDocNo)
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".gridview_Click : " & ex.ToString)
    '    Finally

    '    End Try

    'End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class



