﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports System.IO
Imports System.Text.RegularExpressions

Partial Class iFFMA_Utility_EasyLoader_EasyLoader
    Inherits System.Web.UI.Page

    Private intPageSize As Integer

    Public ReadOnly Property PageName() As String
        Get
            Return "Easy Loader"
        End Get
    End Property

    Public Property Action() As String
        Get
            Return Session("Action")
        End Get
        Set(ByVal value As String)
            Session("Action") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.EASYLOADER)
            .DataBind()
            .Visible = True
        End With

        'Toolbar
        With wuc_toolbar
            .SubModuleID = SubModuleType.EASYLOADER
            If Not IsPostBack Then
                .DataBind()
            End If
            .Visible = True
        End With

        If Not IsPostBack Then
            btnUpload.Visible = False
            wuc_dgpaging.Visible = False

            LoadDDL()
        End If

        lblMsg.Text = ""
    End Sub

    Private ReadOnly Property strFilePath() As String
        Get
            Return ConfigurationManager.AppSettings("DataTemplatePath")
        End Get
    End Property

#Region "EVENTS"
    Protected Sub ddlUploadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUploadType.SelectedIndexChanged
        Dim dtUploadType As DataTable
        Dim clsEasyLoader As mst_Utility.clsEasyLoader
        clsEasyLoader = New mst_Utility.clsEasyLoader

        Try
            resetDDL()
            
            dtUploadType = ViewState("dtUploadType")

            If ddlUploadType.SelectedValue <> "" Then
                Dim drCurrRow As DataRow()
                drCurrRow = dtUploadType.Select("EL_CODE = '" & ddlUploadType.SelectedValue & "'")

                If drCurrRow.Length > 0 Then

                    If drCurrRow(0)("COLUMN_HDR").ToString = "" Then
                        proceedValidation(False)
                    Else
                        proceedValidation(True)

                        lblDesc.Text = drCurrRow(0)("EL_DESC").ToString
                        lblColumns.Text = "<ol style='margin-top: 5px; margin-bottom: 0px'>" & drCurrRow(0)("COLUMN_HDR").ToString & "</ol>"
                    End If
                End If

                'dtDesc = clsEasyLoader.GetELDesc(ddlUploadType.SelectedValue)
                'If dtDesc.Rows.Count > 0 Then
                '    lblDesc.Text = dtDesc.Rows(0)("EL_DESC")
                'End If

                'dtColumnHdr = clsEasyLoader.GetELDtl(ddlUploadType.SelectedValue)
                'strColumnHdr = "<ol style='margin-top: 5px; margin-bottom: 0px'>"
                'For Each DR As DataRow In dtColumnHdr.Rows
                '    strColumnHdr = strColumnHdr & "<li>" & DR("COLUMN_CODE")
                'Next
                'strColumnHdr = strColumnHdr & "</ol>"
                'lblColumns.text = strColumnHdr
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim clsEasyLoader As mst_Utility.clsEasyLoader

        Try
            Action = "Upload"

            clsEasyLoader = New mst_Utility.clsEasyLoader
            clsEasyLoader.InsertELAuditLog(ddlUploadType.SelectedValue)

            clsEasyLoader = New mst_Utility.clsEasyLoader
            clsEasyLoader.Upload(ddlUploadType.SelectedValue)

            RenewDataBind()

            btnUpload.Visible = False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidate.Click

        Action = "Validate"
        lblMsg.Text = ""
        pnlFileError.Visible = False

        If fuLocation.HasFile Then
            Dim tmpExt As String = fuLocation.FileName.Split(".", 3, StringSplitOptions.RemoveEmptyEntries)(1)

            Try
                If tmpExt.ToUpper = "CSV" Then
                    fuLocation.SaveAs(Server.MapPath("~/Documents/CSVImport" + Session("userid") + "." + tmpExt))

                    lblUploadInfo.Text = "Upload Type : " & ddlUploadType.SelectedItem.Text & "<br>" & _
                                         "Upload File Name : " & fuLocation.PostedFile.FileName & "<br>" & _
                                         "Type : " & fuLocation.PostedFile.ContentType & "<br>" & _
                                         "File Size : " & fuLocation.PostedFile.ContentLength & " kb<br>"

                    If tmpExt.ToUpper = "CSV" Or tmpExt.ToUpper = "TXT" Then
                        LoadFile("~/Documents/CSVImport" + Session("userid") + "." + tmpExt)
                    End If

                    ShowResult(True)
                Else
                    lblMsg.Text = "Kindly upload csv files only."
                    'lblTtlRecords.Visible = False
                End If

            Catch ex As Exception
                lblMsg.Text = "Error: " & ex.ToString & Server.MapPath("~/Documents/CSVImport" + Session("userid") + "." + tmpExt)
            Finally
                Dim filePath = Server.MapPath("~/Documents/CSVImport" + Session("userid") + "." + tmpExt)
                System.IO.File.Delete(filePath)
            End Try
        Else
            lblMsg.Text = "Please select a file to upload."
            'lblTtlRecords.Visible = False
        End If

        fuLocation.Dispose()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim clsEasyLoader As mst_Utility.clsEasyLoader

        Try
            Action = Nothing
            lblErr.Text = ""
            resetList()

            clsEasyLoader = New mst_Utility.clsEasyLoader
            clsEasyLoader.Delete(ddlUploadType.SelectedValue)

            ShowResult(False)
            LoadDDL()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btndltemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndltemplate.Click
        Try

            Dim strPath As String = Request.PhysicalApplicationPath & strFilePath + GetFileName()
            Dim strFileName As String = GetFileName()

            Dim success As Boolean = downloadFile(strPath, strFileName)
            If Not success Then
                lblMsg.Text = "Template do not exists!"
                UpdateDatagrid.Update()
            End If

            'Page.Response.Clear()
            'Dim success As Boolean = ResponseFile(Page.Request, Page.Response, strFileName, strPath, 1024000)
            'If Not success Then
            '    lblMsg.Text = "Template do not exists!"
            '    UpdateDatagrid.Update()
            'End If
            'Page.Response.[End]()


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function downloadFile(ByVal strPath As String, ByVal strFileName As String) As Boolean
        Try
            Dim fs As FileStream
            fs = File.Open(strPath, FileMode.Open)
            Dim bytBytes(fs.Length) As Byte
            fs.Read(bytBytes, 0, fs.Length)

            ' Close the file stream to release the resource, if not close the resource the next person cnt download our file 
            fs.Close()
            With Response
                .Clear()
                .AddHeader("content-disposition", "attachment;filename=" & strFileName)
                .Charset = ""
                '.ContentType = "application/vnd.xls"
                .ContentType = "application/octet-stream"
                .BinaryWrite(bytBytes)
            End With
            Response.End()
            'Catch e As Threading.ThreadAbortException

        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
#End Region

#Region "FUNCTIONS"
    Private Sub LoadDDL()
        Dim dtUploadType As DataTable
        Dim clsEasyLoader As New mst_Utility.clsEasyLoader

        Try
            dtUploadType = clsEasyLoader.GetELList()
            With ddlUploadType
                .Items.Clear()
                .DataSource = dtUploadType.DefaultView
                .DataTextField = "EL_NAME"
                .DataValueField = "EL_CODE"
                .DataBind()
                .Items.Insert(0, New ListItem("-- SELECT --", ""))
                .SelectedIndex = 0
            End With

            dtUploadType.Columns.Add("COLUMN_HDR")

            For Each DR As DataRow In dtUploadType.Rows
                Dim dtColumnHdr As DataTable
                Dim strColumnHdr As String = ""
                dtColumnHdr = clsEasyLoader.GetELDtl(DR("EL_CODE"))
                For Each DR2 As DataRow In dtColumnHdr.Rows
                    strColumnHdr = strColumnHdr & "<li>" & DR2("COLUMN_CODE")
                Next

                DR("COLUMN_HDR") = strColumnHdr
            Next
            
            ViewState("dtUploadType") = dtUploadType
            resetDDL()

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ShowResult(ByVal boolVisible As Boolean)
        pnlValidate.Visible = Not boolVisible
        pnlUpload.Visible = boolVisible
    End Sub

    Private Sub LoadFile(ByVal strFilePathName As String)
        Dim dt As DataTable = ParseCSVFile(Server.MapPath(strFilePathName))
        Dim dtColumnHdr As DataTable
        Dim clsEasyLoader As mst_Utility.clsEasyLoader
        Dim aryColumnHdr, aryColumn As ArrayList

        Try
            clsEasyLoader = New mst_Utility.clsEasyLoader
            clsEasyLoader.Delete(ddlUploadType.SelectedValue) 'Clear temporary table first

            dtColumnHdr = clsEasyLoader.GetELDtl(ddlUploadType.SelectedValue)

            aryColumnHdr = New ArrayList
            For Each DR As DataRow In dtColumnHdr.Rows
                aryColumnHdr.Add(DR("COLUMN_CODE"))
            Next

            'Check header column name
            For i As Integer = 0 To aryColumnHdr.Count - 1
                If (dt.Rows(0).Item(i).ToString.ToUpper <> aryColumnHdr.Item(i).ToString.ToUpper) Then
                    pnlFileError.Visible = True
                    Exit Sub
                End If
            Next

            For i = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item(0).ToString.ToUpper Like "*" & aryColumnHdr.Item(0).ToString.ToUpper & "*" Then
                    'Skip header row if detected
                Else
                    aryColumn = New ArrayList
                    For j As Integer = 0 To dt.Columns.Count - 1
                        aryColumn.Insert(j, dt.Rows(i).Item(j))
                    Next

                    If aryColumnHdr.Count = aryColumn.Count Then
                        clsEasyLoader.Insert(ddlUploadType.SelectedValue, aryColumnHdr, aryColumn) 'insert into temp table
                    Else
                        pnlFileError.Visible = True
                        Exit Sub
                    End If
                End If
            Next

            clsEasyLoader.Validate(ddlUploadType.SelectedValue)
            RenewDataBind()

            'btnUpload.Visible = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function ParseCSV(ByVal inputString As String) As DataTable
        Dim dt As New DataTable()

        ' declare the Regular Expression that will match versus the input string 
        Dim re As New Regex("((?<field>[^"",\r\n]+)|""(?<field>([^""]|"""")+)"")(,|(?<rowbreak>\r\n|\n|$))")

        Dim colArray As New ArrayList()
        Dim rowArray As New ArrayList()

        Dim colCount As Integer = 0
        Dim maxColCount As Integer = 0
        Dim rowbreak As String = ""
        Dim field As String = ""

        Dim mc As MatchCollection = re.Matches(inputString)

        For Each m As Match In mc

            ' retrieve the field and replace two double-quotes with a single double-quote 
            field = m.Result("${field}").Replace("""""", """")

            rowbreak = m.Result("${rowbreak}")

            If field.Length > 0 Then
                colArray.Add(field)
                colCount += 1
            End If

            If rowbreak.Length > 0 Then

                ' add the column array to the row Array List 
                rowArray.Add(colArray.ToArray())

                ' create a new Array List to hold the field values 
                colArray = New ArrayList()

                If colCount > maxColCount Then
                    maxColCount = colCount
                End If

                colCount = 0
            End If
        Next

        If rowbreak.Length = 0 Then
            ' this is executed when the last line doesn't end with a line break 
            rowArray.Add(colArray.ToArray())
            If colCount > maxColCount Then
                maxColCount = colCount
            End If
        End If

        ' create the columns for the table 
        For i As Integer = 0 To maxColCount - 1
            dt.Columns.Add([String].Format("col{0:000}", i))
        Next

        ' convert the row Array List into an Array object for easier access 
        Dim ra As Array = rowArray.ToArray()
        For i As Integer = 0 To ra.Length - 1

            ' create a new DataRow 
            Dim dr As DataRow = dt.NewRow()

            ' convert the column Array List into an Array object for easier access 
            Dim ca As Array = DirectCast((ra.GetValue(i)), Array)

            ' add each field into the new DataRow 
            For j As Integer = 0 To ca.Length - 1
                dr(j) = ca.GetValue(j)
            Next

            ' add the new DataRow to the DataTable 
            dt.Rows.Add(dr)
        Next

        If dt.Columns.Count = 0 Then
            dt.Columns.Add("NoData")
        End If

        Return dt
    End Function

    Private Function ParseCSVFile(ByVal path As String) As DataTable
        Dim inputLine As String = ""
        Dim inputString As String = ""

        Dim writer As New StringWriter()

        If File.Exists(path) Then   'check that the file exists before opening it 
            Dim sr As New StreamReader(path)
            'inputString = sr.ReadToEnd()

            While Not sr.EndOfStream
                inputLine = sr.ReadLine

                inputLine = inputLine.Replace(",,", ", ,").Replace(",,", ", ,")

                If inputLine.EndsWith(",") Then
                    inputLine = inputLine.Insert(inputLine.Length(), " ")
                End If

                If inputLine.StartsWith(",") Then
                    inputLine = inputLine.Insert(0, " ")
                End If

                writer.WriteLine(inputLine)
            End While

            sr.Close()
        End If

        Return ParseCSV(writer.ToString())
    End Function

    Private Sub proceedValidation(ByVal boolProceed As Boolean)
        If boolProceed Then
            pnlDesc.Visible = True
            pnlSystemError.Visible = False
            btnValidate.Enabled = True
        Else
            pnlDesc.Visible = False
            pnlSystemError.Visible = True
            btnValidate.Enabled = False
        End If
    End Sub

    Private Sub resetDDL()
        pnlDesc.Visible = False
        lblDesc.Text = ""
        lblColumns.Text = ""

        pnlSystemError.Visible = False

        btnValidate.Enabled = True
    End Sub

    Private Sub resetList()
        dgList.DataSource = Nothing
        dgList.DataBind()

        btnUpload.Visible = False

        lblTtlRecords.Visible = False
        wuc_dgpaging.Visible = False
    End Sub

    Public Shared Function ResponseFile(ByVal _Request As HttpRequest, ByVal _Response As HttpResponse, ByVal _fileName As String, ByVal _fullPath As String, ByVal _speed As Long) As Boolean
        Try
            Dim myFile As New FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim br As New BinaryReader(myFile)
            Try
                _Response.AddHeader("Accept-Ranges", "bytes")
                _Response.Buffer = False
                Dim fileLength As Long = myFile.Length
                Dim startBytes As Long = 0

                Dim pack As Integer = 10240
                '10K bytes 
                Dim sleep As Integer = CInt(Math.Floor(CDbl(1000 * pack \ _speed))) + 1
                If _Request.Headers("Range") IsNot Nothing Then
                    _Response.StatusCode = 206
                    Dim range As String() = _Request.Headers("Range").Split(New Char() {"="c, "-"c})
                    startBytes = Convert.ToInt64(range(1))
                End If
                _Response.AddHeader("Content-Length", (fileLength - startBytes).ToString())
                If startBytes <> 0 Then
                    _Response.AddHeader("Content-Range", String.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength))
                End If
                _Response.AddHeader("Connection", "Keep-Alive")
                _Response.ContentType = "application/octet-stream"
                '_fileName = _fileName.Replace(" ", "_")
                '_Response.AddHeader("Content-Disposition", "attachment;filename=" & HttpUtility.UrlEncode(_fileName, System.Text.Encoding.UTF8))
                _Response.AddHeader("Content-Disposition", "attachment;filename=" & HttpUtility.UrlPathEncode(_fileName))

                br.BaseStream.Seek(startBytes, SeekOrigin.Begin)
                Dim maxCount As Integer = CInt(Math.Floor(CDbl((fileLength - startBytes) \ pack))) + 1

                For i As Integer = 0 To maxCount - 1
                    If _Response.IsClientConnected Then
                        _Response.BinaryWrite(br.ReadBytes(pack))
                        Threading.Thread.Sleep(sleep)
                    Else
                        i = maxCount
                    End If
                Next
            Catch
                Return False
            Finally
                br.Close()
                myFile.Close()
            End Try
        Catch
            Return False
        End Try
        Return True
    End Function

    Private Function GetFileName() As String
        Try
            Dim dt As DataTable
            Dim strFileName As String
            Dim clsEasyLoader As New mst_Utility.clsEasyLoader
            dt = clsEasyLoader.GetDLFileName(ddlUploadType.SelectedValue, Portal.UserSession.UserID)
            strFileName = Portal.Util.GetValue(Of String)(dt.Rows(0)(0))
            Return strFileName
        Catch TAE As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetFileName : " & ex.ToString)
        End Try
    End Function
#End Region

#Region "DGLIST"
    Public Sub RenewDataBind()
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDatabinding()
    End Sub

    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrentTable = GetRecList()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            End If

            dgList_Init(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .DataBind()
                .AllowSorting = IIf(isExport, False, IIf(dgList.Rows.Count > 0, True, False))
            End With

            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .RowCount = dvCurrentView.Count
                .Visible = IIf(dgList.Rows.Count > 0, True, False)
            End With

            With lblTtlRecords
                .Text = "No of row(s) effected " + CStr(dtCurrentTable.Rows.Count)
                .Visible = True
            End With

            If dtCurrentTable.Rows.Count = 0 Then
                If Action = "Validate" And pnlFileError.Visible = False Then
                    lblErrMsgValidate.Text = "There is no record in the upload file."
                ElseIf Action = "Upload" And pnlFileError.Visible = False Then
                    lblErrMsgValidate.Text = "File contains errors. Please refer 'valid_data' column for more information"
                Else
                    lblErrMsgValidate.Text = ""
                End If
                wuc_dgpaging.Visible = False
                btnUpload.Visible = False
            Else
                wuc_dgpaging.Visible = True
                lblErrMsgValidate.Text = ""

                If Action = "Validate" Then
                    btnUpload.Visible = True
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsEasyLoader As New mst_Utility.clsEasyLoader
            DT = clsEasyLoader.GetList(ddlUploadType.SelectedValue)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            dgList.Columns.Clear()

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_EasyLoader.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_EasyLoader.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_EasyLoader.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_EasyLoader.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_EasyLoader.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                End Select
            Next

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_toolbar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            wuc_toolbar.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class

Public Class CF_EasyLoader
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "VALID_DATA"
                strFieldName = "Validation Result"
            Case "VISIT_FREQ"
                strFieldName = "Visit Freq"
            Case "CUST_PRIORITY_CODE"
                strFieldName = "Cust. Priority Code"
            Case "CUST_PTL"
                strFieldName = "Cust. PTL"
            Case "CONT_PRIORITY_CODE"
                strFieldName = "Cont. Priority Code"
            Case "CONT_PTL"
                strFieldName = "Cont. PTL"
            Case "PRICE_GRP_CODE"
                strFieldName = "Price Group Code"
            Case "QTY"
                strFieldName = "Qty"
            Case "UOM_CODE"
                strFieldName = "UOM"
            Case "START_DATE"
                strFieldName = "Start Date"
            Case "END_DATE"
                strFieldName = "End Date"
            Case "FOC_PRD_CODE"
                strFieldName = "FOC Product Code"
            Case "FOC_QTY"
                strFieldName = "FOC Qty"
            Case "FOC_UOM_CODE"
                strFieldName = "FOC UOM"
            Case "FOC_TYPE"
                strFieldName = "FOC Type"
            Case "MAX_FOC_QTY"
                strFieldName = "Maximum FOC Qty"
            Case "ALLOC_QTY"
                strFieldName = "Allocate Qty"
            Case "USED_QTY"
                strFieldName = "Used Qty"
            Case "PACKAGE_CODE"
                strFieldName = "Package Code"
            Case "PACKAGE_NAME"
                strFieldName = "Package Name"
            Case "TYPE"
                strFieldName = "Type"
            Case "AMT"
                strFieldName = "Amount"
            Case "GOODS_MUL"
                strFieldName = "GOODS_MUL"
            Case "PRD_QTY"
                strFieldName = "Product Qty"
            Case "MUST_IND"
                strFieldName = "Must Indicator"
            Case "PRD_LISTPRICE"
                strFieldName = "Product List Price"
            Case "FOC_MUST_IND"
                strFieldName = "FOC Must Indicator"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*ID" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If

            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd h:mm tt}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName Like "*VALUE" OrElse strColumnName = "COND_RATE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False   'HL:20070711
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class