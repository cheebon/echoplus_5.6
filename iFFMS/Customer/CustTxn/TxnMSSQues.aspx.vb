Imports System.Data
Partial Class TxnMSSQues
    Inherits System.Web.UI.Page

    Private strMessage As String = String.Empty

#Region "Property"
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property

#End Region
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        'Call Header
        With wuc_lblHeader
            .Title = "Market Survey Questions" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()

        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TitleCode = Trim(Request.QueryString("TitleCode"))
            QuesCode = Trim(Request.QueryString("QuesCode"))
            BindQues()
            LoadDetails()
        End If




    End Sub


#Region "Function"

    Public Sub BindQues()
        Try
            Dim DT As DataTable
            Dim dtCriteria As DataTable
            Dim dvCriteria As DataView
            Dim strTypeS1, strTypeS2, strTypeS3, strTypeS4 As String
            Dim clsMSS As New txn_WebActy.clsMSS

            DT = clsMSS.GetMSSQuesAdv(TitleCode, QuesCode)

            Reset()

            If DT.Rows.Count > 0 Then
                Message = Trim(Request.QueryString("QuesCode")) + " - " + DT.Rows(0)("QUES_NAME")

                lblS1.Text = DT.Rows(0)("SUB_QUES_NAME_S1")
                lblS2.Text = DT.Rows(0)("SUB_QUES_NAME_S2")
                lblS3.Text = DT.Rows(0)("SUB_QUES_NAME_S3")
                lblS4.Text = DT.Rows(0)("SUB_QUES_NAME_S4")

                lblS1Code.Text = DT.Rows(0)("SUB_QUES_CODE_S1")
                lblS2Code.Text = DT.Rows(0)("SUB_QUES_CODE_S2")
                lblS3Code.Text = DT.Rows(0)("SUB_QUES_CODE_S3")
                lblS4Code.Text = DT.Rows(0)("SUB_QUES_CODE_S4")

                strTypeS1 = DT.Rows(0)("SUB_QUES_TYPE_S1")
                strTypeS2 = DT.Rows(0)("SUB_QUES_TYPE_S2")
                strTypeS3 = DT.Rows(0)("SUB_QUES_TYPE_S3")
                strTypeS4 = DT.Rows(0)("SUB_QUES_TYPE_S4")

                'S1------------------------------------
                If strTypeS1 = "0" Then
                    S1.Visible = False
                    S1Div.Style.Item("Display") = "None"
                ElseIf strTypeS1 = "1" Then
                    txtS1.Visible = True
                ElseIf strTypeS1 = "2" Then
                    ddlS1.Visible = True
                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S1")
                    With ddlS1
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS1 = "3" Then
                    txtDateS1.Visible = True
                ElseIf strTypeS1 = "4" Then
                    rdbListS1.Visible = True
                ElseIf strTypeS1 = "5" Then
                    'S1.Style.Item("height") = "100px"
                    dgTextS1.Visible = True

                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S1")
                    dvCriteria = dtCriteria.DefaultView
                    dgTextS1.DataSource = dvCriteria
                    dgTextS1.DataBind()
                    S1.Style.Item("height") = CStr((dtCriteria.Rows.Count * 25) + 40) + "px"
                    dtCriteria = Nothing
                ElseIf strTypeS1 = "6" Then
                    'S1.Style.Item("height") = "100px"
                    dgSelectS1.Visible = True

                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S1")
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectS1.DataSource = dvCriteria
                    dgSelectS1.DataBind()
                    S1.Style.Item("height") = CStr((dtCriteria.Rows.Count * 25) + 40) + "px"
                    dtCriteria = Nothing
                Else
                    S1Div.Style.Item("Display") = "None"
                End If

                'S2------------------------------------
                If strTypeS2 = "0" AndAlso (strTypeS1 = "5" OrElse strTypeS1 = "6") Then
                    S2.Visible = False
                    S2Div.Style.Item("Display") = "None"
                ElseIf strTypeS2 = "1" Then
                    txtS2.Visible = True
                ElseIf strTypeS2 = "2" Then
                    ddlS2.Visible = True
                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S2")
                    With ddlS2
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS2 = "3" Then
                    txtDateS2.Visible = True
                ElseIf strTypeS2 = "4" Then
                    rdbListS2.Visible = True
                ElseIf strTypeS2 = "5" Then
                    'S2.Style.Item("height") = "100px"
                    dgTextS2.Visible = True

                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S2")
                    dvCriteria = dtCriteria.DefaultView
                    dgTextS2.DataSource = dvCriteria
                    dgTextS2.DataBind()
                    S2.Style.Item("height") = CStr((dtCriteria.Rows.Count * 25) + 40) + "px"
                    dtCriteria = Nothing
                ElseIf strTypeS2 = "6" Then
                    'S2.Style.Item("height") = "100px"
                    dgSelectS2.Visible = True

                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S2")
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectS2.DataSource = dvCriteria
                    dgSelectS2.DataBind()
                    S2.Style.Item("height") = CStr((dtCriteria.Rows.Count * 25) + 40) + "px"
                    dtCriteria = Nothing
                Else
                    S2Div.Style.Item("Display") = "None"
                End If

                'S3------------------------------------
                If strTypeS3 = "0" AndAlso (strTypeS2 = "5" OrElse strTypeS2 = "6") Then
                    S3.Visible = False
                    S3Div.Style.Item("Display") = "None"
                ElseIf strTypeS3 = "1" Then
                    txtS3.Visible = True
                ElseIf strTypeS3 = "2" Then
                    ddlS3.Visible = True
                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S3")
                    With ddlS3
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS3 = "3" Then
                    txtDateS3.Visible = True
                ElseIf strTypeS3 = "4" Then
                    rdbListS3.Visible = True
                ElseIf strTypeS3 = "5" Then
                    'S3.Style.Item("height") = "100px"
                    dgTextS3.Visible = True

                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S3")
                    dvCriteria = dtCriteria.DefaultView
                    dgTextS3.DataSource = dvCriteria
                    dgTextS3.DataBind()
                    S3.Style.Item("height") = CStr((dtCriteria.Rows.Count * 25) + 40) + "px"
                    dtCriteria = Nothing
                ElseIf strTypeS3 = "6" Then
                    'S3.Style.Item("height") = "100px"
                    dgSelectS3.Visible = True

                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S3")
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectS3.DataSource = dvCriteria
                    dgSelectS3.DataBind()
                    S3.Style.Item("height") = CStr((dtCriteria.Rows.Count * 25) + 40) + "px"
                    dtCriteria = Nothing
                Else
                    S3Div.Style.Item("Display") = "None"
                End If

                'S4------------------------------------
                If strTypeS4 = "0" AndAlso (strTypeS3 = "5" OrElse strTypeS3 = "6") Then
                    S4.Visible = False
                    S4Div.Style.Item("Display") = "None"
                ElseIf strTypeS4 = "1" Then
                    txtS4.Visible = True
                ElseIf strTypeS4 = "2" Then
                    ddlS4.Visible = True
                    dtCriteria = clsMSS.GetMSSQuesAdvCriteria(TitleCode, QuesCode, "S4")
                    With ddlS4
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS4 = "3" Then
                    txtDateS4.Visible = True
                ElseIf strTypeS4 = "4" Then
                    rdbListS4.Visible = True
                Else
                    S4Div.Style.Item("Display") = "None"
                End If
            End If
            UpdatePage.Update()
            'Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub Reset()
        Message = ""

        lblS1.Text = ""
        lblS2.Text = ""
        lblS3.Text = ""
        lblS4.Text = ""

        S1.Visible = True
        S2.Visible = True
        S3.Visible = True
        S4.Visible = True

        S1.Style.Item("height") = "50px"
        S2.Style.Item("height") = "50px"
        S3.Style.Item("height") = "50px"
        S4.Style.Item("height") = "50px"

        txtS1.Visible = False
        txtS2.Visible = False
        txtS3.Visible = False
        txtS4.Visible = False

        ddlS1.Visible = False
        ddlS2.Visible = False
        ddlS3.Visible = False
        ddlS4.Visible = False

        txtDateS1.Visible = False
        txtDateS2.Visible = False
        txtDateS3.Visible = False
        txtDateS4.Visible = False

        rdbListS1.Visible = False
        rdbListS2.Visible = False
        rdbListS3.Visible = False
        rdbListS4.Visible = False

        dgTextS1.Visible = False
        dgTextS2.Visible = False
        dgTextS3.Visible = False

        dgSelectS1.Visible = False
        dgSelectS2.Visible = False
        dgSelectS3.Visible = False
    End Sub

    Public Sub LoadDetails()
        Try
            Dim dtDetails As DataTable
            Dim strSessionId As String = Trim(Request.QueryString("sessionid"))
            Dim strTxnNo As String = Trim(Request.QueryString("TXNNO"))
            Dim strUserId As String = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            Dim strTitleCode As String = Trim(Request.QueryString("TitleCode"))
            Dim strQuesCode As String = Trim(Request.QueryString("QuesCode"))
            Dim strSubQuesCode As String = Nothing, StrCriteriaCode As String = Nothing, StrExpec As String = Nothing, StrDateVal As String = Nothing, StrOptVal As String = Nothing

            Dim clsMSS As New txn_WebActy.clsMSS
            ' dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
            'dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)

            'S1--------------------------------------------------------------------
            If Not IsNothing(lblS1Code.Text) Then
                strSubQuesCode = Trim(lblS1Code.Text)
                If txtS1.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrExpec = dtDetails.Rows(0)("EXPEC")
                        txtS1.Text = StrExpec
                        StrCriteriaCode = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf ddlS1.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrCriteriaCode = dtDetails.Rows(0)("CRITERIA_CODE")
                        ddlS1.SelectedValue = StrCriteriaCode
                        StrExpec = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf txtDateS1.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)

                    If dtDetails.Rows.Count > 0 Then
                        StrDateVal = dtDetails.Rows(0)("DATE_VAL")
                        txtDateS1.Text = StrDateVal
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf rdbListS1.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrOptVal = dtDetails.Rows(0)("OPT_VAL")
                        rdbListS1.SelectedValue = StrOptVal
                        StrDateVal = Nothing
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf dgTextS1.Visible = True Then
                    If dgTextS1.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing

                        For Each DR As GridViewRow In dgTextS1.Rows
                            DK = dgTextS1.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgTextS1.DataKeys(i).Item("CRITERIA_CODE")
                                Dim txtColumnS1 As TextBox = dgTextS1.Rows(i).FindControl("txtColumnS1")

                                dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)
                                If dtDetails.Rows.Count > 0 Then
                                    StrExpec = dtDetails.Rows(0)("EXPEC")
                                    txtColumnS1.Text = StrExpec
                                    dtDetails = Nothing
                                End If

                            End If
                            i += 1
                        Next
                    End If
                ElseIf dgSelectS1.Visible = True Then
                    If dgSelectS1.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgSelectS1.Rows
                            DK = dgSelectS1.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgSelectS1.DataKeys(i).Item("CRITERIA_CODE")
                                Dim chkSelectS1 As CheckBox = dgSelectS1.Rows(i).FindControl("chkSelectS1")

                                dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)
                                If dtDetails.Rows.Count > 0 Then
                                    StrExpec = dtDetails.Rows(0)("EXPEC")
                                    If StrExpec = 1 Then
                                        chkSelectS1.Checked = True
                                    Else
                                        chkSelectS1.Checked = False
                                    End If
                                    dtDetails = Nothing
                                End If


                            End If
                            i += 1
                        Next
                    End If
                End If

            End If
            'S2--------------------------------------------------------------------
            If Not IsNothing(lblS2Code.Text) Then
                strSubQuesCode = Trim(lblS2Code.Text)
                If txtS2.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrExpec = dtDetails.Rows(0)("EXPEC")
                        txtS2.Text = StrExpec
                        StrCriteriaCode = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf ddlS2.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrCriteriaCode = dtDetails.Rows(0)("CRITERIA_CODE")
                        ddlS2.SelectedValue = StrCriteriaCode
                        StrExpec = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf txtDateS2.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrDateVal = dtDetails.Rows(0)("DATE_VAL")
                        txtDateS2.Text = StrDateVal
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If


                ElseIf rdbListS2.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrOptVal = dtDetails.Rows(0)("OPT_VAL")
                        rdbListS2.SelectedValue = StrOptVal
                        StrDateVal = Nothing
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        dtDetails = Nothing
                    End If


                ElseIf dgTextS2.Visible = True Then
                    If dgTextS2.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgTextS2.Rows
                            DK = dgTextS2.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgTextS2.DataKeys(i).Item("CRITERIA_CODE")
                                Dim txtColumnS2 As TextBox = dgTextS2.Rows(i).FindControl("txtColumnS2")

                                dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)
                                If dtDetails.Rows.Count > 0 Then
                                    StrExpec = dtDetails.Rows(0)("EXPEC")
                                    txtColumnS2.Text = StrExpec
                                    dtDetails = Nothing
                                End If

                            End If
                            i += 1
                        Next
                    End If
                ElseIf dgSelectS2.Visible = True Then
                    If dgSelectS2.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgSelectS1.Rows
                            DK = dgSelectS2.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgSelectS2.DataKeys(i).Item("CRITERIA_CODE")
                                Dim chkSelectS2 As CheckBox = dgSelectS2.Rows(i).FindControl("chkSelectS2")

                                dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)
                                If dtDetails.Rows.Count > 0 Then
                                    StrExpec = dtDetails.Rows(0)("EXPEC")
                                    If StrExpec = 1 Then
                                        chkSelectS2.Checked = True
                                    Else
                                        chkSelectS2.Checked = False
                                    End If
                                    dtDetails = Nothing
                                End If

                            End If
                            i += 1
                        Next
                    End If
                End If
            End If

            'S3--------------------------------------------------------------------
            If Not IsNothing(lblS3Code.Text) Then
                strSubQuesCode = Trim(lblS3Code.Text)
                If txtS3.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrExpec = dtDetails.Rows(0)("EXPEC")
                        txtS3.Text = StrExpec
                        StrCriteriaCode = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf ddlS3.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrCriteriaCode = dtDetails.Rows(0)("CRITERIA_CODE")
                        ddlS3.SelectedValue = StrCriteriaCode
                        StrExpec = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf txtDateS3.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrDateVal = dtDetails.Rows(0)("DATE_VAL")
                        txtDateS3.Text = StrDateVal
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If


                ElseIf rdbListS3.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrOptVal = dtDetails.Rows(0)("OPT_VAL")
                        rdbListS3.SelectedValue = StrOptVal
                        StrDateVal = Nothing
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        dtDetails = Nothing
                    End If


                ElseIf dgTextS3.Visible = True Then
                    If dgTextS3.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgTextS3.Rows
                            DK = dgTextS3.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgTextS3.DataKeys(i).Item("CRITERIA_CODE")
                                Dim txtColumnS3 As TextBox = dgTextS3.Rows(i).FindControl("txtColumnS3")

                                dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)
                                If dtDetails.Rows.Count > 0 Then
                                    StrExpec = dtDetails.Rows(0)("EXPEC")
                                    txtColumnS3.Text = StrExpec
                                    dtDetails = Nothing
                                End If

                            End If
                            i += 1
                        Next
                    End If
                ElseIf dgSelectS3.Visible = True Then
                    If dgSelectS3.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgSelectS3.Rows
                            DK = dgSelectS3.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgSelectS3.DataKeys(i).Item("CRITERIA_CODE")
                                Dim chkSelectS3 As CheckBox = dgSelectS3.Rows(i).FindControl("chkSelectS3")

                                dtDetails = clsMSS.GetTMPMSSDetailMultiAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, strSessionId)
                                If dtDetails.Rows.Count > 0 Then
                                    StrExpec = dtDetails.Rows(0)("EXPEC")
                                    If StrExpec = 1 Then
                                        chkSelectS3.Checked = True
                                    Else
                                        chkSelectS3.Checked = False
                                    End If
                                    dtDetails = Nothing
                                End If

                            End If
                            i += 1
                        Next
                    End If
                End If
            End If

            'S4--------------------------------------------------------------------
            If Not IsNothing(lblS4Code.Text) Then
                strSubQuesCode = Trim(lblS4Code.Text)
                If txtS4.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrExpec = dtDetails.Rows(0)("EXPEC")
                        txtS4.Text = StrExpec
                        StrCriteriaCode = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf ddlS4.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrCriteriaCode = dtDetails.Rows(0)("CRITERIA_CODE")
                        ddlS4.SelectedValue = StrCriteriaCode
                        StrExpec = Nothing
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If

                ElseIf txtDateS4.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrDateVal = dtDetails.Rows(0)("DATE_VAL")
                        txtDateS4.Text = StrDateVal
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        StrOptVal = Nothing
                        dtDetails = Nothing
                    End If


                ElseIf rdbListS4.Visible = True Then
                    dtDetails = clsMSS.GetTMPMSSDetailAdv(strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, strSessionId)
                    If dtDetails.Rows.Count > 0 Then
                        StrOptVal = dtDetails.Rows(0)("OPT_VAL")
                        rdbListS4.SelectedValue = StrOptVal
                        StrDateVal = Nothing
                        StrCriteriaCode = Nothing
                        StrExpec = Nothing
                        dtDetails = Nothing
                    End If


                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


    Protected Sub Save()
        Try
            Dim clsMSS As New txn_WebActy.clsMSS
            Dim strSessionId As String = Trim(Request.QueryString("sessionid"))
            Dim strTxnNo As String = Trim(Request.QueryString("TXNNO"))
            Dim strUserId As String = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            Dim strTitleCode As String = Trim(Request.QueryString("TitleCode"))
            Dim strQuesCode As String = Trim(Request.QueryString("QuesCode"))
            Dim strSubQuesCode As String = Nothing, StrCriteriaCode As String = Nothing, StrExpec As String = Nothing, StrDateVal As String = Nothing, StrOptVal As String = Nothing

            'S1--------------------------------------------------------------------
            If Not IsNothing(lblS1Code.Text) Then
                strSubQuesCode = Trim(lblS1Code.Text)
                If txtS1.Visible = True Then
                    StrExpec = txtS1.Text
                    StrCriteriaCode = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf ddlS1.Visible = True Then
                    StrCriteriaCode = ddlS1.SelectedValue.ToString
                    StrExpec = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf txtDateS1.Visible = True Then
                    StrDateVal = txtDateS1.Text
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf rdbListS1.Visible = True Then
                    StrOptVal = rdbListS1.SelectedValue.ToString()
                    StrDateVal = Nothing
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf dgTextS1.Visible = True Then
                    If dgTextS1.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing

                        For Each DR As GridViewRow In dgTextS1.Rows
                            DK = dgTextS1.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgTextS1.DataKeys(i).Item("CRITERIA_CODE")
                                Dim txtColumnS1 As TextBox = dgTextS1.Rows(i).FindControl("txtColumnS1")
                                Dim strAnswer As String = Trim(txtColumnS1.Text)
                                StrExpec = strAnswer

                                clsMSS.InstTMPMSSDetailsMulti(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, strUserId)
                            End If
                            i += 1
                        Next
                    End If
                ElseIf dgSelectS1.Visible = True Then
                    If dgSelectS1.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgSelectS1.Rows
                            DK = dgSelectS1.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgSelectS1.DataKeys(i).Item("CRITERIA_CODE")
                                Dim chkSelectS1 As CheckBox = dgSelectS1.Rows(i).FindControl("chkSelectS1")
                                Dim strAnswer As String = IIf(chkSelectS1.Checked, 1, 0)
                                StrExpec = strAnswer
                                clsMSS.InstTMPMSSDetailsMulti(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, strUserId)
                            End If
                            i += 1
                        Next
                    End If
                End If

            End If
            'S2--------------------------------------------------------------------
            If Not IsNothing(lblS2Code.Text) Then
                strSubQuesCode = Trim(lblS2Code.Text)
                If txtS2.Visible = True Then
                    StrExpec = txtS2.Text
                    StrCriteriaCode = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)
                ElseIf ddlS2.Visible = True Then
                    StrCriteriaCode = ddlS2.SelectedValue.ToString
                    StrExpec = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)
                ElseIf txtDateS2.Visible = True Then
                    StrDateVal = txtDateS2.Text
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf rdbListS2.Visible = True Then
                    StrOptVal = rdbListS2.SelectedValue.ToString()
                    StrDateVal = Nothing
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf dgTextS2.Visible = True Then
                    If dgTextS2.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgTextS2.Rows
                            DK = dgTextS2.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgTextS2.DataKeys(i).Item("CRITERIA_CODE")
                                Dim txtColumnS2 As TextBox = dgTextS2.Rows(i).FindControl("txtColumnS2")
                                Dim strAnswer As String = Trim(txtColumnS2.Text)
                                StrExpec = strAnswer
                                clsMSS.InstTMPMSSDetailsMulti(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, strUserId)

                            End If
                            i += 1
                        Next
                    End If
                ElseIf dgSelectS2.Visible = True Then
                    If dgSelectS2.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgSelectS1.Rows
                            DK = dgSelectS2.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgSelectS2.DataKeys(i).Item("CRITERIA_CODE")
                                Dim chkSelectS2 As CheckBox = dgSelectS2.Rows(i).FindControl("chkSelectS2")
                                Dim strAnswer As String = IIf(chkSelectS2.Checked, 1, 0)
                                StrExpec = strAnswer
                                clsMSS.InstTMPMSSDetailsMulti(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, strUserId)
                            End If
                            i += 1
                        Next
                    End If
                End If
            End If

            'S3--------------------------------------------------------------------
            If Not IsNothing(lblS3Code.Text) Then
                strSubQuesCode = Trim(lblS3Code.Text)
                If txtS3.Visible = True Then
                    StrExpec = txtS3.Text
                    StrCriteriaCode = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)
                ElseIf ddlS3.Visible = True Then
                    StrCriteriaCode = ddlS3.SelectedValue.ToString
                    StrExpec = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)
                ElseIf txtDateS3.Visible = True Then
                    StrDateVal = txtDateS3.Text
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf rdbListS3.Visible = True Then
                    StrOptVal = rdbListS3.SelectedValue.ToString()
                    StrDateVal = Nothing
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf dgTextS3.Visible = True Then
                    If dgTextS3.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgTextS3.Rows
                            DK = dgTextS3.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgTextS3.DataKeys(i).Item("CRITERIA_CODE")
                                Dim txtColumnS3 As TextBox = dgTextS3.Rows(i).FindControl("txtColumnS3")
                                Dim strAnswer As String = Trim(txtColumnS3.Text)
                                StrExpec = strAnswer
                                clsMSS.InstTMPMSSDetailsMulti(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, strUserId)

                            End If
                            i += 1
                        Next
                    End If
                ElseIf dgSelectS3.Visible = True Then
                    If dgSelectS3.Rows.Count > 0 Then
                        Dim DK As DataKey
                        Dim i As Integer = 0
                        StrDateVal = Nothing
                        StrOptVal = Nothing
                        For Each DR As GridViewRow In dgSelectS3.Rows
                            DK = dgSelectS3.DataKeys(i)
                            If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                                StrCriteriaCode = dgSelectS3.DataKeys(i).Item("CRITERIA_CODE")
                                Dim chkSelectS3 As CheckBox = dgSelectS3.Rows(i).FindControl("chkSelectS3")
                                Dim strAnswer As String = IIf(chkSelectS3.Checked, 1, 0)
                                StrExpec = strAnswer
                                clsMSS.InstTMPMSSDetailsMulti(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, strUserId)
                            End If
                            i += 1
                        Next
                    End If
                End If
            End If

            'S4--------------------------------------------------------------------
            If Not IsNothing(lblS4Code.Text) Then
                strSubQuesCode = Trim(lblS4Code.Text)
                If txtS4.Visible = True Then
                    StrExpec = txtS4.Text
                    StrCriteriaCode = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)
                ElseIf ddlS4.Visible = True Then
                    StrCriteriaCode = ddlS4.SelectedValue.ToString
                    StrExpec = Nothing
                    StrDateVal = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)
                ElseIf txtDateS4.Visible = True Then
                    StrDateVal = txtDateS4.Text
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    StrOptVal = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                ElseIf rdbListS4.Visible = True Then
                    StrOptVal = rdbListS4.SelectedValue.ToString()
                    StrDateVal = Nothing
                    StrCriteriaCode = Nothing
                    StrExpec = Nothing
                    clsMSS.InstTMPMSSDetails(strSessionId, strTxnNo, strTitleCode, strQuesCode, strSubQuesCode, StrCriteriaCode, StrExpec, StrDateVal, StrOptVal, strUserId)

                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub GoNext()
        Try
            Dim strSessionId As String = Trim(Request.QueryString("sessionid"))
            Dim strTxnNo As String = Trim(Request.QueryString("TXNNO"))
            Dim strUserId As String = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            Dim strTitleCode As String = Trim(Request.QueryString("TitleCode"))
            Dim strQuesCode As String = Trim(Request.QueryString("QuesCode"))

            Dim DT As DataTable
            Dim clsMSS As New txn_WebActy.clsMSS
            DT = clsMSS.GetMSSQuesAdvNext(strTitleCode, strQuesCode)

            If DT.Rows.Count > 0 Then
                strQuesCode = DT.Rows(0)("QUES_CODE")
                Dim strurl = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMSSQues.aspx?sessionid=" + strSessionId + "&TXNNO=" + strTxnNo + "&TitleCode=" + strTitleCode + "&QuesCode=" + strQuesCode + "';"
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Refresh", "Refresh('DOWN');", True)
                'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "ResizeFrameWidth('SubDetailBar','50%');ResizeFrameWidth('SubDetailConfBar','50%');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have reached the last question!')", True)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub GoPrevious()
        Try
            Dim strSessionId As String = Trim(Request.QueryString("sessionid"))
            Dim strTxnNo As String = Trim(Request.QueryString("TXNNO"))
            Dim strUserId As String = Trim(Web.HttpContext.Current.Session("UserID").ToString)
            Dim strTitleCode As String = Trim(Request.QueryString("TitleCode"))
            Dim strQuesCode As String = Trim(Request.QueryString("QuesCode"))


            Dim DT As DataTable
            Dim clsMSS As New txn_WebActy.clsMSS
            DT = clsMSS.GetMSSQuesAdvPrevious(strTitleCode, strQuesCode)
            If DT.Rows.Count > 0 Then
                strQuesCode = DT.Rows(0)("QUES_CODE")
                Dim strurl = "parent.document.getElementById('SubDetailConfBarIframe').src='../../iFFMS/Customer/CustTxn/TxnMSSQues.aspx?sessionid=" + strSessionId + "&TXNNO=" + strTxnNo + "&TitleCode=" + strTitleCode + "&QuesCode=" + strQuesCode + "';"
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnClick", strurl, True)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Refresh", "Refresh('UP');", True)
                'ScriptManager.RegisterClientScriptBlock(Page, Me.GetType, "OnLoad", "ResizeFrameWidth('SubDetailBar','50%');ResizeFrameWidth('SubDetailConfBar','50%');ShowElement('SubDetailConfBar');MaximiseFrameHeight('SubDetailConfBarIframe');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Alert", "alert('You have reached the first question!')", True)
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "Event Handler"

    Protected Sub btnnext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnext.Click

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Confirm", "ConfirmNext();", True)

    End Sub

    Protected Sub btnNextConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNextConfirm.Click
        GoNext()
    End Sub

    Protected Sub btnprevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnprevious.Click

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Confirm", "ConfirmPrevious();", True)

    End Sub

    Protected Sub btnPreviousConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreviousConfirm.Click
        GoPrevious()
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If Page.IsValid Then
            Save()
            btnNextConfirm_Click(sender, e)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Refresh", "Refresh('DOWN');", True)

        End If

    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class