﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsPrincipal
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function PrincipalList(ByVal strSearchType As String, ByVal strSearchValue As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_LIST"
                .addItem("SEARCH_TYPE", strSearchType, cor_DB.clsDB.DataType.DBString)
                .addItem("SEARCH_VALUE", strSearchValue, cor_DB.clsDB.DataType.DBString)
                PrincipalList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.PrincipalList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function PrincipalDtl(ByVal strPrincipalID As String, ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_DTL"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                PrincipalDtl = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.PrincipalDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetSetting(ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_LIST_SETTING"
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                GetSetting = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.GetSetting" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function GetCountry(ByVal strUserId As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_LIST_COUNTRY"
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                GetCountry = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.GetCountry" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function Create(ByVal strCountryID As String, ByVal strSettingID As String, ByVal strPrincipalCode As String, _
                           ByVal strPrincipalName As String, ByVal strPrincipalName1 As String, ByVal strPrincipalDesc As String, _
                           ByVal strPrincipalDesc1 As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_CREATE"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SETTING_ID", strSettingID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_NAME", strPrincipalName, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_NAME_1", strPrincipalName1, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_DESC", strPrincipalDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_DESC_1", strPrincipalDesc1, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserId, cor_DB.clsDB.DataType.DBString)
                Create = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.Create" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Sub Update(ByVal strPrincipalID As String, ByVal strCountryID As String, ByVal strSettingID As String, ByVal strPrincipalCode As String, _
                           ByVal strPrincipalName As String, ByVal strPrincipalName1 As String, ByVal strPrincipalDesc As String, _
                           ByVal strPrincipalDesc1 As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_UPDATE"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SETTING_ID", strSettingID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("PRINCIPAL_CODE", strPrincipalCode, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_NAME", strPrincipalName, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_NAME_1", strPrincipalName1, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_DESC", strPrincipalDesc, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_DESC_1", strPrincipalDesc1, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spUpdate()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Public Sub Delete(ByVal strPrincipalID As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_DELETE"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spDelete()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsPrincipal.Update" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
