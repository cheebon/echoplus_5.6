﻿Imports System.Data
Imports System.Threading
Imports System.Globalization
''' <summary>
'''  ************************************************************************
'''	Author	    :	KIWI
'''	Date	    :	2013/4/19
'''	Purpose	    :   map
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''************************************************************************
'''  
''' </summary>
''' <remarks></remarks>
''' 
Partial Class CustomerGetAddress
    Inherits System.Web.UI.Page

    'Protected Overrides Sub InitializeCulture()

    '    Dim lang As String = Portal.UserSession.LangCode

    '    If Not String.IsNullOrEmpty(lang) Then
    '        Thread.CurrentThread.CurrentCulture = _
    '                   CultureInfo.CreateSpecificCulture(lang)
    '        Thread.CurrentThread.CurrentUICulture = New  _
    '            CultureInfo(lang)
    '    End If


    '    MyBase.InitializeCulture()
    'End Sub

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
    Public Property CustAddress() As DataTable
        Get
            Return ViewState("CustAddress")
        End Get
        Set(ByVal value As DataTable)
            ViewState("CustAddress") = value
        End Set
    End Property
    Public ReadOnly Property PageName() As String
        Get
            Return "GetAddress"
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strCustCode As String
        Try
            strCustCode = Trim(Request.QueryString("custcode"))
            If Not IsPostBack Then
                LoadCustomerAddress(strCustCode)
            End If
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGPS, SubModuleAction.Create) Then

                btnComfirm.Visible = False
                btnDeny.Visible = False
                btnUpdate.Visible = True
                pnlUnConfirmGPS.Attributes.Add("style", "display:none")
                pnlConfirmGPS.Attributes.Add("style", "display:none")
               
            Else
                If lblNLatitude.Text.Trim() = "" And lblNLongitude.Text.Trim() = "" Then
                    btnComfirm.Visible = False
                    btnDeny.Visible = False
                Else
                    btnComfirm.Visible = True
                    btnDeny.Visible = True
                End If
               
                btnUpdate.Visible = True
                pnlUnConfirmGPS.Attributes.Add("style", "display:block")
                pnlConfirmGPS.Attributes.Add("style", "display:block")

            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Private Sub LoadCustomerAddress(ByVal strCustCode As String)
        Try

            Dim dt As DataTable
            Dim clsCustomer As New mst_SalesAccount.clsCustomer

            dt = clsCustomer.getCustAddressList(strCustCode)
            If (dt.Rows.Count > 0) Then
                lblCustCode.Text = Trim(dt.Rows(0)("cust_code").ToString())
                lblCustName.Text = Trim(dt.Rows(0)("cust_name").ToString())
                lblCustAddress.Text = Trim(dt.Rows(0)("full_add").ToString())
                txtaddress.Value = Trim(dt.Rows(0)("full_add").ToString())
                txtlatitude.Value = Trim(dt.Rows(0)("latitude").ToString())
                txtlongitude.Value = Trim(dt.Rows(0)("longitude").ToString())
                lblNLatitude.Text = Trim(dt.Rows(0)("unconfirmed_latitude").ToString())
                lblNLongitude.Text = Trim(dt.Rows(0)("unconfirmed_longitude").ToString())
                lblCLatitude.Text = Trim(dt.Rows(0)("confirmed_latitude").ToString())
                lblCLongitude.Text = Trim(dt.Rows(0)("confirmed_longitude").ToString())
                hfAccuracy.Value = Trim(dt.Rows(0)("accuracy").ToString())
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "ShowCoordinate", "Submit();PlotCust();", True)


            End If
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.CUSTGPS, SubModuleAction.Create) Then

                btnComfirm.Visible = False
                btnDeny.Visible = False
                btnUpdate.Visible = True
                pnlUnConfirmGPS.Attributes.Add("style", "display:none")
                pnlConfirmGPS.Attributes.Add("style", "display:none")

            Else
                If lblNLatitude.Text.Trim() = "" And lblNLongitude.Text.Trim() = "" Then
                    btnComfirm.Visible = False
                    btnDeny.Visible = False
                Else
                    btnComfirm.Visible = True
                    btnDeny.Visible = True
                End If

                btnUpdate.Visible = True
                pnlUnConfirmGPS.Attributes.Add("style", "display:block")
                pnlConfirmGPS.Attributes.Add("style", "display:block")

            End If

            '    ' CustAddress = dt
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim dt As DataTable
        Dim clsCustomer As New mst_SalesAccount.clsCustomer
        Try

            clsCustomer.setCustAddressList(Trim(lblCustCode.Text), Trim(hfAccuracy.Value), Trim(txtlatitude.Value), Trim(txtlongitude.Value), "")
            LoadCustomerAddress(Trim(lblCustCode.Text))
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnComfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnComfirm.Click
        Dim dt As DataTable
        Dim clsCustomer As New mst_SalesAccount.clsCustomer
        Try

            clsCustomer.setCustAddressList(Trim(lblCustCode.Text), Trim(hfAccuracy.Value), Trim(lblNLatitude.Text), Trim(lblNLongitude.Text), "C")
            LoadCustomerAddress(Trim(lblCustCode.Text))
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnDeny_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeny.Click
        Dim dt As DataTable
        Dim clsCustomer As New mst_SalesAccount.clsCustomer
        Try

            clsCustomer.setCustAddressList(Trim(lblCustCode.Text), Trim(hfAccuracy.Value), Trim(lblNLatitude.Text), Trim(lblNLongitude.Text), "D")
            LoadCustomerAddress(Trim(lblCustCode.Text))
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
     
        Try


            LoadCustomerAddress(Trim(lblCustCode.Text))
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
