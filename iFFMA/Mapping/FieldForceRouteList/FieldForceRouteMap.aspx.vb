﻿Imports System.Data
Imports mst_Mapping

Partial Class iFFMA_Mapping_FieldForceRouteList_FieldForceRouteMap
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strSalesrep, strRouteCode, strSalesrepName, strRouteName As String

        Try
            If Not Page.IsPostBack Then
                strSalesrep = Request.QueryString("SalesrepCode")
                strRouteCode = Request.QueryString("RouteCode")
                strSalesrepName = Request.QueryString("SalesrepName")
                strRouteName = Request.QueryString("RouteName")

                'Set Root path for client side, to show cust photo
                Dim strRoot As String = ResolveClientUrl("~")
                strRoot = strRoot.Substring(0, strRoot.Length - 1)
                AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "RootVar", "root= '" & strRoot & "';", True)

                'Load DDl
                LoadRoute(strSalesrep, strRouteCode)

                'Inject values to javascript variables
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "FindPath", "SalesrepCode='" & strSalesrep & "'; RouteCode='" & strRouteCode & "'; SalesrepName='" & strSalesrepName & "'; RouteName='" & strRouteName & "'", True)

                'Check can view previous planned route
                CheckRoute(strSalesrep, strRouteCode)


            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "Event Handler"
    Protected Sub btnGetRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetRoute.Click
        Dim data As String()
        Dim script As String = "FindPath();"
        Try
            For Each li As ListItem In lboxRoutePlanned.Items
                data = li.Value.Split(",")
                If data(1) = ddlStartPoint.SelectedValue Then
                    lblGetRouteErr.Text = "Starting point of route cannot be an end point in custom route sequence."
                    Return
                End If
            Next

            lblGetRouteErr.Text = ""
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "FindPath", script, True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim point As String()
        Try
            If lboxRoutePlanned.SelectedIndex > -1 Then
                point = lboxRoutePlanned.SelectedItem.Value.Split(",")
                lboxRoutePlanned.Items.RemoveAt(lboxRoutePlanned.SelectedIndex)
                RemoveItemtoJSarray(point(0))
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim _Idx, _display, _startname, _endname As String
        Dim _name As String()
        Dim delimiter As Char() = {"-"c}
        Try
            If Not isSelectedItemSame() Then
                If Not isStartEndAdded() Then
                    If Not isEndEqualRootPoint() Then
                        _Idx = ddlPlanRouteStartPoint.SelectedValue.Trim & "," & ddlPlanRouteEndPoint.SelectedValue.Trim
                        _name = ddlPlanRouteStartPoint.SelectedItem.Text.Trim.Split(delimiter, 2)
                        _startname = _name(1)
                        _display = _name(0).Trim + " - " + _name(1).Trim

                        _name = ddlPlanRouteEndPoint.SelectedItem.Text.Trim.Split(delimiter, 2)
                        _endname = _name(1)
                        _display += " -> " & _name(0).Trim + " - " + _name(1).Trim

                        lboxRoutePlanned.Items.Add(New ListItem(_display, _Idx))
                        AddItemtoJSarray(_Idx)

                        lblRouteSeqErr.Text = ""
                    Else
                        lblRouteSeqErr.Text = "End point cannot be the starting point of entire route."
                    End If
                Else
                    lblRouteSeqErr.Text = "Start point or end point already added."
                End If
            Else
                lblRouteSeqErr.Text = "Start point and end point cannot be the same."
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Protected Sub btnRouteSeq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRouteSeq.Click
        lblRouteSeqErr.Text = ""
        lblDistance.Text = hfDistance.Value
    End Sub
    Protected Sub btnPrevPlan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevPlan.Click
        Dim strSalesrep, strRouteCode As String
        Dim dt As DataTable
        Try
            strSalesrep = Request.QueryString("SalesrepCode")
            strRouteCode = Request.QueryString("RouteCode")

            'Load all records
            dt = LoadPrevRoute(strSalesrep, strRouteCode)

            'Populate custom route ddl
            BuildCustomRouteList(dt)

            'Select start point ddl
            SelectStartPoint(dt)

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "FindPath", "FindPath();", True)

            lblGetRouteErr.Text = ""
            lblMsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "Query Route"
    Private Sub LoadRoute(ByVal strSalesrep As String, ByVal strRouteCode As String)
        Dim obj As New clsFieldForceRoute
        Dim blnInvalid As Boolean = False
        Dim strInvalidList As String = ""
        Try
            Dim dt As DataTable = obj.GetCustByRouteDay(strSalesrep, strRouteCode, Session("NetValue"), DateTime.Now.ToString("yyyy-MM-dd"))
            Dim i As Integer = 0

            ResetCustomerListJS()
            ResetPlannedRouteJS()
            'ResetMap()
            lblMsg.Text = ""

            With ddlStartPoint
                .Items.Clear()
                ddlPlanRouteStartPoint.Items.Clear()
                ddlPlanRouteEndPoint.Items.Clear()
                lboxRoutePlanned.Items.Clear()

                For Each dr As DataRow In dt.Rows
                    If Not IsInvalid(dr("latitude")) And Not IsInvalid(dr("longitude")) Then
                        .Items.Add(New ListItem(dr("CUST_NAME").ToString, dr("CUST_CODE").ToString))
                        ddlPlanRouteStartPoint.Items.Add(New ListItem(dr("CUST_NAME").ToString, dr("CUST_CODE").ToString))
                        ddlPlanRouteEndPoint.Items.Add(New ListItem(dr("CUST_NAME").ToString, dr("CUST_CODE").ToString))
                        i += 1

                        AddCustomerJSarray(dr("CUST_CODE").ToString, dr("CUST_NAME").ToString, dr("CUST_TYPE").ToString, dr("latitude").ToString(), _
                                            dr("longitude").ToString, dr("CONT_NAME").ToString, dr("MTD_SALES").ToString, _
                                             dr("YTD_SALES").ToString, dr("NO_SKU").ToString, dr("IMG_LOC").ToString, dr("CLASS").ToString, i)

                    Else
                        strInvalidList += IIf(strInvalidList.Length > 0, ",<br />", "") & dr("CUST_NAME").ToString & " (" & dr("CUST_CODE").ToString & ")"
                        blnInvalid = True
                    End If
                Next

                .Items.Insert(0, New ListItem("-- Select --", ""))
            End With

            If blnInvalid Then
                ShowMessage(strInvalidList & "<br /> does not have valid coordinates.")
            End If


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub CheckRoute(ByVal strSalesrep As String, ByVal strRouteCode As String)
        Dim obj As New clsFieldForceRoute
        Try
            Dim dt As DataTable = obj.CheckPreviousRoutePlan(strSalesrep, strRouteCode)

            If Not dt Is Nothing Then
                If dt.Rows.Count > 0 Then
                    If dt.Rows(0)("RESULTS").ToString = "1" Then
                        pnlViewPrevRoute.Attributes("style") = "display:inline"
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub
    Private Function LoadPrevRoute(ByVal strSalesrep As String, ByVal strRouteCode As String) As DataTable
        Dim obj As New clsFieldForceRoute
        Try
            Dim dt As DataTable = obj.GetPreviousRoutePlan(strSalesrep, strRouteCode)
            Dim i As Integer = 0

            'ResetCustomerListJS()
            'ResetPlannedRouteJS()

            'With ddlStartPoint
            '    .Items.Clear()
            '    ddlPlanRouteStartPoint.Items.Clear()
            '    ddlPlanRouteEndPoint.Items.Clear()


            '    For Each dr As DataRow In dt.Rows
            '        If Not IsInvalid(dr("latitude")) And Not IsInvalid(dr("longitude")) Then
            '            .Items.Add(New ListItem(dr("CUST_NAME").ToString, dr("CUST_CODE").ToString))
            '            ddlPlanRouteStartPoint.Items.Add(New ListItem(dr("CUST_NAME").ToString, dr("CUST_CODE").ToString))
            '            ddlPlanRouteEndPoint.Items.Add(New ListItem(dr("CUST_NAME").ToString, dr("CUST_CODE").ToString))
            '            i += 1

            '            AddCustomerJSarray(dr("CUST_CODE").ToString, dr("CUST_NAME").ToString, dr("latitude").ToString(), _
            '                                dr("longitude").ToString, i)
            '        End If
            '    Next

            '    .Items.Insert(0, New ListItem("-- Select --", ""))
            'End With

            Return dt
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub BuildCustomRouteList(ByVal dt As DataTable)
        Dim _Idx, _display As String

        Dim delimiter As Char() = {"-"c}
        Try
            If Not IsNothing(dt) Then
                lboxRoutePlanned.Items.Clear()
                ResetPlannedRouteJS()

                If dt.Rows.Count > 1 Then 'Expect at least 2 records. If just 1 then there is no need to populate ddl
                    For i As Integer = 1 To dt.Rows.Count - 1 Step 1
                        _Idx = dt.Rows(i - 1)("CUST_CODE").ToString().Trim() & "," & dt.Rows(i)("CUST_CODE").ToString().Trim()

                        _display = dt.Rows(i - 1)("CUST_NAME_2").ToString()
                        _display += " -> " & dt.Rows(i)("CUST_NAME_2").ToString()

                        lboxRoutePlanned.Items.Add(New ListItem(_display, _Idx))
                        AddItemtoJSarray(_Idx, i)
                    Next
                End If

                UpdateRouteSeq.Update()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub SelectStartPoint(ByVal dt As DataTable)
        Try
            If Not IsNothing(dt) Then
                If dt.Rows.Count > 0 Then
                    ddlStartPoint.SelectedValue = dt.Rows(0)("CUST_CODE").ToString
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Function"
    Private Function IsInvalid(ByVal obj As Object) As Boolean
        Try
            If IsNothing(obj) Then
                Return True
            ElseIf IsDBNull(obj) Then
                Return True
            ElseIf obj.ToString.Trim = String.Empty Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub AddCustomerJSarray(ByVal strCustCode As String, ByVal strCustName As String, ByVal strCustType As String, _
                                   ByVal strLatitude As String, ByVal strLongitude As String, ByVal strContName As String, ByVal strMtdSales As String, ByVal strYtdSales As String, _
                                   ByVal strNoSKU As String, ByVal strImgLoc As String, ByVal strClass As String, ByVal counter As Integer)
        Try
            Dim script As New StringBuilder

            script.AppendLine(String.Format("myCustpoint.push(new CustomerDtl('" & strCustCode & "','" & strCustName & "','" & strLatitude _
                              & "','" & strLongitude & "', '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}'));", strContName, strMtdSales, strYtdSales, strNoSKU, strImgLoc, strClass, strCustType))

            script.AppendLine("mypoint.push(new Customer('" & strCustCode & "','" & strCustName & "','" & strLatitude _
                              & "','" & strLongitude & "', false));")

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Inject" & strCustCode, script.ToString, True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub PlotCustomer()
        Try
            Dim script As New StringBuilder

            script.AppendLine("PlotCustomer();")

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PlotCustomer", script.ToString, True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Private Sub ShowMessage(ByVal strMsg As String)
        lblMsg.Text = strMsg
        pnlSysMesg.Visible = True
    End Sub
    Private Sub ResetPlannedRouteJS()
        Dim script As New StringBuilder

        script.AppendLine("udRoute = new UserDefinedRoute();")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "DefineUDRoute", script.ToString, True)
    End Sub
    Private Sub ResetMap()
        Dim script As New StringBuilder

        script.AppendLine("map.clearOverlays();")
        script.AppendLine("$('#map_step').css({display:'none'});")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ResetMap", script.ToString, True)
    End Sub
    Private Sub ResetCustomerListJS()
        Dim script As New StringBuilder

        script.AppendLine("myCustpoint = [];")
        script.AppendLine("mypoint = [];")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "DefineMyPoint", script.ToString, True)
    End Sub
    Private Sub RemoveItemtoJSarray(ByVal index As String)
        Dim script As New StringBuilder

        script.AppendLine("udRoute.RemovePoint('" & index & "');")

        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "RemoveCuzRoute", script.ToString, True)
    End Sub
    Private Function isSelectedItemSame() As Boolean
        If ddlPlanRouteEndPoint.SelectedValue = ddlPlanRouteStartPoint.SelectedValue Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function isStartEndAdded() As Boolean
        Dim _point() As String
        Try
            For Each r As ListItem In lboxRoutePlanned.Items
                _point = r.Value.Split(",")

                If ddlPlanRouteStartPoint.SelectedValue = _point(0) Or ddlPlanRouteEndPoint.SelectedValue = _point(1) Then
                    Return True
                End If
            Next

            Return False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function
    Private Function isEndEqualRootPoint() As Boolean
        Try
            If ddlStartPoint.SelectedValue = ddlPlanRouteEndPoint.SelectedValue Then
                Return True
            End If

            Return False
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Function
    Private Sub AddItemtoJSarray(ByVal index As String, Optional ByVal iloop As String = "")
        Try
            Dim script As New StringBuilder
            Dim point As String()

            point = index.Split(",")
            script.AppendLine("udRoute.AddPoint('" & point(0).Trim & "','" & point(1).Trim & "');")

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AddCuzRoute" & iloop, script.ToString, True)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


End Class

