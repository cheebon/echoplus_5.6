﻿Option Explicit On

Imports System.Data

Partial Class Admin_AccessRight_SFMSCatAccessRight
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtAccessRightID.Text = Trim(Request.QueryString("AccessRightID"))

        If Not Page.IsPostBack Then
            RefreshDatabinding()
        End If


    End Sub

    Private Sub RefreshDatabinding()
        Try
            GetELList(txtAccessRightID.Text)

            If dgELList.Rows.Count > 0 Then
                btnSave.Visible = True
                lblErr.Text = ""
            Else
                btnSave.Visible = False
                lblErr.Text = "There is no easy loader for this principal."
            End If

            For Each gvr As GridViewRow In dgELList.Rows
                Dim cb As CheckBox = CType(gvr.FindControl("chkAccessRight"), CheckBox)

                ClientScript.RegisterArrayDeclaration("CheckBoxIDs", String.Concat("'", cb.ClientID, "'"))
            Next
        Catch ex As Exception
            ExceptionMsg("DFAccessRight.RefreshDatabinding : " & ex.ToString)
        End Try
    End Sub

    Private Sub GetELList(ByVal accessrightID As Integer)
        Dim dt As DataTable
        Dim objAccessRightQuery As adm_AccessRight.clsAccessRightQuery
        Dim i As Integer

        Try
            objAccessRightQuery = New adm_AccessRight.clsAccessRightQuery
            With objAccessRightQuery
                dt = objAccessRightQuery.GetELList(accessrightID, txtELName.Text)
            End With

            With dgELList
                .DataSource = dt
                .DataBind()
            End With

            For i = 0 To dgELList.Rows.Count - 1
                Dim row As GridViewRow = dgELList.Rows(i)
                Dim chkBox As CheckBox = row.FindControl("chkAccessRight")

                If dgELList.DataKeys(i)(1).ToString = "1" Then
                    chkBox.Checked = True
                Else
                    chkBox.Checked = False
                End If
            Next

        Catch ex As Exception
            ExceptionMsg("ELAccessRight.GetELList : " & ex.ToString)
        Finally
            objAccessRightQuery = Nothing
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>window.close()</script>")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg("ELAccessRight.btnSearch_Click : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim i, cnt As Integer
        Dim strELID As String
        Dim objDFAccessRight As adm_AccessRight.clsAccessRightDtl

        strELID = ""
        cnt = 0

        Try
            For i = 0 To dgELList.Rows.Count - 1
                Dim cb As CheckBox = CType(dgELList.Rows(i).FindControl("chkAccessRight"), CheckBox)

                If cb.Checked Then
                    If cnt > 0 Then
                        strELID = strELID + ","
                    End If
                    strELID = strELID + dgELList.DataKeys(i)(0).ToString()
                    cnt = cnt + 1
                End If
            Next

            If strELID = "" Then
                strELID = "0"
            End If

            objDFAccessRight = New adm_AccessRight.clsAccessRightDtl
            With objDFAccessRight
                .clsProperties.accessright_id = txtAccessRightID.Text
                .clsProperties.changed_user_id = Session("UserID")
                .UpdateELAccessRight(strELID)
            End With

        Catch ex As Exception
            ExceptionMsg("ELAccessRight.btnSave_Click : " & ex.ToString)
        Finally
            objDFAccessRight = Nothing
            Page.ClientScript.RegisterStartupScript(Me.GetType, "", "<script language=javascript>alert('Easy Loader accessright successfully updated.');window.close()</script>")
        End Try

    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            'Dim objLog As cor_Log.clsLog
            'objLog = New cor_Log.clsLog
            'With objLog
            '    .clsProperties.LogTypeID = 1
            '    .clsProperties.DateLogIn = Now
            '    .clsProperties.DateLogOut = Now
            '    .clsProperties.SeverityID = 4
            '    .clsProperties.LogMsg = strMsg
            '    .Log()
            'End With
            'objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
