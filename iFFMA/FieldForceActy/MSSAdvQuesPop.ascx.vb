Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdvQuesPop
    Inherits System.Web.UI.UserControl

    Public Event SaveButton_Click As EventHandler
    Public Event PopLockAlert()
    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Private Sub LoadDDL(ByVal DT As DataTable)
        Try
            Dim ddlLvlInd As DropDownList = CType(DetailsView1.FindControl("ddlLvlInd"), DropDownList)
            If ddlLvlInd IsNot Nothing Then
                ddlLvlInd.SelectedValue = IIf(IsDBNull(DT.Rows(0)("LVL_IND")), "", DT.Rows(0)("LVL_IND"))
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "DETAIL VIEW"
    Public Sub LoadDvViewMode()
        pnlViewMode.Visible = True
        pnlEditMode.Visible = False

        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly)
    End Sub

    Public Sub LoadDvInsertMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Insert)
    End Sub

    Public Sub LoadDvEditMode()
        pnlViewMode.Visible = False
        pnlEditMode.Visible = True

        DetailsView1.ChangeMode(DetailsViewMode.Edit)
    End Sub

    Public Sub BindDetailsView()
        Try
            Dim DT As DataTable
            Dim dvDetailView As DataView
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv

            DT = clsMSSAdv.GetMSSQuesAdvDetails(TeamCode, TitleCode, QuesCode)
            If DT.Rows.Count > 0 Then

            Else
                DT.Rows.Add(DT.NewRow())
            End If

            dvDetailView = DT.DefaultView
            DetailsView1.DataSource = dvDetailView
            DetailsView1.DataBind()

            Dim lblLvlInd As Label = CType(DetailsView1.FindControl("lblLvlInd"), Label)
            If lblLvlInd IsNot Nothing Then
                lblLvlInd.Text = IIf(lblLvlInd.Text = "1", "Yes", "No")
            End If

            If DT IsNot Nothing Then
                LoadDDL(DT)
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub DetailsView1_ModeChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewModeEventArgs)

    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                lblInfo.Text = ""
            End If

            'HL: 20120511 - Due to BDF, Edit button can click to View/Edit details---------------------------------
            If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.MSSADV, SubModuleAction.Edit) Then
                btnSave.Visible = False
            End If
            '----------------------------------------------------------------------------------------------------
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim txtQuesCode As TextBox = CType(DetailsView1.FindControl("txtQuesCode"), TextBox)
            Dim lblQuesCode As Label = CType(DetailsView1.FindControl("lblQuesCode"), Label)
            Dim txtQuesName As TextBox = CType(DetailsView1.FindControl("txtQuesName"), TextBox)
            Dim ddlLvlInd As DropDownList = CType(DetailsView1.FindControl("ddlLvlInd"), DropDownList)

            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv

            If DetailsView1.CurrentMode = DetailsViewMode.Edit Then
                clsMSSAdv.UpdateMSSQuesAdv(TeamCode, TitleCode, Trim(lblQuesCode.Text), Trim(txtQuesName.Text), Trim(ddlLvlInd.SelectedValue))
                lblInfo.Text = "The record is successfully saved."
                LoadDvViewMode()
            Else
                Dim DT As DataTable
                Dim isDuplicate As Integer = 1
                Dim strIsDeleted As String = ""

                DT = clsMSSAdv.CreateMSSQuesAdv(TeamCode, TitleCode, Trim(txtQuesCode.Text), Trim(txtQuesName.Text), Trim(ddlLvlInd.SelectedValue))
                If DT.Rows.Count > 0 Then
                    isDuplicate = DT.Rows(0)("IS_DUPLICATE")
                    strIsDeleted = DT.Rows(0)("IS_DELETED")
                End If

                If isDuplicate = 1 Then
                    lblInfo.Text = "This Question Code already exists!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                ElseIf strIsDeleted = "Y" Then
                    lblInfo.Text = "This Question Code is used before, please create a new one!"
                    LoadDvInsertMode()
                    Show()
                    Exit Sub
                Else
                    QuesCode = Trim(txtQuesCode.Text)
                    lblInfo.Text = "The record is successfully created."
                    LoadDvViewMode()
                End If
            End If

            BindDetailsView()                       'Rebind the details view
            RaiseEvent SaveButton_Click(sender, e)  'Renew the main listing
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        Hide()
    '    Catch ex As Exception
    '        ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '    End Try
    'End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class


