
Partial Class include_wuc_pnlCallAnalyEnquiry
    Inherits System.Web.UI.UserControl

    Public Event ResetBtn_Click As EventHandler
    Public Event RefreshBtn_Click As EventHandler
    Dim strSubmoduleid As Long

    Private _strRestoreSupplierCode As String
    Private _strRestoreTeamCode As String
    Private _strRestoreSalesrepCode As String
    Private _strRestoreGroupingField As String



    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_pnlCallAnalysisEnquiry"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                LoadLsbSupplier()
                LoadLsbTeam()
                If Not String.IsNullOrEmpty(RestoreSupplierCode) Then RestoreCriteria()
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub

#Region "PROPERTY"

    Public Property SupplierCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedSupplier))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedTeam))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedSalesrep))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property GroupingField() As String
        Get
            Return Trim(GetItemsInString(lstSelectedGrouping))
        End Get
        Set(ByVal value As String)

        End Set
    End Property



    Public Property RestoreSupplierCode() As String
        Get
            Return _strRestoreSupplierCode
        End Get
        Set(ByVal value As String)
            _strRestoreSupplierCode = value
        End Set
    End Property

    Public Property RestoreTeamCode() As String
        Get
            Return _strRestoreTeamCode
        End Get
        Set(ByVal value As String)
            _strRestoreTeamCode = value
        End Set
    End Property

    Public Property RestoreSalesrepCode() As String
        Get
            Return _strRestoreSalesrepCode
        End Get
        Set(ByVal value As String)
            _strRestoreSalesrepCode = value
        End Set
    End Property

    Public Property RestoreGroupingField() As String
        Get
            Return _strRestoreGroupingField
        End Get
        Set(ByVal value As String)
            _strRestoreGroupingField = value

        End Set
    End Property

    Public WriteOnly Property PanelCollapese() As Boolean
        Set(ByVal value As Boolean)
            CPE_pnlCallAnalysisEnquiry.ClientState = value.ToString
            CPE_pnlCallAnalysisEnquiry.Collapsed = value
        End Set
    End Property
   
    Public Property SubmoduleID() As String
        Get
            Return strSubmoduleid
        End Get
        Set(ByVal value As String)
            strSubmoduleid = value
        End Set
    End Property

#End Region

#Region "SUPPLIER"
    Private Sub LoadLsbSupplier()
        Try
            'Clear list box before fill
            lsbSupplier.Items.Clear()
            lsbSelectedSupplier.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbSupplier
                .DataSource = clsSFMSExtract.GetSupplier(strUserID, strPrincipalID)
                .DataTextField = "AGENCY_NAME"
                .DataValueField = "AGENCY_CODE"
                .DataBind()
            End With

            If SubmoduleID = SubModuleType.CALLANALYENQ Then
                lsbSupplier.Items.Add(New ListItem("DKSH-DKSH", "DKSH"))
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSupplier : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSupplier.Click
        Try
            AddToListBox(lsbSupplier, lsbSelectedSupplier)
            ClassificationSeparation(lsbSelectedSupplier)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSupplier_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSupplier.Click
        Try
            AddToListBox(lsbSelectedSupplier, lsbSupplier)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSupplier_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSupplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSupplier.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSupplier.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSupplier, lsbSelectedSupplier)
            ClassificationSeparation(lsbSelectedSupplier)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllSupplier_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSuppplier_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSupplier.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSupplier.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSupplier, lsbSupplier)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllSuppplier_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()

        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "GROUPING FIELDS"
    Protected Sub lnkAddGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddGrouping.Click
        Try
            AddToListBox(lstGrouping, lstSelectedGrouping)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddGrouping_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveGrouping.Click
        Try
            AddToListBox(lstSelectedGrouping, lstGrouping)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveGrouping_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllGrouping.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lstGrouping.Items
                liToShow.Selected = True
            Next
            AddToListBox(lstGrouping, lstSelectedGrouping)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllGrouping_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllGrouping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllGrouping.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lstSelectedGrouping.Items
                liToShow.Selected = True
            Next
            AddToListBox(lstSelectedGrouping, lstGrouping)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllGrouping_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ItemMoveUp(ByVal liItemToMove As ListItem)
        Try
            liItemToMove = lstSelectedGrouping.Items.FindByText(liItemToMove.Text)
            If liItemToMove IsNot Nothing Then
                Dim intIndex As Integer = lstSelectedGrouping.Items.IndexOf(liItemToMove)
                If intIndex > 0 Then
                    intIndex -= 1
                    If intIndex >= 0 AndAlso Not lstSelectedGrouping.Items(intIndex).Selected Then
                        lstSelectedGrouping.Items.Remove(liItemToMove)
                        lstSelectedGrouping.Items.Insert(intIndex, liItemToMove)
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ItemMoveUp : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ItemMoveDown(ByVal liItemToMove As ListItem)
        Try
            liItemToMove = lstSelectedGrouping.Items.FindByText(liItemToMove.Text)
            If liItemToMove IsNot Nothing Then
                Dim intIndex As Integer = lstSelectedGrouping.Items.IndexOf(liItemToMove)
                If intIndex < lstSelectedGrouping.Items.Count - 1 AndAlso lstSelectedGrouping.Items(intIndex + 1).Selected = False Then
                    intIndex += 1
                    lstSelectedGrouping.Items.Remove(liItemToMove)
                    lstSelectedGrouping.Items.Insert(intIndex, liItemToMove)
                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".ItemMoveDown : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkUp.Click
        Try

            If lstSelectedGrouping.GetSelectedIndices().Length > 0 Then
                Dim licItemCollector As New ListItemCollection
                Dim intIndex As Integer

                For Each intIndex In lstSelectedGrouping.GetSelectedIndices
                    licItemCollector.Add(lstSelectedGrouping.Items(intIndex))
                Next

                Dim liItemToMove As ListItem
                For Each liItemToMove In licItemCollector
                    ItemMoveUp(liItemToMove)
                Next
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkUp_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDown.Click
        Try
            If lstSelectedGrouping.GetSelectedIndices().Length > 0 Then
                Dim licItemCollector As New ListItemCollection
                Dim intIndex As Integer

                For Each intIndex In lstSelectedGrouping.GetSelectedIndices
                    licItemCollector.Add(lstSelectedGrouping.Items(intIndex))
                Next

                Dim liItemToMove As ListItem
                intIndex = licItemCollector.Count - 1
                While intIndex >= 0
                    liItemToMove = licItemCollector.Item(intIndex)
                    If liItemToMove IsNot Nothing Then ItemMoveDown(liItemToMove)
                    intIndex -= 1
                End While

            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkDown_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            LoadLsbSupplier()
            LoadLsbTeam()

            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()
            lnkRemoveAllGrouping_Click(sender, e)

            RaiseEvent ResetBtn_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnReset_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            'RebindListItem(hdf_Show, lstSelectedGrouping)
            RaiseEvent RefreshBtn_Click(sender, e)

            CPE_pnlCallAnalysisEnquiry.ClientState = "true"
            CPE_pnlCallAnalysisEnquiry.Collapsed = True


            UpdatePnlEnquiry.Update()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region


    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub ClassificationSeparation(ByVal lsbFrom As ListBox)
        Dim liToCatch As String
        Dim intIndex As Integer
        Dim catched As Boolean
        catched = False
        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToCatch = lsbFrom.Items(intIndex).Value
            If liToCatch IsNot Nothing Then
                If liToCatch = "DKSH" Then
                    catched = True
                End If
            End If
        Next

        If catched = True Then
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSupplier.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSupplier, lsbSupplier)
            lsbSelectedSupplier.Items.Add(New ListItem("DKSH-DKSH", "DKSH"))
            lsbSupplier.Items.Remove(New ListItem("DKSH-DKSH", "DKSH"))
        End If
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Public Sub GenerateFiledList(ByVal lngSubModuleID As Long, Optional ByVal blnClearBeforeFill As Boolean = False)
        'If Not (lsbGroupField_Hide.Items.Count = 0 AndAlso lstSelectedGrouping.Items.Count = 0) _
        'AndAlso blnClearBeforeFill = False Then Exit Sub
        If (Not String.IsNullOrEmpty(lstGrouping.DataTextField)) _
        And blnClearBeforeFill = False Then Exit Sub

        Try
            Dim clsAdmUser As New adm_User.clsUserQuery
            'Session("GroupingValue") = ""
            With lstGrouping
                .DataSource = clsAdmUser.GetGroupFieldList(lngSubModuleID).DefaultView
                .DataTextField = "field_desc" '"field_name"
                .DataValueField = "field_name" '"field_id"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".GenerateFiledList : " & ex.ToString)
        Finally
        End Try
    End Sub

    Public Sub RestoreCriteria()

        RestoreLsb(lsbSupplier, lsbSelectedSupplier, RestoreSupplierCode)
        RestoreLsb(lsbTeam, lsbSelectedTeam, RestoreTeamCode)
        LoadLsbSalesrep()
        RestoreLsb(lsbSalesrep, lsbSelectedSalesrep, RestoreSalesrepCode)
        RestoreLsb(lstGrouping, lstSelectedGrouping, RestoreGroupingField)

    End Sub

    Public Sub RestoreLsb(ByRef lsbOri As ListBox, ByRef lsbDest As ListBox, ByRef strValueList As String)
        Dim strValues As String() = strValueList.Replace("'", "").Split(",")
        Dim idx As Integer = 0
        While idx <= strValues.GetUpperBound(0)
            SwapListItem(lsbOri, lsbDest, strValues(idx))
            idx = idx + 1
        End While
    End Sub

    Public Sub SwapListItem(ByRef lsbOri As ListBox, ByRef lsbDest As ListBox, ByRef strValue As String)
        Dim itmOri As ListItem = lsbOri.Items.FindByValue(strValue)
        If itmOri IsNot Nothing Then
            Dim itmDest = New ListItem(itmOri.Text, itmOri.Value)
            lsbDest.Items.Add(itmDest)
            lsbOri.Items.Remove(itmOri)
        End If

    End Sub

End Class
