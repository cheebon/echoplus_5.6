Imports System.Data

Partial Class iFFMR_SFA_RPT_SalesByCust
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Dim aryDataItem As New ArrayList
    Dim dtCustHeader As DataTable
    Dim dtSalesTotal As DataTable
    Dim dtTotalRows As DataTable
    Dim licItemFigureCollector As ListItemCollection
    Private Property dtCustInViewState() As DataTable
        Get
            Return ViewState("dtCustInViewState")
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtCustInViewState") = value
        End Set
    End Property
    Private Property dtTotalInViewState() As DataTable
        Get
            Return ViewState("dtTotalInViewState")
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtTotalInViewState") = value
        End Set
    End Property
    Private Property EnablePager() As Boolean
        Get
            If ViewState("EnablePager") Is Nothing Then
                Return True
            Else
                Return ViewState("EnablePager")
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("EnablePager") = value
        End Set
    End Property
    'Public Property SortingExpression() As String
    '    Get
    '        Return ViewState("strSortExpression")
    '    End Get
    '    Set(ByVal value As String)
    '        ViewState("strSortExpression") = value
    '    End Set
    'End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblErr.Text = ""
        If Not IsPostBack Then
            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControl1.Enabled = True

            wuc_ctrlPnl.SubModuleID = SubModuleType.SFA_SALESBYCUST
            wuc_ctrlPnl.DataBind()
        End If
    End Sub
    Public ReadOnly Property PageName() As String
        Get
            Return "RPT_SalesByCust"
        End Get
    End Property

#Region "Event Handler"
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click, wuc_ctrlPnl.SearchBtn_Click
        ViewState("CustCode") = Trim(txtCustCode.Text)
        ViewState("CustName") = Trim(txtCustName.Text)
        ViewState("Year") = wuc_ctrlPnl.Year
        ViewState("Month") = wuc_ctrlPnl.Month
        ViewState("AgencyCode") = wuc_ctrlPnl.AgencyCode
        ViewState("DistrictName") = Trim(txtDistrictName.Text)
        ViewState("ChannelName") = Trim(txtChannelName.Text)
        ViewState("Hospital") = Trim(txtHospital.Text)
        RefreshDataBind()
        wuc_ctrlPnl.UpdateControlPanel()
    End Sub
    'Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
    '    If IsPostBack Then RenewDataBind()
    'End Sub

    'Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click, wuc_ctrlpanel.NetValue_Changed
    '    RenewDataBind()
    'End Sub

    'Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
    '    RefreshDataBind()
    'End Sub

    'Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
    '    UpdateDatagrid_Update()
    'End Sub


#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlPnl.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            'Dim blnAllowSorting As Boolean = dgList.AllowSorting
            'Dim blnAllowPaging As Boolean = dgList.AllowPaging

            'dgList.AllowSorting = False
            'dgList.AllowPaging = False
            'RefreshDatabinding()
            'EnablePager = False
            'RenewDataBind()
            RefreshPagingNExport(1, wuc_Page1.TotRows)

            wuc_ctrlPnl.ExportToFile(PnlExport, PageName)

            'EnablePager = True
            RefreshPagingNExport(wuc_Page1.StartRec, wuc_Page1.EndRec)
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind(Optional ByVal intPassFlag As Integer = 0)
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind(intPassFlag)
    End Sub

    Public Sub RefreshDataBind(Optional ByVal intPassFlag As Integer = 0)
        RefreshDatabinding(intPassFlag)
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    'Public Sub RefreshDatabinding(Optional ByVal intPassFlag As Integer = 0)
    '    Dim ds As DataSet = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
    '    Dim dvCustView As DataView
    '    Dim dvSalesTotal As DataView
    '    'Dim strSortExpression As String = CType(SortingExpression, String)
    '    If intPassFlag <> 1 Or IsNothing(dtCustInViewState) Then
    '        ds = GetRecList()
    '        If ds Is Nothing Then
    '            dtCustHeader = New DataTable
    '            dtSalesTotal = New DataTable
    '        Else
    '            dtCustHeader = ds.Tables(0)
    '            dtSalesTotal = ds.Tables(1)

    '        End If
    '        dtCustInViewState = dtCustHeader
    '        dtTotalInViewState = dtSalesTotal
    '    Else
    '        dtCustHeader = dtCustInViewState
    '        dtSalesTotal = dtTotalInViewState
    '    End If

    '    If dtCustHeader.Rows.Count = 0 Then
    '        dtCustHeader.Rows.Add(dtCustHeader.NewRow())
    '    End If
    '    If dtSalesTotal.Rows.Count = 0 Then
    '        Dim row As DataRow = dtSalesTotal.NewRow()
    '        row("SalesTotal") = 0
    '        dtSalesTotal.Rows.Add(row)
    '    End If

    '    wuc_Page1.TotRows = dtCustHeader.Rows.Count
    '    wuc_Page1.DataBind()

    '    If EnablePager = True Then 'for export
    '        dvCustView = New DataView(dtCustHeader, "ROWINDEX>=" & wuc_Page1.StartRec & "and ROWINDEX<=" & wuc_Page1.EndRec, "", DataViewRowState.CurrentRows)
    '        dvSalesTotal = New DataView(dtSalesTotal)
    '    Else
    '        dvCustView = New DataView(dtCustHeader)
    '        dvSalesTotal = New DataView(dtSalesTotal)
    '    End If
    '    'If Not String.IsNullOrEmpty(strSortExpression) Then
    '    '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
    '    '    dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
    '    'End If

    '    RptrSalesByCustHdr.DataSource = dvCustView
    '    RptrSalesByCustHdr.DataBind()

    '    'PrdDtliFrame.Attributes("src") = "SalesByCust_PrdDtl.aspx?"
    '    UpdateDatagrid.Update()
    '    'UpdateDatagrid_Update()
    'End Sub
    Public Sub RefreshDatabinding(Optional ByVal intPassFlag As Integer = 0)
        Dim ds As DataSet = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        'Dim dvCustView As DataView
        'Dim dvSalesTotal As DataView
        'Dim strSortExpression As String = CType(SortingExpression, String)

        ds = GetRecList()
        If ds Is Nothing Then
            dtCustHeader = New DataTable
            dtSalesTotal = New DataTable
            dtTotalRows = New DataTable
        Else
            dtCustHeader = ds.Tables(0)
            dtSalesTotal = ds.Tables(1)
            dtTotalRows = ds.Tables(2)
        End If
        

        If dtCustHeader.Rows.Count = 0 Then
            dtCustHeader.Rows.Add(dtCustHeader.NewRow())
        End If
        If dtSalesTotal.Rows.Count = 0 Then
            Dim row As DataRow = dtSalesTotal.NewRow()
            row("SalesTotal") = 0
            dtSalesTotal.Rows.Add(row)
        End If
        If dtTotalRows.Rows.Count = 0 Then
            Dim row As DataRow = dtTotalRows.NewRow()
            row("TotalRows") = 0
            dtTotalRows.Rows.Add(row)
        End If

        wuc_Page1.TotRows = IIf(IsDBNull(dtTotalRows.Rows(0)("TotalRows")), 0, dtTotalRows.Rows(0)("TotalRows"))
        wuc_Page1.CurPage = 1
        wuc_Page1.DataBind()

        'If EnablePager = True Then 'for export
        '    dvCustView = New DataView(dtCustHeader, "ROWINDEX>=" & wuc_Page1.StartRec & "and ROWINDEX<=" & wuc_Page1.EndRec, "", DataViewRowState.CurrentRows)
        '    dvSalesTotal = New DataView(dtSalesTotal)
        'Else
        '    dvCustView = New DataView(dtCustHeader)
        '    dvSalesTotal = New DataView(dtSalesTotal)
        'End If
        'If Not String.IsNullOrEmpty(strSortExpression) Then
        '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
        '    dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        'End If

        RptrSalesByCustHdr.DataSource = dtCustHeader.DefaultView
        RptrSalesByCustHdr.DataBind()

        'PrdDtliFrame.Attributes("src") = "SalesByCust_PrdDtl.aspx?"
        UpdateDatagrid.Update()
        'UpdateDatagrid_Update()
    End Sub

    Public Sub RefreshPagingNExport(ByVal intStart As Integer, ByVal intEnd As Integer)
        Dim ds As DataSet = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        'Dim dvCustView As DataView
        'Dim dvSalesTotal As DataView
        'Dim strSortExpression As String = CType(SortingExpression, String)

        ds = GetRecList2(intStart, intEnd)
        If ds Is Nothing Then
            dtCustHeader = New DataTable
            dtSalesTotal = New DataTable
        Else
            dtCustHeader = ds.Tables(0)
            dtSalesTotal = ds.Tables(1)
            dtTotalRows = ds.Tables(2)
        End If


        If dtCustHeader.Rows.Count = 0 Then
            dtCustHeader.Rows.Add(dtCustHeader.NewRow())
        End If
        If dtTotalRows.Rows.Count = 0 Then
            Dim row As DataRow = dtTotalRows.NewRow()
            row("TotalRows") = 0
            dtTotalRows.Rows.Add(row)
        End If
        If dtSalesTotal.Rows.Count = 0 Then
            Dim row As DataRow = dtSalesTotal.NewRow()
            row("SalesTotal") = 0
            dtSalesTotal.Rows.Add(row)
        End If



        'If EnablePager = True Then 'for export
        '    dvCustView = New DataView(dtCustHeader, "ROWINDEX>=" & wuc_Page1.StartRec & "and ROWINDEX<=" & wuc_Page1.EndRec, "", DataViewRowState.CurrentRows)
        '    dvSalesTotal = New DataView(dtSalesTotal)
        'Else
        '    dvCustView = New DataView(dtCustHeader)
        '    dvSalesTotal = New DataView(dtSalesTotal)
        'End If
        'If Not String.IsNullOrEmpty(strSortExpression) Then
        '    Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
        '    dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        'End If

        RptrSalesByCustHdr.DataSource = dtCustHeader
        RptrSalesByCustHdr.DataBind()

        'PrdDtliFrame.Attributes("src") = "SalesByCust_PrdDtl.aspx?"
        UpdateDatagrid.Update()
        'UpdateDatagrid_Update()
    End Sub
    'Public Sub UpdateDatagrid_Update()
    '    If dgList.Rows.Count < 10 Then
    '        dgList.GridHeight = Nothing
    '    End If
    '    UpdateDatagrid.Update()
    'End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        '    Dim i As Integer
        '    Dim columnDataType As System.Type = Nothing

        '    dgList.Columns.Clear()

        '    Dim ColumnName As String = ""
        '    For i = 0 To dtToBind.Columns.Count - 1
        '        ColumnName = dtToBind.Columns(i).ColumnName
        '        Select Case CF_IN.GetFieldColumnType(ColumnName)
        '            Case FieldColumntype.HyperlinkColumn
        '                'Dim dgColumn As New HyperLinkField ' HyperLinkColumn

        '                'dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
        '                'dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
        '                'dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

        '                'If String.IsNullOrEmpty(CF_IN.GetOutputFormatString(ColumnName)) = False Then
        '                '    dgColumn.DataTextFormatString = CF_IN.GetOutputFormatString(ColumnName)
        '                'End If
        '                'dgColumn.ItemStyle.HorizontalAlign = CF_IN.ColumnStyle(ColumnName).HorizontalAlign
        '                'dgColumn.ItemStyle.Wrap = CF_IN.ColumnStyle(ColumnName).Wrap

        '                'dgColumn.HeaderText = CF_IN.GetDisplayColumnName(ColumnName)
        '                'dgColumn.DataTextField = ColumnName
        '                'dgColumn.SortExpression = ColumnName

        '                'Dim strUrlFormatString As String
        '                'Dim strUrlFields() As String = Nothing

        '                'strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields)
        '                'dgColumn.DataNavigateUrlFields = strUrlFields
        '                'dgColumn.DataNavigateUrlFormatString = strUrlFormatString
        '                'dgColumn.Target = "_self"
        '                'dgList.Columns.Add(dgColumn)
        '                'dgColumn = Nothing

        '                ''Add the field name
        '                'aryDataItem.Add(ColumnName)
        '            Case FieldColumntype.InvisibleColumn
        '            Case Else
        '                Dim dgColumn As New BoundField 'BoundColumn

        '                dgColumn.ReadOnly = True

        '                dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
        '                dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
        '                dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

        '                Dim strFormatString As String = CF_IN.GetOutputFormatString(ColumnName)
        '                If String.IsNullOrEmpty(strFormatString) = False Then
        '                    dgColumn.DataFormatString = strFormatString 'CF_IN.GetOutputFormatString(ColumnName)
        '                    dgColumn.HtmlEncode = False
        '                End If
        '                dgColumn.ItemStyle.HorizontalAlign = CF_IN.ColumnStyle(ColumnName).HorizontalAlign
        '                dgColumn.ItemStyle.Wrap = CF_IN.ColumnStyle(ColumnName).Wrap

        '                dgColumn.HeaderText = CF_IN.RemColPrefix(CF_IN.GetDisplayColumnName(ColumnName))
        '                'dgColumn.HeaderText = CF_IN.GetDisplayColumnName(ColumnName)
        '                dgColumn.DataField = ColumnName
        '                dgColumn.SortExpression = ColumnName
        '                dgList.Columns.Add(dgColumn)
        '                dgColumn = Nothing

        '                'Add the field name
        '                aryDataItem.Add(ColumnName.ToUpper)
        '        End Select
        '    Next
    End Sub

    '''' <summary>
    '''' Pass in the datatable, and generate the url format string based on the
    '''' fields exist in the DT. And return 2 params, strUrlFields and strFormatString
    '''' to form the hyperlink fields need.
    '''' </summary>
    '''' <param name="intIndex">indicate the link index, incase there are more than 1 link field to format</param>
    '''' <param name="dtToBind">data table, pass by ref to get the column field name</param>
    '''' <param name="strUrlFields">strUrlFields, pass by ref, to return the UrlFields to use.</param>
    '''' <returns>URLFormatString, as url to pass with parameters</returns>
    '''' <remarks></remarks>
    'Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
    '    Dim strUrlFormatString As String = String.Empty
    '    Select Case intIndex
    '        Case 0 'Generate sales info by date links
    '            Dim strbFilterGroup As New Text.StringBuilder 'build the string for grouping
    '            Dim arrFields As New ArrayList 'store field name used in grouping
    '            Dim licGroupFieldList As New ListItemCollection
    '            licGroupFieldList.Add(New ListItem("TEAM_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("REGION_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SALESREP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CHAIN_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CHANNEL_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CUST_GRP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("CUST_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("PRD_GRP_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("PRD_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SALES_AREA_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("SHIPTO_CODE", -1))
    '            licGroupFieldList.Add(New ListItem("LVL_GROUP", -1))

    '            For Each liFieldToFind As ListItem In licGroupFieldList
    '                For Each DC As DataColumn In dtToBind.Columns
    '                    If String.Compare(DC.ColumnName.ToUpper, liFieldToFind.Text, True) = 0 Then
    '                        liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
    '                        strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
    '                        Exit For
    '                    ElseIf liFieldToFind.Text = "LVL_GROUP" AndAlso DC.ColumnName Like "TREE*CODE" Then
    '                        liFieldToFind.Text = DC.ColumnName.ToUpper.Trim
    '                        liFieldToFind.Value = arrFields.Add(liFieldToFind.Text)
    '                        strbFilterGroup.Append(IIf(strbFilterGroup.Length > 0, "&", "") & liFieldToFind.Text & "=" & "{" & liFieldToFind.Value & "}")
    '                        Exit For
    '                    End If
    '                Next
    '            Next

    '            ReDim strUrlFields(arrFields.Count - 1)
    '            For intIndx As Integer = 0 To arrFields.Count - 1
    '                strUrlFields(intIndx) = arrFields.Item(intIndx)
    '            Next

    '            Dim intTeamInd As Integer = licGroupFieldList.FindByText("TEAM_CODE").Value
    '            Dim intRegionInd As Integer = licGroupFieldList.FindByText("REGION_CODE").Value
    '            Dim intSalesrepInd As Integer = licGroupFieldList.FindByText("SALESREP_CODE").Value
    '            strUrlFormatString = "SalesInfoByDate.aspx?" + _
    '            strbFilterGroup.ToString + _
    '           IIf(intTeamInd < 0, "&TEAM_CODE=" & Session("TEAM_CODE"), "") + _
    '           IIf(intRegionInd < 0, "&REGION_CODE=" & Session("REGION_CODE"), "") + _
    '           IIf(intSalesrepInd < 0, "&SALESREP_CODE=" & Session("SALESREP_CODE"), "")

    '            Session("SalesInfoByDate_GroupingField") = arrFields
    '        Case Else
    '            strUrlFields = Nothing
    '            strUrlFormatString = ""
    '    End Select
    '    Return strUrlFormatString
    'End Function

    'Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
    '    If e.Row.RowType = DataControlRowType.Header Then
    '        Dim oGridView As GridView = dgList 'CType(sender, GridView)
    '        Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
    '        Dim TC As TableHeaderCell
    '        Dim CF As ListItem
    '        Dim intCounter As Integer = 0
    '        Dim intActualIndex As Integer = 0

    '        For Each CF In licHeaderCollector
    '            If CF.Value = 1 Then
    '                Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
    '                intActualIndex = IIf(intCounter > 0, intCounter, 0)
    '                If iIndex >= 0 Then
    '                    e.Row.Cells(intActualIndex).RowSpan = 2
    '                    e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
    '                    GVR.Cells.Add(e.Row.Cells(intActualIndex))
    '                End If
    '            Else
    '                TC = New TableHeaderCell
    '                TC.Text = CF.Text
    '                TC.ColumnSpan = CF.Value
    '                intCounter += CF.Value
    '                GVR.Cells.Add(TC)
    '            End If
    '        Next
    '        oGridView.Controls(0).Controls.AddAt(0, GVR)
    '    End If
    'End Sub

    'Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
    '    Select Case e.Row.RowType
    '        Case DataControlRowType.DataRow
    '        Case DataControlRowType.Footer
    '            Dim iIndex As Integer
    '            For Each li As ListItem In licItemFigureCollector
    '                iIndex = aryDataItem.IndexOf(li.Text)
    '                If iIndex >= 0 Then
    '                    e.Row.Cells(iIndex).Text = String.Format(CF_IN.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
    '                End If
    '            Next
    '    End Select
    'End Sub

#Region "Math Function"
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function
    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function
    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function

#End Region

    Private Function GetRecList() As DataSet
        Dim DS As DataSet = Nothing
        Try
            Dim clsRpt As New rpt_SFA.SFA_RPT
            With wuc_ctrlPnl
                DS = clsRpt.GET_SalesByCust(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), .Year, .Month, Session("SALESREP_LIST"), Trim(txtCustCode.Text), Trim(txtCustName.Text), .AgencyCode, Trim(txtDistrictName.Text), Trim(txtChannelName.Text), Trim(txtHospital.Text))
                'DS = clsRpt.GET_SalesByCust(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), .Year, .Month, Session("SALESREP_LIST"), "", "", .AgencyCode, "", "", "")
            End With
            'If DS Is Nothing OrElse DS.Columns.Count = 0 Then Return DS
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        'PreRenderMode(DS)
        Return DS
    End Function
    Private Function GetRecList2(ByVal intStart As Integer, ByVal intEnd As Integer) As DataSet
        Dim DS As DataSet = Nothing
        Try
            Dim clsRpt As New rpt_SFA.SFA_RPT
            With wuc_ctrlPnl
                DS = clsRpt.GET_SalesByCust(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), ViewState("Year"), ViewState("Month"), Session("SALESREP_LIST"), ViewState("CustCode"), ViewState("CustName"), ViewState("AgencyCode"), ViewState("DistrictName"), ViewState("ChannelName"), ViewState("Hospital"), intStart, intEnd)
                'DS = clsRpt.GET_SalesByCust(Session("UserID"), Session("PRINCIPAL_ID"), Session("PRINCIPAL_CODE"), .Year, .Month, Session("SALESREP_LIST"), "", "", .AgencyCode, "", "", "")
            End With
            'If DS Is Nothing OrElse DS.Columns.Count = 0 Then Return DS
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        End Try
        'PreRenderMode(DS)
        Return DS
    End Function
    Private Sub PreRenderMode(ByRef DT As DataTable)
        '    aryDataItem.Clear()
        '    dgList_Init(DT)

        '    Cal_ItemFigureCollector(DT)
    End Sub

    Private Sub Cal_CustomerHeader()
        '    'aryDataItem will refresh each time rebind
        '    'aryColumnFieldCollector will refresh only reget the data from the database
        '    licHeaderCollector = New ListItemCollection

        '    Dim blnisNew As Boolean
        '    Dim strColumnName As String
        '    Dim liColumnField As ListItem
        '    Dim aryCuttedName As New ArrayList

        '    'collect the column name that ready to show on the screen
        '    For Each strColumnName In aryDataItem
        '        strColumnName = strColumnName.ToUpper
        '        If strColumnName Like "MTD*" Then
        '            strColumnName = "MTD Sales"
        '        ElseIf strColumnName Like "QTD*" Then
        '            strColumnName = "QTD Sales"
        '        ElseIf strColumnName Like "YTD*" Then
        '            If strColumnName Like "YTDGROSS" Then
        '                strColumnName = "PYTD Sales"
        '            Else
        '                strColumnName = "YTD Sales"
        '            End If
        '        ElseIf strColumnName Like "PYTD*" Then
        '            strColumnName = "PYTD Sales"
        '        Else
        '            strColumnName = CF_IN.GetDisplayColumnName(strColumnName)
        '        End If
        '        aryCuttedName.Add(strColumnName)
        '    Next

        '    'witht the cutted columnName, fill in the Occorance count
        '    For Each strColumnName In aryCuttedName
        '        blnisNew = True
        '        liColumnField = licHeaderCollector.FindByText(strColumnName)

        '        If Not liColumnField Is Nothing Then
        '            liColumnField.Value = CInt(liColumnField.Value) + 1
        '        Else
        '            liColumnField = New ListItem(strColumnName, 1)
        '            licHeaderCollector.Add(liColumnField)
        '        End If

        '    Next
        '    licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper

            'If (strColumnName Like "SALES_*") Then
            '    liColumnField = New ListItem(strColumnName.ToUpper(), 0)
            '    licItemFigureCollector.Add(liColumnField)
            'End If

            If (strColumnName Like "*_AMT") Then
                liColumnField = New ListItem(strColumnName.ToUpper(), 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub

    'Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
    '    Dim strSortExpression As String = SortingExpression
    '    If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
    '        If strSortExpression Like (e.SortExpression & "*") Then
    '            If strSortExpression.IndexOf(" DESC") > 0 Then
    '                strSortExpression = e.SortExpression
    '            Else
    '                strSortExpression = e.SortExpression & " DESC"
    '            End If
    '        Else
    '            strSortExpression = e.SortExpression
    '        End If
    '    Else
    '        strSortExpression = e.SortExpression
    '    End If
    '    SortingExpression = strSortExpression
    '    RefreshDatabinding()
    'End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing
    End Sub

    'Public Class CF_IN
    '    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
    '        Dim strFieldName As String = ""
    '        Select Case ColumnName.ToUpper.Replace("SALES_", "")
    '            Case "AMT"
    '                strFieldName = "Actual Sales"
    '            Case "MTG"
    '                strFieldName = "Month to Go"
    '            Case "RR"
    '                strFieldName = "RR vs Target"
    '            Case "TGT"
    '                strFieldName = "Target"
    '            Case Else
    '                strFieldName = Report.GetDisplayColumnName(ColumnName)
    '        End Select

    '        Return strFieldName
    '    End Function

    '    Public Shared Function RemColPrefix(ByVal ColumnName As String) As String
    '        ColumnName = ColumnName.ToUpper.Replace("_AMT", "")

    '        Return ColumnName
    '    End Function

    '    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
    '        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
    '        'Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
    '        'strColumnName = strColumnName.ToUpper
    '        'Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
    '        'If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
    '        '   (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
    '        '    FCT = FieldColumntype.InvisibleColumn
    '        'Else
    '        '    FCT = FieldColumntype.BoundColumn
    '        'End If

    '        FCT = FieldColumntype.BoundColumn

    '        Return FCT
    '    End Function

    '    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
    '        Dim strStringFormat As String = ""
    '        If strColumnName.ToUpper Like "SALES_*" Then
    '            strStringFormat = "{0:#,0.00}"
    '        End If
    '        If strColumnName.ToUpper Like "*_AMT" Then
    '            strStringFormat = "{0:#,0}"
    '        End If
    '        'Select Case strColumnName.ToUpper
    '        '    Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
    '        '         "MTD_TGT", "QTD_TGT", "YTD_TGT"
    '        '        strStringFormat = "{0:#,0.00}"
    '        '    Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
    '        '        strStringFormat = "{0:#,0.0}"
    '        '    Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
    '        '         "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
    '        '        strStringFormat = "{0:#,0}"
    '        '    Case Else
    '        '        strStringFormat = ""
    '        'End Select

    '        Return strStringFormat
    '    End Function

    '    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
    '        Dim CS As New ColumnStyle
    '        With CS
    '            Dim strColumnName As String = ColumnName.ToUpper
    '            .FormatString = GetOutputFormatString(ColumnName)

    '            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
    '                .HorizontalAlign = HorizontalAlign.Center
    '            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
    '                .HorizontalAlign = HorizontalAlign.Right
    '                .Wrap = False
    '            Else
    '                .HorizontalAlign = HorizontalAlign.Left
    '            End If

    '        End With

    '        Return CS
    '    End Function
    'End Class

    Protected Sub RptrSalesByCustHdr_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        'Dim _lbltotal As Label

        'If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
        '    Dim row As HtmlTableRow = CType(e.Item.FindControl("Drow"), HtmlTableRow)
        '    Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)

        '    If Not drv Is Nothing Then
        '        row.Attributes.Add("onclick", "Redirect('SalesByCust_PrdDtl.aspx?cust_code=" & drv.Item("CUST_CODE").ToString() & "&agency=" & wuc_ctrlPnl.AgencyCode & "&year=" & wuc_ctrlPnl.Year & "&month=" & wuc_ctrlPnl.Month & "')")
        '    End If
        'End If

        'If e.Item.ItemType = ListItemType.Footer Then
        '    _lbltotal = CType(e.Item.FindControl("lblTotal"), Label)
        '    _lbltotal.Text = dtSalesTotal.Rows(0)("SalesTotal").ToString()
        'End If
    End Sub

    Protected Sub RptrSalesByCustHdr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        Dim _lbltotal As Label

        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim row As HtmlTableRow = CType(e.Item.FindControl("Drow"), HtmlTableRow)
            Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)

            If Not drv Is Nothing Then
                row.Attributes.Add("onclick", "Redirect('SalesByCust_PrdDtl.aspx?cust_code=" & drv.Item("CUST_CODE").ToString() & "&agency=" & wuc_ctrlPnl.AgencyCode & "&year=" & wuc_ctrlPnl.Year & "&month=" & wuc_ctrlPnl.Month & "')")
            End If
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            _lbltotal = CType(e.Item.FindControl("lblTotal"), Label)
            _lbltotal.Text = IIf(IsDBNull(dtSalesTotal.Rows(0)("SalesTotal")), "0", Format("0:#,0.00", dtSalesTotal.Rows(0)("SalesTotal").ToString()))
        End If
    End Sub

#Region "Paging"

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_Page1.Go_Click
        Try
            wuc_Page1.DataBind()

            RefreshPagingNExport(wuc_Page1.StartRec, wuc_Page1.EndRec)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("Admin_User_UserList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_Page1.Previous_Click
        Try
            'wuc_Page1.CurPage = wuc_Page1.CurPage - 1
            wuc_Page1.DataBind()

            RefreshPagingNExport(wuc_Page1.StartRec, wuc_Page1.EndRec)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("Admin_User_UserList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_Page1.Next_Click
        Try
            'wuc_Page1.CurPage = wuc_Page1.CurPage + 1
            wuc_Page1.DataBind()

            RefreshPagingNExport(wuc_Page1.StartRec, wuc_Page1.EndRec)
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("Admin_User_UserList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region
End Class
