<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wuc_ddlBranch.ascx.vb" Inherits="include_wuc_ddlBranch" %>
<asp:DropDownList id="ddlBranchID" runat="server" CssClass="cls_dropdownlist"></asp:DropDownList>
<asp:CompareValidator ID="cfvBranchID" ControlToValidate="ddlBranchID" ValueToCompare="0" Operator="GreaterThan" Display="Dynamic" CssClass="cls_validator" runat="server" ErrorMessage="Please select Branch."></asp:CompareValidator>
