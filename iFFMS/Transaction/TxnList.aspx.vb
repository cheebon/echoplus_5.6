Imports System.Data
Partial Class iFFMS_Transaction_TxnList
    Inherits System.Web.UI.Page
    Private dtAR As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If

        'Call Header
        With wuc_lblHeader
            .Title = GetTitle()
            .DataBind()
        End With


        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()
            lblsalesrepcode.text = Trim(Session("SALESREP_CODE"))
            lblsessionid.text = Trim(GetGuid())
        End If

    End Sub

    Private Function GetTitle() As String
        Dim strTitle As String
        Dim dt As DataTable
        Dim clsCommon As New txn_WebActy.clsCommon
        dt = clsCommon.GetAdmConfig("CONFIG_LAST_TXN_DATE", Session("UserID"))
        If dt.Rows.Count > 0 And dt.Rows(0)(0) = "1" Then
            Dim strLastDays As String = dt.Rows(0)(1)
            strTitle = " Last " + strLastDays + " Days Transaction List"
        Else
            strTitle = " Last 7 Days Transaction List"  'Report.GetName(SubModuleType.TRAORDER)
        End If
        Return strTitle
    End Function

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

    Private Function GetKIVTxnList(ByVal strTxnType As String) As DataTable

        Dim dt As DataTable = Nothing

        If Not String.IsNullOrEmpty(Web.HttpContext.Current.Session("SALESREP_CODE")) Then
            Dim strUserid As String, StrSalesrepCode As String
            StrSalesrepCode = Trim(Web.HttpContext.Current.Session("SALESREP_CODE"))
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))

            Dim clssalemancode As New txn_WebActy.clsCommon
            dt = clssalemancode.GetSRKIVTxnList(strTxnType, StrSalesrepCode, strUserid)

        End If

        Return dt
    End Function

    Public Property GetGuid()
        Get
            If String.IsNullOrEmpty(Session("GUID")) Then Session("GUID") = Session("USER_ID") & Guid.NewGuid.ToString.ToUpper.Substring(24, 8)
            Return Session("GUID")
        End Get
        Set(ByVal value)
            Session("GUID") = value
        End Set
    End Property


    Public Function GetAccessRight(ByVal dblModuleID As Double, Optional ByVal dblSubModuleID As Double = 0, Optional ByVal strActionID As String = "") As Boolean
        Dim drCurrRow As DataRow()
        Dim blnValue As Boolean = False

        dtAR = Session("UserAccessRight")
        If Not IsNothing(dtAR) Then
            If dblSubModuleID = 0 Then
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND (action_id=1 OR action_id=4)", "")
            Else
                drCurrRow = dtAR.Select("module_id=" & Trim(dblModuleID) & " AND submodule_id=" & Trim(dblSubModuleID) & " AND action_id IN (" & Trim(strActionID) & ")", "")
            End If

            If drCurrRow.Length > 0 Then
                blnValue = True
            End If

            Return blnValue
        End If
        'Try
        'Catch ex As Exception
        '    ExceptionMsg("index.GetAccessRight : " & ex.ToString)
        'End Try
    End Function

End Class
