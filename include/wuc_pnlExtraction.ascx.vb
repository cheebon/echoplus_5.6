
Partial Class wuc_pnlExtraction
    Inherits System.Web.UI.UserControl

    Public Event ResetBtn_Click As EventHandler
    Public Event RefreshBtn_Click As EventHandler

    Public ReadOnly Property ClassName() As String
        Get
            Return "wuc_pnlExtraction"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                LoadLsbTeam()
            End If

        Catch ex As Exception
            ExceptionMsg(ClassName & ".Page_Load : " & ex.ToString)
        End Try

    End Sub

#Region "PROPERTY"
    Public Property DateFrom() As String
        Get
            Return WucSFMSExtract_Date.DateStart
        End Get
        Set(ByVal value As String)
            WucSFMSExtract_Date.DateStart = value
        End Set
    End Property

    Public Property Dateto() As String
        Get
            Return WucSFMSExtract_Date.DateEnd
        End Get
        Set(ByVal value As String)
            WucSFMSExtract_Date.DateEnd = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedTeam))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SalesrepCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedSalesrep))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property CatCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedCat))
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property SubCatCode() As String
        Get
            Return Trim(GetItemsInString(lsbSelectedSubCat))
        End Get
        Set(ByVal value As String)

        End Set
    End Property
#End Region

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsSFMSExtract.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            LoadLsbCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            LoadLsbCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            LoadLsbCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            LoadLsbCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SALESREP"
    Private Sub LoadLsbSalesrep()
        Try
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsSFMSExtract.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "CAT"
    Private Sub LoadLsbCat()
        Try
            lsbCat.Items.Clear()
            lsbSelectedCat.Items.Clear()
            lsbSubCat.Items.Clear()
            lsbSelectedSubCat.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)

            With lsbCat
                .DataSource = clsSFMSExtract.GetCat(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "CAT_NAME"
                .DataValueField = "CAT_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbCat : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddCat.Click
        Try
            AddToListBox(lsbCat, lsbSelectedCat)
            LoadLsbSubCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveCat.Click
        Try
            AddToListBox(lsbSelectedCat, lsbCat)
            LoadLsbSubCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllCat.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbCat.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbCat, lsbSelectedCat)
            LoadLsbSubCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllCat.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedCat.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedCat, lsbCat)
            LoadLsbSubCat()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "SUB CAT"
    Private Sub LoadLsbSubCat()
        Try
            lsbSubCat.Items.Clear()
            lsbSelectedSubCat.Items.Clear()

            Dim clsSFMSExtract As New rpt_Customize.clsSFMSExtract

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String
            Dim strCatList As String

            strTeamList = GetItemsInString(lsbSelectedTeam)
            strCatList = GetItemsInString(lsbSelectedCat)

            With lsbSubCat
                .DataSource = clsSFMSExtract.GetSubCat(strUserID, strPrincipalID, strTeamList, strCatList)
                .DataTextField = "SUB_CAT_NAME"
                .DataValueField = "SUB_CAT_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(ClassName & ".LoadLsbSubCat : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSubCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSubCat.Click
        Try
            AddToListBox(lsbSubCat, lsbSelectedSubCat)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddSubCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSubCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSubCat.Click
        Try
            AddToListBox(lsbSelectedSubCat, lsbSubCat)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveSubCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSubCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSubCat.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSubCat.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSubCat, lsbSelectedSubCat)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkAddAllSubCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSubCat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSubCat.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSubCat.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSubCat, lsbSubCat)
        Catch ex As Exception
            ExceptionMsg(ClassName & ".lnkRemoveAllSubCat_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "EVENT HANDLER"
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            LoadLsbTeam()

            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()
            lsbCat.Items.Clear()
            lsbSelectedCat.Items.Clear()
            lsbSubCat.Items.Clear()
            lsbSelectedSubCat.Items.Clear()
            WucSFMSExtract_Date.DateStart = ""
            WucSFMSExtract_Date.DateEnd = ""

            RaiseEvent ResetBtn_Click(sender, e)

        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnReset_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            RaiseEvent RefreshBtn_Click(sender, e)

            CPE_pnlSFMSExtraction.ClientState = "true"
            CPE_pnlSFMSExtraction.Collapsed = True

            UpdatePnlExtraction.Update()
        Catch ex As Exception
            ExceptionMsg(ClassName & ".btnRefresh_Click : " & ex.ToString)
        Finally
        End Try
    End Sub


#End Region

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(ClassName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
