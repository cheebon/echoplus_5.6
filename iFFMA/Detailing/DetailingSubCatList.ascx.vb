'************************************************************************
'	Author	    :	YONG SOO FONG
'	Date	    :	26/02/2016
'	Purpose	    :	Detailing Sub Cat List 
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data

Partial Class iFFMA_Detailing_DetailingSubCatList
    Inherits System.Web.UI.UserControl

#Region "Local Variable"
    Private intPageSize As Integer

    Private _aryDataItem As ArrayList

    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15)) '10

        If Not Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Edit) Then
            btnAdd.Visible = False
        End If

        'If Master_Row_Count > 0 Then
        '    For Each dr As GridViewRow In dgSubCatList.Rows
        '        Dim btnPreview As Button = CType(dr.Cells(aryDataItem.IndexOf("BTN_PREVIEW")).Controls(0), Button)
        '        If btnPreview IsNot Nothing Then
        '            btnPreview.CssClass = "cls_button"
        '        End If
        '    Next
        'End If
    End Sub

#Region "PROPERTY"

    Public Property DetCode() As String
        Get
            Return Trim(hdDetCode.Value)
        End Get
        Set(ByVal value As String)
            hdDetCode.Value = value
        End Set
    End Property
#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        dgSubCatList.PageIndex = 0
        wuc_dgSubCatPaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsDetailing As New mst_Detailing.clsDetailing

            DT = clsDetailing.GetDetailingActivityList(DetCode)
            dgSubCatList.DataKeyNames = New String() {"DET_CODE", "SUB_DET_CODE"}

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "dgSubCatList"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrentTable = GetRecList()

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If

            PreRenderMode(dtCurrentTable)

            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgSubCatList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
                .DataBind()
            End With

            'Call Paging
            With wuc_dgSubCatPaging
                .PageCount = dgSubCatList.PageCount
                .CurrentPageIndex = dgSubCatList.PageIndex
                .DataBind()
                .RowCount = Master_Row_Count
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdatePnlDetailingSubCatList.Update()
        End Try
    End Sub

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            dgSubCatList_Init(DT)
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubCatList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            aryDataItem.Clear()
            dgSubCatList.Columns.Clear()

            'CHECKBOX
            'If Report.GetAccessRight(ModuleID.FFMA, SubModuleType.DETAIL_BRAND, SubModuleAction.Delete) Then

            aryDataItem.Add("chkSelect")
            Dim chkbox As New CheckBoxField
            dgSubCatList.Columns.Add(chkbox)
            dgSubCatList.Columns(0).HeaderStyle.Width = "25"
            dgSubCatList.Columns(0).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Else
            'dgSubCatList.Columns.Clear()
            'End If

            'ADD BUTTON EDIT
            If Master_Row_Count > 0 Then
                Dim dgBtnColumn As New CommandField

                dgBtnColumn.HeaderStyle.Width = "50"
                dgBtnColumn.HeaderText = "Edit"
                dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                If dtToBind.Rows.Count > 0 Then
                    dgBtnColumn.ButtonType = ButtonType.Link
                    dgBtnColumn.ShowEditButton = True
                    dgBtnColumn.EditText = "<img src='../../images/ico_edit.gif' alt='Edit' border='0' height='18px' width='18px'/>"
                    dgBtnColumn.Visible = True
                End If

                dgSubCatList.Columns.Add(dgBtnColumn)
                dgBtnColumn = Nothing
                aryDataItem.Add("BTN_EDIT")
            End If

            ''--------ADD BUTTON PREVIEW -----------------'
            'If Master_Row_Count > 0 Then
            '    Dim dgBtnColumn As New CommandField

            '    dgBtnColumn.HeaderStyle.Width = "80"
            '    dgBtnColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            '    dgBtnColumn.HeaderText = "Preview"

            '    If dtToBind.Rows.Count > 0 Then
            '        dgBtnColumn.ButtonType = ButtonType.Button
            '        dgBtnColumn.ControlStyle.CssClass = "cls_button"
            '        dgBtnColumn.ShowSelectButton = True
            '        dgBtnColumn.SelectText = "Preview"
            '        dgBtnColumn.Visible = True
            '    End If

            '    dgSubCatList.Columns.Add(dgBtnColumn)
            '    dgBtnColumn = Nothing
            '    aryDataItem.Add("BTN_PREVIEW")
            'End If

            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName
                Select Case CF_DetailingSubCatList.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.HyperlinkColumn
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Middle
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_DetailingSubCatList.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_DetailingSubCatList.ColumnStyle(ColumnName).HorizontalAlign
                        dgColumn.ItemStyle.Wrap = CF_DetailingSubCatList.ColumnStyle(ColumnName).Wrap

                        dgColumn.HeaderText = CF_DetailingSubCatList.GetDisplayColumnName(ColumnName)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgSubCatList.Columns.Add(dgColumn)
                        dgColumn = Nothing
                        aryDataItem.Add(ColumnName)
                End Select
            Next

            aryDataItem = aryDataItem
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
    Protected Sub dgSubCatList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgSubCatList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgSubCatList_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles dgSubCatList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header AndAlso Master_Row_Count > 0 Then
                Dim chkAll As CheckBox = CType(e.Row.FindControl("chkSelectAll"), CheckBox)
                If chkAll Is Nothing Then
                    chkAll = New CheckBox
                    chkAll.ID = "chkSelectAll"
                    e.Row.Cells(0).Controls.Add(chkAll)
                End If
                chkAll.Attributes.Add("onClick", "SelectAllRows(this)")
                chkAll.ToolTip = "Click to toggle the selection of ALL rows"
            ElseIf e.Row.RowType = DataControlRowType.DataRow AndAlso Master_Row_Count > 0 Then
                Dim chk As CheckBox = CType(e.Row.FindControl("chkSelect"), CheckBox)
                If chk Is Nothing Then
                    chk = New CheckBox
                    chk.ID = "chkSelect"
                    e.Row.Cells(0).Controls.Add(chk)
                End If
                chk.Attributes.Add("onClick", "SelectRow(this)")
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgSubCatList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgSubCatList.RowEditing
        Try
            Dim strSelectedDetCode As String = sender.datakeys(e.NewEditIndex).item("DET_CODE")
            Dim strSelectedSubDetCode As String = sender.datakeys(e.NewEditIndex).item("SUB_DET_CODE")


            With (wuc_DetailingSubCatPop)
                .ResetPage()
                .DetCode = strSelectedDetCode
                .SubDetCode = strSelectedSubDetCode
                .LoadDvEditMode()
                .BindDetailsView()
            End With
            e.Cancel = True
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Function GetdgSubCatList() As GridView
        Dim dgSubCatListToExport As GridView = Nothing

        Try
            Dim blnAllowSorting As Boolean = dgSubCatList.AllowSorting
            Dim blnAllowPaging As Boolean = dgSubCatList.AllowPaging

            dgSubCatList.AllowSorting = False
            dgSubCatList.AllowPaging = False
            RefreshDatabinding(True)

            dgSubCatListToExport = dgSubCatList

            dgSubCatList.AllowPaging = blnAllowPaging
            dgSubCatList.AllowSorting = blnAllowSorting
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

        Return dgSubCatListToExport
    End Function
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgSubCatPaging.Go_Click
        Try
            dgSubCatList.PageIndex = CInt(wuc_dgSubCatPaging.PageNo - 1)

            dgSubCatList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgSubCatPaging.Previous_Click
        Try
            If dgSubCatList.PageIndex > 0 Then
                dgSubCatList.PageIndex = dgSubCatList.PageIndex - 1
            End If
            wuc_dgSubCatPaging.PageNo = dgSubCatList.PageIndex + 1

            dgSubCatList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgSubCatPaging.Next_Click
        Try
            If dgSubCatList.PageCount - 1 > dgSubCatList.PageIndex Then
                dgSubCatList.PageIndex = dgSubCatList.PageIndex + 1
            End If
            wuc_dgSubCatPaging.PageNo = dgSubCatList.PageIndex + 1

            dgSubCatList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "EVENTS"
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        With (wuc_DetailingSubCatPop)
            .ResetPage()
            .DetCode = DetCode
            .SubDetCode = ""
            .LoadDvInsertMode()
            .BindDetailsView()
        End With
    End Sub

    Protected Sub btnDetailingSubCatPopSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_DetailingSubCatPop.SaveButton_Click
        RenewDataBind()
    End Sub
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim sbSelectedStr As New Text.StringBuilder

            If dgSubCatList.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0

                Dim DK As DataKey
                For Each DR As GridViewRow In dgSubCatList.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                        DK = dgSubCatList.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            Dim clsDetailing As New mst_Detailing.clsDetailing
                            clsDetailing.DeleteDetailingActivity(DK(0), DK(1))
                        End If
                    End If
                    i += 1
                Next
            End If

            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

#Region "COMMON FUNCTION"
    Private Function GetSelectedString() As String
        Dim sbSelectedStr As New Text.StringBuilder

        If dgSubCatList.Rows.Count > 0 Then
            Dim chkSelected As CheckBox
            Dim i As Integer = 0

            Dim DK As DataKey
            For Each DR As GridViewRow In dgSubCatList.Rows
                chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)
                If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then
                    DK = dgSubCatList.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                        sbSelectedStr.Append(IIf(sbSelectedStr.ToString.Length = 0, String.Empty, ",") & Trim(DK(0)))
                    End If
                End If
                i += 1
            Next
        End If
        Return sbSelectedStr.ToString
    End Function
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgSubCatList_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles dgSubCatList.RowDeleting
            Try
            Dim clsDetailing As New mst_Detailing.clsDetailing

            Dim strSelectedDetCode As String = sender.datakeys(e.RowIndex).item("DET_CODE")
            Dim strSelectedSubDetCode As String = sender.datakeys(e.RowIndex).item("SUB_DET_CODE")

                Dim DT As DataTable
                Dim isExtraCatExist As Integer

            DT = clsDetailing.DeleteDetailingActivity(strSelectedDetCode, strSelectedSubDetCode)

                If DT.Rows.Count > 0 Then
                isExtraCatExist = DT.Rows(0)("IS_EXTRA_CAT_EXISTS")
                End If

                If isExtraCatExist > 0 Then
                    lblMsgPop.Message = "There are " + isExtraCatExist.ToString + " sub item(s) under this category!"
                    lblMsgPop.Show()
                Else
                    RenewDataBind()
                End If
                e.Cancel = True
            Catch ex As Exception
                ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
            End Try
    End Sub

    Protected Sub dgSubCatList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dgSubCatList.SelectedIndexChanged
        Dim clsDetailing As New mst_Detailing.clsDetailing

        Dim strCatCode As String = sender.SelectedDataKey.Item("DET_CODE").ToString
        Dim strSubCatCode As String = sender.SelectedDataKey.Item("SUB_DET_CODE").ToString
        With (wuc_DetailingSubPreviewPop)
            .CatCode = strCatcode
            .SubCatCode = strSubCatCode
            .BindCatPreview()
        End With

    End Sub
End Class

Public Class CF_DetailingSubCatList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Select Case ColumnName.ToUpper
            Case "START_DATE"
                strFieldName = "Start Date"
            Case "END_DATE"
                strFieldName = "End Date"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String, Optional ByVal blnShiptoExist As Boolean = False) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            strColumnName = strColumnName.ToUpper

            If strColumnName = "TEAM_CODE" Or strColumnName = "DET_CODE" Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strFormatString As String = ""
        Try
            strColumnName = strColumnName.ToUpper

            If strColumnName Like "*DATE" Then
                strFormatString = "{0:yyyy-MM-dd}"
            ElseIf strColumnName Like "*UNIT" OrElse strColumnName Like "*QTY*" Then
                strFormatString = "{0:#,0}"
            ElseIf strColumnName Like "*AMT" OrElse strColumnName = "CONV_FACTOR" OrElse strColumnName Like "*PRICE" Then
                strFormatString = "{0:#,0.00}"
            Else
                strFormatString = ""
            End If

        Catch ex As Exception
        End Try

        Return strFormatString
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" _
                OrElse strColumnName Like "*_NO" OrElse strColumnName Like "*_IND" _
                OrElse strColumnName Like "TIME_*" Then
                    .HorizontalAlign = HorizontalAlign.Center
                    .Wrap = False
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function
End Class

