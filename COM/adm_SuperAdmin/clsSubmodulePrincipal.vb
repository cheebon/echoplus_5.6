﻿Imports System.ComponentModel
Imports System.Configuration

Public Class clsSubmodulePrincipal
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strEchoplusConn As String = Web.HttpContext.Current.Session("echoplus_conn")

    Public Function SubModulePrincipalList(ByVal strCountryID As String, ByVal strPrincipalID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEPRINCIPAL_LIST"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                SubModulePrincipalList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.SubModulePrincipalList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function SubModulePrincipalDtl(ByVal strPrincipalID As String, ByVal strModuleID As String, ByVal strUserID As String, ByVal strMode As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEPRINCIPAL_DTL"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("MODULE_ID", strModuleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("MODE", strMode, cor_DB.clsDB.DataType.DBString)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                SubModulePrincipalDtl = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.SubModulePrincipalDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadCountryList(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEPRINCIPAL_LIST_COUNTRY"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadCountryList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.SubModulePrincipalDtl" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function
    Public Function LoadCountryDDL(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_COUNTRY_DDLLIST"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadCountryDDL = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.LoadCountryDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadPrincipalList(ByVal strCountryID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEPRINCIPAL_LIST_PRINCIPAL"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadPrincipalList = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.LoadPrincipalList" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadPrincipalDDL(ByVal strCountryID As String, ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_MST_PRINCIPAL_DDLLIST"
                .addItem("COUNTRY_ID", strCountryID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadPrincipalDDL = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.LoadPrincipalDDL" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Function LoadPrincipalListAll(ByVal strUserID As String) As DataTable
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEPRINCIPAL_ALLPRINCIPAL"
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                LoadPrincipalListAll = .spRetrieve()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.LoadPrincipalListAll" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Function

    Public Sub Save(ByVal strPrincipalID As String, ByVal strModuleID As String, ByVal strSubmoduleList As String, ByVal strUserID As String)
        Dim objDB As cor_DB.clsDB
        Try
            objDB = New cor_DB.clsDB
            With objDB
                .ConnectionString = strEchoplusConn
                .CmdText = "SPP_ADM_SUBMODULEPRINCIPAL_CREATE"
                .addItem("PRINCIPAL_ID", strPrincipalID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("MODULE_ID", strModuleID, cor_DB.clsDB.DataType.DBDouble)
                .addItem("SUBMODULELIST", strSubmoduleList, cor_DB.clsDB.DataType.DBString, True)
                .addItem("USER_ID", strUserID, cor_DB.clsDB.DataType.DBString)
                .spInsert()
                '.spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg("adm_SuperAdmin.clsSubmodulePrincipal.Save" & ex.ToString))
        Finally
            objDB = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
