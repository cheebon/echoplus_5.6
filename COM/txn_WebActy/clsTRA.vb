'************************************************************************
'	Author	    :	
'	Date	    :	21/08/2008
'	Purpose	    :	Class to build datatables for getting summary details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsTRA
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsTRA"
        End Get
    End Property
#Region "DDL"
    Public Function GetSRWhsList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_WHSCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCustList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_CUSTCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCustContList(ByVal strSalesRepCode As String, ByVal strCustCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_CONTCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRPrdList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_PRDCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRMReasonList(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_MREASONCODE"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetReasonList() As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_REASONCODE"
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetStorLocalList(ByVal strSalesrepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_STORLOCALCODE"
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetCollByList() As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_COLLCODE"
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetUOMCode(ByVal strPrdCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_UOMCODE"
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetUOMPrice(ByVal strPrdCode As String, ByVal strUOMCode As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_UOMPRICE"
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUOMCode, clsDB.DataType.DBString)
            .addOutput("UOM_PRICE", 2)
            Return .spRetrieveValue("UOM_PRICE")
        End With
        objDB = Nothing
    End Function

#End Region

#Region "TXN NO"

    ''Public Function GetSRVouchNo(ByVal strSalesRepCode As String) As String
    ''    Dim objDB As clsDB

    ''    objDB = New clsDB
    ''    With objDB
    ''        .ConnectionString = strFFMSConn
    ''        .CmdText = "SPP_WEB_GET_TRA_VOUCHNO"
    ''        .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
    ''        .addOutput("VOUCH_NO", 2)
    ''        Return .spRetrieveValue("VOUCH_NO")
    ''    End With
    ''    objDB = Nothing
    ''End Function

    ''Public Function GetSRTxnNo(ByVal strSalesRepCode As String) As String
    ''    Dim objDB As clsDB

    ''    objDB = New clsDB
    ''    With objDB
    ''        .ConnectionString = strFFMSConn
    ''        .CmdText = "SPP_WEB_GET_TRA_TXNNO"
    ''        .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
    ''        .addOutput("TXN_NO", 2)
    ''        Return .spRetrieveValue("TXN_NO")
    ''    End With
    ''    objDB = Nothing
    ''End Function

    ''Public Function GetSRVisitID(ByVal strSalesRepCode As String) As String
    ''    Dim objDB As clsDB

    ''    objDB = New clsDB
    ''    With objDB
    ''        .ConnectionString = strFFMSConn
    ''        .CmdText = "SPP_WEB_GET_TRA_VISITID"
    ''        .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
    ''        .addOutput("VISIT_ID", 2)
    ''        Return .spRetrieveValue("VISIT_ID")
    ''    End With
    ''    objDB = Nothing
    ''End Function

    Public Function GetTRANo(ByVal strSalesRepCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_NO"
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetSRCode(ByVal strUserId As String) As String
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_GET_TRA_SALESREPCODE"
            .addItem("USER_ID", strUserId, clsDB.DataType.DBString)
            .addOutput("SALESREP_CODE", 2)
            Return .spRetrieveValue("SALESREP_CODE")
        End With
        objDB = Nothing
    End Function
#End Region

#Region "TMP TRA DTL "

    Public Function GetTMPTRADetails(ByVal strTXNNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL"
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTMPTRADetailsEdit(ByVal strTXNNo As String, ByVal strLineNo As String, ByVal strUserId As String, ByVal strsessionid As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL_EDIT"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Sub InstTMPTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal strUomCode As String, _
    ByVal strBNo As String, ByVal strRetQty As String, ByVal strListPrice As String, ByVal strExpDate As String, ByVal strRetAmt As String _
    , ByVal strReasonCode As String, ByVal strRemarks As String, ByVal strUserId As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("BNO", strBNo, clsDB.DataType.DBString)
            .addItem("RET_QTY", strRetQty, clsDB.DataType.DBString)
            .addItem("LIST_PRICE", strListPrice, clsDB.DataType.DBString)
            .addItem("EXP_DATE", strExpDate, clsDB.DataType.DBString)
            .addItem("RET_AMT", strRetAmt, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Sub UpdTMPTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strPrdCode As String, ByVal strUomCode As String, _
   ByVal strBNo As String, ByVal strRetQty As String, ByVal strListPrice As String, ByVal strExpDate As String, ByVal strRetAmt As String _
   , ByVal strReasonCode As String, ByVal strRemarks As String, ByVal strUserId As String, ByVal strLineNo As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL_UPDATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("PRD_CODE", strPrdCode, clsDB.DataType.DBString)
            .addItem("UOM_CODE", strUomCode, clsDB.DataType.DBString)
            .addItem("BNO", strBNo, clsDB.DataType.DBString)
            .addItem("RET_QTY", strRetQty, clsDB.DataType.DBString)
            .addItem("LIST_PRICE", strListPrice, clsDB.DataType.DBString)
            .addItem("EXP_DATE", strExpDate, clsDB.DataType.DBString)
            .addItem("RET_AMT", strRetAmt, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .spUpdate()
        End With
        objDB = Nothing
    End Sub

    Public Sub DelTMPTRADetails(ByVal strTXNNo As String, ByVal strLineNo As String, ByVal strUserId As String, ByVal strsessionid As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL_DELETE"
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("LINE_NO", strLineNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .spDelete()
        End With
        objDB = Nothing
    End Sub

#End Region

#Region "TMP TRA HDR"
    Public Sub InstTMPTRAHeader(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVisitid As String, ByVal strVoucherNo As String, _
        ByVal strSalesRepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strCollCode As String, ByVal strReasonCode As String, ByVal strSlocCode As String _
        , ByVal strCtnNo As String, ByVal strRefno As String, ByVal strRemarks As String, ByVal strUserId As String, ByVal strTxnDate As String)
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_HDR_CREATE"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitid, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesRepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("COLL_CODE", strCollCode, clsDB.DataType.DBString)
            .addItem("REASON_CODE", strReasonCode, clsDB.DataType.DBString)
            .addItem("SLOC_CODE", strSlocCode, clsDB.DataType.DBString)
            .addItem("CTN_NO", strCtnNo, clsDB.DataType.DBString)
            .addItem("REF_NO", strRefno, clsDB.DataType.DBString)
            .addItem("REMARKS", strRemarks, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("TXN_DATE", strTxnDate, clsDB.DataType.DBString)
            .spInsert()
        End With
        objDB = Nothing
    End Sub

    Public Function GetTMPTRAHeader(ByVal strTXNNo As String, ByVal strUserId As String, ByVal strsessionid As String, _
    ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_HDR"
            .addItem("SESSIONID", strsessionid, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTXNNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function
#End Region

#Region "TRA SUBMIT"

    Public Function InstTRADetails(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strVoucherNo As String, ByVal strVisitID As String, _
        ByVal strSalesrepCode As String, ByVal strCustCode As String, ByVal strContCode As String, ByVal strStatus As String, ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL_SUBMIT"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("VOUCHER_NO", strVoucherNo, clsDB.DataType.DBString)
            .addItem("VISIT_ID", strVisitID, clsDB.DataType.DBString)
            .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            .addItem("STATUS_CODE", strStatus, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

#End Region

#Region "TRA SUM LST"
    Public Function GetTRASumHdrs(ByVal strSessionId As String, ByVal strTxnNo As String, ByVal strUserId As String, _
      ByVal strCustCode As String, ByVal strContCode As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_HDR"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString)
            .addItem("CONT_CODE", strContCode, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function

    Public Function GetTRASumDtl(ByVal strSessionId As String, ByVal strTxnNo As String,  ByVal strUserId As String) As DataTable
        Dim objDB As clsDB

        objDB = New clsDB
        With objDB
            .ConnectionString = strFFMSConn
            .CmdText = "SPP_WEB_TMP_TXN_TRADE_RET_DTL"
            .addItem("SESSIONID", strSessionId, clsDB.DataType.DBString)
            .addItem("TXN_NO", strTxnNo, clsDB.DataType.DBString)
            .addItem("USERID", strUserId, clsDB.DataType.DBString)
            Return .spRetrieve()
        End With
        objDB = Nothing
    End Function


#End Region

#Region "COMMON"
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
#End Region

End Class
