﻿Imports System.Data
Imports mst_SalesAccount
Imports System.IO

Partial Class iFFMA_SalesAccount_CustProfileMaintenance_CustProfileMaintenanceGrid
    Inherits System.Web.UI.Page
    '    Private Property Master_Row_Count() As Integer
    '        Get
    '            Return CInt(ViewState("Master_Row_Count"))
    '        End Get
    '        Set(ByVal value As Integer)
    '            ViewState("Master_Row_Count") = value
    '        End Set
    '    End Property
    '    Private intPageSize As Integer
    '    Private strCustName, strAddress, strDistrict, strCustGrp, strCustClass, strCustType As String

    '    Public Property ExportFlag() As String
    '        Get
    '            Return ViewState("ExportFlag")
    '        End Get
    '        Set(ByVal value As String)
    '            ViewState("ExportFlag") = value
    '        End Set
    '    End Property

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try

    '            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

    '            If Not Page.IsPostBack Then
    '                ExportFlag = IIf(Request.QueryString("Export") Is Nothing, "", Request.QueryString("Export"))
    '                strCustName = IIf(Request.QueryString("CustName") Is Nothing, "", Request.QueryString("CustName"))
    '                strAddress = IIf(Request.QueryString("Address") Is Nothing, "", Request.QueryString("Address"))
    '                strDistrict = IIf(Request.QueryString("District") Is Nothing, "", Request.QueryString("District"))
    '                strCustGrp = IIf(Request.QueryString("CustGrpName") Is Nothing, "", Request.QueryString("CustGrpName"))
    '                strCustClass = IIf(Request.QueryString("CustClass") Is Nothing, "", Request.QueryString("CustClass"))
    '                strCustType = IIf(Request.QueryString("CustType") Is Nothing, "", Request.QueryString("CustType"))



    '                'Call Paging
    '                With wuc_dgpaging
    '                    .PageCount = dgList.PageCount
    '                    .CurrentPageIndex = dgList.PageIndex
    '                    .DataBind()
    '                    .Visible = False
    '                End With

    '                'TimerControl1.Enabled = True
    '                If ExportFlag.ToUpper = "TRUE" Then

    '                Else
    '                    RenewDataBind()
    '                End If

    '            End If



    '            lblErr.Text = ""
    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub
    '#Region "Standard Template"

    '    'Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    '    If TimerControl1.Enabled Then
    '    '        TimerControl1.Enabled = False

    '    '        RefreshDatabinding()
    '    '    End If

    '    'End Sub

    '    Private Sub ExceptionMsg(ByVal strMsg As String)
    '        Try
    '            lblErr.Text = ""
    '            lblErr.Text = strMsg

    '            'Call error log class
    '            Dim objLog As cor_Log.clsLog
    '            objLog = New cor_Log.clsLog
    '            With objLog
    '                .clsProperties.LogTypeID = 1
    '                .clsProperties.DateLogIn = Now
    '                .clsProperties.DateLogOut = Now
    '                .clsProperties.SeverityID = 4
    '                .clsProperties.LogMsg = strMsg
    '                .Log()
    '            End With
    '            objLog = Nothing

    '        Catch ex As Exception

    '        End Try
    '    End Sub

    '#End Region

    '#Region "DATA BIND"
    '    Public Sub RenewDataBind()
    '        dgList.PageIndex = 0
    '        wuc_dgpaging.PageNo = 1

    '        ViewState.Clear()
    '        RefreshDataBind()
    '    End Sub

    '    Public Sub RefreshDataBind()
    '        RefreshDatabinding()
    '    End Sub

    '    Private Function GetRecList() As DataTable
    '        Dim DT As DataTable = Nothing
    '        Try
    '            Dim clsCustomer As New clsCustProfileMaintain

    '            DT = clsCustomer.SearchCustProfile(strCustName, strAddress, strDistrict, strCustGrp, strCustClass, strCustType)

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        Finally
    '        End Try
    '        Return DT
    '    End Function

    '    Private Function GetRecListExport() As DataTable
    '        Dim DT As DataTable = Nothing
    '        Try
    '            Dim clsCustomer As New clsCustProfileMaintain
    '            DT = clsCustomer.SearchCustProfile(strCustName, strAddress, strDistrict, strCustGrp, strCustClass, strCustType)
    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        Finally
    '        End Try
    '        Return DT
    '    End Function
    '#End Region

    '#Region "DGLIST"
    '    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
    '        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
    '        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

    '        Try
    '            If isExport = True Then
    '                dtCurrentTable = GetRecListExport()
    '            Else
    '                dtCurrentTable = GetRecList()
    '            End If

    '            If dtCurrentTable Is Nothing Then
    '                dtCurrentTable = New DataTable
    '            Else
    '                If dtCurrentTable.Rows.Count = 0 Then
    '                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
    '                    Master_Row_Count = 0
    '                Else
    '                    Master_Row_Count = dtCurrentTable.Rows.Count
    '                End If
    '            End If

    '            Dim dvCurrentView As New DataView(dtCurrentTable)
    '            If Not String.IsNullOrEmpty(strSortExpression) Then
    '                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
    '                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
    '            End If

    '            With dgList
    '                .DataSource = dvCurrentView
    '                .PageSize = intPageSize
    '                .AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False))
    '                .DataBind()
    '            End With

    '            'Call Paging
    '            With wuc_dgpaging
    '                .PageCount = dgList.PageCount
    '                .CurrentPageIndex = dgList.PageIndex
    '                .DataBind()
    '                .RowCount = Master_Row_Count
    '                .Visible = IIf(Master_Row_Count > 0, True, False)
    '            End With

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        Finally
    '            UpdateDatagrid_Update()
    '        End Try
    '    End Sub

    '    Public Sub UpdateDatagrid_Update()
    '        Try
    '            If dgList.Rows.Count < 15 Then
    '                dgList.GridHeight = Nothing
    '            End If

    '            UpdateDatagrid.Update()
    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub

    '    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
    '        Try
    '            Select Case e.Row.RowType
    '                Case DataControlRowType.DataRow
    '                    Dim strPK As String = DirectCast(sender, GridView).DataKeys(e.Row.RowIndex).Value.ToString()

    '                    Dim bfPhoto As LinkButton = CType(e.Row.Cells(6).Controls(1), LinkButton)
    '                    bfPhoto.OnClientClick = "NavigatePhoto('" & strPK & "');"
    '                    bfPhoto.Attributes("href") = "#"

    '                    Dim bfGPS As LinkButton = CType(e.Row.Cells(9).Controls(1), LinkButton)
    '                    bfGPS.OnClientClick = "NavigateGPS('" & strPK & "');"
    '                    bfGPS.Attributes("href") = "#"
    '            End Select
    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub

    '    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgList.RowCommand
    '        Select Case e.CommandName.ToUpper
    '            'Case "PHOTO"
    '            '    'ShowCustPhotoPop()

    '            '    Dim rowIndex As Integer = Convert.ToInt32(e.CommandArgument)
    '            '    Dim strCustCode As String = dgList.DataKeys(rowIndex)(0).ToString

    '            '    'hfCustCode.Value = strCustCode
    '            '    'updPnlMaintenanceCustomerPhoto.Update()

    '            '    'BindCustPhoto(strCustCode)

    '            '    wuc_CustomerPhoto.CustCode = strCustCode
    '            '    wuc_CustomerPhoto.Show()
    '            'Case "GPS"
    '            '    wuc_CustomerGPS.Show()
    '            '    wuc_CustomerGPS.GetDefaultPrincipalLoc()
    '            'AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShowPhotoPopup", "map.checkResize();", True)
    '        End Select
    '    End Sub


    '    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
    '        Dim strSortExpression As String = ViewState("strSortExpression")
    '        Try
    '            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
    '                If strSortExpression Like (e.SortExpression & "*") Then
    '                    If strSortExpression.IndexOf(" DESC") > 0 Then
    '                        strSortExpression = e.SortExpression
    '                    Else
    '                        strSortExpression = e.SortExpression & " DESC"
    '                    End If
    '                Else
    '                    strSortExpression = e.SortExpression
    '                End If
    '            Else
    '                strSortExpression = e.SortExpression
    '            End If
    '            'CriteriaCollector.SortExpression = strSortExpression
    '            ViewState("strSortExpression") = strSortExpression

    '            RefreshDatabinding()
    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region
    '#Region "Paging Control"
    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub btnGo_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
    '        Try
    '            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkPrevious_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
    '        Try
    '            If dgList.PageIndex > 0 Then
    '                dgList.PageIndex = dgList.PageIndex - 1
    '            End If
    '            wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub

    '    '---------------------------------------------------------------------------------------------------------
    '    ' Procedure         : 	Sub lnkNext_OnClick
    '    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    '    ' Calling Methods   :   1) ...
    '    '                       2) ...
    '    ' Parameters: [in]  : 
    '    '		      [out] : 
    '    '---------------------------------------------------------------------------------------------------------

    '    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
    '        Try
    '            If dgList.PageCount - 1 > dgList.PageIndex Then
    '                dgList.PageIndex = dgList.PageIndex + 1
    '            End If
    '            wuc_dgpaging.PageNo = dgList.PageIndex + 1

    '            dgList.EditIndex = -1
    '            RefreshDatabinding()
    '            Exit Sub

    '        Catch ex As Exception
    '            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
    '        End Try
    '    End Sub
    '#End Region
    '    Private Sub ExportGrid()
    '        Dim objStringWriter As New System.IO.StringWriter
    '        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
    '        Try
    '            Dim blnAllowSorting As Boolean = dgList.AllowSorting
    '            Dim blnAllowPaging As Boolean = dgList.AllowPaging

    '            dgList.AllowSorting = False
    '            dgList.AllowPaging = False
    '            RefreshDatabinding()

    '            'wuc_ctrlpanel.ExportToFile(dgList, "SalesList")
    '            wuc_toolbar.ExportToFile(dgList, PageName)

    '            dgList.AllowPaging = blnAllowPaging
    '            dgList.AllowSorting = blnAllowSorting
    '            RefreshDatabinding()
    '        Catch ex As Threading.ThreadAbortException
    '        Catch ex As Exception
    '            ExceptionMsg(PageName & ".wuc_toolbar_ExportBtn_Click : " & ex.ToString)
    '        End Try
    '    End Sub
End Class
