Imports System.Data

Partial Class CustomerContactGeneralInfo
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "CustomerGeneralInfo"
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        Try


            'Call Header
            With wuc_lblheader
                .Title = "Customer Contact General Infomation"
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                TimerControl1.Enabled = True
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

    Sub bindGridView(ByVal strSalesmanCode As String, ByVal strCustCode As String)
        Dim dt, dtnew As DataTable
        Dim obj As New txn_Customer.clsCustInfo

        Try
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                gridview1.DataSource = dt
                gridview1.DataBind()
            Else
                dtnew = dt.Copy
                dtnew.Rows.Add(dtnew.NewRow)
                gridview1.DataSource = dtnew
                gridview1.DataBind()
                gridview1.Columns(6).Visible = False
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".bindGridView : " & ex.ToString)
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub addClickEvent(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gridview1.RowCreated
        Dim dt As DataTable
        Dim obj As New txn_Customer.clsCustInfo
        Dim strSalesmanCode, strCustCode As String
        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                If e.Row.RowIndex <> -1 Then
                    Dim PostBack As String = Page.ClientScript.GetPostBackEventReference(gridview1, "Select$" & e.Row.RowIndex)
                    e.Row.Cells(0).Attributes.Add("OnClick", PostBack)
                    e.Row.Cells(0).Attributes.Add("Style", "cursor:hand")
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".addClickEvent : " & ex.ToString)
        End Try


    End Sub

    Protected Sub dg_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridview1.SelectedIndexChanged
        Dim strContactCode, strCustCode, strSalesmanCode As String
        Dim obj As New txn_Customer.clsCustInfo
        Dim dt As DataTable
        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                'strContactCode = gridview1.SelectedRow.Cells(0).Text
                strContactCode = gridview1.DataKeys(gridview1.SelectedIndex).Values("cont_acc_code").ToString()
                Response.Redirect("../Customer/CustomerContactDetail.aspx?contactcode=" + strContactCode, False)
            Else
                Exit Sub
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".dg_SelectedIndexChanged : " & ex.ToString)
        End Try
    End Sub

    Sub gridview_Selected(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gridview1.RowCommand
        Dim strContactCode, strCustCode, strSalesmanCode As String
        Dim obj As New txn_Customer.clsCustInfo
        Dim dt As DataTable
        Try
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            dt = obj.getContactGeneralInfoDT(strSalesmanCode, strCustCode, "1")
            If dt.Rows.Count > 0 Then
                'strContactCode = gridview1.Rows(e.CommandArgument).Cells(0).Text
                strContactCode = gridview1.DataKeys(e.CommandArgument).Values("cont_acc_code").ToString()
                Response.Redirect("../Customer/CustomerContactDetail.aspx?contactcode=" + strContactCode, False)
            Else
                Exit Sub
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".gridview_Selected : " & ex.ToString)
        End Try

    End Sub

    'Sub xxx(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gridview1.RowCreated
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        e.Row.Attributes.Add("onclick", "javascript:__doPostBack('gridview1','Select$" & e.Row.RowIndex & "')")
    '        e.Row.Attributes.Add("style", "cursor:hand;")
    '    End If
    'End Sub

    'Sub yyy(ByVal sender As Object, ByVal e As EventArgs) Handles gridview1.SelectedIndexChanged
    '    Dim r As GridViewRow = gridview1.SelectedRow
    '    MsgBox(r.Cells(1).Text)
    'End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub


    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then

            Dim strSalesmanCode, strCustCode As String
            strSalesmanCode = Trim(Session("SALESREP_CODE"))
            strCustCode = Trim(Session("customercode"))
            bindGridView(strSalesmanCode, strCustCode)
            UpdateDatagrid.Update()
            TimerControl1.Enabled = False
        End If

    End Sub

End Class



