Imports System.Data

Partial Class MthOverview
    Inherits System.Web.UI.Page

#Region "Local Variable"

    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_HeaderCollector_MthOverview"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

    Public Property GroupingValue() As String
        Get
            Return ViewState("GroupingValue")
        End Get
        Set(ByVal value As String)
            ViewState("GroupingValue") = value
        End Set
    End Property

    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
        'Session.Remove(strCollectorName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Try

        If Not IsPostBack Then

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MTHOVER) '"Monthly Overview"
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.MTHOVER
                .DataBind()
                .Visible = True
            End With

            If PreviousPage IsNot Nothing Then
                With CriteriaCollector
                    Session.Item("Year") = .Year
                    Session.Item("Month") = .Month
                    Session("PRINCIPAL_CODE") = .PrincipalCode
                    Session("PRINCIPAL_ID") = .PrincipalID
                    Session("TREE_PATH") = .Tree_Path
                    SortingExpression = .SortExpression
                    'If .MonthType < ddlMonthType.Items.Count Then ddlMonthType.SelectedIndex = .MonthType
                    wuc_Menu.RestoreSessionState = True
                End With
            Else
                CriteriaCollector = Nothing ' New clsSharedValue
                'SetButtonVisibility(ViewMode.ViewByFigure)
            End If

            ChangeReportType()
            'Dim intMonth As Integer
            'If IsNumeric(Session.Item("Month")) Then intMonth = Session.Item("Month")
            'Select Case intMonth
            '    Case 4, 5, 6
            '        ddlReportType.SelectedValue = 2
            '    Case 7, 8, 9
            '        ddlReportType.SelectedValue = 3
            '    Case 10, 11, 12
            '        ddlReportType.SelectedValue = 4
            '    Case Else
            '        ddlReportType.SelectedValue = 1
            'End Select
            TimerControl1.Enabled = True
        End If

        'lblErr.Text = ""
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        'End Try
    End Sub

    Public ReadOnly Property PageName() As String
        Get
            Return "MthOverview"
        End Get
    End Property

#Region "Event Handler"

    'Protected Sub ActivateCloseSearchPanel(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.CloseThisPanel
    '    ActivateSearchBtn_Click(sender, e)
    '    LayoutChanged(sender, e)
    'End Sub

    'Protected Sub ActivateSearchBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.SearchBtn_Click
    '    Try
    '        Dim wcuPnlWidth As UI.WebControls.Unit = pnlTreeMenuControl.Width
    '        If wcuPnlWidth.Value > 2 Then
    '            wcuPnlWidth = New UI.WebControls.Unit(2)
    '            wuc_Menu.HideOrShow(False)
    '        Else
    '            wcuPnlWidth = New UI.WebControls.Unit(210)
    '            wuc_Menu.HideOrShow(True)
    '        End If

    '        pnlTreeMenuControl.Width = wcuPnlWidth

    '        UpdateMenuPanel.Update()
    '    Catch ex As Exception
    '        ExceptionMsg(PageName & ".ActivateSearchBtn_Click : " & ex.ToString)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        ' Try
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
        If IsPostBack Then RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        'Try
        ChangeReportType()
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub ChangeReportType()
        '   Try
        Dim intMonth As Integer
        If IsNumeric(Session.Item("Month")) Then intMonth = Session.Item("Month")
        Select Case intMonth
            Case 4, 5, 6
                ddlReportType.SelectedValue = 2
            Case 7, 8, 9
                ddlReportType.SelectedValue = 3
            Case 10, 11, 12
                ddlReportType.SelectedValue = 4
            Case Else
                ddlReportType.SelectedValue = 1
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        '   Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        '  Try
        RefreshDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub

#End Region

#Region "Export Extender"

    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        'Try
        Dim blnAllowSorting As Boolean = dgList.AllowSorting
        Dim blnAllowPaging As Boolean = dgList.AllowPaging

        dgList.AllowSorting = False
        dgList.AllowPaging = False
        RefreshDatabinding()

        'PrepareGridViewForExport(dgList)
        'wuc_ctrlpanel.ExportToFile(dgList, "CallCoverage1")
        wuc_ctrlpanel.ExportToFile(dgList, PageName)

        dgList.AllowPaging = blnAllowPaging
        dgList.AllowSorting = blnAllowSorting
        RefreshDatabinding()
        'Catch ex As Threading.ThreadAbortException
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        'End Try
    End Sub

    'Private Sub PrepareGridViewForExport(ByRef gvList As GridView)
    '    Dim lb As LinkButton = New LinkButton
    '    Dim l As Literal = New Literal
    '    Dim name As String = String.Empty
    '    Dim i As Integer = 0
    '    While i < gvList.Controls.Count
    '        If gvList.Controls(i).GetType Is GetType(LinkButton) Then
    '            l.Text = (CType(gvList.Controls(i), LinkButton)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(DropDownList) Then
    '            l.Text = (CType(gvList.Controls(i), DropDownList)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        ElseIf gvList.Controls(i).GetType Is GetType(CheckBox) Then
    '            l.Text = (CType(gvList.Controls(i), CheckBox)).Text
    '            gvList.Controls.Remove(gvList.Controls(i))
    '            gvList.Controls.AddAt(i, l)
    '        End If

    '        'If gvList.Controls(i).HasControls Then
    '        '    PrepareGridViewForExport(gvList.Controls(i))
    '        'End If
    '        System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
    '    End While
    'End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "DataBinding Event"
    Public Sub RenewDataBind()
        'ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
        'RefreshDatabinding()
        'wuc_ctrlpanel.RefreshDetails()
        'wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()

        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)
        ' Try
        ' If dtCurrentTable Is Nothing Then
        dtCurrentTable = GetRecList()
        'ViewState("dtCurrentView") = dtCurrentTable
        'ViewState("strSortExpression") = Nothing
        'End If
        If dtCurrentTable Is Nothing Then
            dtCurrentTable = New DataTable
        Else
            PreRenderMode(dtCurrentTable)
            If dtCurrentTable.Rows.Count = 0 Then
                dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
            End If
        End If

        Dim dvCurrentView As New DataView(dtCurrentTable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If

        dgList.DataSource = dvCurrentView
        dgList.DataBind()
        'Catch ICE As InvalidCastException
        '    'due to add new row
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        UpdateDatagrid_Update()
        ' End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        'Try
        If dgList.Rows.Count < 13 Then
            dgList.GridHeight = Nothing
        End If
        UpdateDatagrid.Update()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        'End Try
    End Sub

#End Region

#Region "Custom DGList"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        ' Try
        'Add Data Grid Columns
        dgList.Columns.Clear()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_MthOverview.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                Case FieldColumntype.TemplateColumn_Percentage
                Case FieldColumntype.InvisibleColumn

                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_MthOverview.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_MthOverview.ColumnStyle(ColumnName).HorizontalAlign

                    dgColumn.HeaderText = CF_MthOverview.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Init : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        '  Try
        If e.Row.RowType = DataControlRowType.Header Then
            Dim oGridView As GridView = dgList 'CType(sender, GridView)
            Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            Dim TC As TableHeaderCell
            Dim CF As ListItem
            Dim intCounter As Integer = 0
            Dim intActualIndex As Integer = 0

            For Each CF In licHeaderCollector
                If CF.Value = 1 Then
                    Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                    intActualIndex = IIf(intCounter > 0, intCounter, 0)
                    If iIndex >= 0 Then
                        e.Row.Cells(intActualIndex).RowSpan = 2
                        e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                        GVR.Cells.Add(e.Row.Cells(intActualIndex))
                    End If
                Else
                    TC = New TableHeaderCell
                    TC.Text = CF.Text
                    TC.ColumnSpan = CF.Value
                    intCounter += CF.Value
                    GVR.Cells.Add(TC)
                End If
            Next
            oGridView.Controls(0).Controls.AddAt(0, GVR)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        'End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        ' Try
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
            Case DataControlRowType.Footer
                e.Row.Cells(0).Text = "Average"
                Dim iIndex As Integer
                Dim intRowCount As Integer = dgList.Rows.Count

                For Each li As ListItem In licItemFigureCollector
                    iIndex = aryDataItem.IndexOf(li.Text)
                    If iIndex >= 0 Then
                        e.Row.Cells(iIndex).Text = String.Format(CF_MthOverview.GetOutputFormatString(li.Text), IIf(intRowCount > 0 AndAlso IsNumeric(li.Value), CDbl(CDbl(li.Value) / CDbl(intRowCount)), 0))
                    End If
                Next
        End Select
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        ' Try
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        'Try
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        'Finally
        'End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        ' Try
        Dim intNetValue, intSelection As Integer
        Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth As String

        strUserID = Session.Item("UserID")
        strPrincipalID = Session("PRINCIPAL_ID")
        strPrincipalCode = Session("PRINCIPAL_CODE")
        strYear = Session.Item("Year")
        strMonth = Session.Item("Month")
        intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))
        intSelection = 1

        Dim strGroupCode, strGroupField, strSalesRepList As String
        strGroupCode = Session("NODE_GROUP_NAME")
        strGroupField = "TREE_" & strGroupCode
        strSalesRepList = Session("SALESREP_LIST")

        'Stored Criteria into Static Value Collector
        With CriteriaCollector
            .GroupField = ""
            .PrincipalID = strPrincipalID
            .PrincipalCode = strPrincipalCode
            .Year = strYear
            .Month = strMonth
            .Tree_Path = Session("TREE_PATH")
            .NetValue = intNetValue
        End With

        Dim clsDB As New rpt_KPI.clsMthOverview
        DT = clsDB.GetMonthlyOverview(strUserID, strPrincipalID, strPrincipalCode, strYear, strMonth, strSalesRepList, strGroupField, intSelection, intNetValue)

        If DT Is Nothing OrElse DT.Columns.Count = 0 Then Return DT

        PreRenderMode(DT)

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        'Finally
        'End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        ' Try
        aryDataItem.Clear()
        dgList_Init(DT)
        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        'Try
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        'M1_*, M2_*, M3_*, QUARTER_*
        Dim intMonth As Integer
        intMonth = Session.Item("Month")

        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "MTD_*" Then
                'strColumnName = CF_MthOverview.GetMonthValue(1, ddlReportType.SelectedValue)
                strColumnName = CF_MthOverview.GetMonthName(intMonth)
            ElseIf strColumnName Like "QTD_*" Then
                'strColumnName = "Quarter " & ddlReportType.SelectedValue
                strColumnName = "Quarter " & CF_MthOverview.GetQuarterValue(Session.Item("Year"), intMonth, Session.Item("UserID"))
                'Else
                '    strColumnName = "UKNOWN"
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        'Try
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            'M1_*, M2_*, M3_*, QUARTER_*
            strColumnName = DC.ColumnName.ToUpper
            If strColumnName Like "MTD_*" OrElse _
             strColumnName Like "QTD_*" Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        'Finally
        'End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = SortingExpression
        'Try
        If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
            If strSortExpression Like (e.SortExpression & "*") Then
                If strSortExpression.IndexOf(" DESC") > 0 Then
                    strSortExpression = e.SortExpression
                Else
                    strSortExpression = e.SortExpression & " DESC"
                End If
            Else
                strSortExpression = e.SortExpression
            End If
        Else
            strSortExpression = e.SortExpression
        End If
        SortingExpression = strSortExpression
        CriteriaCollector.SortExpression = strSortExpression
        RefreshDatabinding()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        'Finally
        'End Try
    End Sub
#End Region

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then RenewDataBind()
        TimerControl1.Enabled = False
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        RenewDataBind()
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        'End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        '    Try
        lblErr.Text = ""
        lblErr.Text = strMsg

        'Call error log class
        Dim objLog As cor_Log.clsLog
        objLog = New cor_Log.clsLog
        With objLog
            .clsProperties.LogTypeID = 1
            .clsProperties.DateLogIn = Now
            .clsProperties.DateLogOut = Now
            .clsProperties.SeverityID = 4
            .clsProperties.LogMsg = strMsg
            .Log()
        End With
        objLog = Nothing

        'Catch ex As Exception

        'End Try
    End Sub
End Class


Public Class CF_MthOverview

    Public Shared Function FilterPrefixName(ByVal columnName As String) As String
        'TEAM, SALESMAN, SMNAME,
        'M1_TOTAL_SALES, M1_TOTAL_CALLS, M1_A_CALL, M1_B_CALL, M1_C_CALL, M1_O_CALL, 
        'M2_TOTAL_SALES, M2_TOTAL_CALLS, M2_A_CALL, M2_B_CALL, M2_C_CALL, M2_O_CALL, 
        'M3_TOTAL_SALES, M3_TOTAL_CALLS, M3_A_CALL, M3_B_CALL, M3_C_CALL, M3_O_CALL, 
        'QUARTER_TOTAL_SALES, QUARTER_TOTAL_CALLS, QUARTER_A_CALL, QUARTER_B_CALL, QUARTER_C_CALL, QUARTER_C_CALL
        Dim strName As String = ""
        Try
            If Not String.IsNullOrEmpty(columnName) Then
                strName = columnName.ToUpper.Replace("MTD_", "").Replace("QTD_", "")
            End If
        Catch ex As Exception
        End Try
        Return strName
    End Function

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        Dim strNewName As String = FilterPrefixName(ColumnName.ToUpper)

        Select Case strNewName
            Case "SALES_TGT"
                strFieldName = "Target Sales"
            Case "SALES_TTL"
                strFieldName = "Sales"
            Case "CALL_TGT"
                strFieldName = "Target Call"
            Case "CALL_TTL"
                strFieldName = "Call"
            Case "EFT_CALL"
                strFieldName = "Effective Call"
            Case "CALL_A_ACH"
                strFieldName = "A %"
            Case "CALL_B_ACH"
                strFieldName = "B %"
            Case "CALL_C_ACH"
                strFieldName = "C %"
            Case "CALL_OTH_ACH"
                strFieldName = "OTH %"
            Case "TGT_ACH"
                strFieldName = "Ach %"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        ' Try
        'Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)

        'Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        ''show only Figure column
        'Select Case strNewName
        '    Case "SALESREP_CODE", "REGION_CODE"
        '        FCT = FieldColumntype.InvisibleColumn
        '    Case Else
        '        FCT = FieldColumntype.BoundColumn
        'End Select
        'Return FCT

        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Return FCT
        'Catch ex As Exception

        'End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = FilterPrefixName(strColumnName.ToUpper)
        '  Try
        ' "TOTAL_SALES", "TIER2", "TIER3", "TIER_O", "TIER_TOTAL"
        Select Case strNewName
            Case "CALL_A_ACH", "CALL_B_ACH", "CALL_C_ACH", "CALL_OTH_ACH", "TGT_ACH"
                strStringFormat = "{0:0.0}"
            Case "SALES_TTL", "SALES_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "CALL_TTL", "CALL_TGT", "EFT_CALL"
                strStringFormat = "{0:0}"
            Case Else
                strStringFormat = ""
        End Select

        'If strColumnName Like "*RATE_*" Then strStringFormat = "{0:0.00}"
        'Catch ex As Exception
        'End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        'Try
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        'Catch ex As Exception

        'End Try
        Return CS
    End Function

    'Public Shared Function GetMonthValue(ByVal intMonthIndex As Integer, ByVal intQuarterIndex As Integer) As String
    '    Dim strMonthValue As String = ""
    '    Try
    '        Dim intMonth As Integer = (3 * intQuarterIndex) - (3 - intMonthIndex)
    '        Select Case intMonth
    '            Case 1
    '                strMonthValue = "January"
    '            Case 2
    '                strMonthValue = "February"
    '            Case 3
    '                strMonthValue = "March"
    '            Case 4
    '                strMonthValue = "April"
    '            Case 5
    '                strMonthValue = "May"
    '            Case 6
    '                strMonthValue = "June"
    '            Case 7
    '                strMonthValue = "July"
    '            Case 8
    '                strMonthValue = "August"
    '            Case 9
    '                strMonthValue = "September"
    '            Case 10
    '                strMonthValue = "Octorber"
    '            Case 11
    '                strMonthValue = "November"
    '            Case 12
    '                strMonthValue = "December"
    '            Case Else
    '                strMonthValue = "UNKNOWN"

    '        End Select
    '    Catch ex As Exception
    '        Throw (New ExceptionMsg("CF_MthOverview.GetMonthValue : " & ex.ToString))
    '    End Try
    '    Return strMonthValue
    'End Function

    Public Shared Function GetMonthName(ByVal intMonth As Integer) As String
        Dim strMonthValue As String = ""
        'Try
        Select Case intMonth
            Case 1
                strMonthValue = "January"
            Case 2
                strMonthValue = "February"
            Case 3
                strMonthValue = "March"
            Case 4
                strMonthValue = "April"
            Case 5
                strMonthValue = "May"
            Case 6
                strMonthValue = "June"
            Case 7
                strMonthValue = "July"
            Case 8
                strMonthValue = "August"
            Case 9
                strMonthValue = "September"
            Case 10
                strMonthValue = "October"
            Case 11
                strMonthValue = "November"
            Case 12
                strMonthValue = "December"
            Case Else
                strMonthValue = "UNKNOWN"

        End Select
        'Catch ex As Exception
        '    Throw (New ExceptionMsg("CF_MthOverview.GetMonthName : " & ex.ToString))
        'End Try
        Return strMonthValue
    End Function

    Public Shared Function GetQuarterValue(ByVal intYear As Integer, ByVal intMonth As Integer, ByVal strUserId As Integer) As String
        Dim strQuarterValue As String = ""
        '  Try
        'Select Case intMonth
        '    Case 4, 5, 6
        '        strQuarterValue = "2"
        '    Case 7, 8, 9
        '        strQuarterValue = "3"
        '    Case 10, 11, 12
        '        strQuarterValue = "4"
        '    Case Else
        '        strQuarterValue = "1"
        'End Select

        Dim dt As DataTable
        Dim clsCallQuery As New rpt_CALL.clsCallQuery

        dt = clsCallQuery.GetQuarterlyCallRptType(intYear, intMonth, strUserId)
        If dt.Rows.Count > 0 Then
            If dt.Rows(0)(0) = "0" Then
                strQuarterValue = "1"
            ElseIf dt.Rows(0)(0) = "1" Then
                strQuarterValue = "2"
            ElseIf dt.Rows(0)(0) = "2" Then
                strQuarterValue = "3"
            Else
                strQuarterValue = "4"
            End If
        End If

        'Catch ex As Exception
        '    Throw (New ExceptionMsg("CF_MthOverview.GetQuarterValue : " & ex.ToString))
        'End Try
        Return strQuarterValue
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class









