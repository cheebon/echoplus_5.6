﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DetailingExtraSubCatList.ascx.vb" Inherits="iFFMA_Detailing_DetailingExtraSubCatList" %>

<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolKit" TagName="wuc_DetailingExtraSubCatPop" Src="~/iFFMA/Detailing/DetailingExtraSubCatPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<asp:UpdatePanel ID="UpdatedgList" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                      <customtoolkit:wuc_dgpaging id="wuc_dgExtrSubCatPaging" runat="server" />
                        <ccgv:clsgridview id="dgExtraSubCatList" runat="server" allowsorting="True" autogeneratecolumns="False"
                            width="98%" freezeheader="True" gridheight="" addemptyheaders="0" cellpadding="2"
                            cssclass="Grid" emptyheaderclass="" freezecolumns="0" freezerows="0" gridwidth=""
                            showfooter="false" allowpaging="True" pagersettings-visible="false"> 
                        </ccgv:clsgridview>
                    </center>
                </td>
            </tr>
        </table>        
        <asp:HiddenField ID="hdDetCode" runat="server" Value="" />
        <asp:HiddenField ID="hdSubDetCode" runat="server" Value="" />
        <customToolkit:wuc_DetailingExtraSubCatPop ID="wuc_DetailingExtraSubCatPop" Title="Extra Category Maintenance (Detailing)" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
