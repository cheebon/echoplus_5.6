'************************************************************************
'	Author	    :	Michelle Ng
'	Date	    :	23/10/2006
'	Purpose	    :	Area User Control 
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Option Explicit On
'Option Strict On

Imports System.Data

Partial Class include_wuc_ddlArea
    Inherits System.Web.UI.UserControl

    Private dt As DataTable
    Public Event SelectedIndexChanged As EventHandler

    Sub OnSelectedIndexChanged(ByVal e As EventArgs)
        RaiseEvent SelectedIndexChanged(Me, e)
    End Sub

    Sub ControlSelectedIndexChanged(ByVal s As Object, ByVal e As EventArgs) Handles ddlAreaID.SelectedIndexChanged
        OnSelectedIndexChanged(e)
    End Sub

    Public Property SelectedValue() As Long
        Get
            If ddlAreaID.SelectedValue = String.Empty Then
                Return 0
            End If
            Return Long.Parse(ddlAreaID.SelectedValue)
        End Get
        Set(ByVal Value As Long)
            Dim checkExist As ListItem
            checkExist = ddlAreaID.Items.FindByValue(Value.ToString())
            If checkExist Is Nothing Then
                ddlAreaID.SelectedValue = "0"
            Else
                ddlAreaID.SelectedValue = Value.ToString()
            End If
        End Set
    End Property

    Public ReadOnly Property SelectedItem() As ListItem
        Get
            Return ddlAreaID.SelectedItem
        End Get
    End Property

    'Public Property CssClass() As String
    '    Get
    '        Return ddlAreaID.CssClass
    '    End Get
    '    Set(ByVal Value As String)
    '        ddlAreaID.CssClass = Value
    '    End Set
    'End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlAreaID.AutoPostBack
        End Get
        Set(ByVal Value As Boolean)
            ddlAreaID.AutoPostBack = Value
        End Set
    End Property

    Public Property SetVisible() As Boolean
        Get
            Return ddlAreaID.Visible
        End Get
        Set(ByVal Value As Boolean)
            ddlAreaID.Visible = Value
        End Set
    End Property

    Public Property SetEnable() As Boolean
        Get
            Return ddlAreaID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            ddlAreaID.Enabled = Value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return dt
        End Get
        Set(ByVal Value As DataTable)
            dt = Value
        End Set
    End Property

    Public Property RequiredValidation() As Boolean
        Get
            Return cfvAreaID.Visible 'cfvAreaID.Enabled
        End Get
        Set(ByVal Value As Boolean)
            'cfvAreaID.Enabled = Value
            cfvAreaID.Visible = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

    End Sub

    Public Overrides Sub DataBind()
        Dim objCommonQuery As cor_Common.clsCommonQuery
        Dim drRow As DataRow

        Try
            If ddlAreaID.Items.Count <= 0 Then
                'Get Area List
                objCommonQuery = New cor_Common.clsCommonQuery
                With objCommonQuery
                    '    .clsProperties.AreaID = lngAreaID
                    '    .clsProperties.DocTypeID = lngDocTypeID
                    'dt = .GetAreaList()
                End With
                objCommonQuery = Nothing

                'ddlAreaID.Items.Clear()
                ddlAreaID.Items.Add(New ListItem("Select", 0))
                If dt.Rows.Count > 0 Then
                    For Each drRow In dt.Rows
                        ddlAreaID.Items.Add(New ListItem(drRow("Name"), drRow("AreaID")))
                    Next
                End If
            End If
            Exit Sub

        Catch ex As Exception
            Throw (New ExceptionMsg("wuc_ddlArea.DataBind : " & ex.ToString))
        Finally
            objCommonQuery = Nothing
        End Try
    End Sub

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
            'Dim objLog As corOB_Log.clsLog
            'objLog = New corOB_Log.clsLog
            'With objLog 
            '    .clsProperties.UserID = "0"
            '    .clsProperties.ModuleID = "0"
            '    .clsProperties.LogMsg = msg
            '    .Log()
            'End With
            'objLog = Nothing
            Exit Sub
        End Sub
    End Class
End Class


