Imports System.Data

Partial Class iFFMA_SAP_SAPList_v2
    Inherits System.Web.UI.Page

    Private intPageSize As Integer
    Dim aryDataItem As New ArrayList
    Dim licItemFigureCollector As ListItemCollection
    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_SalesList")
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_SalesList") = value
        End Set
    End Property
    Public Property SortingExpression() As String
        Get
            Return ViewState("strSortExpression")
        End Get
        Set(ByVal value As String)
            ViewState("strSortExpression") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblErr.Text = ""
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            With wuc_lblheader
                .Title = "SAP List"
                .DataBind()
                .Visible = True
            End With

            If Not Page.IsPostBack Then


                'Call Paging
                With wuc_dgpaging
                    .PageCount = dgList.PageCount
                    .CurrentPageIndex = dgList.PageIndex
                    .DataBind()
                    .Visible = True
                End With

                DissectIncomingRequest()
                FirstTimeLoader()
            End If
        Catch ex As Exception
            ExceptionMsg("SAPList_v2.Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "DGList"
    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Dim drv As DataRowView
        Dim imgButton As Image
        Dim url As HyperLink

        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow

                    imgButton = CType(e.Row.Cells(0).Controls(0), Image)
                    drv = CType(e.Row.DataItem, DataRowView)
                    If drv("SAP_STATUS").ToString = "1" Then
                        'If Not IsNothing(CType(e.Row.FindControl("lnkUnBlock_SAP"), LinkButton)) Then
                        '    CType(e.Row.FindControl("lnkUnBlock_SAP"), LinkButton).Visible = True
                        'End If
                        imgButton.Visible = True
                    Else
                        'If Not IsNothing(CType(e.Row.FindControl("lnkUnBlock_SAP"), LinkButton)) Then
                        '    CType(e.Row.FindControl("lnkUnBlock_SAP"), LinkButton).Visible = False
                        'End If
                        imgButton.Visible = False
                    End If

                    url = CType(e.Row.Cells(licHeaderCollector.IndexOf(licHeaderCollector.FindByText("Txn No"))).Controls(0), HyperLink)


            End Select
        Catch ex As Exception
            Throw
        End Try

    End Sub
    Protected Sub dgList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim objSAP As mst_SAP.clsSAP
        Dim intCurrentPage As Integer

        Try

            Select Case e.CommandName.ToUpper

                Case "UNBLOCKSAP"
                    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                    UpdateSAP(index)

                    intCurrentPage = wuc_dgpaging.CurrentPageIndex
                    dgList.PageIndex = 0

                    BindGrid(ViewState("SortCol"))

                    If wuc_dgpaging.PageCount - 1 < intCurrentPage Then
                        wuc_dgpaging.CurrentPageIndex = wuc_dgpaging.PageCount - 1
                    Else
                        wuc_dgpaging.CurrentPageIndex = intCurrentPage
                    End If
                    wuc_dgpaging.PageNo = wuc_dgpaging.CurrentPageIndex + 1
                    dgList.PageIndex = wuc_dgpaging.CurrentPageIndex

                    BindGrid(ViewState("SortCol"))
            End Select

            'End If

        Catch ex As Exception
            ExceptionMsg("SAPList.dgList_RowCommand : " + ex.ToString)
        Finally
            objSAP = Nothing
        End Try
    End Sub
    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        ' Dim SortCol As 

        Try
            'For Each SortCol In dgList.Columns
            '    If SortCol.SortExpression = e.SortExpression Then
            '        If e.SortExpression.IndexOf(" DESC") <> -1 Then
            '            SortCol.SortExpression = SortCol.SortExpression.Replace(" DESC", "")
            '        Else
            '            SortCol.SortExpression = SortCol.SortExpression + " DESC"
            '        End If
            '    End If
            'Next SortCol

            Dim strSortExpression As String = ViewState("SortCol")

            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If

            dgList.EditIndex = -1
            ViewState("SortCol") = strSortExpression
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region
#Region "Binding"
    Private Sub BindGrid(ByVal SortExpression As String)
        Dim dt As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(SortingExpression, String)

        Try

            'If dtCurrentTable Is Nothing Then
            dt = GetRecList()
           
            If dt Is Nothing Then
                dt = New DataTable
            Else
                If dt.Rows.Count = 0 Then
                    dt.Rows.Add(dt.NewRow())
                End If
            End If

            dt.DefaultView.Sort = SortExpression


            'dgList.PagerStyle.PagerStyle.Mode = PagerMode.NumericPages
            dgList.PageSize = intPageSize
            dgList.GridWidth = Unit.Percentage(100)
            dgList.DataSource = dt.DefaultView
            dgList.DataBind()

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount.ToString
                .CurrentPageIndex = dgList.PageIndex
            End With

        Catch ex As Exception
            ExceptionMsg("SAPList.BindGrid : " & ex.ToString)
        Finally
            dt = Nothing
        End Try
    End Sub

    Private Function GetRecList() As DataTable
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable = Nothing

        Try
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.doc_type = Trim(ddlDocType.SelectedValue)
                .clsProperties.search_type = Trim(ddlSearchType.SelectedValue)
                .clsProperties.team_code = Trim(ddlTeam.SelectedValue)
                .clsProperties.user_code = Trim(Session("UserCode"))
                Select Case ddlSearchType.SelectedValue.ToUpper
                    Case "TXN_TIMESTAMP"
                        .clsProperties.search_value = Trim(Replace(Wuc_txtDate1.Text, "*", "%"))
                        .clsProperties.search_value_1 = Trim(Replace(Wuc_txtDate2.Text, "*", "%"))
                    Case "TXN_STATUS" '"StatusID"   'HL:20070622
                        .clsProperties.search_value = Trim(ddlList.SelectedValue)
                    Case Else
                        .clsProperties.search_value = Trim(Replace(txtSearchValue.Text, "*", "%"))
                End Select
                dt = .GetSAPList
            End With
            If dt Is Nothing OrElse dt.Columns.Count = 0 Then Return dt

            PreRenderMode(dt)

        Catch ex As Exception
            ExceptionMsg("SAPList.GetRecList : " & ex.ToString)
        Finally
            'dt = Nothing
            objSAPQuery = Nothing
        End Try
        Return dt
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        aryDataItem.Clear()
        dgList_Init(DT)

        Cal_CustomerHeader()
        Cal_ItemFigureCollector(DT)
    End Sub
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        'Add Data Grid Columns
        'dgList.Columns.Clear()
        ClearDGListColumns()

        Dim ColumnName As String = ""
        For i = 0 To dtToBind.Columns.Count - 1
            ColumnName = dtToBind.Columns(i).ColumnName
            Select Case CF_SalesList.GetFieldColumnType(ColumnName)
                Case FieldColumntype.HyperlinkColumn
                    Dim dgColumn As New HyperLinkField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_SalesList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_SalesList.GetOutputFormatString(ColumnName)
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SalesList.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SalesList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataTextField = ColumnName
                    dgColumn.SortExpression = ColumnName

                    Dim strUrlFormatString As String
                    Dim strUrlFields(1) As String
                    Dim strUrlField(0) As String
                    strUrlField(0) = ColumnName

                    strUrlFormatString = FormUrlFormatString(0, dtToBind, strUrlFields)
                    dgColumn.DataNavigateUrlFields = strUrlField
                    dgColumn.DataNavigateUrlFormatString = strUrlFormatString
                    dgColumn.Target = "_self"
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case FieldColumntype.ButtonColumn
                    Dim dgColumn As New ButtonField ' HyperLinkColumn

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                    dgColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center

                    If String.IsNullOrEmpty(CF_SalesList.GetOutputFormatString(ColumnName)) = False Then
                        dgColumn.DataTextFormatString = CF_SalesList.GetOutputFormatString(ColumnName)
                    End If
                    'dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SalesList.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SalesList.GetDisplayColumnName(ColumnName)
                    dgColumn.Text = "Unblock"
                    dgColumn.SortExpression = ColumnName
                    dgColumn.CommandName = "UnblockSAP"
                    dgColumn.ButtonType = ButtonType.Image
                    dgColumn.ImageUrl = "../../images/ico_Unblock.gif"
                    Dim strUrlFormatString As String
                    Dim strUrlFields() As String = Nothing

                   
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
                Case Else
                    Dim dgColumn As New BoundField 'BoundColumn

                    dgColumn.ReadOnly = True

                    dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                    dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                    Dim strFormatString As String = CF_SalesList.GetOutputFormatString(ColumnName)
                    If String.IsNullOrEmpty(strFormatString) = False Then
                        dgColumn.DataFormatString = strFormatString 'CF_SalesList.GetOutputFormatString(ColumnName)
                        dgColumn.HtmlEncode = False
                    End If
                    dgColumn.ItemStyle.HorizontalAlign = CF_SalesList.ColumnStyle(ColumnName).HorizontalAlign
                    dgColumn.ItemStyle.Wrap = CF_SalesList.ColumnStyle(ColumnName).Wrap

                    dgColumn.HeaderText = CF_SalesList.GetDisplayColumnName(ColumnName)
                    dgColumn.DataField = ColumnName
                    dgColumn.SortExpression = ColumnName
                    dgList.Columns.Add(dgColumn)
                    dgColumn = Nothing

                    'Add the field name
                    aryDataItem.Add(ColumnName)
            End Select
        Next
    End Sub
    Private Sub ClearDGListColumns()
        For i As Integer = dgList.Columns.Count - 1 To 0 Step -1
            dgList.Columns.RemoveAt(i)
        Next
    End Sub
    Private Sub Cal_CustomerHeader()
        'aryDataItem will refresh each time rebind
        'aryColumnFieldCollector will refresh only reget the data from the database
        licHeaderCollector = New ListItemCollection

        Dim blnisNew As Boolean
        Dim strColumnName As String
        Dim liColumnField As ListItem
        Dim aryCuttedName As New ArrayList

        'collect the column name that ready to show on the screen
        For Each strColumnName In aryDataItem
            strColumnName = strColumnName.ToUpper
            If strColumnName Like "MTD*" Then
                strColumnName = "MTD Sales"
            ElseIf strColumnName Like "QTD*" Then
                strColumnName = "QTD Sales"
            ElseIf strColumnName Like "YTD*" Then
                If strColumnName Like "YTDGROSS" Then
                    strColumnName = "PYTD Sales"
                Else
                    strColumnName = "YTD Sales"
                End If
            ElseIf strColumnName Like "PYTD*" Then
                strColumnName = "PYTD Sales"
            Else
                strColumnName = CF_SalesList.GetDisplayColumnName(strColumnName)
            End If
            aryCuttedName.Add(strColumnName)
        Next

        'witht the cutted columnName, fill in the Occorance count
        For Each strColumnName In aryCuttedName
            blnisNew = True
            liColumnField = licHeaderCollector.FindByText(strColumnName)

            If Not liColumnField Is Nothing Then
                liColumnField.Value = CInt(liColumnField.Value) + 1
            Else
                liColumnField = New ListItem(strColumnName, 1)
                licHeaderCollector.Add(liColumnField)
            End If

        Next
        licHeaderCollector = _licCustomHeaderCollector
    End Sub
    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'Calculate ALL column Figure except PERCENT_*
        licItemFigureCollector = New ListItemCollection
        Dim strColumnName As String
        Dim liColumnField As ListItem
        For Each DC As DataColumn In DT.Columns
            strColumnName = DC.ColumnName.ToUpper
            '
            '
            If (strColumnName Like "MTD*" OrElse _
             strColumnName Like "QTD*" OrElse _
             strColumnName Like "YTD*" OrElse strColumnName Like "PYTD*") _
             AndAlso Not (strColumnName Like "*VAR" OrElse strColumnName Like "*GROSS") Then
                liColumnField = New ListItem(strColumnName, 0)
                licItemFigureCollector.Add(liColumnField)
            End If
        Next

        For Each DR As DataRow In DT.Rows
            For Each li As ListItem In licItemFigureCollector
                li.Value = SUM(li.Value, DR(li.Text))
            Next
        Next
    End Sub
#End Region
#Region "Function"
    Private Function FormUrlFormatString(ByVal intIndex As Integer, ByRef dtToBind As DataTable, ByRef strUrlFields() As String) As String
        Dim strUrlFormatString As String = String.Empty
        Dim DocType, SearchType, SearchValue, SearchValue1 As String

        Select Case intIndex
            Case 0 'Generate sales info by date links
                DocType = Trim(ddlDocType.SelectedValue.ToString)
                SearchType = Trim(ddlSearchType.SelectedValue.ToString)

                If Trim(ddlSearchType.SelectedValue.ToString) = "TXN_TIMESTAMP" Then
                    'Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(Wuc_txtDate1.Text) & "&search_value_1=" & Trim(Wuc_txtDate2.Text), False)
                    SearchValue = Trim(Wuc_txtDate1.Text)
                    SearchValue1 = Trim(Wuc_txtDate2.Text)
                ElseIf Trim(ddlSearchType.SelectedValue.ToString) = "TXN_STATUS" Then
                    'Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(ddlList.SelectedValue) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
                    SearchValue = Trim(ddlList.SelectedValue)
                    SearchValue1 = Trim(txtSearchValue1.Text)
                Else
                    'Response.Redirect("SAPDtl.aspx?txn_no=" & strTxn_no & "&doc_type=" & Trim(ddlDocType.SelectedValue.ToString) & "&search_type=" & Trim(ddlSearchType.SelectedValue.ToString) & "&search_value=" & Trim(txtSearchValue.Text) & "&search_value_1=" & Trim(txtSearchValue1.Text), False)
                    SearchValue = Trim(txtSearchValue.Text)
                    SearchValue1 = Trim(txtSearchValue1.Text)
                End If

                strUrlFormatString = "SAPDtl_v2.aspx?txn_no={0}&doc_type=" & DocType & "&search_type=" & SearchType & "&search_value=" & SearchValue & "&search_value_1=" & SearchValue1 & "&team=" & ddlTeam.SelectedValue

            Case Else
                strUrlFields = Nothing
                strUrlFormatString = ""
        End Select
        Return strUrlFormatString
    End Function
    Private Sub ddlSearchType_onChanged()
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            'txtSearchValue.Text = ""
            'txtSearchValue1.Text = ""
            lblSearchValue.Text = ""
            ddlList.SelectedValue = "0"
            Wuc_txtDate1.Text = ""
            Wuc_txtDate2.Text = ""

            lblSearchValue.Visible = False
            'lblDot.Visible = False
            lblTo.Visible = False
            txtSearchValue.Visible = False
            txtSearchValue.ReadOnly = False
            txtSearchValue1.Visible = False
            txtSearchValue1.ReadOnly = False
            btnSearch.Visible = False
            ddlList.Visible = False
            Wuc_txtDate1.Visible = False
            Wuc_txtDate2.Visible = False

            Select Case ddlSearchType.SelectedValue.ToUpper
                Case "ALL"
                    btnSearch.Visible = True

                Case "TXN_TIMESTAMP"
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    lblTo.Visible = True
                    'txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    'txtSearchValue.ReadOnly = True
                    'txtSearchValue1.Visible = True
                    'txtSearchValue1.ReadOnly = True
                    'txtSearchValue1.Text = ""
                    btnSearch.Visible = True
                    Wuc_txtDate1.Visible = True
                    Wuc_txtDate2.Visible = True

                Case "STATUS"
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True
                    txtSearchValue.Text = ""   'HL:20070622

                    'Get Status Record
                    ddlList.Items.Clear()
                    Dim objSAPQuery As mst_SAP.clsSAPQuery
                    objSAPQuery = New mst_SAP.clsSAPQuery
                    With objSAPQuery
                        dt = .GetSAPStatusList
                    End With
                    objSAPQuery = Nothing
                    If dt.Rows.Count > 0 Then
                        ddlList.Items.Add(New ListItem("Select", 0))
                        For Each drRow In dt.Rows
                            ddlList.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
                        Next
                    End If

                Case "TXN_STATUS"
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    btnSearch.Visible = True
                    ddlList.Visible = True
                    txtSearchValue.Text = ""   'HL:20070622

                    'Get Status Record
                    ddlList.Items.Clear()
                    Dim objSAPQuery As mst_SAP.clsSAPQuery
                    objSAPQuery = New mst_SAP.clsSAPQuery
                    With objSAPQuery
                        dt = .GetSAPTxnStatusList
                    End With
                    objSAPQuery = Nothing
                    If dt.Rows.Count > 0 Then
                        ddlList.Items.Add(New ListItem("Select", 0))
                        For Each drRow In dt.Rows
                            ddlList.Items.Add(New ListItem(drRow("STATUS"), drRow("STATUS")))
                        Next
                    End If

                Case Else
                    lblSearchValue.Visible = True
                    'lblDot.Visible = True
                    lblSearchValue.Text = ddlSearchType.SelectedItem.Text
                    txtSearchValue.Visible = True
                    'txtSearchValue.Text = ""
                    btnSearch.Visible = True
            End Select

        Catch ex As Exception
            lblErr.Text = "SAPList_v2.ddlSearchType_onChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Private Sub DissectIncomingRequest()
        Dim strDocType, strSearchType, strSearchValue, strSearchValue1, strTeam As String

        Try
            strDocType = IIf(Trim(Request.QueryString("doc_type")) <> "", Trim(Request.QueryString("doc_type")), "SO")
            strSearchType = IIf(Trim(Request.QueryString("search_type")) <> "", Trim(Request.QueryString("search_type")), "LASTUPDATE")
            strSearchValue = Trim(Request.QueryString("search_value"))
            strSearchValue1 = Trim(Request.QueryString("search_value_1"))

            'Prepare Recordset,Createobject as needed
            ddlDocType.SelectedValue = strDocType
            ddlSearchType.SelectedValue = strSearchType
            ddlSearchType_onChanged()

            If Trim(strSearchType) = "TXN_TIMESTAMP" Then
                Wuc_txtDate1.Text = IIf(strSearchValue <> "", strSearchValue, Now.ToString("yyyy-MM-dd"))
                Wuc_txtDate2.Text = IIf(strSearchValue1 <> "", strSearchValue1, Now.ToString("yyyy-MM-dd"))
            ElseIf Trim(strSearchType) = "TXN_STATUS" Then
                ddlList.SelectedValue = IIf(strSearchValue <> "", strSearchValue, "")
            Else
                txtSearchValue.Text = IIf(strSearchValue <> "", strSearchValue, "")
                txtSearchValue1.Text = IIf(strSearchValue1 <> "", strSearchValue1, "")
            End If



        Catch ex As Exception
            ExceptionMsg("SAPList_v2.DissectIncomingRequest : " & ex.ToString)
        End Try
    End Sub

    Private Sub FirstTimeLoader()
        Dim strTeam As String
        Try
            GetTeam()
            strTeam = Trim(Request.QueryString("team"))
            ddlTeam.SelectedValue = strTeam
            BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            ExceptionMsg("SAPList_v2.FirstTimeLoader : " & ex.ToString)
        End Try
    End Sub
    Private Sub GetTeam()
        Dim objSAPQuery As mst_SAP.clsSAPQuery
        Dim dt As DataTable
        Dim drRow As DataRow

        Try
            ddlTeam.Items.Clear()
            objSAPQuery = New mst_SAP.clsSAPQuery
            With objSAPQuery
                .clsProperties.user_code = Trim(Session("UserCode"))
                dt = .GetSAPTeamList
            End With
            If dt.Rows.Count > 0 Then
                ddlTeam.Items.Add(New ListItem("All", "ALL"))
                For Each drRow In dt.Rows
                    ddlTeam.Items.Add(New ListItem(drRow("team_code").ToString.Trim, drRow("team_code").ToString.Trim))
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function DIVISION(ByVal liDividend As ListItem, ByVal liDivisor As ListItem) As Double
        Dim dblValue As Double = 0.0
        'Try
        If liDividend IsNot Nothing AndAlso liDivisor IsNot Nothing AndAlso liDivisor.Value > 0 Then
            dblValue = ConvertToDouble(liDividend.Value) / ConvertToDouble(liDivisor.Value)
        End If
        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".GetValue : " & ex.ToString)
        'End Try
        Return dblValue
    End Function

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Dim iValue1 As Double = ConvertToDouble(Value1)
        Dim iValue2 As Double = ConvertToDouble(Value2)
        Return iValue1 + iValue2
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Dim dblValue As Double = 0.0

        If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)
        Return dblValue
    End Function
#End Region
#Region "Paging"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = wuc_dgpaging.PageNo - 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"))
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.btnGo_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"))
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.lnkPrevious_OnClick : " + ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            BindGrid(ViewState("SortCol"))
            Exit Sub

        Catch ex As Exception
            ExceptionMsg("SAPList.lnkNext_OnClick : " + ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgList.PageIndex = 0
            BindGrid(ViewState("SortCol"))
            'UpdateSearch.Update()

        Catch ex As Exception
            lblErr.Text = "SAPList.btnSearch_Click : " + ex.ToString()
        End Try
    End Sub

    
    Protected Sub ddlSearchType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ddlSearchType_onChanged()
            'UpdateSearch.Update()
            'BindGrid(ViewState("SortCol"))

        Catch ex As Exception
            lblErr.Text = "SAPList.ddlSearchType_SelectedIndexChanged : " + ex.ToString()
        Finally

        End Try
    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    UpdateSAP()
        'Catch ex As Exception
        '    ExceptionMsg("SAPList_v2.DissectIncomingRequest : " & ex.ToString)
        'End Try
    End Sub

#Region "Update SAP"
    Private Sub UpdateSAP(ByVal i As Integer)
        Dim obj As New mst_SAP.clsSAP
        Dim row As GridViewRow
        Dim ckb As CheckBox
        Dim txn_no As String
        Dim txn_timestamp As String

        Try
            'For i As Integer = 0 To dgList.Rows.Count - 1
            row = dgList.Rows(i)
           
            txn_no = CType(row.Cells(licHeaderCollector.IndexOf(licHeaderCollector.FindByText("Txn No"))).Controls(0), HyperLink).Text
            txn_timestamp = Convert.ToDateTime(row.Cells(licHeaderCollector.IndexOf(licHeaderCollector.FindByText("Timestamp"))).Text)

            obj.SubmitSAP(txn_no, txn_timestamp)

            'Next
        Catch ex As Exception
            Throw
        Finally
            obj = Nothing
        End Try
    End Sub
#End Region

    
    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        Try
            'For i As Integer = 0 To dgList.Rows.Count - 1
           
            'lblErr.Text = dgList.Rows(e.Row.RowIndex).Cells(0).Text
            'Next
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub

    


End Class

Public Class CF_SalesList
    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String) As String
        Dim strFieldName As String = ""
        'SALESREP_CODE ,SALESREP_NAME ,
        'CHAIN_CODE ,CHAIN_NAME ,CHANNEL_CODE ,CHANNEL_NAME ,
        'CUST_CODE ,CUST_NAME ,CUST_GRP_CODE ,CUST_GRP_NAME ,
        'PRD_CODE ,PRD_NAME ,PRD_GRP_CODE ,PRD_GRP_NAME ,
        'SALES_AREA_CODE ,SALES_AREA_NAME ,
        'SHIPTO_CODE ,SHIPTO_NAME

        'TEAM_CODE ,REGION_CODE ,REGION_NAME 
        'MTD_SALES ,MTD_FOC_QTY ,MTD_SALES_QTY ,MTD_TGT ,MTDVAR, 
        'QTD_SALES ,QTD_FOC_QTY ,QTD_SALES_QTY ,QTD_TGT ,QTDVAR ,
        'YTD_SALES ,YTD_FOC_QTY ,YTD_SALES_QTY ,YTD_TGT ,YTDVAR ,
        'PYTD_SALES,PYTD_FOC_QTY ,PYTD_SALES_QTY ,YTDGROSS

        Select Case ColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES"
                strFieldName = "Sales"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY"
                strFieldName = "FOC"
            Case "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strFieldName = "QTY"
            Case "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strFieldName = "Target"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strFieldName = "+/-%"
            Case "TXN_NO"
                strFieldName = "Txn No"
            Case "TXN_STATUS"
                strFieldName = "Txn Status"
            Case "SHIPTO_DATE"
                strFieldName = "Shipto Date"
            Case "VISIT_ID"
                strFieldName = "Visit ID"
            Case "TXN_DATE_IN"
                strFieldName = "Txn Date In"
            Case "TXN_DATE_OUT"
                strFieldName = "Txn Date Out"
            Case "TTL_ORD_AMT"
                strFieldName = "Ord Amt"
            Case "DISC_AMT"
                strFieldName = "Disc Amt"
            Case "PAY_AMT"
                strFieldName = "Pay Amt"
            Case "URGENT_FLAG"
                strFieldName = "Urgent Flag"
            Case "GST_AMT"
                strFieldName = "GST Amt"
            Case "PAY_TERM_NAME"
                strFieldName = "Payterm Name"
            Case "PAY_DATE"
                strFieldName = "Pay Date"
            Case "CUST_DISC_AMT"
                strFieldName = "Cust Disc Amt"
            Case "PARTIAL_DLVY"
                strFieldName = "Partial Dlvy"
            Case "TXN_TIMESTAMP"
                strFieldName = "Timestamp"
            Case "SAP_STATUS"
                strFieldName = "Confirm"
            Case Else
                strFieldName = Report.GetDisplayColumnName(ColumnName)
        End Select

        Return strFieldName
    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
        Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
        strColumnName = strColumnName.ToUpper
        Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
        If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
           (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
            FCT = FieldColumntype.InvisibleColumn
        Else
            FCT = FieldColumntype.BoundColumn
        End If

        Select Case strColumnName
            Case "TXN_NO"
                'If Report.GetAccessRight(ModuleID.FFMR, SubModuleType.SALESINFOBYDATE, "'1','8'") Then
                FCT = FieldColumntype.HyperlinkColumn
                'End If
            Case "SAP_STATUS"
                FCT = FieldColumntype.ButtonColumn
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                FCT = FieldColumntype.TemplateColumn_Percentage
        End Select
        Return FCT
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Select Case strColumnName.ToUpper
            Case "MTD_SALES", "QTD_SALES", "YTD_SALES", "PYTD_SALES", _
                 "MTD_TGT", "QTD_TGT", "YTD_TGT"
                strStringFormat = "{0:#,0.00}"
            Case "MTDVAR", "QTDVAR", "YTDVAR", "YTDGROSS"
                strStringFormat = "{0:#,0.0}"
            Case "MTD_FOC_QTY", "QTD_FOC_QTY", "YTD_FOC_QTY", "PYTD_FOC_QTY", _
                 "MTD_SALES_QTY", "QTD_SALES_QTY", "YTD_SALES_QTY", "PYTD_SALES_QTY"
                strStringFormat = "{0:#,0}"
            Case "SHIPTO_DATE", "TXN_DATE_IN", "TXN_DATE_OUT", "PAY_DATE", "TXN_TIMESTAMP"
                strStringFormat = "{0:yyyy-MM-dd}"
            Case Else
                strStringFormat = ""
        End Select

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        With CS
            Dim strColumnName As String = ColumnName.ToUpper
            .FormatString = GetOutputFormatString(ColumnName)

            If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                .HorizontalAlign = HorizontalAlign.Center
            ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                .HorizontalAlign = HorizontalAlign.Right
                .Wrap = False
            Else
                .HorizontalAlign = HorizontalAlign.Left
            End If

        End With

        Return CS
    End Function
End Class