<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TxnList.aspx.vb" Inherits="iFFMS_Transaction_TxnList" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%--<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>--%>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Transaction List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />

    <script language="javascript" type="text/javascript" src="../../../include/layout.js"></script>
    <script language="javascript" type="text/javascript">
function RefreshContentBar(page)
{var salesrepcode; var custcode; var contcode; var datein; var timein; var visitid;var sessionid;
salesrepcode = document.getElementById("lblsalesrepcode").innerHTML;
sessionid = document.getElementById('lblsessionid').innerHTML;
if (page == 'home') { parent.document.getElementById('ContentBarIframe').src="../../iFFMS/Transaction/TxnDefault.aspx?salesrepcode="+salesrepcode+"&sessionid="+sessionid+"&txntype=drc";ResizeFrameWidth('SubDetailBar','100%');ResizeFrameWidth('SubDetailConfBar','100%');}
if (page == 'drc') { parent.document.getElementById('ContentBarIframe').src="../../iFFMS/Transaction/TxnListGrid.aspx?salesrepcode="+salesrepcode+"&sessionid="+sessionid+"&txntype=drc";ResizeFrameWidth('SubDetailBar','100%');ResizeFrameWidth('SubDetailConfBar','100%');}
if (page == 'mss') { parent.document.getElementById('ContentBarIframe').src="../../iFFMS/Transaction/TxnListGrid.aspx?salesrepcode="+salesrepcode+"&sessionid="+sessionid+"&txntype=mss";ResizeFrameWidth('SubDetailBar','100%');ResizeFrameWidth('SubDetailConfBar','100%');}
if (page == 'so') { parent.document.getElementById('ContentBarIframe').src="../../iFFMS/Transaction/TxnListGrid.aspx?salesrepcode="+salesrepcode+"&sessionid="+sessionid+"&txntype=so";ResizeFrameWidth('SubDetailBar','100%');ResizeFrameWidth('SubDetailConfBar','100%');}
if (page == 'tra') {parent.document.getElementById('ContentBarIframe').src="../../iFFMS/Transaction/TxnListGrid.aspx?salesrepcode="+salesrepcode+"&sessionid="+sessionid+"&txntype=tra";ResizeFrameWidth('SubDetailBar','100%');ResizeFrameWidth('SubDetailConfBar','100%');}
if (page == 'sfms') {parent.document.getElementById('ContentBarIframe').src="../../iFFMS/Transaction/TxnListGrid.aspx?salesrepcode="+salesrepcode+"&sessionid="+sessionid+"&txntype=sfms";ResizeFrameWidth('SubDetailBar','100%');ResizeFrameWidth('SubDetailConfBar','100%');}
if (page == 'sfmsCuz') { parent.document.getElementById('ContentBarIframe').src = "../../iFFMS/Transaction/TxnListGrid.aspx?salesrepcode=" + salesrepcode + "&sessionid=" + sessionid + "&txntype=sfmsCuz"; ResizeFrameWidth('SubDetailBar', '100%'); ResizeFrameWidth('SubDetailConfBar', '100%'); }
if (page == 'main') { HideElement('SubDetailConfBar');HideElement('SubDetailBar');HideElement('DetailBar');ShowElement('TopBar');ShowElement('ContentBar');MaximiseFrameHeight('TopBarIframe');MaximiseFrameHeight('ContentBarIframe');ResizeFrameWidth('ContentBar','50%');;ResizeFrameWidth('TopBar','50%')};
}
function setClass(element)
{if(element){var pElement = element.parentElement.parentElement;for(var i=0;i<pElement.children.length;i++){var innerElement= pElement.children[i].children[0];if(innerElement){if(innerElement==element){innerElement.className='current';}else{innerElement.className='';}}}}}

    </script>

    <style type="text/css">
#styleone{position:relative;display:block;height:21px;font-size:11px;font-weight:bold;background:transparent url(../../images/bgOFF.gif) repeat-x top left;font-family:Arial,Verdana,Helvitica,sans-serif;border-bottom:1px solid #d9d9d9;}
#styleone ul{margin:0;padding:0;list-style-type:none;width:auto;}
#styleone ul li{display:block;float:left;margin:0 1px 0 0;}
#styleone ul li a{display:block;float:left;color:#034895;text-decoration:none;padding:3px 20px 0 20px;height:18px;}
#styleone ul li a:hover,#styleone ul li a.current{color:#034895;background:transparent url(../../images/bgON.gif) repeat-x top left;}
</style>
</head>
<!--#include File="~/include/commonutil.js"-->
<body onload="HideElement('SubDetailConfBar');ShowElement('SubDetailBar'); MaximiseFrameHeight('SubDetailBarIframe');RefreshContentBar('home');"
    style="background-color: #DDDDDD;">
    <form id="frmtxnlist" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <asp:UpdatePanel ID="UpdatePage" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
                <div id="title">
             <%--       <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>--%>
                    <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                    <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                </div>
                <div style="width: 100%; position: relative; padding-left: 1px; margin: 0;" class="Bckgroundreport">
                    <table id="tbldtl" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td>
                                <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblheader>
                                <div style="display:none;">
                                <span class="cls_label">TXN INFO - Salesrep Code :</span><asp:Label runat="server"
                                    ID="lblsalesrepcode" CssClass="cls_label"></asp:Label>| <span class="cls_label">Session
                                        Id:</span><asp:Label runat="server" ID="lblsessionid" CssClass="cls_label"></asp:Label>|
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="styleone">
                                    <ul>
                                           <%  If GetAccessRight(3, 19, "'3'") Then%>
                                        <li><a id="btnTxnDrc" href="#" onclick="RefreshContentBar('drc');setClass(this)">Inventory
                                            Forecast.</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 22, "'3'") Then%>
                                        <li><a id="btnTxnSalesOrd" href="#" onclick="RefreshContentBar('so');setClass(this)">
                                            Sales Order</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 23, "'3'") Then%>
                                        <li><a id="btnTxnTradeRtn" href="#" onclick="RefreshContentBar('tra');setClass(this)">
                                            Goods Return</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 20, "'3'") Then%>
                                        <li><a id="btnsfms" href="#" onclick="RefreshContentBar('sfms');setClass(this)">Field
                                            Activity</a></li>
                                        <%  End If%>
                                          <%  If GetAccessRight(3, 239, "'1'") Then%>
                                        <li><a id="btnsfmsCuz" href="#" onclick="RefreshContentBar('sfmsCuz');setClass(this)">Field
                                            Activity</a></li>
                                        <%  End If%>
                                        <%  If GetAccessRight(3, 21, "'3'") Then%>
                                        <li><a id="btnTxnMss" href="#" onclick="RefreshContentBar('mss');setClass(this)">Market
                                            Survey</a></li>
                                        <%  End If%>
                                        <%-- <li><a href="#" id="btnHome" onclick="RefreshContentBar('home');setClass(this)">Home</a></li>
                                        <li><a id="btnsfms" href="#" onclick="RefreshContentBar('sfms');setClass(this)">Field
                                            Activity</a></li>
                                        <li><a id="btnTxnDrc" href="#" onclick="RefreshContentBar('drc');setClass(this)">Inventory
                                            Forecast.</a></li>
                                        <li><a id="btnTxnMss" href="#" onclick="RefreshContentBar('mss');setClass(this)">Market
                                            Survey</a></li>
                                        <li><a id="btnTxnSalesOrd" href="#" onclick="RefreshContentBar('so');setClass(this)">
                                            Sales Order</a></li>
                                        <li><a id="btnTxnTradeRtn" href="#" onclick="RefreshContentBar('tra');setClass(this)">
                                            Trade Return</a></li>
                                        <%--<li><a id="btnback" href="#" onclick="RefreshContentBar('main');setClass(this)">
                                            Back </a></li>--%>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="Message!!" runat="server" />
    </form>
</body>
</html>
