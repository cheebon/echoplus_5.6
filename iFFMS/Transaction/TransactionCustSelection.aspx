<%@ Page Language="vb" AutoEventWireup="false" Inherits="TransactionCustSelection"
    CodeFile="TransactionCustSelection.aspx.vb" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Reference Control="~/include/wuc_ctrlpanel.ascx" %>
<%@ Reference Control="~/include/wuc_lblheader.ascx" %>
<%@ Reference Control="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_txtDate" Src="~/include/wuc_txtDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PreplanList</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmCallAnalysisListByMonth" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="300">
        </asp:ScriptManager>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <uc1:wuc_ctrlpanel ID="Wuc_ctrlpanel" runat="server"></uc1:wuc_ctrlpanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr">
                                    <ContentTemplate>
                                       
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout">
                                <table border="0" cellpadding="0" cellspacing="0" width="98%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td colspan="3">
                                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                                        </td>
                                    </tr>
                                    <%--<tr class="Bckgroundreport">
                                        <td colspan="3">
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                        </td>
                                        <td valign="top" class="Bckgroundreport">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <%--<tr class="Bckgroundreport">
                                                    <td >
                                                        &nbsp;</td>
                                                        <td>
                                                            <asp:Panel ID="pnlCallDate" runat="server">
                                                                <asp:Label ID="lblSelectCallDate" runat="server" Text="Select a call date" CssClass="cls_label"></asp:Label>&nbsp;&nbsp;<uc1:wuc_txtDate runat="server" ID="txtCallDate" />
                                                            </asp:Panel>                                                            
                                                        </td>
                                                </tr>--%>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td class="Bckgroundreport">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                            <tr>
                                                                <td align="left" style="width: 150px; height: 10px;">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:UpdatePanel runat="server" ID="updateDG">
                                                            <ContentTemplate>
                                                                <ccGV:clsGridView ID="GridView1" runat="server" AddEmptyHeaders="0" AllowSorting="True"
                                                                    AutoGenerateColumns="False" CellPadding="2" CssClass="Grid" EmptyHeaderClass=""
                                                                    FreezeColumns="0" FreezeHeader="True" FreezeRows="0" GridHeight="100px" GridWidth=""
                                                                    RowHighlightColor="AntiqueWhite" ShowFooter="false" Width="98%">
                                                                    <EmptyDataRowStyle Font-Bold="True" HorizontalAlign="Center" />
                                                                    <%--<SelectedRowStyle BackColor="DodgerBlue" Font-Bold="True" ForeColor="Black" />--%>
                                                                    <EmptyDataTemplate>
                                                                        There is no data to display.</EmptyDataTemplate>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="cont_acc_code" HeaderText="Contact Code" />
                                                                        <asp:BoundField DataField="contact_name" HeaderText="Contact Name" />
                                                                        <asp:BoundField DataField="rep_speciality" HeaderText="Rep Specialty" />
                                                                        <asp:BoundField DataField="position" HeaderText="Position" />
                                                                        <asp:BoundField DataField="department" HeaderText="Department" />
                                                                        <asp:BoundField DataField="phone" HeaderText="Phone" />
                                                                        <asp:ButtonField CommandName="Select" Text="Select" />
                                                                    </Columns>
                                                                    <FooterStyle CssClass="GridFooter" />
                                                                    <HeaderStyle CssClass="GridHeader" />
                                                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                                                    <RowStyle CssClass="GridNormal" />
                                                                </ccGV:clsGridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
