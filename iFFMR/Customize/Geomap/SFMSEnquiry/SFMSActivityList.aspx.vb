﻿Imports System.Data
Imports System.IO
Imports rpt_Customize
Imports rpt_Customer

Partial Class iFFMR_Customize_SFMSEnquiry_SFMSActivityList
    Inherits System.Web.UI.Page

    Dim strTxnNo, strTitleCode, strTitleName, strSource As String
    Dim strCurrentCatCode As String
    Dim intRowIndex As Integer
    Dim strColor As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strTxnNo = IIf(IsNothing(Request.QueryString("txnno")), "", Request.QueryString("txnno"))
            strTitleCode = IIf(IsNothing(Request.QueryString("TitleCode")), "", Request.QueryString("TitleCode"))
            strTitleName = IIf(IsNothing(Request.QueryString("TitleName")), "", Request.QueryString("TitleName"))
            strSource = IIf(IsNothing(Request.QueryString("source")), "", Request.QueryString("source"))
            strCurrentCatCode = ""

            If Not Page.IsPostBack Then
                HeaderDataBind()
                RenewDataBind()

                If strSource = "map" Then
                    btnBack.Attributes("onclick") = "ShowElement('MapBar');HideElement('DetailBar');MaximiseFrameHeight('MapBarIframe');"
                Else
                    btnBack.Attributes("onclick") = "ShowElement('ContentBar');HideElement('DetailBar');MaximiseFrameHeight('ContentBarIframe');"
                End If

                'Set Root path for client side
                Dim strRoot As String = ResolveClientUrl("~")
                strRoot = strRoot.Substring(0, strRoot.Length - 1)
                AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, "RootVar", "root= '" & strRoot & "';", True)
            End If

            lblErr.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

#Region "DATA BIND"
    Public Sub HeaderDataBind()
        RefreshHeaderDataBind()
    End Sub
    Public Sub RenewDataBind()
        intRowIndex = 0
        ViewState.Clear()
        RefreshDataBind()
    End Sub
    Public Sub RefreshHeaderDataBind()
        RefreshHeaderDatabinding()
    End Sub
    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSS As New clsSFMSEnqQuery

            DT = clsMSS.GetSFMSAnswer(strTxnNo)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
    Private Function GetHeaderList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMSS As New clsSFMSEnqQuery

            DT = clsMSS.GetSFMSHeader(strTxnNo)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Public Sub RefreshHeaderDatabinding()
        Dim dtCurrentTable As DataTable = Nothing
        Try
            dtCurrentTable = GetHeaderList()

            If Not IsNothing(dtCurrentTable) Then
                If dtCurrentTable.Rows.Count > 0 Then
                    lblTxnNo.Text = dtCurrentTable.Rows(0)("TXN_NO").ToString
                    lblTxnDate.Text = dtCurrentTable.Rows(0)("TXN_DATE").ToString

                    lblSalesrep.Text = dtCurrentTable.Rows(0)("SALESREP_CODE").ToString & " - " & dtCurrentTable.Rows(0)("SALESREP_NAME").ToString
                    lblCustomer.Text = dtCurrentTable.Rows(0)("CUST_CODE").ToString & " - " & dtCurrentTable.Rows(0)("CUST_NAME").ToString

                End If
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False)
        Dim dtCurrentTable As DataTable = Nothing
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            dtCurrentTable = GetRecList()


            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    'dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    'Master_Row_Count = 0
                Else
                    'Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If




            With rptrAnswer
                .DataSource = dtCurrentTable
                .DataBind()
            End With


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

    Public Function GetGroup(ByVal strCatCode As String, ByVal strCatName As String, ByVal intCount As Integer) As String

        If strCurrentCatCode <> strCatCode Then
            strCurrentCatCode = strCatCode

            If (intRowIndex Mod 2) = 0 Then
                strColor = "#e7e7ff"
            Else
                strColor = "#eff8fc"
            End If

            intRowIndex += 1
            Return "<tr class='Row' style='background-color:" & strColor & "'><td rowspan='" & intCount & "'  style='border-bottom-color:Black; border-top-color:Black; border-left-color:black; border-right-color:black; font-weight:bold'>" & strCatCode & " - " & strCatName & "</td>"
        Else
            Return "<tr class='Row' style='background-color:" & strColor & "'>"
        End If

        Return ""
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Sub RefreshDatabindingrpCust()

        Dim dt As DataTable
        dt = GetRecListrpCust()
        If dt.Rows.Count > 0 Then
            rpCust.DataSource = dt
            rpCust.DataBind()
            lblImgLoadFailMsg.Visible = False
        Else
            rpCust.DataSource = Nothing
            rpCust.DataBind()
            lblImgLoadFailMsg.Visible = True
        End If

        'lblCustCode.Text = hfPicCustCode.Value
        lblCustName.Text = hfPicCustName.Value
        'lblAddress.Text = hfPicCustAdd.Value
    End Sub

    Private Function GetRecListrpCust() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsCustomer As New clsCustomerProfile
            Dim strCustCode As String = hfPicCustCode.Value
            DT = clsCustomer.GetCustomerProfilePicture(strCustCode, Portal.UserSession.UserID)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Protected Sub btnRefreshPic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshPic.Click
        RefreshDatabindingrpCust()
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "Show", "SetVisit('Show'); ShowVisit(); $('#Visit').center();", True)
    End Sub


    Protected Sub rpCust_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCust.ItemDataBound
        Dim imgCust As Image = DirectCast(e.Item.FindControl("imgCust"), Image)
        Dim hfCustCode As HiddenField = DirectCast(e.Item.FindControl("hfCustCode"), HiddenField)
        Dim hfCustName As HiddenField = DirectCast(e.Item.FindControl("hfCustName"), HiddenField)
        Dim hfImgLoc As HiddenField = DirectCast(e.Item.FindControl("hfImgLoc"), HiddenField)


        Dim strImgLoc As String = hfImgLoc.Value
        Dim strCustName As String = hfCustName.Value

        If strImgLoc.ToString.Trim <> "" Then
            If File.Exists(Server.MapPath("~" + strImgLoc.ToString)) Then
                imgCust.ImageUrl = "~" & strImgLoc.ToString & "?" & DateTime.Now.ToString("yyyyMMddHHmmss")

            Else
                imgCust.ImageUrl = "~" + "\images\anonymous.jpg"

            End If

            ' EmbedConfirmation(UploadedFlag)
        Else
            imgCust.ImageUrl = "~" + "\images\anonymous.jpg"

        End If
    End Sub
End Class
