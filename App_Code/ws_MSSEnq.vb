﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports cor_DB
Imports System.Web.Script.Services
Imports System.Collections.Generic

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<ScriptService()> _
Public Class ws_MSSEnq
    Inherits System.Web.Services.WebService

#Region "Helper class"
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffmr_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class
    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class
    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
#End Region

#Region "Class Entity Defintion"
    Public Class CustomerMSS
        Inherits BaseEntity
        Private _Latitude As String = String.Empty
        Private _Longitude As String = String.Empty
        Private strCustCode As String
        Private strCustName As String
        Private strContCode As String
        Private strContName As String
        Private strAddress As String
        Private strDistrict As String
        Private strCustGrp As String
        Private strCustClass As String
        Private strCustType As String
        Private strTxnDate As String
        Private strTxnNo As String
        Private strTitleCode As String
        Private strTitleName As String
        Private strMTD As String
        Private strYTD As String
        Private strSKU As String
        Private strCreditLimit As String
        Private strOutBal As String
        Private strImgLoc As String

        Public Property CUST_CODE() As String
            Get
                Return strCustCode
            End Get
            Set(ByVal value As String)
                strCustCode = value
            End Set
        End Property
        Public Property CUST_NAME() As String
            Get
                Return strCustName
            End Get
            Set(ByVal value As String)
                strCustName = value
            End Set
        End Property
        Public Property CONT_CODE() As String
            Get
                Return strContCode
            End Get
            Set(ByVal value As String)
                strContCode = value
            End Set
        End Property
        Public Property CONT_NAME() As String
            Get
                Return strContName
            End Get
            Set(ByVal value As String)
                strContName = value
            End Set
        End Property
        Public Property ADDRESS() As String
            Get
                Return strAddress
            End Get
            Set(ByVal value As String)
                strAddress = value
            End Set
        End Property
        Public Property DISTRICT() As String
            Get
                Return strDistrict
            End Get
            Set(ByVal value As String)
                strDistrict = value
            End Set
        End Property
        Public Property CUST_GRP_NAME() As String
            Get
                Return strCustGrp
            End Get
            Set(ByVal value As String)
                strCustGrp = value
            End Set
        End Property
        Public Property CUST_CLASS() As String
            Get
                Return strCustClass
            End Get
            Set(ByVal value As String)
                strCustClass = value
            End Set
        End Property
        Public Property CUST_TYPE() As String
            Get
                Return strCustType
            End Get
            Set(ByVal value As String)
                strCustType = value
            End Set
        End Property
        Public Property TXN_DATE() As String
            Get
                Return strTxnDate
            End Get
            Set(ByVal value As String)
                strTxnDate = value
            End Set
        End Property
        Public Property TXN_NO() As String
            Get
                Return strTxnNo
            End Get
            Set(ByVal value As String)
                strTxnNo = value
            End Set
        End Property
        Public Property TITLE_CODE() As String
            Get
                Return strTitleCode
            End Get
            Set(ByVal value As String)
                strTitleCode = value
            End Set
        End Property
        Public Property TITLE_NAME() As String
            Get
                Return strTitleName
            End Get
            Set(ByVal value As String)
                strTitleName = value
            End Set
        End Property
        Public Property LATITUDE() As String
            Get
                Return _Latitude
            End Get
            Set(ByVal value As String)
                _Latitude = value
            End Set
        End Property
        Public Property LONGITUDE() As String
            Get
                Return _Longitude
            End Get
            Set(ByVal value As String)
                _Longitude = value
            End Set
        End Property
        Public Property MTD_SALES() As String
            Get
                Return strMTD
            End Get
            Set(ByVal value As String)
                strMTD = value
            End Set
        End Property
        Public Property YTD_SALES() As String
            Get
                Return strYTD
            End Get
            Set(ByVal value As String)
                strYTD = value
            End Set
        End Property
        Public Property NO_SKU() As String
            Get
                Return strSKU
            End Get
            Set(ByVal value As String)
                strSKU = value
            End Set
        End Property
        Public Property IMG_LOC() As String
            Get
                Return strImgLoc
            End Get
            Set(ByVal value As String)
                strImgLoc = value
            End Set
        End Property
        Public Property CREDITLIMIT() As String
            Get
                Return strCreditLimit
            End Get
            Set(ByVal value As String)
                strCreditLimit = value
            End Set
        End Property
        Public Property OUTBAL() As String
            Get
                Return strOutBal
            End Get
            Set(ByVal value As String)
                strOutBal = value
            End Set
        End Property
    End Class
#End Region

    '   <WebMethod(EnableSession:=True)> _
    'Public Function ReturnCustomerMSS(ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
    '                                   ByVal strCustGrp As String, ByVal strCustClass As String, _
    '                                   ByVal strCustType As String, ByVal strTitleCode As String, _
    '                                   ByVal strStartDate As String, ByVal strEndDate As String) As PagedResult(Of CustomerMSS)
    '       Dim obj As clsDB
    '       Dim DR As IDataReader

    '       Try
    '           Dim rptRows As New List(Of CustomerMSS)()
    '           Dim rptResult

    '           Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
    '               Using sqlCmd As IDbCommand = sqlConn.CreateCommand
    '                   sqlCmd.CommandText = "SPP_RPT_MSS_CUST_TITLE_SEARCH"
    '                   sqlCmd.CommandType = CommandType.StoredProcedure
    '                   clsCONN.addInputParam(sqlCmd, "CUST_NAME", strCustName, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "ADDRESS", strAddress, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "DISTRICT", strDistrict, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "CUST_GRP_NAME", strCustGrp, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "CUST_CLASS", strCustClass, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "CUST_TYPE", strCustType, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "TITLE_CODE", strTitleCode, clsCONN.dtType.dbString)
    '                   clsCONN.addInputParam(sqlCmd, "MSS_START_DATE", strStartDate, clsCONN.dtType.dbDateTime)
    '                   clsCONN.addInputParam(sqlCmd, "MSS_END_DATE", strEndDate, clsCONN.dtType.dbDateTime)
    '                   clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)

    '                   Using sqlReader As IDataReader = sqlCmd.ExecuteReader
    '                       While sqlReader.Read
    '                           rptRows.Add(Build_CustomerMSS(sqlReader))
    '                       End While
    '                   End Using
    '               End Using
    '           End Using

    '           rptResult = New PagedResult(Of CustomerMSS)
    '           With rptResult
    '               .Rows = rptRows
    '               .Total = rptRows.Count
    '           End With

    '           Return rptResult
    '       Catch ex As Exception
    '           Throw (New ExceptionMsg("ws_MSSEnq.ReturnCustomerMSS :" & ex.Message))
    '       Finally
    '           obj = Nothing
    '       End Try
    '   End Function
    '   Public Shared Function Build_CustomerMSS(ByRef reader As IDataReader) As CustomerMSS
    '       Dim rptRow As New CustomerMSS
    '       With rptRow
    '           For i As Integer = 0 To reader.FieldCount - 1
    '               Select Case reader.GetName(i)
    '                   Case "CUST_CODE"
    '                       SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
    '                   Case "CUST_NAME"
    '                       SetValue(Of String)(.CUST_NAME, reader("CUST_NAME"), String.Empty)
    '                   Case "LATITUDE"
    '                       SetValue(Of String)(.LATITUDE, reader("LATITUDE"), 0)
    '                   Case "LONGITUDE"
    '                       SetValue(Of String)(.LONGITUDE, reader("LONGITUDE"), 0)
    '                   Case "ADDRESS"
    '                       SetValue(Of String)(.ADDRESS, reader("ADDRESS"), String.Empty)
    '                   Case "DISTRICT"
    '                       SetValue(Of String)(.DISTRICT, reader("DISTRICT"), String.Empty)
    '                   Case "CUST_GRP_NAME"
    '                       SetValue(Of String)(.CUST_GRP_NAME, reader("CUST_GRP_NAME"), String.Empty)
    '                   Case "CUST_CLASS"
    '                       SetValue(Of String)(.CUST_CLASS, reader("CLASS"), String.Empty)
    '                   Case "CUST_TYPE"
    '                       SetValue(Of String)(.CUST_TYPE, reader("CUST_TYPE"), String.Empty)
    '                   Case "TXN_NO"
    '                       SetValue(Of String)(.TXN_NO, reader("TXN_NO"), String.Empty)
    '                   Case "TXN_DATE"
    '                       SetValue(Of String)(.TXN_DATE, reader("TXN_DATE"), String.Empty)
    '                   Case "TITLE_CODE"
    '                       SetValue(Of String)(.TITLE_CODE, reader("TITLE_CODE"), String.Empty)
    '                   Case "TITLE_NAME"
    '                       SetValue(Of String)(.TITLE_NAME, reader("TITLE_NAME"), String.Empty)
    '               End Select
    '           Next
    '       End With
    '       Return rptRow
    '   End Function

    <WebMethod(EnableSession:=True)> _
 Public Function ReturnCustomerMSS(ByVal strGUID As String, ByVal strCustName As String, ByVal strAddress As String, ByVal strDistrict As String, _
                                       ByVal strCustGrp As String, ByVal strCustClass As String, _
                                       ByVal strCustType As String, ByVal strTitleCode As String, _
                                       ByVal strQuesCode As String, ByVal strSubQuesCode As String, _
                                       ByVal strMssStartDate As String, ByVal strMssEndDate As String, _
                                       ByVal strMssAnswer As String, ByVal strSubQuesType As String, _
                                       ByVal intNetValue As Integer, ByVal strDate As String, _
                                       ByVal strMtdStart As String, ByVal strMtdEnd As String, ByVal strYtdStart As String, ByVal strYtdEnd As String, _
                                       ByVal strNoSkuStart As String, ByVal strNoSkuEnd As String, _
                                       ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strPrdCode As String) As PagedResult(Of CustomerMSS)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of CustomerMSS)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffmr_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_RPT_MSS_CUST_TITLE_SEARCH_STATS"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "GUID", strGUID, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_NAME", strCustName, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "ADDRESS", strAddress, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DISTRICT", strDistrict, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_GRP_NAME", strCustGrp, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_CLASS", strCustClass, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "CUST_TYPE", strCustType, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TITLE_CODE", strTitleCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "QUES_CODE", strQuesCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SUB_QUES_CODE", strSubQuesCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MSS_START_DATE", strMssStartDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MSS_END_DATE", strMssEndDate, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "ANSWER", strMssAnswer, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SUB_QUES_TYPE", strSubQuesType, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MTDSTART", strMtdStart, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "MTDEND", strMtdEnd, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "YTDSTART", strYtdStart, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "YTDEND", strYtdEnd, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NOSKUSTART", strNoSkuStart, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NOSKUEND", strNoSkuEnd, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "TEAM_CODE", strTeamCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "SALESREP_CODE", strSalesrepCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRD_CODE", strPrdCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "PRINCIPAL_ID", Session("PRINCIPAL_ID"), clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "NET_VALUE", intNetValue, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DATE", strDate, clsCONN.dtType.dbString)
                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_CustomerMSS(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            rptResult = New PagedResult(Of CustomerMSS)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_MSSEnq.ReturnCustomerMSS :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    Public Shared Function Build_CustomerMSS(ByRef reader As IDataReader) As CustomerMSS
        Dim rptRow As New CustomerMSS
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i).ToUpper
                    Case "CUST_CODE"
                        SetValue(Of String)(.CUST_CODE, reader("CUST_CODE"), String.Empty)
                    Case "CUST_NAME"
                        SetValue(Of String)(.CUST_NAME, reader("CUST_NAME"), String.Empty)
                    Case "CONT_CODE"
                        SetValue(Of String)(.CONT_CODE, reader("CONT_CODE"), String.Empty)
                    Case "CONT_NAME"
                        SetValue(Of String)(.CONT_NAME, reader("CONT_NAME"), String.Empty)
                    Case "LATITUDE"
                        SetValue(Of String)(.LATITUDE, reader("LATITUDE"), 0)
                    Case "LONGITUDE"
                        SetValue(Of String)(.LONGITUDE, reader("LONGITUDE"), 0)
                    Case "ADDRESS"
                        SetValue(Of String)(.ADDRESS, reader("ADDRESS"), String.Empty)
                    Case "DISTRICT"
                        SetValue(Of String)(.DISTRICT, reader("DISTRICT"), String.Empty)
                    Case "CUST_GRP_NAME"
                        SetValue(Of String)(.CUST_GRP_NAME, reader("CUST_GRP_NAME"), String.Empty)
                    Case "CLASS"
                        SetValue(Of String)(.CUST_CLASS, reader("CLASS"), String.Empty)
                    Case "CUST_TYPE"
                        SetValue(Of String)(.CUST_TYPE, reader("CUST_TYPE"), String.Empty)
                    Case "TXN_NO"
                        SetValue(Of String)(.TXN_NO, reader("TXN_NO"), String.Empty)
                    Case "TXN_DATE"
                        SetValue(Of String)(.TXN_DATE, reader("TXN_DATE"), String.Empty)
                    Case "TITLE_CODE"
                        SetValue(Of String)(.TITLE_CODE, reader("TITLE_CODE"), String.Empty)
                    Case "TITLE_NAME"
                        SetValue(Of String)(.TITLE_NAME, reader("TITLE_NAME"), String.Empty)
                    Case "MTD_SALES"
                        SetValue(Of String)(.MTD_SALES, Convert.ToDecimal(reader("MTD_SALES")).ToString("N2"), "0.00")
                    Case "YTD_SALES"
                        SetValue(Of String)(.YTD_SALES, Convert.ToDecimal(reader("YTD_SALES")).ToString("N2"), "0.00")
                    Case "CREDITLIMIT"
                        SetValue(Of String)(.CREDITLIMIT, Convert.ToDecimal(reader("CREDITLIMIT")).ToString("N2"), "0.00")
                    Case "OUTBAL"
                        SetValue(Of String)(.OUTBAL, Convert.ToDecimal(reader("OUTBAL")).ToString("N2"), "0.00")
                    Case "NO_SKU"
                        SetValue(Of String)(.NO_SKU, reader("NO_SKU"), String.Empty)
                    Case "IMG_LOC"
                        SetValue(Of String)(.IMG_LOC, reader("IMG_LOC"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class
