﻿var infowindow = null;
function CreateMarker(Latitude, Longitude) {
      var  marker = new google.maps.Marker({
          position: new google.maps.LatLng(Latitude, Longitude),
           map: map
       });
     
    return marker;
}

function CreateMarkerWithIcon(Latitude, Longitude, newIcon) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(Latitude, Longitude),
        icon: newIcon,
        map: map
    });
  
    return marker;
}


function AddInfoWindow(Marker, Description) {

    google.maps.event.addListener(Marker, "click", function () {
        if (infowindow) {
            infowindow.close();
        }
        infowindow = new google.maps.InfoWindow({
            content: Description
        });
        if (Description != undefined && Description != "") {
            var moveEnd = google.maps.event.addListener(map, "moveend", function () {
               // Marker.openInfoWindowHtml(Description);
                infowindow.setContent(Description);
              
                google.maps.event.removeListener(moveEnd);
            });
        }
        infowindow.open(map,Marker);
        map.panTo(Marker.getPosition());

        //map.panTo(Marker.getLatLng());
    }
    );
}