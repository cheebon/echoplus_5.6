﻿
Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsMSSExtraction
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsMSSExtraction"
        End Get
    End Property

    Public Function GetMSSExtractList(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, _
                                        ByVal strSalesrepCode As String, ByVal strTitleCode As String, ByVal strQuesCode As String, _
                                        ByVal strSubQuesCode As String, ByVal strCustCode As String, ByVal strTxnStartDate As String, ByVal strTxnEndDate As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_EXTRACTION"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString, True)
                .addItem("QUES_CODE", strQuesCode, clsDB.DataType.DBString, True)
                .addItem("SUB_QUES_CODE", strSubQuesCode, clsDB.DataType.DBString, True)
                .addItem("CUST_CODE", strCustCode, clsDB.DataType.DBString, False)
                .addItem("TXN_START_DATE", strTxnStartDate, clsDB.DataType.DBString, False)
                .addItem("TXN_END_DATE", strTxnEndDate, clsDB.DataType.DBString, False)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetMSSExtractList :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function


    Public Function GetSalesTeam(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_GET_TEAM"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesTeam :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSalesrep(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamList As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_GET_SALESREP"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamList, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSalesrep :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetTitle(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_GET_TITLE"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetTitle :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetQues(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strTitleCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_GET_QUES"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetQues :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function

    Public Function GetSubQues(ByVal strUserID As String, ByVal strPrincipalID As String, ByVal strTeamCode As String, ByVal strSalesrepCode As String, ByVal strTitleCode As String, ByVal strSubQuesCode As String) As DataTable
        Dim clsSFMSExtractDB As clsDB
        Try
            clsSFMSExtractDB = New clsDB
            With clsSFMSExtractDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_MSS_GET_SUB_QUES"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                .addItem("TEAM_CODE", strTeamCode, clsDB.DataType.DBString, True)
                .addItem("SALESREP_CODE", strSalesrepCode, clsDB.DataType.DBString, True)
                .addItem("TITLE_CODE", strTitleCode, clsDB.DataType.DBString, True)
                .addItem("QUES_CODE", strSubQuesCode, clsDB.DataType.DBString, True)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSubQues :" & ex.Message))
        Finally
            clsSFMSExtractDB = Nothing
        End Try
    End Function


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
