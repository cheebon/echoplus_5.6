﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustSalesSummList.aspx.vb" Inherits="iFFMR_Customize_CustSalesAnalysis_CustSalesSummList" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Sales Summarized Report</title>
    <link rel="stylesheet" href='~/include/DKSH.css' />
     <script type="text/javascript" language="javascript">
         function resizeLayout2() { var dgList = $get('div_dgList'); if (dgList) { dgList.scrollTop = 0; dgList.style.height = Math.max((getFrameHeight('DetailBarIframe') - 70), 0) + 'px'; } }
            window.onresize=function(){resizeLayout2();}
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar');MaximiseFrameHeight('DetailBarIframe');resetSize('div_dgList','DetailBarIframe');">
    <form id="form1" runat="server">
    <AjaxToolkit:ToolkitScriptManager  ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%; ">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <customToolkit:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server"></customToolkit:wuc_ctrlpanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 2px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                        <tr>
                                            <td colspan="3" style="width: 100%">
                                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server"></customToolkit:wuc_lblHeader>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="BckgroundBenealthTitle" colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td valign="top" class="Bckgroundreport">
                                                                                <customToolkit:wuc_UpdateProgress ID="Wuc_UpdateProgress1" runat="server" />
                                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                    <ContentTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <%--<td>&nbsp;</td>--%>
                                                                <td align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                        <tr>
                                                                            <td style="width: 2%; float: left;">
                                                                                <%--<asp:Button ID="btnBack" runat="server" CssClass="cls_button" Text="Back" ToolTip="Back To Previous Page."
                                                                                  OnClientClick="javascript:history.back();"  PostBackUrl="" />--%>
                                                                                <input id="btnBack" type="button" class="cls_button" 
                                                                                        value="Back" onclick="HideElement('DetailBar');HideElement('TopBar');ShowElement('ContentBar');MaximiseFrameHeight('ContentBarIframe');resetSize('div_dgList','ContentBarIframe');" />
                                                                            </td>
                                                                            <td align="left">
                                                                            </td>
                                                                            <td align="right">
                                                                                <asp:Timer ID="TimerControl1" runat="server" Interval="50" OnTick="TimerControl1_Tick">
                                                                                </asp:Timer>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%--<td>&nbsp;</td>--%>
                                                                <td align="left">
                                                                    <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="True" AllowSorting="True"
                                                                        AutoGenerateColumns="FALSE" Width="98%" FreezeHeader="True" GridHeight="455"
                                                                        AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                                                        FreezeRows="0" GridWidth="98%" RowHighlightColor="AntiqueWhite">
                                                                        <EmptyDataRowStyle HorizontalAlign="Center" Font-Bold="True" />
                                                                        <EmptyDataTemplate>
                                                                            There is no data to display.</EmptyDataTemplate>
                                                                    </ccGV:clsGridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr class="Bckgroundreport" style="height: 10px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>       
    </form>
</body>
</html>
