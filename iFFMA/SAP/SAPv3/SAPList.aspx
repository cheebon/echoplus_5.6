﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SAPList.aspx.vb" Inherits="iFFMA_SAP_New_SAPList" %>
<%@ Register Src="~/include/wuc_lblHeader.ascx" TagName="wuc_lblHeader" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress" TagPrefix="customToolkit" %>
<%@ Register Src="~/include/wuc_txtDate.ascx" TagName="wuc_txtDate" TagPrefix="customToolkit" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl"
 Assembly="cor_CustomCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/include/wuc_txtCalendarRange.ascx" TagName="wuc_txtCalendarRange" TagPrefix="customToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SAP List</title>
    <link rel="stylesheet" href='~/include/DKSH.css' /> 
    <script type="text/javascript" language="javascript">
//    function resizeLayout3(){
//        var dgList = $get('div_dgList');
//        if (dgList){
//            dgList.scrollTop=0;
//            dgList.style.height=Math.max((getFrameHeight('ContentBarIframe') - 70),0)+'px';
//            }
//            }
    window.onresize=function(){resizeLayout3('div_dgList','ContentBarIframe',135);}
    function getSelectedCriteria(){var frm =self.parent.document.frames[0];if(frm){var hdf=document.getElementById('hdfSalesrepName');var spn_ori = frm.document.getElementById('hdnSalesrepName');if(hdf && spn_ori){hdf.value=spn_ori.innerHTML;}hdf=document.getElementById('hdfSalesTeamName');spn_ori=frm.document.getElementById('hdnSalesTeamName');if(hdf && spn_ori){hdf.value=spn_ori.innerHTML;}}}
    setTimeout("getSelectedCriteria()", 1000);
    function ShowElement(element){var TopDiv=self.parent.document.getElementById(element);TopDiv.style.display='';}

    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ShowElement('ContentBar');resetSize('div_dgList','ContentBarIframe');MaximiseFrameHeight('ContentBarIframe');HideElement('DetailBar');">
    <form id="frmSAPList" runat="server">
    <AjaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="300" ScriptMode="Release" />
    
        <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
            <ContentTemplate>
                
                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                    <tr>
                        <td><asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                        </td>
                    </tr>
                    <tr><td class="BckgroundBenealthTitle" height="5"></td></tr>
                    <tr class="Bckgroundreport">
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td  style="padding:0px 10px 0px 10px">                                
                                    <%--<table>
                                    <tr>
                                        <td align="left" colspan="2">--%>
                                        <div style="border-style:solid; border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-width:1px; border-color:#007799;width:90%;padding:1px 5px 1px 5px">
                                        <asp:Image ID="imgSearch" ImageUrl="~/images/ico_Search_Details.gif" runat="server"
                                            CssClass="cls_button" ToolTip="Search Details" EnableViewState="false" />
                                        </div>
                                    <%--</td>
                                    </tr>                
                                    </table>--%>
                                    <asp:Panel ID="pnlSearch" runat="server">
                                        <div style="border-style:solid; border-bottom-width:1px;border-left-width:1px;border-right-width:1px;border-top-width:0px; border-color:#007799;width:90%;padding:2px 5px 2px 5px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="left" style="width:100px">
                                                    <span class="cls_label_header">Document Type:</span>
                                                </td>
                                                <td style="width:150px">
                                                    <asp:DropDownList ID="ddlDocType" runat="server" CssClass="cls_dropdownlist">
                                                        <asp:ListItem Text="Sales Order" Value="SO"></asp:ListItem>
                                                        <asp:ListItem Text="Trade Return" Value="TRA"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>       
                                            <tr>
                                                <td align="left">
                                                    <span class="cls_label_header">Team:</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlTeam" runat="server" CssClass="cls_dropdownlist">                                
                                                    </asp:DropDownList>
                                                </td>                        
                                            </tr>    
                                            <tr>
                                                <td align="left">
                                                    <span class="cls_label_header">Error Reason:</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtErrReason" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td> 
                                                <td align="left" style="width:100px">
                                                    <span class="cls_label_header">Sales Office:</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSalesOffice" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                 <td align="left">
                                                    <span class="cls_label_header">Txn No:</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTxnNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td> 
                                                <td align="left">
                                                    <span class="cls_label_header">Salesrep Code:</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSalesrepCode" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td> 
                                            </tr>     
                                            <tr>
                                                <td align="left">
                                                    <span class="cls_label_header">Last Update:</span>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtLastUpdate" runat="server" CssClass="cls_textbox"></asp:TextBox>--%>
                                                    <customToolkit:wuc_txtCalendarRange ID="Wuc_txtDateLastUpdate" RequiredValidation="false" runat="server" ControlType="DateOnly" />
                                                </td> 
                                               
                                                <td align="left">
                                                    <span class="cls_label_header">Status:</span>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtStatus" runat="server" CssClass="cls_textbox"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="cls_dropdownlist" Width="150">
                                                    </asp:DropDownList>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="cls_label_header">Txn Date:</span>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtTxnDate" runat="server" CssClass="cls_textbox"></asp:TextBox>--%>
                                                    <customToolkit:wuc_txtCalendarRange ID="Wuc_txtDateTxnDate" RequiredValidation="false" runat="server" ControlType="DateOnly" />
                                                </td> 
                                                <td align="left">
                                                    <span class="cls_label_header">Saving No:</span>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSavingNo" runat="server" CssClass="cls_textbox"></asp:TextBox>
                                                </td> 
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <span class="cls_label_header">Txn Status:</span>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtTxnStatus" runat="server" CssClass="cls_textbox"></asp:TextBox>--%>
                                                    <asp:DropDownList ID="ddlTxnStatus" runat="server" CssClass="cls_dropdownlist" Width="150">
                                                    </asp:DropDownList>
                                                </td> 
                                                <td align="left">
                                                    <%--<span class="cls_label_header">Note:</span>--%>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="txtNote" runat="server" CssClass="cls_textbox"></asp:TextBox>--%>
                                                </td> 
                                            </tr>                                                   
                                            <tr>
                                                <td align="left" colspan = "2">
                                                    <asp:Button ID="cmdSearch" runat="server" Text="Search" cssclass="cls_button"/>
                                                </td> 
                                            </tr>
                                        </table>
                                        </div>
                                     </asp:Panel>
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CPE_PnlSearch" runat="server" CollapseControlID="imgSearch"
                                                            ExpandControlID="imgSearch" TargetControlID="pnlSearch" CollapsedSize="0" Collapsed="false"
                                                            ExpandDirection="Vertical" SuppressPostBack="true">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0px 10px 0px 10px">
                                     <br />
                                     <uc1:wuc_dgpaging ID="wuc_dgpaging" runat="server" Visible="false"></uc1:wuc_dgpaging>
                                    <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True"
                                    AutoGenerateColumns="False" Width="98%" FreezeHeader="True" GridHeight="300px"
                                    AddEmptyHeaders="0" CellPadding="2" CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0"
                                    FreezeRows="0" GridWidth="" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="txn_no" BorderColor="Black" BorderWidth="1" GridBorderColor="Black" GridBorderWidth="1px" RowHighlightColor="AntiqueWhite">
                                    <%--<Columns>    
                                       <asp:ButtonField CommandName="Resubmit" AccessibleHeaderText="Resubmit" ImageUrl="~/images/icoCopy.gif" ButtonType="Image" HeaderText="Copy"/>   
                                        <asp:ButtonField CommandName="Block" AccessibleHeaderText="Block" ImageUrl="~/images/ico_Unblock.gif" ButtonType="Image" HeaderText="Copy" />       
                                        <asp:ButtonField CommandName="Details" DatatextField="txn_no" HeaderText="Txn No" SortExpression="txn_no">
                                           <itemstyle horizontalalign="Center" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="txn_status" HeaderText="Txn Status" ReadOnly="True" SortExpression="txn_status">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="STATUS" HeaderText="Status" ReadOnly="True" SortExpression="STATUS">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="ln_status" HeaderText="LN Status" ReadOnly="True" SortExpression="ln_status">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField> 
                                        <asp:BoundField DataField="ln_reason" HeaderText="LN Remarks" ReadOnly="True" SortExpression="ln_reason">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField> 
                                        <asp:ButtonField CommandName="Details" DatatextField="DOC_NO" HeaderText="Saving No" SortExpression="DOC_NO">
                                           <itemstyle horizontalalign="Center" />
                                        </asp:ButtonField>              
                                        <asp:BoundField DataField="txn_timestamp" HeaderText="Txn Date" ReadOnly="True" SortExpression="txn_timestamp">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>                                                                                                                         
                                        <asp:BoundField DataField="LASTUPDATE" HeaderText="Last Update" ReadOnly="True" SortExpression="LASTUPDATE">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>                                                                                                                
                                        <asp:BoundField DataField="AUART" HeaderText="Order Type" ReadOnly="True" SortExpression="AUART">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VKORG" HeaderText="Sales Org" ReadOnly="True" SortExpression="VKORG">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VTWEG" HeaderText="Distribution Channel" ReadOnly="True" SortExpression="VTWEG">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="SPART" HeaderText="Division" ReadOnly="True" SortExpression="SPART">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="VKBUR" HeaderText="Sales Office" ReadOnly="True" SortExpression="VKBUR">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="VKGRP" HeaderText="Sales Group" ReadOnly="True" SortExpression="VKGRP">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="KUNNR" HeaderText="Soldto Party" ReadOnly="True" SortExpression="KUNNR">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="KUNDE" HeaderText="Shipto Party" ReadOnly="True" SortExpression="KUNDE">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="PAYER" HeaderText="Payer Code" ReadOnly="True" SortExpression="PAYER">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="salesrep_code" HeaderText="Salesrep Code" ReadOnly="True" SortExpression="salesrep_code">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="BSTNK" HeaderText="PO No" ReadOnly="True" SortExpression="BSTNK">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="BSTDK" HeaderText="PO Date" ReadOnly="True" SortExpression="BSTDK">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>   
                                         <asp:BoundField DataField="KETDAT" HeaderText="Request Delivery Date" ReadOnly="True" SortExpression="KETDAT">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField> 
                                         <asp:BoundField DataField="RESENDCOUNTER" HeaderText="Resubmit Counter" ReadOnly="True" SortExpression="RESENDCOUNTER">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField> 
                                        <asp:BoundField DataField="REMARKS" HeaderText="Err Reason" ReadOnly="True" SortExpression="REMARKS">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>                                                        
                                        <asp:BoundField DataField="NOTE" HeaderText="Note" ReadOnly="True" SortExpression="NOTE">
                                            <itemstyle horizontalalign="Center" />
                                        </asp:BoundField>       
                                    </Columns>    --%>                                            
                                    <FooterStyle CssClass="GridFooter" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAlternate" />
                                    <RowStyle CssClass="GridNormal" />
                                    <PagerSettings Visible="False" />
                                </ccGV:clsGridView>
                                </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                
                
                
                
            
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </form>
</body>
</html>
