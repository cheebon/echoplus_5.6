<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Perf.aspx.vb" Inherits="iFFMS_Performance_Perf" %>

<%@ Register Src="~/include/wuc_UpdateProgress.ascx" TagName="wuc_UpdateProgress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Performance</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout" onload="ResizeFrameWidth('TopBar','100%');ShowElement('TopBar');MaximiseFrameHeight('TopBarIframe');">
    <form id="frmperformance" runat="server" method="post">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <fieldset style="width: 98%">
            <uc1:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
            <div style="width: 100%; position: relative; padding: 0; margin: 0; float: left;">
                <uc1:wuc_lblheader ID="wuc_lblheader" runat="server"></uc1:wuc_lblheader>
                <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
            </div>
            <div style="width: 100%;">
                <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                    width="100%" border="0" style="height: 30px">
                    <tr align="left" valign="bottom">
                        <td>
                            <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server"
                                CssClass="cls_button" ToolTip="Export" EnableViewState="false" />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:UpdatePanel runat="server" ID="UpdatePage" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                        float: left; margin: 0;">
                        <span class="cls_label_header">Tgt. Duration :</span>
                        <asp:DropDownList ID="ddltgtduration" runat="server" AutoPostBack="True" Width="216px"
                            CssClass="cls_dropdownlist">
                        </asp:DropDownList>
                    </div>
                    <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                    <asp:Panel ID="pnldglist" runat="server">
                        <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                            float: left; margin: 0;">
                            <div style="width: 800px;">
                                <span style="float: left; width: 400px">
                                    <asp:DetailsView ID="dtviewleft" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                        Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                        <RowStyle CssClass="cls_DV_Row_MST" />
                                        <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                        <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                            Width="35%" Wrap="False" />
                                    </asp:DetailsView>
                                </span><span style="float: left; width: 400px">
                                    <asp:DetailsView ID="dtviewright" runat="server" CssClass="Grid" AutoGenerateRows="False"
                                        Width="100%" BorderColor="black" BorderStyle="solid" BorderWidth="1px">
                                        <RowStyle CssClass="cls_DV_Row_MST" />
                                        <AlternatingRowStyle CssClass="cls_DV_Alt_Row_MST" />
                                        <FieldHeaderStyle CssClass="cls_DV_Header_MST" VerticalAlign="Top" HorizontalAlign="Left"
                                            Width="35%" Wrap="False" />
                                    </asp:DetailsView>
                                </span>
                            </div>
                        </div>
                        <div style="width: 98%; position: relative; padding-left: 10px; padding-top: 10px;
                            float: left; margin: 0;">
                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                Width="98%" FreezeHeader="True" GridHeight="320" AddEmptyHeaders="0" CellPadding="2"
                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                ShowFooter="false" AllowPaging="false" PagerSettings-Visible="false">
                            </ccGV:clsGridView>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
        <table cellspacing="0" cellpadding="0" width="100%" border="0" class="cls_panel_header">
            <asp:Panel ID="pnlExport" runat="server" Visible="false">
                <tr>
                    <td>
                        <table class="cls_panel" id="Table1" cellspacing="0" cellpadding="0" width="100%"
                            border="0">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlExport" CssClass="cls_dropdownlist" runat="server">
                                                    <asp:ListItem Value="0" Selected="True">Excel</asp:ListItem>
                                                    <asp:ListItem Value="1">Text</asp:ListItem>
                                                </asp:DropDownList></td>
                                            <td>
                                                <asp:Button ID="btnExport" CssClass="cls_button" runat="server" Text="Export"></asp:Button></td>
                                            <td align="right" width="90%">
                                                <asp:ImageButton ID="imgClose_Export" runat="server" ImageUrl="~/images/ico_close.gif">
                                                </asp:ImageButton></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </asp:Panel>
        </table>
    </form>
</body>
</html>
