﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CallDtlByGroup.aspx.vb"
    Inherits="iFFMR_Customize_CallDtlByGroup" %>

<%@ Register TagPrefix="uctoolbar" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="uclblHeader" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="uclblInfo" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="ucUpdateProgress" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="ucdgpaging" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register TagPrefix="ucctrlpanel" TagName="wuc_ctrlpanel" Src="~/include/wuc_ctrlpanel.ascx" %>
<%@ Register TagPrefix="ucpnlRecordNotFound" TagName="wuc_pnlRecordNotFound" Src="~/include/wuc_pnlRecordNotFound.ascx" %>
<%@ Register TagPrefix="ucMenu" TagName="wuc_Menu" Src="~/include/menu/wuc_Menu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Call Dtl By Group</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmCallDtlByGroup" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" align="left">
                    <asp:UpdatePanel ID="UpdateMenuPanel" runat="server" UpdateMode="Conditional"  RenderMode="Inline" >
                        <ContentTemplate>
                            <asp:Panel ID="pnlTreeMenuControl" runat="server" >
                                <ucMenu:wuc_Menu ID="wuc_Menu" runat="server" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                        <tr>
                            <td align="left">
                                <ucctrlpanel:wuc_ctrlpanel ID="wuc_ctrlpanel" runat="server" EnableViewState="true" /> 
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 2px">
                                <uclblHeader:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                    <ContentTemplate>
                                        <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="BckgroundInsideContentLayout" align="left">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" class="Bckgroundreport">
                                    <tr>
                                        <td class="BckgroundBenealthTitle">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="Bckgroundreport">
                                            <ucUpdateProgress:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                            <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                <ContentTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr align="right">
                                                            <td>
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                            </td>
                                                        </tr>
                                                        <tr align="left">
                                                            <td style="width: 98%">
                                                                <%-- <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server"></customToolkit:wuc_dgpaging>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 95%;">
                                                                <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="FAlse" AutoGenerateColumns="False"
                                                                    Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                    CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                    ShowFooter="False" AllowPaging="false" PagerSettings-Visible="false">
                                                                    <EmptyDataTemplate>
                                                                        <ucpnlRecordNotFound:wuc_pnlRecordNotFound ID="Wuc_pnlRecordNotFound" runat="server" ShowPanel="true" />
                                                                    </EmptyDataTemplate>
                                                                </ccGV:clsGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr class="Bckgroundreport">
                                        <td style="height: 5px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
