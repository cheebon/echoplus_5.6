﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports cor_DB
Imports System.Web.Script.Services
Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class ws_Config
    Inherits System.Web.Services.WebService

#Region "Helper class"
    Private Shared Function CreateConnection(Optional ByVal connStr As String = "") As IDbConnection
        'HttpContext.Current.Session("ffmr_conn") = ""
        Dim connection As IDbConnection = New SqlClient.SqlConnection(IIf(String.IsNullOrEmpty(connStr), "ffmr_conn", connStr))
        connection.Open()
        Return connection
    End Function
    Private Shared Sub CloseConnection(ByRef current_conn As IDbConnection)
        If current_conn.State <> ConnectionState.Closed Then current_conn.Close()
        Try
            current_conn.Dispose()
        Catch ex As Exception
        End Try
    End Sub
    Private Shared Sub SetValue(Of T)(ByRef dbObj As Object, ByRef dbOri As Object, Optional ByVal dfValue As Object = Nothing)

        If IsDBNull(dbOri) = False Then
            dbObj = Trim(dbOri)
        ElseIf GetType(T) Is GetType(String) Then
            dbObj = String.Empty
        ElseIf GetType(T) Is GetType(Integer) Then
            dbObj = 0
        Else
            dbObj = Nothing
        End If
    End Sub
    Public Class PagedResult(Of T As BaseEntity)
        ' Fields
        Private _total As Integer
        Private _rows As List(Of T)
        Private _message As String = String.Empty
        Public Property Total() As Integer
            Get
                Return _total
            End Get
            Set(ByVal value As Integer)
                _total = value
            End Set
        End Property
        Public Property Rows() As List(Of T)
            Get
                Return _rows
            End Get
            Set(ByVal value As List(Of T))
                _rows = value
            End Set
        End Property
        Public Property Message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
    End Class
    Public MustInherit Class BaseEntity
        ' Methods
        Protected Sub New()
        End Sub
    End Class
    Public Class clsCONN
        Public Shared Sub addInputParam(ByRef sqlCmd As IDbCommand, ByVal mFieldName As String, ByVal mFieldValue As String, ByVal mFieldType As dtType, Optional ByVal blnParseExact As Boolean = False)
            If IsDBNull(mFieldValue) Then mFieldValue = String.Empty
            If blnParseExact = False Then mFieldValue = mFieldValue.Replace("*", "%").Replace("'", "''").Replace("~", "&").Replace("\", "\\")

            mFieldName = "@" & mFieldName
            Dim sqlPrm = New SqlClient.SqlParameter(mFieldName, mFieldType)
            sqlCmd.Parameters.Add(sqlPrm)

            If dtType.dbDateTime And String.IsNullOrEmpty(mFieldValue) Then
                sqlCmd.Parameters(mFieldName).value = System.Data.SqlTypes.SqlDateTime.Null
            Else
                sqlCmd.Parameters(mFieldName).value = mFieldValue
            End If
        End Sub

        Public Enum dtType As Integer
            dbINT = 8
            dbString = 22
            dbDateTime = 4
            dbLong = 0
            dbFloat = 6
        End Enum
    End Class
#End Region

#Region "Class Entity Defintion"
    Public Class SettingDtlTeamSR
        Inherits BaseEntity
        Private strCode As String
        Public Property CODE() As String
            Get
                Return strCode
            End Get
            Set(ByVal value As String)
                strCode = value
            End Set
        End Property

        Private strName As String
        Public Property NAME() As String
            Get
                Return strName
            End Get
            Set(ByVal value As String)
                strName = value
            End Set
        End Property

        Private strType As String
        Public Property TYPE() As String
            Get
                Return strType
            End Get
            Set(ByVal value As String)
                strType = value
            End Set
        End Property

    End Class
#End Region

    <WebMethod(EnableSession:=True)> _
 Public Function ReturnSettingTeamSR(ByVal strSettingCode As String, ByVal strDtlType As String) As PagedResult(Of SettingDtlTeamSR)
        Dim obj As clsDB
        Dim DR As IDataReader

        Try
            Dim rptRows As New List(Of SettingDtlTeamSR)()
            Dim rptResult

            Using sqlConn As IDbConnection = CreateConnection(Session("ffms_conn"))
                Using sqlCmd As IDbCommand = sqlConn.CreateCommand
                    sqlCmd.CommandText = "SPP_ADM_CONFIG_DEVICE_SETTING_DTL_LIST"
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    clsCONN.addInputParam(sqlCmd, "SETTING_CODE", strSettingCode, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "DTL_TYPE", strDtlType, clsCONN.dtType.dbString)
                    clsCONN.addInputParam(sqlCmd, "USER_ID", Session("UserID"), clsCONN.dtType.dbString)
                    Using sqlReader As IDataReader = sqlCmd.ExecuteReader
                        While sqlReader.Read
                            rptRows.Add(Build_SettingDtlTeamSR(sqlReader))
                        End While
                    End Using
                End Using
            End Using

            rptResult = New PagedResult(Of SettingDtlTeamSR)
            With rptResult
                .Rows = rptRows
                .Total = rptRows.Count
            End With

            Return rptResult
        Catch ex As Exception
            Throw (New ExceptionMsg("ws_Config.ReturnSettingDtlTeamSR :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    Public Shared Function Build_SettingDtlTeamSR(ByRef reader As IDataReader) As SettingDtlTeamSR
        Dim rptRow As New SettingDtlTeamSR
        With rptRow
            For i As Integer = 0 To reader.FieldCount - 1
                Select Case reader.GetName(i).ToUpper
                    Case "CODE"
                        SetValue(Of String)(.CODE, reader("CODE"), String.Empty)
                    Case "NAME"
                        SetValue(Of String)(.NAME, reader("NAME"), String.Empty)
                    Case "TYPE"
                        SetValue(Of String)(.TYPE, reader("TYPE"), String.Empty)
                End Select
            Next
        End With
        Return rptRow
    End Function

    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class
End Class
