﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext
Imports rpt_Customize

Partial Class iFFMR_Customize_CallRateByBrand_CallRateByBrandSearch
    Inherits System.Web.UI.Page

    Public ReadOnly Property PageName() As String
        Get
            Return "CallRateByBrandSearch"
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Call Header
        With wuc_lblHeader
            .Title = Report.GetName(SubModuleType.CALLRATE_BYBRAND)
            .DataBind()
            .Visible = True
        End With

        If Not IsPostBack Then
            LoadDDLYear()
            LoadDDLMonth()
            LoadLsbTeam()
            LoadLsbDistrict()
            LoadLsbChannel()
            LoadLsbCustClass()
            LoadLsbSpecialty()
            LoadLsbPrdClass()
            LoadLsbCriteria()
            'ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
        End If
    End Sub

    Protected Sub btnreset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        LoadDDLYear()
        LoadDDLMonth()
        LoadLsbTeam()
        LoadLsbDistrict()
        LoadLsbChannel()
        LoadLsbCustClass()
        LoadLsbSpecialty()
        LoadLsbPrdClass()
        LoadLsbCriteria()
    End Sub

#Region "YEAR"
    Private Sub LoadDDLYear()
        Dim strcurrentyear As Integer, strpreviousyear As Integer
        strcurrentyear = Now.Date.Year
        strpreviousyear = Now.Date.Year - 1

        With ddlyear
            .Items.Insert(0, New ListItem(strcurrentyear, strcurrentyear))
            .Items.Insert(1, New ListItem(strpreviousyear, strpreviousyear))
            .SelectedIndex = 0
        End With
    End Sub

#End Region

#Region "MONTH"
    Private Sub LoadDDLMonth()
        Dim strcurrentmonth As Integer
        strcurrentmonth = Now.Date.Month - 1
        With ddlmonth
            .SelectedIndex = strcurrentmonth
        End With
    End Sub


#End Region

#Region "TEAM"
    Private Sub LoadLsbTeam()
        Try
            'Clear list box before fill
            lsbTeam.Items.Clear()
            lsbSelectedTeam.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")

            With lsbTeam
                .DataSource = clsCallRateByBrand.GetSalesTeam(strUserID, strPrincipalID)
                .DataTextField = "TEAM_NAME"
                .DataValueField = "TEAM_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbTeam : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddTeam.Click
        Try
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            LoadLsbDistrict()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveTeam.Click
        Try
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            LoadLsbDistrict()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbTeam, lsbSelectedTeam)
            LoadLsbSalesrep()
            LoadLsbDistrict()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllTeam_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllTeam.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedTeam.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedTeam, lsbTeam)
            LoadLsbSalesrep()
            LoadLsbDistrict()
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllTeam_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Salesrep"
    Private Sub LoadLsbSalesrep()
        Try
            'Clear list box before fill
            lsbSalesrep.Items.Clear()
            lsbSelectedSalesrep.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbSalesrep
                .DataSource = clsCallRateByBrand.GetSalesrep(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "SALESREP_NAME"
                .DataValueField = "SALESREP_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSalesrep : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSalesrep.Click
        Try
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSalesrep.Click
        Try
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSalesrep, lsbSelectedSalesrep)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSalesrep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSalesrep.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSalesrep.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSalesrep, lsbSalesrep)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllSalesrep_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "District"
    Private Sub LoadLsbDistrict()
        Try
            'Clear list box before fill
            lsbDistrict.Items.Clear()
            lsbSelectedDistrict.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbDistrict
                .DataSource = clsCallRateByBrand.GetDistrict(strUserID, strPrincipalID, strTeamList)
                .DataTextField = "DISTRICT_NAME"
                .DataValueField = "DISTRICT_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbDistrict : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddDistrict_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddDistrict.Click
        Try
            AddToListBox(lsbDistrict, lsbSelectedDistrict)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddDistrict_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveDistrict_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveDistrict.Click
        Try
            AddToListBox(lsbSelectedDistrict, lsbDistrict)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveDistrict_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllDistrict_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllDistrict.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbDistrict.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbDistrict, lsbSelectedDistrict)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllDistrict_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllDistrict_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllDistrict.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedDistrict.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedDistrict, lsbDistrict)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllDistrict_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Channel"
    Private Sub LoadLsbChannel()
        Try
            'Clear list box before fill
            lsbChannel.Items.Clear()
            lsbSelectedChannel.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbChannel
                .DataSource = clsCallRateByBrand.GetChannel(strUserID, strPrincipalID)
                .DataTextField = "CHANNEL_NAME"
                .DataValueField = "CHANNEL_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbChannel : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddChannel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddChannel.Click
        Try
            AddToListBox(lsbChannel, lsbSelectedChannel)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddChannel_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveChannel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveChannel.Click
        Try
            AddToListBox(lsbSelectedChannel, lsbChannel)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveChannel_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllChannel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllChannel.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbChannel.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbChannel, lsbSelectedChannel)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllChannel_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllChannel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllChannel.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedChannel.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedChannel, lsbChannel)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllChannel_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "Cust Class"
    Private Sub LoadLsbCustClass()
        Try
            'Clear list box before fill
            lsbCustClass.Items.Clear()
            lsbSelectedCustClass.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbCustClass
                .DataSource = clsCallRateByBrand.GetCustClass(strUserID, strPrincipalID)
                .DataTextField = "CUST_CLASS"
                .DataValueField = "CUST_CLASS"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbCustClass : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddCustClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddCustClass.Click
        Try
            AddToListBox(lsbCustClass, lsbSelectedCustClass)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddCustClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveCustClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveCustClass.Click
        Try
            AddToListBox(lsbSelectedCustClass, lsbCustClass)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveCustClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllCustClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllCustClass.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbCustClass.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbCustClass, lsbSelectedCustClass)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllCustClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllCustClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllCustClass.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedCustClass.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedCustClass, lsbCustClass)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllCustClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Specialty"

    Private Sub LoadLsbSpecialty()
        Try
            'Clear list box before fill
            lsbSpecialty.Items.Clear()
            lsbSelectedSpecialty.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbSpecialty
                .DataSource = clsCallRateByBrand.GetSpecialty(strUserID, strPrincipalID)
                .DataTextField = "SPECIALTY"
                .DataValueField = "SPECIALTY"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbSpecialty : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddSpecialty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSpecialty.Click
        Try
            AddToListBox(lsbSpecialty, lsbSelectedSpecialty)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddSpecialty_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveSpecialty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveSpecialty.Click
        Try
            AddToListBox(lsbSelectedSpecialty, lsbSpecialty)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveSpecialty_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllSpecialty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllSpecialty.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSpecialty.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSpecialty, lsbSelectedSpecialty)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllSpecialty_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllSpecialty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllSpecialty.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedSpecialty.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedSpecialty, lsbSpecialty)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllSpecialty_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Prd Class"
    Private Sub LoadLsbPrdClass()
        Try
            'Clear list box before fill
            lsbPrdClass.Items.Clear()
            lsbSelectedPrdClass.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbPrdClass
                .DataSource = clsCallRateByBrand.GetPrdClass(strUserID, strPrincipalID)
                .DataTextField = "PRD_CLASS"
                .DataValueField = "PRD_CLASS"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbPrdClass : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddPrdClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddPrdClass.Click
        Try
            AddToListBox(lsbPrdClass, lsbSelectedPrdClass)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddPrdClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemovePrdClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemovePrdclass.Click
        Try
            AddToListBox(lsbSelectedPrdClass, lsbPrdClass)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemovePrdClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllPrdClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllPrdClass.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbPrdClass.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbPrdClass, lsbSelectedPrdClass)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllPrdClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllPrdClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllPrdClass.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedPrdClass.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedPrdClass, lsbPrdClass)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllPrdClass_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region

#Region "Criteria"
    Private Sub LoadLsbCriteria()
        Try
            'Clear list box before fill
            lsbCriteria.Items.Clear()
            lsbSelectedCriteria.Items.Clear()

            Dim clsCallRateByBrand As New rpt_Customize.clsCallRateByBrand

            Dim strUserID As String = Session.Item("UserID")
            Dim strPrincipalID As String = Session("PRINCIPAL_ID")
            Dim strTeamList As String = GetItemsInString(lsbSelectedTeam)

            With lsbCriteria
                .DataSource = clsCallRateByBrand.GetCriteria(strUserID, strPrincipalID)
                .DataTextField = "CRITERIA_NAME"
                .DataValueField = "CRITERIA_CODE"
                .DataBind()
            End With

        Catch ex As Exception
            ExceptionMsg(PageName & ".LoadLsbCriteria : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddCriteria.Click
        Try
            AddToListBox(lsbCriteria, lsbSelectedCriteria)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddCriteria_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveCriteria.Click
        Try
            AddToListBox(lsbSelectedCriteria, lsbCriteria)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveCriteria_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkAddAllCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddAllCriteria.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbCriteria.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbCriteria, lsbSelectedCriteria)
        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkAddAllCriteria_Click : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub lnkRemoveAllCriteria_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRemoveAllCriteria.Click
        Try
            Dim liToShow As ListItem
            For Each liToShow In lsbSelectedCriteria.Items
                liToShow.Selected = True
            Next
            AddToListBox(lsbSelectedCriteria, lsbCriteria)

        Catch ex As Exception
            ExceptionMsg(PageName & ".lnkRemoveAllCriteria_Click : " & ex.ToString)
        Finally
        End Try
    End Sub
#End Region



    Private Sub MoveListboxItem(ByVal index As Integer, ByVal listBox As ListBox)
        If listBox.SelectedIndex <> -1 Then
            'is there an item selected?
            'if it's moving up, the loop moves from first to last, otherwise, it moves from last to first
            Dim i As Integer = (If(index < 0, 0, listBox.Items.Count - 1))
            While If(index < 0, i < listBox.Items.Count, i > -1)
                If listBox.Items(i).Selected Then
                    'if it's moving up, it should not be the first item, or, if it's moving down, it should not be the last
                    If (index < 0 AndAlso i > 0) OrElse (index > 0 AndAlso i < listBox.Items.Count - 1) Then
                        'if it's moving up, the previous item should not be selected, or, if it's moving down, the following item should not be selected
                        If (index < 0 AndAlso Not listBox.Items(i - 1).Selected) OrElse (index > 0 AndAlso Not listBox.Items(i + 1).Selected) Then
                            Dim itemA As ListItem = listBox.Items(i)
                            'the selected item
                            listBox.Items.Remove(itemA)
                            'is removed
                            'and swapped
                            listBox.Items.Insert(i + index, itemA)
                        End If
                    End If
                End If
                i -= index
            End While
        End If
    End Sub

    Private Sub AddToListBox(ByVal lsbFrom As ListBox, ByVal lsbTo As ListBox)
        Try
            If lsbFrom.GetSelectedIndices().Length > 0 Then
                'Add into destination listbox
                Dim liToAdd As ListItem
                'lsbTo.SelectedIndex = -1
                For Each intIndex As Integer In lsbFrom.GetSelectedIndices()
                    liToAdd = lsbFrom.Items(intIndex)
                    If liToAdd IsNot Nothing Then
                        lsbTo.Items.Add(liToAdd)
                    End If
                Next

                'Remove from source listbox
                Dim liToHide As ListItem
                For Each intindex As Integer In lsbTo.GetSelectedIndices
                    liToHide = lsbTo.Items(intindex)
                    If liToHide IsNot Nothing Then
                        lsbFrom.Items.Remove(liToHide)
                    End If
                Next
                'lsbFrom.SelectedIndex = IIf(lsbFrom.Items.Count > 0, 0, -1)
                lsbFrom.SelectedIndex = -1
                lsbTo.SelectedIndex = -1
            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".AddToListBox : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function GetItemsInString(ByVal lsbFrom As ListBox) As String
        Dim sbString As New Text.StringBuilder
        Dim aryList As New ArrayList
        Dim liToAdd As String
        Dim intIndex As Integer

        For intIndex = 0 To lsbFrom.Items.Count - 1
            liToAdd = lsbFrom.Items(intIndex).Value
            If liToAdd IsNot Nothing Then
                If aryList.IndexOf(Trim(liToAdd)) < 0 Then
                    sbString.Append(IIf(aryList.Count > 0, ",", String.Empty) & "'" & Trim(liToAdd) & "'")
                    aryList.Add(Trim(liToAdd))
                End If
            End If
        Next
        Return sbString.ToString
    End Function

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Cuzrpt.CallRateByBrandSession.Year = ddlyear.SelectedValue
        Cuzrpt.CallRateByBrandSession.Month = ddlmonth.SelectedValue
        Cuzrpt.CallRateByBrandSession.TeamCode = GetItemsInString(lsbSelectedTeam)
        Cuzrpt.CallRateByBrandSession.SalesrepCode = GetItemsInString(lsbSelectedSalesrep)
        Cuzrpt.CallRateByBrandSession.DistrictCode = GetItemsInString(lsbSelectedDistrict)
        Cuzrpt.CallRateByBrandSession.ChannelCode = GetItemsInString(lsbSelectedChannel)
        Cuzrpt.CallRateByBrandSession.CustClass = GetItemsInString(lsbSelectedCustClass)
        Cuzrpt.CallRateByBrandSession.Specialty = GetItemsInString(lsbSelectedSpecialty)
        Cuzrpt.CallRateByBrandSession.PrdClass = GetItemsInString(lsbSelectedPrdClass)
        Cuzrpt.CallRateByBrandSession.Criteria = GetItemsInString(lsbSelectedCriteria)

        Dim StrScript As String
        StrScript = "parent.document.getElementById('ContentBarIframe').src='../../iFFMR/Customize/CallRateByBrand/CallRateByBrandGrid.aspx';"
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
    End Sub
End Class
