<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Itinerary.aspx.vb" Inherits="iFFMA_Utility_Upload_Itinerary" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Itinerary Upload</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmItineraryUpload" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr>
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <%--<asp:UpdatePanel ID="UpdSelection" runat="server" UpdateMode="Conditional" RenderMode="block">
                            <ContentTemplate>--%>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td colspan="2">
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <%--   <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />--%>
                                </td>
                            </tr>
                            <tr align="left">
                                <td width="10%" style="padding: 2px;">
                                    <span class="cls_label_header">File Location:</span></td>
                                <td width="50%" style="padding: 2px;">
                                    <asp:FileUpload ID="fulocation" runat="server" CssClass="cls_textbox" Width="500px"
                                        Height="20px" />
                                    <asp:RequiredFieldValidator ID="rfvfuprd" runat="server" ErrorMessage="Enter Path"
                                        ControlToValidate="fulocation" ValidationGroup="Fileupload" Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                     <asp:Button ID="btnvalidate" runat="server" Text="Validate" CssClass="cls_button" /><br />
                                    <asp:Label ID="lblmsg" runat="server" CssClass="cls_label_header"></asp:Label>
                                </td>
                            </tr>
                            <tr align="left">
                                <td style="padding: 2px;">
                                    <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="cls_button" />
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="2" align="center">
                                    <div style="width: 100%; padding: 2px;">
                                        <asp:Label ID="lblttlrecords" runat="server" CssClass="cls_button" Visible="false"></asp:Label></div>
                                    <div style="width: 100%; padding: 2px;">
                                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                            ShowFooter="false" AllowPaging="FALSE" PagerSettings-Visible="false" Visible="false">
                                             <Columns>
                                                <asp:BoundField DataField="salesrep_Code" HeaderText="Field Force Code" HtmlEncode="False" />
                                                <asp:BoundField DataField="cust_code" HeaderText="Customer Code" HtmlEncode="False" />
                                                <asp:BoundField DataField="cont_code" HeaderText="Contact code" HtmlEncode="False" />
                                                <asp:BoundField DataField="route_code" HeaderText="Route Code" HtmlEncode="False" />
                                                <asp:BoundField DataField="valid_data" HeaderText="Status" HtmlEncode="False" />
                                            </Columns>
                                        </ccGV:clsGridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <%--      </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
