Imports System.Data
Imports Microsoft.VisualBasic
Imports System.Web.HttpContext
Partial Class iFFMS_Plan_PlanMSS
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection
    Dim licItemFigureCollectorDtl As ListItemCollection

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property

    Private Property Master_Row_CountSelectedMSS() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountSelectedMSS"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountSelectedMSS") = value
        End Set
    End Property

    Private Property Master_Row_CountNewMSS() As Integer
        Get
            Return CInt(ViewState("Master_Row_CountNewMSS"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_CountNewMSS") = value
        End Set
    End Property

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("UserID") = "" Then
        '    Dim strScript As String = ""
        '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
        '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
        'End If


        'Call Header
        With wuc_lblHeader
            .Title = "Assign Market Survey" 'Report.GetName(SubModuleType.TRAORDER)
            .DataBind()
        End With

        If Not IsPostBack Then

            ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")
            TimerControlSelectedMSS.Enabled = True
            TimerControlNewMSS.Enabled = True
        End If
    End Sub


#Region "DGLISTSelectedMss"
    Protected Sub TimerControlSelectedMSS_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlSelectedMSS.Tick
        If TimerControlSelectedMSS.Enabled Then RefreshDatabindingSelectedMSS()
        TimerControlSelectedMSS.Enabled = False
    End Sub

    Public Sub RenewDataBindSelectedMSS()
        RefreshDataBindSelectedMSS()
    End Sub

    Public Sub RefreshDataBindSelectedMSS()
        RefreshDatabindingSelectedMSS()
    End Sub

    Public Sub RefreshDatabindingSelectedMSS(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableSelectedMSS As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionSelectedMSS As String = CType(ViewState("strSortExpressionSelectedMSS"), String)


        dtCurrentTableSelectedMSS = GetRecListSelectedMSS()

        If dtCurrentTableSelectedMSS Is Nothing Then
            dtCurrentTableSelectedMSS = New DataTable
        Else
            If dtCurrentTableSelectedMSS.Rows.Count = 0 Then
                Master_Row_CountSelectedMSS = 0
                btncheckalldelete.Visible = False
                btnuncheckalldelete.Visible = False
                btndelete.Visible = False
                'dtCurrentTableSelectedCont.Rows.Add(dtCurrentTableSelectedCont.NewRow())
            Else

                Master_Row_CountSelectedMSS = dtCurrentTableSelectedMSS.Rows.Count
                btncheckalldelete.Visible = True
                btnuncheckalldelete.Visible = True
                btndelete.Visible = True
            End If
        End If

        Dim dvCurrentViewSelectedMSS As New DataView(dtCurrentTableSelectedMSS)
        If Not String.IsNullOrEmpty(strSortExpressionSelectedMSS) Then
            Dim strSortExpressionNameSelectedMSS As String = strSortExpressionSelectedMSS.Replace(" DESC", "")
            dvCurrentViewSelectedMSS.Sort = IIf(dtCurrentTableSelectedMSS.Columns.Contains(strSortExpressionSelectedMSS), strSortExpressionSelectedMSS, "")
        End If

        With dglistSelectedMSS
            .DataSource = dvCurrentViewSelectedMSS
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountSelectedMSS > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateSelectedMSS()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListSelectedMSS() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strUserId As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))
            strCustCode = Trim(Request.QueryString("CustCode"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonMSS(strSalesrepCode, strRouteDate, strCustCode, strUserId)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateSelectedMSS()

        UpdateSelectedMSS.Update()

    End Sub

    Protected Sub dglistSelectedMSS_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistSelectedMSS.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountSelectedMSS > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListSelectedMSS_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistSelectedMSS.Sorting
        Dim strSortExpressionSelectedMSS As String = ViewState("strSortExpressionSelectedMSS")

        If strSortExpressionSelectedMSS IsNot Nothing AndAlso strSortExpressionSelectedMSS.Length > 0 Then
            If strSortExpressionSelectedMSS Like (e.SortExpression & "*") Then
                If strSortExpressionSelectedMSS.IndexOf(" DESC") > 0 Then
                    strSortExpressionSelectedMSS = e.SortExpression
                Else
                    strSortExpressionSelectedMSS = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionSelectedMSS = e.SortExpression
            End If
        Else
            strSortExpressionSelectedMSS = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionSelectedMSS") = strSortExpressionSelectedMSS

        RefreshDatabindingSelectedMSS()

    End Sub

#End Region

#Region "DGLISTNewCust"
    Protected Sub TimerControlNewMSS_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControlNewMSS.Tick
        If TimerControlNewMSS.Enabled Then RefreshDatabindingNewMSS()
        TimerControlNewMSS.Enabled = False
    End Sub

    Public Sub RenewDataBindNewMSS()
        RefreshDataBindNewMSS()
    End Sub

    Public Sub RefreshDataBindNewMSS()
        RefreshDatabindingNewMSS()
    End Sub

    Public Sub RefreshDatabindingNewMSS(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTableNewMSS As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpressionNewMSS As String = CType(ViewState("strSortExpressionNewMSS"), String)


        dtCurrentTableNewMSS = GetRecListNewMSS()

        If dtCurrentTableNewMSS Is Nothing Then
            dtCurrentTableNewMSS = New DataTable
        Else
            If dtCurrentTableNewMSS.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                Master_Row_CountSelectedMSS = 0
                btncheckall.Visible = False
                btnuncheckall.Visible = False
                btnadd.Visible = False
            Else
                Master_Row_CountSelectedMSS = dtCurrentTableNewMSS.Rows.Count
                btncheckall.Visible = True
                btnuncheckall.Visible = True
                btnadd.Visible = True
            End If
        End If

        Dim dvCurrentViewNewMSS As New DataView(dtCurrentTableNewMSS)
        If Not String.IsNullOrEmpty(strSortExpressionNewMSS) Then
            Dim strSortExpressionNameSelectedCont As String = strSortExpressionNewMSS.Replace(" DESC", "")
            dvCurrentViewNewMSS.Sort = IIf(dtCurrentTableNewMSS.Columns.Contains(strSortExpressionNewMSS), strSortExpressionNewMSS, "")
        End If

        With dglistNewMSS
            .DataSource = dvCurrentViewNewMSS
            '.PageSize = intPageSize
            .AllowSorting = IIf(isExport, False, IIf(Master_Row_CountNewMSS > 0, True, False)) 'HL:20080424
            .DataBind()
        End With

        UpdateDatagrid_UpdateNewMSS()
        UpdatePage.Update()
    End Sub

    Private Function GetRecListNewMSS() As DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strSalesrepCode As String, strCustCode As String, strUserId As String, strRouteDate As String
            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
            strUserId = Trim(Session("UserID"))
            strCustCode = Trim(Request.QueryString("CustCode"))
            strRouteDate = Trim(Request.QueryString("RouteDate"))

            Dim clsPrePlan As New txn_PrePlan.clsPrePlan
            DT = clsPrePlan.GetPlanAddonContByMSS(strSalesrepCode, strCustCode, strUserId, strRouteDate)

        End If

        Return DT
    End Function

    Public Sub UpdateDatagrid_UpdateNewMSS()

        UpdateNewMSS.Update()

    End Sub

    Protected Sub dglistNewMSS_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dglistNewMSS.RowCreated
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.DataRow
                    If Master_Row_CountNewMSS > 0 Then

                    End If
            End Select
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try

    End Sub

    Protected Sub dgListNewMSS_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dglistNewMSS.Sorting
        Dim strSortExpressionNewMSS As String = ViewState("strSortExpressionNewMSS")

        If strSortExpressionNewMSS IsNot Nothing AndAlso strSortExpressionNewMSS.Length > 0 Then
            If strSortExpressionNewMSS Like (e.SortExpression & "*") Then
                If strSortExpressionNewMSS.IndexOf(" DESC") > 0 Then
                    strSortExpressionNewMSS = e.SortExpression
                Else
                    strSortExpressionNewMSS = e.SortExpression & " DESC"
                End If
            Else
                strSortExpressionNewMSS = e.SortExpression
            End If
        Else
            strSortExpressionNewMSS = e.SortExpression
        End If
        'CriteriaCollector.SortExpression = strSortExpression
        ViewState("strSortExpressionNewMSS") = strSortExpressionNewMSS

        RefreshDatabindingNewMSS()

    End Sub

#End Region


#Region "Event Handler"

    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnadd.Click
        AddNewMSS()
        TimerControlSelectedMSS.Enabled = True
        TimerControlNewMSS.Enabled = True
    End Sub

    Protected Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        DelNewMSS()
        TimerControlSelectedMSS.Enabled = True
        TimerControlNewMSS.Enabled = True
    End Sub

#End Region

    Private Sub AddNewMSS()
        Try
            If dglistNewMSS.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strTitleCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistNewMSS.Rows
                    chkSelected = CType(DR.FindControl("chkSelect"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistNewMSS.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strTitleCode = dglistNewMSS.DataKeys(i).Item("TITLE_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.InstPlanAddonMSS(strSalesrepCode, strRouteDate, strCustCode, strTitleCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblNmsg.Text = "Record(s) added !"
            lblsmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub DelNewMSS()
        Try
            If dglistSelectedMSS.Rows.Count > 0 Then
                Dim chkSelected As CheckBox
                Dim i As Integer = 0
                Dim clsPrePlan As New txn_PrePlan.clsPrePlan
                Dim strSalesrepCode As String, strRouteDate As String, strCustCode As String, strTitleCode As String, StrUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dglistSelectedMSS.Rows
                    chkSelected = CType(DR.FindControl("chkDelete"), CheckBox)

                    If chkSelected IsNot Nothing AndAlso chkSelected.Checked Then

                        DK = dglistSelectedMSS.DataKeys(i)
                        If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then
                            strSalesrepCode = Trim(Request.QueryString("SalesrepCode"))
                            strRouteDate = Trim(Request.QueryString("RouteDate"))
                            strCustCode = Trim(Request.QueryString("CustCode"))
                            strTitleCode = dglistSelectedMSS.DataKeys(i).Item("TITLE_CODE")
                            StrUserId = Trim(Session("UserID"))

                            clsPrePlan.DelPlanAddonMSS(strSalesrepCode, strRouteDate, strCustCode, strTitleCode, StrUserId)
                        End If
                    End If
                    i += 1
                Next

            End If
            lblSmsg.Text = "Record(s) deleted !"
            lblNmsg.Text = ""
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
