Imports System.Data

Partial Class iFFMA_FieldForceActy_MSSAdv2QuesPreviewPop
    Inherits System.Web.UI.UserControl

    Private strTitle As String = "Maintenance"
    Private strMessage As String = String.Empty

#Region "Property Control"
    ''' <summary>
    ''' Property of Message box header text
    ''' </summary>
    ''' <value>Set the STRING text of the header</value>
    ''' <returns>Get the STRING text of the header</returns>
    ''' <remarks>Set /  Get the string text of the header</remarks>
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            strTitle = value
            If lblTitle IsNot Nothing Then lblTitle.Text = value
        End Set
    End Property

    ''' <summary>
    ''' Property of Message box content message text
    ''' </summary>
    ''' <value>Set the STRING text of the content message</value>
    ''' <returns>Get the STRING text of the content message</returns>
    ''' <remarks>Set /  Get the string text of the content message</remarks>
    Public Property Message() As String
        Get
            Return lblContent.Text
        End Get
        Set(ByVal value As String)
            strMessage = value
            If lblContent IsNot Nothing Then lblContent.Text = value
        End Set
    End Property

    Public Property TeamCode() As String
        Get
            Return Trim(hdTeamCode.Value)
        End Get
        Set(ByVal value As String)
            hdTeamCode.Value = value
        End Set
    End Property

    Public Property TitleCode() As String
        Get
            Return Trim(hdTitleCode.Value)
        End Get
        Set(ByVal value As String)
            hdTitleCode.Value = value
        End Set
    End Property

    Public Property QuesCode() As String
        Get
            Return Trim(hdQuesCode.Value)
        End Get
        Set(ByVal value As String)
            hdQuesCode.Value = value
        End Set
    End Property
#End Region

#Region "Function Control"
    Public Sub Show()
        DataBind()
        ModalPopupMaintenance.Show()
    End Sub

    Public Sub Hide()
        ModalPopupMaintenance.Hide()
    End Sub

    Public Overrides Sub DataBind()
        lblTitle.Text = strTitle
        lblContent.Text = strMessage

        updPnlMaintenance.Update()
    End Sub

    Public Sub BindQuesPreview()
        Try
            Dim DT As DataTable
            Dim dtCriteria As DataTable
            Dim dvCriteria As DataView
            Dim strTypeS1, strTypeS2, strTypeS3, strTypeS4 As String
            Dim clsMSSAdv As New mst_FieldForceActy.clsMSSAdv2

            DT = clsMSSAdv.GetMSSQuesAdvPreview(TeamCode, TitleCode, QuesCode)

            ResetPreview()

            If DT.Rows.Count > 0 Then
                Message = DT.Rows(0)("QUES_NAME")

                lblS1.Text = DT.Rows(0)("SUB_QUES_NAME_S1")
                lblS2.Text = DT.Rows(0)("SUB_QUES_NAME_S2")
                lblS3.Text = DT.Rows(0)("SUB_QUES_NAME_S3")
                lblS4.Text = DT.Rows(0)("SUB_QUES_NAME_S4")

                strTypeS1 = DT.Rows(0)("SUB_QUES_TYPE_S1")
                strTypeS2 = DT.Rows(0)("SUB_QUES_TYPE_S2")
                strTypeS3 = DT.Rows(0)("SUB_QUES_TYPE_S3")
                strTypeS4 = DT.Rows(0)("SUB_QUES_TYPE_S4")

                'S1------------------------------------
                If strTypeS1 = "0" Then
                    'S1.Visible = False
                ElseIf strTypeS1 = "1" Then
                    txtS1.Visible = True
                ElseIf strTypeS1 = "2" Then
                    ddlS1.Visible = True
                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S1")
                    With ddlS1
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS1 = "3" Then
                    txtDateS1.Visible = True
                ElseIf strTypeS1 = "4" Then
                    rdbListS1.Visible = True
                ElseIf strTypeS1 = "5" Then
                    S1.Style.Item("height") = "100px"
                    dgTextS1.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S1")
                    dvCriteria = dtCriteria.DefaultView
                    dgTextS1.DataSource = dvCriteria
                    dgTextS1.DataBind()
                    dtCriteria = Nothing
                ElseIf strTypeS1 = "6" Then
                    S1.Style.Item("height") = "100px"
                    dgSelectS1.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S1")
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectS1.DataSource = dvCriteria
                    dgSelectS1.DataBind()
                    dtCriteria = Nothing
                End If

                'S2------------------------------------
                If strTypeS2 = "0" AndAlso (strTypeS1 = "5" OrElse strTypeS1 = "6") Then
                    S2.Visible = False
                ElseIf strTypeS2 = "1" Then
                    txtS2.Visible = True
                ElseIf strTypeS2 = "2" Then
                    ddlS2.Visible = True
                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S2")
                    With ddlS2
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS2 = "3" Then
                    txtDateS2.Visible = True
                ElseIf strTypeS2 = "4" Then
                    rdbListS2.Visible = True
                ElseIf strTypeS2 = "5" Then
                    S2.Style.Item("height") = "100px"
                    dgTextS2.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S2")
                    dvCriteria = dtCriteria.DefaultView
                    dgTextS2.DataSource = dvCriteria
                    dgTextS2.DataBind()
                    dtCriteria = Nothing
                ElseIf strTypeS2 = "6" Then
                    S2.Style.Item("height") = "100px"
                    dgSelectS2.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S2")
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectS2.DataSource = dvCriteria
                    dgSelectS2.DataBind()
                    dtCriteria = Nothing
                End If

                'S3------------------------------------
                If strTypeS3 = "0" AndAlso (strTypeS2 = "5" OrElse strTypeS2 = "6") Then
                    S3.Visible = False
                ElseIf strTypeS3 = "1" Then
                    txtS3.Visible = True
                ElseIf strTypeS3 = "2" Then
                    ddlS3.Visible = True
                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S3")
                    With ddlS3
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS3 = "3" Then
                    txtDateS3.Visible = True
                ElseIf strTypeS3 = "4" Then
                    rdbListS3.Visible = True
                ElseIf strTypeS3 = "5" Then
                    S3.Style.Item("height") = "100px"
                    dgTextS3.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S3")
                    dvCriteria = dtCriteria.DefaultView
                    dgTextS3.DataSource = dvCriteria
                    dgTextS3.DataBind()
                    dtCriteria = Nothing
                ElseIf strTypeS3 = "6" Then
                    S3.Style.Item("height") = "100px"
                    dgSelectS3.Visible = True

                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S3")
                    dvCriteria = dtCriteria.DefaultView
                    dgSelectS3.DataSource = dvCriteria
                    dgSelectS3.DataBind()
                    dtCriteria = Nothing
                End If

                'S4------------------------------------
                If strTypeS4 = "0" AndAlso (strTypeS3 = "5" OrElse strTypeS3 = "6") Then
                    S4.Visible = False
                ElseIf strTypeS4 = "1" Then
                    txtS4.Visible = True
                ElseIf strTypeS4 = "2" Then
                    ddlS4.Visible = True
                    dtCriteria = clsMSSAdv.GetMSSQuesAdvPreviewCriteria(TeamCode, TitleCode, QuesCode, "S4")
                    With ddlS4
                        .Items.Clear()
                        .DataSource = dtCriteria.DefaultView
                        .DataTextField = "CRITERIA_NAME"
                        .DataValueField = "CRITERIA_CODE"
                        .DataBind()
                        .Items.Insert(0, New ListItem("-- SELECT --", ""))
                        .SelectedIndex = 0
                    End With
                    dtCriteria = Nothing
                ElseIf strTypeS4 = "3" Then
                    txtDateS4.Visible = True
                ElseIf strTypeS4 = "4" Then
                    rdbListS4.Visible = True
                End If
            End If

            Show()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Sub ResetPreview()
        Message = ""

        lblS1.Text = ""
        lblS2.Text = ""
        lblS3.Text = ""
        lblS4.Text = ""

        S1.Visible = True
        S2.Visible = True
        S3.Visible = True
        S4.Visible = True

        S1.Style.Item("height") = "50px"
        S2.Style.Item("height") = "50px"
        S3.Style.Item("height") = "50px"
        S4.Style.Item("height") = "50px"

        txtS1.Visible = False
        txtS2.Visible = False
        txtS3.Visible = False
        txtS4.Visible = False

        ddlS1.Visible = False
        ddlS2.Visible = False
        ddlS3.Visible = False
        ddlS4.Visible = False

        txtDateS1.Visible = False
        txtDateS2.Visible = False
        txtDateS3.Visible = False
        txtDateS4.Visible = False

        rdbListS1.Visible = False
        rdbListS2.Visible = False
        rdbListS3.Visible = False
        rdbListS4.Visible = False

        dgTextS1.Visible = False
        dgTextS2.Visible = False
        dgTextS3.Visible = False

        dgSelectS1.Visible = False
        dgSelectS2.Visible = False
        dgSelectS3.Visible = False
    End Sub
#End Region

#Region "Event Control"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblContent.Text = String.Empty

            If Not Page.IsPostBack Then
                lblTitle.Text = strTitle
                ModalPopupMaintenance.BehaviorID = ModalPopupMaintenance.UniqueID
            Else
                'lblInfo.Text = ""
            End If
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            'lblErr.Text = ""
            'lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class
