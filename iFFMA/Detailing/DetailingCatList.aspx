<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DetailingCatList.aspx.vb" Inherits="iFFMA_Detailing_DetailingCatList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_txtCalendarRange" Src="~/include/wuc_txtCalendarRange.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_DetailingSubCatList" Src="~/iFFMA/Detailing/DetailingSubCatList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_DetailingPreviewPop" Src="~/iFFMA/Detailing/DetailingPreviewPop.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblMsgPop" Src="~/include/wuc_lblMsgPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<%@ Register Src="~/iFFMA/Detailing/DetailingSubCatList.ascx" TagPrefix="customToolkit" TagName="DetailingSubCatList" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Detailing Category List</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script src="../../scripts/jquery-1.10.2.min.js" type="text/javascript"></script>

    <script type="text/javascript" language="Javascript">

        function checkFileSize() {
            var maxFileSize = 20971520;
            var fileUpload = $('.uploadBtn');
            if (fileUpload.val() !== '') {
                var file = fileUpload[0].files[0];
                if (file.size > maxFileSize) {
                    var sizeInMB = (file.size / (1024 * 1024)).toFixed(2);
                    alert('File size ' + sizeInMB + 'Mb is too big! Please select file less than 20Mb in size.');
                    fileUpload.val("");
                }

                if (!(file.type.indexOf("image/") > -1 || file.type.indexOf("video/") > -1 || file.type.indexOf("audio/") > -1 ||
                    file.type == "text/plain" || file.type == "text/html" || file.type == "application/vnd.ms-excel" || 
                    file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                    file.type == "application/vnd.ms-powerpoint" || file.type == "application/pdf" || file.type == "application/msword" ||
                    file.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                    file.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation"))
                {
                    alert('File type not supported! Please select another file.');
                    fileUpload.val("");
                }
            }
        }


        function SelectRow(chkSelected) {
            if (chkSelected.checked == false) {

                for (var i = 0; i < document.forms[0].elements.length; i++) {
                    var e = document.forms[0].elements[i];

                    if (e.type == 'checkbox') {
                        e.checked = chkSelected.checked;
                        break;
                    }
                }
            }
        }

        function SelectAllRows(chkAll) {

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox') {
                    e.checked = chkAll.checked;
                }
            }
        }

        function checkDelete() {
            var isChecked = false;

            for (var i = 0; i < document.forms[0].elements.length; i++) {
                var e = document.forms[0].elements[i];

                if (e.type == 'checkbox' && e.checked) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                if (!confirm('Are you sure want to delete?')) {
                    return false;
                }
            }
            else {
                alert('Please select at least one of the record to delete!');
                return false;
            }
            return true;
        }
    </script>
</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmDetailingCatList" runat="server" enctype="multipart/form-data">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300" ScriptMode="Release" />
        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                            <tr align="left">
                                <td>
                                    <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                    <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                    <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                    <br />
                                    <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />

                                    <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />

                                            <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height="" ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                                <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Panel ID="pnlList" runat="server">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <center>
                                                                            <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                            <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                                Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                                CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                                ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="DET_CODE">
                                                                            </ccGV:clsGridView>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlDetails" runat="server" Visible="false">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <asp:Button ID="btnSave" runat="server" CssClass="cls_button" Text="Save" ValidationGroup="Save" /><br />
                                                                        <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="padding-left: 15px">
                                                                        <table border="0" cellpadding="2" cellspacing="0" width="95%">
                                                                            <tr>
                                                                                <td style="width: 17%"></td>
                                                                                <td style="width: 3%"></td>
                                                                                <td style="width: 80%"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Detailing Code</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtDetCode" runat="server" CssClass="cls_textbox" MaxLength="50" Enabled="false" />
                                                                                    <asp:Label ID="lblMsgDetCode" runat="server" CssClass="cls_label" MaxLength="50" Enabled="false" />
                                                                                    <asp:RequiredFieldValidator ID="rfvDetCode" runat="server" ControlToValidate="txtDetCode"
                                                                                    ErrorMessage="Detailing Code is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator" /></td>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Detailing Name</span> <span class="cls_label_mark">*</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtDetName" runat="server" CssClass="cls_textbox" MaxLength="50" Enabled="false" />
                                                                                <asp:RequiredFieldValidator ID="rfvDetName" runat="server" ControlToValidate="txtDetName"
                                                                                    ErrorMessage="Detailing Name is Required." ValidationGroup="Save"
                                                                                    Display="Dynamic" CssClass="cls_validator" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><span class="cls_label_header">Date</span></td>
                                                                                <td><span class="cls_label_header">:</span></td>
                                                                                <td valign="top" align="left" style="white-space: nowrap" colspan="3">
                                                                                    <customToolkit:wuc_txtCalendarRange
                                                                                        ID="txtDate"
                                                                                        runat="server"
                                                                                        RequiredValidation="true"
                                                                                        RequiredValidationGroup="Save"
                                                                                        DateFormatString="yyyy-MM-dd"
                                                                                        CompareDateRangeValidation="true" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3">&nbsp;</td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel ID="TabPanel2" runat="server">
                                                    <ContentTemplate>
                                                        <customToolkit:wuc_DetailingSubCatList ID="wuc_DetailingSubCatList" runat="server" />
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>


                                </td>
                            </tr>

                            <tr class="Bckgroundreport">
                                <td style="height: 5px"></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <customToolkit:wuc_lblMsgPop ID="lblMsgPop" Title="This category cannot be deleted." runat="server" />
        <customToolkit:wuc_DetailingPreviewPop ID="wuc_DetailingPreviewPop" Title="Detailing Preview" runat="server" />
    </form>
</body>
</html>
