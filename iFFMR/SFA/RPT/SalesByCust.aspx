<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SalesByCust.aspx.vb" Inherits="iFFMR_SFA_RPT_SalesByCust" EnableEventValidation = "false" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_ctrlPnl" Src="~/iFFMR/SFA/USRCTRL/wuc_ctrlpnl.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblheader" Src="~/include/wuc_lblheader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_Page" Src="~/iFFMR/SFA/USRCTRL/wuc_page.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Sales By Customer</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">window.onresize=function(){resizeLayout();}

function Redirect(page){document.getElementById("PrdDtliFrame").src = page;}
</script>
</head>
<body style="background-color:#EEEEEE;">
    <form id="frmPrdSales" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
            ScriptMode="Release" />
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout" align="center">
                    <fieldset class="" style="width: 98%;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="">
                            <tr>
                                <td align="left">
                                    <asp:UpdatePanel runat="server" ID="Update_lblErr" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:Label ID="lblErr" runat="server" CssClass="cls_label_err"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <customToolkit:wuc_ctrlPnl ID="wuc_ctrlPnl" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="BckgroundInsideContentLayout" align="left">
                                                                        
                                            <table id="tblReport" runat="server" border="0" cellpadding="0" cellspacing="5" width="100%"
                                                align="center" class="Bckgroundreport">
                                                <tr>
                                                    <td colspan="2">
                                                        <customToolkit:wuc_lblheader ID="wuc_lblHeader" runat="server" Title="Sales By Customer"></customToolkit:wuc_lblheader>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="BckgroundBenealthTitle" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <customToolkit:wuc_Page ID="wuc_Page1" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td valign="top" class="Bckgroundreport" style="padding-top: 10px; width:70%">
                                                        <%--<customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                                        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                            <ContentTemplate>
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                <asp:Button ID="btnRefresh" runat="server" Text="" Style="display: none" />
                                                                <ccGV:clsGridView ID="dgList" runat="server" ShowFooter="true" AllowPaging="false"
                                                                    AllowSorting="true" AutoGenerateColumns="false" Width="98%" FreezeHeader="true"
                                                                    GridHeight="480" RowSelectionEnabled="true">
                                                                </ccGV:clsGridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>--%>
                                                        <customToolkit:wuc_UpdateProgress ID="wuc_UpdateProgress1" runat="server" />
                                                        <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                                            <ContentTemplate>
                                                                <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                                                <asp:Button ID="btnRefresh" runat="server" Text="" Style="display: none" />
                                                                    <div style="height: 400px; overflow: auto;">
                                                                    <asp:Panel ID="PnlExport" runat="server">
                                                                    <table border="1" cellpadding='2' cellspacing='0' width="97%" class="RepeaterBorder">
                                                                        <tr class="GridHeader">
                                                                            <th>
                                                                                <asp:TextBox ID="txtCustCode" runat="server" Width="100"></asp:TextBox></th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtCustName" runat="server" Width="100"></asp:TextBox></th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtDistrictName" runat="server" Width="100"></asp:TextBox></th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtChannelName" runat="server" Width="100"></asp:TextBox></th>
                                                                            <th>
                                                                                <asp:TextBox ID="txtHospital" runat="server" Width="100"></asp:TextBox></th>
                                                                            <th>&nbsp;
                                                                                </th>
                                                                        </tr>
                                                                    <asp:Repeater ID="RptrSalesByCustHdr" runat="server" OnItemCreated="RptrSalesByCustHdr_ItemCreated" OnItemDataBound="RptrSalesByCustHdr_ItemDataBound">                                                            
                                                                    <HeaderTemplate>                                                           <%-- adjust width--%>
                                                                        
                                                                        <tr class="GridHeader">
                                                                            <th>Customer</th><th>Name</th><th>District</th><th>Channel</th><th>Hospital</th><th>Amount</th>
                                                                        </tr>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr class="mo" runat="server" id="Drow" valign="top">
                                                                            <td>
                                                                                <%#Eval("CUST_CODE")%></td>
                                                                            <td>
                                                                                <%# EVAL("CUST_NAME") %></td>
                                                                            <td>
                                                                                <%# EVAL("DISTRICT_NAME") %></td>
                                                                            <td>
                                                                                <%# EVAL("CHANNEL_NAME") %></td>
                                                                            <td>
                                                                               <%# EVAL("HOSPITAL_NAME") %></td>
                                                                            <td align="right">
                                                                                <%#Format("{0:#,0.00}", Convert.ToDouble(Eval("SALES_AMT")))%></td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <tr class="GridFooter">
                                                                            <td colspan="4" align="center">
                                                                                Total</td>
                                                                                <td colspan="2" align="right">
                                                                                    <asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label>
                                                                                </td>
                                                                        </tr>
                                                                        
                                                                    </FooterTemplate>
                                                                    </asp:Repeater>
                                                                    </table>
                                                                    </asp:Panel>
                                                                    </div>
                                                        </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td  valign="top" class="Bckgroundreport" style="padding-top: 10px; width:30%">
                                                        <iframe src="SalesByCust_PrdDtl.aspx" width="100%" height="400px" id="PrdDtliFrame"  runat="server" frameborder="0" marginwidth="0" marginheight="0"
                                                            scrolling="no" style="border: 0; position: relative; display: inline; top: 0px;"></iframe>
                                                    </td>
                                                    
                                                </tr>
                                                <tr class="Bckgroundreport" style="height: 10px">
                                                    <td colspan="2">
                                                    </td>
                                                </tr>
                                            </table>
                                    
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>

    
    </form>
</body>

</html>