Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsPackage
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsPAFQuery"
        End Get
    End Property

    Public Function GetSRList(ByVal strUserID As String, ByVal strPrincipalID As String) As DataTable
        Dim obj As clsDB
        Try
            obj = New clsDB
            With obj
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_PACKAGE_SR_LIST"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipalID, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSRList :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Public Function GetPackageList(ByVal strStartDate As String, ByVal strEndDate As String, ByVal strSalesRep As String) As DataTable
        Dim obj As clsDB
        Try
            obj = New clsDB
            With obj
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_PACKAGE_LIST"
                .addItem("STARTDATE", strStartDate, clsDB.DataType.DBString)
                .addItem("ENDDATE", strEndDate, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesRep, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPackageList :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Public Function GetPackageListDtl(ByVal strStartDate As String, ByVal strEndDate As String, ByVal strSalesRep As String, ByVal strPackageCode As String) As DataTable
        Dim obj As clsDB
        Try
            obj = New clsDB
            With obj
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_PACKAGE_DTL_LIST"
                .addItem("PACKAGE_CODE", strPackageCode, clsDB.DataType.DBString)
                .addItem("STARTDATE", strStartDate, clsDB.DataType.DBString)
                .addItem("ENDDATE", strEndDate, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", strSalesRep, clsDB.DataType.DBString)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetPackageListDtl :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
