﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MSSTeamMap.aspx.vb" Inherits="iFFMA_FieldForceActy_MSSTeamMap" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MSS Team Mapping</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />

    <script src="../../include/jquery-1.4.2.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function SelectdgListAllCheckboxes() {
            $("#tcResult_TabPanel1_dgList INPUT:enabled[type='checkbox']")
                    .attr('checked', true);
        }

        function SelectdgListAllUnCheckboxes() {
            $("#tcResult_TabPanel1_dgList INPUT:enabled[type='checkbox']")
                 .attr('checked', false);
        }
    </script>

</head>
<!--#include File="~/include/commonutil.js"-->
<body class="BckgroundInsideContentLayout">
    <form id="frmMSSTeamMapping" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                        <tr align="left">
                            <td>
                                <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" Visible="false" />
                                <table id="tblctrlpanel" class="cls_panel_header" cellspacing="0" cellpadding="0"
                                    width="100%" border="0" style="height: 30px; padding: 2px 0px 2px 2px">
                                    <tr valign="bottom">
                                        <td>
                                            <asp:Image ID="imgEnquiry" ImageUrl="~/images/ico_field.gif" runat="server" CssClass="cls_button"
                                                ToolTip="Search"></asp:Image>
                                        </td>
                                        <td style="padding-left: 5px; padding-right: 5px">
                                            <asp:Image ID="imgEnquirySeparator" runat="server" ImageUrl="~/images/toolbarseparator.gif" />
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgExport" ImageUrl="~/images/ico_update.gif" runat="server"
                                                CssClass="cls_button" ToolTip="Export"></asp:ImageButton>
                                        </td>
                                        <td style="width: 95%;">
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                    <ContentTemplate>
                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                        <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                            ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Team Mapping">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlList" runat="server">
                                                        <div class="tblctrlpanel" ></div>
                                                        <table width="100%" class="cls_panel">
                                                            <tr>
                                                                <td style="width: 45%;">
                                                                </td>
                                                                <td style="width: 10%;">
                                                                </td>
                                                                <td style="width: 45%;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="10%">
                                                                                <span id="lblSalesTeam" class="cls_label_header">Sales Team</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlSalesTeam" runat="server" AutoPostBack="true" CssClass="cls_dropdownlist">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="10%">
                                                                                <span id="lblTitle" class="cls_label_header">Title</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlTitle" runat="server" AutoPostBack="true" CssClass="cls_dropdownlist">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="center">
                                                                    <span id="lblQues" class="cls_label_header">Question</span><br>
                                                                    <asp:ListBox ID="lsbQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddQues" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveQues" runat="server" CssClass="cls_button" Width="35" Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllQues" runat="server" CssClass="cls_button" Width="35" Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllQues" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedQues" class="cls_label_header">Selected Question</span><br>
                                                                    <asp:ListBox ID="lsbSelectedQues" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" align="center">
                                                                    <span id="lblDestTeam" class="cls_label_header">Destination Team</span><br>
                                                                    <asp:ListBox ID="lsbDestTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddDestTeam" runat="server" CssClass="cls_button" Width="35" Text=">" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveDestTeam" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkAddAllDestTeam" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text=">>" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center">
                                                                                <asp:Button ID="lnkRemoveAllDestTeam" runat="server" CssClass="cls_button" Width="35"
                                                                                    Text="<<" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td align="center">
                                                                    <span id="lblSelectedDestTeam" class="cls_label_header">Selected Destination Team</span><br>
                                                                    <asp:ListBox ID="lsbSelectedDestTeam" CssClass="cls_listbox" runat="server" SelectionMode="Multiple"
                                                                        Height="130px" ForeColor="Black" Width="180px"></asp:ListBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Button ID="btnPopulate" runat="server" Text="Populate" CssClass="cls_button" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CPE_pnlList" runat="server" CollapseControlID="imgEnquiry"
                                                        ExpandControlID="imgEnquiry" TargetControlID="pnlList" CollapsedSize="0" Collapsed="false"
                                                        ExpandDirection="Vertical" SuppressPostBack="true">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                    <asp:Panel ID="pnldglist" runat="server">
                                                        <table width="100%">
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="cls_button" Visible="false" />
                                                                    <input id="btndgListCheckAll" type="button" value="Check All" onclick="SelectdgListAllCheckboxes();"
                                                                        runat="server" visible="false" class="cls_button" style="width: 80px" />
                                                                    <input id="btndgListUnCheckAll" type="button" value="UnCheck All" onclick="SelectdgListAllUnCheckboxes();"
                                                                        runat="server" visible="false" class="cls_button" style="width: 80px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <center>
                                                                        <%--  <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />--%>
                                                                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                            Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                            ShowFooter="false" AllowPaging="False" PagerSettings-Visible="False" DataKeyNames="TEAM_CODE,TITLE_CODE,QUES_CODE,SUB_QUES_CODE,CRITERIA_CODE">
                                                                        </ccGV:clsGridView>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport">
                            <td style="height: 5px">
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
