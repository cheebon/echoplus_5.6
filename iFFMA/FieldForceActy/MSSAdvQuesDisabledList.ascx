﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MSSAdvQuesDisabledList.ascx.vb" Inherits="iFFMA_FieldForceActy_MSSAdvQuesDisabledList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_MSSAdvQuesDisabledPop" Src="MSSAdvQuesDisabledPop.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>

<customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
<asp:UpdatePanel ID="UpdatePnlQuesDisabled" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="98%" >
            <tr>
                <td align="left" style="padding-left:15px">
                    <span style="float:left; width:12%; " class="cls_label_header">Team </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblTeamCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <span style="float:left; width:12%; " class="cls_label_header">Title </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblTitleCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <span style="float:left; width:12%; " class="cls_label_header">Question </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblQuesCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <span style="float:left; width:12%; " class="cls_label_header">Sub Question </span>
                    <span style="float:left; width:2%; " class="cls_label_header">:</span>
                    <asp:Label ID="lblSubQuesCode" runat="server" CssClass="cls_label_header"></asp:Label>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left:15px">
                    <asp:Panel ID="pnlCtrlAction" runat="server"> 
                        <asp:Button ID="btnAdd" CssClass="cls_button" runat="server" Text="Add" />
                    </asp:Panel> 
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <customToolkit:wuc_dgpaging ID="wuc_dgQuesDisabledPaging" runat="server" />
                        <ccGV:clsGridView ID="dgQuesDisabledList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            Width="98%" FreezeHeader="True" GridHeight="" AddEmptyHeaders="0" CellPadding="2"
                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false">
                        </ccGV:clsGridView>
                    </center>
                </td>
            </tr>
        </table>
                
        <asp:HiddenField ID="hdTeamCode" runat="server" Value="" />
        <asp:HiddenField ID="hdTitleCode" runat="server" Value="" />
        <asp:HiddenField ID="hdQuesCode" runat="server" Value="" />
        <asp:HiddenField ID="hdSubQuesCode" runat="server" Value="" />
                
        <customToolkit:wuc_MSSAdvQuesDisabledPop ID="wuc_MSSAdvQuesDisabledPop" Title="MSS Question Disabled Maintenance" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
