﻿
Imports System.Data
Imports mst_SalesAccount
Imports System.IO
Imports System.Configuration

Partial Class iFFMA_SalesAccount_CustProfileMaintenance_CustProfileMaintenancePhoto
    Inherits System.Web.UI.Page

    Public Property CustCode() As String
        Get
            Return ViewState("CustCode")
        End Get
        Set(ByVal value As String)
            ViewState("CustCode") = value
        End Set
    End Property

    Public Property UploadedFlag() As Boolean
        Get
            Return ViewState("UploadedFlag")
        End Get
        Set(ByVal value As Boolean)
            ViewState("UploadedFlag") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

        If Not IsPostBack Then
            CustCode = IIf(IsNothing(Request.QueryString("CustCode").Trim), "", Request.QueryString("CustCode").Trim)

            BindCustPhoto(CustCode)
        End If
        lblUploadInfo.Text = ""
    End Sub

#Region "Photo"
    Private Sub BindCustPhoto(ByVal strCustCode As String)
        Dim objCust As clsCustProfileMaintain
        Dim dt As DataTable
        Try
            objCust = New clsCustProfileMaintain
            dt = objCust.GetCustPhoto(strCustCode)


            If dt IsNot Nothing Then
                'If dt.Rows.Count > 0 Then
                If dt.Rows(0)("IMG_LOC").ToString.Trim <> "" Then
                    If File.Exists(Server.MapPath("~" + dt.Rows(0)("IMG_LOC").ToString)) Then
                        imgCust.ImageUrl = "~" & dt.Rows(0)("IMG_LOC").ToString & "?" & DateTime.Now.ToString("yyyyMMddHHmmss")
                        lblCustMsg.Text = "Picture of " + dt.Rows(0)("CUST_NAME").ToString

                        UploadedFlag = True
                    Else
                        imgCust.ImageUrl = "~" + "\images\anonymous.jpg"
                        lblCustMsg.Text = "The picture linked to " & dt.Rows(0)("CUST_NAME").ToString & " cannot be found. Please reupload new picture."

                        UploadedFlag = True
                    End If

                    EmbedConfirmation(UploadedFlag)
                Else
                    imgCust.ImageUrl = "~" + "\images\anonymous.jpg"
                    lblCustMsg.Text = "No picture has been linked to " & dt.Rows(0)("CUST_NAME").ToString
                    UploadedFlag = False
                End If
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            updPnlMaintenanceCustomerPhoto.Update()
        End Try
    End Sub

    Private Sub EmbedConfirmation(ByVal blnFlag As Boolean)
        Select Case blnFlag
            Case True
                btnUpload.OnClientClick = "return confirm('This customer has an existing picture. Do you wish to overwrite the picture?')"
            Case False
                btnUpload.OnClientClick = ""
        End Select

    End Sub
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim strPath As String = ""
        Try
            If UploadPhoto(strPath) Then
                SaveCustPhoto(CustCode, strPath)

                BindCustPhoto(CustCode)
            End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Private Function UploadPhoto(ByRef strPath As String) As Boolean
        Dim strCustProfilePath As String = System.Configuration.ConfigurationManager.AppSettings("CustProfileFilePath")
        Try
            If fuCustPhoto.HasFile Then
                Dim tmpExt As String = fuCustPhoto.FileName.Split(".", 3, StringSplitOptions.RemoveEmptyEntries)(1)

                If Not IsValidFileExtension(tmpExt) Then

                    lblUploadInfo.Text = "Selected file's extension is not allowed."
                    Return False
                End If

                If Not Directory.Exists(Server.MapPath("~" & strCustProfilePath & Session("dflCountryCode") & "/" & Session("CURRENT_PRINCIPAL_CODE") & "/")) Then
                    Directory.CreateDirectory(Server.MapPath("~" & strCustProfilePath & Session("dflCountryCode") & "/" & Session("CURRENT_PRINCIPAL_CODE") & "/"))
                End If


                fuCustPhoto.SaveAs(Server.MapPath("~" & strCustProfilePath & Session("dflCountryCode") & "/" & Session("CURRENT_PRINCIPAL_CODE") & "/" & CustCode & "." & tmpExt))




                'Return save path
                strPath = strCustProfilePath & Session("dflCountryCode") & "/" & Session("CURRENT_PRINCIPAL_CODE") & "/" & CustCode & "." & tmpExt

                Return True
            Else
                lblUploadInfo.Text = "Please select a file to upload."
            End If

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function IsValidFileExtension(ByVal strExtension As String) As Boolean
        Try
            If strExtension.ToUpper = "JPG" Or strExtension.ToUpper = "JPEG" Or strExtension.ToUpper = "PNG" Or strExtension.ToUpper = "GIF" Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub SaveCustPhoto(ByVal strCustCode As String, ByVal strImgLoc As String)
        Dim objCust As clsCustProfileMaintain
        Try
            objCust = New clsCustProfileMaintain
            objCust.SaveCustPhoto(strCustCode, strImgLoc)
        Catch ex As Exception
            Throw
        Finally
            objCust = Nothing
        End Try
    End Sub
#End Region
    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

End Class
