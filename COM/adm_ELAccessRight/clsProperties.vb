﻿''' <summary>
'''************************************************************************
'''	Author	    :	EJ
'''	Date	    :	2011-11-15
'''	Purpose	    :	Class EAsy Loader Accessright Properties
'''
'''	Revision	: 	
''' ------------------------------------------------------------------------
''' |No       |Date Change	|Author     	    |Remarks	   			 |	
''' ------------------------------------------------------------------------
''' |1	    |		    	| 	    	        |	    		    	 |
''' |2	    |			    |		            |		    		     |
''' ------------------------------------------------------------------------
'''*************************************************************************
'''  
''' </summary>
''' <remarks></remarks>
Public Class clsProperties
    Public Class clsQueryPpt
        Public user_type As String
        Public search_type As String
        Public search_value As String
        Public usr_id As String

        Public accessright_id As Double
        Public country_id As Double
        Public principal_id As Double
        Public module_id As Double
        Public type As Integer

        Public el_code As String
        Public cat_code As String
    End Class

    Public Class clsARPpt
        Public accessright_id As Double
        Public accessright_name As String
        Public accessright_name_1 As String
        Public accessright_desc As String
        Public accessright_desc_1 As String
        Public country_id As Double
        Public principal_id As Double

        Public module_id As Double
        Public submodule_id As Double

        Public cat_code As String
        Public el_code As String
    End Class
End Class
