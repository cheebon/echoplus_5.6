﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CustomerSalesTgtList.aspx.vb"
    Inherits="iFFMA_SalesAccount_CustomerSalesTgtList" %>

<%@ Register TagPrefix="customToolkit" TagName="wuc_toolbar" Src="~/include/wuc_toolbar.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblHeader" Src="~/include/wuc_lblHeader.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_lblInfo" Src="~/include/wuc_lblInfo.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_UpdateProgress" Src="~/include/wuc_UpdateProgress.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_dgpaging" Src="~/include/wuc_dgpaging.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerPriceGrpList" Src="CustomerPriceGrpList.ascx" %>
<%@ Register TagPrefix="customToolkit" TagName="wuc_CustomerContList" Src="CustomerContList.ascx" %>
<%@ Register TagPrefix="ccGV" Namespace="cor_CustomCtrl" Assembly="cor_CustomCtrl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Sales Target</title>
    <link href="~/include/DKSH.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function getDglist(element, idx) { var dglist; idx = idx + 1; if (element) { if (element.tagName == 'TABLE') { dglist = element; } else if (idx < 10) { dglist = getDglist(element.parentElement, idx); } } return dglist; }
        function ChangeAllCheckBoxStates(element) { var dglist = getDglist(element, 1); var checkState = element.status; for (var i = 1; i < dglist.rows.length; i++) { if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].type == "checkbox") dglist.rows[i].cells[0].childNodes[0].childNodes[0].checked = checkState; } }
        function ChangeHeaderCheckBoxStates(element) { var dglist = getDglist(element, 1); var checkTrueStateCount = 0; for (var i = 1; i < dglist.rows.length; i++) { if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].type == "checkbox") if (dglist.rows[i].cells[0].childNodes[0].childNodes[0].checked == true) { checkTrueStateCount = checkTrueStateCount + 1; } } if (checkTrueStateCount == dglist.rows.length - 1) { dglist.rows[0].cells[0].childNodes[0].childNodes[0].checked = true; } else { dglist.rows[0].cells[0].childNodes[0].childNodes[0].checked = false; } }
    </script>
</head>
<body class="BckgroundInsideContentLayout">
    <form id="frmcustsalestgt" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="300"
        ScriptMode="Release" />
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="BckgroundInsideContentLayout">
        <tr align="center">
            <td valign="top" style="width: 100%" class="BckgroundInsideContentLayout">
                <fieldset class="" style="width: 98%;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="Bckgroundreport">
                        <tr align="left">
                            <td>
                                <customToolkit:wuc_lblInfo ID="lblErr" runat="server" />
                                <customToolkit:wuc_lblHeader ID="wuc_lblHeader" runat="server" />
                                <customToolkit:wuc_toolbar ID="wuc_toolbar" runat="server" />
                                <br />
                                <customToolkit:wuc_UpdateProgress ID="general_UpdateProgress" runat="server" />
                                <asp:UpdatePanel ID="UpdateDatagrid" runat="server" UpdateMode="Conditional" RenderMode="block">
                                    <ContentTemplate>
                                        <asp:Timer ID="TimerControl1" runat="server" Enabled="False" Interval="100" OnTick="TimerControl1_Tick" />
                                        <ajaxToolkit:TabContainer ID="tcResult" runat="server" ActiveTabIndex="0" Height=""
                                            ScrollBars="Auto" Width="100%" CssClass="ajax__tab_portal">
                                            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlList" runat="server">
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="left" style="padding-left: 15px">
                                                                    <customToolkit:wuc_lblInfo ID="lblInfo" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="padding-left: 15px">
                                                                    <asp:Button ID="btnSave" CssClass="cls_button" runat="server" Text="Save Selected" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <center>
                                                                        <customToolkit:wuc_dgpaging ID="wuc_dgpaging" runat="server" />
                                                                        <ccGV:clsGridView ID="dgList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                            Width="98%" FreezeHeader="True" GridHeight="440" AddEmptyHeaders="0" CellPadding="2"
                                                                            CssClass="Grid" EmptyHeaderClass="" FreezeColumns="0" FreezeRows="0" GridWidth=""
                                                                            ShowFooter="false" AllowPaging="True" PagerSettings-Visible="false" DataKeyNames="YEAR,MONTH,CUST_CODE,SALESREP_CODE">
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="AllCheckBox" CssClass="cls_checkbox" runat="server" onclick="ChangeAllCheckBoxStates(this);" />
                                                                                    </HeaderTemplate>
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkSelection" runat="server" CssClass="cls_checkbox" onclick="ChangeHeaderCheckBoxStates(this);" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="YEAR" HeaderText="Year" ReadOnly="True"
                                                                                    SortExpression="YEAR">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MONTH" HeaderText="Month" ReadOnly="True"
                                                                                    SortExpression="MONTH">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SALESREP_CODE" HeaderText="Field Force Code" ReadOnly="True"
                                                                                    SortExpression="SALESREP_CODE">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SALESREP_NAME" HeaderText="Field Force Name" ReadOnly="True"
                                                                                    SortExpression="SALESREP_NAME">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code" ReadOnly="True"
                                                                                    SortExpression="CUST_CODE">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name" ReadOnly="True"
                                                                                    SortExpression="CUST_NAME">
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Sales Target">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtSalesTgt" runat="server" MaxLength="18" Width="150px" CssClass="cls_textbox" Text='<%#Bind("SALES_TGT") %>' ></asp:TextBox>
                                                                                        <itemstyle horizontalalign="Left" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </ccGV:clsGridView>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>
                                        </ajaxToolkit:TabContainer>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr class="Bckgroundreport">
                            <td style="height: 5px">
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
