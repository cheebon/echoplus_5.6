

'************************************************************************
'	Author	    :	Alex Chia
'	Date	    :	2008-06-10
'	Purpose	    :	
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB
Public Class clsSFPrdFreq
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsSFPrdFreq"
        End Get
    End Property

    Public Function GetSFPrdFreqList() As DataTable
        Dim clsIntDB As clsDB
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_PRD_FREQ"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", properties.SalesrepCode, clsDB.DataType.DBString, True)
                .addItem("STARTYEAR", properties.StartYear, clsDB.DataType.DBString)
                .addItem("STARTMONTH", properties.StartMonth, clsDB.DataType.DBString)
                .addItem("ENDYEAR", properties.EndYear, clsDB.DataType.DBString)
                .addItem("ENDMONTH", properties.EndMonth, clsDB.DataType.DBString)
                .addItem("PTL_CODE", properties.PTLCode, clsDB.DataType.DBString)
                .addItem("CLASS", properties.strClass, clsDB.DataType.DBString)

                GetSFPrdFreqList = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFPrdFreqList :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return GetSFPrdFreqList
    End Function

    Public Function GetSFPrdFreqSFMS() As DataTable
        Dim clsIntDB As clsDB
        Try
            clsIntDB = New clsDB
            With clsIntDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_SALESREP_SFMS_BY_CUST_CONT_CAT"
                .addItem("USER_ID", properties.UserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", properties.PrinID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", properties.PrinCode, clsDB.DataType.DBString)
                .addItem("SALESREP_CODE", properties.SalesrepCode, clsDB.DataType.DBString)
                .addItem("CUST_CODE", properties.CustCode, clsDB.DataType.DBString)
                .addItem("CONT_CODE", properties.ContCode, clsDB.DataType.DBString)
                .addItem("STARTYEAR", properties.StartYear, clsDB.DataType.DBString)
                .addItem("STARTMONTH", properties.StartMonth, clsDB.DataType.DBString)
                .addItem("ENDYEAR", properties.EndYear, clsDB.DataType.DBString)
                .addItem("ENDMONTH", properties.EndMonth, clsDB.DataType.DBString)
                .addItem("PTL_CODE", properties.PTLCode, clsDB.DataType.DBString)

                GetSFPrdFreqSFMS = .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetSFPrdFreqSFMS :" & ex.Message))
        Finally
            clsIntDB = Nothing
        End Try
        Return GetSFPrdFreqSFMS
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region
End Class
