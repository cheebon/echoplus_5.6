Imports System.Data
Partial Class iFFMS_Messaging_MessagingList
    Inherits System.Web.UI.Page
#Region "Local Variable"
    Private _aryDataItem As ArrayList

    Dim licItemFigureCollector As ListItemCollection

    Protected Property aryDataItemInbox() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemInbox")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemInbox") = value
        End Set
    End Property
    Protected Property aryDataItemOutbox() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItemOutbox")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItemOutbox") = value
        End Set
    End Property

    'Shared CriteriaCollector As New clsSharedValues
    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "iFFMS_MSG"

    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property

    Protected Sub StorePageCriteria(ByVal sender As Object, ByVal e As System.EventArgs) Handles clsCriteriaCollector.OnValueChanged
        Session(strCollectorName) = sender
    End Sub

#End Region

    Public Enum dgcolInbox As Integer
        CREATED_DATE = 3
        MSG_CODE = 4
        SENDER_ID = 5
        SENDER_NAME = 6
        SUBJECT = 7
        CONTENT = 8
    End Enum

    Public Enum dgcolOutbox As Integer
        CREATED_DATE = 2
        MSG_CODE = 3
        USER_ID = 4
        RECEIVER_NAME = 5
        SUBJECT = 6
        CONTENT = 7
    End Enum

    Public Enum dgcolDeleted As Integer
        CREATED_DATE = 1
        MSG_CODE = 2
        USER_ID = 3
        RECEIVER_NAME = 4
        SUBJECT = 5
        CONTENT = 6
    End Enum
#End Region

    Private intPageSize As Integer

    Public ReadOnly Property PageName() As String
        Get
            Return "MessagingList"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            'If Session("UserID") = "" Then
            '    Dim strScript As String = ""
            '    strScript = "self.parent.parent.location='../../../../../../Echoplus/login.aspx?ErrMsg=Session Time Out, Please login again !!!';"
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "PopupScript", "" & strScript & "", True)
            'End If

            With wuc_lblheader
                .Title = "Message"
                .DataBind()
                .Visible = True
            End With

            If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then Session("SALESREP_CODE") = GetSalesrepCode()

            If Not Page.IsPostBack Then
                TimerControl1.Enabled = True
                LoadTxnNo()
            End If


        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        Finally

        End Try
    End Sub

    Private Function GetSalesrepCode() As String

        Dim SalesrepCode As String = Nothing

        If Not String.IsNullOrEmpty(Session("UserID")) Then
            Dim strUserid As String
            strUserid = Web.HttpContext.Current.Session("UserID")

            Dim clssalemancode As New txn_WebActy.clsCommon
            SalesrepCode = clssalemancode.GetSRCode(strUserid)

        End If

        Return SalesrepCode
    End Function

#Region "DGLISTInbox"
    Private Sub RefreshDatabindingInbox()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewInbox"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionInbox"), String)

        dtCurrenttable = GetRecListInbox()

        If dtCurrenttable Is Nothing Then
            dtCurrenttable = New DataTable
        Else
            If dtCurrenttable.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                btncheckallinbox.Visible = False
                btnuncheckallinbox.Visible = False
                btndeleteinbox.Visible = False
            Else
                btncheckallinbox.Visible = True
                btnuncheckallinbox.Visible = True
                btndeleteinbox.Visible = True
            End If
        End If

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListInbox.DataSource = dvCurrentView
        dgListInbox.DataBind()

        UpdateInbox.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Private Function GetRecListInbox() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then

            Dim strSalesrepCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strSalesrepCode = Trim(Session("SALESREP_CODE"))

            Dim clsMessage As New txn_Message.clsMessage
            DT = clsMessage.GetInbox(strSalesrepCode, strUserid)

        End If

        Return DT
    End Function

    Protected Sub dgList_SortingInbox(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListInbox.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionInbox")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionInbox") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingInbox()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgListInbox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgListInbox.SelectedIndexChanged
        Dim index As Integer
        index = Convert.ToInt32(dgListInbox.SelectedIndex)

        lblDate.Text = dgListInbox.Rows(index).Cells(dgcolInbox.CREATED_DATE).Text()
        lblIdtxt.Text = "Sender Id"
        lblId.Text = dgListInbox.Rows(index).Cells(dgcolInbox.SENDER_ID).Text()
        lblNametxt.Text = "Sender Name"
        lblName.Text = dgListInbox.Rows(index).Cells(dgcolInbox.SENDER_NAME).Text()
        lblSubject.Text = dgListInbox.Rows(index).Cells(dgcolInbox.SUBJECT).Text()
        lblMsgContent.Text = dgListInbox.Rows(index).Cells(dgcolInbox.CONTENT).Text()
        tcResult.ActiveTabIndex = 3
        ResetLblMsg()
        UpdateView.Update()
        UpdatePage.Update()
    End Sub

    Protected Sub dgListInbox_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgListInbox.RowEditing
        Dim index As Integer
        index = Convert.ToInt32(e.NewEditIndex)

        txtSenderId.Text = dgListInbox.Rows(index).Cells(dgcolInbox.SENDER_ID).Text()
        Txtsubject.Text = "Re:" + dgListInbox.Rows(index).Cells(dgcolInbox.SUBJECT).Text()
        txtcontent.Text = "" + vbCrLf + vbCrLf + vbCrLf + "Sent by: " + dgListInbox.Rows(index).Cells(dgcolInbox.SENDER_ID).Text() + vbCrLf + dgListInbox.Rows(index).Cells(dgcolInbox.CONTENT).Text()
        tcResult.ActiveTabIndex = 4
        ResetLblMsg()
        UpdateCompose.Update()
        UpdatePage.Update()
    End Sub
#End Region

#Region "DGLISTOutbox"
    Private Sub RefreshDatabindingOutbox()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewOutbox"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionOutbox"), String)

        dtCurrenttable = GetRecListOutbox()

        If dtCurrenttable Is Nothing Then
            dtCurrenttable = New DataTable
        Else
            If dtCurrenttable.Rows.Count = 0 Then
                'dtCurrentTableDtl.Rows.Add(dtCurrentTableDtl.NewRow())
                btncheckalloutbox.Visible = False
                btnuncheckalloutbox.Visible = False
                btndeleteoutbox.Visible = False
            Else
                btncheckalloutbox.Visible = True
                btnuncheckalloutbox.Visible = True
                btndeleteoutbox.Visible = True
            End If
        End If

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListOutbox.DataSource = dvCurrentView
        dgListOutbox.DataBind()

        UpdateOutBox.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Private Function GetRecListOutbox() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then

            Dim strSalesrepCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strSalesrepCode = Trim(Session("SALESREP_CODE"))

            Dim clsMessage As New txn_Message.clsMessage
            DT = clsMessage.GetOutbox(strSalesrepCode, strUserid)

        End If

        Return DT
    End Function

    Protected Sub dgListOutbox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgListOutbox.SelectedIndexChanged
        Dim index As Integer
        index = Convert.ToInt32(dgListOutbox.SelectedIndex)

        lblDate.Text = dgListOutbox.Rows(index).Cells(dgcolOutbox.CREATED_DATE).Text()
        lblIdTxt.Text = "Receiver Id"
        lblId.Text = dgListOutbox.Rows(index).Cells(dgcolOutbox.USER_ID).Text()
        lblNameTxt.Text = "Receiver Name"
        lblName.Text = dgListOutbox.Rows(index).Cells(dgcolOutbox.RECEIVER_NAME).Text()
        lblSubject.Text = dgListOutbox.Rows(index).Cells(dgcolOutbox.SUBJECT).Text()
        lblMsgContent.Text = dgListOutbox.Rows(index).Cells(dgcolOutbox.CONTENT).Text()
        tcResult.ActiveTabIndex = 3
        ResetLblMsg()
        UpdateView.Update()
        UpdatePage.Update()
    End Sub

    Protected Sub dgList_SortingOutbox(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListOutbox.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionOutbox")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionOutbox") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingOutbox()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "DGLISTDeleted"
    Private Sub RefreshDatabindingDeleted()
        Dim dtCurrenttable As Data.DataTable = CType(ViewState("dtCurrentViewDeleted"), Data.DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpressionDeleted"), String)

        dtCurrenttable = GetRecListDeleted()

        Dim dvCurrentView As New Data.DataView(dtCurrenttable)
        If Not String.IsNullOrEmpty(strSortExpression) Then
            Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
            dvCurrentView.Sort = IIf(dtCurrenttable.Columns.Contains(strSortExpressionName), strSortExpression, "")
        End If
        dgListDeleted.DataSource = dvCurrentView
        dgListDeleted.DataBind()

        UpdateDeleted.Update()
        'Try
        '    

        'Catch ex As Exception
        '    ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        'Finally
        '    

        'End Try
    End Sub

    Private Function GetRecListDeleted() As Data.DataTable
        Dim DT As DataTable = Nothing
        If Not String.IsNullOrEmpty(Session("UserID")) Then

            Dim strSalesrepCode As String, strUserid As String
            strUserid = Trim(Web.HttpContext.Current.Session("UserID"))
            strSalesrepCode = Trim(Session("SALESREP_CODE"))

            Dim clsMessage As New txn_Message.clsMessage
            DT = clsMessage.GetDeletedbox(strSalesrepCode, strUserid)

        End If

        Return DT
    End Function

    Protected Sub dgListDeleted_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgListDeleted.SelectedIndexChanged
        Dim index As Integer
        index = Convert.ToInt32(dgListDeleted.SelectedIndex)

        lblDate.Text = dgListDeleted.Rows(index).Cells(dgcolDeleted.CREATED_DATE).Text()
        lblIdTxt.Text = "Receiver Id"
        lblId.Text = dgListDeleted.Rows(index).Cells(dgcolDeleted.USER_ID).Text()
        lblNameTxt.Text = "Receiver Name"
        lblName.Text = dgListDeleted.Rows(index).Cells(dgcolDeleted.RECEIVER_NAME).Text()
        lblSubject.Text = dgListDeleted.Rows(index).Cells(dgcolDeleted.SUBJECT).Text()
        lblMsgContent.Text = dgListDeleted.Rows(index).Cells(dgcolDeleted.CONTENT).Text()
        tcResult.ActiveTabIndex = 3
        ResetLblMsg()
        UpdateView.Update()
        UpdatePage.Update()
    End Sub

    Protected Sub dgList_SortingDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgListDeleted.Sorting
        Dim strSortExpression As String = ViewState("strSortExpressionDeleted")
        Try
            If strSortExpression IsNot Nothing AndAlso Not String.IsNullOrEmpty(strSortExpression) Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            ViewState("strSortExpressionDeleted") = strSortExpression
            CriteriaCollector.SortExpression = strSortExpression
            RefreshDatabindingDeleted()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

#Region "COMPOSE MSG"
    Private Sub LoadTxnNo()
        Dim msgcode As String
        msgcode = lblMsgCode.Text

        Dim clsMessage As New txn_Message.clsMessage
        Dim dt As DataTable
        dt = clsMessage.GetTRANo(Trim(Session("SALESREP_CODE")))
        lblMsgCode.Text = dt.Rows(0)("TXN_NO")

        lbldatecompose.Text = Format(Now(), "yyyy-MM-dd")
    End Sub
#End Region

#Region "Event Handler"
    Protected Sub btnsend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsend.Click
        InsertMsg("P")
        RefreshDatabindingOutbox()
        'tcResult.ActiveTabIndex = 1
        lblcomposemsg.Text = "Message sent !"
        lblinboxmsg.Text = ""
        lbloutboxmsg.Text = ""
        ResetMsg()
        UpdatePage.Update()
    End Sub

    Protected Sub btndeleteinbox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeleteinbox.Click
        Try
            If dgListInbox.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim exists As Boolean = False
                Dim strMsgCode As String, strReceiverName As String, strUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dgListInbox.Rows

                    DK = dgListInbox.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dgListInbox.Rows(i).FindControl("chkdeleteinbox"), CheckBox)

                        If chkdelete.Checked = True Then
                            strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
                            strMsgCode = Trim(dgListInbox.DataKeys(i).Item("MSG_CODE"))
                            strReceiverName = Trim(IIf(IsDBNull(dgListInbox.DataKeys(i).Item("SENDER_ID")), "", dgListInbox.DataKeys(i).Item("SENDER_ID")))

                            Dim clsMessage As New txn_Message.clsMessage
                            clsMessage.DelMSGInbox(strMsgCode, strReceiverName, strUserId)
                            exists = True
                        End If

                    End If

                    i += 1
                Next
                TimerControl1.Enabled = True

                If exists = True Then
                    lblcomposemsg.Text = ""
                    lblinboxmsg.Text = "Message(s) deleted"
                    lbloutboxmsg.Text = ""
                End If
            End If

         

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub btndeleteoutbox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndeleteoutbox.Click
        Try
            If dgListOutbox.Rows.Count > 0 Then
                Dim i As Integer = 0
                Dim strMsgCode As String, strReceiverName As String, strUserId As String

                Dim DK As DataKey
                For Each DR As GridViewRow In dgListOutbox.Rows

                    DK = dgListOutbox.DataKeys(i)
                    If DK IsNot Nothing AndAlso Not String.IsNullOrEmpty(DK(0)) Then

                        Dim chkdelete As CheckBox = CType(dgListOutbox.Rows(i).FindControl("chkdeleteoutbox"), CheckBox)

                        If chkdelete.Checked = True Then
                            strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
                            strMsgCode = Trim(dgListOutbox.DataKeys(i).Item("MSG_CODE"))
                            strReceiverName = Trim(IIf(IsDBNull(dgListOutbox.DataKeys(i).Item("USER_ID")), "", dgListOutbox.DataKeys(i).Item("USER_ID")))

                            Dim clsMessage As New txn_Message.clsMessage
                            clsMessage.DelMSGOutBox(strMsgCode, strReceiverName, strUserId)
                        End If

                    End If

                    i += 1
                Next
                TimerControl1.Enabled = True
            End If

            lblcomposemsg.Text = ""
            lblinboxmsg.Text = ""
            lbloutboxmsg.Text = "Message(s) deleted"
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


#End Region


    Private Sub ResetMsg()
        LoadTxnNo()
        txtSenderId.Text = ""
        Txtsubject.Text = ""
        txtcontent.Text = ""
        UpdateCompose.Update()
    End Sub

    Private Sub ResetLblMsg()
        lblcomposemsg.Text = ""
        lblinboxmsg.Text = ""
        lbloutboxmsg.Text = ""
    End Sub

    Private Sub InsertMsg(ByVal strtxnstatus As String)
        Dim strMsgCode As String, strReceiverName As String, strSubject As String, strContent As String, strUserId As String

        strMsgCode = Trim(lblMsgCode.Text)
        strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
        strReceiverName = Trim(txtSenderId.Text)
        strSubject = Trim(Txtsubject.Text)
        strContent = Trim(txtcontent.Text)

        Dim clsMessage As New txn_Message.clsMessage
        clsMessage.InstMSG(strMsgCode, strReceiverName, strSubject, strContent, StrTxnStatus, strUserId)
    End Sub

    'Private Sub DeleteMsg(ByVal strtxnstatus As String)
    '    Dim strMsgCode As String, strReceiverName As String, strUserId As String

    '    strMsgCode = Trim(lblMsgCode.Text)
    '    strUserId = Trim(Web.HttpContext.Current.Session("UserID"))
    '    strReceiverName = Trim(txtSenderId.Text)

    '    Dim clsMessage As New txn_Message.clsMessage
    '    clsMessage.DelMSG(strMsgCode, strReceiverName, strUserId)
    'End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)
        If TimerControl1.Enabled Then
            RefreshDatabindingInbox()
            RefreshDatabindingOutbox()
            RefreshDatabindingDeleted()
        End If
        TimerControl1.Enabled = False
        UpdatePage.Update()
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub

  
   
End Class
