'************************************************************************
'	Author	    :	Ze-Ming Ng
'	Date	    :	30/10/2006
'	Purpose	    :	Class to build datatables for getting DRC transaction details
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
'Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsDRC
    Implements IDisposable

    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private strFFMSConn As String = CStr(Web.HttpContext.Current.Session("ffms_conn"))


    Public ReadOnly Property ClassName() As String
        Get
            Return "clsDRC"
        End Get
    End Property

    Function getProductGroupDT(ByVal strSalesmanCode As String) As DataTable
        Dim dt As Data.DataTable = Nothing
        Dim objdb As New cor_DB.clsDB

        Try
            With objdb
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_WEB_DRCPRDLISTING"
                .addItem("salesmancode", strSalesmanCode, 2)
                dt = .spRetrieve
            End With
            Return dt
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getProductGroupDT :" & ex.Message))
        Finally
            objdb = Nothing
        End Try

    End Function

    Function getCartDT(ByVal strSalesmanCode As String, ByVal strCustCode As String, ByVal strType As String) As DataTable
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_DRCCARTDTL"
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("type", strType, 2)
                Return .spRetrieve
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".getProductGroupDT :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Function

    Sub insertDRCHeader(ByVal strTxnno As String, ByVal strCustCode As String, ByVal strContactCode As String, ByVal strBranchCode As String, ByVal strSalesmanCode As String, ByVal strVisitID As String)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_DRCINSERTHDR"
                .addItem("txnno", strTxnno, 2)
                .addItem("custcode", strCustCode, 2)
                .addItem("contactcode", strContactCode, 2)
                .addItem("branchcode", strBranchCode, 2)
                .addItem("salesmancode", strSalesmanCode, 2)
                .addItem("visitid", strVisitID, 2)
                .addItem("txntime", FormatDateTime(DateTime.Now, DateFormat.ShortTime), 2)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertDRCHeader :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub insertDRCDetail(ByVal strTxnNo As String, ByVal strItemCode As String, ByVal intOpenBal As Integer, ByVal intShelf As Integer, ByVal intStore As Integer, ByVal intPo As Integer, ByVal intICO As Integer, ByVal intAMS As Integer)
        Dim obj As New cor_DB.clsDB

        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_DRCINSERTDTL"
                .addItem("txnno", strTxnNo, 2)
                .addItem("itemcode", strItemCode, 2)
                .addItem("openbal", intOpenBal, 1)
                .addItem("shelf", intShelf, 1)
                .addItem("store", intStore, 1)
                .addItem("po", intPo, 1)
                .addItem("ico", intICO, 1)
                .addItem("ams", intAMS, 1)
                .spInsert()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".insertDRCHeader :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub

    Sub deleteDRCHeaderAndDetail(ByVal strTxnno As String)
        Dim obj As New cor_DB.clsDB
        Try
            With obj
                .ConnectionString = strFFMSConn
                .CmdText = "SPP_TXN_DRCDELETEHDRDET"
                .addItem("txnno", strTxnno, 2)
                .spDelete()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".deleteDRCHeaderAndDetail :" & ex.Message))
        Finally
            obj = Nothing
        End Try
    End Sub


    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub
End Class
