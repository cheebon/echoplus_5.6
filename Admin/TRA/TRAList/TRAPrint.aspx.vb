﻿Imports System.Data

Imports System
Imports System.IO
Imports System.Text
Imports System.Drawing.Imaging
Imports System.Drawing.Printing
Imports System.Collections.Generic
Imports Microsoft.Reporting.WebForms
Partial Class Admin_TRA_TRAList_TRAPrint
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UpdatePrintNo()
            Print()

        End If


    End Sub
    Public Sub UpdatePrintNo()
        Dim strTxnNo As String
        strTxnNo = Request.QueryString("TXN_NO")
        Dim clsTRA As New adm_TRA.clsTRA
        With clsTRA
            .clsProperties.txn_no = strTxnNo
            .clsProperties.user_code = Session("UserCode")
            .UpdTRAPrintNo()
        End With

    End Sub

    Public Sub Print()
        Try

            Dim strTxnNo As String
            strTxnNo = Request.QueryString("TXN_NO")

            Dim clsTRA As New adm_TRA.clsTRA
            Dim dtDtl As DataTable
            Dim dtHdr As DataTable

            With clsTRA
                .clsProperties.txn_no = strTxnNo
                .clsProperties.user_code = Session("UserCode")
                dtHdr = .GetTRAListHdrReport
            End With

            With clsTRA
                .clsProperties.txn_no = strTxnNo
                .clsProperties.user_code = Session("UserCode")
                dtDtl = .GetTRAListDtlReport
            End With
            ReportViewer1.ProcessingMode = ProcessingMode.Local
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.Dispose()
            ReportViewer1.LocalReport.ReportPath = "Admin/TRA/TRAList/TRA.rdlc"
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WebForms.ReportDataSource("SAP_TRA_SPP_RPT_SAP_TRA_HDR", dtHdr))
            ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WebForms.ReportDataSource("SAP_TRA_SPP_RPT_SAP_TRA_DTL", dtDtl))
            ReportViewer1.ZoomMode = ZoomMode.Percent
            ReportViewer1.LocalReport.Refresh()


        Catch ex As Exception

        End Try
    End Sub

End Class
