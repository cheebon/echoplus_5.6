Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
'Imports System.EnterpriseServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("cor_ClassTemplate")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("DKSH CSSC Sdn Bhd")> 
<Assembly: AssemblyProduct("")> 
<Assembly: AssemblyCopyright("")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 
'<Assembly: AssemblyKeyFile("..\..\cor_ClassTemplate.snk")> 
'<Assembly: ApplicationName("iFlex")> 
'<Assembly: ApplicationActivation(ActivationOption.Library)> 
'<Assembly: ApplicationAccessControl(AccessChecksLevelOption.Application)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9B0566B8-1AF3-477E-9E85-10A8671A7B9B")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.*")> 
