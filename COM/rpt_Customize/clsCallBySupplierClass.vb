'************************************************************************
'	Author	    :	Puah Hong Ling
'	Date	    :	09/06/2008
'	Purpose	    :	Class to build Query for report Call by Supplier Class.
'
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************

Option Explicit On
Option Strict On

Imports Microsoft.VisualBasic
Imports System.ComponentModel
Imports System.Data
Imports cor_DB

Public Class clsCallBySupplierClass
    Implements IDisposable

    Private strConn As String = CStr(Web.HttpContext.Current.Session("ffmr_conn"))
    Public properties As New clsProperties

    Public ReadOnly Property ClassName() As String
        Get
            Return "clsCallBySupplierClass"
        End Get
    End Property

    Public Function GetCallBySupplierClass(ByVal strUserID As String, ByVal strPrincipal_ID As String, ByVal strPrincipal_CODE As String, ByVal strYear As String, ByVal strMonth As String, ByVal strSalesRepList As String, ByVal intSelection As Integer) As DataTable
        Dim clsCustomizeDB As clsDB
        Try
            clsCustomizeDB = New clsDB
            With clsCustomizeDB
                .ConnectionString = strConn
                .CmdText = "SPP_RPT_CALL_BY_SUPPLIER_CLASS"
                .addItem("USER_ID", strUserID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_ID", strPrincipal_ID, clsDB.DataType.DBString)
                .addItem("PRINCIPAL_CODE", strPrincipal_CODE, clsDB.DataType.DBString)
                .addItem("YEAR", strYear, clsDB.DataType.DBString)
                .addItem("MONTH", strMonth, clsDB.DataType.DBString)
                .addItem("TREE_SALESREP_LIST", strSalesRepList, clsDB.DataType.DBDouble, True)
                .addItem("SELECTION", CStr(intSelection), clsDB.DataType.DBInt)
                Return .spRetrieve()
            End With
        Catch ex As Exception
            Throw (New ExceptionMsg(ClassName & ".GetCallBySupplierClass :" & ex.Message))
        Finally
            clsCustomizeDB = Nothing
        End Try
    End Function

#Region "Internal"
    Private handle As IntPtr            ' Pointer to an external unmanaged resource.
    Private Components As Component     ' Other managed resource this class uses.
    Private disposed As Boolean = False ' Track whether Dispose has been called.
    Private Class ExceptionMsg
        Inherits System.Exception
        Public Sub New(ByVal msg As String)
            MyBase.New(msg)
            'Call error log class
        End Sub
    End Class

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not (Me.disposed) Then
            If (disposing) Then
                Components.Dispose()
            End If
            CloseHandle(handle)
            handle = IntPtr.Zero
        End If
        Me.disposed = True
    End Sub

    <System.Runtime.InteropServices.DllImport("Kernel32")> _
     Private Shared Function CloseHandle(ByVal handle As IntPtr) As [Boolean]
    End Function

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Close()
        Dispose()
    End Sub

#End Region

End Class

