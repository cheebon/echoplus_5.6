
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web.HttpContext

Partial Class iFFMA_HCD_MssCont_MssContHdr
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Private intPageSize As Integer
    Dim licItemFigureCollector As ListItemCollection

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Private Property Master_Row_Count() As Integer
        Get
            Return CInt(ViewState("Master_Row_Count"))
        End Get
        Set(ByVal value As Integer)
            ViewState("Master_Row_Count") = value
        End Set
    End Property
#End Region

    Public Enum dgcol As Integer
        TXN_NO = 1
        TXN_DATE = 2
        SALESREP_CODE = 3
        SALESREP_NAME = 4
        CUST_CODE = 5
        CUST_NAME = 6
        CONT_CODE = 7
        CONT_NAME = 8
        TITLE_CODE = 9
        TITLE_NAME = 10
        STATUS = 11
    End Enum

    Public ReadOnly Property PageName() As String
        Get
            Return "MssContactList.aspx"
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            intPageSize = CInt(IIf(IsNumeric(Session("PageSize")), CInt(Session("PageSize")), 15))

            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.MSS_CONT)
                .DataBind()
                .Visible = True
            End With

            'Toolbar
            With wuc_pnlStdBar
                .SubModuleID = SubModuleType.MSS_CONT
                If Not IsPostBack Then
                    .DataBind()
                End If
                .Visible = True
            End With

            'Call Paging
            'With wuc_dgpaging
            '    .PageCount = dgList.PageCount
            '    '.CurrentPageIndex = dgList.PageIndex
            '    .DataBind()
            '    .Visible = True
            'End With

            If Not IsPostBack Then
                TimerControl1.Enabled = True
                ScriptManager.RegisterClientScriptInclude(Page, GetType(Page), "LayoutJs", "/" & ConfigurationManager.AppSettings("ServerName") & "/include/layout.js")

            End If

            lblErr.Text = ""


        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub TimerControl1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerControl1.Tick
        If TimerControl1.Enabled Then RefreshDatabinding()
        TimerControl1.Enabled = False
    End Sub

#Region "EVENT HANDLER"

#End Region

#Region "DATA BIND"
    Public Sub RenewDataBind()
        'HL:20080527
        dgList.PageIndex = 0
        wuc_dgpaging.PageNo = 1

        ViewState.Clear()
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
    End Sub

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim clsMssCont As New mst_Contact.clsMssCont
            Dim strTxnNo As String, strSalesrepCode As String, strCustCode As String, strContCode As String, strTitleCode As String, _
            strStatus As String, strDateStart As String, strDateEnd As String

            strTxnNo = Request.QueryString("TXN_NO")
            strSalesrepCode = Request.QueryString("SALESREP_CODE")
            strCustCode = Request.QueryString("CUST_CODE")
            strContCode = Request.QueryString("CONT_CODE")
            strTitleCode = Request.QueryString("TITLE_CODE")
            strStatus = Request.QueryString("STATUS")
            strDateStart = Request.QueryString("DATE_START")
            strDateEnd = Request.QueryString("DATE_END")

            DT = clsMssCont.GetMssContList(strTxnNo, strSalesrepCode, strCustCode, strContCode, strTitleCode, strStatus, strDateStart, strDateEnd)

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function
#End Region

#Region "DGLIST"
    Public Sub RefreshDatabinding(Optional ByVal isExport As Boolean = False) 'HL:20080424
        Dim dtCurrentTable As DataTable = Nothing 'CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)

        Try
            'If dtCurrentTable Is Nothing Then
            dtCurrentTable = GetRecList()

            '    ViewState("dtCurrentView") = dtCurrentTable
            '    ViewState("strSortExpression") = Nothing
            '    dgList.PageIndex = 0
            '    wuc_dgpaging.PageNo = 1 'HL:20080327
            'End If

            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                    Master_Row_Count = 0
                Else
                    Master_Row_Count = dtCurrentTable.Rows.Count
                End If
            End If


            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            With dgList
                .DataSource = dvCurrentView
                .PageSize = intPageSize
                '.AllowSorting = IIf(isExport, False, IIf(Master_Row_Count > 0, True, False)) 'HL:20080424
                .DataBind()
            End With

            'Call Paging
            With wuc_dgpaging
                .PageCount = dgList.PageCount
                .CurrentPageIndex = dgList.PageIndex
                .DataBind()
                .Visible = IIf(Master_Row_Count > 0, True, False)
            End With

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If


            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Select Case e.Row.RowType
            Case DataControlRowType.DataRow
                If Master_Row_Count > 0 Then
                    '    Dim strstatusCode As String
                    '    strstatusCode = Trim(e.Row.Cells(dgcol.STATUS).Text)
                    '    Dim btnedit As ImageButton = CType(e.Row.FindControl("btnEdit"), ImageButton)
                    '    If strstatusCode.ToUpper = "APPROVED" Then

                    '         btnedit.Visible = False
                    '    Else
                    '        btnEdit.Visible = True
                    '    End If

                End If
        End Select
    End Sub

    Protected Sub dgList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgList.SelectedIndexChanged
        Dim strTxnNo As String, strSalesrepCode As String, strCustCode As String, strContCode As String, strTitleCode As String
        Dim index As Integer = dgList.SelectedIndex

        strTxnNo = sender.datakeys(index).item("TXN_NO")
        strSalesrepCode = sender.datakeys(index).item("SALESREP_CODE")
        strCustCode = sender.datakeys(index).item("CUST_CODE")
        strContCode = sender.datakeys(index).item("CONT_CODE")
        strTitleCode = sender.datakeys(index).item("TITLE_CODE")
        Dim StrScript As String
        StrScript = "parent.document.getElementById('DetailBarIframe').src='../../iFFMA/HCD/MssCont/MssContDtl.aspx?TXN_NO=" + strTxnNo + "&SALESREP_CODE=" + strSalesrepCode + "&CUST_CODE=" + strCustCode + "&CONT_CODE=" + strContCode + "&TITLE_CODE=" + strTitleCode + "';"
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "RedirectUrl", StrScript, True)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType, "HideTopShowDetail", "HideElement('TopBar');HideElement('ContentBar');ShowElement('DetailBar'); MaximiseFrameHeight('DetailBarIframe')", True)
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            'CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression

            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgList.RowEditing
        Try
            'Dim strSelectedCode As String = sender.datakeys(e.NewEditIndex).item("CONT_CODE")
            'Dim DT As DataTable
            'Dim clsContact As New mst_Contact.clsContact
            'Dim lstSelected As ListItem
            'lstSelected = Nothing

            'DT = clsContact.GetContDetails(strSelectedCode)

            'If DT.Rows.Count > 0 AndAlso DT.Rows(0)("LOCK_FLAG") = "0" Then
            '    ViewState("CONT_CODE") = strSelectedCode
            '    txtContCode.Text = IIf(IsDBNull(DT.Rows(0)("CONT_CODE")), "", DT.Rows(0)("CONT_CODE"))
            '    txtContName.Text = IIf(IsDBNull(DT.Rows(0)("CONT_NAME")), "", DT.Rows(0)("CONT_NAME"))
            '    ddlStatus.SelectedValue = IIf(IsDBNull(DT.Rows(0)("STATUS")), "", DT.Rows(0)("STATUS"))

            '    txtTitle.Text = IIf(IsDBNull(DT.Rows(0)("TITLE")), "", DT.Rows(0)("TITLE"))
            '    txtLastName.Text = IIf(IsDBNull(DT.Rows(0)("LAST_NAME")), "", DT.Rows(0)("LAST_NAME"))
            '    txtFirstName.Text = IIf(IsDBNull(DT.Rows(0)("FIRST_NAME")), "", DT.Rows(0)("FIRST_NAME"))
            '    txtChristianName.Text = IIf(IsDBNull(DT.Rows(0)("CHRISTIAN_NAME")), "", DT.Rows(0)("CHRISTIAN_NAME"))

            '    ddlGender.SelectedIndex = -1
            '    lstSelected = ddlGender.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("GENDER")), "", DT.Rows(0)("GENDER")))
            '    If lstSelected IsNot Nothing Then lstSelected.Selected = True

            '    txtBirthDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("BIRTH_DATE")), "", DT.Rows(0)("BIRTH_DATE")))

            '    ddlMaritalStatus.SelectedIndex = -1
            '    lstSelected = ddlMaritalStatus.Items.FindByValue(IIf(IsDBNull(DT.Rows(0)("MARITAL_STATUS")), "", DT.Rows(0)("MARITAL_STATUS")))
            '    If lstSelected IsNot Nothing Then lstSelected.Selected = True

            '    txtMedSchool.Text = IIf(IsDBNull(DT.Rows(0)("MED_SCHOOL")), "", DT.Rows(0)("MED_SCHOOL"))

            '    txtGradDate.Text = String.Format("{0:yyyy-MM-dd}", IIf(IsDBNull(DT.Rows(0)("GRAD_DATE")), "", DT.Rows(0)("GRAD_DATE")))

            '    txtEmail.Text = IIf(IsDBNull(DT.Rows(0)("EMAIL")), "", DT.Rows(0)("EMAIL"))
            '    txtTelNo1.Text = IIf(IsDBNull(DT.Rows(0)("TEL_NO_1")), "", DT.Rows(0)("TEL_NO_1"))
            '    txtTelNo2.Text = IIf(IsDBNull(DT.Rows(0)("TEL_NO_2")), "", DT.Rows(0)("TEL_NO_2"))

            '    ActivateEditTab()
            'Else
            '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Error", "<script>alert('This record is locked by other user!');</script>", False)
            '    RenewDataBind()
            'End If

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub


#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_pnlStdBar.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding(True) 'HL:20080424

            wuc_pnlStdBar.ExportToFile(dgList, wuc_lblHeader.Title.ToString.Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub
#End Region

#Region "Paging Control"
    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub btnGo_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub btnGo_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Go_Click
        Try
            dgList.PageIndex = CInt(wuc_dgpaging.PageNo - 1)

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkPrevious_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkPrevious_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Previous_Click
        Try
            If dgList.PageIndex > 0 Then
                dgList.PageIndex = dgList.PageIndex - 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Procedure         : 	Sub lnkNext_OnClick
    ' Purpose	        :	This Sub manipulate the page change via user conntrol
    ' Calling Methods   :   1) ...
    '                       2) ...
    ' Parameters: [in]  : 
    '		      [out] : 
    '---------------------------------------------------------------------------------------------------------

    Sub lnkNext_OnClick(ByVal s As Object, ByVal e As EventArgs) Handles wuc_dgpaging.Next_Click
        Try
            If dgList.PageCount - 1 > dgList.PageIndex Then
                dgList.PageIndex = dgList.PageIndex + 1
            End If
            wuc_dgpaging.PageNo = dgList.PageIndex + 1

            dgList.EditIndex = -1
            RefreshDatabinding()
            Exit Sub

        Catch ex As Exception
            ExceptionMsg(ex.TargetSite.ReflectedType.Name & "." & ex.TargetSite.Name & " : " & ex.ToString)
        End Try
    End Sub
#End Region

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

