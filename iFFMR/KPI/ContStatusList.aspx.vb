'************************************************************************
'	Author	    :	Yong Soo Fong
'	Date	    :	14/03/2016
'	Purpose	    :	Show Contact Status
'	Revision	: 	
' ------------------------------------------------------------------------
' |No       |Date Change	|Author     	    |Remarks	   			 |	
' ------------------------------------------------------------------------
' |1	    |		    	| 	    	        |	    		    	 |
' |2	    |			    |		            |		    		     |
' ------------------------------------------------------------------------
'*************************************************************************
Imports System.Data

Partial Class ContStatusList
    Inherits System.Web.UI.Page

#Region "Local Variable"
    Dim licItemFigureCollector As ListItemCollection

    Private _licCustomHeaderCollector As ListItemCollection
    Protected Property licHeaderCollector() As ListItemCollection
        Get
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = Session("HeaderCollector_" & PageName)
            If _licCustomHeaderCollector Is Nothing Then _licCustomHeaderCollector = New ListItemCollection
            Return _licCustomHeaderCollector
        End Get
        Set(ByVal value As ListItemCollection)
            _licCustomHeaderCollector = value
            Session("HeaderCollector_" & PageName) = value
        End Set
    End Property

    Private _aryDataItem As ArrayList
    Protected Property aryDataItem() As ArrayList
        Get
            If _aryDataItem Is Nothing Then _aryDataItem = ViewState("DataItem")
            If _aryDataItem Is Nothing Then _aryDataItem = New ArrayList
            Return _aryDataItem
        End Get
        Set(ByVal value As ArrayList)
            ViewState("DataItem") = value
        End Set
    End Property

    Dim WithEvents clsCriteriaCollector As clsSharedValue
#Region "Criteria Collector"
    Dim strCollectorName As String = "Collector_CustStatus"
    Private Property CriteriaCollector() As clsSharedValue
        Get
            'clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = Session(strCollectorName)
            If clsCriteriaCollector Is Nothing Then clsCriteriaCollector = New clsSharedValue
            Return clsCriteriaCollector
        End Get
        Set(ByVal value As clsSharedValue)
            clsCriteriaCollector = value
            Session(strCollectorName) = clsCriteriaCollector
        End Set
    End Property
#End Region

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public ReadOnly Property PageName() As String
        Get
            Return "ContStatus.aspx"
        End Get
    End Property

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("HeaderCollector_" & PageName)
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Try
            'Call Header
            With wuc_lblHeader
                .Title = Report.GetName(SubModuleType.CONTKPI) 'Contact KPI
                .DataBind()
                .Visible = True
            End With

            'Call Panel
            With wuc_ctrlpanel
                .SubModuleID = SubModuleType.CUSTSTATUS
                .DataBind()
                .Visible = True
            End With

            If Not IsPostBack Then
                BindDDLYear()

                If PreviousPage IsNot Nothing Then
                    With CriteriaCollector
                        Session.Item("Year") = .Year
                        Session.Item("Month") = .Month
                        Session("PRINCIPAL_ID") = .PrincipalID
                        Session("PRINCIPAL_CODE") = .PrincipalCode
                        Session("SALESREP_CODE") = .SalesrepCode
                        Session("NetValue") = .NetValue
                        If .ReportType < ddlReportType.Items.Count Then ddlReportType.SelectedIndex = .ReportType
                        ViewState("strSortExpression") = .SortExpression
                        wuc_Menu.RestoreSessionState = True

                    End With
                Else
                    CriteriaCollector = Nothing
                    ChangeReportType()
                    If String.IsNullOrEmpty(Session("SALESREP_CODE")) Then wuc_Menu.TreeMenu_Visibility = True 'ActivateSearchBtn_Click(Nothing, Nothing)
                End If
                licHeaderCollector = Nothing
            End If
            lblErr.Text = ""

        Catch ex As Exception
            ExceptionMsg(PageName & ".Page_Load : " & ex.ToString)
        End Try
    End Sub

#Region "EVENT HANDLER"
    Protected Sub ActivateSelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_Menu.SelectedNodeChanged
        Try
            wuc_ctrlpanel.RefreshDetails()
            wuc_ctrlpanel.UpdateControlPanel()
            'If IsPostBack Then RenewDataBind()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateSelectedNodeChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.RefreshQueryDate_Click
        Try
            ChangeReportType()
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub
    Private Sub BindDDLYear()
        Dim liLast2Year As New ListItem
        liLast2Year.Text = Date.Now.AddYears(-2).Year
        liLast2Year.Value = Date.Now.AddYears(-2).Year
        ddlYear.Items.Add(liLast2Year)

        Dim liLastYear As New ListItem
        liLastYear.Text = Date.Now.AddYears(-1).Year
        liLastYear.Value = Date.Now.AddYears(-1).Year
        ddlYear.Items.Add(liLastYear)

        Dim liThisYear As New ListItem
        liThisYear.Text = Date.Now.Year
        liThisYear.Value = Date.Now.Year
        ddlYear.Items.Add(liThisYear)
    End Sub
    Private Sub ChangeReportType()
        Try

            Dim dt As DataTable
            Dim clsCallQuery As New rpt_CALL.clsCallQuery

            dt = clsCallQuery.GetMonthlyCallRptType(Session.Item("Year"), Session.Item("Month"), Session.Item("UserID"))
            If dt.Rows.Count > 0 Then
                If dt.Rows(0)(0) = "2" Then 'ddlReportType.SelectedIndex = dt.Rows(0)(0) = "2" Then
                    ddlReportType.SelectedIndex = 1
                Else
                    ddlReportType.SelectedIndex = dt.Rows(0)(0)
                End If

                If ddlYear.Items.Contains(New ListItem(dt.Rows(0)("START_YEAR").ToString, dt.Rows(0)("START_YEAR").ToString)) Then ddlYear.SelectedValue = dt.Rows(0)("START_YEAR").ToString 'ddlYear.Items.FindByValue(dt.Rows(0)("START_YEAR").ToString).Selected = True
            End If

        Catch ex As Exception
            ExceptionMsg(PageName & ".ChangeReportType : " & ex.ToString)
        End Try
    End Sub

    Protected Sub ActivateRenewDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.NetValue_Changed
        Try
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRenewDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ActivateRefreshDatabinding(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.FieldConfig_Changed
        Try
            RefreshDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateRefreshDatabinding : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub LayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.LayoutChanged
        UpdateDatagrid_Update()
    End Sub
#End Region

#Region "Export Extender"
    Protected Sub ActivateExportBtnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles wuc_ctrlpanel.ExportBtn_Click
        Dim objStringWriter As New System.IO.StringWriter
        Dim objHtmlTextWriter As New System.Web.UI.HtmlTextWriter(objStringWriter)
        Try
            Dim blnAllowSorting As Boolean = dgList.AllowSorting
            Dim blnAllowPaging As Boolean = dgList.AllowPaging

            dgList.AllowSorting = False
            dgList.AllowPaging = False
            RefreshDatabinding()

            wuc_ctrlpanel.ExportToFile(dgList, (wuc_lblHeader.Title.ToString & ddlReportType.SelectedItem.Text).Replace(" ", "_"))

            dgList.AllowPaging = blnAllowPaging
            dgList.AllowSorting = blnAllowSorting
            RefreshDatabinding()
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            ExceptionMsg(PageName & ".ActivateExportBtnClick : " & ex.ToString)
        End Try
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ''Hide this function to prevent internal asp.net error
        ''To enable export function
        'MyBase.VerifyRenderingInServerForm(control)
    End Sub

#End Region

#Region "Databinding"
    Public Sub RenewDataBind()
        ViewState("dtCurrentView") = Nothing
        RefreshDataBind()
    End Sub

    Public Sub RefreshDataBind()
        RefreshDatabinding()
        wuc_ctrlpanel.RefreshDetails()
        wuc_ctrlpanel.UpdateControlPanel()
    End Sub

    Public Sub RefreshDatabinding()
        lblMessage.Visible = String.IsNullOrEmpty(Session("SALESREP_CODE"))

        Dim dtCurrentTable As DataTable = CType(ViewState("dtCurrentView"), DataTable)
        Dim strSortExpression As String = CType(ViewState("strSortExpression"), String)
        Try
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = GetRecList()
                ViewState("dtCurrentView") = dtCurrentTable
                ViewState("strSortExpression") = Nothing
            End If
            If dtCurrentTable Is Nothing Then
                dtCurrentTable = New DataTable
            Else
                PreRenderMode(dtCurrentTable)
                If dtCurrentTable.Rows.Count = 0 Then
                    dtCurrentTable.Rows.Add(dtCurrentTable.NewRow())
                End If
            End If


            Dim dvCurrentView As New DataView(dtCurrentTable)
            If Not String.IsNullOrEmpty(strSortExpression) Then
                Dim strSortExpressionName As String = strSortExpression.Replace(" DESC", "")
                dvCurrentView.Sort = IIf(dtCurrentTable.Columns.Contains(strSortExpressionName), strSortExpression, "")
            End If

            dgList.DataSource = dvCurrentView
            dgList.DataBind()
        Catch ICE As InvalidCastException
            'due to add new row
        Catch ex As Exception
            ExceptionMsg(PageName & ".RefreshDatabinding : " & ex.ToString)
        Finally
            UpdateDatagrid_Update()
        End Try
    End Sub

    Public Sub UpdateDatagrid_Update()
        Try
            If dgList.Rows.Count < 15 Then
                dgList.GridHeight = Nothing
            End If
            UpdateDatagrid.Update()
        Catch ex As Exception
            ExceptionMsg(PageName & ".UpdateDatagrid_Update : " & ex.ToString)
        End Try
    End Sub

#End Region

#Region "CUSTOM DGLIST"
    Protected Sub dgList_Init(ByRef dtToBind As DataTable)
        Dim i As Integer
        Dim columnDataType As System.Type = Nothing

        Try
            'Add Data Grid Columns
            dgList.Columns.Clear()
            aryDataItem.Clear()
            Dim ColumnName As String = ""
            For i = 0 To dtToBind.Columns.Count - 1
                ColumnName = dtToBind.Columns(i).ColumnName.ToUpper
                Select Case CF_ContStatus.GetFieldColumnType(ColumnName)
                    Case FieldColumntype.InvisibleColumn

                    Case FieldColumntype.BoundColumn
                        Dim dgColumn As New BoundField 'BoundColumn

                        dgColumn.ReadOnly = True

                        dgColumn.HeaderStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.ItemStyle.VerticalAlign = VerticalAlign.Top
                        dgColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center

                        Dim strFormatString As String = CF_ContStatus.GetOutputFormatString(ColumnName)
                        If String.IsNullOrEmpty(strFormatString) = False Then
                            dgColumn.DataFormatString = strFormatString
                            dgColumn.HtmlEncode = False
                        End If
                        dgColumn.ItemStyle.HorizontalAlign = CF_ContStatus.ColumnStyle(ColumnName).HorizontalAlign

                        dgColumn.HeaderText = CF_ContStatus.GetDisplayColumnName(ColumnName, ddlReportType.SelectedValue)
                        dgColumn.DataField = ColumnName
                        dgColumn.SortExpression = ColumnName
                        dgList.Columns.Add(dgColumn)
                        dgColumn = Nothing

                        ''Add the field name
                        aryDataItem.Add(ColumnName)
                End Select
            Next
            ViewState("DataItem") = aryDataItem
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowCreated
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim oGridView As GridView = dgList 'CType(sender, GridView)
                Dim GVR As GridViewRow = New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                Dim TC As TableHeaderCell
                Dim CF As ListItem
                Dim intCounter As Integer = 0
                Dim intActualIndex As Integer = 0

                For Each CF In licHeaderCollector
                    If CF.Value = 1 Then
                        Dim iIndex As Integer = licHeaderCollector.IndexOf(CF)
                        intActualIndex = IIf(intCounter > 0, intCounter, 0)
                        If iIndex >= 0 Then
                            e.Row.Cells(intActualIndex).RowSpan = 2
                            e.Row.Cells(intActualIndex).VerticalAlign = VerticalAlign.Middle
                            GVR.Cells.Add(e.Row.Cells(intActualIndex))
                        End If
                    Else
                        TC = New TableHeaderCell
                        TC.Text = CF.Text
                        TC.ColumnSpan = CF.Value
                        intCounter += CF.Value
                        GVR.Cells.Add(TC)
                    End If
                Next
                oGridView.Controls(0).Controls.AddAt(0, GVR)
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

            End If
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowCreated : " & ex.ToString)
        End Try
    End Sub

    Protected Sub dgList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgList.RowDataBound
        Try
            Select Case e.Row.RowType
                Case DataControlRowType.Header
                Case DataControlRowType.DataRow
                    For Each TC As TableCell In e.Row.Cells
                        If IsNumeric(TC.Text) AndAlso CDbl(TC.Text) = 0 Then TC.ForeColor = Drawing.Color.Red
                    Next
                Case DataControlRowType.Footer
                    Dim iIndex As Integer

                    For Each li As ListItem In licItemFigureCollector
                        iIndex = aryDataItem.IndexOf(li.Text)
                        If iIndex > 0 Then
                            e.Row.Cells(iIndex).Text = String.Format(CF_ContStatus.GetOutputFormatString(li.Text), IIf(IsNumeric(li.Value), CDbl(li.Value), 0))
                        End If
                    Next
            End Select
            ViewState("DataItem") = aryDataItem

        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_RowDataBound : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Function SUM(ByVal Value1 As Object, ByVal Value2 As Object) As Double
        Try
            Dim iValue1 As Double = ConvertToDouble(Value1)
            Dim iValue2 As Double = ConvertToDouble(Value2)
            Return iValue1 + iValue2
        Catch ex As Exception
            ExceptionMsg(PageName & ".SUM : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function ConvertToDouble(ByVal objValue As Object) As Double
        Try
            Dim dblValue As Double = 0
            If IsNumeric(objValue) Then dblValue = Convert.ToDouble(objValue)

            Return dblValue
        Catch ex As Exception
            ExceptionMsg(PageName & ".ConvertToDouble : " & ex.ToString)
        Finally
        End Try
    End Function

    Private Function GetRecList() As DataTable
        Dim DT As DataTable = Nothing
        Try
            Dim strUserID, strPrincipalID, strPrincipalCode, strYear, strStartYear, strMonth, strSalesrepCode As String
            Dim intNetValue As Integer

            strUserID = Session.Item("UserID")
            strPrincipalCode = Session("PRINCIPAL_CODE")
            strPrincipalID = Session("PRINCIPAL_ID")
            strYear = Session.Item("Year")
            'add startyear to get from a new ddl, HK ES year cycle requirement
            strStartYear = ddlYear.SelectedValue '
            strMonth = Session.Item("Month")
            strSalesrepCode = Session("SALESREP_CODE")
            intNetValue = IIf(CInt(Session("NetValue")) = 0, 1, CInt(Session("NetValue")))

            'Stored Criteria into Static Value Collector
            With CriteriaCollector
                .ReportType = ddlReportType.SelectedIndex
                .Year = strYear
                .StartYear = strStartYear
                .Month = strMonth
                .PrincipalCode = strPrincipalCode
                .PrincipalID = strPrincipalID
                .SalesrepCode = strSalesrepCode
                .NetValue = intNetValue
            End With

            Dim clsCallDB As New rpt_CALL.clsCallQuery
            DT = clsCallDB.GetContStatus(strUserID, strPrincipalID, strPrincipalCode, strStartYear, strSalesrepCode, ddlReportType.SelectedValue, intNetValue)

            'dgList_Init(DT)
            PreRenderMode(DT)
        Catch ex As Exception
            ExceptionMsg(PageName & ".GetRecList : " & ex.ToString)
        Finally
        End Try
        Return DT
    End Function

    Private Sub PreRenderMode(ByRef DT As DataTable)
        Try
            aryDataItem.Clear()
            dgList_Init(DT)

            Cal_CustomerHeader()
            Cal_ItemFigureCollector(DT)


        Catch ex As Exception
            ExceptionMsg(PageName & ".PreRenderMode : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_CustomerHeader()
        Try
            'aryDataItem will refresh each time rebind
            'aryColumnFieldCollector will refresh only reget the data from the database
            licHeaderCollector = New ListItemCollection

            Dim blnisNew As Boolean
            Dim strColumnName As String
            Dim liColumnField As ListItem
            Dim aryCuttedName As New ArrayList
            Dim intMonth As Integer

            'collect the column name that ready to show on the screen
            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper.Replace("MONTH", "").Replace("CALL", "")
                strColumnName = strColumnName.Replace("_", "")
                strColumnName = strColumnName.Replace("TTL", "Total")

                If IsNumeric(strColumnName) Then
                    If ddlReportType.SelectedValue() = 1 Then
                        intMonth = CInt(strColumnName) + 6
                    Else
                        intMonth = CInt(strColumnName)
                    End If
                    strColumnName = CF_ContStatus.GetMonthName(intMonth)
                End If

                aryCuttedName.Add(strColumnName)
            Next

            'witht the cutted columnName, fill in the Occorance count
            For Each strColumnName In aryCuttedName
                blnisNew = True
                liColumnField = licHeaderCollector.FindByText(strColumnName)

                If Not liColumnField Is Nothing Then
                    liColumnField.Value = CInt(liColumnField.Value) + 1
                Else
                    liColumnField = New ListItem(strColumnName, 1)
                    'liColumnField.Text = strColumnName
                    'liColumnField.Value = 1
                    licHeaderCollector.Add(liColumnField)
                End If

            Next
            licHeaderCollector = _licCustomHeaderCollector
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_CustomerHeader : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub Cal_ItemFigureCollector(ByRef DT As DataTable)
        'used to get the collection list of column that need to sum the figure
        Try
            licItemFigureCollector = New ListItemCollection
            Dim strColumnName As String
            Dim liColumnField As ListItem
            For Each strColumnName In aryDataItem
                strColumnName = strColumnName.ToUpper
                If strColumnName Like "*_CALL" Then
                    liColumnField = New ListItem(strColumnName, 0)
                    licItemFigureCollector.Add(liColumnField)
                End If
            Next

            For Each DR As DataRow In DT.Rows
                For Each li As ListItem In licItemFigureCollector
                    li.Value = SUM(li.Value, DR(li.Text))
                Next
            Next
        Catch ex As Exception
            ExceptionMsg(PageName & ".Cal_ItemFigureCollector : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub dgList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgList.Sorting
        Dim strSortExpression As String = ViewState("strSortExpression")
        Try
            If strSortExpression IsNot Nothing AndAlso strSortExpression.Length > 0 Then
                If strSortExpression Like (e.SortExpression & "*") Then
                    If strSortExpression.IndexOf(" DESC") > 0 Then
                        strSortExpression = e.SortExpression
                    Else
                        strSortExpression = e.SortExpression & " DESC"
                    End If
                Else
                    strSortExpression = e.SortExpression
                End If
            Else
                strSortExpression = e.SortExpression
            End If
            CriteriaCollector.SortExpression = strSortExpression
            ViewState("strSortExpression") = strSortExpression
            RefreshDatabinding()
        Catch ex As Exception
            ExceptionMsg(PageName & ".dgList_Sorting : " & ex.ToString)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        Try
            ViewState("strSortExpression") = Nothing
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlReportType_SelectedIndexChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Try
            ViewState("strSortExpression") = Nothing
            RenewDataBind()
        Catch ex As Exception
            ExceptionMsg(PageName & ".ddlYear_SelectedIndexChanged : " & ex.ToString)
        Finally
        End Try
    End Sub

    Private Sub ExceptionMsg(ByVal strMsg As String)
        Try
            lblErr.Text = ""
            lblErr.Text = strMsg

            'Call error log class
            Dim objLog As cor_Log.clsLog
            objLog = New cor_Log.clsLog
            With objLog
                .clsProperties.LogTypeID = 1
                .clsProperties.DateLogIn = Now
                .clsProperties.DateLogOut = Now
                .clsProperties.SeverityID = 4
                .clsProperties.LogMsg = strMsg
                .Log()
            End With
            objLog = Nothing

        Catch ex As Exception

        End Try
    End Sub
End Class

Public Class CF_ContStatus

    Public Shared Function GetDisplayColumnName(ByVal ColumnName As String, ByRef ReportType As String) As String
        Dim strColumnName As String = ""

        If ReportType = "0" Then
            If ColumnName Like "MONTH_1_CALL" Then
                strColumnName = "JAN"

            ElseIf ColumnName Like "MONTH_2_CALL" Then
                strColumnName = "FEB"

            ElseIf ColumnName Like "MONTH_3_CALL" Then
                strColumnName = "MAR"

            ElseIf ColumnName Like "MONTH_4_CALL" Then
                strColumnName = "APR"

            ElseIf ColumnName Like "MONTH_5_CALL" Then
                strColumnName = "MAY"

            ElseIf ColumnName Like "MONTH_6_CALL" Then
                strColumnName = "JUN"

            ElseIf ColumnName Like "TTL_CALL" Then
                strColumnName = "TOTAL"
            Else
                strColumnName = Report.GetDisplayColumnName(ColumnName)
            End If
        Else
            If ColumnName Like "MONTH_1_CALL" Then
                strColumnName = "JULY"

            ElseIf ColumnName Like "MONTH_2_CALL" Then
                strColumnName = "AUG"

            ElseIf ColumnName Like "MONTH_3_CALL" Then
                strColumnName = "SEPT"

            ElseIf ColumnName Like "MONTH_4_CALL" Then
                strColumnName = "OCT"

            ElseIf ColumnName Like "MONTH_5_CALL" Then
                strColumnName = "NOV"

            ElseIf ColumnName Like "MONTH_6_CALL" Then
                strColumnName = "DEC"

            ElseIf ColumnName Like "TTL_CALL" Then
                strColumnName = "TOTAL"

            Else
                strColumnName = Report.GetDisplayColumnName(ColumnName)
            End If
        End If
        Return strColumnName

    End Function

    Public Shared Function GetFieldColumnType(ByVal strColumnName As String) As FieldColumntype
        Try
            Dim FCT As FieldColumntype = FieldColumntype.BoundColumn
            Dim strDisplayMode As String = HttpContext.Current.Session("FieldConfig")
            strColumnName = strColumnName.ToUpper
            Dim enDisplayMode As DisplayMode = IIf(String.IsNullOrEmpty(strDisplayMode), DisplayMode.NameOnly, CType(strDisplayMode, DisplayMode))
            If (enDisplayMode = DisplayMode.NameOnly AndAlso strColumnName Like "*_CODE") OrElse _
               (enDisplayMode = DisplayMode.CodeOnly AndAlso strColumnName Like "*_NAME") Then
                FCT = FieldColumntype.InvisibleColumn
            Else
                FCT = FieldColumntype.BoundColumn
            End If
            Return FCT
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function GetOutputFormatString(ByVal strColumnName As String) As String
        Dim strStringFormat As String = ""
        Dim strNewName As String = strColumnName.ToUpper
        Try
            If strNewName Like "*_CALL" Then
                strStringFormat = "{0:0}"
            Else
                strStringFormat = ""
            End If

        Catch ex As Exception
        End Try

        Return strStringFormat
    End Function

    Public Shared Function ColumnStyle(ByVal ColumnName As String) As ColumnStyle
        Dim CS As New ColumnStyle
        Try
            With CS
                Dim strColumnName As String = ColumnName.ToUpper
                .FormatString = GetOutputFormatString(ColumnName)

                If strColumnName Like "*_CODE" OrElse strColumnName Like "*DATE*" OrElse strColumnName Like "*_NO" OrElse strColumnName Like "TIME*" OrElse strColumnName Like "DURATION" Then
                    .HorizontalAlign = HorizontalAlign.Center
                ElseIf Not String.IsNullOrEmpty(.FormatString) Then
                    .HorizontalAlign = HorizontalAlign.Right
                Else
                    .HorizontalAlign = HorizontalAlign.Left
                End If

            End With

        Catch ex As Exception

        End Try
        Return CS
    End Function

    Public Shared Function GetMonthName(ByVal intMonth As Integer) As String
        Dim strMonthValue As String = ""
        Try
            Select Case intMonth
                Case 1
                    strMonthValue = "JAN"
                Case 2
                    strMonthValue = "FEB"
                Case 3
                    strMonthValue = "MAR"
                Case 4
                    strMonthValue = "APR"
                Case 5
                    strMonthValue = "MAY"
                Case 6
                    strMonthValue = "JUN"
                Case 7
                    strMonthValue = "JULY"
                Case 8
                    strMonthValue = "AUG"
                Case 9
                    strMonthValue = "SEPT"
                Case 10
                    strMonthValue = "OCT"
                Case 11
                    strMonthValue = "NOV"
                Case 12
                    strMonthValue = "DEC"
                Case Else
                    strMonthValue = "UNKNOWN"

            End Select

        Catch ex As Exception

        End Try
        Return strMonthValue
    End Function
End Class